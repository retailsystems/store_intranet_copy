using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;

namespace SecurityPrinting
{
    class TktFileManager
    {
        private System.Collections.Specialized.NameValueCollection _appSettings;
        private string _ticketFile;
        private string _ppticketFile;
        private string _fileFolder;
        private string _fieldSeperator = ",";
        private string _packRet = "\"\"";  
        private string _archiveFolder = "";
        private string _SMTPServer = "";
        private string _emailSender = "";
        private string _emailReceivers = "";
        private string _emailBody = "";
        private string _emailSubject = "";
        private string _v_curr_packno = "";
        private string _v_curr_ponum = "";
		private string _storedProcedure = string.Empty;

        //string[] _fileArr;
        //string[] _venFileArr;
        private static HTError.ErrorHandler _err;

        public TktFileManager(HTError.ErrorHandler err)
        {
            _appSettings = ConfigurationManager.AppSettings;
            _fileFolder = _appSettings["FileFolder"];
            if (!_fileFolder.EndsWith(@"\")) _fileFolder += @"\";
            _archiveFolder = _appSettings["ArchiveFolder"];
            if (!_archiveFolder.EndsWith(@"\")) _archiveFolder += @"\";
            // _fileArr = _appSettings["FILE_ARR"].Split(',');
            // _venFileArr = _appSettings["VEN_FILE_ARR"].Split(',');
            _SMTPServer = _appSettings["SMTPServer"];
            _emailSender = _appSettings["EmailSender"];
            _emailReceivers = _appSettings["EmailReceivers"];
            _emailBody = _appSettings["EmailBody"];
            _emailSubject = _appSettings["EmailSubject"];
            _err = err;
			_storedProcedure = _appSettings["RMS_PROCEDURE"];
        }
        public void CreateTktFiles()
        {
            try
            {
                // create Hottopic Full company Ticket files 
                CreateTktFile();

                // create Hottopic Pre Pack Ticket files 
                //CreatePrepackFile();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CreateTktFile()
        {
            try
            {
                _ticketFile = _fileFolder + GetFilename();
                _err.LogInformation("Creating ticket file:" + _ticketFile);

                DataSet ds = DataAccess.GetTicketDetailsByCompany(_storedProcedure);
                if (File.Exists(_ticketFile))
                {
                    File.Delete(_ticketFile);
                }
                using (StreamWriter sw = File.CreateText(_ticketFile))
                {
                    //sw.Write(GetStringVal("Test"));

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();

                        sb.Append(GetStringVal(dr["dept"].ToString()) + _fieldSeperator);
                        sb.Append(GetStringVal(dr["supplier"].ToString()) + _fieldSeperator);
                       // sb.Append(GetStringVal(dr["item_parent"].ToString()) + _fieldSeperator);
                        sb.Append(GetStringVal(dr["item"].ToString()) + _fieldSeperator);
                        sb.Append(dr["order_no"].ToString() + _fieldSeperator);
                        if (string.Equals(dr["ticket_type_id"], "PK"))
                        {
                           sb.Append(_packRet+_fieldSeperator);
                        }
                        else
                        {
                          if (dr["unit_retail"].ToString().Length > 0)
                          {
                            sb.Append(decimal.Parse(dr["unit_retail"].ToString()).ToString("#0.00") + _fieldSeperator);
                          }
                          else
                          {
                            sb.Append(_fieldSeperator);
                          }
                        }
                        sb.Append(DateTime.Parse(dr["earliest_ship_date"].ToString()).ToString("MM/dd/yyyy") + _fieldSeperator);
                        sb.Append(GetStringVal(dr["sku_num"].ToString()) + _fieldSeperator);
                        sb.Append(GetStringVal(dr["sku_num2"].ToString().Replace("-", "").Trim()) + _fieldSeperator);
                        sb.Append(dr["qty_ordered"].ToString() + _fieldSeperator);
                        sb.Append(GetStringVal(dr["short_desc"].ToString()) + _fieldSeperator);
                        sb.Append(GetStringVal(dr["item_size"].ToString()) + _fieldSeperator);
                        sb.Append(GetStringVal(dr["ticket_type_id"].ToString()) + _fieldSeperator);
                        sb.Append(GetStringVal(dr["rpt_cd"].ToString()) + _fieldSeperator);
                        sb.Append(GetStringVal(dr["UDF029"].ToString()) + _fieldSeperator);
                        sb.Append(GetStringVal(dr["UDF083"].ToString()));
                        sw.WriteLine(sb.ToString());
                    }
                    sw.Close();

                }
                //email file
                _err.LogInformation("Emailing ticket file:" + _ticketFile);
                SendMail(_ticketFile, GetCompanyName());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CreatePrepackFile()
        {
            try
            {
                _ppticketFile = _fileFolder + GetPPFilename();
                _err.LogInformation("Creating PrePack ticket file:" + _ppticketFile);

                DataSet ds = DataAccess.GetPrePackTicket();
                if (File.Exists(_ppticketFile))
                {
                    File.Delete(_ppticketFile);
                }
                using (StreamWriter sw = File.CreateText(_ppticketFile))
                {

                    _v_curr_packno = "";
                    _v_curr_ponum = "";
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        if ((_v_curr_packno != dr["PACK_NO"].ToString()) || (_v_curr_ponum != dr["PO_NUM"].ToString()))
                        {
                            sw.WriteLine(GetStringVal("Head") + "," + GetStringVal(dr["PO_NUM"].ToString()) + "," + GetStringVal(dr["PACK_NO"].ToString()) + "," + GetStringVal(dr["PACK_UPC"].ToString()) + "," + dr["ORD_QTY"] + "," + GetStringVal(dr["PACK_DESC"].ToString()) + "," + GetStringVal(dr["SUPPLIER"].ToString()) + "," + DateTime.Parse(dr["ORD_SHIP_DATE"].ToString()).ToString("MM/dd/yyyy"));
                            _v_curr_packno = dr["PACK_NO"].ToString();
                            _v_curr_ponum = dr["PO_NUM"].ToString();
                        }
                        sb.Append(GetStringVal("Detail") + _fieldSeperator);
                        sb.Append(GetStringVal(dr["PACK_NO"].ToString()) + _fieldSeperator);
                        sb.Append(GetStringVal(dr["COMP_STYLE"].ToString()) + _fieldSeperator);
                        sb.Append(GetStringVal(dr["COMP_SKU"].ToString()) + _fieldSeperator);
                        sb.Append(GetStringVal(dr["ITEM_SIZE"].ToString()) + _fieldSeperator);
                        sb.Append(dr["COMP_QTY"].ToString() + _fieldSeperator);
                        sb.Append(GetStringVal(dr["COMP_DESC"].ToString()));
                        sw.WriteLine(sb.ToString());
                    }
                    sw.Close();

                }
                //email file
                _err.LogInformation("Emailing Pre pack ticket file:" + _ppticketFile);
                SendMail(_ppticketFile, GetPPCompanyName());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        private void SendMail(string fileName, string company)
        {
            //string company = GetCompanyName(divCd);
            // Create a message and set up the recipients.
            MailMessage message = new MailMessage();
            //message.Sender = new MailAddress(_emailSender);
            message.Subject = _emailSubject.Replace("%company%", company);
            message.IsBodyHtml = true;
            message.From = new MailAddress(_emailSender);
            message.Body = _emailBody.Replace("%loc%", fileName);
            // Create  the file attachment for this e-mail message.
            Attachment data = new Attachment(fileName);
            // Add the file attachment to this e-mail message.
            message.Attachments.Add(data);
            message.To.Add(_emailReceivers);
            //Send the message.
            SmtpClient client = new SmtpClient(_SMTPServer);
            // Add credentials if the SMTP server requires them.
            //client.Credentials = CredentialCache.DefaultNetworkCredentials;
            client.Send(message);
        }

        public void ArchiveFiles()
        {
            //_archiveFolder += DateTime.Now.ToString("MMddyyHHmi") + @"\";
            string archivePath = _archiveFolder + DateTime.Now.ToString("MMddyyHHmm") + @"\";
            DirectoryInfo diSource = new DirectoryInfo(_fileFolder);
            DirectoryInfo diTarget = new DirectoryInfo(archivePath);
            if (diSource.Exists)
            {
                //System.Collections.IEnumerator ienum = diSource.GetFiles("*.txt").GetEnumerator();
                System.Collections.IEnumerator ienum = diSource.GetFiles("*.*").GetEnumerator();
                while (ienum.MoveNext())
                {
                    if (!diTarget.Exists) diTarget.Create();
                    FileInfo fi = (FileInfo)ienum.Current;
                    FileInfo fiTarget = new FileInfo(archivePath + fi.Name);
                    if (fiTarget.Exists) fiTarget.Delete();
                    fi.MoveTo(archivePath + fi.Name);
                }
            }
        }

        private string GetFilename()
        {
            //string fileName = "HotTopicFull.txt";
            string fileName = "Hottopic" + DateTime.Now.ToString("MMddyyHHmmss") + ".txt";
            return fileName;
        }

        private string GetPPFilename()
        {
            //string fileName = "HotTopicFull.txt";
            string fileName = "Hottopic_Prepack" + DateTime.Now.ToString("MMddyyHHmmss") + ".txt";
            return fileName;
        }

        private string GetCompanyName()
        {
            string companyName = "HotTopic";
            return companyName;
        }

        private string GetPPCompanyName()
        {
            string companyName = "HotTopic PrePack ";
            return companyName;
        }

        private string GetStringVal(string colVal)
        {
            return '"' + colVal + '"';
        }

    }
}
