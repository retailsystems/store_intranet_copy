using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Security;
using System.Diagnostics;
using System.Configuration;
namespace SecurityPrinting
{
    class DataAccess
    {
        public static DataSet GetTicketDetailsByCompany(string StoredProcedure)
        {
            DataSet ds = null;
            System.Data.OracleClient.OracleCommand command = null;
            System.Data.OracleClient.OracleConnection conn = null;
            System.Data.OracleClient.OracleDataAdapter dataAdapter = null;
                  
            try
            {
                conn = new System.Data.OracleClient.OracleConnection();
                conn.ConnectionString = ConfigurationManager.AppSettings["RMS_CONN"].ToString();

                command = new System.Data.OracleClient.OracleCommand(StoredProcedure, conn);
                command.CommandType = CommandType.StoredProcedure;

                System.Data.OracleClient.OracleParameter p2 = new System.Data.OracleClient.OracleParameter();
                p2.OracleType = OracleType.Cursor;
                p2.ParameterName = "p_ticket_cur";
                p2.Direction = ParameterDirection.Output;
                command.Parameters.Add(p2);                

                dataAdapter = new System.Data.OracleClient.OracleDataAdapter(command);
                ds = new DataSet();
                dataAdapter.Fill(ds);

            }
            catch (Exception e)
            {
                throw new Exception("data access error: " + e.Message);
            }
            finally
            {
                conn.Close();
            }

            return ds;       
        }


        public static DataSet GetPrePackTicket()
        {
            DataSet ds = null;
            System.Data.OracleClient.OracleCommand command = null;
            System.Data.OracleClient.OracleConnection conn = null;
            System.Data.OracleClient.OracleDataAdapter dataAdapter = null;

            try
            {
                conn = new System.Data.OracleClient.OracleConnection();
                conn.ConnectionString = ConfigurationManager.AppSettings["RMS_CONN"].ToString();

                command = new System.Data.OracleClient.OracleCommand("RTLINTF.HT_SECURITY_PRINT.GET_TICKET_PREPACK", conn);
                command.CommandType = CommandType.StoredProcedure;

                System.Data.OracleClient.OracleParameter p2 = new System.Data.OracleClient.OracleParameter();
                p2.OracleType = OracleType.Cursor;
                p2.ParameterName = "p_ppticket_cur";
                p2.Direction = ParameterDirection.Output;
                command.Parameters.Add(p2);

                dataAdapter = new System.Data.OracleClient.OracleDataAdapter(command);
                ds = new DataSet();
                dataAdapter.Fill(ds);

            }
            catch (Exception e)
            {
                throw new Exception("data access error: " + e.Message);
            }
            finally
            {
                conn.Close();
            }

            return ds;
        }
   
    }
}
