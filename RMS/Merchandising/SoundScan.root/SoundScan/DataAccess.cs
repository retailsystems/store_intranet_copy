using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.Security;
using System.Diagnostics;
using System.Configuration;
 
namespace SoundScan
{
    class DataAccess
    {
        public static DataSet GetSoundScanDetails(Int16 fileCd)
        {
            DataSet ds = null;
            System.Data.OracleClient.OracleCommand command = null;
            System.Data.OracleClient.OracleConnection conn = null;
            System.Data.OracleClient.OracleDataAdapter dataAdapter = null;

            try
            {
                conn = new System.Data.OracleClient.OracleConnection();
                conn.ConnectionString = ConfigurationManager.AppSettings["RMS_CONN"].ToString();

				command = new System.Data.OracleClient.OracleCommand(ConfigurationManager.AppSettings["RMS_PROCEDURE"].ToString(), conn);
                command.CommandType = CommandType.StoredProcedure;

                System.Data.OracleClient.OracleParameter p1 = new System.Data.OracleClient.OracleParameter();
                p1.OracleType = OracleType.Cursor;
                p1.ParameterName = "p_ss_cur";
                p1.Direction = ParameterDirection.Output;
                command.Parameters.Add(p1);

				OracleParameter p2 = new OracleParameter();
				p2.OracleType = OracleType.Number;
				p2.ParameterName = "p_country";
				p2.Direction = ParameterDirection.Input;
				p2.Value = fileCd;
				command.Parameters.Add(p2);

                dataAdapter = new System.Data.OracleClient.OracleDataAdapter(command);
                ds = new DataSet();
                dataAdapter.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("data access error: " + e.Message);
            }
            finally
            {
                conn.Close();
            }

            return ds;
        }
    }
}
