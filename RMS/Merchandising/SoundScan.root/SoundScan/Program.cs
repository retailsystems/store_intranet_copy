using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SoundScan
{
    class Program
    {
        private static HTError.ErrorHandler _err;
        private static System.Collections.Specialized.NameValueCollection _appSettings;
        static void Main(string[] args)
        {
            int exitCode = 1;            
            try
            {
                Initialize();
                SScanFileManager SScanfm = new SScanFileManager(_err);
                _err.LogInformation("Begin process");

                _err.LogInformation("Archiving old files");
                SScanfm.ArchiveFiles();               

                SScanfm.CreateSScanFiles();
                _err.LogInformation("End process");
                
                exitCode = 0;
            }
            catch (Exception ex)
            {
                _err.LogError(ex);
                exitCode = 1;
            }
            finally
            {
                Environment.Exit(exitCode);
            }
        }

        private static void Initialize()
        {
            _appSettings = ConfigurationManager.AppSettings;
            _err = new HTError.ErrorHandler(_appSettings["ProductionLog"], _appSettings["DebugLog"],_appSettings["SMTPServer"],_appSettings["EmailSender"],_appSettings["ErrorEmailReceivers"], HTError.LogLevel.Debug);
            if (_appSettings["EnableDebugLog"].ToUpper().Equals("TRUE"))
            {
                _err.EnableDebugLog = true;
            }
            else
            {
                _err.EnableDebugLog = false;
            }
            if (_appSettings["LogMetaData"].ToUpper().Equals("TRUE"))
            {
                _err.IncludeDriveSpaceInfo = true;
                _err.IncludeNetworkDrives = true;
                _err.IncludeNetworkStatus = true;
                _err.IncludeRamInfo = true;
                _err.IncludeCurrentUser = true;
            }
            else
            {
                _err.IncludeDriveSpaceInfo = false;
                _err.IncludeNetworkDrives = false;
                _err.IncludeNetworkStatus = false;
                _err.IncludeRamInfo = false;
                _err.IncludeCurrentUser = false;
            }
            if (_appSettings["EnableEmail"].ToUpper().Equals("TRUE"))
            {
                _err.EnableEmail = true;
            }
            else
            {
                _err.EnableEmail = false;
            }
        }
    }
}
