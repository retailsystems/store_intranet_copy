using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;

namespace SoundScan
{
    class SScanFileManager
    {
        private System.Collections.Specialized.NameValueCollection _appSettings;
        private string _ticketFile;
        private string _fileFolder;
        private string _fieldSeperator = ",";
        private string _archiveFolder = "";
        private string _SMTPServer = "";
        private string _emailSender = "";
        private string _emailReceivers = "";
        private string _emailBody = "";
        private string _emailSubject = "";
        string[] _fileArr;
        private static HTError.ErrorHandler _err;
        String s1 = DateTime.Today.AddDays(((int)DateTime.Today.DayOfWeek + 1) * -1).ToString("yyMMdd");
        //String s1 = "110312";

        public SScanFileManager(HTError.ErrorHandler err)
        {
            _appSettings = ConfigurationManager.AppSettings;
            _fileFolder = _appSettings["FileFolder"];
            if (!_fileFolder.EndsWith(@"\")) _fileFolder += @"\";
            _archiveFolder = _appSettings["ArchiveFolder"];
            if (!_archiveFolder.EndsWith(@"\")) _archiveFolder += @"\";
            _fileArr = _appSettings["FILE_ARR"].Split(',');
            _SMTPServer = _appSettings["SMTPServer"];
            _emailSender = _appSettings["EmailSender"];
            _emailReceivers = _appSettings["EmailReceivers"];
            _emailBody = _appSettings["EmailBody"];
            _emailSubject = _appSettings["EmailSubject"];
            _err = err;

        }
        public void CreateSScanFiles()
        {
            try
            {
                // Create SoundScan files
                for (int i = 0; i < _fileArr.Length; i++)
                {
                    Int16 fileCd = 0;
                    Int16.TryParse(_fileArr[i].Substring(0, 1), out fileCd);

                    if (fileCd < 3)
                    {
                        switch (fileCd)
                        {
                            case 1:
                                _err.LogInformation("Begin Creating SoundScan HT Stores file..." + fileCd);
                                //CreateSScanFile_Store(fileCd);
                                CreateSScanFile_Store(fileCd);
                                break;
                            case 2:
                                _err.LogInformation("Begin Creating SoundScan Canada file..." + fileCd);
                                CreateSScanFile_Canada(fileCd);
                                break;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CreateSScanFile_Store(Int16 fileCd)
        {
            try
            {
                _ticketFile = _fileFolder + GetFilenameByFileCD(fileCd);
                _err.LogInformation("Creating SoundScan Stores file:" + _ticketFile);
                Console.WriteLine("Creating SoundScan HT Stores file.....");

                DataSet ds = DataAccess.GetSoundScanDetails(fileCd);
                Int32 i = 0;
                Int16 subStoreSalesRecord = 0;
                Int16 subStoreUnitsSold = 0;
                Int16 StoreUnitsSold = 0;
                decimal StoreUnits = 0;
                String CurStore = "";
                String FirstRecord = "Y";

                if (File.Exists(_ticketFile))
                {
                    File.Delete(_ticketFile);
                }

                using (StreamWriter sw = File.CreateText(_ticketFile))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
						s1 = DateTime.Parse(dr["eow_date"].ToString()).ToString("yyMMdd");
                        // use this to reran for item not setup for soundscan UDA by busniess
                        //if ( dr["ITEM"].ToString() == "850537004503" )
                        //{
                        if (FirstRecord == "Y")
                        {
                            //sw.WriteLine("9200802" + dr["store_cd"].ToString().PadLeft(5,'0') + dr["LW_ENDDATE"].ToString() + "  ");
                            sw.WriteLine("920802" + dr["LOC"].ToString().PadLeft(5, '0') + s1 + "  ");
                            FirstRecord = "N";
                        }

                        if (CurStore != dr["LOC"].ToString().Trim())
                        {
                            if ((CurStore != "") && (i > 0))
                            {
                                // write SubStore Total
                                sw.WriteLine("94" + subStoreSalesRecord.ToString().PadLeft(5) + subStoreUnitsSold.ToString().PadLeft(7) + "      ");

                                //sw.WriteLine("9200802" + dr["store_cd"].ToString().PadLeft(5,'0') + dr["LW_ENDDATE"].ToString() + "  "); 
                                sw.WriteLine("920802" + dr["LOC"].ToString().PadLeft(5, '0') + s1 + "  ");
                            }
                            CurStore = dr["LOC"].ToString();
                            subStoreSalesRecord = 0;
                            subStoreUnitsSold = 0;
                        }

                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        sb.Append("I3" + dr["ITEM"].ToString().Replace(" ", "").PadLeft(13, '0'));
                        sb.Append(dr["SALES_ISSUES"].ToString().PadLeft(5));
                        sw.WriteLine(sb.ToString());

                        subStoreSalesRecord += 1;
                        subStoreUnitsSold += Int16.Parse(dr["SALES_ISSUES"].ToString());
                        i += 1;
                    //}
                    }
                    sw.WriteLine("94" + subStoreSalesRecord.ToString().PadLeft(5) + subStoreUnitsSold.ToString().PadLeft(7) + "      ");
                    sw.Close();

                }
                //email file
                _err.LogInformation("Emailing SoundScan Stores File:" + _ticketFile);
                SendMail(_ticketFile, GetFileCDName(fileCd));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // private void CreateSScanFile_Store(Int16 fileCd)
        // {
        //     try
        //     {
        //         _ticketFile = _fileFolder + GetFilenameByFileCD(fileCd);
        //         _err.LogInformation("Creating SoundScan Stores file:" + _ticketFile);
        //         Console.WriteLine("Creating SoundScan HT Stores file.....");
        //       
        //         DataSet ds = DataAccess.GetSoundScanDetails_Store(fileCd);
        //         Int32 i = 0;
        //         Int16 subStoreSalesRecord = 0;
        //         Int16 subStoreUnitsSold = 0;
        //         Int16 StoreUnitsSold = 0;
        //         decimal StoreUnits = 0;                 
        //         String CurStore = "";
        //         String FirstRecord = "Y";
        //        
        //         if (File.Exists(_ticketFile))
        //         {
        //             File.Delete(_ticketFile);
        //         }
        //         
        //         using (StreamWriter sw = File.CreateText(_ticketFile))
        //         {
        //             foreach (DataRow dr in ds.Tables[0].Rows)                    
        //             {
        //                 if (FirstRecord == "Y")
        //                 {
        //                     //sw.WriteLine("9200802" + dr["store_cd"].ToString().PadLeft(5,'0') + dr["LW_ENDDATE"].ToString() + "  ");
        //                     sw.WriteLine("9200802" + dr["store_cd"].ToString().PadLeft(5,'0') + s1 + "  ");
        //                     FirstRecord = "N";
        //                 }
        //                 
        //                 if ( CurStore != dr["store_cd"].ToString().Trim() )
        //                 {                            
        //                     if ((CurStore != "") && (i > 0))
        //                     {
        //                     // write SubStore Total
        //                     sw.WriteLine("94" + subStoreSalesRecord.ToString().PadLeft(5) + subStoreUnitsSold.ToString().PadLeft(7) + "      ");
        //                  
        //                     //sw.WriteLine("9200802" + dr["store_cd"].ToString().PadLeft(5,'0') + dr["LW_ENDDATE"].ToString() + "  "); 
        //                     sw.WriteLine("9200802" + dr["store_cd"].ToString().PadLeft(5,'0') + s1 + "  ");  
        //                     }                            
        //                     CurStore = dr["store_cd"].ToString();                            
        //                     subStoreSalesRecord = 0;
        //                     subStoreUnitsSold = 0;
        //                 }
        //                                        
        //                 System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //                 sb.Append("I3" + dr["upc_cd"].ToString().Replace(" ", "").PadLeft(13, '0'));                        
        //                 sb.Append(dr["tot_qty"].ToString().PadLeft(5) );                        
        //                 sw.WriteLine(sb.ToString());
        //                 
        //                 subStoreSalesRecord += 1;
        //                 subStoreUnitsSold += Int16.Parse(dr["tot_qty"].ToString());
        //                 i += 1;
        //                 
        //             }
        //             sw.WriteLine("94" + subStoreSalesRecord.ToString().PadLeft(5) + subStoreUnitsSold.ToString().PadLeft(7) + "      ");
        //             sw.Close();
        //                               
        //         }
        //         //email file
        //         _err.LogInformation("Emailing SoundScan Stores File:" + _ticketFile);
        //         SendMail(_ticketFile, GetFileCDName(fileCd));
        //         
        //     }
        //     catch (Exception ex)
        //     {
        //         throw ex;
        //     }
        // }

        private void CreateSScanFile_Canada(Int16 fileCd)
        {
            try
            {
                _ticketFile = _fileFolder + GetFilenameByFileCD(fileCd);
                _err.LogInformation("Creating SoundScan Canada file:" + _ticketFile);
                Console.WriteLine("Creating SoundScan Canada file.....");

                DataSet ds = DataAccess.GetSoundScanDetails(fileCd);
                Int32 i = 0;
                Int16 subStoreSalesRecord = 0;
                Int16 subStoreUnitsSold = 0;
                Int16 StoreUnitsSold = 0;
                String CurStore = "";
                String FirstRecord = "Y";

                if (File.Exists(_ticketFile))
                {
                    File.Delete(_ticketFile);
                }

                using (StreamWriter sw = File.CreateText(_ticketFile))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
						s1 = DateTime.Parse(dr["eow_date"].ToString()).ToString("yyMMdd");
                        if (FirstRecord == "Y")
                        {
                            //sw.WriteLine("9200802" + dr["store_cd"].ToString().PadLeft(5, '0') + dr["LW_ENDDATE"].ToString() + "  ");
                            sw.WriteLine("920802" + dr["LOC"].ToString().PadLeft(5, '0') + s1 + "  ");
                            FirstRecord = "N";
                        }

                        if (CurStore != dr["LOC"].ToString().Trim())
                        {

                            if ((CurStore != "") && (i > 0))
                            {
                                // write SubStore Total
                                sw.WriteLine("94" + subStoreSalesRecord.ToString().PadLeft(5) + subStoreUnitsSold.ToString().PadLeft(7) + "      ");

                                //sw.WriteLine("9200802" + dr["store_cd"].ToString().PadLeft(5, '0') + dr["LW_ENDDATE"].ToString() + "  ");
                                sw.WriteLine("920802" + dr["LOC"].ToString().PadLeft(5, '0') + s1 + "  ");
                            }

                            CurStore = dr["LOC"].ToString();

                            subStoreSalesRecord = 0;
                            subStoreUnitsSold = 0;
                        }

                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        //sb.Append("I3" + dr["upc_cd"].ToString().PadLeft(13,'0'));
                        sb.Append("I3" + dr["ITEM"].ToString().Replace(" ", "").PadLeft(13, '0'));
                        sb.Append(dr["SALES_ISSUES"].ToString().PadLeft(5));

                        sw.WriteLine(sb.ToString());

                        subStoreSalesRecord += 1;
                        subStoreUnitsSold += Int16.Parse(dr["SALES_ISSUES"].ToString());
                        i += 1;

                    }
                    sw.WriteLine("94" + subStoreSalesRecord.ToString().PadLeft(5) + subStoreUnitsSold.ToString().PadLeft(7) + "      ");
                    sw.Close();
                }
                //email file
                _err.LogInformation("Emailing SoundScan Canada File:" + _ticketFile);
                SendMail(_ticketFile, GetFileCDName(fileCd));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void SendMail(string fileName, string company)
        {
            //string company = GetCompanyName(fileCd);
            //Create a message and set up the recipients.
            MailMessage message = new MailMessage();
            //message.Sender = new MailAddress(_emailSender);
            message.Subject = _emailSubject.Replace("%company%", company);
            message.IsBodyHtml = true;
            message.From = new MailAddress(_emailSender);
            message.Body = _emailBody.Replace("%loc%", fileName);
            //Create  the file attachment for this e-mail message.
            Attachment data = new Attachment(fileName);
            //Add the file attachment to this e-mail message.
            message.Attachments.Add(data);
            message.To.Add(_emailReceivers);
            //Send the message.
            SmtpClient client = new SmtpClient(_SMTPServer);
            //Add credentials if the SMTP server requires them.
            //Client.Credentials = CredentialCache.DefaultNetworkCredentials;
            client.Send(message);

        }
        public void ArchiveFiles()
        {
            //_archiveFolder += DateTime.Now.ToString("MMddyyHHmi") + @"\";
            string archivePath = _archiveFolder + DateTime.Now.ToString("MMddyyHHmm") + @"\";
            DirectoryInfo diSource = new DirectoryInfo(_fileFolder);
            DirectoryInfo diTarget = new DirectoryInfo(archivePath);
            if (diSource.Exists)
            {
                //System.Collections.IEnumerator ienum = diSource.GetFiles("*.txt").GetEnumerator();
                System.Collections.IEnumerator ienum = diSource.GetFiles("*.*").GetEnumerator();
                while (ienum.MoveNext())
                {
                    if (!diTarget.Exists) diTarget.Create();
                    FileInfo fi = (FileInfo)ienum.Current;
                    FileInfo fiTarget = new FileInfo(archivePath + fi.Name);
                    if (fiTarget.Exists) fiTarget.Delete();
                    fi.MoveTo(archivePath + fi.Name);
                }
            }
        }
        private string GetFilenameByFileCD(Int16 fileCd)
        {
            //string[] _fileArr = _appSettings["FILE_ARR"].Split(',');
            string fileName = "";
            //String s2 = DateTime.Today.AddDays(((int)DateTime.Today.DayOfWeek + 1) * -1).ToString("MMdd");

            for (int i = 0; i < _fileArr.Length; i++)
            {
                if (_fileArr[i].StartsWith(fileCd.ToString()))
                {
                    fileName = _fileArr[i].Substring(_fileArr[i].IndexOf('|') + 1);
                    break;
                }
            }
			return fileName + DateTime.Today.AddDays(-1).ToString("yyMMdd") + ".Txt";
        }

        private string GetFileCDName(Int16 fileCd)
        {
            //string[] _fileArr = _appSettings["FILE_ARR"].Split(',');
            string FileCDName = "";
            switch (fileCd)
            {
                case 1:
                    FileCDName = "Hot Topic Stores";
                    break;
                case 2:
                    FileCDName = "Canada Stores";
                    break;
            }
            return FileCDName;
        }


    }
}
