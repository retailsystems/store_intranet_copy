using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.ComponentModel;
using System.Drawing;
using System.Web.SessionState;



namespace PhotoTracker
{
    public partial class Login : System.Web.UI.Page
    {
        //protected System.Web.UI.WebControls.TextBox txtEmpNum;
        //protected System.Web.UI.WebControls.Button btnSubmit;
        //protected System.Web.UI.WebControls.RequiredFieldValidator valEmpID;
        //protected System.Web.UI.WebControls.Label lblOut;
        //protected System.Web.UI.WebControls.LinkButton LinkButton1;
        //protected System.Web.UI.HtmlControls.HtmlForm Form1;
	  
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			
			//this.txtEmpNum.Attributes.Add("OnKeyPress", "javascript:if (event.keyCode == 13)__doPostBack('" + btnSubmit.UniqueID + "','');"); 
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

        private void btnSubmit_Click(object sender, System.EventArgs e)
        { 
            string strEmpID = this.txtEmpNum.Text.Trim();
            string strEmpPWD = this.txtPassword.Text.Trim();

            if ((strEmpID != "") && (strEmpPWD != ""))
            {
                //DataTable dtEmpAccessRole = AppUser.GetUserAccessRoles(strEmpID);
                DataTable dtUser = AppUser.GetUserProfile(strEmpID,strEmpPWD);
                if (dtUser.Rows.Count > 0)
                {
                    string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
                    string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();

                    DataTable dtPermit = AppUser.GetUserAccessRoles(strEmpID);
                    if (dtPermit.Rows.Count > 0)
                    {
                        Session.Add("ViewPermission", dtPermit);
                        Session.Add("UserProfile", dtUser);

                        Page.Response.Redirect("./TrackMenu.aspx");
                       
                      
                    }
                    else
                    {
                        //print out error message - not enough permission
                        this.valEmpID.IsValid = false;
                        this.valEmpID.ErrorMessage = "Access role denied.";

                    }
                    //int intPermit = AppUser.SetUserPermission(strLocation, strJobCode);
                }
                else
                {
                    //print out error message - not enough permission
                    this.valEmpID.IsValid = false;
                    this.valEmpID.ErrorMessage = "Invalid Employee# and Password, Access denied.";

                }
            }
            else 
            {
                this.valEmpID.ErrorMessage = "Please enter Employee# and Password.";

            }
        }
    }
}
