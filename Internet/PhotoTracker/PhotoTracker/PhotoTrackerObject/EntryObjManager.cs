using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

namespace PhotoTracker
{
    public class EntryObjManager
    {
        PhotoTracking m_PhotoTrack = null;
        List<EntryBase> m_listObj = new List<EntryBase>();
        public EntryObjManager(PhotoTracking aPhotoTrack)
        {
            m_PhotoTrack = aPhotoTrack;
        }

        //public void InitialBinding(WebControl objWebControl, string strMsgType)
        public void InitialBinding()
        {
            //EntryBase objRec = new SubEntryRec(m_PhotoTrack.getWebControl(0)); //text itemnum control
            //m_listObj.Add(objRec);
            
            //EntryBase objBtn = new SubEntryRec(m_PhotoTrack.getWebControl(20)); //btnEntry control
            //m_listObj.Add(objBtn);

            //EntryBase obj = new SubEntryBy(m_PhotoTrack.getWebControl(1));  //drop down name control
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            //obj.SetSender(objBtn);

            //obj = new SubEntryBy(m_PhotoTrack.getWebControl(2));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            //obj.SetSender(objBtn);

            //obj = new SubEntryBy(m_PhotoTrack.getWebControl(3));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);

            //obj = new SubEntryBy(m_PhotoTrack.getWebControl(4));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            //obj.SetSender(objBtn);

            //obj = new SubEntryBy(m_PhotoTrack.getWebControl(5));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);

            //obj = new SubEntryBy(m_PhotoTrack.getWebControl(6));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            //obj.SetSender(objBtn);

            //obj = new SubEntryBy(m_PhotoTrack.getWebControl(7));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            //obj.SetSender(objBtn);

            //obj = new SubEntryBy(m_PhotoTrack.getWebControl(8));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);

            //obj = new SubEntryTime(m_PhotoTrack.getWebControl(9)); // drop down time control
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            //obj.SetSender(objBtn);

            //obj = new SubEntryTime(m_PhotoTrack.getWebControl(10));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            //obj.SetSender(objBtn);

            //obj = new SubEntryTime(m_PhotoTrack.getWebControl(11));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);

            //obj = new SubEntryTime(m_PhotoTrack.getWebControl(12));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            //obj.SetSender(objBtn);

            //obj = new SubEntryTime(m_PhotoTrack.getWebControl(13));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            //obj.SetSender(objBtn);

            //obj = new SubEntryTime(m_PhotoTrack.getWebControl(14));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            //obj.SetSender(objBtn);

            //obj = new SubEntryTime(m_PhotoTrack.getWebControl(15));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            //obj.SetSender(objBtn);

            //obj = new SubEntryTime(m_PhotoTrack.getWebControl(16));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);

            //obj = new SubEntryRec(m_PhotoTrack.getWebControl(17)); //txt entry rec control
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            

            //obj = new SubEntryRec(m_PhotoTrack.getWebControl(18));
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);
            

            //obj = new SubEntryChk(m_PhotoTrack.getWebControl(19)); //radio top control
            //m_listObj.Add(obj);
            //obj.SetSender(objRec);

            ////foreach (EntryBase objEntry in m_listObj)
            ////{
            ////    objEntry.SetSender(objRec);
            ////}

            foreach (EntryBase objEntry in m_listObj)
            {
                objEntry.StartRegister();
            }
        }

        //public void CatchEvent(WebControl objWebControl, string strMsgType, long lValue)
        //{
        //    foreach (EntryBase obj in m_listObj)
        //    {
        //        obj.Notification(strMsgType, lValue);
        //    }
        //}
        public void CatchEvent(WebControl objWebControl, string strMsgType, int nMsgValue, int nEntryType, bool bValue, int intEmpID, string strItmNum, string strDiv)  //strMsgType is Control Type - dropdown or txt
        {
            foreach (EntryBase obj in m_listObj)
            {
                obj.Notification(strMsgType, nMsgValue, nEntryType, bValue, intEmpID, strItmNum, strDiv);
            }
        }
    }
}
