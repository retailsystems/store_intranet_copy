using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace PhotoTracker
{
    public class AppUser
    {
        #region Members

        string _strEmployeeID = String.Empty;
        bool _bValidUser;
        
        string _strUserName = String.Empty;
        string _strUserID = String.Empty;

        #endregion Members

        #region Properties

        public string StrEmployeeID
        {
            get { return _strEmployeeID; }
            set { _strEmployeeID = value; }
        }
       
        //public string StrPassword
        //{
        //    get { return _strPassword; }
        //    set { _strPassword = value; }
        //}
        public bool BValidUser
        {
            get { return _bValidUser; }
            set { _bValidUser = value; }
        }

        public string StrUserName
        {
            get { return _strUserName; }
            set { _strUserName = value; }
        }
        public string StrUserID
        {
            get { return _strUserID; }
            set { _strUserID = value; }
        }
        #endregion Properties

        public AppUser()
        {
        }

        public static DataTable GetUserProfile(string strEmpID, string strEmpPassword)
        {
            DataTable dtProfile;

            SqlParameter[] Parameters = new SqlParameter[2];
            Parameters[0] = new SqlParameter("@EmployeeId", SqlDbType.VarChar);
            Parameters[0].Size = 6;
            Parameters[0].Value = strEmpID;
            Parameters[1] = new SqlParameter("@EmpPassword", SqlDbType.VarChar);
            Parameters[1].Size = 40;
            Parameters[1].Value = strEmpPassword;
            dtProfile = Data.ExecuteStoredProcedure("Employee_CheckPWD", Parameters, ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString());

            return dtProfile;
        }

        //public static DataTable GetUserProfile(string strEmpID)
        //{
        //    DataTable dtProfile;

        //    SqlParameter[] Parameters = new SqlParameter[1];
        //    Parameters[0] = new SqlParameter("@EmpId", SqlDbType.VarChar);
        //    Parameters[0].Size = 5;
        //    Parameters[0].Value = strEmpID;
        //    dtProfile = Data.ExecuteStoredProcedure("spGetEmployeeInfo", Parameters, ConfigurationSettings.AppSettings["strEmpSQLConnection"].ToString());

        //    return dtProfile;
        //}
        public static bool CheckSecurity(string strRole, DataTable dtPerm)
        {
            bool IsOK = false;
            if (dtPerm.Rows.Count > 0)
            {
                for (int i = 0; i < (dtPerm.Rows.Count); i++)
                {
                    string strGroupNo = dtPerm.Rows[i]["GroupNo"].ToString().Trim();
                    if (strGroupNo == strRole)
                    {
                        IsOK = true;
                    }

                }
            }
            return IsOK;
        }
        public static DataTable GetUserAccessRoles(string strEmpID)
        {
            DataTable dtAccessRoles;

            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@EmployeeId", SqlDbType.VarChar);
            Parameters[0].Size = 6;
            Parameters[0].Value = strEmpID;
            dtAccessRoles = Data.ExecuteStoredProcedure("Emp_GetAccessRoles", Parameters, ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString());

            return dtAccessRoles;

        }
       
    }
}
