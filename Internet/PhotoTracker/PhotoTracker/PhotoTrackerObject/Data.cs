using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace PhotoTracker
{
    public class Data
    {
        static string strItemNum = string.Empty;
        public Data()
        {
          
        }

        public static DataTable GetCopyUsers()
		{
			DataTable dtCopyUsers  = new DataTable();
            dtCopyUsers = ExecuteStoredProcedure("Track_GetAllCopyUsers", null, null);
            return dtCopyUsers;
		}

        public static DataTable GetPhotoUsers()
        {
            DataTable dtPhotoUsers = new DataTable();
            dtPhotoUsers = ExecuteStoredProcedure("Track_GetAllPhotoUsers", null, null);
            return dtPhotoUsers;
        }

        public static DataTable GetPhotoEditUsers()
        {
            DataTable dtPhotoEditUsers = new DataTable();
            dtPhotoEditUsers = ExecuteStoredProcedure("Track_GetAllPhotoEditUsers", null, null);
            return dtPhotoEditUsers;
        }

        public static DataTable GetFinalApprovers()
        {
            DataTable dtFinalApp = new DataTable();
            dtFinalApp = ExecuteStoredProcedure("Track_GetAllFinalApprovers", null, null);
            return dtFinalApp;
        }
        public static DataTable GetCopyUsersByDiv(string strDiv)
        {
            DataTable dtCopyUsers = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@Division", SqlDbType.VarChar);
            Parameters[0].Value = strDiv;
            dtCopyUsers = ExecuteStoredProcedure("Track_GetAllCopyUsersByDiv", Parameters, null);
            return dtCopyUsers;
        }

        public static DataTable GetPhotoUsersByDiv(string strDiv)
        {
            DataTable dtPhotoUsers = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@Division", SqlDbType.VarChar);
            Parameters[0].Value = strDiv;
            dtPhotoUsers = ExecuteStoredProcedure("Track_GetAllPhotoUsersByDiv", Parameters, null);
            return dtPhotoUsers;
        }
        public static DataTable GetPhotoEditUsersByDiv(string strDiv)
        {
            DataTable dtPhotoEditUsers = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@Division", SqlDbType.VarChar);
            Parameters[0].Value = strDiv;
            dtPhotoEditUsers = ExecuteStoredProcedure("Track_GetAllPhotoEditUsersByDiv", Parameters, null);
            return dtPhotoEditUsers;
        }

        public static DataTable GetFinalApproversByDiv(string strDiv)
        {
            DataTable dtFinalApp = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@Division", SqlDbType.VarChar);
            Parameters[0].Value = strDiv;
            dtFinalApp = ExecuteStoredProcedure("Track_GetAllFinalApproversByDiv", Parameters, null);
            return dtFinalApp;
        }

        public static DataTable GetCompanyBranchList()
        {
            DataTable dtComBranch = new DataTable();
            dtComBranch = ExecuteStoredProcedure("Report_GetAllCompBranches", null, null);
            return dtComBranch;
        }

        //public static DataTable GetRevUsersByDiv(string strItemNum)
        //{
        //    DataTable dtRevUsers = new DataTable();
        //    SqlParameter[] Parameters = new SqlParameter[1];
        //    Parameters[0] = new SqlParameter("@ItemNum", SqlDbType.VarChar);
        //    Parameters[0].Value = strItemNum;
        //    dtRevUsers = ExecuteStoredProcedure("Track_GetAllReceiveUsers", Parameters, null);
        //    return dtRevUsers;
        //}
        public static DataTable GetRevUsers(string strItemNum)
        {
            DataTable dtRevUsers = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@ItemNum", SqlDbType.VarChar);
            Parameters[0].Value = strItemNum;
            dtRevUsers = ExecuteStoredProcedure("Track_GetAllReceiveUsers", Parameters, null);
            return dtRevUsers;
        }

        public static DataTable GetGersInfo(string strItemNum)
        {
            DataTable dtGersInfo = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@ItemNum", SqlDbType.VarChar);
            Parameters[0].Value = strItemNum;
            dtGersInfo = ExecuteStoredProcedure("Track_GetGersInfoByItemNo", Parameters, null);
            return dtGersInfo;
        }

        public static int CheckGersItem(string strItemNum,int employeeID)
        {
            int intComResult = 0;
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Track_CheckItemExist";
                cm.CommandTimeout = 60;

                cm.Parameters.Clear();

                SqlParameter qqq = new SqlParameter();
                qqq.ParameterName = "@ItemNum";
                qqq.SqlDbType = SqlDbType.VarChar;
                qqq.Size = 12;
                qqq.Value = strItemNum;
                cm.Parameters.Add(qqq);

                //SqlParameter mmm = new SqlParameter();
                //mmm.ParameterName = "@Division";
                //mmm.SqlDbType = SqlDbType.VarChar;
                //mmm.Size = 2;
                //if (employeeID == 89008 || employeeID == 71846 || employeeID == 11999 || employeeID == 79879)
                //    mmm.Value = "0";
                //else
                //{
                //    mmm.Value = strDiv;
                //}
                //cm.Parameters.Add(mmm);

                SqlParameter sss = new SqlParameter();
                sss.ParameterName = "@Result";
                sss.SqlDbType = SqlDbType.Int;
                sss.Value = intComResult;
                sss.Direction = ParameterDirection.Output;
                cm.Parameters.Add(sss);

                cm.ExecuteNonQuery();

                intComResult = Convert.ToInt32(cm.Parameters["@Result"].Value);

            }
            //catch (Exception ex)
            //{
            //    //int s = 0;

            //}
            finally
            {
                cn.Close();
            }
            return intComResult;
        }

        //public static int CheckGersItemByDiv(string strItemNum, string strDiv,int employeeID)
        //{
        //    int intComResult = 0;
        //    SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
        //    SqlCommand cm = new SqlCommand();

        //    try
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandType = CommandType.StoredProcedure;
        //        cm.CommandText = "Track_CheckItemExistByDiv";
        //        cm.CommandTimeout = 60;

        //        cm.Parameters.Clear();

        //        SqlParameter qqq = new SqlParameter();
        //        qqq.ParameterName = "@ItemNum";
        //        qqq.SqlDbType = SqlDbType.VarChar;
        //        qqq.Size = 12;
        //        qqq.Value = strItemNum;
        //        cm.Parameters.Add(qqq);

        //        SqlParameter mmm = new SqlParameter();
        //        mmm.ParameterName = "@Division";
        //        mmm.SqlDbType = SqlDbType.VarChar;
        //        mmm.Size = 2;
        //        if (employeeID == 89008 ||employeeID == 71846 ||employeeID == 11999 ||employeeID == 79879)
        //            mmm.Value = "0";
        //        else
        //        {
        //            mmm.Value = strDiv;
        //        }
        //        cm.Parameters.Add(mmm);

        //        SqlParameter sss = new SqlParameter();
        //        sss.ParameterName = "@Result";
        //        sss.SqlDbType = SqlDbType.Int;
        //        sss.Value = intComResult;
        //        sss.Direction = ParameterDirection.Output;
        //        cm.Parameters.Add(sss);

        //        cm.ExecuteNonQuery();

        //        intComResult = Convert.ToInt32(cm.Parameters["@Result"].Value);

        //    }
        //    //catch (Exception ex)
        //    //{
        //    //    //int s = 0;

        //    //}
        //    finally
        //    {
        //        cn.Close();
        //    }
        //    return intComResult;
        //}
        public static DataTable GetItm4RptPrnt(int intDivision)
        {
            DataTable dtGetItem = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@Division", SqlDbType.Int);
            Parameters[0].Value = intDivision;
            dtGetItem = ExecuteStoredProcedure("Report_GetItemByDivision", Parameters, null);
            return dtGetItem;
        }
    
        public static DataTable GetItem4MigrateBarCode() 
        {
            DataTable dtGetItem = new DataTable();
            dtGetItem = ExecuteStoredProcedure("GetItem4MigrateBarcode", null, null);
            return dtGetItem;
        }
        public static DataTable GetItmTopInfo(string strItemNum)
        {
            DataTable dtGetItemTop = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@ItemNum", SqlDbType.VarChar);
            Parameters[0].Value = strItemNum;
            dtGetItemTop = ExecuteStoredProcedure("Track_GetTopInfoByItemNo", Parameters, null);
            return dtGetItemTop;
        }
        public static DataTable GetRevEntry(string strItemNum)
        {
            DataTable dtGetRevEntry = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@ItemNum", SqlDbType.VarChar);
            Parameters[0].Value = strItemNum;
            dtGetRevEntry = ExecuteStoredProcedure("Track_GetRevEntryInfo", Parameters, null);
            return dtGetRevEntry;

        }
        public static int CheckEmpRevRight(int intEmpID)
        {
            int intResult = 0;
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Emp_CheckRecRight";
                cm.CommandTimeout = 60;

                cm.Parameters.Clear();

                SqlParameter qqq = new SqlParameter();
                qqq.ParameterName = "@EmployeeID";
                qqq.SqlDbType = SqlDbType.Int;
                qqq.Value = intEmpID;
                cm.Parameters.Add(qqq);

                SqlParameter sss = new SqlParameter();
                sss.ParameterName = "@Result";
                sss.SqlDbType = SqlDbType.Int;
                sss.Value = intResult;
                sss.Direction = ParameterDirection.Output;
                cm.Parameters.Add(sss);

                cm.ExecuteNonQuery();

                intResult = Convert.ToInt32(cm.Parameters["@Result"].Value);

            }
            //catch (Exception ex)
            //{
            //    //int s = 0;

            //}
            finally
            {
                cn.Close();
            }
            return intResult;
        }

        public static int EntryUpdate(string strItemNum, int intEntryType, DateTime dtTrackDate, int intEmpTrackAccStatus)
        {
            int intResultEntry = 0;
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Track_Update";
                cm.CommandTimeout = 60;

                cm.Parameters.Clear();

                SqlParameter qqq = new SqlParameter();
                qqq.ParameterName = "@ItemNum";
                qqq.SqlDbType = SqlDbType.VarChar;
                qqq.Size = 12;
                qqq.Value = strItemNum;
                cm.Parameters.Add(qqq);

                SqlParameter xxx = new SqlParameter();
                xxx.ParameterName = "@EntryType";
                xxx.SqlDbType = SqlDbType.Int;
                xxx.Value = intEntryType;
                cm.Parameters.Add(xxx);

                SqlParameter lll = new SqlParameter();
                lll.ParameterName = "@TrackDate";
                lll.SqlDbType = SqlDbType.DateTime;
                lll.Value = dtTrackDate;
                cm.Parameters.Add(lll);

                SqlParameter ttt = new SqlParameter();
                ttt.ParameterName = "@EmpTrackAccStatus";
                ttt.SqlDbType = SqlDbType.Int;
                ttt.Value = intEmpTrackAccStatus;
                cm.Parameters.Add(ttt);

                SqlParameter sss = new SqlParameter();
                sss.ParameterName = "@Result";
                sss.SqlDbType = SqlDbType.Int;
                sss.Value = intResultEntry;
                sss.Direction = ParameterDirection.Output;
                cm.Parameters.Add(sss);

                cm.ExecuteNonQuery();

                intResultEntry = Convert.ToInt32(cm.Parameters["@Result"].Value);

            }
            //catch (Exception ex)
            //{
            //    //int s = 0;

            //}
            finally
            {
                cn.Close();
            }
            return intResultEntry;
        }
        public static string strBarCodePath()
        {
            string strBarCodePath = ConfigurationManager.AppSettings["BarCodePath"];
            return strBarCodePath;
        }
        public static int EntryInsert(string strItemNum, int intEmpID, int intEntryType, int intEmpTrackAccStatus)
        {
            int intResultEntry = 0;
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Track_Insert";
                cm.CommandTimeout = 60;

                cm.Parameters.Clear();

                SqlParameter qqq = new SqlParameter();
                qqq.ParameterName = "@ItemNum";
                qqq.SqlDbType = SqlDbType.VarChar;
                qqq.Size = 12;
                qqq.Value = strItemNum;
                cm.Parameters.Add(qqq);

                SqlParameter ppp = new SqlParameter();
                ppp.ParameterName = "@EmployeeID";
                ppp.SqlDbType = SqlDbType.Int;
                ppp.Value = intEmpID;
                cm.Parameters.Add(ppp);

                SqlParameter xxx = new SqlParameter();
                xxx.ParameterName = "@EntryType";
                xxx.SqlDbType = SqlDbType.Int;
                xxx.Value = intEntryType;
                cm.Parameters.Add(xxx);

                SqlParameter ttt = new SqlParameter();
                ttt.ParameterName = "@EmpTrackAccStatus";
                ttt.SqlDbType = SqlDbType.Int;
                ttt.Value = intEmpTrackAccStatus;
                cm.Parameters.Add(ttt);

                SqlParameter sss = new SqlParameter();
                sss.ParameterName = "@Result";
                sss.SqlDbType = SqlDbType.Int;
                sss.Value = intResultEntry;
                sss.Direction = ParameterDirection.Output;
                cm.Parameters.Add(sss);

                cm.ExecuteNonQuery();

                intResultEntry = Convert.ToInt32(cm.Parameters["@Result"].Value);

            }
            //catch (Exception ex)
            //{
            //    //int s = 0;

            //}
            finally
            {
                cn.Close();
            }
            return intResultEntry;
        }
        //public static int BarCodeInsert(string strItemNum, Image imgBarCode) 

       
        public static int Track_BarCodeInsert4Migrate(string strItemNum, string strBarCodePath)
        {
            int intResultEntry = 0;
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();

            //FileStream fs = new FileStream(@"C:\BarCodeImageTest\BarcodeHandler.gif", FileMode.OpenOrCreate, FileAccess.Read);
            FileStream fs = new FileStream(@strBarCodePath, FileMode.OpenOrCreate, FileAccess.Read);

            byte[] MyData = new byte[fs.Length];
            fs.Read(MyData, 0, System.Convert.ToInt32(fs.Length));

            fs.Close();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Track_BarCodeInsert4Migrate";
                cm.CommandTimeout = 90;

                cm.Parameters.Clear();

                SqlParameter qqq = new SqlParameter();
                qqq.ParameterName = "@ItemNum";
                qqq.SqlDbType = SqlDbType.VarChar;
                qqq.Size = 12;
                qqq.Value = strItemNum;
                cm.Parameters.Add(qqq);

                SqlParameter ppp = new SqlParameter();
                ppp.ParameterName = "@BarCodeImage";
                ppp.SqlDbType = SqlDbType.Image;
                ppp.Value = MyData;
                cm.Parameters.Add(ppp);

                SqlParameter sss = new SqlParameter();
                sss.ParameterName = "@Result";
                sss.SqlDbType = SqlDbType.Int;
                sss.Value = intResultEntry;
                sss.Direction = ParameterDirection.Output;
                cm.Parameters.Add(sss);

                cm.ExecuteNonQuery();

                intResultEntry = Convert.ToInt32(cm.Parameters["@Result"].Value);

            }
            //catch (Exception ex)
            //{
            //    //int s = 0;

            //}
            finally
            {
                cn.Close();
            }
            return intResultEntry;
        }
        public static int BarCodeInsert(string strItemNum, string strBarCodePath)
        {
            int intResultEntry = 0;
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();

            //FileStream fs = new FileStream(@"C:\BarCodeImageTest\BarcodeHandler.gif", FileMode.OpenOrCreate, FileAccess.Read);
            FileStream fs = new FileStream(@strBarCodePath, FileMode.OpenOrCreate, FileAccess.Read);

            byte[] MyData = new byte[fs.Length];
            fs.Read(MyData, 0, System.Convert.ToInt32(fs.Length));

            fs.Close();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Track_BarCodeInsert";
                cm.CommandTimeout = 90;

                cm.Parameters.Clear();

                SqlParameter qqq = new SqlParameter();
                qqq.ParameterName = "@ItemNum";
                qqq.SqlDbType = SqlDbType.VarChar;
                qqq.Size = 12;
                qqq.Value = strItemNum;
                cm.Parameters.Add(qqq);

                SqlParameter ppp = new SqlParameter();
                ppp.ParameterName = "@BarCodeImage";
                ppp.SqlDbType = SqlDbType.Image;
                ppp.Value = MyData;
                cm.Parameters.Add(ppp);

                SqlParameter sss = new SqlParameter();
                sss.ParameterName = "@Result";
                sss.SqlDbType = SqlDbType.Int;
                sss.Value = intResultEntry;
                sss.Direction = ParameterDirection.Output;
                cm.Parameters.Add(sss);

                cm.ExecuteNonQuery();

                intResultEntry = Convert.ToInt32(cm.Parameters["@Result"].Value);

            }
            //catch (Exception ex)
            //{
            //    //int s = 0;

            //}
            finally
            {
                cn.Close();
            }
            return intResultEntry;
        }
        public static int RptPrintInsert(string strItemNum, int intEmpID, int intPrintCount)
        {
            int intResult = 0;
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Report_PrintInsert";
                cm.CommandTimeout = 60;

                cm.Parameters.Clear();

                SqlParameter qqq = new SqlParameter();
                qqq.ParameterName = "@ItemNum";
                qqq.SqlDbType = SqlDbType.VarChar;
                qqq.Size = 12;
                qqq.Value = strItemNum;
                cm.Parameters.Add(qqq);

                SqlParameter mmm = new SqlParameter();
                mmm.ParameterName = "@EmployeeID";
                mmm.SqlDbType = SqlDbType.Int;
                mmm.Value = intEmpID;
                cm.Parameters.Add(mmm);

                SqlParameter sss = new SqlParameter();
                sss.ParameterName = "@Result";
                sss.SqlDbType = SqlDbType.Int;
                sss.Value = intResult;
                sss.Direction = ParameterDirection.Output;
                cm.Parameters.Add(sss);

                cm.ExecuteNonQuery();

                intResult = Convert.ToInt32(cm.Parameters["@Result"].Value);

            }
            //catch (Exception ex)
            //{
            //    //int s = 0;

            //}
            finally
            {
                cn.Close();
            }
            return intResult;
        }
       public static int TrkTopUpdate(string strItemNum, string strTopSample, int intItemType)
       {
           int intBcdResult = 0;
           SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
           SqlCommand cm = new SqlCommand();

           try
           {
               cn.Open();
               cm.Connection = cn;
               cm.CommandType = CommandType.StoredProcedure;
               cm.CommandText = "Track_ItmTopUpdate";
               cm.CommandTimeout = 60;

               cm.Parameters.Clear();

               SqlParameter qqq = new SqlParameter();
               qqq.ParameterName = "@ItemNum";
               qqq.SqlDbType = SqlDbType.VarChar;
               qqq.Size = 12;
               qqq.Value = strItemNum;
               cm.Parameters.Add(qqq);

               SqlParameter aaa = new SqlParameter();
               aaa.ParameterName = "@TopSample";
               aaa.SqlDbType = SqlDbType.VarChar;
               aaa.Size = 10;
               aaa.Value = strTopSample;
               cm.Parameters.Add(aaa);

               SqlParameter xxx = new SqlParameter();
               xxx.ParameterName = "@ItemType";
               xxx.SqlDbType = SqlDbType.Int;
               xxx.Value = intItemType;
               cm.Parameters.Add(xxx);

               SqlParameter sss = new SqlParameter();
               sss.ParameterName = "@Result";
               sss.SqlDbType = SqlDbType.Int;
               sss.Value = intBcdResult;
               sss.Direction = ParameterDirection.Output;
               cm.Parameters.Add(sss);

               cm.ExecuteNonQuery();

               intBcdResult = Convert.ToInt32(cm.Parameters["@Result"].Value);

           }
           //catch (Exception ex)
           //{
           //    //int s = 0;

           //}
           finally
           {
               cn.Close();
           }
           return intBcdResult;
       }

        public static int TrkTopInsert(string strItemNum, string strTopSample, int intItemType)
        {
            int intBcdResult = 0;
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Track_ItmTopInsert";
                cm.CommandTimeout = 60;

                cm.Parameters.Clear();

                SqlParameter qqq = new SqlParameter();
                qqq.ParameterName = "@ItemNum";
                qqq.SqlDbType = SqlDbType.VarChar;
                qqq.Size = 12;
                qqq.Value = strItemNum;
                cm.Parameters.Add(qqq);

                SqlParameter aaa = new SqlParameter();
                aaa.ParameterName = "@TopSample";
                aaa.SqlDbType = SqlDbType.VarChar;
                aaa.Size = 10;
                aaa.Value = strTopSample;
                cm.Parameters.Add(aaa);

                SqlParameter xxx = new SqlParameter();
                xxx.ParameterName = "@ItemType";
                xxx.SqlDbType = SqlDbType.Int;
                xxx.Value = intItemType;
                cm.Parameters.Add(xxx);

                SqlParameter sss = new SqlParameter();
                sss.ParameterName = "@Result";
                sss.SqlDbType = SqlDbType.Int;
                sss.Value = intBcdResult;
                sss.Direction = ParameterDirection.Output;
                cm.Parameters.Add(sss);

                cm.ExecuteNonQuery();

                intBcdResult = Convert.ToInt32(cm.Parameters["@Result"].Value);

            }
            //catch (Exception ex)
            //{
            //    //int s = 0;

            //}
            finally
            {
                cn.Close();
            }
            return intBcdResult;
        }
        public static int CommentInsert(string strItemNum, int intEmpID, string strComments)
        {
            int intComResult = 0;
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Comment_Insert";
                cm.CommandTimeout = 60;

                cm.Parameters.Clear();

                SqlParameter qqq = new SqlParameter();
                qqq.ParameterName = "@ItemNum";
                qqq.SqlDbType = SqlDbType.VarChar;
                qqq.Size = 12;
                qqq.Value = strItemNum;
                cm.Parameters.Add(qqq);

                SqlParameter xxx = new SqlParameter();
                xxx.ParameterName = "@EmployeeID";
                xxx.SqlDbType = SqlDbType.Int;
                xxx.Value = intEmpID;
                cm.Parameters.Add(xxx);
  
                SqlParameter mmm = new SqlParameter();
                mmm.ParameterName = "@Comments";
				mmm.SqlDbType = SqlDbType.VarChar;
				mmm.Size=500;
                mmm.Value = strComments;
				cm.Parameters.Add(mmm);

                SqlParameter sss = new SqlParameter();
                sss.ParameterName = "@Result";
                sss.SqlDbType = SqlDbType.Int;
                sss.Value = intComResult;
                sss.Direction = ParameterDirection.Output;
                cm.Parameters.Add(sss);

                cm.ExecuteNonQuery();

                intComResult = Convert.ToInt32(cm.Parameters["@Result"].Value);

            }
            //catch (Exception ex)
            //{
            //    //int s = 0;

            //}
            finally
            {
                cn.Close();
            }
            return intComResult;
        }
        public static void RptUpdateItmPrntFlag(string strItemNum, int intEmpID)
        {
            //int intResult = 0;
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Report_UpdatePntFlag";
                cm.CommandTimeout = 60;

                cm.Parameters.Clear();

                SqlParameter qqq = new SqlParameter();
                qqq.ParameterName = "@ItemNum";
                qqq.SqlDbType = SqlDbType.VarChar;
                qqq.Size = 12;
                qqq.Value = strItemNum;
                cm.Parameters.Add(qqq);

                SqlParameter mmm = new SqlParameter();
                mmm.ParameterName = "@EmployeeID";
                mmm.SqlDbType = SqlDbType.Int;
                mmm.Value = intEmpID;
                cm.Parameters.Add(mmm);
            
                cm.ExecuteNonQuery();

            }
            //catch (Exception ex)
            //{
            //    //int s = 0;

            //}
            finally
            {
                cn.Close();
            }
        }

        //public static int GetItm4RptPrnt(int intDivision)
        //{
        //    SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strPhotoTrackerSQLConn"]);
        //    SqlCommand cm = new SqlCommand();
        //    int intResultRec = 0;

        //    try
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandType = CommandType.StoredProcedure;
        //        cm.CommandText = "Report_GetItemByDivision";
        //        cm.CommandTimeout = 90;

        //        cm.Parameters.Clear();

        //        SqlParameter qqq = new SqlParameter();
        //        qqq.ParameterName = "@Division";
        //        qqq.SqlDbType = SqlDbType.Int;
        //        qqq.Value = intDivision;
        //        cm.Parameters.Add(qqq);

        //        SqlParameter sss = new SqlParameter();
        //        sss.ParameterName = "@Result";
        //        sss.SqlDbType = SqlDbType.Int;
        //        sss.Value = intResultRec;
        //        sss.Direction = ParameterDirection.Output;
        //        cm.Parameters.Add(sss);

        //        SqlParameter kkk = new SqlParameter();
        //        kkk.ParameterName = "@ItemNum";
        //        kkk.SqlDbType = SqlDbType.VarChar;
        //        kkk.Size = 12;
        //        kkk.Value = strItemNum;
        //        kkk.Direction = ParameterDirection.Output;
        //        cm.Parameters.Add(kkk);

        //        cm.ExecuteNonQuery();

        //        //intResultRec = Convert.ToInt32(cm.Parameters["@Result"].Value);
        //        strItemNum = Convert.ToString(cm.Parameters["@ItemNum"].Value);
        //        intResultRec = Convert.ToInt16(cm.Parameters["@Result"].Value);

        //    }
        //    catch (SqlException ex)
        //    {
        //        intResultRec = 99;
        //    }

        //    finally
        //    {
        //        cn.Close();
        //    }
        //    return intResultRec;
        //}
        //public static DataTable GetEmpAccessRoles(string strEmployeeID)
        //{
        //    DataTable dtGetAccessRoles = new DataTable();
        //    SqlParameter[] Parameters = new SqlParameter[1];
        //    Parameters[0] = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
        //    Parameters[0].Value = strEmployeeID;
        //    dtGetAccessRoles = ExecuteStoredProcedure("Emp_GetAccessRoles", Parameters, null);
        //    return dtGetAccessRoles;
        //}

        public static DataTable GetEmpTrackAccessRoles(string strEmployeeID)
        {
            DataTable dtGetTrackAccessRoles = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            Parameters[0].Value = strEmployeeID;
            dtGetTrackAccessRoles = ExecuteStoredProcedure("Emp_GetTrkAccRolesByEmpID", Parameters, null);
            return dtGetTrackAccessRoles;
        }

        public static DataTable GetItemTrackInfo(string strItemNum)
        {
            DataTable dtItemTrackInfo = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@ItemNum", SqlDbType.VarChar);
            Parameters[0].Value = strItemNum;
            dtItemTrackInfo = ExecuteStoredProcedure("Track_GetItemTrackInfo", Parameters, null);
            return dtItemTrackInfo;
        }

        //public static DataTable GetComments(string strItemNum)
        //{
        //    DataTable dtComments = new DataTable();
        //    SqlParameter[] Parameters = new SqlParameter[1];
        //    Parameters[0] = new SqlParameter("@ItemNum", SqlDbType.VarChar);
        //    Parameters[0].Value = strItemNum;
        //    dtComments = ExecuteStoredProcedure("Track_GetComments", Parameters, null);
        //    return dtComments;
        //}
       
        //public static Employee_Insert()
        //{

        //    SqlConnection cn = new SqlConnection(Generals.PollingDatabase);
        //    SqlCommand cm = new SqlCommand();

        //    try
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandType = CommandType.StoredProcedure;
        //        SqlParameter pm = new SqlParameter("@StatusID", _statusID);

        //        cm.CommandText = "StatusInsert";
        //        cm.Parameters.Add(pm);
        //        cm.Parameters.Clear();
        //        cm.Parameters.AddWithValue("@Description", _statusDescription);

        //        cm.ExecuteNonQuery();
        //    }
        //    finally
        //    {
        //        cn.Close();
        //    }
        //}

        /// <returns>Datatable</returns>
        public static DataTable ExecuteStoredProcedure(string ProcedureName, SqlParameter[] Parameters, string Connection)
        {
            // Declare SQL objects needed to execute stored procedure and initialize to null
            DataTable dt = null;
            SqlCommand command = null;
            SqlConnection conn = null;
            SqlDataAdapter dataAdapter = null;

            try
            {

                if (Connection != null)
                {
                    conn = new SqlConnection(Connection);
                }
                else
                {
                    conn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
                }

                // Instantiate sqlcommand and set cmdtext to name of stored procedure and connection to sqlconnection
                command = new SqlCommand(ProcedureName, conn);
                // set sqlcommand commandtype to stored procedure
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 90;
                if (Parameters != null)
                {
                    foreach (SqlParameter parameter in Parameters)
                    {
                        // Make sure parameter in array is not null
                        if (parameter != null)
                            command.Parameters.Add(parameter);
                    }
                }
                // Instantiate sqldataadapter and set selectcommand to sqlcommand
                dataAdapter = new SqlDataAdapter(command);
                // Instantiate datatable
                dt = new DataTable();
                // Populate datatable by calling sqldataadapter's fill method and passing in datatable
                dataAdapter.Fill(dt);
            }
            catch (Exception e)
            {
                // Bubble SQL Server error up
                throw new Exception("Data Access Error: " + e.Message);
            }
            finally
            {
                // Close the sqlconnection
                //This will always get called regardless if there is an error or not

                conn.Close();
            }

            return dt;
        }


        //internal static bool CheckGersItemByDiv(string strItemNum)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}
    }
}
