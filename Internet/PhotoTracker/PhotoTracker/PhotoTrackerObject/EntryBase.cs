using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;


namespace PhotoTracker
{
    public abstract class EntryBase
    {
        List<EntryBase> m_listRecver = new List<EntryBase>();
        List<EntryBase> m_listSender = new List<EntryBase>();
        public WebControl m_webControl = null;
        public string m_strMsgType = null;

        public EntryBase(WebControl webControl)
        {
            m_webControl = webControl; 
        }

        //call from obj of StartRegister()
        protected void Register(EntryBase RecvObj)
        {
            m_listRecver.Add(RecvObj);
        }
        //call from EntryObjManager
        public void Notification(String strMsgType, int nMsgValue, int nEntryType, bool bMsgStatus, int intEmpID, string strItmNum, string strDiv)
        {
            foreach (EntryBase obj in m_listRecver)
            {
                obj.DoNotification(strMsgType, nMsgValue, nEntryType, bMsgStatus, intEmpID, strItmNum, strDiv);
            }
        }

        public virtual void DoNotification(String strMsgType, int nMsgValue, int nEntryType, bool bMsgStatus, int intEmpID, string strItmNum, string strDiv)
        {
            //if (lMsgValue == 1)  //text box
            //{
            //    TextBox txtRev = null;
            //    txtRev = (TextBox)m_webControl;
            //    if ((strMsgType == "ShowEntryRev") && (nEntryType == 1) && (bMsgStatus == true))
            //    {
            //        txtRev.Enabled = false;
            //    }
            //}
            //else if (lMsgValue == 2)  //drop down list
            //{
            //    DropDownList dll_webControl = null;
            //    dll_webControl = (DropDownList)m_webControl;

            //    //if ((strMsgType == "SelectShow")  && (nEntryType == 1) && (bMsgStatus == true)) //drop down copy list in
            //    if ((strMsgType == "SelectShow") && (bMsgStatus == true)) //drop down copy list in
            //    {
            //        dll_webControl.Items.Clear();
            //        DataTable dtCopy = Data.GetCopyUsers();
            //        dll_webControl.DataSource = dtCopy;
            //        DropDownDataBind(dll_webControl);
            //    }
            //}
        }
       
        //public void DropDownDataBind(DropDownList dll_webControl)
        //{
        //    dll_webControl.DataTextField = "EmpName";
        //    dll_webControl.DataValueField = "EmpName";
        //    dll_webControl.DataBind();
        //    dll_webControl.Enabled = true;
        //}
        //call from EntryObjManager
        public void SetSender(EntryBase SenderObj, string strMsgType)
        {
            SenderObj.m_strMsgType = strMsgType;
            m_listSender.Add(SenderObj);
        }
        public void SetSender(EntryBase SenderObj)
        {
            m_listSender.Add(SenderObj);
        }

        //call from EntryObjManager
        public void StartRegister()
        {
            foreach (EntryBase obj in m_listSender)
            {
                obj.Register(this);
            }
        }
    }
}
