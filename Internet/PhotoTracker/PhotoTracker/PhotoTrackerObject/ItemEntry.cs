using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections;
using System.Text;


namespace PhotoTracker
{
    public class ItemEntry : CollectionBase
    {
        #region Members
        string _strItemNum = String.Empty;
        string _strEmpName = String.Empty;
       
        string _strEmpID = String.Empty;

        DateTime _dtTrackDate;
        int _intEntryType;
        int _intEmpID;
        int _intEmpTrkAccStatus;
        bool _bPerfRight = false;


        #endregion Members

        #region Properties
        public string StrItemNum
        {
            get { return _strItemNum; }
            set { _strItemNum = value; }
        }
        public string StrEmpName
        {
            get { return _strEmpName; }
            set { _strEmpName = value; }
        }
     
        public string StrEmpID
        {
            get { return _strEmpID; }
            set { _strEmpID = value; }
        }
        public DateTime DtTrackDate
        {
            get { return _dtTrackDate; }
            set { _dtTrackDate = value; }
        }
        public int IntEntryType
        {
            get { return _intEntryType; }
            set { _intEntryType = value; }
        }
        public int IntEmpTrkAccStatus
        {
            get { return _intEmpTrkAccStatus; }
            set { _intEmpTrkAccStatus = value; }
        }
     
        public int IntEmpID
        {
            get { return _intEmpID; }
            set { _intEmpID = value; }
        }

        public bool BPerfRight
        {
            get { return _bPerfRight; }
            set { _bPerfRight = value; }
        }
        #endregion Properties

        public void PopulateItemEntryProperties(SqlDataReader dr)
        {
            _strItemNum = Convert.ToString(dr["ItemNum"]);
            _strEmpID = Convert.ToString(dr["EmployeeID"]);
            _strEmpName = Convert.ToString(dr["EmpName"]);
           // _strEmpLstName = Convert.ToString(dr["EmpLastName"]);

            if (dr["TrackDate"] !=System.DBNull.Value)
            {
                _dtTrackDate = Convert.ToDateTime(dr["TrackDate"]);
            }
            else
			{
                _dtTrackDate = DateTime.Parse("1/1/1980");
			}

            _intEntryType = Convert.ToInt16(dr["EntryType"]);
            _intEmpTrkAccStatus = Convert.ToInt16(dr["EmpTrackAccStatus"]);
            _bPerfRight = true;
           
        }
        internal static ItemEntry GetItem(SqlDataReader dr)
        {
            ItemEntry _oneItem = new ItemEntry();
            _oneItem.PopulateItemEntryProperties(dr);
            return _oneItem;
        }

        //internal static bool GetEntryRight(int EntryType)
        //{
        //    return;
        //}

        //public static Item GetOneItemEntry(string strItemNo)
        //{
        //    ItemEntry theOneItemEntry = new ItemEntry();
        //    theOneItemEntry.OneItemEntryFetch(strItemNo);
        //    return theOneItemEntry;
        //}
        //private void OneItemEntryFetch(string strItemNo)
        //{

        //    SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strPhotoTrackerSQLConn"]);
        //    SqlCommand cm = new SqlCommand();

        //    try
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandType = CommandType.StoredProcedure;
        //        cm.CommandText = "usp_GetItemEntryByItemNo";

        //        SqlDataReader dr;
        //        cm.Parameters.Clear();

        //        SqlParameter mmm = new SqlParameter();
        //        mmm.ParameterName = "@ItemNum";
        //        mmm.SqlDbType = SqlDbType.VarChar;
        //        mmm.Value = strItemNo;
        //        cm.Parameters.Add(mmm);

        //        dr = cm.ExecuteReader();

        //        while (dr.Read())
        //            List.Add(ItemEntry.GetItem(dr));
        //    }
        //    catch (Exception ex)
        //    {
        //        //int s=0;

        //    }
        //    finally
        //    {
        //        cn.Close();
        //    }
        //}
    }
}
