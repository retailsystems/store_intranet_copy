<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Comments.aspx.cs" Inherits="PhotoTracker.Comments" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Commments</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
</head>
<body>
		<form id="Form1" method="post" runat="server">
		<TABLE class="innerTableDetail" id="tblOption" cellSpacing="0" cellPadding="0" width="60%" align="center" border="0" runat="server">
					<tr height=20><td></td></tr>
					<tr>
					<td colSpan="3">
			<asp:datalist id="dlItemView" runat="server" Width="100%"
				EnableViewState="True" RepeatLayout="Flow" RepeatDirection="Horizontal" OnSelectedIndexChanged="dlItemView_SelectedIndexChanged">
				<HeaderTemplate>
					<table width="560" cellspacing="0" cellpadding="2" class="innerTableView" align="center">
						<tr class="tableTitle">
							<td colspan="3" class="tableTitle">Comments</td>
						</tr>
						
						<tr class="tableHeader">
							<td align="center" width="60"><b>
								Employee Name</b>
							</td>
							<td align="center" width="53"><b>
								Date</b>
							</td>
							<td align="center" width="387"><b>
								Comments</b>
							</td>
						</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr id="total" runat="server">
						<td align="left">
							<%# DataBinder.Eval(Container.DataItem, "StrEmpName")%>
						</td>
						<td align="left">
							<%# DataBinder.Eval(Container.DataItem, "DtCommentdDate")%>
						</td>
						<td align="left">
							<%# DataBinder.Eval(Container.DataItem, "StrComments")%>
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					</table>
				</FooterTemplate>
			</asp:datalist> 
			
			</td></tr>
			<tr><td><table align="center">
			<tr height="20"><td colspan=3></td></tr>
			<tr>
			<TD colspan=3 vAlign="top" align="left" rowSpan="3" class="tableTitle" style="width: 555px">Add New Comment Here:<BR>
					<asp:textbox id="txtComments" runat="server" CssClass="tableInput" Width="550" TextMode="MultiLine" Rows="6"></asp:textbox></TD>
			</tr></table>	
			</td></tr>
			<tr><td width="550"><font color="#cc0000">&nbsp;&nbsp;&nbsp;<asp:Label ID ="lblNoItemMsg" runat="server"></asp:Label></font>
            </td></tr>
			<tr><TD align="right" valign="middle">
					<asp:button id="btnSaveNotes" runat="server" Text="SAVE" CssClass="buttonYellow" OnClick="btnSaveNotes_Click"></asp:button>&nbsp;&nbsp;
					<asp:button id="btnClose" runat="server" Text="EXIT" CssClass="buttonYellow" OnClick="btnClose_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</TD></tr>
			<tr><td></td></tr>
			</table>
			</form>
	</body>
</HTML>