<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SingleReport.aspx.cs" Inherits="PhotoTracker.SingleReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<HTML>
	<HEAD>
		<title>SingleReport</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
</head>
<body bottomMargin="0" bgColor="#000000" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header>
			<div id="main" style="width: 989px"><br>
  
				<TABLE id="tblRange" class="innerTable" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0" runat="server">
					<tr class="tableTitle">
						<td colSpan="2">&nbsp; Print SINGLE Photo Tracking Paperwork</td>
					</tr>
					<tr height="50"><td colSpan="2" style="height: 50px"></td></tr>
					<tr><td colSpan="2" style="height: 134px"><TABLE id="TABLE1" class="innerTableDetailThin" cellSpacing="0" cellPadding="1" width="30%" align="center" border="0" runat="server">
					<tr align= "left"><td colSpan="2" valign = "top" class="innerTableDetailThin">&nbsp;&nbsp;Please Input Item Number:</td></tr>
					<tr height = "90" align="left" width ="330" >
						<td valign = "middle">&nbsp;&nbsp;<table><tr style="HEIGHT: 21px"><td width="150">&nbsp;<asp:textbox id="txtItemNum" runat="server" CssClass="tableInput" columns="12" OnTextChanged="txtItemNum_TextChanged"></asp:textbox></td>
																    <td width="300"><font color="#cc0000"><asp:Label ID ="lblNoItemMsg" runat="server"></asp:Label></font></td></tr></table></TD></tr>
                    <tr><td>&nbsp;&nbsp;<asp:Button id="btnPrint" runat="server" Text="Generate Report" OnClick="btnPrint_Click" OnClientClick="return confirm('Please confirm your Selection for print then click OK?')"></asp:Button>&nbsp;&nbsp;</td>
					</tr>
					<tr><td colSpan="2"><font color="#cc0000"><asp:Label id="lblItmMsg" runat="server">&nbsp;</asp:Label></font></td></tr>
					</TABLE></td></tr>
				    <tr height="170"><td colSpan="2"></td></tr>
				</TABLE>
				<br /><br />
			</div>
			</div>
		</form>
	</body>
</HTML>