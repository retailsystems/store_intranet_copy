using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PhotoTracker
{
    public partial class Calendar : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Calendar calDate;

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
        }
        protected void Calendar_DayRender(object sender, System.Web.UI.WebControls.DayRenderEventArgs e)
        {

            // Clear the link from this day

            e.Cell.Controls.Clear();

            // Add the custom link

            System.Web.UI.HtmlControls.HtmlGenericControl Link = new System.Web.UI.HtmlControls.HtmlGenericControl();

            Link.TagName = "a";

            Link.InnerText = e.Day.DayNumberText;

            Link.Attributes.Add("href", String.Format("JavaScript:window.opener.document.{0}.value = \'{1:d}\'; window.close();", Request.QueryString["field"], e.Day.Date));

            if (e.Day.Date < DateTime.Now.Date)
            {
                e.Cell.BackColor = System.Drawing.Color.DarkGray;
                //e.Cell.Visible = false;
                e.Cell.Enabled = false;
                
                
            }
           
            // By default, this will highlight today's date.

            //if (e.Day.IsSelected)
           // if (e.Day.IsSelectable == true) 
            if (e.Cell.Enabled ==true)
            {
                Link.Attributes.Add("style", this.calDate.SelectedDayStyle.ToString());
            }

            // Now add our custom link to the page

            e.Cell.Controls.Add(Link);

        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.calDate.DayRender += new System.Web.UI.WebControls.DayRenderEventHandler(this.Calendar_DayRender);
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
