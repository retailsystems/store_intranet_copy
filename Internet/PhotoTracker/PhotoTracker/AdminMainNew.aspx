<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminMainNew.aspx.cs" Inherits="PhotoTracker.AdminMainNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>AdminMain Page</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
    <script language="javascript" type="text/javascript">
   
    //Function checkBoxClicked checks the checkboxes based on the user input
	function checkBoxClicked(cbxSelect,cbxAdmin,cbxPT,cbxNew,cbxCP,cbsPH,cbsPHED,cbxAPR,cbxReports,ctl)
	{
		var cbkSelect = document.getElementById(cbxSelect);
		var cbkAdmin = document.getElementById(cbxAdmin);
		var cbkPT = document.getElementById(cbxPT);
		var cbkReports = document.getElementById(cbxReports);
        var cbkNew= document.getElementById(cbxNew);
		var cbkCP = document.getElementById(cbxCP);
		var cbkPH= document.getElementById(cbsPH);
		var cbkPHED = document.getElementById(cbsPHED);
		var cbkAPR = document.getElementById(cbxAPR);
		
		var retVal = "false";
				
			if(cbkAdmin.checked == true || cbkPT.checked == true || cbkReports.checked == true || cbkNew.checked == true || cbkCP.checked == true|| cbkPH.checked == true || cbkPHED.checked == true || cbkAPR.checked == true)
			{
				retVal = "true";
		
			}
			else if(cbkAdmin.checked == false || cbkPT.checked == false || cbkReports.checked == false || cbkNew.checked == false || cbkCP.checked == false|| cbkPH.checked == false || cbkPHED.checked == false || cbkAPR.checked == false)
			{
				retVal = "true";
				
			}
			else
			{
				retVal = "false";
			}
		if(retVal == "true")
		{
		  //cbkSelect.style.visibility = 'visible';
		 // alert("3.");
		 // alert("abc:" + abc.value);
		 // alert("5.");
		   cbkSelect.checked = true;
			//alert("5.");
		}
	}

</script>

</HEAD>
<body>
    <form id="form1" runat="server">
     <div id="header"  align="center" style="width: 926px; height: 517px"><uc1:header id="Header1" runat="server" ></uc1:header>
        &nbsp;
        <strong><span style="font-size: 11pt"><table border="0" bgcolor="#D3D3D3"><tr><td><font size="2"><b>&nbsp;  SELECT &nbsp;&nbsp;EMPID &nbsp; FIRSTNAME&nbsp; FULLNAME&nbsp;&nbsp;&nbsp;PWD&nbsp;&nbsp;&nbsp;&nbsp;ADMIN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp; &nbsp;&nbsp; PT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; REPORTS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;NEW&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;COPY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;PHOTO&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PH/ED&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> </td></tr></table><br />&nbsp;<br />   
        </span></strong>                                                                                                                                                                                                                                                                                                     

        <div style=" OVERFLOW: auto; WIDTH: 931px; HEIGHT: 600px; position: relative; left: 0px; top: -37px; border: solid 5px #ffffff;" >
         
        

        <asp:GridView ID="GridView1" runat="server" ShowHeader="false" ShowFooter="false" AutoGenerateColumns ="false" OnRowDataBound="GridView1_RowDataBound" width="899px" Height="141px" style="text-align: center;"  HeaderStyle-BackColor = "red" FooterStyle-BackColor="red"
         Font-Names = "Arial"  Font-Size = "8pt"   onrowupdating = "UpdateEmployee" EnableViewState=true >
             
             <Columns>
              
               <asp:TemplateField HeaderText="Select" FooterText="Select">
                    <ItemTemplate>
                   
                       <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="false" />
                    
                    </ItemTemplate>
               </asp:TemplateField> 
              
           
               <asp:TemplateField >
                    <HeaderTemplate> 
                    </HeaderTemplate> 
                    <ItemTemplate>
                    </ItemTemplate>
               </asp:TemplateField> 
                
                
                <asp:BoundField DataField="EmployeeID" HeaderText="EmployeeID" FooterText="EmployeeID" ReadOnly="True">
                    <ItemStyle Width="10px" />
                </asp:BoundField>
                <asp:BoundField DataField="UserName" HeaderText="USERNAME"  FooterText="USERNAME" ReadOnly="True">
                    <ItemStyle Width="10px" />
                </asp:BoundField>
                <asp:BoundField DataField="LongName" HeaderText="LONGNAME" FooterText="LONGNAME" ReadOnly ="True">
                    <ItemStyle Width="10px" />
                </asp:BoundField>
                <asp:BoundField DataField="Password" HeaderText="PASSWORD" FooterText="PASSWORD" ReadOnly="True">
                    <ItemStyle Width="10px" />
                </asp:BoundField>
                              
               
                  <asp:TemplateField HeaderText="ADMIN" FooterText="ADMIN">
                     <ItemTemplate>
                        <asp:CheckBox ID="Admin" runat="server" />
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PT" FooterText="PT">
                     <ItemTemplate>
                        <asp:CheckBox ID="PT" runat="server"/>
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                <asp:TemplateField HeaderText="REPORTS" FooterText="REPORTS">
                     <ItemTemplate>
                        <asp:CheckBox ID="Reports" runat="server" />
                     </ItemTemplate>
                   <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
               
                <asp:TemplateField HeaderText="NEW" FooterText="NEW">
                     <ItemTemplate>
                        <asp:CheckBox ID="New" runat="server" />
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                              
                </asp:TemplateField>
                <asp:TemplateField HeaderText="COPY" FooterText="COPY">
                     <ItemTemplate>
                        <asp:CheckBox ID="Copy" runat="server" />
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PHOTO" FooterText="PHOTO">
                     <ItemTemplate>
                        <asp:CheckBox ID="Photo" runat="server" />
                     </ItemTemplate>
                    <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PH/ED" FooterText="PH/ED">
                     <ItemTemplate>
                        <asp:CheckBox ID="PHED" runat="server" />
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                <asp:TemplateField HeaderText="APPR" FooterText="APPR">
                     <ItemTemplate>
                        <asp:CheckBox ID="APPR" runat="server" />
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                                
             </Columns>   

            <HeaderStyle CssClass="headerGD" ForeColor="Black" BackColor="Red" />
            <FooterStyle ForeColor="Black" BackColor="Red" />
         </asp:GridView> 
    

         </div>
    <asp:Button ID="btnAdd" runat="server" Style="z-index: 101; left: 201px; 
            top: 600px" Text="AddUser" Font-Bold="True" Font-Size="10pt" OnClick="btnAdd_Click" Width="155px" />&nbsp;&nbsp; 
                
    <asp:Button ID="btnPwdChg" runat="server" Style="z-index: 101; left: 201px; 
            top: 600px" Text="ChangeUserPassword" Font-Bold="True" Font-Size="10pt"  Width="155px" OnClick="btnPwdChg_Click" UseSubmitBehavior="False" />&nbsp;&nbsp; 
    <asp:Button ID="btnWholUpdate" runat="server" Style="z-index: 101; left: 201px; 
            top: 600px" Text="Update Users" Font-Bold="True" Font-Size="10pt"  Width="155px" OnClick="btnWUpdate_Click" UseSubmitBehavior="False" />
            </div>

      </form>
</body>
</html>
