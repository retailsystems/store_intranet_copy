using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PhotoTracker
{
    public partial class TrackMenu : System.Web.UI.Page
    {
        DataTable dtUser;
        DataTable dtPermit;
        string strGroupNo = String.Empty; 

        protected void Page_Load(object sender, EventArgs e)
        {
            pngAdmin.Visible = false;
            pngPhotoTrk.Visible = false;
            pngAdminNo.Visible = false;
            pngReport.Visible = false;

            if (!this.IsPostBack)
            {
                try
                {
                    //get user DataTable from session
                    dtUser = (DataTable)Session["UserProfile"];

                    dtPermit = (DataTable)Session["ViewPermission"];
                    //strUserProfile = Session["UserProfile"].ToString();  //add session

                    if (dtPermit.Rows.Count > 0)
                    {
                         for (int i = 0; i < (dtPermit.Rows.Count); i++)
                         {
                            strGroupNo = dtPermit.Rows[i]["GroupNo"].ToString().Trim();
                            if (strGroupNo == "1")
                            {
                               pngAdmin.Visible = true;
                            }
                            else if (strGroupNo == "2")
                            {
                               pngPhotoTrk.Visible = true;
                               
                            }
                            else if (strGroupNo == "3")
                            {
                               pngReport.Visible = true;
                                
                            }
                         }
                         pngAdminNo.Visible = true;
                         
                        //LoadDefaultDate();
                        //LoadUserOptions();
                    }
                    else
                    {
                        //no proper permission go back to Login
                        Response.Redirect("./Login.aspx");

                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.StackTrace + "\n\n" + ex.Message);
                }
            }
          
        }
    }
    }
