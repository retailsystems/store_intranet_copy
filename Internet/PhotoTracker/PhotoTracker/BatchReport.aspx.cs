using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PhotoTracker
{
    public partial class BatchReport : System.Web.UI.Page
    {
        DataTable dtUser;
        DataTable dtPermit;
        public DataTable dtItem; 
        protected void Page_Load(object sender, EventArgs e)
        {
            //DropDownStoreGroupList.SelectedIndex = 0;
            ////StoreGroupLoad(DropDownStoreGroupList.SelectedIndex);
            //StoreGroupLoad(); 
            if (!Page.IsPostBack)
            {
                dtUser = (DataTable)Session["UserProfile"];
                //int nCount=dtUser.Rows.Count;
                //int[] div= new int[nCount];

                //for(int i=0;i<nCount;i++)
                //    div[i]=int.Parse(dtUser.Rows[i]["Division"].ToString().Trim());
                
                dtPermit = (DataTable)Session["ViewPermission"];
                bool bHasPermt = false;

                bHasPermt = AppUser.CheckSecurity("3", dtPermit);

                if (bHasPermt == true)
                {
                    //ddlDivisionList.Items.Clear();
                    //for(int i=0;i<nCount;i++)
                    //    LoadDropdown(ddlDivisionList, div[i].ToString(), false);
                    //LoadDropdown(ddlDivisionList, intDivision.ToString(), false);
                    DropDownDataBind();
                }
                else
                {
                    //no proper permission go back to Login
                    Response.Redirect("./Login.aspx");

                }
               
            }
        }
        private void DropDownDataBind()
        {
            ddlDivisionList.Items.Clear();
                  
            DataTable dtComps = Data.GetCompanyBranchList();
            ddlDivisionList.DataSource = dtComps;
            ddlDivisionList.DataTextField = "DivisionName";
            ddlDivisionList.DataValueField = "Division";
            ddlDivisionList.DataBind();
            ddlDivisionList.Enabled = true;
        }

        private void LoadDropdown(DropDownList ddlList, string strName, bool IsSelected)
        {
            string strBrandName = GetBrandName(strName);
           // ddlList.Items.Clear();
            ListItem itm = new ListItem();
            itm.Text = strBrandName;
            itm.Value = strName;
            ddlList.Items.Add(itm);
            if (IsSelected)
            {
                ddlList.Enabled = false;

            }
        }
        protected void DropDownDivisionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //SelectDivision();
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            SelectDivision();
            
        }
        public void LoadDivision(int intDiv)
        {
            
            dtUser = (DataTable)Session["UserProfile"];
            //string dbconn = ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString();
            int intPrintAllowTime = int.Parse(ConfigurationManager.AppSettings["ItemMutiPrintAllow"].ToString());
            if (intPrintAllowTime == 0)
            {
                if (dtUser != null)
                {
                    int EmployeeID = int.Parse(dtUser.Rows[0]["EmployeeID"].ToString().Trim());

                    dtItem = Data.GetItm4RptPrnt(intDiv);
                    if (dtItem.Rows.Count > 0)
                    {
                        string strItm = string.Empty;

                        for (int i = 0; i < (dtItem.Rows.Count); i++)
                        {
                            strItm += dtItem.Rows[i]["ItemNum"].ToString().Trim();

                            Data.RptUpdateItmPrntFlag(dtItem.Rows[i]["ItemNum"].ToString(), EmployeeID);
                            if (i == (dtItem.Rows.Count-1))
                            { strItm = strItm; }
                            else 
                            {
                              strItm += ",";
                            }
                        }

                        string jScriptValidator;
                        jScriptValidator = "<script>" +
                        "window.open( ' http://CAPHOTRKPRD/ReportServer/Pages/ReportViewer.aspx?%2fPhoto+Tracker%2fPhotoTrackPaperWork&rs:Command=Render&rc:Parameters=true"
                        + "&P_ItemList="
                        + strItm
                        + "', '', 'toolbar=0');";
                        jScriptValidator += " </script>";
                        Page.RegisterStartupScript("regJSval1", jScriptValidator);

                    }
                    else
                    {
                        lblItmMsg.Text = "There is no Item needed to be printed at this time. Please try later.";
                    }
                }
                else
                {
                    lblItmMsg.Text = "Can not find Employee ID.";
                }
            }
            else 
            {   
                //allow the item multiple print
            }
        }

        protected void SelectDivision()
        {   
            //bool bHasItm = false;
            //int intResult = 0;
            //int intDiv =0;
            switch (int.Parse(ddlDivisionList.SelectedValue.ToString()))
            {
               case 1:
                   
                   //intDiv = 1;
                   LoadDivision(1);
                   //intResult = Data.GetItm4RptPrnt(1);
                   //if (intResult == 1)
                   //{
                   //    bHasItm = true;
                   //}
                   //else if (intResult == 2)
                   //{bHasItm = false ;} 
                   break;
                   
               case 5:
                   //intDiv = 5;
                   LoadDivision(5);
                   // intResult = Data.GetItm4RptPrnt(5);
                   //if (intResult == 1)
                   //{
                   //    bHasItm = true;
                   //}
                   //else if (intResult == 2)
                   //{bHasItm = false ;} 
                   break;
                   
               case 8:
                   LoadDivision(8);
                   //  intResult = Data.GetItm4RptPrnt(8);
                   //if (intResult == 1)
                   //{
                   //    bHasItm = true;
                   //}
                   //else if (intResult == 2)
                   //{ bHasItm = false; } 
                   break;
               
            }
            //return bHasItm;
        }
        protected string GetBrandName(string strBrand)
        {
            string strBrandName = string.Empty;
            switch (int.Parse(strBrand))
            {
                case 1:
                    strBrandName = "Hot Topic";
                    break;
                case 5:
                    strBrandName = "Torrid";
                    break;
                case 8:
                    strBrandName = "Black Heart";
                    break;
            }
            return strBrandName;
        }
    }
}
