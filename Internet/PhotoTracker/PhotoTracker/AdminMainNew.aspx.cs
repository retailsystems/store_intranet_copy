using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections.Generic;


namespace PhotoTracker
{
    public partial class AdminMainNew : System.Web.UI.Page
    {
        string dbconn = ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString();
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand _cmd = new SqlCommand();
        DataSet ds = new DataSet();
        string _EmpSql, _EmpAccessSql, _EmpTrackSQL, GroupId, TrackGroupId;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BindData();

            }
            //  GridViewRow grdRow = GridView1.Rows[2]; //specific grid row
            //   CheckBox _ckb;
            //  _ckb = (CheckBox)grdRow.FindControl("Admin");
            //  _ckb.AutoPostBack = true;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("Admin.aspx", false);
        }
        private void BindData()
        {
            SqlConnection _Sqlconn = new SqlConnection(dbconn);
            try
            {
                _Sqlconn.Open();
                //_EmpSql = "select EmployeeID as EmployeeId,substring(EmpFirstName,1,1)+EmpLastName as UserName" +
                //          " ,substring(EmpFirstName,1,len(EmpFirstName))+' '+substring(EmpLastName,1,len(EmpLastName)) as LongName" +
                //          " ,EmpPassword as Password" +
                //          " from employee";
                _EmpSql = "select EmployeeID as EmployeeId,substring(EmpFirstName,1,1)+EmpLastName as UserName" +
                          " ,substring(EmpFirstName,1,len(EmpFirstName))+' '+substring(EmpLastName,1,len(EmpLastName)) as LongName" +
                          " ,'***********' as Password" +
                          " from employee where active not in ('0') order by EmpFirstName";

                da.SelectCommand = new SqlCommand(_EmpSql, _Sqlconn);
                da.Fill(ds, "tblEmployee");
                //GridView.DataSource = ds.Tables["tblEmployee"];
                //GridView.DataBind();
                GridView1.DataSource = ds.Tables["tblEmployee"];
                GridView1.DataBind();

                foreach (GridViewRow grdRow in GridView1.Rows)
                {

                    _EmpAccessSql = "Select EmployeeID,GroupNo from dbo.EmpAccess where  EmployeeID = " + grdRow.Cells[2].Text.ToString();


                    da = new SqlDataAdapter(_EmpAccessSql, _Sqlconn);
                    da.Fill(ds, "tblEmpAccess");

                    CheckBox _checkbox;
                    foreach (System.Data.DataRow dr in ds.Tables["tblEmpAccess"].Rows)
                    {
                        switch (dr["GroupNo"].ToString())
                        {
                            case "1":
                                _checkbox = (CheckBox)grdRow.FindControl("Admin");
                                break;
                            case "2":
                                _checkbox = (CheckBox)grdRow.FindControl("PT");
                                break;
                            case "3":
                                _checkbox = (CheckBox)grdRow.FindControl("Reports");
                                break;
                            default:
                                _checkbox = (CheckBox)grdRow.FindControl("Reports");
                                break;
                        }
                        if (dr["EmployeeID"].ToString() == grdRow.Cells[2].Text.ToString())
                        {
                            _checkbox.Checked = true;
                        }
                        else
                        {
                            _checkbox.Checked = false;
                        }
                    }

                    _EmpTrackSQL = "Select EmployeeID,TrackGroupNo from dbo.EmpTrackAccess where  EmployeeID = " + grdRow.Cells[2].Text.ToString();

                    da = new SqlDataAdapter(_EmpTrackSQL, _Sqlconn);
                    da.Fill(ds, "tblEmpTrackAccess");

                    foreach (System.Data.DataRow dr in ds.Tables["tblEmpTrackAccess"].Rows)
                    {
                        switch (dr["TrackGroupNo"].ToString())
                        {
                            case "1":
                                _checkbox = (CheckBox)grdRow.FindControl("New");
                                break;
                            case "2":
                                _checkbox = (CheckBox)grdRow.FindControl("Copy");
                                break;
                            case "3":
                                _checkbox = (CheckBox)grdRow.FindControl("Photo");
                                break;
                            case "4":
                                _checkbox = (CheckBox)grdRow.FindControl("PHED");
                                break;
                            case "5":
                                _checkbox = (CheckBox)grdRow.FindControl("APPR");
                                break;
                            default:
                                _checkbox = (CheckBox)grdRow.FindControl("New");
                                break;
                        }
                        if (dr["EmployeeID"].ToString() == grdRow.Cells[2].Text.ToString())
                        {
                            _checkbox.Checked = true;
                        }
                        else
                        {
                            _checkbox.Checked = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                _Sqlconn.Close();
                _Sqlconn.Dispose();
            }

        }
        protected void DeleteEmployee(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow grdRow = GridView1.Rows[e.RowIndex];
            string id = GridView1.Rows[e.RowIndex].Cells[2].Text; //get the id of the selected row
            DeleteRecord(id);//call delete method
            //BindData();//rebind grid to reflect changes made



        }
        protected void DeleteRecord(string ID)
        {
            SqlConnection _Sqlconn = new SqlConnection(dbconn);
            string sqlStatement = "Delete from EmpAccess where EmployeeID=@EmployeeID;" +
                                   "Delete from EmpTrackAccess  where EmployeeID=@EmployeeID" +
                                   "Delete from Employee where EmployeeID=@EmployeeID;";
            try
            {
                _Sqlconn.Open();

                _cmd = new SqlCommand(sqlStatement, _Sqlconn);
                _cmd.Parameters.AddWithValue("@EmployeeID", ID);
                _cmd.CommandType = CommandType.Text;
                _cmd.ExecuteNonQuery();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "Deletion Error:";
                msg += ex.Message;
                throw new Exception(msg);

            }
            finally
            {
                _Sqlconn.Close();
            }
        }


        protected void UpdateEmployee(object sender, GridViewUpdateEventArgs e)
        {
            //Loop through gridview rows to find checkbox 
            //and check whether it is checked or not 

            string id = GridView1.Rows[e.RowIndex].Cells[2].Text; //get the id of the selected row
            GridViewRow grdRow = GridView1.Rows[e.RowIndex]; //specific grid row

            CheckBox _ckb;
            _ckb = (CheckBox)grdRow.FindControl("Admin");
            bool bRet = _ckb.Checked;

            SqlConnection _Sqlconn = new SqlConnection(dbconn);
            //SqlDataReader _rdr = null;


            //delete from EmpAccess table
            string _deleteSql = "delete FROM PhotoTracker.dbo.EmpAccess WHERE EmployeeID = @EmployeeID ;" +
                                "delete from PhotoTracker.dbo.EmpTrackAccess WHERE EmployeeID = @EmployeeID";

            _Sqlconn.Open();
            _cmd = new SqlCommand(_deleteSql, _Sqlconn);
            _cmd.Parameters.AddWithValue("@EmployeeID", id);
            _cmd.CommandType = CommandType.Text;
            _cmd.ExecuteNonQuery();

            List<CheckBox> _cbkFormlist = new List<CheckBox>();
            _cbkFormlist.Add((CheckBox)grdRow.FindControl("Admin"));
            _cbkFormlist.Add((CheckBox)grdRow.FindControl("PT"));
            _cbkFormlist.Add((CheckBox)grdRow.FindControl("Reports"));

            List<CheckBox> _cbkWorkGrplist = new List<CheckBox>();
            _cbkWorkGrplist.Add((CheckBox)grdRow.FindControl("New"));
            _cbkWorkGrplist.Add((CheckBox)grdRow.FindControl("Copy"));
            _cbkWorkGrplist.Add((CheckBox)grdRow.FindControl("Photo"));
            _cbkWorkGrplist.Add((CheckBox)grdRow.FindControl("PHED"));
            _cbkWorkGrplist.Add((CheckBox)grdRow.FindControl("APPR"));

            //AdminAccess loop
            foreach (CheckBox c in _cbkFormlist)
            {
                if (c.Checked == true)
                {
                    switch (c.ID)
                    {
                        case "Admin":
                            GroupId = "1";
                            break;
                        case "PT":
                            GroupId = "2";
                            break;
                        case "Reports":
                            GroupId = "3";
                            break;
                        default:
                            GroupId = "1";
                            break;
                    }

                    //insert into AdminAccess
                    string _insertSql = "insert into PhotoTracker.dbo.EmpAccess (EmployeeID,GroupNo) values(@EmployeeID,@GroupNo)";
                    SqlCommand cmd = new SqlCommand(_insertSql, _Sqlconn);
                    cmd.Parameters.AddWithValue("@EmployeeID", id);
                    cmd.Parameters.AddWithValue("@GroupNo", GroupId);

                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }

            }
            //TrackEmployeeAccess loop
            foreach (CheckBox c in _cbkWorkGrplist)
            {
                if (c.Checked == true)
                {
                    switch (c.ID)
                    {
                        case "New":
                            TrackGroupId = "1";
                            break;
                        case "Copy":
                            TrackGroupId = "2";
                            break;
                        case "Photo":
                            TrackGroupId = "3";
                            break;
                        case "PHED":
                            TrackGroupId = "4";
                            break;
                        case "APPR":
                            TrackGroupId = "5";
                            break;
                        default:
                            TrackGroupId = "1";
                            break;
                    }

                    //insert into PhotoTracker.dbo.EmpTrackAccess
                    string _insertSql = "insert into PhotoTracker.dbo.EmpTrackAccess (EmployeeID,TrackGroupNo) values(@EmployeeID,@TrackGroupNo)";
                    SqlCommand cmd = new SqlCommand(_insertSql, _Sqlconn);
                    cmd.Parameters.AddWithValue("@EmployeeID", id);
                    cmd.Parameters.AddWithValue("@TrackGroupNo", TrackGroupId);

                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();


                }

            }

            _Sqlconn.Close();

            Response.Write("Successfully updated record for EmployeeID: " + id);
        }

        protected void btnPwdChg_Click(object sender, EventArgs e)
        {
            //Response.Redirect("AdminPassUpdate.aspx");
            Response.Redirect("AdminPassUpdate.aspx", false);
        }


        protected void btnWUpdate_Click(object sender, EventArgs e)
        {
            //CheckBox _ckb;
            //_ckb = (CheckBox)grdRow.FindControl("Admin");
            //bool bRet = _ckb.Checked;

            //SqlConnection _Sqlconn = new SqlConnection(dbconn);
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                CheckBox chkUpdate = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkSelect");


                if (chkUpdate != null)
                {
                    if (chkUpdate.Checked)
                    {
                        // Get the values of textboxes using findControl
                        string id = GridView1.Rows[i].Cells[2].Text;
                        //CheckBox _ckb;
                        //_ckb = (CheckBox)grdRow.FindControl("Admin");
                        //bool bRet = _ckb.Checked;

                        SqlConnection _Sqlconn = new SqlConnection(dbconn);



                        //delete from EmpAccess table
                        string _deleteSql = "delete FROM PhotoTracker.dbo.EmpAccess WHERE EmployeeID = @EmployeeID ;" +
                                            "delete from PhotoTracker.dbo.EmpTrackAccess WHERE EmployeeID = @EmployeeID";

                        _Sqlconn.Open();
                        _cmd = new SqlCommand(_deleteSql, _Sqlconn);
                        _cmd.Parameters.AddWithValue("@EmployeeID", id);
                        _cmd.CommandType = CommandType.Text;
                        _cmd.ExecuteNonQuery();

                        List<CheckBox> _cbkFormlist = new List<CheckBox>();
                        _cbkFormlist.Add((CheckBox)GridView1.Rows[i].FindControl("Admin"));
                        _cbkFormlist.Add((CheckBox)GridView1.Rows[i].FindControl("PT"));
                        _cbkFormlist.Add((CheckBox)GridView1.Rows[i].FindControl("Reports"));

                        List<CheckBox> _cbkWorkGrplist = new List<CheckBox>();
                        _cbkWorkGrplist.Add((CheckBox)GridView1.Rows[i].FindControl("New"));
                        _cbkWorkGrplist.Add((CheckBox)GridView1.Rows[i].FindControl("Copy"));
                        _cbkWorkGrplist.Add((CheckBox)GridView1.Rows[i].FindControl("Photo"));
                        _cbkWorkGrplist.Add((CheckBox)GridView1.Rows[i].FindControl("PHED"));
                        _cbkWorkGrplist.Add((CheckBox)GridView1.Rows[i].FindControl("APPR"));


                        //AdminAccess loop
                        foreach (CheckBox c in _cbkFormlist)
                        {
                            if (c.Checked == true)
                            {
                                switch (c.ID)
                                {
                                    case "Admin":
                                        GroupId = "1";
                                        break;
                                    case "PT":
                                        GroupId = "2";
                                        break;
                                    case "Reports":
                                        GroupId = "3";
                                        break;
                                    default:
                                        GroupId = "1";
                                        break;
                                }

                                //insert into AdminAccess
                                string _insertSql = "insert into PhotoTracker.dbo.EmpAccess (EmployeeID,GroupNo) values(@EmployeeID,@GroupNo)";
                                SqlCommand cmd = new SqlCommand(_insertSql, _Sqlconn);
                                cmd.Parameters.AddWithValue("@EmployeeID", id);
                                cmd.Parameters.AddWithValue("@GroupNo", GroupId);

                                cmd.CommandType = CommandType.Text;
                                cmd.ExecuteNonQuery();
                            }

                        }
                        //TrackEmployeeAccess loop
                        foreach (CheckBox c in _cbkWorkGrplist)
                        {
                            if (c.Checked == true)
                            {
                                switch (c.ID)
                                {
                                    case "New":
                                        TrackGroupId = "1";
                                        break;
                                    case "Copy":
                                        TrackGroupId = "2";
                                        break;
                                    case "Photo":
                                        TrackGroupId = "3";
                                        break;
                                    case "PHED":
                                        TrackGroupId = "4";
                                        break;
                                    case "APPR":
                                        TrackGroupId = "5";
                                        break;
                                    default:
                                        TrackGroupId = "1";
                                        break;
                                }

                                //insert into PhotoTracker.dbo.EmpTrackAccess
                                string _insertSql = "insert into PhotoTracker.dbo.EmpTrackAccess (EmployeeID,TrackGroupNo) values(@EmployeeID,@TrackGroupNo)";
                                SqlCommand cmd = new SqlCommand(_insertSql, _Sqlconn);
                                cmd.Parameters.AddWithValue("@EmployeeID", id);
                                cmd.Parameters.AddWithValue("@TrackGroupNo", TrackGroupId);

                                cmd.CommandType = CommandType.Text;
                                cmd.ExecuteNonQuery();

                            }

                        }

                        _Sqlconn.Close();

                        //Response.Write("Successfully updated EmpID: " + id + ";");
                    }
                }

                chkUpdate.Checked = false;
            }
        }
        //protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            //Check for the row type, which should be data row
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex == 0)
                    e.Row.Style.Add("height", "30px");


                ((CheckBox)e.Row.FindControl("chkSelect")).Attributes.Add("onclick", "checkBoxClicked('" +
    ((CheckBox)e.Row.FindControl("chkSelect")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Admin")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("PT")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Reports")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("New")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Copy")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("Photo")).ClientID + "','" + ((CheckBox)e.Row.FindControl("PHED")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("APPR")).ClientID + "'," + "'SELECT')");

                ((CheckBox)e.Row.FindControl("Admin")).Attributes.Add("onclick", "checkBoxClicked('" +
    ((CheckBox)e.Row.FindControl("chkSelect")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Admin")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("PT")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Reports")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("New")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Copy")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("Photo")).ClientID + "','" + ((CheckBox)e.Row.FindControl("PHED")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("APPR")).ClientID + "'," + "'SELECT')");

                ((CheckBox)e.Row.FindControl("PT")).Attributes.Add("onclick", "checkBoxClicked('" +
    ((CheckBox)e.Row.FindControl("chkSelect")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Admin")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("PT")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Reports")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("New")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Copy")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("Photo")).ClientID + "','" + ((CheckBox)e.Row.FindControl("PHED")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("APPR")).ClientID + "'," + "'SELECT')");

                ((CheckBox)e.Row.FindControl("Reports")).Attributes.Add("onclick", "checkBoxClicked('" +
    ((CheckBox)e.Row.FindControl("chkSelect")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Admin")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("PT")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Reports")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("New")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Copy")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("Photo")).ClientID + "','" + ((CheckBox)e.Row.FindControl("PHED")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("APPR")).ClientID + "'," + "'SELECT')");

                ((CheckBox)e.Row.FindControl("New")).Attributes.Add("onclick", "checkBoxClicked('" +
    ((CheckBox)e.Row.FindControl("chkSelect")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Admin")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("PT")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Reports")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("New")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Copy")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("Photo")).ClientID + "','" + ((CheckBox)e.Row.FindControl("PHED")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("APPR")).ClientID + "'," + "'SELECT')");

                ((CheckBox)e.Row.FindControl("Copy")).Attributes.Add("onclick", "checkBoxClicked('" +
    ((CheckBox)e.Row.FindControl("chkSelect")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Admin")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("PT")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Reports")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("New")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Copy")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("Photo")).ClientID + "','" + ((CheckBox)e.Row.FindControl("PHED")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("APPR")).ClientID + "'," + "'SELECT')");


                ((CheckBox)e.Row.FindControl("Photo")).Attributes.Add("onclick", "checkBoxClicked('" +
    ((CheckBox)e.Row.FindControl("chkSelect")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Admin")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("PT")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Reports")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("New")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Copy")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("Photo")).ClientID + "','" + ((CheckBox)e.Row.FindControl("PHED")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("APPR")).ClientID + "'," + "'SELECT')");


                ((CheckBox)e.Row.FindControl("PHED")).Attributes.Add("onclick", "checkBoxClicked('" +
    ((CheckBox)e.Row.FindControl("chkSelect")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Admin")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("PT")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Reports")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("New")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Copy")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("Photo")).ClientID + "','" + ((CheckBox)e.Row.FindControl("PHED")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("APPR")).ClientID + "'," + "'SELECT')");

                ((CheckBox)e.Row.FindControl("APPR")).Attributes.Add("onclick", "checkBoxClicked('" +
    ((CheckBox)e.Row.FindControl("chkSelect")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Admin")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("PT")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Reports")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("New")).ClientID + "','" + ((CheckBox)e.Row.FindControl("Copy")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("Photo")).ClientID + "','" + ((CheckBox)e.Row.FindControl("PHED")).ClientID
    + "','" + ((CheckBox)e.Row.FindControl("APPR")).ClientID + "'," + "'SELECT')");

            }
    }

    }
}
