<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminMain.aspx.cs" Inherits="PhotoTracker.AdminMain"  Debug="true"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<HEAD>
		<title>AdminMain</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
 
	</HEAD>
<body>
    <form id="form1" runat="server">
    <div id="header"  align="center" style="width: 1003px; height: 638px"><uc1:header id="Header1" runat="server" ></uc1:header>
        &nbsp;
        <strong><span style="font-size: 12pt">&nbsp; &nbsp;
    User Details:<br /><br />
            &nbsp;<br /><br />
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="font-size: 10pt">ADMINISTRATION TOOL</span> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="font-size: 10pt">FORM PERMISSIONS</span> &nbsp; &nbsp; &nbsp;
           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="font-size: 10pt">WORK GROUP</span>
        
        </span></strong>
        
        <div style=" OVERFLOW: auto; WIDTH: 910px; HEIGHT: 300px; position: relative; left: 0px; top: 0px;" >
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns ="false" width="899px" Height="338px" style="text-align: center;"  HeaderStyle-BackColor = "red"
         Font-Names = "Arial"  Font-Size = "8pt"   onrowupdating = "UpdateEmployee" EnableViewState=true>
        
             <Columns>
             
                
               <asp:TemplateField >
                    <ItemTemplate>
                        <asp:LinkButton ID="btnUpdate" runat="server" CommandName="Update" Text="Update"    >
                        </asp:LinkButton>
                    </ItemTemplate>
               </asp:TemplateField>   
               <asp:TemplateField >
                    <ItemTemplate>
              
                    </ItemTemplate>
               </asp:TemplateField> 
                
                
                <asp:BoundField DataField="EmployeeID" HeaderText="EmployeeID"  visible=true ReadOnly="True"   ItemStyle-Width=10px/>
                <asp:BoundField DataField="UserName" HeaderText="USERNAME"  ReadOnly="True" />
                <asp:BoundField DataField="LongName" HeaderText="LONGNAME" ReadOnly ="True" />
                  <asp:BoundField DataField="Password" HeaderText="PASSWORD" ReadOnly="True" />
                              
               
                <asp:TemplateField HeaderText="ADMIN">
                     <ItemTemplate>
                        <asp:CheckBox ID="Admin" runat="server"/>
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PT">
                     <ItemTemplate>
                        <asp:CheckBox ID="PT" runat="server"/>
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                <asp:TemplateField HeaderText="REPORTS">
                     <ItemTemplate>
                        <asp:CheckBox ID="Reports" runat="server" />
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                
                
                <asp:TemplateField HeaderText="NEW">
                     <ItemTemplate>
                        <asp:CheckBox ID="New" runat="server" />
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                <asp:TemplateField HeaderText="COPY">
                     <ItemTemplate>
                        <asp:CheckBox ID="Copy" runat="server" />
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PHOTO">
                     <ItemTemplate>
                        <asp:CheckBox ID="Photo" runat="server" />
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PH/ED">
                     <ItemTemplate>
                        <asp:CheckBox ID="PHED" runat="server" />
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                <asp:TemplateField HeaderText="APPR">
                     <ItemTemplate>
                        <asp:CheckBox ID="APPR" runat="server" />
                     </ItemTemplate>
                     <ItemStyle Font-Size="X-Small" />                               
                </asp:TemplateField>
                                
             </Columns>   
            <HeaderStyle ForeColor="Black" />
         </asp:GridView> 
         </div>
    <br />
    <br />
   
    <asp:Button ID="btnAdd" runat="server" Style="z-index: 101; left: 201px; 
            top: 623px" Text="AddUser" Font-Bold="True" Font-Size="10pt" OnClick="btnAdd_Click" Width="155px" />
    <br />
    <br />
                
    <asp:Button ID="btnPwdChg" runat="server" Style="z-index: 101; left: 201px; 
            top: 623px" Text="ChangeUserPassword" Font-Bold="True" Font-Size="10pt"  Width="155px" OnClick="btnPwdChg_Click" UseSubmitBehavior="False" />
    </div>
    </form>
</body>
</html>
