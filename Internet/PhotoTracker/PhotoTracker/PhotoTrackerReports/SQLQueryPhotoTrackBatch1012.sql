USE [PhotoTracker]
GO
/****** Object:  StoredProcedure [dbo].[Create_PhotoTrackerBatchPaperwork_Reports]    Script Date: 10/12/2010 10:34:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Regina Taylor>
-- Create date: <September 22, 2010>
-- Description:	<Create PhotoTracker Batch Reports>
-- =============================================
ALTER PROCEDURE [dbo].[Create_PhotoTrackerBatchPaperwork_Reports] 
	-- Add the parameters for the stored procedure here

--@ItemList NVARCHAR(4000),
--@delimiter CHAR(1)

	@BRAND_TYPE	nvarchar(1), 
	@ITEM_LIST	nvarchar(4000) 


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DECLARE @ITEM_NUMBER INT
	SET @ITEM_NUMBER = '121771'

	DECLARE @ITEM_NUMBER2 INT
	SET @ITEM_NUMBER2 = '245647'

	DECLARE @ITEM_NUMBER3 INT
	SET @ITEM_NUMBER3 = '345678'

	DECLARE @delimiter NVARCHAR(1)
	SET @delimiter = ','
	
	DECLARE @NUMBER_ITEMS INT

	
	-- get item list

DECLARE @tempItemList NVARCHAR(4000)
--SET @tempItemList = ' ' + @ITEM_NUMBER	+ @delimiter 
--						+ @ITEM_NUMBER2 + @delimiter + @ITEM_NUMBER3

--SET @tempItemList = '121771,245647,345678' 
SET @tempItemList = '121771,245647,345678' 

SELECT @tempItemList

DECLARE @i INT
DECLARE @j INT
DECLARE @listlen INT
DECLARE @Item NVARCHAR(40)


SET @i = CHARINDEX(@delimiter, @tempItemList)
set @j = 1
--SELECT @i
--SELECT @j
set @listlen = LEN(@tempItemList)
--select @listlen
SET @NUMBER_ITEMS = 0

WHILE @listlen > 0 

	BEGIN
	IF @i = 0 
		SET @ITEM_NUMBER = @tempItemList
	ELSE
		BEGIN
			SET @ITEM_NUMBER = substring(@tempItemList,@j, @i - 1)
			SET @listlen = @listlen - @i
--			SELECT @ITEM_NUMBER  
--			SELECT @listlen
			SET @j= @j+@i
--			SELECT @j
--			SELECT @i
		END
	SET @NUMBER_ITEMS = @NUMBER_ITEMS + 1
--	SELECT @NUMBER_ITEMS 

	END

DECLARE @loopcnt INT
SET @loopcnt = @NUMBER_ITEMS
WHILE @loopcnt > 0

BEGIN

DECLARE @AreaReceivedValue	NVARCHAR(1) 
DECLARE @AreaCopyValue		NVARCHAR(1) 
DECLARE @AreaPhotoValue		NVARCHAR(1) 
DECLARE @AreaPhotoEditValue	NVARCHAR(1) 
DECLARE @AreaApproverValue	NVARCHAR(1) 

DECLARE @EntryTypeIn		NVARCHAR (6)
DECLARE @EntryTypeOut 		NVARCHAR (6)
 
SET @AreaReceivedValue	= '1'
SET @AreaCopyValue		= '2'  
SET @AreaPhotoValue		= '3'
SET @AreaPhotoEditValue	= '4'
SET @AreaApproverValue	= '5'

SET @EntryTypeIn		= '1'
SET @EntryTypeOut		= '2'

CREATE  TABLE #MyTempEmpEntryTable  (
EmpID INT 
,EmpName NVARCHAR(50)
,EmpDiv NVARCHAR(1)
 )

INSERT INTO #MyTempEmpEntryTable (
EmpID  
,EmpName
,EmpDiv  
)

select distinct Emp.EmployeeID
,Emp.EmpFirstName + ''+ Emp.EmpLastName AS EmpName 
,Emp.Division
from Employee AS Emp INNER JOIN
PhotoTrackEntry  on PhotoTrackEntry.EmployeeID = Emp.EmployeeID  
and  Emp.Division = @BRAND_TYPE 

SET @tempItemList = '121771,107348,120755'  


if @loopcnt = 1
SET @ITEM_NUMBER = '121771' 
select @ITEM_NUMBER

if @loopcnt = 2
SET @ITEM_NUMBER = '107348' 
if @loopcnt = 3
SET @ITEM_NUMBER = '120755' 

SELECT     CONVERT(CHAR(11), GETDATE(), 101) AS CurrentDate 
,a1.EmpName 
,a.Description	 
,a.Division	 
,a.Department	
,a.PONumber	 
,a.Class	 
,a.SubClass	 
,a.PrivLabel	 
,a.CurrentRetailPrice	 
,a.CancelDate	 
,a.Theme1	 
,a.Theme2	 
,a.Theme3	 
,a.Theme4	 
,a.Theme5	 
,b.Comments  

--,c.PhotoTakenInDate  
--,d.PhotoTakenOutDate  
--,e.PhotoEditedInDate  
--,f.PhotoEditedOutDate
--,g.ApprovedInDate  
--,h.ApprovedOutDate 

,j.ItemNum 
,j.CopyInTrackDate2  
,j.CopyOutTrackDate2  
,j.PhotoInTrackDate3  
,j.PhotoOutTrackDate3  
,j.PhotoEdInTrackDate4  
,j.PhotoEdOutTrackDate4
,j.ApprovedInTrackDate5 
,j.ApprovedOutTrackDate5  

,i.ItemBarCode  


FROM  
(select PhotoTracking.Description,Division,Department,PONumber,Class	 
,SubClass,PrivLabel,CurrentRetailPrice,CancelDate,Theme1,Theme2,Theme3	 
,Theme4	,Theme5	 
 from PhotoTracking
where ItemNum = @ITEM_NUMBER --'121771'
and Division = @BRAND_TYPE --'1'
	 	  ) a, 

(select  EmpName
from 
#MyTempEmpEntryTable
		 	  )   a1, 

(select Comments from Comments
where ItemNum = @ITEM_NUMBER --'121771'
and EmployeeID = EmployeeID --'11999'
		 	  )   b, 

--(select TrackDate as PhotoTakenInDate  from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and EmpTrackAccStatus = @AreaReceivedValue --3
--and EntryType= @EntryTypeIn
--			) c,
--
--(select TrackDate as PhotoTakenOutDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and emptrackaccstatus = @AreaReceivedValue  --3
--and entrytype= @EntryTypeOut
--			) d,
--
--(select TrackDate as PhotoEditedInDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and emptrackaccstatus = @AreaReceivedValue --4
--and entrytype= @EntryTypeIn
--			) e,
--
--(select TrackDate  as PhotoEditedOutDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and emptrackaccstatus = @AreaReceivedValue --4
--and entrytype= @EntryTypeOut
--			) f,
--
--(select TrackDate  as ApprovedInDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and EmpTrackAccStatus = @AreaReceivedValue --5
--and Entrytype= @EntryTypeIn
--			) g,
--
--(select TrackDate as ApprovedOutDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and EmpTrackAccStatus = @AreaReceivedValue --5
--and EntryType= @EntryTypeOut
--			) h,

(SELECT DISTINCT @ITEM_NUMBER AS ItemNum, dbo.CopyInTrackDate(@ITEM_NUMBER, EmployeeID) as CopyInTrackDate2, dbo.CopyOutTrackDate(@ITEM_NUMBER, EmployeeID) as CopyOutTrackDate2
,dbo.PhotoInTrackDate(@ITEM_NUMBER, EmployeeID) as PhotoInTrackDate3, dbo.PhotoOutTrackDate(@ITEM_NUMBER, EmployeeID) as PhotoOutTrackDate3
,dbo.PhotoEdInTrackDate(@ITEM_NUMBER, EmployeeID) as PhotoEdInTrackDate4, dbo.PhotoEdOutTrackDate(@ITEM_NUMBER, EmployeeID) as PhotoEdOutTrackDate4
,dbo.ApprovedInTrackDate(@ITEM_NUMBER, EmployeeID) as ApprovedInTrackDate5, dbo.ApprovedOutTrackDate(@ITEM_NUMBER, EmployeeID) as ApprovedOutTrackDate5
FROM PhotoTrackEntry
where ItemNum = @ITEM_NUMBER) j,


(select  BarCodeImage as ItemBarCode from dbo.BarCodeImage 
where ItemNum = @ITEM_NUMBER --'123050'
			) i

--(select  itemBarCode  as ItemBarCode from dbo.ItemEntry
--where ItemNum = '127751'
--			) i

SET @loopcnt = @loopcnt - 1

DROP TABLE #MyTempEmpEntryTable   


END


END
