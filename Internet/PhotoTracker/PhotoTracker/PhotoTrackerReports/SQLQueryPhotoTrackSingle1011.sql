USE [PhotoTracker]
GO
/****** Object:  StoredProcedure [dbo].[Create_PhotoTrackerSinglePaperwork_Report]    Script Date: 10/11/2010 16:36:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Create_PhotoTrackerSinglePaperwork_Report]
	-- Add the parameters for the stored procedure here

	@BRAND_TYPE	NVARCHAR(1), 
	@ITEM_NUMBER NVARCHAR(12) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here




DECLARE @AreaReceivedValue	NVARCHAR(1) 
DECLARE @AreaCopyValue		NVARCHAR(1) 
DECLARE @AreaPhotoValue		NVARCHAR(1) 
DECLARE @AreaPhotoEditValue	NVARCHAR(1) 
DECLARE @AreaApproverValue	NVARCHAR(1) 

DECLARE @EntryTypeIn		NVARCHAR (6)
DECLARE @EntryTypeOut 		NVARCHAR (6)
 
SET @AreaReceivedValue	= '1'
SET @AreaCopyValue		= '2'  
SET @AreaPhotoValue		= '3'
SET @AreaPhotoEditValue	= '4'
SET @AreaApproverValue	= '5'

SET @EntryTypeIn		= '1'
SET @EntryTypeOut		= '2'


--CREATE FUNCTION dbo.Date1()
--RETURNS DATETIME
--AS BEGIN
--select TrackDate as PhotoTakenInDate  from PhotoTrackEntry
--where ItemNum = '121771' --@ITEM_NUMBER --'121771'
--and EmployeeID = '7823' --EmployeeID --'11999'
--and EmpTrackAccStatus = '4' --@AreaReceivedValue --3
--and EntryType= '1' --@EntryTypeIn
--RETURN
--END


CREATE  TABLE #MyTempEmpEntryTable  (
EmpID INT 
,EmpName NVARCHAR(50)
,EmpDiv NVARCHAR(1)
 )

CREATE  TABLE #MyTempDateEntryTable  (
 RecInDate		NVARCHAR(30)
,RecOutDate		NVARCHAR(30)
,CpyInDate		NVARCHAR(30)
,CpyOutDate		NVARCHAR(30)
,PhtInDate		NVARCHAR(30)
,PhtOutDate		NVARCHAR(30)
,PhtEdInDate	NVARCHAR(30)
,PhtEdOutDate	NVARCHAR(30)
,AprInDate		NVARCHAR(30)
,AprOutDate		NVARCHAR(30)
 )


INSERT INTO #MyTempEmpEntryTable (
EmpID  
,EmpName
,EmpDiv  
)

select distinct Emp.EmployeeID
,Emp.EmpFirstName + ''+ Emp.EmpLastName AS EmpName 
,Emp.Division
from Employee AS Emp INNER JOIN
PhotoTrackEntry  on PhotoTrackEntry.EmployeeID = Emp.EmployeeID  
and  Emp.Division = @BRAND_TYPE 

-- DELETE  FROM dbo.ItemEntry 
-- WHERE ItemNum = '121771'

--INSERT  dbo.ItemEntry(ItemNum, TopSample, ItemType, ItemBarCode)
-- values(121771, 'TOP', Null, 'c:\temp\J0099188.JPG')

-- THIS WORKS
--INSERT INTO dbo.ItemEntry(ItemNum, TopSample, ItemType, ItemBarCode)
--SELECT 127751, 'TOP', '1', *
--from Openrowset( Bulk 'c:\PhotoTrack\BarCode\Current\127837.gif', Single_Blob) RS



--
-- DELETE  FROM dbo.ItemEntry 
-- WHERE ItemNum = '245647'

-- DELETE  FROM dbo.ItemEntry 
-- WHERE ItemNum = '121771'

 
--INSERT INTO dbo.ItemEntry(ItemNum, TopSample, ItemType, BarCode)
-- values(121771, 'TOP', Null, 'c:\temp\J0099188.JPG')
--
--INSERT INTO dbo.ItemEntry(ItemNum, TopSample, ItemType, BarCode)
-- values(245647, 'SAMPLE', Null, 'c:\temp\J0099189.JPG')


--select  EmpName
--from 
--#MyTempEmpEntryTable

SELECT     CONVERT(CHAR(11), GETDATE(), 101) AS CurrentDate 
,a1.EmpName 
,a.Description	 
,a.Division	 
,a.Department	
,a.PONumber	 
,a.Class	 
,a.SubClass	 
,a.PrivLabel	 
,a.CurrentRetailPrice	 
,a.CancelDate	 
,a.Theme1	 
,a.Theme2	 
,a.Theme3	 
,a.Theme4	 
,a.Theme5	 
,b.Comments  
--,c.PhotoTakenInDate  
--,d.PhotoTakenOutDate  
--,e.PhotoEditedInDate  
--,f.PhotoEditedOutDate
--,g.ApprovedInDate  
--,h.ApprovedOutDate 
,j.ItemNum 
,j.CopyInTrackDate2  
,j.CopyOutTrackDate2  
,j.PhotoInTrackDate3  
,j.PhotoOutTrackDate3  
,j.PhotoEdInTrackDate4  
,j.PhotoEdOutTrackDate4
,j.ApprovedInTrackDate5 
,j.ApprovedOutTrackDate5  

,i.ItemBarCode  


FROM  
(select PhotoTracking.Description,Division,Department,PONumber,Class	 
,SubClass,PrivLabel,CurrentRetailPrice,CancelDate,Theme1,Theme2,Theme3	 
,Theme4	,Theme5	 
 from PhotoTracking
where ItemNum = @ITEM_NUMBER --'121771'
and Division = @BRAND_TYPE --'1'
	 	  ) a, 

(select  EmpName
from 
#MyTempEmpEntryTable
		 	  )   a1, 

(select Comments from Comments
where ItemNum = @ITEM_NUMBER --'121771'
and EmployeeID = EmployeeID --'11999'
		 	  )   b  , 

--(select TrackDate as CopyInDate  from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and EmpTrackAccStatus = @AreaCopyValue --2
--and EntryType= @EntryTypeIn
--			) c,
--
--(select TrackDate as CopyOutDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and emptrackaccstatus = @AreaCopyValue  --2
--and entrytype= @EntryTypeOut
--			) d,


--(select TrackDate as PhotoTakenInDate  from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and EmpTrackAccStatus = @AreaPhotoValue --3
--and EntryType= @EntryTypeIn
--			) c,
--
--(select TrackDate as PhotoTakenOutDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and emptrackaccstatus = @AreaPhotoValue  --3
--and entrytype= @EntryTypeOut
--			) d,
--
--(select TrackDate as PhotoEditedInDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and emptrackaccstatus = @AreaPhotoEditValue --4
--and entrytype= @EntryTypeIn
--			) e,
--
--(select TrackDate  as PhotoEditedOutDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and emptrackaccstatus = @AreaPhotoEditValue --4
--and entrytype= @EntryTypeOut
--			) f,
--
--(select TrackDate  as ApprovedInDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and EmpTrackAccStatus = @AreaApproverValue --5
--and Entrytype= @EntryTypeIn
--			) g,
--
--(select TrackDate as ApprovedOutDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and EmpTrackAccStatus = @AreaApproverValue --5
--and EntryType= @EntryTypeOut
--			) h,

-- INSERT INTO dbo.ItemEntry(ItemNum, TopSample, ItemType, BarCode)
-- values(121771, 'TOP', Null, 'c:\temp\J0099188.JPG')

(SELECT DISTINCT @ITEM_NUMBER AS ItemNum, dbo.CopyInTrackDate(@ITEM_NUMBER, EmployeeID) as CopyInTrackDate2, dbo.CopyOutTrackDate(@ITEM_NUMBER, EmployeeID) as CopyOutTrackDate2
,dbo.PhotoInTrackDate(@ITEM_NUMBER, EmployeeID) as PhotoInTrackDate3, dbo.PhotoOutTrackDate(@ITEM_NUMBER, EmployeeID) as PhotoOutTrackDate3
,dbo.PhotoEdInTrackDate(@ITEM_NUMBER, EmployeeID) as PhotoEdInTrackDate4, dbo.PhotoEdOutTrackDate(@ITEM_NUMBER, EmployeeID) as PhotoEdOutTrackDate4
,dbo.ApprovedInTrackDate(@ITEM_NUMBER, EmployeeID) as ApprovedInTrackDate5, dbo.ApprovedOutTrackDate(@ITEM_NUMBER, EmployeeID) as ApprovedOutTrackDate5
FROM PhotoTrackEntry
where ItemNum = @ITEM_NUMBER) j,



(select  BarCodeImage as ItemBarCode from dbo.BarCodeImage 
where ItemNum = @ITEM_NUMBER
			) i

--select TrackDate as CopyInDate  from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and EmpTrackAccStatus = @AreaCopyValue --2
--and EntryType= @EntryTypeIn
--	
--
--select TrackDate as CopyOutDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and emptrackaccstatus = @AreaCopyValue  --2
--and entrytype= @EntryTypeOut
--			 
--
--select TrackDate as PhotoTakenInDate  from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and EmpTrackAccStatus = @AreaPhotoValue --3
--and EntryType= @EntryTypeIn
--	
--
--select TrackDate as PhotoTakenOutDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and emptrackaccstatus = @AreaPhotoValue  --3
--and entrytype= @EntryTypeOut
--			 
--
--select TrackDate as PhotoEditedInDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and emptrackaccstatus = @AreaPhotoEditValue --4
--and entrytype= @EntryTypeIn
--		 
--
--select TrackDate  as PhotoEditedOutDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and emptrackaccstatus = @AreaPhotoEditValue --4
--and entrytype= @EntryTypeOut
--			 
--
--select TrackDate  as ApprovedInDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and EmpTrackAccStatus = @AreaApproverValue --5
--and Entrytype= @EntryTypeIn
--			 
--
--select TrackDate as ApprovedOutDate from PhotoTrackEntry
--where ItemNum = @ITEM_NUMBER --'121771'
--and EmployeeID = EmployeeID --'11999'
--and EmpTrackAccStatus = @AreaApproverValue --5
--and EntryType= @EntryTypeOut


			 

END
