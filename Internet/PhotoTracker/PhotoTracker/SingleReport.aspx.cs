using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PhotoTracker
{
    public partial class SingleReport : System.Web.UI.Page
    {
        DataTable dtUser;
        DataTable dtPermit;
        //int intDivision = 0;
        string strDivision = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                dtUser = (DataTable)Session["UserProfile"];

                strDivision = dtUser.Rows[0]["Division"].ToString().Trim();
                dtPermit = (DataTable)Session["ViewPermission"];
                

                bool bHasPermt = false;

                bHasPermt = AppUser.CheckSecurity("3", dtPermit);

                if (bHasPermt == true)
                {
                    ViewState["StrDivision"] = strDivision;
                    txtItemNum.Enabled = true;
                }
                else
                {
                    
                    //no proper permission go back to Login
                    Response.Redirect("./Login.aspx");

                }

            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            string strItemNum = this.txtItemNum.Text.Trim();

            if (strItemNum != "")
            {
                ViewState["StrItemNum"] = strItemNum;

                LoadItem(strItemNum);

            }   
            
        }
        public void LoadItem(string strItemNum)
        {
            int intDiv = 0;
            intDiv = int.Parse(ViewState["StrDivision"].ToString().Trim());

            int intPrintAllowTime = int.Parse(ConfigurationManager.AppSettings["ItemMutiPrintAllow"].ToString());

            if (intPrintAllowTime == 0)
            {
                dtUser = (DataTable)Session["UserProfile"];
                if (dtUser != null)
                {
                    int EmployeeID = int.Parse(dtUser.Rows[0]["EmployeeID"].ToString().Trim());
     
                    if (intDiv > 0)
                    {
                        Data.RptUpdateItmPrntFlag(strItemNum, EmployeeID);

                        string jScriptValidator;
                        jScriptValidator = "<script>" +
                        "window.open( ' http://CAPHOTRKPRD/ReportServer/Pages/ReportViewer.aspx?%2fPhoto+Tracker%2fPhotoTrackPaperWork&rs:Command=Render&rc:Parameters=true"
                        + "&P_ItemList="
                        + strItemNum 
                        + "', '', 'toolbar=0');";
                        jScriptValidator += " </script>";
                        Page.RegisterStartupScript("regJSval1", jScriptValidator);

                                       
                    }
                    else
                    {
                        lblItmMsg.Text = "Could not get company name for the current user.";
                    }
                }
                else
                {
                    lblItmMsg.Text = "Can not find Employee ID.";
                }
            }
            else
            {
                //allow the item multiple print
            }
        }

        protected void txtItemNum_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
