<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="admin.aspx.cs" Inherits="PhotoTracker.admin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<HEAD>
		<title>AdminSecurity</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
 
	</HEAD>
<body>
    <form id="form1" runat="server">
    <div id="header"  align="center" style="width: 1003px; height: 263px"><uc1:header id="Header1" runat="server" ></uc1:header>
        <table border="0"  align= "right" cellpadding="0" cellspacing="0" style="z-index: 100; left: 29px;
            width: 99%; top: 156px; height: 253%; border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
            <tr>
                <td style="height: 25px; width: 1407px; text-align: center;" align="center">
                    
                 
                    <asp:RequiredFieldValidator ID="RFVLastName" runat="server" ControlToValidate="Lastname"
                        ErrorMessage="LastName cannot be blank" Style="z-index: 102; left: 907px; position: absolute;
                        top: 322px" ValidationGroup="CreateUserAccount" Width="7px">*</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="RFVFirstName" runat="server" ControlToValidate="Firstname"
                        ErrorMessage="FirstName cannot be blank" Style="z-index: 103; left: 907px; position: absolute;
                        top: 290px" ValidationGroup="CreateUserAccount" Width="7px">*</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="RFVPassword" runat="server" ControlToValidate="Password"
                        ErrorMessage="Password cannot be blank." Style="z-index: 104; left: 907px; position: absolute;
                        top: 355px" ValidationGroup="CreateUserAccount" Width="7px">*</asp:RequiredFieldValidator>
                   
                </td>
            </tr>
           
            <tr align="center">                
                <td style="height: 200px; width: 1407px; text-align: center;">
                    <table style="z-index: 100; left: 427px; width: 476px; position: absolute; top: 218px; 
                        height: 140px; border-right: #000000 thin solid; border-top: #000000 thin solid; border-left: #000000 thin solid; border-bottom: #000000 thin solid; text-align: center;">
                       
                        <tr>
                            <td style="border-bottom: #000000 thin solid; height: 29px;" colspan="2">
                                <strong><span style="font-size: 14pt">Create a New User Account</span></strong></td>
                        </tr>
                      <tr>
                            <td style="width: 161px; height: 27px; text-align: right; font-weight: bold;" align="right">
                                Enter User's EmployeeID: &nbsp;
                            </td>
                            <td style="width: 101px; height: 28px; text-align: left;">
                                <asp:TextBox ID="EmpID"  MaxLength="20" runat="server"></asp:TextBox>
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 161px; height: 27px; text-align: right; font-weight: bold;" align="right">
                                Enter User's First Name: &nbsp;
                            </td>
                            <td style="width: 101px; height: 28px; text-align: left;">
                                <asp:TextBox ID="Firstname"  MaxLength="20" runat="server"></asp:TextBox>
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 161px; height: 27px; text-align: right; font-weight: bold;" align="right">
                                Enter User's Last Name: &nbsp;
                            </td>
                            <td style="width: 101px; height: 28px; text-align: left;">
                                <asp:TextBox ID="Lastname"  MaxLength= "20" runat="server"></asp:TextBox>
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 161px; height: 29px; text-align: right; font-weight: bold;" align="right">
                                Enter Password: &nbsp;
                            </td>
                            <td style="width: 101px; height: 28px; text-align: left;">
                                <asp:TextBox ID="Password"  TextMode="Password" runat="server" Width="150px"></asp:TextBox>
                               </td>
                        </tr>
                        
                    </table>                  
                    <table style="z-index: 101; left: 681px; position: absolute; top: 399px; width: 259px; border-right: #000000 thin solid; border-top: #000000 thin solid; border-left: #000000 thin solid; border-bottom: #000000 thin solid; height: 214px;">
                        <tr>
                            <td colspan="2" style="height: 33px; border-bottom: #000000 thin solid;">
                                <strong>
                                Work Group</strong></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 132px">
                            </td>
                        </tr>
                       
                        <tr>
                            <td style="width: 100px" align="right">
                                New</td>
                            <td style="width: 132px" align="left">
                                <asp:CheckBox ID="NewWrkGrp" runat="server" />
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" align="right">
                                Copy</td>
                            <td style="width: 132px" align="left">
                                <asp:CheckBox ID="CopyWrkGrp" runat="server" />
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" align="right">
                                Photo</td>
                            <td style="width: 132px" align="left">
                                <asp:CheckBox ID="PhotoWrkGrp" runat="server" />
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" align="right">
                                PH/ED</td>
                            <td style="width: 132px" align="left">
                                <asp:CheckBox ID="PHEDWrkGrp" runat="server" />
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" align="right">
                                APPR</td>
                            <td style="width: 132px" align="left">
                                <asp:CheckBox ID="APPRWrkGrp" runat="server" />
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 132px">
                            </td>
                        </tr>
                    </table>
                    <table style="z-index: 104; left: 397px; position: absolute; top: 401px; width: 259px; border-right: #000000 thin solid; border-top: #000000 thin solid; border-left: #000000 thin solid; border-bottom: #000000 thin solid; height: 172px;">
                        <tr>
                            <td colspan="2" style="height: 33px; border-bottom: #000000 thin solid;">
                                <strong>
                                Application Permission</strong></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 132px">
                            </td>
                        </tr>
                       
                        <tr>
                            <td style="width: 100px" align="right">Admin</td>
                            <td style="width: 132px" align="left">
                                <asp:CheckBox ID="AdminAccess"  runat="server" OnCheckedChanged="AdminAccess_CheckedChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" align="right">
                                PT&nbsp;</td>
                            <td style="width: 132px" align="left">
                                <asp:CheckBox ID="PTAccess" runat="server" OnCheckedChanged="PTAccess_CheckedChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" align="right">
                                Reports</td>
                            <td style="width: 132px" align="left">
                                <asp:CheckBox ID="RptAccess" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" align="right">
                            </td>
                            <td style="width: 132px" align="left">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 100px" align="right">
                            </td>
                            <td style="width: 132px" align="left">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 100px; height: 15px;">
                            </td>
                            <td style="width: 132px; height: 15px;">
                            </td>
                        </tr>
                    </table>
                      <asp:Button ID="btnUserAcct" runat="server" Font-Bold="True" Height="27px" 
                        Style="z-index: 100; left: 492px; position: absolute; top: 620px" Text="Create a User Account"
                        Width="297px" OnClick="btnUserAcct_Click" ValidationGroup="CreateUserAccount" />                         
                         <asp:Label ID="lblCreateAccountResults"  runat="server" Height="23px" Style="z-index: 101; left: 311px;
                        position: absolute; top: 656px" Width="657px" Font-Size="14pt"></asp:Label>
                         <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Error creating account for given user"
                        Style="z-index: 105; left: 517px; position: absolute; top: 690px" ValidationGroup="CreateUserAccount"
                        Width="229px" /> 
                    
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
