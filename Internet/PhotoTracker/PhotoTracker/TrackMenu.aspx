<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrackMenu.aspx.cs" Inherits="PhotoTracker.TrackMenu" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >
<HTML>
	<HEAD>
		<title>TrackMenu</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
	    <script language="javascript" type="text/javascript">

            function SetVisibility(thePanel1, thePanel2, lnk)
			{
				var panel1 = document.getElementById(thePanel1);
				var panel2 = document.getElementById(thePanel2);
				var lnkMore = document.getElementById(lnk);
				
				panel1.style.display = 'none';
				panel2.style.display = 'block';
				lnkMore.style.display='none';
			}
	    </script>	
	</HEAD>
	<body bottomMargin="0" bgColor="#000000" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header>
			<div id="main">
			<asp:panel runat=server id=pngPhotoTrk visible=False>
				<TABLE class="innerTable" id="tblOption" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0" runat="server">
					<tr class="tableTitle">
						<td colSpan="2">&nbsp; Photo Tracking</td>
					</tr>
					<tr>
						<td align = "center"><A href="./PhotoTracking.aspx"><IMG src="Image/PhotoTrackHT.jpg" vspace="5" border="0"></A></td>
					</tr>
					
					
				</TABLE>
				<br></asp:panel>
				<asp:panel runat=server id=pngReport visible=False>
				<TABLE class="innerTable" id="tblRange" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0" runat="server">
					<tr class="tableTitle">
						<td colSpan="2">&nbsp; Reports</td>
					</tr>
					<tr>
						<td align = "center"><A href="./ReportMenu.aspx"><IMG src="Image/ReportHT.jpg" vspace="5" border="0"></A></td>
 					</tr>	
 					
					<tr>
						<td align = "center"><A href="./ReviewItmStatus.aspx"><IMG src="Image/ReviewHT.jpg" vspace="5" border="0"></A></td>
 					</tr>	
 					
				</TABLE>
				<br></asp:panel>
				<asp:panel runat=server id=pngAdmin visible=False>
				<TABLE class="innerTable" id="tblAdminTool" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0" runat="server">
					<tr class="tableTitle">
						<td colSpan="2">&nbsp; Administration</td>
					</tr>
					
					<tr>
						<td align = "center"><A href="./AdminMainNew.aspx"><IMG src="Image/AdminHT.jpg" vspace="5" border="0"></A></td>
 					</tr>
 					
				</TABLE><br /></asp:panel>
				<asp:panel runat=server id=pngAdminNo visible=False>
				<TABLE class="innerTable" id="tblAdminNo" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0" runat="server">
					<tr class="tableTitle">
						<td colSpan="2">&nbsp; Security</td>
					</tr>
					<tr>
						<td align = "center"><A href="./ChangePWD.aspx"><IMG src="Image/ChangePwdHT.jpg" vspace="5" border="0"></A></td>
					</tr>
					
					
				</TABLE></asp:panel>
                <br>
			</div>
			</div>
		</form>
	</body>
</HTML>
