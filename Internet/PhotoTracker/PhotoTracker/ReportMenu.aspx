<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportMenu.aspx.cs" Inherits="PhotoTracker.ReportMenu" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ReportMenu</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
		<script language="javascript" type="text/javascript">

            function ItemRpt()
            {
              window.open('http://CAPHOTRKPRD/ReportServer/Pages/ReportViewer.aspx?%2fPhoto+Tracker%2fPhotoTrackerItemStatusRpt&rs:Command=Render', 'Comments', 'width=760,height=600,resizable=yes');
            }
            
             function IndivRpt()
            {
              window.open('http://CAPHOTRKPRD/ReportServer/Pages/ReportViewer.aspx?%2fPhoto+Tracker%2fPhotoTrackerIndividualProductivityRpt&rs:Command=Render', 'Comments', 'width=760,height=600,resizable=yes');
            }
		</script>
</head>
<body bottomMargin="0" bgColor="#000000" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header>
			<div id="main">

				<TABLE id="tblRange" class="innerTable" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0" runat="server">
					
					<tr class="tableTitle" align="center">
						<td colSpan="2">&nbsp; Reporting Menu</td>
					</tr>
				
				</TABLE>

			   	<TABLE class="innerTable" id="tblOption" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0" runat="server">
					<tr class="tableTitle">
						<td colSpan="2">&nbsp; Batch Print</td>
					</tr>
					<tr>
						<td align = "center"><A href="./BatchReport.aspx"><IMG src="Image/BatchTrack.jpg" vspace="5" border="0"></A></td>
 					</tr>	
				
						
				</TABLE>
				<br>
					<TABLE class="innerTable" id="TABLE1" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0" runat="server">
					<tr class="tableTitle">
						<td colSpan="2">&nbsp; Single Print</td>
					</tr>
					<tr>
						<td align = "center"><A href="./SingleReport.aspx"><IMG src="Image/SingleTrack.jpg" vspace="5" border="0"></A></td>
 					</tr>
					
						
				</TABLE>
				<br>
				<TABLE class="innerTable" id="TABLE2" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0" runat="server">
					<tr class="tableTitle">
						<td colSpan="2">&nbsp; Individual Report</td>
					</tr>
					
					<tr>
						<td align = "center"><a href="javascript:IndivRpt()"><IMG src="Image/IndivRept.jpg" border="0"></a></td>
 					</tr>
						
				</TABLE>
				<br>
				<TABLE class="innerTable" id="TABLE3" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0" runat="server">
					<tr class="tableTitle">
						<td colSpan="2">&nbsp; Item Report</td>
					</tr>
					<tr>
						<td align = "center"><a href="javascript:ItemRpt()"><IMG src="Image/ItemStatus.jpg" border="0"></a></td>
 					</tr>
						
				</TABLE>
				<br />

			   	<br />
			</div>
			</div>
		</form>
	</body>
</HTML>
