using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Security;
using System.Diagnostics;
using System.Configuration;

namespace FreightForwarder
{
    class DataAccess
    {
        public static void PopulateIntlPOTempTable()
        {
            System.Data.OracleClient.OracleCommand command = null;
            System.Data.OracleClient.OracleConnection conn = null;

            try
            {
                conn = new System.Data.OracleClient.OracleConnection();
                conn.ConnectionString = ConfigurationManager.AppSettings["RMS_CONN"].ToString();

                command = new System.Data.OracleClient.OracleCommand("RTLINTF.HTTD_FF_INTL_PO.HTTD_INTL_PO_TEMP_LOAD", conn);
                command.CommandType = CommandType.StoredProcedure;

                System.Data.OracleClient.OracleParameter vReturnCode = new System.Data.OracleClient.OracleParameter();
                vReturnCode.ParameterName = "O_return_code";
                vReturnCode.OracleType = OracleType.Number;
                vReturnCode.Direction = ParameterDirection.Output;

                System.Data.OracleClient.OracleParameter vErrorMsg = new System.Data.OracleClient.OracleParameter();
                vErrorMsg.ParameterName = "O_error_message";
                vErrorMsg.OracleType = OracleType.VarChar;
                vErrorMsg.Size = 32767;
                vErrorMsg.Direction = ParameterDirection.Output;

                command.Parameters.Add(vReturnCode);
                command.Parameters.Add(vErrorMsg);
                conn.Open();

                command.ExecuteNonQuery();

                // Check for error & throw exception
                if ((System.Convert.ToInt32(command.Parameters["O_return_code"].Value.ToString())) == 2 )
                {
                   throw new System.ArgumentException(command.Parameters["O_error_message"].Value.ToString());
                }

            }
            catch (Exception e)
            {
                throw new Exception("PopulateIntlPOTempTable Error: " + e.Message);
            }
            finally
            {
                conn.Close();
            }

        }

        public static int GetBatchControlNo()
        {            
            System.Data.OracleClient.OracleCommand command = null;
            System.Data.OracleClient.OracleConnection conn = null;

            try
            {
                conn = new System.Data.OracleClient.OracleConnection();
                conn.ConnectionString = ConfigurationManager.AppSettings["RMS_CONN"].ToString();

                command = new System.Data.OracleClient.OracleCommand("RTLINTF.HT_FF_INTL_PO2.SET_BATCH_CTRL_NUM", conn);
                command.CommandType = CommandType.StoredProcedure;

                System.Data.OracleClient.OracleParameter vBatchCtrlNo = new System.Data.OracleClient.OracleParameter();
                    vBatchCtrlNo.ParameterName = "o_Batch_Ctrl_No";
                    vBatchCtrlNo.OracleType = OracleType.Number;
                    vBatchCtrlNo.Direction = ParameterDirection.Output;

                System.Data.OracleClient.OracleParameter vReturnCode = new System.Data.OracleClient.OracleParameter();
                    vReturnCode.ParameterName = "o_Return_Code";
                    vReturnCode.OracleType = OracleType.Number;
                    vReturnCode.Direction = ParameterDirection.Output;

                System.Data.OracleClient.OracleParameter vErrorMsg = new System.Data.OracleClient.OracleParameter();
                    vErrorMsg.ParameterName = "o_error_message";
                    vErrorMsg.OracleType = OracleType.VarChar;
                    vErrorMsg.Size = 32767;
                    vErrorMsg.Direction = ParameterDirection.Output;
                                                         
                command.Parameters.Add(vBatchCtrlNo);
                command.Parameters.Add(vReturnCode);
                command.Parameters.Add(vErrorMsg);
                conn.Open();
                
                command.ExecuteNonQuery();

                // Check for error & throw exception
                if ((System.Convert.ToInt32(command.Parameters["o_Return_Code"].Value.ToString())) == 2)
                {
                    throw new System.ArgumentException(command.Parameters["o_error_message"].Value.ToString());
                }

                // If no errors, return result
                return System.Convert.ToInt32(command.Parameters["o_Batch_Ctrl_No"].Value.ToString());

            }
            catch (Exception e)
            {
                throw new Exception("GetBatchControlNo Error: " + e.Message);
            }
            finally
            {
                conn.Close();
            }

        }

        static public void ValidatePOs(int pBatchCtrlNo)
        {
            System.Data.OracleClient.OracleCommand command = null;
            System.Data.OracleClient.OracleConnection conn = null;

            try
            {
                conn = new System.Data.OracleClient.OracleConnection();
                conn.ConnectionString = ConfigurationManager.AppSettings["RMS_CONN"].ToString();

                command = new System.Data.OracleClient.OracleCommand("RTLINTF.HT_FF_INTL_PO2.VALIDATE_PO_RECS", conn);
                command.CommandType = CommandType.StoredProcedure;

                System.Data.OracleClient.OracleParameter vBatchCtrlNo = new System.Data.OracleClient.OracleParameter();
                vBatchCtrlNo.ParameterName = "o_Batch_Ctrl_No";
                vBatchCtrlNo.OracleType = OracleType.Number;
                vBatchCtrlNo.Direction = ParameterDirection.Input;
                vBatchCtrlNo.Value = pBatchCtrlNo;

                System.Data.OracleClient.OracleParameter vReturnCode = new System.Data.OracleClient.OracleParameter();
                vReturnCode.ParameterName = "o_Return_Code";
                vReturnCode.OracleType = OracleType.Number;
                vReturnCode.Direction = ParameterDirection.Output;

                System.Data.OracleClient.OracleParameter vErrorMsg = new System.Data.OracleClient.OracleParameter();
                vErrorMsg.ParameterName = "o_error_message";
                vErrorMsg.OracleType = OracleType.VarChar;
                vErrorMsg.Size = 32767;
                vErrorMsg.Direction = ParameterDirection.Output;



                command.Parameters.Add(vBatchCtrlNo);
                command.Parameters.Add(vReturnCode);
                command.Parameters.Add(vErrorMsg);
                conn.Open();

                command.ExecuteNonQuery();

                // Check for error & throw exception
                if ((System.Convert.ToInt32(command.Parameters["o_Return_Code"].Value.ToString())) == 2)
                {
                    throw new System.ArgumentException(command.Parameters["o_error_message"].Value.ToString());
                }

            }
            catch (Exception e)
            {
                throw new Exception("ValidatePOs Error: " + e.Message);
            }
            finally
            {
                conn.Close();
            }

        }


        public static void CleanStagingTable()
        {
            System.Data.OracleClient.OracleCommand command = null;
            System.Data.OracleClient.OracleConnection conn = null;

            try
            {
                conn = new System.Data.OracleClient.OracleConnection();
                conn.ConnectionString = ConfigurationManager.AppSettings["RMS_CONN"].ToString();

                command = new System.Data.OracleClient.OracleCommand("RTLINTF.HTTD_FF_INTL_PO.HTTD_INTL_PO_TEMP_CLEANUP", conn);
                command.CommandType = CommandType.StoredProcedure;

                System.Data.OracleClient.OracleParameter vReturnCode = new System.Data.OracleClient.OracleParameter();
                vReturnCode.ParameterName = "O_return_code";
                vReturnCode.OracleType = OracleType.Number;
                vReturnCode.Direction = ParameterDirection.Output;

                System.Data.OracleClient.OracleParameter vErrorMsg = new System.Data.OracleClient.OracleParameter();
                vErrorMsg.ParameterName = "O_error_message";
                vErrorMsg.OracleType = OracleType.VarChar;
                vErrorMsg.Size = 32767;
                vErrorMsg.Direction = ParameterDirection.Output;

                command.Parameters.Add(vReturnCode);
                command.Parameters.Add(vErrorMsg);
                conn.Open();

                command.ExecuteNonQuery();

                // Check for error & throw exception
                if ((System.Convert.ToInt32(command.Parameters["O_return_code"].Value.ToString())) == 2)
                {
                    throw new System.ArgumentException(command.Parameters["O_error_message"].Value.ToString());
                }

            }
            catch (Exception e)
            {
                throw new Exception("CleanStagingTable Error: " + e.Message);
            }
            finally
            {
                conn.Close();
            }

        }


        public static DataSet GetINTLPO(int pBatchCtrlNo)
        {
            // Create dataset from oracle pkg
            DataSet ds = null;
            System.Data.OracleClient.OracleCommand command = null;
            System.Data.OracleClient.OracleConnection conn = null;
            System.Data.OracleClient.OracleDataAdapter dataAdapter = null;
                  
            try
            {
                conn = new System.Data.OracleClient.OracleConnection();
                conn.ConnectionString = ConfigurationManager.AppSettings["RMS_CONN"].ToString();

                command = new System.Data.OracleClient.OracleCommand("RTLINTF.HT_FF_INTL_PO2.GET_INTL_PO", conn);
                command.CommandType = CommandType.StoredProcedure;

                System.Data.OracleClient.OracleParameter vBatchCtrlNo = new System.Data.OracleClient.OracleParameter();
                vBatchCtrlNo.ParameterName = "p_Batch_Ctrl_No";
                vBatchCtrlNo.OracleType = OracleType.Number;
                vBatchCtrlNo.Direction = ParameterDirection.Input;
                vBatchCtrlNo.Value = pBatchCtrlNo;

                System.Data.OracleClient.OracleParameter dsPO = new System.Data.OracleClient.OracleParameter();
                dsPO.OracleType = OracleType.Cursor;
                dsPO.ParameterName = "p_INTL_PO_cur";
                dsPO.Direction = ParameterDirection.Output;

                System.Data.OracleClient.OracleParameter vReturnCode = new System.Data.OracleClient.OracleParameter();
                vReturnCode.ParameterName = "o_return_code";
                vReturnCode.OracleType = OracleType.Number;
                vReturnCode.Direction = ParameterDirection.Output;

                System.Data.OracleClient.OracleParameter vErrorMsg = new System.Data.OracleClient.OracleParameter();
                vErrorMsg.ParameterName = "o_error_message";
                vErrorMsg.OracleType = OracleType.VarChar;
                vErrorMsg.Size = 32767;
                vErrorMsg.Direction = ParameterDirection.Output;

                command.Parameters.Add(vBatchCtrlNo);
                command.Parameters.Add(dsPO);
                command.Parameters.Add(vReturnCode);
                command.Parameters.Add(vErrorMsg);
                conn.Open();

                dataAdapter = new System.Data.OracleClient.OracleDataAdapter(command);
                ds = new DataSet();
                dataAdapter.Fill(ds);

                // Check for error & throw exception
                if ((System.Convert.ToInt32(command.Parameters["o_Return_Code"].Value.ToString())) == 2)
                {
                    throw new System.ArgumentException(command.Parameters["o_error_message"].Value.ToString());
                }

            }
            catch (Exception e)
            {
                throw new Exception("GetINTLPO: " + e.Message);
            }
            finally
            {
                conn.Close();
            }

            return ds;       
        }



        static public void UpdatePOTemp(int pBatchCtlNo)
        {
            //DataSet ds_d = null;
            System.Data.OracleClient.OracleCommand command = null;
            System.Data.OracleClient.OracleConnection conn = null;
            //System.Data.OracleClient.OracleDataAdapter dataAdapter = null;

            try
            {
                conn = new System.Data.OracleClient.OracleConnection();
                conn.ConnectionString = ConfigurationManager.AppSettings["RMS_CONN"].ToString();

                command = new System.Data.OracleClient.OracleCommand("RTLINTF.HT_FF_INTL_PO2.UPDATE_PO_TEMP", conn);
                command.CommandType = CommandType.StoredProcedure;

                System.Data.OracleClient.OracleParameter vBatchCtrlNo = new System.Data.OracleClient.OracleParameter();
                vBatchCtrlNo.ParameterName = "p_Batch_Ctrl_No";
                vBatchCtrlNo.OracleType = OracleType.Number;
                vBatchCtrlNo.Direction = ParameterDirection.Input;
                vBatchCtrlNo.Value = pBatchCtlNo;

                System.Data.OracleClient.OracleParameter vReturnCode = new System.Data.OracleClient.OracleParameter();
                vReturnCode.ParameterName = "o_return_code";
                vReturnCode.OracleType = OracleType.Number;
                vReturnCode.Direction = ParameterDirection.Output;

                System.Data.OracleClient.OracleParameter vErrorMsg = new System.Data.OracleClient.OracleParameter();
                vErrorMsg.ParameterName = "o_error_message";
                vErrorMsg.OracleType = OracleType.VarChar;
                vErrorMsg.Size = 32767;
                vErrorMsg.Direction = ParameterDirection.Output;

                command.Parameters.Add(vBatchCtrlNo);
                command.Parameters.Add(vReturnCode);
                command.Parameters.Add(vErrorMsg);
                conn.Open();

                command.ExecuteNonQuery();

                // Check for error & throw exception
                if ((System.Convert.ToInt32(command.Parameters["o_Return_Code"].Value.ToString())) == 2)
                {
                    throw new System.ArgumentException(command.Parameters["o_error_message"].Value.ToString());
                }


                //System.Data.OracleClient.OracleParameter p1 = new System.Data.OracleClient.OracleParameter();
                //p1.OracleType = OracleType.Number;
                //p1.ParameterName = "p_PO_NUM";
                //p1.Direction = ParameterDirection.Input;
                //command.Parameters.Add(p1);

               // command.ExecuteNonQuery();
                
                //dataAdapter = new System.Data.OracleClient.OracleDataAdapter(command);
                //ds_d = new DataSet();
                //dataAdapter.Fill(ds_d);

            }
            catch (Exception e)
            {
                throw new Exception("UpdatePO Temp error: " + e.Message);
            }
            finally
            {
                conn.Close();
            }

        }

        //public static DataSet GetPrepackDetails()
        //{
        //    DataSet ds = null;
        //    System.Data.OracleClient.OracleCommand command = null;
        //    System.Data.OracleClient.OracleConnection conn = null;
        //    System.Data.OracleClient.OracleDataAdapter dataAdapter = null;
        //     
        //    try
        //    {
        //        conn = new System.Data.OracleClient.OracleConnection();
        //        conn.ConnectionString = ConfigurationManager.AppSettings["RMS_CONN"].ToString();

        //        command = new System.Data.OracleClient.OracleCommand("HOTTOPIC.HT_SECURITY_PRINT.GET_PREPACK_DETAILS", conn);
        //        command.CommandType = CommandType.StoredProcedure;

        // Add parameter(s)
        //        
        //        System.Data.OracleClient.OracleParameter p1 = new System.Data.OracleClient.OracleParameter();
        //        p1.OracleType = OracleType.Cursor;
        //        p1.ParameterName = "p_ppk_cur";
        //        p1.Direction = ParameterDirection.Output;
        //        command.Parameters.Add(p1);                

        //        dataAdapter = new System.Data.OracleClient.OracleDataAdapter(command);
        //        ds = new DataSet();
        //        dataAdapter.Fill(ds);
        //
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception("data access error: " + e.Message);
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //
        //    return ds;       
        //}

    }
}
