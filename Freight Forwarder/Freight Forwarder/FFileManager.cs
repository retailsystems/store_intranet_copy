using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;

namespace FreightForwarder
{
    class FFileManager
    {
        private System.Collections.Specialized.NameValueCollection _appSettings;
        private string _FFile;
        private string _FileName;
        private string _fileFolder;
        private string _ErrorFolder;
        private string _fieldSeperator = ",";
        private string _archiveFolder = "";
        private string _SMTPServer = "";
        private string _emailSender = "";
        private string _emailReceivers = "";
        private string _emailBody = "";
        private string _emailSubject = "";
        //private string  v_batch_ctrl_num;
        private string _v_curr_ponum = "";
        private string _v_comment = "";
        private string _v_SUPPLIER="";
        private string _HTS = "";
        //private string _v_SUP_NAME="";
        //private string _v_ADDR1="";
        //private string _v_ADDR2="";
        //private string _v_CITY="";
        //private string _v_STATE="";
        //private string _v_POST="";
        
        //string[] _fileArr;        
        private static HTError.ErrorHandler _err;
        
        public FFileManager(HTError.ErrorHandler err)
        {
            _appSettings = ConfigurationManager.AppSettings;
            _fileFolder = _appSettings["FileFolder"];
            _ErrorFolder = _appSettings["ErrorFolder"];
            if (!_fileFolder.EndsWith(@"\"))  _fileFolder += @"\";
            _archiveFolder = _appSettings["ArchiveFolder"];
            if (!_archiveFolder.EndsWith(@"\")) _archiveFolder += @"\";
         // _fileArr = _appSettings["FILE_ARR"].Split(',');          
            _SMTPServer = _appSettings["SMTPServer"];
            _emailSender = _appSettings["EmailSender"];
            _emailReceivers = _appSettings["EmailReceivers"];
            _emailBody = _appSettings["EmailBody"];
            _emailSubject = _appSettings["EmailSubject"];
            _err = err;
        }

        public void CreateFFiles()
        {
            try
            {   
                // call proc to populate staging table
                DataAccess.PopulateIntlPOTempTable();

                // Set and get batch control no
                int vBatchCtrlNo = DataAccess.GetBatchControlNo();
               // int vBatchCtrlNo = 1921;  // for testing

                // Validate the PO records in the staging table 
                DataAccess.ValidatePOs(vBatchCtrlNo);

                // Get dataset and generate the flat file
                CreateFFile(vBatchCtrlNo);         
      
                // Update and flag po Temp record as processed 
                DataAccess.UpdatePOTemp(vBatchCtrlNo); 

                // Call record cleanup proc
                DataAccess.CleanStagingTable();                

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CreateFFile(int pBatchCtlNo)
        {
            try
            {
                // Get dataset & check if data present.  Exit if not.
                DataSet ds = DataAccess.GetINTLPO(pBatchCtlNo);
                
                if (ds.Tables[0].Rows.Count != 0)
                {
                    _FileName = GetFilename();
                    _FFile = _fileFolder + _FileName;
                    _err.LogInformation("Creating Freight Forwarder file:" + _FFile);

                    // if old file exists, delete it
                    if (File.Exists(_FFile))
                    {
                        File.Delete(_FFile);
                    }

                    using (StreamWriter sw = File.CreateText(_FFile))
                    {
                        //String v_batch_ctrl_num = DataAccess.GetBatchCtrlNo();
                        _v_curr_ponum = "";
                        //trail = "N";
                        //sw.WriteLine(GetStringVal("TRANSAHEADER" + "," + "HOTTOPIC" + "," + "OLL" + "," + "PO" + "," + "BatchNO " + "," + DateTime.Now.ToString("yyyyMMdd") + "," + DateTime.Now.ToString("HHmmss")   ));
                        sw.WriteLine("TRANSACHEADER" + "," + "HOTTOPIC" + "," + "OLL" + "," + "PO" + "," + pBatchCtlNo + "," + DateTime.Now.ToString("yyyyMMdd") + "," + DateTime.Now.ToString("HHmmss"));
                      
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            System.Text.StringBuilder sb = new System.Text.StringBuilder();
                            if ((_v_curr_ponum != dr["order_no"].ToString()))
                            {
                                if (_v_curr_ponum != "")
                                {
									//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "1" + "," + "Important - This order is automatically cancelled on the Cancel If Not Received By Date.");
									//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "2" + "," + "Orders received after this date will be refused.");
									//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "3" + "," + "OUR ORDER NUMBER MUST APPEAR ON ALL PACKAGING SLIPS MAILING LABELS AND INVOICES.");
									//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "4" + "," + "Shipments without our order number may be refused.");
									//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "5" + "," + "All terms of payment are from receipt of goods.");
									//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "6" + "," + "If order is not shipping complete please contact the buyer.");
									//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "7" + "," + "REFER TO PRETICKETING INSTRUCTIONS.");
									//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "8" + "," + "CREATE ASN & SCHEDULE APPOINTMENT AT A MINIMUM OF 48 HOURS PRIOR TO SHIPPING.");
									//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "9" + "," + "DO NOT BACK ORDER.");
									//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "10" + "," + "DO NOT PRESHIP WITHOUT APPROVAL.");
									//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "11" + "," + "DO NOT SHIP PRIOR TO T.O.P. SAMPLE APPROVAL.");
									//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "12" + "," + _v_comment.ToString());
                                    sw.WriteLine("ADDRESS" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "BUY" + "," + "HOTTOPIC" + "," + "," + "," + "," + "," + ",");
                                    sw.WriteLine("ADDRESS" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "DLY" + "," + "HOTTOPIC" + "," + "," + "," + "," + "," + ",");
                                    sw.WriteLine("ADDRESS" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "INV" + "," + "HOTTOPIC" + "," + "," + "," + "," + "," + ",");
                                    sw.Write("ADDRESS" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum + "," + "SUP" + ",");
                                   // sw.WriteLine(dr["SUPPLIER"].ToString() + "," + "VE_NAME" + "," + "ADDR1" + "," + "ADDR2" + "," + "CITY" + "," + "ST" + "," + "POST");
                                    sw.WriteLine(dr["SUPPLIER"].ToString() + "," + dr["SUP_NAME"].ToString().Replace(",", "") + "," + dr["ADD_1"].ToString().Replace(",", "") + "," + dr["ADD_2"].ToString().Replace(",", "") + "," + dr["CITY"].ToString().Replace(",", "") + "," + dr["STATE"].ToString().Replace(",", "") + "," + dr["POST"].ToString().Replace(",", ""));
                                }

                                // ** Write Header line **
                                _v_curr_ponum = dr["order_no"].ToString();
                                sb.Append("HEADER" + "," + "HOTTOPIC" + "," + "001" + ",");
                                sb.Append(dr["order_no"].ToString() + "," + ",");
                                sb.Append(DateTime.Parse(dr["WRITTEN_DATE"].ToString()).ToString("yyyyMMdd") + ",");
                                sb.Append("" + ",");
                                sb.Append("C" + ",");
                                sb.Append(GetStringVal(dr["SHIP_METHOD"].ToString()) + ",");
                                sb.Append("," + "," + "," + "," + "," + "," + "," + "," + "," + ",");
                                sb.Append(dr["SUPPLIER"].ToString() + ",");
                                sb.Append("" + ",");
                                sb.Append(dr["SUP_NAME"].ToString().Replace(",", "") + ",");
                                sb.Append("," + ",");
                                sb.Append(dr["BUYER_NAME"].ToString()+ ",");
                                sb.Append("," + "," + ",");
                                sb.Append(dr["LADING_PORT"].ToString().Replace(",", "") + ",");
                                sb.Append(dr["LADING_PORT_NAME"].ToString().Replace(",", "") + ",");
                                sb.Append(dr["DISCHARGE_PORT"].ToString().Replace("," , "")+ ",");
                                sb.Append(dr["DISCHARGE_PORT_NAME"].ToString().Replace("," , "")+ ",");
                                sb.Append(",");
                                sb.Append(DateTime.Parse(dr["EARLIEST_SHIP_DATE"].ToString()).ToString("yyyyMMdd"));
                                sb.Append(","); // EarliestShipmentDate
                                sb.Append(","); // LatestShipmentDate
                                sb.Append(","); // LatestShipmentDate
                                if (dr["CLOSE_DATE"].ToString() != "")
                                {
                                    sb.Append(DateTime.Parse(dr["CLOSE_DATE"].ToString()).ToString("yyyyMMdd"));
                                    sb.Append(",");
                                }
                                else
                                {
                                    sb.Append(","); // Filter1
                                }
                                
                                //sb.Append(","); // Filter1
                                sb.Append(","); // Filter2
                                sb.Append("," + "N" + ","); //HeaderOnly

                                sb.Append(dr["SUPPLIER"].ToString() + ",");
                                sb.Append(dr["BUYER_NAME"].ToString() + ",");
                                sb.Append(dr["BUYER_NAME"].ToString() + ",");
                                sb.Append(dr["LADING_PORT"].ToString() + ",");
                                sb.Append(dr["DISCHARGE_PORT"].ToString() + ",");
                                sb.Append("," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "PO" + ",");
                                sb.Append(dr["ORIGIN_COUNTRY_ID"].ToString().Replace(",", "") + _fieldSeperator);
								sb.Append(DateTime.Parse(dr["PO_CANCEL_DATE"].ToString()).ToString("yyyyMMdd") + ",");
                                // Set status to be CANCEL if Status = 'C'
                                if (dr["STATUS"].ToString() == "C")
                                {
                                    sb.Append("CANCEL");
                                }
                                else
                                {
                                    sb.Append(",");
                                }
                                sw.WriteLine(sb.ToString());
                            }

                            // ** Write Detail lines **
                            System.Text.StringBuilder sb_d = new System.Text.StringBuilder();
                            sb_d.Append("DETAIL" + "," + "HOTTOPIC" + "," + "001" + ",");
                            sb_d.Append(dr["ORDER_NO"].ToString() + _fieldSeperator);
                            sb_d.Append(GetStringVal(dr["ITEM"].ToString().Replace("-", "")) + ",");
                            sb_d.Append(dr["ITEM_DESC"].ToString().Replace(",", "") + ",");                            
                            sb_d.Append(dr["COLOR_CD"].ToString() + ",");
                            sb_d.Append(GetStringVal(dr["SIZE_CD"].ToString()) + ",");
                            sb_d.Append(",");
                            sb_d.Append("1" + _fieldSeperator);
                            sb_d.Append(GetStringVal(dr["QTY_ORDERED"].ToString()) + ",");
                            sb_d.Append("CTN" + _fieldSeperator);
                            sb_d.Append(GetStringVal(dr["QTY_ORDERED"].ToString()) + ",");
                            sb_d.Append("PCS" + _fieldSeperator);
                            sb_d.Append(GetStringVal(dr["UNIT_COST"].ToString()) + ",");
                            //sb_d.Append("," + "," + "," + "," + "," + "," + "," + "," + "," + "USD");
                            sb_d.Append("," + "," + "," + "," + "," + "," + "," + "USD");
                            sb_d.Append(",");

                            _HTS = dr["HTS"].ToString().Substring(0, 4) + "." + dr["HTS"].ToString().Substring(4, 2) + "." + dr["HTS"].ToString().Substring(6, 4);
                            sb_d.Append(_HTS + ",");

                            //sb_d.Append(GetStringVal(dr["HTS"].ToString().Replace(",", "")) + ",");
                            
                            //sb_d.Append("," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + ",");
                            sb_d.Append("," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + "," + ",");
                            sb_d.Append(GetStringVal(dr["DEPT"].ToString()) + ",");
                            sb_d.Append("," + "," + ",");
                            sb_d.Append(GetStringVal(dr["SUPPLIER"].ToString()));
                            //sb_d.Append("," + "," + "," + ",");
                            sb_d.Append("," + "," + "," + "," + ",");

                            // MFG data
                            sb_d.Append(GetStringVal(dr["SUP_NAME"].ToString()) + ",");
                            sb_d.Append(GetStringVal(dr["MFG_NAME"].ToString()) + ",");
                            sb_d.Append(GetStringVal(dr["MFG_ADDR"].ToString()) + ",");
                            sb_d.Append(GetStringVal(dr["MFG_CITY"].ToString()) + ",");
                            sb_d.Append(GetStringVal(dr["MFG_STATE"].ToString()) + ",");
							sb_d.Append(GetStringVal(dr["CLASS"].ToString()) + ",");
							sb_d.Append(GetStringVal(dr["CLASS_NAME"].ToString()) + ",");
							//sb_d.Append(DateTime.Parse(dr["PO_CANCEL_DATE"].ToString()).ToString("yyyyMMdd") + ",");
							sb_d.Append(GetStringVal(dr["MATERIAL_CONTENT"].ToString()) + ",");
							sb_d.Append(GetStringVal(dr["COMMERCIAL_INVOICE_DESCRIPTION"].ToString()) + ",");
							sb_d.Append(GetStringVal(dr["GENDER"].ToString()) + ",");
							sb_d.Append(GetStringVal(dr["KNITORWOVEN"].ToString()));
                            sw.WriteLine(sb_d.ToString());

                            _v_comment = dr["comment_desc"].ToString();
                            _v_SUPPLIER = dr["supplier"].ToString();

                        }

						//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "1" + "," + "Important - This order is automatically cancelled on the Cancel If Not Received By Date.");
						//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "2" + "," + "Orders received after this date will be refused.");
						//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "3" + "," + "OUR ORDER NUMBER MUST APPEAR ON ALL PACKAGING SLIPS MAILING LABELS AND INVOICES.");
						//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "4" + "," + "Shipments without our order number may be refused.");
						//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "5" + "," + "All terms of payment are from receipt of goods.");
						//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "6" + "," + "If order is not shipping complete please contact the buyer.");
						//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "7" + "," + "REFER TO PRETICKETING INSTRUCTIONS.");
						//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "8" + "," + "CREATE ASN & SCHEDULE APPOINTMENT AT A MINIMUM OF 48 HOURS PRIOR TO SHIPPING.");
						//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "9" + "," + "DO NOT BACK ORDER.");
						//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "10" + "," + "DO NOT PRESHIP WITHOUT APPROVAL.");
						//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "11" + "," + "DO NOT SHIP PRIOR TO T.O.P. SAMPLE APPROVAL.");
						//sw.WriteLine("NOTE" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "12" + "," + _v_comment.ToString());

                        sw.WriteLine("ADDRESS" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "BUY" + "," + "HOTTOPIC" + "," + "," + "," + "," + "," + ",");
                        sw.WriteLine("ADDRESS" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "DLY" + "," + "HOTTOPIC" + "," + "," + "," + "," + "," + ",");
                        sw.WriteLine("ADDRESS" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "INV" + "," + "HOTTOPIC" + "," + "," + "," + "," + "," + ",");
                        sw.WriteLine("ADDRESS" + "," + "HOTTOPIC" + "," + "001" + "," + _v_curr_ponum.ToString() + "," + "SUP" + ",");
                        //sw.WriteLine(dr["SUPPLIER"].ToString() + "," + "VE_NAME" + "," + "ADDR1" + "," + "ADDR2" + "," + "CITY" + "," + "ST" + "," + "POST");
                        //sw.WriteLine(dr["SUPPLIER"].ToString() + "," + dr["_v_SUP_NAME"].ToString().Replace(",", "") + "," + dr["_v_ADDR1"].ToString().Replace(",", "") + "," + dr["_v_ADDR2"].ToString().Replace(",", "") + "," + dr["_v_CITY"].ToString().Replace(",", "") + "," + dr["_v_STATE"].ToString().Replace(",", "") + "," + dr["_v_POST"].ToString().Replace(",", ""));
                        sw.Close();

                    }
                    // ftp Freight Forwarder file to OOCL
                    ftpFile();

                    //email Freight Forwarder file
                    _err.LogInformation("Emailing Freight file:" + _FFile);
                    SendMail(_FFile, GetFFileName());
                
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


           
       // private void CreatePrepackFile()
       // {
       //     try
       //     {
       //         _ticketFile = _fileFolder + "Prepack.txt";
       //         _err.LogInformation("Creating prepack file:" + _ticketFile);
       //         DataSet ds = DataAccess.GetPrepackDetails();
       //         if (File.Exists(_ticketFile))
       //         {
       //             File.Delete(_ticketFile);
       //         }
       //         using (StreamWriter sw = File.CreateText(_ticketFile))
       //         {
       //             // sw.Write(GetStringVal("Test"));
       //             foreach (DataRow dr in ds.Tables[0].Rows)
       //             {
       //                 System.Text.StringBuilder sb = new System.Text.StringBuilder();
       // 
       //                 sb.Append(GetStringVal(dr["dept_cd"].ToString()) + _fieldSeperator);
       //                 sb.Append(GetStringVal(dr["ve_cd"].ToString()) + _fieldSeperator);
       //                 //sb.Append(GetStringVal(dr["itm_cd"].ToString()) + _fieldSeperator);
       //                 sb.Append(dr["po_num"].ToString() + _fieldSeperator);                   
       //                 sb.Append(GetStringVal(dr["sku_num"].ToString()) + _fieldSeperator);
       //                 sb.Append(dr["qty"].ToString() + _fieldSeperator);
       //                 sb.Append(GetStringVal(dr["item_desc"].ToString()) + _fieldSeperator);
       //                 sb.Append(GetStringVal(dr["PREPACK_CD"].ToString()) + _fieldSeperator);
       //                 sb.Append(GetStringVal(dr["ppk_des"].ToString()));                   
       //                 sw.WriteLine(sb.ToString());                        
       //             }
       //             sw.Close();
       //         }
                //email file
       //         _err.LogInformation("Emailing prepack file:" + _ticketFile);
       //         SendMail(_ticketFile, "Prepack");
       //     }
       //     catch (Exception ex)
       //     {
       //         throw ex;
       //     }
       // }


        private void ftpFile()
        {
            string _ftpURI = _appSettings["ftpURI"];
            string _ftpUser = _appSettings["ftpUser"];
            string _ftpPassword = _appSettings["ftpPassword"];
            string _ftpRemotePath = _appSettings["ftpRemotePath"];

            ftp ftpClient = new ftp(_ftpURI, _ftpUser, _ftpPassword);
            ftpClient.upload(_ftpRemotePath + _FileName, _appSettings["FileFolder"] + _FileName);                
        }

        private void SendMail(string fileName, string company)
        {
            //string company = GetCompanyName(divCd);
            // Create a message and set up the recipients.
            MailMessage message = new MailMessage();
            //message.Sender = new MailAddress(_emailSender);
            message.Subject = _emailSubject.Replace("%company%", company);
            message.IsBodyHtml = true;
            message.From = new MailAddress(_emailSender);
            message.Body = _emailBody.Replace("%loc%", fileName); 
            // Create  the file attachment for this e-mail message.
            Attachment data = new Attachment(fileName);          
            // Add the file attachment to this e-mail message.
            message.Attachments.Add(data);
            message.To.Add(_emailReceivers);
            //Send the message.
            SmtpClient client = new SmtpClient(_SMTPServer);
            // Add credentials if the SMTP server requires them.
            //client.Credentials = CredentialCache.DefaultNetworkCredentials;
            client.Send(message);
        }

        public void ArchiveFiles()
        {
            //_archiveFolder += DateTime.Now.ToString("MMddyyHHmi") + @"\";
            string archivePath = _archiveFolder + DateTime.Now.ToString("MMddyyHHmm") + @"\";
            DirectoryInfo diSource = new DirectoryInfo(_fileFolder);
            DirectoryInfo diTarget = new DirectoryInfo(archivePath);            
            if (diSource.Exists)
            {
                //System.Collections.IEnumerator ienum = diSource.GetFiles("*.txt").GetEnumerator();
                System.Collections.IEnumerator ienum = diSource.GetFiles("*.*").GetEnumerator();
                while (ienum.MoveNext()){
                    if (!diTarget.Exists) diTarget.Create();
                    FileInfo fi = (FileInfo)ienum.Current;
                    FileInfo fiTarget = new FileInfo(archivePath + fi.Name);
                    if (fiTarget.Exists) fiTarget.Delete();
                    fi.MoveTo(archivePath + fi.Name);
                }
            }
        }

        private string GetFilename()
        {
            //string fileName = "HotTopicFull.txt";
            string fileName = "POEDHOTTOPIC" + DateTime.Now.ToString("MMddyyHHmmss") + ".csv";
            return fileName;
        }

        private string GetFFileName()
        {
            string companyName = _appSettings["CompanyName"];
            return companyName;
        }

        private string GetStringVal(string colVal){
            return '"' + colVal + '"';
        }

    }
}
