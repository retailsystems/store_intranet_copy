using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Net.Mail;

namespace FreightForwarder
{
    class Program
    {
        private static HTError.ErrorHandler _err;
        private static System.Collections.Specialized.NameValueCollection _appSettings;                
                 
       
        static void Main(string[] args)
        {
            int exitCode = 1;
            
            try
            {
                //System.Collections.Specialized.NameValueCollection appSettings = System.Configuration.ConfigurationSettings.AppSettings["TEST"];
                Initialize();
                FFileManager tfm = new FFileManager(_err);
                               

                _err.LogInformation("Begin process");
                _err.LogInformation("Archiving old files");
                
                  tfm.ArchiveFiles();                
                  tfm.CreateFFiles();
              
                _err.LogInformation("End process");
                exitCode = 0;
            }
            catch (Exception ex)
            {
                _err.LogError(ex);
              //  SendErrorMail(ex.ToString());
                
                exitCode = 1;
            }
            finally
            {
                Environment.Exit(exitCode);
            }
        }

//        private static void SendErrorMail(string err)
//        {          
//            // string company = GetCompanyName(divCd);
//            // Create a message and set up the recipients.
//            MailMessage message = new MailMessage();
//            message.Sender = new MailAddress(_appSettings["ErrEmailSender"]);
//            message.Subject = _appSettings["ErrEmailSubject"];
//            message.IsBodyHtml = true;
//            message.From = new MailAddress(_appSettings["ErrEmailSender"]);
////            message.Body = _ErrEmailBody;
//            message.Body = (err);

//            // Attachment data = new Attachment(fileName);
//            // Add the file attachment to this e-mail message.
//            // message.Attachments.Add(data);
//            message.To.Add(_appSettings["ErrEmailReceivers"]);
//            // Send the message.
//            SmtpClient client = new SmtpClient(_appSettings["SMTPServer"]);
//            // Add credentials if the SMTP server requires them.
//            // client.Credentials = CredentialCache.DefaultNetworkCredentials;
//            client.Send(message);
//        }

        private static void Initialize()
        {
            _appSettings = ConfigurationManager.AppSettings;
            _err = new HTError.ErrorHandler(_appSettings["ProductionLog"], _appSettings["DebugLog"],_appSettings["SMTPServer"],_appSettings["EmailSender"],_appSettings["ErrorEmailReceivers"], HTError.LogLevel.Debug);
            if (_appSettings["EnableDebugLog"].ToUpper().Equals("TRUE"))
            {
                _err.EnableDebugLog = true;
            }
            else
            {
                _err.EnableDebugLog = false;
            }
            if (_appSettings["LogMetaData"].ToUpper().Equals("TRUE"))
            {
                _err.IncludeDriveSpaceInfo = true;
                _err.IncludeNetworkDrives = true;
                _err.IncludeNetworkStatus = true;
                _err.IncludeRamInfo = true;
                _err.IncludeCurrentUser = true;
            }
            else
            {
                _err.IncludeDriveSpaceInfo = false;
                _err.IncludeNetworkDrives = false;
                _err.IncludeNetworkStatus = false;
                _err.IncludeRamInfo = false;
                _err.IncludeCurrentUser = false;
            }
            if (_appSettings["EnableEmail"].ToUpper().Equals("TRUE"))
            {
                _err.EnableEmail = true;
            }
            else
            {
                _err.EnableEmail = false;
            }
        }
    }
}
