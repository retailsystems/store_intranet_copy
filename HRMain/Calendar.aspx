<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Calendar.aspx.vb" Inherits="HRMain.frmCalendar"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Calendar</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK id="lnkStyles" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body leftMargin="0" rightMargin="0">
		<form id="frmCalendar" method="post" runat="server">
			<div class="container home" role="main">
			<iframe src="Header.aspx" frameBorder="no" width="100%" scrolling="no" height="75"></iframe>
			<hr />
			<table width="100%">
				<tr>
					<td class="HR_ArticleTitle">Training Class Calendar:
						<asp:DropDownList id="cboDates" runat="server" AutoPostBack="True"></asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td vAlign="top">
						<br />
						<asp:datalist id="DataList1" runat="server" RepeatColumns="7" RepeatDirection="Horizontal" Width="100%" EnableViewState="False">
							<HeaderTemplate>
								<table class="archtblhdr" width="100%">
									<tr>
										<td align="center" style="FONT-WEIGHT: bold" width="14%">SUN</td>
										<td align="center" style="FONT-WEIGHT: bold" width="14%">MON</td>
										<td align="center" style="FONT-WEIGHT: bold" width="14%">TUE</td>
										<td align="center" style="FONT-WEIGHT: bold" width="14%">WED</td>
										<td align="center" style="FONT-WEIGHT: bold" width="14%">THU</td>
										<td align="center" style="FONT-WEIGHT: bold" width="14%">FRI</td>
										<td align="center" style="FONT-WEIGHT: bold" width="14%">SAT</td>
									</tr>
								</table>
							</HeaderTemplate>
							<AlternatingItemStyle CssClass="HR_DataGridAlternatingRows"></AlternatingItemStyle>
							<ItemStyle Height="100px" Width="14%" VerticalAlign="Top" cssclass="HR_CalendarItems"></ItemStyle>
							<ItemTemplate>
								<div class="HR_ArticleBody" align="center"><b>-<%# Container.DataItem("TrainingDate") %>-</b></div>
								<asp:DataList id="DataList2" runat="server" CellSpacing=6 datasource='<%# Container.DataItem.Row.GetChildRows("Training") %>' Width="100%">
									<ItemTemplate>
										<span class="HR_ArticleBody">&bull;
											<%# format(Container.DataItem("TrainingTime"), "h:mm tt") %>
											-
											<%# format(Container.DataItem("EndTime"), "h:mm tt") %>
											<%# iif(Container.DataItem("ClassFull"), " ***FULL***","") %>
											<br>
										</span><font color="white" size="1"><a class="HR_Link" href='ViewTraining.aspx?ID=<%# Container.DataItem("TrainingClassID") %>'>
												<%# Container.DataItem("TrainingClass") %>
											</a></font>
									</ItemTemplate>
								</asp:DataList>
							</ItemTemplate>
						</asp:datalist></td>
				</tr>
				<tr>
					<td align="center">
						<asp:Button CssClass="btn btn-danger" id="cmdBack" runat="server" Text="Back"></asp:Button>
					</td>
				</tr>
			</table>
			</div>
			<script src="/Global/js/jquery.min.js"></script>
			<script src="/Global/js/bootstrap.min.js"></script>
		</form>
	</body>
</HTML>
