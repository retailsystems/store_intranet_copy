Option Explicit On 

Partial Class frmLogin

    Inherits System.Web.UI.Page




#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim objDAL As New clsDAL()
    Dim objDataRow As DataRow
    Dim objDataTable As New DataTable()
    Private objHistory As New clsHistory
    Dim objUtilSessionHist As New TSessionHistory
    Dim objPageStyleSheet As New TPageStyleSheet
    Dim sessionCompName As String
    Dim objPageSession As New TPageSession

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        objUtilSessionHist.SetSessionHist(Me, objHistory, "History")
        If Session("CompanyName") Is Nothing Then
            objPageSession.GetSession(Me)
        End If
        sessionCompName = Convert.ToString(Session("CompanyName"))

        lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))

        Session("User/HRMainManager") = "N"

        'lblError.Visible = False

        If txtLoginID.Text = "" Then
            Call gSetFocus(txtLoginID)
        Else
            Call gSetFocus(txtPassword)
        End If

    End Sub

    Private Sub cmdSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSubmit.Click

        Dim blnHotTopic As Boolean

        blnHotTopic = True 'Check store from cookie

        objDataTable = objDAL.SelectUserMst(blnHotTopic, txtLoginID.Text, txtPassword.Text)

        If objDataTable.Rows.Count > 0 Then
            objDataRow = objDataTable.Rows(0)

            objDataTable = objDAL.SelectSecurity(blnHotTopic, objDataRow("User_ID"), objDataRow("Job_Class").ToString)

            For Each objDataRow In objDataTable.Rows
                Session("User/" & objDataRow("Function")) = objDataRow("Access")
            Next

            If Session("User/HRMainManager") = "Y" Then
                Response.Redirect("HRHome.aspx?Section=" & Request("Section"))
            End If
        End If

        lblError.Visible = True

    End Sub

    Private Sub cmdHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHome.Click

        Response.Redirect("HRHome.aspx")

    End Sub

End Class
