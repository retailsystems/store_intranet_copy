Public Class clsHistory
    'Inherits System.Web.UI.Page
    Private Pages As New Stack(20)
    Private PreviousPage As String


    Public Sub New()
    End Sub

    Public Sub Add(ByVal URL As String)

        If Not PreviousPage = URL Then Pages.Push(URL)
        PreviousPage = URL

    End Sub

    Public Function GetPrev(ByVal CurrentPage As String)

        If Pages.Count > 1 Then
            Pages.Pop()
            Return Pages.Pop
        Else
            Return "HRHome.aspx"
        End If

    End Function


End Class
