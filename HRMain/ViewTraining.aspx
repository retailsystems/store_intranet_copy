<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ViewTraining.aspx.vb" Inherits="HRMain.frmViewTraining"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ViewTraining</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmViewTraining" method="post" runat="server">
			<div class="container home" role="main">
			<iframe src="Header.aspx" frameBorder="no" width="100%" scrolling="no" height="75"></iframe>
			<hr />
			<table width="100%">
				<tr>
					<td vAlign="top"><asp:label id="lblTrainingClass" runat="server" CssClass="ArticleTitle">Training Class</asp:label></td>
				</tr>
				<tr>
					<td vAlign="top"><br /><asp:label id="lblDescription" runat="server" CssClass="ArticleBody">Description</asp:label></td>
				</tr>
				<tr>
					<td vAlign="top">
						<br />
						<asp:datalist id="lstTrainingDates" runat="server" EnableViewState="False">
							<ItemStyle VerticalAlign="Top"></ItemStyle>
							<ItemTemplate>
								<%# iif(Container.DataItem("ClassFull"), "***FULL***", "Sign Up -- <a href='mailto:" & Container.DataItem("Contact") & "?subject=Training Class Signup&body=" & Container.DataItem("TrainingClass") & " -- " & format(Container.DataItem("TrainingDate"), "MMM d, yyyy, h:mm tt") & " - " & format(Container.DataItem("EndTime"), "h:mm tt") & "'>") %>
								<%# format(Container.DataItem("TrainingDate"), "MMM d, yyyy, h:mm tt") %>
								-
								<%# format(Container.DataItem("EndTime"), "h:mm tt") %>
								<%# iif(Container.DataItem("ClassFull"), "", "</a>") %>
							</ItemTemplate>
						</asp:datalist>
						<asp:label id="lblNone" runat="server" CssClass="ArticleBody">Presently, there are no dates for this class.</asp:label>
					</td>
				</tr>
				<tr>
					<td vAlign="bottom" align="center"><INPUT class="btn btn-danger" onclick="window.history.back()" type="button" value="Back"></td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
