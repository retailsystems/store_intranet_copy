Option Explicit On 

Public Class frmHeader

    Inherits System.Web.UI.Page

    Protected WithEvents ctlLogo As Logo
    Protected WithEvents cmdSearch As System.Web.UI.WebControls.HyperLink
    Protected WithEvents cmdContactHR As System.Web.UI.WebControls.HyperLink

    Protected WithEvents cmdSection1 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents cmdSection2 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents cmdSection3 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents cmdSection4 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents cmdSection5 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents cmdSection6 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents cmdSection7 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents cmdSection8 As System.Web.UI.WebControls.HyperLink

    Protected WithEvents cmdLoginHere As System.Web.UI.WebControls.HyperLink
    Protected WithEvents cmdHotTopicWAN As System.Web.UI.WebControls.HyperLink
    Protected WithEvents ImgSiteMap As System.Web.UI.WebControls.ImageButton
    Protected WithEvents cmdSiteMap As System.Web.UI.WebControls.HyperLink
    Protected WithEvents hypWAN As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents imgSiteMapArrow As System.Web.UI.WebControls.Image
    Protected WithEvents imgSearchArrow As System.Web.UI.WebControls.Image
    Protected WithEvents imgContactHRArrow As System.Web.UI.WebControls.Image
    Protected WithEvents imgSiteAdminArrow As System.Web.UI.WebControls.Image
    Protected WithEvents imgWANArrow As System.Web.UI.WebControls.Image
    Protected WithEvents lnkWAN As System.Web.UI.WebControls.HyperLink

    'Menu Bar Links
    Public MnuItem_1_Link As String
    Public MnuItem_2_Link As String
    Public MnuItem_3_Link As String
    Public MnuItem_4_Link As String
    Public MnuItem_5_Link As String
    Public MnuItem_6_Link As String
    Public MnuItem_7_Link As String
    Public MnuItem_8_Link As String

    'Menu Bar Text
    Public MnuItem_1_Text As String
    Public MnuItem_2_Text As String
    Public MnuItem_3_Text As String
    Public MnuItem_4_Text As String
    Public MnuItem_5_Text As String
    Public MnuItem_6_Text As String
    Public MnuItem_7_Text As String
    Public MnuItem_8_Text As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim objDAL As New clsDAL()
    Dim objDataRow As DataRow
    Dim objDataRowView As DataRowView
    Dim objDataTable As New DataTable()
    Dim objDataView As DataView

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim bytItem As Byte
        Dim objHyperlink As HyperLink

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
      
        'Initialize page depending on the session found.
        If Session("CompanyName") = "HOT TOPIC" Then
            ctlLogo.Show = Logo.LogoConstants.HotTopic
            lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet_Hottopic"))
            imgSiteMapArrow.ImageUrl = "Graphics/ArrowButtonSmall.gif"
            imgSearchArrow.ImageUrl = "Graphics/ArrowButtonSmall.gif"
            imgContactHRArrow.ImageUrl = "Graphics/ArrowButtonSmall.gif"
            imgSiteAdminArrow.ImageUrl = "Graphics/ArrowButtonSmall.gif"
            imgWANArrow.ImageUrl = "Graphics/ArrowButtonSmall.gif"
            lnkWAN.NavigateUrl = ConfigurationSettings.AppSettings("WAN_Hottopic")
			lnkWAN.Text = "Intranet"

            '    With hypWAN
            '        .ImageUrl = "Graphics\btnHottWANNew.GIF"
            '        .NavigateUrl = ConfigurationSettings.AppSettings("WAN_HotTopic")
            '        .Attributes.Add("onMouseOver", "this.firstChild.src='Graphics/btnHottWANOld.gif'")
            '        .Attributes.Add("onMouseOut", "this.firstChild.src='Graphics/btnHottWANNew.gif'")
            '    End With
        Else
            ctlLogo.Show = Logo.LogoConstants.Torrid
            lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet_Torrid"))
            imgSiteMapArrow.ImageUrl = "Graphics/ArrowButtonSmall_T.gif"
            imgSearchArrow.ImageUrl = "Graphics/ArrowButtonSmall_T.gif"
            imgContactHRArrow.ImageUrl = "Graphics/ArrowButtonSmall_T.gif"
            imgSiteAdminArrow.ImageUrl = "Graphics/ArrowButtonSmall_T.gif"
            imgWANArrow.ImageUrl = "Graphics/ArrowButtonSmall_T.gif"
            lnkWAN.NavigateUrl = ConfigurationSettings.AppSettings("WAN_Torrid")
			lnkWAN.Text = "Intranet"

            '    With hypWAN
            '        .ImageUrl = "Graphics\btnTorridWANNew.GIF"
            '        .NavigateUrl = ConfigurationSettings.AppSettings("WAN_Torrid")
            '        .Attributes.Add("onMouseOver", "this.firstChild.src='Graphics/btnTorridWANOld.gif'")
            '        .Attributes.Add("onMouseOut", "this.firstChild.src='Graphics/btnTorridWANNew.gif'")
            '    End With
        End If

        'Extract to DataTable values for the Menu Items.
        objDataTable = objDAL.SelectContentSection(, "Sequence")

        For bytItem = 1 To 8
            Select Case bytItem
                Case 1
                    'assign the hyperlink object to local hyperlink
                    objHyperlink = cmdSection1
                Case 2
                    objHyperlink = cmdSection2
                Case 3
                    objHyperlink = cmdSection3
                Case 4
                    objHyperlink = cmdSection4
                Case 5
                    objHyperlink = cmdSection5
                Case 6
                    objHyperlink = cmdSection6
                Case 7
                    objHyperlink = cmdSection7
                Case 8
                    objHyperlink = cmdSection8
            End Select

            With objHyperlink
                .Visible = False

                For Each objDataRow In objDataTable.Rows
                    If objDataRow("Sequence") = bytItem Then
                        .Text = Replace(objDataRow("DisplayName") & "", " ", "&nbsp;")
                        .NavigateUrl = "HRHome.aspx?Section=" & objDataRow("csSectionID")
                        '.Visible = True

                        Select Case bytItem
                            Case 1
                                MnuItem_1_Link = objHyperlink.NavigateUrl.ToLower
                                MnuItem_1_Text = objHyperlink.Text
                            Case 2
                                MnuItem_2_Link = objHyperlink.NavigateUrl.ToLower
                                MnuItem_2_Text = objHyperlink.Text
                            Case 3
                                MnuItem_3_Link = objHyperlink.NavigateUrl.ToLower
                                MnuItem_3_Text = objHyperlink.Text
                            Case 4
                                MnuItem_4_Link = objHyperlink.NavigateUrl.ToLower
                                MnuItem_4_Text = objHyperlink.Text
                            Case 5
                                MnuItem_5_Link = objHyperlink.NavigateUrl.ToLower
                                MnuItem_5_Text = objHyperlink.Text
                            Case 6
                                MnuItem_6_Link = objHyperlink.NavigateUrl.ToLower
                                MnuItem_6_Text = objHyperlink.Text
                            Case 7
                                MnuItem_7_Link = objHyperlink.NavigateUrl.ToLower
                                MnuItem_7_Text = objHyperlink.Text
                            Case 8
                                MnuItem_8_Link = objHyperlink.NavigateUrl.ToLower
                                MnuItem_8_Text = objHyperlink.Text
                        End Select

                        Exit For

                    End If
                Next
            End With
        Next

    End Sub


End Class
