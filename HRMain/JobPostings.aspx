<%@ Page Language="vb" AutoEventWireup="false" Codebehind="JobPostings.aspx.vb" Inherits="HRMain.frmJobPostings"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>JobPostings</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body leftMargin="0" rightMargin="0" topmargin="0" bottommargin="0">
		<form id="frmJobPostings" method="post" runat="server">
			<div class="container home" role="main">
			<iframe src="Header.aspx" frameBorder="no" width="100%" scrolling="no" height="75"></iframe>
			<hr />
			<table width="100%" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table cellSpacing="0" width="100%">
							<tr>
								<td align="center" class="HR_ThinTableOutline"><asp:image id="imgSection" runat="server" ImageAlign="Middle" EnableViewState="False" ImageUrl="Graphics/TitleGraphic2.gif"></asp:image></td>
							</tr>
							<tr>
								<td>
									<table width="100%">
										<tr>
											<td width="48%"><span class="HR_ArticleTitle">Location Search:</span></td>
											<td width="4%" rowSpan="2"></td>
											<td width="48%"><span class="HR_ArticleTitle">Department&nbsp;Search:</span></td>
										</tr>
										<tr>
											<td><asp:listbox id="lstLocation" runat="server" DataTextField="StateLocation" DataValueField="JobLocationID"
													SelectionMode="Multiple" Rows="5" Width="100%"></asp:listbox></td>
											<td><asp:listbox id="lstCategory" runat="server" DataTextField="jcDept" DataValueField="jcID" SelectionMode="Multiple"
													Rows="5" Width="100%"></asp:listbox></td>
										</tr>
										<tr>
											<td colSpan="3"><br /><span class="HR_ArticleTitle">Keyword Search:</span>
												<asp:textbox id="txtKeyword" runat="server" Width="30em" MaxLength="30"></asp:textbox></td>
										</tr>
										<tr>
											<td align="right" colSpan="3"><br />
												<asp:button CssClass="btn btn-danger" id="cmdSearch" runat="server" Text="Search for Jobs"></asp:button>&nbsp;&nbsp;
												<asp:button CssClass="btn btn-danger" id="cmdClear" runat="server" Text="Clear Search"></asp:button>&nbsp;&nbsp;
												<asp:button CssClass="btn btn-danger" id="btnShowAll" runat="server" Text="Show All Jobs"></asp:button>
											</td>
										</tr>
										<tr>
											<td><asp:label id="lblJobsFound" runat="server" CssClass="HR_ArticleTitle">Jobs Found: ##</asp:label></td>
											<td align="right" colSpan="2"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<asp:datalist id="lstSections" runat="server" EnableViewState="False" Width="100%">
										<ItemTemplate>
											<span class='HR_Link2'><font size="larger">
													<%# Container.DataItem("jcDept") %>
												</font></span>
											<hr>
											<asp:DataList runat="server" datasource='<%# Container.DataItem.Row.GetChildRows("JobCatJobs") %>' Width="100%" ID="Datalist1" cssclass="table archtbl">
												<ItemStyle cssclass="archtbltr"></ItemStyle>
												<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
												<ItemTemplate>
													<table cellpadding="3" width="100%">
														<tr valign="top">
															<td colspan="1" nowrap>
																<span class="HR_ArticleBody">Open Date: <b>
																		<%# Format(Container.DataItem("OpenDate"), "M/d/yy") %>
																	</b></span>
															</td>
															<td align="center" colspan="1" nowrap>
																<span class="HR_ArticleBody">Apply by Date: <b>
																		<%# Format(Container.DataItem("ApplyByDate"), "M/d/yy") %>
																	</b></span>
															</td>
															<td align="right" colspan="1">
																<span class="HR_ArticleBody">Location: <b>
																		<%# Container.DataItem("State") %>
																		-
																		<%# Container.DataItem("Location") %>
																	</b></span>
															</td>
														</tr>
														<tr>
															<td colspan="3">
																<a  href='<%=ConfigurationSettings.AppSettings("Other_Docs")%>/<%# Container.DataItem("jpPath") %>'>
																	<span class="HR_Link"><font style="font-size:15px;">
																			<%# Container.DataItem("jpTitle") %>
																		</font></span></a>
															</td>
														</tr>
														<tr>
															<td class="HR_ArticleBody" colspan="3">
																<%# rtrim(Container.DataItem("jpShort")) %>
																...
															</td>
														</tr>
														<tr>
															<td colspan="1" nowrap>
																<a class="HR_Link2" href='<%=ConfigurationSettings.AppSettings("Other_Docs")%>/<%# Container.DataItem("jpPath") %>'>
																	Click here for detail </a>
															</td>
															<td align="right" colspan="2">
																<span class="HR_ArticleBody">To apply contact: </span><a class="HR_Link2" href='mailto:<%# Container.DataItem("Contact") %>?subject=Job Posting -- <%# Container.DataItem("jpTitle") %>, <%# Container.DataItem("jcDept") %>'>
																	<%# Container.DataItem("Contact") %>
																</a>
															</td>
														</tr>
													</table>
												</ItemTemplate>
											</asp:DataList>
											<hr>
										</ItemTemplate>
									</asp:datalist>
								</td>
							</tr>
							<tr>
								<td align="center">
									<INPUT class="btn btn-danger" onclick="window.history.back()" type="button" value="Back">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
