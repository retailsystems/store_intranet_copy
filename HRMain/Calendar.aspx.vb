Option Explicit On 

Partial Class frmCalendar

    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Dim objDAL As New clsDAL()
    Dim objDataTable As New DataTable()
    Private objHistory As New clsHistory
    Dim objPageStyleSheet As New TPageStyleSheet
    Dim objUtilSessionHist As New TSessionHistory
    Dim sessionCompName As String
    Dim objPageSession As New TPageSession

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim dteEnd As Date
        Dim dteSelected As Date
        Dim dteStart As Date
        Dim intItem As Integer
        Dim intMonth As Integer
        Dim intStart As Integer
        Dim intYear As Integer
        Dim strDate As String

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        objUtilSessionHist.SetSessionHist(Me, objHistory, "History")

        If Session("CompanyName") Is Nothing Then
            objPageSession.GetSession(Me)
        End If

        sessionCompName = Convert.ToString(Session("CompanyName"))

        lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))


        If Not IsPostBack Then

            Try
                If InStr(Request.UrlReferrer.AbsoluteUri, "ViewTraining.aspx", CompareMethod.Text) = 0 Then
                    Session("Back") = Request.UrlReferrer.AbsoluteUri
                End If
            Catch
                Session("Back") = "HRHome.aspx"
            End Try

            intStart = Month(Now)

            With cboDates.Items
                .Add(New ListItem("Next 5 Weeks", Format(Now, "M/d/yy")))

                For intItem = intStart + 1 To intStart + 6
                    If intItem > 12 Then
                        intMonth = intItem - 12
                        intYear = Year(Now) + 1
                    Else
                        intMonth = intItem
                        intYear = Year(Now)
                    End If
                    .Add(New ListItem(MonthName(intMonth) & " " & intYear, intMonth & "/1/" & intYear))
                Next
            End With
        End If

        dteSelected = DateValue(cboDates.SelectedItem.Value)
        dteStart = dteSelected.AddDays(-dteSelected.DayOfWeek)
        dteEnd = dteStart.AddDays(34)

        DataList1.DataSource = objDAL.SelectTrainingDate(dteStart, dteEnd)
        DataList1.DataBind()

    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click

        Response.Redirect(Session("Back"))

    End Sub

    Private Sub DataList1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataList1.SelectedIndexChanged

    End Sub
End Class
