<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ContactInfo.aspx.vb" Inherits="HRMain.frmContactInfo"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ContactInfo</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body leftMargin="0" rightMargin="0" topmargin="0" bottommargin="0">
		<form id="frmContactInfo" method="post" runat="server">
			<div class="container home" role="main">
			<iframe src="Header.aspx" frameBorder="no" width="100%" scrolling="no" height="75"></iframe>
			<hr />
			<table width="100%" border="0">
				<tr>
					<td vAlign="bottom"><font>Contact Information</font></td>
					<td rowspan="4" vAlign="top">
						<asp:Image id="imgDepartment" runat="server" ImageUrl="Graphics\spacer.gif" Visible="false"></asp:Image>
						<br>
						<asp:Label id="lblCaption" runat="server" CssClass="ArticleBody" EnableViewState="False">Caption</asp:Label>
					</td>
				</tr>
				<tr>
					<td vAlign="bottom" height="30">
						<asp:Label id="lblDepartment" runat="server" CssClass="ArticleTitle" EnableViewState="False">Department</asp:Label>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td vAlign="top">
						<asp:DataList id="lstContacts" runat="server" Width="100%" EnableViewState="False">
							<ItemTemplate>
								<span class="HR_ArticleTitle">
									<%# Container.DataItem("chName") %>
									, </span><a href='mailto:<%# Container.DataItem("chEmail") %>'>
									<%# Container.DataItem("chEmail") %>
								</a>
								<br>
								<span class="HR_ArticleBody">
									<%# Container.DataItem("chDesc") %>
									, x<%# Container.DataItem("chPhone") %>
								</span>
								<br>
								<br>
							</ItemTemplate>
						</asp:DataList>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<INPUT class="btn btn-danger" onclick="window.history.back()" type="button" value="Back">
					</td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
