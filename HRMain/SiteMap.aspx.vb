Option Explicit On 

Partial Class frmSiteMap

    Inherits System.Web.UI.Page

    'Protected WithEvents imgSection As System.Web.UI.WebControls.Image

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim mlngID As Long
    Dim objDAL As New clsDAL()
    Dim objDataTable As New DataTable()
    Private objHistory As New clsHistory
    Dim objUtilSessionHist As New TSessionHistory
    Dim objPageStyleSheet As New TPageStyleSheet
    Dim sessionCompName As String
    Dim objPageSession As New TPageSession

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        objUtilSessionHist.SetSessionHist(Me, objHistory, "History")

        If Session("CompanyName") Is Nothing Then
            objPageSession.GetSession(Me)
        End If

        sessionCompName = Convert.ToString(Session("CompanyName"))
        lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))

        lblSiteMap.Text = HRMain.SectionName("Site Map")

        lstSections.DataSource = objDAL.SelectContentSectionContentHeader(Session("User/HRMAINManager") = "Y")
        lstSections.DataBind()

    End Sub

    Private Sub lstSections_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles lstSections.ItemDataBound

        Dim objLabel As Label

        Try
            objLabel = e.Item.FindControl("lblSection")
            objLabel.Text = HRMain.SectionName(e.Item.DataItem("DisplayName"))
        Catch
        End Try

    End Sub

End Class
