Option Explicit On 

Partial Class frmContactHR

    Inherits System.Web.UI.Page


    Private objHistory As New clsHistory
    Dim objUtilSessionHist As New TSessionHistory
    Dim objPageStyleSheet As New TPageStyleSheet


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim objDAL As New clsDAL()
    Dim objDataRowView As DataRowView
    Dim objDataView As DataView
    Dim sessionCompName As String
    Dim sm As String
    Protected strCompImageandExtension As String
    Dim objPageSession As New TPageSession

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        objUtilSessionHist.SetSessionHist(Me, objHistory, "History")

        If Session("CompanyName") Is Nothing Then
            objPageSession.GetSession(Me)
            'Response.Write("2-inside session sub" & Session("CompanyName"))
        End If

        sessionCompName = Convert.ToString(Session("CompanyName"))
        lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))
        If sessionCompName = "HOT TOPIC" Then
            strCompImageandExtension = ".gif"
            imgContactHR.ImageUrl = "Graphics/HotTopicAndTorridLogo.gif"
        Else
            strCompImageandExtension = "_T" & ".gif"
            imgContactHR.ImageUrl = "Graphics/HotTopicAndTorridLogo_T.gif"
        End If

        objDataView = New DataView(objDAL.SelectContactGroup)
        objDataView.RowFilter = "MainGraphic = 1"

        If objDataView.Count > 0 Then
            objDataRowView = objDataView(0)
            'imgContactHR.ImageUrl = objDataRowView("cgGraphicLg")

            lblCaption.Text = objDataRowView("Caption")
        Else
            lblCaption.Visible = False
        End If

        objDataView.RowFilter = "MainGraphic = 0"
        objDataView.Sort = "cgGroup"

        lstContacts.DataSource = objDataView
        lstContacts.DataBind()

    End Sub

End Class
