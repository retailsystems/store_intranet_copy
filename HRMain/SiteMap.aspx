<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SiteMap.aspx.vb" Inherits="HRMain.frmSiteMap"%>
<!DOCTYPE html>
<HTML>
	<HEAD>
		<title>SiteMap</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body leftMargin="0" rightMargin="0" topmargin="0" bottommargin="0">
		<form id="frmSiteMap" method="post" runat="server">
			<div class="container home" role="main">
			<iframe src="Header.aspx" frameBorder="no" width="100%" scrolling="no" height="75"></iframe>
			<hr />
			<table width="100%" border="0">
				<tr>
					<td><asp:Label id="lblSiteMap" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="12pt">SiteMap</asp:Label></td>
				</tr>
				<tr>
					<td><asp:datalist id="lstSections" runat="server" Width="100%" EnableViewState="False">
							<ItemTemplate>
								<br>
								<asp:Label id="lblSection" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="12pt"></asp:Label>&nbsp;
								<asp:DataList id="DataList1" runat="server" Width="100%" datasource='<%# Container.DataItem.Row.GetChildRows("SectionHeader") %>'>
									<ItemTemplate>
										<span class='HR_ArticleTitle'>
											<a class="HR_Link" href='ViewArticle.aspx?ID=<%# Container.DataItem("ID") %>'><%# Container.DataItem("Content_Title") %></a>
										</span>
										<span class='HR_Link3'>(Updated: <%# Format(Container.DataItem("Datestamp"), "M/d/yy") %>)</span>
									</ItemTemplate>
								</asp:DataList>
							</ItemTemplate>
						</asp:datalist></td>
				</tr>
				<tr>
					<td align="center">
						<INPUT class="btn btn-danger" onclick="window.history.back()" type="button" value="Back">
					</td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
