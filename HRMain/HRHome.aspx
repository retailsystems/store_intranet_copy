<%@ Page Language="vb" AutoEventWireup="false" Codebehind="HRHome.aspx.vb" Inherits="HRMain.frmHRHome" %>
<!DOCTYPE html>
<HTML>
	<HEAD>
		<title>HRHome</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body class="HRMain" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmHRHome" method="post" runat="server">
			<div class="container home" role="main">
			<iframe src="Header.aspx" frameBorder="no" width="100%" scrolling="no" height="75"></iframe>
			<hr />
			<table cellSpacing="0" width="100%" border="0">
				<tr>
					<td align="left">
						<asp:image id="imgSection" runat="server" EnableViewState="False" ImageAlign="Left" ImageUrl="Graphics/TitleGraphic2.gif"></asp:image>
						<asp:label id="lblSection" runat="server" CssClass="h3">Section</asp:label>
					</td>
				</tr>
				<tr>
					<td><br>
						<asp:label id="lblIntro" runat="server" EnableViewState="False" CssClass="HR_ArticleBody">Intro</asp:label><br>
						<br>
					</td>
				</tr>
				<tr>
					<td>
						<asp:datalist id="lstArticles" runat="server" EnableViewState="False" Width="100%">
							<ItemTemplate>
								<IMG src='<%# iif(len(Container.DataItem("GraphicLg")) > 4, Container.DataItem("GraphicLg"), "./Graphics/spacer.gif") %>'><BR>
								<!--<A class="HR_Link" href='ViewArticle.aspx?ID=<%# Container.DataItem("ID") %>'><IMG src='<%# Container.DataItem("Graphic")%>' align=absMiddle border=0> -->
								<A class="HR_Link" href='ViewArticle.aspx?ID=<%# Container.DataItem("ID") %>'><IMG src='<%# (Container.DataItem("Graphic")).Substring(0,22) & strCompImageandExtension%>' align=absMiddle border=0>
									<%# Container.DataItem("Content_Title") %>
								</A><SPAN class="HR_Link3">(Updated:
									<%# DataBinder.Eval(Container.DataItem, "Datestamp", "{0:M/d/yy}") %>
									) </SPAN>
								<BR>
								<SPAN class="HR_ArticleBody">
									<%# Container.DataItem("Content_Short") %>
								</SPAN><A class="HR_Link2" href='ViewArticle.aspx?ID=<%# Container.DataItem("ID") %>'>
									<%# Container.DataItem("More_Link") %>
								</A>
								<BR>
								<BR>
							</ItemTemplate>
						</asp:datalist>
					</td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
