Public Class TPageStyleSheet

    Public Function GetStyleSheet(ByVal CompanyName As String)
        Dim strStyle As String
        If CompanyName = "HOT TOPIC" Then
            strStyle = ConfigurationSettings.AppSettings("Stylesheet_Hottopic")
        Else
            strStyle = ConfigurationSettings.AppSettings("Stylesheet_Torrid")

        End If
        Return strStyle
    End Function

End Class
