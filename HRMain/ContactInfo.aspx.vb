Option Explicit On 

Partial Class frmContactInfo

    Inherits System.Web.UI.Page



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim mlngID As Long
    Dim objDAL As New clsDAL()
    Dim objDataRow As DataRow
    Dim objDataTable As New DataTable()
    Private objHistory As New clsHistory
    Dim objUtilSessionHist As New TSessionHistory
    Dim objPageStyleSheet As New TPageStyleSheet
    Dim sessionCompName As String
    Dim objPageSession As New TPageSession

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        objUtilSessionHist.SetSessionHist(Me, objHistory, "History")

        If Session("CompanyName") Is Nothing Then
            objPageSession.GetSession(Me)
        End If

        sessionCompName = Convert.ToString(Session("CompanyName"))
        lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))

        Dim strImage As String

        mlngID = Request("ID")

        If mlngID > 0 Then
            objDataTable = objDAL.SelectContactDetail(mlngID)

            If objDataTable.Rows.Count > 0 Then
                objDataRow = objDataTable.Rows(0)
                'strImage = objDataRow("cgGraphicLg")
                If sessionCompName = "HOT TOPIC" Then
                    strImage = "Graphics/HotTopicAndTorridLogo.gif"
                Else
                    strImage = "Graphics/HotTopicAndTorridLogo_T.gif"
                End If


                If strImage <> "0" Then
                    imgDepartment.ImageUrl = strImage
                End If

                lblDepartment.Text = objDataRow("cgGroup") & " Department"
                lblCaption.Text = objDataRow("Caption") & ""
            Else
                lblCaption.Text = ""
            End If

            lstContacts.DataSource = objDataTable
            lstContacts.DataBind()
        Else
            Response.Redirect("ContactHR.aspx")
        End If
    End Sub

End Class
