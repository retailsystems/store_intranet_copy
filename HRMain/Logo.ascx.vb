Option Explicit On 

Partial  Class Logo

    Inherits System.Web.UI.UserControl


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public Enum LogoConstants
        Both
        HotTopic
        Neither
        Torrid
    End Enum

    Public Property Show() As LogoConstants

        Get

        End Get

        Set(ByVal vstrShow As LogoConstants)

            hypHotTopic.Visible = ((vstrShow = LogoConstants.HotTopic) Or (vstrShow = LogoConstants.Both))
            hypTorrid.Visible = ((vstrShow = LogoConstants.Torrid) Or (vstrShow = LogoConstants.Both))

        End Set

    End Property

End Class
