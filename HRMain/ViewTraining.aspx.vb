Option Explicit On 

Partial Class frmViewTraining

    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Dim mlngID As Long
    Dim objDAL As New clsDAL()
    Dim objDataRow As DataRow
    Dim objDataTable As New DataTable
    Private objHistory As New clsHistory
    Dim objUtilSessionHist As New TSessionHistory
    Dim objPageStyleSheet As New TPageStyleSheet
    Dim objPageSession As New TPageSession
    Dim sessionCompName As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        objUtilSessionHist.SetSessionHist(Me, objHistory, "History")

        If Session("CompanyName") Is Nothing Then
            objPageSession.GetSession(Me)
        End If

        sessionCompName = Convert.ToString(Session("CompanyName"))

        lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))

        mlngID = Request("ID")

        If mlngID > 0 Then
            objDataTable = objDAL.SelectTrainingClass(mlngID)

            If objDataTable.Rows.Count > 0 Then
                objDataRow = objDataTable.Rows(0)
                lblTrainingClass.Text = objDataRow("TrainingClass")
                lblDescription.Text = objDataRow("Description")

                If IsDBNull(objDataRow("ClassFull")) Then
                    lstTrainingDates.Visible = False
                Else
                    lblNone.Visible = False
                    lstTrainingDates.DataSource = objDataTable
                    lstTrainingDates.DataBind()
                End If
            End If
        Else
            Response.Redirect("Calendar.aspx")
        End If

      

    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect(Request.UrlReferrer.AbsoluteUri)
    End Sub

End Class
