Public Class TPageSession
    Dim mstrStoreNo As String
    Dim objDataTable As New DataTable
    Dim objDAL As New clsDAL
    Dim mblnHotTopic As Boolean

    Public Sub GetSession(ByRef objPage As System.Web.UI.Page)

        Dim blnDefaultHotTopic As Boolean

        blnDefaultHotTopic = (InStr(HttpRuntime.AppDomainAppPath, "Torrid", CompareMethod.Text) = 0)

        If Not IsNothing(objPage.Request.Cookies("User")) Then
            mstrStoreNo = objPage.Request.Cookies("User")("Store")
        ElseIf Not IsNothing(objPage.Request.Cookies("StoreNo")) Then
            mstrStoreNo = objPage.Request.Cookies("StoreNo").Value
        End If

        If mstrStoreNo > "" Then
            'Check Hot Topic
            objDataTable = objDAL.SelectStore(True, mstrStoreNo)

            If objDataTable.Rows.Count = 0 Then
                'Check Torrid
                objDataTable = objDAL.SelectStore(False, mstrStoreNo)

                If objDataTable.Rows.Count = 0 Then
                    'Invalid Store No
                    mblnHotTopic = blnDefaultHotTopic

                End If
            Else
                mblnHotTopic = True
            End If
        Else
            'No Store No
            mblnHotTopic = blnDefaultHotTopic
        End If

        objPage.Session("CompanyName") = IIf(mblnHotTopic, "HOT TOPIC", "TORRID")


    End Sub
End Class
