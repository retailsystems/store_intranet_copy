<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ContactHR.aspx.vb" Inherits="HRMain.frmContactHR"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ContactHR</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK id="lnkStyles" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body leftMargin="0" rightMargin="0" topmargin="0" bottommargin="0">
		<form id="frmContactHR" method="post" runat="server">
			<div class="container home" role="main">
			<iframe src="Header.aspx" frameBorder="no" width="100%" scrolling="no" height="75"></iframe>
			<hr />
			<table width="100%" border="0">
				<tr>
					<td><font class="HR_DarkShade_Font">Contact HR</font></td>
					<td rowspan="3" vAlign="top">
						<asp:Image id="imgContactHR" runat="server" Visible="false"></asp:Image>
						<br>
						<asp:Label id="lblCaption" runat="server" CssClass="ArticleBody" EnableViewState="False">Caption</asp:Label>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<asp:DataList id="lstContacts" runat="server" EnableViewState="False" RepeatColumns="2" Width="100%">
							<ItemTemplate>
								<span class='HR_ArticleTitle'><a class="HR_Link" href='ContactInfo.aspx?ID=<%# Container.DataItem("cgID") %>'>
										<!--<img src='Graphics/<%# Container.DataItem("cgGraphic") %>' align='absmiddle' border='0'> -->
										<img src='Graphics/<%# (Container.DataItem("cgGraphic")).Substring(0,11) & strCompImageandExtension %>' align='absmiddle' border='0'>
										<%# Container.DataItem("cgGroup") %>
									</a></span>
								<br>
								<span class='HR_ArticleBody'>
									<%# Container.DataItem("cgGroupText") %>
								</span>
								<br>
								<br>
							</ItemTemplate>
						</asp:DataList>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<INPUT class="btn btn-danger" onclick="window.history.back()" type="button" value="Back">
					</td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
