<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Login.aspx.vb" Inherits="HRMain.frmLogin"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Login</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body leftMargin="0" rightMargin="0" topmargin="0" bottommargin="0">
		<form id="frmLogin" method="post" runat="server">
			<div class="container home" role="main">
			<iframe src="Header.aspx?ID=Login" frameBorder="no" width="100%" scrolling="no" height="75"></iframe>
			<hr />
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td align="middle" colspan="4">
						<asp:label CssClass="HR_ArticleBody" id="lblError" runat="server">The "My Management Tools" section is not available presently. Please click [Home] to return to the HR Home page.</asp:label>
					</td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr class="input-group">
					<td width="30%"></td>
					<td class="input-group-addon"><font><b>Login ID</b></font></td>
					<td><asp:textbox id="txtLoginID" runat="server" MaxLength="8" class="form-control"></asp:textbox></td>
					<td width="30%"></td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr class="input-group">
					<td width="30%"></td>
					<td class="input-group-addon"><font><b>Password</b></font></td>
					<td><asp:textbox id="txtPassword" runat="server" TextMode="Password" MaxLength="8" class="form-control"></asp:textbox></td>
					<td width="30%"></td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td width="30%"></td>
					<td align="right" colspan="2">
						<asp:button CssClass="btn btn-danger" id="cmdSubmit" runat="server" Text="Submit"></asp:button>&nbsp;&nbsp;
						<asp:button CssClass="btn btn-danger" id="cmdHome" runat="server" Text="Home"></asp:button>
					</td>
					<td width="30%"></td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
