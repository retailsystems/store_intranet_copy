Option Explicit On 

Partial Class frmSidebar

    Inherits System.Web.UI.Page

    Protected WithEvents cmdLoginHere As System.Web.UI.WebControls.HyperLink
    Protected WithEvents cmdHotTopicWAN As System.Web.UI.WebControls.HyperLink
    Dim objPageStyleSheet As New TPageStyleSheet
    Dim objUtilSessionHist As New TSessionHistory
    Dim sessionCompName As String
    Dim objPageSession As New TPageSession
    Private objHistory As New clsHistory


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        objUtilSessionHist.SetSessionHist(Me, objHistory, "History")
        If Session("CompanyName") Is Nothing Then
            objPageSession.GetSession(Me)
        End If
        sessionCompName = Convert.ToString(Session("CompanyName"))
        lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))
      
    End Sub

End Class
