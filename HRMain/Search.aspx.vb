Option Explicit On 

Partial Class frmSearch

    Inherits System.Web.UI.Page





#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim objDAL As New clsDAL()
    Dim objDataRow As DataRow
    Dim objDataTable As New DataTable()
    Dim objDataView As New DataView()
    Dim mstrSearch As String
    Private objHistory As New clsHistory
    Dim objUtilSessionHist As New TSessionHistory
    Dim objPageStyleSheet As New TPageStyleSheet
    Dim sessionCompName As String
    Dim objPageSession As New TPageSession

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        objUtilSessionHist.SetSessionHist(Me, objHistory, "History")
        If Session("CompanyName") Is Nothing Then
            objPageSession.GetSession(Me)
        End If
        sessionCompName = Convert.ToString(Session("CompanyName"))
        lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))

        mstrSearch = Request("txtSearchStr")
        lblResults.Visible = False

        If IsPostBack Or mstrSearch > "" Then
            If Not IsPostBack And mstrSearch > "" Then
                txtSearch.Text = mstrSearch
            End If

            objDataTable = objDAL.SelectContentHeader(, , txtSearch.Text, chkTitle.Checked, chkContent.Checked, , (Session("User/HRMAINManager") = "Y"))

            lblResults.Text = "Search Results for '" & txtSearch.Text & "' = " & objDataTable.Rows.Count & " articles"
            lblResults.Visible = True

            objDataView = New DataView(objDataTable)
            objDataView.Sort = "Content_Title"

            lstArticles.DataSource = objDataView
            lstArticles.DataBind()
        End If

    End Sub

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click

    End Sub
End Class
