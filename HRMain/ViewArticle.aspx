<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ViewArticle.aspx.vb" Inherits="HRMain.frmViewArticle" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ViewArticle</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body leftMargin="0" rightMargin="0" topmargin="0" bottommargin="0">
		<form id="frmViewArticle" method="post" runat="server">
			<div class="container home" role="main">
			<iframe id="iframeHeader" src="Header.aspx" frameborder="no" scrolling="no" width="100%" height="75"></iframe>
			<hr />
			<table width="100%">
				<tr>
					<td><asp:image id="imgHeader" runat="server" EnableViewState="False"></asp:image></td>
				</tr>
				<tr>
					<td><asp:label id="lblTitle" runat="server" CssClass="HR_ArticleTitle" EnableViewState="False"><span class='ArticleTitle'>Title</span></asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="lblDate" runat="server" CssClass="HR_ArticleBody" EnableViewState="False">Date</asp:label>
						<hr>
					</td>
				</tr>
				<tr>
					<td class="HR_ArticleBody">
						<%call mShowContent%>
					</td>
				</tr>
				<tr>
					<!--<td align="middle"><%=sessionCompName%><BR>-->
					<td align="center"><BR>
						<BR>
						<!--<asp:Button id="cmdSwitch" runat="server" Text="Switch HT/TD" CssClass="btn btn-danger"></asp:Button>-->
						<!--<asp:Button Runat="server" CssClass="HR_Button" Text="Back" ID="btnBack"></asp:Button>-->
						<INPUT onclick="window.history.back()" type="button" value="Back" Class="btn btn-danger">
					</td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
