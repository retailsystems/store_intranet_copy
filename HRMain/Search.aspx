<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Search.aspx.vb" Inherits="HRMain.frmSearch" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Search</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK id="lnkStyles" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body leftMargin="0" rightMargin="0" topmargin="0" bottommargin="0">
		<form id="frmSearch" method="post" runat="server">
			<div class="container home" role="main">
			<iframe src="Header.aspx" frameborder="no" scrolling="no" width="100%" height="75"></iframe>
			<hr />
			<table width="100%">
				<tr>
					<td>
						<br>
						<font class="HR_DarkShade_Font"><b>Search:</b></font>
						<br>
						<br>
						<asp:textbox id="txtSearch" Runat="server" Width="300px" MaxLength="255"></asp:textbox>&nbsp;&nbsp;
						<asp:button class="btn btn-danger" id="cmdSearch" runat="server" Text="Search"></asp:button></td>
				</tr>
				<tr>
					<td><asp:CheckBox id="chkTitle" runat="server" Text="Search in Title" Checked="True"></asp:CheckBox></td>
				</tr>
				<tr>
					<td><asp:CheckBox id="chkContent" runat="server" Text="Search in Content" Checked="True"></asp:CheckBox></td>
				</tr>
				<tr>
					<td><asp:Label id="lblResults" runat="server" Font-Italic="True" Font-Size="Larger" EnableViewState="False">Search Results for ''</asp:Label></td>
				</tr>
				<tr>
					<td>
						<hr class="Dark_Shade">
					</td>
				</tr>
				<tr>
					<td>
						<asp:DataList id="lstArticles" runat="server" Width="100%" EnableViewState="False">
							<ItemTemplate>
								<!--
								<img src='<%# iif(len(Container.DataItem("GraphicLg")) > 4, Container.DataItem("GraphicLg"), "./Graphics/spacer.gif") %>'><br>
								-->
								<a class="hr_link2" href='ViewArticle.aspx?ID=<%# Container.DataItem("ID") %>'>
									<!--
								<img src='<%# Container.DataItem("Graphic") %>' align='absmiddle' border='0'>
								-->
									<span class='HR_ArticleTitle'>
										<%# Container.DataItem("Content_Title") %>
									</span></a><span class='HR_Link3'>(Updated:
									<%# DataBinder.Eval(Container.DataItem, "Datestamp", "{0:M/d/yy}") %>
									) </span>
								<br>
								<span class='HR_ArticleBody'>
									<%# Container.DataItem("Content_Short") %>
								</span><span class='HR_Link2'><a href='ViewArticle.aspx?ID=<%# Container.DataItem("ID") %>'>
										<%# Container.DataItem("More_Link") %>
									</a></span>
								<br>
								<br>
							</ItemTemplate>
						</asp:DataList>
					</td>
				</tr>
				<tr>
					<td align="center">
						<INPUT class="btn btn-danger" onclick="window.history.back()" type="button" value="Back">
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
