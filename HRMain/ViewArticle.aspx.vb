Option Explicit On 

Partial Class frmViewArticle

    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim mblnHotTopic As Boolean
    Dim mlngID As Long
    Dim objDAL As New clsDAL()
    Dim objDataRow As DataRow
    Dim objDataTable As New DataTable()
    Dim objItems As New DataTable()
    Private objHistory As New clsHistory
    Dim objUtilSessionHist As New TSessionHistory
    Dim objPageStyleSheet As New TPageStyleSheet
    Protected sessionCompName As String
    Dim mstrStoreNo As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim blnDefaultHotTopic As Boolean
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        sessionCompName = Convert.ToString(Session("CompanyName"))
        objUtilSessionHist.SetSessionHist(Me, objHistory, "History")

        Select Case sessionCompName
            Case "HOT TOPIC", "TORRID"
                lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))

            Case Else
                blnDefaultHotTopic = (InStr(HttpRuntime.AppDomainAppPath, "Torrid", CompareMethod.Text) = 0)

                If Not IsNothing(Request.Cookies("User")) Then
                    mstrStoreNo = Request.Cookies("User")("Store")
                ElseIf Not IsNothing(Request.Cookies("StoreNo")) Then
                    mstrStoreNo = Request.Cookies("StoreNo").Value
                End If

                If mstrStoreNo > "" Then
                    'Check Hot Topic
                    objDataTable = objDAL.SelectStore(True, mstrStoreNo)

                    If objDataTable.Rows.Count = 0 Then
                        'Check Torrid
                        objDataTable = objDAL.SelectStore(False, mstrStoreNo)

                        If objDataTable.Rows.Count = 0 Then
                            'Invalid Store No
                            mblnHotTopic = blnDefaultHotTopic

                        End If
                    Else
                        mblnHotTopic = True
                    End If
                Else
                    'No Store No
                    mblnHotTopic = blnDefaultHotTopic
                End If

                Session("CompanyName") = IIf(mblnHotTopic, "HOT TOPIC", "TORRID")
                sessionCompName = Convert.ToString(Session("CompanyName"))
                lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))

        End Select

        mlngID = Val(Request("ID"))

        If mlngID > 0 Then
            'Initialize page

            mblnHotTopic = (Session("CompanyName") = "HOT TOPIC")

            objDataTable = objDAL.SelectContentHeader(mlngID)

            If objDataTable.Rows.Count > 0 Then
                objDataRow = objDataTable.Rows(0)

                If Len(objDataRow("GraphicLg")) > 4 Then
                    imgHeader.ImageUrl = objDataRow("GraphicLg")
                Else
                    imgHeader.Visible = False
                End If

                lblTitle.Text = objDataRow("Content_Title")
                lblDate.Text = Format(CDate(objDataRow("Datestamp")), "M/d/yy")
            End If

            objItems = objDAL.SelectContentDetail(mlngID)
            cmdSwitch.Text = "View " & IIf(mblnHotTopic, "Torrid", "Hot Topic") & " version"
        Else
            Response.Redirect("HRHome.aspx")
        End If


    End Sub

    Public Sub mShowContent()

        Dim blnDifferentVersions As Boolean
        Dim blnShowBoth As Boolean
        Dim strLink As String
        Dim strPath As String
        Dim strText As String

        With Response
            For Each objDataRow In objItems.Rows
                .Write("<p style='MARGIN-TOP: 8px; MARGIN-BOTTOM: 8px'>")

                Select Case objDataRow("ItemType")
                    Case "Story Text"
                        strText = "<SPAN Class='HR_ArticleBody'>" & objDataRow("ItemText") & "</SPAN>"
                        strText = Replace(strText, Chr(13) & Chr(10), "<br>")

                        .Write(strText)

                    Case "Other Link"
                        If mblnHotTopic Then
                            If objDataRow("ItemLink") > "" Then
                                .Write("<a class='HR_Link2' href='" & objDataRow("ItemLink") & "'>")

                                If objDataRow("ItemText") > "" Then
                                    .Write(objDataRow("ItemText"))
                                Else
                                    .Write("Click Here")
                                End If

                                .Write("</a>")
                            End If
                        Else
                            If objDataRow("ItemLink2") > "" Then
                                .Write("<a class='hr_link2' href='" & objDataRow("ItemLink2") & "'>")

                                If objDataRow("ItemText") > "" Then
                                    .Write(objDataRow("ItemText"))
                                Else
                                    .Write("Click Here")
                                End If

                                .Write("</a>")
                            End If
                        End If

                        If objDataRow("ItemLink") <> objDataRow("ItemLink2") Then
                            blnDifferentVersions = True
                        End If

                    Case "Story Link"
                        If objDataRow("ItemLink") > "" Then
                            objDataTable = objDAL.SelectContentHeader(objDataRow("ItemLink"))

                            .Write("<a class='hr_link2' href='ViewArticle.aspx?ID=" & objDataRow("ItemLink") & "'>")

                            If objDataRow("ItemText") > "" Then
                                .Write(objDataRow("ItemText"))
                            Else
                                .Write(objDataTable.Rows(0)("Content_Title"))
                            End If

                            .Write("</a>")

                            If objDataRow("Show") And objDataTable.Rows(0)("Content_Short") > "" Then
                                .Write("<br>")
                                .Write(objDataTable.Rows(0)("Content_Short"))
                            End If
                        End If

                    Case "Train Link"
                        If objDataRow("ItemLink") > "" Then
                            objDataTable = objDAL.SelectTrainingClass(objDataRow("ItemLink"))

                            .Write("<a class='hr_link2' href='ViewTraining.aspx?ID=" & objDataRow("ItemLink") & "'>")

                            If objDataRow("ItemText") > "" Then
                                .Write(objDataRow("ItemText"))
                            Else
                                If objDataTable.Rows.Count > 0 Then
                                    .Write(objDataTable.Rows(0)("TrainingClass"))
                                End If
                            End If

                            .Write("</a>")

                            If objDataTable.Rows.Count > 0 Then
                                If objDataTable.Rows(0)("Description") > "" And objDataRow("Show") Then
                                    .Write("<br>")
                                    .Write(objDataTable.Rows(0)("Description"))
                                End If
                            End If
                        End If

                    Case "Image"
                        If objDataRow("ItemLink") > "" Then
                            .Write("<img src='" & objDataRow("ItemLink") & "'>")
                        End If

                    Case "Forms Link"
                        blnShowBoth = objDataRow("Show")
                        strPath = "NOT FOUND"

                        If (blnShowBoth Or mblnHotTopic) And Val(objDataRow("ItemLink") & "") > 0 Then
                            strPath = ConfigurationSettings.AppSettings("Forms_HotTopic")
                            objDataTable = objDAL.SelectEForms(True, Val(objDataRow("ItemLink")))

                            If objDataTable.Rows.Count > 0 And strPath <> "NOT FOUND" Then
                                strLink = "Form_" & objDataTable.Rows(0)("Form_ID") & ".pdf"

                                If blnShowBoth Then
                                    .Write("Hot Topic: ")
                                End If

                                .Write("<a class='hr_link2' href='" & strPath & "\" & strLink & "' target='_blank'>")

                                If objDataRow("ItemText") > "" Then
                                    .Write(objDataRow("ItemText"))
                                Else
                                    .Write(objDataTable.Rows(0)("Form_Title"))
                                End If

                                .Write("</a>")
                            End If
                        End If

                        'Showing Both, start a new line
                        If blnShowBoth And Val(objDataRow("ItemLink") & "") > 0 And Val(objDataRow("ItemLink2") & "") > 0 Then
                            .Write("<br>")
                        End If

                        If (blnShowBoth Or Not mblnHotTopic) And Val(objDataRow("ItemLink2") & "") > 0 Then
                            strPath = ConfigurationSettings.AppSettings("Forms_Torrid")
                            objDataTable = objDAL.SelectEForms(False, Val(objDataRow("ItemLink2")))

                            If objDataTable.Rows.Count > 0 And strPath <> "NOT FOUND" Then
                                strLink = "Form_" & objDataTable.Rows(0)("Form_ID") & ".pdf"

                                If blnShowBoth Then
                                    .Write("Torrid: ")
                                End If

                                .Write("<a class='hr_link2' href='" & strPath & "\" & strLink & "' target='_blank'>")

                                If objDataRow("ItemText") > "" Then
                                    .Write(objDataRow("ItemText"))
                                Else
                                    .Write(objDataTable.Rows(0)("Form_Title"))
                                End If

                                .Write("</a>")
                            End If
                        End If

                        If Not blnShowBoth Then
                            blnDifferentVersions = True
                        End If

                    Case "SOP Link"
                        blnShowBoth = objDataRow("Show")

                        If (blnShowBoth Or mblnHotTopic) And Val(objDataRow("ItemLink") & "") > 0 Then
                            If Val(objDataRow("ItemLink")) > 0 Then
                                objDataTable = objDAL.SelectSOPDtl(True, objDataRow("ItemLink"))

                                'Add ".pdf" to the end if needed
                                strLink = objDataRow("ItemLink") & IIf(LCase(Right(objDataRow("ItemLink"), 4)) = ".pdf", "", ".pdf")
                                strPath = ConfigurationSettings.AppSettings("SOP_HotTopic")

                                If blnShowBoth Then
                                    .Write("Hot Topic: ")
                                End If

                                .Write("<a class='hr_link2' href='" & strPath & "\" & strLink & "' target='_blank'>")

                                If objDataRow("ItemText") > "" Then
                                    .Write(objDataRow("ItemText"))
                                ElseIf objDataTable.Rows.Count > 0 Then
                                    .Write(objDataTable.Rows(0)("SOP_Title"))
                                Else
                                    .Write("SOP " & objDataRow("ItemLink"))
                                End If

                                .Write("</a>")
                            End If
                        End If

                        'Showing Both, start a new line
                        If blnShowBoth And Val(objDataRow("ItemLink") & "") > 0 And Val(objDataRow("ItemLink2") & "") > 0 Then
                            .Write("<br>")
                        End If

                        If (blnShowBoth Or Not mblnHotTopic) And Val(objDataRow("ItemLink2") & "") > 0 Then
                            If Val(objDataRow("ItemLink2")) > 0 Then
                                objDataTable = objDAL.SelectSOPDtl(False, objDataRow("ItemLink2"))

                                'Add ".pdf" to the end if needed
                                strLink = objDataRow("ItemLink2") & IIf(LCase(Right(objDataRow("ItemLink2"), 4)) = ".pdf", "", ".pdf")
                                strPath = ConfigurationSettings.AppSettings("SOP_Torrid")

                                If blnShowBoth Then
                                    .Write("Torrid: ")
                                End If

                                .Write("<a class='hr_link2' href='" & strPath & "\" & strLink & "' target='_blank'>")

                                If objDataRow("ItemText") > "" Then
                                    .Write(objDataRow("ItemText"))
                                ElseIf objDataTable.Rows.Count > 0 Then
                                    .Write(objDataTable.Rows(0)("SOP_Title"))
                                Else
                                    .Write("SOP " & objDataRow("ItemLink2"))
                                End If

                                .Write("</a>")
                            End If
                        End If

                        If Not blnShowBoth Then
                            blnDifferentVersions = True
                        End If

                    Case "Other Docs"
                        strPath = ConfigurationSettings.AppSettings("Other_Docs")
                        strLink = objDataRow("ItemLink")

                        If objDataRow("ItemLink") > "" Then
                            .Write("<a class='hr_link2' href='" & strPath & "\" & strLink & "' target='_blank'>")

                            If objDataRow("ItemText") > "" Then
                                .Write(objDataRow("ItemText"))
                            Else
                                .Write(strLink)
                            End If

                            .Write("</a>")
                        End If

                    Case "Email Link"
                        If objDataRow("ItemLink") > "" Then
                            .Write("<a class='HR_Link2' href='mailto:" & objDataRow("ItemLink") & "'>")

                            If objDataRow("ItemText") > "" Then
                                .Write(objDataRow("ItemText"))
                            Else
                                .Write(objDataRow("ItemLink"))
                            End If

                            .Write("</a>")
                        End If

                End Select

                .Write("</p>")
            Next
        End With

        cmdSwitch.Visible = blnDifferentVersions

    End Sub

    Private Sub cmdSwitch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSwitch.Click

        Session("CompanyName") = IIf(Session("CompanyName") = "HOT TOPIC", "TORRID", "HOT TOPIC")


        'If Session("CompanyName") = "HOT TOPIC" Then
        '    lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet_Hottopic"))
        'Else
        '    lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet_Torrid"))
        'End If
        sessionCompName = Convert.ToString(Session("CompanyName"))
        lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))

        mblnHotTopic = Not mblnHotTopic
        cmdSwitch.Text = "View " & IIf(mblnHotTopic, "Torrid", "Hot Topic") & " version"

    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect(objHistory.GetPrev(Request.Url.AbsoluteUri))
    End Sub
End Class
