Option Explicit On 

Public Class frmHRHome

    Inherits System.Web.UI.Page

    Protected WithEvents lstArticles As System.Web.UI.WebControls.DataList
    Protected WithEvents lblIntro As System.Web.UI.WebControls.Label
    Protected WithEvents HyperLink1 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents HyperLink2 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblSection As System.Web.UI.WebControls.Label
    Protected WithEvents imgSection As System.Web.UI.WebControls.Image
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim mblnHotTopic As Boolean
    Dim mbytSection As Byte
    Dim mstrSection As String
    Dim mstrStoreNo As String
    Dim objDAL As New clsDAL()
    Dim objDataRow As DataRow
    Dim objDataRowView As DataRowView
    Dim objDataTable As New DataTable()
    Dim objDataView As DataView
    Dim objHistory As New clsHistory
    Dim objUtilSessionHist As New TSessionHistory
    Dim objPageStyleSheet As New TPageStyleSheet
    Protected sessionCompName As String
    Protected strCompImageandExtension As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim blnDefaultHotTopic As Boolean
        Dim dteJobsUpdated As Date
        Dim Company As String
        'Dim objHistory As New clsHistory

        sessionCompName = Convert.ToString(Session("CompanyName"))

        objUtilSessionHist.SetSessionHist(Me, objHistory, "History")

        Select Case sessionCompName
            Case "HOT TOPIC", "TORRID"
                lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))

            Case Else
                blnDefaultHotTopic = (InStr(HttpRuntime.AppDomainAppPath, "Torrid", CompareMethod.Text) = 0)
                If Not IsNothing(Request.Cookies("User")) Then
                    mstrStoreNo = Request.Cookies("User")("Store")
                ElseIf Not IsNothing(Request.Cookies("StoreNo")) Then
                    mstrStoreNo = Request.Cookies("StoreNo").Value
                End If

                If mstrStoreNo > "" Then
                    'Check Hot Topic
                    objDataTable = objDAL.SelectStore(True, mstrStoreNo)

                    If objDataTable.Rows.Count = 0 Then
                        'Check Torrid
                        objDataTable = objDAL.SelectStore(False, mstrStoreNo)

                        If objDataTable.Rows.Count = 0 Then
                            'Invalid Store No
                            mblnHotTopic = blnDefaultHotTopic

                        End If

                    Else
                        mblnHotTopic = True
                    End If
                Else
                    'No Store No

                    mblnHotTopic = blnDefaultHotTopic

                End If

                Session("CompanyName") = IIf(mblnHotTopic, "HOT TOPIC", "TORRID")

                sessionCompName = Convert.ToString(Session("CompanyName"))
                lnkStyles.Attributes.Add("href", objPageStyleSheet.GetStyleSheet(sessionCompName))

        End Select

        mbytSection = Val(Request("Section"))

        If sessionCompName = "HOT TOPIC" Then
            strCompImageandExtension = ".gif"
        Else
            strCompImageandExtension = "_T" & ".gif"
        End If

        If Not IsPostBack Then
            'Initialize page
            objDataTable = objDAL.SelectContentSection(mbytSection)
            objDataRow = objDataTable.Rows(0)

            If mbytSection = 0 Then
                If Session("CompanyName") = "HOT TOPIC" Then
                    imgSection.ImageUrl = objDataRow("Target")
                Else
                    imgSection.ImageUrl = Replace(objDataRow("Target"), ".", "_T.")
                End If

                imgSection.Visible = True
                lblSection.Text = ""
            Else
                imgSection.Visible = False
                lblSection.Text = HRMain.SectionName(objDataRow("DisplayName") & "")
            End If


            lblIntro.Text = objDataRow("Intro")

            If mbytSection > 0 Then
                mstrSection = objDataRow("DisplayName")
                imgSection.ImageAlign = ImageAlign.Left

                If InStr(mstrSection, "Manage", CompareMethod.Text) > 0 And Session("User/HRMAINManager") <> "Y" Then
                    Response.Redirect("Login.aspx?Section=" & mbytSection)
                End If
            End If
        End If

        'Extract the Data required for the Articles
        objDataTable = objDAL.SelectContentHeader(, mstrSection)
        objDataView = New DataView(objDataTable)

        With objDataView
            If mbytSection = 0 Then
                .RowFilter = "TopStory = 1"
                .Sort = "Category_Code"
            Else
                .RowFilter = "SubStory = 0"
                .Sort = "GroupType_Code"
            End If
        End With

        'Response.Write("0" & sessionCompName)

        lstArticles.DataSource = objDataView

        lstArticles.DataBind()


    End Sub

    Private Sub lstArticles_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstArticles.SelectedIndexChanged

    End Sub
End Class
