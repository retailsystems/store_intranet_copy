Option Explicit On 

Imports System.Data.SqlClient

Public Class clsDAL

    Dim blnScript As Boolean
    Private objSqlCommand As SqlCommand
    Private mstrDatabase As String

    Private Function cnnConnection() As SqlConnection

        cnnConnection = New SqlConnection(ConfigurationSettings.AppSettings(ConfigurationSettings.AppSettings("Database_" & mstrDatabase)))

    End Function

    Private Function Execute() As String

        Dim intRowsAffected As Integer

        Try
            With objSqlCommand
                .Connection = cnnConnection()
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
            End With
        Catch
            Return "Error #" & Err.Number & ": " & Err.Description
        End Try

    End Function

    Private Function GetDataTable() As DataTable

        Dim objSqlDataAdapter As SqlDataAdapter
        Dim objDataTable As New DataTable()

        objSqlCommand.Connection = cnnConnection()

        objSqlDataAdapter = New SqlDataAdapter(objSqlCommand)
        objSqlDataAdapter.Fill(objDataTable)

        Return objDataTable

    End Function

    Public Function SelectContactDetail(ByVal vlngID As Long) As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = "HR"

        With objSqlCommand
            .CommandText = "spContactDetailSelect"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@cgID", vlngID)
        End With

        Return GetDataTable()

    End Function

    Public Function SelectContactGroup() As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = "HR"

        With objSqlCommand
            .CommandText = "spContactGroupSelect"
            .CommandType = CommandType.StoredProcedure
        End With

        Return GetDataTable()

    End Function

    Public Function SelectContentDetail(ByVal vlngContentHeaderID As Long) As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = "HR"

        With objSqlCommand
            .CommandText = "spContentDetailSelect"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@ContentHeaderID", vlngContentHeaderID)
        End With

        Return GetDataTable()

    End Function

    Public Function SelectContentHeader(Optional ByVal vlngID As Long = 0, Optional ByVal vstrDisplayName As String = "", Optional ByVal vstrSearch As String = "", Optional ByVal vblnTitle As Boolean = False, Optional ByVal vblnContent As Boolean = False, Optional ByVal vblnNoSubStories As Boolean = False, Optional ByVal vblnManager As Boolean = False) As DataTable

        Dim strSQL As String

        objSqlCommand = New SqlCommand()
        mstrDatabase = "HR"

        strSQL = "SELECT DISTINCT CH.*, CS.DisplayName, CONVERT(VARCHAR(10), CH.Datestamp, 111) AS DateOnly, LEFT(CH.Content_Short, 100) AS Content_Shorter, CH.Content_Title + ' (' + CS.DisplayName + ')' AS TitleAndSection"
        strSQL += " FROM Content_Header CH"
        strSQL += " INNER JOIN Content_Section CS ON CH.Group_Code = CS.csSectionID"
        strSQL += " INNER JOIN Content_Detail CD ON CH.ID = CD.ContentHeaderID"
        strSQL += " WHERE Deleted = 0"

        If vlngID > 0 Then
            strSQL += " AND ID = " & vlngID
        End If

        If vstrDisplayName > "" Then
            strSQL += " AND CS.DisplayName = '" & Replace(vstrDisplayName, "'", "''") & "'"
        End If

        If vblnTitle And vblnContent Then
            strSQL += " AND (CH.Content_Title LIKE '%" & Replace(vstrSearch, "'", "''") & "%'"
            strSQL += " OR CH.Content_Short LIKE '%" & Replace(vstrSearch, "'", "''") & "%'"
            strSQL += " OR CD.ItemText LIKE '%" & Replace(vstrSearch, "'", "''") & "%')"
        ElseIf vblnTitle Then
            strSQL += " AND CH.Content_Title LIKE '%" & Replace(vstrSearch, "'", "''") & "%'"
        ElseIf vblnContent Then
            strSQL += " AND (CH.Content_Short LIKE '%" & Replace(vstrSearch, "'", "''") & "%'"
            strSQL += " OR CD.ItemText LIKE '%" & Replace(vstrSearch, "'", "''") & "%')"
        End If

        If Not vblnManager Then
            strSQL += " AND NOT CS.csSection LIKE '%Manage%'"
        End If

        If vblnNoSubStories Then
            strSQL += " AND CH.SubStory = 0"
        End If

        With objSqlCommand
            .CommandText = strSQL
            .CommandType = CommandType.Text
        End With

        Return GetDataTable()

    End Function

    Public Function SelectContentSection(Optional ByVal vlngSectionID As Long = 0, Optional ByVal vstrOrderBy As String = "", Optional ByVal vblnGreaterThan As Boolean = False, Optional ByVal vblnManager As Boolean = False) As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = "HR"

        With objSqlCommand
            .CommandText = "spContentSectionSelect"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@SectionID", vlngSectionID)
            .Parameters.Add("@OrderBy", vstrOrderBy)
            .Parameters.Add("@GreaterThan", IIf(vblnGreaterThan, 1, 0))
            .Parameters.Add("@Manager", IIf(vblnManager, 1, 0))
        End With

        Return GetDataTable()

    End Function

    Public Function SelectContentSectionContentHeader(Optional ByVal vblnManager As Boolean = False) As DataTable

        Dim objDataSet As New DataSet()
        Dim objDataTable As DataTable

        mstrDatabase = "HR"

        objDataTable = SelectContentSection(, "Sequence", True, vblnManager)
        objDataTable.TableName = "Sections"
        objDataSet.Tables.Add(objDataTable)

        objDataTable = SelectContentHeader(, , , , , True, vblnManager)
        objDataTable.TableName = "Articles"
        objDataSet.Tables.Add(objDataTable)

        objDataSet.Relations.Add("SectionHeader", objDataSet.Tables("Sections").Columns("csSectionID"), objDataSet.Tables("Articles").Columns("Group_Code"))

        Return objDataSet.Tables("Sections")

    End Function

    Public Function SelectEForms(ByVal vblnHotTopic As Boolean, ByVal vlngFormID As String) As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = "Zombie_" & IIf(vblnHotTopic, "HotTopic", "Torrid")

        With objSqlCommand
            .CommandText = "spEFormsSelect"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@FormID", vlngFormID)
        End With

        Return GetDataTable()

    End Function

    Public Function SelectJobCat(Optional ByVal vblnExists As Boolean = False, Optional ByVal vblnCurrent As Boolean = False) As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = "HR"

        With objSqlCommand
            .CommandText = "spJobCatSelect"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@Exists", IIf(vblnExists, 1, 0))
            .Parameters.Add("@Current", IIf(vblnCurrent, 1, 0))
        End With

        Return GetDataTable()

    End Function

    Public Function SelectJobCatJobs(ByVal vstrLocations As String, ByVal vstrCategories As String, ByVal vstrKeyword As String, ByRef rintJobsFound As Integer) As DataTable

        Dim objDataSet As New DataSet()
        Dim objSqlDataAdapter As SqlDataAdapter
        Dim objSqlDataAdapter2 As SqlDataAdapter
        Dim strSQL As String

        mstrDatabase = "HR"

        strSQL = "SELECT DISTINCT JC.* FROM Job_Cat JC"
        strSQL += " INNER JOIN Jobs J ON JC.jcID = J.jpDeptID"
        strSQL += " WHERE J.ApplyByDate >= GETDATE()"
        strSQL += " AND (J.jpTitle LIKE '%" & Replace(vstrKeyword, "'", "''") & "%'"
        strSQL += " OR J.jpShort LIKE '%" & Replace(vstrKeyword, "'", "''") & "%')"

        If vstrLocations > "" Then
            strSQL += " AND J.JobLocationID IN (0" & vstrLocations & ")"
        End If

        If vstrCategories > "" Then
            strSQL += " AND JC.jcID IN (0" & vstrCategories & ")"
        End If

        strSQL += " ORDER BY JC.jcDept"

        objSqlDataAdapter = New SqlDataAdapter(strSQL, cnnConnection)
        objSqlDataAdapter.Fill(objDataSet, "Job_Cat")

        strSQL = "SELECT J.*, JL.State, JL.Location, JC.jcDept"
        strSQL += " FROM Jobs J"
        strSQL += " LEFT JOIN Job_Location JL ON J.JobLocationID = JL.JobLocationID"
        strSQL += " LEFT JOIN Job_Cat JC ON J.jpDeptID = JC.jcID"
        strSQL += " WHERE J.ApplyByDate >= GETDATE()"

        If vstrLocations > "" Then
            strSQL += " AND J.JobLocationID IN (0" & vstrLocations & ")"
        End If

        If vstrCategories > "" Then
            strSQL += " AND J.jpDeptID IN (0" & vstrCategories & ")"
        End If

        If vstrKeyword > "" Then
            strSQL += " AND (J.jpTitle LIKE '%" & Replace(vstrKeyword, "'", "''") & "%'"
            strSQL += " OR J.jpShort LIKE '%" & Replace(vstrKeyword, "'", "''") & "%')"
        End If

        strSQL += " ORDER BY J.ApplyByDate DESC, J.jpTitle"

        objSqlDataAdapter2 = New SqlDataAdapter(strSQL, cnnConnection)
        objSqlDataAdapter2.Fill(objDataSet, "Jobs")

        objDataSet.Relations.Add("JobCatJobs", objDataSet.Tables("Job_Cat").Columns("jcID"), objDataSet.Tables("Jobs").Columns("jpDeptID"))

        rintJobsFound = objDataSet.Tables("Jobs").Rows.Count

        Return objDataSet.Tables("Job_Cat")

    End Function

    Public Function SelectJobLocation(Optional ByVal vblnExists As Boolean = False, Optional ByVal vblnCurrent As Boolean = False) As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = "HR"

        With objSqlCommand
            .CommandText = "spJobLocationSelect"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@Exists", IIf(vblnExists, 1, 0))
            .Parameters.Add("@Current", IIf(vblnCurrent, 1, 0))
        End With

        Return GetDataTable()

    End Function

    Public Function SelectSecurity(ByVal vblnHotTopic As Boolean, Optional ByVal vstrLoginID As String = "", Optional ByVal vstrJobClass As String = "") As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = IIf(vblnHotTopic, "HotTopic", "Torrid")

        With objSqlCommand
            .CommandText = "spSecuritySelect"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@UserID", vstrLoginID)
            .Parameters.Add("@JobClass", vstrJobClass)
        End With

        Return GetDataTable()

    End Function

    Public Function SelectSOPDtl(ByVal vblnHotTopic As Boolean, Optional ByVal vstrSOPNo As String = "") As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = "Zombie_" & IIf(vblnHotTopic, "HotTopic", "Torrid")

        With objSqlCommand
            .CommandText = "spSOPDtlSelect"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@SOPNo", vstrSOPNo)
        End With

        Return GetDataTable()

    End Function

    Public Function SelectSQL(ByVal vstrSQL As String) As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = "HR"

        With objSqlCommand
            .CommandText = vstrSQL
            .CommandType = CommandType.Text
        End With

        Return GetDataTable()

    End Function

    Public Function SelectStore(ByVal vblnHotTopic As Boolean, ByVal vstrStoreNo As String) As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = IIf(vblnHotTopic, "HotTopic", "Torrid")

        With objSqlCommand
            .CommandText = "spStoreSelect"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@StoreNum", vstrStoreNo)
        End With

        Return GetDataTable()

    End Function

    Public Function SelectTrainingClass(ByVal vlngTrainingClassID As Long) As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = "HR"

        With objSqlCommand
            .CommandText = "spTrainingClassSelect"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@TrainingClassID", vlngTrainingClassID)
        End With

        Return GetDataTable()

    End Function

    Public Function SelectTrainingDate(ByVal vdteStartDate As Date, ByVal vdteEndDate As Date) As DataTable

        Dim dteDateValue As Date
        Dim objDataColumn As DataColumn
        Dim objDataRow As DataRow
        Dim objDataSet As New DataSet()
        Dim objDataTable As New DataTable()
        Dim objSqlDataAdapter As SqlDataAdapter
        Dim strSQL As String

        mstrDatabase = "HR"

        strSQL = "SELECT TD.TrainingClassID, CONVERT(VARCHAR(8), TD.TrainingDate, 1) AS TrainingDate, TD.TrainingDate AS TrainingTime, TD.EndTime, TD.ClassFull, TC.TrainingClass"
        strSQL += " FROM Training_Date TD"
        strSQL += " INNER JOIN Training_Class TC ON TD.TrainingClassID = TC.TrainingClassID"
        strSQL += " WHERE TD.TrainingDate BETWEEN '" & vdteStartDate & "' AND '" & vdteEndDate & "'"

        objSqlDataAdapter = New SqlDataAdapter(strSQL, cnnConnection)
        objSqlDataAdapter.Fill(objDataSet, "TrainingClass")

        objDataTable.TableName = "TrainingDate"
        objDataTable.Columns.Add("TrainingDate", objDataSet.Tables("TrainingClass").Columns("TrainingDate").DataType)
        dteDateValue = vdteStartDate

        Do Until dteDateValue > vdteEndDate
            objDataRow = objDataTable.NewRow
            objDataRow("TrainingDate") = Format(dteDateValue, "MM/dd/yy")
            objDataTable.Rows.Add(objDataRow)
            dteDateValue = dteDateValue.AddDays(1)
        Loop

        objDataSet.Tables.Add(objDataTable)

        objDataSet.Relations.Add("Training", objDataSet.Tables("TrainingDate").Columns("TrainingDate"), objDataSet.Tables("TrainingClass").Columns("TrainingDate"))

        Return objDataSet.Tables("TrainingDate")

    End Function

    Public Function SelectUserMst(ByVal vblnHotTopic As Boolean, ByVal vstrLoginID As String, ByVal vstrPassword As String) As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = IIf(vblnHotTopic, "HotTopic", "Torrid")

        With objSqlCommand
            .CommandText = "spUserMstSelect"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@UserLogin", vstrLoginID)
            .Parameters.Add("@Password", vstrPassword)
        End With

        Return GetDataTable()

    End Function

    Public Sub TextboxButton(ByVal objTextBox As TextBox, ByVal objButton As Control)

        objTextBox.Attributes.Add("TargetButton", objButton.ID)

        Call ButtonScript(objTextBox.Page)

    End Sub

    Private Sub ButtonScript(ByVal objPage As Page)

        Dim objControl As Control
        Dim objHtmlForm As HtmlForm

        If Not blnScript Then
            For Each objControl In objPage.Controls
                If TypeName(objControl) = "HtmlForm" Then
                    objHtmlForm = objControl
                    objHtmlForm.Attributes.Add("onkeydown", "CatchKeyPress(window.event.keyCode, window.event.srcElement);")
                End If
            Next

            With objPage.Response
                .Write("<script language='javascript'> " & vbCrLf)
                .Write("function CatchKeyPress(KeyCode, Sender) { " & vbCrLf)
                .Write("var btnToBeClicked = null; " & vbCrLf)
                .Write("RemoveEnterAndEscEvents(); " & vbCrLf)
                .Write("if(KeyCode == '13') { " & vbCrLf)
                .Write("var ButtonName = Sender.getAttribute('TargetButton'); " & vbCrLf)
                .Write("btnToBeClicked = document.getElementById(ButtonName); " & vbCrLf)
                .Write("if(btnToBeClicked) { " & vbCrLf)
                .Write("btnToBeClicked.click(); " & vbCrLf)
                .Write("} " & vbCrLf)

                .Write("else {" & vbCrLf)
                .Write("Sender = document.getElementById('" & objHtmlForm.ID & "'); " & vbCrLf)
                .Write("ButtonName = Sender.getAttribute('TargetButton'); " & vbCrLf)
                .Write("btnToBeClicked = document.getElementById(ButtonName); " & vbCrLf)
                .Write("if(btnToBeClicked) { " & vbCrLf)
                .Write("btnToBeClicked.click(); " & vbCrLf)
                .Write("} " & vbCrLf)
                .Write("} " & vbCrLf)

                .Write("} " & vbCrLf)
                .Write("} " & vbCrLf)

                .Write("function RemoveEnterAndEscEvents() { " & vbCrLf)
                .Write("if (event.keyCode == 13 || event.keyCode == 27) { " & vbCrLf)
                .Write("event.cancelBubble = true; event.returnValue = false; " & vbCrLf)
                .Write("} " & vbCrLf)
                .Write("} " & vbCrLf)
                .Write("</script> " & vbCrLf)
            End With

            blnScript = True
        End If

    End Sub

End Class
