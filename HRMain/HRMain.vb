Option Explicit On 

Module HRMain

    Public Sub gMsgBox(ByVal objPage As Page, ByVal strMessage As String)

        Dim s As String = "<SCRIPT language='javascript'>alert(""" & strMessage & """);</SCRIPT>"

        objPage.RegisterStartupScript("alert", s)

    End Sub

    Public Sub gSetFocus(ByVal objControl As System.Web.UI.Control)

        objControl.Page.RegisterStartupScript("focus", "<SCRIPT language='javascript'>document.getElementById('" & objControl.ID & "').focus() </SCRIPT>")

    End Sub

    Public Sub LocationOnClick(ByVal objButton As Button, ByVal strLocation As String)

        objButton.Attributes.Add("OnClick", "javascript:{location.href('" & strLocation & "');return false;}")

    End Sub

    Public Function SectionName(ByVal strSection As String) As String

        Dim blnRed As Boolean
        Dim bytChar As Byte
        Dim strChar As String

        strSection = Replace(strSection, " ", "")

        For bytChar = 1 To Len(strSection)
            strChar = Mid(strSection, bytChar, 1)

            If Asc(strChar) <= Asc("Z") Then
                blnRed = Not blnRed

                If bytChar > 1 Then
                    strSection &= "</font>"
                End If

                SectionName &= "<font class='" & IIf(blnRed, "HR_SectionOn", "HR_SectionOff") & "'>"
            End If

            SectionName &= strChar
        Next

        SectionName &= "</font>"

    End Function

End Module
