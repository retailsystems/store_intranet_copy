<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Header.aspx.vb" Inherits="HRMain.frmHeader" %>
<%@ Register TagPrefix="uc1" TagName="Logo" Src="Logo.ascx" %>
<!DOCTYPE html>
<HTML>
	<HEAD>
		<title>Header</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body class="header" leftMargin="0" rightMargin="0">
		<form id="frmHeader"  method="post" runat="server">
			<table width="100%">
				<tr vAlign="bottom">
					<td vAlign="top"><uc1:logo id="ctlLogo" runat="server"></uc1:logo></td>
					<td vAlign="top" align="right">
						<table cellSpacing="0" cellPadding="0">
							<tr>
								<td class="HR_HeaderButton">
									<asp:Image runat="server" id="imgSiteMapArrow" ImageUrl="Graphics/ArrowButtonSmall.gif"></asp:Image>
									<a class="HR_Link" Target="_top" href="SiteMap.aspx">SiteMap</a>
								</td>
								<td class="HR_HeaderButton">
									<asp:Image runat="server" id="imgSearchArrow" ImageUrl="Graphics/ArrowButtonSmall.gif"></asp:Image>
									<a class="HR_Link" Target="_top" href="Search.aspx">Search</a>
								</td>
								<td class="HR_HeaderButton">
									<asp:Image runat="server" id="imgContactHRArrow" ImageUrl="Graphics/ArrowButtonSmall.gif"></asp:Image>
									<a class="HR_Link" Target="_top" href="ContactHR.aspx">Contact HR</a>
								</td>
								<td class="HR_HeaderButton">
									<asp:Image runat="server" id="imgSiteAdminArrow" ImageUrl="Graphics/ArrowButtonSmall.gif"></asp:Image>
									<a class="HR_Link" Target="_top" href="../HRAdmin/Login.aspx">Site Admin</a>
								</td>
								<td class="HR_HeaderButton">
									<asp:Image runat="server" id="imgWANArrow" ImageUrl="Graphics/ArrowButtonSmall.gif"></asp:Image>
									<asp:HyperLink cssclass="HR_Link" Target="_top" Runat="server" ID="lnkWAN" NavigateUrl=""></asp:HyperLink>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br />
			<table class="HR_DarkShade" cellPadding="1" width="100%">
				<tr>
					<td align="middle"><a class="HR_Link" href="<%=MnuItem_1_Link%>" target="_top"><font class="HR_SectionText"><%=MnuItem_1_Text%></font></a></td>
					<td align="middle"><a class="HR_Link" href="<%=MnuItem_2_Link%>" target="_top"><font class="HR_SectionText"><%=MnuItem_2_Text%></font></a></td>
					<td align="middle"><a class="HR_Link" href="<%=MnuItem_3_Link%>" target="_top"><font class="HR_SectionText"><%=MnuItem_3_Text%></font></a></td>
					<td align="middle"><a class="HR_Link" href="<%=MnuItem_4_Link%>" target="_top"><font class="HR_SectionText"><%=MnuItem_4_Text%></font></a></td>
					<td align="middle"><a class="HR_Link" href="<%=MnuItem_5_Link%>" target="_top"><font class="HR_SectionText"><%=MnuItem_5_Text%></font></a></td>
					<td align="middle"><a class="HR_Link" href="<%=MnuItem_6_Link%>" target="_top"><font class="HR_SectionText"><%=MnuItem_6_Text%></font></a></td>
					<td align="middle"><a class="HR_Link" href="<%=MnuItem_7_Link%>" target="_top"><font class="HR_SectionText"><%=MnuItem_7_Text%></font></a></td>
					<td align="middle"><a class="HR_Link" href="<%=MnuItem_8_Link%>" target="_top"><font class="HR_SectionText"><%=MnuItem_8_Text%></font></a></td>
					<td align="middle"><A class="HR_Link" href="HRHome.aspx" target="_top"><font class = "HR_SectionText">HR Home</font></A></td>
					<!-- These are rendered not visible -->
					<td align="middle"><asp:hyperlink id="cmdSection1" runat="server" EnableViewState="False" Target="_top" ForeColor="White" Font-Size="Smaller">Section 1</asp:hyperlink></td>
					<td align="middle"><asp:hyperlink id="cmdSection2" runat="server" EnableViewState="False" Target="_top" ForeColor="White" Font-Size="Smaller">Section 2</asp:hyperlink></td>
					<td align="middle"><asp:hyperlink id="cmdSection3" runat="server" EnableViewState="False" Target="_top" ForeColor="White" Font-Size="Smaller">Section 3</asp:hyperlink></td>
					<td align="middle"><asp:hyperlink id="cmdSection4" runat="server" EnableViewState="False" Target="_top" ForeColor="White" Font-Size="Smaller">Section 4</asp:hyperlink></td>
					<td align="middle"><asp:hyperlink id="cmdSection5" runat="server" EnableViewState="False" Target="_top" ForeColor="White" Font-Size="Smaller">Section 5</asp:hyperlink></td>
					<td align="middle"><asp:hyperlink id="cmdSection6" runat="server" EnableViewState="False" Target="_top" ForeColor="White" Font-Size="Smaller">Section 6</asp:hyperlink></td>
					<td align="middle"><asp:hyperlink id="cmdSection7" runat="server" EnableViewState="False" Target="_top" ForeColor="White" Font-Size="Smaller">Section 7</asp:hyperlink></td>
					<td align="middle"><asp:hyperlink id="cmdSection8" runat="server" EnableViewState="False" Target="_top" ForeColor="White" Font-Size="Smaller">Section 8</asp:hyperlink></td>
					<!-- This is hard coded -->
					<!-- <td align="middle"><asp:hyperlink id="cmdHome" runat="server" NavigateUrl="HRHome.aspx" Target="_top" EnableViewState="False" Font-Size="Smaller" ForeColor="White">HR Home</asp:hyperlink></td> --></tr>
			</table>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
