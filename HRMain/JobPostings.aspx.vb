Option Explicit On 

Public Class frmJobPostings

    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Private objHistory As New clsHistory()

    Dim objDAL As New clsDAL()
    Protected WithEvents lstSections As System.Web.UI.WebControls.DataList
    Protected WithEvents lstLocation As System.Web.UI.WebControls.ListBox
    Protected WithEvents lstCategory As System.Web.UI.WebControls.ListBox
    Protected WithEvents txtKeyword As System.Web.UI.WebControls.TextBox
    Protected WithEvents cmdSearch As System.Web.UI.WebControls.Button
    Protected WithEvents cmdClear As System.Web.UI.WebControls.Button
    Protected WithEvents btnShowAll As System.Web.UI.WebControls.Button
    Protected WithEvents lblJobsFound As System.Web.UI.WebControls.Label
    Protected WithEvents imgSection As System.Web.UI.WebControls.Image
    Dim objDataTable As New DataTable

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        If Not Session("History") Is Nothing Then objHistory = Session("History")
        objHistory.Add(Request.Url.AbsoluteUri)
        Session("History") = objHistory

        If Session("CompanyName") = "HOT TOPIC" Then
            lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet_Hottopic"))
            imgSection.ImageUrl = "Graphics/TitleGraphic2.gif"
        Else
            lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet_Torrid"))
            imgSection.ImageUrl = "Graphics/TitleGraphic2_T.gif"
        End If

        Call objDAL.TextboxButton(txtKeyword, cmdSearch)

        If Not IsPostBack Then
            lblJobsFound.Visible = False

            lstLocation.DataSource = objDAL.SelectJobLocation(, True)
            lstLocation.DataBind()

            lstCategory.DataSource = objDAL.SelectJobCat(, True)
            lstCategory.DataBind()
        End If

    End Sub

    Private Sub mPageLoad()

        Dim intJobsFound As Integer
        Dim objListItem As ListItem
        Dim strCategory As String
        Dim strLocation As String

        For Each objListItem In lstLocation.Items
            If objListItem.Selected Then
                strLocation += ", " & objListItem.Value
            End If
        Next

        For Each objListItem In lstCategory.Items
            If objListItem.Selected Then
                strCategory += ", " & objListItem.Value
            End If
        Next

        lstSections.DataSource = objDAL.SelectJobCatJobs(strLocation, strCategory, txtKeyword.Text, intJobsFound)
        lstSections.DataBind()
        lstSections.Visible = True

        lblJobsFound.Text = "Jobs Found: " & intJobsFound
        lblJobsFound.Visible = True

    End Sub

    Private Sub cmdClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lstLocation.ClearSelection()
        lstCategory.ClearSelection()
        txtKeyword.Text = ""
        'optBrief.Checked = True

        lblJobsFound.Visible = False
        lstSections.Visible = False

    End Sub

    Private Sub btnShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lstLocation.ClearSelection()
        lstCategory.ClearSelection()
        txtKeyword.Text = ""

        Call mPageLoad()

    End Sub

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Call mPageLoad()

    End Sub

End Class
