ECHO on
CLS

REM  0 is returned there is no error is detected.
REM  1 is returned there is an error is detected.
 

"C:\Program Files\WinSCP\Winscp.exe" /Console /script="C:\_ShopTrak\Script\SFTP_Labor_to_ShopperTrak.txt" 

ECHO Exit Code is %errorlevel%

IF %errorlevel% == 0 (EXIT /B 0) ELSE (EXIT /B 1)


ECHO ON
exit 

 