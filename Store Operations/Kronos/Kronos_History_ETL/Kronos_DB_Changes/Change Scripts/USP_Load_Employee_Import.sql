﻿USE [Kronos_History]
GO
/****** Object:  StoredProcedure [dbo].[USP_Load_Employee_Import]    Script Date: 06/20/2014 13:05:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[USP_Load_Employee_Import]
AS
BEGIN
	TRUNCATE TABLE dbo.Employee_Import;
	
	insert into dbo.Employee_Import
	select	EMPLOYEE as Employee_ID
			,FIRST_NAME as First_Name
			,SUBSTRING(LAST_NAME, 1, 20) as Last_Name
			,MIDDLE_INIT as Middle_Initial
			,NICK_NAME as Short_Name
			,DATE_HIRED as Hire_Date
			,BIRTHDATE as Birth_Date
			,JOB_CODE as Job_Title
			,EMP_STATUS as [Status]
			,ANNIVERS_DATE as Pay_Rule_Effectived_Date
			,ANNUAL_HOURS as Full_Time_Part_Time
			,SUBSTRING(LOCAT_CODE, 1, 4) as Location
			,SUBSTRING(PHONE, 1 , 14) as Primary_Cell
			,SUBSTRING(HM_PHONE_NBR, 1, 14) as Primary_Home
			,EMAIL_ADDRESS as Email
			,null as Report_To
			,BEG_DATE as Status_Date
			,ISNULL(A_FIELD, 'N') as Meal_Waiver
			,ADJ_HIRE_DATE as Rehire_Date
			,ANNIVERS_DATE as Position_Date
			,Round(PAY_RATE, 2) as Base_Wage
			,BASE_WAGE_EFFECTIVE_DATE as Base_Wage_Effective_Date
			,HM_DIST_CO as Company
			,PROCESS_LEVEL as User_Level
			,SUPERVISOR as Supervisor
			,null as Sub_Department
			,ANNIVERS_DATE as Primary_Account_Effective_Date
	from dbo.Lawson_Employee;
	
END

 