/*-----------------------------------------------------------------------*/
-- PROGRAM       Kronos_Employee_Extract.sql
-- USAGE         Kronos_Employee_Extract.sql
-- FUNCTION      Executes an Oracle PL/SQL stored procedure for
--		     	 Kronos data extract
-- CALLED BY     
-- AUTHOR        Christine P. Dao
-- Date          06/26/2014
-- Mods		 
/*-----------------------------------------------------------------------*/
set echo on

@@connect_KRONOS_EXTRACT.sql

-- set up error exit codes  first
WHENEVER SQLERROR EXIT 8
WHENEVER OSERROR EXIT 6

-- spool $ORASPOOL
spool kronos_employee_hist_extract_error.lst

Call EXTRACT_EMPLOYEE.SP_EMPLOYEE_HIST_OUTPUT();

set echo off

Commit;

spool off;

exit;

