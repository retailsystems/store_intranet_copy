#!/bin/ksh
#-----------------------------------------------------------------------
# PROGRAM       kronos_extracts.sh
# USAGE         kronos_extracts.sh
# FUNCTION      Multi function script that calls oracle 
#		packages and initiates ftp sessions for the following:
#			- Employee Extract and sftp
#			- Employee Accruals Extract and sftp

#
#		The script requires that the desired extract be 
#		provided as an input parameter.  Thus this script
#		runs for a single request set up as seperate jobs
#		in CONTROL-M
#	
#		Oracle Package
# CALLED BY     Scheduled in CONTROL-M Enterprise Scheduler
# RUNS AS	oracle
# USAGE		<script location>/kronos_extracts.sh <extract type> <DB Server> <wallet location> 
# AUTHOR        Christine P. Dao
# Date          06/26/2014
# Mods		
#-----------------------------------------------------------------------
# change directory to script location
cd `dirname $0`

echo "before environment"
echo "------------------------"

# set environment variables
export ORACLE_SID="$2"
export ORACLE_OWNER=oracle
export ORACLE_HOME=`grep "^$ORACLE_SID" /etc/oratab | awk -F: '{print $2}'`
export PATH=$ORACLE_HOME/bin:$PATH

######	KRONOS EMPLOYEE EXTRACT
##	Run this condition if passed parameter is EMPLOYEE_EXTRACT
if [ "$1" = 'EMPLOYEE_EXTRACT' ];then

	echo "before oracle connection"
	echo "------------------------"

	##Run sqlplus with no logon and input command file
	sqlplus /nolog @Kronos_Employee_Extract.sql

	STATUS=$?
	
	echo "-----------------------"
	echo "after oracle connection"
	echo "-----------------------"

	##Return error to Control M
	if [ $STATUS -ne 0 ]
	then
	    echo Kronos employee extract failed.
	    echo Return code = $STATUS
	    exit $STATUS
	fi
fi
######   END OF KRONOS EMPLOYEE EXTRACT

######	KRONOS EMPLOYEE HISTORY EXTRACT
##	Run this condition if passed parameter is EMPLOYEE_EXTRACT
if [ "$1" = 'EMPLOYEE_HIST_EXTRACT' ];then

	echo "before oracle connection"
	echo "------------------------"

	##Run sqlplus with no logon and input command file
	sqlplus /nolog @Kronos_Employee_Hist_Extract.sql

	STATUS=$?
	
	echo "-----------------------"
	echo "after oracle connection"
	echo "-----------------------"

	##Return error to Control M
	if [ $STATUS -ne 0 ]
	then
	    echo Kronos employee history extract failed.
	    echo Return code = $STATUS
	    exit $STATUS
	fi
fi
######   END OF KRONOS EMPLOYEE HISTORY EXTRACT

######	KRONOS EMPLOYEE ACRRUAL EXTRACT
##	Run this condition if passed parameter is EMPLOYEE_EXTRACT
if [ "$1" = 'EMPLOYEE_ACCR_EXTRACT' ];then

	echo "before oracle connection"
	echo "------------------------"

	##Run sqlplus with no logon and input command file
	sqlplus /nolog @Kronos_Employee_Accrual_Extract.sql
	
	STATUS=$?

	echo "-----------------------"
	echo "after oracle connection"
	echo "-----------------------"

	##Return error to Control M
	if [ $STATUS -ne 0 ]
	then
	    echo Kronos employee accrual extract failed.
	    echo Return code = $STATUS
	    exit $STATUS
	fi
fi
######   END OF KRONOS EMPLOYEE ACRRUAL EXTRACT
