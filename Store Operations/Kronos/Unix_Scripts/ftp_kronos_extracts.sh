#!/bin/ksh
#-----------------------------------------------------------------------
# PROGRAM       ftp_kronos_extract.sh
# USAGE         ftp_kronos_extract.sh
# FUNCTION      ftp Employee file
#		    	
# CALLED BY     Scheduled in CONTROL-M Enterprize Scheduler
# RUNS AS	oracle
# USAGE		<script location>/ftp_kronos_extracts.sh <local directory> <remote directory> <file name>
# AUTHOR        Christine P. Dao
# Date          06/27/2014
# Mods		
#-----------------------------------------------------------------------
##SFTP file to distination server
sftp kronos@caftp << EOCMDS
lcd "$1"
cd "$2"
put "$3"
EOCMDS

##Return error to Control M
STATUS=$?
if [ $STATUS -ne 0 ]
then
    echo archive kronos extract failed.
    echo Return code = $STATUS
    exit $STATUS
fi