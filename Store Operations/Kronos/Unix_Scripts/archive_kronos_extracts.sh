#!/bin/ksh
#-----------------------------------------------------------------------
# PROGRAM       archive_kronos_extracts.sh
# USAGE         archive_kronos_extracts.sh
# FUNCTION      Archive employee files and deletes files older than 15 days old
#		    	
# CALLED BY     Scheduled in CONTROL-M Enterprize Scheduler
# RUNS AS	oracle
# USAGE		<script location>/archive_rkonso_extracts.sh <file source> <file destination> <filename> <file extension>
# AUTHOR        Christine P. Dao
# Date          6/24/2013
# Mods		
#-----------------------------------------------------------------------

##Archive file with datetimestamp
mv "$1"/"$3"."$4" "$2"/"$3"_`date +"%Y%m%d%H%M%S"`."$4"

##Delete files older than 15 days
find "$2" -name ""$3"*."$4"" -mtime +15 -exec rm {} \;

##Return error to Control M
STATUS=$?
if [ $STATUS -ne 0 ]
then
    echo archive kronos extract failed.
    echo Return code = $STATUS
    exit $STATUS
fi