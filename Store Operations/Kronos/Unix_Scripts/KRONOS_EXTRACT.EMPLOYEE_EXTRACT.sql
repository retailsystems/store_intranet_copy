/*<TOAD_FILE_CHUNK>*/
CREATE OR REPLACE PACKAGE  KRONOS_EXTRACT.EXTRACT_EMPLOYEE AS
/******************************************************************************
   NAME:       	KRONOS_EXTRACT
   PURPOSE:	EXTRACT EMLOYEE DATA

   REVISIONS:
   Ver        Date        Author           	Description
   ---------  ----------  ---------------  	------------------------------------
   1.0        06/25/2014  Christine P. Dao    	1. Extract Employee Data.
******************************************************************************/


PROCEDURE SP_EMPLOYEE_OUTPUT;

PROCEDURE SP_EMPLOYEE_HIST_OUTPUT;

PROCEDURE SP_EMPLOYEE_ACCRUAL_OUTPUT;

END EXTRACT_EMPLOYEE;
/





/*<TOAD_FILE_CHUNK>*/

CREATE OR REPLACE PACKAGE BODY  KRONOS_EXTRACT.EXTRACT_EMPLOYEE AS

/******************************************************************************
   NAME:       KRONOS_EXTRACT
   PURPOSE:

   REVISIONS:
   Ver          Date            Author               Description
   ---------    ----------      ---------------     ------------------------------------
   1.0        06/25/2014  Christine P. Dao       1. Extract Employee Data
******************************************************************************/

PROCEDURE SP_EMPLOYEE_OUTPUT IS

	v_FileName 		VARCHAR(50);
	v_FileHandle    	UTL_FILE.FILE_TYPE;
	
    CURSOR CURR_EMPLOYEES IS
	
		select e.employee as EMPLOYEE_ID
                  ,trim(e.first_name) as FIRST_NAME
                  ,trim(e.last_name) as LAST_NAME
                  ,case when length(trim(case when e.middle_init = ' ' then null else e.middle_init end)) > 1 
                    then substr(trim(case when e.middle_init = ' ' then null else e.middle_init end), 1, 1) 
                    else trim(case when e.middle_init = ' ' then null else e.middle_init end) end as MIDDLE_INITIAL
                  ,case when length(trim(e.nick_name)) > 20 then substr(trim(e.nick_name), 1, 20) else trim(e.nick_name) end as SHORT_NAME
                  ,to_char(e.date_hired, 'mm/dd/yyyy')  as HIRE_DATE
                  ,to_char(e.birthdate, 'mm/dd/yyyy')  as BIRTH_DATE
                  ,trim(e.job_code) as JOB_TITLE
                  ,trim(e.emp_status) as STATUS
                  ,to_char(e.annivers_date, 'mm/dd/yyyy') as PAY_RULE_EFFECTIVE_DATE
                  ,trim(e.annual_hours) as FULL_TIME_PART_TIME
                  ,trim(e.department) as LOCATION
                  ,trim(e.phone) as PRIMARY_CELL
                  ,trim(e.hm_phone_nbr) as PRIMARY_HOME
                  ,case when length(trim(e.email_address)) > 50 then substr(trim(e.email_address), 1, 50) end as EMAIL
                  ,to_char(e.beg_date, 'mm/dd/yyyy') as STATUS_DATE
                  ,case when trim(e.a_field) = 'Y' then 'Yes' else 'No' end as MEAL_WAIVER
                  ,to_char(e.adj_hire_date, 'mm/dd/yyyy') as REHIRE_DATE
                  ,to_char(e.annivers_date, 'mm/dd/yyyy') as POSITION_DATE
                  ,e.pay_rate as BASE_WAGE
                  ,to_char(e.Base_Wage_Effective_Date, 'mm/dd/yyyy') as BASE_WAGE_EFFECTIVE_DATE
                  ,trim(e.hm_dist_co) as COMPANY
                  ,trim(e.process_level) as PROCESS_LEVEL
                  ,trim(e.supervisor)  as SUPERVISOR
                  ,null as SUB_DEPARTMENT
                  ,to_char(e.annivers_date, 'mm/dd/yyyy') as PRIMARY_ACCOUNT_EFFECTIVE_DATE
                  ,trim(e.position) as POSITION_CODE
		from v_kronos_emp e;

	EMP_RECORD	CURR_EMPLOYEES%rowtype;

	BEGIN
	
		v_FileName 	 := 'EMPLOYEE_IMPORT.csv';
		v_FileHandle := UTL_FILE.FOPEN('UTLFILE', v_FileName, 'w') ;			  	 -- UTLFILE Directory is /utlfile/dev 
		UTL_FILE.put_line (v_FileHandle, 'EMPLOYEE_ID' ||'|'||		  
					'FIRST_NAME'||'|'||
					'LAST_NAME'||'|'|| 	  		 		  	  
					'MIDDLE_INITIAL'||'|'||
					'SHORT_NAME'||'|'||
					'HIRE_DATE'||'|'||
					'BIRTH_DATE'||'|'||
					'JOB_TITLE'||'|'||
					'STATUS'||'|'||
					'PAY_RULE_EFFECTIVE_DATE'||'|'||
					'FULL_TIME_PART_TIME'||'|'||
					'LOCATION'||'|'||
					'PRIMARY_CELL'||'|'||
					'PRIMARY_HOME'||'|'||
					'EMAIL'||'|'||
					'STATUS_DATE'||'|'||
					'MEAL_WAIVER'||'|'||
					'REHIRE_DATE'||'|'||
					'POSITION_DATE'||'|'||
					'BASE_WAGE'||'|'||			
					'BASE_WAGE_EFFECTIVE_DATE'||'|'||
                    'COMPANY'||'|'||
                    'PROCESS_LEVEL'||'|'||
                    'SUPERVISOR'||'|'||
                    'SUB_DEPARTMENT'||'|'||
                    'PRIMARY_ACCOUNT_EFFECTIVE_DATE'||'|'||
                    'POSITION_CODE');

		OPEN CURR_EMPLOYEES;
		LOOP
		FETCH CURR_EMPLOYEES INTO EMP_RECORD;
		EXIT WHEN CURR_EMPLOYEES%NOTFOUND OR CURR_EMPLOYEES%NOTFOUND IS NULL;

	 	UTL_FILE.put_line (v_FileHandle, EMP_RECORD.EMPLOYEE_ID ||'|'||		  
					EMP_RECORD.FIRST_NAME||'|'||
					EMP_RECORD.LAST_NAME||'|'|| 	  		 		  	  
					EMP_RECORD.MIDDLE_INITIAL||'|'||
					EMP_RECORD.SHORT_NAME||'|'||
					EMP_RECORD.HIRE_DATE||'|'||
					EMP_RECORD.BIRTH_DATE||'|'||
					EMP_RECORD.JOB_TITLE||'|'||
					EMP_RECORD.STATUS||'|'||
					EMP_RECORD.PAY_RULE_EFFECTIVE_DATE||'|'|| 
					EMP_RECORD.FULL_TIME_PART_TIME||'|'||
					EMP_RECORD.LOCATION||'|'||
					EMP_RECORD.PRIMARY_CELL||'|'||	
					EMP_RECORD.PRIMARY_HOME||'|'||
					EMP_RECORD.EMAIL||'|'||
					EMP_RECORD.STATUS_DATE||'|'||
					EMP_RECORD.MEAL_WAIVER||'|'||
					EMP_RECORD.REHIRE_DATE||'|'||
					EMP_RECORD.POSITION_DATE||'|'||
					EMP_RECORD.BASE_WAGE||'|'||
                    EMP_RECORD.BASE_WAGE_EFFECTIVE_DATE||'|'|| 	
					EMP_RECORD.COMPANY||'|'||
					EMP_RECORD.PROCESS_LEVEL||'|'||
                    EMP_RECORD.SUPERVISOR||'|'||
                    EMP_RECORD.SUB_DEPARTMENT||'|'||
                    EMP_RECORD.PRIMARY_ACCOUNT_EFFECTIVE_DATE||'|'||
                    EMP_RECORD.POSITION_CODE);

		END LOOP;
		CLOSE CURR_EMPLOYEES;

		UTL_FILE.fclose_all;

		EXCEPTION
			WHEN OTHERS THEN
				RAISE;		

	END SP_EMPLOYEE_OUTPUT;
    
    
    PROCEDURE SP_EMPLOYEE_HIST_OUTPUT IS

    v_FileName         VARCHAR(50);
    v_FileHandle        UTL_FILE.FILE_TYPE;
    
    CURSOR CURR_EMPLOYEES IS
    
        select e.employee as EMPLOYEE_ID
                  ,trim(e.first_name) as FIRST_NAME
                  ,trim(e.last_name) as LAST_NAME
                  ,case when length(trim(case when e.middle_init = ' ' then null else e.middle_init end)) > 1 
                    then substr(trim(case when e.middle_init = ' ' then null else e.middle_init end), 1, 1) 
                    else trim(case when e.middle_init = ' ' then null else e.middle_init end) end as MIDDLE_INITIAL
                  ,case when length(trim(e.nick_name)) > 20 then substr(trim(e.nick_name), 1, 20) else trim(e.nick_name) end as SHORT_NAME
                  ,to_char(e.date_hired, 'mm/dd/yyyy')  as HIRE_DATE
                  ,to_char(e.birthdate, 'mm/dd/yyyy')  as BIRTH_DATE
                  ,trim(e.job_code) as JOB_TITLE
                  ,trim(e.emp_status) as STATUS
                  ,to_char(e.annivers_date, 'mm/dd/yyyy') as PAY_RULE_EFFECTIVE_DATE
                  ,trim(e.annual_hours) as FULL_TIME_PART_TIME
                  ,trim(e.department) as LOCATION
                  ,trim(e.phone) as PRIMARY_CELL
                  ,trim(e.hm_phone_nbr) as PRIMARY_HOME
                  ,case when length(trim(e.email_address)) > 50 then substr(trim(e.email_address), 1, 50) else trim(e.email_address) end as EMAIL
                  ,to_char(e.beg_date, 'mm/dd/yyyy') as STATUS_DATE
                  ,case when trim(e.a_field) = 'Y' then 'Yes' else 'No' end as MEAL_WAIVER
                  ,to_char(e.adj_hire_date, 'mm/dd/yyyy') as REHIRE_DATE
                  ,to_char(e.annivers_date, 'mm/dd/yyyy') as POSITION_DATE
                  ,e.pay_rate as BASE_WAGE
                  ,to_char(e.Base_Wage_Effective_Date, 'mm/dd/yyyy') as BASE_WAGE_EFFECTIVE_DATE
                  ,trim(e.hm_dist_co) as COMPANY
                  ,trim(e.process_level) as PROCESS_LEVEL
                  ,trim(e.supervisor)  as SUPERVISOR
                  ,null as SUB_DEPARTMENT
                  ,to_char(e.annivers_date, 'mm/dd/yyyy') as PRIMARY_ACCOUNT_EFFECTIVE_DATE
                  ,trim(e.position) as POSITION_CODE
        from v_kronos_emp_hist e
        where e.emp_status <> 'T';

    EMP_RECORD    CURR_EMPLOYEES%rowtype;

    BEGIN
    
        v_FileName      := 'EMPLOYEE_IMPORT_HISTORY.csv';
        v_FileHandle := UTL_FILE.FOPEN('UTLFILE', v_FileName, 'w') ;                   -- UTLFILE Directory is /utlfile/dev 
        UTL_FILE.put_line (v_FileHandle, 'EMPLOYEE_ID' ||'|'||          
                    'FIRST_NAME'||'|'||
                    'LAST_NAME'||'|'||                                
                    'MIDDLE_INITIAL'||'|'||
                    'SHORT_NAME'||'|'||
                    'HIRE_DATE'||'|'||
                    'BIRTH_DATE'||'|'||
                    'JOB_TITLE'||'|'||
                    'STATUS'||'|'||
                    'PAY_RULE_EFFECTIVE_DATE'||'|'||
                    'FULL_TIME_PART_TIME'||'|'||
                    'LOCATION'||'|'||
                    'PRIMARY_CELL'||'|'||
                    'PRIMARY_HOME'||'|'||
                    'EMAIL'||'|'||
                    'STATUS_DATE'||'|'||
                    'MEAL_WAIVER'||'|'||
                    'REHIRE_DATE'||'|'||
                    'POSITION_DATE'||'|'||
                    'BASE_WAGE'||'|'||            
                    'BASE_WAGE_EFFECTIVE_DATE'||'|'||
                    'COMPANY'||'|'||
                    'PROCESS_LEVEL'||'|'||
                    'SUPERVISOR'||'|'||
                    'SUB_DEPARTMENT'||'|'||
                    'PRIMARY_ACCOUNT_EFFECTIVE_DATE'||'|'||
                    'POSITION_CODE');

        OPEN CURR_EMPLOYEES;
        LOOP
        FETCH CURR_EMPLOYEES INTO EMP_RECORD;
        EXIT WHEN CURR_EMPLOYEES%NOTFOUND OR CURR_EMPLOYEES%NOTFOUND IS NULL;

         UTL_FILE.put_line (v_FileHandle, EMP_RECORD.EMPLOYEE_ID ||'|'||          
                    EMP_RECORD.FIRST_NAME||'|'||
                    EMP_RECORD.LAST_NAME||'|'||                                
                    EMP_RECORD.MIDDLE_INITIAL||'|'||
                    EMP_RECORD.SHORT_NAME||'|'||
                    EMP_RECORD.HIRE_DATE||'|'||
                    EMP_RECORD.BIRTH_DATE||'|'||
                    EMP_RECORD.JOB_TITLE||'|'||
                    EMP_RECORD.STATUS||'|'||
                    EMP_RECORD.PAY_RULE_EFFECTIVE_DATE||'|'|| 
                    EMP_RECORD.FULL_TIME_PART_TIME||'|'||
                    EMP_RECORD.LOCATION||'|'||
                    EMP_RECORD.PRIMARY_CELL||'|'||    
                    EMP_RECORD.PRIMARY_HOME||'|'||
                    EMP_RECORD.EMAIL||'|'||
                    EMP_RECORD.STATUS_DATE||'|'||
                    EMP_RECORD.MEAL_WAIVER||'|'||
                    EMP_RECORD.REHIRE_DATE||'|'||
                    EMP_RECORD.POSITION_DATE||'|'||
                    EMP_RECORD.BASE_WAGE||'|'||
                    EMP_RECORD.BASE_WAGE_EFFECTIVE_DATE||'|'||     
                    EMP_RECORD.COMPANY||'|'||
                    EMP_RECORD.PROCESS_LEVEL||'|'||
                    EMP_RECORD.SUPERVISOR||'|'||
                    EMP_RECORD.SUB_DEPARTMENT||'|'||
                    EMP_RECORD.PRIMARY_ACCOUNT_EFFECTIVE_DATE||'|'||
                    EMP_RECORD.POSITION_CODE);

        END LOOP;
        CLOSE CURR_EMPLOYEES;

        UTL_FILE.fclose_all;

        EXCEPTION
            WHEN OTHERS THEN
                RAISE;        

    END SP_EMPLOYEE_HIST_OUTPUT;
    
   PROCEDURE SP_EMPLOYEE_ACCRUAL_OUTPUT IS

    v_FileName         VARCHAR(50);
    v_FileHandle        UTL_FILE.FILE_TYPE;
    
    CURSOR CURR_EMPLOYEES IS
    
        select employee as EMPL_ID
                ,ACCRUAL_CODE
                ,to_char(ACCRUAL_EFF_DATE, 'mm/dd/yyyy') as ACCRUAL_EFF_DATE
                ,round(avail_hrs_bal, 4) as ACCRUAL_BALANCE
        from v_kronos_emp_accrual;

    EMP_RECORD    CURR_EMPLOYEES%rowtype;

    BEGIN
    
        v_FileName      := 'Accruals.csv';
        v_FileHandle := UTL_FILE.FOPEN('UTLFILE', v_FileName, 'w') ;                   -- UTLFILE Directory is /utlfile/dev 
        UTL_FILE.put_line (v_FileHandle, 'EMPL_ID' ||'|'||          
                    'ACCRUAL_CODE'||'|'||
                    'ACCRUAL_EFF_DATE'||'|'||                                
                    'ACCRUAL_BALANCE');

        OPEN CURR_EMPLOYEES;
        LOOP
        FETCH CURR_EMPLOYEES INTO EMP_RECORD;
        EXIT WHEN CURR_EMPLOYEES%NOTFOUND OR CURR_EMPLOYEES%NOTFOUND IS NULL;

         UTL_FILE.put_line (v_FileHandle, EMP_RECORD.EMPL_ID ||'|'||          
                    EMP_RECORD.ACCRUAL_CODE||'|'||
                    EMP_RECORD.ACCRUAL_EFF_DATE||'|'||                                
                    EMP_RECORD.ACCRUAL_BALANCE);

        END LOOP;
        CLOSE CURR_EMPLOYEES;

        UTL_FILE.fclose_all;

        EXCEPTION
            WHEN OTHERS THEN
                RAISE;        

    END SP_EMPLOYEE_ACCRUAL_OUTPUT;

END EXTRACT_EMPLOYEE;
/
