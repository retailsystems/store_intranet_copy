<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PickUp.aspx.cs" Inherits="ShipToStore.PickUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD>
		<title>Pick Up</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
		 <script language="javascript" type="text/javascript">
   
    //Function checkBoxClicked checks the checkboxes based on the user input
	function checkBoxClicked(cbxSelect,cbxAdmin,cbxPT,cbxNew,cbxCP,cbsPH,cbsPHED,cbxAPR,cbxReports,ctl)
	{
		var cbkSelect = document.getElementById(cbxSelect);
				
		//var retVal = "false";
				
			//if(cbkAdmin.checked == true || cbkPT.checked == true || cbkReports.checked == true || cbkNew.checked == true || cbkCP.checked == true|| cbkPH.checked == true || cbkPHED.checked == true || cbkAPR.checked == true)
			//{
				retVal = "true";
		
			//}
			//else if(cbkAdmin.checked == false || cbkPT.checked == false || cbkReports.checked == false || cbkNew.checked == false || cbkCP.checked == false|| cbkPH.checked == false || cbkPHED.checked == false || cbkAPR.checked == false)
			//{
			//	retVal = "true";
				
			//}
			//else
			//{
			//	retVal = "false";
			//}
		if(retVal == "true")
		{
		  //cbkSelect.style.visibility = 'visible';
		 // alert("3.");
		 // alert("abc:" + abc.value);
		 // alert("5.");
		   cbkSelect.checked = true;
			//alert("5.");
		}
	}

</script>
	</HEAD>
	<body>
		<form id="frmPickUp" method="post" runat="server">
				<asp:GridView ID="gvOrderPikUpView" runat="server" ShowHeader="true" ShowFooter="false" AutoGenerateColumns ="false"  width="299px" Height="141px" style="text-align: center;"  HeaderStyle-BackColor = "#DCDCDC" FooterStyle-BackColor="#DCDCDC"
         Font-Names = "Arial"  Font-Size = "8pt"  onrowupdating = "UpdateOrdPikUp"  OnRowDataBound="gvOrderPikUpView_RowDataBound" EnableViewState=true >
             <Columns>
                <asp:TemplateField ShowHeader ="true" HeaderText="Select" FooterText="Select">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server" 
                            AutoPostBack="true" 
                            enabled ='<%# Convert.ToBoolean(Eval("FLAG")) %>'
                            Text='<%# Eval("FLAG").ToString().Equals("True") ? "" : "" %>' />
                    </ItemTemplate>                    
                </asp:TemplateField>
            
                 <asp:BoundField ShowHeader="true" DataField="TRACKNO" HeaderText="TRACKNO" FooterText="TRACKNO" ReadOnly="True">
                    <ItemStyle Width="10px" />
                </asp:BoundField>
                <asp:BoundField ShowHeader =true DataField="FULLORDERNO" HeaderText="FULLORDERNO"  FooterText="FULLORDERNO" ReadOnly="True">
                    <ItemStyle Width="10px" />
                </asp:BoundField>
                <asp:BoundField ShowHeader =true DataField="DATERECINSTORE" HeaderText="RECEIVED ON" FooterText="RECEIVED ON" ReadOnly ="True">
                    <ItemStyle Width="10px" />
                </asp:BoundField>
                <asp:BoundField ShowHeader =true DataField="DATECUSTPICKUP" HeaderText="PICKUP DATE" FooterText="PU DATE" ReadOnly="True">
                    <ItemStyle Width="10px" />
                </asp:BoundField>
             </Columns>   
            <HeaderStyle CssClass="headerGD" ForeColor="Black" BackColor="#D3D3D3" />
            <FooterStyle ForeColor="Black" BackColor="#D3D3D3" />
         </asp:GridView> 
         <div>
           <asp:Button ID="btnWholUpdate" runat="server" Style="z-index: 101; left: 201px; 
            top: 600px" Text="CUSTOMER PICK UP" Font-Bold="True" Font-Size="10pt"  Width="155px" OnClick="btnWPickUp_Click" UseSubmitBehavior="False" />
            <asp:Button ID="btnEmailReminder" runat="server" Style="z-index: 101; left: 201px; 
            top: 600px" Text="EMAIL REMINDER" Font-Bold="True" Font-Size="10pt"  Width="155px" OnClick="btnEmailReminder_Click" UseSubmitBehavior="False" />
         <table><tr class="PageError">
					<td colspan="2"><font color="#cc0000"><asp:Label ID="lblErrorText" runat="server" Visible="false"></asp:Label></font></td>
				</tr></table>
         </div>
            </form>
	</body>
</HTML>
