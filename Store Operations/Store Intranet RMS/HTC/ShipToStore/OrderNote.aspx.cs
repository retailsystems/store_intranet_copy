using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text;
using System.Data.SqlClient;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for OrderNote.
	/// </summary>
	public class OrderNote : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.TextBox txtNotes;
		string strOrderNumber = string.Empty;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
						
			strOrderNumber= Request.QueryString["StrOrderNo"];

            txtNotes.Text  =Data.GetNote(strOrderNumber).ToString();
		}

//		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			Response.Write("<script language='javascript'> { self.close() }</script>");
		}
		
	}
}
