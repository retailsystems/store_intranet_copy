using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for OrderSearch.
	/// </summary>
	public class OrderSearch : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList ddlRegion;
		protected System.Web.UI.WebControls.DropDownList ddlDistrict;
		protected System.Web.UI.WebControls.DropDownList ddlStore;
		protected System.Web.UI.WebControls.TextBox txtOrderDateFrom;
		protected System.Web.UI.WebControls.TextBox txtOrderDateTo;
		protected System.Web.UI.WebControls.TextBox txtShipDateFrom;
		protected System.Web.UI.WebControls.TextBox txtShipDateTo;
		protected System.Web.UI.WebControls.DropDownList ddlStatus;
		protected System.Web.UI.HtmlControls.HtmlTable tblOption;
		protected System.Web.UI.WebControls.TextBox txtOrderNumber;
		protected System.Web.UI.WebControls.TextBox txtFirstName;
		protected System.Web.UI.WebControls.TextBox txtLastName;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
	
		public string nStrID ="29";
		public string regionID = "20";

		private int intPermit;
		private string strUserProfile;  //session
		ListItem itmAll;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
		protected System.Web.UI.WebControls.CustomValidator cvalShipTo;
		protected System.Web.UI.WebControls.CustomValidator cvalShipFrom;
		protected System.Web.UI.WebControls.CustomValidator cvalDateRange;
		protected System.Web.UI.WebControls.CustomValidator cvalDdlStatus;
		//protected System.Web.UI.WebControls.CustomValidator cvalDdlStatus;
		DataTable dtUser;


		private void Page_Load(object sender, System.EventArgs e)
		{
			

			// check user actually logged into the page.
			if (!this.IsPostBack)
			{
				try
				{
					intPermit = int.Parse(Session["ViewPermission"].ToString());
					//strUserProfile = Session["UserProfile"].ToString();  //add session

					if (intPermit !=0)
					{
						LoadDefaultDate();
						LoadUserOptions();
					}
					else
					{
						//no proper permission go back to Login
						Response.Redirect("./Login.aspx");

					}
				}
				catch (Exception ex)
				{
					Response.Write(ex.StackTrace + "\n\n" + ex.Message);
				}

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlRegion.SelectedIndexChanged += new System.EventHandler(this.ddlRegion_SelectedIndexChanged);
			this.ddlDistrict.SelectedIndexChanged += new System.EventHandler(this.ddlDistrict_SelectedIndexChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void LoadDefaultDate()
		{
			DateTime dtNow = DateTime.Now;
			DateTime dtFrom = dtNow.AddMonths(-3); 
			this.txtOrderDateTo.Text= dtNow.ToShortDateString();
			this.txtOrderDateFrom.Text = dtFrom.ToShortDateString();
//			this.txtShipDateTo.Text = dtNow.ToShortDateString();
//			this.txtShipDateFrom.Text = dtFrom.ToShortDateString();
		}
		private void getRegion()
		{
			string strRegion;

			if (ddlRegion.SelectedIndex > 0)
			{
				strRegion = ddlRegion.SelectedItem.Text;
               
			}

		}

#region LoadDropDown Based On User Permission
		private void LoadDefaultOptions(DropDownList ddlList, bool IsAllSelected)
		{
			itmAll = new ListItem();
			itmAll.Text="All";
			itmAll.Value = "0";

			ddlList.Items.Insert(0, itmAll);
			if (IsAllSelected)
			{
				ddlList.Enabled = false;

			}
		
		}
		private void LoadRegionDropDown()
		{
			
			DataTable dtRegion =(DataTable)Application["dtRegion"];
				
			this.ddlRegion.DataSource = dtRegion;
			this.ddlRegion.DataTextField = "Region";
			this.ddlRegion.DataValueField = "Region";
			ddlRegion.DataBind();
			//make sure all is selected
			LoadDefaultOptions(ddlRegion, false);
			LoadDefaultOptions(ddlDistrict, true);
			LoadDefaultOptions(ddlStore, true);
			
		}


		protected void LoadUserOptions()
		{
			
		
			switch (intPermit)
			{
				case 1:
					//admin show all
				case 5:
					//executive show all
				case 6:
					//LP show all
					LoadRegionDropDown();
					break;
				case 2:
					//RD show only that region
					LoadRegionOnly();
					break;
				case 3:
					//DM show only that district
					LoadDistrictOnly();
					break;
				case 4:
					//Store show only that store
					LoadStoreOnly();
					break;

			}

		}

		private void LoadRegionOnly()
		{
			ddlRegion.Items.Clear();
			ddlDistrict.Items.Clear();
			ddlStore.Items.Clear();

			dtUser = (DataTable)Session["UserProfile"];
			string RegionNo = Utility.ParseDepartment(dtUser.Rows[0]["Dept"].ToString().Trim());

			DataTable dtDistrict = Data.GetDistrict(RegionNo);

			if (dtDistrict!=null)
			{
				string strRegion = dtDistrict.Rows[0]["Region"].ToString().Trim();
				string strDistrict = dtDistrict.Rows[0]["District"].ToString().Trim();
			

				ListItem itmRegion = new ListItem();
				itmRegion.Text=strRegion;
				itmRegion.Value=strRegion;
				ddlRegion.Items.Add(itmRegion);
				ddlRegion.Enabled = false;

				

				ddlDistrict.DataSource = dtDistrict;
				ddlDistrict.DataTextField="District";
				ddlDistrict.DataValueField="District";
				ddlDistrict.DataBind();
				LoadDefaultOptions(ddlDistrict, false);
				LoadDefaultOptions(ddlStore, true);
					
			}
			
		}

		private void LoadDistrictOnly()
		{
			ddlRegion.Items.Clear();
			ddlDistrict.Items.Clear();
			ddlStore.Items.Clear();

			dtUser = (DataTable)Session["UserProfile"];
			string DistrictNo = Utility.ParseDepartment(dtUser.Rows[0]["Dept"].ToString().Trim());

			DataTable dtStore = Data.GetStore(DistrictNo);

			if (dtStore!=null)
			{
				string strRegion = dtStore.Rows[0]["Region"].ToString().Trim();
				string strDistrict = dtStore.Rows[0]["District"].ToString().Trim();
			

				ListItem itmRegion = new ListItem();
				itmRegion.Text=strRegion;
				itmRegion.Value=strRegion;
				ddlRegion.Items.Add(itmRegion);
				ddlRegion.Enabled = false;

				ListItem itmDistrict = new ListItem();
				itmDistrict.Text=strDistrict;
				itmDistrict.Value=strDistrict;
				ddlDistrict.Items.Add(itmDistrict);
				ddlDistrict.Enabled = false;

				ddlStore.DataSource = dtStore;
				ddlStore.DataTextField="StoreName";
				ddlStore.DataValueField="StoreNum";
				ddlStore.DataBind();
				LoadDefaultOptions(ddlStore, false);
				//ddlStore.Enabled = true;			
			}			
		}

		private void LoadStoreOnly()
		{
			ddlRegion.Items.Clear();
			ddlDistrict.Items.Clear();
			ddlStore.Items.Clear();
			dtUser = (DataTable)Session["UserProfile"];
			string StoreNo = dtUser.Rows[0]["Location"].ToString().Trim();

			DataTable dtStore = Data.GetStoreSingle(StoreNo);

			if (dtStore!=null)
			{
				string strRegion = dtStore.Rows[0]["Region"].ToString().Trim();
				string strDistrict = dtStore.Rows[0]["District"].ToString().Trim();
				string strStoreName = dtStore.Rows[0]["StoreName"].ToString().Trim();

				ListItem itmRegion = new ListItem();
				itmRegion.Text=strRegion;
				itmRegion.Value=strRegion;
				ddlRegion.Items.Add(itmRegion);
				ddlRegion.Enabled = false;

				ListItem itmDistrict = new ListItem();
				itmDistrict.Text=strDistrict;
				itmDistrict.Value=strDistrict;
				ddlDistrict.Items.Add(itmDistrict);
				ddlDistrict.Enabled = false;

				ListItem itmStore = new ListItem();
				itmStore.Text=strStoreName;
				itmStore.Value=StoreNo;
				ddlStore.Items.Add(itmStore);
				ddlStore.Enabled = false;			
			}
			
		}

		private void ddlRegion_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (ddlRegion.SelectedValue!="0")
			{
				this.ddlStore.Enabled = false;
				this.ddlDistrict.Enabled = true;			
			
				//depends on selection, load District
				string strR = ddlRegion.SelectedValue;
				DataTable dtDistrict = Data.GetDistrict(strR);
				ddlDistrict.DataSource = dtDistrict;
				this.ddlDistrict.DataTextField = "District";
				this.ddlDistrict.DataValueField = "District";

				LoadDefaultOptions(ddlDistrict, false);
				ddlDistrict.DataBind();
				LoadDefaultOptions(ddlDistrict, false);
			}
			else
			{
				ddlDistrict.Items.Clear();
				ddlStore.Items.Clear();

				LoadDefaultOptions(ddlDistrict, true);
				LoadDefaultOptions(ddlStore, true);
		

			}
		}

		private void ddlDistrict_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//depends on selection, load stores
			if (ddlDistrict.SelectedValue!="0")
			{
				this.ddlStore.Enabled = true;
			
			
				//depends on selection, load Store
				string strD = ddlDistrict.SelectedValue;
				DataTable dtStore = Data.GetStore(strD);
				ddlStore.DataSource = dtStore;
				this.ddlStore.DataTextField = "StoreName";
				this.ddlStore.DataValueField = "StoreNum";

				
				ddlStore.DataBind();
				LoadDefaultOptions(ddlStore, false);
			}
			else

			{
				ddlStore.Items.Clear();
				LoadDefaultOptions(ddlStore, true);
				
			}

		}
		#endregion

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			ValidateShipDates();
			//ValidateLstStatus();

			if (Page.IsValid)
			{ 
                string strOrderNoValid = string.Empty;
                
				strOrderNoValid = txtOrderNumber.Text.Trim().ToUpper();
				if ((strOrderNoValid.Length == 12) || (strOrderNoValid.Length == 13))
				{
                   strOrderNoValid = strOrderNoValid.Substring(0,8);
				}
			
				this.btnSearch.Enabled = false;
				Response.Redirect("OrderView.aspx?" + 
					"Reg=" +   Server.UrlEncode(this.ddlRegion.SelectedValue)+
					"&Dis=" +   Server.UrlEncode(this.ddlDistrict.SelectedValue)+
					"&Str=" +   Server.UrlEncode(this.ddlStore.SelectedValue)+
					"&OrdFdt=" + Server.UrlEncode(this.txtOrderDateFrom.Text.Trim()) +
					"&OrdTDt=" + Server.UrlEncode(this.txtOrderDateTo.Text.Trim()) + 
					"&ShpFDt=" + Server.UrlEncode(this.txtShipDateFrom.Text.Trim()) +
					"&ShpTDt=" + Server.UrlEncode(this.txtShipDateTo.Text.Trim()) + 
					"&OrdNo=" + Server.UrlEncode(strOrderNoValid) + 
					//"&OrdNo=" + Server.UrlEncode(this.txtOrderNumber.Text.Trim().ToUpper()) + 
					"&FName=" + Server.UrlEncode(this.txtFirstName.Text.Trim().ToUpper()) +
					"&LName=" + Server.UrlEncode(this.txtLastName.Text.Trim().ToUpper()) +
					"&Email=" + Server.UrlEncode(this.txtEmail.Text.Trim().ToLower()) + 
					"&Stat=" + Server.UrlEncode(this.ddlStatus.SelectedValue)); 
			}
		}
//		protected void ValidateLstStatus() 
//		{
//			if(this.ddlStatus.SelectedIndex == 0)
//			{
//				cvalDdlStatus.IsValid = false;
//			}
//			else
//			{
//			    cvalDdlStatus.IsValid = true;
//			}
//		}

		protected void ValidateShipDates() 		
		{
			if (this.txtShipDateFrom.Text!="" || this.txtShipDateTo.Text!="")
			{
				DateTime dtFrom = DateTime.Parse("1/1/0001");
				DateTime dtTo = DateTime.Parse("1/1/0001");
				bool IsFail = false;
				if (this.txtShipDateFrom.Text!="")
				{
					try 
					{
						dtFrom = DateTime.Parse(this.txtShipDateFrom.Text.Trim());
					}
					catch
					{
						this.cvalShipFrom.IsValid =false;
						IsFail = true;		
					}

				}
				if (this.txtShipDateTo.Text!="")
				{
					try
					{
						dtTo = DateTime.Parse(this.txtShipDateTo.Text.Trim());
					}
					catch
					{
						this.cvalShipTo.IsValid=false;
						IsFail = true;
					}
				}
				if (!IsFail)
				{				
					//To has to be later than From
					if (txtShipDateFrom.Text=="")
					{
						cvalShipFrom.IsValid = false;

					}
					else if (txtShipDateTo.Text=="")
					{
						cvalShipTo.IsValid = false;

					}
					else if (dtFrom.CompareTo(dtTo)>0)
					{
						this.cvalDateRange.IsValid = false;
					}
				}
									
			
			}

		}

	

		private void GatherInputData()
		{


			//string strOrderNo; 
			ShipToStore.Order objOneOrder = new ShipToStore.Order();
            
			if (txtOrderNumber.Text!= "")
			{
				objOneOrder.StrOrderNo = txtOrderNumber.Text;
			}

			if (txtOrderDateFrom.Text.Trim()!= "")
			{ 
              
				//				if (IsDate(txtOrderDateFrom.Text.Trim())
				//				{
				//					if (CDate(txtOrderDateFrom.Text) < CDate("1/1/1900"))
				//					{
				//					
				//					}
				//				
				//				}
				//			
			}
			

		}

		
	}
}
