using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;

using System.Text;

using System.Diagnostics;
using System.IO;


namespace ShipToStore
{
    /// <summary>
    /// Summary description for General.
    /// </summary>
    public class Data
    {
        public Data()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static DataTable GetRegion()
        {
            DataTable dtRegion = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@Company", SqlDbType.VarChar);
            Parameters[0].Value = ConfigurationSettings.AppSettings["Company"];
            dtRegion = ExecuteStoredProcedure("Region_SelectAll", Parameters, null);

            return dtRegion;
        }
        public static DataTable GetChannels()
        {
            DataTable dtChannel = new DataTable();           
            dtChannel= ExecuteStoredProcedure("usp_GetChannels", null, null);
            return dtChannel;
        }
        public static string GetRegion(string strDist)
        {
            string strRegion = null;
            DataTable dtRegion = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[2];
            Parameters[0] = new SqlParameter("@DistrictNo", SqlDbType.VarChar);
            Parameters[0].Value = strDist;
            Parameters[1] = new SqlParameter("@Company", SqlDbType.VarChar);
            Parameters[1].Value = ConfigurationSettings.AppSettings["Company"];
            dtRegion = ExecuteStoredProcedure("Region_SelectByDistrict", Parameters, null);

            if (dtRegion != null)
            {
                strRegion = dtRegion.Rows[0]["Region"].ToString();
            }

            return strRegion;
        }

        public static DataTable GetDistrict(string strRegion)
        {

            DataTable dtDistrict = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[2];
            Parameters[0] = new SqlParameter("@RegionNo", SqlDbType.VarChar);
            Parameters[0].Value = strRegion;
            Parameters[1] = new SqlParameter("@Company", SqlDbType.VarChar);
            Parameters[1].Value = ConfigurationSettings.AppSettings["Company"];
            dtDistrict = ExecuteStoredProcedure("District_SelectByRegion", Parameters, null);
            return dtDistrict;
        }

        public static DataTable GetStore(string strDistrict)
        {
            DataTable dtStore = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[2];
            Parameters[0] = new SqlParameter("@DistrictNo", SqlDbType.VarChar);
            Parameters[0].Value = strDistrict;
            Parameters[1] = new SqlParameter("@Company", SqlDbType.VarChar);
            Parameters[1].Value = ConfigurationSettings.AppSettings["Company"];
            dtStore = ExecuteStoredProcedure("Store_SelectByDistrict", Parameters, null);
            return dtStore;
        }


        public static DataTable GetStoreSingle(string strStore)
        {

            DataTable dtStore = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[2];
            Parameters[0] = new SqlParameter("@StoreNo", SqlDbType.VarChar);
            Parameters[0].Value = strStore;
            Parameters[1] = new SqlParameter("@Company", SqlDbType.VarChar);
            Parameters[1].Value = ConfigurationSettings.AppSettings["Company"];
            dtStore = ExecuteStoredProcedure("Store_SelectByStore", Parameters, null);
            return dtStore;
        }

        public static string GetNote(string strOrderNumber)
        {
            string strNote = string.Empty;
            DataTable dtNote = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@ORDERNO", SqlDbType.VarChar);
            Parameters[0].Value = strOrderNumber;
            dtNote = ExecuteStoredProcedure("usp_GetOrderNote_ByOrdNo", Parameters, null);

            if (dtNote != null)
            {
                strNote = dtNote.Rows[0]["NOTES"].ToString();
            }
            return strNote;

        }

        public static DataTable GetTrackNos(string strOrderNumber)
        {
            DataTable dtTrackNo = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@OrderNo", SqlDbType.VarChar);
            Parameters[0].Value = strOrderNumber;

            dtTrackNo = ExecuteStoredProcedure("usp_TrackNos_ByOrderNo", Parameters, null);
            return dtTrackNo;

        }
        public static DataTable GetGiftCards(string strOrderNumber)
        {
            DataTable dtGCkNo = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@ORDERNO", SqlDbType.VarChar);
            Parameters[0].Value = strOrderNumber;

            dtGCkNo = ExecuteStoredProcedure("usp_GetGiftNo_ByOrdNo", Parameters, null);
            return dtGCkNo;

        }
        public static DataTable GetTrackNoByFullOrdNo(string strFullOrderNumber)
        {
            DataTable dtTrackNum = new DataTable();

            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@FullOrderNo", SqlDbType.VarChar);
            Parameters[0].Value = strFullOrderNumber;

            dtTrackNum = ExecuteStoredProcedure("usp_TrackNo_ByFullOrderNo", Parameters, null);
            return dtTrackNum;
        }

        public static DataTable GetOrdsbyTrackNo(string strTrackNo)
        {
            DataTable dtOrderNumber = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@TRACKNO", SqlDbType.VarChar);
            Parameters[0].Value = strTrackNo;
            dtOrderNumber = ExecuteStoredProcedure("usp_GetOrdRecInstr_byTrackNo", Parameters, null);
            return dtOrderNumber;
        }
        //add source code
        public static DataTable GetSorCdbyChnl(string strChannel)
        {
            DataTable dtSorCode = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@Channel", SqlDbType.VarChar);
            Parameters[0].Value = strChannel;
            dtSorCode = ExecuteStoredProcedure("usp_GetSorCod_byChannel", Parameters, null);
            return dtSorCode;
        }

        public static DataTable GetDevice()
        {
            DataTable dtDevice = new DataTable();
            dtDevice = ExecuteStoredProcedure("usp_GetDevice_All", null, null);
            return dtDevice;
        }

        public static string GetSorNmbyIndex(int nSource)
        {
            string strSorName = string.Empty;
            DataTable dtSorName = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@DeviceID", SqlDbType.Int);
            Parameters[0].Value = nSource;
            dtSorName = ExecuteStoredProcedure("usp_GetDevice_byIndex", Parameters, null);

            if (dtSorName != null)
            {
                strSorName = dtSorName.Rows[0]["DEVICE"].ToString();
            }
            return strSorName;
        }

        public static string GetStoreNo(string strFullOrderNo)
        {
            string strStoreNo = string.Empty;
            DataTable dtStoreNo = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@FullOrderNo", SqlDbType.VarChar);
            Parameters[0].Value = strFullOrderNo;
            dtStoreNo = ExecuteStoredProcedure("usp_StoreNo_ByFullOrderNo", Parameters, null);

            if (dtStoreNo != null)
            {
                strStoreNo = dtStoreNo.Rows[0]["STORENO"].ToString().Trim();
                // strStoreNo = "5098";
            }
            return strStoreNo;
        }
        //end add source code
        public static void TrackEmployeeScan(string strTrackNo, string strFullOrderNo, string strEmpID, string strStrNum, string strEnterType)
        {
            SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
            SqlCommand cm = new SqlCommand();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "usp_TrackEmpScanTime";
                cm.CommandTimeout = 60;

                cm.Parameters.Clear();

                SqlParameter qqq = new SqlParameter();
                qqq.ParameterName = "@TRACKNO";
                qqq.SqlDbType = SqlDbType.VarChar;
                qqq.Value = strTrackNo;
                cm.Parameters.Add(qqq);

                SqlParameter hhh = new SqlParameter();
                hhh.ParameterName = "@FULLORDERNO";
                hhh.SqlDbType = SqlDbType.VarChar;
                hhh.Size = 12;
                hhh.Value = strFullOrderNo;
                cm.Parameters.Add(hhh);

                SqlParameter mmm = new SqlParameter();
                mmm.ParameterName = "@EMPLOYEEID";
                mmm.SqlDbType = SqlDbType.VarChar;
                mmm.Size = 6; //changed from 5 to 6 for 6 digit empID login
                mmm.Value = strEmpID;
                cm.Parameters.Add(mmm);

                SqlParameter nnn = new SqlParameter();
                nnn.ParameterName = "@STORENUM";
                nnn.SqlDbType = SqlDbType.VarChar;
                nnn.Size = 4;
                nnn.Value = strStrNum;
                cm.Parameters.Add(nnn);

                SqlParameter xxx = new SqlParameter();
                xxx.ParameterName = "@ENTERTYPE";
                xxx.SqlDbType = SqlDbType.VarChar;
                xxx.Size = 1;
                xxx.Value = strEnterType;
                cm.Parameters.Add(xxx);

                cm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                //int s = 0;

            }
            finally
            {
                cn.Close();
            }
        }

        public static void TrackEmployeeLogin(string strEmpID, string strStrNum, string strLoginType)
        {
            SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
            SqlCommand cm = new SqlCommand();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "usp_TrackEmpLoginTime";
                cm.CommandTimeout = 60;

                cm.Parameters.Clear();

                SqlParameter mmm = new SqlParameter();
                mmm.ParameterName = "@EMPLOYEEID";
                mmm.SqlDbType = SqlDbType.VarChar;
                mmm.Size = 6; //change from 5 to 6
                mmm.Value = strEmpID;
                cm.Parameters.Add(mmm);

                SqlParameter nnn = new SqlParameter();
                nnn.ParameterName = "@STORENUM";
                nnn.SqlDbType = SqlDbType.VarChar;
                nnn.Size = 4;
                nnn.Value = strStrNum;
                cm.Parameters.Add(nnn);

                SqlParameter xxx = new SqlParameter();
                xxx.ParameterName = "@LOGINTYPE";
                xxx.SqlDbType = SqlDbType.VarChar;
                xxx.Size = 1;
                xxx.Value = strLoginType;
                cm.Parameters.Add(xxx);

                cm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                //int s = 0;

            }
            finally
            {
                cn.Close();
            }
        }
        public static string GetOrdStatus(string strOrderNumber)
        {
            string strOrdStatus = string.Empty;
            DataTable dtOrdStatus = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@OrderNo", SqlDbType.VarChar);
            Parameters[0].Value = strOrderNumber;
            dtOrdStatus = ExecuteStoredProcedure("usp_GetStatus_OrderNo_PTL", Parameters, null);

            if (dtOrdStatus != null)
            {
                strOrdStatus = dtOrdStatus.Rows[0]["OrderStatus"].ToString();
            }
            return strOrdStatus;

        }

        public static string GetSourceType(string strOrderNumber)
        {
            string strSource = string.Empty;
            DataTable dtSource = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@OrderNo", SqlDbType.VarChar);
            Parameters[0].Value = strOrderNumber;
            dtSource = ExecuteStoredProcedure("usp_GetOrderSourceByOrderNo", Parameters, null);

            if (dtSource != null)
            {
                strSource = dtSource.Rows[0]["SOURCE"].ToString();
            }
            return strSource;
        }

        public static string GetShippingCost(string strOrderNumber)
        {
            string strShippingCost = "0";
            DataTable dtShipping = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@OrderNo", SqlDbType.VarChar);
            Parameters[0].Value = strOrderNumber;
            dtShipping = ExecuteStoredProcedure("usp_GetShippingByOrderNo", Parameters, null);

            if (dtShipping != null && dtShipping.Rows.Count > 0)
            {
                strShippingCost = dtShipping.Rows[0]["SHIPPING"].ToString();
            }
            return strShippingCost;

        }

        public static DataTable GetOrdPikUpStatus(string strOrderNumber)
        {

            DataTable dtOrdPikUp = new DataTable();
            SqlParameter[] Parameters = new SqlParameter[1];
            Parameters[0] = new SqlParameter("@OrderNo", SqlDbType.VarChar);
            Parameters[0].Value = strOrderNumber;
            dtOrdPikUp = ExecuteStoredProcedure("usp_TrkPicUpView_ByOrderNo", Parameters, null);
            return dtOrdPikUp;
        }

        /// <returns>Datatable</returns>
        public static DataTable ExecuteStoredProcedure(string ProcedureName, SqlParameter[] Parameters, string Connection)
        {
            // Declare SQL objects needed to execute stored procedure and initialize to null
            DataTable dt = null;
            SqlCommand command = null;
            SqlConnection conn = null;
            SqlDataAdapter dataAdapter = null;

            try
            {

                if (Connection != null)
                {
                    conn = new SqlConnection(Connection);
                }
                else
                {
                    conn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
                }

                // Instantiate sqlcommand and set cmdtext to name of stored procedure and connection to sqlconnection
                command = new SqlCommand(ProcedureName, conn);
                // set sqlcommand commandtype to stored procedure
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 300;
                if (Parameters != null)
                {
                    foreach (SqlParameter parameter in Parameters)
                    {
                        // Make sure parameter in array is not null
                        if (parameter != null)
                            command.Parameters.Add(parameter);
                    }
                }
                // Instantiate sqldataadapter and set selectcommand to sqlcommand
                dataAdapter = new SqlDataAdapter(command);
                // Instantiate datatable
                dt = new DataTable();
                // Populate datatable by calling sqldataadapter's fill method and passing in datatable
                dataAdapter.Fill(dt);
            }
            catch (Exception e)
            {
                // Bubble SQL Server error up
                throw new Exception("Data Access Error: " + e.Message);
            }
            finally
            {
                // Close the sqlconnection
                //This will always get called regardless if there is an error or not

                conn.Close();
            }

            return dt;
        }

		/// <summary>
		/// Returns orders for Admin page that meet the search criteria.
		/// </summary>
		/// <param name="TrackingNumber">Tracking Number will return all orders with shipments with that tracking number</param>
		/// <param name="OrderNumber">Order Number will return all shipments for the order</param>
		/// <param name="FullOrderNumber">Full Order Number will return just that shipment</param>
		/// <returns>Datatable with all useful information for each order to troubleshoot</returns>
		public static DataTable GetAdminSearchOrders(string TrackingNumber, string OrderNumber, string FullOrderNumber)
		{
			SqlParameter[] parameters = new SqlParameter[1];
			if (TrackingNumber != null)
				parameters[0] = new SqlParameter("@trackno", TrackingNumber);
			else if (OrderNumber != null)
				parameters[0] = new SqlParameter("@orderno", OrderNumber);
			else if (FullOrderNumber != null)
				parameters[0] = new SqlParameter("@fullorderno", FullOrderNumber);
			else
				throw new Exception("No search parameters provided.");

			DataTable result = new DataTable();
			result = ExecuteStoredProcedure("Admin_Search", parameters, null);
			return result;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="FullOrderNumber"></param>
		/// <param name="User"></param>
		public static void AbandonOrder(string FullOrderNumber, string User)
		{
			SqlConnection connection = null;

			try
			{
				connection = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
				SqlCommand command = new SqlCommand("AbandonedOrders_Insert", connection);
				command.CommandType = CommandType.StoredProcedure;

				if (FullOrderNumber != null)
					command.Parameters.AddWithValue("@FullOrderNumber", FullOrderNumber);
				else
					throw new ArgumentNullException("FullOrderNumber");

				if (User != null)
					command.Parameters.AddWithValue("@UserID", User);

				connection.Open();
				command.ExecuteNonQuery();
			}
			catch
			{
				throw;
			}
			finally
			{
				if (connection != null && connection.State != ConnectionState.Closed)
					connection.Close();
			}
		}
    }
}
