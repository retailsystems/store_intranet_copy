using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for Order.
	/// </summary>
	/// 
	  [Serializable]
	public class LP_Order
	{
		#region Members
		string _strRegionID= String.Empty;
		string _strDistrictID = String.Empty;
		string _strStoreID = String.Empty;
		//int _intStoreID;
		int _intCompanyID;

		string _strCustFirstName = String.Empty;
		string _strCustLastName = String.Empty;
		string _strCustDayPhone = String.Empty;
		string _strCustNightPhone = String.Empty;
		string _strCustName = String.Empty;
		string _strEmail = String.Empty;

		string _strOrderNo = String.Empty;
		DateTime _dtOrderDate;
		string _strPaymentType = String.Empty;
		double _dblOrderTotal;

		DateTime _dtShippedDate;
		string _strShippedSource = String.Empty;
		string _strShipMethod = String.Empty;
		string _strTrackingNo = String.Empty;
		double _dblOrderWeight;

		DateTime _dtReceivedDate;
		string _strStoreNotes = String.Empty;
		DateTime _dtCustPickUpDate;
		string _strPackageNo = String.Empty; 
		string _strStatus = String.Empty;
		string _strRecFullOrdNo = String.Empty;

		string  _strEmployeeNo = String.Empty;
		string _strEmployeeDiscount = String.Empty;
		string  _strGiftCardNo = String.Empty;


		//double _dblTotalSales = 0;
		int _intOrderUnit= 0;
//		double _dblADT = 0;
//		double _dblUPT = 0;

		#endregion Members

		#region Properties
		public string StrRegionID
		{
			get { return _strRegionID; }
			set { _strRegionID = value; }
		}
		public string StrStoreID
		{
			get { return _strStoreID; }
			set { _strStoreID = value; }
		}
		public string StrDistrictID
		{
			get { return _strDistrictID; }
			set { _strDistrictID = value; }
		}
		
		public int IntCompanyID
		{
			get {  return _intCompanyID; }
			set { _intCompanyID = value; }
		}
		public string StrCustFirstName
		{
			get { return _strCustFirstName; }
			set { _strCustFirstName = value; }
		}
		public string StrCustLastName
		{
			get { return _strCustLastName; }
			set { _strCustLastName = value; }
		}
		public string StrCustName
		{
			get { return _strCustName; }
			set { _strCustName = value; }
		}
		public string StrEmail
		{
			get { return _strEmail; }
			set { _strEmail = value; }
		}
		public string StrCustDayPhone
		{
			get { return _strCustDayPhone; }
			set { _strCustDayPhone = value; }
		}
		public string StrCustNightPhone
		{
			get { return _strCustNightPhone; }
			set { _strCustNightPhone = value; }
		}
		public string StrOrderNo
		{
			get { return _strOrderNo; }
			set { _strOrderNo = value; }
		}
		public DateTime DtOrderDate 
		{
			get { return _dtOrderDate; }
			set { _dtOrderDate = value; }
		}
		public string StrPaymentType
		{
			get { return _strPaymentType; }
			set { _strPaymentType = value; }
		}

		public double DblOrderTotal
		{
			get { return _dblOrderTotal; }
			set { _dblOrderTotal = value; }
		}

		public int IntOrderUnits
		{
			get { return _intOrderUnit; }
			set { _intOrderUnit = value; }
		}

		public DateTime DtShippedDate 
		{
			get { return _dtShippedDate; }
			set { _dtShippedDate = value; }
		}
		public string StrShippedSource
		{
			get { return _strShippedSource; }
			set { _strShippedSource = value; }
		}
		public string StrShipMethod
		{
			get { return _strShipMethod; }
			set { _strShipMethod = value; }
		}
		public string StrTrackingNo
		{
			get { return _strTrackingNo; }
			set { _strTrackingNo = value; }
		}

		public double DblOrderWeight
		{
			get { return _dblOrderWeight; }
			set { _dblOrderWeight = value; }
		}
		public DateTime DtReceivedDate 
		{
			get { return _dtReceivedDate; }
			set { _dtReceivedDate = value; }
		}
		public string StrStoreNotes
		{
			get { return _strStoreNotes; }
			set { _strStoreNotes = value; }
		}
		public DateTime DtCustPickUpDate 
		{
			get { return _dtCustPickUpDate; }
			set { _dtCustPickUpDate = value; }
		}
		public string StrPackageNo
		{
			get { return _strPackageNo; }
			set { _strPackageNo = value; }
		}
		public string StrStatus
		{
			get { return _strStatus; }
			set { _strStatus = value; }
		}

		public string StrRecFullOrdNo
		{
			get { return _strRecFullOrdNo; }
			set { _strRecFullOrdNo = value; }
		}
		
		  public string StrGiftCardNo
		  {
			  get { return _strGiftCardNo; }
			  set { _strGiftCardNo = value; }
		  }

		  public string StrEmployeeNo
		  {
			  get { return _strEmployeeNo; }
			  set { _strEmployeeNo = value; }
		  }

          //public string StrEmployeeDiscount
          //{
          //    get { return _strEmployeeDiscount; }
          //    set { _strEmployeeDiscount = value; }
          //}




		
		#endregion Properties

		public LP_Order()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public void PopulateProperties(SqlDataReader dr)
		{
			_strRegionID= Convert.ToString(dr["REGION"]);
			_strDistrictID= Convert.ToString(dr["DISTRICT"]);
			_strStoreID = Convert.ToString(dr["STORENO"]);
			_strOrderNo = Convert.ToString(dr["ORDERNO"]);
			if (dr["ENTRYDATE"]!=System.DBNull.Value)
			{
				_dtOrderDate = Convert.ToDateTime(dr["ENTRYDATE"]);
			}
			else
			{
				_dtOrderDate = DateTime.Parse("1/1/1980");
			}
			_strPaymentType = Convert.ToString(dr["PAYMETHOD"]);
			_strCustFirstName = Convert.ToString(dr["FNAME"]);
			_strCustLastName = Convert.ToString(dr["LNAME"]);
			_strCustName = Convert.ToString(dr["CNAME"]);
			_strCustDayPhone = Convert.ToString(dr["DAYPHONE"]);
			_strEmail = Convert.ToString(dr["EMAIL"]);
			_strShipMethod = Convert.ToString(dr["SHIPMETHOD"]);
			if (dr["PRODUCTDOLLARS"]!=System.DBNull.Value)
			{
				_dblOrderTotal = Convert.ToDouble(dr["PRODUCTDOLLARS"]);
			}		
			if (dr["QTY"]!=System.DBNull.Value)
			{
				_intOrderUnit = Convert.ToInt16(dr["QTY"]);
			}

			if (dr["SHIPDATE"]!=System.DBNull.Value)
			{
				_dtShippedDate = Convert.ToDateTime(dr["SHIPDATE"]); 
			}
			else
			{
				_dtShippedDate = DateTime.Parse("1/1/1980");
			}
			if (dr["ORDERWEIGHT"]!=System.DBNull.Value)
			{
				_dblOrderWeight = Convert.ToDouble(dr["ORDERWEIGHT"]);
			}
			else
			{
				_dblOrderWeight =0;
			}
			_strTrackingNo = Convert.ToString(dr["TRACKNO"]);
			if (dr["DATERECINSTORE"]!=System.DBNull.Value)
			{
				_dtReceivedDate = Convert.ToDateTime(dr["DATERECINSTORE"]);
			}
			else
			{
				_dtReceivedDate = DateTime.Parse("1/1/1980");
			}

			_strShippedSource = Convert.ToString(dr["SHIPSOURCE"]);
			_strStoreNotes = Convert.ToString(dr["NOTES"]);
			if (dr["DATECUSTPICKUP"]!=System.DBNull.Value)
			{
				_dtCustPickUpDate = Convert.ToDateTime(dr["DATECUSTPICKUP"]);
			}
			else
			{
				_dtCustPickUpDate = DateTime.Parse("1/1/1980");
			}
								
			_strPackageNo = Convert.ToString(dr["PACKAGENO"]);
            _strStatus = Convert.ToString(dr["STATUS"]);

            //TODO:CONFIRM with LAN
			_strRecFullOrdNo = Convert.ToString(dr["FULLORDERNO"]);
            if (dr["EMPNO"]!=System.DBNull.Value)
            {
                _strEmployeeNo = Convert.ToString(dr["EMPNO"]);
            }
            else
            {
                _strEmployeeNo = "";
            }

            if (dr["GCNO"] != System.DBNull.Value)
            {
                _strGiftCardNo = Convert.ToString(dr["GCNO"]);
            }
            else
            {
                _strGiftCardNo = "";
            }
     		
//			_dblTotalSales = Convert.ToDouble(dr["TotalGrossSales"]);
//			_strTotalOrder = Convert.ToString (dr["TotalOrder"]);
//			_dblADT = Convert.ToDouble(dr["ADT"]);
//			_dblUPT = Convert.ToDouble(dr["UPT"]);
			
		}
 
		internal static LP_Order GetOrder(SqlDataReader dr)
		{
			LP_Order _order = new LP_Order();
			_order.PopulateProperties(dr);
			return _order;
		}

		

		public static LP_Order GetTheOneOrder(string strOrderNo)	        
		{
			LP_Order theOneOrder = new LP_Order();
			theOneOrder.OneOrderFetch(strOrderNo);
			return theOneOrder;
		}
		
		private void OneOrderFetch(string strOrderNo)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();


			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				//cm.CommandText = "OrderSearchDetail_byOrdNo_PrdPTL";

                cm.CommandText = "OrderSearchDetail_byOrdNo_LP";
				
				SqlDataReader dr;              
				cm.Parameters.Clear();

				SqlParameter nnn = new SqlParameter();
				nnn.ParameterName = "@OrderNo";
				nnn.SqlDbType = SqlDbType.VarChar;
				nnn.Value = strOrderNo;
				cm.Parameters.Add(nnn);

				dr = cm.ExecuteReader();
				
				try
				{
					if (dr.Read())
					{
						this.PopulateProperties(dr);

					}
				}
				finally
				{
					dr.Close();
				}

			}
			catch(Exception ex)
			{
				//int s=0;

			}
			finally
			{
				cn.Close();
			}
		}
		



	}
}

