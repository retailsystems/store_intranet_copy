using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;

using System.Text;
using System.Reflection;

using System.Reflection.Emit;
using System.Diagnostics; 
using System.IO;


namespace ShipToStore
{
	/// <summary>
	/// Summary description for Utility.
	/// </summary>
	public class Utility
	{

			public Utility()
			{
				//
				// TODO: Add constructor logic here
				//
			}
		public static string ParseDepartment(string strDept)
		{
			if (strDept.Length>1)
			{
				if (strDept.Substring(1,1)=="5")
				{
					//this is regional director, return last two digits
					strDept = strDept.Substring(2, 2);
	
				}
				else
				{
					//this is Ditrict Manager, return last 3 digits
					strDept = strDept.Substring(1,3);

				}
			}
			else
			{
				strDept = null;
			}
			
			return strDept;
		}
		
		public static bool CheckSecurity(int permit, string region, string district, string store, string userCode, string userLoc, string userRegion)
		{
			
			bool isOK = false;
			switch (permit)
			{
				case 1:
					isOK = true;
					break;
				case 5:
					isOK = true;
					break;
				case 2:
					//RD, see if they are inside their own region
					if (userCode == region)
					{
						isOK = true;
					}
					break;
					
				case 3:
					//DM, see if they are inside their own region and district
					if (userCode== district && userRegion==region)
					{
						isOK = true;
					}
					break;
				case 4:
					//Store, see if they are inside their own store
					if (userLoc== store)
					{
						isOK = true;
					}
					break;

				case 6:
					if (userLoc == "8000")
					{
						isOK = true;
					}
					break;
			}
			return isOK;

		}


		/// <summary>

		///

		/// </summary>

		/// <param name="o"></param>

		/// <returns></returns>

		public static DataTable ConvertToDataTable(OrderGroup group)

		{

			PropertyInfo[] properties = group.GetType().GetElementType().GetProperties();
			DataTable dt = CreateDataTable(properties);

 

			if (group.Count!= 0)

			{

				foreach(object o in group)

					FillData(properties, dt, o);

 

			}

 

			return dt;

		}


		private static DataTable CreateDataTable(PropertyInfo[] properties)

		{

			DataTable dt = new DataTable();

			DataColumn dc = null;

 

			foreach(PropertyInfo pi in properties)

			{

				dc = new DataColumn();

				dc.ColumnName = pi.Name;

				dc.DataType = pi.PropertyType;

                               

				dt.Columns.Add(dc);                            

			}

 

			return dt;

		}


		private static void FillData(PropertyInfo[] properties, DataTable dt, Object o)

		{

			DataRow dr = dt.NewRow();

 

			foreach(PropertyInfo pi in properties)

				dr[pi.Name] = pi.GetValue(o, null);

 

			dt.Rows.Add(dr);       

		}

        public static int CountFullOrdNos(DataTable dtCountFullOrdNo)  //See if it is the split order, if yes, will send email for partial order received. 
       {
         // see if it is split order
        //dtCountFullOrdNo = Data.GetOrdPikUpStatus(strFullOrderNo.Substring(0, 8));
        int nCountFullOrdNo = 0;
        string strDiffFullOrd = string.Empty; 
        foreach (DataRow row in dtCountFullOrdNo.Rows)
        {
            string strFullOrd =  row["FULLORDERNO"].ToString();
           
            if ((strFullOrd!="") && (strFullOrd !=null))
            {
                if (nCountFullOrdNo == 0)
                {
                    strDiffFullOrd = strFullOrd;
                    nCountFullOrdNo = nCountFullOrdNo + 1;
                }
                else 
                {
                    if (strDiffFullOrd != strFullOrd)
                    {
                        nCountFullOrdNo = nCountFullOrdNo + 1;
                    }
                }
            }
        }
        return nCountFullOrdNo;
       }

 

	

	

		
	}
}
