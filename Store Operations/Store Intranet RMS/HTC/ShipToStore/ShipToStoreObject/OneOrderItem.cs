using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for OrderItem.
	/// </summary>
	public class OneOrderItem
	{
		public OneOrderItem() {}

		#region Members
		string _strItemNo= String.Empty;
		string _strItemDESC = String.Empty;
		string _strQtyOrd = String.Empty;
		string _strQtyShp = String.Empty;
		string _strQtyCancel = String.Empty;   
		string _strOrderNo = String.Empty;
		string _strFullOrderNo = String.Empty;
		string _strSprice = String.Empty;
		string _strTotSprice = String.Empty;
		string _strCprice = String.Empty;
		string _strTax = String.Empty;
		#endregion Members

		#region Properties
		public string StrItemNo
		{
			get { return _strItemNo; }
			set { _strItemNo = value; }
		}
		public string StrItemDESC
		{
			get { return _strItemDESC; }
			set { _strItemDESC = value; }
		}
		public string StrQtyOrd
		{
			get { return _strQtyOrd; }
			set { _strQtyOrd = value; }
		}
		public string StrQtyShp
		{
			get { return _strQtyShp; }
			set { _strQtyShp = value; }
		}
		public string StrQtyCancel
		{
			get { return _strQtyCancel; }
			set { _strQtyCancel = value; }
		}
		public string StrOrderNo
		{
			get { return _strOrderNo; }
			set { _strOrderNo = value; }
		}
		public string StrFullOrderNo
		{
			get { return _strFullOrderNo; }
			set { _strFullOrderNo = value; }
		}
		public string StrSprice
		{
			get { return _strSprice; }
			set { _strSprice = value; }
		}
		public string StrTotSprice
		{
			get { return _strTotSprice; }
			set { _strTotSprice = value; }
		}
		public string StrCprice
		{
			get { return _strCprice; }
			set { _strCprice = value; }
		}
		public string StrTax
		{
			get { return _strTax; }
			set { _strTax = value; }
		}
		#endregion Properties

		public void PopulateItemProperties(SqlDataReader dr)
		{
			_strItemNo = Convert.ToString(dr["ITEMNO"]); 
			_strItemDESC = Convert.ToString(dr["DESCRIPTION"]);
			_strQtyOrd = Convert.ToString(dr["TOTALQTY"]);
			_strQtyShp = Convert.ToString(dr["SQTY"]);
			_strQtyCancel = Convert.ToString(dr["CQTY"]);
			_strOrderNo = Convert.ToString(dr["ORDERNO"]);
			_strFullOrderNo = Convert.ToString(dr["FULLORDERNO"]);
			_strSprice = Convert.ToString(dr["SPRICE"]);
			_strTotSprice = Convert.ToString(dr["TOTSPRICE"]);
			if (HasColumn(dr,"CPRICE"))
				_strCprice = Convert.ToString(dr["CPRICE"]);
			if (HasColumn(dr, "TAX"))
				_strTax = Convert.ToString(dr["TAX"]);
		}

		internal static OneOrderItem GetOrderItem(SqlDataReader dr)
		{
			OneOrderItem _oneOrderItem = new OneOrderItem ();
			_oneOrderItem.PopulateItemProperties(dr);
			return _oneOrderItem;
		}

		public static bool HasColumn(IDataReader dr, string columnName)
		{
			for (int i=0; i < dr.FieldCount; i++)
			{
				if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
					return true;
			}
			return false;
		}
	}
}