using System;
using System.Text;
using System.Web.Mail;
using System.IO;
using System.Data.SqlClient; 
using System.Configuration;

namespace ShipToStore
{  

	public class Emails
	{
       
		#region Members
		private MailMessage _smtp;
		private string _sender;
		private string _recipients;
		private string _subject;
		private string _body;
        private string _attachment;
		#endregion Members

		#region Properties
		/// <summary>
		/// List of recipients to send email to.
		/// </summary>
		public String To
		{
			get { return _smtp.To; }
			set { _smtp.To = value; }
		}

		/// <summary>
		/// Subject in the header of the email.
		/// </summary>
		public String Subject
		{
			get { return _smtp.Subject; }
			set { _smtp.Subject = value; }
		}

		/// <summary>
		/// Message in the body of the email.
		/// </summary>
		public String Message
		{
			get { return _smtp.Body; }
			set 
			{
					_smtp.Body = value; 
				_smtp.BodyFormat = MailFormat.Html;
			}
		} 
		public string Attachment
		{
			get { return _attachment; }
			set { _attachment = value; }
		}
		#endregion Properties
		#region Constructors
        
		public Emails()
		{
			Initialize();
		}
		

		public static void SendHTML(String To, String Subject, String Message)
		{
			
			//newHtmMail.BodyFormat = MailFormat.Html;
			SmtpMail.Send(EmailSender, To, Subject, Message);

		}

		/// <summary>
		/// Optional Constructor with Properties.
		/// </summary>
		/// <param name="To">List of recipients to send email to.</param>
		/// <param name="Subject">Subject in the header of the email.</param>
		/// <param name="Message">Message in the body of the email.</param>
		public Emails(String To, String Subject, String Message, String Attachment)
		{
			Initialize();
			_recipients = To;
			_subject = Subject;
			_body = Message;
			_attachment = Attachment;

		}
		#endregion Constructors

		#region Methods
		/// <summary>
		/// Sends the email.
		/// </summary>
		//		public void SendText()
		//		{
		//			SmtpMail.Send(_sender, _recipients, _subject, _body);
		//			
		//		}
		//		

		/// <summary>
		/// Static function to send an email.
		/// </summary>
		/// <param name="To">List of recipients to send email to.</param>
		/// <param name="Subject">Subject in the header of the email.</param>
		/// <param name="Message">Message in the body of the email.</param>
		public static void SendText(String To, String Subject, String Message)
		{
			SmtpMail.SmtpServer = "filter";
			MailMessage _smtp = new MailMessage();

			_smtp.To = To; 
      
			_smtp.From= EmailSender;
			_smtp.Subject = Subject;
			_smtp.Body = Message;

			_smtp.BodyFormat = MailFormat.Html;
			SmtpMail.Send(_smtp);
	
		}


		/// <summary>
		/// Internal method to initialize class variables.
		/// </summary>
		private void Initialize()
		{
			// Initialize class variable
			//MailMessage _smtp = new MailMessage();
			_smtp = new MailMessage();
			
		
			// Name of smtp server from configuration
			SmtpMail.SmtpServer = EmailServer;
			
			// Sender email address from configuration
			_sender = EmailSender;

			
		}

		public static String EmailSender
		{
			get { return ConfigurationSettings.AppSettings["EmailSender"].ToString(); }
		}

		public static String EmailServer
		{
			get { return ConfigurationSettings.AppSettings["EmailServer"].ToString(); }

		}
		
		#endregion Methods


	}
} 
