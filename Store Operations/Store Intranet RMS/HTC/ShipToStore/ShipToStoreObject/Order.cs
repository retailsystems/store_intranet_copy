using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for Order.
	/// </summary>
	/// 
	  [Serializable]
	public class Order
	{
		#region Members
		string _strRegionID= String.Empty;
		string _strDistrictID = String.Empty;
		string _strStoreID = String.Empty;
		//int _intStoreID;
		int _intCompanyID;

		string _strCustFirstName = String.Empty;
		string _strCustLastName = String.Empty;
		string _strCustDayPhone = String.Empty;
		string _strCustNightPhone = String.Empty;
		string _strCustName = String.Empty;
		string _strEmail = String.Empty;

		string _strOrderNo = String.Empty;
		DateTime _dtOrderDate;
		string _strPaymentType = String.Empty;
		double _dblOrderTotal;

		DateTime _dtShippedDate;
		string _strShippedSource = String.Empty;
		string _strShipMethod = String.Empty;
		string _strTrackingNo = String.Empty;
		double _dblOrderWeight;

		DateTime _dtReceivedDate;
		string _strStoreNotes = String.Empty;
		DateTime _dtCustPickUpDate;
		string _strPackageNo = String.Empty; 
		string _strStatus = String.Empty;
		string _strRecFullOrdNo = String.Empty;

		//double _dblTotalSales = 0;
		int _intOrderUnit= 0;
//		double _dblADT = 0;
//		double _dblUPT = 0;
		int _cancelledunits = 0;

		string _channel = string.Empty;
		#endregion Members

		#region Properties
		public string StrRegionID
		{
			get { return _strRegionID; }
			set { _strRegionID = value; }
		}
		public string StrStoreID
		{
			get { return _strStoreID; }
			set { _strStoreID = value; }
		}
		public string StrDistrictID
		{
			get { return _strDistrictID; }
			set { _strDistrictID = value; }
		}
		
		public int IntCompanyID
		{
			get {  return _intCompanyID; }
			set { _intCompanyID = value; }
		}
		public string StrCustFirstName
		{
			get { return _strCustFirstName; }
			set { _strCustFirstName = value; }
		}
		public string StrCustLastName
		{
			get { return _strCustLastName; }
			set { _strCustLastName = value; }
		}
		public string StrCustName
		{
			get { return _strCustName; }
			set { _strCustName = value; }
		}
		public string StrEmail
		{
			get { return _strEmail; }
			set { _strEmail = value; }
		}
		public string StrCustDayPhone
		{
			get { return _strCustDayPhone; }
			set { _strCustDayPhone = value; }
		}
		public string StrCustNightPhone
		{
			get { return _strCustNightPhone; }
			set { _strCustNightPhone = value; }
		}
		public string StrOrderNo
		{
			get { return _strOrderNo; }
			set { _strOrderNo = value; }
		}
		public DateTime DtOrderDate 
		{
			get { return _dtOrderDate; }
			set { _dtOrderDate = value; }
		}
		public string StrPaymentType
		{
			get { return _strPaymentType; }
			set { _strPaymentType = value; }
		}

		public double DblOrderTotal
		{
			get { return _dblOrderTotal; }
			set { _dblOrderTotal = value; }
		}

		public int IntOrderUnits
		{
			get { return _intOrderUnit; }
			set { _intOrderUnit = value; }
		}

		public DateTime DtShippedDate 
		{
			get { return _dtShippedDate; }
			set { _dtShippedDate = value; }
		}
		public string StrShippedSource
		{
			get { return _strShippedSource; }
			set { _strShippedSource = value; }
		}
		public string StrShipMethod
		{
			get { return _strShipMethod; }
			set { _strShipMethod = value; }
		}
		public string StrTrackingNo
		{
			get { return _strTrackingNo; }
			set { _strTrackingNo = value; }
		}

		public double DblOrderWeight
		{
			get { return _dblOrderWeight; }
			set { _dblOrderWeight = value; }
		}
		public DateTime DtReceivedDate 
		{
			get { return _dtReceivedDate; }
			set { _dtReceivedDate = value; }
		}
		public string StrStoreNotes
		{
			get { return _strStoreNotes; }
			set { _strStoreNotes = value; }
		}
		public DateTime DtCustPickUpDate 
		{
			get { return _dtCustPickUpDate; }
			set { _dtCustPickUpDate = value; }
		}
		public string StrPackageNo
		{
			get { return _strPackageNo; }
			set { _strPackageNo = value; }
		}
		public string StrStatus
		{
			get { return _strStatus; }
			set { _strStatus = value; }
		}

		public string StrRecFullOrdNo
		{
			get { return _strRecFullOrdNo; }
			set { _strRecFullOrdNo = value; }
		}

		public string Channel { get { return _channel; } set { _channel = value; } }
		public int CancelledUnits { get { return _cancelledunits; } set { _cancelledunits = value; } }
		#endregion Properties

		public Order()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public void PopulateProperties(SqlDataReader dr)
		{
			_strRegionID= Convert.ToString(dr["REGION"]);
			_strDistrictID= Convert.ToString(dr["DISTRICT"]);
			_strStoreID = Convert.ToString(dr["STORENO"]);
			_strOrderNo = Convert.ToString(dr["ORDERNO"]);
			if (dr["ENTRYDATE"]!=System.DBNull.Value)
			{
				_dtOrderDate = Convert.ToDateTime(dr["ENTRYDATE"]);
			}
			else
			{
				_dtOrderDate = DateTime.Parse("1/1/1980");
			}
			_strPaymentType = Convert.ToString(dr["PAYMETHOD"]);
			_strCustFirstName = Convert.ToString(dr["FNAME"]);
			_strCustLastName = Convert.ToString(dr["LNAME"]);
			_strCustName = Convert.ToString(dr["CNAME"]);
			_strCustDayPhone = Convert.ToString(dr["DAYPHONE"]);
			_strEmail = Convert.ToString(dr["EMAIL"]);
			_strShipMethod = Convert.ToString(dr["SHIPMETHOD"]);
			if (dr["PRODUCTDOLLARS"]!=System.DBNull.Value)
			{
				_dblOrderTotal = Convert.ToDouble(dr["PRODUCTDOLLARS"]);
			}		
			if (dr["QTY"]!=System.DBNull.Value)
			{
				_intOrderUnit = Convert.ToInt16(dr["QTY"]);
			}

			if (dr["SHIPDATE"]!=System.DBNull.Value)
			{
				_dtShippedDate = Convert.ToDateTime(dr["SHIPDATE"]); 
			}
			else
			{
				_dtShippedDate = DateTime.Parse("1/1/1980");
			}
			if (dr["ORDERWEIGHT"]!=System.DBNull.Value)
			{
				_dblOrderWeight = Convert.ToDouble(dr["ORDERWEIGHT"]);
			}
			else
			{
				_dblOrderWeight =0;
			}
			_strTrackingNo = Convert.ToString(dr["TRACKNO"]);
			if (dr["DATERECINSTORE"]!=System.DBNull.Value)
			{
				_dtReceivedDate = Convert.ToDateTime(dr["DATERECINSTORE"]);
			}
			else
			{
				_dtReceivedDate = DateTime.Parse("1/1/1980");
			}

			_strShippedSource = Convert.ToString(dr["SHIPSOURCE"]);
			_strStoreNotes = Convert.ToString(dr["NOTES"]);
			if (dr["DATECUSTPICKUP"]!=System.DBNull.Value)
			{
				_dtCustPickUpDate = Convert.ToDateTime(dr["DATECUSTPICKUP"]);
			}
			else
			{
				_dtCustPickUpDate = DateTime.Parse("1/1/1980");
			}
								
			_strPackageNo = Convert.ToString(dr["PACKAGENO"]);
            _strStatus = Convert.ToString(dr["STATUS"]);
			_strRecFullOrdNo = Convert.ToString(dr["FULLORDERNO"]);
            
			
//			_dblTotalSales = Convert.ToDouble(dr["TotalGrossSales"]);
//			_strTotalOrder = Convert.ToString (dr["TotalOrder"]);
//			_dblADT = Convert.ToDouble(dr["ADT"]);
//			_dblUPT = Convert.ToDouble(dr["UPT"]);
			_channel = CalculateChannel(Convert.ToString(dr["SOURCE"]));

			if (dr["CANCELQUANTITY"] != System.DBNull.Value)
			{
				_cancelledunits = Convert.ToInt32(dr["CANCELQUANTITY"]);
			}
		}
 
		internal static Order GetOrder(SqlDataReader dr)
		{
			Order _order = new Order ();
			_order.PopulateProperties(dr);
			return _order;
		}

		

		public static Order GetTheOneOrder(string strOrderNo)	        
		{
			Order theOneOrder = new Order();
			theOneOrder.OneOrderFetch(strOrderNo);
			return theOneOrder;
		}
		
		private void OneOrderFetch(string strFullOrderNo)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();
            string strCompany = ConfigurationSettings.AppSettings["Company"];


			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				cm.CommandText = "OrderSearchDetail_byOrdNo_PrdPTL";
				
				SqlDataReader dr;              
				cm.Parameters.Clear();

				SqlParameter p1 = new SqlParameter();
				p1.ParameterName = "@FullOrderNo";
				p1.SqlDbType = SqlDbType.VarChar;
                p1.Value = strFullOrderNo;
                cm.CommandTimeout = 600;
				cm.Parameters.Add(p1);
                cm.Parameters.Add(new SqlParameter("@Company", strCompany));
			
				dr = cm.ExecuteReader();
				
				try
				{
					if (dr.Read())
					{
						this.PopulateProperties(dr);

					}
				}
				finally
				{
					dr.Close();
				}

			}
			catch(Exception ex)
			{
				//int s=0;

			}
			finally
			{
				cn.Close();
			}
		}

		private string CalculateChannel(string source)
		{
			if (source.Length > 1)
			{
				string temp = source.Substring(0, 2);
				switch (temp)
				{
					case "PS":
						return "POS";
						break;
					case "EC":
						return "ECOM";
						break;
					case "KI":
						return "HTC";
						break;
					default:
						return string.Empty;
				}
			}
			else
			{
				return string.Empty;
			}
		}
	}
}

