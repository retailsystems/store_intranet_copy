using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Text;
using System.IO;


namespace ShipToStore
{
	/// <summary>
	/// Summary description for OrderGroup.
	/// </summary>
	/// 
    [Serializable]
	public class LP_OrderGroup: CollectionBase
	{
		private double _totalAmount;
		private int _totalUnit;
			
		
	

		public LP_OrderGroup()
		{
			
		}


		public Order Item(int index)
		{
			IEnumerator myEnumer = List.GetEnumerator();
			for (int pos = 0; pos <= index; pos++)
				myEnumer.MoveNext();
			return (Order)myEnumer.Current;
		}


		public void Add(Order order)
		{
			if (!List.Contains(order))
			{
				List.Add(order);
              
               
			}
			else
			{
				throw new IndexOutOfRangeException("object already exists in this list.");
			}
		}

		public double TotalAmount
		{
			get { return _totalAmount; }
			set { _totalAmount = value; }
		}

		public int TotalUnit
		{
			get { return _totalUnit; }
			set { _totalUnit = value; }
		}

        public static LP_OrderGroup GetOrderGroup(string strReg, string strDist, string strStr, DateTime strOrdFDt, DateTime strOrdTDt, DateTime strShpFDt, DateTime strShpTDt, string strOrderNo, string strFName, string strLName, string strEmail, int intStat, int intSource, int intBrand, int intChannel, int PageNumber, int PageSize, int Emp, out int Totalpages)	        
		{
			LP_OrderGroup theOrderGroup = new LP_OrderGroup();
			strOrdTDt.AddHours(24);
			strShpTDt.AddHours(24);
//			strStr = int.Parse(strStr).ToString();
			
			//Receive In Store or Picked up already
			theOrderGroup.OrderGroup_Fetch(strReg, strDist, strStr,strOrdFDt,strOrdTDt,strShpFDt,strShpTDt,strOrderNo,strFName,strLName,strEmail, intStat, intSource, intBrand, intChannel,PageNumber,PageSize, Emp, out Totalpages);
			
			return theOrderGroup;
		}
		//add channel to filter
        public static LP_OrderGroup GetOrderGroupFilter(string strReg, string strDist, string strStr, DateTime strOrdFDt, DateTime strOrdTDt, DateTime strShpFDt, DateTime strShpTDt, string strOrderNo, string strFName, string strLName, string strEmail, int intStat, int intSource, int intBrand, int intChannel, int PageNumber, int PageSize, int Emp, out int Totalpages)
		{
			LP_OrderGroup theOrderGroupF = new LP_OrderGroup();
			strOrdTDt.AddHours(24);
			strShpTDt.AddHours(24);
//			strStr = int.Parse(strStr).ToString();
			
			//Receive In Store or Picked up already
			//theOrderGroupF.OrderGroupFilter_Fetch(strReg, strDist, strStr,strOrdFDt,strOrdTDt,strShpFDt,strShpTDt,strOrderNo,strFName,strLName,strEmail,intStat, intSource, intBrand, intChannel);
            theOrderGroupF.OrderGroup_Fetch(strReg, strDist, strStr, strOrdFDt, strOrdTDt, strShpFDt, strShpTDt, strOrderNo, strFName, strLName, strEmail, intStat, intSource, intBrand, intChannel, PageNumber, PageSize, Emp, out Totalpages);
			
			return theOrderGroupF;
		}
		
		private void OrderGroupFetch(string strStr,DateTime strOrdFDt,DateTime strOrdTDt,DateTime strShpFDt,DateTime strShpTDt,string strOrderNo,string strFName,string strLName,string strEmail)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();

			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				//cm.CommandText = "OrderView_Select";
				cm.CommandText = "OrdersSearchView_WithRollup";
				
				SqlDataReader dr;              
				cm.Parameters.Clear();
				
				//cm.Parameters.Add(("@StoreNo", strStr);
				SqlParameter xxx= new SqlParameter();
				xxx.ParameterName="@StoreNo";
				xxx.SqlDbType=SqlDbType.VarChar;
				xxx.Value=strStr;
				cm.Parameters.Add(xxx); 

				SqlParameter aaa = new SqlParameter();
				aaa.ParameterName = "@OrderNo";
				aaa.SqlDbType = SqlDbType.VarChar;
				aaa.Value = strOrderNo;
				cm.Parameters.Add(aaa);

				SqlParameter bbb = new SqlParameter();
				bbb.ParameterName = "@OrderFDate";
				bbb.SqlDbType = SqlDbType.DateTime;
				bbb.Value = strOrdFDt;
				cm.Parameters.Add(bbb); 
				//cm.Parameters.Add("@StoreNo", SqlDbType.VarChar, 4, strStr); 
		
				SqlParameter ccc = new SqlParameter();
				ccc.ParameterName = "@OrderTDate";
				ccc.SqlDbType = SqlDbType.DateTime;
				ccc.Value = strOrdTDt;
				cm.Parameters.Add(ccc); 
				
				

				SqlParameter ddd = new SqlParameter();
				ddd.ParameterName = "@ShipFDate";
				ddd.SqlDbType = SqlDbType.DateTime;
				
					ddd.Value = strShpFDt;
				
				cm.Parameters.Add(ddd); 

				SqlParameter eee = new SqlParameter();
				eee.ParameterName = "@ShipTDate";
				eee.SqlDbType = SqlDbType.DateTime;
				
					eee.Value = strShpTDt;
				
				
				cm.Parameters.Add(eee); 

				SqlParameter fff = new SqlParameter();
				fff.ParameterName = "@FName";
				fff.SqlDbType = SqlDbType.VarChar;
				fff.Value = strFName;
				cm.Parameters.Add(fff); 

				SqlParameter ggg = new SqlParameter();
				ggg.ParameterName = "@LName";
				ggg.SqlDbType = SqlDbType.VarChar;
				ggg.Value = strLName;
				cm.Parameters.Add(ggg); 

				SqlParameter hhh = new SqlParameter();
				hhh.ParameterName = "@Email";
				hhh.SqlDbType = SqlDbType.VarChar;
				hhh.Value = strEmail;
				cm.Parameters.Add(hhh); 


				dr = cm.ExecuteReader();
				
				while (dr.Read())
					List.Add(Order.GetOrder(dr));
			}
			catch(Exception ex)
			{
				//int s=0;

			}
			finally
			{
				cn.Close();
			}
		}
		
		//fetch OrderGroup regardless of 
		private void OrderGroup_Fetch(
			string strReg, string strDist, string strStr,
			DateTime strOrdFDt,DateTime strOrdTDt,DateTime strShpFDt,DateTime strShpTDt,
			string strOrderNo,
			string strFName, string strLName,string strEmail, int intStat, int intSource, int intBrand, int intChannel,int PageNumber,int PageSize, int intEmp, out int TotalPages)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();

			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				//cm.CommandText = "OrdersSearchDetail";
				//cm.CommandText = "OrderView_Select";
				//cm.CommandText = "OrderView_Select_change4ManytoMany";
                cm.CommandText = "OrderView_Select_Paged";
				//cm.CommandText = "OrderView_Select_Source_PTL";
			    
				
				cm.CommandTimeout = 450; //120;
				
				SqlDataReader dr;              
				cm.Parameters.Clear();


				cm.Parameters.Add(new SqlParameter("@Region", strReg));
				cm.Parameters.Add(new SqlParameter("@District", strDist));
			
			
				SqlParameter xxx= new SqlParameter();
				xxx.ParameterName="@StoreNo";
				xxx.SqlDbType=SqlDbType.VarChar;
				xxx.Value=strStr;
				cm.Parameters.Add(xxx); 

				SqlParameter aaa = new SqlParameter();
				aaa.ParameterName = "@OrderNo";
				aaa.SqlDbType = SqlDbType.VarChar;
				aaa.Value = strOrderNo;
				cm.Parameters.Add(aaa);

				SqlParameter bbb = new SqlParameter();
				bbb.ParameterName = "@EntryDateFrom";
				bbb.SqlDbType = SqlDbType.DateTime;
				bbb.Value = strOrdFDt;
				cm.Parameters.Add(bbb); 
				//cm.Parameters.Add("@StoreNo", SqlDbType.VarChar, 4, strStr); 
		
				SqlParameter ccc = new SqlParameter();
				ccc.ParameterName = "@EntryDateTo";
				ccc.SqlDbType = SqlDbType.DateTime;
				ccc.Value = strOrdTDt;
				cm.Parameters.Add(ccc); 

				SqlParameter ddd = new SqlParameter();
				ddd.ParameterName = "@ShipDateFrom";
				ddd.SqlDbType = SqlDbType.DateTime;
				ddd.Value = strShpFDt;
				cm.Parameters.Add(ddd); 

				SqlParameter eee = new SqlParameter();
				eee.ParameterName = "@ShipDateTo";
				eee.SqlDbType = SqlDbType.DateTime;
				eee.Value = strShpTDt;
				cm.Parameters.Add(eee); 

				SqlParameter fff = new SqlParameter();
				fff.ParameterName = "@FirstName";
				fff.SqlDbType = SqlDbType.VarChar;
				fff.Value = strFName;
				cm.Parameters.Add(fff); 

				SqlParameter ggg = new SqlParameter();
				ggg.ParameterName = "@LastName";
				ggg.SqlDbType = SqlDbType.VarChar;
				ggg.Value = strLName;
				cm.Parameters.Add(ggg); 

				SqlParameter hhh = new SqlParameter();
				hhh.ParameterName = "@Email";
				hhh.SqlDbType = SqlDbType.VarChar;
				hhh.Value = strEmail;
				cm.Parameters.Add(hhh); 

				cm.Parameters.Add(new SqlParameter("@Status", intStat));
				cm.Parameters.Add(new SqlParameter("@Source", intSource));
				cm.Parameters.Add(new SqlParameter("@Brand", intBrand));
				cm.Parameters.Add(new SqlParameter("@Channel", intChannel));

    			//Jay T - 2/5/2010 - added new parameter
				SqlParameter IsEmployee = new SqlParameter();
				IsEmployee.ParameterName = "@IsEmp";
				IsEmployee.SqlDbType = SqlDbType.Int;
				IsEmployee.Value = intEmp; 
				cm.Parameters.Add(IsEmployee);

                cm.Parameters.Add(new SqlParameter("@PageIndex", PageNumber));
                cm.Parameters.Add(new SqlParameter("@PageSize", PageSize));



                SqlParameter TotalPagesParameter = new SqlParameter();
                TotalPagesParameter.ParameterName = "@TotalPages";
                TotalPagesParameter.Direction = ParameterDirection.InputOutput;
                TotalPagesParameter.SqlDbType = SqlDbType.Int;
                TotalPagesParameter.DbType = DbType.Int32;
                TotalPagesParameter.Value = 0;
                TotalPagesParameter.Size = 99999;
                cm.Parameters.Add(TotalPagesParameter);
                
                
                dr = cm.ExecuteReader();
				
				while (dr.Read())
				{
					LP_Order newOrder = LP_Order.GetOrder(dr);
					List.Add(newOrder);
					this._totalAmount += newOrder.DblOrderTotal;
					this._totalUnit+= newOrder.IntOrderUnits;
				}
			}
			catch(Exception ex)
			{
				int s = 0;

			}
			finally
			{
                cn.Close();
			}

            TotalPages = (int)cm.Parameters["@TotalPages"].Value;


		}
		//fetch OrderGroup for filter
		private void OrderGroupFilter_Fetch(
			string strReg, string strDist, string strStr,
			DateTime strOrdFDt,DateTime strOrdTDt,DateTime strShpFDt,DateTime strShpTDt,
			string strOrderNo,
			string strFName, string strLName,string strEmail, int intStat, int intSource, int intBrand, int intChannel)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();

			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				//cm.CommandText = "OrdersSearchDetail";
				//cm.CommandText = "OrderView_Select";
				//cm.CommandText = "OrderView_Select_change4ManytoMany";
				//cm.CommandText = "OrderView_Select_Filt";
				cm.CommandText = "OrderView_Select_Filt_PTL";
				
				cm.CommandTimeout = 400; //120;
				
				SqlDataReader dr;              
				cm.Parameters.Clear();


				cm.Parameters.Add(new SqlParameter("@Region", strReg));
				cm.Parameters.Add(new SqlParameter("@District", strDist));
			
			
				SqlParameter xxx= new SqlParameter();
				xxx.ParameterName="@StoreNo";
				xxx.SqlDbType=SqlDbType.VarChar;
				xxx.Value=strStr;
				cm.Parameters.Add(xxx); 

				SqlParameter aaa = new SqlParameter();
				aaa.ParameterName = "@OrderNo";
				aaa.SqlDbType = SqlDbType.VarChar;
				aaa.Value = strOrderNo;
				cm.Parameters.Add(aaa);

				SqlParameter bbb = new SqlParameter();
				bbb.ParameterName = "@EntryDateFrom";
				bbb.SqlDbType = SqlDbType.DateTime;
				bbb.Value = strOrdFDt;
				cm.Parameters.Add(bbb); 
				//cm.Parameters.Add("@StoreNo", SqlDbType.VarChar, 4, strStr); 
		
				SqlParameter ccc = new SqlParameter();
				ccc.ParameterName = "@EntryDateTo";
				ccc.SqlDbType = SqlDbType.DateTime;
				ccc.Value = strOrdTDt;
				cm.Parameters.Add(ccc); 

				SqlParameter ddd = new SqlParameter();
				ddd.ParameterName = "@ShipDateFrom";
				ddd.SqlDbType = SqlDbType.DateTime;
				ddd.Value = strShpFDt;
				cm.Parameters.Add(ddd); 

				SqlParameter eee = new SqlParameter();
				eee.ParameterName = "@ShipDateTo";
				eee.SqlDbType = SqlDbType.DateTime;
				eee.Value = strShpTDt;
				cm.Parameters.Add(eee); 

				cm.Parameters.Add(new SqlParameter("@Status", intStat));
				cm.Parameters.Add(new SqlParameter("@Source", intSource));
				cm.Parameters.Add(new SqlParameter("@Brand", intBrand));
				cm.Parameters.Add(new SqlParameter("@Channel", intChannel));

				dr = cm.ExecuteReader();
				
				while (dr.Read())
				{
					Order newOrder = Order.GetOrder(dr);
					List.Add(newOrder);
					this._totalAmount += newOrder.DblOrderTotal;
					this._totalUnit+= newOrder.IntOrderUnits;
				}
			}
			catch(Exception ex)
			{
				int s = 0;

			}
			finally
			{
				cn.Close();
			}
		}
	}
}
