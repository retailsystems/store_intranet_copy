using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for User.
	/// </summary>
	public class AppUser
	{
		#region Members

		string _strEmployeeID = String.Empty;
		int _intStoreID;
		string _strJobCode = String.Empty;
		string _strPassword = String.Empty;
		bool _bValidUser;
		bool _bValidStore;
		string _strUserName = String.Empty;
		string _strUserID = String.Empty;

		#endregion Members

		#region Properties

		public string StrEmployeeID
		{
			get { return _strEmployeeID; }
			set { _strEmployeeID = value; }
		}
		public int IntStoreID
		{
			get {  return _intStoreID; }
			set { _intStoreID = value; }
		}
		public string StrJobCode
		{
			get { return _strJobCode; }
			set { _strJobCode = value; }
		}
		public string StrPassword
		{
			get { return _strPassword; }
			set { _strPassword = value; }
		}
		public bool BValidUser
		{
			get { return _bValidUser; }
			set { _bValidUser = value;}
		}
		public bool BValidStore
		{
			get { return _bValidStore; }
			set { _bValidStore = value;}
		}
		public string StrUserName
		{
			get { return _strUserName; }
			set { _strUserName = value; }
		}
		public string StrUserID
		{
			get { return _strUserID; }
			set { _strUserID = value; }
		}
		#endregion Properties
		public AppUser()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static DataTable GetUserProfile(string strEmpID)
		{
			DataTable dtProfile;
			char chrZero= '0';
			string strEmployeeID = strEmpID.PadLeft(6,chrZero);  //change for 6 character employeeID increase

			SqlParameter[] Parameters = new SqlParameter[1];
			Parameters[0] = new SqlParameter("@EmpId", SqlDbType.VarChar);
			Parameters[0].Size = 6;
			Parameters[0].Value = strEmployeeID;
			dtProfile = Data.ExecuteStoredProcedure("spGetEmployeeInfo", Parameters, ConfigurationSettings.AppSettings["strEmpSQLConnection"].ToString());
		
			return dtProfile;
		}
		public static DataTable GetLPUserProfile(string strEmpID)
		{
			DataTable dtLPProfile;
			char chrZero= '0';
			string strEmployeeID = strEmpID.PadLeft(6,chrZero);  //change for 6 character employeeID increase

			SqlParameter[] Parameters = new SqlParameter[1];
			Parameters[0] = new SqlParameter("@EmpId", SqlDbType.VarChar);
			Parameters[0].Size = 6;
			Parameters[0].Value = strEmployeeID;
			dtLPProfile = Data.ExecuteStoredProcedure("spHTCGetEmpInfo", Parameters, ConfigurationSettings.AppSettings["strEmpSQLConnection"].ToString());
		
			return dtLPProfile;
		}
		public static int GetUserFlag(string strEmpID)
		{  
            int intUserFlag = 0;
			DataTable dtUserFlag;
			char chrZero= '0';
			string strEmployeeID = strEmpID.PadLeft(6,chrZero);  //change for 6 character employeeID increase

			try
			{
				SqlParameter[] Parameters = new SqlParameter[1];
				Parameters[0] = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
				Parameters[0].Size = 6;
				Parameters[0].Value = strEmployeeID;
				dtUserFlag = Data.ExecuteStoredProcedure("spHTCFindLPUser", Parameters, ConfigurationSettings.AppSettings["strEmpSQLConnection"].ToString());

                if (dtUserFlag.Rows.Count > 0)
                {
                    intUserFlag = int.Parse(dtUserFlag.Rows[0]["FLAG"].ToString());
                }
                else 
                {
                    intUserFlag = 2;
                }
			}
			catch(Exception ex)
			{
               throw ex;
			}

			return intUserFlag;

		}

		public static int SetUserPermission(string strDepartment, string strJobCode)
		{
			int intPermit;
			if (strDepartment=="8000" || strDepartment=="9001" || strDepartment=="9000")
			{
				//HQ level, see whether it is HQ, RD or DM

				string strJobCdKeyWd= ConfigurationSettings.AppSettings["strJobCodeKeyWdFst3"].ToString();
                string strJobCodeKeyWdFst1= ConfigurationSettings.AppSettings["strJobCodeKeyWdFst1"].ToString();
				string strJobCodeKeyWdTrd1= ConfigurationSettings.AppSettings["strJobCodeKeyWdTrd1"].ToString();
				string strJobCodeKeyWdFst2= ConfigurationSettings.AppSettings["strJobCodeKeyWdFst2"].ToString();

				if (strJobCode.Length > 2)
				{
					string[] myARSplit = strJobCdKeyWd.Split(',');
					
					foreach (string x in myARSplit)
					{
						if (strJobCode.Substring(0, 3)== x)
						{
							strJobCode = "HQ";
							break;
						}
					}
					
					if (strJobCode.Substring(0,1)==strJobCodeKeyWdFst1 && strJobCode.Substring(2,1)==strJobCodeKeyWdTrd1 || strJobCode.Substring(0,2)==strJobCodeKeyWdFst2)
					{
						strJobCode ="Executive";

					}	
					
//					if (strJobCode.Substring(0, 3)=="TCP" || strJobCode.Substring(0, 3)=="MGR" || strJobCode.Substring(0, 3)=="ANA" || strJobCode.Substring(0, 3)=="TCN" || strJobCode.Substring(0, 3)=="DIR" || strJobCode.Substring(0, 3)=="SUP" || strJobCode.Substring(0, 3)=="ACC" || strJobCode.Substring(0, 4)=="DCAM" || strJobCode.Substring(0, 3)=="ADM" || strJobCode.Substring(0, 3)=="COR" || strJobCode.Substring(0, 3)=="DCM" || strJobCode.Substring(0, 3)=="DCS")
//					{
//						strJobCode = "HQ";
//
//					}
//					else if (strJobCode.Substring(0,1)=="C" && strJobCode.Substring(2,1)=="O" || strJobCode.Substring(0,2)=="VP")
//					if (strJobCode.Substring(0,1)=="C" && strJobCode.Substring(2,1)=="O" || strJobCode.Substring(0,2)=="VP")
//					
//				    {
//						strJobCode ="Executive";
//
//					}

				}



				switch (strJobCode)
				{
					case "RD":
						intPermit =2;
						break;
					case "DM":
						intPermit = 3;
						break;
					case "HQ":
						intPermit = 1;//TODO: remove this 
						break;
					case "Executive":
						intPermit = 5;
						break;
                    //TODO: Assuming it's LP, Confirm with Lan
					case "LP":
						intPermit = 6;
						break;

					default:
						intPermit = 0;
						break;
				}


			}
			else
			{
				//ignore job code, this is store permission
				intPermit = 4;

			}


			return intPermit;
		}
     
//		public int ParseLine(string strLine)
//		{
//			string[] myARSplit = strLine.Split(',');
//            
//			foreach (string x in myARSplit)
//			{
//				m_AryCols.Add(x);
//
//			}
//			return m_nCount = m_AryCols.Count;
//		}

		//		public bool GetUserInfo()
		//		{
		//		
		//		}
	}
}
