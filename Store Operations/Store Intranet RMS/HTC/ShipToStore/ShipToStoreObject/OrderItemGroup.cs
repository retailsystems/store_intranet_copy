using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Text;
using System.IO;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for OrderItemGroup.
	/// </summary>
	public class OrderItemGroup: CollectionBase
	{
		public OrderItemGroup()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static OrderItemGroup GetOrderItemDataTable(string strOrderNo)	        
		{
			OrderItemGroup theOrderItemGroup = new OrderItemGroup();
			theOrderItemGroup.OrderItemGroupFetch(strOrderNo);
			return theOrderItemGroup;
		}

		public static OrderItemGroup GetOrderItemForEmail(string strFullOrderNo)	        
		{
			OrderItemGroup _orderItemGroup = new OrderItemGroup();
			_orderItemGroup.OrderItemGroupEmailFetch(strFullOrderNo);
			return _orderItemGroup;
		}

		private void OrderItemGroupFetch(string strOrderNo)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();

			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				cm.CommandText = "usp_GetOrderDetailByOrderNo";
				
				SqlDataReader dr;              
				cm.Parameters.Clear();
		
				SqlParameter mmm = new SqlParameter();
				mmm.ParameterName = "@OrderNo";
				mmm.SqlDbType = SqlDbType.VarChar;
				mmm.Value = strOrderNo;
				cm.Parameters.Add(mmm);

				dr = cm.ExecuteReader();
				
				while (dr.Read())
					List.Add(OneOrderItem.GetOrderItem(dr));
			}
			catch(Exception ex)
			{
				//int s=0;

			}
			finally
			{
				cn.Close();
			}
		}
		private void OrderItemGroupEmailFetch(string strFullOrderNo)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();

			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				cm.CommandText = "usp_EmailItems_Rollup";
				
				SqlDataReader dr;              
				cm.Parameters.Clear();
		
				SqlParameter mmm = new SqlParameter();
				mmm.ParameterName = "@FullOrderNo";
				mmm.SqlDbType = SqlDbType.VarChar;
				mmm.Value = strFullOrderNo;
				cm.Parameters.Add(mmm);

				dr = cm.ExecuteReader();
				
				while (dr.Read())
					List.Add(OneOrderItem.GetOrderItem(dr));
			}
			catch(Exception ex)
			{
				//int s=0;

			}
			finally
			{
				cn.Close();
			}
		}
	}
}
