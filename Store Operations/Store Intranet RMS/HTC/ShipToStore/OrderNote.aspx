<%@ Page language="c#" Codebehind="OrderNote.aspx.cs" AutoEventWireup="false" Inherits="ShipToStore.OrderNote" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OrderNote</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<table class="innerTable" cellpadding="1" cellspacing="0">
				<tr>
					<td class="tableCell">Store Notes:
					</td>
				</tr>
				<tr>
					<td class="tableCell">
						<asp:TextBox id="txtNotes" ReadOnly="True" runat="server" CssClass="tableInput" Columns="25"
							Rows="8" TextMode="MultiLine"></asp:TextBox>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="tableCell">
						<asp:Button id="btnClose" runat="server" CssClass="buttonRed" Text="CLOSE"></asp:Button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
