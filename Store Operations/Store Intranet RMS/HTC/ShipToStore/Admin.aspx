<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="ShipToStore.Admin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Ship To Store Administration</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
		<b>Please enter one of the following:</b><br />
		Tracking Number: <asp:textbox id="TrackingNumber" runat="server"></asp:textbox>
		Order Number: <asp:textbox id="OrderNumber" runat="server"></asp:textbox>
		Full Order Number: <asp:textbox id="FullOrderNumber" runat="server"></asp:textbox>
		<asp:button id="Submit" runat="server" text="Search" onclick="Submit_Click" />
		<asp:label id="SearchCriteriaEmpty" runat="server" style="color:Red" visible="false">Please enter data to search!</asp:label>
    </div>
    <br />
    <div>
		<asp:datalist id="SearchResults" runat="server" width="100%" onitemcommand="SearchResults_ItemCommand" onitemdatabound="SearchResults_ItemDataBound">
			<itemtemplate>
				<b>Order Header:</b>
				<table cellpadding="1px" cellspacing="1px" border="1px">
					<tr>
						<td>Order Number</td>
						<td>Store Number</td>
						<td>Status</td>
						<td>Entry Date</td>
						<td>Last Shipment Date</td>
						<td>Number of Packages</td>
					</tr>
					<tr>
						<td><%# DataBinder.Eval(Container.DataItem,"ORDERNO")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"STORENO")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"STATUS")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"ENTRYDATE", "{0:d}")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"LASTSHIPMENT","{0:d}")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"NOPACKAGE")%></td>
					</tr>
				</table>
				<br />
				<b>Order Detail:</b>
				<table border="1px" cellpadding="1px" cellspacing="1px">
					<tr>
						<td>Full Order Number</td>
						<td>Ship Date</td>
						<td>Ship Method</td>
						<td>Item Status</td>
						<td>Order Status</td>
					</tr>
					<tr>
						<td><%# DataBinder.Eval(Container.DataItem,"FULLORDERNO")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"SHIPDATE","{0:d}")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"SHIPMETHOD")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"ITM_STATUS")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"ORD_STATUS")%></td>
					</tr>
				</table>
				<br />
				<b>Order Shipping/Tracking:</b>
				<table border="1px" cellpadding="1px" cellspacing="1px">
					<tr>
						<td>Tracking Number</td>
						<td>Date Received in Store</td>
						<td>Date Customer Picked Up</td>
						<td>Date Returned to DC</td>
						<td>Date Abandoned</td>
						<td>Check In Number</td>
					</tr>
					<tr>
						<td><%# DataBinder.Eval(Container.DataItem,"TRACKNO")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"DATERECINSTORE","{0:d}")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"DATECUSTPICKUP","{0:d}")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"DATERETURNDC","{0:d}")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"AbandonDate","{0:d}")%></td>
						<td><%# DataBinder.Eval(Container.DataItem,"XTRACKNO")%></td>
					</tr>
				</table>
				<br />
				<b>Actions:</b>
				<asp:placeholder id="Actions" runat="server" visible="true">
					<asp:linkbutton id="ForceCheckIn" runat="server" commandname="checkin" commandargument='<%# DataBinder.Eval(Container.DataItem,"FULLORDERNO")%>'>Force Receive in Store (No Email)</asp:linkbutton> | 
					<asp:linkbutton id="UndoAbandon" runat="server" commandname="undoabandon" commandargument='<%# DataBinder.Eval(Container.DataItem,"FULLORDERNO")%>'>Undo Abandon Order</asp:linkbutton> | 
					<asp:linkbutton id="ReminderEmail" runat="server" commandname="email" commandargument='<%# DataBinder.Eval(Container.DataItem,"FULLORDERNO")%>' visible="false">Send Reminder Email</asp:linkbutton>
					<asp:linkbutton id="ForcePickup" runat="server" commandname="checkout" commandargument='<%# DataBinder.Eval(Container.DataItem,"FULLORDERNO")%>' visible="false">Force Customer Pick Up (Sends Email)</asp:linkbutton>
					<asp:linkbutton id="UndoCheckOut" runat="server" commandname="undocheckout" commandargument='<%# DataBinder.Eval(Container.DataItem,"FULLORDERNO")%>' visible="false">Undo Customer Pickup</asp:linkbutton>
				</asp:placeholder>
				<br />
				<asp:label id="ActionResult" runat="server" visible="false" style="color:Red"></asp:label>
			</itemtemplate>
			<alternatingitemstyle backcolor="LightGrey" />
		</asp:datalist>
    </div>
    </form>
</body>
</html>