<%@ Page language="c#" Codebehind="Print_LP.aspx.cs" AutoEventWireup="false" Inherits="ShipToStore.Print_LP" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Print</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
		<script language="javascript" type="text/javascript">
		
		    function CallPrint(strid)
			{
			var prtButton = document.getElementById(strid);
			//var WinPrint = window.open('','','letf=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
			//WinPrint.document.write('<style>td{mso-style-parent:text; mso-number-format:\\@;white-space:normal;border:1px solid black;}</style>');		
			//WinPrint.document.write(prtContent.innerHTML);
			//WinPrint.document.close();
			//WinPrint.focus();
			prtButton.style.display= "none";
			self.print();
		    prtButton.style.display = "";

			//WinPrint.close();
			
			}
		</script>
	</HEAD>
	<body topmargin="1" leftmargin="2">
		<form id="Form1" method="post" runat="server">		
	
			<asp:datalist id="dlOrderPrintView" runat="server" Width="80%" EnableViewState="True" RepeatLayout="Flow"
					RepeatDirection="Horizontal" DataKeyField="StrOrderNo">
					<HeaderTemplate>
						<table width="76%"  cellspacing="0" cellpadding="2" class="innerTableView" align="center">
							<thead>
							<tr class="tableTitle">
								<td colspan="19" class="tableTitle">Shop And Ship Search Results  <A id="btnP" onclick="Javascript: CallPrint('btnP');" href="#">Print</A></td>
							</tr>
							<tr class="printHeadTDLP">
								<td align="center" width="30">
									No.
								</td>
								<td align="center" width="30">
									Reg
								</td>
								<td align="center" width="30">
									Dist
								</td>
								<td align="center" width="40">
									Store
								</td>
								<td align="center" width="50">
									Order No.
								</td>
								<td align="center" width="50">
									Order Date
								</td>
								<td align="center" width="50">
									Pay
								</td>
								<td align="center" width="50">
									Gift Card
								</td>
								<td align="center" width="80">
									Customer
								</td>
								<td align="center" width="80">
									EmpID
								</td>
								<td align="center" width="50">
									Phone
								</td>
								<td align="center" width="60">
									Total
								</td>
								<td align="center" width="50">
									Shipped On
								</td>
								<td align="center" width="50">
									Weight
								</td>
								<td align="center" width="50">
									Tracking #
								</td>
								<td align="center" width="50">
									Received On
								</td>
								<td align="center" width="50">
									Ship Source</td>
								<td align="center" width="90">
									Notes</td>
								<td align="center" width="50">
									PU Date</td>
							</tr>
							</thead>
					</HeaderTemplate>
					<ItemTemplate>
					    <tbody>
						<tr height="25" id="total" runat="server" class="innerTableLP">
							<td align="right">
								<%#(Container.ItemIndex+1)%>
							</td>
							<td align="right">
								<%# DataBinder.Eval(Container.DataItem,"StrRegionID") %>
							</td>
							<td align="right">
								<%# DataBinder.Eval(Container.DataItem,"StrDistrictID") %>
							</td>
							<td align="right">
								<%# DataBinder.Eval(Container.DataItem, "StrStoreID")%>
							</td>
							<td align="right">
								<%# DataBinder.Eval(Container.DataItem,"StrOrderNo") %>
							</td>
							<td align="right">
								<%# DataBinder.Eval(Container.DataItem, "DtOrderDate","{0:d}")%>
							</td>
							<td align="right">
								<%# DataBinder.Eval(Container.DataItem, "StrPaymentType")%>
							</td>
							<td align="right">
								<%# DataBinder.Eval(Container.DataItem, "StrGiftCardNo")%>
							</td>
							<td align="right">
								<%# DataBinder.Eval(Container.DataItem, "StrCustName")%>
							</td>
							<td align="right">
								<%# DataBinder.Eval(Container.DataItem, "StrEmployeeNo")%>
							</td>
							<td align="right">
								<%# DataBinder.Eval(Container.DataItem, "StrCustDayPhone")%>
							</td>
							<td align="right">
								<%# DataBinder.Eval(Container.DataItem, "DblOrderTotal", "{0:f}")%>
							</td>
							<td align="right">
								<%# ConvertDate(DataBinder.Eval(Container.DataItem, "DtShippedDate", "{0:d}"))%>
							</td>
							<td align="right">
								<%# ConvertWeight(DataBinder.Eval(Container.DataItem, "DblOrderWeight", "{0:f}"))%>
							</td>
							<td align="left">
								<%# DataBinder.Eval(Container.DataItem,"StrTrackingNo") %>
							</td>
							<td align="right">
								<%# ConvertDate(DataBinder.Eval(Container.DataItem, "DtReceivedDate", "{0:d}"))%>
							</td>
							<td align="right">
								<%# DataBinder.Eval(Container.DataItem, "StrShippedSource")%>
							</td>
							<td align="right">
								<%# ConvertNotes(DataBinder.Eval(Container.DataItem,"StrStoreNotes", "{0}"))%>
							</td>
							<td align="right">
								<%# ConvertDate(DataBinder.Eval(Container.DataItem, "DtCustPickUpDate", "{0:d}"))%>
							</td>
						</tr>
						</tbody>
					</ItemTemplate>
					<FooterTemplate>
						<tr height="25" id="Tr1" runat="server" class="tableCell">
							<td align="left">
							</td>
							<td align="left">
							</td>
							<td align="left">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
								Total Sales
							</td>
							<td align="right">
							</td>
							<td align="right">
								Total Count
							</td>
							<td align="right">
								ADT
							</td>
							<td align="right">
								UPT
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
						</tr>
						<tr height="25" id="Tr2" runat="server" class="tableCell">
							<td align="left">
							</td>
							<td align="left">
							</td>
							<td align="left">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="Right">
								<%=TotalSales%>
							</td>
							<td align="right">
							</td>
							<td align="right">
								<%=TotalCount%>
							</td>
							<td align="right">
								<%=ADT%>
							</td>
							<td align="right">
								<%=UPT%>
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
							<td align="right">
							</td>
						</tr>
						</table>
					</FooterTemplate>
				</asp:datalist>
		</form>
	</body>
</HTML>
