using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for Print.
	/// </summary>
	public class Print : System.Web.UI.Page
	{	protected System.Web.UI.WebControls.DataList dlOrderPrintView;
		protected double TotalSales;
		protected int TotalCount;
		protected double ADT;
		protected double UPT;
		OrderGroup printGroup;
		private void Page_Load(object sender, System.EventArgs e)
		{
			// If Session does not exist, send the user back to the login page
			CheckUserSessions();	
			if (!IsPostBack)
			{
				if(Session["PrintObject"]!=null)
				{
					printGroup = (OrderGroup)Session["PrintObject"];
					dlOrderPrintView.DataSource=printGroup;
					dlOrderPrintView.DataBind();
					TotalSales = Math.Round(printGroup.TotalAmount, 2);
					TotalCount =printGroup.Count;
					ADT = (TotalCount==0? 0: Math.Round(TotalSales/TotalCount, 2));
					UPT = (TotalCount==0? 0: printGroup.TotalUnit/TotalCount);	
					Session["PrintObject"] = null;

				}
				else
				{
					Response.Write("Session expired. Please close window and re-open print screen.");
				}
			}

		}

		private void CheckUserSessions()
		{
			if (Session["ViewPermission"]==null)
			{
				Response.Redirect("./Login.aspx");
			}			 
		}
	
		//convert weight
		protected string ConvertWeight(string wt)
		{
			if (wt!="")
			{
				double dwt = Double.Parse(wt);
				return (dwt/10000).ToString() + " lbs";

			}
			else
			{
				return wt;
			}
		}

		//filter out bad dates
		protected string ConvertDate(string dt)
		{
			if (DateTime.Parse(dt) > DateTime.Parse("1/1/1980"))
			{
				return dt.ToString();

			}
			else
			{
				return "";

			}
		}
	
		//filter out long notes
		protected string ConvertNotes(string nt)
		{
			if (nt.Length>10)
			{
				return nt.Substring(0, 10)+"...";

			}
			else
			{
				return nt;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
