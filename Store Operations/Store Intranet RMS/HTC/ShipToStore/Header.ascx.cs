namespace ShipToStore
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.IO;

	/// <summary>
	///		Summary description for Header.
	/// </summary>
	public class Header : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnReSearch;
        protected System.Web.UI.HtmlControls.HtmlInputButton btnMenu;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnAdmin;
		protected System.Web.UI.WebControls.Label lblPermit;
		protected System.Web.UI.WebControls.LinkButton lnkLogout;
		protected System.Web.UI.WebControls.Image Image2;
		DataTable dtUser;
        protected int intPermit;
       
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Session["ViewPermission"]!=null)
			{	
				intPermit = int.Parse(Session["ViewPermission"].ToString());
				DisplayLocation(intPermit);
				this.lnkLogout.Visible = true;
				this.btnReSearch.Visible = true;
                if (intPermit == 4)
                {
                    this.btnMenu.Visible = true;
                }
                else
                {
                    this.btnMenu.Visible = false;
                }
				if (intPermit == 1)
				{
					this.btnAdmin.Visible = true;
				}
				else
				{
					this.btnAdmin.Visible = false;
				}
			}
			else
			{
				this.lnkLogout.Visible = false;
				this.lblPermit.Text = "View Mode: Guest";
				this.btnReSearch.Visible = false;

				if (GetCurrentPage()!="Login.aspx")
				{
				   Response.Redirect("./Login.aspx");
				}
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lnkLogout.Click += new System.EventHandler(this.lnkLogout_Click);
			this.btnReSearch.ServerClick += new System.EventHandler(this.btnReSearch_ServerClick);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void DisplayLocation(int intPermit)
		{
			DataTable dtUserProfile = (DataTable)Session["UserProfile"];
			if (dtUserProfile!=null)
			{
				switch(intPermit)
				{
					case 1:
						//executive/admin level
						this.lblPermit.Text = "View Mode: HQ";
						break;
					case 2:
						this.lblPermit.Text = "View Mode: Region " + Utility.ParseDepartment(dtUserProfile.Rows[0]["Dept"].ToString());
						break;
					case 3:
						this.lblPermit.Text = "View Mode: District " + Utility.ParseDepartment(dtUserProfile.Rows[0]["Dept"].ToString());
						break;
					case 4:
						this.lblPermit.Text = "View Mode: Store " +dtUserProfile.Rows[0]["Location"].ToString();
						break;
					case 6:
						this.lblPermit.Text = "View Mode: LP";
						break;
					default:
						break;
						


						}
			}

		}

		private void lnkLogout_Click(object sender, System.EventArgs e)
		{
			dtUser = (DataTable)Session["UserProfile"];
			string strEmployeeID = dtUser.Rows[0]["EmployeeID"].ToString().Trim();
			string strStoreNum = dtUser.Rows[0]["Location"].ToString().Trim();
			Data.TrackEmployeeLogin(strEmployeeID, strStoreNum, "O");
			
			Session.Clear();
			Session.Abandon();
			Response.Redirect("./Login.aspx");
		
		}

		private void btnReSearch_ServerClick(object sender, System.EventArgs e)
		{
            if (intPermit == 6)
            {
                if (GetCurrentPage() != "./OrderSearch_LP.aspx")
                {
                    Response.Redirect("./OrderSearch_LP.aspx");
                }
            }
            else
            {
                if (GetCurrentPage() != "./OrderSearch.aspx")
                {
                    Response.Redirect("./OrderSearch.aspx");
                }
            }
		}
		private string GetCurrentPage()
		{
			string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath; 
			System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath); 
			string sRet = oInfo.Name; 
			return sRet; 

		}

        protected void btnMenu_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("./StoreActionOption.aspx");
        }

		protected void btnAdmin_ServerClick(object sender, EventArgs e)
		{
			Response.Redirect("./Admin.aspx");
		}
	}
}
