<%@ Page language="c#" Codebehind="CustPickUp.aspx.cs" AutoEventWireup="false" Inherits="ShipToStore.CustPickUp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OrderNote</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table class="innerTable" cellpadding="1" cellspacing="0">
				<tr>
					<td class="tableCell" colspan="2"><table cellSpacing="0" cellPadding="0" width="100" border="0">
							<TR>
								<TD>
									<TABLE style="WIDTH: 220px; HEIGHT: 180px" cellSpacing="0" cellPadding="0" width="219"
										align="center" border="0">
										<tr>
											<td class="fontMediumBold" align="left">The following Tracking Numbers are 
												associated with this order.
											</td>
										</tr>
										<tr>
											<td><asp:listbox id="lstTrackView" runat="server" Width="250" DataKeyField="StrTrackNo" RepeatDirection="Horizontal"
													RepeatLayout="Flow" EnableViewState="True" SelectionMode="Single"></asp:listbox></td>
										</tr>
									</TABLE>
								</TD>
							</TR>
						</table>
					</td>
				</tr>
				<tr>
					<td class="tableCell" colspan="2">Are you sure you want to change the order status 
						to customer picked up?
					</td>
				</tr>
				<tr>
					<td class="tableCell">
						<asp:Button id="btnPickedUp" runat="server" CssClass="buttonRed" Text=" YES "></asp:Button>
					</td>
					<td class="tableCell">
						<asp:Button id="btnClose" runat="server" CssClass="buttonRed" Text="NO and CLOSE"></asp:Button>
					</td>
				</tr>
				<tr class="PageError">
					<td colspan="2"><font color="#cc0000"><asp:Label ID="lblErrorText" runat="server" Visible="false"></asp:Label></font></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
