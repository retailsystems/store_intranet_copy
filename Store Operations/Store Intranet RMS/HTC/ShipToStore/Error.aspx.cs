using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for Error.
	/// </summary>
	public class Error : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			
//			HttpContext ctx = HttpContext.Current;
//
//			Exception exception = ctx.Server.GetLastError ();
//
//			ctx.Response.Write(Server.GetLastError());
//			
//			throw(new ArgumentNullException());
		}
		
	
//		public void Page_Error(object sender,EventArgs e)
//		{
//			Exception objErr = Server.GetLastError().GetBaseException();
//			string err =	"<b>Error Caught in Page_Error event</b><hr><br>" + 
//				"<br><b>Error in: </b>" + Request.Url.ToString() +
//				"<br><b>Error Message: </b>" + objErr.Message.ToString()+
//				"<br><b>Stack Trace:</b><br>" + 
//				objErr.StackTrace.ToString();
//			Response.Write(err.ToString());
//			Server.ClearError();
////		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
