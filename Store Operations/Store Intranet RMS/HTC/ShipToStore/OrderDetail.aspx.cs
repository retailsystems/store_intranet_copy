using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Web.Security;
using System.Text;
using System.Data.SqlClient;



namespace ShipToStore
{
	/// <summary>
	/// Summary description for OrderDetail.
	/// </summary>
	public class OrderDetail : System.Web.UI.Page
	{
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.WebControls.Button btnReturnToSearch;
		protected System.Web.UI.WebControls.Button btnCustomerPickUp;
		protected System.Web.UI.WebControls.Button btnEmailReminder;
		protected System.Web.UI.WebControls.Button btnSaveNotes;
		protected System.Web.UI.WebControls.TextBox txtNotes;
		protected System.Web.UI.WebControls.Label lblOrderDate;
		protected System.Web.UI.WebControls.Label lblShipDate;
		protected System.Web.UI.WebControls.Label lblShipType;
		protected System.Web.UI.WebControls.Label lblBoxNumber;
		protected System.Web.UI.WebControls.Label lblSource;
		protected System.Web.UI.WebControls.Label lblDateReceived;
		protected System.Web.UI.WebControls.Label lblPickUp;
		protected System.Web.UI.WebControls.Label lblStatus;
		protected System.Web.UI.WebControls.Label lblPayType;
		protected System.Web.UI.WebControls.Label lblTotal;
		protected System.Web.UI.WebControls.Label lblFirstName;
		protected System.Web.UI.WebControls.Label lblLastName;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.Label lblHPhone;
		protected System.Web.UI.WebControls.Label lblMPhone;
		protected System.Web.UI.HtmlControls.HtmlTable tblOption;
        protected System.Web.UI.WebControls.GridView gvOrderPikUpView;
		protected Order theOneOrder;
		protected string strOrderNumber = string.Empty;
		protected string strFullOrderNo = string.Empty;
		protected string strStatus = string.Empty;
		protected OrderItemGroup theOrdItmEmailGroup;
		protected OrderItemGroup newOrdItmEmailGroup; 
		string strNotes = string.Empty;
		string strCustName =  string.Empty;
		string strCustEmail =  string.Empty;
		string strDivision = string.Empty;
		//int intIsEmail = 0;
		string strCustStreet = string.Empty;
		string strCustCity = string.Empty;
		string strCustState = string.Empty;
		string strCustZip = string.Empty;
		string strStrName = string.Empty;
		string strStrAddr = string.Empty;
		string strStrCity = string.Empty;
		string strStrState = string.Empty;
		string strStrZip = string.Empty;
		string strHTEmailBody = string.Empty;
		string strItemDetail = string.Empty;
		string strSHEmailBody = string.Empty;
		string strHTEmailLogo = ConfigurationSettings.AppSettings["HTEmailLogo"];
		string strSHEmailLogo = ConfigurationSettings.AppSettings["SHEmailLogo"];
		string strHTEmlBotImage = ConfigurationSettings.AppSettings["HTEmlBotImage"];
		string strSHEmlBotImage = ConfigurationSettings.AppSettings["SHEmlBotImage"];
		double dblProdAmt = 0.00;
		double dblTax = 0.00;
		double dblTotPaid = 0.00;
        double dblShipping = 0.00;
		protected System.Web.UI.WebControls.Label lblErrorText;
		StringBuilder strHTEmailBodyBl = new StringBuilder();
		string strErrorMsg = string.Empty;
		int intRemResult = 0;
		protected string strFullOrderNum = string.Empty;
		protected System.Web.UI.WebControls.ListBox lstTrackView;
		protected System.Web.UI.WebControls.Button btnTrackOrder;
		protected DataTable newDtTrackNos;
		protected System.Web.UI.HtmlControls.HtmlInputImage btnPickUp;
		//protected System.Web.UI.HtmlControls.HtmlInputImage btnPickUp;
		protected string strNewStatus;
        protected DataTable dtOrderPikUp;
        protected OrderItemGroup newOrderItmGroup;
        int intIsEmail = 0;
        DataTable dtUser;
        protected OrderItemGroup theOrderItmGroup;
        //protected System.Web.UI.WebControls.Button btnEmailReminder;
        //protected System.Web.UI.WebControls.Button btnWholUpdate;
        protected System.Web.UI.WebControls.ImageButton btnWholUpdate;
        DataTable dtCountFullOrdNo;
        int nAcct = 0;
       
		private void Page_Load(object sender, System.EventArgs e)
		{
			//string strOrderNo = Request.QueryString["strOrderNo"];
			if (!IsPostBack)
			{
				//string strOrderNo = Request.QueryString["OrdNo"];
                //prod before --strOrderNumber= Request.QueryString["StrOrderNo"]; 

                strFullOrderNum = Request.QueryString["StrOrderNo"];
                strOrderNumber = strFullOrderNum.Substring(0, 8);
                theOneOrder = Order.GetTheOneOrder(strFullOrderNum);

				lblOrderDate.Text = FilterDate(theOneOrder.DtOrderDate);
				lblShipDate.Text= FilterDate(theOneOrder.DtShippedDate);
				lblShipType.Text = theOneOrder.StrShipMethod.ToString();;
				lblBoxNumber.Text = theOneOrder.StrPackageNo.ToString();
				lblSource.Text = theOneOrder.StrShippedSource.ToString();
				lblPayType.Text = theOneOrder.StrPaymentType.ToString();
				lblTotal.Text = "$" + string.Format("{0:f}", Math.Round(theOneOrder.DblOrderTotal,2));
				lblFirstName.Text = theOneOrder.StrCustFirstName.ToString();
				lblLastName.Text = theOneOrder.StrCustLastName.ToString();
				lblEmail.Text = theOneOrder.StrEmail.ToString();

				string dayphone = theOneOrder.StrCustDayPhone.Trim();
				if (dayphone.Length == 10)
					dayphone = string.Format("{0}-{1}-{2}", dayphone.Substring(0,3), dayphone.Substring(3,3), dayphone.Substring(6));
				else if (dayphone.Length == 11)
					dayphone = string.Format("{0}-{1}-{2}-{3}", dayphone.Substring(0,1), dayphone.Substring(1,3), dayphone.Substring(4,3), dayphone.Substring(7));
				lblHPhone.Text = dayphone;
				//lblDateReceived.Text= FilterDate(theOneOrder.DtReceivedDate);
				//lblPickUp.Text = FilterDate(theOneOrder.DtCustPickUpDate);
				
				lblBoxNumber.Text = theOneOrder.StrPackageNo.ToString();
                //UPT = (TotalCount == 0 ? 0 : newOrderGroup.TotalUnit / TotalCount);
                
                txtNotes.Text = theOneOrder.StrStoreNotes.ToString() == string.Empty ? "" : theOneOrder.StrStoreNotes.ToString();
                strNotes = theOneOrder.StrStoreNotes.ToString();
				ViewState["FullOrderNumber"] = theOneOrder.StrRecFullOrdNo.ToString();
				strFullOrderNum = theOneOrder.StrRecFullOrdNo.ToString();
				ViewState["strFullOrderNum"] = strFullOrderNum;
				//ViewState["status"] =  theOneOrder.StrStatus.ToString(); 
				strCustName = theOneOrder.StrCustFirstName.ToString()+ " " + theOneOrder.StrCustLastName.ToString();
				strCustEmail = theOneOrder.StrEmail.ToString();
				ViewState["StrOrderNo"] = strOrderNumber;

				newDtTrackNos = Data.GetTrackNos(strOrderNumber);

                //DisplayStatus(strOrderNumber);

				//ViewState["status"] = this.lblStatus.Text;
				//lblStatus.Text = theOneOrder.StrStatus.ToString();
				lstTrackView.DataSource = newDtTrackNos;
				lstTrackView.DataTextField = "TRACKNO";
				lstTrackView.DataValueField = "TRACKNO";
										
				lstTrackView.DataBind();
				if (lstTrackView.Items.Count>0)
				{
					lstTrackView.SelectedIndex=0;

				}

                BindData();
				//if (!string.IsNullOrEmpty(Request.QueryString["strOrderNo"]))
				//{
				//e.CommandName + " Item " + e.CommandArgument;

				// Session["strOrderNo"] = Request.QueryString["strOrderNo"];

				//}
		
				//theOrderGroup = Session["theOrderGroup"];
			}
			else
			{
				strOrderNumber =ViewState["StrOrderNo"].ToString();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnTrackOrder.Click += new System.EventHandler(this.btnTrackOrder_Click);
			this.btnEmailReminder.Click += new System.EventHandler(this.btnEmailReminderCust_Click);
			this.btnSaveNotes.Click += new System.EventHandler(this.btnSaveNotes_Click);
			this.btnReturnToSearch.Click += new System.EventHandler(this.btnReturnToSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
        private void BindData()
        {
            dtOrderPikUp = Data.GetOrdPikUpStatus(strOrderNumber);

            foreach (System.Data.DataRow dr in dtOrderPikUp.Rows)
            {
                bool bReceiv = false;
                bool bPickup = false;

                if (dr["DATERECINSTORE"] != System.DBNull.Value)
                {
                    dr["DATERECINSTORE"] = Convert.ToDateTime(dr["DATERECINSTORE"]);
                    bReceiv = true;
                }
                if (dr["DATECUSTPICKUP"] != System.DBNull.Value)
                {
                    dr["DATECUSTPICKUP"] = Convert.ToDateTime(dr["DATECUSTPICKUP"]);
                    bPickup = true;
                }

				if (dr["AbandonDate"] != System.DBNull.Value)
				{
					dr["FLAG"] = false;
				}
                else if ((bReceiv == false) && (bPickup == false))
                {
                    dr["FLAG"] = false;
                }
                else if ((bReceiv == true) && (bPickup == false))
                {
                    dr["FLAG"] = true;
                }
                else if ((bReceiv == true) && (bPickup == true))
                {
                    dr["FLAG"] = false;
                }
                else { dr["FLAG"] = false; }
            }

            gvOrderPikUpView.DataSource = dtOrderPikUp;
            gvOrderPikUpView.DataBind();

        }

        protected void gvOrderPikUpView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            //Check for the row type, which should be data row
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex == 0)
                    e.Row.Style.Add("height", "30px");

            }
        }

        protected void DisplayStatus(string strOrderNumber)
		{
			strNewStatus = Data.GetOrdStatus(strOrderNumber);
			if (strNewStatus!=null)
			{
				switch(strNewStatus)
				{
					case "I1":
						this.lblStatus.Text = "Order Partially Received";
						break;
					case "I2":
						this.lblStatus.Text = "Order Received & Partially Picked Up";
						break;
					case "I3":
						this.lblStatus.Text = "Received at Store";
						break;
					case "I4":
						this.lblStatus.Text = "Customer Picked Up";
						break;
					case "I5":
						this.lblStatus.Text = "Return to DC";
						break;
					case "PC":
						this.lblStatus.Text = "Shipped";
						break;
					case "P7":
						this.lblStatus.Text = "Warehouse Pending";
						break;
					case "P1":
						this.lblStatus.Text = "Reg Ord Ready Process";
						break;
					case "P2":
						this.lblStatus.Text = "Reg Ord Ready Process";
						break;
					case "PD":
						this.lblStatus.Text = "Order Cancelled";
						break;
					case "PG":
						this.lblStatus.Text = "Order Cancelled";
						break;
					case "PA":
						this.lblStatus.Text = "All Backordered";
						break;
					case "PB":
						this.lblStatus.Text = "Partial Backordered";
						break;
					case "WB":
						this.lblStatus.Text = "Warehouse Backorder";
						break;
					default:
						this.lblStatus.Text = "Pending";
						break;
					
				}
			}

		}
		//Filter out invalid dates
		private string FilterDate (DateTime dt)
		{
			if (dt> DateTime.Parse("1/1/1980"))
			{
				return dt.ToShortDateString();
			}
			else
			{
				return "N/A";
			}

		}
		private void btnReturnToSearch_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("./OrderSearch.aspx" );
		}

		
		private void btnTrackOrder_Click(object sender, System.EventArgs e)
		{
			if (lstTrackView.SelectedValue.Length==22)
			{
				//Response.Redirect("http://www.usps.com/shipping/trackandconfirm.htm");
				Response.Redirect("http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?origTrackNum=" + lstTrackView.SelectedValue);
	
			}
			else 
			{
				Response.Redirect("http://www.fedex.com/Tracking?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers=" + lstTrackView.SelectedValue);
			}
		}
      
		private OrderItemGroup GetOrdItem4RemindEmail(string strFullOrderNo)
		{
			newOrdItmEmailGroup = OrderItemGroup.GetOrderItemForEmail(strFullOrderNo);	
			return newOrdItmEmailGroup;
		}
        private string GetHTEmailBody(string strFullOrderNo)
        {

            string strPlate = ConfigurationSettings.AppSettings["PickupTemplate"].ToString();
            strHTEmailBody = System.IO.File.ReadAllText(strPlate);
            //title
            strHTEmailBody = strHTEmailBody.Replace("xCustNamex", strCustName);
            strHTEmailBody = strHTEmailBody.Replace("xOrderNumberx", strFullOrderNo);
            //detail
            strHTEmailBody = strHTEmailBody.Replace("xOrderDetailx", strItemDetail + strHTEmailBodyBl);

            return strHTEmailBody;
        }

        private string GetSHEmailBody(string strFullOrderNo)
        {
            string strPlate = ConfigurationSettings.AppSettings["PickupTemplate_TR"].ToString();
            strSHEmailBody = System.IO.File.ReadAllText(strPlate);
            //title
            strSHEmailBody = strSHEmailBody.Replace("xCustNamex", strCustName);
            strSHEmailBody = strSHEmailBody.Replace("xOrderNumberx", strFullOrderNo);
            //detail
            strSHEmailBody = strSHEmailBody.Replace("xOrderDetailx", strItemDetail + strHTEmailBodyBl);

            return strSHEmailBody;
        }
        private string GetHTPartialEmailBody(string strFullOrderNo)
        {
            string strPlate = ConfigurationSettings.AppSettings["PartialTemplate"].ToString();
            strHTEmailBody = System.IO.File.ReadAllText(strPlate);
            //title
            strHTEmailBody = strHTEmailBody.Replace("xCustNamex", strCustName);
            strHTEmailBody = strHTEmailBody.Replace("xOrderNumberx", strFullOrderNo);
            strHTEmailBody = strHTEmailBody.Replace("xStoreNamex", strStrName);
            //address
            strHTEmailBody = strHTEmailBody.Replace("xStoreStreetx", strStrAddr);
            strHTEmailBody = strHTEmailBody.Replace("xCustStreetx", strCustStreet);
            strHTEmailBody = strHTEmailBody.Replace("xStoreAddressx", strStrCity + ", " + strStrState + " " + strStrZip);
            strHTEmailBody = strHTEmailBody.Replace("xCustAddressx", strCustCity + ", " + strCustState + " " + strCustZip);
            //detail
            strHTEmailBody = strHTEmailBody.Replace("xOrderDetailx", strItemDetail + strHTEmailBodyBl);

            return strHTEmailBody;
        }
        private string GetHTEmailRecevBody(string strFullOrderNo)
        {
            string strPlate = ConfigurationSettings.AppSettings["ReceiveTemplate"].ToString();
            strHTEmailBody = System.IO.File.ReadAllText(strPlate);
            //title
            strHTEmailBody = strHTEmailBody.Replace("xCustNamex", strCustName);
            strHTEmailBody = strHTEmailBody.Replace("xOrderNumberx", strFullOrderNo);
            strHTEmailBody = strHTEmailBody.Replace("xStoreNamex", strStrName);
            //address
            strHTEmailBody = strHTEmailBody.Replace("xStoreStreetx", strStrAddr);
            strHTEmailBody = strHTEmailBody.Replace("xCustStreetx", strCustStreet);
            strHTEmailBody = strHTEmailBody.Replace("xStoreAddressx", strStrCity + ", " + strStrState + " " + strStrZip);
            strHTEmailBody = strHTEmailBody.Replace("xCustAddressx", strCustCity + ", " + strCustState + " " + strCustZip);
            //detail
            strHTEmailBody = strHTEmailBody.Replace("xOrderDetailx", strItemDetail + strHTEmailBodyBl);

            return strHTEmailBody;
        }

        
		private int InsertOrderNotes(string strOrderNo, string strOrderNotes)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();
			int intResultRec = 0;
			
			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				cm.CommandText = "usp_OrdNotes_Insert";
				cm.CommandTimeout=90;
				           
				cm.Parameters.Clear();

				SqlParameter qqq = new SqlParameter();
				qqq.ParameterName = "@ORDERNO";
				qqq.SqlDbType = SqlDbType.VarChar;
				qqq.Value = strOrderNo;
				cm.Parameters.Add(qqq);

				SqlParameter PPP = new SqlParameter();
				PPP.ParameterName = "@NOTES";
				PPP.SqlDbType = SqlDbType.VarChar;
				PPP.Value = strOrderNotes;
				cm.Parameters.Add(PPP);

				SqlParameter sss = new SqlParameter();
				sss.ParameterName = "@Result";
				sss.SqlDbType = SqlDbType.Int;
				sss.Value = intResultRec;
				sss.Direction = ParameterDirection.Output;
				cm.Parameters.Add(sss);

				cm.ExecuteNonQuery();

				intResultRec = Convert.ToInt16(cm.Parameters["@Result"].Value);
				
			}
			catch (SqlException ex)
			{
				intResultRec = 99;
			}
			
			finally
			{
				cn.Close();
			}
			return intResultRec;
		}
		private OrderItemGroup GetOrdItem4RemEmail(string strFullOrderNo)
		{
			newOrdItmEmailGroup = OrderItemGroup.GetOrderItemForEmail(strFullOrderNo);	
			return newOrdItmEmailGroup;
		}
		
		private int GetOrdShp4RemindEmail(string strFullOrderNo)
		{
			int intRemindResult = 0;
			intRemindResult = this.GetOrdShp4RemEmail(strFullOrderNo);

			return intRemindResult;
		}

		private int GetOrdShp4RemEmail(string strFullOrderNo)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();
			int intResultShp = 0;
			
			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				cm.CommandText = "usp_RemnidEmlOrdInfo_ByOrdNo";
				cm.CommandTimeout=90;
				           
				cm.Parameters.Clear();

				SqlParameter qqq = new SqlParameter();
				qqq.ParameterName = "@FULLORDERNO";
				qqq.SqlDbType = SqlDbType.VarChar;
				qqq.Value = strFullOrderNo;
				cm.Parameters.Add(qqq);

				SqlParameter sss = new SqlParameter();
				sss.ParameterName = "@Result";
				sss.SqlDbType = SqlDbType.Int;
				sss.Value = intResultShp;
				sss.Direction = ParameterDirection.Output;
				cm.Parameters.Add(sss);

				SqlParameter xxx = new SqlParameter();
				xxx.ParameterName = "@Division";
				xxx.SqlDbType = SqlDbType.VarChar;
				xxx.Size = 4;
				xxx.Value = strDivision;
				xxx.Direction = ParameterDirection.Output;
				cm.Parameters.Add(xxx);

				SqlParameter aaa = new SqlParameter();
				aaa.ParameterName = "@CustName";
				aaa.SqlDbType = SqlDbType.VarChar;
				aaa.Size = 32;
				aaa.Value = strCustName;
				aaa.Direction = ParameterDirection.Output;
				cm.Parameters.Add(aaa);

				SqlParameter bbb = new SqlParameter();
				bbb.ParameterName = "@CustEmail";
				bbb.SqlDbType = SqlDbType.VarChar;
				bbb.Size = 50;
				bbb.Value = strCustEmail;
				bbb.Direction = ParameterDirection.Output;
				cm.Parameters.Add(bbb);

				SqlParameter rrr = new SqlParameter();
				rrr.ParameterName = "@Street";
				rrr.SqlDbType = SqlDbType.VarChar;
				rrr.Size = 30;
				rrr.Value = strCustStreet;
				rrr.Direction = ParameterDirection.Output;
				cm.Parameters.Add(rrr);

				SqlParameter ttt = new SqlParameter();
				ttt.ParameterName = "@City";
				ttt.SqlDbType = SqlDbType.VarChar;
				ttt.Size = 30;
				ttt.Value = strCustCity;
				ttt.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ttt);

				SqlParameter uuu = new SqlParameter();
				uuu.ParameterName = "@State";
				uuu.SqlDbType = SqlDbType.VarChar;
				uuu.Size = 2;
				uuu.Value = strCustState;
				uuu.Direction = ParameterDirection.Output;
				cm.Parameters.Add(uuu);

				SqlParameter vvv = new SqlParameter();
				vvv.ParameterName = "@Zip";
				vvv.SqlDbType = SqlDbType.VarChar;
				vvv.Size = 14;
				vvv.Value = strCustZip;
				vvv.Direction = ParameterDirection.Output;
				cm.Parameters.Add(vvv);

				SqlParameter www = new SqlParameter();
				www.ParameterName = "@StoreName";
				www.SqlDbType = SqlDbType.VarChar;
				www.Size = 50;
				www.Value = strStrName;
				www.Direction = ParameterDirection.Output;
				cm.Parameters.Add(www);

				SqlParameter yyy = new SqlParameter();
				yyy.ParameterName = "@StrAddress";
				yyy.SqlDbType = SqlDbType.VarChar;
				yyy.Size = 105;
				yyy.Value = strStrAddr;
				yyy.Direction = ParameterDirection.Output;
				cm.Parameters.Add(yyy);

				SqlParameter ppp = new SqlParameter();
				ppp.ParameterName = "@StrCity";
				ppp.SqlDbType = SqlDbType.VarChar;
				ppp.Size = 35;
				ppp.Value = strStrCity;
				ppp.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ppp);

				SqlParameter lll = new SqlParameter();
				lll.ParameterName = "@StrState";
				lll.SqlDbType = SqlDbType.VarChar;
				lll.Size = 2;
				lll.Value = strStrState;
				lll.Direction = ParameterDirection.Output;
				cm.Parameters.Add(lll);

				SqlParameter ooo = new SqlParameter();
				ooo.ParameterName = "@StrZip";
				ooo.SqlDbType = SqlDbType.VarChar;
				ooo.Size = 35;
				ooo.Value = strStrZip;
				ooo.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ooo);
		
				cm.ExecuteNonQuery();

				intResultShp = Convert.ToInt16(cm.Parameters["@Result"].Value);
				strFullOrderNo = Convert.ToString(cm.Parameters["@FullOrderNo"].Value);
				strDivision = Convert.ToString(cm.Parameters["@Division"].Value);
				strCustEmail =Convert.ToString(cm.Parameters["@CustEmail"].Value);
				strCustName = Convert.ToString(cm.Parameters["@CustName"].Value);
				strCustStreet = Convert.ToString(cm.Parameters["@Street"].Value);
				strCustCity = Convert.ToString(cm.Parameters["@City"].Value);
				strCustState = Convert.ToString(cm.Parameters["@State"].Value);
				strCustZip = Convert.ToString(cm.Parameters["@Zip"].Value);
				strStrName = Convert.ToString(cm.Parameters["@StoreName"].Value);
				strStrAddr = Convert.ToString(cm.Parameters["@StrAddress"].Value);
				strStrCity = Convert.ToString(cm.Parameters["@StrCity"].Value);
				strStrState = Convert.ToString(cm.Parameters["@StrState"].Value);
				strStrZip = Convert.ToString(cm.Parameters["@StrZip"].Value);

			}
			catch (SqlException ex)
			{
				intResultShp = 99;
			}
			
			finally
			{
				cn.Close();
			}
			return intResultShp;
		}


		private void btnSaveNotes_Click(object sender, System.EventArgs e)
		{
		
			int intSaveNote = 0;
			string strOrderNotes = txtNotes.Text.Trim();
			string strOrderNo= string.Empty;

			//strOrderNo = ViewState["FullOrderNumber"].ToString().Substring(0,8);
			strOrderNo = ViewState["StrOrderNo"].ToString();

			if (IsPostBack)
			{
				strOrderNotes =	txtNotes.Text.Trim();
				intSaveNote = InsertOrderNotes(strOrderNo, strOrderNotes);
				if (intSaveNote != 1)
				{
					//Response.Redirect("./Error.aspx" );
				}
			}
		
		}
        //private string GetHTEmailBody()
        //{
        //    strHTEmailBody = "<table border =0><tr><td colspan=4><img src='" + strHTEmailLogo + "'></td></tr><tr><td  colspan=4>Hi " + strCustName + ": </td></tr>" 
        //        + "<tr><td colspan=4>Your order, "+ strFullOrderNo.Substring(0,8) +", has been delivered to the " + strStrName + " store and is ready for pick up!</td></tr>"
        //        + "<tr><td colspan=4>All you have to do is print and bring this receipt with you when you go to get your order. If you don�t have a printer," 
        //        + "just bring the order number. Easy, huh?</td></tr><tr><td colspan=4>&nbsp;</td></tr>"
        //        + "<tr><td colspan=4>SHIPMENT INFORMATION</td></tr><tr><td colspan = 4></td></tr>";

        //    strHTEmailBody += "<tr><td colspan=4><table border = 0><tr><td width = 100>SHIP TO: </td><td width = 250>" + strStrName + "</td><td width = 100>BILL TO:</td><td width =250>" + strCustName + "</td></tr>";
        //    strHTEmailBody += "<tr><td width = 100></td><td width = 250>" + strStrAddr  + "</td><td width = 100></td><td width = 250>" + strCustStreet + "</td></tr><tr><td width = 100></td><td width = 250>" + strStrCity + ", " + strStrState + " " + strStrZip + "</td><td width = 100></td><td width = 250>"+ strCustCity + ", " + strCustState + " " + strCustZip + "</td></tr></table></td></tr><tr><td colspan=4></td></tr>";
			
        //    //	strHTEmailBody += "SHIP TO: " + strStrName + "\n" + strStrAddr + "\n"+ "\n";
        //    //	strHTEmailBody += "BILL TO: " + strCustName + "\n" + strCustStreet + " " + strCustCity + ", " + strCustState + " " + strCustZip + "\n"+ "\n";
        //    strHTEmailBody += "<tr><td colspan=4></td><tr><tr><td colspan=4>Your order will be available to pick up for 45 days. If you don�t get it within 45 days, we will begin the</td></tr>"
        //        + "<tr><td colspan=4> refund process. Refund is based on original form of payment. Please note: your package will no longer</td></tr>" 
        //        + "<tr><td colspan=4> be available for pick up once the refund process has begun.</td></tr><tr><tr><td colspan=4></td></tr>" ;
        //    strHTEmailBody += strItemDetail;
        //    strHTEmailBody += strHTEmailBodyBl;
        //    strHTEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4>*If you paid with a credit card, the total paid amount was charged to your card at the time your order was shipped.</td></tr>";
        //    strHTEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan = 4>Thanks! </td></tr><tr><td td colspan=4>" + "Hottopic.com</td></tr>" + "<tr><td td colspan=4>Customer Service</td></tr>" + "<tr><td td colspan=4></td></tr><tr><td colspan=4></td><tr>";
        //    //strHTEmailBody += "<tr><td colspan=4><img src='http://localhost/Home/ShipToStore_4/Image/HTEmlBotImg.gif'></td></tr></table>";
        //    strHTEmailBody += "<tr><td colspan=4></td></tr></table>";
			
        //    return strHTEmailBody;
        //}

        //private string GetSHEmailBody()
        //{
        //    strSHEmailBody = "<table border =0><tr><td colspan=4><img src='" + strSHEmailLogo + "'></td></tr><tr><td  colspan=4>Hi " + strCustName + ": </td></tr>" 
        //        + "<tr><td colspan=4>Your order, "+ strFullOrderNo.Substring(0,8) +", has been delivered to the " + strStrName + " store and is ready for pick up!</td></tr>"
        //        + "<tr><td colspan=4>Life is good! Please print out this receipt and bring it with you when you pick up your order. If you don�t have a printer,</td></tr>" 
        //        + "<tr><td colspan=4> just bring the order number with you and we�ll take it from there.</td></tr><tr><td colspan=4>&nbsp;</td></tr>"
        //        + "<tr><td colspan=4>SHIPMENT INFORMATION</td></tr><tr><td colspan=4></td><tr>";

        //    strSHEmailBody += "<tr><td colspan=4><table border = 0><tr><td width = 100>SHIP TO: </td><td width = 250>" + strStrName + "</td><td width = 100>BILL TO:</td><td width =250>" + strCustName + "</td></tr>";
        //    strSHEmailBody += "<tr><td width = 100></td><td width = 250>" + strStrAddr  + "</td><td width = 100></td><td width = 250>" + strCustStreet + "</td></tr><tr><td width = 100></td><td width = 250>" + strStrCity + ", " + strStrState + " " + strStrZip + "</td><td width = 100></td><td width = 250>"+ strCustCity + ", " + strCustState + " " + strCustZip + "</td></tr></table></td></tr><tr><td colspan=4></td></tr>";
			
        //    //	strHTEmailBody += "SHIP TO: " + strStrName + "\n" + strStrAddr + "\n"+ "\n";
        //    //	strHTEmailBody += "BILL TO: " + strCustName + "\n" + strCustStreet + " " + strCustCity + ", " + strCustState + " " + strCustZip + "\n"+ "\n";
        //    strSHEmailBody += "<tr><td colspan=4></td><tr><tr><td colspan=4>Your order will be available for pickup for 45 days. If you do not pick up your order within 45 days, we will begin the</td></tr>"
        //        + "<tr><td colspan=4> refund process. (Refund is based on original form of payment.) Once the refund process has begun, your package will no longer</td></tr>" 
        //        + "<tr><td colspan=4> be available for pick up � so come on down and get your dang order, would ya?</td></tr><tr><td colspan =4 height =8></td></tr>" ;
        //    strSHEmailBody += strItemDetail;
        //    strSHEmailBody += strHTEmailBodyBl;
        //    strSHEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4>Thanks again for shopping on ShockHound.com!</td></tr><tr><td td colspan=4></td></tr>";
        //    strSHEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4>ShockHound Customer Service </td></tr><tr><td td colspan=4>" + "ShockHound is all about the fans; let us know what you think. If you ever need ShockHound customer </td></tr>" + "<tr><td td colspan=4>service, contact us at 866-272-1124 (US & Canada), or email us by going to <a href='http://shockhound.com'>http://shockhound.com </a></td></tr><tr><td colspan=4></td><tr>";
        //    //strSHEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4><img src='http://localhost/Home/ShipToStore_4/Image/SHEmlBotImg.gif'></td></tr></table>";
        //    strSHEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4></td></tr></table>";
		
        //    return strSHEmailBody;
        //}
		private void ShowError(string strErrMsg)
		{
			if (strErrMsg.Trim()!="")
			{
				lblErrorText.Text = strErrMsg;
				lblErrorText.Visible= true;
			}
		}
        private OrderItemGroup GetOrderItemsForPKEmail(string strFullOrderNo)
        {
            newOrderItmGroup = OrderItemGroup.GetOrderItemForEmail(strFullOrderNo);
            return newOrderItmGroup;
        }
        protected void UpdateOrdPikUp(object sender, GridViewUpdateEventArgs e)
        {
        }


        private int InsertPickUpOrderTrack(string strTrackNo, string strOrderTrackNo, string strFullOrderNo)
        {
            SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
            SqlCommand cm = new SqlCommand();
            int intResultRec = 0;

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "usp_CustPickUp_Update_ByOrdTrack";
                cm.CommandTimeout = 150;

                cm.Parameters.Clear();

                SqlParameter aaa = new SqlParameter();
                aaa.ParameterName = "@OrderTrackNo";
                aaa.SqlDbType = SqlDbType.VarChar;
                aaa.Size = 52;
                aaa.Value = strOrderTrackNo;
                cm.Parameters.Add(aaa);

                SqlParameter qqq = new SqlParameter();
                qqq.ParameterName = "@TRACKNO";
                qqq.SqlDbType = SqlDbType.VarChar;
                qqq.Value = strTrackNo;
                cm.Parameters.Add(qqq);

                SqlParameter sss = new SqlParameter();
                sss.ParameterName = "@Result";
                sss.SqlDbType = SqlDbType.Int;
                sss.Value = intResultRec;
                sss.Direction = ParameterDirection.Output;
                cm.Parameters.Add(sss);

                SqlParameter hhh = new SqlParameter();
                hhh.ParameterName = "@FullOrderNo";
                hhh.SqlDbType = SqlDbType.VarChar;
                hhh.Size = 12;
                hhh.Value = strFullOrderNo;
                cm.Parameters.Add(hhh);

                SqlParameter jjj = new SqlParameter();
                jjj.ParameterName = "@CustEmail";
                jjj.SqlDbType = SqlDbType.VarChar;
                jjj.Size = 50;
                jjj.Value = strCustEmail;
                jjj.Direction = ParameterDirection.Output;
                cm.Parameters.Add(jjj);

                SqlParameter xxx = new SqlParameter();
                xxx.ParameterName = "@Division";
                xxx.SqlDbType = SqlDbType.VarChar;
                xxx.Size = 4;
                xxx.Value = strDivision;
                xxx.Direction = ParameterDirection.Output;
                cm.Parameters.Add(xxx);

                SqlParameter mmm = new SqlParameter();
                mmm.ParameterName = "@IsEmail";
                mmm.SqlDbType = SqlDbType.Int;
                mmm.Value = intIsEmail;
                mmm.Direction = ParameterDirection.Output;
                cm.Parameters.Add(mmm);

                SqlParameter kkk = new SqlParameter();
                kkk.ParameterName = "@CustName";
                kkk.SqlDbType = SqlDbType.VarChar;
                kkk.Size = 32;
                kkk.Value = strCustName;
                kkk.Direction = ParameterDirection.Output;
                cm.Parameters.Add(kkk);

                SqlParameter rrr = new SqlParameter();
                rrr.ParameterName = "@Street";
                rrr.SqlDbType = SqlDbType.VarChar;
                rrr.Size = 30;
                rrr.Value = strCustStreet;
                rrr.Direction = ParameterDirection.Output;
                cm.Parameters.Add(rrr);

                SqlParameter ttt = new SqlParameter();
                ttt.ParameterName = "@City";
                ttt.SqlDbType = SqlDbType.VarChar;
                ttt.Size = 30;
                ttt.Value = strCustCity;
                ttt.Direction = ParameterDirection.Output;
                cm.Parameters.Add(ttt);

                SqlParameter uuu = new SqlParameter();
                uuu.ParameterName = "@State";
                uuu.SqlDbType = SqlDbType.VarChar;
                uuu.Size = 2;
                uuu.Value = strCustState;
                uuu.Direction = ParameterDirection.Output;
                cm.Parameters.Add(uuu);

                SqlParameter vvv = new SqlParameter();
                vvv.ParameterName = "@Zip";
                vvv.SqlDbType = SqlDbType.VarChar;
                vvv.Size = 14;
                vvv.Value = strCustZip;
                vvv.Direction = ParameterDirection.Output;
                cm.Parameters.Add(vvv);

                SqlParameter www = new SqlParameter();
                www.ParameterName = "@StoreName";
                www.SqlDbType = SqlDbType.VarChar;
                www.Size = 50;
                www.Value = strStrName;
                www.Direction = ParameterDirection.Output;
                cm.Parameters.Add(www);

                SqlParameter yyy = new SqlParameter();
                yyy.ParameterName = "@StrAddress";
                yyy.SqlDbType = SqlDbType.VarChar;
                yyy.Size = 105;
                yyy.Value = strStrAddr;
                yyy.Direction = ParameterDirection.Output;
                cm.Parameters.Add(yyy);

                SqlParameter ppp = new SqlParameter();
                ppp.ParameterName = "@StrCity";
                ppp.SqlDbType = SqlDbType.VarChar;
                ppp.Size = 35;
                ppp.Value = strStrCity;
                ppp.Direction = ParameterDirection.Output;
                cm.Parameters.Add(ppp);

                SqlParameter bbb = new SqlParameter();
                bbb.ParameterName = "@StrState";
                bbb.SqlDbType = SqlDbType.VarChar;
                bbb.Size = 2;
                bbb.Value = strStrState;
                bbb.Direction = ParameterDirection.Output;
                cm.Parameters.Add(bbb);

                SqlParameter ooo = new SqlParameter();
                ooo.ParameterName = "@StrZip";
                ooo.SqlDbType = SqlDbType.VarChar;
                ooo.Size = 35;
                ooo.Value = strStrZip;
                ooo.Direction = ParameterDirection.Output;
                cm.Parameters.Add(ooo);

                cm.ExecuteNonQuery();

                intResultRec = Convert.ToInt32(cm.Parameters["@Result"].Value);
                //				strFullOrderNo = Convert.ToString(cm.Parameters["@FullOrderNo"].Value);
                strCustEmail = Convert.ToString(cm.Parameters["@CustEmail"].Value);
                strDivision = Convert.ToString(cm.Parameters["@Division"].Value);
                intIsEmail = Convert.ToInt32(cm.Parameters["@IsEmail"].Value);
                strCustName = Convert.ToString(cm.Parameters["@CustName"].Value);
                strCustStreet = Convert.ToString(cm.Parameters["@Street"].Value);
                strCustCity = Convert.ToString(cm.Parameters["@City"].Value);
                strCustState = Convert.ToString(cm.Parameters["@State"].Value);
                strCustZip = Convert.ToString(cm.Parameters["@Zip"].Value);
                strStrName = Convert.ToString(cm.Parameters["@StoreName"].Value);
                strStrAddr = Convert.ToString(cm.Parameters["@StrAddress"].Value);
                strStrCity = Convert.ToString(cm.Parameters["@StrCity"].Value);
                strStrState = Convert.ToString(cm.Parameters["@StrState"].Value);
                strStrZip = Convert.ToString(cm.Parameters["@StrZip"].Value);

            }
            catch (SqlException ex)
            {
                intResultRec = 99;
            }

            finally
            {
                cn.Close();
            }
            return intResultRec;
        }


        protected void btnWPickUp_Click(object sender, ImageClickEventArgs e)
        {

            //Loop through gridview rows to find checkbox 
            //and check whether it is checked or not 
            int dbResult = 0;
            //strErrorMsg = "";
            dtUser = (DataTable)Session["UserProfile"];
            string strTrackNo = string.Empty;
            string strFullOrderNo = string.Empty;
            string strEmployeeID = dtUser.Rows[0]["EmployeeID"].ToString().Trim();
            string strStoreNum = dtUser.Rows[0]["Location"].ToString().Trim();
            int nchecked = 0;
            for (int i = 0; i < gvOrderPikUpView.Rows.Count; i++)
            {
                CheckBox chkUpdate = (CheckBox)gvOrderPikUpView.Rows[i].Cells[0].FindControl("chkSelect");
                if (chkUpdate != null)
                {
                    if (chkUpdate.Checked)
                    {
                        nchecked = nchecked + 1;
                    }
                }
            }
            if (nchecked >= 1)
            {
                for (int i = 0; i < gvOrderPikUpView.Rows.Count; i++)
                {
                    CheckBox chkUpdate = (CheckBox)gvOrderPikUpView.Rows[i].Cells[0].FindControl("chkSelect");
                    if (chkUpdate != null)
                    {
                        if (chkUpdate.Checked)
                        {

                            strTrackNo = gvOrderPikUpView.Rows[i].Cells[1].Text;
                            strFullOrderNo = gvOrderPikUpView.Rows[i].Cells[2].Text;
                            //UPDATE TO db HERE 

                            string strOrderTrackNo = strFullOrderNo + strTrackNo;
                            dbResult = InsertPickUpOrderTrack(strTrackNo, strOrderTrackNo, strFullOrderNo);

                            if (dbResult == 1)
                            {
                                if (intIsEmail == 1)
                                {
                                    //if ((strCustEmail != "") && (strStrName != "") && (strCustStreet != ""))
                                    //{
                                    theOrderItmGroup = GetOrderItemsForPKEmail(strFullOrderNo);
                                    //strItemDetail =  "ShipQTY" + " " + "CancelQTY"  + "\t" + "DESCRIPTION" + "                                            "+ "AMOUNT" + "\n";
                                    strItemDetail = "<table class='inner' border='1' cellpadding='0' cellspacing='0'><tr><td width='20'>ShipQTY</td><td width='20'>CancelQTY</td><td width'='400'>DESCRIPTION</td><td>AMOUNT</td></tr>";

                                    foreach (OneOrderItem oneItem in theOrderItmGroup)
                                    {
                                        //strHTItemDetail +=  oneItem.StrQtyShp + "       " + oneItem.StrQtyCancel  + "\t" + "\t"+ oneItem.StrItemDESC + "      " + oneItem.StrSprice + "\n";
                                        strItemDetail += "<tr><td width='20' align='center'>" + oneItem.StrQtyShp + "</td><td width='20' align='center'>" + oneItem.StrQtyCancel + "</td><td width='400'>" + oneItem.StrItemDESC + "</td><td align='right'>" + double.Parse(oneItem.StrSprice).ToString("0.00") + "</td></tr>";

                                        //strItemDetail +=  oneItem.StrQtyShp + "       " + oneItem.StrQtyCancel  + "\t" + "\t"+ oneItem.StrItemDESC + "     " + double.Parse(oneItem.StrSprice).ToString("0.00")  + "\n";
                                        dblProdAmt = double.Parse(oneItem.StrTotSprice);
                                        dblTax = double.Parse(oneItem.StrTax);
                                    }
                                    string source = Data.GetSourceType(strFullOrderNo);
                                    if (source.Substring(0, 2) == "PS" || source.Substring(0, 2) == "EC")
                                    {
                                        dblShipping = double.Parse(Data.GetShippingCost(strFullOrderNo));
                                    }
                                    else
                                    {
                                        dblShipping = Double.Parse(ConfigurationSettings.AppSettings["ShipmentFee"]);
                                    }
                                    dblTotPaid = dblProdAmt + dblTax + dblShipping;
                                    strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> PRODUCT:</td><td align='right'>" + dblProdAmt.ToString("0.00") + "</td></tr>";      //dblProdAmt.ToString("0.00") + "\n";
                                    strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> TAX:</td><td align='right'>" + dblTax.ToString("0.00") + "</td></tr>";
                                    strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> SHIPPING AND HANDLING:</td><td align='right'>" + dblShipping.ToString("0.00") + "</td></tr>";
                                    strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> *TOTAL PAID:</td><td align='right'>" + dblTotPaid.ToString("0.00") + "</td></tr></table>";

                                    int nCountFullOrdNo = 0;
                                    dtCountFullOrdNo = Data.GetOrdPikUpStatus(strFullOrderNo.Substring(0, 8));

                                    nCountFullOrdNo = Utility.CountFullOrdNos(dtCountFullOrdNo);
                                    //string strBrand = (strDivision == "1" ? "HotTopic" : "Torrid");
									string EmailSubject = string.Empty;

                                    
                                    //DO COMMENT
                                   string adminMail = ConfigurationSettings.AppSettings["EmailRec"].ToString();
                                   if (adminMail.Trim() != "")
                                   {
                                       strCustEmail = adminMail;
                                   }
                             if (strDivision !="")
                                    {
                                        if (strCustEmail != "")
                                        {
                                            if (nCountFullOrdNo == 1)
                                            {
												EmailSubject = ConfigurationSettings.AppSettings["PickupSubject"];
                                                Emails.SendText(strCustEmail, EmailSubject, GetHTEmailBody(strFullOrderNo));
                                            }
                                            else if (nCountFullOrdNo > 1)
                                            {   //send patial order received email
												EmailSubject = ConfigurationSettings.AppSettings["PartialPickupSubject"];
                                                Emails.SendText(strCustEmail, EmailSubject, GetHTEmailBody(strFullOrderNo));

                                            }
                                        }
                                    }
                                    else
                                    {
										EmailSubject = ConfigurationSettings.AppSettings["PickupSubject"];
                                        Emails.SendText(strCustEmail, EmailSubject, GetSHEmailBody(strFullOrderNo));
                                    }
                                    strErrorMsg += "Tracking number " + strTrackNo.ToString() + " and Full Order number " + strFullOrderNo.ToString() +  " has been picked up successfully.\n";
                                   
                                    //track store employee scan box time for picked up status. 
                                    Data.TrackEmployeeScan(strTrackNo.ToString(), strFullOrderNo, strEmployeeID, strStoreNum, "P");
                                    //}
                                    //else
                                    //{
                                    //    strErrorMsg += "Could not get enough customer and store info to send email out from database;" + "<br>";
                                    //    ShowError(strErrorMsg);
                                    //}
                                    ShowError(strErrorMsg);
                                }
                            }
                            else if (dbResult == 2) //DUPLICATE, entry already
                            {
                                strErrorMsg += "Tracking number " + strTrackNo.ToString() + " has already been picked up.<br>";
                                ShowError(strErrorMsg);
                            }
                            else if (dbResult == 5) //not receive in store
                            {
                                strErrorMsg += "Tracking number " + strTrackNo.ToString() + " has not been received at store yet.<br>";
                                ShowError(strErrorMsg);
                            }
                            else if (dbResult == 6) //not complete receive in store
                            {
                                strErrorMsg += "Tracking number " + strTrackNo.ToString() + " Picked Up. (Partial order) \n";

                                ShowError(strErrorMsg);
                                //BindData();
                            }
                            //							else if (dbResult ==4) //can not find order due to order did not receive in store
                            //							{
                            //								strErrorMsg += "Tracking #" + arrlist[i].ToString()+" has not been checked in at store yet; "+ "<br>";
                            //								ShowError(strErrorMsg); 
                            //							}
                            

                        }
                        //else
                        //{
                        //    strErrorMsg = "Please make sure that the checkbox has been chosen before you click Customer Pick Up button.";
                        //    ShowError(strErrorMsg);
                        //}
                    }

                } 
                BindData();
            }
            else
            {
                strErrorMsg = "Please make sure that the checkbox has been chosen before you click Customer Pick Up button.\n";
                ShowError(strErrorMsg);
            }  
        }

       

        protected void btnEmailReminderCust_Click(object sender, EventArgs e)
        {
            if (nAcct < 1)
            {
                int nchecked = 0;
                for (int i = 0; i < gvOrderPikUpView.Rows.Count; i++)
                {
                    CheckBox chkUpdate = (CheckBox)gvOrderPikUpView.Rows[i].Cells[0].FindControl("chkSelect");
                    if (chkUpdate != null)
                    {
                        if (chkUpdate.Checked)
                        {
                            nchecked = nchecked + 1;
                        }
                    }
                }
                if (nchecked >= 1)
                {
                    for (int i = 0; i < gvOrderPikUpView.Rows.Count; i++)
                    {
                        CheckBox chkUpdate = (CheckBox)gvOrderPikUpView.Rows[i].Cells[0].FindControl("chkSelect");
                        if (chkUpdate != null)
                        {
                            if (chkUpdate.Checked)
                            {
                                string strFullOrderNo = gvOrderPikUpView.Rows[i].Cells[2].Text;
                                string strTrackNo = gvOrderPikUpView.Rows[i].Cells[1].Text;

                                theOrdItmEmailGroup = GetOrdItem4RemindEmail(strFullOrderNo);
                                strItemDetail = "<table class='inner' border='1' cellpadding='0' cellspacing='0'><tr><td width='20'>ShipQTY</td><td width='20'>CancelQTY</td><td width='400'>DESCRIPTION</td><td>AMOUNT</td></tr>";

                                foreach (OneOrderItem oneItem in theOrdItmEmailGroup)
                                {
                                    strItemDetail += "<tr><td width='20' align='center'>" + oneItem.StrQtyShp + "</td><td width='20' align='center'>" + oneItem.StrQtyCancel + "</td><td width='400'>" + oneItem.StrItemDESC + "</td><td align='right'>" + double.Parse(oneItem.StrSprice).ToString("0.00") + "</td></tr>";
                                    dblProdAmt = double.Parse(oneItem.StrTotSprice);
                                    dblTax = double.Parse(oneItem.StrTax);
                                }
                              
                                string source = Data.GetSourceType(strFullOrderNo);
                                if (source.Substring(0, 2) == "PS" || source.Substring(0, 2) == "EC")
                                {
                                    dblShipping = double.Parse(Data.GetShippingCost(strFullOrderNo));
                                }
                                else
                                {
                                    dblShipping = Double.Parse(ConfigurationSettings.AppSettings["ShipmentFee"]);
                                }
                                dblTotPaid = dblProdAmt + dblTax + dblShipping;
                                strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> PRODUCT:</td><td align='right'>" + dblProdAmt.ToString("0.00") + "</td></tr>";      //dblProdAmt.ToString("0.00") + "\n";
                                strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> TAX:</td><td align='right'>" + dblTax.ToString("0.00") + "</td></tr>";
                                strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> SHIPPING AND HANDLING:</td><td align='right'>" + dblShipping.ToString("0.00") + "</td></tr>";
                                strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> *TOTAL PAID:</td><td align='right'>" + dblTotPaid.ToString("0.00") + "</td></tr></table>";
                                intRemResult = GetOrdShp4RemindEmail(strFullOrderNo);
                                                                  

                                int nCountFullOrdNo = 0;
                                dtOrderPikUp = Data.GetOrdPikUpStatus(strOrderNumber);

                                nCountFullOrdNo = Utility.CountFullOrdNos(dtOrderPikUp);
                                //string strBrand = (strDivision == "1" ? "HotTopic" : "Torrid");
								string EmailSubject = string.Empty;
                                
                                //DO COMMENT
                                 string adminMail = ConfigurationSettings.AppSettings["EmailRec"].ToString();
                                 if (adminMail != "")
                                 {
                                     strCustEmail = adminMail; //use for test not to send email to customer
                                 }
                                if (strCustEmail != "")
                                {
                                    if (nCountFullOrdNo == 1)
                                    {
										EmailSubject = ConfigurationSettings.AppSettings["ReceiveSubject"];
                                        Emails.SendText(strCustEmail, EmailSubject, GetHTEmailRecevBody(strFullOrderNo));

                                    }
                                    else if (nCountFullOrdNo > 1) //send patial order received email
                                    {
										EmailSubject = ConfigurationSettings.AppSettings["PartialReceiveSubject"];
                                        Emails.SendText(strCustEmail, EmailSubject, GetHTPartialEmailBody(strFullOrderNo));
                                    }
                                    strErrorMsg += "Email Reminder for Full Order number " + strFullOrderNo.ToString() + " and Tracking number " + strTrackNo.ToString() + " sent to Customer successfully.\n";


                                    ShowError(strErrorMsg);

                                }
                            }

                        }
                    }
                }
                else
                {
                    strErrorMsg = "Please make sure that the checkbox has been chosen before you click Reminder Email button.";
                    ShowError(strErrorMsg);
                }
                //BindData();
            } nAcct = nAcct + 1;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnAbandon_Click(object sender, EventArgs e)
		{
			try
			{
				// Call stored procedure
				string ordernumber = ViewState["FullOrderNumber"].ToString();
				DataTable userprofile = (DataTable)Session["UserProfile"];
				string user = null;

				if (userprofile.Rows.Count > 0)
				{
					if ( userprofile.Rows[0]["EmployeeID"] != DBNull.Value)
					{
						user = userprofile.Rows[0]["EmployeeID"].ToString();
					}
				}

				Data.AbandonOrder(ordernumber,user);

				ShowError(string.Format("Order Number {0} was successfully marked as abandoned.", ordernumber));
			}
			catch (Exception ex)
			{
				ShowError("Error occurred in processing abandonment. [" + ex.Message + "]");
			}
		}
	}
}
