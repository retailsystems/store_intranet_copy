<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page language="c#" Codebehind="Error.aspx.cs" AutoEventWireup="false" Inherits="ShipToStore.Error" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Error</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" topMargin="0" MS_POSITIONING="FlowLayout">
		<div id="header"><uc1:header id="Header1" runat="server"></uc1:header></div>
		<div class="height: 540px;" id="main">
			<form id="Form1" method="post" runat="server">
				<br>
				<TABLE class="innerTableLog" id="tblLogin" height="450" cellSpacing="0" cellPadding="1"
					width="96%" align="center" border="0">
					<TBODY>
						<tr>
							<td>
								<table cellSpacing="0" cellPadding="1" width="50%" align="center" border="0">
									<TBODY>
										<tr class="tableCell">
											<td colSpan="3"><font color="#cc0000">There is an error occurred. </font>
											</td>
										</tr>
										<tr class="tableCell">
											<td class="PageError" colSpan="3"><font color="#cc0000"><asp:label id="Label1" runat="server" Visible="false"></asp:label></font></td>
										</tr>
									</TBODY>
								</table>
								<br>
								<table height="50" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0">
									<tr>
										<td vAlign="top" align="right"></td>
									</tr>
								</table>
			</form>
			<DIV></DIV>
			</TD></TR></TBODY></TABLE><br>
			<table height="90" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0">
				<tr>
					<td vAlign="top" align="right"><asp:button id="btnSearch" runat="server" CssClass="buttonRed" Text="RETURN TO LOGIN"></asp:button></td>
				</tr>
			</table>
		</div>
	</body>
</HTML>
