<%@ Page language="c#" Codebehind="OrderDetail_LP.aspx.cs" AutoEventWireup="false" Inherits="ShipToStore.OrderDetail_LP" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OrderSearch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
		<script language="javascript" type="text/javascript">

            function calendarPicker(strField)
            {

                        window.open('Calendar.aspx?field=' + strField, 'calendarPopup', 'width=250,height=200,resizable=yes');

            }
            
            function PickUpOrder()
            {
              //alert('strOrderNumber');
              //var ordernum1 = 'P123456';
              //var ordernum = ordernum1;
              //var fullOrdernum = "<%=strFullOrderNum%>";
              var fullOrdernum = "<%=strFullOrderNum%>";
              window.open('CustPickUp.aspx?strOrderNo=' + fullOrdernum, 'PickUpOrder', 'width=300,height=350,resizable=yes');
            }

		</script>
	</HEAD>
	<body bottomMargin="0" topMargin="0" MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header></div>
			<div id="main"><br>
				<TABLE class="innerTableDetail" id="tblOption" cellSpacing="0" cellPadding="0" width="96%"
					align="center" border="0" runat="server">
					<TBODY>
						<tr>
							<td class="innerTableDetail" colSpan="2">
								<TABLE cellSpacing="0" cellPadding="1" width="100%" align="center" border="0">
									<TBODY>
										<tr>
											<td colSpan="2">
												<table style="WIDTH: 584px; HEIGHT: 166px" width="60%">
													<TR>
														<TD class="tableTitle" colSpan="2">Order Details</TD>
													</TR>
													<TR>
														<td>
															<table style="WIDTH: 576px; HEIGHT: 148px" border="0">
																<TR>
																	<TD style="HEIGHT: 21px" width="126">Order Date:</TD>
																	<TD style="HEIGHT: 21px">&nbsp;
																		<asp:label id="lblOrderDate" runat="server"></asp:label></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 21px" width="126">Ship Date:</TD>
																	<TD style="HEIGHT: 21px">&nbsp;
																		<asp:label id="lblShipDate" runat="server"></asp:label></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 21px" width="126">Shipping Type:</TD>
																	<TD style="HEIGHT: 21px">&nbsp;
																		<asp:label id="lblShipType" runat="server"></asp:label></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 21px" width="126"># of Boxes:</TD>
																	<TD style="HEIGHT: 21px">&nbsp;
																		<asp:label id="lblBoxNumber" runat="server"></asp:label></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 21px" width="126">Ship Source:</TD>
																	<TD style="HEIGHT: 21px">&nbsp;
																		<asp:label id="lblSource" runat="server"></asp:label></TD>
																</TR>
																<tr>
																	<td colSpan="2"></td>
																</tr>
															</table>
														</td>
													</TR>
													<tr>
														<TD class="fontSmallBoldRedColor" align="left" colSpan="2">For questions regarding 
															this order, contact Customer Service at 1.866.468.5577
														</TD>
													</tr>
												</table>
											</td>
											<td>
												<table cellSpacing="0" cellPadding="0" width="100" border="0">
													<TR>
														<TD>
															<TABLE style="WIDTH: 219px; HEIGHT: 160px" cellSpacing="0" cellPadding="0" width="219"
																align="center" border="0">
																<tr>
																	<td class="fontMediumBold" align="left">Please Select a Track No, then click Track 
																		Order</td>
																</tr>
																<tr>
																	<td><asp:listbox id="lstTrackView" runat="server" Width="300" DataKeyField="StrTrackNo" RepeatDirection="Horizontal"
																			RepeatLayout="Flow" OnItemCommand="dlTrackView_TrackCommand" EnableViewState="True" SelectionMode="Single"></asp:listbox></td>
																</tr>
																<tr>
																	<td align="right"><asp:button id="btnTrackOrder" runat="server" Text="TRACK ORDER" CssClass="buttonYellow"></asp:button></td>
																</tr>
															</TABLE>
														</TD>
													</TR>
												</table>
											</td>
										</tr>
									</TBODY>
								</TABLE>
							</td>
						</tr>
						<tr>
							<td class="innerTableDetail" vAlign="top">
								<TABLE cellSpacing="0" cellPadding="1" width="100%" align="center" border="0">
									<TR>
										<TD class="tableTitle" style="HEIGHT: 27px" colSpan="2">Customer&nbsp;Information</TD>
									</TR>
									<TR>
										<TD width="100">First Name:</TD>
										<TD>&nbsp;
											<asp:label id="lblFirstName" runat="server"></asp:label></TD>
									</TR>
									<TR>
										<TD width="100">Last Name:</TD>
										<TD>&nbsp;
											<asp:label id="lblLastName" runat="server"></asp:label></TD>
									</TR>
									<TR>
										<TD width="100">Email:</TD>
										<TD>&nbsp;
											<asp:label id="lblEmail" runat="server"></asp:label></TD>
									</TR>
									<TR>
										<TD width="100">Home Phone:</TD>
										<TD>&nbsp;
											<asp:label id="lblHPhone" runat="server"></asp:label></TD>
									</TR>
									
									<TR>
										<TD width="100">Employee #:</TD>
										<TD>&nbsp;
											<asp:label id="lblEmpNo" runat="server"></asp:label></TD>
									</TR>
								</TABLE>
							</td>
							<TD class="innerTableDetail" vAlign="top" width="60%">
								<TABLE cellSpacing="0" cellPadding="1" width="100%" align="center" border="0">
									<TBODY>
										<TR>
											<TD class="tableTitle" style="HEIGHT: 26px" colSpan="2">Order&nbsp;Information</TD>
										</TR>
										<tr>
											<td>
												<table style="WIDTH: 512px; HEIGHT: 48px" border= "0">
													<tr>
														<TD width="110" style="WIDTH: 110px; HEIGHT: 12px">PaymentType:</TD>
														<TD style="WIDTH: 93px; HEIGHT: 12px">&nbsp;
															<asp:label id="lblPayType" runat="server"></asp:label></TD>
														<td><table><tr><td><asp:datalist id="dlGCNumView" runat="server" OnItemCommand="dlGCNumView_ItemCommand" Width="18%"
				EnableViewState="True" RepeatLayout="Flow" RepeatDirection="Horizontal" DataKeyField="GCNo">
				<HeaderTemplate>
					<table width="110" cellspacing="0" cellpadding="2" class="innerTableView" align="left">
						<tr class="tableTitle">
							<td align="left" width="110">
								Gift Card #:
							</td>
						</tr>
     			</HeaderTemplate>
				<ItemTemplate>
				     <tr height="18" id="total" runat="server">
						<td align="left">
							<%# DataBinder.Eval(Container.DataItem,"GCNo") %>
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					</table>
				</FooterTemplate>
			</asp:datalist></td></tr></table></td>	
															
													</tr>
													<TR>
														<TD width="110" style="WIDTH: 110px">Order Total:</TD>
														<TD style="WIDTH: 93px">&nbsp;
															<asp:label id="lblTotal" runat="server"></asp:label></TD>
														<TD></TD>
														<TD></TD>
													</TR>
													<TR>
														<TD width="110" style="WIDTH: 110px">Order Items</TD>
														<TD style="WIDTH: 93px">&nbsp;</TD>
														<TD></TD>
														<TD></TD>
													</TR>
												</table>
											</td>
										</tr>
										<TR>
											<TD colSpan="2"><iframe id=frmItem name=OrderItem 
            src="OrderItem.aspx?StrOrderNo=<%=strOrderNumber%>" frameBorder=0 
            width=500 style="width: 526px"></iframe>
											</TD>
										</TR>
										<TR>
											<TD class="fontSmallBoldRedColor" colSpan="2">If product received at your store 
												does not match order information shown above, contact Customer Service at 
												1.866.468.5577&nbsp;</TD>
										</TR>
									</TBODY>
								</TABLE>
							</TD>
						</tr>
						<TR>
							<TD class="innerTableDetail" colSpan="2">
								<TABLE cellSpacing="0" cellPadding="1" width="100%" align="center" border="0">
									<TBODY>
										<TR>
											<TD class="tableTitle" colSpan="3">Store Information</TD>
										</TR>
										<TR>
											<TD width="200">Date Received in Store:</TD>
											<TD width="350">&nbsp;
												<asp:label id="lblDateReceived" runat="server"></asp:label></TD>
											<TD vAlign="top" align="right" rowSpan="3"></TD>
										</TR>
										<TR>
											<TD width="200">Date Customer Picked Up:</TD>
											<TD>&nbsp;
												<asp:label id="lblPickUp" runat="server"></asp:label></TD>
										</TR>
										<TR>
											<TD width="200">Status:</TD>
											<TD>&nbsp;
												<asp:label id="lblStatus" runat="server"></asp:label></TD>
										</TR>
										<TR >
											<TD colSpan="2">
												<table border="0">
													<tr>
														<td style="WIDTH: 227px">
															<table border="0">
																<tr>
																	<td>
																	</td>
																</tr>
															</table>
														</td>
														<td>
															<table border="0">
																<tr>
																	<td>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</TD>
											<TD align="right" valign="middle">
												
											</TD>
										</TR>
										<tr class="PageError">
											<td colSpan="2"><font color="#cc0000"><asp:label id="lblErrorText" runat="server" Visible="false"></asp:label></font></td>
										</tr>
									</TBODY>
								</TABLE>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
				<br>
				<table height="140" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0">
					<tr>
						<td vAlign="top" align="right"><asp:button id="btnReturnToSearch" runat="server" Text="RETURN TO SEARCH" CssClass="buttonRed"></asp:button></td>
					</tr>
				</table>
			</div>
		
			<DIV></DIV>
			<DIV></DIV>
		</form>
	</body>
</HTML>
