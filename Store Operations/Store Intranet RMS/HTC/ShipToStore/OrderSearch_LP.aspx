<%@ Page language="c#" Codebehind="OrderSearch_LP.aspx.cs" AutoEventWireup="false" Inherits="ShipToStore.OrderSearch_LP" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OrderSearch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
		<script language="javascript" type="text/javascript">

            function calendarPicker(strField)
            {

                        window.open('Calendar.aspx?field=' + strField, 'calendarPopup', 'width=250,height=200,resizable=yes');

            }
			function ValidateShipDates(source, args)
				{
					var ShipFrom = document.getElementById("<%=txtShipDateFrom.ClientID %>").value;
					var ShipTo = document.getElementById("<%=txtShipDateTo.ClientID %>").value;
					
					var ShipDate = new Date(args.toString());
								
					
					if (ShipFrom!="" && ShipTo==""){
					args.IsValid = false;					
					}
					else if (ShipFrom=="" && ShipTo!=""){
					args.IsValid=false;
					}
					/*else if (ShipDate.toString() == "NaN" || ShipDate.toString() == "Invalid Date"){
					alert ("??");
					args.IsValid = false;
					}*/
					else
					{
					args.IsValid=true;
					}
					
				} 

		</script>
	</HEAD>
	<body bottomMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header></div>
			<div id="main"><br>
				<TABLE class="innerTable" id="tblOption" cellSpacing="0" cellPadding="1" width="96%" align="center"
					border="0" runat="server">
					<tr class="tableTitle">
						<td colSpan="2">Search Options</td>
					</tr>
					<tr>
						<td class="tableCell" width="100">Region:</td>
						<td><asp:dropdownlist id="ddlRegion" runat="server" AutoPostBack="True" Width="100px" CssClass="tableInput"></asp:dropdownlist></td>
					</tr>
					<tr>
						<td class="tableCell" width="100">District:</td>
						<td><asp:dropdownlist id="ddlDistrict" runat="server" AutoPostBack="True" Width="100px" CssClass="tableInput"></asp:dropdownlist></td>
					</tr>
					<tr>
						<td class="tableCell" width="100">Store:</td>
						<td><asp:dropdownlist id="ddlStore" runat="server" Width="300" CssClass="tableInput"></asp:dropdownlist></td>
					</tr>
				</TABLE>
				<br>
				<TABLE class="innerTable" id="tblRange" cellSpacing="0" cellPadding="1" width="96%" align="center"
					border="0">
					<tr class="tableTitle">
						<td colSpan="4">Date Range</td>
					</tr>
					<tr>
						<td class="tableCell" width="100">Order Date:</td>
						<td vAlign="middle" width="287" style="WIDTH: 287px"><asp:textbox id="txtOrderDateFrom" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtOrderDateFrom');"
								href="javascript:;"><IMG height="25" alt="cal" hspace="3" src="Image/calendar.jpg" width="30" vspace="5"
									border="0"></A><BR>
							<font class="fontSmallBold">Format: MM/DD/YYYY
								<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtOrderDateFrom"
									Display="Dynamic" ErrorMessage="Order Date From Is Required."></asp:RequiredFieldValidator></font></td>
						<TD class="tableCell" width="50">To
						</TD>
						<TD><asp:textbox id="txtOrderDateTo" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtOrderDateTo');"
								href="javascript:;"><IMG height="25" alt="cal" hspace="3" src="Image/calendar.jpg" width="30" vspace="5"
									border="0"></A><BR>
							<font class="fontSmallBold">Format: MM/DD/YYYY
								<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ControlToValidate="txtOrderDateTo" Display="Dynamic"
									ErrorMessage="Order Date To Is Required"></asp:RequiredFieldValidator></font></TD>
					</tr>
					<tr>
						<td class="tableCell" width="100">Ship Date:</td>
						<td width="287" style="WIDTH: 287px"><asp:textbox id="txtShipDateFrom" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtShipDateFrom');"
								href="javascript:;"><IMG height="25" alt="cal" hspace="3" src="Image/calendar.jpg" width="30" vspace="5"
									border="0"></A><BR>
							<font class="fontSmallBold">Format: MM/DD/YYYY
								<asp:CustomValidator id="cvalShipFrom" runat="server" ErrorMessage="Must specify valid begin ship date"
									Display="Dynamic" ControlToValidate="txtShipDateFrom"></asp:CustomValidator></font></td>
						<TD class="tableCell" width="50">To</TD>
						<TD><asp:textbox id="txtShipDateTo" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtShipDateTo');"
								href="javascript:;"><IMG height="25" alt="cal" hspace="3" src="Image/calendar.jpg" width="30" vspace="5"
									border="0"></A>
							<BR>
							<font class="fontSmallBold">Format: MM/DD/YYYY
								<asp:CustomValidator id="cvalShipTo" runat="server" ErrorMessage="Must specify valid end ship date" Display="Dynamic"
									ControlToValidate="txtShipDateTo"></asp:CustomValidator>
								<asp:CustomValidator id="cvalDateRange" runat="server" ErrorMessage="Begin Date must be earlier than End Date"
									Display="Dynamic" ControlToValidate="txtShipDateTo"></asp:CustomValidator></font></TD>
					</tr>
				</TABLE>
				<br>
				<TABLE class="innerTable" id="tblCustomer" cellSpacing="0" cellPadding="1" width="96%"
					align="center" border="0">
					<tr class="tableTitle">
						<td colSpan="2" style="WIDTH: 639px">Customer Search</td>
						<TD style="WIDTH: 125px"></TD>
						<TD></TD>
					</tr>
					<tr>
						<td class="tableCell" width="200">Order Number:</td>
						<td style="WIDTH: 435px"><asp:textbox id="txtOrderNumber" runat="server" CssClass="tableInput" columns="20"></asp:textbox></td>
						<TD style="WIDTH: 125px">Employee&nbsp;Order:</TD>
						<TD><asp:CheckBox ID="chkEmp" Checked="true" runat="server" />
						</TD>
					</tr>
					<tr>
						<td class="tableCell" width="200">Customer First Name:</td>
						<td style="WIDTH: 435px"><asp:textbox id="txtFirstName" runat="server" CssClass="tableInput" columns="35"></asp:textbox></td>
						<TD style="WIDTH: 125px"></TD>
						<TD></TD>
					</tr>
					<tr>
						<td class="tableCell" width="200">Customer Last Name:</td>
						<td style="WIDTH: 435px"><asp:textbox id="txtLastName" runat="server" CssClass="tableInput" columns="35"></asp:textbox></td>
						<TD style="WIDTH: 125px"></TD>
						<TD></TD>
					</tr>
					<tr>
						<td class="tableCell" width="200">Customer Email:</td>
						<td style="WIDTH: 435px"><asp:textbox id="txtEmail" runat="server" CssClass="tableInput" columns="35"></asp:textbox>
							<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" ErrorMessage="Email not in valid format."
								ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" ControlToValidate="txtEmail"></asp:RegularExpressionValidator></td>
						<TD style="WIDTH: 125px"></TD>
						<TD></TD>
					</tr>
					<tr>
						<td class="tableCell" width="200">Status:</td>
						<td style="WIDTH: 435px"><asp:dropdownlist id="ddlStatus" runat="server" CssClass="tableInput" width="180">
								<asp:ListItem Value="7">Choose a Status</asp:ListItem>
								<asp:ListItem Value="1">Pending</asp:ListItem>
								<asp:ListItem Value="2">Shipped</asp:ListItem>
								<asp:ListItem Value="3">Received</asp:ListItem>
								<asp:ListItem Value="4">Picked Up</asp:ListItem>
								<asp:ListItem Value="5">Cancelled</asp:ListItem>
								<asp:ListItem Value="8">Backordered</asp:ListItem>
								<asp:ListItem Value="6">All</asp:ListItem>
							</asp:dropdownlist>
							<asp:CustomValidator id="cvalDdlStatus" runat="server" ErrorMessage="Must Choose a Valid Status" Display="Dynamic"
								ControlToValidate="ddlStatus"></asp:CustomValidator></td>
						<TD style="WIDTH: 125px"></TD>
						<TD></TD>
						
					</tr>
				</TABLE>
				<br>
				<table height="140" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0">
					<tr>
						<td vAlign="top" align="right"><asp:button id="btnSearch" runat="server" CssClass="buttonRed" Text="SEARCH"></asp:button></td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</HTML>
