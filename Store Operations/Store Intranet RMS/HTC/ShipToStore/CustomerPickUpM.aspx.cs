using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;


namespace ShipToStore
{
	/// <summary>
	/// Summary description for CustomerPickUp.
	/// </summary>
	public class CustomerPickUpM : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtCode1;
		protected System.Web.UI.WebControls.TextBox txtCode2;
		protected System.Web.UI.WebControls.TextBox txtCode3;
		protected System.Web.UI.WebControls.TextBox txtCode4;
		protected System.Web.UI.WebControls.TextBox txtCode5;
		protected System.Web.UI.WebControls.Button btnSubmit;
		
		protected System.Web.UI.WebControls.Label lblErrDupInput;
		protected System.Web.UI.WebControls.Label lblErrorText;
		protected System.Web.UI.WebControls.Button btnReset; 
		protected System.Web.UI.HtmlControls.HtmlGenericControl Panel1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl Panel2;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected string strCode1;
		protected string strCode2;
		protected string strCode3;
		protected string strCode4;
		protected string strCode5;
		
		protected ArrayList arrlist = new ArrayList();
		string strFullOrderNo =  string.Empty;
		string strCustEmail =  string.Empty;
		string strDivision = string.Empty;
		int intIsEmail = 0;
		string strCustName =  string.Empty;
		string strCustStreet = string.Empty;
		string strCustCity = string.Empty;
		string strCustState = string.Empty;
		string strCustZip = string.Empty;
		string strStrName = string.Empty;
		string strStrAddr = string.Empty;
		string strStrCity = string.Empty;
		string strStrState = string.Empty;
		string strStrZip = string.Empty;
		protected OrderItemGroup newOrderItmGroup;
		protected OrderItemGroup theOrderItmGroup; 
		string strHTEmailBody = string.Empty;
		string strItemDetail = string.Empty;
		string strSHEmailBody = string.Empty;
		string strHTEmailLogo = ConfigurationSettings.AppSettings["HTEmailLogo"];
		string strSHEmailLogo = ConfigurationSettings.AppSettings["SHEmailLogo"];
		string strHTEmlBotImage = ConfigurationSettings.AppSettings["HTEmlBotImage"];
		string strSHEmlBotImage = ConfigurationSettings.AppSettings["SHEmlBotImage"];
		double dblProdAmt = 0.00;
		double dblTax = 0.00;
		double dblTotPaid = 0.00;
		StringBuilder strHTEmailBodyBl = new StringBuilder();
		string strErrorMsg = string.Empty;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularexpressionvalidator1;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularexpressionvalidator2;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularexpressionvalidator3;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularexpressionvalidator4;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularexpressionvalidator5;

		protected System.Web.UI.WebControls.Button btnSearch;
		DataTable dtUser;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			try
			{
				int dbResult = 0;
				strCode1 =  txtCode1.Text.Trim().ToUpper(); 
				if (strCode1 != "")
				{
					string strCode1Vld = GetValidTrackNo(strCode1);

					arrlist.Add(strCode1Vld);

				}

				strCode2 =  txtCode2.Text.Trim().ToUpper(); 
				if (strCode2 != "")
				{
					string strCode2Vld = GetValidTrackNo(strCode2);

					arrlist.Add(strCode2Vld);
				}

				strCode3 =  txtCode3.Text.Trim().ToUpper(); 
				if (strCode3 != "")
				{
					string strCode3Vld = GetValidTrackNo(strCode3);

					arrlist.Add(strCode3Vld);
				}

				strCode4 =  txtCode4.Text.Trim().ToUpper(); 
				if (strCode4 != "")
				{
					string strCode4Vld = GetValidTrackNo(strCode4);

					arrlist.Add(strCode4Vld);
				}

				strCode5 =  txtCode5.Text.Trim().ToUpper();
				if (strCode5 != "")
				{
					string strCode5Vld = GetValidTrackNo(strCode5);

					arrlist.Add(strCode5Vld);
				}
                                
				dtUser = (DataTable)Session["UserProfile"];
				string strEmployeeID = dtUser.Rows[0]["EmployeeID"].ToString().Trim();
				string strStoreNum = dtUser.Rows[0]["Location"].ToString().Trim();
			
				for(int i = 0; i < arrlist.Count; i++)
				{
					//dbResult = InsertPickUpOrderTrack(arrlist[i].ToString());
					DataTable dtOrders = Data.GetOrdsbyTrackNo(arrlist[i].ToString());

					if (dtOrders != null)
					{
						foreach (DataRow ordRow in dtOrders.Rows) 
						{
							string strFullOrderNo =  ordRow["FULLORDERNO"].ToString().Trim();
							string strOrderTrackNo = strFullOrderNo.Substring (0,8) +  arrlist[i].ToString();
							dbResult = InsertPickUpOrderTrack(arrlist[i].ToString(),strOrderTrackNo,strFullOrderNo);
							if (dbResult ==1)
							{
								if (intIsEmail ==1)
								{
									if ((strCustEmail != "") && (strStrName!= "") && (strCustStreet!= ""))
									{
										theOrderItmGroup = GetOrderItemsForPKEmail(strFullOrderNo);
										//strItemDetail =  "ShipQTY" + " " + "CancelQTY"  + "\t" + "DESCRIPTION" + "                                            "+ "AMOUNT" + "\n";
										strItemDetail =  "<table border= 1 cellpadding='0' cellspacing='0'><tr><td width = 20>ShipQTY</td><td width = 20>CancelQTY</td><td width = 400>DESCRIPTION</td><td>AMOUNT</td></tr>";
                    
										foreach (OneOrderItem oneItem in theOrderItmGroup)
										{
											//strHTItemDetail +=  oneItem.StrQtyShp + "       " + oneItem.StrQtyCancel  + "\t" + "\t"+ oneItem.StrItemDESC + "      " + oneItem.StrSprice + "\n";
											strItemDetail +=  "<tr><td width = 20 align = center>" + oneItem.StrQtyShp + "</td><td width = 20 align = center>" + oneItem.StrQtyCancel  + "</td><td width = 400>"+ oneItem.StrItemDESC + "</td><td Align = right>" + double.Parse(oneItem.StrSprice).ToString("0.00")  + "</td></tr>";
									
											//strItemDetail +=  oneItem.StrQtyShp + "       " + oneItem.StrQtyCancel  + "\t" + "\t"+ oneItem.StrItemDESC + "     " + double.Parse(oneItem.StrSprice).ToString("0.00")  + "\n";
											dblProdAmt = double.Parse(oneItem.StrTotSprice);
											dblTax = double.Parse(oneItem.StrCprice);
										}
										dblTotPaid = dblProdAmt + dblTax + 1.00;
										strItemDetail += "<tr><td width = 20></td><td width = 20></td><td Align = right> PRODUCT:</td><td Align = right>"  + dblProdAmt.ToString("0.00")+ "</td></tr>";      //dblProdAmt.ToString("0.00") + "\n";
										strItemDetail += "<tr><td width = 20></td><td width = 20></td><td Align = right> TAX:" + "</td><td Align = right>" + dblTax.ToString("0.00") + "</td></tr>";
										strItemDetail += "<tr><td width = 20></td><td width = 20></td><td Align = right> SHIPPING AND HANDLING:" + "</td><td Align = right>" + "1.00" + "</td></tr>";
										strItemDetail += "<tr><td width = 20></td><td width = 20></td><td Align = right> *TOTAL PAID:" + "</td><td Align = right>" + dblTotPaid.ToString("0.00")+ "</td></tr></table>";
										//prod need change
										//strCustEmail = ConfigurationSettings.AppSettings["EmailRec"].ToString();
										string strCustEmail1 = ConfigurationSettings.AppSettings["EmailRec1"].ToString();
										//string strCustEmail2 = ConfigurationSettings.AppSettings["EmailRec2"].ToString();
										//string strCustEmail3 = ConfigurationSettings.AppSettings["EmailRec3"].ToString();
										if (strDivision =="1")
										{
											Emails.SendText(strCustEmail1, "HotTopic Order Picked Up for cust email " + strCustEmail, GetHTEmailBody(strFullOrderNo));
											//Emails.SendText(strCustEmail, "HotTopic Order Picked Up", GetHTEmailBody(strFullOrderNo));
									
											//Emails.SendText(strCustEmail2, "HotTopic Order Picked Up for cust email " + strCustEmail, GetHTEmailBody(strFullOrderNo));
											//Emails.SendText(strCustEmail3, "HotTopic Order Picked Up for cust email " + strCustEmail, GetHTEmailBody(strFullOrderNo));

										}
										else if(strDivision =="8")
										{
											//Emails.SendText(strCustEmail, "ShockHound Order Picked Up", GetSHEmailBody(strFullOrderNo));
											Emails.SendText(strCustEmail1, "ShockHound Order Picked Up" + strCustEmail, GetSHEmailBody(strFullOrderNo));
											//Emails.SendText(strCustEmail2, "ShockHound Order Picked Up" + strCustEmail, GetSHEmailBody(strFullOrderNo));
											//Emails.SendText(strCustEmail3, "ShockHound Order Picked Up" + strCustEmail, GetSHEmailBody(strFullOrderNo));
										}
										strErrorMsg += "Tracking No #" + arrlist[i].ToString() +" successful picked up. " + "\n";
										ShowError(strErrorMsg); 
										//track store employee scan box time for picked up status. 
										Data.TrackEmployeeScan(arrlist[i].ToString(), strFullOrderNo,strEmployeeID,strStoreNum,"P");
									}
									else 
									{
										strErrorMsg += "Could not get enough customer and store info to send email out from database;" + "<br>";
										ShowError(strErrorMsg); 
									}
								}
							}
							else if (dbResult ==2) //DUPLICATE, entry already
							{ 
								strErrorMsg += "Tracking #" + arrlist[i].ToString() +" has already been checked in for pick up;" + "<br>";
								ShowError(strErrorMsg); 
							}
							else if (dbResult ==5) //not receive in store
							{
								strErrorMsg += "Tracking #" + arrlist[i].ToString()+" has not been received at store yet;"+ "<br>";
								ShowError(strErrorMsg); 
							}
							else if (dbResult ==6) //not complete receive in store
							{
								strErrorMsg += "Tracking No #" + arrlist[i].ToString() +" Picked Up. (Partial order) " + "\n";
						
								ShowError(strErrorMsg); 
							}
								//							else if (dbResult ==4) //can not find order due to order did not receive in store
								//							{
								//								strErrorMsg += "Tracking #" + arrlist[i].ToString()+" has not been checked in at store yet; "+ "<br>";
								//								ShowError(strErrorMsg); 
								//							}
							else 
							{
								strErrorMsg += " There was an error while entering Tracking No # "+ arrlist[i].ToString()+  "into DB;" + "\n";
								ShowError(strErrorMsg); 
							}
						}
					}
					else 
					{
						strErrorMsg += "OrderNo does not exist for Tracking No #" + arrlist[i].ToString()+";"+ "\n";
						ShowError(strErrorMsg); 
					}
				} //end of loop
				Reset();
			}
			catch(Exception ex)
			{
				ShowError(ex.Message);
				//Response.Redirect("~/Error.aspx");
			}
		
		}
		private void ShowError(string strErrMsg)
		{
			if (strErrMsg.Trim()!="")
			{
				lblErrorText.Text = strErrMsg;
				lblErrorText.Visible= true;
			}
		}

		private string GetHTEmailBody(string strFullOrderNo)
		{
			strHTEmailBody = "<table border =0><tr><td colspan=4><img src='" + strHTEmailLogo + "'></td></tr><tr><td  colspan=4>Hi " + strCustName + ": </td></tr>" 
				+ "<tr><td colspan=4>Thanks for shopping with us!  We see that you�ve picked up your in-store kiosk order "+ strFullOrderNo.Substring(0,8) +".</td></tr>"
				+ "<tr><td colspan=4>We really appreciate your business and hope you come back and shop with us again soon!</td></tr><tr><td colspan = 4></td></tr><tr><td colspan=4>&nbsp;</td></tr>"; 

			strHTEmailBody += strItemDetail;
			strHTEmailBody += strHTEmailBodyBl;
			strHTEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4>*If you paid with a credit card, the total paid amount was charged to your card at the time your order was shipped.</td></tr>";
			strHTEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan = 4>Thanks! </td></tr><tr><td colspan=4></td><tr><tr><td td colspan=4>" + "Hottopic.com</td></tr>" + "<tr><td td colspan=4>Customer Service</td></tr>" + "<tr><td td colspan=4><a href='http://community.hottopic.com/contact/customer_service'>http://community.hottopic.com/contact/customer_service</a></td></tr>";
			//			strHTEmailBody += "<tr><td colspan=4><img src='http://localhost/Home/ShipToStore_4/Image/HTEmlBotImg.gif'></td></tr></table>";
			strHTEmailBody += "<tr><td colspan=4><img src='" + strHTEmlBotImage + "'></td></tr></table>";
						
			return strHTEmailBody;
		}
 
		private string GetSHEmailBody(string strFullOrderNo)
		{
			strSHEmailBody = "<table border =0><tr><td colspan=4><img src='" + strSHEmailLogo + "'></td></tr><tr><td  colspan=4>Hi " + strCustName + ": </td></tr>" 
				+ "<tr><td colspan=4>Thanks for shopping with us.  We see that you have picked up your order "+ strFullOrderNo.Substring(0,8) +" that you have placed on our kiosk.</td></tr>"
				+ "<tr><td colspan=4>We appreciate your business and would love to have you as a regular customer. </td></tr><tr><td colspan=4>&nbsp;</td></tr><tr><td colspan=4>&nbsp;</td></tr>";

			strSHEmailBody += strItemDetail;
			strSHEmailBody += strHTEmailBodyBl;
			strSHEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4>Thanks again for shopping on ShockHound.com!</td></tr><tr><td td colspan=4></td></tr>";
			strSHEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4>ShockHound Customer Service </td></tr><tr><td td colspan=4>" + "ShockHound is all about the fans; let us know what you think. If you ever need ShockHound customer </td></tr>" + "<tr><td td colspan=4>service, contact us at 866-272-1124 (US & Canada), or email us by going to <a href='http://shockhound.com'>http://shockhound.com </a> </td></tr>";
			//strSHEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4><img src='http://localhost/Home/ShipToStore_4/Image/SHEmlBotImg.gif'></td></tr></table>";
			strSHEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4><img src='" + strSHEmlBotImage + "'></td></tr></table>";
		
			return strSHEmailBody;
		}

	

		private OrderItemGroup GetOrderItemsForPKEmail(string strFullOrderNo)
		{
			newOrderItmGroup = OrderItemGroup.GetOrderItemForEmail(strFullOrderNo);	
			return newOrderItmGroup;
		}
		 
		private int InsertPickUpOrderTrack(string strTrackNo,string strOrderTrackNo, string strFullOrderNo)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();
			int intResultRec = 0;

			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				cm.CommandText = "usp_CustPickUp_Update_ByOrdTrack";
				cm.CommandTimeout=90;
				              
				cm.Parameters.Clear();

				SqlParameter aaa = new SqlParameter();
				aaa.ParameterName = "@OrderTrackNo";
				aaa.SqlDbType = SqlDbType.VarChar;
				aaa.Size=52;
				aaa.Value = strOrderTrackNo;
				cm.Parameters.Add(aaa);

				SqlParameter qqq = new SqlParameter();
				qqq.ParameterName = "@TRACKNO";
				qqq.SqlDbType = SqlDbType.VarChar;
				qqq.Value = strTrackNo;
				cm.Parameters.Add(qqq);

				SqlParameter sss = new SqlParameter();
				sss.ParameterName = "@Result";
				sss.SqlDbType = SqlDbType.Int;
				sss.Value = intResultRec;
				sss.Direction = ParameterDirection.Output;
				cm.Parameters.Add(sss);

				SqlParameter hhh = new SqlParameter();
				hhh.ParameterName = "@FullOrderNo";
				hhh.SqlDbType = SqlDbType.VarChar;
				hhh.Size=12; 
				hhh.Value = strFullOrderNo;
				cm.Parameters.Add(hhh);
		
				SqlParameter jjj = new SqlParameter();
				jjj.ParameterName = "@CustEmail";
				jjj.SqlDbType = SqlDbType.VarChar;
				jjj.Size = 50;
				jjj.Value = strCustEmail;
				jjj.Direction = ParameterDirection.Output;
				cm.Parameters.Add(jjj);

				SqlParameter xxx = new SqlParameter();
				xxx.ParameterName = "@Division";
				xxx.SqlDbType = SqlDbType.VarChar;
				xxx.Size = 4;
				xxx.Value = strDivision;
				xxx.Direction = ParameterDirection.Output;
				cm.Parameters.Add(xxx);

				SqlParameter mmm = new SqlParameter();
				mmm.ParameterName = "@IsEmail";
				mmm.SqlDbType = SqlDbType.Int;
				mmm.Value = intIsEmail;
				mmm.Direction = ParameterDirection.Output;
				cm.Parameters.Add(mmm);

				SqlParameter kkk = new SqlParameter();
				kkk.ParameterName = "@CustName";
				kkk.SqlDbType = SqlDbType.VarChar;
				kkk.Size = 32;
				kkk.Value = strCustName;
				kkk.Direction = ParameterDirection.Output;
				cm.Parameters.Add(kkk);

				SqlParameter rrr = new SqlParameter();
				rrr.ParameterName = "@Street";
				rrr.SqlDbType = SqlDbType.VarChar;
				rrr.Size = 30;
				rrr.Value = strCustStreet;
				rrr.Direction = ParameterDirection.Output;
				cm.Parameters.Add(rrr);

				SqlParameter ttt = new SqlParameter();
				ttt.ParameterName = "@City";
				ttt.SqlDbType = SqlDbType.VarChar;
				ttt.Size = 30;
				ttt.Value = strCustCity;
				ttt.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ttt);

				SqlParameter uuu = new SqlParameter();
				uuu.ParameterName = "@State";
				uuu.SqlDbType = SqlDbType.VarChar;
				uuu.Size = 2;
				uuu.Value = strCustState;
				uuu.Direction = ParameterDirection.Output;
				cm.Parameters.Add(uuu);

				SqlParameter vvv = new SqlParameter();
				vvv.ParameterName = "@Zip";
				vvv.SqlDbType = SqlDbType.VarChar;
				vvv.Size = 14;
				vvv.Value = strCustZip;
				vvv.Direction = ParameterDirection.Output;
				cm.Parameters.Add(vvv);

				SqlParameter www = new SqlParameter();
				www.ParameterName = "@StoreName";
				www.SqlDbType = SqlDbType.VarChar;
				www.Size = 50;
				www.Value = strStrName;
				www.Direction = ParameterDirection.Output;
				cm.Parameters.Add(www);

				SqlParameter yyy = new SqlParameter();
				yyy.ParameterName = "@StrAddress";
				yyy.SqlDbType = SqlDbType.VarChar;
				yyy.Size = 105;
				yyy.Value = strStrAddr;
				yyy.Direction = ParameterDirection.Output;
				cm.Parameters.Add(yyy);

				SqlParameter ppp = new SqlParameter();
				ppp.ParameterName = "@StrCity";
				ppp.SqlDbType = SqlDbType.VarChar;
				ppp.Size = 35;
				ppp.Value = strStrCity;
				ppp.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ppp);

				SqlParameter bbb = new SqlParameter();
				bbb.ParameterName = "@StrState";
				bbb.SqlDbType = SqlDbType.VarChar;
				bbb.Size = 2;
				bbb.Value = strStrState;
				bbb.Direction = ParameterDirection.Output;
				cm.Parameters.Add(bbb);

				SqlParameter ooo = new SqlParameter();
				ooo.ParameterName = "@StrZip";
				ooo.SqlDbType = SqlDbType.VarChar;
				ooo.Size = 35;
				ooo.Value = strStrZip;
				ooo.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ooo);

				cm.ExecuteNonQuery();

				intResultRec = Convert.ToInt32(cm.Parameters["@Result"].Value);
				//				strFullOrderNo = Convert.ToString(cm.Parameters["@FullOrderNo"].Value);
				strCustEmail = Convert.ToString(cm.Parameters["@CustEmail"].Value);
				strDivision = Convert.ToString(cm.Parameters["@Division"].Value);
				intIsEmail = Convert.ToInt32(cm.Parameters["@IsEmail"].Value);
				strCustName = Convert.ToString(cm.Parameters["@CustName"].Value);
				strCustStreet = Convert.ToString(cm.Parameters["@Street"].Value);
				strCustCity = Convert.ToString(cm.Parameters["@City"].Value);
				strCustState = Convert.ToString(cm.Parameters["@State"].Value);
				strCustZip = Convert.ToString(cm.Parameters["@Zip"].Value);
				strStrName = Convert.ToString(cm.Parameters["@StoreName"].Value);
				strStrAddr = Convert.ToString(cm.Parameters["@StrAddress"].Value);
				strStrCity = Convert.ToString(cm.Parameters["@StrCity"].Value);
				strStrState = Convert.ToString(cm.Parameters["@StrState"].Value);
				strStrZip = Convert.ToString(cm.Parameters["@StrZip"].Value);

			}
			catch (SqlException ex)
			{
				intResultRec = 99;
			}
			
			finally
			{
				cn.Close();
			}
			return intResultRec;
		}

		private void btnReset_Click(object sender, System.EventArgs e)
		{
			txtCode1.Text = String.Empty;
			txtCode2.Text = String.Empty;
			txtCode3.Text = String.Empty;
			txtCode4.Text = String.Empty;
			txtCode5.Text = String.Empty;

			lblErrorText.Text = String.Empty;
			lblErrorText.Visible= false;
		}
		private void Reset()
		{
			txtCode1.Text = String.Empty;
			txtCode2.Text = String.Empty;
			txtCode3.Text = String.Empty;
			txtCode4.Text = String.Empty;
			txtCode5.Text = String.Empty;

		}

		private string GetValidTrackNo(string strInputTrack)
		{
			string strValidTrackNo =string.Empty;

			if (strInputTrack.Length ==22) 
			{
				if (strInputTrack.Substring(0,7)=="9612804") //
				{
					strValidTrackNo = strInputTrack.Substring(7, 15);
				}
				else
				{
					strValidTrackNo = strInputTrack;
				}
			}
			else if (strInputTrack.Length ==30)
			{
				if (strInputTrack.Substring(0,3)=="420") //usps first class and piority start with 420 
				{
					strValidTrackNo = strInputTrack.Substring(8, 22);
				}
				else
				{
					strValidTrackNo = strInputTrack;
				}
			}
			else
			{
				strValidTrackNo = strInputTrack;
			}

			return strValidTrackNo;
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("./StoreActionOption.aspx");
		}
	}
}
