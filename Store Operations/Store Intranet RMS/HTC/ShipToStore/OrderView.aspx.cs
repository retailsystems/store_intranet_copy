using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for OrderView.
	/// </summary>
	public class OrderView : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.DropDownList ddlRegion;
		protected System.Web.UI.WebControls.DropDownList ddlDistrict;
		protected System.Web.UI.WebControls.DropDownList ddlStore;
		protected System.Web.UI.WebControls.DropDownList ddlSource;
		protected System.Web.UI.WebControls.DropDownList ddlBrand;
		protected System.Web.UI.WebControls.DropDownList ddlChannel;
		protected System.Web.UI.WebControls.DataList dlOrderView;
		protected System.Web.UI.WebControls.LinkButton lnkPrev;
		protected System.Web.UI.WebControls.TextBox txtPageCurrent;
		protected System.Web.UI.WebControls.Label lblTotal;
		protected System.Web.UI.WebControls.Button btnJump;
		protected System.Web.UI.WebControls.LinkButton lnkNext;
		protected System.Web.UI.WebControls.LinkButton lnkPreviousTop;
		protected System.Web.UI.WebControls.TextBox txtPageNoTop;
		protected System.Web.UI.WebControls.Label lbltxtPageTotalTop;
		protected System.Web.UI.WebControls.LinkButton lnkNextTop;
		protected System.Web.UI.WebControls.Button btnJumpTop;
        protected System.Web.UI.WebControls.Button btnMenu;
		protected System.Web.UI.WebControls.LinkButton lnkExport;
		protected System.Web.UI.WebControls.LinkButton lnkPrint;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
        //protected System.Web.UI.WebControls.Button btnFilter_Click;
		protected double TotalSales;
		protected int TotalCount;
		protected double ADT;
		protected double UPT;
		protected OrderGroup newOrderGroup;
		protected System.Web.UI.WebControls.Button btnFilter;
		protected Order oneOrder;
		DataTable dtUser;
		ListItem itmAll;
		PagedDataSource sGroup;
		int intPermit;
        
		DataTable dtDevice;
		DataTable dtSourceCd;
		protected string strSourceNm = string.Empty; 
		

		public int CurrentPage
		{

			get
			{
				// look for current page in ViewState
				object o = this.ViewState["_CurrentPage"];
				if (o == null)

					return 0; // default to showing the first page
				else
					return (int)o;
			}
			set
			{
				this.ViewState["_CurrentPage"] = value;

			}
		}
		public int PageSize
		{
			get
			{
				return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["PageSize"]);

			}
		}
		private void Page_Load(object sender, System.EventArgs e)
		{

			sGroup = new PagedDataSource();
			sGroup.AllowPaging = true;
			sGroup.PageSize =PageSize;
		
			//get user DataTable from session
			dtUser = (DataTable)Session["UserProfile"];

			if (!IsPostBack)
			{
				
				//load default drop down
				//LoadRegionDropDown();
				//load Data
				string strReg = Request.QueryString["Reg"].Trim();
				string strDis = Request.QueryString["Dis"].Trim();
		
                string strStr = Request.QueryString["Str"].Trim();
				//string strStr = strStr1.ToString().PadLeft(4, '0');

				if (strStr != "0")
				{
				   strStr = strStr.ToString().PadLeft(4, '0');
				}

				//make sure the user's current Security allows him/her to browse this Reg-Dis-Store combo
				intPermit = int.Parse(Session["ViewPermission"].ToString());
				string userCode = Utility.ParseDepartment(dtUser.Rows[0]["Dept"].ToString().Trim());
				string userLoc = dtUser.Rows[0]["Location"].ToString().Trim();				
				string userRegion = Session["UserRegion"].ToString();
                if (intPermit == 4){
                    btnMenu.Visible = true;
                }
				if (Utility.CheckSecurity(intPermit, strReg, strDis, strStr, userCode, userLoc, userRegion)==true)
				{
					LoadUserOptions();

					string strOrdFDt = Request.QueryString["OrdFDt"].Trim();
					string strOrdTDt = Request.QueryString["OrdTDt"].Trim();
					string strShpFDt;
					string strShpTDt;

					if (Request.QueryString["ShpFDt"]!="" && Request.QueryString["ShpTDt"]!="")
					{
						strShpFDt = Request.QueryString["ShpFDt"].Trim();
						strShpTDt = Request.QueryString["ShpTDt"].Trim();
					}
					else
					{
						strShpFDt = "1/1/1980";
						strShpTDt = "1/1/1980";
					}
					
					string strOrderNo = Request.QueryString["OrdNo"].Trim();
					string strFName = Request.QueryString["FName"].Trim();
					string strLName = Request.QueryString["LName"].Trim();
					string strEmail = Request.QueryString["Email"].Trim();
					int strStatus = int.Parse(Request.QueryString["Stat"].Trim());
				

					//GetOrders
					newOrderGroup = OrderGroup.GetOrderGroup(strReg, strDis, strStr, 
						DateTime.Parse(strOrdFDt),DateTime.Parse(strOrdTDt),DateTime.Parse(strShpFDt),
						DateTime.Parse(strShpTDt),strOrderNo,strFName, strLName,strEmail, strStatus, 0, 0, 0);
					ViewState["OrderGroup"] = newOrderGroup;
					LoadData(CurrentPage);
				}
				else

				{
					Response.Redirect("./Login.aspx?Error=NoPermission");

			
				}
				
			}
			else
			{
				newOrderGroup = (OrderGroup)ViewState["OrderGroup"];
                TotalSales = Math.Round(newOrderGroup.TotalAmount, 2);
                TotalCount = newOrderGroup.Count;
                ADT = (TotalCount == 0 ? 0 : Math.Round(TotalSales / TotalCount, 2));
                UPT = (TotalCount == 0 ? 0 : newOrderGroup.TotalUnit / TotalCount);	

			}
			
		}

		#region DropDown Options

		protected void LoadUserOptions()
		{
			
		
			switch (intPermit)
			{
				case 1:
					//admin show all
				case 5:
					//executive show all
				case 6:
					//LP show all
					LoadRegionDropDown();
					break;
				case 2:
					//RD show only that region
					LoadRegionOnly();
					break;
				case 3:
					//DM show only that district
					LoadDistrictOnly();
					break;
				case 4:
					//Store show only that store
					LoadStoreOnly();
					break;

			}

            LoadChannel();

		}

        private void LoadChannel()
        {

            dtSourceCd = Data.GetChannels();
            this.ddlChannel.DataSource = dtSourceCd;
            ddlChannel.DataTextField = "CHNAME";
            ddlChannel.DataValueField = "CHNO";
            ddlChannel.DataBind();

            ListItem itmAllC = new ListItem();
            itmAllC.Text = "ALL";
            itmAllC.Value = "0";
            ddlChannel.Items.Insert(0, itmAllC);
        }
		private void LoadRegionOnly()
		{
			ddlRegion.Items.Clear();
			ddlDistrict.Items.Clear();
			ddlStore.Items.Clear();

			dtUser = (DataTable)Session["UserProfile"];
			string RegionNo = Utility.ParseDepartment(dtUser.Rows[0]["Dept"].ToString().Trim());

			DataTable dtDistrict = Data.GetDistrict(RegionNo);

			if (dtDistrict!=null)
			{
				string strRegion = dtDistrict.Rows[0]["Region"].ToString().Trim();
				string strDistrict = dtDistrict.Rows[0]["District"].ToString().Trim();
			
				ListItem itmRegion = new ListItem();
				itmRegion.Text=strRegion;
				itmRegion.Value=strRegion;
				ddlRegion.Items.Add(itmRegion);
				ddlRegion.Enabled = false;
				ddlDistrict.DataSource = dtDistrict;
				ddlDistrict.DataTextField="District";
				ddlDistrict.DataValueField="District";
				ddlDistrict.DataBind();
				LoadDefaultOptions(ddlDistrict, false);
				LoadDefaultOptions(ddlStore, true);
					
			}
			
		}

		private void LoadDistrictOnly()
		{
			ddlRegion.Items.Clear();
			ddlDistrict.Items.Clear();
			ddlStore.Items.Clear();

			dtUser = (DataTable)Session["UserProfile"];
			string DistrictNo = Utility.ParseDepartment(dtUser.Rows[0]["Dept"].ToString().Trim());

			DataTable dtStore = Data.GetStore(DistrictNo);

			if (dtStore!=null)
			{
				string strRegion = dtStore.Rows[0]["Region"].ToString().Trim();
				string strDistrict = dtStore.Rows[0]["District"].ToString().Trim();
			

				ListItem itmRegion = new ListItem();
				itmRegion.Text=strRegion;
				itmRegion.Value=strRegion;
				ddlRegion.Items.Add(itmRegion);
				ddlRegion.Enabled = false;

				ListItem itmDistrict = new ListItem();
				itmDistrict.Text=strDistrict;
				itmDistrict.Value=strDistrict;
				ddlDistrict.Items.Add(itmDistrict);
				ddlDistrict.Enabled = false;

				ddlStore.DataSource = dtStore;
				ddlStore.DataTextField="StoreName";
				ddlStore.DataValueField="StoreNum";
				ddlStore.DataBind();
				LoadDefaultOptions(ddlStore, false);
				//ddlStore.Enabled = true;			
			}			
		}

		private void LoadStoreOnly()
		{
			ddlRegion.Items.Clear();
			ddlDistrict.Items.Clear();
			ddlStore.Items.Clear();
			dtUser = (DataTable)Session["UserProfile"];
			string StoreNo = dtUser.Rows[0]["Location"].ToString().Trim();

			DataTable dtStore = Data.GetStoreSingle(StoreNo);

			if (dtStore!=null)
			{
				string strRegion = dtStore.Rows[0]["Region"].ToString().Trim();
				string strDistrict = dtStore.Rows[0]["District"].ToString().Trim();
				string strStoreName = dtStore.Rows[0]["StoreName"].ToString().Trim();

				ListItem itmRegion = new ListItem();
				itmRegion.Text=strRegion;
				itmRegion.Value=strRegion;
				ddlRegion.Items.Add(itmRegion);
				ddlRegion.Enabled = false;

				ListItem itmDistrict = new ListItem();
				itmDistrict.Text=strDistrict;
				itmDistrict.Value=strDistrict;
				ddlDistrict.Items.Add(itmDistrict);
				ddlDistrict.Enabled = false;

				ListItem itmStore = new ListItem();
				itmStore.Text=strStoreName;
				itmStore.Value=StoreNo;
				ddlStore.Items.Add(itmStore);
				ddlStore.Enabled = false;			
			}
			
		}
		#endregion
		#region LoadData
		private void LoadData(int CurrentPage)
		{			
				sGroup.CurrentPageIndex = CurrentPage;
				sGroup.DataSource = newOrderGroup;					
				dlOrderView.DataSource = sGroup; 					
				dlOrderView.DataBind();

				//paging 
				this.txtPageNoTop.Text = (CurrentPage+1).ToString();
				this.txtPageCurrent.Text=(CurrentPage+1).ToString();
				this.lbltxtPageTotalTop.Text = sGroup.PageCount.ToString();
				this.lblTotal.Text = sGroup.PageCount.ToString();
				this.lnkPrev.Enabled = !sGroup.IsFirstPage;
				this.lnkPreviousTop.Enabled = !sGroup.IsFirstPage;
				this.lnkNext.Enabled = !sGroup.IsLastPage;
				this.lnkNextTop.Enabled = !sGroup.IsLastPage;

				TotalSales = Math.Round(newOrderGroup.TotalAmount, 2);
				TotalCount =newOrderGroup.Count;
				ADT = (TotalCount==0? 0: Math.Round(TotalSales/TotalCount, 2));
				UPT = (TotalCount==0? 0: newOrderGroup.TotalUnit/TotalCount);				
		}

		#endregion
		private bool CheckSecurity(int permit, string region, string district, string store)
		{
			string userCode = Utility.ParseDepartment(dtUser.Rows[0]["Dept"].ToString().Trim());
			string userLoc = dtUser.Rows[0]["Location"].ToString().Trim();
			bool isOK = false;
			switch (permit)
			{
				case 1:
					isOK = true;
					break;
				case 5:
					isOK = true;
					break;
				case 2:
					//RD, see if they are inside their own region
					if (userCode == region)
					{
						isOK = true;
					}
					break;
					
				case 3:
					//DM, see if they are inside their own region and district
					if (userCode== district)
					{
						isOK = true;
					}
					break;
				case 4:
					//Store, see if they are inside their own store
					if (userLoc== store)
					{
						isOK = true;
					}
					break;
			}
			return isOK;

		}


		private void LoadDefaultOptions(DropDownList ddlList, bool IsAllSelected)
		{
			itmAll = new ListItem();
			itmAll.Text="All";
			itmAll.Value = "0";

			ddlList.Items.Insert(0, itmAll);
			if (IsAllSelected)
			{
				ddlList.Enabled = false;

			}
			

		}
		private void LoadRegionDropDown()
		{
			
				DataTable dtRegion =(DataTable)Application["dtRegion"];
				
				this.ddlRegion.DataSource = dtRegion;
				this.ddlRegion.DataTextField = "Region";
				this.ddlRegion.DataValueField = "Region";
				ddlRegion.DataBind();
				//make sure all is selected
				LoadDefaultOptions(ddlRegion, false);
				LoadDefaultOptions(ddlDistrict, true);
				LoadDefaultOptions(ddlStore, true);
			
			
		

		}


		//get order details
		//		protected void GetOrderDetail()
		//		{
		//			dlOrderView.DataSource = ShipToStoreObject.OrderGroup.GetOrderDataTable(strStr, DateTime.Parse(strOrdFDt),DateTime.Parse(strOrdTDt),DateTime.Parse(strShpFDt),DateTime.Parse(strShpTDt),strOrderNo,strFName, strLName,strEmail);		        
		//			
		//			dlOrderView.DataBind();
		//			
		//		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   
			this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
			this.lnkExport.Click += new System.EventHandler(this.lnkExport_Click);
			this.lnkPrint.Click += new System.EventHandler(this.lnkPrint_Click);
			this.lnkPreviousTop.Click += new System.EventHandler(this.lnkPreviousTop_Click);
			this.btnJumpTop.Click += new System.EventHandler(this.btnJumpTop_Click);
			this.lnkNextTop.Click += new System.EventHandler(this.lnkNextTop_Click);
			this.dlOrderView.SelectedIndexChanged += new System.EventHandler(this.dlOrderView_SelectedIndexChanged);
			this.lnkPrev.Click += new System.EventHandler(this.lnkPrev_Click);
			this.btnJump.Click += new System.EventHandler(this.btnJump_Click);
			this.lnkNext.Click += new System.EventHandler(this.lnkNext_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void ddlRegion_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (ddlRegion.SelectedValue!="0")
			{
				this.ddlStore.Enabled = false;
				this.ddlDistrict.Enabled = true;			
			
				//depends on selection, load District
				string strR = ddlRegion.SelectedValue;
				DataTable dtDistrict = Data.GetDistrict(strR);
				ddlDistrict.DataSource = dtDistrict;
				this.ddlDistrict.DataTextField = "District";
				this.ddlDistrict.DataValueField = "District";

				LoadDefaultOptions(ddlDistrict, false);
				ddlDistrict.DataBind();
				LoadDefaultOptions(ddlDistrict, false);
			}
			else
			{
				ddlDistrict.Items.Clear();
				ddlStore.Items.Clear();

				LoadDefaultOptions(ddlDistrict, true);
				LoadDefaultOptions(ddlStore, true);
		

			}

		}

		protected void ddlDistrict_SelectedIndexChanged(object sender, System.EventArgs e)
		{

			//depends on selection, load stores
			if (ddlDistrict.SelectedValue!="0")
			{
				this.ddlStore.Enabled = true;
			
			
				//depends on selection, load Store
				string strD = ddlDistrict.SelectedValue;
				DataTable dtStore = Data.GetStore(strD);
				ddlStore.DataSource = dtStore;
				this.ddlStore.DataTextField = "StoreName";
				this.ddlStore.DataValueField = "StoreNum";

				
				ddlStore.DataBind();
				LoadDefaultOptions(ddlStore, false);
			}
			else

			{
				ddlStore.Items.Clear();
				LoadDefaultOptions(ddlStore, true);
				
			}

		}

		private void dlOrderView_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		public void dlOrderView_ItemCommand(object sender, DataListCommandEventArgs e)
		{
			if (e.CommandName=="OrderDetail")
			{
				string strOrderNumber = e.CommandArgument.ToString();
			    Response.Redirect("./OrderDetail.aspx?StrOrderNo=" + strOrderNumber);
       
			}
			else if (e.CommandName=="TrackInfo")
			{
                int nItem = e.Item.ItemIndex;
                LinkButton theLink = (LinkButton)e.Item.FindControl("lnkClient");
                string strOrderNumber = null;
                if (theLink != null)
                {
                    strOrderNumber = theLink.ToolTip.ToString();
                }

               // string strOrderNumber = e.CommandArgument.ToString();

                Response.Redirect("./OrderDetail.aspx?StrOrderNo=" + strOrderNumber);

                //string strTrackNo = e.CommandArgument.ToString();
                //if (strTrackNo.Length == 22)
                //{
                //    Response.Redirect("http://www.usps.com/shipping/trackandconfirm.htm");
                //}
                //else
                //{
                //    Response.Redirect("http://www.fedex.com/Tracking?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers=" + strTrackNo);
                //}
			}
            
		}
		private void lnkExport_Click(object sender, System.EventArgs e)
		{
			

			if (newOrderGroup!=null)
			{
				
				TotalSales = Math.Round(newOrderGroup.TotalAmount, 2);
				TotalCount =newOrderGroup.Count;
				ADT = (TotalCount==0? 0: Math.Round(TotalSales/TotalCount, 2));
				UPT = (TotalCount==0? 0: newOrderGroup.TotalUnit/TotalCount);
				
				//DataList dlExportView = new DataList 

				dlOrderView.DataSource = newOrderGroup;
				dlOrderView.DataBind();

				Response.Clear();
				Response.AddHeader("content-disposition", "attachment;filename=OrderSearchExport.xls");
				Response.Charset = "";
				Response.Cache.SetCacheability(HttpCacheability.NoCache);
				//Response.ContentType = "application/vnd.xls";
				Response.ContentType = "application/vnd.ms-excel";
				String strStyle  = "<style>td{mso-style-parent:text; mso-number-format:\\@;white-space:normal;border:1px solid black;height:50px;width:100px;}</style>";
				System.IO.StringWriter stringWrite = new System.IO.StringWriter();
				System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
			
				this.ClearControls(dlOrderView);
				dlOrderView.RenderControl(htmlWrite);
				Response.Write(strStyle);
				Response.Write(stringWrite.ToString());
				Response.End();
			}
		}
		private void lnkPrint_Click(object sender, System.EventArgs e)
		{
			

			if (newOrderGroup!=null)
			{
				
				Session["PrintObject"] = newOrderGroup;
				this.RegisterStartupScript("Print", "<script>OpenPrint()</script>");
			
			}
		}
	
		private void ClearControls(Control control)
		{
			for (int i=control.Controls.Count -1; i>=0; i--)
			{
				ClearControls(control.Controls[i]);
			}
			if (!(control is TableCell))
			{
				if (control.GetType().GetProperty("SelectedItem") != null)
				{
					LiteralControl literal = new LiteralControl();
					control.Parent.Controls.Add(literal);
					try
					{
							literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control,null);
					}
					catch
					{
					}
					control.Parent.Controls.Remove(control);
				}
				else if (control.GetType() == typeof(LinkButton))
				{
					LiteralControl literal = new LiteralControl();
					control.Parent.Controls.Add(literal);					
					literal.Text = (control as LinkButton).CommandArgument.ToString();
					
					control.Parent.Controls.Remove(control);
				}

				else
					if (control.GetType().GetProperty("Text") != null)
				{
					LiteralControl literal = new LiteralControl();
					control.Parent.Controls.Add(literal);
					literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control,null);
					control.Parent.Controls.Remove(control);
				}
			}
			return;
		}

		protected void btnJump_Click(object sender, EventArgs e)
		{
			//Jump page
			CurrentPage = int.Parse(this.txtPageCurrent.Text.ToString())-1;
			LoadData(CurrentPage);
		}
		protected void btnJumpTop_Click(object sender, System.EventArgs e)
		{
			CurrentPage = int.Parse(this.txtPageNoTop.Text.ToString())-1;
			LoadData(CurrentPage);
		}
		protected void Page_Click(object sender, CommandEventArgs e)
		{
			//Click page
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
		   Response.Redirect("./OrderSearch.aspx");
		}

		private void btnFilter_Click(object sender, System.EventArgs e)
		{
			//Quick Search data
			string strReg = ddlRegion.SelectedValue;
			string strDis = ddlDistrict.SelectedValue;
			string strStr = ddlStore.SelectedValue;
			if (strStr != "0")
			{
				strStr = strStr.ToString().PadLeft(4, '0');
			}
						
			string strOrdFDt = Request.QueryString["OrdFDt"].Trim();
			string strOrdTDt = Request.QueryString["OrdTDt"].Trim();
			string strShpFDt;
			string strShpTDt;

			if (Request.QueryString["ShpFDt"]!="" && Request.QueryString["ShpTDt"]!="")
			{
				strShpFDt = Request.QueryString["ShpFDt"].Trim();
				strShpTDt = Request.QueryString["ShpTDt"].Trim();
			}
			else
			{
				strShpFDt = "1/1/1980";
				strShpTDt = "1/1/1980";
			}
					
			string strOrderNo = "";
			string strFName = "";
			string strLName = "";
			string strEmail = "";

			int strStatus = int.Parse(Request.QueryString["Stat"].Trim());
			int intSource = int.Parse(ddlSource.SelectedValue);
			int intBrand = int.Parse(ddlBrand.SelectedValue);
			int intChannel = int.Parse(ddlChannel.SelectedValue);


			newOrderGroup = OrderGroup.GetOrderGroupFilter(strReg, strDis, strStr, 
				DateTime.Parse(strOrdFDt),DateTime.Parse(strOrdTDt),DateTime.Parse(strShpFDt),
				DateTime.Parse(strShpTDt),strOrderNo,strFName, strLName,strEmail, strStatus, intSource, intBrand, intChannel);

		
//			ViewState["OrderGroup"] = newOrderGroup;
//			LoadData(0);
			ViewState["OrderGroup"] = newOrderGroup;
			LoadData(CurrentPage);

		}

		private void lnkNextTop_Click(object sender, System.EventArgs e)
		{
			CurrentPage+=1;
			LoadData(CurrentPage);
		
		}

		private void lnkPreviousTop_Click(object sender, System.EventArgs e)
		{
			
			CurrentPage -= 1;
			LoadData(CurrentPage);
		}

		private void lnkPrev_Click(object sender, System.EventArgs e)
		{
			CurrentPage-=1;
			LoadData(CurrentPage);
		
		}

		private void lnkNext_Click(object sender, System.EventArgs e)
		{
			CurrentPage+=1;
			LoadData(CurrentPage);
		
		}

		//display Package total if more than one is shipped
		protected string PickPackage(string TrackNo, string total)
		{
			if (total!="")
			{
				if (int.Parse(total) >1)
				{
					return total + " pcs";			
				}
				else 
				{
					return TrackNo;

				}
			}
			else
			{
				return null;
			}
		}
		//convert weight
		protected string ConvertWeight(string wt)
		{
			if (wt!="")
			{
				double dwt = Double.Parse(wt);
				return (dwt/10000).ToString() + " lbs";

			}
			else
			{
				return wt;
			}
		}

	//filter out bad dates
		protected string ConvertDate(string dt)
		{
			if (DateTime.Parse(dt) > DateTime.Parse("1/1/1980"))
			{
				return dt.ToString();

			}
			else
			{
				return "";

			}
		}
	
	//filter out long notes
		protected string ConvertNotes(string nt)
		{
			if (nt.Length>10)
			{
				return nt.Substring(0, 10)+"...";

			}
			else
			{
				return nt;
			}
		}

        protected void btnMenu_Click(object sender, EventArgs e)
        {
            ViewState["OrderGroup"] = null; 
            Response.Redirect("./StoreActionOption.aspx");
        }

		protected string FormatPhone(string phone)
		{
			string dayphone = phone.Trim();
			if (dayphone.Length == 10)
				dayphone = string.Format("{0}-{1}-{2}", dayphone.Substring(0, 3), dayphone.Substring(3, 3), dayphone.Substring(6));
			else if (dayphone.Length == 11)
				dayphone = string.Format("{0}-{1}-{2}-{3}", dayphone.Substring(0,1), dayphone.Substring(1, 3), dayphone.Substring(4, 3), dayphone.Substring(7));

			return dayphone;
		}
	}
}
