using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for RecevInStore.
	/// </summary>
	public class RecevInStore : System.Web.UI.Page
	{
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.WebControls.TextBox txtCode1;
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected System.Web.UI.WebControls.TextBox txtCode2;
		protected System.Web.UI.WebControls.TextBox txtCode3;
		protected System.Web.UI.WebControls.TextBox txtCode4;
		protected System.Web.UI.WebControls.LinkButton lnkScanMore;
		protected System.Web.UI.WebControls.TextBox txtCode5;
		
		protected System.Web.UI.WebControls.Label lblErrDupInput;
		protected string strCode1;
		protected string strCode2;
		protected string strCode3;
		protected string strCode4;
		protected string strCode5;
		
		protected System.Web.UI.HtmlControls.HtmlGenericControl Panel1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl Panel2;

		protected ArrayList arrlist = new ArrayList();
		string strFullOrderNo =  string.Empty;
		string strCustName =  string.Empty;
		string strCustEmail =  string.Empty;
		string strDivision = string.Empty;
		int intIsEmail = 0;
		string strCustStreet = string.Empty;
		string strCustCity = string.Empty;
		string strCustState = string.Empty;
		string strCustZip = string.Empty;
		string strStrName = string.Empty;
		string strStrAddr = string.Empty;
		string strStrCity = string.Empty;
		string strStrState = string.Empty;
		string strStrZip = string.Empty;
        string strPhoneNo = string.Empty;
        string strStoreNo = string.Empty;
		protected OrderItemGroup newOrderItmGroup; 
		protected OrderItemGroup theOrderItmGroup; 
		string strHTEmailBody = string.Empty;
		string strItemDetail = string.Empty;
		string strSHEmailBody = string.Empty;
		string strHTEmailLogo = ConfigurationSettings.AppSettings["HTEmailLogo"];
		string strSHEmailLogo = ConfigurationSettings.AppSettings["SHEmailLogo"];
		string strHTEmlBotImage = ConfigurationSettings.AppSettings["HTEmlBotImage"];
		string strSHEmlBotImage = ConfigurationSettings.AppSettings["SHEmlBotImage"];
		double dblProdAmt = 0.00;
		double dblTax = 0.00;
		double dblTotPaid = 0.00;
        double dblShipping = 0.00;
		protected System.Web.UI.WebControls.Button btnReset;
		protected System.Web.UI.WebControls.Label lblErrorText;
		StringBuilder strHTEmailBodyBl = new StringBuilder();
		string strErrorMsg = string.Empty;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularexpressionvalidator1;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularexpressionvalidator2;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularexpressionvalidator3;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularexpressionvalidator4;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularexpressionvalidator5;

		protected System.Web.UI.WebControls.Button btnSearch;
		DataTable dtUser;
        DataTable dtCountFullOrdNo;

		private void Page_Load(object sender, System.EventArgs e)
		{
			lblErrorText.Text = "";
			lblErrorText.Visible = false;
			
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		
		
		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			try
			{
				int dbResult = 0;
				
				strCode1 =  txtCode1.Text.Trim().ToUpper(); 
				if (strCode1 != "")
				{
					string strCode1Vld = GetValidTrackNo(strCode1);

					arrlist.Add(strCode1Vld);

				}

				strCode2 =  txtCode2.Text.Trim().ToUpper(); 
				if (strCode2 != "")
				{
					string strCode2Vld = GetValidTrackNo(strCode2);

					arrlist.Add(strCode2Vld);
				}

				strCode3 =  txtCode3.Text.Trim().ToUpper(); 
				if (strCode3 != "")
				{
					string strCode3Vld = GetValidTrackNo(strCode3);

					arrlist.Add(strCode3Vld);
				}

				strCode4 =  txtCode4.Text.Trim().ToUpper(); 
				if (strCode4 != "")
				{
					string strCode4Vld = GetValidTrackNo(strCode4);

					arrlist.Add(strCode4Vld);
				}

				strCode5 =  txtCode5.Text.Trim().ToUpper();
				if (strCode5 != "")
				{
					string strCode5Vld = GetValidTrackNo(strCode5);

					arrlist.Add(strCode5Vld);
				}
                
				dtUser = (DataTable)Session["UserProfile"];
				string strEmployeeID = dtUser.Rows[0]["EmployeeID"].ToString().Trim();
				string strStoreNum = dtUser.Rows[0]["Location"].ToString().Trim();
                
				
				for( int i = 0; i < arrlist.Count; i++)
				{
					DataTable dtOrders = Data.GetOrdsbyTrackNo(arrlist[i].ToString());
					//if (dtOrders != null)
                    if (dtOrders.Rows.Count != 0)
           			{
						foreach (DataRow ordRow in dtOrders.Rows) 
						{
							string strFullOrderNo =  ordRow["FULLORDERNO"].ToString().Trim();
                            string strSource = ordRow["SOURCE"].ToString().Trim();
                            strSource = strSource.Substring(0,2);
							string strOrderTrackNo = strFullOrderNo +  arrlist[i].ToString();
                            string strStoreNo = Data.GetStoreNo(strFullOrderNo);
                            
                            if (strStoreNo == strStoreNum)
                            {
                                dbResult = InsertReceiveOrderTrack(arrlist[i].ToString(), strOrderTrackNo, strFullOrderNo);

                                if (dbResult == 1)
                                {
                                    if (intIsEmail == 1)
                                    {
                                        //if ((strCustEmail != "") && (strStrName!= "") && (strCustStreet!= ""))
                                        //{
                                        theOrderItmGroup = GetOrderItemsDetail(strFullOrderNo);

                                        strItemDetail = "<table border='1' cellpadding='0' cellspacing='0' class='inner'><tr><td width='20'>ShipQTY</td><td width='20'>CancelQTY</td><td width='400'>DESCRIPTION</td><td>AMOUNT</td></tr>";

                                        foreach (OneOrderItem oneItem in theOrderItmGroup)
                                        {
                                            //strHTItemDetail +=  oneItem.StrQtyShp + "       " + oneItem.StrQtyCancel  + "\t" + "\t"+ oneItem.StrItemDESC + "      " + oneItem.StrSprice + "\n";
                                            strItemDetail += "<tr><td width='20' align='center'>" + oneItem.StrQtyShp + "</td><td width='20' align='center'>" + oneItem.StrQtyCancel + "</td><td width='400'>" + oneItem.StrItemDESC + "</td><td align='right'>" + double.Parse(oneItem.StrSprice).ToString("0.00") + "</td></tr>";
                                            dblProdAmt = double.Parse(oneItem.StrTotSprice);
                                            dblTax = double.Parse(oneItem.StrTax);
                                            dblShipping = double.Parse(oneItem.StrSprice);
                                        }
                                        dblTotPaid = dblProdAmt + dblTax + double.Parse(ConfigurationSettings.AppSettings["ShipmentFee"]);
                                        strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> PRODUCT:</td><td align='right'>" + dblProdAmt.ToString("0.00") + "</td></tr>"; //dblProdAmt.ToString("0.00") + "\n";
                                        strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> TAX:</td><td align='right'>" + dblTax.ToString("0.00") + "</td></tr>";
                                        if (strSource == "PS" || strSource == "EC")
                                        {
                                            //POS Web order, use actual shipping rate
                                            dblShipping = double.Parse(Data.GetShippingCost(strFullOrderNo));
                                            strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> SHIPPING AND HANDLING:</td><td align='right'>" + dblShipping.ToString("0.00") + "</td></tr>";
                                            dblTotPaid = dblProdAmt + dblTax + dblShipping;
                                        }
                                        else
                                        {
                                            //Kiosk/Rockwall order, use flat shipping rate
                                            strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> SHIPPING AND HANDLING:</td><td align='right'>" + ConfigurationSettings.AppSettings["ShipmentFee"] + "</td></tr>";
                                        }
                                        strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> *TOTAL PAID:</td><td align='right'>" + dblTotPaid.ToString("0.00") + "</td></tr></table>";
                                        //Admin Mail Option
                                        string adminMail = ConfigurationSettings.AppSettings["EmailRec"].ToString();
                                        if (adminMail!="")
                                        {
                                        strCustEmail = adminMail;  //use for test not to send email to customer
                                        }
                                        // see if it is split order
                                        dtCountFullOrdNo = Data.GetOrdPikUpStatus(strFullOrderNo.Substring(0, 8));
                                        int nCountFullOrdNo = 0;

                                        nCountFullOrdNo = Utility.CountFullOrdNos(dtCountFullOrdNo);
                                        //string strBrand= (strDivision=="1"?"HotTopic":"Torrid");
										string EmailSubject= string.Empty;

                                        if (strDivision !="")
                                        {
                                
                                            if ((strCustEmail != "") && (strStrName != "") && (strCustStreet != ""))
                                            {
                                                if (nCountFullOrdNo == 1) //send complete email
                                                {
													EmailSubject = ConfigurationSettings.AppSettings["ReceiveSubject"];
                                                    Emails.SendText(strCustEmail, EmailSubject, GetHTEmailBody(strFullOrderNo));

                                                }
                                                else if (nCountFullOrdNo > 1)//send patial order received email
                                                {
													EmailSubject = ConfigurationSettings.AppSettings["PartialReceiveSubject"];
                                                    Emails.SendText(strCustEmail, EmailSubject, GetHTPartialEmailBody(strFullOrderNo));
                                                }
                                            }
                                            else //add enhancement here, send notification to store to contact customer by phone. 
                                            {
                                                strErrorMsg += "Could not get enough customer and store info from Database to send notification email out. Please contact customer by Phone. Detail order info sent to store email inbox." + "\n";
                                                ShowError(strErrorMsg);
                                                strCustEmail = "s" + strStoreNo + "@hottopic.com";
                                                if (nCountFullOrdNo == 1) //send complete email
                                                {
                                                    //For Production
                                                    Emails.SendText(strCustEmail, "Call customer by phone# " + strPhoneNo + " for HT Order ready for Pick Up ", GetHTEmailBody(strFullOrderNo));

                                                }
                                                else if (nCountFullOrdNo > 1)
                                                {   //send patial order received email
                                                    Emails.SendText(strCustEmail, "Call customer by phone# " + strPhoneNo + " for HT part of Order ready for Pick Up ", GetHTPartialEmailBody(strFullOrderNo));

                                                }
                                            }
                                        }
                             
                                        else
                                        {
                                           //For Production
											EmailSubject = ConfigurationSettings.AppSettings["ReceiveSubject"];
                                            Emails.SendText(strCustEmail, EmailSubject, GetSHEmailBody(strFullOrderNo));
                                        }
                                        strErrorMsg += "Order number " + strFullOrderNo + " successful received.\n";
                                        ShowError(strErrorMsg);
                                        //track store employee scan box time for received at store status. 
                                        Data.TrackEmployeeScan(arrlist[i].ToString(), strFullOrderNo, strEmployeeID, strStoreNum, "R");

                                    }
                                }
                                else if (dbResult == 2)
                                {
                                    strErrorMsg += "Tracking number " + arrlist[i].ToString() + " has already been received.\n";
                                    ShowError(strErrorMsg);
                                }
								else if (dbResult == 3)
								{
									strErrorMsg += "Order number " + arrlist[i].ToString() + " has already been received.\n";
								}
								else if (dbResult == 5)
								{
									strErrorMsg += "No package sent for Tracking number " + arrlist[i].ToString() + ".\n";
									ShowError(strErrorMsg);
								}
								else if (dbResult == 6)
								{
									strErrorMsg += "Tracking number " + arrlist[i].ToString() + " received. (Partial order)\n";
									ShowError(strErrorMsg);
								}
								else if (dbResult == 9)
								{
									strErrorMsg += "Order " + arrlist[i].ToString() 
										+ " has been abandoned and customer returns are being processed by Customer Service. Please remove item(s) from packaging and place onto your sales floor.\n";
									ShowError(strErrorMsg);
								}
								else
								{
									strErrorMsg += "There was an error while entering Tracking number " + arrlist[i].ToString() + "into the database.\n";
									ShowError(strErrorMsg);
								}
                            }
                            else {
                                strErrorMsg += "The Order number " + strFullOrderNo + " shipped to store " + strStoreNo + " by Tracking number " + arrlist[i].ToString() + " could not be received by employee who is belong to store of " + strStoreNum.Trim() + ".\n";
                                ShowError(strErrorMsg);
                            }
						} //end foreach loop
					}
					else 
					{
						strErrorMsg += "Order number does not exist for Tracking number " + arrlist[i].ToString() + ".\n";
						ShowError(strErrorMsg); 
					}
				
				}
				Reset();
            
			}
			catch(Exception ex)
			{
                throw (ex);
				//ShowError(ex.Message);
				//Response.Redirect("~/Error.aspx");
			}
		}
		
		private void ShowError(string strErrMsg)
		{
			if (strErrMsg.Trim()!="")
			{
				lblErrorText.Text = strErrMsg;
				lblErrorText.Visible= true;
			}
		}
		private string GetHTEmailBody(string strFullOrderNo)
		{
            string strPlate = ConfigurationSettings.AppSettings["ReceiveTemplate"].ToString();
            strHTEmailBody = System.IO.File.ReadAllText(strPlate);
            //title
            strHTEmailBody = strHTEmailBody.Replace("xCustNamex", strCustName);
            strHTEmailBody = strHTEmailBody.Replace("xOrderNumberx", strFullOrderNo);
            strHTEmailBody = strHTEmailBody.Replace("xStoreNamex", strStrName);
            //address
            strHTEmailBody = strHTEmailBody.Replace("xStoreStreetx", strStrAddr);
            strHTEmailBody = strHTEmailBody.Replace("xCustStreetx", strCustStreet);
            strHTEmailBody = strHTEmailBody.Replace("xStoreAddressx", strStrCity + ", " + strStrState + " " + strStrZip);
            strHTEmailBody = strHTEmailBody.Replace("xCustAddressx", strCustCity + ", " + strCustState + " " + strCustZip);
            //detail
            strHTEmailBody = strHTEmailBody.Replace("xOrderDetailx", strItemDetail + strHTEmailBodyBl);
			
			return strHTEmailBody;
		}
        private string GetHTPartialEmailBody(string strFullOrderNo)
        {
            string strPlate = ConfigurationSettings.AppSettings["PartialTemplate"].ToString();
            strHTEmailBody = System.IO.File.ReadAllText(strPlate);
            //title
            strHTEmailBody = strHTEmailBody.Replace("xCustNamex", strCustName);
            strHTEmailBody = strHTEmailBody.Replace("xOrderNumberx", strFullOrderNo);
            strHTEmailBody = strHTEmailBody.Replace("xStoreNamex", strStrName);
            //address
            strHTEmailBody = strHTEmailBody.Replace("xStoreStreetx", strStrAddr);
            strHTEmailBody = strHTEmailBody.Replace("xCustStreetx", strCustStreet);
            strHTEmailBody = strHTEmailBody.Replace("xStoreAddressx", strStrCity + ", " + strStrState + " " + strStrZip);
            strHTEmailBody = strHTEmailBody.Replace("xCustAddressx", strCustCity + ", " + strCustState + " " + strCustZip);
            //detail
            strHTEmailBody = strHTEmailBody.Replace("xOrderDetailx", strItemDetail + strHTEmailBodyBl);
         
            return strHTEmailBody;
        }

		private string GetSHEmailBody(string strFullOrderNo)
		{
            string strPlate = ConfigurationSettings.AppSettings["ReceiveTemplateTR"].ToString();
            strSHEmailBody = System.IO.File.ReadAllText(strPlate);
            //title
            strSHEmailBody = strSHEmailBody.Replace("xCustNamex", strCustName);
            strSHEmailBody = strSHEmailBody.Replace("xOrderNumberx", strFullOrderNo);
            strSHEmailBody = strSHEmailBody.Replace("xStoreNamex", strStrName);
            //address
            strSHEmailBody = strSHEmailBody.Replace("xStoreStreetx", strStrAddr);
            strSHEmailBody = strSHEmailBody.Replace("xCustStreetx", strCustStreet);
            strSHEmailBody = strSHEmailBody.Replace("xStoreAddressx", strStrCity + ", " + strStrState + " " + strStrZip);
            strSHEmailBody = strSHEmailBody.Replace("xCustAddressx", strCustCity + ", " + strCustState + " " + strCustZip);
            //detail
            strSHEmailBody = strSHEmailBody.Replace("xOrderDetailx", strItemDetail + strHTEmailBodyBl);
			return strSHEmailBody;
		}

		private OrderItemGroup GetOrderItemsDetail(string strFullOrderNo)
		{
			newOrderItmGroup = OrderItemGroup.GetOrderItemForEmail(strFullOrderNo);	
			return newOrderItmGroup;
		}

		private int InsertReceiveOrderTrack(string strTrackNo,string strOrderTrackNo, string strFullOrderNo)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();
			int intResultRec = 0;
            string strCompany = ConfigurationSettings.AppSettings["Company"];
			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				cm.CommandText = "usp_RecInstr_Insert_ByOrdTrack";
				cm.CommandTimeout=350;
				           
				cm.Parameters.Clear();
                cm.Parameters.Add(new SqlParameter("@Company", strCompany));
				SqlParameter qqq = new SqlParameter();
				qqq.ParameterName = "@TRACKNO";
				qqq.SqlDbType = SqlDbType.VarChar;
				qqq.Size=40;
				qqq.Value = strTrackNo;
				cm.Parameters.Add(qqq);

				SqlParameter sss = new SqlParameter();
				sss.ParameterName = "@Result";
				sss.SqlDbType = SqlDbType.Int;
				sss.Value = intResultRec;
				sss.Direction = ParameterDirection.Output;
				cm.Parameters.Add(sss);

				SqlParameter aaa = new SqlParameter();
				aaa.ParameterName = "@OrderTrackNo";
				aaa.SqlDbType = SqlDbType.VarChar;
				aaa.Size=52;
				aaa.Value = strOrderTrackNo;
				cm.Parameters.Add(aaa);

				SqlParameter hhh = new SqlParameter();
				hhh.ParameterName = "@FullOrderNo";
				hhh.SqlDbType = SqlDbType.VarChar;
				hhh.Size=12;
				hhh.Value = strFullOrderNo;
				cm.Parameters.Add(hhh);

				SqlParameter mmm = new SqlParameter();
				mmm.ParameterName = "@IsEmail";
				mmm.SqlDbType = SqlDbType.Int;
				mmm.Value = intIsEmail;
				mmm.Direction = ParameterDirection.Output;
				cm.Parameters.Add(mmm);

				SqlParameter jjj = new SqlParameter();
				jjj.ParameterName = "@CustEmail";
				jjj.SqlDbType = SqlDbType.VarChar;
				jjj.Size = 50;
				jjj.Value = strCustEmail;
				jjj.Direction = ParameterDirection.Output;
				cm.Parameters.Add(jjj);

				SqlParameter kkk = new SqlParameter();
				kkk.ParameterName = "@CustName";
				kkk.SqlDbType = SqlDbType.VarChar;
				kkk.Size = 32;
				kkk.Value = strCustName;
				kkk.Direction = ParameterDirection.Output;
				cm.Parameters.Add(kkk);

				SqlParameter xxx = new SqlParameter();
				xxx.ParameterName = "@Division";
				xxx.SqlDbType = SqlDbType.VarChar;
				xxx.Size = 4;
				xxx.Value = strDivision;
				xxx.Direction = ParameterDirection.Output;
				cm.Parameters.Add(xxx);

				SqlParameter rrr = new SqlParameter();
				rrr.ParameterName = "@Street";
				rrr.SqlDbType = SqlDbType.VarChar;
				rrr.Size = 30;
				rrr.Value = strCustStreet;
				rrr.Direction = ParameterDirection.Output;
				cm.Parameters.Add(rrr);

				SqlParameter ttt = new SqlParameter();
				ttt.ParameterName = "@City";
				ttt.SqlDbType = SqlDbType.VarChar;
				ttt.Size = 30;
				ttt.Value = strCustCity;
				ttt.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ttt);

				SqlParameter uuu = new SqlParameter();
				uuu.ParameterName = "@State";
				uuu.SqlDbType = SqlDbType.VarChar;
				uuu.Size = 2;
				uuu.Value = strCustState;
				uuu.Direction = ParameterDirection.Output;
				cm.Parameters.Add(uuu);

				SqlParameter vvv = new SqlParameter();
				vvv.ParameterName = "@Zip";
				vvv.SqlDbType = SqlDbType.VarChar;
				vvv.Size = 14;
				vvv.Value = strCustZip;
				vvv.Direction = ParameterDirection.Output;
				cm.Parameters.Add(vvv);

				SqlParameter www = new SqlParameter();
				www.ParameterName = "@StoreName";
				www.SqlDbType = SqlDbType.VarChar;
				www.Size = 50;
				www.Value = strStrName;
				www.Direction = ParameterDirection.Output;
				cm.Parameters.Add(www);

				SqlParameter yyy = new SqlParameter();
				yyy.ParameterName = "@StrAddress";
				yyy.SqlDbType = SqlDbType.VarChar;
				yyy.Size = 105;
				yyy.Value = strStrAddr;
				yyy.Direction = ParameterDirection.Output;
				cm.Parameters.Add(yyy);

				SqlParameter ppp = new SqlParameter();
				ppp.ParameterName = "@StrCity";
				ppp.SqlDbType = SqlDbType.VarChar;
				ppp.Size = 35;
				ppp.Value = strStrCity;
				ppp.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ppp);

				SqlParameter bbb = new SqlParameter();
				bbb.ParameterName = "@StrState";
				bbb.SqlDbType = SqlDbType.VarChar;
				bbb.Size = 2;
				bbb.Value = strStrState;
				bbb.Direction = ParameterDirection.Output;
				cm.Parameters.Add(bbb);

				SqlParameter ooo = new SqlParameter();
				ooo.ParameterName = "@StrZip";
				ooo.SqlDbType = SqlDbType.VarChar;
				ooo.Size = 35;
				ooo.Value = strStrZip;
				ooo.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ooo);

                SqlParameter zzz = new SqlParameter();
                zzz.ParameterName = "@StrPhoneNo";
                zzz.SqlDbType = SqlDbType.VarChar;
                zzz.Size = 16;
                zzz.Value = strPhoneNo;
                zzz.Direction = ParameterDirection.Output;
                cm.Parameters.Add(zzz);

                SqlParameter fff = new SqlParameter();
                fff.ParameterName = "@StoreNum";
                fff.SqlDbType = SqlDbType.VarChar;
                fff.Size = 4;
                fff.Value = strStoreNo;
                fff.Direction = ParameterDirection.Output;
                cm.Parameters.Add(fff);
		
				cm.ExecuteNonQuery();

				intResultRec = Convert.ToInt16(cm.Parameters["@Result"].Value);
				//strFullOrderNo = Convert.ToString(cm.Parameters["@FullOrderNo"].Value);
				intIsEmail = Convert.ToInt16(cm.Parameters["@IsEmail"].Value);
				strCustEmail = Convert.ToString(cm.Parameters["@CustEmail"].Value);
				strCustName = Convert.ToString(cm.Parameters["@CustName"].Value);
				strDivision = Convert.ToString(cm.Parameters["@Division"].Value);
				strCustStreet = Convert.ToString(cm.Parameters["@Street"].Value);
				strCustCity = Convert.ToString(cm.Parameters["@City"].Value);
				strCustState = Convert.ToString(cm.Parameters["@State"].Value);
				strCustZip = Convert.ToString(cm.Parameters["@Zip"].Value);
				strStrName = Convert.ToString(cm.Parameters["@StoreName"].Value);
				strStrAddr = Convert.ToString(cm.Parameters["@StrAddress"].Value);
				strStrCity = Convert.ToString(cm.Parameters["@StrCity"].Value);
				strStrState = Convert.ToString(cm.Parameters["@StrState"].Value);
				strStrZip = Convert.ToString(cm.Parameters["@StrZip"].Value);
                strPhoneNo = Convert.ToString(cm.Parameters["@StrPhoneNo"].Value);
                strStoreNo = Convert.ToString(cm.Parameters["@StoreNum"].Value); 

			}
			catch (SqlException ex)
			{
				intResultRec = 99;
			}
			
			finally
			{
				cn.Close();
			}
			return intResultRec;
		}

		private void lnkScanMore_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnReset_Click(object sender, System.EventArgs e)
		{
			txtCode1.Text = String.Empty;
			txtCode2.Text = String.Empty;
			txtCode3.Text = String.Empty;
			txtCode4.Text = String.Empty;
			txtCode5.Text = String.Empty;
			
			lblErrorText.Text = String.Empty;
			lblErrorText.Visible= false;

		}
		private void Reset()
		{
			txtCode1.Text = String.Empty;
			txtCode2.Text = String.Empty;
			txtCode3.Text = String.Empty;
			txtCode4.Text = String.Empty;
			txtCode5.Text = String.Empty;
			
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("./StoreActionOption.aspx");
		}

		private string GetValidTrackNo(string strInputTrack)
		{
			string strValidTrackNo =string.Empty; 
      
			if (strInputTrack.Length ==22) 
			{
				if (strInputTrack.Substring(0,7)=="9612804")
				{
					strValidTrackNo = strInputTrack.Substring(7, 15);
				}
				else if(strInputTrack.Substring(0,7)=="9612019")
				{
					strValidTrackNo = strInputTrack.Substring(7, 15);
				}
				else
				{
					strValidTrackNo = strInputTrack;
				}
			}
			//this is for USPS - comments out on 0716_2010
			else if (strInputTrack.Length ==30)
			{
				if (strInputTrack.Substring(0,3)=="420") //usps first class and piority start with 420 
				{
					strValidTrackNo = strInputTrack.Substring(8, 22);
				}
				else
				{
					strValidTrackNo = strInputTrack;
				}
			}
				//else if (strInputTrack.Length > 0)
          	else if (strInputTrack.Length > 0)
			{
                long lnIsOrderNumer = CheckIsOrderNumber(strInputTrack);
                if (lnIsOrderNumer == 0)
                {
                    strValidTrackNo = strInputTrack.Substring(0, 12);
                }
                else
                {
                    strValidTrackNo = strInputTrack;
                }
			}

			return strValidTrackNo;
		}

        //return 0 if not a integer, to see if scan by order number for puerto rico store
        protected long CheckIsOrderNumber(string sNumber)
        {
            try
            {
               return long.Parse(sNumber);
            }
            catch
            {
                return 0;
            }
        }
   
	}
}
