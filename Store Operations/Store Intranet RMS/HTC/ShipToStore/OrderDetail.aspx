<%@ Page language="c#" Codebehind="OrderDetail.aspx.cs" AutoEventWireup="false" Inherits="ShipToStore.OrderDetail" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OrderSearch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
		<script language="javascript" type="text/javascript">

            function calendarPicker(strField)
            {

                        window.open('Calendar.aspx?field=' + strField, 'calendarPopup', 'width=250,height=200,resizable=yes');

            }
            
            function PickUpOrder()
            {
              //alert('strOrderNumber');
              //var ordernum1 = 'P123456';
              //var ordernum = ordernum1;
              //var fullOrdernum = "<%=strFullOrderNum%>";
              var fullOrdernum = "<%=strFullOrderNum%>";
              window.open('CustPickUp.aspx?strOrderNo=' + fullOrdernum, 'PickUpOrder', 'width=300,height=350,resizable=yes');
            }

           // function scriptPickUp()
            //{ 
               //if (confirm("Are you sure you want to change the order status to customer picked up?")) 
             //  {
             //  return true;
             //  } else {
             //  return false;
             //  }

            //return confirm('Are you sure you want to change the order status to customer picked up?');
            //}

           // function scriptEmlReminder()
           // {
           // return confirm('Are you sure you want to send Email Reminder to Customer?');
           // }
		</script>
	</HEAD>
	<body bottomMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header></div>
			<div id="main"><br>
				<TABLE class="innerTableDetail" id="tblOption" cellSpacing="0" cellPadding="0" width="98%"
					align="center" border="0" runat="server">
					<TBODY>
						<tr>
							<td class="innerTableDetail" colSpan="2">
								<TABLE cellSpacing="0" cellPadding="1" width="100%" align="center" border="0">
									<TBODY>
										<tr>
											<td colSpan="2">
												<table style="WIDTH: 584px; HEIGHT: 166px" width="60%">
													<TR>
														<TD class="tableTitle" colSpan="2">Order Details</TD>
													</TR>
													<TR>
														<td>
															<table style="WIDTH: 576px; HEIGHT: 148px" border="0">
																<TR>
																	<TD style="HEIGHT: 21px" width="126">Order Date:</TD>
																	<TD style="HEIGHT: 21px">&nbsp;
																		<asp:label id="lblOrderDate" runat="server"></asp:label></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 21px" width="126">Ship Date:</TD>
																	<TD style="HEIGHT: 21px">&nbsp;
																		<asp:label id="lblShipDate" runat="server"></asp:label></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 21px" width="126">Shipping Type:</TD>
																	<TD style="HEIGHT: 21px">&nbsp;
																		<asp:label id="lblShipType" runat="server"></asp:label></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 21px" width="126"># of Boxes:</TD>
																	<TD style="HEIGHT: 21px">&nbsp;
																		<asp:label id="lblBoxNumber" runat="server"></asp:label></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 21px" width="126">Ship Source:</TD>
																	<TD style="HEIGHT: 21px">&nbsp;
																		<asp:label id="lblSource" runat="server"></asp:label></TD>
																</TR>
																<tr>
																	<td colSpan="2"></td>
																</tr>
															</table>
														</td>
													</TR>
													<tr>
														<TD class="fontSmallBoldRedColor" align="left" colSpan="2">For questions regarding 
															this order, contact Customer Service at 1.866.468.5577
														</TD>
													</tr>
												</table>
											</td>
											<td> 
												<table cellSpacing="0" cellPadding="0" width="100" border="0">
												   
													<TR>
														<TD style="height: 196px">
															<TABLE style="WIDTH: 219px; HEIGHT: 140px" cellSpacing="0" cellPadding="0" width="219"
																align="center" border="0">
																
																<tr>
																	<td class="fontMediumBold" align="left">Please Select a Track No, then click Track 
																		Order</td>
																</tr>
																<tr>
																	<td><asp:listbox id="lstTrackView" runat="server" Width="300" DataKeyField="StrTrackNo" RepeatDirection="Horizontal"
																			RepeatLayout="Flow" OnItemCommand="dlTrackView_TrackCommand" EnableViewState="True" SelectionMode="Single"></asp:listbox></td>
																</tr>
																<tr>
																	<td align="right"><asp:button id="btnTrackOrder" runat="server" Text="TRACK ORDER" CssClass="buttonYellow"></asp:button></td>
																</tr>
															</TABLE>
														</TD>
													</TR>
												</table>
                                                </td>
										</tr>
									</TBODY>
								</TABLE>
							</td>
						</tr>
						<tr>
							<td class="innerTableDetail" vAlign="top">
								<TABLE cellSpacing="0" cellPadding="1" width="100%" align="center" border="0">
									<TR>
										<TD class="tableTitle" style="HEIGHT: 27px" colSpan="2">Customer&nbsp;Information</TD>
									</TR>
									<TR>
										<TD width="100">First Name:</TD>
										<TD>&nbsp;
											<asp:label id="lblFirstName" runat="server"></asp:label></TD>
									</TR>
									<TR>
										<TD width="100">Last Name:</TD>
										<TD>&nbsp;
											<asp:label id="lblLastName" runat="server"></asp:label></TD>
									</TR>
									<TR>
										<TD width="100">Email:</TD>
										<TD>&nbsp;
											<asp:label id="lblEmail" runat="server"></asp:label></TD>
									</TR>
									<TR>
										<TD width="100">Home Phone:</TD>
										<TD>&nbsp;
											<asp:label id="lblHPhone" runat="server"></asp:label></TD>
									</TR>
									<TR>
										<TD width="100">Mobile Phone:</TD>
										<TD>&nbsp;
											<asp:label id="lblMPhone" runat="server"></asp:label></TD>
									</TR>
								</TABLE>
							</td>
							<TD class="innerTableDetail" vAlign="top" width="60%">
								<TABLE cellSpacing="0" cellPadding="1" width="100%" align="center" border="0">
									<TBODY>
										<TR>
											<TD class="tableTitle" style="HEIGHT: 26px" colSpan="2">Order&nbsp;Information</TD>
										</TR>
										<tr>
											<td>
												<table style="WIDTH: 512px; HEIGHT: 68px">
													<tr>
														<TD width="127">PaymentType:</TD>
														<TD style="width: 377px">&nbsp;
															<asp:label id="lblPayType" runat="server"></asp:label></TD>
													</tr>
													<TR>
														<TD width="127">Order Total:</TD>
														<TD style="width: 377px">&nbsp;
															<asp:label id="lblTotal" runat="server"></asp:label></TD>
													</TR>
													<TR>
														<TD width="127">Order Items</TD>
														<TD style="width: 377px">&nbsp;</TD>
													</TR>
												</table>
											</td>
										</tr>
										<TR>
											<TD colSpan="2"><iframe id=frmItem name=OrderItem 
            src="OrderItem.aspx?StrOrderNo=<%=strOrderNumber%>" frameBorder=0 width="100%"
            ></iframe><!--width=500 style="width: 538px"-->
											</TD>
										</TR>
										<TR>
											<TD class="fontSmallBoldRedColor" colSpan="2">If product received at your store 
												does not match order information shown above, contact Customer Service at 
												1.866.468.5577&nbsp;</TD>
										</TR>
									</TBODY>
								</TABLE>
							</TD>
						</tr>
						<TR>
							<TD class="innerTableDetail" colSpan="2">
								<TABLE cellSpacing="0" cellPadding="1" width="100%" align="center" border="0">
									<TBODY>
										<TR>
											<TD class="tableTitle" colSpan="3">Store Information</TD>
										</TR>
										<TR>
											<TD style="width: 386px; height: 159px;"><table><tr><td style="width: 382px">			
											<asp:GridView ID="gvOrderPikUpView" runat="server" ShowHeader="true" ShowFooter="false" AutoGenerateColumns ="false"  width="368px" Height="141px" style="text-align: center;"  HeaderStyle-BackColor = "#DCDCDC" FooterStyle-BackColor="#DCDCDC"
         Font-Names = "Arial"  Font-Size = "8pt"  onrowupdating = "UpdateOrdPikUp"  OnRowDataBound="gvOrderPikUpView_RowDataBound" EnableViewState=true >
             <Columns>
                <asp:TemplateField HeaderText="Select" FooterText="Select">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server" 
                            AutoPostBack="true" 
                            enabled ='<%# Convert.ToBoolean(Eval("FLAG")) %>'
                            Text='<%# Eval("FLAG").ToString().Equals("True") ? "" : "" %>' />
                    </ItemTemplate>                    
                </asp:TemplateField>
            
                 <asp:BoundField DataField="TRACKNO" HeaderText="TRACKNO" FooterText="TRACKNO" ReadOnly="True">
                    <ItemStyle Width="30px" />
                </asp:BoundField>
                <asp:BoundField DataField="FULLORDERNO" HeaderText="FULLORDERNO"  FooterText="FULLORDERNO" ReadOnly="True">
                    <ItemStyle Width="30px" />
                </asp:BoundField>
                <asp:BoundField DataField="DATERECINSTORE" HeaderText="RECEIVED ON" FooterText="RECEIVED ON" ReadOnly ="True">
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="DATECUSTPICKUP" HeaderText="PICKUP DATE" FooterText="PU DATE" ReadOnly="True">
                    <ItemStyle Width="100px" />
                </asp:BoundField>
             </Columns>   
            <HeaderStyle CssClass="headerGD" ForeColor="Black" BackColor="LightGray" />
            <FooterStyle ForeColor="Black" BackColor="LightGray" />
         </asp:GridView> </td></tr></table>	
         
</TD><TD style="width: 177px; height: 159px;">&nbsp;
												</TD>
											<TD vAlign="top" align="right" rowSpan="3">Store Notes:<BR>
												<asp:textbox id="txtNotes" runat="server" CssClass="tableInput" width="100%" TextMode="MultiLine"
													Rows="6" Height="171px"></asp:textbox><!--Width="300px"-->
												<asp:button id="btnSaveNotes" runat="server" Text="SAVE NOTES" CssClass="buttonYellow"></asp:button></TD>
										</TR>
										
										<TR>
											<TD colSpan="2" style="width: 496px; height: 88px;">
												<table border="0">
													<tr>
														<td style="WIDTH: 227px">
															<table border="0">
																<tr>
																	<td> <asp:ImageButton id="btnWholUpdate" OnClick="btnWPickUp_Click" runat="server" AlternateText="CUSTOMER PICK UP" ImageAlign="left" ImageUrl="Image/CustPickUp.jpg" />




																	</td>
																</tr>
															</table>
														</td>
														<td>
															<table border="0">
																<tr>
																	<td><asp:button id="btnEmailReminder"  OnClick="btnEmailReminderCust_Click" runat="server" Text="EMAIL REMINDER" CssClass="buttonYellow"  ></asp:button>&nbsp;
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td align="center">
															<asp:button id="btnAbandon" onclick="btnAbandon_Click" runat="server" text="ABANDON ORDER" width="90%" cssclass="buttonYellow" onclientclick="return confirm('Are you sure you want to abandon this order?');" />
														</td>
														<td></td>
													</tr>
												</table>
											</TD>
											<TD align="right" valign="middle" style="height: 88px">
                                                &nbsp;</TD>
										
										</TR>
										<tr class="PageError">
											<td colSpan="2" style="width: 496px"><font color="#cc0000"><asp:label id="lblErrorText" runat="server" Visible="false"></asp:label></font></td>
										</tr>
									</TBODY>
								</TABLE>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
				<br>
				<table height="140" cellSpacing="0" cellPadding="1" width="98%" align="center" border="0">
					<tr>
						<td vAlign="top" align="right"><asp:button id="btnReturnToSearch" runat="server" Text="SEARCH MENU" CssClass="buttonRed"></asp:button></td>
					</tr>
				</table>
			</div>

			<DIV></DIV>
			<DIV></DIV>
		</form>
	</body>
</HTML>
