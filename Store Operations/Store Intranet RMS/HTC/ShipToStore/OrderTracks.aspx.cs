using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for OrderTracks.
	/// </summary>
	public class OrderTracks : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.ListBox lstTrackView;
        protected DataTable newDtTrackNos;
		protected string strOrderNumber;
		protected System.Web.UI.WebControls.Button btnTrackOrder;


	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

			//LBGame.Items.Add(new ListItem(datareader.Item(1), datareader.Item(0)) 
			if (!IsPostBack)
			{
//				string strOrderNumber = Request.QueryString["StrOrderNo"];
				string strOrderNumber = "P0258813";
	        	newDtTrackNos = Data.GetTrackNos(strOrderNumber);
			    lstTrackView.DataSource = newDtTrackNos;
				lstTrackView.DataTextField = "TRACKNO";
				lstTrackView.DataValueField = "TRACKNO";
										
				lstTrackView.DataBind();
				
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.lstTrackView.SelectedIndexChanged += new System.EventHandler(this.lstTrackView_SelectedIndexChanged);
			this.btnTrackOrder.Click += new System.EventHandler(this.btnTrackOrder_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
//		private void GetTrackNos(string strOrderNo)
//		{
//			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
//			SqlCommand cm = new SqlCommand();
//
//
//			try
//			{
//				cn.Open();
//				cm.Connection = cn;
//				cm.CommandType = CommandType.StoredProcedure;
//				cm.CommandText = "usp_TrackNos_ByOrderNo";
//				
//				SqlDataReader dr;              
//				cm.Parameters.Clear();
//
//				SqlParameter nnn = new SqlParameter();
//				nnn.ParameterName = "@OrderNo";
//				nnn.SqlDbType = SqlDbType.VarChar;
//				nnn.Value = strOrderNo;
//				cm.Parameters.Add(nnn);
//
//				dr = cm.ExecuteReader();
//				
//				try
//				{
//					while (dr.Read())
//					{
//						//this.PopulateProperties(dr);
//						if (dr["TRACKNO"].ToString() != string.Empty)
//						lstTrackView.Items.Add(dr["TRACKNO"].ToString());
//						{
//							newListTrackNos.Add(dr["TRACKNO"].ToString());
//						}
//
//					}
//				}
//				finally
//				{
//					dr.Close();
//				}
//
//			}
//			catch(Exception ex)
//			{
//				//int s=0;
//
//			}
//			finally
//			{
//				cn.Close();
//			}
//		
//		}

		private void lstTrackView_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void btnTrackOrder_Click(object sender, System.EventArgs e)
		{
			if (lstTrackView.SelectedValue.Length==22)
			{
				Response.Redirect("http://www.usps.com/shipping/trackandconfirm.htm");
			}
			else 
			{
				Response.Redirect("http://www.fedex.com/Tracking?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers=" + lstTrackView.SelectedValue);
			}
		}
	}
}
