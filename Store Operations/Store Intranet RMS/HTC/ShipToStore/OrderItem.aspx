<%@ Page language="c#" Codebehind="OrderItem.aspx.cs" AutoEventWireup="false" Inherits="ShipToStore.OrderItem" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OrderItem</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:datalist id="dlItemView" runat="server" OnItemCommand="dlItemView_ItemCommand" Width="100%"
				EnableViewState="True" RepeatLayout="Flow" RepeatDirection="Horizontal" DataKeyField="StrItemNo">
				<HeaderTemplate>
					<table width="100%" cellspacing="0" cellpadding="2" class="innerTableView" align="left">
						<tr class="tableTitle">
							<td colspan="8" class="tableTitle">Order Items</td>
						</tr>
						<tr class="tableHeader">
							<td align="left" width="50">
								SKU
							</td>
							<td align="left" width="80">
							    Full Order No
							</td>
							<td align="left" width="50">
								Description
							</td>
							<td align="left" width="50">
								Qty Ord
							</td>
							<td align="left" width="50">
								Qty Shp
							</td>
							<td align="left" width="50">
								Qty Cancel
							</td>
							<td align="left" width="50">
								Pri Shp 
							</td>
							<td align="left" width="50">
								Pri Cancel
							</td>
						</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr height="25" id="total" runat="server">
						<td align="left">
							<%# DataBinder.Eval(Container.DataItem,"StrItemNo") %>
						</td>
						<td align="left">
							<%# DataBinder.Eval(Container.DataItem, "StrFullOrderNo")%>
						</td>
						<td align="right">
							<%# DataBinder.Eval(Container.DataItem,"StrItemDESC") %>
						</td>
						<td align="right">
							<%# DataBinder.Eval(Container.DataItem,"StrQtyOrd") %>
						</td>
						<td align="right">
							<%# DataBinder.Eval(Container.DataItem, "StrQtyShp")%>
						</td>
						<td align="right">
							<%# DataBinder.Eval(Container.DataItem, "StrQtyCancel")%>
						</td>
						
						<td align="right">
							<%# DataBinder.Eval(Container.DataItem, "StrSprice")%>
						</td>
						<td align="right">
							<%# DataBinder.Eval(Container.DataItem, "StrCprice")%>
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					</table>
				</FooterTemplate>
			</asp:datalist></form>
	</body>
</HTML>
