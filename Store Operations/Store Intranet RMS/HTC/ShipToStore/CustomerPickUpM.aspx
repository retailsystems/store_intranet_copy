<%@ Page language="c#" Codebehind="CustomerPickUpM.aspx.cs" AutoEventWireup="false" Inherits="ShipToStore.CustomerPickUpM" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomerPickUp</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
		<script language="javascript" type="text/javascript">

            function calendarPicker(strField)
            {

                        window.open('Calendar.aspx?field=' + strField, 'calendarPopup', 'width=250,height=200,resizable=yes');

            }

		</script>
	</HEAD>
	<body onload="javascript:document.Form1.txtCode1.focus();" bottomMargin="0"
		topMargin="0" MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header></div>
			<div class="height: 540px;" id="main"><br>
				<TABLE class="innerTableLog" id="tblLogin" height="450" cellSpacing="0" cellPadding="1"
					width="96%" align="center" border="0">
					<TBODY>
						<tr>
							<td>
								<table cellSpacing="0" cellPadding="1" width="65%" align="center" border="0">
									<TBODY>
										<tr class="tableCell">
											<td colSpan="2">To complete the customer pick up please enter digits located on the 
												tracking bar code.</td>
										</tr>
										<tr>
											<td colSpan="2">
												<div id="Panel1" style="DISPLAY: block" runat="server">
													<table width="100%" align="left">
														<TBODY>
															<TR class="tableCell">
																<TD class="tableTitle" colSpan="2"><asp:textbox id="txtCode1" runat="server" CssClass="tableInputWhite" Columns="35"></asp:textbox>
																<td><asp:RegularExpressionValidator id="Regularexpressionvalidator1" runat="server" ErrorMessage="TrackNo not in valid format."
																		ValidationExpression="^\d[0-9]+$" Display="Dynamic" ControlToValidate="txtCode1"></asp:RegularExpressionValidator></td>
															</TR>
															<TR class="tableCell">
																<TD class="tableTitle" colSpan="2"><asp:textbox id="txtCode2" runat="server" CssClass="tableInputWhite" Columns="35"></asp:textbox>
																<td><asp:RegularExpressionValidator id="Regularexpressionvalidator2" runat="server" ErrorMessage="TrackNo not in valid format."
																		ValidationExpression="^\d[0-9]+$" Display="Dynamic" ControlToValidate="txtCode2"></asp:RegularExpressionValidator></td>
															</TR>
															<TR class="tableCell">
																<TD class="tableTitle" colSpan="2"><asp:textbox id="txtCode3" runat="server" CssClass="tableInputWhite" Columns="35"></asp:textbox>
																<td><asp:RegularExpressionValidator id="Regularexpressionvalidator3" runat="server" ErrorMessage="TrackNo not in valid format."
																		ValidationExpression="^\d[0-9]+$" Display="Dynamic" ControlToValidate="txtCode3"></asp:RegularExpressionValidator></td>
															</TR>
															<TR class="tableCell">
																<TD class="tableTitle" colSpan="2"><asp:textbox id="txtCode4" runat="server" CssClass="tableInputWhite" Columns="35"></asp:textbox>
																<td><asp:RegularExpressionValidator id="Regularexpressionvalidator4" runat="server" ErrorMessage="TrackNo not in valid format."
																		ValidationExpression="^\d[0-9]+$" Display="Dynamic" ControlToValidate="txtCode4"></asp:RegularExpressionValidator></td>
															</TR>
															<TR class="tableCell">
																<TD class="tableTitle" colSpan="2"><asp:textbox id="txtCode5" runat="server" CssClass="tableInputWhite" Columns="35"></asp:textbox>
																<td><asp:RegularExpressionValidator id="Regularexpressionvalidator5" runat="server" ErrorMessage="TrackNo not in valid format."
																		ValidationExpression="^\d[0-9]+$" Display="Dynamic" ControlToValidate="txtCode5"></asp:RegularExpressionValidator></td>
															</TR>
														</TBODY>
													</table>
												</div>
											</td>
										</tr>
										<tr>
											<td colSpan="2">
												
											</td>
										</tr>
										<TR>
											<TD></TD>
										</TR>
										<TR class="tableCell">
											<td>
											</td>
											<TD class="tableTitle" align="right"><asp:button id="btnSubmit" runat="server" CssClass="buttonRed" Text="SUBMIT"></asp:button></TD>
											<TD class="tableTitle" align="right"><asp:button id="btnReset" runat="server" CssClass="buttonRed" Text="RESET"></asp:button></TD>
										</TR>
										<br>
										<tr class="PageError">
											<td colspan="2"><font color="#cc0000"><asp:Label ID="lblErrorText" runat="server" Visible="false"></asp:Label></font></td>
										</tr>
									</TBODY>
								</table>
							</td>
						</tr>
					</TBODY>
				</TABLE>
				<br>
				<table height="90" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0">
					<tr>
						<td vAlign="top" align="right"><asp:button id="btnSearch" runat="server" CssClass="buttonRed" Text="RETURN TO MENU"></asp:button></td>
					</tr>
				</table>
				<DIV></DIV>
				</TD></TR></TBODY></TABLE>
				<DIV></DIV>
		</form>
		</DIV>
	</body>
</HTML>
