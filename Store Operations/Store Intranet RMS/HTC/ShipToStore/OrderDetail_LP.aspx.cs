using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Web.Security;
using System.Text;
using System.Data.SqlClient;



namespace ShipToStore
{
	/// <summary>
	/// Summary description for OrderDetail.
	/// </summary>
	public class OrderDetail_LP : System.Web.UI.Page
	{
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.WebControls.Button btnReturnToSearch;
		protected System.Web.UI.WebControls.Button btnCustomerPickUp;
		protected System.Web.UI.WebControls.Button btnEmailReminder;
		protected System.Web.UI.WebControls.Button btnSaveNotes;
		protected System.Web.UI.WebControls.TextBox txtNotes;
		protected System.Web.UI.WebControls.Label lblOrderDate;
		protected System.Web.UI.WebControls.Label lblShipDate;
		protected System.Web.UI.WebControls.Label lblShipType;
		protected System.Web.UI.WebControls.Label lblBoxNumber;
		protected System.Web.UI.WebControls.Label lblSource;
		protected System.Web.UI.WebControls.Label lblDateReceived;
		protected System.Web.UI.WebControls.Label lblPickUp;
		protected System.Web.UI.WebControls.Label lblStatus;
		protected System.Web.UI.WebControls.Label lblPayType;
		protected System.Web.UI.WebControls.Label lblTotal;
		protected System.Web.UI.WebControls.Label lblFirstName;
		protected System.Web.UI.WebControls.Label lblLastName;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.Label lblHPhone;
		protected System.Web.UI.WebControls.Label lblMPhone;
	    
		//Added new columns
        protected System.Web.UI.WebControls.Label lblEmpNo;
        //protected System.Web.UI.WebControls.Label lblGiftCard;
		//protected System.Web.UI.WebControls.Label lblEmpDisc;
        protected System.Web.UI.WebControls.ListBox lstTrackView;

		protected System.Web.UI.HtmlControls.HtmlTable tblOption;
		protected LP_Order theOneOrder;
		protected string strOrderNumber = string.Empty;
		protected string strFullOrderNo = string.Empty;
		protected string strStatus = string.Empty;
		protected OrderItemGroup theOrdItmEmailGroup;
		protected OrderItemGroup newOrdItmEmailGroup; 
        //string strNotes = string.Empty;
		string strCustName =  string.Empty;
		string strCustEmail =  string.Empty;
		string strDivision = string.Empty;
        string strEmpNo = string.Empty;
    
		//int intIsEmail = 0;
		string strCustStreet = string.Empty;
		string strCustCity = string.Empty;
		string strCustState = string.Empty;
		string strCustZip = string.Empty;
		string strStrName = string.Empty;
		string strStrAddr = string.Empty;
		string strStrCity = string.Empty;
		string strStrState = string.Empty;
		string strStrZip = string.Empty;
		string strHTEmailBody = string.Empty;
		string strItemDetail = string.Empty;
		string strSHEmailBody = string.Empty;
		string strHTEmailLogo = ConfigurationSettings.AppSettings["HTEmailLogo"];
		string strSHEmailLogo = ConfigurationSettings.AppSettings["SHEmailLogo"];
		string strHTEmlBotImage = ConfigurationSettings.AppSettings["HTEmlBotImage"];
		string strSHEmlBotImage = ConfigurationSettings.AppSettings["SHEmlBotImage"];
		double dblProdAmt = 0.00;
		double dblTax = 0.00;
		double dblTotPaid = 0.00;
		protected System.Web.UI.WebControls.Label lblErrorText;
		StringBuilder strHTEmailBodyBl = new StringBuilder();
		string strErrorMsg = string.Empty;
		int intRemResult = 0;
		protected string strFullOrderNum = string.Empty;
		//protected System.Web.UI.WebControls.ListBox lstGiftCard;
		protected System.Web.UI.WebControls.Button btnTrackOrder;
		protected DataTable newDtTrackNos;
        protected DataTable dtGiftCardNos;
		protected System.Web.UI.HtmlControls.HtmlInputImage btnPickUp;
		//protected System.Web.UI.HtmlControls.HtmlInputImage btnPickUp;
		protected string strNewStatus;
        protected System.Web.UI.WebControls.DataList dlGCNumView;

		private void Page_Load(object sender, System.EventArgs e)
		{
			//string strOrderNo = Request.QueryString["strOrderNo"];
			if (!IsPostBack)
			{
				//string strOrderNo = Request.QueryString["OrdNo"];
				strOrderNumber= Request.QueryString["StrOrderNo"];

				theOneOrder = LP_Order.GetTheOneOrder(strOrderNumber);

				lblOrderDate.Text = FilterDate(theOneOrder.DtOrderDate);
				lblShipDate.Text= FilterDate(theOneOrder.DtShippedDate);
				lblShipType.Text = theOneOrder.StrShipMethod.ToString();;
				lblBoxNumber.Text = theOneOrder.StrPackageNo.ToString();
				lblSource.Text = theOneOrder.StrShippedSource.ToString();
				lblPayType.Text = theOneOrder.StrPaymentType.ToString();
				lblTotal.Text = "$" + string.Format("{0:f}", Math.Round(theOneOrder.DblOrderTotal,2));
				lblFirstName.Text = theOneOrder.StrCustFirstName.ToString();
				lblLastName.Text = theOneOrder.StrCustLastName.ToString();
				lblEmail.Text = theOneOrder.StrEmail.ToString();
				lblHPhone.Text = theOneOrder.StrCustDayPhone.ToString();
				lblDateReceived.Text= FilterDate(theOneOrder.DtReceivedDate);
				lblPickUp.Text = FilterDate(theOneOrder.DtCustPickUpDate);
				
				//Jay T.- populated the labels with data. 
                //string strEmpNo = int.Parse(theOneOrder.StrEmployeeNo.ToString()).ToString();
                //lblEmpNo.Text = strEmpNo; 
                strEmpNo = theOneOrder.StrEmployeeNo.ToString();
                //if ((strEmpNo != string.Empty) || (strEmpNo != ""))
                //{
                //    //lblEmpNo.Text = int.Parse(strEmpNo).ToString();
                    lblEmpNo.Text = strEmpNo;
                //}
                //else 
                //{
                //    lblEmpNo.Text = "";
                //}
               
				//lblGiftCard.Text = theOneOrder.StrGiftCardNo;
				//lblEmpDisc.Text = theOneOrder.StrEmployeeDiscount;

				lblBoxNumber.Text = theOneOrder.StrPackageNo.ToString();
                //txtNotes.Text  = theOneOrder.StrStoreNotes.ToString();
                //strNotes = theOneOrder.StrStoreNotes.ToString();
				ViewState["FullOrderNumber"] = theOneOrder.StrRecFullOrdNo.ToString();
				strFullOrderNum = theOneOrder.StrRecFullOrdNo.ToString();
				ViewState["strFullOrderNum"] = strFullOrderNum;
				//ViewState["status"] =  theOneOrder.StrStatus.ToString(); 
				strCustName = theOneOrder.StrCustFirstName.ToString()+ " " + theOneOrder.StrCustLastName.ToString();
				strCustEmail = theOneOrder.StrEmail.ToString();
                
				ViewState["StrOrderNo"] = strOrderNumber;

				newDtTrackNos = Data.GetTrackNos(strOrderNumber);

				DisplayStatus(strOrderNumber);

				ViewState["status"] = this.lblStatus.Text;
				//lblStatus.Text = theOneOrder.StrStatus.ToString();
				lstTrackView.DataSource = newDtTrackNos;
				lstTrackView.DataTextField = "TRACKNO";
				lstTrackView.DataValueField = "TRACKNO";
										
				lstTrackView.DataBind();
				if (lstTrackView.Items.Count>0)
				{
					lstTrackView.SelectedIndex=0;
				}
               // string strOrderNumber = Request.QueryString["StrOrderNo"];
                dtGiftCardNos = Data.GetGiftCards(strOrderNumber);
                dlGCNumView.DataSource = dtGiftCardNos;
                dlGCNumView.DataBind();
                //dtGiftCardNos = Data.GetGiftCards(strOrderNumber);
                //lstGiftCard.DataSource = dtGiftCardNos;
                //lstGiftCard.DataTextField = "GCNO";
                //lstGiftCard.DataValueField = "GCNO";
                //lstGiftCard.DataBind();
               
				//if (!string.IsNullOrEmpty(Request.QueryString["strOrderNo"]))
				//{
				//e.CommandName + " Item " + e.CommandArgument;

				// Session["strOrderNo"] = Request.QueryString["strOrderNo"];

				//}
		
				//theOrderGroup = Session["theOrderGroup"];
			}
			else
			{
				strOrderNumber =ViewState["StrOrderNo"].ToString();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnTrackOrder.Click += new System.EventHandler(this.btnTrackOrder_Click);
            //this.btnEmailReminder.Click += new System.EventHandler(this.btnEmailReminder_Click);
            //this.btnSaveNotes.Click += new System.EventHandler(this.btnSaveNotes_Click);
			this.btnReturnToSearch.Click += new System.EventHandler(this.btnReturnToSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void DisplayStatus(string strOrderNumber)
		{
			strNewStatus = Data.GetOrdStatus(strOrderNumber);
			if (strNewStatus!=null)
			{
				switch(strNewStatus)
				{
					case "I1":
						this.lblStatus.Text = "Order Partially Received";
						break;
					case "I2":
						this.lblStatus.Text = "Order Received & Partially Picked Up";
						break;
					case "I3":
						this.lblStatus.Text = "Received at Store";
						break;
					case "I4":
						this.lblStatus.Text = "Customer Picked Up";
						break;
					case "I5":
						this.lblStatus.Text = "Return to DC";
						break;
					case "PC":
						this.lblStatus.Text = "Shipped";
						break;
					case "P7":
						this.lblStatus.Text = "Warehouse Pending";
						break;
					case "P1":
						this.lblStatus.Text = "Reg Ord Ready Process";
						break;
					case "P2":
						this.lblStatus.Text = "Reg Ord Ready Process";
						break;
					case "PD":
						this.lblStatus.Text = "Order Cancelled";
						break;
					case "PG":
						this.lblStatus.Text = "Order Cancelled";
						break;
					case "PA":
						this.lblStatus.Text = "All Backordered";
						break;
					case "PB":
						this.lblStatus.Text = "Partial Backordered";
						break;
					case "WB":
						this.lblStatus.Text = "Warehouse Backorder";
						break;
					default:
						this.lblStatus.Text = "Pending";
						break;
					
				}
			}

		}
		//Filter out invalid dates
		private string FilterDate (DateTime dt)
		{
			if (dt> DateTime.Parse("1/1/1980"))
			{
				return dt.ToShortDateString();
			}
			else
			{
				return "N/A";
			}

		}
		private void btnReturnToSearch_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("./OrderSearch_LP.aspx" );
		}

        public void dlGCNumView_ItemCommand(object sender, DataListCommandEventArgs e)
        {
            //load data

        }
		private void btnTrackOrder_Click(object sender, System.EventArgs e)
		{
			if (lstTrackView.SelectedValue.Length==22)
			{
				//Response.Redirect("http://www.usps.com/shipping/trackandconfirm.htm");
				Response.Redirect("http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?origTrackNum=" + lstTrackView.SelectedValue);
	
			}
			else 
			{
				Response.Redirect("http://www.fedex.com/Tracking?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers=" + lstTrackView.SelectedValue);
			}
		}

	}
}
