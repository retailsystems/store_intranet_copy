using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for OrderItem.
	/// </summary>
	public class OrderItem : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataList dlItemView;
		protected OrderItemGroup newOrderItemGroup;
		protected string strOrderNumber;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				string strOrderNumber = Request.QueryString["StrOrderNo"];
				
				newOrderItemGroup = OrderItemGroup.GetOrderItemDataTable(strOrderNumber);		        
			
				dlItemView.DataSource = newOrderItemGroup;
							
				dlItemView.DataBind();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		public void dlItemView_ItemCommand(object sender, DataListCommandEventArgs e)
		{
			//load data

		}
		
	}
}
