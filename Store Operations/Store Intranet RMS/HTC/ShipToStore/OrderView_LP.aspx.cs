using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for OrderView.
	/// </summary>
	public class OrderView_LP : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.DropDownList ddlRegion;
		protected System.Web.UI.WebControls.DropDownList ddlDistrict;
		protected System.Web.UI.WebControls.DropDownList ddlStore;
		protected System.Web.UI.WebControls.DropDownList ddlSource;
		protected System.Web.UI.WebControls.DropDownList ddlBrand;
		protected System.Web.UI.WebControls.DropDownList ddlChannel;
		protected System.Web.UI.WebControls.DataList dlOrderView;
		protected System.Web.UI.WebControls.LinkButton lnkPrev;
		protected System.Web.UI.WebControls.TextBox txtPageCurrent;
		protected System.Web.UI.WebControls.Label lblTotal;
		protected System.Web.UI.WebControls.Button btnJump;
		protected System.Web.UI.WebControls.LinkButton lnkNext;
		protected System.Web.UI.WebControls.LinkButton lnkPreviousTop;
		protected System.Web.UI.WebControls.TextBox txtPageNoTop;
		protected System.Web.UI.WebControls.Label lbltxtPageTotalTop;
		protected System.Web.UI.WebControls.LinkButton lnkNextTop;
		protected System.Web.UI.WebControls.Button btnJumpTop;
		protected System.Web.UI.WebControls.LinkButton lnkExport;
		protected System.Web.UI.WebControls.LinkButton lnkPrint;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
        protected System.Web.UI.WebControls.CheckBox chkEmp;
		protected double TotalSales;
		protected int TotalCount;
		protected double ADT;
		protected double UPT;
      
		static LP_OrderGroup newOrderGroup; 
        //protected LP_OrderGroup newOrderGroupNEW = new LP_OrderGroup ();

		protected System.Web.UI.WebControls.Button btnFilter;
		protected LP_Order oneOrder;
		DataTable dtUser;
		ListItem itmAll;
        protected System.Web.UI.WebControls.ObjectDataSource ObjectDataSource1 ;
		int intPermit;
        





        private int totalPages;
        private int pageSize;
        private int _pageIndex;

		public int PageIndex
		{

			get
			{
                object o = (object)(_pageIndex);
				if (o == null)

					return 0; // default to showing the first page
				else
                    return _pageIndex;
			}
			set
			{
                _pageIndex = value;

			}
		}
        
        public int TotalPages
        {

            get
            {
                // look for current page in ViewState
                object o = totalPages;
                if (o == (object)0)
                    return 0; // default to showing the first page
                else
                    return (int)o;
            }
            set
            {
                totalPages = value;

            }
        }

        public int PageSize
		{
			get
			{
                return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["PageSize"]);
			}

            set
            {
                pageSize = value;

            }
		}




        //Jay T. - To measure ViewState size
        //protected override void SavePageStateToPersistenceMedium(object viewState)

        //{

        //    base.SavePageStateToPersistenceMedium(viewState);

        //    LosFormatter format = new LosFormatter();

        //    StringWriter writer = new StringWriter();

        //    format.Serialize(writer, viewState);

        //    int viewStateLength = writer.ToString().Length;
        //}



        string strReg;
        string strDis;
        string strStr;
        string userCode;
        string userLoc;
        string userRegion;
        string strOrdFDt;
        string strOrdTDt;
        string strShpFDt;
        string strShpTDt;
        string strOrderNo;
        string strFName;
        string strLName;
        string strEmail;
        int strStatus;
        int intSource = 0;
        int intBrand = 0;
        int intChannel = 0;
        int intEmp = 1;

		private void Page_Load(object sender, System.EventArgs e)
		{

			try
			{

            //Jay T - chnaged datasource type
			
            //sGroup = new PagedDataSource();
			//sGroup.AllowPaging = true;
			//sGroup.PageSize =PageSize;

           // ObjectDataSource1 = new ObjectDataSource("ShipToStore.OrderView_LP", "GetOrders");
            //ObjectDataSource1.EnablePaging = true;
            //ObjectDataSource1.StartRowIndexParameterName = "PageNumber";
            //ObjectDataSource1.MaximumRowsParameterName = "PageSize";


            

			//get user DataTable from session
			dtUser = (DataTable)Session["UserProfile"];

            strReg = Request.QueryString["Reg"].Trim();
            strDis = Request.QueryString["Dis"].Trim();
            strStr = Request.QueryString["Str"].Trim();

            //string strStr = strStr1.ToString().PadLeft(4, '0');

            if (strStr != "0")
            {
                strStr = strStr.ToString().PadLeft(4, '0');
            }

            //make sure the user's current Security allows him/her to browse this Reg-Dis-Store combo
            intPermit = int.Parse(Session["ViewPermission"].ToString());
            userCode = Utility.ParseDepartment(dtUser.Rows[0]["Dept"].ToString().Trim());
            userLoc = dtUser.Rows[0]["Location"].ToString().Trim();
            userRegion = Session["UserRegion"].ToString();

            strOrdFDt = Request.QueryString["OrdFDt"].Trim();
            strOrdTDt = Request.QueryString["OrdTDt"].Trim();

            if (Request.QueryString["ShpFDt"] != "" && Request.QueryString["ShpTDt"] != "")
            {
                strShpFDt = Request.QueryString["ShpFDt"].Trim();
                strShpTDt = Request.QueryString["ShpTDt"].Trim();
            }
            else
            {
                strShpFDt = "1/1/1980";
                strShpTDt = "1/1/1980";
            }

            strOrderNo = Request.QueryString["OrdNo"].Trim();
            strFName = Request.QueryString["FName"].Trim();
            strLName = Request.QueryString["LName"].Trim();
            strEmail = Request.QueryString["Email"].Trim();
            strStatus = int.Parse(Request.QueryString["Stat"].Trim());
            intEmp = int.Parse(Request.QueryString["Emp"].Trim());

			if (!IsPostBack)
			{


					if (Utility.CheckSecurity(intPermit, strReg, strDis, strStr, userCode, userLoc, userRegion)==true)
					{
                        LoadUserOptions();
                        Session["LP_OrderGroup"] = newOrderGroup;
                        LoadData(PageIndex);

					}
					else

					{
						Response.Redirect("./Login.aspx?Error=NoPermission");

					}
				
				}
			else
			{

                if(Session["PageIndex"]!=null)
                PageIndex = (int)Session["PageIndex"];

            newOrderGroup = (LP_OrderGroup)Session["LP_OrderGroup"];
            TotalSales = Math.Round(newOrderGroup.TotalAmount, 2);
            TotalCount = newOrderGroup.Count;
            ADT = (TotalCount == 0 ? 0 : Math.Round(TotalSales / TotalCount, 2));
            UPT = (TotalCount == 0 ? 0 : newOrderGroup.TotalUnit / TotalCount);	
			}
			}
			catch(Exception ex)
			{
               throw ex;
			}
		}

        public LP_OrderGroup GetOrders(string Region, string District, string Store, string OrderFromDate, string OrderToDate, string ShipFromDate, string ShipToDate, string OrderNumber, string FirstName, string LastName, string Email, int Status, int Source, int Brand, int Channel, int PageNumber, int Emp, int PageSize,out int TotalPages)        
        {

            try
            {
                if (true)
                {
                    newOrderGroup = LP_OrderGroup.GetOrderGroup(Region, District, Store,
                    DateTime.Parse(OrderFromDate), DateTime.Parse(OrderToDate), DateTime.Parse(ShipFromDate),
                    DateTime.Parse(ShipToDate), OrderNumber, FirstName, LastName, Email, Status, Source, Brand, Channel, PageNumber, PageSize, Emp, out TotalPages);
                }
                else
                {
                    Response.Redirect("./Login.aspx?Error=NoPermission");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            Session["LP_OrderGroup"] = newOrderGroup;
            return newOrderGroup;
        }

		#region DropDown Options

		protected void LoadUserOptions()
		{
			
		
			switch (intPermit)
			{
				case 1:
					//admin show all
					LoadRegionDropDown();
					break;
				case 5:
					//executive show all
					LoadRegionDropDown();
					break;
				case 2:
					//RD show only that region
					LoadRegionOnly();
					break;
				case 3:
					//DM show only that district
					LoadDistrictOnly();
					break;
				case 4:
					//Store show only that store
					LoadStoreOnly();
					break;

                case 6:
                    //Store show only that store
                    LoadRegionDropDown();
                    break;


			}

		}
		private void LoadRegionOnly()
		{
			ddlRegion.Items.Clear();
			ddlDistrict.Items.Clear();
			ddlStore.Items.Clear();

			dtUser = (DataTable)Session["UserProfile"];
			string RegionNo = Utility.ParseDepartment(dtUser.Rows[0]["Dept"].ToString().Trim());

			DataTable dtDistrict = Data.GetDistrict(RegionNo);

			if (dtDistrict!=null)
			{
				string strRegion = dtDistrict.Rows[0]["Region"].ToString().Trim();
				string strDistrict = dtDistrict.Rows[0]["District"].ToString().Trim();
			
				ListItem itmRegion = new ListItem();
				itmRegion.Text=strRegion;
				itmRegion.Value=strRegion;
				ddlRegion.Items.Add(itmRegion);
				ddlRegion.Enabled = false;
				ddlDistrict.DataSource = dtDistrict;
				ddlDistrict.DataTextField="District";
				ddlDistrict.DataValueField="District";
				ddlDistrict.DataBind();
				LoadDefaultOptions(ddlDistrict, false);
				LoadDefaultOptions(ddlStore, true);
					
			}
			
		}

		private void LoadDistrictOnly()
		{
			ddlRegion.Items.Clear();
			ddlDistrict.Items.Clear();
			ddlStore.Items.Clear();

			dtUser = (DataTable)Session["UserProfile"];
			string DistrictNo = Utility.ParseDepartment(dtUser.Rows[0]["Dept"].ToString().Trim());

			DataTable dtStore = Data.GetStore(DistrictNo);

			if (dtStore!=null)
			{
				string strRegion = dtStore.Rows[0]["Region"].ToString().Trim();
				string strDistrict = dtStore.Rows[0]["District"].ToString().Trim();
			

				ListItem itmRegion = new ListItem();
				itmRegion.Text=strRegion;
				itmRegion.Value=strRegion;
				ddlRegion.Items.Add(itmRegion);
				ddlRegion.Enabled = false;

				ListItem itmDistrict = new ListItem();
				itmDistrict.Text=strDistrict;
				itmDistrict.Value=strDistrict;
				ddlDistrict.Items.Add(itmDistrict);
				ddlDistrict.Enabled = false;

				ddlStore.DataSource = dtStore;
				ddlStore.DataTextField="StoreName";
				ddlStore.DataValueField="StoreNum";
				ddlStore.DataBind();
				LoadDefaultOptions(ddlStore, false);
				//ddlStore.Enabled = true;			
			}			
		}

		private void LoadStoreOnly()
		{
			ddlRegion.Items.Clear();
			ddlDistrict.Items.Clear();
			ddlStore.Items.Clear();
			dtUser = (DataTable)Session["UserProfile"];
			string StoreNo = dtUser.Rows[0]["Location"].ToString().Trim();

			DataTable dtStore = Data.GetStoreSingle(StoreNo);

			if (dtStore!=null)
			{
				string strRegion = dtStore.Rows[0]["Region"].ToString().Trim();
				string strDistrict = dtStore.Rows[0]["District"].ToString().Trim();
				string strStoreName = dtStore.Rows[0]["StoreName"].ToString().Trim();

				ListItem itmRegion = new ListItem();
				itmRegion.Text=strRegion;
				itmRegion.Value=strRegion;
				ddlRegion.Items.Add(itmRegion);
				ddlRegion.Enabled = false;

				ListItem itmDistrict = new ListItem();
				itmDistrict.Text=strDistrict;
				itmDistrict.Value=strDistrict;
				ddlDistrict.Items.Add(itmDistrict);
				ddlDistrict.Enabled = false;

				ListItem itmStore = new ListItem();
				itmStore.Text=strStoreName;
				itmStore.Value=StoreNo;
				ddlStore.Items.Add(itmStore);
				ddlStore.Enabled = false;			
			}
			
		}
		#endregion





		#region LoadData
		private void LoadData(int CurrentPage)
		{			
			try
			{

				//sGroup.CurrentPageIndex = CurrentPage;
				//sGroup.DataSource = newOrderGroup;					
				dlOrderView.DataSource = ObjectDataSource1; 	
				dlOrderView.DataBind();
				//paging 
                //this.txtPageNoTop.Text = Request.QueryString["PageNumber"];
                //this.txtPageCurrent.Text = Request.QueryString["PageNumber"];

                this.txtPageNoTop.Text = (PageIndex + 1).ToString();
                this.txtPageCurrent.Text = (PageIndex + 1).ToString();
                this.lbltxtPageTotalTop.Text = TotalPages.ToString(); 
                this.lblTotal.Text = TotalPages.ToString();
                this.lnkPrev.Enabled = (PageIndex +1 > 1) ?true : false;
                this.lnkPreviousTop.Enabled = (PageIndex + 1 > 1) ?true : false;
                this.lnkNext.Enabled = (PageIndex + 1)  < TotalPages ? true : false; ;
                this.lnkNextTop.Enabled = (PageIndex + 1)  < TotalPages ? true : false; ;
                newOrderGroup = (LP_OrderGroup)Session["LP_OrderGroup"];
				TotalSales = Math.Round(newOrderGroup.TotalAmount, 2);
				TotalCount =newOrderGroup.Count;
				ADT = (TotalCount==0? 0: Math.Round(TotalSales/TotalCount, 2));
				UPT = (TotalCount==0? 0: newOrderGroup.TotalUnit/TotalCount);				
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion


		private void LoadDefaultOptions(DropDownList ddlList, bool IsAllSelected)
		{
			itmAll = new ListItem();
			itmAll.Text="All";
			itmAll.Value = "0";

			ddlList.Items.Insert(0, itmAll);
			if (IsAllSelected)
			{
				ddlList.Enabled = false;

			}
			

		}
		private void LoadRegionDropDown()
		{
			
				DataTable dtRegion =(DataTable)Application["dtRegion"];
				
				this.ddlRegion.DataSource = dtRegion;
				this.ddlRegion.DataTextField = "Region";
				this.ddlRegion.DataValueField = "Region";
				ddlRegion.DataBind();
				//make sure all is selected
				LoadDefaultOptions(ddlRegion, false);
				LoadDefaultOptions(ddlDistrict, true);
				LoadDefaultOptions(ddlStore, true);
			
			
		

		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   
			this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
			this.lnkExport.Click += new System.EventHandler(this.lnkExport_Click);
			this.lnkPrint.Click += new System.EventHandler(this.lnkPrint_Click);
			this.lnkPreviousTop.Click += new System.EventHandler(this.lnkPreviousTop_Click);
			this.btnJumpTop.Click += new System.EventHandler(this.btnJumpTop_Click);
			this.lnkNextTop.Click += new System.EventHandler(this.lnkNextTop_Click);
			this.dlOrderView.SelectedIndexChanged += new System.EventHandler(this.dlOrderView_SelectedIndexChanged);
			this.lnkPrev.Click += new System.EventHandler(this.lnkPrev_Click);
			this.btnJump.Click += new System.EventHandler(this.btnJump_Click);
			this.lnkNext.Click += new System.EventHandler(this.lnkNext_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void ddlRegion_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (ddlRegion.SelectedValue!="0")
			{
				this.ddlStore.Enabled = false;
				this.ddlDistrict.Enabled = true;			
			
				//depends on selection, load District
				string strR = ddlRegion.SelectedValue;
				DataTable dtDistrict = Data.GetDistrict(strR);
				ddlDistrict.DataSource = dtDistrict;
				this.ddlDistrict.DataTextField = "District";
				this.ddlDistrict.DataValueField = "District";

				LoadDefaultOptions(ddlDistrict, false);
				ddlDistrict.DataBind();
				LoadDefaultOptions(ddlDistrict, false);
			}
			else
			{
				ddlDistrict.Items.Clear();
				ddlStore.Items.Clear();

				LoadDefaultOptions(ddlDistrict, true);
				LoadDefaultOptions(ddlStore, true);
		

			}

		}

		protected void ddlDistrict_SelectedIndexChanged(object sender, System.EventArgs e)
		{

			//depends on selection, load stores
			if (ddlDistrict.SelectedValue!="0")
			{
				this.ddlStore.Enabled = true;
			
			
				//depends on selection, load Store
				string strD = ddlDistrict.SelectedValue;
				DataTable dtStore = Data.GetStore(strD);
				ddlStore.DataSource = dtStore;
				this.ddlStore.DataTextField = "StoreName";
				this.ddlStore.DataValueField = "StoreNum";

				
				ddlStore.DataBind();
				LoadDefaultOptions(ddlStore, false);
			}
			else

			{
				ddlStore.Items.Clear();
				LoadDefaultOptions(ddlStore, true);
				
			}

		}

		private void dlOrderView_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		public void dlOrderView_ItemCommand(object sender, DataListCommandEventArgs e)
		{
			if (e.CommandName=="OrderDetail_LP")
			{
				string strOrderNumber = e.CommandArgument.ToString();
				Response.Redirect("./OrderDetail_LP.aspx?StrOrderNo=" + strOrderNumber);
			}
			else if (e.CommandName=="TrackInfo")
			{
                //int nItem = e.Item.ItemIndex;
				LinkButton theLink= (LinkButton)e.Item.FindControl("lnkClient");
				string strOrderNumber=null;
				if(theLink!=null)
				{
					strOrderNumber = theLink.ToolTip.ToString();
				}

//    			string strOrderNumber = e.CommandArgument.ToString();
			    Response.Redirect("./OrderDetail_LP.aspx?StrOrderNo=" + strOrderNumber);
//				string strTrackNo = e.CommandArgument.ToString();
//				if (strTrackNo.Length == 22)
//				{
//					Response.Redirect("http://www.usps.com/shipping/trackandconfirm.htm");
//				}
//				else
//				{
//				    Response.Redirect("http://www.fedex.com/Tracking?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers=" + strTrackNo);
//				}
			}
            
		}
		private void lnkExport_Click(object sender, System.EventArgs e)
		{
            newOrderGroup = (LP_OrderGroup)Session["LP_OrderGroup"];
            //newOrderGroupNEW = (LP_OrderGroup)ViewState["LP_OrderGroup"];
			if (newOrderGroup!=null)
			{
				
				TotalSales = Math.Round(newOrderGroup.TotalAmount, 2);
				TotalCount =newOrderGroup.Count;
				ADT = (TotalCount==0? 0: Math.Round(TotalSales/TotalCount, 2));
				UPT = (TotalCount==0? 0: newOrderGroup.TotalUnit/TotalCount);
				
				//DataList dlExportView = new DataList 

				dlOrderView.DataSource = newOrderGroup;
				dlOrderView.DataBind();

				Response.Clear();
				Response.AddHeader("content-disposition", "attachment;filename=ShopShipExport.xls");
				Response.Charset = "";
				Response.Cache.SetCacheability(HttpCacheability.NoCache);
				//Response.ContentType = "application/vnd.xls";
				Response.ContentType = "application/vnd.ms-excel";
				String strStyle  = "<style>td{mso-style-parent:text; mso-number-format:\\@;white-space:normal;border:1px solid black;height:50px;width:100px;}</style>";
				System.IO.StringWriter stringWrite = new System.IO.StringWriter();
				System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
			
				this.ClearControls(dlOrderView);
				dlOrderView.RenderControl(htmlWrite);
				Response.Write(strStyle);
				Response.Write(stringWrite.ToString());
				Response.End();
			}
		}
		private void lnkPrint_Click(object sender, System.EventArgs e)
		{

            newOrderGroup = (LP_OrderGroup)Session["LP_OrderGroup"];
			if (newOrderGroup!=null)
			{
				
				Session["PrintObject"] = newOrderGroup;
				this.RegisterStartupScript("Print", "<script>OpenPrint()</script>");
			
			}
		}
	
		private void ClearControls(Control control)
		{
			for (int i=control.Controls.Count -1; i>=0; i--)
			{
				ClearControls(control.Controls[i]);
			}
			if (!(control is TableCell))
			{
				if (control.GetType().GetProperty("SelectedItem") != null)
				{
					LiteralControl literal = new LiteralControl();
					control.Parent.Controls.Add(literal);
					try
					{
							literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control,null);
					}
					catch
					{
					}
					control.Parent.Controls.Remove(control);
				}
				else if (control.GetType() == typeof(LinkButton))
				{
					LiteralControl literal = new LiteralControl();
					control.Parent.Controls.Add(literal);					
					literal.Text = (control as LinkButton).CommandArgument.ToString();
					
					control.Parent.Controls.Remove(control);
				}

				else
					if (control.GetType().GetProperty("Text") != null)
				{
					LiteralControl literal = new LiteralControl();
					control.Parent.Controls.Add(literal);
					literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control,null);
					control.Parent.Controls.Remove(control);
				}
			}
			return;
		}

		protected void btnJump_Click(object sender, EventArgs e)
		{
			//Jump page
            if (int.Parse(this.txtPageCurrent.Text.ToString()) >= 1)
            PageIndex = int.Parse(this.txtPageCurrent.Text.ToString()) - 1;
            Session["PageIndex"] = PageIndex;
            LoadData(PageIndex);
		}
		protected void btnJumpTop_Click(object sender, System.EventArgs e)
		{
            if(int.Parse(this.txtPageNoTop.Text.ToString()) >= 1)
            PageIndex = int.Parse(this.txtPageNoTop.Text.ToString()) - 1;
            Session["PageIndex"] = PageIndex;
            LoadData(PageIndex);
		}
		protected void Page_Click(object sender, CommandEventArgs e)
		{
			//Click page
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
		   Response.Redirect("./OrderSearch_LP.aspx");
		}

		private void btnFilter_Click(object sender, System.EventArgs e)
		{
			//Quick Search data
			 strReg = ddlRegion.SelectedValue;
			 strDis = ddlDistrict.SelectedValue;
			 strStr = ddlStore.SelectedValue;
			if (strStr != "0")
			{
				strStr = strStr.ToString().PadLeft(4, '0');
			}
						
			strOrdFDt = Request.QueryString["OrdFDt"].Trim();
			strOrdTDt = Request.QueryString["OrdTDt"].Trim();
			
			if (Request.QueryString["ShpFDt"]!="" && Request.QueryString["ShpTDt"]!="")
			{
				strShpFDt = Request.QueryString["ShpFDt"].Trim();
				strShpTDt = Request.QueryString["ShpTDt"].Trim();
			}
			else
			{
				strShpFDt = "1/1/1980";
				strShpTDt = "1/1/1980";
			}
					
			strOrderNo = "";
			strFName = "";
			strLName = "";
			strEmail = "";

			strStatus = int.Parse(Request.QueryString["Stat"].Trim());
			intSource = int.Parse(ddlSource.SelectedValue);
			intBrand = int.Parse(ddlBrand.SelectedValue);
			intChannel = int.Parse(ddlChannel.SelectedValue);
            intEmp = int.Parse(Request.QueryString["Emp"].Trim());
            //if (chkEmp != null && chkEmp.Checked)
            //{
            //    intEmp = 1;         
            //}
            //else
            //{
            //    intEmp = 0; 
            //}


			//newOrderGroup = LP_OrderGroup.GetOrderGroupFilter(strReg, strDis, strStr, 
			//	DateTime.Parse(strOrdFDt),DateTime.Parse(strOrdTDt),DateTime.Parse(strShpFDt),
			//	DateTime.Parse(strShpTDt),strOrderNo,strFName, strLName,strEmail, strStatus, intSource, intBrand, intChannel);

            Session["LP_OrderGroup"] = newOrderGroup;
            PageIndex = 0;
            Session["PageIndex"] = PageIndex;
            LoadData(PageIndex);

		}

		private void lnkNextTop_Click(object sender, System.EventArgs e)
		{
            PageIndex += 1;
            Session["PageIndex"] = PageIndex;
            LoadData(PageIndex);
		
		}

		private void lnkPreviousTop_Click(object sender, System.EventArgs e)
		{

            PageIndex -= 1;
            Session["PageIndex"] = PageIndex;
            LoadData(PageIndex);
		}

		private void lnkPrev_Click(object sender, System.EventArgs e)
		{
            PageIndex -= 1;
            Session["PageIndex"] = PageIndex;
            LoadData(PageIndex);
		
		}

		private void lnkNext_Click(object sender, System.EventArgs e)
		{
            PageIndex += 1;
            Session["PageIndex"] = PageIndex;
            LoadData(PageIndex);
		
		}

		//display Package total if more than one is shipped
		protected string PickPackage(string TrackNo, string total)
		{
			if (total!="")
			{
				if (int.Parse(total) >1)
				{
					return total + " pcs";			
				}
				else 
				{
					return TrackNo;

				}
			}
			else
			{
				return null;
			}
		}
		//convert weight
		protected string ConvertWeight(string wt)
		{
			if (wt!="")
			{
				double dwt = Double.Parse(wt);
				return (dwt/10000).ToString() + " lbs";

			}
			else
			{
				return wt;
			}
		}

	//filter out bad dates
		protected string ConvertDate(string dt)
		{
			if (DateTime.Parse(dt) > DateTime.Parse("1/1/1980"))
			{
				return dt.ToString();

			}
			else
			{
				return "";

			}
		}
	
	//filter out long notes
		protected string ConvertNotes(string nt)
		{
			if (nt.Length>10)
			{
				return nt.Substring(0, 10)+"...";

			}
			else
			{
				return nt;
			}
		}

        protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            try
            {
                e.InputParameters["Region"] = strReg;
                e.InputParameters["District"] = strDis;
                e.InputParameters["Store"] = strStr;
                e.InputParameters["OrderFromDate"] = strOrdFDt;
                e.InputParameters["OrderToDate"] = strOrdTDt;
                e.InputParameters["ShipFromDate"] = strShpFDt;
                e.InputParameters["ShipToDate"] = strShpTDt;
                e.InputParameters["OrderNumber"] = strOrderNo;
                e.InputParameters["FirstName"] = strFName;
                e.InputParameters["LastName"] = strLName;
                e.InputParameters["Email"] = strEmail;
                e.InputParameters["Status"] = strStatus;
                e.InputParameters["Source"] = intSource;
                e.InputParameters["Brand"] = intBrand;
                e.InputParameters["Channel"] = intChannel;
                e.InputParameters["Emp"] = intEmp;
                e.Arguments.StartRowIndex = PageIndex * PageSize;
                e.Arguments.MaximumRows = PageSize;

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        protected void ObjectDataSource1_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            int totalRows = (int)e.OutputParameters["TotalPages"];
            TotalPages = (int)Math.Ceiling((decimal)totalRows/ (decimal)PageSize);
            

        }
	}
}
