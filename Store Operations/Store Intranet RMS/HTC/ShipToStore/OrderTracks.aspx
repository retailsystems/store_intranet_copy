<%@ Page language="c#" Codebehind="OrderTracks.aspx.cs" AutoEventWireup="false" Inherits="ShipToStore.OrderTracks" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OrderTracks</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE cellSpacing="0" cellPadding="0" width="219" align="center" border="1" style="WIDTH: 219px; HEIGHT: 160px">
				<tr>
					<td>Please select one Track No</td>
				</tr>
				<tr>
					<td><asp:ListBox ID="lstTrackView" runat="server" Width="300" SelectionMode="Single" OnItemCommand="dlTrackView_TrackCommand"
							EnableViewState="True" RepeatLayout="Flow" RepeatDirection="Horizontal" DataKeyField="StrTrackNo"></asp:ListBox>
					</td>
				</tr>
				<tr>
					<td align="right"><asp:button id="btnTrackOrder" runat="server" CssClass="buttonYellow" Text="TRACK ORDER"></asp:button></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
