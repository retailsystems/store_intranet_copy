<%@ Page language="c#" Codebehind="OrderView.aspx.cs" AutoEventWireup="false" Inherits="ShipToStore.OrderView" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >
<html>
	<head>
		<title>OrderView</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
		<script language="javascript" type="text/javascript">

            function StoreNotes(strField)
            {
                        window.open('OrderNote.aspx?strOrderNo=' + strField, 'StoreNotes', 'width=250,height=350,resizable=yes');
            }
            
			function OpenPrint(){
			
					window.open('Print.aspx', 'PrintResults', 'width=820, height=700, resizable=yes,scrollbars=yes');
			}
            
        
			
	
		</script>
	</head>
	<body bottomMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header></div>
			<div id="main"><br>
				<TABLE class="innerTable" id="Table1" cellSpacing="0" cellPadding="1" width="98%" align="center"
					border="0">
					<TR>
						<TD class="tableTitle" width="850">Filter By: </td>
						</tr>
						<tr><TD class="tableCell" width="850">Region
							<asp:dropdownlist id="ddlRegion" runat="server" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged"
								AutoPostBack="True" CssClass="tableInput" Width="45px"></asp:dropdownlist>&nbsp;District
							<asp:dropdownlist id="ddlDistrict" runat="server" OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged"
								AutoPostBack="True" CssClass="tableInput" Width="58px"></asp:dropdownlist>&nbsp;Store
							<asp:dropdownlist id="ddlStore" runat="server" CssClass="tableInput" Width="180px"></asp:dropdownlist>&nbsp;Source
							<asp:dropdownlist id="ddlSource" runat="server" CssClass="tableInput" Width="66px">
								<asp:ListItem Value="0">All</asp:ListItem>
								<asp:ListItem Value="1">Store</asp:ListItem>
								<asp:ListItem Value="2">Home</asp:ListItem>
							</asp:dropdownlist><!--Brand-->
							<asp:dropdownlist  Visible="false" id="ddlBrand" runat="server" CssClass="tableInput" Width="50px">
								<asp:ListItem Value="0">All</asp:ListItem>
								<asp:ListItem Value="1">HT</asp:ListItem>
								<asp:ListItem Value="5">TD</asp:ListItem>
								<asp:ListItem Value="2">SH</asp:ListItem>
							</asp:dropdownlist>&nbsp;Channel
							<asp:dropdownlist id="ddlChannel" runat="server" CssClass="tableInput" Width="140px">
						
							</asp:dropdownlist> 
							<asp:button id="btnFilter" runat="server" Text=" Go "></asp:button></TD>
						<TD align="right"><asp:linkbutton id="lnkExport" runat="server" CommandName="Export">Export</asp:linkbutton>&nbsp;&nbsp;
							<!--<A onclick="Javascript: CallPrint('dvPrint');" href="#">Print</A>-->
							<asp:linkbutton id="lnkPrint" runat="server" CommandName="Print">Print</asp:linkbutton>&nbsp;&nbsp;
						</TD>
					</TR>
				</TABLE>
				<BR>
				<table class="footerTable" cellSpacing="0" width="98%" align="center">
					<tr>
						<td align="right"><asp:linkbutton id="lnkPreviousTop" runat="server" CommandName="Prev" OnCommand="Page_Click">&lt; Previous</asp:linkbutton>| 
							Page
							<asp:textbox id="txtPageNoTop" runat="server" CssClass="tableInput" Columns="4"></asp:textbox>&nbsp;of
							<asp:label id="lbltxtPageTotalTop" runat="server" Visible="true"></asp:label><asp:button id="btnJumpTop" onclick="btnJumpTop_Click" runat="server" Text="Jump"></asp:button>|
							<asp:linkbutton id="lnkNextTop" runat="server" CommandName="Next" OnCommand="Page_Click">Next &gt;</asp:linkbutton><br>
							&nbsp;</td>
					</tr>
				</table>
				<div id="dvPrint"><asp:datalist id="dlOrderView" runat="server" Width="100%" OnItemCommand="dlOrderView_ItemCommand"
						EnableViewState="True" RepeatLayout="Flow" RepeatDirection="Horizontal" DataKeyField="StrOrderNo">
						<HeaderTemplate>
							<table width="98%" cellspacing="0" cellpadding="2" class="innerTableView" align="center">
								<tr class="tableTitle">
									<td colspan="19" class="tableTitle">Order Search Results</td>
								</tr>
								
								<tr class="tableHeader">
									<td align="center" width="30">
										No.
									</td>
									<td align="center" width="30">
										Reg
									</td>
									<td align="center" width="30">
										Dist
									</td>
									<td align="center" width="40">
										Store
									</td>
									<td align="center" width="80">
										Full Order No
									</td>
									<td align="center" width="50">
										Order Date
									</td>
									<td align="center" width="50">
										Pay
									</td>
									<td align="center" width="80">
										Customer
									</td>
									<td align="center" width="50">
										Phone
									</td>
									<td align="center" width="60">
										Total
									</td>
									<td align="center" width="50">
										Shipped On
									</td>
									<td align="center" width="50">
										Weight
									</td>
									<td align="center" width="50">
										Tracking #
									</td>
									<td align="center" width="50">
										Received On
									</td>
									<td align="center" width="50">
										Ship Source</td>
									<td align="center" width="40">
										Channel
									</td>
									<td align="center" width="30">
										Can-celled
									</td>
									<td align="center" width="90">
										Notes</td>
									<td align="center" width="50">
										PU Date</td>
								</tr>
						</HeaderTemplate>
						<ItemTemplate>
						    
							<tr height="25" id="total" runat="server">
								<td align="right">
									<%#(Container.ItemIndex+1)%>
								</td>
								<td align="right">
									<%# DataBinder.Eval(Container.DataItem,"StrRegionID") %>
								</td>
								<td align="right">
									<%# DataBinder.Eval(Container.DataItem,"StrDistrictID") %>
								</td>
								<td align="right">
									<%# DataBinder.Eval(Container.DataItem, "StrStoreID")%>
								</td>
								<td align="right">
									<asp:LinkButton ID="lnkOrder" runat="server" CommandName='OrderDetail' ToolTip='<%# DataBinder.Eval(Container.DataItem,"StrRecFullOrdNo")%>' CommandArgument='<%# DataBinder.Eval(Container.DataItem,"StrRecFullOrdNo")%>'>
										<%# DataBinder.Eval(Container.DataItem, "StrRecFullOrdNo")%>
									</asp:LinkButton>
								</td>
								<td align="right">
									<%# DataBinder.Eval(Container.DataItem, "DtOrderDate","{0:d}")%>
								</td>
								<td align="right">
									<%# DataBinder.Eval(Container.DataItem, "StrPaymentType")%>
								</td>
								<td align="right">
									<%# DataBinder.Eval(Container.DataItem, "StrCustName")%>
								</td>
								<td align="right">
									<%# FormatPhone(DataBinder.Eval(Container.DataItem, "StrCustDayPhone", "{0}"))%>
								</td>
								<td align="right">
									<%# DataBinder.Eval(Container.DataItem, "DblOrderTotal", "{0:f}")%>
								</td>
								<td align="right">
									<%# ConvertDate(DataBinder.Eval(Container.DataItem, "DtShippedDate", "{0:d}"))%>
								</td>
								<td align="right">
									<%# ConvertWeight(DataBinder.Eval(Container.DataItem, "DblOrderWeight", "{0:f}"))%>
								</td>
								<td align="left">
									<asp:LinkButton ID="lnkClient" runat="server" CommandName="TrackInfo" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"StrTrackingNo", "{0}")%>' ToolTip='<%# DataBinder.Eval(Container.DataItem,"StrRecFullOrdNo")%>'>
										<%# DataBinder.Eval(Container.DataItem,"StrTrackingNo", "{0}") %>
									</asp:LinkButton>
								</td>
								<td align="right">
									<%# ConvertDate(DataBinder.Eval(Container.DataItem, "DtReceivedDate", "{0:d}"))%>
								</td>
								<td align="right">
									<%# DataBinder.Eval(Container.DataItem, "StrShippedSource")%>
								</td>
								<td align="center">
									<%# DataBinder.Eval(Container.DataItem, "Channel") %>
								</td>
								<td align="right">
									<%# DataBinder.Eval(Container.DataItem, "CancelledUnits") %>
								</td>
								<td align="right">
								<a href="#" onclick="javascript:StoreNotes('<%# DataBinder.Eval(Container.DataItem,"StrOrderNo")%>');"><%# ConvertNotes(DataBinder.Eval(Container.DataItem,"StrStoreNotes", "{0}"))%></a>
								
								</td>
								<td align="right">
									<%# ConvertDate(DataBinder.Eval(Container.DataItem, "DtCustPickUpDate", "{0:d}"))%>
								</td>
							</tr>
							
						</ItemTemplate>
						<FooterTemplate>
							<tr height="25" id="Tr1" runat="server" class="tableCell">
								<td align="left">
								</td>
								<td align="left">
								</td>
								<td align="left">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
									Total Sales
								</td>
								<td align="right">
								</td>
								<td align="right">
									Total Count
								</td>
								<td align="right">
									ADT
								</td>
								<td align="right">
									UPT
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
							</tr>
							<tr height="25" id="Tr2" runat="server" class="tableCell">
							<td align="left">
								</td>
								<td align="left">
								</td>
								<td align="left">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="Right">
									<%=TotalSales%>
								</td>
								<td align="right">
								</td>
								<td align="right">
									<%=TotalCount%>
								</td>
								<td align="right">
									<%=ADT%>
								</td>
								<td align="right">
									<%=UPT%>
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
								<td align="right">
								</td>
							</tr>
							</table>
						</FooterTemplate>
					</asp:datalist></div>
				<br />
				<table class="footerTable" cellSpacing="0" width="98%" align="center">
					<tr>
						<td align="right"><asp:linkbutton id="lnkPrev" runat="server" CommandName="Prev" OnCommand="Page_Click">&lt; Previous</asp:linkbutton>| 
							Page
							<asp:textbox id="txtPageCurrent" runat="server" CssClass="tableInput" Columns="4"></asp:textbox>&nbsp;of
							<asp:label id="lblTotal" runat="server" Visible="true"></asp:label><asp:button id="btnJump" onclick="btnJump_Click" runat="server" Text="Jump"></asp:button>|
							<asp:linkbutton id="lnkNext" runat="server" CommandName="Next" OnCommand="Page_Click">Next &gt;</asp:linkbutton><br>
							&nbsp;</td>
					</tr>
				</table>
				<br />
				<table height="140" cellSpacing="0" cellPadding="1" width="98%" align="center" border="0">
					<tr>
						<td vAlign="top" align="right"><asp:button id="btnMenu" runat="server" CssClass="buttonRed" Text="STORE MENU" Visible="false" OnClick="btnMenu_Click"></asp:button>
						&nbsp;<asp:button id="btnSearch" runat="server" CssClass="buttonRed" Text="SEARCH MENU"></asp:button></td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</html>
