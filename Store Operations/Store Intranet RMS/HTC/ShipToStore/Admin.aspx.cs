using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace ShipToStore
{
	public partial class Admin : System.Web.UI.Page
	{
		#region Constants
		#endregion

		#region Members
		protected TextBox TrackingNumber;
		protected TextBox OrderNumber;
		protected TextBox FullOrderNumber;
		protected DataList SearchResults;
		private string HelpdeskDepartments;
		#endregion Members

		#region Properties
		#endregion

		#region Public Methods
		#endregion

		#region Private Methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				try
				{
					int intPermit = 0;
					if (Session["ViewPermission"] != null)
						intPermit = int.Parse(Session["ViewPermission"].ToString());

					if (intPermit == 0)
					{
						Response.Redirect("./Login.aspx");
					}
					else if (intPermit != 1)
					{
						Response.Redirect("./OrderSearch.aspx");
					}
				}
				catch (Exception ex)
				{
					Response.Write(ex.StackTrace + "\n\n" + ex.Message);
				}

				SearchCriteriaEmpty.Visible = false;
			}

			HelpdeskDepartments = ConfigurationSettings.AppSettings["HelpdeskDepartments"];
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Submit_Click(object sender, EventArgs e)
		{
			Search();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void SearchResults_ItemCommand(object sender, DataListCommandEventArgs e)
		{
			if (e.CommandName == "checkin")
			{
				string result = ForcedCheckin(e.CommandArgument.ToString());
				if (result != string.Empty)
				{
					Label lbl = (Label)e.Item.FindControl("ActionResult");
					lbl.Text = result;
					lbl.Visible = true;
				}
			}
			else if (e.CommandName == "checkout")
			{
				string result = ForcedCheckout(e.CommandArgument.ToString());
				if (result != string.Empty)
				{
					Label lbl = (Label)e.Item.FindControl("ActionResult");
					lbl.Text = result;
					lbl.Visible = true;
				}
			}
			else if (e.CommandName == "email")
			{
				string result = SendReminderEmail(e.CommandArgument.ToString());
				if (result != string.Empty)
				{
					Label lbl = (Label)e.Item.FindControl("ActionResult");
					lbl.Text = result;
					lbl.Visible = true;
				}
			}
			else if (e.CommandName == "undocheckout")
			{
			}
			else if (e.CommandName == "undoabandon")
			{
				string result = UndoAbandon(e.CommandArgument.ToString());
				if (result != string.Empty)
				{
					Label lbl = (Label)e.Item.FindControl("ActionResult");
					lbl.Text = result;
					lbl.Visible = true;
				}
			}
		}

		protected void SearchResults_ItemDataBound(object sender, DataListItemEventArgs e)
		{
			DataTable user = (DataTable)Session["UserProfile"];
			if (HelpdeskDepartments.Contains(user.Rows[0]["Dept"].ToString().Trim()))
			{
				e.Item.FindControl("Actions").Visible = true;
			}
			else
			{
				e.Item.FindControl("Actions").Visible = false;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		private void Search()
		{
			string tnum = null;
			string onum = null;
			string fonum = null;

			if (TrackingNumber.Text.Trim().Length > 0)
				tnum = TrackingNumber.Text.Trim();
			if (OrderNumber.Text.Trim().Length > 0)
				onum = OrderNumber.Text.Trim();
			if (FullOrderNumber.Text.Trim().Length > 0)
				fonum = FullOrderNumber.Text.Trim();

			if (tnum == null && onum == null && fonum == null)
			{
				SearchCriteriaEmpty.Visible = true;
			}
			else
			{
				SearchCriteriaEmpty.Visible = false;
				SearchResults.DataSource = Data.GetAdminSearchOrders(tnum, onum, fonum);
				SearchResults.DataBind();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="FullOrderNumber"></param>
		/// <returns></returns>
		private string ForcedCheckin(string FullOrderNumber)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();
			int resultid = 0;
            string strCompany = ConfigurationSettings.AppSettings["Company"];
			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				cm.CommandText = "usp_RecInstr_Insert_ByOrdTrack";
				cm.CommandTimeout = 350;

				cm.Parameters.Clear();
				cm.Parameters.Add("@Company", SqlDbType.VarChar).Value = strCompany;
				cm.Parameters.Add("@TRACKNO", SqlDbType.VarChar).Value = FullOrderNumber;
				cm.Parameters.Add("@OrderTrackNo", SqlDbType.VarChar).Value = FullOrderNumber + FullOrderNumber;
				cm.Parameters.Add("@FullOrderNo", SqlDbType.VarChar).Value = FullOrderNumber;
				cm.Parameters.Add("@Admin", SqlDbType.Bit).Value = 1;

				SqlParameter d = new SqlParameter();
				d.ParameterName = "@Result";
				d.SqlDbType = SqlDbType.Int;
				d.Value = resultid;
				d.Direction = ParameterDirection.Output;
				cm.Parameters.Add(d);

				SqlParameter mmm = new SqlParameter();
				mmm.ParameterName = "@IsEmail";
				mmm.SqlDbType = SqlDbType.Int;
				mmm.Value = new int();
				mmm.Direction = ParameterDirection.Output;
				cm.Parameters.Add(mmm);

				SqlParameter jjj = new SqlParameter();
				jjj.ParameterName = "@CustEmail";
				jjj.SqlDbType = SqlDbType.VarChar;
				jjj.Size = 50;
				jjj.Value = string.Empty;
				jjj.Direction = ParameterDirection.Output;
				cm.Parameters.Add(jjj);

				SqlParameter kkk = new SqlParameter();
				kkk.ParameterName = "@CustName";
				kkk.SqlDbType = SqlDbType.VarChar;
				kkk.Size = 32;
				kkk.Value = string.Empty;
				kkk.Direction = ParameterDirection.Output;
				cm.Parameters.Add(kkk);

				SqlParameter xxx = new SqlParameter();
				xxx.ParameterName = "@Division";
				xxx.SqlDbType = SqlDbType.VarChar;
				xxx.Size = 4;
				xxx.Value = string.Empty;
				xxx.Direction = ParameterDirection.Output;
				cm.Parameters.Add(xxx);

				SqlParameter rrr = new SqlParameter();
				rrr.ParameterName = "@Street";
				rrr.SqlDbType = SqlDbType.VarChar;
				rrr.Size = 30;
				rrr.Value = string.Empty;
				rrr.Direction = ParameterDirection.Output;
				cm.Parameters.Add(rrr);

				SqlParameter ttt = new SqlParameter();
				ttt.ParameterName = "@City";
				ttt.SqlDbType = SqlDbType.VarChar;
				ttt.Size = 30;
				ttt.Value = string.Empty;
				ttt.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ttt);

				SqlParameter uuu = new SqlParameter();
				uuu.ParameterName = "@State";
				uuu.SqlDbType = SqlDbType.VarChar;
				uuu.Size = 2;
				uuu.Value = string.Empty;
				uuu.Direction = ParameterDirection.Output;
				cm.Parameters.Add(uuu);

				SqlParameter vvv = new SqlParameter();
				vvv.ParameterName = "@Zip";
				vvv.SqlDbType = SqlDbType.VarChar;
				vvv.Size = 14;
				vvv.Value = string.Empty;
				vvv.Direction = ParameterDirection.Output;
				cm.Parameters.Add(vvv);

				SqlParameter www = new SqlParameter();
				www.ParameterName = "@StoreName";
				www.SqlDbType = SqlDbType.VarChar;
				www.Size = 50;
				www.Value = string.Empty;
				www.Direction = ParameterDirection.Output;
				cm.Parameters.Add(www);

				SqlParameter yyy = new SqlParameter();
				yyy.ParameterName = "@StrAddress";
				yyy.SqlDbType = SqlDbType.VarChar;
				yyy.Size = 105;
				yyy.Value = string.Empty;
				yyy.Direction = ParameterDirection.Output;
				cm.Parameters.Add(yyy);

				SqlParameter ppp = new SqlParameter();
				ppp.ParameterName = "@StrCity";
				ppp.SqlDbType = SqlDbType.VarChar;
				ppp.Size = 35;
				ppp.Value = string.Empty;
				ppp.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ppp);

				SqlParameter bbb = new SqlParameter();
				bbb.ParameterName = "@StrState";
				bbb.SqlDbType = SqlDbType.VarChar;
				bbb.Size = 2;
				bbb.Value = string.Empty;
				bbb.Direction = ParameterDirection.Output;
				cm.Parameters.Add(bbb);

				SqlParameter ooo = new SqlParameter();
				ooo.ParameterName = "@StrZip";
				ooo.SqlDbType = SqlDbType.VarChar;
				ooo.Size = 35;
				ooo.Value = string.Empty;
				ooo.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ooo);

				SqlParameter zzz = new SqlParameter();
				zzz.ParameterName = "@StrPhoneNo";
				zzz.SqlDbType = SqlDbType.VarChar;
				zzz.Size = 16;
				zzz.Value = string.Empty;
				zzz.Direction = ParameterDirection.Output;
				cm.Parameters.Add(zzz);

				SqlParameter fff = new SqlParameter();
				fff.ParameterName = "@StoreNum";
				fff.SqlDbType = SqlDbType.VarChar;
				fff.Size = 4;
				fff.Value = string.Empty;
				fff.Direction = ParameterDirection.Output;
				cm.Parameters.Add(fff);

				cm.ExecuteNonQuery();

				resultid = Convert.ToInt16(cm.Parameters["@Result"].Value);

				switch (resultid)
				{
					case 1:
						return "Order number " + FullOrderNumber + " successful received.\n";
						break;
					case 2:
						return "Tracking number " + FullOrderNumber + " has already been received.\n";
						break;
					case 3:
						return "Order number " + FullOrderNumber + " has already been received multiple times.\n";
						break;
					case 5:
						return "No package sent for Tracking number " + FullOrderNumber + ".\n";
						break;
					case 6:
						return "Tracking number " + FullOrderNumber + " received. (Partial order)\n";
						break;
					default:
						return "There was an error while entering Tracking number " + FullOrderNumber + "into the database.\n";
				}
			}
			catch (Exception ex)
			{
				return "There was an error while entering Tracking number " + FullOrderNumber + "into the database: " + ex.Message;
			}
			finally
			{
				cn.Close();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="FullOrderNumber"></param>
		/// <returns></returns>
		private string ForcedCheckout(string FullOrderNumber)
		{
			return string.Empty;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="FullOrderNumber"></param>
		/// <returns></returns>
		private string SendReminderEmail(string FullOrderNumber)
		{
			double dblProdAmt = 0;
			double dblTax = 0;
			double dblShipping = 0;
			double dblTotPaid = 0;

			try
			{
				OrderItemGroup theOrdItmEmailGroup = OrderItemGroup.GetOrderItemForEmail(FullOrderNumber);
				string strItemDetail = "<table class='inner' border='1' cellpadding='0' cellspacing='0'><tr><td width='20'>ShipQTY</td><td width='20'>CancelQTY</td><td width='400'>DESCRIPTION</td><td>AMOUNT</td></tr>";

				foreach (OneOrderItem oneItem in theOrdItmEmailGroup)
				{
					strItemDetail += "<tr><td width='20' align='center'>" + oneItem.StrQtyShp + "</td><td width='20' align='center'>" + oneItem.StrQtyCancel + "</td><td width='400'>" + oneItem.StrItemDESC + "</td><td align='right'>" + double.Parse(oneItem.StrSprice).ToString("0.00") + "</td></tr>";
					dblProdAmt = double.Parse(oneItem.StrTotSprice);
					dblTax = double.Parse(oneItem.StrCprice);
				}

				string source = Data.GetSourceType(FullOrderNumber);
				if (source.Substring(0, 2) == "PS" || source.Substring(0, 2) == "EC")
				{
					dblShipping = double.Parse(Data.GetShippingCost(FullOrderNumber));
				}
				else
				{
					dblShipping = Double.Parse(ConfigurationSettings.AppSettings["ShipmentFee"]);
				}
				dblTotPaid = dblProdAmt + dblTax + dblShipping;
				strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> PRODUCT:</td><td align='right'>" + dblProdAmt.ToString("0.00") + "</td></tr>";      //dblProdAmt.ToString("0.00") + "\n";
				strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> TAX:</td><td align='right'>" + dblTax.ToString("0.00") + "</td></tr>";
				strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> SHIPPING AND HANDLING:</td><td align='right'>" + dblShipping.ToString("0.00") + "</td></tr>";
				strItemDetail += "<tr><td width='20'></td><td width='20'></td><td align='right'> *TOTAL PAID:</td><td align='right'>" + dblTotPaid.ToString("0.00") + "</td></tr></table>";
				
				Dictionary<string, string> emailinfo = GetOrdShp4RemEmail(FullOrderNumber);

				DataTable dtOrderPikUp = Data.GetOrdPikUpStatus(FullOrderNumber.Substring(0, 8));

				int nCountFullOrdNo = Utility.CountFullOrdNos(dtOrderPikUp);

				string adminMail = ConfigurationSettings.AppSettings["EmailRec"].ToString();
				if (adminMail != "")
				{
					emailinfo["strCustEmail"] = adminMail; //use for test not to send email to customer
				}
				if (emailinfo["strCustEmail"] != "")
				{
					string EmailSubject = string.Empty;
					string EmailTemplate = string.Empty;

					if (nCountFullOrdNo == 1)
					{
						EmailSubject = ConfigurationSettings.AppSettings["ReceiveSubject"];
						EmailTemplate = ConfigurationSettings.AppSettings["ReceiveTemplate"];
					}
					else if (nCountFullOrdNo > 1) //send patial order received email
					{
						EmailSubject = ConfigurationSettings.AppSettings["PartialSubject"];
						EmailTemplate = ConfigurationSettings.AppSettings["PartialTemplate"];
					}

					string EmailBody = System.IO.File.ReadAllText(EmailTemplate);
					//title
					EmailBody = EmailBody.Replace("xCustNamex", emailinfo["strCustName"]);
					EmailBody = EmailBody.Replace("xOrderNumberx", FullOrderNumber);
					EmailBody = EmailBody.Replace("xStoreNamex", emailinfo["strStrName"]);
					//address
					EmailBody = EmailBody.Replace("xStoreStreetx", emailinfo["strStrAddr"]);
					EmailBody = EmailBody.Replace("xCustStreetx", emailinfo["strCustStreet"]);
					EmailBody = EmailBody.Replace("xStoreAddressx", emailinfo["strStrCity"] + ", " + emailinfo["strStrState"] + " " + emailinfo["strStrZip"]);
					EmailBody = EmailBody.Replace("xCustAddressx", emailinfo["strCustCity"] + ", " + emailinfo["strCustState"] + " " + emailinfo["strCustZip"]);
					//detail
					EmailBody = EmailBody.Replace("xOrderDetailx", strItemDetail);

					Emails.SendText(emailinfo["strCustEmail"], EmailSubject, EmailBody);

					return "Email Reminder for Full Order number " + FullOrderNumber.ToString() + " sent to Customer successfully.\n";
				}
				return string.Empty;
			}
			catch (Exception ex)
			{
				return "Email was not sent because the following error occurred: " + ex.Message;
			}
		}


		private string UndoAbandon(string FullOrderNumber)
		{
			SqlConnection cn = null;
			try
			{
				cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
				SqlCommand cm = new SqlCommand("dbo.AbandonedOrders_Delete", cn);
				cm.CommandType = CommandType.StoredProcedure;


				cm.Parameters.Add(new SqlParameter("@FullOrderNumber", FullOrderNumber));

				cn.Open();
				cm.ExecuteNonQuery();
				return string.Format("Order Number {0} was successfully removed from abandonment.", FullOrderNumber);
			}
			catch (Exception ex)
			{
				return "There was an error while trying to delete abandon record from the database: " + ex.Message;
			}
			finally
			{
				cn.Close();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="strFullOrderNo"></param>
		/// <returns></returns>
		private Dictionary<string, string> GetOrdShp4RemEmail(string strFullOrderNo)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();
			int intResultShp = 0;
			Dictionary<string, string> results = new Dictionary<string, string>();

			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				cm.CommandText = "usp_RemnidEmlOrdInfo_ByOrdNo";
				cm.CommandTimeout = 90;

				cm.Parameters.Clear();

				SqlParameter qqq = new SqlParameter();
				qqq.ParameterName = "@FULLORDERNO";
				qqq.SqlDbType = SqlDbType.VarChar;
				qqq.Value = strFullOrderNo;
				cm.Parameters.Add(qqq);

				SqlParameter sss = new SqlParameter();
				sss.ParameterName = "@Result";
				sss.SqlDbType = SqlDbType.Int;
				sss.Value = intResultShp;
				sss.Direction = ParameterDirection.Output;
				cm.Parameters.Add(sss);

				SqlParameter xxx = new SqlParameter();
				xxx.ParameterName = "@Division";
				xxx.SqlDbType = SqlDbType.VarChar;
				xxx.Size = 4;
				xxx.Value = string.Empty;
				xxx.Direction = ParameterDirection.Output;
				cm.Parameters.Add(xxx);

				SqlParameter aaa = new SqlParameter();
				aaa.ParameterName = "@CustName";
				aaa.SqlDbType = SqlDbType.VarChar;
				aaa.Size = 32;
				aaa.Value = string.Empty;
				aaa.Direction = ParameterDirection.Output;
				cm.Parameters.Add(aaa);

				SqlParameter bbb = new SqlParameter();
				bbb.ParameterName = "@CustEmail";
				bbb.SqlDbType = SqlDbType.VarChar;
				bbb.Size = 50;
				bbb.Value = string.Empty;
				bbb.Direction = ParameterDirection.Output;
				cm.Parameters.Add(bbb);

				SqlParameter rrr = new SqlParameter();
				rrr.ParameterName = "@Street";
				rrr.SqlDbType = SqlDbType.VarChar;
				rrr.Size = 30;
				rrr.Value = string.Empty;
				rrr.Direction = ParameterDirection.Output;
				cm.Parameters.Add(rrr);

				SqlParameter ttt = new SqlParameter();
				ttt.ParameterName = "@City";
				ttt.SqlDbType = SqlDbType.VarChar;
				ttt.Size = 30;
				ttt.Value = string.Empty;
				ttt.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ttt);

				SqlParameter uuu = new SqlParameter();
				uuu.ParameterName = "@State";
				uuu.SqlDbType = SqlDbType.VarChar;
				uuu.Size = 2;
				uuu.Value = string.Empty;
				uuu.Direction = ParameterDirection.Output;
				cm.Parameters.Add(uuu);

				SqlParameter vvv = new SqlParameter();
				vvv.ParameterName = "@Zip";
				vvv.SqlDbType = SqlDbType.VarChar;
				vvv.Size = 14;
				vvv.Value = string.Empty;
				vvv.Direction = ParameterDirection.Output;
				cm.Parameters.Add(vvv);

				SqlParameter www = new SqlParameter();
				www.ParameterName = "@StoreName";
				www.SqlDbType = SqlDbType.VarChar;
				www.Size = 50;
				www.Value = string.Empty;
				www.Direction = ParameterDirection.Output;
				cm.Parameters.Add(www);

				SqlParameter yyy = new SqlParameter();
				yyy.ParameterName = "@StrAddress";
				yyy.SqlDbType = SqlDbType.VarChar;
				yyy.Size = 105;
				yyy.Value = string.Empty;
				yyy.Direction = ParameterDirection.Output;
				cm.Parameters.Add(yyy);

				SqlParameter ppp = new SqlParameter();
				ppp.ParameterName = "@StrCity";
				ppp.SqlDbType = SqlDbType.VarChar;
				ppp.Size = 35;
				ppp.Value = string.Empty;
				ppp.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ppp);

				SqlParameter lll = new SqlParameter();
				lll.ParameterName = "@StrState";
				lll.SqlDbType = SqlDbType.VarChar;
				lll.Size = 2;
				lll.Value = string.Empty;
				lll.Direction = ParameterDirection.Output;
				cm.Parameters.Add(lll);

				SqlParameter ooo = new SqlParameter();
				ooo.ParameterName = "@StrZip";
				ooo.SqlDbType = SqlDbType.VarChar;
				ooo.Size = 35;
				ooo.Value = string.Empty;
				ooo.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ooo);

				cm.ExecuteNonQuery();

				intResultShp = Convert.ToInt16(cm.Parameters["@Result"].Value);
				if (intResultShp == 1)
				{
					results.Add("strDivision", Convert.ToString(cm.Parameters["@Division"].Value));
					results.Add("strCustEmail", Convert.ToString(cm.Parameters["@CustEmail"].Value));
					results.Add("strCustName", Convert.ToString(cm.Parameters["@CustName"].Value));
					results.Add("strCustStreet", Convert.ToString(cm.Parameters["@Street"].Value));
					results.Add("strCustCity", Convert.ToString(cm.Parameters["@City"].Value));
					results.Add("strCustState", Convert.ToString(cm.Parameters["@State"].Value));
					results.Add("strCustZip", Convert.ToString(cm.Parameters["@Zip"].Value));
					results.Add("strStrName", Convert.ToString(cm.Parameters["@StoreName"].Value));
					results.Add("strStrAddr", Convert.ToString(cm.Parameters["@StrAddress"].Value));
					results.Add("strStrCity", Convert.ToString(cm.Parameters["@StrCity"].Value));
					results.Add("strStrState", Convert.ToString(cm.Parameters["@StrState"].Value));
					results.Add("strStrZip", Convert.ToString(cm.Parameters["@StrZip"].Value));
				}
			}
			catch (SqlException)
			{
				throw;
			}
			finally
			{
				cn.Close();
			}
			return results;
		}
		#endregion
	}
}