using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace ShipToStore
{
	/// <summary>
	/// Summary description for Login.
	/// </summary>
	public class Login : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtEmpNum;
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected System.Web.UI.WebControls.RequiredFieldValidator valEmpID;
		protected System.Web.UI.WebControls.Label lblOut;
		protected System.Web.UI.WebControls.LinkButton LinkButton1;
		protected System.Web.UI.WebControls.TextBox Textbox1;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
	  
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			
			//this.txtEmpNum.Attributes.Add("OnKeyPress", "javascript:if (event.keyCode == 13)__doPostBack('" + btnSubmit.UniqueID + "','');"); 
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{

			//get employee ID from the textbox and get profile from zombie
			string empID = this.txtEmpNum.Text.Trim();
			if (empID!="")
			{
				int intUserFlag = 0;
				DataTable dtUser = null;
				//add LP logic 
				intUserFlag = AppUser.GetUserFlag(empID); 
				  
				if (intUserFlag ==1)  //LP user
				{
				    dtUser = AppUser.GetLPUserProfile(empID);
				}
				else if (intUserFlag ==0)
				{
					dtUser = AppUser.GetUserProfile(empID);

				}
				else if (intUserFlag >1) 
				{
                    dtUser = new DataTable ();
                    this.valEmpID.IsValid = false;
					this.valEmpID.ErrorMessage="The employeeID is duplicated on LP table.";
				}

				//DataTable dtUser = AppUser.GetUserProfile(empID);
				//if (dtUser!=null)
				if (dtUser.Rows.Count > 0) 
				{
					string strDepartment = dtUser.Rows[0]["Dept"].ToString().Trim();
					string strJobCode = dtUser.Rows[0]["JobCode"].ToString().Trim();
					string strLocation = dtUser.Rows[0]["Location"].ToString().Trim();
					string strEmployeeID = dtUser.Rows[0]["EmployeeID"].ToString().Trim();
					string strRegion = null;
					int intPermit = AppUser.SetUserPermission(strLocation, strJobCode);
					if (intPermit!=0)
					{
						if (intPermit==3)
						{
							strRegion= Data.GetRegion(Utility.ParseDepartment(strDepartment));						

						}
						else if (intPermit==2)
						{

							strRegion = strDepartment.Substring(2,2);
						}
					    
						else
						{
							strRegion = "00";
						}
						Session.Add("ViewPermission", intPermit); 
						Session.Add("UserProfile", dtUser);
						Session.Add("UserRegion", strRegion);

						Data.TrackEmployeeLogin(strEmployeeID, strLocation, "I");

						if (intPermit == 4)
						{
							Page.Response.Redirect("./StoreActionOption.aspx");
						}
						else
						{
							Page.Response.Redirect("./OrderSearch.aspx");
							//if (intPermit == 6)
							//{
							//    Page.Response.Redirect("./OrderSearch_LP.aspx");
							//}
							//else
							//{
							//    Page.Response.Redirect("./OrderSearch.aspx");
							//}
						}
					}
					else
					{
						//print out error message - not enough permission
						this.valEmpID.IsValid = false;
						this.valEmpID.ErrorMessage="Access denied.";

					}

				
				}
				else
				{
					//print out error message - not enough permission
					this.valEmpID.IsValid = false;
					this.valEmpID.ErrorMessage="Invalid Employee#, Access denied.";

				}

			}
		
		}

		

		private void LinkButton2_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}
