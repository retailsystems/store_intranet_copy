<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page language="c#" Codebehind="Login.aspx.cs" AutoEventWireup="false" Inherits="ShipToStore.Login" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OrderSearch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" topMargin="0" MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header></div>
			<div id="main" class="height: 540px;">
				<br>
				<TABLE id="tblLogin" class="innerTableLog" cellSpacing="0" cellPadding="1" width="98%"
					height="450" align="center" border="0">
					<tr>
						<td>
							<table width="50%" align="center" border="0" cellSpacing="0" cellPadding="1">
								<TBODY>
									<tr class="tableCell">
										<td colspan="3">
											<asp:Label id="lblOut" runat="server" Visible="False">You have just logged out. </asp:Label>
											To proceed enter in your employee number below.
											<asp:RequiredFieldValidator id="valEmpID" runat="server" ErrorMessage="You must input your employee ID." Display="Dynamic"
												ControlToValidate="txtEmpNum"></asp:RequiredFieldValidator></td>
									</tr>
									<tr class="tableCell">
										<td class="tableTitle">
											Employee #:
										</td>
										<td>
											<asp:TextBox id="txtEmpNum" runat="server" Columns="20" CssClass="tableInputWhite" MaxLength="6"></asp:TextBox><asp:TextBox runat="server" style="DISPLAY:none;VISIBILITY:hidden" ID="Textbox1" NAME="Textbox1" /></td>
										<td>
											<asp:Button id="btnSubmit" runat="server" Text="SUBMIT" CssClass="buttonRed"></asp:Button></td>
						
						</tr></table>
						</td>
					</tr>
			
				</TABLE>
			
				<br>
				<table height="50" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0">
					<tr>
						<td vAlign="top" align="right"></td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</HTML>
