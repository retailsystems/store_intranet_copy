<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Header.ascx.cs" Inherits="ShipToStore.Header" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<table id="headerTable" width="98%" height="90" border="0" align="center" bgcolor="white">
	<tr>
		<td align="left" valign="middle" width="60%">
			<asp:Image id="Image1" runat="server" ImageUrl="Image/htlogo.jpg"></asp:Image>
		</td>
		<td align="right" valign="middle">
			<input type="button" id="btnAdmin" runat="server" class="buttonRed" value="ADMIN" visible="false" onserverclick="btnAdmin_ServerClick">
			<input type="button" id="btnMenu" runat="server" class="buttonRed" value="STORE MENU" visible="False" onserverclick="btnMenu_ServerClick">&nbsp;
			<input type="button" id="btnReSearch" runat="server" class="buttonRed" value="SEARCH MENU">
		</td>
	</tr>
	<tr>
		<td align="left" class="tableTitle" valign="middle" width="60%">
			ORDER TRACKING SYSTEM
		</td>
		<td align="right" valign="middle" class="tableTitle">
			<asp:Label id="lblPermit" runat="server"></asp:Label>&nbsp;
			<asp:LinkButton id="lnkLogout" Text="Log out" runat="server"></asp:LinkButton>
		</td>
	</tr>
</table>
