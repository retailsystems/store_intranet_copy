using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;


namespace ShipToStore
{
	/// <summary>
	/// Summary description for CustPickUp.
	/// </summary>
	public class CustPickUp : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.TextBox txtNotes;
		//protected System.Web.UI.WebControls.Button btnPickedUp;
		protected System.Web.UI.HtmlControls.HtmlInputImage btnPickUp;
		//protected System.Web.UI.WebControls.Image btnPickedUp;
		string strFullOrderNumber = string.Empty;
		string strOrderNumber = string.Empty;
		protected System.Web.UI.WebControls.ListBox lstTrackView;
        protected DataTable dtTrackNos;
		protected DataTable newDtTrackNos;

		string strFullOrderNo =  string.Empty;
		string strCustEmail =  string.Empty;
		string strDivision = string.Empty;
		int intIsEmail = 0;
		string strCustName =  string.Empty;
		string strCustStreet = string.Empty;
		string strCustCity = string.Empty;
		string strCustState = string.Empty;
		string strCustZip = string.Empty;
		string strStrName = string.Empty;
		string strStrAddr = string.Empty;
		string strStrCity = string.Empty;
		string strStrState = string.Empty;
		string strStrZip = string.Empty;
		protected OrderItemGroup newOrderItmGroup;
		protected OrderItemGroup theOrderItmGroup; 
		string strHTEmailBody = string.Empty;
		string strItemDetail = string.Empty;
		string strSHEmailBody = string.Empty;
		string strHTEmailLogo = ConfigurationSettings.AppSettings["HTEmailLogo"];
		string strSHEmailLogo = ConfigurationSettings.AppSettings["SHEmailLogo"];
		string strHTEmlBotImage = ConfigurationSettings.AppSettings["HTEmlBotImage"];
		string strSHEmlBotImage = ConfigurationSettings.AppSettings["SHEmlBotImage"];
		double dblProdAmt = 0.00;
		double dblTax = 0.00;
		double dblTotPaid = 0.00;
		StringBuilder strHTEmailBodyBl = new StringBuilder();
		string strErrorMsg = string.Empty;
        protected System.Web.UI.WebControls.Label lblErrorText;
		protected System.Web.UI.WebControls.Button btnPickedUp;
		DataTable dtUser;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
						
			strFullOrderNo= Request.QueryString["strOrderNo"];
			
			if (strFullOrderNo!="")
			{
				//ViewState["strFullOrderNo"] = strFullOrderNo;
				strOrderNumber = strFullOrderNo.Substring (0,8).ToString();
                newDtTrackNos = Data.GetTrackNos(strOrderNumber);
			}
			
			//lblStatus.Text = theOneOrder.StrStatus.ToString();
			lstTrackView.DataSource = newDtTrackNos;
			lstTrackView.DataTextField = "TRACKNO";
			lstTrackView.DataValueField = "TRACKNO";
										
			lstTrackView.DataBind();
			if (lstTrackView.Items.Count>0)
			{
				lstTrackView.SelectedIndex=0;

			}

			//txtNotes.Text  =Data.GetNote(strOrderNumber).ToString();
			if(strFullOrderNo =="")
			{
				strErrorMsg += " There is no TrackNo for this order. Order status could not be changed to Customer Picked Up.";
				ShowError(strErrorMsg); 
			}
		}

		//		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnPickedUp.Click += new System.EventHandler(this.btnPickedUp_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		private void btnPickedUp_Click(object sender, System.EventArgs e)
		{
			//			string a = strOrderNumber; 
//			try
//			{
				

				//			string strTrackNo = dtTrackNos.Rows[0]["TRACKNO"].ToString().Trim();
				//			string strOrderTrackNo = strFullOrderNumber +  strTrackNo; 
				//			dbResult = InsertPickUpOrderTrack(strTrackNo,strOrderTrackNo,strFullOrderNumber);
				//			dtTrackNos = Data.GetTrackNoByFullOrdNo(strFullOrderNumber);
				//			if (dtTrackNos != null)
//			}
			try
			{
				int dbResult = 0;

				dtUser = (DataTable)Session["UserProfile"];
				string strEmployeeID = dtUser.Rows[0]["EmployeeID"].ToString().Trim();
				string strStoreNum = dtUser.Rows[0]["Location"].ToString().Trim();

				dtTrackNos = Data.GetTrackNoByFullOrdNo(strFullOrderNo);

					if (dtTrackNos.Rows.Count > 0)
					{
//						foreach (DataRow ordRow in dtOrders.Rows) 
//						{
						
							string strTrackNo = dtTrackNos.Rows[0]["TRACKNO"].ToString().Trim();
							
							string strOrderTrackNo = strFullOrderNo +  strTrackNo;
							dbResult = InsertPickUpOrderTrack(strTrackNo,strOrderTrackNo,strFullOrderNo);

							if (dbResult ==1)
							{
								if (intIsEmail ==1)
								{
									if ((strCustEmail != "") && (strStrName!= "") && (strCustStreet!= ""))
									{
										theOrderItmGroup = GetOrderItemsForPKEmail(strFullOrderNo);
										//strItemDetail =  "ShipQTY" + " " + "CancelQTY"  + "\t" + "DESCRIPTION" + "                                            "+ "AMOUNT" + "\n";
										strItemDetail =  "<table border= 1 cellpadding='0' cellspacing='0'><tr><td width = 20>ShipQTY</td><td width = 20>CancelQTY</td><td width = 400>DESCRIPTION</td><td>AMOUNT</td></tr>";
                    
										foreach (OneOrderItem oneItem in theOrderItmGroup)
										{
											//strHTItemDetail +=  oneItem.StrQtyShp + "       " + oneItem.StrQtyCancel  + "\t" + "\t"+ oneItem.StrItemDESC + "      " + oneItem.StrSprice + "\n";
											strItemDetail +=  "<tr><td width = 20 align = center>" + oneItem.StrQtyShp + "</td><td width = 20 align = center>" + oneItem.StrQtyCancel  + "</td><td width = 400>"+ oneItem.StrItemDESC + "</td><td Align = right>" + double.Parse(oneItem.StrSprice).ToString("0.00")  + "</td></tr>";
									
											//strItemDetail +=  oneItem.StrQtyShp + "       " + oneItem.StrQtyCancel  + "\t" + "\t"+ oneItem.StrItemDESC + "     " + double.Parse(oneItem.StrSprice).ToString("0.00")  + "\n";
											dblProdAmt = double.Parse(oneItem.StrTotSprice);
											dblTax = double.Parse(oneItem.StrCprice);
										}
										dblTotPaid = dblProdAmt + dblTax + 1.00;
										strItemDetail += "<tr><td width = 20></td><td width = 20></td><td Align = right> PRODUCT:</td><td Align = right>"  + dblProdAmt.ToString("0.00")+ "</td></tr>";      //dblProdAmt.ToString("0.00") + "\n";
										strItemDetail += "<tr><td width = 20></td><td width = 20></td><td Align = right> TAX:" + "</td><td Align = right>" + dblTax.ToString("0.00") + "</td></tr>";
										strItemDetail += "<tr><td width = 20></td><td width = 20></td><td Align = right> SHIPPING AND HANDLING:" + "</td><td Align = right>" + "1.00" + "</td></tr>";
										strItemDetail += "<tr><td width = 20></td><td width = 20></td><td Align = right> *TOTAL PAID:" + "</td><td Align = right>" + dblTotPaid.ToString("0.00")+ "</td></tr></table>";
										//prod need change
                                        //DO COMMENT
										strCustEmail = ConfigurationSettings.AppSettings["EmailRec"].ToString();
  									//string strCustEmail1 = ConfigurationSettings.AppSettings["EmailRec1"].ToString();
//										string strCustEmail2 = ConfigurationSettings.AppSettings["EmailRec2"].ToString();
//										
										if (strDivision =="1")
										{
											//Emails.SendText(strCustEmail, "HotTopic Order Picked Up for cust email " + strCustEmail, GetHTEmailBody(strFullOrderNo));
											Emails.SendText(strCustEmail, "HotTopic Order Picked Up", GetHTEmailBody(strFullOrderNo));
									
											//Emails.SendText(strCustEmail2, "HotTopic Order Picked Up", GetHTEmailBody(strFullOrderNo));
											
										}
										else 
										{
											Emails.SendText(strCustEmail, "Torrid Order Picked Up", GetSHEmailBody(strFullOrderNo));
											//Emails.SendText(strCustEmail1, "ShockHound Order Picked Up" + strCustEmail, GetSHEmailBody(strFullOrderNo));
//											Emails.SendText(strCustEmail2, "ShockHound Order Picked Up", GetSHEmailBody(strFullOrderNo));
										}
										strErrorMsg += "Tracking No #" + strTrackNo.ToString () +" successful picked up. " + "\n";
										ShowError(strErrorMsg); 
										//track store employee scan box time for picked up status. 
										Data.TrackEmployeeScan(strTrackNo.ToString (), strFullOrderNo,strEmployeeID,strStoreNum,"P");
									}
									else 
									{
										strErrorMsg += "Could not get enough customer and store info to send email out from database;" + "<br>";
										ShowError(strErrorMsg); 
									}
								}
							}
							else if (dbResult ==2) //DUPLICATE, entry already
							{ 
								strErrorMsg += "Tracking #" + strTrackNo.ToString () +" has already been picked up;" + "<br>";
								ShowError(strErrorMsg); 
							}
							else if (dbResult ==5) //not receive in store
							{
								strErrorMsg += "Tracking #" + strTrackNo.ToString ()+" has not been received at store yet;"+ "<br>";
								ShowError(strErrorMsg); 
							}
							else if (dbResult ==6) //not complete receive in store
							{
								strErrorMsg += "Tracking No #" + strTrackNo.ToString () +" Picked Up. (Partial order) " + "\n";
						
								ShowError(strErrorMsg); 
							}
								//							else if (dbResult ==4) //can not find order due to order did not receive in store
								//							{
								//								strErrorMsg += "Tracking #" + arrlist[i].ToString()+" has not been checked in at store yet; "+ "<br>";
								//								ShowError(strErrorMsg); 
								//							}
							else 
							{
								strErrorMsg += " There was an error while entering Tracking No # "+ strTrackNo.ToString ()+  "into DB;" + "\n";
								ShowError(strErrorMsg); 
							}
//						}
					}
					else 
					{
						strErrorMsg += "Tracking No does not exist for FullOrder No #" + strFullOrderNumber.ToString()+";"+ "\n";
						ShowError(strErrorMsg); 
					}
				
				
			}
			catch(Exception ex)
			{
				strErrorMsg += "One TrackingNo could not be found. Please contact tech support to change order status to Customer Picked Up." + "<br>";
				ShowError(ex.Message);
				//Response.Redirect("~/Error.aspx");
			}
		
		}
		private void ShowError(string strErrMsg)
		{
			if (strErrMsg.Trim()!="")
			{
				lblErrorText.Text = strErrMsg;
				lblErrorText.Visible= true;
			}
		}

		private string GetHTEmailBody(string strFullOrderNo)
		{
			strHTEmailBody = "<table border =0><tr><td colspan=4><img src='" + strHTEmailLogo + "'></td></tr><tr><td  colspan=4>Hi " + strCustName + ": </td></tr>" 
				+ "<tr><td colspan=4>Thanks for shopping with us!  We see that you�ve picked up your in-store kiosk/POS web order "+ strFullOrderNo.Substring(0,8) +".</td></tr>"
				+ "<tr><td colspan=4>We really appreciate your business and hope you come back and shop with us again soon!</td></tr><tr><td colspan = 4></td></tr><tr><td colspan=4>&nbsp;</td></tr>"; 

			strHTEmailBody += strItemDetail;
			strHTEmailBody += strHTEmailBodyBl;
			strHTEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4>*If you paid with a credit card, the total paid amount was charged to your card at the time your order was shipped.</td></tr>";
			strHTEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan = 4>Thanks! </td></tr><tr><td colspan=4></td><tr><tr><td td colspan=4>" + "Hottopic.com</td></tr>" + "<tr><td td colspan=4>Customer Service</td></tr>" + "<tr><td td colspan=4><a href='http://community.hottopic.com/contact/customer_service'>http://community.hottopic.com/contact/customer_service</a></td></tr>";
			//			strHTEmailBody += "<tr><td colspan=4><img src='http://localhost/Home/ShipToStore_4/Image/HTEmlBotImg.gif'></td></tr></table>";
			strHTEmailBody += "<tr><td colspan=4><img src='" + strHTEmlBotImage + "'></td></tr></table>";
						
			return strHTEmailBody;
		}
 
		private string GetSHEmailBody(string strFullOrderNo)
		{
			strSHEmailBody = "<table border =0><tr><td colspan=4><img src='" + strHTEmailLogo + "'></td></tr><tr><td  colspan=4>Hi " + strCustName + ": </td></tr>" 
				+ "<tr><td colspan=4>Thanks for shopping with us.  We see that you have picked up your order "+ strFullOrderNo.Substring(0,8) +" that you have placed on our POS.</td></tr>"
				+ "<tr><td colspan=4>We appreciate your business and would love to have you as a regular customer. </td></tr><tr><td colspan=4>&nbsp;</td></tr><tr><td colspan=4>&nbsp;</td></tr>";

			strSHEmailBody += strItemDetail;
			strSHEmailBody += strHTEmailBodyBl;
			strSHEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4>Thanks again for shopping on Torrid!</td></tr><tr><td td colspan=4></td></tr>";
            strSHEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4>Torrid Customer Service </td></tr><tr><td td colspan=4>" + "Torrid is all about the fans; let us know what you think. If you ever need Torrid customer </td></tr>" + "<tr><td td colspan=4>service, contact us at 866-867-7431(US & Canada), or email us by going to <a href='http://torrid.com'>http://torrid.com </a> </td></tr>";
		//	strSHEmailBody += "<tr><td colspan=4></td></tr><tr><td colspan=4><img src='" + strSHEmlBotImage + "'></td></tr></table>";
		
			return strSHEmailBody;
		}

	

		private OrderItemGroup GetOrderItemsForPKEmail(string strFullOrderNo)
		{
			newOrderItmGroup = OrderItemGroup.GetOrderItemForEmail(strFullOrderNo);	
			return newOrderItmGroup;
		}
		 
		private int InsertPickUpOrderTrack(string strTrackNo,string strOrderTrackNo, string strFullOrderNo)
		{
			SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["strKioskSQLConnection"]);
			SqlCommand cm = new SqlCommand();
			int intResultRec = 0;

			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
				cm.CommandText = "usp_CustPickUp_Update_ByOrdTrack";
				cm.CommandTimeout=150;
				              
				cm.Parameters.Clear();

				SqlParameter aaa = new SqlParameter();
				aaa.ParameterName = "@OrderTrackNo";
				aaa.SqlDbType = SqlDbType.VarChar;
				aaa.Size=52;
				aaa.Value = strOrderTrackNo;
				cm.Parameters.Add(aaa);

				SqlParameter qqq = new SqlParameter();
				qqq.ParameterName = "@TRACKNO";
				qqq.SqlDbType = SqlDbType.VarChar;
				qqq.Value = strTrackNo;
				cm.Parameters.Add(qqq);

				SqlParameter sss = new SqlParameter();
				sss.ParameterName = "@Result";
				sss.SqlDbType = SqlDbType.Int;
				sss.Value = intResultRec;
				sss.Direction = ParameterDirection.Output;
				cm.Parameters.Add(sss);

				SqlParameter hhh = new SqlParameter();
				hhh.ParameterName = "@FullOrderNo";
				hhh.SqlDbType = SqlDbType.VarChar;
				hhh.Size=12; 
				hhh.Value = strFullOrderNo;
				cm.Parameters.Add(hhh);
		
				SqlParameter jjj = new SqlParameter();
				jjj.ParameterName = "@CustEmail";
				jjj.SqlDbType = SqlDbType.VarChar;
				jjj.Size = 50;
				jjj.Value = strCustEmail;
				jjj.Direction = ParameterDirection.Output;
				cm.Parameters.Add(jjj);

				SqlParameter xxx = new SqlParameter();
				xxx.ParameterName = "@Division";
				xxx.SqlDbType = SqlDbType.VarChar;
				xxx.Size = 4;
				xxx.Value = strDivision;
				xxx.Direction = ParameterDirection.Output;
				cm.Parameters.Add(xxx);

				SqlParameter mmm = new SqlParameter();
				mmm.ParameterName = "@IsEmail";
				mmm.SqlDbType = SqlDbType.Int;
				mmm.Value = intIsEmail;
				mmm.Direction = ParameterDirection.Output;
				cm.Parameters.Add(mmm);

				SqlParameter kkk = new SqlParameter();
				kkk.ParameterName = "@CustName";
				kkk.SqlDbType = SqlDbType.VarChar;
				kkk.Size = 32;
				kkk.Value = strCustName;
				kkk.Direction = ParameterDirection.Output;
				cm.Parameters.Add(kkk);

				SqlParameter rrr = new SqlParameter();
				rrr.ParameterName = "@Street";
				rrr.SqlDbType = SqlDbType.VarChar;
				rrr.Size = 30;
				rrr.Value = strCustStreet;
				rrr.Direction = ParameterDirection.Output;
				cm.Parameters.Add(rrr);

				SqlParameter ttt = new SqlParameter();
				ttt.ParameterName = "@City";
				ttt.SqlDbType = SqlDbType.VarChar;
				ttt.Size = 30;
				ttt.Value = strCustCity;
				ttt.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ttt);

				SqlParameter uuu = new SqlParameter();
				uuu.ParameterName = "@State";
				uuu.SqlDbType = SqlDbType.VarChar;
				uuu.Size = 2;
				uuu.Value = strCustState;
				uuu.Direction = ParameterDirection.Output;
				cm.Parameters.Add(uuu);

				SqlParameter vvv = new SqlParameter();
				vvv.ParameterName = "@Zip";
				vvv.SqlDbType = SqlDbType.VarChar;
				vvv.Size = 14;
				vvv.Value = strCustZip;
				vvv.Direction = ParameterDirection.Output;
				cm.Parameters.Add(vvv);

				SqlParameter www = new SqlParameter();
				www.ParameterName = "@StoreName";
				www.SqlDbType = SqlDbType.VarChar;
				www.Size = 50;
				www.Value = strStrName;
				www.Direction = ParameterDirection.Output;
				cm.Parameters.Add(www);

				SqlParameter yyy = new SqlParameter();
				yyy.ParameterName = "@StrAddress";
				yyy.SqlDbType = SqlDbType.VarChar;
				yyy.Size = 105;
				yyy.Value = strStrAddr;
				yyy.Direction = ParameterDirection.Output;
				cm.Parameters.Add(yyy);

				SqlParameter ppp = new SqlParameter();
				ppp.ParameterName = "@StrCity";
				ppp.SqlDbType = SqlDbType.VarChar;
				ppp.Size = 35;
				ppp.Value = strStrCity;
				ppp.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ppp);

				SqlParameter bbb = new SqlParameter();
				bbb.ParameterName = "@StrState";
				bbb.SqlDbType = SqlDbType.VarChar;
				bbb.Size = 2;
				bbb.Value = strStrState;
				bbb.Direction = ParameterDirection.Output;
				cm.Parameters.Add(bbb);

				SqlParameter ooo = new SqlParameter();
				ooo.ParameterName = "@StrZip";
				ooo.SqlDbType = SqlDbType.VarChar;
				ooo.Size = 35;
				ooo.Value = strStrZip;
				ooo.Direction = ParameterDirection.Output;
				cm.Parameters.Add(ooo);

				cm.ExecuteNonQuery();

				intResultRec = Convert.ToInt32(cm.Parameters["@Result"].Value);
				//				strFullOrderNo = Convert.ToString(cm.Parameters["@FullOrderNo"].Value);
				strCustEmail = Convert.ToString(cm.Parameters["@CustEmail"].Value);
				strDivision = Convert.ToString(cm.Parameters["@Division"].Value);
				intIsEmail = Convert.ToInt32(cm.Parameters["@IsEmail"].Value);
				strCustName = Convert.ToString(cm.Parameters["@CustName"].Value);
				strCustStreet = Convert.ToString(cm.Parameters["@Street"].Value);
				strCustCity = Convert.ToString(cm.Parameters["@City"].Value);
				strCustState = Convert.ToString(cm.Parameters["@State"].Value);
				strCustZip = Convert.ToString(cm.Parameters["@Zip"].Value);
				strStrName = Convert.ToString(cm.Parameters["@StoreName"].Value);
				strStrAddr = Convert.ToString(cm.Parameters["@StrAddress"].Value);
				strStrCity = Convert.ToString(cm.Parameters["@StrCity"].Value);
				strStrState = Convert.ToString(cm.Parameters["@StrState"].Value);
				strStrZip = Convert.ToString(cm.Parameters["@StrZip"].Value);

			}
			catch (SqlException ex)
			{
				intResultRec = 99;
			}
			
			finally
			{
				cn.Close();
			}
			return intResultRec;
		}
			
	
		private void btnClose_Click(object sender, System.EventArgs e)
		{
			Response.Write("<script language='javascript'> { self.close() }</script>");
		}
	}
}
