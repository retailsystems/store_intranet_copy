/****** Object:  StoredProcedure [dbo].[TRN_ORDERSUBVIEW]    Script Date: 07/02/2014 17:50:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Michelle An>
-- Create date: <3/4/2014>
-- 7/2/2014 Apurva Parikh - Change insert to check at the line level
-- Description:	<merge order sub header>
-- =============================================
ALTER PROCEDURE [dbo].[TRN_ORDERSUBVIEW]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
BEGIN TRAN;
  --first to update the orders with new shipment date and product amount and status and source
  
  
 -- Update  FOS
 -- SET 
 
	--FOS.[SHIPDATE]=(CASE WHEN ISDATE(BOS.[SHIPDATE])=0 THEN NULL ELSE CAST(BOS.[SHIPDATE] as Datetime)END) ,
	--FOS.[SHIPMETHOD]=BOS.[SHIPMETHOD],
	--FOS.[TAXRATE]= BOS.[TAXRATE],
	--FOS.LINE_NO = BOS.LINE_NO,
	--FOS.EDP_NO = BOS.EDP_NO,
	--FOS.SKU_NUM = BOS.SKU_NUM,
	--FOS.SKU_DESC = BOS.SKU_DESC,
 --   FOS.QTY = BOS.QTY,
 --   FOS.EXT_PRC = BOS.EXT_PRC,
 --   FOS.EXT_CST = BOS.EXT_CST,
	--FOS.ITM_STATUS = BOS.ITM_STATUS,
 --   FOS.ORD_STATUS = BOS.ORD_STATUS,
	--FOS.[SOURCE] =BOS.[SOURCE] 
 -- FROM FACT_ORDERSUBVIEW  FOS
 -- JOIN BASE_ORDERSUBVIEW  BOS  
 -- ON (FOS.FULLORDERNO = BOS.FULLORDERNO)
  --;
 
  --update orders that later on converted into different a shipment number 0101
  Update  FOS
  SET 
    FOS.FULLORDERNO = BOS.FULLORDERNO,
	FOS.[SHIPDATE]=(CASE WHEN ISDATE(BOS.[SHIPDATE])=0 THEN NULL ELSE CAST(BOS.[SHIPDATE] as Datetime)END) ,
	FOS.[SHIPMETHOD]=BOS.[SHIPMETHOD],
	FOS.[TAXRATE]= BOS.[TAXRATE],
	--FOS.LINE_NO = BOS.LINE_NO,
	FOS.EDP_NO = BOS.EDP_NO,
    --FOS.SKU_NUM = BOS.SKU_NUM,
	FOS.SKU_DESC = BOS.SKU_DESC,
    FOS.QTY = BOS.QTY,
    FOS.EXT_PRC = BOS.EXT_PRC,
    FOS.EXT_CST = BOS.EXT_CST,
	FOS.ITM_STATUS = BOS.ITM_STATUS,
    FOS.ORD_STATUS = BOS.ORD_STATUS,
	FOS.[SOURCE] =BOS.[SOURCE] 
  FROM FACT_ORDERSUBVIEW  FOS
  JOIN BASE_ORDERSUBVIEW  BOS  
  ON (FOS.ORDERNO = BOS.ORDERNO and FOS.LINE_NO = BOS.LINE_NO and FOS.SKU_NUM = BOS.SKU_NUM)
  ;
 
  
  
  --insert the data not found into the fact table

    
        INSERT INTO FACT_ORDERSUBVIEW 
              
        SELECT           
        BOS.ORDERNO,
        BOS.FULLORDERNO,
        CASE WHEN ISDATE(BOS.[SHIPDATE])=0 THEN NULL ELSE CAST(BOS.[SHIPDATE] as Datetime) END ,
	    BOS.[SHIPMETHOD],
	BOS.[TAXRATE],
	BOS.LINE_NO,
	BOS.EDP_NO,
	BOS.SKU_NUM,
	BOS.SKU_DESC,
    BOS.QTY,
    BOS.EXT_PRC,
    BOS.EXT_CST,
	BOS.ITM_STATUS,
    BOS.ORD_STATUS,
	BOS.[SOURCE]              
        
        FROM BASE_ORDERSUBVIEW  BOS  
        left join FACT_ORDERSUBVIEW FOS
			on BOS.FULLORDERNO = FOS.FULLORDERNO
			and BOS.LINE_NO = FOS.LINE_NO
        Where FOS.FULLORDERNO is null
      
       ;
        
--delete '0000' orders if '0001', '0101' are shipped and order number changed
DELETE FROM dbo.FACT_ORDERSUBVIEW 
Where
SUBSTRING(FULLORDERNO,9,4)in ('0000') and FULLORDERNO not in
(Select MAX(FULLORDERNO) from dbo.FACT_ORDERSUBVIEW GROUP BY ORDERNO);


if (@@Error >0) 
BEGIN
ROLLBACK TRAN
print @@Error
END
ELSE
BEGIN
COMMIT
END

END





GO


