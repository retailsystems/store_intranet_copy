/****** Object:  StoredProcedure [dbo].[OrderView_Select_Filt_PTL]    Script Date: 06/25/2014 15:22:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[OrderView_Select_Filt_PTL]
(
@Region varchar(3),
@District varchar(4), 
@Company varchar(1),
@StoreNo varchar(5),
@EntryDateFrom DateTime,
@EntryDateTo DateTime,
@ShipDateFrom DateTime,
@ShipDateTo DateTime,
@OrderNo varchar(12),
@Status int,
@Source int,
@Brand int,
@Channel int
)
as

/**********************************************************
Name: OrderView_Select_Filt_PTL
Author: Lan Jin 08/04/2010 add source code for pos, rockwall, surface
        Lan Jin 08/24/2010 add PTL 
        Lan Jin 08/01/2011 HTC enhancement
        Apurva Parikh 06/24/2014 Added Cancelled Quantity
Description: Select orders from the table according to different search criteria.
**********************************************************/

DECLARE @BaseSQL nvarchar(max)
DECLARE @EndSQL nvarchar(1000)
DECLARE @RollUpDate varchar(20) 
DECLARE @Table varchar(20) 
DECLARE @SourceCode varchar(2)

IF @Company = '1'
BEGIN
Set @Table = 'dbo.Hottopic2_STORE' 
END
ELSE 
BEGIN
Set @Table = 'dbo.Torrid_STORE' 
END
SET @RollUpDate = convert(varchar,dateadd(month,-12,getdate()),112) 
Set @EndSQL =''

/* add in order no */
IF @OrderNo <>''
BEGIN
set @EndSQL = ' AND VOH.ORDERNO ='''+ @OrderNo+''''
END
ELSE
BEGIN
	/* adding in daterange regardless of criteria */ 
      IF @EntryDateFrom IS NOT NULL
	  BEGIN	 
	  set @EndSQL = @EndSQL + ' AND (VOH.ENTRYDATE BETWEEN ''' +  convert(varchar,@EntryDateFrom,101) +'''' + 
	                                                 ' AND ''' +   convert(varchar,isNull(@EntryDateTo,GetDate()),101) + ''')';
	  END 
	                                                
	  IF @ShipDateFrom>'1/1/1980' AND @ShipDateTo>'1/1/1980' AND (@Status='2' OR @Status='3' OR @Status='4')
	  BEGIN
	  set @EndSQL = @EndSQL + ' AND (SO.SHIPDATE2 BETWEEN ''' + CONVERT(varchar, @ShipDateFrom,101)+'''' + 
													  ' AND ''' + CONVERT(varchar, @ShipDateTo,101) + ''')'
	  END

	IF @Region<> '0'
	BEGIN
	set @EndSQL = @EndSQL + ' AND RTRIM(ST.Region) ='''+@Region+'''' 
	END
	
	IF @District<> '0'
	BEGIN
	set @EndSQL = @EndSQL + ' AND RTRIM(ST.District) ='''+@District+'''' 
	END
	
	IF @StoreNo <>'0'
	BEGIN
	set @EndSQL = @EndSQL + ' AND VOH.STORENO='''+@StoreNo+''''
	END 

 	IF @Source <>'0'
	BEGIN
	IF @Source = '2'
	BEGIN
	set @EndSQL =@EndSQL + ' AND (SUBSTRING(RTRIM(VOH.SOURCE),5,4)= ''HOME'' OR SUBSTRING(RTRIM(VOH.SOURCE),7,2)= ''HO'') '	
    END
	ELSE IF @Source = '1'
	BEGIN
	 set @EndSQL =@EndSQL + ' AND (SUBSTRING(RTRIM(VOH.SOURCE),5,4)= ''STOR'' OR SUBSTRING(RTRIM(VOH.SOURCE),7,2)= ''ST'') '
	END	
	END

	IF @Brand <>'0'
	BEGIN
	IF @Brand= '2'
	BEGIN
	 set @EndSQL =@EndSQL + ' AND (SUBSTRING(RTRIM(VOH.SOURCE),3,2)= ''SH'' OR SUBSTRING(RTRIM(VOH.SOURCE),5,2)= ''SH'') '	
	END
	ELSE IF @Brand= '1'
	BEGIN
	  set @EndSQL =@EndSQL + ' AND (SUBSTRING(RTRIM(VOH.SOURCE),3,2)= ''HT'' OR SUBSTRING(RTRIM(VOH.SOURCE),5,2)= ''HT'') '
	END	
	ELSE IF @Brand= '5'
	BEGIN
	  set @EndSQL =@EndSQL + ' AND (SUBSTRING(RTRIM(VOH.SOURCE),3,2)= ''HT'' OR SUBSTRING(RTRIM(VOH.SOURCE),5,2)= ''TR'') '
	END	
	END
    
    /*	ready to use */
   IF @Channel <>'0'
	  BEGIN	  
	  Select @SourceCode =CHSOURCE from dbo.CHANNEL Where CHNO = @Channel;
	  set @EndSQL =@EndSQL + ' AND Substring(VOH.SOURCE,1,2)='''+@SourceCode+''' '	
	  END	
	 
    

	IF @Status <>'0'
	BEGIN
	
	/*we face several different situations if user picked a status*/
	
	/*ReceiveInStore or CustomerPickUp - see if those turn up to be null */
		IF @Status = '3'
		BEGIN
		set @EndSQL =@EndSQL + ' AND KS.PACKAGENO=KS.PackQty AND KS.PACKAGENO > KS.PackPicQty ' 		
		END
		ELSE IF @Status = '4'
		BEGIN
        set  @EndSQL =@EndSQL + ' AND KS.PACKAGENO=KS.PackQty AND KS.PACKAGENO = KS.PackPicQty '
		END	
	/*Pending - see if OH.BIGSTATUS is in 'In Warehouse'P7 or 'Reg Ord ready process'P1 */
		ELSE IF @Status = '1'
		BEGIN
		 /*set @EndSQL =@EndSQL + ' AND (OH.STATUS= ''P7'' OR OH.STATUS = ''P1'' OR OH.STATUS=''P3'' )'*/
         set @EndSQL =@EndSQL + ' AND (RTRIM(VOH.STATUS)<>''PC'' AND RTRIM(VOH.STATUS)<>''PD'' AND RTRIM(VOH.STATUS)<>''PG'' AND RTRIM(VOH.STATUS)<> ''PB'' AND RTRIM(VOH.STATUS) <> ''PA'' AND RTRIM(VOH.STATUS)<>''WB'') AND (KS.DATERECINSTORE IS NULL) ' 		
        END
	
	/*Shipped - see if OH.BIGSTATUS is in 'Ship completed'PC*/
		ELSE IF @Status='2'
		BEGIN
		set @EndSQL =@EndSQL +' AND RTRIM(VOH.STATUS)=''PC'' AND KS.DATERECINSTORE IS NULL'
		END

    /*Cancelled - */
		ELSE IF @Status='5'
		BEGIN
		set @EndSQL =@EndSQL +' AND (RTRIM(VOH.STATUS)=''PD'' OR RTRIM(VOH.STATUS)=''PG'' )'
		END


    /*Choose a status default would be backorderred status */
		ELSE IF @Status='8'
		BEGIN
		set @EndSQL =@EndSQL +' AND (RTRIM(VOH.STATUS)=''PB'' OR RTRIM(VOH.STATUS)=''PA'' OR RTRIM(VOH.STATUS)=''WB'' )'
		END
	/* pending in warehouse and pending of payment as Pending status*/
        ELSE 
		BEGIN
		set @EndSQL =@EndSQL 
		END
	
	/*Include abandoned orders if status is abandoned, else exclude abandoned orders */
		if @Status = '9'
		begin
		set @EndSQL = @EndSQL + ' and AO.AbandonDate is not null'
		end
		else
		begin
		set @EndSQL = @EndSQL + ' and AO.AbandonDate is null'
		end
	END
END

/* set base SQL */
/* set base SQL */
set @BaseSQL = CAST('SELECT RTRIM(ST.Region) AS REGION,RTRIM(ST.District) AS DISTRICT,'' ''AS DIVISION,
RTRIM(VOH.[SOURCE]) AS [SOURCE],VOH.STORENO,VOH.PAYMETHOD,
VOH.ENTRYDATE,SO.ORDERNO, SO.FULLORDERNO,
SO.SHIPDATE2 as SHIPDATE,
SO.TOT_RTL as PRODUCTDOLLARS,
VOH.FNAME,VOH.LNAME,VOH.CNAME,
VOH.DAYPHONE AS DAYPHONE, 
VOH.EMAIL AS EMAIL,
VOH.SHIPSOURCE,             
SO.QTY,
ISNULL(MN.WEIGHT,MN.ACTWEIGHT) AS ORDERWEIGHT,
VOH.NOPACKAGE as PACKAGENO,
MN.TRACKNO,
'' '' as SHIPMETHOD,
VOH.STATUS,
KO.NOTES as NOTES,
KS.DATERECINSTORE,
KS.DATECUSTPICKUP,
CQ.CANQTY as CANCELQUANTITY 
 FROM dbo.FACT_ORDERHEADVIEW  AS VOH WITH (NOLOCK) 
 JOIN
 (SELECT FULLORDERNO,ORDERNO, MAX(CASE WHEN UPPER(ITM_STATUS) IN (''1'',''4'',''5'',''6'',''7'',''E'',''S'',''U'',''X'',''Y'') THEN SHIPDATE ELSE NULL END) AS SHIPDATE2, SUM(QTY) AS QTY, SUM(EXT_PRC) AS TOT_RTL FROM   
 dbo.FACT_ORDERSUBVIEW  WHERE ITM_STATUS not in ( ''N'', ''F'',''C'', ''R'')    
 GROUP BY FULLORDERNO,ORDERNO 
)AS SO
 ON SUBSTRING(VOH.FULLORDERNO,1,8) = SO.ORDERNO 
 JOIN '+@Table+' AS ST (NOLOCK) ON VOH.STORENO = CAST(ST.StoreNum AS int) 
 LEFT JOIN 
 (Select 
 TRACKNO,
 WEIGHT,
 ACTWEIGHT,  
 FULLORDERNO 
 FROM
 dbo.FACT_ORDERTRACKING
 Where SHIPDATE>='''+@RollUpDate+'''    
) AS MN 
  ON MN.FULLORDERNO = SO.FULLORDERNO  
  LEFT JOIN (Select RE.DATERECINSTORE
,RE.DATECUSTPICKUP
,RE.ORDERNO
,RE.PackQty
,RE.PACKAGENO
,RE.FULLORDERNO
,RE.PackPicQty
,RE.TRACKNO  
from (Select MAX(DATERECINSTORE) AS DATERECINSTORE
,MAX(DATECUSTPICKUP) AS DATECUSTPICKUP
,OTP.ORDERNO
,count(DATERECINSTORE) AS PackQty
,ISNULL(count(DATECUSTPICKUP),''0'') AS PackPicQty
,OP.PACKAGENO
,OP.FULLORDERNO
,TK.TRACKNO
 from dbo.ORDPACKAGE AS OP (NOLOCK) join   
 dbo.ORDTRACKRECPICKUP AS OTP (NOLOCK)  
 ON OP.FULLORDERNO =  OTP.FULLORDERNO 
join FACT_ORDERTRACKING TK on OTP.FULLORDERNO =TK.FULLORDERNO AND (OTP.TRACKNO = TK.TRACKNO OR OTP.TRACKNO= TK.FULLORDERNO) 
 GROUP BY OTP.ORDERNO, OP.PACKAGENO, OP.FULLORDERNO,TK.TRACKNO ) AS RE ) AS KS 
 ON KS.FULLORDERNO = SO.FULLORDERNO AND (KS.TRACKNO = MN.TRACKNO OR KS.TRACKNO= SO.FULLORDERNO)  
 LEFT OUTER JOIN
                      dbo.ORDERNOTE AS KO ON KO.ORDERNO = SUBSTRING(VOH.FULLORDERNO, 1, 8)     
 left join (select FULLORDERNO,ORDERNO,NULL AS SHIPDATE2,SUM(QTY) AS CANQTY,SUM(EXT_PRC) AS CAN_TOT_RTL 
	FROM dbo.FACT_ORDERSUBVIEW WHERE ITM_STATUS in (''N'', ''F'',''C'', ''R'') GROUP BY FULLORDERNO,ORDERNO) as CQ
	ON SUBSTRING(VOH.FULLORDERNO,1,8) = CQ.ORDERNO
 left join dbo.AbandonedOrders as AO with (nolock)
	on AO.FULLORDERNO = SO.FULLORDERNO
 WHERE VOH.FULLORDERNO is not null' AS nvarchar(max)) 




/* print @EndSQL */
set @BaseSQL  = @BaseSQL + @EndSQL + ' ORDER BY VOH.ENTRYDATE DESC, SO.FULLORDERNO' 
/* set @BaseSQL  = @BaseSQL + @EndSQL;*/
--print @BaseSQL

EXECUTE sp_executesql @BaseSQL  










GO


