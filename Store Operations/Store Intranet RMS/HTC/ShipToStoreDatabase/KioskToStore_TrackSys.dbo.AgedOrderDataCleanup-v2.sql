use KioskToStore_TrackSys;

CREATE TABLE dbo.AgedOrderDataCleanup2
(
    OrderNumber nvarchar(8) NULL,
	FullOrderNumber nvarchar(12) NULL,
    Processed datetime NULL
);

insert into dbo.AgedOrderDataCleanup2
(OrderNumber, FullOrderNumber, Processed)
select
	A1.OrderNumber,
	null as FullOrderNumber,
	A1.Processed
from dbo.AgedOrderDataCleanup as A1;

drop table dbo.AgedOrderDataCleanup;

exec sp_rename @objname = 'dbo.AgedOrderDataCleanup2', @newname = 'AgedOrderDataCleanup';