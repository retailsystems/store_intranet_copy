create procedure dbo.AbandonedOrders_Delete
(
	@FullOrderNumber nvarchar(12)
)
as
begin
	if @FullOrderNumber is null
	begin
		raiserror('Order number cannot be null!', 11, 3)
		return 1
	end
	
	declare @abandondate datetime
	select @abandondate = AbandonDate from dbo.AbandonedOrders where FULLORDERNO = @FullOrderNumber
	
	if @abandondate is not null
	begin
		delete
		from dbo.AbandonedOrders
		where FULLORDERNO = @FullOrderNumber
		
		update n
		set NOTES = REPLACE(NOTES , 'Abandoned Order ' + @FullOrderNumber + ' as of ' + convert(varchar, @abandondate, 101), '')
		from dbo.ORDERNOTE as n
		where n.ORDERNO = substring(@FullOrderNumber,1,8)
	end
	else
	begin
		raiserror('Order number %s has not been marked as abandoned. Nothing to undo.', 11, 1, @FullOrderNumber)
	end
end

grant execute on dbo.AbandonedOrders_Insert to R_OrderSummary