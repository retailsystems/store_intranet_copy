-- =============================================
-- Author:		<Michelle An>
-- Create date: <3/4/2014>
-- Description:	<merge order shipping>
-- =============================================
/*
* Modifier | Date | Description
* Apurva Parikh | 05/22/2014 | Added check to only update non received orders
*/
ALTER PROCEDURE [dbo].[TRN_ORDERTRACKING]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
BEGIN TRAN;
  --first to update the orders with new shipment date and product amount and status and source
  
  
  Update  TOS
  SET 
 
    TOS.[TRACKNO] = BOS.TRACKNO,
    TOS.[WEIGHT] = BOS.[WEIGHT],  
	TOS.[SHIPDATE]=CAST(BOS.[SHIPDATE] as Datetime) ,
	TOS.[DATCHG]=CASE WHEN ISDATE(BOS.[DATCHG]) = 0 THEN NULL ELSE CAST(BOS.[DATCHG] as Datetime) END, 
	TOS.ACTWEIGHT = BOS.ACTWEIGHT, 
	TOS.PHONE= BOS.PHONE  
	 
  FROM FACT_ORDERTRACKING TOS
  JOIN BASE_ORDERTRACKING BOS  
  ON (TOS.FULLORDERNO = BOS.FULLORDERNO)
  left join ORDTRACKRECPICKUP P
	on P.FULLORDERNO = TOS.FULLORDERNO
  Where ISDATE (BOS.SHIPDATE)= 1
  and P.ORDTRACKNO is null;
 
  
  
  --second to insert the data not found int the fact table
    
        INSERT INTO FACT_ORDERTRACKING
              
        SELECT           
        BOS.ORDERNO,
        BOS.FULLORDERNO,
        BOS.TRACKNO,
        BOS.WEIGHT,  
	    CAST(BOS.[SHIPDATE] as Datetime) ,
	    CASE WHEN ISDATE(BOS.DATCHG) =0 THEN NULL ELSE CAST(BOS.[DATCHG] as Datetime) END, 
	    BOS.ACTWEIGHT,  
        BOS.PHONE  
        
        
        FROM BASE_ORDERTRACKING  BOS  
        Where BOS.FULLORDERNO not in (Select FULLORDERNO from FACT_ORDERTRACKING)
        and ISDATE (BOS.SHIPDATE)= 1;
        


if (@@Error >0) 
BEGIN
ROLLBACK TRAN
print @@Error
END
ELSE
BEGIN
COMMIT
END

END


GO


