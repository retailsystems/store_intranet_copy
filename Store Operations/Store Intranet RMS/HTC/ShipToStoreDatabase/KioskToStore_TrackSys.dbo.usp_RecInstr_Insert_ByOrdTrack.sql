alter procedure [dbo].[usp_RecInstr_Insert_ByOrdTrack]
(
	@OrderTrackNo   varchar(52),
    @Company varchar(1),
	@TRACKNO varchar(40),
	@FullOrderNo varchar(12),
    @Result int output,
	@CustEmail varchar(50) output,
	@CustName varchar(32) output,
	@Street varchar(30) output,
	@City varchar(30) output,
	@State varchar(2) output,
	@Zip varchar(14) output,
	@Division varchar(4) output,  
	@IsEmail int output, 
	@StoreName varchar(50) output,
	@StrAddress varchar (105) output,
	@StrCity varchar (35) output,
	@StrState varchar (2) output,
	@StrZip varchar (5) output,
    @StrPhoneNo varchar (16) output, 
    @StoreNum varchar (4) output,
	@Admin bit = 0
)     
as

/**********************************************************
Name: usp_RecInstr_Insert_ByOrdTrack
Author: Lan Jin 03/15/2010 created and Add PTL on 05/19/2010
Modified by: Sri Bajjuri on 6/15/2011 
Modified by Lan on 06/23/2011 get cust phoneNo and StoreNo(email) if no email address 
Modified by: Apurva Parikh on 06/09/2014 Add Admin parameter and new result value
Description: Insert receiveInStore tracking order info to the tables.
**********************************************************/
declare @PackSent int
declare @OrderCount int
declare @PackReceived int 
declare @IsUpdate int 
declare @StoreNo int 

SET @IsEmail = 0   --TRUE =1, FALSE =0
SET @IsUpdate = 0   --TRUE =1, FALSE =0 
SET @PackSent = 0 
SET @Result = 0

--Modif 1 - Update_2 file:

--SET TRANSACTION ISOLATION LEVEL SNAPSHOT
--BEGIN TRANSACTION

-- Check for Abandoned Order
if exists (select 1 from dbo.AbandonedOrders with (nolock) where FULLORDERNO = @FullOrderNo)
begin
set @Result = 9
set @IsEmail = 0

return 0
end

IF NOT EXISTS (SELECT * FROM dbo.ORDTRACKRECPICKUP WHERE ORDTRACKNO = @OrderTrackNo)  
BEGIN
	
	SELECT @PackSent = COUNT(*) FROM dbo.FACT_ORDERTRACKING WITH (NOLOCK)	WHERE FULLORDERNO = @FullOrderNo;	

       IF @PackSent = 0  
			if @Admin = 0
			BEGIN 
               SELECT @PackSent = COUNT(DISTINCT SUB.FULLORDERNO) FROM dbo.FACT_ORDERSUBVIEW SUB
               JOIN dbo.FACT_ORDERHEADVIEW OH on OH.ORDERNO = SUB.ORDERNO and OH.STATUS = 'PC' 
               and SHIPSOURCE = 'STORE' 
               WHERE SUB.FULLORDERNO =  @FullOrderNo;
			END 
			else if @Admin = 1
			begin
               SELECT @PackSent = COUNT(DISTINCT SUB.FULLORDERNO) FROM dbo.FACT_ORDERSUBVIEW SUB
               JOIN dbo.FACT_ORDERHEADVIEW OH on OH.ORDERNO = SUB.ORDERNO 
               and SHIPSOURCE = 'STORE' 
               WHERE SUB.FULLORDERNO =  @FullOrderNo;
			end
        IF @PackSent>0 
        BEGIN

			  IF NOT EXISTS (SELECT * FROM dbo.ORDPACKAGE WITH (NOLOCK) WHERE FULLORDERNO = @FullOrderNo)
			  BEGIN 
                  INSERT INTO dbo.ORDPACKAGE 
                    (
					FULLORDERNO,
					PACKAGENO
                    )
                values 
            		(
					@FullOrderNo,
					@PackSent
					)
		      END
              ELSE
              BEGIN 
                    UPDATE dbo.ORDPACKAGE 
						   SET PACKAGENO = @PackSent   
                    WHERE FULLORDERNO = @FullOrderNo 
              END 

             SELECT DISTINCT 
								@Division = SUBSTRING(OH.DIVISION,2,1), 
								@StoreNo = OH.STORENO, 
                                @StoreNum = OH.STORENO,
								@CustName =	RTRIM(OH.CNAME), 
								@Street = RTRIM(OH.STREET),
								@City =	RTRIM(OH.CITY),
								@State = RTRIM(OH.STATE),
								@Zip =	RTRIM(OH.ZIP),
								@CustEmail = ISNULL(RTRIM(OH.EMAIL), ' '),
                                @StrPhoneNo = ISNULL(RTRIM(OH.DAYPHONE), ' ')
					FROM     (SELECT ORDERNO,FULLORDERNO,DIVISION, STORENO,SOURCE, PRODUCTDOLLARS,
                              CNAME,STREET,CITY,[STATE],ZIP,EMAIL, DAYPHONE       
                              FROM dbo.FACT_ORDERHEADVIEW WITH (NOLOCK)) AS OH
                    WHERE   OH.ORDERNO = SUBSTRING(@FullOrderNo,1,8);

		        If @Company = '1' 
                BEGIN
				SELECT DISTINCT 
				@StoreName =RIGHT(rtrim(HS.StoreName), LEN(HS.StoreName) - PATINDEX('%[ a-zA-z]%', HS.StoreName)), 
				@StrAddress = HS.Address, 
				@StrCity = HS.City,
				@StrState = HS.State,
				@StrZip = HS.Zip 
				FROM dbo.Hottopic2_STORE AS HS WITH (NOLOCK) 
				WHERE (Deleted IS NULL OR
							  Deleted <> 'True') 
					  AND CAST(HS.StoreNum AS int)= @StoreNo; 
                END
                ELSE
                BEGIN
	           SELECT DISTINCT 
				@StoreName =RIGHT(rtrim(HS.StoreName), LEN(HS.StoreName) - PATINDEX('%[ a-zA-z]%', HS.StoreName)), 
				@StrAddress = HS.Address, 
				@StrCity = HS.City,
				@StrState = HS.State,
				@StrZip = HS.Zip 
				FROM dbo.Torrid_STORE AS HS WITH (NOLOCK) 
				WHERE (Deleted IS NULL OR
							  Deleted <> 'True') 
					  AND CAST(HS.StoreNum AS int)= @StoreNo; 
                END 



                
					
                INSERT INTO dbo.ORDTRACKRECPICKUP 
                    (
					ORDTRACKNO,
					TRACKNO,
					FULLORDERNO,
					DATERECINSTORE,
					ORDERNO
                    )
                values 
            		(
					@OrderTrackNo,
					@TRACKNO ,
					@FullOrderNo,
					GETDATE(),
					SUBSTRING(@FullOrderNo,1,8)
					)
 
			
				SELECT @PackReceived = COUNT(*) 
				FROM dbo.ORDTRACKRECPICKUP WITH (NOLOCK)   
				WHERE FULLORDERNO = @FullOrderNo  


                IF @PackSent = @PackReceived 
					BEGIN 
					SET @IsEmail = 1  
					SET @Result = 1 
					END 
                ELSE IF @PackSent > @PackReceived 
					BEGIN 
					SET @IsEmail = 0 
					SET @Result = 6
					END
				else
					begin
					set @IsEmail = 0
					set @Result = 3
					end
			 END
			 ELSE 
				BEGIN 
				SET @Result = 5 --ERROR SENT BOX =0 
				END 
END
ELSE
BEGIN
   SET @Result = 2 --DUPLICATE entry
END
if @Admin = 1
begin
	update dbo.FACT_ORDERSUBVIEW
	set ITM_STATUS = 'S',
		ORD_STATUS = 'S' 
	where FULLORDERNO = @FullOrderNo
	
	declare @orderno varchar(8)
	set @orderno = substring(@FullOrderNo, 1, 8)
	
	update dbo.FACT_ORDERHEADVIEW
	set STATUS = 'PC'
	where ORDERNO = @orderno
	
	declare @message varchar(4800)
	set @message = @FullOrderNo + ' marked as received by Helpdesk.'
	
	if exists (select 1 from dbo.ORDERNOTE with (nolock) where ORDERNO = @orderno)
	begin
		update dbo.ORDERNOTE
		set NOTES = isnull(NOTES,'') + @message
		where ORDERNO = @orderno
	end
	else
	begin
		insert into dbo.ORDERNOTE (ORDERNO, NOTES)
		values (@orderno, @message)
	end
end
GO