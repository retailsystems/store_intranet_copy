/****** Object:  StoredProcedure [dbo].[UPDATE_ORDERTRACKING]    Script Date: 05/08/2014 09:24:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Michelle An>
-- Create date: <4/24/2014>
-- Description:	<merge order shipping>
-- =============================================
/*
* Modifier | Date | Description
* Apurva Parikh | 05/22/2014 | Added check to only update non received orders
*/
ALTER PROCEDURE [dbo].[UPDATE_ORDERTRACKING]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRAN;
		--first to update the orders with new shipment date and product amount and status and source
	Update TOS
	SET [TRACKNO] = BOS.TRACKNO
	FROM FACT_ORDERTRACKING TOS
	JOIN IMPORT_ORDERTRACKING BOS
		ON (TOS.FULLORDERNO = BOS.FULLORDERNO)
	Left join ORDTRACKRECPICKUP as r
		on r.FULLORDERNO = TOS.FULLORDERNO
	where ORDTRACKNO is null
 
	if (@@Error >0) 
	BEGIN
		ROLLBACK TRAN
		print @@Error
	END

	ELSE
	BEGIN
		COMMIT
	END

	begin tran;
	insert into FACT_ORDERTRACKING
	(ORDERNO, FULLORDERNO, TRACKNO, SHIPDATE)
	select
		substring(i.FULLORDERNO,1,8),
		i.FULLORDERNO,
		i.TRACKNO,
		s.SHIPDATE
	from IMPORT_ORDERTRACKING as i
	join FACT_ORDERSUBVIEW as s
		on s.FULLORDERNO = i.FULLORDERNO
	left join FACT_ORDERTRACKING as f
		on f.FULLORDERNO = i.FULLORDERNO
	where f.TRACKNO is null

	if (@@error > 0)
	begin
		rollback tran
		print @@error
	end
	else
	begin
		commit
	end
END
GO