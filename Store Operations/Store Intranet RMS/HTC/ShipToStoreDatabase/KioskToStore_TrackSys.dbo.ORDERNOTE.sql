select distinct
	n.ORDERNO,
	d.FULLORDERNO,
	n.NOTES
into dbo.NewORDERNOTES
from dbo.ORDERNOTE as n with (nolock)
left join dbo.FACT_ORDERSUBVIEW as d with (nolock)
	on d.ORDERNO = n.ORDERNO
order by d.FULLORDERNO;

-- Copy permissions

-- Drop old table
-- Rename new table