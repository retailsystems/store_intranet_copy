CREATE TABLE "AgedOrderDataCleanup" (
    "Region" NVARCHAR(2),
    "District" NVARCHAR(3),
    "Store" SMALLINT,
    "OrderDate" DATETIME,
    "OrderNumber" NVARCHAR(8),
    "MassCheckin" NVARCHAR(1),
    "ShipDate" DATETIME,
    "ProductDollars" MONEY,
    "CustomerName" NVARCHAR(255),
    "DaysOutstanding" SMALLINT,
    "DayPhone" NVARCHAR(15),
    "Email" NVARCHAR(255),
    "ShipSource" NVARCHAR(10),
    "Quantity" TINYINT,
    "TrackingNumber" NVARCHAR(30),
    "DateReceivedInStore" DATETIME,
    "Processed" DATETIME
)

grant select, insert, update, delete, alter to R_HTCTracker