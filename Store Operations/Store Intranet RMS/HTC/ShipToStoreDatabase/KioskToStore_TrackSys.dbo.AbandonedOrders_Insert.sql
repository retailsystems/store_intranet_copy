create procedure dbo.AbandonedOrders_Insert
(
	@FullOrderNumber nvarchar(12),
	@UserID varchar(6)
)
as
begin
	if @FullOrderNumber is null
	begin
		raiserror('Order number cannot be null!', 11, 3)
		return 1
	end
	
	if exists (
		select 1 
		from dbo.ORDTRACKRECPICKUP with (nolock)
		where FULLORDERNO = @FullOrderNumber
		and DATECUSTPICKUP is not null)
	begin
		raiserror('Order number %s has been picked up and cannot be abandoned.', 11, 2, @FullOrderNumber)
	end
	
	if not exists (select 1 from dbo.AbandonedOrders where FULLORDERNO = @FullOrderNumber)
	begin
		insert into dbo.AbandonedOrders
		(FULLORDERNO,AbandonDate,AbandonUserID)
		values
		(@FullOrderNumber, getdate(), @UserID)
		
		update n
		set NOTES = NOTES + ' Abandoned Order ' + FULLORDERNO + ' as of ' + convert(varchar, getdate(), 101)
		from dbo.AbandonedOrders as a
		inner join dbo.ORDERNOTE as n
			on n.ORDERNO = substring(a.FULLORDERNO,1,8)
		where a.FULLORDERNO = @FullOrderNumber

		insert into dbo.ORDERNOTE (ORDERNO,NOTES)
		select substring(a.FULLORDERNO,1,8), 'Abandoned Order ' + FULLORDERNO + ' as of ' + convert(varchar, getdate(), 101)
		from dbo.AbandonedOrders as a
		left join dbo.ORDERNOTE as n
			on n.ORDERNO = substring(a.FULLORDERNO,1,8)
		where a.FULLORDERNO = @FullOrderNumber
		and n.ORDERNO is null
	end
	else
	begin
		raiserror('Order number %s has already been marked as abandoned.', 11, 1, @FullOrderNumber)
	end
end

grant execute on dbo.AbandonedOrders_Insert to R_OrderSummary