/****** Object:  StoredProcedure [dbo].[UPDATE_ORDERTRACKING]    Script Date: 05/08/2014 09:24:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Michelle An>
-- Create date: <4/24/2014>
-- Description:	<merge order shipping>
-- =============================================
CREATE PROCEDURE [dbo].[UPDATE_ORDERTRACKING]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
BEGIN TRAN;
  --first to update the orders with new shipment date and product amount and status and source
  
  Update  TOS
  SET  
  TOS.[TRACKNO] = BOS.TRACKNO
	 
  FROM FACT_ORDERTRACKING TOS
  JOIN IMPORT_ORDERTRACKING BOS  
  ON (TOS.FULLORDERNO = BOS.FULLORDERNO)
 
if (@@Error >0) 
BEGIN
ROLLBACK TRAN
print @@Error
END
ELSE
BEGIN
COMMIT
END

END


GO