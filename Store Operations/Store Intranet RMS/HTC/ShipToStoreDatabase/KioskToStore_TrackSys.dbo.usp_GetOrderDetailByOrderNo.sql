/****** Object:  StoredProcedure [dbo].[usp_GetOrderDetailByOrderNo]    Script Date: 06/25/2014 16:40:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

alter procedure [dbo].[usp_GetOrderDetailByOrderNo]
(
	 @OrderNo    varchar(12)
)
as

/**********************************************************
Name: OrderItemDetail_SelectByOrderNo
Author: Lan Jin 
Last Modified by: Michelle An 3/5/2014
		Apurva Parikh 06/25/2014 Added all pertinent statuses
Description: get all order items detail by select orderno.
**********************************************************/

 SELECT  SKU_NUM AS ITEMNO
 ,SKU_DESC AS DESCRIPTION
 ,FULLORDERNO
 ,ORDERNO
 ,EDP_NO as EDPNO
 ,SUM(CASE WHEN ITM_STATUS in ('S','U') THEN QTY ELSE 0 END) AS SQTY 
 ,SUM(CASE WHEN ITM_STATUS in ('N', 'F','C', 'R') THEN QTY ELSE 0 END) AS CQTY 
 ,SUM(QTY) AS TOTALQTY
 ,LTRIM(RTRIM(STR(SUM(CASE WHEN ITM_STATUS in ('S','U') THEN EXT_PRC ELSE 0 END),10,2))) AS SPRICE 
 ,LTRIM(RTRIM(STR(SUM(CASE WHEN ITM_STATUS in ('N', 'F','C', 'R') THEN EXT_PRC ELSE 0 END),10,2))) AS CPRICE
 ,1 AS TOTSPRICE 
 FROM
 
 dbo.FACT_ORDERSUBVIEW S
 
 Where S.ORDERNO = @OrderNo
 GROUP BY  SKU_NUM  ,SKU_DESC ,FULLORDERNO ,ORDERNO ,EDP_NO



GO


