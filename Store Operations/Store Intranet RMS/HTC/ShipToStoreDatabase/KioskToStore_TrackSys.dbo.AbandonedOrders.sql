create table dbo.AbandonedOrders
(
	FULLORDERNO nvarchar(12) not null primary key,
	AbandonDate datetime null,
	AbandonUserID varchar(6) null
)

grant select, insert, update, delete, alter on dbo.AbandonedOrders to R_OrderSummary
grant select on dbo.AbandonedOrders to R_HTCTracker
grant select on dbo.AbandonedOrders to R_HTCTrackerRpt