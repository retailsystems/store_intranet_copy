use STAGING

create table HTCTracker.OrderSync
( ORDERNO varchar(8) not null, ORDERHEADERNO varchar(12) not null, FULLORDERNO varchar(12) not null )

-- DBA to create role R_HTCTracker on database
-- DBA to assign role to HTCTracker user
grant select, insert, update, delete, alter on STAGING.HTCTracker.OrderSync to R_HTCTracker