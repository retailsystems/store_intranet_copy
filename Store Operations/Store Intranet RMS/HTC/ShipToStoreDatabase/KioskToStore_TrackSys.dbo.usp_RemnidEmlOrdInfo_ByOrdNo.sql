USE [KioskToStore_TrackSys]
GO

/****** Object:  StoredProcedure [dbo].[usp_RemnidEmlOrdInfo_ByOrdNo]    Script Date: 07/29/2014 14:25:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

alter procedure [dbo].[usp_RemnidEmlOrdInfo_ByOrdNo]
(
	@FULLORDERNO varchar(12),
	@Street varchar(30) output,
	@City varchar(30) output,
	@State varchar(2) output,
	@Zip varchar(14) output,
	@Division varchar(4) output, 
    @CustName varchar(32) output,
    @CustEmail varchar(50) output,
	@StoreName varchar(50) output,
	@StrAddress varchar (105) output,
	@StrCity varchar (35) output,
	@StrState varchar (2) output,
	@StrZip varchar (7) output,
    @Result int output 
)
as

/**********************************************************
Name: usp_RemnidEmlOrdInfo_ByOrdNo
Author: Lan Jin 02/26/2010
Last Modified by: Michelle An 3/4/2014
Modified by: Apurva Parikh 07/29/2014 - Added fix to storename to handle extra spaces
Description: Insert Order notes into the table.
**********************************************************/

SELECT DISTINCT 
		@Division = SUBSTRING(OH.DIVISION,2,1),  
        @CustName = RTRIM(OH.CNAME), 
        @Street = RTRIM(OH.STREET),
        @City = RTRIM(OH.CITY),
		@State = RTRIM(OH.STATE),
		@Zip = RTRIM(OH.ZIP),
        @CustEmail = ISNULL(RTRIM(OH.EMAIL), ' '),
        @StoreName =RIGHT(rtrim(HS.StoreName), LEN(HS.StoreName) - PATINDEX('%[ a-zA-z]%', HS.StoreName)), 
		@StrAddress = HS.Address,
		@StrCity = HS.City,
		@StrState = HS.State,
		@StrZip = HS.Zip 
            
		FROM     (SELECT ORDERNO,FULLORDERNO,DIVISION, STORENO,SOURCE, PRODUCTDOLLARS,
                         CNAME,STREET,CITY,[STATE],ZIP,EMAIL 
                              FROM FACT_ORDERHEADVIEW WITH (NOLOCK)) AS OH

		INNER JOIN 
               (Select * from dbo.Hottopic2_STORE UNION Select * from dbo.Torrid_STORE) AS HS ON OH.STORENO = CAST(HS.StoreNum AS int) 

		WHERE   OH.ORDERNO = SUBSTRING(@FULLORDERNO,1,8)   

                 
   
Set @Result = 1
	
If @@Error <>0
BEGIN

Set @Result = 0
END
ELSE
BEGIN

Set @Result = 1
END

GO


