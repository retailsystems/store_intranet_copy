alter procedure dbo.Admin_Search
(
	@trackno nvarchar(30) = null,
	@orderno nvarchar(8) = null,
	@fullorderno nvarchar(12) = null
)
as
begin
declare @orderdetails table( FULLORDERNO nvarchar(12) not null )

if @trackno is not null
begin
	insert into @orderdetails
	select FULLORDERNO
	from dbo.FACT_ORDERTRACKING with (nolock)
	where TRACKNO = @trackno
end
else if @orderno is not null
begin
	insert into @orderdetails
	select FULLORDERNO
	from dbo.FACT_ORDERSUBVIEW with (nolock)
	where ORDERNO = @orderno
end
else if @fullorderno is not null
begin
	insert into @orderdetails
	values(@fullorderno)
end
else
	return 0

select distinct
	d.FULLORDERNO,
	d.ORDERNO,
	h.DIVISION,
	h.STORENO,
	d.ITM_STATUS,
	d.ORD_STATUS,
	h.STATUS,
	d.SHIPMETHOD,
	t.TRACKNO,
	h.NOPACKAGE,
	h.ENTRYDATE,
	h.LASTSHIPMENT,
	d.SHIPDATE,
	p.DATERECINSTORE,
	p.DATECUSTPICKUP,
	p.DATERETURNDC,
	p.TRACKNO as XTRACKNO,
	a.AbandonDate
from dbo.FACT_ORDERSUBVIEW as d with (nolock)
inner join @orderdetails as o
	on o.FULLORDERNO = d.FULLORDERNO
inner join dbo.FACT_ORDERHEADVIEW as h with (nolock)
	on h.ORDERNO = d.ORDERNO
left join dbo.FACT_ORDERSHIPPING as s with (nolock)
	on s.FULLORDERNO = d.FULLORDERNO
left join dbo.FACT_ORDERTRACKING as t with (nolock)
	on t.FULLORDERNO = d.FULLORDERNO
left join dbo.ORDTRACKRECPICKUP as p with (nolock)
	on p.FULLORDERNO = d.FULLORDERNO
left join dbo.AbandonedOrders as a with (nolock)
	on a.FULLORDERNO = d.FULLORDERNO
order by h.DIVISION, h.STORENO, d.FULLORDERNO
end
go
grant execute on dbo.Admin_Search to R_OrderSummary
go