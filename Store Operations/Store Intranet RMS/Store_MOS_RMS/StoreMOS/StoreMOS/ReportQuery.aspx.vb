Public Class ReportQuery

    Inherits System.Web.UI.Page

    Protected WithEvents btnReport As System.Web.UI.WebControls.Button
    Protected WithEvents btnHome As System.Web.UI.WebControls.Button
    Protected WithEvents txtStore As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtStartDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEndDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Protected WithEvents txtToDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ddlStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblStore As System.Web.UI.WebControls.Label
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifmHeader As System.Web.UI.HtmlControls.HtmlGenericControl
    'Protected WithEvents ucHeader As Header

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private objUser As New User()

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim objSecurity As New Security()
        Dim strStore As String
        Dim strUser As String
        Dim strStoreCD As String

        pageBody.Attributes("onload") = ""

        strStoreCD = Request("Store")

        objUser.GetUserInfo()
        If objUser.IsValidUser Then
            ' ucHeader.lblUserName.Text = objUser.User
            strUser = objUser.User
        Else
            Response.Redirect("Login.aspx")
        End If

        objUser.GetStoreInfo()
        If objUser.IsValidStore Then
            ' ucHeader.lblStore.Text = objUser.UserStore
            '  ucHeader.lblHeaderPage.Text = "MOS Reports"
            strStore = "Store: " & objUser.UserStore.ToString.PadLeft(4, "0")
        Else
            Response.Redirect("GetStoreInfo.aspx")
        End If

        If Not objSecurity.HasAccess(objUser, "STOREMOS") Then
            Response.Redirect("AccessDenied.aspx")
        End If

        If objSecurity.HasAccess(objUser, "STOREMOS_DMREPORT") Then
            If Not Page.IsPostBack Then
                If FillStoreDropDown(strStoreCD) Then
                    ddlStore.Visible = True
                    lblStore.Visible = True
                End If
            End If
        End If

        'Add javascript to capture return key
        txtStartDate.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){event.returnValue=false;event.cancel = true;document.forms[0].btnReport.click();}")
        txtEndDate.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){event.returnValue=false;event.cancel = true;document.forms[0].btnReport.click();}")
        ddlStore.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){event.returnValue=false;event.cancel = true;document.forms[0].btnReport.click();}")

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=" & strStore & "&user=" & strUser & "&title=MOS Reports&Version=" & ConfigurationSettings.AppSettings("strVersion"))

    End Sub

    Private Function FillStoreDropDown(ByVal Store As Integer) As Boolean

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        Dim objSQLDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        'prefix 6 zero to employee ID reflected lawson employee change to 6 char. 
        strStoredProcedure = "spGetDMStores '" & objUser.UserId.PadLeft(6, "0"c) & "' "

        Dim objACommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        objSQLDataReader = objACommand.ExecuteReader()

        ddlStore.DataSource = objSQLDataReader
        ddlStore.DataBind()

        ddlStore.SelectedIndex = ddlStore.Items.IndexOf(ddlStore.Items.FindByValue(Store))

        objSQLDataReader.Close()
        objACommand.Dispose()
        objConnection.Close()

        If ddlStore.Items.Count >= 1 Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click

        If FormIsValid() Then

            If ddlStore.Visible Then
                Response.Redirect("Reports.aspx?Store=" & ddlStore.SelectedItem.Value & "&StartDate=" & txtStartDate.Text & "&EndDate=" & txtEndDate.Text)
            Else
                Response.Redirect("Reports.aspx?Store=" & objUser.UserStore & "&StartDate=" & txtStartDate.Text & "&EndDate=" & txtEndDate.Text)
            End If

        End If

    End Sub



    Private Function FormIsValid() As Boolean

        Dim ErrorMsg As String
        Dim booValid As Boolean = True

        'If Not IsNumeric(txtStore.Text) Then
        'booValid = False
        'ErrorMsg = ErrorMsg & "Store number is invalid.."
        'End If

        If Not IsDate(txtStartDate.Text) Then
            booValid = False
            ErrorMsg = ErrorMsg & "Start Date is not valid. Use MM/DD/YYYY format, like 04/07/2004"
        End If

        If Not IsDate(txtEndDate.Text) Then
            booValid = False
            ErrorMsg = ErrorMsg & "End Date is not valid. Use MM/DD/YYYY format, like 04/07/2004"
        End If

        If IsDate(txtEndDate.Text) And IsDate(txtStartDate.Text) Then
            If CDate(txtStartDate.Text) <= "12/31/1752" Or CDate(txtEndDate.Text) <= "12/31/1752" Then
                booValid = False
                ErrorMsg = ErrorMsg & "Dates must be greater than 1752.."
            End If
        End If

        If booValid Then
            If CDate(txtStartDate.Text) > CDate(txtEndDate.Text) Then
                booValid = False
                ErrorMsg = ErrorMsg & "Date range invalid.."
            End If
        End If

        If Not booValid Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(ErrorMsg) & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
        End If

        Return booValid

    End Function

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Response.Redirect("default.aspx")
    End Sub

    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        objUser.ClearUserInfo()
        Response.Redirect("..\Home.asp")
    End Sub

End Class
