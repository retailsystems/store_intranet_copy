<%@ Page Language="vb" AutoEventWireup="false" Codebehind="GetStoreInfo.aspx.vb" Inherits="StoreMOS.GetStoreInfo"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>GetUserInfo</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body>
		<form id="FormReportQuery" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server" />
			<table width="100%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td class="alert alert-warning">You have not set your store number. <A href="..\SetStore.asp">
							Click here to set your store.</A><BR>
						You will not be able to MOS items until your store number is set.
					</td>
				</tr>
			</table>
			<br>
			<table width="100%">
				<tr>
					<td>
						<hr>
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;&nbsp;
						<asp:button CssClass="btn btn-danger" id="btnReturn" runat="server" Text="Return"></asp:button>&nbsp;&nbsp;
						<asp:button CssClass="btn btn-danger" id="btnHome" runat="server" Text="Home"></asp:button></td>
				</tr>
			</table>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
