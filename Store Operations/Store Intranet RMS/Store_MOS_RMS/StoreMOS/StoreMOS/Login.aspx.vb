Public Class Login
    Inherits System.Web.UI.Page
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents txtEmpId As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnHome As System.Web.UI.WebControls.Button
    'Protected WithEvents ucHeader As Header
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifmHeader As System.Web.UI.HtmlControls.HtmlGenericControl


    Private objUser As New User()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strStore As String

        objUser.GetStoreInfo()
        Dim test1 As Object

        'Dim MyService As RMSProcess.InvAdjustPublishingService = New _
        '                        RMSProcess.InvAdjustPublishingService
        'Dim str As String
        'str = MyService.ping("test")
        'Response.Write(str)
        'Response.End()

        If objUser.IsValidStore Then
            'ucHeader.lblStore.Text = objUser.UserStore
            'ucHeader.lblHeaderPage.Text = "MOS Login"
            strStore = "Store: " & objUser.UserStore.ToString.PadLeft(4, "0")
            ''strStore = "5001"
        Else
            Response.Redirect("GetStoreInfo.aspx")
        End If

        'Add javascript to capture return key
        txtEmpId.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){event.returnValue=false;event.cancel = true;document.forms[0].btnSubmit.click();}")

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=" & strStore & "&user=&title=MOS Login&Version=" & ConfigurationSettings.AppSettings("strVersion"))

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim objSecurity As New Security()

        If Len(txtEmpId.Text.Trim) > 0 Then

            objUser.SetUser(Replace(txtEmpId.Text, "'", "''"))

            If objUser.IsValidUser Then

                Response.Cookies("User")("UserName") = objUser.UserName
                Response.Cookies("User")("Name") = objUser.User
                Response.Cookies("User")("Id") = objUser.UserId
                Response.Cookies("User")("JobCode") = objUser.JobCode

                If objSecurity.HasAccess(objUser, "STOREMOS") Then
                    Response.Redirect("default.aspx")
                Else
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Employee Id not authorized to use this application.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
                End If
            Else
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Employee Id does not exist. Please try again.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            End If
        Else
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter an employee Id.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
        End If

    End Sub

    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        objUser.ClearUserInfo()
        Response.Redirect("..\Home.asp")
    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        objUser.ClearUserInfo()
        Response.Redirect("..\Home.asp")
    End Sub

End Class
