Public MustInherit Class Header
    Inherits System.Web.UI.UserControl
    Public WithEvents lblUserName As System.Web.UI.WebControls.Label
    Public WithEvents lblStore As System.Web.UI.WebControls.Label
    Protected WithEvents ibLogo As System.Web.UI.WebControls.ImageButton
    Public WithEvents lblHeaderPage As System.Web.UI.WebControls.Label

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ibLogo.ImageUrl = ConfigurationSettings.AppSettings("strStoreImage")

    End Sub

    Private Sub ibLogo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibLogo.Click

        Response.Redirect(ConfigurationSettings.AppSettings("strLogoLink"))

    End Sub

End Class
