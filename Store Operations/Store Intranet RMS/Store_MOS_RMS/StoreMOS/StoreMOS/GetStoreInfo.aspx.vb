Public Class GetStoreInfo
    Inherits System.Web.UI.Page
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Protected WithEvents btnHome As System.Web.UI.WebControls.Button
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifmHeader As System.Web.UI.HtmlControls.HtmlGenericControl
    'Protected WithEvents ucHeader As Header

    Private objUser As New User()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUser.GetUserInfo()
        If objUser.IsValidUser Then
            'ucHeader.lblUserName.Text = objUser.User
        End If

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=No Store Number&Version=" & ConfigurationSettings.AppSettings("strVersion"))

    End Sub

    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        objUser.ClearUserInfo()
        Response.Redirect("..\Home.asp")
    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        objUser.ClearUserInfo()
        Response.Redirect("..\Home.asp")
    End Sub

End Class
