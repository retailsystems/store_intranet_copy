<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MOS.aspx.vb" Inherits="StoreMOS.MOS" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MOS</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body id="pageBody" runat="server">
		<form id="FormMOS" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server" />
			<div class="container home" role="main">
			<table width="100%">
				<tr>
					<td>SKU&nbsp;&nbsp;
						<asp:textbox id="txtSKU" runat="server" MaxLength="12"></asp:textbox>&nbsp;&nbsp;REASON&nbsp;&nbsp;
						<asp:dropdownlist id="ddlReason" AutoPostBack="False" EnableViewState="True" runat="server" DataTextField="Reason_Name" DataValueField="Reason_ID"></asp:dropdownlist>&nbsp;
						<asp:button id="btnAddItem" runat="server" CssClass="btn btn-danger" Text="Add"></asp:button></td>
				</tr>
			</table>
			<br>
			<table class="table archtbl" width="100%">
				<tr>
					<!--bgcolor below has to be there, or table turns burgandy-->
					<td colSpan="9">
						<div style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 170px"><asp:datagrid id="dgMOSItems" runat="server" CssClass="table archtbl" showfooter="True" Width="100%"
								ShowHeader="True" AutoGenerateColumns="False" EnableViewState="True" AllowSorting="True">
								<ItemStyle CssClass="archtbltr"></ItemStyle>
								<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
								<Columns>
									<asp:BoundColumn Visible="false" ItemStyle-Width="0" DataField="Item_Id"></asp:BoundColumn>
									<asp:BoundColumn HeaderText="Item" ItemStyle-Width="3%" DataField="Item_Id"></asp:BoundColumn>
									<asp:BoundColumn HeaderText="SKU" ItemStyle-Wrap="False" ItemStyle-Width="10%" DataField="SKU_NUM"></asp:BoundColumn>
									<asp:BoundColumn HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" HeaderText="Vendor"
										ItemStyle-Wrap="False" ItemStyle-Width="5%" DataField="VE_CD"></asp:BoundColumn>
									<asp:BoundColumn HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" HeaderText="Size"
										ItemStyle-Wrap="False" ItemStyle-Width="3%" DataField="SIZE_CD"></asp:BoundColumn>
									<asp:BoundColumn HeaderStyle-HorizontalAlign="right" HeaderText="Retail" ItemStyle-Wrap="False" ItemStyle-Width="10%"
										DataField="CURR" DataFormatString="{0:c}">
										<ItemStyle wrap="False" HorizontalAlign="Right"></ItemStyle>
										<FooterStyle wrap="False" HorizontalAlign="Right"></FooterStyle>
									</asp:BoundColumn>
									<asp:BoundColumn HeaderText="Description" ItemStyle-Width="40%" DataField="DES1"></asp:BoundColumn>
									<asp:BoundColumn HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" HeaderText="MOS Qty"
										HeaderStyle-Wrap="false" ItemStyle-Wrap="False" ItemStyle-Width="8%" DataField="MOS_Qty">
										<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
										<FooterStyle Wrap="false" HorizontalAlign="Center"></FooterStyle>
									</asp:BoundColumn>
									<asp:BoundColumn HeaderText="Reason" ItemStyle-Wrap="False" ItemStyle-Width="8%" DataField="Reason_Name"></asp:BoundColumn>
									<asp:BoundColumn HeaderText="Date" ItemStyle-Wrap="False" ItemStyle-Width="10%" DataField="Date_Created"
										DataFormatString="{0:MM-dd-yyyy}"></asp:BoundColumn>
									<asp:ButtonColumn ItemStyle-Width="4%" ButtonType="PushButton" Text="Undo" CommandName="DeleteItem">
										<ItemStyle wrap="false" HorizontalAlign="Center"></ItemStyle>
									</asp:ButtonColumn>
								</Columns>
							</asp:datagrid></div>
					</td>
				</tr>
			</table>
			<br>
			<table width="100%">
				<tr>
					<td>
						<hr>
					</td>
				</tr>
				<tr>
					<td align="right"><asp:button id="btnMOS" runat="server" CssClass="btn btn-danger" Text="MOS Items"></asp:button>&nbsp;&nbsp;
						<asp:button id="btnReturn" runat="server" CssClass="btn btn-danger" Text="Return"></asp:button>&nbsp;&nbsp;<asp:button id="btnHome" runat="server" CssClass="btn btn-danger" Text="Home"></asp:button></td>
				</tr>
			</table>
			</div>
		</form>
		<script language="javascript">
		<!--
			document.forms[0].txtSKU.focus();	
		//-->
		</script>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
