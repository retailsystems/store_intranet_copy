<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MOSActivity.aspx.vb" Inherits="StoreMOS.MOSActivity"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MOS Activity</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmDailyDefects" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td align="middle"><asp:label id="lblReportName" runat="server" Font-Size="14pt" Font-Names="Arial" Font-Bold="True" ForeColor="Black"></asp:label><br>
						<asp:label id="lblReportDate" runat="server" Font-Size="10pt" Font-Names="Arial" Font-Bold="True" ForeColor="Black"></asp:label></td>
				</tr>
				<tr>
					<td align="middle"><font face="arial" size="2"><asp:label id="lblDate" runat="server" Font-Size="10pt" Font-Names="Arial" ForeColor="Black"></asp:label></font></td>
				</tr>
			</table>
			<br>
			<asp:datagrid id="dgMOSItems" runat="server" BorderColor="black" BorderWidth="1" CellSpacing="0" Font-Size="XX-Small" Font-Names="Arial" ForeColor="Black" showfooter="True" Width="100%" BackColor="#FFFFFF" AutoGenerateColumns="False" EnableViewState="False" CellPadding="2" AllowSorting="True">
				<AlternatingItemStyle BackColor="#AAAAAA"></AlternatingItemStyle>
				<FooterStyle BackColor="#000000" ForeColor="white" Font-Name="arial" Font-Bold="True" />
				<HeaderStyle BackColor="#000000" ForeColor="white" Font-Name="arial" Font-Bold="True" />
				<Columns>
					<asp:BoundColumn ItemStyle-Wrap="False" Visible="false" ItemStyle-Width="0" HeaderText="Id" DataField="Item_Id"></asp:BoundColumn>
					<asp:BoundColumn ItemStyle-Wrap="False" ItemStyle-Width="3%" HeaderText="Item" DataField="Item_Id"></asp:BoundColumn>
					<asp:BoundColumn ItemStyle-Wrap="False" ItemStyle-Width="10%" HeaderText="SKU" DataField="SKU_NUM"></asp:BoundColumn>
					<asp:BoundColumn HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" ItemStyle-Wrap="False" ItemStyle-Width="5%" HeaderText="Vendor" DataField="VE_CD" Visible="false"></asp:BoundColumn>
					<asp:BoundColumn HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" ItemStyle-Wrap="False" ItemStyle-Width="3%" HeaderText="Size" DataField="SIZE_CD"></asp:BoundColumn>
					<asp:BoundColumn HeaderStyle-HorizontalAlign="right" ItemStyle-Wrap="False" ItemStyle-Width="10%" HeaderText="Retail" DataField="CURR" DataFormatString="{0:c}">
						<ItemStyle wrap="False" HorizontalAlign="Right"></ItemStyle>
						<FooterStyle wrap="False" HorizontalAlign="Right"></FooterStyle>
					</asp:BoundColumn>
					<asp:BoundColumn ItemStyle-Width="35%" HeaderText="Description" DataField="DES1"></asp:BoundColumn>
					<asp:BoundColumn HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" ItemStyle-Wrap="False" ItemStyle-Width="8%" HeaderStyle-Wrap="False" HeaderText="MOS Qty" DataField="MOS_Qty">
				
						<ItemStyle wrap="False" HorizontalAlign="Center"></ItemStyle>
						<FooterStyle wrap="False" HorizontalAlign="Center"></FooterStyle>
					</asp:BoundColumn>
					<asp:BoundColumn HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" ItemStyle-Wrap="False" ItemStyle-Width="13%" HeaderStyle-Wrap="False" HeaderText="Reason" DataField="Reason_Name"></asp:BoundColumn>				
					<asp:BoundColumn ItemStyle-Wrap="False" ItemStyle-Width="10%" HeaderText="Employee" DataField="EMP_Name"></asp:BoundColumn>
					<asp:BoundColumn ItemStyle-Wrap="False" ItemStyle-Width="4%" HeaderText="Date" DataField="Date_Created" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundColumn>
				</Columns>
			</asp:datagrid>
			<br>
			<br>
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td align="right"><asp:label id="lblSignature" runat="server" Font-Size="small" Font-Names="Arial" Font-Bold="True" ForeColor="Black"></asp:label><br>
						<br>
						<br>
						<asp:label id="lblSignatureLine" runat="server" Font-Names="Arial" Font-Bold="True" ForeColor="Black">_________________________</asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
