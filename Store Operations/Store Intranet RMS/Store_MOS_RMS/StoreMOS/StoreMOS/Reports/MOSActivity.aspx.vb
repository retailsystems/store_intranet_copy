Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class MOSActivity
    Inherits System.Web.UI.Page
    Protected WithEvents lblReportName As System.Web.UI.WebControls.Label
    Protected WithEvents lblReportDate As System.Web.UI.WebControls.Label
    Protected WithEvents lblDate As System.Web.UI.WebControls.Label
    Protected WithEvents dgMOSItems As System.Web.UI.WebControls.DataGrid


    Private StoreCd As Integer
    Private StartDate As Date
    Private EndDate As Date
    Private intTotalMOSQty As Integer = 0
    Private decTotalRetail As Decimal = 0
    Private intMOSId As Integer
    Protected WithEvents lblSignature As System.Web.UI.WebControls.Label
    Protected WithEvents lblSignatureLine As System.Web.UI.WebControls.Label
    Private intReportType As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        StoreCd = Request("StoreCd")
        StartDate = Request("StartDate")
        EndDate = Request("EndDate")
        intMOSId = Request("MOSListId")
        intReportType = Request("RT")

        Select Case intReportType
            Case 1
                lblReportName.Text = "Store #" & StoreCd & " MOS Report"
                lblReportDate.Text = StartDate & " to " & EndDate
                lblSignature.Text = "DM Approval"
                CreateDateRangeReport()
            Case 2
                lblReportName.Text = "Store #" & StoreCd & " MOS Confirmation"
                lblReportDate.Text = Now.Date
                lblSignature.Text = "Management Approval"
                CreateMOSIdReport()
        End Select

    End Sub

    Private Sub CreateMOSIdReport()
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetMOSReportById " & intMOSId, objConnection)

        objDataReader = objCommand.ExecuteReader()

        dgMOSItems.DataSource = objDataReader
        dgMOSItems.DataBind()

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
    End Sub


    Private Sub CreateDateRangeReport()
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetMOSReport " & StoreCd & ",'" & StartDate & "','" & EndDate & "'", objConnection)

        objDataReader = objCommand.ExecuteReader()

        dgMOSItems.DataSource = objDataReader
        dgMOSItems.DataBind()

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
    End Sub

    Private Sub dgMOSItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgMOSItems.ItemDataBound

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim MOSCount As Integer = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "MOS_Qty"))
            Dim CURRCount As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "CURR"))

            intTotalMOSQty = intTotalMOSQty + MOSCount
            decTotalRetail = decTotalRetail + CURRCount
        ElseIf e.Item.ItemType = ListItemType.Footer Then
            e.Item.Cells(7).Text = intTotalMOSQty
            e.Item.Cells(5).Text = String.Format("{0:c}", decTotalRetail)
        End If

    End Sub

End Class
