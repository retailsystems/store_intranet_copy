Imports System.Data

Imports System.Collections


Public Class ItemInfo

    Private strSKUNum As String
    Public intXferType As Integer
    Public strXferShortDesc As String
    Public intItemQty As Integer
    Public intCustomerId As Integer
    Public intReceivedQty As Integer
    Public intDiscrepancy As Integer
    Public decRetailPrice As Decimal
    Public strVender As String
    Public strSize As String
    Public strDescription As String
    Public strDept As String
    Public strItemCd As String
    Public strClassCd As String
    Public strSubClassCd As String
    Public dblItemLBS As Double
    Public strDBConnection As String
    Public strRMSConnection As String
    Public strStore As String
    Public intMOS As Integer
    Public strErr As String
    Public intOrigXferType As Integer
    Public strOrigXferShortDesc As String
    Public booNewItem As Boolean
    Public booDeletedItem As Boolean
    Public decDiscrepancyAmount As Decimal
    Public inStoreQty As Integer

    Sub New()
        ' TODO: Complete member initialization 
    End Sub

    Property SKUNum()
        Get
            Return strSKUNum
        End Get
        Set(ByVal Value)
            strSKUNum = ConvertSKUNumber(Value)
        End Set
    End Property

    'Return Discrepency item amount
    ReadOnly Property ItemDiscrepencyAmount()
        Get
            Return Math.Abs(intItemQty - intReceivedQty)
        End Get
    End Property

    'Return Discrepency dollar amount
    ReadOnly Property CurrDiscrepencyAmount()
        Get
            Return Math.Abs(intItemQty - intReceivedQty) * decRetailPrice
        End Get
    End Property

    Public Sub New(ByVal DBConnection)
        strDBConnection = DBConnection
        booDeletedItem = False
    End Sub

    Public Sub Copy(ByVal Item As ItemInfo)

        strSKUNum = Item.strSKUNum
        intXferType = Item.intXferType
        strXferShortDesc = Item.intXferType
        intItemQty = Item.intItemQty
        intCustomerId = Item.intCustomerId
        intReceivedQty = Item.intReceivedQty
        intDiscrepancy = Item.intDiscrepancy
        decRetailPrice = Item.decRetailPrice
        strVender = Item.strVender
        strSize = Item.strSize
        strDescription = Item.strDescription
        strDept = Item.strDept
        strItemCd = Item.strItemCd
        strClassCd = Item.strClassCd
        strSubClassCd = Item.strSubClassCd
        dblItemLBS = Item.dblItemLBS
        strDBConnection = Item.strDBConnection
        strStore = Item.strStore
        intMOS = Item.intMOS
        strErr = Item.strErr
        intOrigXferType = Item.intOrigXferType
        strOrigXferShortDesc = Item.strOrigXferShortDesc
        decDiscrepancyAmount = Item.decDiscrepancyAmount
        booNewItem = Item.booNewItem
        booDeletedItem = Item.booDeletedItem

    End Sub

    Public Function GetItemData(ByVal SKU As Integer) As Boolean
        Dim isItemExist As Boolean = False

        Dim objConnection As New SqlClient.SqlConnection(strDBConnection)
        Dim objDataReader As SqlClient.SqlDataReader
        objConnection.Open()
        Dim objCommand As New SqlClient.SqlCommand("spGetItemAddToBox", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@Item_Id", SKU)
        objDataReader = objCommand.ExecuteReader()
        While objDataReader.Read
            SKUNum = Convert.ToString(objDataReader("SKU_Num"))
            decRetailPrice = objDataReader("CURR")
            strVender = objDataReader("VE_CD")
            strSize = objDataReader("SIZE_CD")
            strDescription = objDataReader("DES1")
            strDept = objDataReader("DEPT_CD")
            strItemCd = objDataReader("ITM_CD")
            strClassCd = objDataReader("CLASS_CD")
            strSubClassCd = objDataReader("SUBCLASS_CD")
            isItemExist = True
        End While

        objCommand.Dispose()
        objDataReader.Close()
        objConnection.Close()
        Return isItemExist
    End Function

    Public Function ConvertSKUNumber(ByVal InSKU As String) As String

        Dim tmpSKU As String

        If InSKU.Length > 9 Then
            If Mid(InSKU, 1, 2) = "00" Then
                tmpSKU = Mid(InSKU, 3, InSKU.Length - 2)
                Return Mid(tmpSKU, 1, 6) & "-" & Mid(tmpSKU, 7, 3)
            Else
                Return InSKU
            End If
        ElseIf Not InStr(InSKU, "-") And InSKU.Length = 9 Then
            Return Mid(InSKU, 1, 6) & "-" & Mid(InSKU, 7, 3)
        Else
            Return InSKU
        End If
    End Function
End Class
