<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ReportQuery.aspx.vb" Inherits="StoreMOS.ReportQuery"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Report Query</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body id="pageBody" runat="server">
		<form id="FormReportQuery" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server" />
			<div class="container home" role="main">
			<table width="100%">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<asp:label id="lblStore" runat="server" Visible="false" DESIGNTIMEDRAGDROP="42">Store</asp:label>&nbsp;
						<asp:dropdownlist id="ddlStore" runat="server" Visible="false" DataTextField="storenum" DataValueField="storenum"></asp:dropdownlist>&nbsp;&nbsp; 
						Date Range&nbsp;
						<asp:textbox id="txtStartDate" runat="server" Width="70px" MaxLength="10"></asp:textbox>&nbsp; 
						To&nbsp;
						<asp:textbox id="txtEndDate" runat="server" Width="70px" MaxLength="10"></asp:textbox></td>
				</tr>
			</table>
			<br>
			<table width="100%">
				<tr>
					<td>
						<hr>
					</td>
				</tr>
				<tr>
					<td align="right">
						<asp:button id="btnReport" runat="server" Text="View Report" CssClass="btn btn-danger"></asp:button>&nbsp;&nbsp;
						<asp:button id="btnReturn" runat="server" Text="Return" CssClass="btn btn-danger"></asp:button>&nbsp;&nbsp;
						<asp:button id="btnHome" runat="server" Text="Home" CssClass="btn btn-danger"></asp:button></td>
				</tr>
			</table>
			</div>
		</form>
		<script language="javascript">
		<!--
			document.forms[0].txtStartDate.focus();	
		//-->
		</script>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
