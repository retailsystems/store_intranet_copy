<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Error.aspx.vb" Inherits="StoreMOS._Error"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Error</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server" onsubmit="return checkSubmit();">
			<table width="100%" height="100%">
				<tr>
					<td height="1%" align="middle" colSpan="2">
						<asp:label id="lblHeader" runat="server" EnableViewState="False">Error!</asp:label></td>
				</tr>
				<tr>
					<td height="99%" align="middle">
						<asp:Label id="lblErrDesc" runat="server"></asp:Label>
						<br>
						<br>
						<input class="btn btn-danger" type="button" value="Close" onclick="javascript:window.close();">
					</td>
				</tr>
			</table>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
