﻿Imports System.Configuration
Imports System.Xml.Serialization

Module Module1

    Sub Main()
        Try
            Console.WriteLine("Started")
            If Not CheckForExistingInstance() Then
                Dim MOSDataInfo As Dictionary(Of Integer, Integer) = New Dictionary(Of Integer, Integer)
                Dim mosListLst As List(Of MOSList)
                mosListLst = GetMOSList()
                For Each mosListObj As MOSList In mosListLst
                    Dim itemLst As List(Of Item) = GetItemList(mosListObj.MOSListId)
                    MOSDataInfo.Add(mosListObj.MOSListId, itemLst.Count)
                    If (itemLst.Count > 0) Then
                        itemLst = itemLst.OrderByDescending(Function(x) x.DateCreated).ToList()
                        Dim lstCount As Integer = itemLst.Count
                        Dim startpos As Integer = 0
                        Try
                            While (True)
                                Dim pixBridge As PIXBridge = New PIXBridge()
                                pixBridge.PIX = New List(Of PIX)
                                Dim isExitWhile As Boolean = False
                                For i As Integer = startpos To (startpos + 99)
                                    If i > lstCount - 1 Then
                                        isExitWhile = True
                                        Exit For
                                    End If

                                    Dim pix As PIX = New PIX()
                                    Dim pixFields As PIXFields = New PIXFields()
                                    'Dim SKUDefinitionFields As SKUDefinition = New SKUDefinition()
                                    Dim SKUDefinitionObj As SKUDefinition = New SKUDefinition()
                                    Dim SubSKUFields As SubSKUFields = New SubSKUFields()
                                    pix.PIXFields = pixFields
                                    pix.SKUDefinition = SKUDefinitionObj
                                    pix.SubSKUFields = SubSKUFields
                                    pixFields.TransReasonCode = itemLst(i).RMSID
                                    pixFields.InvAdjustmentQty = itemLst(i).MOSQty
                                    pixFields.InvAdjustmentType = itemLst(i).FromDisposition
                                    pixFields.DateCreated = itemLst(i).DateCreated
                                    pixFields.Warehouse = "STOREMOS-" & mosListObj.StoreId
                                    pixFields.UserID = mosListObj.EmpId
                                    SKUDefinitionObj.StyleSuffix = itemLst(i).SKUNum
                                    initFields(pixFields)
                                    initSkufields(SKUDefinitionObj)
                                    initSubSKUFields(SubSKUFields)
                                    initPixFields(pix)
                                    pixBridge.PIX.Add(pix)
                                Next
                                Dim directoryPath As String = System.Configuration.ConfigurationSettings.AppSettings("XMLDirectoryPath")
                                Dim fullPath As String = directoryPath & "INVAdjPixMessage_" & Guid.NewGuid().ToString() & "_" & DateTime.Now.ToString("MM-dd-yyyy HH-mm-ss")
                                Dim serializer As New XmlSerializer(GetType(List(Of PIX)), New XmlRootAttribute("PIXBridge"))
                                Using file As System.IO.FileStream = System.IO.File.Open(fullPath, IO.FileMode.OpenOrCreate, IO.FileAccess.Write)
                                    serializer.Serialize(file, pixBridge.PIX)

                                End Using
                                If isExitWhile Then
                                    Exit While
                                End If
                                startpos = startpos + 100
                            End While
                        Catch ex As Exception
                            Console.WriteLine(ex.Message)
                        End Try
                       

                    End If
                Next
                If MOSDataInfo.Count > 0 Then
                    SendMail(MOSDataInfo)
                End If
                ' Console.ReadLine()
                'Else
                '    Console.WriteLine("Not processed")
                '    'Console.ReadLine()
            End If
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub
    Private Sub SendMail(ByVal mosData As Dictionary(Of Integer, Integer))

    End Sub
    Public Function GetMOSList() As List(Of MOSList)
        Dim mosList As List(Of MOSList) = New List(Of MOSList)
        Dim objConnection As New SqlClient.SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("strMainDB"))
        Dim objDataReader As SqlClient.SqlDataReader
        objConnection.Open()
        Dim objCommand As New SqlClient.SqlCommand("spGetMOSItems_ToProcess", objConnection)
        objDataReader = objCommand.ExecuteReader()
        While objDataReader.Read
            Dim mosListObj As MOSList = New MOSList()
            mosListObj.MOSListId = Integer.Parse(objDataReader("MOSList_Id"))
            mosListObj.StoreId = Integer.Parse(objDataReader("Store_Cd"))
            mosListObj.EmpId = objDataReader("Emp_Id")
            mosList.Add(mosListObj)
        End While
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        Return mosList
    End Function
    Public Function GetItemList(ByVal MOSId As Integer) As List(Of Item)
        Dim itemLst As List(Of Item) = New List(Of Item)
        Try
            Dim objConnection As New SqlClient.SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("strMainDB"))
            Dim objDataReader As SqlClient.SqlDataReader
            objConnection.Open()
            Dim objCommand As New SqlClient.SqlCommand("spGetMOSItems", objConnection)
            objCommand.CommandType = CommandType.StoredProcedure
            Dim parListId As SqlClient.SqlParameter = objCommand.Parameters.Add("@MOSList_Id", SqlDbType.Int)
            parListId.Direction = ParameterDirection.Input
            parListId.Value = MOSId

            objDataReader = objCommand.ExecuteReader()
            While objDataReader.Read
                Dim item As Item = New Item()
                item.SKUNum = Convert.ToString(objDataReader("SKU_Num"))
                item.DateCreated = Convert.ToDateTime(objDataReader("Date_Created"))
                item.FromDisposition = Convert.ToString(objDataReader("From_Dis"))
                item.MOSQty = Convert.ToString(objDataReader("MOS_Qty"))
                item.RMSID = Convert.ToString(objDataReader("RMS_ID"))
                item.ToDisposition = Convert.ToString(objDataReader("To_Dis"))
                itemLst.Add(item)
            End While
            objDataReader.Close()
            objCommand.Dispose()
            objConnection.Close()
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try

        Return itemLst
    End Function
    Private Sub initFields(ByRef pix As PIXFields)
        pix.CaseNumber = ""
        pix.UnitOfMeasure = ""
        'pix.Warehouse = ""
        pix.ReferenceWhse = ""
        'pix.TransReasonCode = ""
        pix.ReceiptsVariance = ""
        pix.ReceiptsCompleted = ""
        pix.CasesShipped = ""
        pix.UnitsShipped = ""
        pix.CasesReceived = ""
        pix.UnitsReceived = "0"
        pix.ReferenceCode1 = ""
        pix.Reference1 = ""
        pix.ReferenceCode2 = ""
        pix.Reference2 = ""
        pix.ReferenceCode3 = ""
        pix.Reference3 = ""
        pix.ReferenceCode4 = ""
        pix.Reference4 = ""
        pix.ReferenceCode5 = ""
        pix.Reference5 = ""
        pix.ReferenceCode6 = ""
        pix.ReferenceField6 = ""
        pix.ReferenceCode7 = ""
        pix.ReferenceField7 = ""
        pix.ReferenceCode8 = ""
        pix.ReferenceField8 = ""
        pix.ReferenceCode9 = ""
        pix.ReferenceField9 = ""
        pix.ReferenceCode10 = ""
        pix.ReferenceField10 = ""
        pix.ActionCode = "09"
        pix.CustomReference = ""
        ' pix.UserID = ""
        pix.As400UserID = ""
        pix.ErrorComment = ""
        pix.WeightAdjustmentQuantity = ""
        pix.WeightAdjustmentType = ""
        pix.ModifyDateTime = ""





    End Sub
    Private Sub initSkufields(ByRef sku As SKUDefinition)
        sku.Company = ""
        sku.Division = ""
        sku.Season = ""
        sku.SeasonYear = ""
        sku.Style = ""
        'sku.StyleSuffix = ""
        sku.Color = ""
        sku.ColorSuffix = ""
        sku.SecDimension = ""
        sku.Quality = ""
        sku.SizeRangeCode = ""
        sku.SizeRelPosninTable = ""
        sku.SizeDesc = ""
        sku.SkuID = ""
    End Sub
    Private Sub initSubSKUFields(ByRef subsku As SubSKUFields)
        subsku.InventoryType = ""
        subsku.ProductStatus = ""
        subsku.BatchNumber = ""
        subsku.SKUAttribute1 = ""
        subsku.SKUAttribute2 = ""
        subsku.SKUAttribute3 = ""
        subsku.SKUAttribute4 = ""
        subsku.SKUAttribute5 = ""
        subsku.CountryOfOrigin = ""
    End Sub
    Private Sub initPixFields(ByRef pix As PIX)
        pix.TransactionType = "300"
        pix.TransactionCode = "01"
        pix.TransactionNumber = ""
        pix.SequenceNumber = ""
    End Sub
    Public Function CheckForExistingInstance() As Boolean
        'Get number of processes of you program
        If Process.GetProcessesByName _
          (Process.GetCurrentProcess.ProcessName).Length > 1 Then

            Console.WriteLine("Another Instance of this process is already running")
            Return True
        Else
            Return False
        End If
    End Function
End Module
