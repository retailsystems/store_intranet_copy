﻿Imports System
Imports System.Xml.Serialization
Imports System.Collections.Generic


<XmlRoot(ElementName:="SKUDefinition")>
Public Class SKUDefinition
    <XmlElement(ElementName:="Company")>
    Public Property Company As String
    <XmlElement(ElementName:="Division")>
    Public Property Division As String
    <XmlElement(ElementName:="Season")>
    Public Property Season As String
    <XmlElement(ElementName:="SeasonYear")>
    Public Property SeasonYear As String
    <XmlElement(ElementName:="Style")>
    Public Property Style As String
    <XmlElement(ElementName:="StyleSuffix")>
    Public Property StyleSuffix As String
    <XmlElement(ElementName:="Color")>
    Public Property Color As String
    <XmlElement(ElementName:="ColorSuffix")>
    Public Property ColorSuffix As String
    <XmlElement(ElementName:="SecDimension")>
    Public Property SecDimension As String
    <XmlElement(ElementName:="Quality")>
    Public Property Quality As String
    <XmlElement(ElementName:="SizeRangeCode")>
    Public Property SizeRangeCode As String
    <XmlElement(ElementName:="SizeRelPosninTable")>
    Public Property SizeRelPosninTable As String
    <XmlElement(ElementName:="SizeDesc")>
    Public Property SizeDesc As String
    <XmlElement(ElementName:="SkuID")>
    Public Property SkuID As String
End Class

<XmlRoot(ElementName:="SubSKUFields")>
Public Class SubSKUFields
    <XmlElement(ElementName:="InventoryType")>
    Public Property InventoryType As String
    <XmlElement(ElementName:="ProductStatus")>
    Public Property ProductStatus As String
    <XmlElement(ElementName:="BatchNumber")>
    Public Property BatchNumber As String
    <XmlElement(ElementName:="SKUAttribute1")>
    Public Property SKUAttribute1 As String
    <XmlElement(ElementName:="SKUAttribute2")>
    Public Property SKUAttribute2 As String
    <XmlElement(ElementName:="SKUAttribute3")>
    Public Property SKUAttribute3 As String
    <XmlElement(ElementName:="SKUAttribute4")>
    Public Property SKUAttribute4 As String
    <XmlElement(ElementName:="SKUAttribute5")>
    Public Property SKUAttribute5 As String
    <XmlElement(ElementName:="CountryOfOrigin")>
    Public Property CountryOfOrigin As String
End Class

<XmlRoot(ElementName:="PIXFields")>
Public Class PIXFields
    <XmlElement(ElementName:="DateCreated")>
    Public Property DateCreated As String
    <XmlElement(ElementName:="CaseNumber")>
    Public Property CaseNumber As String
    <XmlElement(ElementName:="InvAdjustmentQty")>
    Public Property InvAdjustmentQty As String
    <XmlElement(ElementName:="UnitOfMeasure")>
    Public Property UnitOfMeasure As String
    <XmlElement(ElementName:="InvAdjustmentType")>
    Public Property InvAdjustmentType As String
    <XmlElement(ElementName:="Warehouse")>
    Public Property Warehouse As String
    <XmlElement(ElementName:="ReferenceWhse")>
    Public Property ReferenceWhse As String
    <XmlElement(ElementName:="TransReasonCode")>
    Public Property TransReasonCode As String
    <XmlElement(ElementName:="ReceiptsVariance")>
    Public Property ReceiptsVariance As String
    <XmlElement(ElementName:="ReceiptsCompleted")>
    Public Property ReceiptsCompleted As String
    <XmlElement(ElementName:="CasesShipped")>
    Public Property CasesShipped As String
    <XmlElement(ElementName:="UnitsShipped")>
    Public Property UnitsShipped As String
    <XmlElement(ElementName:="CasesReceived")>
    Public Property CasesReceived As String
    <XmlElement(ElementName:="UnitsReceived")>
    Public Property UnitsReceived As String
    <XmlElement(ElementName:="ReferenceCode1")>
    Public Property ReferenceCode1 As String
    <XmlElement(ElementName:="Reference1")>
    Public Property Reference1 As String
    <XmlElement(ElementName:="ReferenceCode2")>
    Public Property ReferenceCode2 As String
    <XmlElement(ElementName:="Reference2")>
    Public Property Reference2 As String
    <XmlElement(ElementName:="ReferenceCode3")>
    Public Property ReferenceCode3 As String
    <XmlElement(ElementName:="Reference3")>
    Public Property Reference3 As String
    <XmlElement(ElementName:="ReferenceCode4")>
    Public Property ReferenceCode4 As String
    <XmlElement(ElementName:="Reference4")>
    Public Property Reference4 As String
    <XmlElement(ElementName:="ReferenceCode5")>
    Public Property ReferenceCode5 As String
    <XmlElement(ElementName:="Reference5")>
    Public Property Reference5 As String
    <XmlElement(ElementName:="ReferenceCode6")>
    Public Property ReferenceCode6 As String
    <XmlElement(ElementName:="ReferenceField6")>
    Public Property ReferenceField6 As String
    <XmlElement(ElementName:="ReferenceCode7")>
    Public Property ReferenceCode7 As String
    <XmlElement(ElementName:="ReferenceField7")>
    Public Property ReferenceField7 As String
    <XmlElement(ElementName:="ReferenceCode8")>
    Public Property ReferenceCode8 As String
    <XmlElement(ElementName:="ReferenceField8")>
    Public Property ReferenceField8 As String
    <XmlElement(ElementName:="ReferenceCode9")>
    Public Property ReferenceCode9 As String
    <XmlElement(ElementName:="ReferenceField9")>
    Public Property ReferenceField9 As String
    <XmlElement(ElementName:="ReferenceCode10")>
    Public Property ReferenceCode10 As String
    <XmlElement(ElementName:="ReferenceField10")>
    Public Property ReferenceField10 As String
    <XmlElement(ElementName:="ActionCode")>
    Public Property ActionCode As String
    <XmlElement(ElementName:="CustomReference")>
    Public Property CustomReference As String
    <XmlElement(ElementName:="UserID")>
    Public Property UserID As String
    <XmlElement(ElementName:="As400UserID")>
    Public Property As400UserID As String
    <XmlElement(ElementName:="ErrorComment")>
    Public Property ErrorComment As String
    <XmlElement(ElementName:="WeightAdjustmentQuantity")>
    Public Property WeightAdjustmentQuantity As String
    <XmlElement(ElementName:="WeightAdjustmentType")>
    Public Property WeightAdjustmentType As String
    <XmlElement(ElementName:="ModifyDateTime")>
    Public Property ModifyDateTime As String
End Class

<XmlRoot(ElementName:="PIX")>
Public Class PIX
    <XmlElement(ElementName:="TransactionType")>
    Public Property TransactionType As String
    <XmlElement(ElementName:="TransactionCode")>
    Public Property TransactionCode As String
    <XmlElement(ElementName:="TransactionNumber")>
    Public Property TransactionNumber As String
    <XmlElement(ElementName:="SequenceNumber")>
    Public Property SequenceNumber As String
    <XmlElement(ElementName:="SKUDefinition")>
    Public Property SKUDefinition As SKUDefinition
    <XmlElement(ElementName:="SubSKUFields")>
    Public Property SubSKUFields As SubSKUFields
    <XmlElement(ElementName:="PIXFields")>
    Public Property PIXFields As PIXFields
End Class

<XmlRoot(ElementName:="PIXBridge")>
Public Class PIXBridge
    <XmlElement(ElementName:="PIX")>
    Public Property PIX As List(Of PIX)
    <XmlAttribute(AttributeName:="version")>
    Public Property Version As String
    <XmlAttribute(AttributeName:="timestamp")>
    Public Property Timestamp As String
End Class
