<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MOSConfirm.aspx.vb" Inherits="StoreMOS.MOSConfirm"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MOS Confirm</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body>
		<form id="FormMOSConfirm" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server" />
			<div class="container home" role="main">
			<br>
			<table height="220" width="750">
				<tr>
					<td align="center">
						<iframe id="ReportFrame" src="" width="100%" height="100%" runat="server"></iframe>
					</td>
				</tr>
			</table>
			<br>
			<table width="750">
				<tr>
					<td>
						<hr>
					</td>
				</tr>
				<tr>
					<td align="right">
						<asp:button id="btnDone" runat="server" Text="Done" CssClass="btn btn-danger"></asp:button>&nbsp;&nbsp;
						<input id="btnPrint" onclick="javascript:ReportFrame.focus(); ReportFrame.print();" type="button" value="Print" class="btn btn-danger">&nbsp;&nbsp;
						<asp:button id="btnHome" runat="server" Text="Home" CssClass="btn btn-danger"></asp:button></td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
