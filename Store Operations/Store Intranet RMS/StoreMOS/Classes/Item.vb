Imports System.Data
Imports System.Data.OleDb
Imports System.Collections


Public Class Item

    Private strSKUNum As String
    Public intXferType As Integer
    Public strXferShortDesc As String
    Public intItemQty As Integer
    Public intCustomerId As Integer
    Public intReceivedQty As Integer
    Public intDiscrepancy As Integer
    Public decRetailPrice As Decimal
    Public strVender As String
    Public strSize As String
    Public strDescription As String
    Public strDept As String
    Public strItemCd As String
    Public strClassCd As String
    Public strSubClassCd As String
    Public dblItemLBS As Double
    Public strDBConnection As String
    Public strRMSConnection As String
    Public strStore As String
    Public intMOS As Integer
    Public strErr As String
    Public intOrigXferType As Integer
    Public strOrigXferShortDesc As String
    Public booNewItem As Boolean
    Public booDeletedItem As Boolean
    Public decDiscrepancyAmount As Decimal
    Public inStoreQty As Integer

    Property SKUNum()
        Get
            Return strSKUNum
        End Get
        Set(ByVal Value)
            strSKUNum = ConvertSKUNumber(Value)
        End Set
    End Property

    'Return Discrepency item amount
    ReadOnly Property ItemDiscrepencyAmount()
        Get
            Return Math.Abs(intItemQty - intReceivedQty)
        End Get
    End Property

    'Return Discrepency dollar amount
    ReadOnly Property CurrDiscrepencyAmount()
        Get
            Return Math.Abs(intItemQty - intReceivedQty) * decRetailPrice
        End Get
    End Property

    Public Sub New(ByVal DBConnection)
        strDBConnection = DBConnection
        booDeletedItem = False
    End Sub

    Public Sub Copy(ByVal Item As Item)

        strSKUNum = Item.strSKUNum
        intXferType = Item.intXferType
        strXferShortDesc = Item.intXferType
        intItemQty = Item.intItemQty
        intCustomerId = Item.intCustomerId
        intReceivedQty = Item.intReceivedQty
        intDiscrepancy = Item.intDiscrepancy
        decRetailPrice = Item.decRetailPrice
        strVender = Item.strVender
        strSize = Item.strSize
        strDescription = Item.strDescription
        strDept = Item.strDept
        strItemCd = Item.strItemCd
        strClassCd = Item.strClassCd
        strSubClassCd = Item.strSubClassCd
        dblItemLBS = Item.dblItemLBS
        strDBConnection = Item.strDBConnection
        strStore = Item.strStore
        intMOS = Item.intMOS
        strErr = Item.strErr
        intOrigXferType = Item.intOrigXferType
        strOrigXferShortDesc = Item.strOrigXferShortDesc
        decDiscrepancyAmount = Item.decDiscrepancyAmount
        booNewItem = Item.booNewItem
        booDeletedItem = Item.booDeletedItem

    End Sub

    'Retreive SKU data from GERS
    Public Function GetItemDataRMS(ByVal intStore As Integer) As Boolean

        Dim strSQL As String
        Dim objORACommand As New OleDbCommand
        Dim objORAConnection As New OleDbConnection(strDBConnection)
        Dim objORADataReader As OleDbDataReader
        Dim booItemExist As Boolean = False
        Dim strSizeColumn As String
        Dim htSize As Hashtable = New Hashtable


        objORAConnection.Open()

        strSQL = "select * from httd_v_item_loc where item ='" & strSKUNum & "' and rownum = 1 and tran_level = item_level"


        objORACommand.Connection = objORAConnection
        objORACommand.CommandText = strSQL
        objORADataReader = objORACommand.ExecuteReader()

        If objORADataReader.HasRows And objORADataReader.Read() Then
            decRetailPrice = objORADataReader("unit_retail")
            strVender = Nothing
            htSize.Add("DIFF_TYPE_1", IIf(IsDBNull(objORADataReader("DIFF_TYPE_1").ToString), "", objORADataReader("DIFF_TYPE_1").ToString))
            htSize.Add("DIFF_TYPE_2", IIf(IsDBNull(objORADataReader("DIFF_TYPE_2").ToString), "", objORADataReader("DIFF_TYPE_2").ToString))
            htSize.Add("DIFF_TYPE_3", IIf(IsDBNull(objORADataReader("DIFF_TYPE_3").ToString), "", objORADataReader("DIFF_TYPE_3").ToString))
            htSize.Add("DIFF_TYPE_4", IIf(IsDBNull(objORADataReader("DIFF_TYPE_4").ToString), "", objORADataReader("DIFF_TYPE_4").ToString))
            strSizeColumn = GetSizeColumn(htSize)
            If (Not strSizeColumn Is Nothing) Then
                strSize = objORADataReader(strSizeColumn).ToString
            Else
                strSize = Nothing
            End If
            strDescription = objORADataReader("SHORT_DESC").ToString
            strDept = objORADataReader("DEPT").ToString
            strItemCd = objORADataReader("ITEM").ToString
            strClassCd = objORADataReader("CLASS").ToString
            strSubClassCd = objORADataReader("SUBCLASS").ToString
            'inStoreQty = Integer.Parse(objORADataReader("AVAILABLE_QTY"))
            booItemExist = True
        End If

        objORAConnection.Close()
        objORACommand.Dispose()
        objORADataReader.Close()

        Return booItemExist

    End Function

    Private Function GetSizeColumn(ByVal htSize As Hashtable)
        Dim i As Integer
        Dim entry As DictionaryEntry
        Dim strColumn As String

        For Each entry In htSize
            If entry.Value.ToString.Trim.ToUpper = "SIZE" Then
                strColumn = "DIFF_" & entry.Key.ToString().Substring(entry.Key.ToString().Length - 1, 1)
                Exit For
            End If

        Next

        Return strColumn
    End Function
    Public Function GetItemData() As Boolean
        Dim strSQL As String
        Dim objORACommand As New OleDbCommand
        Dim objORAConnection As New OleDbConnection(strDBConnection)
        Dim objORADataReader As OleDbDataReader
        Dim booItemExist As Boolean = False

        objORAConnection.Open()

        strSQL = " SELECT DISTINCT U.SKU_NUM, A.DES1, hottopic.ht_get_ret_prc.get_sku_ret_prc(u.sku_num) CURR, U.SIZE_CD, A.DES1, A.VE_CD, A.ITM_CD, A.DEPT_CD, A.CLASS_CD, A.SUBCLASS_CD "
        strSQL = strSQL & " FROM gm_itm A, GM_SKU U "
        strSQL = strSQL & " WHERE A.ITM_CD = U.ITM_CD AND U.SKU_NUM = '" & Replace(Me.SKUNum, "'", "''") & "' "

        objORACommand.Connection = objORAConnection
        objORACommand.CommandText = strSQL
        objORADataReader = objORACommand.ExecuteReader()

        If objORADataReader.Read() Then
            decRetailPrice = objORADataReader("CURR")
            strVender = objORADataReader("VE_CD").ToString
            strSize = objORADataReader("SIZE_CD").ToString
            strDescription = objORADataReader("DES1").ToString
            strDept = objORADataReader("DEPT_CD").ToString
            strItemCd = objORADataReader("ITM_CD").ToString
            strClassCd = objORADataReader("CLASS_CD").ToString
            strSubClassCd = objORADataReader("SUBCLASS_CD").ToString

            booItemExist = True
        End If

        objORAConnection.Close()
        objORACommand.Dispose()
        objORADataReader.Close()

        Return booItemExist
    End Function

    Public Function GetCurrentStoreQtyRMS(ByVal Store_Cd As String) As Integer
        Dim strSQL As String
        Dim objORACommand As New OleDbCommand
        Dim objORAConnection As New OleDbConnection(strDBConnection)
        Dim objORADataReader As OleDbDataReader
        Dim intSKUQty As Integer

        objORAConnection.Open()

        strSQL = "Select available_qty from httd_v_item_loc where item ='" & strSKUNum & "' and store='" & Store_Cd & "'"
       

        objORACommand.Connection = objORAConnection
        objORACommand.CommandText = strSQL
        objORADataReader = objORACommand.ExecuteReader()

        If objORADataReader.Read() Then
            If Not IsDBNull(objORADataReader("available_qty")) Then
                intSKUQty = objORADataReader("available_qty")
            Else
                intSKUQty = 0
            End If
        Else
            intSKUQty = 0
        End If

        objORAConnection.Close()
        objORACommand.Dispose()
        objORADataReader.Close()

        Return intSKUQty

    End Function

    Public Function GetCurrentStoreQty(ByVal Store_Cd As String) As Integer

        Dim strSQL As String
        Dim objORACommand As New OleDbCommand
        Dim objORAConnection As New OleDbConnection(strDBConnection)
        Dim objORADataReader As OleDbDataReader
        Dim intSKUQty As Integer

        objORAConnection.Open()

        strSQL = "Select sum(avail_qty) as TOT_AVAIL_QTY "
        strSQL = strSQL & "from gm_inv_loc "
        strSQL = strSQL & "where loc_cd = 'RECV' "
        strSQL = strSQL & "and sku_num = '" & Replace(Me.SKUNum, "'", "''") & "' "
        strSQL = strSQL & "and store_cd = '" & Replace(Store_Cd.ToString.PadLeft(4, "0"c), "'", "''") & "' "

        objORACommand.Connection = objORAConnection
        objORACommand.CommandText = strSQL
        objORADataReader = objORACommand.ExecuteReader()

        If objORADataReader.Read() Then
            If Not IsDBNull(objORADataReader("TOT_AVAIL_QTY")) Then
                intSKUQty = objORADataReader("TOT_AVAIL_QTY")
            Else
                intSKUQty = 0
            End If
        Else
            intSKUQty = 0
        End If

        objORAConnection.Close()
        objORACommand.Dispose()
        objORADataReader.Close()

        Return intSKUQty

    End Function

    'Pad SKU for GERS query
    Public Function ConvertSKUNumber(ByVal InSKU As String) As String

        Dim tmpSKU As String

        If InSKU.Length > 9 Then
            If Mid(InSKU, 1, 2) = "00" Then
                tmpSKU = Mid(InSKU, 3, InSKU.Length - 2)
                Return Mid(tmpSKU, 1, 6) & "-" & Mid(tmpSKU, 7, 3)
            Else
                Return InSKU
            End If
        ElseIf Not InStr(InSKU, "-") And InSKU.Length = 9 Then
            Return Mid(InSKU, 1, 6) & "-" & Mid(InSKU, 7, 3)
        Else
            Return InSKU
        End If

    End Function


End Class
