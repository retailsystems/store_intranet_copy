Public Class Security

    Public Sub New()

    End Sub

    Public Function HasAccess(ByVal objUser As User, ByVal UserFunction As String) As Boolean

        Dim booHasAccess As Boolean = False
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        Dim objSQLDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        'strStoredProcedure = "spGetSecurityAccess '" & objUser.UserId.ToString.PadLeft(4, "0"c) & "','" & objUser.JobCode & "','" & UserFunction & "' "
        '6 digit empid change by Lan on 102232010
        strStoredProcedure = "spGetSecurityAccess '" & objUser.UserId.ToString.PadLeft(5, "0"c) & "','" & objUser.JobCode & "','" & UserFunction & "' "

        Dim objACommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        objSQLDataReader = objACommand.ExecuteReader()

        Do While objSQLDataReader.Read

            If objSQLDataReader("Access").ToString.ToUpper.Trim = "Y" Then
                booHasAccess = True
            Else
                booHasAccess = False
                Exit Do
            End If

        Loop

        objSQLDataReader.Close()
        objACommand.Dispose()
        objConnection.Close()

        Return booHasAccess

    End Function

End Class
