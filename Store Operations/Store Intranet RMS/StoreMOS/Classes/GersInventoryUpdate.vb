Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class GersInventoryUpdate

    Public strBoxId As String
    Public strTrackingNum As String
    Public strBoxStatusDesc As String
    Public strSendingStoreNum As String
    Public strSendingStoreName As String
    Public strDBConnection As String
    Public strReceivingStoreNum As String
    Public strReceivingStoreName As String
    Public dteShipDate As Date
    Public dteReceiveDate As Date
    Public strEmpId As String

    Public strSkuNum As String
    Public strQty As Integer
    Public strLnNum As Integer
    Public strRetPrc As Decimal
    Public strShipNum As String
    Public strXferType As String
    Public strDefaultLocation As String = "RECV"

    Dim connHeaderSQL As System.Data.SqlClient.SqlConnection
    Dim connHeaderGERS As System.Data.OleDb.OleDbConnection

    Dim blnGERSUpdate As Boolean

    Dim sysdate As Date

    Public Sub GetBoxHeader(ByVal BoxId As String, ByVal MOS As Boolean)
        'get data for the box header
        sysdate = Today

        ConnSQLOpen()

        'Dim objConnection As New SqlClient.SqlConnection(strDBConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeaderDT", connHeaderSQL)
        objCommand.CommandType = CommandType.StoredProcedure
        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = BoxId

        objDataReader = objCommand.ExecuteReader()

        If objDataReader.Read() Then

            strBoxStatusDesc = objDataReader("Status_Desc").ToString
            strSendingStoreNum = objDataReader("SStoreNum").ToString
            strReceivingStoreNum = objDataReader("RStoreNum").ToString
            strBoxId = objDataReader("Box_Id").ToString
            dteShipDate = objDataReader("Shipment_Date").ToString
            dteReceiveDate = objDataReader("Received_Date").ToString
            strEmpId = objDataReader("Emp_Id").ToString

            make_store_number()

            If strBoxStatusDesc = "Received" Then
                ConnSQLClose()
                GetBoxItems(BoxId)
                If MOS Then
                    GetBoxItemsMOS(BoxId)
                End If
            End If

        End If

        objDataReader.Close()
        objCommand.Dispose()


    End Sub
    Public Sub GetBoxItems(ByVal BoxId As String)
        'get data for the box items
        ConnSQLOpen()

        Dim objDataReader As SqlClient.SqlDataReader

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxItemsType", connHeaderSQL)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = BoxId

        objDataReader = objCommand.ExecuteReader()

        If objDataReader.Read() Then

            strXferType = objDataReader("Xfer_Type_Cd").ToString

            strSkuNum = objDataReader("Sku_Num").ToString
            strQty = objDataReader("Received_Qty").ToString

            strLnNum = 1

            GetShipmentNumber()

            Populate_POS_INV_TRN_LN()

            strLnNum = 2
            Do While objDataReader.Read

                strSkuNum = objDataReader("Sku_Num").ToString
                strQty = objDataReader("Received_Qty").ToString
                strRetPrc = CType(objDataReader("CURR").ToString, Decimal)
                Populate_POS_INV_TRN_LN()
                strLnNum = strLnNum + 1
            Loop

            Populate_POS_INV_TRN()


        End If
        objDataReader.Close()
        objCommand.Dispose()

        ConnSQLClose()

    End Sub

    Public Sub GetBoxItemsMOS(ByVal BoxId As String)
        'get data for the box items
        ConnSQLOpen()

        Dim objDataReader As SqlClient.SqlDataReader

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxItemsTypeMOS", connHeaderSQL)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = BoxId

        objDataReader = objCommand.ExecuteReader()

        If objDataReader.Read() Then
            strSkuNum = objDataReader("Sku_Num").ToString
            strQty = objDataReader("Received_Qty").ToString
            MOS_Procedure()
            Do While objDataReader.Read
                strSkuNum = objDataReader("Sku_Num").ToString
                strQty = objDataReader("Received_Qty").ToString
                MOS_Procedure()
            Loop

        End If
        objDataReader.Close()
        objCommand.Dispose()

        ConnSQLClose()

    End Sub

    Public Function Store_MOS(ByVal str_sku_num, ByVal str_store_cd, ByVal str_qty) As Boolean

        Try

            strSkuNum = str_sku_num
            strQty = str_qty
            strSendingStoreNum = str_store_cd

            make_store_number()

            MOS_Procedure()

            Return True

        Catch
            Return False
        End Try


    End Function



    Public Sub MOS_Procedure()
        'for xfer_type 11, mark merchandise out of stock
        ConnGERSOpen()

        Dim strSQL As String

        'if the store is doing MOS, there is no box id, so put in a fake

        If strBoxId = Nothing Then
            strBoxId = "0000-9999999"
        End If

        sysdate = Today

        strSQL = " INSERT INTO TEMP_MOS "
        strSQL = strSQL & " (trn_dt, box_id, store_cd, sku_num, loc_cd, qty, processed )"
        strSQL = strSQL & " VALUES (to_date('" & sysdate & "', 'MM/DD/YYYY'),  "
        strSQL = strSQL & " '" & strBoxId & "',  "
        strSQL = strSQL & " '" & strSendingStoreNum & "', '" & strSkuNum & "', '" & strDefaultLocation & "' , '" & strQty & "', 'N') "

        Dim cmdOraDD As System.Data.OleDb.OleDbCommand
        cmdOraDD = New OleDbCommand(strSQL, connHeaderGERS)
        cmdOraDD.CommandType = CommandType.Text
        cmdOraDD.ExecuteNonQuery()


        ConnGERSClose()

    End Sub


    Public Sub GetShipmentNumber()
        'the sequence generator produces the doc_num
        ConnGERSOpen()

        Dim strSQL As String
        Dim cmdOraDD As System.Data.OleDb.OleDbCommand
        strSQL = " SELECT SHIP_SEQ_NUM.NEXTVAL as ShipNum FROM SYS.DUAL "

        cmdOraDD = New OleDbCommand(strSQL, connHeaderGERS)
        cmdOraDD.CommandType = CommandType.Text
        cmdOraDD.ExecuteNonQuery()

        Dim drOraDD As System.Data.OleDb.OleDbDataReader
        drOraDD = cmdOraDD.ExecuteReader

        If drOraDD.Read Then
            strShipNum = drOraDD("ShipNum")
        End If

        ConnGERSClose()

    End Sub



    Public Sub Populate_POS_INV_TRN()
        'insert data into the holding TEMP_POS_INV_TRN table in GERS

        ConnGERSOpen()

        Dim strSQL As String

        strSQL = " INSERT INTO TEMP_POS_INV_TRN "
        strSQL = strSQL & " ( trn_dt, trn_time, box_id,  "
        strSQL = strSQL & " shipment_date, "
        strSQL = strSQL & " received_date, "
        strSQL = strSQL & " store_cd, term_num, trn_num, "
        strSQL = strSQL & " doc_num, trn_tp, po_num, cshr_num, sess_num, other_store_cd, "
        strSQL = strSQL & " rcv_pcs, reason_cd, ship_tp, stat_cd, processed) "
        strSQL = strSQL & " VALUES (to_date('" & sysdate & "', 'MM/DD/YYYY'),  "
        strSQL = strSQL & " TO_CHAR(sysdate, 'SSSSS'), "
        strSQL = strSQL & " '" & strBoxId & "',  "
        strSQL = strSQL & " to_date('" & dteShipDate & "', 'MM/DD/YYYY'), "
        strSQL = strSQL & " to_date('" & dteReceiveDate & "', 'MM/DD/YYYY'), "
        strSQL = strSQL & " '" & strSendingStoreNum & "', '99', NULL, '" & strShipNum & "', "
        strSQL = strSQL & " 'TFO', NULL, '" & strEmpId & "', 1, "
        strSQL = strSQL & " '" & strReceivingStoreNum & "', 0, NULL, 'XXX', 'N', 'N') "

        Dim cmdOraDD As System.Data.OleDb.OleDbCommand
        cmdOraDD = New OleDbCommand(strSQL, connHeaderGERS)
        cmdOraDD.CommandType = CommandType.Text
        cmdOraDD.ExecuteNonQuery()

        ConnGERSClose()

    End Sub

    Public Sub Populate_POS_INV_TRN_End()
        'end record to place a PTST entry into the BQ table

        ConnGERSOpen()

        Dim strSQL As String
        strSQL = " INSERT INTO TEMP_POS_INV_TRN "
        strSQL = strSQL & " ( trn_dt, trn_time, box_id,  "
        strSQL = strSQL & " shipment_date, "
        strSQL = strSQL & " received_date, "
        strSQL = strSQL & " store_cd, term_num, trn_num, "
        strSQL = strSQL & " doc_num, trn_tp, po_num, cshr_num, sess_num, other_store_cd, "
        strSQL = strSQL & " rcv_pcs, reason_cd, ship_tp, stat_cd, processed) "
        strSQL = strSQL & " VALUES (to_date('" & sysdate & "', 'MM/DD/YYYY'),  "
        strSQL = strSQL & " 0, "
        strSQL = strSQL & " '" & strBoxId & "',  "
        strSQL = strSQL & " to_date('" & dteShipDate & "', 'MM/DD/YYYY'), "
        strSQL = strSQL & " to_date('" & dteReceiveDate & "', 'MM/DD/YYYY'), "
        strSQL = strSQL & " '" & strSendingStoreNum & "', '99', NULL, '" & strShipNum & "', "
        strSQL = strSQL & " 'TFO', NULL, '" & strEmpId & "', 1, "
        strSQL = strSQL & " '" & strReceivingStoreNum & "', 0, NULL, 'XXX', 'N', 'N') "


        Dim cmdOraDD As System.Data.OleDb.OleDbCommand
        cmdOraDD = New OleDbCommand(strSQL, connHeaderGERS)
        cmdOraDD.CommandType = CommandType.Text
        cmdOraDD.ExecuteNonQuery()

        ConnGERSClose()

    End Sub


    Public Sub Populate_POS_INV_TRN_LN()
        'insert data into the holding TEMP_POS_INV_TRN table in GERS

        ConnGERSOpen()

        Dim strSQL As String

        strSQL = " INSERT INTO TEMP_POS_INV_TRN_LN ( trn_dt, trn_time, box_id, "
        strSQL = strSQL & " store_cd, term_num, trn_num, "
        strSQL = strSQL & " doc_num, ln_num, sku_num, qty, ret, stat_cd, processed) "
        strSQL = strSQL & " VALUES (to_date('" & sysdate & "', 'MM/DD/YYYY'), "
        strSQL = strSQL & " TO_CHAR(sysdate, 'SSSSS'), '" & strBoxId & "', "
        strSQL = strSQL & " '" & strSendingStoreNum & "', "
        strSQL = strSQL & " '99', NULL, '" & strShipNum & "', '" & strLnNum & "', '" & strSkuNum & "', "
        strSQL = strSQL & " '" & strQty & "', '" & strRetPrc & "', 'N', 'N') "

        Dim cmdOraDD As System.Data.OleDb.OleDbCommand
        cmdOraDD = New OleDbCommand(strSQL, connHeaderGERS)
        cmdOraDD.CommandType = CommandType.Text
        cmdOraDD.ExecuteNonQuery()

        ConnGERSClose()

    End Sub

    Public Function FlagGERSMovement() As Boolean

        Dim tmpReturn As Boolean = False

        ConnGERSOpen()

        Dim strSQL As String
        Dim cmdOraDD As System.Data.OleDb.OleDbCommand
        strSQL = "select t.doc_num from temp_pos_inv_trn t, pos_inv_trn p "
        strSQL = strSQL & "where(t.doc_num = p.doc_num) "
        strSQL = strSQL & "and t.box_id = '" & Replace(strBoxId, "'", "''") & "' "
        strSQL = strSQL & "and t.processed = 'Y' "

        cmdOraDD = New OleDbCommand(strSQL, connHeaderGERS)
        cmdOraDD.CommandType = CommandType.Text
        cmdOraDD.ExecuteNonQuery()

        Dim drOraDD As System.Data.OleDb.OleDbDataReader
        drOraDD = cmdOraDD.ExecuteReader

        If drOraDD.Read Then
            UpdateTHGERSStatus(drOraDD("doc_num"))
            tmpReturn = True
        End If

        ConnGERSClose()

        Return tmpReturn

    End Function

    Private Sub UpdateTHGERSStatus(ByVal DocNum As String)

        ConnSQLOpen()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateGERSStatus", connHeaderSQL)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)
        Dim DocNumParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Doc_Num", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = strBoxId
        DocNumParam.Value = DocNum

        objCommand.ExecuteReader()

        objCommand.Dispose()

    End Sub


    Public Sub make_store_number()

        Dim lengthDept As Integer
        Dim NeededLength As Integer
        Dim NeededZeros As Integer
        Dim strStore As String

        strStore = strSendingStoreNum
        NeededLength = 4
        lengthDept = Len(Trim(strStore))
        NeededZeros = NeededLength - lengthDept
        If NeededZeros = 1 Then
            strSendingStoreNum = "0" & strStore
        ElseIf NeededZeros = 2 Then
            strSendingStoreNum = "00" & strStore
        ElseIf NeededZeros = 3 Then
            strSendingStoreNum = "000" & strStore
        End If

        strStore = strReceivingStoreNum
        NeededLength = 4
        lengthDept = Len(Trim(strStore))
        NeededZeros = NeededLength - lengthDept
        If NeededZeros = 1 Then
            strReceivingStoreNum = "0" & strStore
        ElseIf NeededZeros = 2 Then
            strReceivingStoreNum = "00" & strStore
        ElseIf NeededZeros = 3 Then
            strReceivingStoreNum = "000" & strStore
        End If

    End Sub


    Public Sub ConnSQLOpen()
        'establish connection to SQL server
        connHeaderSQL = New System.Data.SqlClient.SqlConnection(strDBConnection)
        connHeaderSQL.Open()
    End Sub

    Public Sub ConnSQLClose()
        'close connection to SQL server
        connHeaderSQL.Close()
    End Sub

    Public Sub ConnGERSOpen()
        'establish connection to SQL server
        connHeaderGERS = New System.Data.OleDb.OleDbConnection(ConfigurationSettings.AppSettings("strGERSConn"))
        connHeaderGERS.Open()
    End Sub

    Public Sub ConnGERSClose()
        'close connection to SQL server
        connHeaderGERS.Close()
    End Sub


End Class 'for GersInventoryUpdate