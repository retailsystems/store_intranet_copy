Public Class WebForm1

    Inherits System.Web.UI.Page

    Protected WithEvents btnHome As System.Web.UI.WebControls.Button
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifmHeader As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    'Protected WithEvents ucHeader As Header
    Private objUser As New User()

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim objSecurity As New Security()
        Dim strStore As String
        Dim strUser As String

        objUser.GetUserInfo()
        If objUser.IsValidUser Then
            'ucHeader.lblUserName.Text = objUser.User
            strUser = objUser.User
        Else
            Response.Redirect("Login.aspx")
        End If

        objUser.GetStoreInfo()
        If objUser.IsValidStore Then
            'ucHeader.lblStore.Text = objUser.UserStore
            'ucHeader.lblHeaderPage.Text = "MOS"

            strStore = "Store: " & objUser.UserStore.ToString.PadLeft(4, "0")
            'strStore = "5001"
        Else
            '''    'klaus UNCOMMENT THE CODE BELOW, IT IS NECC.!!!!!!!!!!
            Response.Redirect("GetStoreInfo.aspx")
        End If

        'klaus UNCOMMENT THE CODE BELOW, IT IS NECC.!!!!!!!!!!
        If Not objSecurity.HasAccess(objUser, "STOREMOS") Then
            Response.Redirect("AccessDenied.aspx")
        End If

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=" & strStore & "&user=" & strUser & "&title=MOS&Version=" & ConfigurationSettings.AppSettings("strVersion"))

    End Sub

    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        objUser.ClearUserInfo()
        Response.Redirect("..\Home.asp")
    End Sub

End Class
