Public Class MOSConfirm
    Inherits System.Web.UI.Page
    Protected WithEvents btnDone As System.Web.UI.WebControls.Button
    Protected WithEvents btnHome As System.Web.UI.WebControls.Button
    Protected WithEvents ReportFrame As System.Web.UI.HtmlControls.HtmlGenericControl
    ' Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifmHeader As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private objUser As New User()
    Private intMOSListId As Integer

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim dteToday As Date
        Dim objSecurity As New Security()
        Dim strStore As String
        Dim strUser As String

        objUser.GetUserInfo()
        If objUser.IsValidUser Then
            '  ucHeader.lblUserName.Text = objUser.User
            strUser = objUser.User
        Else
            Response.Redirect("Login.aspx")
        End If

        objUser.GetStoreInfo()
        If objUser.IsValidStore Then
            ' ucHeader.lblStore.Text = objUser.UserStore
            ' ucHeader.lblHeaderPage.Text = "MOS Confirmation"
            strStore = "Store: " & objUser.UserStore.ToString.PadLeft(4, "0")
        Else
            Response.Redirect("GetStoreInfo.aspx")
        End If

        If Not objSecurity.HasAccess(objUser, "STOREMOS") Then
            Response.Redirect("AccessDenied.aspx")
        End If

        intMOSListId = Request("MOSListId")
        dteToday = Now.Date

        ReportFrame.Attributes("src") = "Reports/MOSActivity.aspx?RT=2&MOSListId=" & intMOSListId & "&StoreCd=" & objUser.UserStore & "&StartDate=" & dteToday & "&EndDate=" & dteToday

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=" & strStore & "&user=" & strUser & "&title=MOS Confirmation&Version=" & ConfigurationSettings.AppSettings("strVersion"))

    End Sub

    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        objUser.ClearUserInfo()
        Response.Redirect("..\home.asp")
    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("mos.aspx")
    End Sub

    Private Sub btnDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDone.Click
        Response.Redirect("default.aspx")
    End Sub

End Class
