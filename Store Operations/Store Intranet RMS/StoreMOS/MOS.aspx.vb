Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System

Public Class MOS

    Inherits System.Web.UI.Page

    Protected WithEvents dgMOSItems As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnMOS As System.Web.UI.WebControls.Button
    Protected WithEvents btnHome As System.Web.UI.WebControls.Button
    Protected WithEvents btnAddItem As System.Web.UI.WebControls.Button
    Protected WithEvents txtSKU As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    'Protected WithEvents ucHeader As Header
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ddlReason As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ifmHeader As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private MOSList_Id As Integer
    Private intTotalMOSQty As Integer = 0
    Private decTotalRetail As Decimal = 0
    Private objUser As New User()

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Dim objSecurity As New Security
        Dim strStore As String
        Dim strUser As String

        pageBody.Attributes("onload") = ""

        objUser.GetUserInfo()
        If objUser.IsValidUser Then
            ' ucHeader.lblUserName.Text = objUser.User
            strUser = objUser.User
        Else
            Response.Redirect("Login.aspx")
        End If

        objUser.GetStoreInfo()
        If objUser.IsValidStore Then
            '  ucHeader.lblStore.Text = objUser.UserStore
            ' ucHeader.lblHeaderPage.Text = "MOS"
            strStore = "Store: " & objUser.UserStore.ToString.PadLeft(4, "0")

        Else
            Response.Redirect("GetStoreInfo.aspx")
        End If

        If Not objSecurity.HasAccess(objUser, "STOREMOS") Then
            Response.Redirect("AccessDenied.aspx")
        End If

        MOSList_Id = Viewstate("MOSList_Id")
        If (Not Page.IsPostBack) Then
            FillReasons()
        End If
        'Add javascript to capture return key
        txtSKU.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){event.preventDefault();event.returnValue=false;event.cancel = true;document.forms[0].btnAddItem.click();}")
        btnReturn.Attributes.Add("onclick", "return confirm('MOS items WILL NOT be processed if you quit. Are you sure you want to continue?')")
        btnHome.Attributes.Add("onclick", "return confirm('All items entered WILL NOT be processed if you quit. Are you sure you want to continue?')")

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=" & strStore & "&user=" & strUser & "&title=MOS&Version=" & ConfigurationSettings.AppSettings("strVersion"))

    End Sub

    Private Sub FillReasons()
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetReasons", objConnection)

        objDataReader = objCommand.ExecuteReader()
        ddlReason.DataSource = objDataReader
        ddlReason.DataBind()

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
    End Sub

    Private Sub FillMOSItems(ByVal MOSList_Id As Integer)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetMOSItems " & MOSList_Id & " ", objConnection)

        objDataReader = objCommand.ExecuteReader()
        If (objDataReader.HasRows) Then
            dgMOSItems.DataSource = objDataReader
            dgMOSItems.DataBind()
            dgMOSItems.Visible = True

        Else
            dgMOSItems.Visible = False
        End If
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()

    End Sub

    Private Sub btnAddItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddItem.Click

        Dim objItem As New Item(ConfigurationSettings.AppSettings("strRMSConn"))
        Dim StoreQty As Integer
        Dim Reason As Integer
        Reason = ddlReason.SelectedValue


        If Trim(txtSKU.Text).Length <= 0 Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("The SKU you have entered is incorrect. Please Try again.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            Exit Sub
        End If

        objItem.SKUNum = Replace(Trim(txtSKU.Text), "'", "''")

        'Response.Write(objItem.GetItemDataRMS())
        'Response.End()
        If Not objItem.GetItemDataRMS(objUser.UserStore) Then
            pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("The SKU you have entered is incorrect. Please Try again.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            Exit Sub
        End If

        If Mid(objItem.SKUNum, 1, 4) = 9999 Then
            pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Dummy SKU's can not be marked out of stock.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            Exit Sub
        End If

        If objItem.decRetailPrice <= 0.01 Then
            pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Merchandise for a penny can not be marked out of stock with this application.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            Exit Sub
        End If

        StoreQty = objItem.GetCurrentStoreQtyRMS(objUser.UserStore)

        'If Not IsNumeric(txtMOSQty.Text) Then
        ' pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Invalid MOS Qty.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
        'Exit Sub
        'End If

        'If StoreQty > 0 And StoreQty >= txtMOSQty.Text Then

        If MOSList_Id = 0 Then
            MOSList_Id = AddMOSList(objUser.UserId, objUser.UserStore)
            Viewstate("MOSList_Id") = MOSList_Id
        End If

        'DeleteItemFromMOSList(MOSList_Id, objItem)
        AddItemToMOSList(MOSList_Id, objItem, StoreQty, 1, objUser.UserStore, Reason)
        'UpdateItemNums()

        'ElseIf StoreQty > 0 And StoreQty < txtMOSQty.Text Then
        'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("MOS exceeds current item stock of " & StoreQty & ".") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
        'Else
        'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("This item is not in stock.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
        'End If

        FillMOSItems(MOSList_Id)

        txtSKU.Text = ""

    End Sub

    Private Sub DeleteItem(ByVal Item_Id As Integer)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spDeleteItem", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        objCommand.Parameters.Add("@Item_Id", Item_Id)

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()

    End Sub



    Private Sub AddItemToMOSList(ByVal MOSList_Id As Integer, ByVal objItem As Item, ByVal Store_Qty As Integer, ByVal MOS_Qty As Integer, ByVal StoreCd As Integer, ByVal ReasonID As Integer)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spAddMOSItem", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        objCommand.Parameters.Add("@MOSList_Id", MOSList_Id)
        objCommand.Parameters.Add("@SKU", objItem.SKUNum)
        objCommand.Parameters.Add("@Store_Qty", Store_Qty)
        objCommand.Parameters.Add("@MOS_Qty", MOS_Qty)
        objCommand.Parameters.Add("@CURR", objItem.decRetailPrice)
        objCommand.Parameters.Add("@VE_CD", IIf(objItem.strVender Is Nothing, "", objItem.strVender))
        objCommand.Parameters.Add("@SIZE_CD", IIf(objItem.strSize Is Nothing, "", objItem.strSize))
        objCommand.Parameters.Add("@DES1", objItem.strDescription)
        objCommand.Parameters.Add("@DEPT_CD", objItem.strDept)
        objCommand.Parameters.Add("@ITM_CD", objItem.strItemCd)
        objCommand.Parameters.Add("@CLASS_CD", objItem.strClassCd)
        objCommand.Parameters.Add("@SUBCLASS_CD", objItem.strSubClassCd)
        'objCommand.Parameters.Add("@GERSMOS", GERSMOS)
        objCommand.Parameters.Add("@Date_Created", Now())
        objCommand.Parameters.Add("@Date_Modified", Now())
        objCommand.Parameters.Add("@Reason", ReasonID)

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()

    End Sub

    Private Function AddMOSList(ByVal EmployeeId As String, ByVal Store As String) As Integer

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spAddMOSList", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parStoreCd As SqlClient.SqlParameter = objCommand.Parameters.Add("@Store_Cd", SqlDbType.VarChar)
        Dim parEmpId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Emp_Id", SqlDbType.VarChar)
        Dim parDateCreated As SqlClient.SqlParameter = objCommand.Parameters.Add("@Date_Created", SqlDbType.DateTime)
        Dim parDateModified As SqlClient.SqlParameter = objCommand.Parameters.Add("@Date_Modified", SqlDbType.DateTime)
        Dim parMOSListId As SqlClient.SqlParameter = objCommand.Parameters.Add("@MOSList_Id", SqlDbType.Int)

        parStoreCd.Direction = ParameterDirection.Input
        parEmpId.Direction = ParameterDirection.Input
        parDateCreated.Direction = ParameterDirection.Input
        parDateModified.Direction = ParameterDirection.Input
        parMOSListId.Direction = ParameterDirection.Output

        parStoreCd.Value = Store
        'parEmpId.Value = EmployeeId.ToString.PadLeft(5, "0"c)
        'prefix 6 zero to employee ID reflected lawson employee change to 6 char. Done by Lan 10/13/2010
        parEmpId.Value = EmployeeId.ToString.PadLeft(6, "0"c)
        parDateCreated.Value = Now()
        parDateModified.Value = Now()

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()

        Return parMOSListId.Value

    End Function

    Private Function HasBeenSubmitted(ByVal MOSList As Integer) As Boolean

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spHasBeenSubmitted", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parMOSListId As SqlClient.SqlParameter = objCommand.Parameters.Add("@MOSList_Id", SqlDbType.Int)
        Dim parSubmitted As SqlClient.SqlParameter = objCommand.Parameters.Add("@Submitted", SqlDbType.Bit)

        parMOSListId.Direction = ParameterDirection.Input
        parSubmitted.Direction = ParameterDirection.Output

        parMOSListId.Value = MOSList

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()

        Return parSubmitted.Value

    End Function

    Private Sub btnMOS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMOS.Click


        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim boolUpdate As Boolean = False
        Dim rows As Integer = 0
        Dim I As Integer = 0
        Dim result As String

        If MOSList_Id <> 0 Then
            If Not HasBeenSubmitted(MOSList_Id) Then
                UpdateMOSList(MOSList_Id, True, Now())
                objConnection.Open()

                Dim objCommand As New SqlClient.SqlCommand("spGetMOSItems " & MOSList_Id & " ", objConnection)
                objDataReader = objCommand.ExecuteReader()
                rows = GetMOSItemCount(MOSList_Id)
                Dim desc As RMSProcess.InvAdjustDesc = New RMSProcess.InvAdjustDesc

                If objDataReader.HasRows And rows > 0 Then

                    desc.dc_dest_id = objUser.UserStore
                    desc.InvAdjustDtl = desc.InvAdjustDtl.CreateInstance(GetType(RMSProcess.InvAdjustDtl), rows)
                    While objDataReader.Read

                        Dim dtl As RMSProcess.InvAdjustDtl = New RMSProcess.InvAdjustDtl
                        dtl.adjustment_reason_code = Integer.Parse(objDataReader("RMS_ID"))
                        dtl.adjustment_reason_codeSpecified = True
                        dtl.item_id = Convert.ToString(objDataReader("SKU_Num"))
                        dtl.unit_qty = Integer.Parse(objDataReader("MOS_Qty"))
                        dtl.from_disposition = Convert.ToString(objDataReader("From_Dis"))
                        dtl.to_disposition = Convert.ToString(objDataReader("To_Dis"))
                        dtl.user_id = objUser.UserId
                        dtl.create_date = DateTime.Parse(objDataReader("Date_Created"))


                        Response.Write(desc.InvAdjustDtl.Length & ": ")
                        desc.InvAdjustDtl(I) = dtl

                        I = I + 1
                    End While
                End If
                objDataReader.Close()
                objCommand.Dispose()
                objConnection.Close()

                'Send them to the web service
                'Response.Write(desc.dc_dest_id & ": " & desc.InvAdjustDtl(0).item_id)

                If (Not desc Is Nothing) Then
                    result = SendItemListToRMS(desc)
                End If

                If I > 0 And result = "success" Then
                    'OK, update the item list and send to confirmation
                    UpdateItemByList(MOSList_Id, True)                    
                    desc = Nothing
                    Response.Redirect("MOSConfirm.aspx?MOSListId=" & MOSList_Id)
                Else
                    'something is wrong, update as failure
                    UpdateItemByList(MOSList_Id, False)
                    desc = Nothing
                    Response.Redirect("MOSError.aspx?MOSListId=" & MOSList_Id)
                End If
            Else

                Response.Redirect("MOSError.aspx?MOSListId=" & MOSList_Id)
            End If

        End If

    End Sub

    Private Function GetMOSItemCount(ByVal listID As Integer)
        Dim rowCount As Integer
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetListItemCount", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        Dim parListId As SqlClient.SqlParameter = objCommand.Parameters.Add("@MOSList_Id", SqlDbType.Int)
        parListId.Direction = ParameterDirection.Input
        parListId.Value = listID    
        rowCount = Integer.Parse(objCommand.ExecuteScalar())

        objCommand.Dispose()
        objConnection.Close()

        Return rowCount
    End Function

    Private Function SendItemListToRMS(ByRef DESC As RMSProcess.InvAdjustDesc)
        Dim message As String
        Dim response As RMSProcess.publishInvAdjustCreateUsingInvAdjustDescResponse = New RMSProcess.publishInvAdjustCreateUsingInvAdjustDescResponse
        Dim MOSService As RMSProcess.InvAdjustPublishingService = New RMSProcess.InvAdjustPublishingService
        MOSService.Timeout = System.Convert.ToInt32(ConfigurationSettings.AppSettings("strRmsWSTimeout"))
        ' MOSService.AllowAutoRedirect = False
        Dim des As RMSProcess.publishInvAdjustCreateUsingInvAdjustDesc = New RMSProcess.publishInvAdjustCreateUsingInvAdjustDesc
        des.InvAdjustDesc = DESC
        response = MOSService.publishInvAdjustCreateUsingInvAdjustDesc(des)

        If (Not response.ServiceOpStatus Is Nothing) Then

            If (Not response.ServiceOpStatus.FailStatus Is Nothing) Then
                message += response.ServiceOpStatus.FailStatus.shortErrorMessage
            ElseIf (Not response.ServiceOpStatus.SuccessStatus Is Nothing) Then
                message += "success"
            End If
        Else
            message = "Service Status Response is Missing."
        End If

        MOSService.Dispose()
        Return message

    End Function



    'Private Sub btnMOS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMOS.Click

    '    Dim objGERSUpdate As New GersInventoryUpdate
    '    Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
    '    Dim objDataReader As SqlClient.SqlDataReader
    '    Dim booGERSUpdate As Boolean = False
    '    Dim I As Integer = 0

    '    If MOSList_Id <> 0 Then
    '        If Not HasBeenSubmitted(MOSList_Id) Then
    '            UpdateMOSList(MOSList_Id, True, Now())

    '            objGERSUpdate.strDBConnection = ConfigurationSettings.AppSettings("strGERSConn")
    '            objConnection.Open()

    '            Dim objCommand As New SqlClient.SqlCommand("spGetMOSItems " & MOSList_Id & " ", objConnection)

    '            objDataReader = objCommand.ExecuteReader()

    '            While objDataReader.Read

    '                I += 1

    '                Try
    '                    If objGERSUpdate.Store_MOS(objDataReader("SKU_Num"), objUser.UserStore, objDataReader("MOS_Qty")) Then
    '                        UpdateItem(objDataReader("Item_Id"), True, Now(), Now())
    '                    Else
    '                        UpdateItem(objDataReader("Item_Id"), False, Now(), Now())
    '                        Response.Redirect("MOSError.aspx?MOSListId=" & MOSList_Id)
    '                    End If
    '                Catch
    '                    UpdateItem(objDataReader("Item_Id"), False, Now(), Now())
    '                    Response.Redirect("MOSError.aspx?MOSListId=" & MOSList_Id)
    '                End Try

    '            End While

    '            objDataReader.Close()
    '            objCommand.Dispose()
    '            objConnection.Close()

    '            If I > 0 Then
    '                Response.Redirect("MOSConfirm.aspx?MOSListId=" & MOSList_Id)
    '            End If

    '        Else

    '            Response.Redirect("MOSError.aspx?MOSListId=" & MOSList_Id)

    '        End If

    '    End If

    'End Sub

    Private Sub UpdateMOSList(ByVal MOSList As Integer, ByVal Submitted As Boolean, ByVal DateModified As DateTime)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateMOSList", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parMOSListId As SqlClient.SqlParameter = objCommand.Parameters.Add("@MOSList_Id", SqlDbType.Int)
        Dim parSubmitted As SqlClient.SqlParameter = objCommand.Parameters.Add("@Submitted", SqlDbType.Bit)
        Dim parDateModified As SqlClient.SqlParameter = objCommand.Parameters.Add("@Date_Modified", SqlDbType.DateTime)

        parMOSListId.Direction = ParameterDirection.Input
        parSubmitted.Direction = ParameterDirection.Input
        parDateModified.Direction = ParameterDirection.Input


        parMOSListId.Value = MOSList
        parSubmitted.Value = Submitted
        parDateModified.Value = DateModified

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()

    End Sub

    Public Sub UpdateItemByList(ByVal listID As Integer, ByVal IsSuccess As Boolean)
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateItemByList", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parListId As SqlClient.SqlParameter = objCommand.Parameters.Add("@List_Id", SqlDbType.Int)
        parListId.Direction = ParameterDirection.Input
        parListId.Value = listID
        Dim parSuccess As SqlClient.SqlParameter = objCommand.Parameters.Add("@MOS_Status", SqlDbType.Bit)
        parSuccess.Direction = ParameterDirection.Input
        parSuccess.Value = IsSuccess

        Dim parDateModified As SqlClient.SqlParameter = objCommand.Parameters.Add("@Date_Modified", SqlDbType.DateTime)
        Dim parRMSMOSDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@Date_RMSMOS", SqlDbType.DateTime)
        parDateModified.Direction = ParameterDirection.Input
        parRMSMOSDate.Direction = ParameterDirection.Input
        parDateModified.Value = DateTime.Now
        parRMSMOSDate.Value = DateTime.Now

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()

    End Sub
    Private Sub UpdateItemLineNum(ByVal ItemId As Integer, ByVal DateModified As DateTime, ByVal LineNum As Integer)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateItemLineNum", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parItemId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Item_Id", SqlDbType.Int)
        Dim parDateModified As SqlClient.SqlParameter = objCommand.Parameters.Add("@Date_Modified", SqlDbType.DateTime)
        Dim parLineNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@Line_Num", SqlDbType.Int)

        parItemId.Direction = ParameterDirection.Input
        parDateModified.Direction = ParameterDirection.Input
        parLineNum.Direction = ParameterDirection.Input

        parItemId.Value = ItemId
        parDateModified.Value = DateModified
        parLineNum.Value = LineNum

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()

    End Sub

    Private Sub UpdateItem(ByVal ItemId As Integer, ByVal GERSFlag As Boolean, ByVal GERSDate As DateTime, ByVal DateModified As DateTime)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateItem", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parItemId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Item_Id", SqlDbType.Int)
        Dim parGERSMOS As SqlClient.SqlParameter = objCommand.Parameters.Add("@GERSMOS", SqlDbType.Bit)
        Dim parDateModified As SqlClient.SqlParameter = objCommand.Parameters.Add("@Date_Modified", SqlDbType.DateTime)
        Dim parGERSMOSDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@Date_GERSMOS", SqlDbType.DateTime)

        parItemId.Direction = ParameterDirection.Input
        parGERSMOS.Direction = ParameterDirection.Input
        parDateModified.Direction = ParameterDirection.Input
        parGERSMOSDate.Direction = ParameterDirection.Input

        parItemId.Value = ItemId
        parGERSMOS.Value = GERSFlag
        parDateModified.Value = DateModified
        parGERSMOSDate.Value = GERSDate

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()

    End Sub

    Private Function UpdateItemNums()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim I As Integer = 0

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetMOSItems " & MOSList_Id & " ", objConnection)

        objDataReader = objCommand.ExecuteReader()

        Do While objDataReader.Read

            I += 1
            UpdateItemLineNum(objDataReader("Item_Id"), Now(), I)

        Loop

        dgMOSItems.DataSource = objDataReader
        dgMOSItems.DataBind()

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()

    End Function

    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        objUser.ClearUserInfo()
        Response.Redirect("..\Home.asp")
    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Response.Redirect("default.aspx")
    End Sub

    Private Sub dgMOSItems_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgMOSItems.ItemCommand
        DeleteItem(e.Item.Cells(0).Text)
        'UpdateItemNums()

        FillMOSItems(MOSList_Id)

    End Sub

    Private Sub dgMOSItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgMOSItems.ItemDataBound

        Dim objButton As Button

        With e.Item
            Select Case .ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem

                    Dim MOSCount As Integer = Convert.ToInt32(DataBinder.Eval(.DataItem, "MOS_Qty"))
                    Dim CURRCount As Decimal = Convert.ToDecimal(DataBinder.Eval(.DataItem, "CURR"))

                    intTotalMOSQty += MOSCount
                    decTotalRetail += CURRCount

                    objButton = .Cells(10).Controls(0)
                    objButton.CssClass = "B1"

                Case ListItemType.Footer
                    .Cells(7).Text = intTotalMOSQty
                    .Cells(5).Text = String.Format("{0:c}", decTotalRetail)
            End Select
        End With

    End Sub

End Class
