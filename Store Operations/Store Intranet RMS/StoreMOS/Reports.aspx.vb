Public Class Reports
    Inherits System.Web.UI.Page
    Protected WithEvents btnHome As System.Web.UI.WebControls.Button
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Protected WithEvents ReportFrame As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifmHeader As System.Web.UI.HtmlControls.HtmlGenericControl
    ' Protected WithEvents ucHeader As Header

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private StoreCd As Integer
    Private StartDate As Date
    Private EndDate As Date
    Private objUser As New User()

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim objSecurity As New Security()
        Dim strStore As String
        Dim strUser As String

        objUser.GetUserInfo()
        If objUser.IsValidUser Then
            '   ucHeader.lblUserName.Text = objUser.User
            strUser = objUser.User
        Else
            Response.Redirect("Login.aspx")
        End If

        objUser.GetStoreInfo()
        If objUser.IsValidStore Then
            '   ucHeader.lblStore.Text = objUser.UserStore
            'ucHeader.lblHeaderPage.Text = "MOS Reports"
            strStore = "Store: " & objUser.UserStore.ToString.PadLeft(4, "0")
        Else
            Response.Redirect("GetStoreInfo.aspx")
        End If

        If Not objSecurity.HasAccess(objUser, "STOREMOS") Then
            Response.Redirect("AccessDenied.aspx")
        End If

        StoreCd = Request("Store")

        If IsDate(Request("StartDate")) Then
            StartDate = Request("StartDate")
        End If

        If IsDate(Request("EndDate")) Then
            EndDate = Request("EndDate")
        End If

        ReportFrame.Attributes("src") = "Reports/MOSActivity.aspx?RT=1&StoreCd=" & StoreCd & "&StartDate=" & StartDate & "&EndDate=" & EndDate

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=" & strStore & "&user=" & strUser & "&title=MOS Reports&Version=" & ConfigurationSettings.AppSettings("strVersion"))

    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Response.Redirect("ReportQuery.aspx?Store=" & StoreCd)
    End Sub

    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        objUser.ClearUserInfo()
        Response.Redirect("..\Home.asp")
    End Sub

End Class
