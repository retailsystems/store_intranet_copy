<%@ Register TagPrefix="ucLayout" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="ucLayout" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="default.aspx.vb" Inherits="SSTransfer.WebForm_TransferHome" smartNavigation="False" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Transfer Home ccc</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<script src="Javascript/DisableClientBack.js" language="Javascript"></script>
		<script src="Javascript/TrapKeyPress.js" language="Javascript"></script>
		<LINK id="lnkStyles" href="http://172.16.2.13/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server" />
	</HEAD>
	<body bottomMargin="0" class="sBody" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="frmTransferHome" method="post" runat="server" onsubmit="return checkSubmit();">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1"><uclayout:header id="layoutHeader" runat="server"></uclayout:header></td>
				</tr>
				<tr>
					<td vAlign="top" align="middle">
						<br>
						<br>
						<table cellSpacing="0" cellPadding="5" width="100%" border="0">
							<tr>
								<td>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td align="middle">
													<asp:PlaceHolder ID="phEA" Runat=server Visible=False>
														<div style="OVERFLOW-Y: auto;WIDTH: 100%;HEIGHT: 320px" class="sInfoDatagrid">
															<table cellSpacing="0" cellPadding="2" width="100%">
																<tr>
																	<td>
																		<asp:datagrid CssClass="sInfoDatagrid" OnSortCommand="SortCommand_Arrivals" AllowSorting="False" autogeneratecolumns="false" id="grdArrivals" runat="server" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="false">
																			<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																			<Columns>
																				<asp:BoundColumn SortExpression="3" DataField="Sending_Store_Name" HeaderText="Sent From">
																					<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn SortExpression="5" DataFormatString="{0:d}" DataField="Shipment_Date" HeaderText="Ship Date">
																					<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:HyperLinkColumn SortExpression="1" DataTextField="Box_Id" HeaderText="Transfer #" DataNavigateUrlField="Box_Id" datanavigateurlformatstring="ViewTransfer.aspx?BID={0}&RET=default.aspx?Tab=1">
																					<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:HyperLinkColumn>
																				<asp:TemplateColumn HeaderText="Type">
																					<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																					<ItemTemplate>
																						<asp:Label Runat="server">
																							<%# GetBoxTransferTypes(Container.dataitem("Box_Id")) %>
																						</asp:Label>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Tracking #">
																					<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																					<ItemTemplate>
																						<asp:HyperLink Target=_blank ID=TL1 Runat=server NavigateUrl='<%# Replace(Container.dataitem("TrackingURL"),"{0}", Container.dataitem("Tracking_Num")) %>'><%# Container.dataitem("Tracking_Num") %></asp:HyperLink>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn DataField="Status_Desc" HeaderText="Status">
																					<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																			</Columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</div>
													</asp:PlaceHolder>
													<asp:PlaceHolder ID="phRS" Runat=server Visible=False>
														<div style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 320px" class="sInfoDatagrid">
															<table cellSpacing="0" cellPadding="2" width="100%" border="0">
																<tr>
																	<td>
																		<asp:datagrid OnSortCommand="SortCommand_Shipped" AllowSorting="False" id="grdShipped" runat="server" CssClass="sInfoDatagrid" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="false" AutoGenerateColumns="False">
																			<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																			<Columns>
																				<asp:BoundColumn SortExpression="3" DataField="Receiving_Store_Name" HeaderText="Sent To">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn SortExpression="6" DataFormatString="{0:d}" DataField="Shipment_Date" HeaderText="Ship Date">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:HyperLinkColumn SortExpression="1" DataTextField="Box_Id" HeaderText="Transfer #" DataNavigateUrlField="Box_Id" datanavigateurlformatstring="ViewTransfer.aspx?BID={0}&RET=default.aspx?Tab=2">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:HyperLinkColumn>
																				<asp:TemplateColumn HeaderText="Type">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																					<ItemTemplate>
																						<asp:Label Runat="server" ID="Label1">
																							<%# GetBoxTransferTypes(Container.dataitem("Box_Id")) %>
																						</asp:Label>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Tracking #">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																					<ItemTemplate>
																						<asp:HyperLink Target=_blank ID="TL2" Runat=server NavigateUrl='<%# Replace(Container.dataitem("TrackingURL"),"{0}", Container.dataitem("Tracking_Num")) %>'><%# Container.dataitem("Tracking_Num") %></asp:HyperLink>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn SortExpression="5" DataField="Status_Desc" HeaderText="Status">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																			</Columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</div>
													</asp:PlaceHolder>
													<asp:PlaceHolder ID="phP" Runat=server Visible=False>
														<div style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 320px" class="sInfoDataGrid">
															<table cellSpacing="0" cellPadding="2" width="100%" border="0">
																<tr>
																	<td>
																		<asp:datagrid OnSortCommand="SortCommand_SavedTransfers" CssClass="sInfoDatagrid" AllowSorting="False" id="grdSavedTransfers" runat="server" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="false" AutoGenerateColumns="False">
																			<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																			<Columns>
																				<asp:BoundColumn SortExpression="3" DataField="Receiving_Store_Name" HeaderText="Sent To">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:TemplateColumn SortExpression="1" HeaderText="Transfer #">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																					<ItemTemplate>
																						<a href='NewTransferForm.aspx?BID=<%# Container.dataitem("Box_Id") %>&TType=<%# Container.dataitem("TType") %>'>
																							<asp:Label Runat="server" ID="Label4">
																								<%# Container.dataitem("Box_Id") %>
																							</asp:Label>
																						</a>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Type">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																					<ItemTemplate>
																						<asp:Label Runat="server" ID="Label2">
																							<%# GetBoxTransferTypes(Container.dataitem("Box_Id")) %>
																						</asp:Label>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn SortExpression="4" DataField="Employee" HeaderText="Employee">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn SortExpression="6" DataFormatString="{0:d}" DataField="Modified_Date" HeaderText="Saved Date">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn SortExpression="5" DataField="Status_Desc" HeaderText="Status">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																			</Columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</div>
													</asp:PlaceHolder>
													<asp:PlaceHolder ID="phU" Runat=server Visible=False>
														<div style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 320px" class="sInfoDatagrid">
															<table cellSpacing="0" cellPadding="2" width="100%" border="0">
																<tr>
																	<td>
																		<asp:datagrid OnSortCommand="SortCommand_SavedCheckIns" CssClass="sInfoDatagrid" AllowSorting="False" id="dgSavedCheckIns" runat="server" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="false" AutoGenerateColumns="False">
																			<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																			<Columns>
																				<asp:BoundColumn SortExpression="3" DataField="Sending_Store_Name" HeaderText="Sent From">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn SortExpression="6" DataFormatString="{0:d}" DataField="Shipment_Date" HeaderText="Ship Date">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:HyperLinkColumn SortExpression="1" DataTextField="Box_Id" HeaderText="Transfer #" DataNavigateUrlField="Box_Id" datanavigateurlformatstring="CheckInTransfer.aspx?BID={0}">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:HyperLinkColumn>
																				<asp:TemplateColumn HeaderText="Type">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																					<ItemTemplate>
																						<asp:Label Runat="server" ID="Label3">
																							<%# GetBoxTransferTypes(Container.dataitem("Box_Id")) %>
																						</asp:Label>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Tracking #">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																					<ItemTemplate>
																						<asp:HyperLink Target=_blank ID="Hyperlink1" Runat=server NavigateUrl='<%# Replace(Container.dataitem("TrackingURL"),"{0}", Container.dataitem("Tracking_Num")) %>'><%# Container.dataitem("Tracking_Num") %></asp:HyperLink>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn SortExpression="5" DataField="Status_Desc" HeaderText="Status">
																					<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																			</Columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</div>
													</asp:PlaceHolder>
													<asp:PlaceHolder ID="phPS" Runat=server Visible=False>
														<div style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 320px" class="sInfoDatagrid">
															<table cellSpacing="0" cellPadding="2" width="100%" border="0">
																<tr>
																	<td>
																		<asp:datagrid OnSortCommand="SortCommand_PrintQueue" CssClass="sInfoDatagrid" AllowSorting="False" id="grdPrintQueue" runat="server" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="false" AutoGenerateColumns="False">
																			<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																			<Columns>
																				<asp:HyperLinkColumn ItemStyle-Wrap="False" HeaderStyle-Width="15%" SortExpression="1" DataTextFormatString="{0:d}" DataTextField="Box_Id" HeaderText="Transfer #" DataNavigateUrlField="Box_Id" datanavigateurlformatstring="ViewTransfer.aspx?BID={0}&RET=default.aspx?Tab=5">
																					<HeaderStyle Wrap="False" CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:HyperLinkColumn>
																				<asp:BoundColumn ItemStyle-Wrap="False" HeaderStyle-Width="15%" SortExpression="2" DataFormatString="{0:d}" DataField="Shipment_Date" HeaderText="Shipment Date">
																					<HeaderStyle Wrap="False" CssClass="sInfoDatagridHeader"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:TemplateColumn HeaderStyle-Width="65%">
																					<HeaderStyle HorizontalAlign="center" CssClass="sInfoDatagridHeader"></HeaderStyle>
																					<ItemStyle HorizontalAlign="center"></ItemStyle>
																					<ItemTemplate>
																						&nbsp;
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderStyle-Width="5%" HeaderText="Label">
																					<HeaderStyle Wrap="False" HorizontalAlign="center" CssClass="sInfoDatagridHeader"></HeaderStyle>
																					<ItemStyle HorizontalAlign="center"></ItemStyle>
																					<ItemTemplate>
																						<asp:PlaceHolder Runat=server Visible='<%# Container.DataItem("UPSDisplay") %>' ID="Placeholder1">
																								<a href="#" onclick="return false;">
																									<%# LabelImage(Container.DataItem("ParcelService_Cd"),Container.dataitem("Tracking_Num")) %>
																								</a>
																						</asp:PlaceHolder>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderStyle-Width="5%" HeaderText="Manifest">
																					<HeaderStyle HorizontalAlign="center" CssClass="sInfoDatagridHeader"></HeaderStyle>
																					<ItemStyle HorizontalAlign="center"></ItemStyle>
																					<ItemTemplate>
																						<asp:PlaceHolder Runat=server Visible='<%# Container.DataItem("ManifestDisplay") %>' ID="Placeholder2">
																							<a target=_blank href='GenerateManifest.aspx?BID=<%# container.dataitem("Box_Id") %>'>
																								<img border=0 src='Images\printed.gif'>
																							</a>
																						</asp:PlaceHolder>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																			</Columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</div>
													</asp:PlaceHolder>
												</td>
										</tr>
									</table>
									<table cellSpacing="0" cellPadding="0" width="100%" border="0">
										<tr>
											<td align="middle">
												<table cellpadding=1 cellspacing=0 border=0>
													<tr>
														<td id='TDEA' runat=server onclick="document.location='default.aspx?Tab=1';" class="sInfoTab" onmouseover="this.className='sInfoTabMouseOver';" onmouseout="this.className='sInfoTab';" width=120 align=center>Expected Arrivals</td>
														<td>&nbsp;</td>
														<td id='TDRS' runat=server onclick="document.location='default.aspx?Tab=2';" class="sInfoTab" onmouseover="this.className='sInfoTabMouseOver';" onmouseout="this.className='sInfoTab';" width=120 align=center>Recently Shipped</td>
														<td>&nbsp;</td>
														<td id='TDP' runat=server onclick="document.location='default.aspx?Tab=3';" class="sInfoTab" onmouseover="this.className='sInfoTabMouseOver';" onmouseout="this.className='sInfoTab';" width=120 align=center>Packing</td>
														<td>&nbsp;</td>
														<td id='TDU' runat=server onclick="document.location='default.aspx?Tab=4';"  class="sInfoTab" onmouseover="this.className='sInfoTabMouseOver';" onmouseout="this.className='sInfoTab';" width=120 align=center>Unpacking</td>
														<td>&nbsp;</td>
														<td id='TDPS' runat=server onclick="document.location='default.aspx?Tab=5';" class="sInfoTab" onmouseover="this.className='sInfoTabMouseOver';" onmouseout="this.className='sInfoTab';" width=120 align=center>Print Shipped</td>
													</tr>
												</table>
											</td>	
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<br>
						<hr width="100%" class="sHr">
						<table cellSpacing="0" cellPadding="2" width="100%" border="0">
							<tr><td width="1%"><img src="images\info.gif" border=0></td><td valign=middle><asp:Label Runat=server EnableViewState=false ID="lblHelpLabel" cssclass="sInfo"></asp:Label><br><asp:Label Runat=server ID="lblLegend" cssclass="sInfo"></asp:Label></td></tr>
						</table>
						<hr width="100%" class="sHr">
					</td>
				</tr>
				<tr>
					<td height="1"><uclayout:footer id="layoutFooter" runat="server"></uclayout:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
