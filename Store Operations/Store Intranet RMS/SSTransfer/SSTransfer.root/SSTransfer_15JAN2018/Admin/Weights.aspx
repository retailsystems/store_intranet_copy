<%@ Register TagPrefix="uc1" TagName="Header" Src="../Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../Layout/Footer.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Weights.aspx.vb" Inherits="SSTransfer.Weights" smartNavigation="False"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Admin - Weights</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Hottopic.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
	</HEAD>
	<body class="sBody" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmDebug" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1%"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="center" align="middle">
						<table height="100%" cellSpacing="0" cellPadding="4" width="100%">
							<tr>
								<td vAlign="top" width="100%"><font face="arial" color="#ff0000" size="2"><b>Box Include 
											Weights</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td align="middle">
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 320px">
													<table cellSpacing="0" cellPadding="2" width="100%" border="0">
														<tr>
															<td>
																<asp:datagrid id="grdBoxIncludeWeight" runat="server" CssClass="sInfoDatagrid" AllowSorting="False" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="false" BorderWidth="0" AutoGenerateColumns="False">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="Box_Includes_Cd" Visible="false"></asp:BoundColumn>
																		<asp:BoundColumn DataField="Box_Includes_Desc" HeaderText="Box Includes Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="LBS">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:TextBox Width="40" Runat="server" Text='<%# Container.dataitem("Item_LBS").tostring %>' ID="txtWeight">
																				</asp:TextBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right"><asp:button id="btnUpdate" runat="server" CssClass="sButton" EnableViewState="False" Text="Update"></asp:button></td>
										</tr>
									</table>
									<font face="arial" color="#ff0000" size="2"><b>Merchandise</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td>
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 320px">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td><asp:datagrid id="dgMerchandiseWeights" runat="server" CssClass="sInfoDatagrid" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="false" BorderWidth="0" AutoGenerateColumns="False">
																	<AlternatingItemStyle CssClass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="Weight_Id" Visible="false"></asp:BoundColumn>
																		<asp:BoundColumn DataField="Weight_Type" HeaderText="Weight Type">
																			<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Weight_Type_Code" HeaderText="Weight Type Code">
																			<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="LBS" HeaderText="LBS">
																			<HeaderStyle CssClass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																	
																		<asp:TemplateColumn HeaderText="Delete">
																			<HeaderStyle HorizontalAlign="Center" CssClass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			<ItemTemplate>
																				<asp:CheckBox Runat="server" ID="chkDeleteWeight"></asp:CheckBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right"><input class="sButton" onclick="javasvript:window.open('AddForms_Weights/AddWeight.aspx','AddWeight','center=1,scroll=0,status=0,width=650,height=200',resizable=0);" type="button" value="Add Weight">&nbsp;
												<asp:button id="btnDeleteMerchandiseWeight" runat="server" CssClass="sButton" EnableViewState="False" Text="Delete"></asp:button></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1%"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
