<%@ Register TagPrefix="uc1" TagName="Footer" Src="../Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Layout/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Debug.aspx.vb" Inherits="SSTransfer.Debug" smartNavigation="False"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Admin - Debug</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Hottopic.css" type="text/css" rel="stylesheet" runat="server" /></LINK>
	</HEAD>
	<body class="sBody" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmDebug" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1%"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="center" align="middle">
						<table height="100%" cellSpacing="0" cellPadding="4" width="100%">
							<tr>
								<td vAlign="top" width="100%"><font face="arial" color="#ff0000" size="2"><b>Users Online</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td align="middle">
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 220px">
													<table cellSpacing="0" cellPadding="2" width="100%" border="0">
														<tr>
															<td><asp:datagrid id="grdEmpOnline" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="false" Width="100%" CellPadding="2" CellSpacing="1" AllowSorting="False" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="StoreNum" HeaderText="Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Emp_Id" HeaderText="Emp Id">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderText="Emp Name">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:Label Runat="server" ID="fullname">
																					<%# Container.dataitem("NameFirst").tostring.trim & " " & Container.dataitem("NameLast").tostring.trim  %>
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:BoundColumn DataField="CurrentPage" HeaderText="Page">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Created_Date" HeaderText="Last Trans">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Log Off">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<a href='debug.aspx?Mode=1&EmpId=<%# Container.dataitem("Emp_Id").tostring %>'><b>Log-Off</b></a>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right">
												<asp:button id="btnLoggOff" runat="server" EnableViewState="False" CssClass="sButton" Text="Log Off All"></asp:button>
											</td>
										</tr>
									</table>
									<font face="arial" color="#ff0000" size="2"><b>UPS Interface Errors</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td>
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 220px">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td><asp:datagrid id="dgXMLErrorLog" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="true" Width="100%" CellPadding="2" CellSpacing="1" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="Box_Id" HeaderText="Transfer #">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="XMLRequest_Type" HeaderText="Request Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ErrorSeverity" HeaderText="Error Severity">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ErrorCode" HeaderText="Error Code">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ErrorDescription" HeaderText="Error Description">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderText="XML Response" ItemStyle-HorizontalAlign="center">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<a onclick="javasvript:window.open('ViewForms_Debug/ViewXML.aspx?Type=1&EID=<%# Container.dataitem("UPS_XMLError_Id") %>','ViewXMLResponse','center=,scroll=0,status=0,Width=700,Height=420,resizable=1');" href='#'>
																					<asp:Label Runat="server" Font-Bold="True" ID="Label4">VIEW</asp:Label>
																				</a>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText="XML Request" ItemStyle-HorizontalAlign="center">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<a onclick="javasvript:window.open('ViewForms_Debug/ViewXML.aspx?Type=2&EID=<%# Container.dataitem("UPS_XMLError_Id") %>','ViewXMLRequest','center=,scroll=0,status=0,Width=700,Height=420,resizable=1');" href='#'>
																					<asp:Label Runat="server" Font-Bold="True" ID="Label5">VIEW</asp:Label>
																				</a>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:BoundColumn DataField="Created_Date" HeaderText="Error Date">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right">
												<asp:button id="btnArchiveUPS" runat="server" EnableViewState="False" CssClass="sButton" Text="Archive All"></asp:button>&nbsp;
												<asp:button id="btnDeleteUPS" runat="server" EnableViewState="False" CssClass="sButton" Text="Delete All"></asp:button>
											</td>
										</tr>
									</table>
									<font face="arial" color="#ff0000" size="2"><b>Application Errors</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td>
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 220px">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td><asp:datagrid id="dgAppErrors" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="true" Width="100%" CellPadding="2" CellSpacing="1" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="App_Error_Id" HeaderText="Error Id">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Error Message">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<a onclick="javasvript:window.open('ViewForms_Debug/viewAppError.aspx?AppError=<%# Container.dataitem("App_Error_Id").tostring %>','ViewXMLRequest','center=,scroll=0,status=0,Width=700,Height=420,resizable=1');" href='#'>
																					<b>View</b></a>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:BoundColumn DataField="Relevent_Data" HeaderText="Relevent Data">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Created_Date" HeaderText="Created Date">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right">
												<asp:button id="btnArchiveApplication" runat="server" EnableViewState="False" CssClass="sButton" Text="Archive All"></asp:button>&nbsp;
												<asp:button id="btnDeleteApplication" runat="server" EnableViewState="False" CssClass="sButton" Text="Delete All"></asp:button>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1%"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
