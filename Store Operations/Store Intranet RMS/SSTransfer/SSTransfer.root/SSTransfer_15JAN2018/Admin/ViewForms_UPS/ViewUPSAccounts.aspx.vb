Public Class ViewUPSAccounts
    Inherits System.Web.UI.Page
    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
    Protected WithEvents lblStores As System.Web.UI.WebControls.Label
    Protected WithEvents lbStores As System.Web.UI.WebControls.ListBox
    Protected WithEvents ddlStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnAddStore As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteStore As System.Web.UI.WebControls.Button

    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        If Not Page.IsPostBack Then
            FillStores()
            FillNewStore()
        End If

        lblHeader.Text = Request("Account") & " Stores"

    End Sub

    Private Sub FillNewStore()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("asFillTStore", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlStore.DataSource = objDataReader
        ddlStore.DataBind()

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub FillStores()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spGetUPSAccountStores " & Request("MYUPSID"), objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        lbStores.DataSource = objDataReader
        lbStores.DataBind()

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Function GetSelectedItems() As String

        Dim I As Integer
        Dim strTemp As String

        For I = 0 To lbStores.Items.Count - 1
            If lbStores.Items(I).Selected Then
                strTemp = strTemp & lbStores.Items(I).Value & ","
            End If
        Next

        If Len(strTemp) > 0 Then
            Return Mid(strTemp, 1, Len(strTemp) - 1)
        Else
            Return strTemp
        End If

    End Function

    Private Sub btnAddStore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddStore.Click

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = ".spInsertUPSUserAccountStore " & ddlStore.SelectedItem.Value & "," & Request("MYUPSID")

        objCommand.CommandText = strStoredProcedure

        objCommand.ExecuteNonQuery()
        objCommand.Dispose()

        FillStores()
        FillNewStore()

    End Sub

    Private Sub btnDeleteStore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteStore.Click

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spDeleteUPSAccountStores '" & GetSelectedItems() & "'"

        objCommand.CommandText = strStoredProcedure

        objCommand.ExecuteNonQuery()
        objCommand.Dispose()

        FillStores()
        FillNewStore()

    End Sub

End Class
