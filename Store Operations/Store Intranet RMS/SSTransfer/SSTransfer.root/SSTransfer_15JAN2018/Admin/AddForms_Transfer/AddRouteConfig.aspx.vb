Public Class AddRouteConfig
    Inherits System.Web.UI.Page

    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
    Protected WithEvents lblConfigType As System.Web.UI.WebControls.Label
    Protected WithEvents ddlConfigType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblTType As System.Web.UI.WebControls.Label
    Protected WithEvents ddlTType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblStart As System.Web.UI.WebControls.Label
    Protected WithEvents txtStart As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEnd As System.Web.UI.WebControls.Label
    Protected WithEvents txtEnd As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSendingStore As System.Web.UI.WebControls.Label
    Protected WithEvents ddlSendingStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblSendingGroup As System.Web.UI.WebControls.Label
    Protected WithEvents lblReceivingStore As System.Web.UI.WebControls.Label
    Protected WithEvents ddlReceivingStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblReceivingGroup As System.Web.UI.WebControls.Label
    Protected WithEvents btnAddRouteConfig As System.Web.UI.WebControls.Button
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ddlSendingGroup As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlReceivingGroup As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        If Not Page.IsPostBack Then
            FillConfigType()
            FillSStoreGroups()
            FillRStoreGroups()
            FillTType()
            FillRStores()
            FillSStores()
        End If
    End Sub

    Private Sub FillRStores()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("asFillTStore", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlReceivingStore.DataSource = objDataReader
        ddlReceivingStore.DataBind()

        objItem.Text = "<Please select>"
        objItem.Value = "NULL"
        ddlReceivingStore.Items.Insert(0, objItem)

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub FillSStores()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("asFillTStore", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlSendingStore.DataSource = objDataReader
        ddlSendingStore.DataBind()

        objItem.Text = "<Please select>"
        objItem.Value = "NULL"
        ddlSendingStore.Items.Insert(0, objItem)

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub FillTType()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spGetTType", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlTType.DataSource = objDataReader
        ddlTType.DataBind()

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub FillRStoreGroups()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spGetStoreGroups", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlReceivingGroup.DataSource = objDataReader
        ddlReceivingGroup.DataBind()

        objItem.Text = "<Please select>"
        objItem.Value = "NULL"
        ddlReceivingGroup.Items.Insert(0, objItem)

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub FillSStoreGroups()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spGetStoreGroups", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlSendingGroup.DataSource = objDataReader
        ddlSendingGroup.DataBind()

        objItem.Text = "<Please select>"
        objItem.Value = "NULL"
        ddlSendingGroup.Items.Insert(0, objItem)

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub FillConfigType()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spGetConfigTypes", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlConfigType.DataSource = objDataReader
        ddlConfigType.DataBind()

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Function IsValidForm() As Boolean

        Dim booReturn As Boolean = True

        If Len(txtStart.Text) > 0 Then
            If Not IsDate(txtStart.Text) Then
                booReturn = False
                lblError.Text = "[Start Date] is invalid. "
            End If
        End If

        If Len(txtEnd.Text) > 0 Then
            If Not IsDate(txtEnd.Text) Then
                booReturn = False
                lblError.Text += "[End Date] is invalid. "
            End If
        End If

        If booReturn And Len(txtEnd.Text) > 0 And Len(txtStart.Text) > 0 Then
            If txtEnd.Text < txtStart.Text Then
                booReturn = False
                lblError.Text = "[End Date] can not come before the start date. "
            End If
        End If

        lblError.Visible = Not booReturn

        Return booReturn

    End Function


    Private Sub btnAddRouteConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddRouteConfig.Click

        If IsValidForm() Then

            Dim strStoredProcedure As String
            Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            Dim strStartDate As String
            Dim strEndDate As String

            If Len(txtStart.Text) > 0 Then
                strStartDate = "'" & Replace(txtStart.Text, "'", "''") & "'"
            Else
                strStartDate = "NULL"
            End If

            If Len(txtEnd.Text) > 0 Then
                strEndDate = "'" & Replace(txtEnd.Text, "'", "''") & "'"
            Else
                strEndDate = "NULL"
            End If


            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spInsertRouteConfig '" & ddlConfigType.SelectedItem.Value & "'," & ddlTType.SelectedItem.Value & "," & ddlSendingStore.SelectedItem.Value & "," & ddlSendingGroup.SelectedItem.Value & "," & ddlReceivingStore.SelectedItem.Value & "," & ddlReceivingGroup.SelectedItem.Value & "," & strStartDate & "," & strEndDate

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()
            objCommand.Dispose()

            pageBody.Attributes.Add("onload", "window.opener.location=window.opener.location;")

        End If

    End Sub

End Class
