Public Class LogOff
    Inherits System.Web.UI.Page
    Protected WithEvents grdEmpOnline As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
    Private m_strShipmentId As String
    Private m_intTrackingInfoId As Int32
    Private m_strBoxId As String
    Private m_strEmpId As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Select Case Request("Mode")
            Case 1 'Logg Off User
                m_strEmpId = Request("EmpId")
                LogOffEmp()

            Case 2 'Void Shipping Label
                m_strShipmentId = Request("SID") 'UPS XML Shipping Id
                m_intTrackingInfoId = Request("TID") ' UPS Tracking Number
                m_strBoxId = Request("BID")
                'VoidShippingLabel()

        End Select

        GetEmpOnline()
    End Sub
    Private Sub LogOffEmp()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spLogOffEmp '" & m_strEmpId & "' "

        objCommand.CommandText = strStoredProcedure

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()

        objConnection.Close()
        objConnection.Dispose()

    End Sub
    Private Sub GetEmpOnline()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetEmpOnline"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        grdEmpOnline.DataSource = objDataReader
        grdEmpOnline.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

End Class
