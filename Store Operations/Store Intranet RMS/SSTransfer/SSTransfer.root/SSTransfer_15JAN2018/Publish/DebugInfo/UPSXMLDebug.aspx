<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UPSXMLDebug.aspx.vb" Inherits="SSTransfer.UPSXMLDebug" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>UPSXMLDebug</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bgColor="#FFFFFF">
		<form id="Form1" method="post" runat="server">
			<table width="100%" cellpadding="2" cellspacing="0">
				<tr>
					<td>
						<asp:DataGrid id="dgXMLErrorLog" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="black" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="true" BorderWidth="0" AutoGenerateColumns="False" BackColor="#FFFFFF">
							<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
							<Columns>
								<asp:BoundColumn DataField="Box_Id" HeaderText="Transfer #">
									<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="XMLRequest_Type" HeaderText="Request Type">
									<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ErrorSeverity" HeaderText="Error Severity">
									<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ErrorCode" HeaderText="Error Code">
									<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ErrorDescription" HeaderText="Error Description">
									<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderText="XML Response" ItemStyle-HorizontalAlign="center">
									<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
									<ItemTemplate>
										<a href='ViewXML.aspx?Type=1&EID=<%# Container.dataitem("UPS_XMLError_Id") %>'>
											<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" ForeColor="black" ID="Label1">VIEW</asp:Label>
										</a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="XML Request" ItemStyle-HorizontalAlign="center">
									<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
									<ItemTemplate>
										<a href='ViewXML.aspx?Type=2&EID=<%# Container.dataitem("UPS_XMLError_Id") %>'>
											<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" ForeColor="black" ID="Label2">VIEW</asp:Label>
										</a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="Created_Date" HeaderText="Error Date">
									<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
								</asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
