<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AddGroup.aspx.vb" Inherits="SSTransfer.AddGroup"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Add Group</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
	</HEAD>
	<body class="sBody" id="pageBody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0" runat="server">
		<form id="frmAddGroup" method="post" runat="server" onsubmit="return checkSubmit();">
			<table width="100%" class="sInfoTable" height="100%" cellSpacing="0" cellPadding="1">
				<tr>
					<td height="1%" align="middle" class="sDialogHeader" colSpan="2"><asp:label id="lblHeader" runat="server" EnableViewState="False">Add Group</asp:label></td>
				</tr>
				<tr>
					<td height="99%" align="middle">
						<table cellSpacing="0" cellPadding="2" width="100%">
							<tr>
								<td nowrap width="1%">
									<asp:label id="lblGroupDescription" runat="server" CssClass="Plain">Group Description:</asp:label>
								</td>
								<td>
									<asp:TextBox ID="txtGroupDescription" Runat="server" Height="100%" Width="100%" MaxLength="25"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<asp:label id="lblStores" runat="server" CssClass="Plain">Stores:</asp:label>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<asp:ListBox DataTextField="StoreName" DataValueField="StoreNum" ID="lbStores" Runat="server" Rows="15" Width="100%" SelectionMode="Multiple"></asp:ListBox>
								</td>
							</tr>
						</table>
						<br>
						<table width="100%">
							<tr>
								<td align="left">
									<input type="button" value="Cancel" onclick="javascript:window.close();" class="sButton">
								</td>
								<td align="right">
									<asp:Button Runat="server" CssClass="sButton" Text="Add Group" ID="btnAddGroup"></asp:Button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
