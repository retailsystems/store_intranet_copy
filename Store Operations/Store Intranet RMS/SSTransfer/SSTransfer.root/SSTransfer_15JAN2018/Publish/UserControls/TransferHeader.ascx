<%@ Control Language="vb" AutoEventWireup="false" Codebehind="TransferHeader.ascx.vb" Inherits="SSTransfer.TransferHeader" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<LINK id="lnkStyles" href="http://172.16.2.13/StyleSheets/hottopic.css" type="text/css"
	rel="stylesheet" runat="server">
</LINK>
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
		<td vAlign="top">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><asp:label id="lblShipDate_Name" runat="server" CssClass="sScanItemBlackLabel" EnableViewState="False">Ship Date:</asp:label>&nbsp;&nbsp;</td>
					<td><asp:label id="lblShipDate_Value" runat="server" CssClass="Plain"></asp:label></td>
					<td><asp:label id="lblUPS_Name" runat="server" CssClass="sScanItemBlackLabel" EnableViewState="False">Tracking #:</asp:label></td>
					<td colSpan="3"><asp:hyperlink id="hlUPS" runat="server" CssClass="Plain" NavigateUrl="" Target="_blank">
							<asp:Label runat="server" ID="lblUPS_Value"></asp:Label>
						</asp:hyperlink></td>
				</tr>
				<tr>
					<td noWrap width="1%"><asp:label id="lblTransferId_Name" runat="server" CssClass="sScanItemBlackLabel" EnableViewState="False">Transfer ID:</asp:label>&nbsp;&nbsp;</td>
					<td><asp:label id="lblTransferId_Value" runat="server" cssclass="Plain"></asp:label></td>
					<td noWrap width="1%"><asp:label id="lblEmp_name" runat="server" CssClass="sScanItemBlackLabel" EnableViewState="False">Employee:</asp:label></td>
					<td><asp:label id="lblEmp_Value" runat="server" CssClass="Plain"></asp:label><input class="sButton" id="btnHistory" style="HEIGHT: 20px" type="button" value="H" runat="server"></td>
					<td><asp:label id="lblRANum_Name" runat="server" CssClass="sScanItemBlackLabel" EnableViewState="False">RA Num:</asp:label>&nbsp;&nbsp;</td>
					<td><asp:label id="lblRANum_Value" runat="server" cssclass="Plain"></asp:label></td>
				</tr>
				<tr>
					<td noWrap width="1%"><asp:label id="lblStatus_Name" runat="server" CssClass="sScanItemBlackLabel" EnableViewState="False">Status:</asp:label>&nbsp;&nbsp;</td>
					<td><asp:label id="lblStatus_Value" runat="server" cssclass="Plain"></asp:label><asp:dropdownlist id="ddlStatus" runat="server" Visible="False"></asp:dropdownlist></td>
					<td noWrap width="1%"><asp:label id="lblSensor" runat="server" CssClass="sScanItemBlackLabel" EnableViewState="False">Sensor Tags:</asp:label></td>
					<td vAlign="middle" colSpan="3"><asp:label id="lblInk_Value" runat="server" cssclass="Plain"></asp:label></td>
				</tr>
				<tr>
					<td noWrap width="1%"><asp:label id="lblSStore_Name" runat="server" CssClass="sScanItemBlackLabel" EnableViewState="False">Sending Store:</asp:label>&nbsp;&nbsp;</td>
					<td><asp:label id="lblSStore_Value" runat="server" cssclass="Plain"></asp:label><asp:dropdownlist id="ddlSStore" runat="server" Visible="False" DataTextField="StoreNum" DataValueField="StoreNum"></asp:dropdownlist></td>
					<td noWrap width="1%"><asp:label id="lblRStore_Name" runat="server" EnableViewState="False" cssclass="sTransferHeaderHighlight">Receiving Store:</asp:label>&nbsp;&nbsp;</td>
					<td colSpan="3"><asp:textbox id="txtRStore" runat="server" Visible="False" Width="80px" MaxLength="4"></asp:textbox><asp:dropdownlist id="ddlRStore" runat="server" Visible="False" DataTextField="StoreName" DataValueField="StoreNum"
							AutoPostBack="True"></asp:dropdownlist><asp:button id="btnStoreChange" runat="server" name="btnStoreChange" Text=" " BackColor="Transparent"
							BorderColor="Transparent" BorderStyle="None"></asp:button><asp:label id="lblRStore_Value" runat="server" cssclass="Plain" font-bold="true"></asp:label></td>
				</tr>
				<tr id="trDiscrepancy" runat="server">
					<td noWrap width="1%"><asp:label id="lblDisQuant_Name" runat="server" CssClass="sScanItemBlackLabel" EnableViewState="False">Discrepancy Quant:</asp:label>&nbsp;&nbsp;</td>
					<td><asp:label id="lblDisQuant_Value" runat="server" cssclass="Plain"></asp:label></td>
					<td noWrap width="1%"><asp:label id="lblDisAmount_Name" runat="server" CssClass="sScanItemBlackLabel" EnableViewState="False">Discrepancy Amount:</asp:label>&nbsp;&nbsp;</td>
					<td colSpan="3"><asp:label id="lblDisAmount_Value" runat="server" cssclass="Plain"></asp:label></td>
				</tr>
				<tr>
					<td colSpan="6"><asp:label id="lblNotes" EnableViewState="False" cssclass="sTransferHeaderHighlight" Runat="server">Shipment Notes: Remember to include notes when shipping items other than Merchandise</FONT></SPAN></asp:label></td>
				</tr>
				<tr>
					<td colSpan="6"><asp:textbox id="txtNotes" runat="server" CssClass="sTransferHeaderTextBox" Width="99%" Height="50px"
							TextMode="MultiLine"></asp:textbox></td>
				</tr>
			</table>
		</td>
		<td vAlign="top">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><asp:label id="lblBoxIncludes_Name" runat="server" EnableViewState="False" cssclass="sTransferHeaderHighlight">Box Includes:</asp:label></td>
				</tr>
				<tr>
					<td>
						<div class="sTransferHeaderDiv" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 130px"><asp:checkboxlist id="cblBoxIncludes" runat="server" CssClass="sBoxIncludes" CellSpacing="0" CellPadding="0"></asp:checkboxlist><asp:label id="lblBoxIncludes" runat="server" cssclass="sBoxIncludes" Visible="False"></asp:label></div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<asp:placeholder id="phFocusRStore" runat="server" EnableViewState="False" Visible="false">
	<SCRIPT language="javascript">
		<!--
		document.forms[0].ucTransferHeader_txtRStore.focus();
		-->
	</SCRIPT>
</asp:placeholder>
