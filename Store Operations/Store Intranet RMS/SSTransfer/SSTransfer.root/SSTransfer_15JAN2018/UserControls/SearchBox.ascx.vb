Public MustInherit Class FilterBox
    Inherits System.Web.UI.UserControl
    Protected WithEvents btnFilter As System.Web.UI.WebControls.Button
    Protected WithEvents lblFilter As System.Web.UI.WebControls.Label
    Protected WithEvents txtFilterQuery As System.Web.UI.WebControls.TextBox
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

    Public Event OnFilterOn(ByVal FilterString As String)
    Public Event OnNewTransfer()
    Public Event OnFilterOff()
    Public Event OnCheckIn()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub btnFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilter.Click

        Dim converter As New System.Drawing.ColorConverter()
        Dim colOn As System.Drawing.Color = converter.ConvertFrom("0xFF990000")
        Dim colOff As System.Drawing.Color = converter.ConvertFrom("0xFF440000")
        Dim CurrentColor As String

        CurrentColor = btnFilter.BackColor.ToString
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        If CurrentColor = "Color [A=255, R=68, G=0, B=0]" And txtFilterQuery.Text.Length > 0 Then

            txtFilterQuery.ReadOnly = True
            btnFilter.BackColor = colOn
            lblFilter.Visible = True
            RaiseEvent OnFilterOn(txtFilterQuery.Text)

        Else

            txtFilterQuery.ReadOnly = False
            btnFilter.BackColor = colOff
            txtFilterQuery.Text = ""
            lblFilter.Visible = False
            RaiseEvent OnFilterOff()

        End If


    End Sub

End Class
