Imports System.Web.Security

Public Class SessionEnd
    Inherits System.Web.UI.Page

    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

    Private m_strMode As String 'Header Button Group 


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Get group from query string
        m_strMode = Request("Mode")
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        'Clear .net authentication cookies on client browser
        FormsAuthentication.SignOut()

        'Set header title
        ucHeader.lblTitle = "Session Timeout"

        'Set header buttons
        If m_strMode = 1 Then
			ucHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Transfer)
		ElseIf m_strMode = 2 Then
			ucHeader.CurrentMode(SSTransfer.Header.HeaderGroup.IC)
        End If

    End Sub

End Class
