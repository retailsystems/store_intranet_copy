<%@ Register TagPrefix="uc1" TagName="TransferBox" Src="UserControls/TransferBox.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TransferHeader" Src="UserControls/TransferHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CheckInTransfer.aspx.vb" Inherits="SSTransfer.CheckInTransfer" smartNavigation="False" %>
<%@ Register TagPrefix="uc1" TagName="ScanItem" Src="UserControls/ScanItem.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Transfer Home</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
        <script language="JavaScript" src="Javascript/jquery-3.3.1.min.js"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server">

		</LINK>
       
        <script type="text/javascript">
        $(document).ready(function () {
            setWidth();
        });
        function setWidth() {
            $('#footerTable').width($('#checkInTransfer').width());
            var typeHeader =$('.typeHeader')[0];
            var imgHeader = $('.imgHeader')[0];
            var col = $('#lastColumn');
            $(col).width($(typeHeader).width() + $(imgHeader).width() +5);
            var totalcol = $('#totalColumn');
            var td1 = $('#totalColumnShipped');
            var shippedQtyHeader = $('.qtyReceived')[0];
            $(totalcol).width($(shippedQtyHeader).width());
            var shippedQtyHeader = $('.qtyShipped')[0];
            $(td1).width($(shippedQtyHeader).width());
            //$(col).html(($(typeHeader).width() - $(imgHeader).width()).toFixed(2));
        }
        
    </script>
	</HEAD>
	<body onresize="setWidth()" class="sBody" id="pageBody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0" runat="server">
		<form id="frmTransferHome" onsubmit="return checkSubmit();" method="post" runat="server">
			<table id="Table1" height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1"><uc1:header id="layoutHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="top" align="left">
						<table id="Table2" cellSpacing="0" cellPadding="2" width="100%" border="0">
							<tr>
								<td align="middle"><uc1:transferheader id="ucTransferHeader" runat="server"></uc1:transferheader></td>
							</tr>
						</table>
						<hr width="100%">
						<table id="Table3" cellSpacing="0" cellPadding="2" width="100%">
							<tr>
								<td align="middle"><uc1:scanitem id="ucScanItem" runat="server"></uc1:scanitem></td>
							</tr>
						</table>
						<hr width="100%">
						<table id="Table4" cellSpacing="0" cellPadding="2" width="100%" border="0">
							<tr>
								<td align="middle">
									<div id="checkInTransfer" style="OVERFLOW-Y: auto; SCROLLBAR-HIGHLIGHT-COLOR: #d60c8c; WIDTH: 100%; SCROLLBAR-ARROW-COLOR: #ffffff; SCROLLBAR-BASE-COLOR: #440000; MAX-HEIGHT: 200px" onmouseout="javascript:var ucScanItem_txtSkuNumber=document.getElementById('ucScanItem_txtSkuNumber');if(!ucScanItem_txtSkuNumber.disabled){ ucScanItem_txtSkuNumber.focus(); }"><uc1:transferbox id="ucTransferBox" runat="server"></uc1:transferbox></div>
								</td>
							</tr>
						</table>
                         <table id="footerTable" style="background-color: #dcddde;border-top: 1px solid;">
                                <tbody>
                                    <tr>
                                    <td align="right"> 
                                    <asp:Label ID="lblShippedQuantity" runat="server" Text="Total Quantity:" cssclass="sScanItemBlackLabel"></asp:Label>
                                   </td>
                                    <td align="center" id="totalColumnShipped" style="border-left: 1px solid gray; border-right: 1px solid gray;">
                                            <asp:label id="lblShippedQuantityTotal" runat="server" text="0" cssclass="sScanItemBlackLabel"></asp:label>
                                           
                                    </td>
                                    <td align="center" id="totalColumn" style="border-left:1px solid gray;border-right:1px solid gray;"> 
                                           <asp:Label ID="lblQuantityTotal"  runat="server" Text="0" cssclass="sScanItemBlackLabel"></asp:Label>
                                     </td>
                                    <td  id="lastColumn">
                                     </td>
                                    </div>
                                    </tr>
                                </tbody>
                            </table>
                         
                         
						<br>
						<table id="Table5" cellSpacing="0" cellPadding="2" width="98%" border="0">
							<tr>
								<td align="middle"><asp:button id="btnUpdate" runat="server" Visible="False" Text="Update" CssClass="sButton"></asp:button>&nbsp;
									<asp:button id="btnCancel" runat="server" Visible="False" Text="Cancel" CssClass="sButton"></asp:button><asp:button id="btnEdit" runat="server" Text="Edit" CssClass="sButton"></asp:button>&nbsp;
									<asp:button id="btnAcceptAll" runat="server" Text="Accept All" CssClass="sButton"></asp:button>&nbsp;
									<asp:button id="btnSave" runat="server" Text="Save for Later" CssClass="sButton"></asp:button>&nbsp;
									<asp:button id="btnCancelCheckIn" runat="server" Text="Cancel" CssClass="sButton"></asp:button>&nbsp;
									<asp:button id="btnSubmit" runat="server" Text="Complete" CssClass="sButton"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
