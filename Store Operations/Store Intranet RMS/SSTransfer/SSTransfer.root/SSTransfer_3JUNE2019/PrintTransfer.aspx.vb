Public Class PrintTransfer
    Inherits System.Web.UI.Page
    Protected WithEvents pdfIFrame As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnDone As System.Web.UI.WebControls.Button
    Protected WithEvents ucHeader As Header
    Protected WithEvents lblHelpLabel As System.Web.UI.WebControls.Label
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

    Private m_objUserInfo As New UserInfo() 'User Info
    Private m_BoxId As String 'Box Id
    Private m_TType As Int32 'Returns/Store to Store
    Private m_objTransferLock As New TransferLock()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Get employee cookie / session info
        m_objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        'If employee is logged on to the system then their locks are kept alive
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
            m_objTransferLock.KeepLocksAlive(m_objUserInfo.EmpId)
        Else
            Response.Redirect("SessionEnd.aspx?Mode=1")
        End If

        'If store number not set then redirect to set store page
        If m_objUserInfo.StoreNumber = -1 Or m_objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

        'Get Query string variales from the url
        m_BoxId = Request.QueryString("BID")
        m_TType = Request.QueryString("TType")

        'set header properties
        ucHeader.lblTitle = "Print Manifest"
		ucHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Transfer)
		ucHeader.CurrentPage(SSTransfer.Header.PageName.NewTransfer)
        ucHeader.DisableButtons()

        'Generate/Stream PDF to client
        If Not Page.IsPostBack Then
            'PrintPDF()
            'DisplayNumberOfCopiesNeeded()
            'Set iframe link
            pdfIFrame.Attributes("src") = "GenerateManifest.aspx?BID=" & m_BoxId
            'Display number of copies to user
            lblHelpLabel.Text = "Please print <font class='sSelectShip'>1</font> copy of this manifest before continuing."
        End If

    End Sub

    'Display the number of printed copies need
    Private Sub DisplayNumberOfCopiesNeeded()

        Dim intNumberOfCopies As Int32

        intNumberOfCopies = intNumberOfCopies + 1

        If m_TType = 1 Then 'Store Transfer

            intNumberOfCopies = intNumberOfCopies + 1

        ElseIf m_TType = 2 Then 'Returns

            Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            Dim objDataReader As SqlClient.SqlDataReader

            intNumberOfCopies = intNumberOfCopies + 1

            objSQLConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand("spGetBoxTransferTypes '" & m_BoxId & "' ", objSQLConnection)
            objDataReader = objCommand.ExecuteReader()

            Do While objDataReader.Read()

                intNumberOfCopies = intNumberOfCopies + 1

            Loop

            objDataReader.Close()
            objDataReader.Close()
            objSQLConnection.Close()

        End If

        If IsWANtoNONWAN(m_BoxId) Then
            intNumberOfCopies = intNumberOfCopies + 1
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Title=Info&Err=" & Server.UrlEncode("Please include all extra copies in the box.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
        End If

        'Display number of copies to user
        lblHelpLabel.Text = "Please print <b><font color='#D60C8C'>" & intNumberOfCopies & "</font></b> copies of this manifest before continuing."

    End Sub

    'Return True if transfer is WAN to NONWAN
    Private Function IsWANtoNONWAN(ByVal Box_Id As String) As Boolean

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        strStoredProcedure = "spIsWANtoNONWAN"

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)
        Dim WANtoNONWANParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@WANtoNONWAN", SqlDbType.Bit)

        'Set parameter direction
        BoxIdParam.Direction = ParameterDirection.Input
        WANtoNONWANParam.Direction = ParameterDirection.Output

        'Set parameter values
        BoxIdParam.Value = Box_Id

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return CType(WANtoNONWANParam.Value, Boolean)

    End Function

    ' Private Sub PrintPDF()

    'Dim objPDFCreator As New TransferToPDF(strBoxId, objUserInfo.StoreNumber)

    'objPDFCreator.CreatePDFReport(TransferToPDF.PDFReportType.Backstock, "E:\Inetpub\wwwroot\SSTRANSFER\PDFDocuments\")
    'objPDFCreator.CreatePDFReport(TransferToPDF.PDFReportType.Damages, "E:\Inetpub\wwwroot\SSTRANSFER\PDFDocuments\")
    'objPDFCreator.CreatePDFReport(TransferToPDF.PDFReportType.RTV, "E:\Inetpub\wwwroot\SSTRANSFER\PDFDocuments\")
    'objPDFCreator.CreatePDFReport(TransferToPDF.PDFReportType.StoreToStore, "E:\Inetpub\wwwroot\SSTRANSFER\PDFDocuments\")
    ' objPDFCreator.CreatePDFReport(TransferToPDF.PDFReportType.Manifest, Server.MapPath("PDFDocuments\"))

    'objPDFCreator.Close()
    'objPDFCreator = Nothing

    ' End Sub

    'Private Function GetManifest() As String

    'Dim strStoredProcedure As String
    'Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
    'objConnection.Open()

    'strStoredProcedure = "spGetBoxReport '" & strBoxId & "',5"

    'Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
    'Dim objDataReader As SqlClient.SqlDataReader

    'objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

    ' If objDataReader.Read() Then

    'Retu() rn "PDFDocuments\" & objDataReader("FileName")

    'End If

    ' End Function 

    'Clear locks
    Private Sub btnDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDone.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer(m_BoxId)

        Response.Redirect("CreateNewTrans.aspx")
        'Response.Redirect("SelectShipping.aspx?Retry=0&BID=" & Request.QueryString("BID"))
    End Sub

End Class
