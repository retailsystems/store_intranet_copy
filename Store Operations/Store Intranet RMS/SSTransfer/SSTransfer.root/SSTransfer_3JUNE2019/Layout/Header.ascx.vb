Public MustInherit Class Header

    Inherits System.Web.UI.UserControl

    Protected WithEvents lblEmpName As System.Web.UI.WebControls.Label
    Protected WithEvents lbLogoff As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbHome As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbNewTrans As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbCheckIn As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdDebug As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdUPS As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdTransfer As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdStore As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdWeights As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdHome As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdNewTrans As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdCheckIn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbExit As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbDebug As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbUPS As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbTransfer As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbStore As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbWeights As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdSearch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbSearch As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbMaint As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdMaint As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbProfileList As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdProfileList As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbICSearch As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdICSearch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblStore As System.Web.UI.WebControls.Label
    Protected WithEvents tdExit As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblHeaderTitle As System.Web.UI.WebControls.Label
    'Protected WithEvents lbDCMaint As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents tdDCMaint As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbProfileAdmin As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdProfileAdmin As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbAdvSearch As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdAdvSearch As System.Web.UI.HtmlControls.HtmlTableCell

    Private strCurrentPage As PageName
    Private objUserInfo As New UserInfo()  
    Protected WithEvents lbDCMaint As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdDCMaint As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgCompany As System.Web.UI.WebControls.Image
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Private strCurrentMode As HeaderGroup

    Public Enum HeaderGroup
        IC = 1
        Transfer = 2
        Login = 3
        SystemErr = 4
        Admin = 5
    End Enum

    Public Enum PageName
        Home = 1
        NewTransfer = 2
        CheckIn = 3
        Search = 4
        Maint = 5
        IC_Profiles = 6
        IC_Search = 7
        'DCMaint = 8
        ProfileAdmin = 9
        Debug = 10
        UPS = 11
        Transfer = 12
        Store = 13
        Weights = 14
    End Enum
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()

        tdMaint.Visible = False

        imgCompany.ImageUrl = ConfigurationSettings.AppSettings("strCompanyImage")

        lblStore.Text = objUserInfo.StoreNumber
        'lbHome.Style.Add("text-decoration", "none")
        'lbNewTrans.Style.Add("text-decoration", "none")
        'lbCheckIn.Style.Add("text-decoration", "none")
        'lbExit.Style.Add("text-decoration", "none")
        'lbSearch.Style.Add("text-decoration", "none")
        'lbMaint.Style.Add("text-decoration", "none")
        'lbICSearch.Style.Add("text-decoration", "none")
        'lbProfileList.Style.Add("text-decoration", "none")
        ''lbDCMaint.Style.Add("text-decoration", "none")
        'lbProfileAdmin.Style.Add("text-decoration", "none")

        tdSearch.Visible = False
        tdMaint.Visible = False
        tdICSearch.Visible = False
        tdProfileList.Visible = False
        tdHome.Visible = False
        'tdDCMaint.Visible = False
        tdProfileAdmin.Visible = False
        tdDebug.Visible = False
        tdUPS.Visible = False
        tdTransfer.Visible = False
        tdStore.Visible = False
        tdWeights.Visible = False

        DisplayItems()
        HighlightButton()
        
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

    End Sub

    Private Sub HighlightButton()

        Select Case strCurrentPage

            Case 1
                tdHome.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"
            Case 2
                tdNewTrans.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"
            Case 3
                tdCheckIn.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"
            Case 4
                tdSearch.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"
            Case 5
                tdMaint.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"
            Case 6
                tdProfileList.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"
            Case 7
                tdICSearch.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"
                'Case 8
                'tdDCMaint.BgColor = "#ff9ace"
            Case 8
                tdProfileAdmin.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"
            Case 10
                tdDebug.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"
            Case 11
                tdUPS.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"
            Case 12
                tdTransfer.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"
            Case 13
                tdStore.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"
            Case 14
                tdWeights.Attributes.Add("class", "sHeaderLinkbuttonHighlight") 'BgColor = "#ff9ace"

        End Select

    End Sub

    Public Sub DisableButtons()

        lbExit.Enabled = False
        lbHome.Enabled = False
        lbNewTrans.Enabled = False
        lbCheckIn.Enabled = False
        lbSearch.Enabled = False
        lbMaint.Enabled = False
        lbICSearch.Enabled = False
        lbProfileList.Enabled = False
        'lbDCMaint.Enabled = False
        lbProfileAdmin.Enabled = False
        lbLogoff.Enabled = False
        lbDebug.Enabled = False
        lbUPS.Enabled = False
        lbTransfer.Enabled = False
        lbStore.Enabled = False
        lbWeights.Enabled = False

    End Sub

    Private Sub DisplayItems()

        Select Case strCurrentMode

            Case HeaderGroup.IC

                If objUserInfo.EmpId Is Nothing Then
                    lbLogoff.Text = "login"
                Else
                    lblEmpName.Text = objUserInfo.EmpFullName
                End If

                tdProfileList.Visible = True
                tdICSearch.Visible = True
                'tdDCMaint.Visible = True
                tdNewTrans.Visible = False
                tdCheckIn.Visible = False
                tdProfileAdmin.Visible = True

            Case HeaderGroup.Transfer

                tdHome.Visible = True
                tdSearch.Visible = True

                If objUserInfo.EmpId Is Nothing Then
                    lbLogoff.Text = "login"
                End If

                lblEmpName.Text = objUserInfo.EmpFullName
                tdNewTrans.Visible = True
                tdCheckIn.Visible = True

            Case HeaderGroup.SystemErr

                tdNewTrans.Visible = False
                tdCheckIn.Visible = False

                If Not objUserInfo.EmpOnline Then
                    lbLogoff.Text = "&nbsp;"
                Else
                    objUserInfo.LogOff()
                End If

            Case HeaderGroup.Admin

                tdSearch.Visible = False
                tdMaint.Visible = False
                tdNewTrans.Visible = False
                tdCheckIn.Visible = False
                tdICSearch.Visible = False
                tdProfileList.Visible = False
                tdProfileAdmin.Visible = False
                tdDebug.Visible = True
                tdUPS.Visible = True
                tdTransfer.Visible = True
                tdStore.Visible = True
                tdWeights.Visible = True

                If objUserInfo.EmpId Is Nothing Then
                    lbLogoff.Text = "login"
                End If

                lblEmpName.Text = objUserInfo.EmpFullName

            Case Else

                lbLogoff.Text = "login"

        End Select

    End Sub

    Private Sub lbLogoff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbLogoff.Click

        If lbLogoff.Text = "login" Then

            If strCurrentMode = HeaderGroup.Transfer Then
                Response.Redirect("Login.aspx?Mode=1")
            ElseIf strCurrentMode = HeaderGroup.Admin Then
                If UCase(Request.Path) = "/SSTRANSFER/LOGIN.ASPX" Then
                    Response.Redirect("Login.aspx?Mode=2")
                Else
                    Response.Redirect("../Login.aspx?Mode=2")
                End If
            Else
                Response.Redirect("Login.aspx?Mode=0")
            End If

        Else
            Dim objTransferLock As New TransferLock()

            objTransferLock.UnlockTransfer("", "", Session.SessionID)

            objUserInfo.LogOff()

            Session.Abandon()
            objUserInfo.ClearEmployeeSession()
            lbLogoff.Text = "login"

            If strCurrentMode = HeaderGroup.IC Then
                Response.Redirect("IC_ProfileList.aspx")
            ElseIf strCurrentMode = HeaderGroup.Admin Then
                If InStr(UCase(Request.Path), "ADMIN") = 0 Then
                    Response.Redirect("Admin/Debug.aspx")
                Else
                    Response.Redirect("Debug.aspx")
                End If
            Else
                Response.Redirect("default.aspx")
            End If

        End If

    End Sub

    Property lblTitle()
        Get
            Return lblHeaderTitle.Text
        End Get
        Set(ByVal Value)
            lblHeaderTitle.Text = Value
        End Set
    End Property

    Public Sub CurrentPage(ByVal Value As PageName)

        strCurrentPage = Value

    End Sub

    Public Sub CurrentMode(ByVal Value As HeaderGroup)

        strCurrentMode = Value

    End Sub

    Private Sub lbHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbHome.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("default.aspx")

    End Sub

    Private Sub lbNewTrans_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbNewTrans.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("CreateNewTrans.aspx")

    End Sub

    Private Sub lbDebug_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbDebug.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)

        If InStr(UCase(Request.Path), "ADMIN") = 0 Then
            Response.Redirect("Admin/Debug.aspx")
        Else
            Response.Redirect("Debug.aspx")
        End If

    End Sub

    Private Sub lbUPS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbUPS.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)

        If InStr(UCase(Request.Path), "ADMIN") = 0 Then
            Response.Redirect("Admin/UPS.aspx")
        Else
            Response.Redirect("UPS.aspx")
        End If

    End Sub

    Private Sub lbTransfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbTransfer.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)

        If InStr(UCase(Request.Path), "ADMIN") = 0 Then
            Response.Redirect("Admin/Transfer.aspx")
        Else
            Response.Redirect("Transfer.aspx")
        End If

    End Sub

    Private Sub lbStore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbStore.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        If InStr(UCase(Request.Path), "ADMIN") = 0 Then
            Response.Redirect("Admin/Stores.aspx?GroupId=1")
        Else
            Response.Redirect("Stores.aspx?GroupId=1")
        End If

    End Sub

    Private Sub lbWeights_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbWeights.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        If InStr(UCase(Request.Path), "ADMIN") = 0 Then
            Response.Redirect("Admin/Weights.aspx")
        Else
            Response.Redirect("Weights.aspx")
        End If

    End Sub

    Private Sub lbCheckIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbCheckIn.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("SelectTransfer.aspx")

    End Sub

    Private Sub lbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbSearch.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("TransferSearch.aspx")

    End Sub

    Private Sub lbMaint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbMaint.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("StoreSearch.aspx")

    End Sub

    Private Sub lbExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbExit.Click

        Dim objTransferLock As New TransferLock()

        objUserInfo.GetEmployeeInfo()

        If objUserInfo.EmpOnline Then
            If strCurrentMode = HeaderGroup.Admin Then
                Response.Redirect("../EmployeeLogOff.aspx?Mode=" & strCurrentMode)
            Else
                Response.Redirect("EmployeeLogOff.aspx?Mode=" & strCurrentMode)
            End If

            'objUserInfo.LogOff()
        Else
            objTransferLock.UnlockTransfer("", "", Session.SessionID)
            Session.Abandon()

            Response.Write("<script languague=javascript>window.close()</script>")
        End If


    End Sub

    Private Sub lbProfileList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbProfileList.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("IC_ProfileList.aspx")

    End Sub

    Private Sub lbICSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbICSearch.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("IC_Search.aspx")

    End Sub

    Private Sub lbProfileAdmin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbProfileAdmin.Click
        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("IC_ProfileAdmin.aspx")

    End Sub

    'Private Sub lbDCMaint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbDCMaint.Click
    '    Dim objTransferLock As New TransferLock()

    '    objTransferLock.UnlockTransfer("", "", Session.SessionID)
    '    Response.Redirect("IC_DCMaint.aspx")
    'End Sub
End Class
