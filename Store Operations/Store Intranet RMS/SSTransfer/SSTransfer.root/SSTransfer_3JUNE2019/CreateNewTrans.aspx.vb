Public Class CreateNewTrans
    Inherits System.Web.UI.Page
    Protected WithEvents radReturn As System.Web.UI.WebControls.RadioButton
    Protected WithEvents radStore As System.Web.UI.WebControls.RadioButton
    Protected WithEvents btnCreateTrans As System.Web.UI.WebControls.Button
    Protected WithEvents radMM As System.Web.UI.WebControls.RadioButton
    Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents radDDS As System.Web.UI.WebControls.RadioButton

    Private m_objUserInfo As New UserInfo()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        m_objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        'If employee is logged on to the system then update last tran
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
        Else
            Response.Redirect("SessionEnd.aspx?Mode=1")
        End If

        If UCase(ConfigurationSettings.AppSettings("EnableDDSReturn")) = "TRUE" Then
            radDDS.Visible = True
        Else
            radDDS.Visible = False
        End If

        If IsStoDCAllowed(m_objUserInfo.StoreNumber) Then
            radReturn.Enabled = True
        Else
            radReturn.Enabled = False
        End If

        'If store number not set then redirect to set store page
        If m_objUserInfo.StoreNumber = -1 Or m_objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

		ucHeader.CurrentPage(SSTransfer.Header.PageName.NewTransfer)
		ucHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Transfer)
        ucHeader.lblTitle = "New Transfer"
    End Sub

    Private Sub btnCreateTrans_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateTrans.Click

        If radStore.Checked Then
            Page.Response.Redirect("NewTransferForm.aspx?TType=1")
        ElseIf radReturn.Checked Then
            Page.Response.Redirect("NewTransferForm.aspx?TType=2")
        ElseIf radMM.Checked Then
            Page.Response.Redirect("NewTransferForm.aspx?TType=3")
        ElseIf radDDS.Checked Then
            Page.Response.Redirect("NewTransferForm.aspx?TType=4")
        End If

    End Sub

    Private Function IsStoDCAllowed(ByVal SendingStore As Integer) As Boolean

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spIsStoDCAllowed", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim SStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@SStoreNum", SqlDbType.SmallInt)
        Dim IsAllowedParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@IsAllowed", SqlDbType.Bit)

        SStoreParam.Direction = ParameterDirection.Input
        IsAllowedParam.Direction = ParameterDirection.Output

        SStoreParam.Value = SendingStore

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return IsAllowedParam.Value

    End Function

End Class
