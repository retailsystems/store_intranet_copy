Public Class Debug
    Inherits System.Web.UI.Page

    Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents grdEmpOnline As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgAppErrors As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgUPSVoid As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgXMLErrorLog As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label

    Private m_objUserInfo As New UserInfo()
    Private m_strShipmentId As String
    Private m_intTrackingInfoId As Int32
    Private m_strBoxId As String
    Protected WithEvents btnArchiveApplication As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteApplication As System.Web.UI.WebControls.Button
    Protected WithEvents btnLoggOff As System.Web.UI.WebControls.Button
    Protected WithEvents btnArchiveUPS As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteUPS As System.Web.UI.WebControls.Button
    Private m_strEmpId As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strLegend As String

        m_objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        'Get querystring data if Transfer ID(TID) is greater than 0
        Select Case Request("Mode")
            Case 1 'Logg Off User
                m_strEmpId = Request("EmpId")
                LogOffEmp()
        End Select

        'If employee is logged on update last Trans
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
        End If

        'Sets both header title and highlights section button
        ucHeader.lblTitle = "SSTransfer Admin"
		ucHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Admin)
		ucHeader.CurrentPage(SSTransfer.Header.PageName.Debug)



        GetEmpOnline()
        GetXMLErrors()
        GetAppErrors()

    End Sub

    Private Sub LogOffEmp()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spLogOffEmp '" & m_strEmpId & "' "

        objCommand.CommandText = strStoredProcedure

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()

        objConnection.Close()
        objConnection.Dispose()

        If m_objUserInfo.EmpOnline Then

            If m_strEmpId = m_objUserInfo.EmpId.ToString.PadLeft(5, "0"c) Then
                Dim objTransferLock As New TransferLock()

                objTransferLock.UnlockTransfer("", "", Session.SessionID)
                m_objUserInfo.LogOff()
                Session.Abandon()
                m_objUserInfo.ClearEmployeeSession()
            End If

        End If

    End Sub


    Private Sub GetAppErrors()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetAppErrors"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        dgAppErrors.DataSource = objDataReader
        dgAppErrors.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub GetEmpOnline()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetEmpOnline"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        grdEmpOnline.DataSource = objDataReader
        grdEmpOnline.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    'Sends a xml void request to UPS
    Private Function VoidShippingLabel()

        Dim objUPS As New UPS_XML_Shipping()

        objUPS.ShipmentId = m_strShipmentId 'XML UPS Shipment Id
        objUPS.TrackingInfoId = m_intTrackingInfoId
        objUPS.BoxId = m_strBoxId

        'Send XML Void Request
        objUPS.SendVoidShipping()

    End Function

    Private Sub GetVoidData()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        Dim objCommand As New SqlClient.SqlCommand("spGetUPSVoid", objConnection)

        objConnection.Open()

        objDataReader = objCommand.ExecuteReader()

        dgUPSVoid.DataSource = objDataReader
        dgUPSVoid.DataBind()

        objCommand.Dispose()
        objDataReader.Close()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub GetXMLErrors()

        Dim strSQL As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        strSQL = "SELECT * FROM UPS_XML_Errors Order by Created_Date "

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strSQL, objConnection)

        objDataReader = objCommand.ExecuteReader()

        dgXMLErrorLog.DataSource = objDataReader
        dgXMLErrorLog.DataBind()

        objDataReader.Close()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub btnArchiveApplication_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnArchiveApplication.Click

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strArchiveConn"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "ArchiveApplicationErrors"

        objCommand.CommandText = strStoredProcedure

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()

        objConnection.Close()
        objConnection.Dispose()

        GetAppErrors()

    End Sub

    Private Sub btnDeleteApplication_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteApplication.Click

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spDeleteApplicationErrors"

        objCommand.CommandText = strStoredProcedure

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()

        objConnection.Close()
        objConnection.Dispose()
        GetXMLErrors()

    End Sub

    Private Sub btnLoggOff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoggOff.Click

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spLogOffAllEmp"

        objCommand.CommandText = strStoredProcedure

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()

        objConnection.Close()
        objConnection.Dispose()

        GetEmpOnline()

    End Sub

    Private Sub btnArchiveUPS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnArchiveUPS.Click

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strArchiveConn"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "ArchiveUPSErrors"

        objCommand.CommandText = strStoredProcedure

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()

        objConnection.Close()
        objConnection.Dispose()

        GetXMLErrors()

    End Sub

    Private Sub btnDeleteUPS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteUPS.Click

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spDeleteUPSErrors"

        objCommand.CommandText = strStoredProcedure

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()

        objConnection.Close()
        objConnection.Dispose()

        GetXMLErrors()

    End Sub

End Class
