<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Stores.aspx.vb" Inherits="SSTransfer.Stores" smartNavigation="False"%>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Admin - Stores</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Hottopic.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
	</HEAD>
	<body class="sBody" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmDebug" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1%"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="center" align="middle">
						<table height="100%" cellSpacing="0" cellPadding="4" width="100%">
							<tr>
								<td vAlign="top" width="100%"><font face="arial" color="#ff0000" size="2"><b>Groups</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td align="middle">
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 320px">
													<table cellSpacing="0" cellPadding="2" width="100%" border="0">
														<tr>
															<td><asp:datagrid id="grdGroups" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="false" Width="100%" CellPadding="2" CellSpacing="1" AllowSorting="False" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="StoreGroup_Id" Visible="false"></asp:BoundColumn>
																		<asp:BoundColumn DataField="Description" HeaderText="Group Description">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Action">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<a href='Stores.aspx?GroupId=<%# Container.dataitem("StoreGroup_Id").tostring %>'><b>Stores</b></a>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Delete">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:CheckBox Runat="server" ID="chkDeleteGroup"></asp:CheckBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right">
												<input type="button" value="Add Group" onclick="javasvript:window.open('AddForms_Store/AddGroup.aspx','AddGroup','center=,scroll=0,status=0,Width=650,Height=420,resizable=1');" class="sButton">&nbsp;
												<asp:button id="btnDeleteGroup" runat="server" EnableViewState="False" CssClass="sButton" Text="Delete"></asp:button>
											</td>
										</tr>
									</table>
									<font face="arial" color="#ff0000" size="2"><b>Stores in selected group</b></font>
									<table Class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td>
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 320px">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td><asp:datagrid id="dgStores" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="false" Width="100%" CellPadding="2" CellSpacing="1" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="Record_Id" Visible="false"></asp:BoundColumn>
																		<asp:BoundColumn DataField="StoreNum" HeaderText="Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="StoreName" HeaderText="Store Name">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Location" HeaderText="Location">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Description" HeaderText="Group">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Delete">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:CheckBox Runat="server" ID="chkDeleteStore"></asp:CheckBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right">
												<input type="button" value="Add Store" onclick="javasvript:window.open('AddForms_Store/AddStore.aspx?GroupID=<%=GetGroupId()%>','AddStore','center=1,scroll=0,status=0,Width=650,Height=420,resizable=1');" class="sButton">&nbsp;
												<asp:button id="btnDeleteStore" runat="server" EnableViewState="False" CssClass="sButton" Text="Delete"></asp:button>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1%"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
