<%@ Register TagPrefix="uc1" TagName="Header" Src="../Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../Layout/Footer.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UPS.aspx.vb" Inherits="SSTransfer.UPS" smartNavigation="False"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Admin - UPS</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Hottopic.css" type="text/css" rel="stylesheet" runat="server" />
		</LINK>
	</HEAD>
	<body class="sBody" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmDebug" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1%"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="center" align="middle">
						<table height="100%" cellSpacing="0" cellPadding="4" width="100%">
							<tr>
								<td vAlign="top" width="100%"><font face="arial" color="#ff0000" size="2"><b>UPS User Accounts</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td align="middle">
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 220px">
													<table cellSpacing="0" cellPadding="2" width="100%" border="0">
														<tr>
															<td><asp:datagrid id="grdUPSUserAccounts" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="false" Width="100%" CellPadding="2" CellSpacing="1" AllowSorting="False" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="MYUPS_Id" Visible="false"></asp:BoundColumn>
																		<asp:BoundColumn DataField="MYUPS_UserId" HeaderText="User">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="MYUPS_Password" HeaderText="Password">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Action">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<a onclick="javasvript:window.open('ViewForms_UPS/viewUPSAccounts.aspx?ACCOUNT=<%# Container.dataitem("MYUPS_UserId").tostring%>&MYUPSID=<%# Container.dataitem("MYUPS_Id").tostring %>','ViewUPSAccountStores','center=,scroll=0,status=0,Width=700,Height=450,resizable=1');" href='#'><b>Stores</b></a>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Delete">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:CheckBox Runat="server" ID="chkDeleteUPSUserAccount"></asp:CheckBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right"><input class="sButton" onclick="javascript:window.open('AddForms_UPS/AddUPSUserAccount.aspx','UPSUserAccount','center=1,scrollbar=0,status=0,width=550,height=420,resizable=1');" type="button" value="Add Account">&nbsp;
												<asp:button id="btnDeleteUPSAccount" runat="server" EnableViewState="False" CssClass="sButton" Text="Delete"></asp:button></td>
										</tr>
									</table>
									<font face="arial" color="#ff0000" size="2"><b>Shipping Defaults</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td>
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 220px">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td><asp:datagrid id="dgUPSDefaultShipment" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="false" Width="100%" CellPadding="2" CellSpacing="1" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="ShipmentDefault_Id" Visible="false"></asp:BoundColumn>
																		<asp:BoundColumn DataField="Package_Code" HeaderText="Package Code">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Service_Code" HeaderText="Service Code">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="TType" HeaderText="Transfer Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="SendingStore" HeaderText="Sending Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ReceivingStore" HeaderText="Receiving Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Delete">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:CheckBox Runat="server" ID="chkDeleteUPSDefaultShipment"></asp:CheckBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right"><input class="sButton" onclick="javascript:window.open('AddForms_UPS/AddUPSDefaultShipment.aspx','UPSDefaultShipment','center=1,scrollbar=0,status=0,width=650,height=200,resizable=0');" type="button" value="Add Default Shipment">&nbsp;
												<asp:button id="btnDeleteDefualtShipment" runat="server" EnableViewState="False" CssClass="sButton" Text="Delete"></asp:button></td>
										</tr>
									</table>
									<font face="arial" color="#ff0000" size="2"><b>Shipping Packages</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td>
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 220px">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td><asp:datagrid id="dgUPSPackage" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="false" Width="100%" CellPadding="2" CellSpacing="1" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="PackageFilter_Id" Visible="false"></asp:BoundColumn>
																		<asp:BoundColumn DataField="Config_Type" HeaderText="Config Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="TType" HeaderText="Transfer Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Package_Code" HeaderText="UPS Package Code">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="SendingStore" HeaderText="Sending Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ReceivingStore" HeaderText="Receiving Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Delete">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:CheckBox Runat="server" ID="chkDeleteUPSPackage"></asp:CheckBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right"><input class="sButton" onclick="javascript:window.open('AddForms_UPS/AddUPSPackageConfig.aspx','UPSPackageConfig','center=1,scrollbar=0,status=0,width=650,height=200,resizable=0');" type="button" value="Add Package Config">&nbsp;
												<asp:button id="btnDeletePackageConfig" runat="server" EnableViewState="False" CssClass="sButton" Text="Delete"></asp:button></td>
										</tr>
									</table>
									<font face="arial" color="#ff0000" size="2"><b>Shipping Services</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td>
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 220px">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td><asp:datagrid id="grdUPSService" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="false" Width="100%" CellPadding="2" CellSpacing="1" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="ServiceFilter_Id" Visible="false"></asp:BoundColumn>
																		<asp:BoundColumn DataField="Config_Type" HeaderText="Config Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="TType" HeaderText="Transfer Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Service_Code" HeaderText="UPS Service Code">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="SendingStore" HeaderText="Sending Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ReceivingStore" HeaderText="Receiving Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Delete">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:CheckBox Runat="server" ID="chkDeleteUPSService"></asp:CheckBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right"><input class="sButton" onclick="javascript:window.open('AddForms_UPS/AddUPSServiceConfig.aspx','UPSServiceConfig','center=1,scrollbar=0,status=0,width=650,height=200,resizable=0');" type="button" value="Add Service Config">&nbsp;
												<asp:button id="btnDeleteServiceConfig" runat="server" EnableViewState="False" CssClass="sButton" Text="Delete"></asp:button></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1%"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
