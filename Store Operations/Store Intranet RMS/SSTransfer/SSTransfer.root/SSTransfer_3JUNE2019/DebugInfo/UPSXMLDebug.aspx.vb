Public Class UPSXMLDebug
    Inherits System.Web.UI.Page
    Protected WithEvents dgXMLErrorLog As System.Web.UI.WebControls.DataGrid

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strSQL As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        strSQL = "SELECT * FROM UPS_XML_Errors Order by Created_Date "

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strSQL, objConnection)

        objDataReader = objCommand.ExecuteReader()

        dgXMLErrorLog.DataSource = objDataReader
        dgXMLErrorLog.DataBind()

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

End Class
