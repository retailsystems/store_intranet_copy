Public Class AdvancedResults
    Inherits System.Web.UI.Page
    Protected WithEvents dgResults As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Protected WithEvents ucHeader As Header
    Protected WithEvents lblResults As System.Web.UI.WebControls.Label
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Private strSearch As String
    Public strSQL As String
    Public strSearchReturnPage As String
    Private m_objUserInfo As New UserInfo()
    Public strTransfer As String
    Public strUPS As String
    Public strSearchType As String  'Simple Search or Advanced Search
    Public strSearchFor As String   'Void, Expected Arrivals, Shipped Boxes, Received Boxes
    Public strFromStore As String   'From Store
    Public strToStore As String     'To Store
    Public strFromDate As String    'From Date
    Public strThruDate As String    'Thru Date
    Public strTransferTypes As String 'Transfer Types user selected in advanced search.
    Public strBoxStatus As String     'Box Statuses
    Public strBoxIncludes As String   'Box Includes
    Public strSKU As String           'Search with SKU number

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        m_objUserInfo.GetEmployeeInfo()

        'If employee is logged on to the system then update last trans
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
        End If

        'If store number not set then redirect to set store page
        If m_objUserInfo.StoreNumber = -1 Or m_objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

        'Put user code to initialize the page here
        ucHeader.lblTitle = "Search Results"
		ucHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Transfer)
		ucHeader.CurrentPage(SSTransfer.Header.PageName.Search)

        strTransfer = Request.Params("ABID")
        strUPS = Request.Params("UPS")
        strSearchType = Request.Params("ST")
        strSearchFor = Request.Params("SF")
        strFromStore = Request.Params("FStore")
        strToStore = Request.Params("TStore")
        strFromDate = Request.Params("FDate")
        strThruDate = Request.Params("TDate")
        strTransferTypes = Request.Params("TT")
        strBoxStatus = Request.Params("BS")
        strBoxIncludes = Request.Params("BI")
        strSKU = Request.Params("SKU")
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        If Not IsPostBack Then
            strSearchReturnPage = Request.Params("SPRTN")
            Call AssignSQLString()
            If strSearch <> "" Then
                'strSQL = "SELECT distinct(Box_xfer_hdr.Box_Id) as Box_Id, Box_Xfer_Hdr.Tracking_Num, Torrid.dbo.STORE.StoreName as Sending_Store, "
                'strSQL = strSQL & "STORE_1.StoreName as Receiving_Store,Box_Status_Type.Status_Desc, "
                strSQL = "SELECT distinct(Box_xfer_hdr.Box_Id) as Box_Id, ISNULL(Box_Xfer_Hdr.Tracking_Num,'') as Tracking_Num, " & ConfigurationSettings.AppSettings("StoreTableName") & ".StoreNum as Sending_Store, "
                strSQL = strSQL & "STORE_1.StoreNum as Receiving_Store,Box_Status_Type.Status_Desc, ISNULL(ParcelService_Type.TrackingURL,'') as TrackingURL, "
                strSQL = strSQL & "Box_xfer_Hdr.Shipment_Date AS Shipment_Date, Box_xfer_Hdr.Received_Date As Received_Date, "
                strSQL = strSQL & "Box_xfer_PDF.FileName AS Manifest,  Case TType When 3 Then 0 Else 1 End as ManifestDisplay "
                strSQL = strSQL & "FROM Box_xfer_Hdr Left Outer Join Box_xfer_Item ON Box_xfer_Hdr.Box_Id=Box_xfer_Item.Box_Id "
                strSQL = strSQL & "Left Outer Join Box_To_Include ON Box_xfer_Hdr.Box_Id=Box_To_Include.Box_Id "
                strSQL = strSQL & "Left Outer Join " & ConfigurationSettings.AppSettings("StoreTableName") & " ON Box_xfer_Hdr.Sending_Store_Cd=" & ConfigurationSettings.AppSettings("StoreTableName") & ".StoreNum "
                strSQL = strSQL & "Left Outer Join " & ConfigurationSettings.AppSettings("StoreTableName") & " STORE_1 ON Box_xfer_Hdr.Receiving_Store_Cd=STORE_1.StoreNUM "
                strSQL = strSQL & "Left Outer Join Box_Status_Type ON Box_xfer_Hdr.Current_Box_Status_Cd=Box_Status_Type.Status_Cd "
                strSQL = strSQL & "Left Outer Join Box_xfer_PDF ON Box_xfer_Hdr.Box_Id=Box_Xfer_PDF.Box_Id "
                strSQL = strSQL & "Left Outer Join ParcelService_Type ON Box_Xfer_Hdr.ParcelService_Cd=ParcelService_Type.ParcelService_Cd "

                If Len(strSearch) > 0 Then
                    strSQL = strSQL & " WHERE " & Mid(strSearch, 4, Len(strSearch) - 3)
                End If
            End If
            Call CountRecords()
        End If
        strSearch = Request.Params("SQL2")  'check to see if this saves strSearch value
    End Sub
    Private Sub AssignSQLString()
        If strTransfer <> "" And strTransfer <> Nothing Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Box_Id = '" & strTransfer & "' ) "
        End If

        If strUPS <> "" And strUPS <> Nothing Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Tracking_Num = '" & strUPS & "') "
        End If

        If strSKU <> "" And strSKU <> Nothing Then
            strSearch = strSearch & "AND (Box_XFer_Item.SKU_NUM='" & strSKU & "') "
        End If

        '*****************************************************************************************************************
        If strSearchType = "Simple" Then  'TransferSearch.aspx
            If strSearchFor = "Shipped" Then
                'Search all shipped transfers from this store out with this user entered date range
                strSearch = "AND (Box_Xfer_Hdr.Sending_Store_Cd IN (" & (strFromStore) & ")) "
                strSearch = strSearch & "AND (Box_Xfer_Hdr.Shipment_Date >='" & strFromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & strThruDate & "' ) "
            ElseIf strSearchFor = "Received" Then
                'Search all received transfers to this store out with this user entered date range
                strSearch = "AND (Box_Xfer_Hdr.Receiving_Store_Cd IN (" & (strToStore) & ")) "
                strSearch = strSearch & "AND (Box_Xfer_Hdr.Received_Date >='" & strFromDate & "' AND Box_Xfer_Hdr.Received_Date <='" & strThruDate & "' ) "
            End If
        ElseIf strSearchType = "Advanced" Then  'AdvancedSearch.aspx
            If strTransferTypes <> Nothing And strTransferTypes <> "" Then
                'Search string for Transfer Types
                strSearch = strSearch & "AND (Box_Xfer_Item.Xfer_Type_Cd IN (" & (strTransferTypes) & ")) "
            End If

            If strSearchFor = "Voided" Then
                'Box Status
                strSearch = strSearch & "AND (Box_Xfer_Hdr.Sending_Store_Cd IN (" & (strFromStore) & ")) "
                strSearch = strSearch & "AND (Box_Xfer_Hdr.Current_Box_Status_Cd IN (7)) "

                If (strFromDate <> Nothing And strFromDate <> "") And (strThruDate <> Nothing And strThruDate <> "") Then
                    strSearch = strSearch & "AND (Shipment_Date >= '" & strFromDate & "' AND Shipment_Date <= '" & strThruDate & "') "
                End If

                If strBoxIncludes <> Nothing And strBoxIncludes <> "" Then
                    'Search string for Box Includes
                    strSearch = strSearch & "AND (Box_To_Include.Box_Includes_Cd IN (" & (strBoxIncludes) & ")) "
                End If
            ElseIf strSearchFor = "Expected" Then
                strSearch = strSearch & "AND ((Box_Xfer_Hdr.Receiving_Store_Cd IN (" & (strToStore) & ")) AND Box_Xfer_Hdr.Received_Date IS NULL) AND Box_Status_Type.Status_Desc <> 'Voided' "
                strSearch = strSearch & "AND ((Box_xfer_hdr.Shipment_Date IS NOT NULL ) AND (Box_xfer_hdr.Received_Date IS NULL)) "

                If strBoxStatus <> Nothing And strBoxStatus <> "" Then
                    'Search for Specific Box Statuses
                    strSearch = strSearch & "AND (Box_Xfer_Hdr.Current_Box_Status_Cd IN (" & (strBoxStatus) & ")) "
                End If

                If strFromStore <> Nothing And strFromStore <> "" Then
                    'Search certain sending stores
                    strSearch = strSearch & "AND (Box_Xfer_Hdr.Sending_Store_Cd IN (" & (strFromStore) & ")) "
                End If

            ElseIf strSearchFor = "Shipped" Then
                strSearch = strSearch & "AND ((Box_Xfer_Hdr.Sending_Store_Cd IN (" & (strFromStore) & ")) AND Box_Status_Type.Status_Desc <> 'Voided') "

                If strBoxIncludes <> Nothing And strBoxIncludes <> "" Then
                    'Search string for Box Includes
                    strSearch = strSearch & "AND (Box_To_Include.Box_Includes_Cd IN (" & (strBoxIncludes) & ")) "
                End If

                If strBoxStatus <> Nothing And strBoxStatus <> "" Then
                    'Search for Specific Box Statuses
                    strSearch = strSearch & "AND (Box_Xfer_Hdr.Current_Box_Status_Cd IN (" & (strBoxStatus) & ")) "
                End If

                If strToStore <> Nothing And strToStore <> "" Then
                    'Search specific To Stores
                    strSearch = strSearch & "AND (Box_Xfer_Hdr.Receiving_Store_Cd IN (" & (strToStore) & ")) "
                End If

                If (strFromDate <> Nothing And strFromDate <> "") And (strThruDate <> Nothing And strThruDate <> "") Then
                    strSearch = strSearch & "AND (Shipment_Date >= '" & strFromDate & "' AND Shipment_Date <= '" & strThruDate & "') "
                End If

            ElseIf strSearchFor = "Received" Then
                strSearch = strSearch & "AND ((Box_Xfer_Hdr.Receiving_Store_Cd IN (" & (strToStore) & ")) AND Box_Status_Type.Status_Desc <> 'Voided') "

                If strBoxStatus <> Nothing And strBoxStatus <> "" Then
                    'Search for Specific Box Statuses
                    strSearch = strSearch & "AND (Box_Xfer_Hdr.Current_Box_Status_Cd IN (" & (strBoxStatus) & ")) "
                End If

                If strFromStore <> Nothing And strFromStore <> "" Then
                    'Search certain sending stores
                    strSearch = strSearch & "AND (Box_Xfer_Hdr.Sending_Store_Cd IN (" & (strFromStore) & ")) "
                End If

                If (strFromDate <> Nothing And strFromDate <> "") And (strThruDate <> Nothing And strThruDate <> "") Then
                    strSearch = strSearch & "AND (Received_Date >= '" & strFromDate & "' AND Received_Date <= '" & strThruDate & "') "
                End If
            End If
        End If

    End Sub
    Private Sub FillDataGrid()
        'This fills in the results into the datagrid
        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()

        Dim cmdSearch As New SqlClient.SqlCommand(strSQL, conSearch)
        dtrSearch = cmdSearch.ExecuteReader

        dgResults.DataSource = dtrSearch
        dgResults.DataBind()
        dtrSearch.Close()
        conSearch.Close()
        cmdSearch.Dispose()
    End Sub
    Public Sub CountRecords()
        'This subprocedure is used to see if there are records returned
        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()

        'Dim cmdSearch As New SqlClient.SqlCommand(strSearch, conSearch)
        Dim cmdSearch As New SqlClient.SqlCommand(strSQL, conSearch)
        dtrSearch = cmdSearch.ExecuteReader
        '******************************************************
        'Count # of records in datareader
        Dim count As Integer
        count = 0
        Do While dtrSearch.Read
            count = count + 1
        Loop

        '******************************************************
        If count > 0 Then
            'If there are results, fill in datagrid
            Call FillDataGrid()
        Else
            lblResults.Text = "No Results Found."
        End If
        dtrSearch.Close()
        conSearch.Close()
        cmdSearch.Dispose()
    End Sub
    Public Sub Sort(ByVal s As Object, ByVal e As DataGridSortCommandEventArgs)
        'handles users' sort click
        Call AssignSQLString()

        strSQL = "SELECT distinct(Box_xfer_hdr.Box_Id), ISNULL(Box_Xfer_Hdr.Tracking_Num,'') as Tracking_Num, " & ConfigurationSettings.AppSettings("StoreTableName") & ".StoreNum as Sending_Store, "
        strSQL = strSQL & "STORE_1.StoreNum as Receiving_Store,Box_Status_Type.Status_Desc, ISNULL(ParcelService_Type.TrackingURL,'') as TrackingURL, "
        strSQL = strSQL & "Box_xfer_Hdr.Shipment_Date AS Shipment_Date, Box_xfer_Hdr.Received_Date As Received_Date, "
        strSQL = strSQL & "Box_xfer_PDF.FileName AS Manifest,  Case TType When 3 Then 0 Else 1 End as ManifestDisplay "
        strSQL = strSQL & "FROM Box_xfer_Hdr Left Outer Join Box_xfer_Item ON Box_xfer_Hdr.Box_Id=Box_xfer_Item.Box_Id "
        strSQL = strSQL & "Left Outer Join Box_To_Include ON Box_xfer_Hdr.Box_Id=Box_To_Include.Box_Id "
        strSQL = strSQL & "Left Outer Join " & ConfigurationSettings.AppSettings("StoreTableName") & " ON Box_xfer_Hdr.Sending_Store_Cd=" & ConfigurationSettings.AppSettings("StoreTableName") & ".StoreNum "
        strSQL = strSQL & "Left Outer Join " & ConfigurationSettings.AppSettings("StoreTableName") & " STORE_1 ON Box_xfer_Hdr.Receiving_Store_Cd=STORE_1.StoreNUM "
        strSQL = strSQL & "Left Outer Join Box_Status_Type ON Box_xfer_Hdr.Current_Box_Status_Cd=Box_Status_Type.Status_Cd "
        strSQL = strSQL & "Left Outer Join Box_xfer_PDF ON Box_xfer_Hdr.Box_Id=Box_Xfer_PDF.Box_Id "
        strSQL = strSQL & "Left Outer Join ParcelService_Type ON Box_Xfer_Hdr.ParcelService_Cd=ParcelService_Type.ParcelService_Cd "

        If Len(strSearch) > 0 Then
            strSQL = strSQL & " WHERE" & Mid(strSearch, 4, Len(strSearch) - 3)
        End If
        strSQL = strSQL & " Order By " & e.SortExpression
        FillDataGrid()
    End Sub
    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        strSearchReturnPage = Request.Params("SPRTN")
        If strSearchReturnPage = Nothing Then
            strSearchReturnPage = "TransferSearch.aspx"
        End If
        Response.Redirect(strSearchReturnPage)
    End Sub
    Protected Function GetImageURL(ByVal strFileName As String) As String
        If strFileName = "" Then
            Return "images\blank.gif"
        Else
            Return "images\printed.gif"
        End If
    End Function
    Protected Function GetReturnQuery() As String
        'strSearch = Server.UrlEncode(Request.Params("SQL2"))
        'strSearch = Request.Url.Query
        Dim strReturn As String
        'strReturn = "AdvancedResults.aspx?ABID=" & strTransfer & "&UPS=" & strUPS & "&ST=" & strSearchType & "&SF=" & strSearchFor & "&FStore=" & strFromStore & "&TStore=" & strToStore & "&FDate=" & strFromDate & "&TDate=" & strThruDate & "&TT=" & strTransferTypes & "&BI=" & strBoxIncludes & "&BS=" & strBoxStatus & "&SKU=" & strSKU & "&SPRTN=" & strSearchReturnPage
        strReturn = "AdvancedResults.aspx?" & "&ABID=" & strTransfer & "&UPS=" & strUPS & "&ST=" & strSearchType & "&SF=" & strSearchFor & "&FStore=" & strFromStore & "&TStore=" & strToStore & "&FDate=" & strFromDate & "&TDate=" & strThruDate & "&TT=" & strTransferTypes & "&BI=" & strBoxIncludes & "&BS=" & strBoxStatus & "&SKU=" & strSKU & "&SPRTN=" & strSearchReturnPage
        Return strReturn
    End Function
    Public Function GetTrackingURL(ByVal TrackingURL As String, ByVal TrackingNum As String) As String

        If TrackingNum.Length > 0 Then
            Return Replace(TrackingURL, "{0}", TrackingNum)
        Else
            Return ""
        End If

    End Function
End Class
