Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Mail

Public Class ApplicationErrors

    Private m_DBConnection As String
    Private m_SmtpServer As String
    Private m_DBTableName As String
    Private m_EmailTo As String
    Private m_EmailFrom As String
    Private m_EmailSubject As String

    Private m_InfoArray As New Collection()

    WriteOnly Property DBConnection()
        Set(ByVal Value)
            m_DBConnection = Value
        End Set
    End Property

    WriteOnly Property SmtpServer()
        Set(ByVal Value)
            m_SmtpServer = Value
        End Set
    End Property

    WriteOnly Property DBTableName()
        Set(ByVal Value)
            m_DBTableName = Value
        End Set
    End Property

    WriteOnly Property EmailTo()
        Set(ByVal Value)
            m_EmailTo = Value
        End Set
    End Property

    WriteOnly Property EmailFrom()
        Set(ByVal Value)
            m_EmailFrom = Value
        End Set
    End Property

    WriteOnly Property EmailSubject()
        Set(ByVal Value)
            m_EmailSubject = Value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal DBConnection As String, ByVal DBTableName As String)

        m_DBConnection = DBConnection
        m_DBTableName = DBTableName

    End Sub

    Public Sub New(ByVal DBConnection As String, ByVal DBTableName As String, ByVal SmtpServer As String, ByVal EmailTo As String, ByVal EmailFrom As String, ByVal EmailSubject As String)

        m_DBConnection = DBConnection
        m_DBTableName = DBTableName
        m_SmtpServer = SmtpServer
        m_EmailTo = EmailTo
        m_EmailFrom = EmailFrom
        m_EmailSubject = EmailSubject

    End Sub

    'Add Extra Information when dumoing debug info to Database
    Public Sub AddRelevantInfo(ByVal Data As String)
        m_InfoArray.Add(Data)
    End Sub

    'Saves error message to database and/or sends email
    'Sendemail - Set True to send email when saving error
    'SaveToDatabase - Set True to save error message to the Database
    'ErrorDescription - Error in string format
    Public Function SaveError(ByVal SendEmail As Boolean, ByVal SaveToDataBase As Boolean, ByVal ErrorDescription As String) As Boolean

        Dim booReturn As Boolean = True

        'Send Email
        If SendEmail Then

            Dim strBody As String
            Dim I As Integer

            Try
                strBody = "The following error has occurred at [" & Now() & "] :" & vbCrLf & vbCrLf
                strBody = strBody & ErrorDescription & vbCrLf & vbCrLf
                strBody = strBody & "Relevant Information:" & vbCrLf & vbCrLf

                'Add extra relevant info added by user
                For I = 0 To m_InfoArray.Count - 1
                    strBody = strBody & m_InfoArray(I) & vbCrLf
                Next

                booReturn = SendErrEmail(strBody)

            Catch

                booReturn = False

            End Try

        End If

        'Save error information to database
        If SaveToDataBase Then

            Dim strSQL As String
            Dim strRelevantData As String
            Dim I As Integer

            Try

                Dim objConnection As New SqlClient.SqlConnection(m_DBConnection)
                Dim objSQLDataReader As SqlClient.SqlDataReader

                objConnection.Open()

                'Add relevant data
                For I = 1 To m_InfoArray.Count
                    strRelevantData = strRelevantData & "[" & m_InfoArray(I) & "]"
                Next

                strSQL = "INSERT INTO " & m_DBTableName & "(Error_Message,Relevent_Data,Created_Date) "
                strSQL = strSQL & "VALUES('" & Replace(ErrorDescription, "'", "''") & "','" & Replace(strRelevantData, "'", "''") & "','" & Now() & "') "

                Dim objACommand As New SqlClient.SqlCommand(strSQL, objConnection)
                objACommand.ExecuteNonQuery()


                objACommand.Dispose()
                objConnection.Close()
                objConnection.Dispose()

            Catch

                booReturn = False

            End Try

        End If

        Return booReturn

    End Function

    'Saves error message to database and/or sends email
    'Sendemail - Set True to send email when saving error
    'SaveToDatabase - Set True to save error message to the Database
    'ErrorDescription - Exception
    Public Function SaveError(ByVal SendEmail As Boolean, ByVal SaveToDataBase As Boolean, ByVal Exc As Exception) As Boolean

        Dim booReturn As Boolean = True

        If SendEmail Then

            Dim strBody As String
            Dim I As Integer

            Try

                strBody = "The following error has occurred at [" & Now() & "] :" & vbCrLf & vbCrLf
                strBody = strBody & Exc.ToString & vbCrLf & vbCrLf
                strBody = strBody & "Relevant Information:" & vbCrLf & vbCrLf

                For I = 1 To m_InfoArray.Count
                    strBody = strBody & m_InfoArray(I) & vbCrLf
                Next

                booReturn = SendErrEmail(strBody)

            Catch

                booReturn = False

            End Try

        End If

        If SaveToDataBase Then

            Dim strSQL As String
            Dim strRelevantData As String
            Dim I As Integer

            Try

                Dim objConnection As New SqlClient.SqlConnection(m_DBConnection)
                Dim objSQLDataReader As SqlClient.SqlDataReader

                For I = 1 To m_InfoArray.Count
                    strRelevantData = strRelevantData & "[" & m_InfoArray(I) & "]"
                Next

                objConnection.Open()

                strSQL = "INSERT INTO " & m_DBTableName & "(Error_Message,Relevent_Data,Created_Date) "
                strSQL = strSQL & "VALUES('" & Replace(Exc.ToString, "'", "''") & "','" & Replace(strRelevantData, "'", "''") & "','" & Now() & "') "

                Dim objACommand As New SqlClient.SqlCommand(strSQL, objConnection)
                objACommand.ExecuteNonQuery()


                objACommand.Dispose()
                objConnection.Close()
                objConnection.Dispose()

            Catch

                booReturn = False

            End Try

        End If

        Return booReturn

    End Function

    'Send Email
    'StrBody - Body of email
    Private Function SendErrEmail(ByVal strBody As String) As Boolean

        Dim objEmail As New MailMessage()
        Dim booReturn As Boolean = True

        Try
            objEmail.To = m_EmailTo
            objEmail.From = m_EmailFrom

            objEmail.BodyFormat = MailFormat.Text
            objEmail.Priority = MailPriority.High
            objEmail.Subject = m_EmailSubject

            objEmail.Body = strBody

            SmtpMail.SmtpServer = m_SmtpServer

            SmtpMail.Send(objEmail)

        Catch

            booReturn = False

        End Try

        Return booReturn

    End Function

End Class
