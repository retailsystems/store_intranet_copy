Public Class Security

    Private strDBConn As String
    Private intProjectId As Integer

    Public Sub New(ByVal DBConn As String, ByVal ProjectId As Integer)

        strDBConn = DBConn
        intProjectId = ProjectId

    End Sub

    Public Function ModuleAuthorized(ByVal EmployeeId As String, ByVal ModuleName As String, ByVal ModuleURL As String) As Boolean

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSecurityConn"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim strSQL As String
        Dim booReturn As Boolean = False

        objConnection.Open()

        strSQL = "SELECT Module.ModuleName, Module.ModuleUrl "
        strSQL = strSQL & "FROM         UserProfile INNER JOIN "
        strSQL = strSQL & "UserProfileRole ON UserProfile.UserId = UserProfileRole.UserId INNER JOIN "
        strSQL = strSQL & "UserRole ON UserProfileRole.UserRoleId = UserRole.UserRoleId INNER JOIN "
        strSQL = strSQL & "Permission ON UserRole.UserRoleId = Permission.UserRoleId INNER JOIN "
        strSQL = strSQL & "ModuleControl ON Permission.ModuleControlId = ModuleControl.ModuleControlId INNER JOIN "
        strSQL = strSQL & "Module ON ModuleControl.ModuleId = Module.ModuleId "
		strSQL = strSQL & "WHERE     (UserProfile.EmployeeId = '" & EmployeeId.PadLeft(6, "0"c) & "') AND (Permission.ProjectId = " & intProjectId & ") "

        Dim objCommand As New SqlClient.SqlCommand(strSQL, objConnection)

        objDataReader = objCommand.ExecuteReader()

        Do While objDataReader.Read

            If UCase(objDataReader("ModuleName")) = UCase(ModuleName) And UCase(objDataReader("ModuleURL")) = UCase(ModuleURL) Then
                booReturn = True
                Exit Do
            End If

        Loop

        objCommand.Dispose()
        objDataReader.Close()

        Return booReturn

    End Function

End Class
