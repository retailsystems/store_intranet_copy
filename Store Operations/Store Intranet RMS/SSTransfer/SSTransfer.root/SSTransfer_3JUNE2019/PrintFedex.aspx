<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PrintFedex.aspx.vb" Inherits="SSTransfer.PrintFedex" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Transfer Home</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<LINK id="lnkStyles" href="http://172.16.2.13/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server"></link>
	</HEAD>
	<body class="sBody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="frmTransferHome" method="post" runat="server" onsubmit="return checkSubmit();">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1%"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="4" width="100%">
							<tr>
								<td vAlign="center" align="middle" height="97%"><iframe id="UPSFrame" src="" width="100%" height="100%" runat="server"></iframe>
								</td>
							</tr>
							<tr>
								<td align="right" height="1%"><asp:button id="btnDone" runat="server" CssClass="sButton" Text="Continue"></asp:button>&nbsp;
								<!--	<input id="btnPrint" class="sButton" onclick="javascript:UPSFrame.focus(); UPSFrame.print();" type="button" value="Print"> -->
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1%"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
