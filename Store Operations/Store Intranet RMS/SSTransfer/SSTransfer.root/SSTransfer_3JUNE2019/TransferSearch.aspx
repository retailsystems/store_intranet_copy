<%@ Page Language="vb" AutoEventWireup="false" Codebehind="TransferSearch.aspx.vb" Inherits="SSTransfer.TransferSearch" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>TransferSearch</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server"/>
	</HEAD>
	<body id="pageBody" runat="server" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="frmtranssearch" method="post" runat="server" onsubmit="return checkSubmit();">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<TBODY>
					<tr>
						<td colSpan="2" height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
					</tr>
					<tr>
						<td align="middle" vAlign="center"><br>
							<table cellSpacing="0" cellPadding="4" width="50%">
								<tr>
									<td vAlign="top" width="50%">
										<table cellSpacing="0" class="sDialogTable" cellPadding="2" width="100%">
											<tr>
												<td noWrap class="sSelectShipDarkBGText">Search By Transfer Number:</td>
											</tr>
											<tr>
												<td class="sNewTransFormTitle">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td class="sListBox" noWrap width="1%">Transfer Number:</td>
															<td><asp:textbox id="txtTransNum" onkeypress="if(event.keyCode==13){event.cancelBubble=true;event.returnValue=false;event.keyCode=0;document.getElementById('btnTransSearch').click();}" Runat="server" Width="100%" tabIndex="1"></asp:textbox></td>
															<td width="7%"><asp:button id="btnTransSearch" CssClass="sButton" Runat="server" Width="100%" Text="Search" tabIndex="2"></asp:button></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<br>
										<table class="sDialogTable" cellSpacing="0" cellPadding="2" width="100%">
											<tr>
												<td noWrap class="sSelectShipDarkBGText">
													Search by&nbsp;Tracking Number:</td>
											</tr>
											<tr>
												<td class="sNewTransFormTitle">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td class="sListBox" noWrap width="1%">
																Tracking&nbsp;Number:</td>
															<td><asp:textbox id="txtUPS" onkeypress="if(event.keyCode==13){event.cancelBubble=true;event.returnValue=false;event.keyCode=0;document.getElementById('btnUPSSearch').click();}" Runat="server" Width="100%" MaxLength="18" tabIndex="3"></asp:textbox></td>
															<td width="7%"><asp:button id="btnUPSSearch" CssClass="sButton" Runat="server" Width="100%" Text="Search" tabIndex="4"></asp:button></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<br>
										<table cellSpacing="0" class="sDialogTable" cellPadding="2" width="100%">
											<tr>
												<td noWrap class="sSelectShipDarkBGText" width="100%" colspan="3">Search by Date:</td>
											</tr>
											<tr>
												<td colspan="3">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td class="sListBox" vAlign="top" noWrap width="1%">Search For:</td>
															<td class="sInfo"><asp:radiobuttonlist CssClass="sInfo" id="rblTransfer" Runat="server" tabIndex="5">
																	<asp:ListItem Value="Received" Selected="True">Received Transfers</asp:ListItem>
																	<asp:ListItem Value="Shipped">Shipped Transfers</asp:ListItem>
																</asp:radiobuttonlist></td>
														</tr>
														<tr>
															<td class="sListBox" vAlign="top" noWrap width="1%">From Date:</td>
															<td><asp:textbox id="txtFDate" onkeypress="if(event.keyCode==13){event.cancelBubble=true;event.returnValue=false;event.keyCode=0;document.getElementById('btnDate').click();}" Runat="server" tabIndex="6"></asp:textbox></td>
														</tr>
														<tr>
															<td class="sListBox" vAlign="top" noWrap width="1%"></td>
															<td class="sInfo"><asp:label id="lblFDateEX" Runat="server">(Format Example: 11/6/02)</asp:label></td>
														</tr>
														<tr>
															<td vAlign="top" class="sListBox" noWrap width="1%"></td>
															<td><br>
															</td>
														</tr>
														<tr>
															<td vAlign="top" class="sListBox" noWrap width="1%">Thru Date:</td>
															<td><asp:textbox id="txtTDate" onkeypress="if(event.keyCode==13){event.cancelBubble=true;event.returnValue=false;event.keyCode=0;document.getElementById('btnDate').click();}" Runat="server" tabIndex="7"></asp:textbox></td>
														</tr>
														<tr>
															<td vAlign="top" class="sListBox" noWrap width="1%"></td>
															<td width="70%" class="sInfo"><asp:label id="lblTDateEX" Runat="server">(Format Example: 12/13/02)</asp:label></td>
															<td><asp:button id="btnDate" CssClass="sButton" Runat="server" Width="100%" Text="Search" tabIndex="8"></asp:button></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<table cellSpacing="0" cellPadding="2" width="100%">
											<tr>
												<td></td>
												<td align="right" nowrap><A href="AdvancedSearch.aspx">Advanced Search</A></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<table cellSpacing="0" cellPadding="1" width="100%">
								<tr align="right" width="100%">
									<td align="left" colSpan="2"><asp:label id="lblError" width="100%" CssClass="sPrintTrans" Runat="server"></asp:label>&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colSpan="2" height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
					</tr>
				</TBODY>
			</table>
		</form>
	</body>
</HTML>
