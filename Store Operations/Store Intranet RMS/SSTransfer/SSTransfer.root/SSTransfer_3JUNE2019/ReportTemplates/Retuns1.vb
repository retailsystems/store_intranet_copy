Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class Returns
    Inherits ActiveReport

    Private m_BoxId As String
    Private m_RowCount As Int32
    Private m_Page As Integer
    Private m_lastTransferType As String
    Private m_TopPic As PicType

    Public Enum PicType
        Hottopic = 1
        Torrid = 2
    End Enum

    Public Sub New(ByVal BoxId As String, ByVal TopPic As PicType)
        MyBase.New()
        m_TopPic = TopPic
        m_BoxId = BoxId
        InitializeReport()
    End Sub

#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghTransferType As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfTransferType As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
	Private srReturns As DataDynamics.ActiveReports.SubReport = Nothing
	Private srSending As DataDynamics.ActiveReports.SubReport = Nothing
	Private Shape28 As DataDynamics.ActiveReports.Shape = Nothing
	Private PicHottopic As DataDynamics.ActiveReports.Picture = Nothing
	Private lblQuantReq As DataDynamics.ActiveReports.Label = Nothing
	Private lblQuantShipped As DataDynamics.ActiveReports.Label = Nothing
	Private Shape11 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape10 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape16 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape15 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtFromStoreName As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtToStoreNum As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtToStoreName As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblVendor As DataDynamics.ActiveReports.Label = Nothing
	Private lblDept As DataDynamics.ActiveReports.Label = Nothing
	Private lblSKU As DataDynamics.ActiveReports.Label = Nothing
	Private lblTransferType As DataDynamics.ActiveReports.Label = Nothing
	Private lblCust As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescription As DataDynamics.ActiveReports.Label = Nothing
	Private lblRetail As DataDynamics.ActiveReports.Label = Nothing
	Private lblSize As DataDynamics.ActiveReports.Label = Nothing
	Private lblStyle As DataDynamics.ActiveReports.Label = Nothing
	Private Shape3 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblTransfer As DataDynamics.ActiveReports.Label = Nothing
	Private lblFromStoreTop As DataDynamics.ActiveReports.Label = Nothing
	Private Shape4 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape5 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape6 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape7 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape9 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape12 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape13 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblFrom As DataDynamics.ActiveReports.Label = Nothing
	Private lblFromStore As DataDynamics.ActiveReports.Label = Nothing
	Private lblStoreName As DataDynamics.ActiveReports.Label = Nothing
	Private lblTo As DataDynamics.ActiveReports.Label = Nothing
	Private lblToStoreNum As DataDynamics.ActiveReports.Label = Nothing
	Private lblToStoreName As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private txtStoreNumTop As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTransfer As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblManifest As DataDynamics.ActiveReports.Label = Nothing
	Private Shape14 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtFromStoreNum As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCopy As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTrackingNum As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblShipdate As DataDynamics.ActiveReports.Label = Nothing
	Private txtShipmentDate As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCarrier As DataDynamics.ActiveReports.Label = Nothing
	Private txtCarrier As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblBoxIncludes As DataDynamics.ActiveReports.Label = Nothing
	Private txtBoxIncludes As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblPage As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblApprovedBy As DataDynamics.ActiveReports.Label = Nothing
	Private txtApprovedBy As DataDynamics.ActiveReports.TextBox = Nothing
	Private PicTorrid As DataDynamics.ActiveReports.Picture = Nothing
	Private txtNotes As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private txtCustName As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape26 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtPhone As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape27 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape25 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape24 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape17 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape18 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape19 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape20 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape21 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape22 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape23 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtTT As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtQS As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDesc As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtRetail As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSize As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtItem As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVendor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDept As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSKU As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtQR As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "SSTransfer.Retuns.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghTransferType = CType(Me.Sections("ghTransferType"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfTransferType = CType(Me.Sections("gfTransferType"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.srReturns = CType(Me.ReportHeader.Controls(0),DataDynamics.ActiveReports.SubReport)
		Me.srSending = CType(Me.ReportHeader.Controls(1),DataDynamics.ActiveReports.SubReport)
		Me.Shape28 = CType(Me.ghTransferType.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.PicHottopic = CType(Me.ghTransferType.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblQuantReq = CType(Me.ghTransferType.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblQuantShipped = CType(Me.ghTransferType.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Shape11 = CType(Me.ghTransferType.Controls(4),DataDynamics.ActiveReports.Shape)
		Me.Shape10 = CType(Me.ghTransferType.Controls(5),DataDynamics.ActiveReports.Shape)
		Me.Shape16 = CType(Me.ghTransferType.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.Shape15 = CType(Me.ghTransferType.Controls(7),DataDynamics.ActiveReports.Shape)
		Me.txtFromStoreName = CType(Me.ghTransferType.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtToStoreNum = CType(Me.ghTransferType.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtToStoreName = CType(Me.ghTransferType.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.lblVendor = CType(Me.ghTransferType.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblDept = CType(Me.ghTransferType.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblSKU = CType(Me.ghTransferType.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblTransferType = CType(Me.ghTransferType.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblCust = CType(Me.ghTransferType.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblDescription = CType(Me.ghTransferType.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblRetail = CType(Me.ghTransferType.Controls(17),DataDynamics.ActiveReports.Label)
		Me.lblSize = CType(Me.ghTransferType.Controls(18),DataDynamics.ActiveReports.Label)
		Me.lblStyle = CType(Me.ghTransferType.Controls(19),DataDynamics.ActiveReports.Label)
		Me.Shape3 = CType(Me.ghTransferType.Controls(20),DataDynamics.ActiveReports.Shape)
		Me.lblTransfer = CType(Me.ghTransferType.Controls(21),DataDynamics.ActiveReports.Label)
		Me.lblFromStoreTop = CType(Me.ghTransferType.Controls(22),DataDynamics.ActiveReports.Label)
		Me.Shape4 = CType(Me.ghTransferType.Controls(23),DataDynamics.ActiveReports.Shape)
		Me.Shape5 = CType(Me.ghTransferType.Controls(24),DataDynamics.ActiveReports.Shape)
		Me.Shape6 = CType(Me.ghTransferType.Controls(25),DataDynamics.ActiveReports.Shape)
		Me.Shape7 = CType(Me.ghTransferType.Controls(26),DataDynamics.ActiveReports.Shape)
		Me.Shape8 = CType(Me.ghTransferType.Controls(27),DataDynamics.ActiveReports.Shape)
		Me.Shape9 = CType(Me.ghTransferType.Controls(28),DataDynamics.ActiveReports.Shape)
		Me.Shape12 = CType(Me.ghTransferType.Controls(29),DataDynamics.ActiveReports.Shape)
		Me.Shape13 = CType(Me.ghTransferType.Controls(30),DataDynamics.ActiveReports.Shape)
		Me.lblFrom = CType(Me.ghTransferType.Controls(31),DataDynamics.ActiveReports.Label)
		Me.lblFromStore = CType(Me.ghTransferType.Controls(32),DataDynamics.ActiveReports.Label)
		Me.lblStoreName = CType(Me.ghTransferType.Controls(33),DataDynamics.ActiveReports.Label)
		Me.lblTo = CType(Me.ghTransferType.Controls(34),DataDynamics.ActiveReports.Label)
		Me.lblToStoreNum = CType(Me.ghTransferType.Controls(35),DataDynamics.ActiveReports.Label)
		Me.lblToStoreName = CType(Me.ghTransferType.Controls(36),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.ghTransferType.Controls(37),DataDynamics.ActiveReports.Line)
		Me.Line1 = CType(Me.ghTransferType.Controls(38),DataDynamics.ActiveReports.Line)
		Me.txtStoreNumTop = CType(Me.ghTransferType.Controls(39),DataDynamics.ActiveReports.TextBox)
		Me.txtTransfer = CType(Me.ghTransferType.Controls(40),DataDynamics.ActiveReports.TextBox)
		Me.lblManifest = CType(Me.ghTransferType.Controls(41),DataDynamics.ActiveReports.Label)
		Me.Shape14 = CType(Me.ghTransferType.Controls(42),DataDynamics.ActiveReports.Shape)
		Me.txtFromStoreNum = CType(Me.ghTransferType.Controls(43),DataDynamics.ActiveReports.TextBox)
		Me.txtCopy = CType(Me.ghTransferType.Controls(44),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.ghTransferType.Controls(45),DataDynamics.ActiveReports.Label)
		Me.txtTrackingNum = CType(Me.ghTransferType.Controls(46),DataDynamics.ActiveReports.TextBox)
		Me.lblShipdate = CType(Me.ghTransferType.Controls(47),DataDynamics.ActiveReports.Label)
		Me.txtShipmentDate = CType(Me.ghTransferType.Controls(48),DataDynamics.ActiveReports.TextBox)
		Me.lblCarrier = CType(Me.ghTransferType.Controls(49),DataDynamics.ActiveReports.Label)
		Me.txtCarrier = CType(Me.ghTransferType.Controls(50),DataDynamics.ActiveReports.TextBox)
		Me.lblBoxIncludes = CType(Me.ghTransferType.Controls(51),DataDynamics.ActiveReports.Label)
		Me.txtBoxIncludes = CType(Me.ghTransferType.Controls(52),DataDynamics.ActiveReports.TextBox)
		Me.lblPage = CType(Me.ghTransferType.Controls(53),DataDynamics.ActiveReports.TextBox)
		Me.Label2 = CType(Me.ghTransferType.Controls(54),DataDynamics.ActiveReports.Label)
		Me.Shape1 = CType(Me.ghTransferType.Controls(55),DataDynamics.ActiveReports.Shape)
		Me.lblApprovedBy = CType(Me.ghTransferType.Controls(56),DataDynamics.ActiveReports.Label)
		Me.txtApprovedBy = CType(Me.ghTransferType.Controls(57),DataDynamics.ActiveReports.TextBox)
		Me.PicTorrid = CType(Me.ghTransferType.Controls(58),DataDynamics.ActiveReports.Picture)
		Me.txtNotes = CType(Me.ghTransferType.Controls(59),DataDynamics.ActiveReports.TextBox)
		Me.Label3 = CType(Me.ghTransferType.Controls(60),DataDynamics.ActiveReports.Label)
		Me.Line3 = CType(Me.ghTransferType.Controls(61),DataDynamics.ActiveReports.Line)
		Me.txtCustName = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Shape26 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.Shape)
		Me.txtPhone = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Shape27 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.Shape)
		Me.Shape25 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Shape)
		Me.Shape24 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.Shape)
		Me.Shape17 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.Shape18 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.Shape)
		Me.Shape19 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Shape)
		Me.Shape20 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.Shape)
		Me.Shape21 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.Shape)
		Me.Shape22 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.Shape)
		Me.Shape23 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.Shape)
		Me.txtTT = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.txtQS = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.txtDesc = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.txtRetail = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.txtSize = CType(Me.Detail.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.txtItem = CType(Me.Detail.Controls(18),DataDynamics.ActiveReports.TextBox)
		Me.txtVendor = CType(Me.Detail.Controls(19),DataDynamics.ActiveReports.TextBox)
		Me.txtDept = CType(Me.Detail.Controls(20),DataDynamics.ActiveReports.TextBox)
		Me.txtSKU = CType(Me.Detail.Controls(21),DataDynamics.ActiveReports.TextBox)
		Me.txtQR = CType(Me.Detail.Controls(22),DataDynamics.ActiveReports.TextBox)
	End Sub

#End Region

    Private Sub Returns_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.DataInitialize

        Me.Fields.Add("SKU_Num")
        Me.Fields.Add("Dept_Cd")
        Me.Fields.Add("VE_CD")
        Me.Fields.Add("ITM_CD")
        Me.Fields.Add("SIZE_CD")
        Me.Fields.Add("CURR")
        Me.Fields.Add("DES1")
        Me.Fields.Add("ITEM_Qty")
        Me.Fields.Add("Cust_Phone")
        Me.Fields.Add("Cust_Name")
        Me.Fields.Add("Xfer_Short_Desc")
        Me.Fields.Add("Received_Qty")

    End Sub

    Private Sub Returns_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        If m_TopPic = PicType.Hottopic Then
            Me.PicHottopic.Visible = True
        ElseIf m_TopPic = PicType.Torrid Then
            Me.PicTorrid.Visible = True
        End If

        Me.srReturns.Report = New Full_Manifest(m_BoxId, "Returns", m_TopPic)
        Me.srSending.Report = New Full_Manifest(m_BoxId, "Sending Store", m_TopPic)

        CreateDataSet()
        GetHeader()

    End Sub

    Private Sub PageFooter_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageFooter.BeforePrint
        Me.lblPage.Text = CType((Me.PageNumber + 1), String)
    End Sub

    Private Sub GetHeader()

        Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim strFileName As String
        Dim strStoredProcedure As String
        Dim objDataReader As SqlClient.SqlDataReader
        Dim strBoxInculdes As String
        Dim intTType As Int32
        Dim strCopies As String
        Dim booONWAN As Boolean

        objSQLConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeader '" & m_BoxId & "' ", objSQLConnection)

        objDataReader = objCommand.ExecuteReader()

        objDataReader.Read()

        Me.txtFromStoreName.Text = objDataReader("SStore").ToString
        Me.txtFromStoreNum.Text = objDataReader("Sending_Store_Cd").ToString
        Me.txtStoreNumTop.Text = objDataReader("Sending_Store_Cd").ToString
        Me.txtTransfer.Text = objDataReader("Box_Id").ToString
        Me.txtToStoreNum.Text = objDataReader("Receiving_Store_Cd").ToString
        Me.txtToStoreName.Text = objDataReader("RStore").ToString
        Me.txtTrackingNum.Text = objDataReader("Tracking_Num").ToString
        Me.txtShipmentDate.Text = String.Format("{0:d}", objDataReader("Shipment_Date"))
        Me.txtCarrier.Text = objDataReader("ParcelCompany").ToString
        Me.txtNotes.Text = objDataReader("Notes").ToString

        If objDataReader("Approved_By").ToString.Trim.Length > 0 Then
            Me.txtApprovedBy.Text = objDataReader("Approved_By").ToString
        End If

        intTType = objDataReader("TType")
        booONWAN = objDataReader("ReceivingStoreWAN")

        objCommand.CommandText = "spGetBoxIncludes '" & m_BoxId & "'," & intTType & "," & objDataReader("Receiving_Store_Cd").ToString & "," & objDataReader("Sending_Store_Cd").ToString

        objDataReader.Close()

        objDataReader = objCommand.ExecuteReader()

        Do While objDataReader.Read()
            If objDataReader("Checked") Then
                strBoxInculdes = strBoxInculdes & objDataReader("Includes_Desc") & ", "
            End If
        Loop

        If strBoxInculdes.Length > 0 Then
            strBoxInculdes = Mid(strBoxInculdes, 1, strBoxInculdes.Length - 2)
        End If

        Me.txtBoxIncludes.Text = strBoxInculdes

        objCommand.Dispose()
        objDataReader.Close()
        objSQLConnection.Close()

    End Sub

    Private Sub ghTransferType_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles ghTransferType.BeforePrint

        m_Page = m_Page + 1
        Me.lblPage.Value = m_Page

    End Sub

    Private Sub ghTransferType_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles ghTransferType.Format

        Me.txtCopy.Value = Me.Fields("Xfer_Short_Desc").Value

        If Me.Fields("Xfer_Short_Desc").Value <> m_lastTransferType Then
            m_Page = 0
        End If

        m_lastTransferType = Me.Fields("Xfer_Short_Desc").Value

    End Sub

    Private Sub CreateDataSet()

        Dim m_ds = New DataDynamics.ActiveReports.DataSources.SqlDBDataSource()
        Dim strSQL As String

        m_ds.ConnectionString = ConfigurationSettings.AppSettings("strSQLConnection")

        strSQL = "SELECT     Box_Xfer_Item.*, Xfer_Type.Xfer_Desc,Xfer_Type.Xfer_Short_Desc,Customer_Info.FullName as Cust_Name, "
        strSQL = strSQL & "              Customer_Info.Phone as Cust_Phone "
        strSQL = strSQL & "FROM         Box_Xfer_Item INNER JOIN "
        strSQL = strSQL & "              Xfer_Type ON Box_Xfer_Item.Xfer_Type_Cd = Xfer_Type.Xfer_Cd LEFT OUTER JOIN "
        strSQL = strSQL & "              Customer_Info ON Box_Xfer_Item.Cust_Id = Customer_Info.Cust_Id "
        strSQL = strSQL & "WHERE Box_Xfer_Item.Box_Id = '" & m_BoxId & "' "
        strSQL = strSQL & "ORDER BY Xfer_Type.Xfer_Desc, Box_Xfer_Item.Dept_Cd, Box_Xfer_Item.SKU_Num "

        m_ds.SQL = strSQL
        Me.DataSource = m_ds

    End Sub

    'Private Sub AddORAItemInfo(ByRef objTargetDataTable As DataTable, ByRef objSourceDataTable As DataTable)

    ' Dim CurrentDataRow As DataRow
    '  Dim FoundRow As DataRow
    ' Dim SKU_Num(0) As Object

    '  For Each CurrentDataRow In objTargetDataTable.Rows

    '   SKU_Num(0) = CurrentDataRow("SKU_Num")
    '    FoundRow = objSourceDataTable.Rows.Find(SKU_Num)

    '    If Not FoundRow Is Nothing Then
    '       CurrentDataRow("VE_CD") = FoundRow("VE_CD")
    '      CurrentDataRow("DES1") = FoundRow("DES1")
    '       CurrentDataRow("SIZE_CD") = FoundRow("SIZE_CD")
    '      CurrentDataRow("CURR") = FoundRow("CURR")
    '      CurrentDataRow("ITM_CD") = FoundRow("ITM_CD")
    '     CurrentDataRow("DEPT_CD") = FoundRow("DEPT_CD")
    '     CurrentDataRow("ITM_CD") = FoundRow("ITM_CD")
    '  End If

    '  Next

    ' End Sub

    'Private Sub Returns_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles MyBase.FetchData

    'Try

    'Me.Fields("SKU_Num").Value = Me.Fields("SKU_Num").Value
    'Me.Fields("DEPT_CD").Value = Me.Fields("DEPT_CD").Value
    'Me.Fields("VE_CD").Value = Me.Fields("VE_CD").Value
    'Me.Fields("ITM_CD").Value = Me.Fields("ITM_CD").Value
    'Me.Fields("SIZE_CD").Value = Me.Fields("SIZE_CD").Value
    'Me.Fields("CURR").Value = Me.Fields("CURR").Value
    'Me.Fields("DES1").Value = Me.Fields("DES1").Value
    'Me.Fields("ITEM_Qty").Value = Me.Fields("ITEM_Qty").Value
    'Me.Fields("Cust_Phone").Value = Me.Fields("Cust_Phone").Value
    'Me.Fields("Cust_Name").Value = Me.Fields("Cust_Name").Value
    'Me.Fields("Xfer_Short_Desc").Value = Me.Fields("Xfer_Short_Desc").Value

    'Catch
    'eArgs.EOF = True
    'End Try

    'End Sub

End Class
