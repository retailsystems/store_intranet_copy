<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Manifest.aspx.vb" Inherits="SSTransfer.Manifest" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Manifest</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td valign="top">
						<table width="100%">
							<tr>
								<td><font face="arial" size="3"><b>Sending Store</b></font></td>
							</tr>
							<tr>
								<td><font face="arial" size="2">Page 1</font></td>
							</tr>
							<tr>
								<td></td>
							</tr>
							<tr>
								<td><font face="arial" size="1">Approved By: N/A</font></td>
							</tr>
							<tr>
								<td><font face="arial" size="1">Shipment Date: 9/3/2004</font></td>
							</tr>
						</table>
					</td>
					<td valign="bottom">
						<table width="100%">
							<tr>
								<td align="middle"><font face="arial" size="4"><b>MANIFEST</b></font></td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<table width="100%">
							<tr>
								<td><font face="arial" size="1"><b>From Store #</b></font></td>
							</tr>
							<tr>
								<td><font face="arial" size="1">1</font></td>
							</tr>
							<tr>
								<td><font face="arial" size="1"><b>Carrier</b></font></td>
							</tr>
							<tr>
								<td><font face="arial" size="1">UPS</font></td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<table width="100%">
							<tr>
								<td><font face="arial" size="1"><b>Transfer #</b></font></td>
							</tr>
							<tr>
								<td><font face="arial" size="1">0001-1000456</font></td>
							</tr>
							<tr>
								<td><font face="arial" size="1"><b>Tracking #</b></font></td>
							</tr>
							<tr>
								<td><font face="arial" size="1">2546436547</font></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="100%" border="1" bordercolor="black" cellpadding="2" cellspacing="0">
				<tr>
					<td></td>
					<td></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
