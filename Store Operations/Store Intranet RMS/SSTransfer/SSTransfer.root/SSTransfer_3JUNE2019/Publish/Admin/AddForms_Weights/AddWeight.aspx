<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AddWeight.aspx.vb" Inherits="SSTransfer.AddWeight"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Add Weight</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
	</HEAD>
	<body class="sBody" id="pageBody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0" runat="server">
		<form id="frmAddWeight" onsubmit="return checkSubmit();" method="post" runat="server">
			<table class="sInfoTable" height="100%" cellSpacing="0" cellPadding="1" width="100%">
				<tr>
					<td class="sDialogHeader" align="middle" colSpan="2" height="1%"><asp:label id="lblHeader" runat="server" EnableViewState="False">Add Weight</asp:label></td>
				</tr>
				<tr>
					<td align="middle" height="99%">
						<table cellSpacing="0" cellPadding="2" width="100%">
							<tr>
								<td noWrap width="1%"><asp:label id="lblWeightType" runat="server" CssClass="Plain">Weight Type:</asp:label></td>
								<td width="49%"><asp:dropdownlist DataValueField="WeightType_Id" DataTextField="WeightType_Desc" id="ddlWeightType" Width="100%" Runat="server"></asp:dropdownlist></td>
								<td noWrap width="1%"><asp:label id="lblWeightTypeCode" runat="server" CssClass="Plain">Weight Type Code:</asp:label></td>
								<td width="24%"><asp:textbox id="txtWeightTypeCode" Width="100%" Runat="server" MaxLength="15"></asp:textbox></td>
								<td noWrap width="1%"><asp:label id="lblLBS" runat="server" CssClass="Plain">LBS:</asp:label></td>
								<td width="24%"><asp:textbox id="txtLBS" Width="100%" Runat="server" MaxLength="5"></asp:textbox></td>
							</tr>
							<tr>
								<td colspan="6"><asp:Label Visible="false" Runat="server" ID="lblError" ForeColor="red"></asp:Label></td>
							</tr>
						</table>
						<table width="100%">
							<tr>
								<td align="left"><input class="sButton" onclick="javascript:window.close();" type="button" value="Cancel"></td>
								<td align="right">
									<asp:Button runat="server" ID="btnAddWeight" Text="Add Weight" CssClass="sButton"></asp:Button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
