<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EmpHistory.aspx.vb" Inherits="SSTransfer.EmpHistory" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Customer Information</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server" />
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server" onsubmit="return checkSubmit();">
			<table width="100%" height="100%" cellSpacing="0" cellPadding="4" class="sDialogTable">
				<tr>
					<td height="1%" align="middle" class="sDialogHeader" colSpan="2"><asp:label id="lblHeader" runat="server" EnableViewState="False">Status History</asp:label></td>
				</tr>
				<tr>
					<td valign="top" height="99%" align="middle" class="sDialog">
						<br>
						<div style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 200" class="sDatagrid">
							<table cellSpacing="0" cellPadding="2" width="100%">
								<tr>
									<td>
										<asp:datagrid id="grdEmpHistory" CssClass="sDatagrid" runat="server" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="false" AutoGenerateColumns="False">
											<AlternatingItemStyle cssclass="sDatagridAlternatingItem"></AlternatingItemStyle>
											<Columns>
												<asp:BoundColumn ItemStyle-Wrap="False" DataField="Emp_FullName" HeaderText="Employee">
													<HeaderStyle Wrap="False" cssclass="sDatagridHeader"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn ItemStyle-Wrap="False" DataField="Status_Desc" HeaderText="Status">
													<HeaderStyle Wrap="False" cssclass="sDatagridHeader"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn ItemStyle-Wrap="False" DataField="Modified_Date" HeaderText="Modified Date">
													<HeaderStyle Wrap="False" cssclass="sDatagridHeader"></HeaderStyle>
												</asp:BoundColumn>
											</Columns>
										</asp:datagrid>
									</td>
								</tr>
							</table>
						</div>
						<br>
						<br>
						<input type="button" value="Close" onclick="javascript:window.close();" class="sButton">
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
