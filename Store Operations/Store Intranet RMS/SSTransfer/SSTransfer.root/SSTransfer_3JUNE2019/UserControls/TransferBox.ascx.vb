Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Public MustInherit Class TransferBox
    Inherits System.Web.UI.UserControl

    Protected WithEvents dgTransferBox As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl


    Public Enum eGERSSTatusType
        Open = 1
        Processed = 3
    End Enum

    Private strBoxId As String
    Private objDataSet As New DataSet()
    Private SelectedRow As Integer
    Private booGenDis As Boolean
    Private booUnassignedOnly As Boolean = False
    Private booViewSnapShot As Boolean = False
    Private Store As Integer = 9999

    'Public Event DiscrepancyGenerated(ByVal DisAmount As Decimal, ByVal DisQuant As Int32)
    Public Event InValidSKU(ByVal SKUNumber As String, ByVal ErrNumber As Byte, ByVal ErrDesc As String)


    Public Enum BoxColumnName
        Remove = 0
        SKU = 1
        Vendor = 2
        Size = 3
        Retail = 4
        Description = 5
        QtyReq = 6
        QtyShipped_Edit = 7
        QtyShipped = 8
        QtyReceived_Edit = 9
        QtyReceived = 10
        Customer = 11
        XferType = 12
    End Enum


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    WriteOnly Property ViewSnapShot()
        Set(ByVal Value)
            booViewSnapShot = Value
        End Set
    End Property

    WriteOnly Property DeleteUnassignedOnly()
        Set(ByVal Value)
            booUnassignedOnly = Value
        End Set
    End Property

    'WriteOnly Property GenDiscrepancy()
    'Set(ByVal Value)
    'booGenDis = Value
    'End Set
    'End Property

    Property Box_Id()
        Get
            Box_Id = strBoxId
        End Get
        Set(ByVal Value)
            strBoxId = Value
        End Set
    End Property

    Property Sending_Store()
        Get
            Sending_Store = Store
        End Get
        Set(ByVal Value)
            Store = Value
        End Set
    End Property

    WriteOnly Property HideColumn() As BoxColumnName
        Set(ByVal Value As BoxColumnName)
            dgTransferBox.Columns(Value).Visible = False
        End Set
    End Property

    WriteOnly Property ShowColumn() As BoxColumnName
        Set(ByVal Value As BoxColumnName)
            dgTransferBox.Columns(Value).Visible = True
        End Set
    End Property


    Public Sub ClearDataset()

        Session("ItemDataSet") = Nothing
        objDataSet = Nothing
        objDataSet = New DataSet

    End Sub

    Public Sub LoadData()

        If Not Page.IsPostBack Then

            If strBoxId Is Nothing Then
                CreateEmptyDataSet()
            Else
                If booViewSnapShot Then
                    CreateDataSet_SnapShot()
                Else
                    CreateDataSet()
                End If
            End If

        ElseIf Not Session("ItemDataSet") Is Nothing Then

            GetState()

            Dim objDataView As New DataView(objDataSet.Tables("BoxItems"))

            objDataView.Sort = "Xfer_Desc"

            dgTransferBox.DataSource = objDataView
            dgTransferBox.DataBind()

        End If
        'If booGenDis Then
        'GenerateDiscrepancy()
        'End If

        SaveState()

    End Sub

    Public Sub UpdateItemReceived(ByVal Box_Id As Int32, ByVal Xfer_Type_Cd As Int32, ByVal SKU_Num As String)

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        strStoredProcedure = "UpdateItemReceived " & Box_Id & "," & Xfer_Type_Cd & ",'" & Replace(SKU_Num, "'", "''") & "'"

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader()

        dgTransferBox.DataSource = objDataReader
        dgTransferBox.DataBind()

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Dispose()
        objConnection.Close()
    End Sub

    Public Sub RemoveSelectedItem()

        If objDataSet.Tables("BoxItems").Rows.Count > 0 Then
            objDataSet.Tables("BoxItems").Rows(SelectedRow).Delete()
            Session("ItemDataSet") = objDataSet

            Dim objDataView As New DataView(objDataSet.Tables("BoxItems"))

            objDataView.Sort = "Xfer_Desc"

            dgTransferBox.DataSource = objDataView
            dgTransferBox.DataBind()
        End If

    End Sub


    Public Sub UpdateICStatus(ByVal GERSStatus As eGERSSTatusType)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateICStatus", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)
        Dim parGERSStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@GERSStatus", SqlDbType.SmallInt)

        parBoxId.Direction = ParameterDirection.Input
        parGERSStatus.Direction = ParameterDirection.Input

        parBoxId.Value = strBoxId
        parGERSStatus.Value = GERSStatus

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub
    Public Function GetQuantityTotal() As Integer
        Dim totalQuantity As Integer = 0
        Dim txtQtyShipped As TextBox
        Dim dgi As DataGridItem
        Dim i As Integer
        For i = dgTransferBox.Items.Count - 1 To 0 Step -1
            dgi = dgTransferBox.Items(i)

            txtQtyShipped = CType(dgi.FindControl("txtItemQty"), TextBox)
            If IsNumeric(txtQtyShipped.Text) Then
                If txtQtyShipped.Text >= 0 Then
                    totalQuantity += Convert.ToInt16(txtQtyShipped.Text)

                End If
            End If

        Next
        Return totalQuantity

    End Function
    Public Function UpdateEdit() As Boolean

        Dim i As Integer
        Dim dgi As DataGridItem
        Dim txtQtyShipped As TextBox
        Dim txtQtyReceived As TextBox
        Dim chkDelete As CheckBox
        'Dim intRemoved As Int32 = 0

        'Dim rc As DataRowCollection = objDataSet.Tables("BoxItems").Rows
        For i = dgTransferBox.Items.Count - 1 To 0 Step -1
            dgi = dgTransferBox.Items(i)

            txtQtyShipped = CType(dgi.FindControl("txtItemQty"), TextBox)
            txtQtyReceived = CType(dgi.FindControl("txtReceivedQty"), TextBox)
            chkDelete = CType(dgi.FindControl("chkDeleteItem"), CheckBox)

            If IsNumeric(txtQtyShipped.Text) And IsNumeric(txtQtyReceived.Text) Then
                If txtQtyShipped.Text >= 0 And txtQtyReceived.Text >= 0 Then

                    'rc(i)("Item_Qty") = txtQtyShipped.Text
                    'rc(i)("Received_Qty") = txtQtyReceived.Text
                    'rc(i)("Discrepancy") = System.Math.Abs(txtQtyShipped.Text - txtQtyReceived.Text)

                    objDataSet.Tables("BoxItems").Rows(i).Item("Item_Qty") = txtQtyShipped.Text
                    objDataSet.Tables("BoxItems").Rows(i).Item("Received_Qty") = txtQtyReceived.Text
                    'objDataSet.Tables("BoxItems").Rows(i).Item("Discrepancy") = System.Math.Abs(txtQtyShipped.Text - txtQtyReceived.Text)
                    objDataSet.Tables("BoxItems").Rows(i).Item("Discrepancy") = 0
                    'If (txtQtyShipped.Text = "0" And objDataSet.Tables("BoxItems").Rows(i)("Xfer_Type_Cd") <> 6) Or (objDataSet.Tables("BoxItems").Rows(i)("Xfer_Type_Cd") = 6 And txtQtyReceived.Text = "0") Then
                    'rc(i).Delete()
                    'End If

                    If (CType(txtQtyShipped.Text, Integer) = 0 And objDataSet.Tables("BoxItems").Rows(i)("Xfer_Type_Cd") <> 6) Or (objDataSet.Tables("BoxItems").Rows(i)("Xfer_Type_Cd") = 6 And CType(txtQtyReceived.Text, Integer) = 0) Then
                        objDataSet.Tables("BoxItems").Rows(i).Delete()
                    End If

                    'If chkDelete.Checked Then
                    'If booUnassignedOnly And objDataSet.Tables("BoxItems").Rows(i)("Xfer_Type_Cd") = 6 Then
                    'objDataSet.Tables("BoxItems").Rows(i - intRemoved).Delete()
                    'ElseIf Not booUnassignedOnly Then
                    'objDataSet.Tables("BoxItems").Rows(i - intRemoved).Delete()
                    'End If

                    'End If

                End If
            End If
        Next

        'objDataSet.Tables("BoxItems").AcceptChanges()

        Dim objDataView As New DataView(objDataSet.Tables("BoxItems"))

        objDataView.Sort = "Xfer_Desc"

        dgTransferBox.DataSource = objDataView
        dgTransferBox.DataBind()

        'Session("ItemDataSet") = objDataSet

    End Function

    Public Function IsXferAllowed(ByVal Xfer_Cd As Integer) As Boolean

        Dim I As Integer
        Dim booReturn As Boolean = False

        For I = 0 To dgTransferBox.Items.Count - 1

            If objDataSet.Tables("BoxItems").Rows(I).Item("Xfer_Type_Cd") = Xfer_Cd Then
                booReturn = True
                Exit For
            End If

        Next

        Return booReturn

    End Function

    Public Function IsXferInBox(ByVal Xfer_Cd As Integer) As Boolean

        Dim I As Integer
        Dim booReturn As Boolean = False

        For I = 0 To dgTransferBox.Items.Count - 1

            If objDataSet.Tables("BoxItems").Rows(I).Item("Xfer_Type_Cd") = Xfer_Cd Then
                booReturn = True
                Exit For
            End If

        Next

        Return booReturn


    End Function

    Public Function GetTotalAmt() As Decimal

        Dim i As Integer
        Dim decAmt As Decimal

        For i = 0 To dgTransferBox.Items.Count - 1

            decAmt = decAmt + (objDataSet.Tables("BoxItems").Rows(i).Item("Item_Qty") * objDataSet.Tables("BoxItems").Rows(i).Item("Curr"))

        Next

        Return decAmt

    End Function

    Public Function GetTotalWeight() As Decimal

        Dim i As Integer
        Dim decWeight As Decimal

        For i = 0 To dgTransferBox.Items.Count - 1
            decWeight = decWeight + (objDataSet.Tables("BoxItems").Rows(i).Item("Item_Qty") * objDataSet.Tables("BoxItems").Rows(i).Item("Item_LBS"))
        Next

        Return Math.Ceiling(decWeight)

    End Function

    Public Function GetTotalReceived() As Int32

        Dim i As Integer
        Dim total As Int32

        If dgTransferBox.Items.Count > 0 Then
            For i = 0 To dgTransferBox.Items.Count - 1

                total = total + objDataSet.Tables("BoxItems").Rows(i).Item("Received_Qty")

            Next

            Return total
        Else
            Return 0
        End If

    End Function

    Public Sub AcceptAllReceived()

        Dim i As Integer


        For i = 0 To dgTransferBox.Items.Count - 1

            objDataSet.Tables("BoxItems").Rows(i).Item("Received_Qty") = objDataSet.Tables("BoxItems").Rows(i).Item("Item_Qty")
            objDataSet.Tables("BoxItems").Rows(i).Item("Discrepancy") = 0

        Next

        Dim objDataView As New DataView(objDataSet.Tables("BoxItems"))

        objDataView.Sort = "Xfer_Desc"

        dgTransferBox.DataSource = objDataView
        dgTransferBox.DataBind()

    End Sub

    Public Sub Save()

        Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objSQLDataAdapter As New SqlDataAdapter

        'InsertCommand
        objSQLDataAdapter.InsertCommand = New SqlCommand("spInsertBoxItems", objSQLConnection)
        objSQLDataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure

        objSQLDataAdapter.InsertCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@Xfer_Type_Cd", SqlDbType.SmallInt)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@SKU_Num", SqlDbType.VarChar)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@Item_Qty", SqlDbType.SmallInt)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@Cust_Id", SqlDbType.Int)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@Received_Qty", SqlDbType.SmallInt)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@Discrepancy", SqlDbType.SmallInt)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@Curr", SqlDbType.Money)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@VE_CD", SqlDbType.VarChar, 6)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@SIZE_CD", SqlDbType.VarChar, 6)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@DES1", SqlDbType.VarChar, 20)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@DEPT_CD", SqlDbType.VarChar, 6)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@ITM_CD", SqlDbType.VarChar, 12)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@CLASS_CD", SqlDbType.VarChar, 6)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@SUBCLASS_CD", SqlDbType.VarChar, 6)
        objSQLDataAdapter.InsertCommand.Parameters.Add("@Item_LBS", SqlDbType.Float)

        objSQLDataAdapter.InsertCommand.Parameters("@Box_Id").Value = strBoxId
        objSQLDataAdapter.InsertCommand.Parameters("@Xfer_Type_Cd").SourceColumn = "Xfer_Type_Cd"
        objSQLDataAdapter.InsertCommand.Parameters("@SKU_Num").SourceColumn = "SKU_Num"
        objSQLDataAdapter.InsertCommand.Parameters("@Item_Qty").SourceColumn = "Item_Qty"
        objSQLDataAdapter.InsertCommand.Parameters("@Cust_Id").SourceColumn = "Cust_Id"
        objSQLDataAdapter.InsertCommand.Parameters("@Received_Qty").SourceColumn = "Received_Qty"
        objSQLDataAdapter.InsertCommand.Parameters("@Discrepancy").SourceColumn = "Discrepancy"
        objSQLDataAdapter.InsertCommand.Parameters("@Curr").SourceColumn = "Curr"
        objSQLDataAdapter.InsertCommand.Parameters("@VE_CD").SourceColumn = "VE_CD"
        objSQLDataAdapter.InsertCommand.Parameters("@SIZE_CD").SourceColumn = "SIZE_CD"
        objSQLDataAdapter.InsertCommand.Parameters("@DES1").SourceColumn = "DES1"
        objSQLDataAdapter.InsertCommand.Parameters("@DEPT_CD").SourceColumn = "DEPT_CD"
        objSQLDataAdapter.InsertCommand.Parameters("@ITM_CD").SourceColumn = "ITM_CD"
        objSQLDataAdapter.InsertCommand.Parameters("@CLASS_CD").SourceColumn = "CLASS_CD"
        objSQLDataAdapter.InsertCommand.Parameters("@SUBCLASS_CD").SourceColumn = "SUBCLASS_CD"
        objSQLDataAdapter.InsertCommand.Parameters("@Item_LBS").SourceColumn = "Item_LBS"

        'UpdateCommand
        objSQLDataAdapter.UpdateCommand = New SqlCommand("spUpdateBoxItems", objSQLConnection)
        objSQLDataAdapter.UpdateCommand.CommandType = CommandType.StoredProcedure

        objSQLDataAdapter.UpdateCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@Xfer_Type_Cd", SqlDbType.SmallInt)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@SKU_Num", SqlDbType.VarChar)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@Item_Qty", SqlDbType.SmallInt)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@Cust_Id", SqlDbType.Int)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@Received_Qty", SqlDbType.SmallInt)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@Discrepancy", SqlDbType.SmallInt)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@Curr", SqlDbType.Money)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@VE_CD", SqlDbType.VarChar, 6)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@SIZE_CD", SqlDbType.VarChar, 6)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@DES1", SqlDbType.VarChar, 20)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@DEPT_CD", SqlDbType.VarChar, 6)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@ITM_CD", SqlDbType.VarChar, 12)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@CLASS_CD", SqlDbType.VarChar, 6)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@SUBCLASS_CD", SqlDbType.VarChar, 6)
        objSQLDataAdapter.UpdateCommand.Parameters.Add("@Item_LBS", SqlDbType.Float)

        objSQLDataAdapter.UpdateCommand.Parameters("@Box_Id").SourceColumn = "Box_Id"
        objSQLDataAdapter.UpdateCommand.Parameters("@Xfer_Type_Cd").SourceColumn = "Xfer_Type_Cd"
        objSQLDataAdapter.UpdateCommand.Parameters("@SKU_Num").SourceColumn = "SKU_Num"
        objSQLDataAdapter.UpdateCommand.Parameters("@Item_Qty").SourceColumn = "Item_Qty"
        objSQLDataAdapter.UpdateCommand.Parameters("@Cust_Id").SourceColumn = "Cust_Id"
        objSQLDataAdapter.UpdateCommand.Parameters("@Received_Qty").SourceColumn = "Received_Qty"
        objSQLDataAdapter.UpdateCommand.Parameters("@Discrepancy").SourceColumn = "Discrepancy"
        objSQLDataAdapter.UpdateCommand.Parameters("@Curr").SourceColumn = "Curr"
        objSQLDataAdapter.UpdateCommand.Parameters("@VE_CD").SourceColumn = "VE_CD"
        objSQLDataAdapter.UpdateCommand.Parameters("@SIZE_CD").SourceColumn = "SIZE_CD"
        objSQLDataAdapter.UpdateCommand.Parameters("@DES1").SourceColumn = "DES1"
        objSQLDataAdapter.UpdateCommand.Parameters("@DEPT_CD").SourceColumn = "DEPT_CD"
        objSQLDataAdapter.UpdateCommand.Parameters("@ITM_CD").SourceColumn = "ITM_CD"
        objSQLDataAdapter.UpdateCommand.Parameters("@CLASS_CD").SourceColumn = "CLASS_CD"
        objSQLDataAdapter.UpdateCommand.Parameters("@SUBCLASS_CD").SourceColumn = "SUBCLASS_CD"
        objSQLDataAdapter.UpdateCommand.Parameters("@Item_LBS").SourceColumn = "Item_LBS"

        'DeleteCommand
        objSQLDataAdapter.DeleteCommand = New SqlCommand("spDeleteBoxItems", objSQLConnection)
        objSQLDataAdapter.DeleteCommand.CommandType = CommandType.StoredProcedure

        objSQLDataAdapter.DeleteCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)
        objSQLDataAdapter.DeleteCommand.Parameters.Add("@Xfer_Type_Cd", SqlDbType.SmallInt)
        objSQLDataAdapter.DeleteCommand.Parameters.Add("@SKU_Num", SqlDbType.VarChar)
        objSQLDataAdapter.DeleteCommand.Parameters.Add("@Cust_Id", SqlDbType.Int)

        objSQLDataAdapter.DeleteCommand.Parameters("@Box_Id").Value = strBoxId
        objSQLDataAdapter.DeleteCommand.Parameters("@Xfer_Type_Cd").SourceColumn = "Xfer_Type_Cd"
        objSQLDataAdapter.DeleteCommand.Parameters("@SKU_Num").SourceColumn = "SKU_Num"
        objSQLDataAdapter.DeleteCommand.Parameters("@Cust_Id").SourceColumn = "Cust_Id"

        objSQLDataAdapter.Update(objDataSet, "BoxItems")

    End Sub

    Public Function ItemCount() As Int32

        Return dgTransferBox.Items.Count()

    End Function

    Public Sub RebindData()

        objDataSet = Session("ItemDataSet")

        Dim objDataView As New DataView(objDataSet.Tables("BoxItems"))

        dgTransferBox.DataSource = objDataView
        dgTransferBox.DataBind()

    End Sub

    Public Sub InsertICStatus()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spInsertICStatus", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)

        parBoxId.Direction = ParameterDirection.Input

        parBoxId.Value = strBoxId

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Returns true if a discrepancy exist
    Public Function IsDiscrepancy() As Boolean

        Dim tmpReturn As Boolean = False
        Dim I As Integer

        For I = 0 To objDataSet.Tables("BoxItems").Rows.Count - 1

            If objDataSet.Tables("BoxItems").Rows(I).RowState <> DataRowState.Deleted Then
                If System.Math.Abs(objDataSet.Tables("BoxItems").Rows(I)("Item_Qty") - objDataSet.Tables("BoxItems").Rows(I)("Received_Qty")) <> 0 Then
                    tmpReturn = True
                    Exit For
                End If
            End If

        Next

        Return tmpReturn

    End Function

    Public Sub GenerateDiscrepancy()

        Dim I As Int32
        'Dim decTmpAmount As Decimal
        'Dim intTmpQuantity As Int32
        'Dim decTmpTotalAmount As Decimal
        'Dim intTmpTotalQuantity As Int32

        For I = 0 To objDataSet.Tables("BoxItems").Rows.Count - 1

            'intTmpQuantity = System.Math.Abs(objDataSet.Tables("BoxItems").Rows(I)("Item_Qty") - objDataSet.Tables("BoxItems").Rows(I)("Received_Qty"))
            'decTmpAmount = objDataSet.Tables("BoxItems").Rows(I)("Discrep_Amt")
            If objDataSet.Tables("BoxItems").Rows(I).RowState <> DataRowState.Deleted Then
                objDataSet.Tables("BoxItems").Rows(I)("Discrepancy") = System.Math.Abs(objDataSet.Tables("BoxItems").Rows(I)("Item_Qty") - objDataSet.Tables("BoxItems").Rows(I)("Received_Qty"))
                objDataSet.Tables("BoxItems").Rows(I)("Discrep_Amt") = objDataSet.Tables("BoxItems").Rows(I)("Discrepancy") * objDataSet.Tables("BoxItems").Rows(I)("CURR")
            End If
            'decTmpTotalAmount = decTmpTotalAmount + decTmpAmount
            'intTmpTotalQuantity = intTmpTotalQuantity + intTmpQuantity
        Next

        'RaiseEvent DiscrepancyGenerated(decTmpTotalAmount, intTmpTotalQuantity)

    End Sub

    Public Sub CreateDataSet_SnapShot()

        Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objSQLCommand As New SqlCommand
        Dim objSQLDataAdapter As New SqlDataAdapter
        Dim I As Int32
        Dim SKUNumbers As String
        Dim strSQL As String
        Dim objRelation As DataRelation

        objSQLCommand.Connection = objSQLConnection
        objSQLCommand.CommandType = CommandType.StoredProcedure
        objSQLCommand.CommandText = "spGetBoxItems"
        objSQLCommand.Parameters.Add("@Box_Id", strBoxId)

        objSQLDataAdapter.SelectCommand = objSQLCommand
        objSQLDataAdapter.FillSchema(objDataSet, SchemaType.Source)
        objSQLDataAdapter.Fill(objDataSet, "BoxItems")

        Dim BoxItemskeys(2) As DataColumn

        BoxItemskeys(0) = objDataSet.Tables("BoxItems").Columns("SKU_Num")
        BoxItemskeys(1) = objDataSet.Tables("BoxItems").Columns("Xfer_Type_Cd")
        BoxItemskeys(2) = objDataSet.Tables("BoxItems").Columns("Cust_Id")

        objDataSet.Tables("BoxItems").PrimaryKey = BoxItemskeys

        objSQLConnection.Close()

        Dim objDataView As New DataView(objDataSet.Tables("BoxItems"))

        objDataView.Sort = "Xfer_Desc"

        dgTransferBox.DataSource = objDataView
        dgTransferBox.DataBind()

    End Sub

    'Public Sub CreateDataSet()

    '    Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
    '    Dim objSQLCommand As New SqlCommand
    '    Dim objSQLDataAdapter As New SqlDataAdapter
    '    Dim objORAConnection As New OleDbConnection(ConfigurationSettings.AppSettings("strGERSConn"))
    '    Dim objORACommand As New OleDbCommand
    '    Dim objORADataAdapter As New OleDbDataAdapter
    '    Dim I As Int32
    '    Dim SKUNumbers As String
    '    Dim strSQL As String
    '    Dim objRelation As DataRelation

    '    objSQLCommand.Connection = objSQLConnection
    '    objSQLCommand.CommandType = CommandType.StoredProcedure
    '    objSQLCommand.CommandText = "spGetBoxItems"
    '    objSQLCommand.Parameters.Add("@Box_Id", strBoxId)

    '    objSQLDataAdapter.SelectCommand = objSQLCommand
    '    objSQLDataAdapter.FillSchema(objDataSet, SchemaType.Source)
    '    objSQLDataAdapter.Fill(objDataSet, "BoxItems")

    '    Dim BoxItemskeys(2) As DataColumn

    '    BoxItemskeys(0) = objDataSet.Tables("BoxItems").Columns("SKU_Num")
    '    BoxItemskeys(1) = objDataSet.Tables("BoxItems").Columns("Xfer_Type_Cd")
    '    BoxItemskeys(2) = objDataSet.Tables("BoxItems").Columns("Cust_Id")

    '    objDataSet.Tables("BoxItems").PrimaryKey = BoxItemskeys

    '    objSQLConnection.Close()

    '    If objDataSet.Tables("BoxItems").Rows.Count > 0 Then

    '        For I = 0 To objDataSet.Tables("BoxItems").Rows.Count - 1
    '            SKUNumbers = SKUNumbers & "'" & Replace(objDataSet.Tables("BoxItems").Rows(I).Item("SKU_Num"), "'", "''") & "',"
    '        Next I

    '        SKUNumbers = Mid(SKUNumbers, 1, SKUNumbers.Length - 1)

    '        'strSQL = " SELECT DISTINCT U.SKU_Num AS SKU_NUM, U.DES1 as DES1, U.CURR, U.DEPT_CD, U.SIZE_CD, U.VE_CD, U.ITM_CD, U.CLASS_CD, U.SUBCLASS_CD " _
    '        '    & " FROM Box_Xfer_Item  U " _
    '        '    & " WHERE U.SKU_NUM IN (" & SKUNumbers & ") "

    '        strSQL = " SELECT DISTINCT U.SKU_NUM, A.DES1, cs_prc_util.eff_ret_prc(U.sku_num, '" & Store.ToString().PadLeft(4, "0") _
    '            & "') CURR, A.DEPT_CD, U.SIZE_CD, A.DES1, A.VE_CD, A.ITM_CD, A.CLASS_CD, A.SUBCLASS_CD " _
    '            & " FROM gm_itm A, GM_SKU U " _
    '            & " WHERE A.ITM_CD = U.ITM_CD AND U.SKU_NUM IN (" & SKUNumbers & ") "

    '        objORACommand.Connection = objORAConnection
    '        objORACommand.CommandType = CommandType.Text
    '        objORACommand.CommandText = strSQL

    '        objORADataAdapter.SelectCommand = objORACommand
    '        objORADataAdapter.Fill(objDataSet, "ORAItemInfo")

    '        Dim keys(0) As DataColumn
    '        keys(0) = objDataSet.Tables("ORAItemInfo").Columns("SKU_Num")
    '        keys(0).Unique = True
    '        keys(0).AllowDBNull = False
    '        objDataSet.Tables("ORAItemInfo").PrimaryKey = keys
    '        objORAConnection.Close()

    '        AddORAItemInfo(objDataSet.Tables("BoxItems"), objDataSet.Tables("ORAItemInfo"))
    '        objDataSet.Tables("ORAItemInfo").Dispose()

    '    End If

    '    Dim objDataView As New DataView(objDataSet.Tables("BoxItems"))

    '    objDataView.Sort = "Xfer_Desc"

    '    dgTransferBox.DataSource = objDataView
    '    dgTransferBox.DataBind()


    'End Sub

    Public Sub UpdateQtyReceived(ByVal Xfer_Type_Cd As Int32, ByVal SKU_Num As String, ByVal strRcvStore As String)

        GetState()

        'If Xfer_Type_Cd = 3 Then
        'UpdateCustReq(SKU_Num)
        'Else
        'UpdateQty(Xfer_Type_Cd, SKU_Num)
        'End If

        UpdateQty_TopDown(SKU_Num, strRcvStore)

    End Sub

    Public Sub AddItemToBox(ByVal Xfer_Type_Cd As Int32, ByVal SKU_Num As String, ByVal Cust_Id As Int32, ByVal RTV_Reason As Byte, ByVal Xfer_Desc As String, ByVal RStore As String)

        Dim FindItems(2) As Object
        Dim strSQL As String
        Dim strSKUNumbers As String  '# added for ORMS 
        strSKUNumbers = SKU_Num

        FindItems(0) = SKU_Num
        FindItems(1) = Xfer_Type_Cd
        FindItems(2) = Cust_Id

        GetState()

        Dim FoundRow = objDataSet.Tables("BoxItems").Rows.Find(FindItems)

        If Not FoundRow Is Nothing Then

            If FoundRow("Item_Qty") < 999 Then
                FoundRow("Item_Qty") = FoundRow("Item_Qty") + 1
                'FoundRow("Discrepancy") = System.Math.Abs(FoundRow("Item_Qty") - FoundRow("Received_Qty"))
                FoundRow("Discrepancy") = 0
            End If
        Else
            '# modified for connecting to RMS 09/21/12 by LJ
            Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            Dim objDataRow As DataRow

            Dim objDataReader As SqlClient.SqlDataReader
            objSQLConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand("spGetItemAddToBox", objSQLConnection)
            objCommand.CommandType = CommandType.StoredProcedure

            Dim ItemIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Item_Id", SqlDbType.VarChar)
            Dim ReceiveStore As SqlClient.SqlParameter = objCommand.Parameters.Add("@RcvStore", SqlDbType.VarChar)


            ItemIdParam.Direction = ParameterDirection.Input

            ItemIdParam.Value = strSKUNumbers

            ReceiveStore.Direction = ParameterDirection.Input

            ReceiveStore.Value = RStore


            objDataReader = objCommand.ExecuteReader()

            If objDataReader.Read() Then
                If Xfer_Type_Cd = 1 Then

                    objDataRow = objDataSet.Tables("BoxItems").NewRow
                    objDataRow("Item_Qty") = 1
                    objDataRow("SKU_Num") = SKU_Num
                    objDataRow("Received_Qty") = 0
                    objDataRow("Cust_Id") = Cust_Id
                    objDataRow("Xfer_Desc") = Xfer_Desc
                    objDataRow("VE_CD") = objDataReader("VE_CD")
                    objDataRow("SIZE_CD") = objDataReader("SIZE_CD")
                    objDataRow("CURR") = objDataReader("CURR")
                    objDataRow("DES1") = objDataReader("DES1")
                    objDataRow("ITM_CD") = objDataReader("ITM_CD")
                    objDataRow("Xfer_Type_Cd") = Xfer_Type_Cd
                    objDataRow("Discrepancy") = 0
                    objDataRow("DEPT_CD") = objDataReader("DEPT_CD")
                    objDataRow("CLASS_CD") = objDataReader("CLASS_CD")
                    objDataRow("SUBCLASS_CD") = objDataReader("SUBCLASS_CD")
                    objDataRow("Item_LBS") = GetItemWeight(objDataReader("DEPT_CD"), objDataReader("CLASS_CD"), objDataReader("SUBCLASS_CD"), objDataReader("ITM_CD"), SKU_Num)
                    objDataSet.Tables("BoxItems").Rows.Add(objDataRow)

                Else

                    objDataRow = objDataSet.Tables("BoxItems").NewRow
                    objDataRow("Item_Qty") = 1
                    objDataRow("SKU_Num") = SKU_Num
                    objDataRow("Received_Qty") = 0
                    objDataRow("Cust_Id") = Cust_Id
                    objDataRow("Xfer_Desc") = Xfer_Desc
                    objDataRow("VE_CD") = objDataReader("VE_CD")
                    objDataRow("SIZE_CD") = objDataReader("SIZE_CD")
                    objDataRow("CURR") = objDataReader("CURR")
                    objDataRow("DES1") = objDataReader("DES1")
                    objDataRow("ITM_CD") = objDataReader("ITM_CD")
                    objDataRow("Xfer_Type_Cd") = Xfer_Type_Cd
                    objDataRow("DEPT_CD") = objDataReader("DEPT_CD")
                    objDataRow("CLASS_CD") = objDataReader("CLASS_CD")
                    objDataRow("SUBCLASS_CD") = objDataReader("SUBCLASS_CD")
                    objDataRow("Item_LBS") = GetItemWeight(objDataReader("DEPT_CD"), objDataReader("CLASS_CD"), objDataReader("SUBCLASS_CD"), objDataReader("ITM_CD"), SKU_Num)
                    objDataRow("Discrepancy") = 0

                    objDataSet.Tables("BoxItems").Rows.Add(objDataRow)
                End If
            Else

                RaiseEvent InValidSKU(SKU_Num, 1, "ITEM DOES NOT EXIST.")

            End If

            objCommand.Dispose()
            objSQLConnection.Dispose()
            objSQLConnection.Close()
        End If
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
    End Sub

    Private Function GetItemWeight(ByVal Dept_Cd As String, ByVal Class_Cd As String, ByVal SubClass_Cd As String, ByVal Item_Cd As String, ByVal Sku_Num As String) As Decimal

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetItemWeight", objConnection)
        Dim objDataReader As SqlClient.SqlDataReader
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parDept As SqlClient.SqlParameter = objCommand.Parameters.Add("@Dept_Cd", SqlDbType.VarChar, 6)
        Dim parClass As SqlClient.SqlParameter = objCommand.Parameters.Add("@Class_Cd", SqlDbType.VarChar, 6)
        Dim parSubClass As SqlClient.SqlParameter = objCommand.Parameters.Add("@SubClass_Cd", SqlDbType.VarChar, 6)
        Dim parItem As SqlClient.SqlParameter = objCommand.Parameters.Add("@Item_Cd", SqlDbType.VarChar, 12)
        Dim parSKU As SqlClient.SqlParameter = objCommand.Parameters.Add("@SKU_Num", SqlDbType.VarChar, 12)
        Dim parItemWeight As SqlClient.SqlParameter = objCommand.Parameters.Add("@Item_Weight", SqlDbType.Float)

        parDept.Direction = ParameterDirection.Input
        parClass.Direction = ParameterDirection.Input
        parSubClass.Direction = ParameterDirection.Input
        parItem.Direction = ParameterDirection.Input
        parSKU.Direction = ParameterDirection.Input
        parItemWeight.Direction = ParameterDirection.Output

        parDept.Value = Dept_Cd
        parClass.Value = Class_Cd
        parSubClass.Value = SubClass_Cd
        parItem.Value = Item_Cd
        parSKU.Value = Sku_Num

        objCommand.ExecuteNonQuery()

        If Not IsDBNull(parItemWeight.Value) Then
            If parItemWeight.Value > 0 Then
                Return parItemWeight.Value
            Else
                Return ConfigurationSettings.AppSettings("DefaultUPSItemWeight")
            End If
        Else
            Return ConfigurationSettings.AppSettings("DefaultUPSItemWeight")
        End If

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Function

    Private Sub SaveState()

        Session("ItemDataSet") = objDataSet

        'ViewState("ItemDateBoxId") = strBoxId
        'ViewState("ItemSelectedRow") = SelectedRow
        'ViewState("ItemGenDis") = booGenDis

    End Sub

    Private Sub GetState()

        objDataSet = Session("ItemDataSet")

        'strBoxId = ViewState("ItemDateBoxId")
        'SelectedRow = ViewState("ItemSelectedRow")
        'booGenDis = ViewState("ItemGenDis")

    End Sub

    Private Sub CreateEmptyDataSet()

        Dim dt As New DataTable("BoxItems")
        objDataSet.Tables.Add(dt)

        objDataSet.Tables("BoxItems").Columns.Add("Xfer_Type_Cd", GetType(Int32))
        objDataSet.Tables("BoxItems").Columns.Add("SKU_Num", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("Item_Qty", GetType(Int32))
        objDataSet.Tables("BoxItems").Columns.Add("Bag_Id", GetType(Int32))
        objDataSet.Tables("BoxItems").Columns.Add("Request_Id", GetType(Int32))
        objDataSet.Tables("BoxItems").Columns.Add("Cust_Id", GetType(Int32))
        objDataSet.Tables("BoxItems").Columns.Add("Received_Qty", GetType(Int32))
        objDataSet.Tables("BoxItems").Columns.Add("Discrepancy", GetType(Int32))
        objDataSet.Tables("BoxItems").Columns.Add("Request_Qty", GetType(Int32))
        objDataSet.Tables("BoxItems").Columns.Add("VE_CD", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("SIZE_CD", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("CURR", GetType(Decimal))
        objDataSet.Tables("BoxItems").Columns.Add("DES1", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("Xfer_Desc", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("DEPT_CD", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("ITM_CD", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("CLASS_CD", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("SUBCLASS_CD", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("Item_LBS", GetType(Decimal))

        Dim BoxItemskeys(2) As DataColumn

        BoxItemskeys(0) = objDataSet.Tables("BoxItems").Columns("SKU_Num")
        BoxItemskeys(1) = objDataSet.Tables("BoxItems").Columns("Xfer_Type_Cd")
        BoxItemskeys(2) = objDataSet.Tables("BoxItems").Columns("Cust_Id")

        objDataSet.Tables("BoxItems").PrimaryKey = BoxItemskeys

        Dim objDataView As New DataView(objDataSet.Tables("BoxItems"))

        objDataView.Sort = "Xfer_Desc"

        dgTransferBox.DataSource = objDataView
        dgTransferBox.DataBind()

    End Sub

    Private Sub AddORAItemInfo(ByRef objTargetDataTable As DataTable, ByRef objSourceDataTable As DataTable)

        Dim CurrentDataRow As DataRow
        Dim FoundRow As DataRow
        Dim SKU_Num(0) As Object

        For Each CurrentDataRow In objTargetDataTable.Rows

            SKU_Num(0) = CurrentDataRow("SKU_Num")
            FoundRow = objSourceDataTable.Rows.Find(SKU_Num)

            If Not FoundRow Is Nothing Then
                CurrentDataRow("VE_CD") = FoundRow("VE_CD")
                CurrentDataRow("DES1") = FoundRow("DES1")
                CurrentDataRow("SIZE_CD") = FoundRow("SIZE_CD")
                CurrentDataRow("CURR") = FoundRow("CURR")
                CurrentDataRow("DEPT_CD") = FoundRow("DEPT_CD")
                CurrentDataRow("ITM_CD") = FoundRow("ITM_CD")
                CurrentDataRow("CLASS_CD") = FoundRow("CLASS_CD")
                CurrentDataRow("SUBCLASS_CD") = FoundRow("SUBCLASS_CD")
            End If

        Next

    End Sub

    Private Sub UpdateCustReq(ByVal SKU_Num As String, ByVal strRcvStore As String)

        Dim I As Int32
        Dim Found As Boolean

        Found = False

        For I = 0 To objDataSet.Tables("BoxItems").Rows.Count - 1

            If objDataSet.Tables("BoxItems").Rows(I)("Xfer_Type_Cd") = 3 Then

                If objDataSet.Tables("BoxItems").Rows(I)("Received_Qty") < objDataSet.Tables("BoxItems").Rows(I)("Item_Qty") Then

                    objDataSet.Tables("BoxItems").Rows(I)("Received_Qty") = objDataSet.Tables("BoxItems").Rows(I)("Received_Qty") + 1
                    Found = True
                    Exit For

                End If

            End If

        Next

        If Not Found Then

            'UpdateQty(3, SKU_Num)
            UpdateQty_TopDown(SKU_Num, strRcvStore)
        End If

    End Sub

    Private Sub UpdateQty_TopDown(ByVal SKU_Num As String, ByVal strRcvStore As string)
        'ucTransferHeader.RStore.ToString()
        Dim I As Int32
        Dim strSQL As String

        Dim UpdateRow As Int32 = -1

        For I = 0 To objDataSet.Tables("BoxItems").Rows.Count - 1

            If objDataSet.Tables("BoxItems").Rows(I)("SKU_Num") = SKU_Num And (objDataSet.Tables("BoxItems").Rows(I)("Received_Qty") < objDataSet.Tables("BoxItems").Rows(I)("Item_Qty")) Then
                UpdateRow = I
                Exit For
            End If

            'Unassigned Entry Is found
            If objDataSet.Tables("BoxItems").Rows(I)("Xfer_Type_Cd") = 6 And objDataSet.Tables("BoxItems").Rows(I)("SKU_Num") = SKU_Num Then
                UpdateRow = I
                Exit For
            End If

        Next

        If UpdateRow <> -1 Then
            objDataSet.Tables("BoxItems").Rows(UpdateRow)("Received_Qty") = objDataSet.Tables("BoxItems").Rows(UpdateRow)("Received_Qty") + 1
        Else
            Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            Dim objDataRow As DataRow

            Dim objDataReader As SqlClient.SqlDataReader
            objSQLConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand("spGetItemAddToBox", objSQLConnection)
            objCommand.CommandType = CommandType.StoredProcedure

            Dim ItemIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Item_Id", SqlDbType.VarChar)
            Dim ReceiveStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@RcvStore", SqlDbType.VarChar)

            ItemIdParam.Direction = ParameterDirection.Input

            ItemIdParam.Value = SKU_Num

            ReceiveStoreParam.Direction = ParameterDirection.Input
            ReceiveStoreParam.Value = strRcvStore

            objDataReader = objCommand.ExecuteReader()

            If objDataReader.Read Then
                objDataRow = objDataSet.Tables("BoxItems").NewRow
                objDataRow("Item_Qty") = 0
                objDataRow("Box_Id") = strBoxId
                objDataRow("SKU_Num") = SKU_Num
                objDataRow("Cust_Id") = 0
                objDataRow("VE_CD") = objDataReader("VE_CD")
                objDataRow("SIZE_CD") = objDataReader("SIZE_CD")
                objDataRow("CURR") = objDataReader("CURR")
                objDataRow("DES1") = objDataReader("DES1")
                objDataRow("ITM_CD") = objDataReader("ITM_CD")
                objDataRow("Xfer_Type_Cd") = 6
                objDataRow("Xfer_Desc") = "Unassigned"
                objDataRow("Discrepancy") = 0
                objDataRow("Received_Qty") = 1
                objDataRow("DEPT_CD") = objDataReader("DEPT_CD")

                objDataRow("CLASS_CD") = objDataReader("CLASS_CD")
                objDataRow("SUBCLASS_CD") = objDataReader("SUBCLASS_CD")
                'Passing 0 to the the item weight by Vijay
                objDataRow("Item_LBS") = 0 'GetItemWeight(objDataReader("DEPT_CD"), objDataReader("CLASS_CD"), objDataReader("SUBCLASS_CD"), objDataReader("ITM_CD"), SKU_Num)
                objDataSet.Tables("BoxItems").Rows.Add(objDataRow)

            Else

                RaiseEvent InValidSKU(SKU_Num, 1, "ITEM DOES NOT EXIST.")

            End If

            objCommand.Dispose()
            objSQLConnection.Dispose()
            objSQLConnection.Close()
        End If

    End Sub

    'Private Sub UpdateQty(ByVal Xfer_Type_Cd As Int32, ByVal SKU_Num As String)

    'Dim FindItems(2) As Object
    'Dim strSQL As String
    'Dim FoundRow As DataRow

    'FindItems(0) = SKU_Num
    'FindItems(1) = Xfer_Type_Cd
    'FindItems(2) = 0

    'FoundRow = objDataSet.Tables("BoxItems").Rows.Find(FindItems)

    'If FoundRow Is Nothing Then

    'FindItems(0) = SKU_Num
    'FindItems(1) = 6
    'FindItems(2) = 0

    'FoundRow = objDataSet.Tables("BoxItems").Rows.Find(FindItems)

    'End If

    'If Not FoundRow Is Nothing Then

    'If FoundRow("Received_Qty") < 999 Then
    'FoundRow("Received_Qty") = FoundRow("Received_Qty") + 1

    'FoundRow("Discrepancy") = 0
    'End If

    'Else

    'Dim objORAConnection As New OleDbConnection(ConfigurationSettings.AppSettings("strGERSConn"))
    'Dim objDataRow As DataRow

    'strSQL = " SELECT DISTINCT  U.SKU_NUM, A.DES1, B.curr, U.SIZE_CD,A.DEPT_CD, A.DES1,A.ITM_CD, A.VE_CD "
    'strSQL = strSQL & " FROM gm_itm A, GM_SKU U, "
    'strSQL = strSQL & " (SELECT P1.sku_num SKU_NUM, P1.ret_prc CURR "
    'strSQL = strSQL & " FROM gm_prc P1,  "
    'strSQL = strSQL & " (SELECT sku_num, MAX(to_char(beg_dt, 'yyyymmdd')||to_char(ent_dt, 'yyyymmdd')||ent_time) last "
    'strSQL = strSQL & "  FROM gm_prc  "
    'strSQL = strSQL & "  WHERE sku_num = '" & SKU_Num & "' AND beg_dt <= SYSDATE "
    'strSQL = strSQL & "  GROUP BY sku_num ) P2 "
    'strSQL = strSQL & "  WHERE P1.sku_num = P2.sku_num "
    'strSQL = strSQL & "  AND TO_CHAR(P1.beg_dt, 'yyyymmdd')||TO_CHAR(P1.ent_dt,'yyyymmdd')||P1.ent_time = P2.last) B "
    'strSQL = strSQL & "  WHERE A.itm_cd = SUBSTR(B.sku_num,1,6) AND (A.ITM_CD = U.ITM_CD) AND U.SKU_NUM = '" & SKU_Num & "' "
    'strSQL = strSQL & "  AND u.sku_num=b.sku_num "

    'objORAConnection.Open()

    'Dim objCommand As New OleDb.OleDbCommand(strSQL, objORAConnection)
    'Dim objDataReader As OleDb.OleDbDataReader

    'objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

    'If objDataReader.Read Then

    'objDataRow = objDataSet.Tables("BoxItems").NewRow
    'objDataRow("Item_Qty") = 0
    'objDataRow("Box_Id") = strBoxId
    'objDataRow("SKU_Num") = SKU_Num
    'objDataRow("Cust_Id") = 0
    'objDataRow("VE_CD") = objDataReader("VE_CD")
    'objDataRow("SIZE_CD") = objDataReader("SIZE_CD")
    'objDataRow("CURR") = objDataReader("CURR")
    'objDataRow("DES1") = objDataReader("DES1")
    'objDataRow("ITM_CD") = objDataReader("ITM_CD")
    'objDataRow("Xfer_Type_Cd") = 6
    'objDataRow("Xfer_Desc") = "Unassigned"
    'objDataRow("Discrepancy") = 0
    'objDataRow("Received_Qty") = 1
    'objDataRow("DEPT_CD") = objDataReader("DEPT_CD")
    'objDataSet.Tables("BoxItems").Rows.Add(objDataRow)

    'Else

    'RaiseEvent InValidSKU(SKU_Num, 1, "ITEM DOES NOT EXIST.")

    'End If

    'objORAConnection.Close()

    'End If

    'End Sub

    Private Function IsValidRTV(ByVal Dept_Cd As String, ByVal Vendor_Cd As String) As Boolean

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spIsValidRTV", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim prmDept As SqlClient.SqlParameter = objCommand.Parameters.Add("@Dept_Cd", SqlDbType.VarChar)
        Dim prmVendor As SqlClient.SqlParameter = objCommand.Parameters.Add("@Vendor_Cd", SqlDbType.VarChar)
        Dim prmValid As SqlClient.SqlParameter = objCommand.Parameters.Add("@ValidSKU", SqlDbType.Bit)

        prmDept.Direction = ParameterDirection.Input
        prmVendor.Direction = ParameterDirection.Input
        prmValid.Direction = ParameterDirection.Output

        prmDept.Value = Dept_Cd
        prmVendor.Value = Vendor_Cd

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return prmValid.Value

    End Function
    Public Sub CreateDataSet()

        Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objSQLCommand As New SqlCommand
        Dim objSQLDataAdapter As New SqlDataAdapter
        Dim I As Int32
        Dim SKUNumbers As String
        Dim strSQL As String
        Dim objRelation As DataRelation

        objSQLCommand.Connection = objSQLConnection
        objSQLCommand.CommandType = CommandType.StoredProcedure
        objSQLCommand.CommandText = "spGetBoxItems"
        objSQLCommand.Parameters.Add("@Box_Id", strBoxId)

        objSQLDataAdapter.SelectCommand = objSQLCommand
        objSQLDataAdapter.FillSchema(objDataSet, SchemaType.Source)
        objSQLDataAdapter.Fill(objDataSet, "BoxItems")

        Dim BoxItemskeys(2) As DataColumn

        BoxItemskeys(0) = objDataSet.Tables("BoxItems").Columns("SKU_Num")
        BoxItemskeys(1) = objDataSet.Tables("BoxItems").Columns("Xfer_Type_Cd")
        BoxItemskeys(2) = objDataSet.Tables("BoxItems").Columns("Cust_Id")

        objDataSet.Tables("BoxItems").PrimaryKey = BoxItemskeys

        objSQLConnection.Close()

        If objDataSet.Tables("BoxItems").Rows.Count > 0 Then

            For I = 0 To objDataSet.Tables("BoxItems").Rows.Count - 1
                SKUNumbers = SKUNumbers & "'" & Replace(objDataSet.Tables("BoxItems").Rows(I).Item("SKU_Num"), "'", "''") & "',"
            Next I

            SKUNumbers = Mid(SKUNumbers, 1, SKUNumbers.Length - 1)

            'strSQL = " SELECT DISTINCT U.SKU_Num AS SKU_NUM, U.DES1 as DES1, U.CURR, U.DEPT_CD, U.SIZE_CD, U.VE_CD, U.ITM_CD, U.CLASS_CD, U.SUBCLASS_CD " _
            '    & " FROM Box_Xfer_Item  U " _
            '    & " WHERE U.SKU_NUM IN (" & SKUNumbers & ") "

        End If

        Dim objDataView As New DataView(objDataSet.Tables("BoxItems"))

        objDataView.Sort = "Xfer_Desc"

        dgTransferBox.DataSource = objDataView
        dgTransferBox.DataBind()


    End Sub
    'Public Sub AddItemToBox(ByVal Xfer_Type_Cd As Int32, ByVal SKU_Num As String, ByVal Cust_Id As Int32, ByVal RTV_Reason As Byte, ByVal Xfer_Desc As String)

    '    Dim FindItems(2) As Object
    '    Dim strSQL As String

    '    FindItems(0) = SKU_Num
    '    FindItems(1) = Xfer_Type_Cd
    '    FindItems(2) = Cust_Id

    '    GetState()

    '    Dim FoundRow = objDataSet.Tables("BoxItems").Rows.Find(FindItems)

    '    If Not FoundRow Is Nothing Then

    '        If FoundRow("Item_Qty") < 999 Then
    '            FoundRow("Item_Qty") = FoundRow("Item_Qty") + 1
    '            'FoundRow("Discrepancy") = System.Math.Abs(FoundRow("Item_Qty") - FoundRow("Received_Qty"))
    '            FoundRow("Discrepancy") = 0
    '        End If

    '    Else

    '        Dim objORAConnection As New OleDbConnection(ConfigurationSettings.AppSettings("strGERSConn"))
    '        Dim objDataRow As DataRow

    '        'BW 01-21-04 apostrophe fix
    '        strSQL = " SELECT DISTINCT U.SKU_NUM, A.DES1, cs_prc_util.eff_ret_prc(U.sku_num, '" & Store.ToString().PadLeft(4, "0") _
    '            & "') CURR, U.SIZE_CD, A.DES1, A.VE_CD,A.ITM_CD, A.DEPT_CD, A.CLASS_CD, A.SUBCLASS_CD " _
    '            & " FROM gm_itm A, GM_SKU U " _
    '            & " WHERE A.ITM_CD = U.ITM_CD AND U.SKU_NUM = '" & Replace(SKU_Num, "'", "''") & "' " _
    '            & " AND (A.grid_flag = 'N' or (A.grid_flag = 'Y' and substr('" & Replace(SKU_Num, "'", "''") & "',-3,3) <> '000'))"
    '        'AP 01/12/2006 added logic so that we don't get -000 skus that we don't want

    '        objORAConnection.Open()

    '        Dim objCommand As New OleDb.OleDbCommand(strSQL, objORAConnection)
    '        Dim objDataReader As OleDb.OleDbDataReader

    '        objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

    '        If objDataReader.Read Then

    '            If Xfer_Type_Cd = 1 Then

    '                'If Not IsValidRTV(objDataReader("DEPT_CD"), objDataReader("VE_CD")) Then
    '                ' RaiseEvent InValidSKU(SKU_Num, 2, "The vendor of this product does not<br>accept returns.  This product should<br>either be marked ""As Is"" for discounted<br>sale (see SOP #3389) or included with<br>DM MOS product (see SOP #2135).")
    '                ' Else
    '                'Removed Per Rick Ito request

    '                objDataRow = objDataSet.Tables("BoxItems").NewRow
    '                objDataRow("Item_Qty") = 1
    '                objDataRow("SKU_Num") = SKU_Num
    '                objDataRow("Received_Qty") = 0
    '                objDataRow("Cust_Id") = Cust_Id
    '                objDataRow("Xfer_Desc") = Xfer_Desc
    '                objDataRow("VE_CD") = objDataReader("VE_CD")
    '                objDataRow("SIZE_CD") = objDataReader("SIZE_CD")
    '                objDataRow("CURR") = objDataReader("CURR")
    '                objDataRow("DES1") = objDataReader("DES1")
    '                objDataRow("ITM_CD") = objDataReader("ITM_CD")
    '                objDataRow("Xfer_Type_Cd") = Xfer_Type_Cd
    '                objDataRow("Discrepancy") = 0
    '                objDataRow("DEPT_CD") = objDataReader("DEPT_CD")
    '                objDataRow("CLASS_CD") = objDataReader("CLASS_CD")
    '                objDataRow("SUBCLASS_CD") = objDataReader("SUBCLASS_CD")
    '                objDataRow("Item_LBS") = GetItemWeight(objDataReader("DEPT_CD"), objDataReader("CLASS_CD"), objDataReader("SUBCLASS_CD"), objDataReader("ITM_CD"), SKU_Num)
    '                objDataSet.Tables("BoxItems").Rows.Add(objDataRow)

    '                ' End If

    '            Else

    '                objDataRow = objDataSet.Tables("BoxItems").NewRow
    '                objDataRow("Item_Qty") = 1
    '                objDataRow("SKU_Num") = SKU_Num
    '                objDataRow("Received_Qty") = 0
    '                objDataRow("Cust_Id") = Cust_Id
    '                objDataRow("Xfer_Desc") = Xfer_Desc
    '                objDataRow("VE_CD") = objDataReader("VE_CD")
    '                objDataRow("SIZE_CD") = objDataReader("SIZE_CD")
    '                objDataRow("CURR") = objDataReader("CURR")
    '                objDataRow("DES1") = objDataReader("DES1")
    '                objDataRow("ITM_CD") = objDataReader("ITM_CD")
    '                objDataRow("Xfer_Type_Cd") = Xfer_Type_Cd
    '                objDataRow("DEPT_CD") = objDataReader("DEPT_CD")
    '                objDataRow("CLASS_CD") = objDataReader("CLASS_CD")
    '                objDataRow("SUBCLASS_CD") = objDataReader("SUBCLASS_CD")
    '                objDataRow("Item_LBS") = GetItemWeight(objDataReader("DEPT_CD"), objDataReader("CLASS_CD"), objDataReader("SUBCLASS_CD"), objDataReader("ITM_CD"), SKU_Num)
    '                objDataRow("Discrepancy") = 0

    '                objDataSet.Tables("BoxItems").Rows.Add(objDataRow)
    '            End If
    '        Else

    '            RaiseEvent InValidSKU(SKU_Num, 1, "ITEM DOES NOT EXIST.")

    '        End If

    '        objORAConnection.Close()

    '    End If

    '    'Session("ItemDataSet") = objDataSet

    'End Sub
End Class


