Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb

Public MustInherit Class ScanItem
    Inherits System.Web.UI.UserControl

    Protected WithEvents lblSkuNumber As System.Web.UI.WebControls.Label
    Protected WithEvents ddlTransferType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblTransferType As System.Web.UI.WebControls.Label
    Protected WithEvents tdTransferType As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdSKU As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdDDS As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblCustomerName As System.Web.UI.WebControls.Label
    Protected WithEvents txtSkuNumber As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDDS As System.Web.UI.WebControls.TextBox
    Protected WithEvents divCustomer As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnScanItem As System.Web.UI.WebControls.Button
    Protected WithEvents txtCustomerName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCustomerPhone As System.Web.UI.WebControls.TextBox
    Protected WithEvents cvTransferType As System.Web.UI.WebControls.CustomValidator
    Protected WithEvents txtHCustId As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdCustomer As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents phFocusName As System.Web.UI.WebControls.PlaceHolder
    Protected WithEvents phFocusPhone As System.Web.UI.WebControls.PlaceHolder
    Protected WithEvents phFocusSKU As System.Web.UI.WebControls.PlaceHolder
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblDDS As System.Web.UI.WebControls.Label
    Protected WithEvents phFocusDDS As System.Web.UI.WebControls.PlaceHolder
    Protected WithEvents btnScanRA As System.Web.UI.WebControls.Button

    Private SKUNumber As String
    Private m_strForm As String
    Private m_TType As Byte
    Private m_objUserInfo As New UserInfo()
    Private m_intSendingStore As Integer
    Private m_intReceivingStore As Integer

    Public Event OnReceivedSKU(ByVal SKU_Num As String, ByVal Xfer_Transfer_Cd As Byte, ByVal Xfer_Transfer_Desc As String, ByVal Xfer_Cust_Id As Int32)
    Public Event OnReceivedRA()


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        m_objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        txtSkuNumber.Attributes.Add("onkeypress", "return clickButton()")

        If Not Page.IsPostBack And tdTransferType.Visible Then

            GetTransferTypes()

        End If

        If tdTransferType.Visible Then
            If ddlTransferType.SelectedItem.Value = 3 Then
                tdCustomer.Visible = True
            Else
                tdCustomer.Visible = False
            End If
        End If

        SetFocus()

    End Sub

    Private Sub SetFocus()

        phFocusPhone.Visible = False
        phFocusName.Visible = False
        phFocusSKU.Visible = False
        phFocusDDS.Visible = False

        If txtDDS.Visible = True Then
            phFocusDDS.Visible = True
        Else

            If txtCustomerName.Visible And txtCustomerName.Enabled And txtCustomerName.Text.ToString.Trim.Length = 0 Then
                phFocusName.Visible = True
            Else
                If txtCustomerPhone.Visible And txtCustomerPhone.Enabled And txtCustomerPhone.Text.ToString.Trim.Length = 0 Then
                    phFocusPhone.Visible = True
                ElseIf txtSkuNumber.Visible = True And txtSkuNumber.Enabled = True Then
                    phFocusSKU.Visible = True
                End If
            End If
        End If

    End Sub

    Private Sub txtCustomerPhone_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomerPhone.TextChanged
        ViewState("booNewCustomer") = True
    End Sub

    Private Sub txtCustomerName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomerName.TextChanged
        ViewState("booNewCustomer") = True
    End Sub

    Private Function SaveCustomer() As Int32

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spInsertCustomer", objConnection)

        objCommand.CommandType = CommandType.StoredProcedure

        Dim prmFullName As SqlClient.SqlParameter = objCommand.Parameters.Add("@FullName", SqlDbType.VarChar, 50)
        Dim prmPhone As SqlClient.SqlParameter = objCommand.Parameters.Add("@Phone", SqlDbType.VarChar, 15)
        Dim prmCustId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Cust_Id", SqlDbType.Int)

        prmFullName.Direction = ParameterDirection.Input
        prmPhone.Direction = ParameterDirection.Input
        prmCustId.Direction = ParameterDirection.Output

        prmFullName.Value = txtCustomerName.Text
        prmPhone.Value = txtCustomerPhone.Text

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return prmCustId.Value

    End Function

    Public Sub ServerValidation(ByVal source As Object, ByVal args As ServerValidateEventArgs)

        If (ddlTransferType.SelectedIndex = 0 And SKUNumber.Length > 0) Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If

    End Sub

    Public Sub GetTransferTypes()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objItem As New ListItem()

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objCommand.CommandText = "spGetTransferTypes " & m_TType & "," & m_intSendingStore & "," & m_intReceivingStore

        objDataReader = objCommand.ExecuteReader()
        ddlTransferType.DataTextField = "Xfer_Desc"
        ddlTransferType.DataValueField = "Xfer_Cd"
        ddlTransferType.DataSource = objDataReader
        ddlTransferType.DataBind()

        objItem.Text = "Select A Type"
        objItem.Value = 0

        ddlTransferType.Items.Insert(0, objItem)

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Function ConvertSmithSKU(ByVal InSKU As String) As String

        Dim strSQL As String
        Dim objORACommand As New OleDbCommand()
        Dim objORAConnection As New OleDbConnection(ConfigurationSettings.AppSettings("strGERSConn"))
        Dim objORADataReader As OleDbDataReader

        objORAConnection.Open()

        strSQL = "SELECT U.SKU_NUM "
        strSQL = strSQL & "FROM TD_PLU_WMOS U "
        strSQL = strSQL & "WHERE U.UPC_CD = '" & Replace(InSKU, "'", "''") & "' "

        objORACommand.Connection = objORAConnection
        objORACommand.CommandText = strSQL
        objORADataReader = objORACommand.ExecuteReader()

        If objORADataReader.Read() Then
            Return objORADataReader("SKU_NUM")
        Else
            Return InSKU
        End If

        objORAConnection.Close()

    End Function
    'Private Function GetSKUNumber(ByVal InSKU As String)

    '    Dim tmpSKU As String

    '    If InSKU.Length > 9 Then
    '        If Mid(InSKU, 1, 2) = "00" Then
    '            tmpSKU = Mid(InSKU, 3, InSKU.Length - 2)
    '            Return Mid(tmpSKU, 1, 6) & "-" & Mid(tmpSKU, 7, 3)
    '        Else
    '            Return InSKU
    '        End If
    '    ElseIf Not InStr(InSKU, "-") Then
    '        Return Mid(InSKU, 1, 6) & "-" & Mid(InSKU, 7, 3)
    '    Else
    '        Return InSKU
    '    End If

    'End Function

    Private Function GetSKUNumber(ByVal InSKU As String)

        Dim tmpSKU As String

        If InSKU.Length > 9 Then
            If Mid(InSKU, 1, 2) = "00" Then
                tmpSKU = Mid(InSKU, 3, InSKU.Length - 2)
                Return Mid(tmpSKU, 1, 6) & "-" & Mid(tmpSKU, 7, 3)
            Else
                Return InSKU
            End If
        ElseIf InStr(InSKU, "-") Then
            Return Mid(InSKU, 1, 6) & "-" & Mid(InSKU, 7, 3)
        Else
            Return InSKU
        End If

    End Function

    WriteOnly Property RAVisible()
        Set(ByVal Value)
            tdDDS.Visible = Value
        End Set
    End Property

    WriteOnly Property SKUVisible()
        Set(ByVal Value)
            tdSKU.Visible = Value
        End Set
    End Property

    WriteOnly Property TransferSelectionVisible()
        Set(ByVal Value)
            tdTransferType.Visible = Value
        End Set
    End Property

    WriteOnly Property CustomerInfoVisible()
        Set(ByVal Value)
            tdCustomer.Visible = Value
        End Set
    End Property

    Public Sub EnableScan(ByVal value As Boolean)

        Dim clrConverter As New System.Drawing.ColorConverter

        ddlTransferType.Enabled = value
        txtSkuNumber.Enabled = value
        txtCustomerPhone.Enabled = value
        txtCustomerName.Enabled = value

        If value = False Then
            txtSkuNumber.Text = ""
            txtHCustId.Value = 0
            txtCustomerName.Text = ""
            txtCustomerPhone.Text = ""

            txtCustomerPhone.BackColor = clrConverter.ConvertFromString("0xFFCCCCCC")
            txtCustomerName.BackColor = clrConverter.ConvertFromString("0xFFCCCCCC")
            txtSkuNumber.BackColor = clrConverter.ConvertFromString("0xFFCCCCCC")
            ddlTransferType.BackColor = clrConverter.ConvertFromString("0xFFCCCCCC")
        Else
            txtCustomerPhone.BackColor = clrConverter.ConvertFromString("0xFFFFFFFF")
            txtCustomerName.BackColor = clrConverter.ConvertFromString("0xFFFFFFFF")
            txtSkuNumber.BackColor = clrConverter.ConvertFromString("0xFFFFFFFF")
            ddlTransferType.BackColor = clrConverter.ConvertFromString("0xFFFFFFFF")
        End If


        SetFocus()

    End Sub

    Private Function Valid_SKU() As Boolean

        Dim strSQL As String
        Dim objORACommand As New OleDbCommand
        Dim objORAConnection As New OleDbConnection(ConfigurationSettings.AppSettings("strGERSConn"))
        Dim objORADataReader As OleDbDataReader

        objORAConnection.Open()

        strSQL = "SELECT U.SKU_NUM "
        strSQL = strSQL & "FROM GM_SKU U "
        strSQL = strSQL & "WHERE U.SKU_NUM = '" & Replace(SKUNumber, "'", "''") & "' "

        objORACommand.Connection = objORAConnection
        objORACommand.CommandText = strSQL
        objORADataReader = objORACommand.ExecuteReader()

        If objORADataReader.Read() Then
            Return True
        Else
            Return False
        End If

        objORAConnection.Close()

    End Function

    WriteOnly Property SetTType()
        Set(ByVal Value)
            m_TType = Value
        End Set
    End Property

    WriteOnly Property SetForm()
        Set(ByVal Value)
            m_strForm = Value
        End Set
    End Property

    ReadOnly Property RA()
        Get
            Return txtDDS.Text
        End Get
    End Property

    Private Sub btnScanItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnScanItem.Click

        If txtSkuNumber.Text.Length > 0 Then
            '# modified for RMS change 09/20/2012 by LJ
            'If txtSkuNumber.Text.Length >= 5 And txtSkuNumber.Text.Length <= 7 Then
            '    SKUNumber = ConvertSmithSKU(txtSkuNumber.Text)
            'Else
            SKUNumber = GetSKUNumber(txtSkuNumber.Text)
            'End If

        If tdTransferType.Visible Then
            If ViewState("booNewCustomer") And ddlTransferType.SelectedItem.Value = 3 Then
                If Len(txtCustomerName.Text.Trim) > 0 And Len(txtCustomerPhone.Text.Trim) > 0 Then
                    txtHCustId.Value = SaveCustomer()
                    ViewState("booNewCustomer") = False
                Else
                    txtHCustId.Value = 0
                End If
            ElseIf txtHCustId.Value = "" Then
                txtHCustId.Value = 0
            End If
        End If

        If tdTransferType.Visible Then
            RaiseEvent OnReceivedSKU(SKUNumber, ddlTransferType.SelectedItem.Value, ddlTransferType.SelectedItem.Text, txtHCustId.Value)
        Else
            RaiseEvent OnReceivedSKU(SKUNumber, 0, 0, 0)
        End If

        txtSkuNumber.Text = ""

        End If

    End Sub

    Private Sub ddlTransferType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTransferType.SelectedIndexChanged

        If tdTransferType.Visible Then
            If ddlTransferType.SelectedItem.Value = 3 Then
                tdCustomer.Visible = True
            Else
                tdCustomer.Visible = False
                txtCustomerPhone.Text = ""
                txtCustomerName.Text = ""
                txtHCustId.Value = 0
            End If
        End If

    End Sub

    Private Sub btnScanRA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnScanRA.Click
        RaiseEvent OnReceivedRA()
    End Sub

    WriteOnly Property SendingStore()
        Set(ByVal Value)
            m_intSendingStore = Value
        End Set
    End Property

    WriteOnly Property ReceivingStore()
        Set(ByVal Value)
            m_intReceivingStore = Value
        End Set
    End Property

End Class
