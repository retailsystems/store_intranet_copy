<%@ Register TagPrefix="uc1" TagName="SearchBox" Src="UserControls/SearchBox.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ScanLabel.aspx.vb" Inherits="SSTransfer.ScanLabel" %>
<%@ Register TagPrefix="uc1" TagName="ScanUPS" Src="UserControls/ScanUPS.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Login</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
  </HEAD>
	<body class="sBody" id="pageBody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0" runat="server">
		<form id="Form1" onsubmit="return checkSubmit();" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="center">
						<table cellSpacing="0" cellPadding="4" width="100%">
							<tr>
								<td vAlign="center" align="middle"><IMG src="images\scan.gif" border="0"></td>
								<td vAlign="center" align="middle">
									<table cellSpacing="0" cellPadding="4" border="0">
										<tr>
											<td class="sScanLabelTitle"><asp:label id="lblInstruction" runat="server">Please use one of your pre-printed shipping labels to assign a tracking number.<br><br>You have selected the following tracking number:</asp:label></td>
										</tr>
										<tr>
											<td><asp:label id="lblTrackNum" cssclass="sScanLabel" runat="server"></asp:label><asp:textbox id="txtTrackNum" runat="server" MaxLength="20" Width="20em"></asp:textbox></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td align="middle"><asp:button CssClass="sButton" id="btnSubmit" runat="server" Text="Submit"></asp:button>&nbsp;
												<asp:button id="btnCancel" CssClass="sButton" runat="server" Text="Cancel"></asp:button>&nbsp;
												<asp:button id="btnClear" CssClass="sButton" runat="server" Text="Clear" Visible="false"></asp:button>&nbsp;
												<asp:button id="btnAssign" CssClass="sButton" runat="server" Text="Assign Tracking Number" visible="false"></asp:button></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
		<script language="javascript">
			if(document.forms[0].txtTrackNum != null){
				document.forms[0].txtTrackNum.focus();
			}
		</script>
	</body>
</HTML>
