<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AdvancedSearch.aspx.vb" Inherits="SSTransfer.AdvancedSearch" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Advanced Search</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server" />
	</HEAD>
	<body class="sBody" id="pageBody" runat="server" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="frmAdvancedSearch" method="post" runat="server" onsubmit="return checkSubmit();">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" class="sInfoTable">
				<tr>
					<td colSpan="2" height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td noWrap valign="top" align="middle" colSpan="2">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td align="right" width="30%"><asp:label id="lblSearchFor" cssClass="sRadiobuttonList" runat="server">Search For:</asp:label>&nbsp;</td>
								<td noWrap width="70%">
									<asp:radiobuttonlist CssClass="sRadiobuttonList" id="rblSearchOptions" tabIndex="1" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
										<asp:ListItem Value="Void">Void Boxes</asp:ListItem>
										<asp:ListItem Value="Expected">Expected Arrivals</asp:ListItem>
										<asp:ListItem Value="Sent">Sent Boxes</asp:ListItem>
										<asp:ListItem Value="Received" Selected="True">Received Boxes</asp:ListItem>
									</asp:radiobuttonlist>
								</td>
							</tr>
						</table>
						<hr class="sHr" width="100%">
					</td>
				</tr>
				<tr width="100%">
					<td width="100%" colSpan="2"><asp:label id="lblError" runat="server" Width="100%"></asp:label></td>
				</tr>
				<tr>
					<TD noWrap width="50%">
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr height="3%">
								<td vAlign="top" align="right" noWrap width="1%">
									<asp:label id="lblSKU" Runat="server" EnableViewState="False">SKU&nbsp;</asp:label></td>
								<td vAlign="top" width="99%" nowrap>
									<asp:textbox id="txtSKU" runat="server" Width="90px" MaxLength="10"></asp:textbox></td>
							</tr>
							<tr height="2%">
								<td vAlign="top" noWrap width="1%">
									<asp:label id="lblDate" Runat="server" EnableViewState="False">Received Date:</asp:label>
									<asp:label id="lblFrom" Runat="server" EnableViewState="False">From&nbsp;</asp:label></td>
								<td vAlign="top" width="99%" nowrap>
									<asp:textbox id="txtFDate" tabIndex="2" runat="server" Width="90px" MaxLength="15"></asp:textbox></td>
							</tr>
							<tr height="2%">
								<td vAlign="top" noWrap width="1%"></td>
								<td vAlign="top" width="99%" nowrap><asp:Label Runat="server" ID="lblFExample">(Format Example: 11/2/02)</asp:Label></td>
							</tr>
							<tr height="2%">
								<td vAlign="top" align="right" noWrap width="1%">
									<asp:label id="lblthru" Runat="server" EnableViewState="False">Thru&nbsp;</asp:label></td>
								<td vAlign="top" width="99%" nowrap>
									<asp:textbox id="txtTDate" tabIndex="3" runat="server" Width="90px" MaxLength="15"></asp:textbox></td>
							</tr>
							<tr height="1%">
								<td vAlign="top" noWrap width="1%">
								<td vAlign="top" width="99%" nowrap><asp:Label Runat="server" ID="lblTExample">(Format Example: 12/6/02)</asp:Label></td>
							</tr>
							<tr height="15%">
								<td vAlign="top" noWrap align="right" width="1%"></td>
								<TD vAlign="top" width="99%"></TD>
							</tr>
						</table>
					</TD>
					<td width="50%">
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td vAlign="top" noWrap width="1%" height="1%"><asp:label id="lblStore" runat="server" Width="20%" EnableViewState="False">From Store:</asp:label></td>
								<td vAlign="top" width="99%" height="1%"><asp:listbox id="lbStores" CssClass="sListbox" tabIndex="4" runat="server" Width="100%" SelectionMode="Multiple" ></asp:listbox></td>
							</tr>
							<tr>
								<td vAlign="top" noWrap width="1%" height="1%"><asp:label id="lblTransferType" Runat="server" EnableViewState="False">Transfer Type:</asp:label></td>
								<td vAlign="top" width="99%" height="1%"><asp:listbox id="lbTransferType" CssClass="sListbox" tabIndex="5" Width="100%" Runat="server" SelectionMode="Multiple"></asp:listbox></td>
							</tr>
							<tr>
								<td vAlign="top" noWrap width="1%" height="1%"><asp:label id="lblStatus" Runat="server" EnableViewState="False">Current Status:</asp:label></td>
								<td vAlign="top" width="99%" height="1%"><asp:listbox id="lbStatus" CssClass="sListbox" tabIndex="6" Width="100%" Runat="server" SelectionMode="Multiple"></asp:listbox></td>
							</tr>
							<tr>
								<td vAlign="top" noWrap width="1%" height="1%"><asp:label id="lblIncludes" runat="server" Visible="False" EnableViewState="False">Box Includes:</asp:label></td>
								<td vAlign="top" width="99%" height="1%"><asp:listbox id="lbIncludes" CssClass="sListbox" tabIndex="7" Width="100%" Runat="server" SelectionMode="Multiple" Visible="False"></asp:listbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="100%" align="right" colSpan="2"><asp:button id="btnSearch" CssClass="sButton" tabIndex="8" runat="server" Text="Submit"></asp:button>&nbsp;
						<asp:button id="btnClear" tabIndex="9" Width="64px" Runat="server" CssClass="sButton" Text="Clear"></asp:button>&nbsp;</td>
				</tr>
				<tr>
					<td colSpan="2" height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
