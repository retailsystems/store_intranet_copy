<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AddUPSServiceConfig.aspx.vb" Inherits="SSTransfer.AddUPSServiceConfig"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Add UPS Service Config</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../../Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="../../Javascript/AllowOneSubmit.js"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
	</HEAD>
	<body class="sBody" id="pageBody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0" runat="server">
		<form id="frmAddUPSUserAccount" onsubmit="return checkSubmit();" method="post" runat="server">
			<table class="sInfoTable" height="100%" cellSpacing="0" cellPadding="1" width="100%">
				<tr>
					<td class="sDialogHeader" align="middle" colSpan="2" height="1%"><asp:label id="lblHeader" runat="server" EnableViewState="False">Add UPS Service Config</asp:label></td>
				</tr>
				<tr>
					<td align="middle" height="99%">
						<table cellSpacing="0" cellPadding="2" width="100%">
							<tr>
								<td noWrap width="1%"><asp:label id="lblConfigType" runat="server" CssClass="Plain">Config Type:</asp:label></td>
								<td><asp:dropdownlist id="ddlConfigType" Width="100%" Runat="server" DataValueField="Config_Type" DataTextField="Description"></asp:dropdownlist></td>
								<td noWrap width="1%"><asp:label id="lblTType" runat="server" CssClass="Plain">Transfer Type:</asp:label></td>
								<td><asp:dropdownlist id="ddlTType" Width="100%" Runat="server" DataValueField="TType" DataTextField="Description"></asp:dropdownlist></td>
								<td noWrap width="1%"><asp:label id="lblServiceCode" runat="server" CssClass="Plain">UPS Service Code:</asp:label></td>
								<td><asp:dropdownlist id="ddlServiceCode" Width="100%" Runat="server" DataValueField="Code" DataTextField="Description"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblSendingStore" runat="server" CssClass="Plain">Sending Store:</asp:label></td>
								<td colSpan="2"><asp:dropdownlist id="ddlSendingStore" Width="100%" Runat="server" DataValueField="StoreNum" DataTextField="StoreName"></asp:dropdownlist></td>
								<td noWrap width="1%"><asp:label id="lblReceivingStore" runat="server" CssClass="Plain">Reciving Store:</asp:label></td>
								<td colSpan="2"><asp:dropdownlist id="ddlReceivingStore" Width="100%" Runat="server" DataValueField="StoreNum" DataTextField="StoreName"></asp:dropdownlist></td>
							</tr>
						</table>
						<br>
						<table width="100%">
							<tr>
								<td align="left"><input class="sButton" onclick="javascript:window.close();" type="button" value="Cancel">
								</td>
								<td align="right"><asp:button id="btnAddServceConfig" EnableViewState="False" CssClass="sButton" Runat="server" Text="Add Service Config"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
