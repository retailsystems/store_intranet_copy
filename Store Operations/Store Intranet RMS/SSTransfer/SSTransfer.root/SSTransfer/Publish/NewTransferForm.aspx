<%@ Register TagPrefix="uc1" TagName="TransferBox" Src="UserControls/TransferBox.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="NewTransferForm.aspx.vb" Inherits="SSTransfer.NewTransferForm" smartNavigation="False" enableViewState="True" trace="False" %>
<%@ Register TagPrefix="uc1" TagName="TransferHeader" Src="UserControls/TransferHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ScanItem" Src="UserControls/ScanItem.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
		<title>Transfer Home</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
  </head>
	<body class="sBody" id="pageBody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0" runat="server">
		<form id="frmTransferHome" onsubmit="return checkSubmit();" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1"><uc1:header id="layoutHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="top" align="center">
						<table cellSpacing="0" cellPadding="4" width="100%">
							<tr>
								<td align="center"><uc1:transferheader id="ucTransferHeader" runat="server"></uc1:transferheader></td>
							</tr>
						</table>
						<hr class="sHr" width="100%">
						<table class="sScanItemDarkBG" cellSpacing="0" cellPadding="4" width="100%">
							<tr>
								<td align="center"><uc1:scanitem id="ucScanItem" runat="server"></uc1:scanitem></td>
							</tr>
						</table>
						<hr class="sHr" width="100%">
						<table cellSpacing="0" cellPadding="4" width="100%" border="0">
							<tr>
								<td align="center">
									<div id="divASDLegend" runat="server">
										<table cellSpacing="0" cellPadding="4" border="0">
											<tr>
												<td><font class="sNewTransFormTitle">Items that should go in the Monday Mail Pack:</font></td>
											</tr>
											<tr>
												<td class="sNormalBig">� Media older than 60 days</td>
											</tr>
											<tr>
												<td class="sNormalBig">� Bankcard Draft envelopes (including signed credit card slips and deposit slips and log)</td>
											</tr>
											<tr>
												<td class="sNormalBig">� HR Accordion (only if there is&nbsp;paperwork inside)</td>
											</tr>
											<tr>
												<td class="sNormalBig">� Expense Reports (including concert and misc. employee expense reports) with all the receipts attached</td>
											</tr>
											<tr>
												<td class="sNormalBig">� Mail or other items for anyone at HQ</td>
											</tr>
											<tr>
												<td class="sNormalBig">� Mail to other stores (may take up to 1 month to arrive at the receiving store)</td>
											</tr>
											<tr>
											    <td class="sNormalBig">� Do NOT send any gift cards</td>
											</tr>
										</table>
									</div>
									<div id="divTransferBox" style="OVERFLOW: auto; WIDTH: 100%; HEIGHT: 200px" runat="server"><uc1:transferbox id="ucTransferBox" runat="server"></uc1:transferbox></div>
								</td>
							</tr>
						</table>
						<br>
						<table cellSpacing="0" cellPadding="4" width="100%">
							<tr>
								<td align="center"><asp:button id="btnVoid" runat="server" Visible="False" Text="Void" EnableViewState="False" CssClass="sButton"></asp:button><asp:button id="btnUpdate" runat="server" Visible="False" Text="Update" EnableViewState="False" CssClass="sButton"></asp:button>&nbsp;
									<asp:button id="btnCancel" runat="server" Visible="False" Text="Cancel" EnableViewState="False" CssClass="sButton"></asp:button><asp:button id="btnEdit" runat="server" Text="Edit" EnableViewState="False" CssClass="sButton"></asp:button>&nbsp;
									<asp:button id="btnSave" runat="server" Text="Save for Later" EnableViewState="False" CssClass="sButton"></asp:button>&nbsp;
									<asp:button id="btnCancelTransfer" runat="server" Text="Cancel" EnableViewState="False" CssClass="sButton"></asp:button>&nbsp;
									<asp:button id="btnSubmit" runat="server" Text="Complete" EnableViewState="False" CssClass="sButton"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</html>
