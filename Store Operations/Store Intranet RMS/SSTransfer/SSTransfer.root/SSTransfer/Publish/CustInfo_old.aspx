<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CustInfo_old.aspx.vb" Inherits="SSTransfer.CustInfo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Customer Information</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<!--<script language="javascript">
		window.focus();
		frmCustInfo.txtHomePhone.focus();
		</script>-->
		<asp:placeholder id="phCloseWindow" visible="false" Runat="server">
			<SCRIPT language="javascript">
			parent.closepopup();		
			</SCRIPT>
		</asp:placeholder>
	</HEAD>
	<body scroll="no" bottomMargin="0" vLink="black" aLink="black" link="black" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmCustInfo" method="post" runat="server">
			<table style="WIDTH: 100%; HEIGHT: 100%" borderColor="#d60c8c" cellSpacing="0" borderColorDark="#d60c8c" cellPadding="1" width="761" borderColorLight="#d60c8c" border="3">
				<tr>
					<td align="middle" bgColor="#ff9ace" colSpan="2"><asp:label id="lblHeader" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="medium" Font-Bold="True">Customer Information</asp:label></td>
				</tr>
				<tr>
					<td align="middle" bgColor="#ffffff">
						<table cellSpacing="0" cellPadding="1" border="0">
							<tr id="trCustNumber" runat="server">
								<td noWrap><asp:label id="lblHomePhone" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="x-small" font-bold="True">Home Phone:</asp:label></td>
								<td noWrap><asp:Label Runat="server" ID="lblOParen" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="medium" font-bold="True" Height="20px">(</asp:Label>&nbsp;<asp:textbox id="txtAreaCode" runat="server" Width="35px" MaxLength="3" TabIndex="1"></asp:textbox>&nbsp;<asp:Label Runat="server" ID="lblEParen" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="medium" font-bold="True" Height="20px">)</asp:Label>&nbsp;<asp:TextBox Runat="server" ID="txtPrefix" Width="35px" MaxLength="3"></asp:TextBox>&nbsp;<asp:Label Runat="server" ID="lblPDash" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="medium" font-bold="True">-</asp:Label>&nbsp;<asp:TextBox Runat="server" ID="txtLast4" Width="45px" MaxLength="4"></asp:TextBox>&nbsp;&nbsp;<asp:button BorderStyle="Solid" id="btnRetrieve" runat="server" ForeColor="black" Font-Bold="True" Width="134px" Text="Retrieve Customer" BackColor="#FAE5F0" BorderColor="#FF9ACE" TabIndex="2"></asp:button></td>
							</tr>
							<tr id="trListCustomers" runat="server" width="100%">
								<td vAlign="top" noWrap><asp:label id="lblCustomerList" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-size="x-small" font-bold="True" Height="22px">List of Customers:</asp:label></td>
								<td vAlign="top" noWrap borderColor="gray" borderColorLight="navy" align="left" bgColor="#ffffff" colSpan="2">
									<asp:datagrid id="dgCustomerList" runat="server" Font-Names="Arial" ForeColor="#FFFFFF" Font-Size="X-Small" Width="100%" BackColor="black" BorderColor="black" Height="40px" CellSpacing="0" CellPadding="0" ItemStyle-Height="15px" ShowHeader="False" AutoGenerateColumns="False" BorderStyle="solid" GridLines="None" TabIndex="3">
										<Columns>
											<asp:TemplateColumn>
												<ItemTemplate>
													<a href='CustInfo.aspx?CIDX=<%# container.dataitem("ID")%>'><font color="#FFFFFF" size="2" face="arial">
															<%# container.dataitem("FirstName") & " " & container.dataitem("LastName")%>
														</font></a>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></td>
							</tr>
							<tr id="trCustomerInfo" runat="server">
								<td noWrap><asp:label id="lblCustomerInfo" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="X-Small" font-bold="True" Font-Underline="False">Customer Information:</asp:label></td>
							</tr>
							<tr id="trCustomerName" runat="server">
								<td noWrap><asp:label id="lblFName" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="x-small" font-bold="True">First Name:</asp:label></td>
								<td noWrap><asp:textbox id="txtFName" runat="server" Width="200px" TabIndex="4"></asp:textbox>
									&nbsp;<asp:label id="lblLName" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="x-small" font-bold="True">Last Name:</asp:label>
									&nbsp;<asp:textbox id="txtLName" runat="server" Width="185px" TabIndex="5"></asp:textbox></td>
							</tr>
							<tr id="trstrAddress" runat="server">
								<td noWrap><asp:label id="lblStrAddress" runat="server" visible="False" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="x-small" font-bold="True">Street Address:</asp:label></td>
								<td noWrap><asp:textbox id="txtStrAddress" runat="server" Width="472px" visible="False" TabIndex="6"></asp:textbox></td>
							</tr>
							<tr id="trCityStateZip" runat="server">
								<td noWrap><asp:label id="lblCity" runat="server" Font-Names="Arial" visible="False" EnableViewState="False" ForeColor="black" Font-Size="x-small" font-bold="True">City:</asp:label></td>
								<td noWrap><asp:textbox id="txtCity" runat="server" Width="212px" visible="False" TabIndex="7"></asp:textbox>
									&nbsp;<asp:label id="lblState" runat="server" Font-Names="Arial" visible="False" EnableViewState="False" ForeColor="black" Font-Size="x-small" font-bold="True">State:</asp:label>
									&nbsp;<asp:textbox id="txtState" runat="server" Width="43px" visible="False" MaxLength="2" TabIndex="8"></asp:textbox>
									&nbsp;<asp:label id="lblZip" runat="server" Font-Names="Arial" visible="False" EnableViewState="False" ForeColor="black" Font-Size="x-small" font-bold="True">Zip:</asp:label>
									&nbsp;<asp:textbox id="txtZip" runat="server" Width="103px" visible="False" MaxLength="12" TabIndex="9"></asp:textbox></td>
							</tr>
							<tr id="trWorkPhone" runat="server">
								<td noWrap><asp:label id="lblWorkPhone" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="x-small" font-bold="True">Work Phone:</asp:label></td>
								<td noWrap><asp:textbox id="txtWorkPhone" runat="server" Width="200px" MaxLength="10" TabIndex="10"></asp:textbox></td>
							</tr>
							<tr id="trNotes" runat="server">
								<td noWrap style="HEIGHT: 71px"><asp:label id="lblNotes" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="x-small" font-bold="True">Notes:</asp:label></td>
								<td noWrap style="HEIGHT: 71px"><asp:textbox id="txtNotes" runat="server" Width="474px" TextMode="MultiLine" Wrap="True" Height="66px" TabIndex="11"></asp:textbox></td>
							</tr>
							<tr id="trMessage" runat="server">
								<td colSpan="2"><asp:label id="lblMessage" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="x-small" font-bold="True" Width="620px" visible="true"></asp:label></td>
							</tr>
							<tr id="TrCloseSubmitbtns" runat="server">
								<td noWrap colSpan="2" align="right"><input id="HCustId" type="hidden" runat="server" NAME="HCustId">
									<input id="HCustName" type="hidden" runat="server" NAME="HCustName">&nbsp;
									<asp:button BorderStyle="Solid" id="btnAddNew" runat="server" ForeColor="black" Font-Bold="True" Width="106px" Text="Add Customer" BackColor="#FAE5F0" BorderColor="#FF9ACE" Visible="False" TabIndex="12"></asp:button>&nbsp;
									<asp:button BorderStyle="Solid" id="btnUpdate" runat="server" ForeColor="black" Font-Bold="True" Width="71px" Text="Update" BackColor="#FAE5F0" BorderColor="#FF9ACE" Visible="False" TabIndex="13"></asp:button>&nbsp;
									<asp:PlaceHolder ID="phAssign" Runat="server" Visible="false">
										<INPUT id="btntest" style="BORDER-WIDTH: 1px; FONT-WEIGHT: bold; BORDER-COLOR: #ff9ace; WIDTH: 61px; COLOR: black; FONT-FAMILY: Arial; HEIGHT: 24px; BACKGROUND-COLOR: #fae5f0; BORDER-STYLE:solid" onclick="javascript:parent.setCustId(HCustId.value,HCustName.value);window.close();" tabIndex="14" type="button" value="Assign"></asp:PlaceHolder>
									&nbsp;<asp:button BorderStyle="Solid" id="btnFClose" runat="server" ForeColor="black" Font-Bold="True" Width="69px" Text="Close" BackColor="#FAE5F0" BorderColor="#FF9ACE" TabIndex="15"></asp:button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<script language="javascript">
		window.focus();
		frmCustInfo.txtHomePhone.focus();
			</script>
		</form>
	</body>
</HTML>
