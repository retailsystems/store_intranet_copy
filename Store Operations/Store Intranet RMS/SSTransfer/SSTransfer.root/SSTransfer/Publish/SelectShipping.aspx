<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SelectShipping.aspx.vb" Inherits="SSTransfer.SelectShipping" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
		<title>Login</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
  </head>
	<body class="sBody" id="pagebody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0" runat="server">
		<form id="frmTransferHome" onsubmit="return checkSubmit();" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td align="center">
						<table cellSpacing="0" cellPadding="2" width="100%" border="0">
							<tr>
								<td class="sSelectShip" vAlign="middle" align="center"><asp:label id="lblHelpLabel" Runat="server"></asp:label></td>
							</tr>
						</table>
						<br>
						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td vAlign="middle" align="center">
									<table class="sDialogTable" id="tbUPS" cellSpacing="0" cellPadding="2" width="400" runat="server">
										<tr class="sSelectShipDarkBG " id="trUPS" runat="server">
											<td class="sSelectShipText "><asp:radiobutton id="rbUPS" runat="server" GroupName="grLabel" Text="Print Shipping Label" Checked="True" AutoPostBack="True"></asp:radiobutton></td>
										</tr>
										<tr>
											<td vAlign="middle" align="center">
												<table cellSpacing="0" cellPadding="2" width="100%" border="0">
													<tr>
														<td class="sSelectShipText" noWrap width="1%"><font>Package Type:&nbsp;</font></td>
														<td><asp:dropdownlist id="ddlPackage" runat="server" AutoPostBack="True" Width="100%"></asp:dropdownlist></td>
													</tr>
													<tr>
														<td class="sSelectShipText" noWrap width="1%"><font>Service Type:&nbsp;</font></td>
														<td><asp:dropdownlist id="ddlService" runat="server" AutoPostBack="True" Width="100%"></asp:dropdownlist></td>
													</tr>
													<tr>
														<td class="sSelectShipText" noWrap width="1%"><asp:label id="lblApproval" runat="server" Visible="false">DM Approval:&nbsp;</asp:label></td>
														<td><asp:textbox id="txtApproval" Runat="server" Width="100%" Visible="false" MaxLength="35"></asp:textbox></td>
													</tr>
													<tr>
														<td align="center" colSpan="2"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<br>
									<table class="sInfoTable" id="tblabel" cellSpacing="0" cellPadding="2" width="400" runat="server">
										<tr class="sTableHighlight" id="trLabel" runat="server">
											<td class="sSelectShipText"><asp:radiobutton id="rbLabel" runat="server" GroupName="grLabel" Text="Use Existing Shipping Label" AutoPostBack="True"></asp:radiobutton></td>
										</tr>
										<tr>
											<td vAlign="middle" align="center">
												<table cellSpacing="0" cellPadding="2" width="100%" border="0">
													<tr>
														<td class="sSelectShipText" noWrap width="1%"><font>Parcel Service:&nbsp;</font></td>
														<td><asp:dropdownlist id="ddlParcel" runat="server" AutoPostBack="True" Width="100%" Enabled="False"></asp:dropdownlist></td>
													</tr>
													<tr>
														<td colSpan="2"><asp:label id="lblParcelInfo" runat="server" CssClass="sInfo"></asp:label></td>
													</tr>
													<tr>
														<td align="center" colSpan="2"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<br>
									<table class="sInfoTable" id="tbOther" cellSpacing="0" cellPadding="2" width="400" runat="server">
										<tr class="sTableHighlight" id="trOther" runat="server">
											<td class="sSelectShipText"><asp:radiobutton id="rbOther" runat="server" GroupName="grLabel" Text="Use Other Shipping Method" AutoPostBack="True"></asp:radiobutton></td>
										</tr>
										<tr>
											<td vAlign="middle" align="center">
												<table cellSpacing="0" cellPadding="2" width="100%" border="0">
													<tr>
														<td class="sSelectShipText" noWrap width="1%"><font>Method:&nbsp;</font></td>
														<td><asp:dropdownlist id="ddlOther" runat="server" AutoPostBack="True" Width="100%" Enabled="False">
																<asp:ListItem Value="1">Hand Carry</asp:ListItem>
															</asp:dropdownlist></td>
													</tr>
													<tr>
														<td class="sSelectShipText" noWrap width="1%"><font>DM Approval:&nbsp;</font></td>
														<td><asp:textbox id="txtHandDMApproval" Runat="server" Width="100%" MaxLength="35" Enabled="false"></asp:textbox></td>
													</tr>
													<tr>
														<td align="center" colSpan="2"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<br>
									<asp:button id="btnSave" runat="server" Text="Save for Later" CssClass="sButton" BorderStyle="Solid"></asp:button>&nbsp;
									<asp:button id="btnSubmit" runat="server" Text="Submit" CssClass="sButton" BorderStyle="Solid"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</html>
