<%@ Control Language="vb" AutoEventWireup="false" Codebehind="TransferBox.ascx.vb" Inherits="SSTransfer.TransferBox" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False" %>
<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server"/>
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr bgColor="#dddddd">
		<td>
			<asp:datagrid id="dgTransferBox" CssClass="sDatagrid" runat="server" AutoGenerateColumns="False" EnableViewState="False" Width="100%" CellPadding="2" CellSpacing="1" AllowSorting="True">
				<AlternatingItemStyle cssclass="sDatagridAlternatingItem"></AlternatingItemStyle>
				<Columns>
					<asp:TemplateColumn Visible=false HeaderText="Remove">
						<HeaderStyle HorizontalAlign="Center" CssClass="sDatagridHeader"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:checkbox ID=chkDeleteItem Runat=server></asp:checkbox>
						</ItemTemplate>	
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="SKU_NUM" HeaderText="SKU">
						<HeaderStyle HorizontalAlign="Center" CssClass="sDatagridHeader"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VE_CD" HeaderText="Vendor">
						<HeaderStyle HorizontalAlign="Center" CssClass="sDatagridHeader"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="SIZE_CD" HeaderText="Size">
						<HeaderStyle HorizontalAlign="Center"  CssClass="sDatagridHeader"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="CURR" HeaderText="Retail" DataFormatString="{0:C}">
						<HeaderStyle HorizontalAlign="Center" CssClass="sDatagridHeader"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="DES1" HeaderText="Description">
						<HeaderStyle CssClass="sDatagridHeader"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Request_Qty" HeaderText="Qty. Req.">
						<HeaderStyle HorizontalAlign="Center" CssClass="sDatagridHeader"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:TemplateColumn headerText="Qty. Shipped">
						<HeaderStyle HorizontalAlign="Center" CssClass="sDatagridHeader"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:TextBox CssClass="sTransferBoxTextBox" ID="txtItemQty" text='<%# Container.DataItem("Item_Qty") %>' MaxLength="3" Runat="server" Width="40" EnableViewState="false">
							</asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="Item_Qty" HeaderText="Qty. Shipped">
						<HeaderStyle HorizontalAlign="Center" CssClass="sDatagridHeader"></HeaderStyle>
						<ItemStyle HorizontalAlign=Center></ItemStyle>
					</asp:BoundColumn>
					<asp:TemplateColumn headerText="Qty. Received">
						<HeaderStyle CssClass="sDatagridHeader" HorizontalAlign="Center"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:TextBox CssClass="sTransferBoxTextBox" ID="txtReceivedQty" text='<%# Container.DataItem("Received_Qty") %>' MaxLength="3" Runat="server" Width="40" EnableViewState="false">
							</asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="Received_Qty" HeaderText="Qty. Received">
						<HeaderStyle HorizontalAlign="Center" CssClass="sDatagridHeader"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:TemplateColumn>
						<HeaderStyle HorizontalAlign="Center" CssClass="sDatagridHeader"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:PlaceHolder Runat=server Visible='<%# Convert.ToBoolean(Container.DataItem("Cust_Id")) %>'>
								<img  style="cursor:hand" onclick="javascript:window.showModalDialog('CustomerInfo.aspx?CID=<%# Container.DataItem("Cust_Id") %>','CustInfo','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');" border=0 id="imgCust" src="images\customer.gif">
							</asp:PlaceHolder>
						</ItemTemplate>	
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="Xfer_Desc" HeaderText="Type">
						<HeaderStyle CssClass="sDatagridHeader"></HeaderStyle>
					</asp:BoundColumn>
				</Columns>
			</asp:datagrid></td>
	</tr>
</table>
