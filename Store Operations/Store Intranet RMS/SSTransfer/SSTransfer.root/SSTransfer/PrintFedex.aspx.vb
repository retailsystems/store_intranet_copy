Public Class PrintFedex
    Inherits System.Web.UI.Page
    Protected WithEvents btnDone As System.Web.UI.WebControls.Button
    Protected WithEvents ShippingLabel As System.Web.UI.WebControls.Image
    Protected WithEvents UPSFrame As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

    Private m_BoxId As String 'box id
    'Private m_DMApproval As String
    Private m_UPSServiceCode As String 'UPS service code according to the XML online shipping tools
    Private m_UPSPackageType As String 'UPS package type according to the XML online shipping tools
    Private m_CarrierCode As String
    Private m_TType As Int32 'return/store to store
    Private m_objUserInfo As New UserInfo   ' user object
    Private m_strApprovedBy As String
    Private m_objTransferLock As New TransferLock
    Private m_strLBS As String
    Private m_strRANum As String

    Private Const FedEx As Integer = 2

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Get user session/cookie info
        m_objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        'If employee is logged on to the system then their locks are kept alive
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
            m_objTransferLock.KeepLocksAlive(m_objUserInfo.EmpId)
        Else
            Response.Redirect("SessionEnd.aspx?Mode=1")
        End If

        'If store number not set then redirect to set store page
        If m_objUserInfo.StoreNumber = -1 Or m_objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

        'set header properties
        ucHeader.lblTitle = "Print Label"
		ucHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Transfer)
		ucHeader.CurrentPage(SSTransfer.Header.PageName.NewTransfer)
        ucHeader.DisableButtons()

        'get querystring url variables
        m_BoxId = Request.QueryString("BID") 'box id
        m_strApprovedBy = Request.QueryString("Approval")
        m_UPSPackageType = Request.QueryString("PAK") 'UPS XML Shipping package type
        m_UPSServiceCode = Request.QueryString("SER") 'UPS XML Shipping service type
        m_TType = Request.QueryString("TType") 'returns\store to store
        m_strLBS = Request.QueryString("LBS")
        m_strRANum = Request.QueryString("RA")

        'If page is not post back and box id exist
        If Not Page.IsPostBack And m_BoxId.Length > 0 Then

            'Generate label if it does not already exist
            If Not LabelExists() Then
                GenerateUPSLabel()
            Else
                LogUPSLabelError()
                Response.Redirect("SelectShipping.aspx?LBS=" & m_strLBS & "&TType=" & m_TType & "&UPSOn=0&Err=2&BID=" & m_BoxId)
            End If

        End If

        If m_TType = 3 Or m_TType = 4 Then
            btnDone.Text = "Done"
        End If

    End Sub

    'Save error to database
    Private Sub LogUPSLabelError()

        m_objUserInfo.GetEmployeeInfo()

        Dim objErrorRecord As New ApplicationErrors(ConfigurationSettings.AppSettings("strSQLConnection"), _
                                                                      ConfigurationSettings.AppSettings("strErrorDBTable"), _
                                                                      ConfigurationSettings.AppSettings("strSMTPServer"), _
                                                                      ConfigurationSettings.AppSettings("strEmailTo"), _
                                                                      ConfigurationSettings.AppSettings("strEmailFrom"), _
                                                                      ConfigurationSettings.AppSettings("strEmailHeader"))

        'Add Additional information for debugging
        objErrorRecord.AddRelevantInfo("Store: " & m_objUserInfo.StoreNumber.ToString)

        If m_objUserInfo.EmpOnline Then
            objErrorRecord.AddRelevantInfo("UserName: " & m_objUserInfo.EmpFullName.ToString)
            objErrorRecord.AddRelevantInfo("Transfer: " & m_BoxId.ToString)
        End If

        objErrorRecord.SaveError(False, True, "UPS Label Error.")

    End Sub

    'Checks to see if an tracking number is already assigned to the current box id
    Private Function LabelExists() As Boolean

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        'Stored procedure returns the total number of tracking number assigned that are not voided
        Dim objCommand As New SqlClient.SqlCommand("spGetTotalTrackingNumbers", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)
        Dim parTotal As SqlClient.SqlParameter = objCommand.Parameters.Add("@Total", SqlDbType.SmallInt)

        'Parameter direction
        parBoxId.Direction = ParameterDirection.Input
        parTotal.Direction = ParameterDirection.Output

        'Set parameters
        parBoxId.Value = m_BoxId

        objCommand.ExecuteNonQuery() 'execute stored procedure

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        'return total
        If parTotal.Value > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function GetFedexServiceCode(ByVal UPSServiceCode As String) As String
        Dim servicecode As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objCommand As New SqlClient.SqlCommand("spGetFedexServiceCode", objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.Add("@UPSServiceCode", UPSServiceCode)

        objConnection.Open()
        objDataReader = objCommand.ExecuteReader()
        objDataReader.Read()
        servicecode = objDataReader("Name")
        m_CarrierCode = objDataReader("Carrier_Code")
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return servicecode
    End Function

    Private Function GetFedexPackageType(ByVal UPSPackageType As String) As String
        Dim packagetype As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objCommand As New SqlClient.SqlCommand("spGetFedexPackageType", objConnection)

        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.Add("@UPSPackageType", UPSPackageType)

        objConnection.Open()
        packagetype = objCommand.ExecuteScalar.ToString()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return packagetype
    End Function


    'This function uses the UPS XML object to generate requests to the UPS servers
    Private Sub GenerateUPSLabel()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objUPS As New Fedex_XML_Shipping   'UPS Object
        Dim strDate As String

        objConnection.Open() 'open connection

        'Stored procedure gets the required information to generate a label
        Dim objCommand As New SqlClient.SqlCommand("spGetShippingInfo '" & m_BoxId & "' ", objConnection)

        objDataReader = objCommand.ExecuteReader

        objDataReader.Read()

        'Send shipping information to the UPS XML shipping object
        objUPS.BoxId = m_BoxId
        objUPS.ServiceCode = GetFedexServiceCode(m_UPSServiceCode)
        objUPS.PackageType = GetFedexPackageType(m_UPSPackageType)
        objUPS.CarrierCode = m_CarrierCode
        objUPS.ReceivingPhone = objDataReader("ReceivingPhone").ToString
        objUPS.ReceivingAddress1 = objDataReader("ReceivingAddress1").ToString
        objUPS.ReceivingAddress2 = objDataReader("ReceivingAddress2").ToString
        objUPS.ReceivingAddress3 = objDataReader("ReceivingAddress3").ToString
        objUPS.ReceivingCity = objDataReader("ReceivingCity").ToString
        objUPS.ReceivingCountry = objDataReader("ReceivingCountry").ToString
        objUPS.ReceivingName = objDataReader("ReceivingCompanyName").ToString & " #" & objDataReader("ReceivingStoreNumber")
        objUPS.ReceivingPostalCode = objDataReader("ReceivingPostalCode").ToString
        objUPS.ReceivingProvince = objDataReader("ReceivingState").ToString
        objUPS.SendingPhone = objDataReader("SendingPhone").ToString
        objUPS.SendingAddress1 = objDataReader("SendingAddress1").ToString
        objUPS.SendingAddress2 = objDataReader("SendingAddress2").ToString
        objUPS.SendingAddress3 = objDataReader("SendingAddress3").ToString
        objUPS.SendingCity = objDataReader("SendingCity").ToString
        objUPS.SendingProvince = objDataReader("SendingState").ToString
        objUPS.SendingCountry = objDataReader("SendingCountry").ToString

        If m_objUserInfo.StoreNumber = ConfigurationSettings.AppSettings("DDSStoreNum") Then
            objUPS.SendingName = objDataReader("SendingCompanyName").ToString
        Else
            objUPS.SendingName = objDataReader("SendingCompanyName").ToString & " #" & m_objUserInfo.StoreNumber
        End If

        objUPS.SendingPostalCode = objDataReader("SendingPostalCode").ToString
        objUPS.SendingProvince = objDataReader("SendingState").ToString
        objUPS.SendingShipperNumber = objDataReader("FedexMeterNumber").ToString
        objUPS.SendingAccountNumber = objDataReader("FedexAccountNumber").ToString
        objUPS.ImageDir = Server.MapPath("UPSLabels\")
        objUPS.TType = m_TType
        objUPS.PackageWeight = m_strLBS.ToString

        objDataReader.Close()
        objCommand.Dispose()

        'Send XML ShippingConfirm Request
        If Not objUPS.SendShippingConfirm() Then 'If request fails

            objConnection.Close()
            objConnection.Dispose()

            'Redirect user back to select shipping screen
            'Limit options depending on error severity
            If objUPS.ErrorSeverity = "Transient" Then
                Response.Redirect("SelectShipping.aspx?LBS=" & m_strLBS & "&TType=" & m_TType & "&UPSOn=1&Err=1&BID=" & m_BoxId)
            Else
                Response.Redirect("SelectShipping.aspx?LBS=" & m_strLBS & "&TType=" & m_TType & "&UPSOn=0&Err=2&BID=" & m_BoxId)
            End If

        Else 'If request is successfull

            'This stored procedure saves the response information to the DB
            objCommand.CommandText = "spInsertBoxTrackingInfo"
            objCommand.CommandType = CommandType.StoredProcedure

            Dim prmBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@BoxId", SqlDbType.VarChar, 14)
            Dim prmParcelService As SqlClient.SqlParameter = objCommand.Parameters.Add("@ParcelService", SqlDbType.SmallInt)
            Dim prmShipmentId As SqlClient.SqlParameter = objCommand.Parameters.Add("@ShipmentId", SqlDbType.VarChar, 18)
            Dim prmTrackingNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@TrackingNum", SqlDbType.VarChar, 30)
            Dim prmXMLShipping As SqlClient.SqlParameter = objCommand.Parameters.Add("@XMLShipping", SqlDbType.Bit)
            Dim prmUPSShipmentConfirmResponse As SqlClient.SqlParameter = objCommand.Parameters.Add("@UPS_ShipmentConfirm_Response", SqlDbType.Text)
            Dim prmUPSShipmentConfirmRequest As SqlClient.SqlParameter = objCommand.Parameters.Add("@UPS_ShipmentConfirm_Request", SqlDbType.Text)
            'Dim prmBoxStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@BoxStatus", SqlDbType.TinyInt)
            Dim prmCreated As SqlClient.SqlParameter = objCommand.Parameters.Add("@Created", SqlDbType.DateTime)
            Dim prmModified As SqlClient.SqlParameter = objCommand.Parameters.Add("@Modified", SqlDbType.DateTime)
            Dim prmTrackingInfoId As SqlClient.SqlParameter = objCommand.Parameters.Add("@TrackingInfo_Id", SqlDbType.Int)

            Dim prmApproved_By As SqlClient.SqlParameter = objCommand.Parameters.Add("@Approved_By", SqlDbType.VarChar, 35)
            Dim prmUPS_PackageType_Code As SqlClient.SqlParameter = objCommand.Parameters.Add("@UPS_PackageType_Code", SqlDbType.VarChar, 2)
            Dim prmUPS_Service_Code As SqlClient.SqlParameter = objCommand.Parameters.Add("@UPS_Service_Code", SqlDbType.VarChar, 2)

            prmBoxId.Direction = ParameterDirection.Input
            prmParcelService.Direction = ParameterDirection.Input
            prmShipmentId.Direction = ParameterDirection.Input
            prmTrackingNum.Direction = ParameterDirection.Input
            prmXMLShipping.Direction = ParameterDirection.Input
            'prmBoxStatus.Direction = ParameterDirection.Input
            prmCreated.Direction = ParameterDirection.Input
            prmModified.Direction = ParameterDirection.Input
            prmApproved_By.Direction = ParameterDirection.Input
            prmUPS_PackageType_Code.Direction = ParameterDirection.Input
            prmUPS_Service_Code.Direction = ParameterDirection.Input
            prmTrackingInfoId.Direction = ParameterDirection.Output
            prmUPSShipmentConfirmResponse.Direction = ParameterDirection.Input
            prmUPSShipmentConfirmRequest.Direction = ParameterDirection.Input

            prmBoxId.Value = m_BoxId
            prmTrackingNum.Value = DBNull.Value
            prmXMLShipping.Value = 1 'True
            prmParcelService.Value = FedEx
            prmShipmentId.Value = objUPS.ShipmentId

            prmUPS_PackageType_Code.Value = m_UPSPackageType
            prmUPS_Service_Code.Value = m_UPSServiceCode
            prmApproved_By.Value = m_strApprovedBy

            prmUPSShipmentConfirmResponse.Value = objUPS.ShipmentConfirm_Response
            prmUPSShipmentConfirmRequest.Value = objUPS.ShipmentConfirm_Request

            strDate = Now()

            prmCreated.Value = strDate
            prmModified.Value = strDate

            objCommand.ExecuteNonQuery()
            objCommand.Dispose()

            'After receiving a successful response, send an accept request to generate the label
            If Not objUPS.SendShippingAccept Then 'Failure

                objConnection.Close()
                objConnection.Dispose()

                'Redirect user to select another shipping method
                'limit options depending on the error severity
                If objUPS.ErrorSeverity = "Transient" Then
                    Response.Redirect("SelectShipping.aspx?LBS=" & m_strLBS & "&TType=" & m_TType & "&UPSOn=1&Err=1&BID=" & m_BoxId)
                Else
                    Response.Redirect("SelectShipping.aspx?LBS=" & m_strLBS & "&TType=" & m_TType & "&UPSOn=0&Err=2&BID=" & m_BoxId)
                End If

            Else 'Successful

                'Clear command object
                objCommand.Parameters.Clear()
                objCommand.CommandType = CommandType.Text

                'Save tracking number
                objCommand.CommandText = "spUpdateBoxTrackingInfo " & prmTrackingInfoId.Value & ",'" & m_BoxId & "','" & objUPS.TrackingNum & "'," & FedEx & ",'" & Replace(objUPS.ShipmentAccept_Request, "'", "''") & "','" & Replace(objUPS.ShipmentAccept_Response, "'", "''") & "','" & Replace(objUPS.TotalCharges_Currency, "'", "''") & "','" & Replace(objUPS.TotalCharges_Value, "'", "''") & "','" & Now() & "' "
                objCommand.ExecuteNonQuery()

                ChangeBoxStatus() 'Change box status to shipped

                If m_TType = 4 Then

                    Dim objDDSReturn As New DDSReturn(ConfigurationSettings.AppSettings("strDDSReturnConn"))

                    objDDSReturn.UpdateRAShipped(m_strRANum, m_objUserInfo.StoreNumber, m_BoxId, True, Now())

                End If

                UpdateBoxHistory(2) 'shipped

                UnlockTransfer() ' unlock box id

                'Display the UPS shipping label to the user
                UPSFrame.Attributes("src") = "UPSLabels\" & objUPS.ImageFileName

                objCommand.Dispose()

                'Output OMS/WMS Interface file
                Dim objTransfer As New Transfer(m_BoxId)
                Dim objStore As New Store(objTransfer.ReceivingStore)

                If ConfigurationSettings.AppSettings("RASNFlatFileOn") = "1" And m_TType = 2 Then
                    Dim objBizTalkInterface As New BizTalkInterface(Replace(ConfigurationSettings.AppSettings("WMS_RASNFlatFileDir"), "~DC~", objTransfer.ReceivingStore))
                    objBizTalkInterface.CreateFlatFile(m_BoxId) ' Create WMS Flat File
                ElseIf ConfigurationSettings.AppSettings("OMSFlatFileOn") = "1" And objStore.IsInternetStore() Then
                    Dim objBizTalkInterface As New BizTalkInterface(ConfigurationSettings.AppSettings("OMS_ASNFlatFileDir"))
                    objBizTalkInterface.CreateFlatFile(m_BoxId) ' Create WMS Flat File
                End If

            End If

        End If

        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Unlock locked transfers
    Private Sub UnlockTransfer()

        Dim objTransferLock As New TransferLock

        objTransferLock.UnlockTransfer(m_BoxId)

        objTransferLock = Nothing

    End Sub

    'Change box status to shipped for the current box
    Private Sub ChangeBoxStatus()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateBoxHdrStatus", objConnection)
        Dim objDataReader As SqlClient.SqlDataReader
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@BID", SqlDbType.VarChar, 14)
        Dim parBoxStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@Status", SqlDbType.TinyInt)
        Dim parModifiedDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@Modified", SqlDbType.DateTime)

        parBoxId.Direction = ParameterDirection.Input
        parBoxStatus.Direction = ParameterDirection.Input
        parModifiedDate.Direction = ParameterDirection.Input

        parBoxId.Value = m_BoxId
        parBoxStatus.Value = 2 'shipped
        parModifiedDate.Value = Now()

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Update box history
    Private Sub UpdateBoxHistory(ByVal intBoxStatus As Int32)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateBoxHistory '" & m_BoxId & "'," & intBoxStatus & ",'" & m_objUserInfo.EmpId & "','" & Replace(m_objUserInfo.EmpFullName, "'", "''") & "','" & Now() & "' ", objConnection)

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub btnDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDone.Click

        If m_TType = 3 Or m_TType = 4 Then
            Response.Redirect("CreateNewTrans.aspx")
        Else
            Response.Redirect("TransferProcessing.aspx?TType=" & m_TType & "&BID=" & m_BoxId)
        End If

    End Sub

End Class
