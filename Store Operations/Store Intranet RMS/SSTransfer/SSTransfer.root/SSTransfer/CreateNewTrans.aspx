<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CreateNewTrans.aspx.vb" Inherits="SSTransfer.CreateNewTrans" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Login</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
	</HEAD>
	<body class="sBody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="frmTransferHome" onsubmit="return checkSubmit();" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td align="middle">
						<table cellSpacing="2" cellPadding="3">
							<tr>
								<td><IMG src="images\box.gif"></td>
								<td>
									<table cellSpacing="0" cellPadding="0" border="0">
										<tr>
											<td class="sNewTransTitle">Please select a transfer type before proceeding.</td>
										</tr>
										<tr>
											<td><font>&nbsp;</font></td>
										</tr>
										<tr>
											<td class="sNewTransRadio"><asp:radiobutton id="radStore" tabIndex="3" runat="server" Checked="True" GroupName="grpTranType" Text="Store to Store"></asp:radiobutton><br>
												<asp:radiobutton id="radReturn" tabIndex="4" runat="server" GroupName="grpTranType" Text="Return to DC/HQ"></asp:radiobutton><br>
												<asp:radiobutton id="radMM" tabIndex="5" runat="server" GroupName="grpTranType" Text="HQ Mail Pack"></asp:radiobutton><br>
												<asp:radiobutton id="radDDS" tabIndex="5" runat="server" GroupName="grpTranType" Text="Supply Returns"></asp:radiobutton>
											</td>
										</tr>
										<tr>
											<td><font>&nbsp;</font></td>
										</tr>
										<tr>
											<td><asp:button id="btnCreateTrans" tabIndex="2" runat="server" Text="Create New Transfer" CssClass="sButton"></asp:button></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
