Public Class SelectTransfer
    Inherits System.Web.UI.Page
    Protected WithEvents lblExpectedArrivals As System.Web.UI.WebControls.Label
    Protected WithEvents grdArrivals As System.Web.UI.WebControls.DataGrid
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ucScanUPS As ScanUPS
    Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

    Private m_objUserInfo As New UserInfo()
    Private m_objTransferLock As New TransferLock()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Get employee data
        m_objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        'If employee is logged on to the system then their locks are kept alive
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
        Else
            Response.Redirect("SessionEnd.aspx?Mode=1")
        End If

        'If store number not set then redirect to set store page
        If m_objUserInfo.StoreNumber = -1 Or m_objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

        'Clear error popup message
        pageBody.Attributes.Add("onload", "")

        'Set Header title
        ucHeader.lblTitle = "Check In"
		ucHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Transfer) 'Set button group
		ucHeader.CurrentPage(SSTransfer.Header.PageName.CheckIn) 'Highlight checkIn button

        FillDataGrid(0) 'Fill Data Grid with expected arrivals

    End Sub

    'This function displays an popup error to the user when the scanups tracking num is invalid
    Private Sub ucScanUPS_InValidTrackNum(ByVal strError As String) Handles ucScanUPS.InValidTrackNum
        pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(strError) & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
    End Sub

    'This function fills the data grid
    Private Sub FillDataGrid(ByVal SortOrder As Byte)

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objArrivalDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        strStoredProcedure = "spGetArrivals " & m_objUserInfo.StoreNumber & "," & SortOrder 'Note sortorder has been removed

        Dim objACommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        objArrivalDataReader = objACommand.ExecuteReader(CommandBehavior.CloseConnection)

        grdArrivals.DataSource = objArrivalDataReader
        grdArrivals.DataBind()

        objArrivalDataReader.Close()
        objACommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Event raised when columns are sorted
    Sub SortCommand_Arrivals(ByVal Source As Object, ByVal E As DataGridSortCommandEventArgs)
        FillDataGrid(E.SortExpression) 'fill data grid
    End Sub

    'This function returns all the transfer types within a box
    Protected Function GetBoxTransferTypes(ByVal Box_Id As String) As String

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim strReturn As String

        objConnection.Open()

        strStoredProcedure = "spGetBoxTransferTypes '" & Box_Id & "' "

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Do While objDataReader.Read
            strReturn = strReturn & objDataReader("Xfer_Short_Desc") & ","
        Loop

        If Len(strReturn) > 0 Then
            strReturn = Mid(strReturn, 1, Len(strReturn) - 1)
        End If

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return strReturn

    End Function

    'This function returns a box id with the corresponding tracking number  
    Sub EventHandler(ByVal TrackingNum As String, ByVal Parcel As Int32) Handles ucScanUPS.OnReceivedTrackNum

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        strStoredProcedure = "spGetBoxFromTrackingNum"

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim TrackingParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@TrackingNum", SqlDbType.VarChar, 30)
        Dim ParcelParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@ParcelService_Cd", SqlDbType.SmallInt)
        Dim RStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Receiving_Store_Cd", SqlDbType.SmallInt)

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)
        Dim ErrParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Err", SqlDbType.Bit)
        Dim ErrMsgParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@ErrDesc", SqlDbType.VarChar, 100)

        'Set parameter direction
        BoxIdParam.Direction = ParameterDirection.Output
        ParcelParam.Direction = ParameterDirection.Input
        RStoreParam.Direction = ParameterDirection.Input
        TrackingParam.Direction = ParameterDirection.Input
        ErrParam.Direction = ParameterDirection.Output
        ErrMsgParam.Direction = ParameterDirection.Output

        'Set parameter values
        TrackingParam.Value = TrackingNum
        ParcelParam.Value = Parcel
        RStoreParam.Value = m_objUserInfo.StoreNumber

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        'If tracking number does not exist then display the err generated by the stored procedure
        If Not ErrParam.Value Then
            Response.Redirect("CheckInTransfer.aspx?BID=" & BoxIdParam.Value)
        Else
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(ErrMsgParam.Value) & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
        End If


    End Sub


End Class
