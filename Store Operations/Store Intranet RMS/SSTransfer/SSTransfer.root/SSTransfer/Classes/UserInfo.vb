Imports System.Web.Security
Imports System.Data
Imports System.Data.SqlClient

Public Class UserInfo

    Private m_intStoreNumber As Int32
    Private m_strEmployeeId As String
    Private m_strEmployeeFirstName As String
    Private m_strEmployeeLastName As String
    Private m_strEmployeeJobCode As String
    Private m_strEmpDept As String

    Public Sub New()
    End Sub

    Public Sub GetEmployeeInfo()

        m_intStoreNumber = GetStoreFromIP()

        If m_intStoreNumber = -1 And Not System.Web.HttpContext.Current.Request.Cookies("StoreNo") Is Nothing Then
            m_intStoreNumber = CInt(System.Web.HttpContext.Current.Request.Cookies("StoreNo").Value)
        End If

        'If Not System.Web.HttpContext.Current.Request.Cookies("StoreNo") Is Nothing Then
        'm_intStoreNumber = CInt(System.Web.HttpContext.Current.Request.Cookies("StoreNo").Value)
        'Else
        'm_intStoreNumber = GetStoreFromIP()
        'End If

        If Not System.Web.HttpContext.Current.Session Is Nothing Then
            m_strEmployeeId = System.Web.HttpContext.Current.Session("EmpId")
            m_strEmployeeFirstName = System.Web.HttpContext.Current.Session("EmpFirstName")
            m_strEmployeeLastName = System.Web.HttpContext.Current.Session("EmpLastName")
            m_strEmployeeJobCode = System.Web.HttpContext.Current.Session("EmpJobCode")
            m_strEmpDept = System.Web.HttpContext.Current.Session("EmpDept")
        End If

    End Sub

    Private Function IsEmpOnline() As Boolean

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spIsEmpOnline '" & m_strEmployeeId & "' "

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        Return objDataReader.Read

        objCommand.Dispose()
        objDataReader.Close()

    End Function

    Public Function IsFullTimeAssistantManager() As Boolean

        If m_strEmployeeJobCode.ToString.Trim.ToUpper = "FTASM" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "FTASMCA" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "FTPADE" Then

            Return True
        Else
            Return False
        End If

    End Function

    Public Function IsPartTimeAssistantManager() As Boolean

        If m_strEmployeeJobCode.ToString.Trim.ToUpper = "PTASM" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "PTASMCA" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "PTPADE" Then

            Return True
        Else
            Return False
        End If

    End Function

    Public Function IsDistrictManager() As Boolean

        If m_strEmployeeJobCode.ToString.Trim.ToUpper = "DM" Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function IsKeyholder() As Boolean

        If m_strEmployeeJobCode.ToString.Trim.ToUpper = "PADEKEY" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "CAKEY" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "KEY" Then

            Return True
        Else
            Return False
        End If

    End Function

    Public Function IsAssociate() As Boolean

        If m_strEmployeeJobCode.ToString.Trim.ToUpper = "SALCA" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "SALES" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "SALPADE" Then

            Return True
        Else
            Return False
        End If

    End Function

    Public Function IsLead() As Boolean

        If m_strEmployeeJobCode.ToString.Trim.ToUpper = "PADELEAD" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "CALEAD" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "LEAD" Then

            Return True
        Else
            Return False
        End If

    End Function

    Public Function IsStoreManager() As Boolean

        If m_strEmployeeJobCode.ToString.Trim.ToUpper = "SMCA" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "SM" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "TSM" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "SMPADE" Or _
             m_strEmployeeJobCode.ToString.Trim.ToUpper = "REG" Then

            Return True
        Else
            Return False
        End If

    End Function

    'Function generates subnet from storeip then uses subnet to look up store number
    Private Function GetStoreFromIP() As Integer

        Dim StoreIP() As String
        Dim StoreSubnetMask() As String
        Dim strStoreSubnet As String
        Dim strTemp As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        StoreIP = Split(System.Web.HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), ".") 'Split("127.0.0.0", ".") 
        StoreSubnetMask = Split(ConfigurationSettings.AppSettings("strStoreSubnetMask"), ".")

        strStoreSubnet = StoreIP(0) And StoreSubnetMask(0)
        strStoreSubnet = strStoreSubnet & "."
        strStoreSubnet = strStoreSubnet & (StoreIP(1) And StoreSubnetMask(1))
        strStoreSubnet = strStoreSubnet & "."
        strStoreSubnet = strStoreSubnet & (StoreIP(2) And StoreSubnetMask(2))
        strStoreSubnet = strStoreSubnet & "."
        strStoreSubnet = strStoreSubnet & (StoreIP(3) And StoreSubnetMask(3))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetStoreFromSubnet", objConnection)

        objCommand.CommandType = CommandType.StoredProcedure

        Dim parStoreSubnet As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreSubnet", SqlDbType.VarChar, 15)
        Dim parStoreNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt)

        parStoreSubnet.Direction = ParameterDirection.Input
        parStoreNum.Direction = ParameterDirection.Output

        parStoreSubnet.Value = strStoreSubnet

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return parStoreNum.Value

    End Function

    Public Sub LogOff()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spDeleteEmployeeLogin '" & m_strEmployeeId.ToString.PadLeft(6, "0"c) & "'", objConnection)

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        ClearEmployeeSession()

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", m_strEmployeeId)

    End Sub

    Public Sub KeepLogonAlive()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateEmployeeLogin '" & m_strEmployeeId.ToString.PadLeft(6, "0"c) & "','" & Replace(System.Web.HttpContext.Current.Request.ServerVariables("URL").ToString, "'", "''") & "','" & Now() & "'", objConnection)

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Dispose()
        objConnection.Close()
        objConnection.Dispose()


    End Sub

    Public Function LogOn() As Boolean

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spInsertEmployeeLogin", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim prmEmpId As SqlClient.SqlParameter = objCommand.Parameters.Add("@EmpId", SqlDbType.VarChar, 6) 'Made the change to insert 6 digits 3/20/2018
        Dim prmStoreNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt)
        Dim prmCurrentPage As SqlClient.SqlParameter = objCommand.Parameters.Add("@CurrentPage", SqlDbType.VarChar, 255)
        Dim prmCreatedDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@Created_Date", SqlDbType.DateTime)
        Dim prmSuccess As SqlClient.SqlParameter = objCommand.Parameters.Add("@Success", SqlDbType.Bit)

        prmEmpId.Direction = ParameterDirection.Input
        prmStoreNum.Direction = ParameterDirection.Input
        prmCurrentPage.Direction = ParameterDirection.Input
        prmCreatedDate.Direction = ParameterDirection.Input
        prmSuccess.Direction = ParameterDirection.Output

        prmEmpId.Value = m_strEmployeeId.ToString.PadLeft(6, "0"c)
        prmStoreNum.Value = m_intStoreNumber
        prmCurrentPage.Value = System.Web.HttpContext.Current.Request.ServerVariables("URL").ToString
        prmCreatedDate.Value = Now()

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return prmSuccess.Value

    End Function

    Public Sub SetEmployeeInfo(ByVal InEmpId As Int32, ByVal InEmpFirstName As String, ByVal InEmpLastName As String, ByVal InJobCode As String, ByVal InDept As String)

        m_strEmployeeId = InEmpId
        m_strEmployeeFirstName = InEmpFirstName
        m_strEmployeeLastName = InEmpLastName
        m_strEmployeeJobCode = InJobCode
        m_strEmpDept = InDept

        System.Web.HttpContext.Current.Session("EmpId") = InEmpId
        System.Web.HttpContext.Current.Session("EmpFirstName") = InEmpFirstName
        System.Web.HttpContext.Current.Session("EmpLastName") = InEmpLastName
        System.Web.HttpContext.Current.Session("EmpJobCode") = InJobCode
        System.Web.HttpContext.Current.Session("EmpDept") = InDept

    End Sub

    Public Sub ClearStoreCookie()

        System.Web.HttpContext.Current.Request.Cookies("StoreNo").Expires = "#1/1/1900#"
        'System.Web.HttpContext.Current.Request.Cookies("").Expires = "AD"

    End Sub

    Public Sub ClearEmployeeSession()

        System.Web.HttpContext.Current.Session("EmpId") = Nothing
        System.Web.HttpContext.Current.Session("EmpFirstName") = Nothing
        System.Web.HttpContext.Current.Session("EmpLastName") = Nothing
        System.Web.HttpContext.Current.Session("EmpJobCode") = Nothing
        System.Web.HttpContext.Current.Session("EmpDept") = Nothing
        FormsAuthentication.SignOut()

    End Sub

    ReadOnly Property EmpOnline()
        Get
            If Len(m_strEmployeeId) > 0 Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    ReadOnly Property EmpDept()
        Get
            Return Trim(m_strEmpDept)
        End Get
    End Property

    ReadOnly Property EmpFirstName()
        Get
            Return Trim(m_strEmployeeFirstName)
        End Get
    End Property

    ReadOnly Property EmpJobCode()
        Get
            Return Trim(m_strEmployeeJobCode)
        End Get
    End Property

    ReadOnly Property EmpFullName()
        Get
            Return Trim(m_strEmployeeFirstName) & " " & Trim(m_strEmployeeLastName)
        End Get
    End Property

    ReadOnly Property EmpLastName()
        Get
            Return Trim(m_strEmployeeLastName)
        End Get
    End Property

    ReadOnly Property EmpId()
        Get
            Return m_strEmployeeId
        End Get
    End Property

    ReadOnly Property StoreNumber()
        Get
            Return m_intStoreNumber
        End Get
    End Property

End Class
