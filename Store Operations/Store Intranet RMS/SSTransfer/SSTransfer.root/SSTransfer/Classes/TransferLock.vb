Imports System.Data
Imports System.Data.SqlClient

Public Class TransferLock

    Public Sub New()

    End Sub

    'Use function to lock transfer from other users
    'Box_Id - Transfer # to lock
    'Emp_ID - Employee Id of user trying to obtain lock
    'Session_Id = Session Id of user trying to obtain lock
    Public Function LockTransfer(ByVal Box_Id As String, ByVal Emp_Id As String, ByVal Session_Id As String) As Boolean

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spLockBox", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)
        Dim parEmpId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Emp_id", SqlDbType.VarChar, 6)
        Dim parSessionId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Session_Id", SqlDbType.VarChar, 24)
        Dim parCreatedDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@Created_Date", SqlDbType.DateTime)
        Dim parSuccess As SqlClient.SqlParameter = objCommand.Parameters.Add("@Success", SqlDbType.Bit)

        parBoxId.Direction = ParameterDirection.Input
        parEmpId.Direction = ParameterDirection.Input
        parSessionId.Direction = ParameterDirection.Input
        parCreatedDate.Direction = ParameterDirection.Input
        parSuccess.Direction = ParameterDirection.Output

        parBoxId.Value = Box_Id
        parEmpId.Value = Emp_Id.ToString.PadLeft(6, "0"c)
        parSessionId.Value = Session_Id
        parCreatedDate.Value = Now()

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return parSuccess.Value

    End Function

    'Updates timestamp of when lock was obtain. This is done to keep lock alive for the length of the session. Lock
    'is cleared if the time of last update is greater than the session duration. SQL job handles lock deletion
    Public Sub KeepLocksAlive(ByVal Emp_Id As String)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spKeepLocksAlive", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parEmpId As SqlClient.SqlParameter = objCommand.Parameters.Add("@EmpId", SqlDbType.VarChar, 5)
        Dim parCreated_Date As SqlClient.SqlParameter = objCommand.Parameters.Add("@Created_Date", SqlDbType.DateTime)

        parEmpId.Direction = ParameterDirection.Input
        parCreated_Date.Direction = ParameterDirection.Input

        parEmpId.Value = Emp_Id.ToString.PadLeft(6, "0"c)
        parCreated_Date.Value = Now()

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Release lock
    Public Sub UnlockTransfer(Optional ByVal Box_Id As String = "", Optional ByVal Emp_Id As String = "", Optional ByVal Session_Id As String = "")

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUnlockBox", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)
        Dim parEmpId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Emp_Id", SqlDbType.VarChar, 5)
        Dim parSessionId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Session_Id", SqlDbType.VarChar, 24)

        parBoxId.Direction = ParameterDirection.Input
        parEmpId.Direction = ParameterDirection.Input
        parSessionId.Direction = ParameterDirection.Input

        parBoxId.Value = Box_Id
        parEmpId.Value = Emp_Id.ToString.PadLeft(6, "0"c)
        parSessionId.Value = Session_Id

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

End Class
