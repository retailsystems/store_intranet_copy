Public Class Transfer

    Private strBoxId As String
    Private intSendingStore As Integer
    Private intCurrentBoxStatus As Integer
    Private strEmpId As String
    Private strEmpFullName As String
    Private dteShipmentDate As Date
    Private intReceivingStore As Integer
    Private intTType As Integer
    Private strTrackingNum As String
    Private intParcelService As Integer
    Private dteReceivedDate As Date
    Private strNotes As String
    Private dteCreatedDate As Date
    Private dteModifiedDate As Date
    Private strEmailSent As String
    Private dteEmailSentDate As Date
    Private booGERSStatus As Boolean
    Private dteGERSStatusDate As Date
    Private strDocNum As String

    Public ReadOnly Property Notes()
        Get
            Return strNotes
        End Get
    End Property
   
    Public ReadOnly Property Trackingnum()
        Get
            Return strTrackingNum
        End Get
    End Property
    Public ReadOnly Property CurrentBoxStatus()
        Get
            Return intCurrentBoxStatus
        End Get
    End Property
    Public ReadOnly Property EmpId()
        Get
            Return strEmpId
        End Get
    End Property
    Public ReadOnly Property EmpFullName()
        Get
            Return strEmpFullName
        End Get
    End Property
    Public ReadOnly Property ReceivingStore()
        Get
            Return intReceivingStore
        End Get
    End Property
    Public ReadOnly Property SendingStore()
        Get
            Return intSendingStore
        End Get
    End Property
    Public ReadOnly Property ShipmentDate()
        Get
            Return dteShipmentDate
        End Get
    End Property

    Public Sub New(ByVal BoxId As String)
        strBoxId = BoxId
        GetData()
    End Sub

    Public Sub New()
    End Sub

    Private Sub GetData()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeader", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = strBoxId

        objDataReader = objCommand.ExecuteReader()

        If objDataReader.Read() Then

            If Not IsDBNull(objDataReader("Notes")) Then strNotes = objDataReader("Notes")
            If Not IsDBNull(objDataReader("Tracking_Num")) Then strTrackingNum = objDataReader("Tracking_Num")
            If Not IsDBNull(objDataReader("Current_Box_Status_Cd")) Then intCurrentBoxStatus = objDataReader("Current_Box_Status_Cd")
            If Not IsDBNull(objDataReader("Emp_Id")) Then strEmpId = objDataReader("Emp_Id")
            If Not IsDBNull(objDataReader("Emp_FullName")) Then strEmpFullName = objDataReader("Emp_FullName")
            If Not IsDBNull(objDataReader("RStoreNum")) Then intReceivingStore = objDataReader("RStoreNum")
            If Not IsDBNull(objDataReader("SStoreNum")) Then intSendingStore = objDataReader("SStoreNum")
            If Not IsDBNull(objDataReader("Shipment_Date")) Then dteShipmentDate = objDataReader("Shipment_Date")

        End If

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Checks to see if store is restricted from sending or receiving store
    Public Function IsTransferRestricted(ByVal Sender As Int16, ByVal Recevier As Int16, ByVal TType As Integer) As Boolean

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spIsTransferRestricted", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim RStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@RStore", SqlDbType.SmallInt)
        Dim SStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@SStore", SqlDbType.SmallInt)
        Dim TTypeParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@TType", SqlDbType.SmallInt)
        Dim IsRestrictedParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@IsRestricted", SqlDbType.Bit)

        RStoreParam.Direction = ParameterDirection.Input
        SStoreParam.Direction = ParameterDirection.Input
        TTypeParam.Direction = ParameterDirection.Input
        IsRestrictedParam.Direction = ParameterDirection.Output

        RStoreParam.Value = Recevier
        SStoreParam.Value = Sender
        TTypeParam.Value = TType
    
        objCommand.ExecuteNonQuery()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return IsRestrictedParam.Value

    End Function


End Class
