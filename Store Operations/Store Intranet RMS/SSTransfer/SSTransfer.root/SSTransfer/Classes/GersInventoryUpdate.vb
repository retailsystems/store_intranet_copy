Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System
Imports System.Web
Imports System.Web.Services.Protocols
Imports System.Web.Services

Public Class GersInventoryUpdate

    Public strBoxId As String
    Public strTrackingNum As String
    Public strBoxStatusDesc As String
    Public strSendingStoreNum As String
    Public strSendingStoreName As String
    Public strDBConnection As String
    Public strReceivingStoreNum As String
    Public strReceivingStoreName As String
    Public dteShipDate As Date
    Public dteReceiveDate As Date
    Public strEmpId As String

    Public strSkuNum As String
    Public strQty As Integer
    Public strLnNum As Integer
    Public strRetPrc As Decimal
    Public strShipNum As String
    Public strXferType As String
    Public strDefaultLocation As String = "RECV"

    Dim connHeaderSQL As System.Data.SqlClient.SqlConnection
    Dim connHeaderGERS As System.Data.OleDb.OleDbConnection

    Dim blnGERSUpdate As Boolean

    Dim sysdate As Date


    Public Sub GetBoxHeader(ByVal BoxId As String, ByVal MOS As Boolean)
        ' data for the box header
        sysdate = Today

        ConnSQLOpen()

        'Dim objConnection As New SqlClient.SqlConnection(strDBConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeaderDT", connHeaderSQL)
        objCommand.CommandType = CommandType.StoredProcedure
        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = BoxId

        objDataReader = objCommand.ExecuteReader()

        'If objDataReader.HasRows Then
        If objDataReader.Read() Then
            strBoxStatusDesc = objDataReader("Status_Desc").ToString
            strSendingStoreNum = objDataReader("SStoreNum").ToString
            strReceivingStoreNum = objDataReader("RStoreNum").ToString
            strBoxId = objDataReader("Box_Id").ToString
            dteShipDate = objDataReader("Shipment_Date").ToString
            dteReceiveDate = objDataReader("Received_Date").ToString
            strEmpId = objDataReader("Emp_Id").ToString

            make_store_number()

            If strBoxStatusDesc = "Received" Then
                ConnSQLClose()
                GetBoxItems(BoxId)
                If MOS Then
                    GetBoxItemsMOS(BoxId)
                End If
            End If

        End If

        objDataReader.Close()
        objCommand.Dispose()


    End Sub
    Public Sub GetBoxItems(ByVal BoxId As String)
        ' data for the box items
        ConnSQLOpen()
        Dim rows As Integer = 0
        Dim I As Integer = 0
        Dim D As Integer = 0
        Dim A As Integer = 0
        Dim result As String


        Dim objDataReader1 As SqlClient.SqlDataReader

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxItemsType", connHeaderSQL)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = BoxId

        objDataReader1 = objCommand.ExecuteReader()

        '# Call ASNout Oracle web services here - 09/10/2012 by LJ
        rows = GetBoxItemCount(BoxId)
        Dim desc As RMSProcess.ASNOutDesc = New RMSProcess.ASNOutDesc
        If objDataReader1.HasRows And rows > 0 Then
            '# need to check the value is hand carry
            '# strTrackingNum = GetTrackingNum(BoxId)
            desc.asn_nbr = BoxId
            desc.container_qty = 1
            desc.container_qtySpecified = True

            desc.trailer_nbr = "15"
            desc.carrier_code = "38"
            desc.shipment_date = DateTime.Parse(DateTime.Today)
            desc.shipment_dateSpecified = True
            desc.est_arr_date = DateTime.Parse(DateTime.Today.AddDays(5))
            desc.est_arr_dateSpecified = True
            desc.schedule_nbrSpecified = True

            desc.to_location = strReceivingStoreNum
            desc.from_location = strSendingStoreNum

            desc.bol_nbr = BoxId
            desc.ASNOutDistro = desc.ASNOutDistro.CreateInstance(GetType(RMSProcess.ASNOutDistro), 1)

            Dim distro As RMSProcess.ASNOutDistro = New RMSProcess.ASNOutDistro
            distro.distro_nbr = strBoxId.Substring(0, 4) + strBoxId.Substring(6, 6)
            distro.distro_doc_type = "T"
            distro.customer_order_nbr = ""
            distro.consumer_direct = ""
            desc.ASNOutDistro(0) = distro

            distro.ASNOutCtn = distro.ASNOutCtn.CreateInstance(GetType(RMSProcess.ASNOutCtn), 1)
            Dim outCtn As RMSProcess.ASNOutCtn = New RMSProcess.ASNOutCtn

            outCtn.container_id = "4324"
            outCtn.weight = Double.Parse(30)
            outCtn.weight_uom = "LBS"
            outCtn.weightSpecified = True
            outCtn.final_location = strReceivingStoreNum

            outCtn.ASNOutItem = outCtn.ASNOutItem.CreateInstance(GetType(RMSProcess.ASNOutItem), rows)
            distro.ASNOutCtn(0) = outCtn

            Dim outItem As RMSProcess.ASNOutItem
            While objDataReader1.Read

                'If (objDataReader1("Received_Qty").ToString > 0) Then

                outItem = New RMSProcess.ASNOutItem

                strSkuNum = objDataReader1("Sku_Num").ToString.Trim
                strQty = objDataReader1("Received_Qty").ToString

                outItem.item_id = strSkuNum
                outItem.unit_qty = Double.Parse(strQty)

                outItem.comments = strEmpId

                outCtn.ASNOutItem(I) = outItem

                I = I + 1
                'End If

            End While

            If (Not desc Is Nothing) Then
                result = SendTransShipmentToRMS(desc)
            End If
        End If

        objDataReader1.Close()
        objCommand.Dispose()
        ConnSQLClose()






    End Sub
    Private Function SendTransShipmentToRMS(ByRef DESC As RMSProcess.ASNOutDesc)
        Dim message As String
        Dim response As RMSProcess.publishASNOutCreateUsingASNOutDescResponse = New RMSProcess.publishASNOutCreateUsingASNOutDescResponse
        Dim STSService As RMSProcess.ASNOutPublishingService = New RMSProcess.ASNOutPublishingService

        STSService.Timeout = ConfigurationSettings.AppSettings("SSTransfer.RMSProcess.Timeout") '2000

        Dim a(0) As RMSProcess.RoutingInfosRoutingInfo
        a(0) = New RMSProcess.RoutingInfosRoutingInfo
        Dim b(0) As RMSProcess.RoutingInfosRoutingInfoDetail
        b(0) = New RMSProcess.RoutingInfosRoutingInfoDetail
        b(0).dtl_name = "consumer_direct"
        b(0).dtl_value = "y"
        a(0).detail = b
        a(0).name = "consumer_direct"
        a(0).value = "y"
        Dim c As RMSProcess.RoutingInfos = New RMSProcess.RoutingInfos

        c.routingInfo = a
        STSService.RoutingInfosValue = c

        Dim des As RMSProcess.publishASNOutCreateUsingASNOutDesc = New RMSProcess.publishASNOutCreateUsingASNOutDesc
        des.ASNOutDesc = DESC


        response = STSService.publishASNOutCreateUsingASNOutDesc(des)

        If (Not response.ServiceOpStatus Is Nothing) Then

            If (Not response.ServiceOpStatus.FailStatus Is Nothing) Then
                message += response.ServiceOpStatus.FailStatus.shortErrorMessage
            ElseIf (Not response.ServiceOpStatus.SuccessStatus Is Nothing) Then
                message += "success"
            End If
        Else
            message = "Service Status Response is Missing."
        End If
        STSService.Dispose()

        Return message

    End Function


    Private Function GetTrackingNum(ByVal BoxId As String)
        Dim strTrackNum As String
        ' Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        ConnSQLOpen()
        Dim objDataReader As SqlClient.SqlDataReader

        Dim objCommand As New SqlClient.SqlCommand("spGetTrackingNumByBoxId", connHeaderSQL)
        objCommand.CommandType = CommandType.StoredProcedure
        Dim parListId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)
        parListId.Direction = ParameterDirection.Input
        parListId.Value = BoxId
        strTrackNum = objCommand.ExecuteScalar()

        objCommand.Dispose()
        ConnSQLClose()

        Return strTrackNum
    End Function
    Private Function GetBoxItemCount(ByVal BoxId As String)
        Dim rowCount As Integer
        ' Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        ConnSQLOpen()
        Dim objDataReader As SqlClient.SqlDataReader

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxListItemCount", connHeaderSQL)
        objCommand.CommandType = CommandType.StoredProcedure
        Dim parListId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)
        parListId.Direction = ParameterDirection.Input
        parListId.Value = BoxId
        rowCount = Integer.Parse(objCommand.ExecuteScalar())


        objCommand.Dispose()
        ConnSQLClose()

        Return rowCount
    End Function
    Public Sub GetBoxItemsMOS(ByVal BoxId As String)
        ' data for the box items
        ConnSQLOpen()

        Dim objDataReader As SqlClient.SqlDataReader

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxItemsTypeMOS", connHeaderSQL)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = BoxId

        objDataReader = objCommand.ExecuteReader()

        If objDataReader.Read() Then
            strSkuNum = objDataReader("Sku_Num").ToString
            strQty = objDataReader("Received_Qty").ToString
            MOS_Procedure()
            Do While objDataReader.Read
                strSkuNum = objDataReader("Sku_Num").ToString
                strQty = objDataReader("Received_Qty").ToString
                MOS_Procedure()
            Loop

        End If
        objDataReader.Close()
        objCommand.Dispose()

        ConnSQLClose()

    End Sub

    Public Sub Store_MOS(ByVal str_sku_num, ByVal str_store_cd, ByVal str_qty)

        strSkuNum = str_sku_num
        strQty = str_qty
        strSendingStoreNum = str_store_cd

        make_store_number()

        MOS_Procedure()

    End Sub



    Public Sub MOS_Procedure()
        'for xfer_type 11, mark merchandise out of stock
        'ConnGERSOpen()

        Dim strSQL As String

        'if the store is doing MOS, there is no box id, so put in a fake

        If strBoxId = Nothing Then
            strBoxId = "0000-9999999"
        End If

        sysdate = Today

        strSQL = " INSERT INTO TEMP_MOS "
        strSQL = strSQL & " (trn_dt, box_id, store_cd, sku_num, loc_cd, qty, processed )"
        strSQL = strSQL & " VALUES (to_date('" & sysdate & "', 'MM/DD/YYYY'),  "
        strSQL = strSQL & " '" & strBoxId & "',  "
        strSQL = strSQL & " '" & strSendingStoreNum & "', '" & strSkuNum & "', '" & strDefaultLocation & "' , '" & strQty & "', 'N') "

        Dim cmdOraDD As System.Data.OleDb.OleDbCommand
        cmdOraDD = New OleDbCommand(strSQL, connHeaderGERS)
        cmdOraDD.CommandType = CommandType.Text
        cmdOraDD.ExecuteNonQuery()


        ConnGERSClose()

    End Sub

    '# comments it out for ORMS change 09/10/2012 -LJ
    'Public Sub GetShipmentNumber()
    '    'the sequence generator produces the doc_num
    '    ConnGERSOpen()

    '    Dim strSQL As String
    '    Dim cmdOraDD As System.Data.OleDb.OleDbCommand
    '    strSQL = " SELECT SHIP_SEQ_NUM.NEXTVAL as ShipNum FROM SYS.DUAL "

    '    cmdOraDD = New OleDbCommand(strSQL, connHeaderGERS)
    '    cmdOraDD.CommandType = CommandType.Text
    '    cmdOraDD.ExecuteNonQuery()

    '    Dim drOraDD As System.Data.OleDb.OleDbDataReader
    '    drOraDD = cmdOraDD.ExecuteReader

    '    If drOraDD.Read Then
    '        strShipNum = drOraDD("ShipNum")
    '    End If

    '    ConnGERSClose()

    'End Sub



    'Public Sub Populate_POS_INV_TRN()
    '    'insert data into the holding TEMP_POS_INV_TRN table in GERS

    '    ConnGERSOpen()

    '    Dim strSQL As String

    '    strSQL = " INSERT INTO TEMP_POS_INV_TRN "
    '    strSQL = strSQL & " ( trn_dt, trn_time, box_id,  "
    '    strSQL = strSQL & " shipment_date, "
    '    strSQL = strSQL & " received_date, "
    '    strSQL = strSQL & " store_cd, term_num, trn_num, "
    '    strSQL = strSQL & " doc_num, trn_tp, po_num, cshr_num, sess_num, other_store_cd, "
    '    strSQL = strSQL & " rcv_pcs, reason_cd, ship_tp, stat_cd, processed) "
    '    strSQL = strSQL & " VALUES (to_date('" & sysdate & "', 'MM/DD/YYYY'),  "
    '    strSQL = strSQL & " TO_CHAR(sysdate, 'SSSSS'), "
    '    strSQL = strSQL & " '" & strBoxId & "',  "
    '    strSQL = strSQL & " to_date('" & dteShipDate & "', 'MM/DD/YYYY'), "
    '    strSQL = strSQL & " to_date('" & dteReceiveDate & "', 'MM/DD/YYYY'), "
    '    strSQL = strSQL & " '" & strSendingStoreNum & "', '99', NULL, '" & strShipNum & "', "
    '    strSQL = strSQL & " 'TFO', NULL, '" & strEmpId & "', 1, "
    '    strSQL = strSQL & " '" & strReceivingStoreNum & "', 0, NULL, 'XXX', 'N', 'N') "

    '    Dim cmdOraDD As System.Data.OleDb.OleDbCommand
    '    cmdOraDD = New OleDbCommand(strSQL, connHeaderGERS)
    '    cmdOraDD.CommandType = CommandType.Text
    '    cmdOraDD.ExecuteNonQuery()

    '    ConnGERSClose()

    'End Sub

    'Public Sub Populate_POS_INV_TRN_End()
    '    'end record to place a PTST entry into the BQ table

    '    ConnGERSOpen()

    '    Dim strSQL As String
    '    strSQL = " INSERT INTO TEMP_POS_INV_TRN "
    '    strSQL = strSQL & " ( trn_dt, trn_time, box_id,  "
    '    strSQL = strSQL & " shipment_date, "
    '    strSQL = strSQL & " received_date, "
    '    strSQL = strSQL & " store_cd, term_num, trn_num, "
    '    strSQL = strSQL & " doc_num, trn_tp, po_num, cshr_num, sess_num, other_store_cd, "
    '    strSQL = strSQL & " rcv_pcs, reason_cd, ship_tp, stat_cd, processed) "
    '    strSQL = strSQL & " VALUES (to_date('" & sysdate & "', 'MM/DD/YYYY'),  "
    '    strSQL = strSQL & " 0, "
    '    strSQL = strSQL & " '" & strBoxId & "',  "
    '    strSQL = strSQL & " to_date('" & dteShipDate & "', 'MM/DD/YYYY'), "
    '    strSQL = strSQL & " to_date('" & dteReceiveDate & "', 'MM/DD/YYYY'), "
    '    strSQL = strSQL & " '" & strSendingStoreNum & "', '99', NULL, '" & strShipNum & "', "
    '    strSQL = strSQL & " 'TFO', NULL, '" & strEmpId & "', 1, "
    '    strSQL = strSQL & " '" & strReceivingStoreNum & "', 0, NULL, 'XXX', 'N', 'N') "


    '    Dim cmdOraDD As System.Data.OleDb.OleDbCommand
    '    cmdOraDD = New OleDbCommand(strSQL, connHeaderGERS)
    '    cmdOraDD.CommandType = CommandType.Text
    '    cmdOraDD.ExecuteNonQuery()

    '    ConnGERSClose()

    'End Sub




    'Public Sub Populate_POS_INV_TRN_LN()
    '    'insert data into the holding TEMP_POS_INV_TRN table in GERS

    '    ConnGERSOpen()

    '    Dim strSQL As String

    '    strSQL = " INSERT INTO TEMP_POS_INV_TRN_LN ( trn_dt, trn_time, box_id, "
    '    strSQL = strSQL & " store_cd, term_num, trn_num, "
    '    strSQL = strSQL & " doc_num, ln_num, sku_num, qty, ret, stat_cd, processed) "
    '    strSQL = strSQL & " VALUES (to_date('" & sysdate & "', 'MM/DD/YYYY'), "
    '    strSQL = strSQL & " TO_CHAR(sysdate, 'SSSSS'), '" & strBoxId & "', "
    '    strSQL = strSQL & " '" & strSendingStoreNum & "', "
    '    strSQL = strSQL & " '99', NULL, '" & strShipNum & "', '" & strLnNum & "', '" & strSkuNum & "', "
    '    strSQL = strSQL & " '" & strQty & "', '" & strRetPrc & "', 'N', 'N') "

    '    Dim cmdOraDD As System.Data.OleDb.OleDbCommand
    '    cmdOraDD = New OleDbCommand(strSQL, connHeaderGERS)
    '    cmdOraDD.CommandType = CommandType.Text
    '    cmdOraDD.ExecuteNonQuery()

    '    ConnGERSClose()

    'End Sub

    'Public Function FlagGERSMovement() As Boolean

    '    Dim tmpReturn As Boolean = False

    '    ConnGERSOpen()

    '    Dim strSQL As String
    '    Dim cmdOraDD As System.Data.OleDb.OleDbCommand
    '    strSQL = "select t.doc_num from temp_pos_inv_trn t, pos_inv_trn p "
    '    strSQL = strSQL & "where(t.doc_num = p.doc_num) "
    '    strSQL = strSQL & "and t.box_id = '" & Replace(strBoxId, "'", "''") & "' "
    '    strSQL = strSQL & "and t.processed = 'Y' "

    '    cmdOraDD = New OleDbCommand(strSQL, connHeaderGERS)
    '    cmdOraDD.CommandType = CommandType.Text
    '    cmdOraDD.ExecuteNonQuery()

    '    Dim drOraDD As System.Data.OleDb.OleDbDataReader
    '    drOraDD = cmdOraDD.ExecuteReader

    '    If drOraDD.Read Then
    '        UpdateTHGERSStatus(drOraDD("doc_num"))
    '        tmpReturn = True
    '    End If

    '    ConnGERSClose()

    '    Return tmpReturn

    'End Function

    Private Sub UpdateTHGERSStatus(ByVal DocNum As String)

        ConnSQLOpen()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateGERSStatus", connHeaderSQL)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)
        Dim DocNumParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Doc_Num", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = strBoxId
        DocNumParam.Value = DocNum

        objCommand.ExecuteReader()

        objCommand.Dispose()

    End Sub


    Public Sub make_store_number()

        Dim lengthDept As Integer
        Dim NeededLength As Integer
        Dim NeededZeros As Integer
        Dim strStore As String

        strStore = strSendingStoreNum
        NeededLength = 4
        lengthDept = Len(Trim(strStore))
        NeededZeros = NeededLength - lengthDept
        If NeededZeros = 1 Then
            strSendingStoreNum = "0" & strStore
        ElseIf NeededZeros = 2 Then
            strSendingStoreNum = "00" & strStore
        ElseIf NeededZeros = 3 Then
            strSendingStoreNum = "000" & strStore
        End If

        strStore = strReceivingStoreNum
        NeededLength = 4
        lengthDept = Len(Trim(strStore))
        NeededZeros = NeededLength - lengthDept
        If NeededZeros = 1 Then
            strReceivingStoreNum = "0" & strStore
        ElseIf NeededZeros = 2 Then
            strReceivingStoreNum = "00" & strStore
        ElseIf NeededZeros = 3 Then
            strReceivingStoreNum = "000" & strStore
        End If

    End Sub


    Public Sub ConnSQLOpen()
        'establish connection to SQL server
        connHeaderSQL = New System.Data.SqlClient.SqlConnection(strDBConnection)
        connHeaderSQL.Open()
    End Sub

    Public Sub ConnSQLClose()
        'close connection to SQL server
        connHeaderSQL.Close()
    End Sub
    '# disable connection to GERS - 09/10/2012 by LJ

    'Public Sub ConnGERSOpen()
    '    'establish connection to SQL server
    '    connHeaderGERS = New System.Data.OleDb.OleDbConnection(ConfigurationSettings.AppSettings("strGERSConn"))
    '    connHeaderGERS.Open()
    'End Sub

    Public Sub ConnGERSClose()
        'close connection to SQL server
        connHeaderGERS.Close()
    End Sub


End Class 'for GersInventoryUpdate
'Public Sub GetBoxItems(ByVal BoxId As String)
'    ' data for the box items
'    ConnSQLOpen()

'    Dim objDataReader As SqlClient.SqlDataReader

'    Dim objCommand As New SqlClient.SqlCommand("spGetBoxItemsType", connHeaderSQL)
'    objCommand.CommandType = CommandType.StoredProcedure

'    Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

'    BoxIdParam.Direction = ParameterDirection.Input

'    BoxIdParam.Value = BoxId

'    objDataReader = objCommand.ExecuteReader()

'    If objDataReader.Read() Then

'        strXferType = objDataReader("Xfer_Type_Cd").ToString

'        strSkuNum = objDataReader("Sku_Num").ToString
'        strQty = objDataReader("Received_Qty").ToString

'        strLnNum = 1

'        GetShipmentNumber()

'        Populate_POS_INV_TRN_LN()

'        strLnNum = 2
'        Do While objDataReader.Read

'            strSkuNum = objDataReader("Sku_Num").ToString
'            strQty = objDataReader("Received_Qty").ToString
'            strRetPrc = CType(objDataReader("CURR").ToString, Decimal)
'            Populate_POS_INV_TRN_LN()
'            strLnNum = strLnNum + 1
'        Loop

'        Populate_POS_INV_TRN()


'    End If
'    objDataReader.Close()
'    objCommand.Dispose()

'    ConnSQLClose()

'End Sub

'Private Function SendTransShipmentToRMS(ByRef DESC As RMSProcess.ASNOutDesc)
'    Dim message As String
'    Dim response As RMSProcess.publishASNOutCreateUsingASNOutDescResponse = New RMSProcess.publishASNOutCreateUsingASNOutDescResponse
'    Dim STSService As RMSProcess.ASNOutPublishingService = New RMSProcess.ASNOutPublishingService

'    STSService.Timeout = 2000

'    ' STSService.ping("y")


'    Dim des As RMSProcess.publishASNOutCreateUsingASNOutDesc = New RMSProcess.publishASNOutCreateUsingASNOutDesc
'    des.ASNOutDesc = DESC

'    response = STSService.publishASNOutCreateUsingASNOutDesc(des)

'    If (Not response.ServiceOpStatus Is Nothing) Then

'        If (Not response.ServiceOpStatus.FailStatus Is Nothing) Then
'            message += response.ServiceOpStatus.FailStatus.shortErrorMessage
'        ElseIf (Not response.ServiceOpStatus.SuccessStatus Is Nothing) Then
'            message += "success"
'        End If
'    Else
'        message = "Service Status Response is Missing."
'    End If

'    STSService.Dispose()
'    Return message

''End Function
'Public Sub GetBoxItems(ByVal BoxId As String)
'    ' data for the box items
'    ConnSQLOpen()
'    Dim rows As Integer = 0
'    Dim I As Integer = 0
'    Dim D As Integer = 0
'    Dim A As Integer = 0
'    Dim result As String


'    Dim objDataReader1 As SqlClient.SqlDataReader

'    Dim objCommand As New SqlClient.SqlCommand("spGetBoxItemsType", connHeaderSQL)
'    objCommand.CommandType = CommandType.StoredProcedure

'    Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

'    BoxIdParam.Direction = ParameterDirection.Input

'    BoxIdParam.Value = BoxId

'    objDataReader1 = objCommand.ExecuteReader()


'    '# add 
'    rows = GetBoxItemCount(BoxId)
'    Dim desc As RMSProcess.ASNOutDesc = New RMSProcess.ASNOutDesc
'    If objDataReader1.HasRows And rows > 0 Then

'        desc.asn_nbr = "567894"
'        desc.container_qty = 6
'        desc.trailer_nbr = "15"
'        desc.carrier_code = "38"


'        desc.to_location = strReceivingStoreNum
'        desc.from_location = strSendingStoreNum
'        strTrackingNum = GetTrackingNum(BoxId)
'        desc.bol_nbr = strTrackingNum 'change
'        desc.ASNOutDistro = desc.ASNOutDistro.CreateInstance(GetType(RMSProcess.ASNOutDistro), 1)

'        Dim distro As RMSProcess.ASNOutDistro = New RMSProcess.ASNOutDistro
'        distro.distro_nbr = strBoxId.Substring(0, 4) + strBoxId.Substring(6, 6)
'        distro.distro_doc_type = "T"
'        desc.ASNOutDistro(0) = distro

'        distro.ASNOutCtn = distro.ASNOutCtn.CreateInstance(GetType(RMSProcess.ASNOutCtn), 1)
'        Dim outCtn As RMSProcess.ASNOutCtn = New RMSProcess.ASNOutCtn

'        outCtn.container_id = "4324"
'        outCtn.weight = 30
'        outCtn.weight_uom = "LBS"

'        'outCtn.ASNOutItem = outCtn.ASNOutItem.CreateInstance(GetType(RMSProcess.ASNOutItem), rows)
'        outCtn.ASNOutItem = outCtn.ASNOutItem.CreateInstance(GetType(RMSProcess.ASNOutItem), rows)
'        distro.ASNOutCtn(0) = outCtn

'        Dim outItem As RMSProcess.ASNOutItem
'        While objDataReader1.Read

'            outItem = New RMSProcess.ASNOutItem

'            strSkuNum = objDataReader1("Sku_Num").ToString
'            strQty = objDataReader1("Received_Qty").ToString

'            outItem.item_id = strSkuNum
'            outItem.unit_qty = Double.Parse(strQty)
'            outItem.comments = strEmpId
'            outCtn.ASNOutItem(I) = outItem

'            I = I + 1

'        End While

'    End If





'    'If objDataReader.Read() Then

'    '    strXferType = objDataReader("Xfer_Type_Cd").ToString

'    '    strSkuNum = objDataReader("Sku_Num").ToString
'    '    strQty = objDataReader("Received_Qty").ToString

'    '    'strLnNum = 1
'    '    ''# disable below to  shipmentNumber for STS
'    '    ''GetShipmentNumber()
'    '    '' call SP to  tracking number by box id
'    '    ''Populate_POS_INV_TRN_LN()
'    '    'Populate_ORMS()

'    '    'strLnNum = 2
'    '    'Do While objDataReader.Read

'    '    '    strSkuNum = objDataReader("Sku_Num").ToString
'    '    '    strQty = objDataReader("Received_Qty").ToString
'    '    '    strRetPrc = CType(objDataReader("CURR").ToString, Decimal)
'    '    '    'Populate_POS_INV_TRN_LN()
'    '    '    Populate_ORMS()
'    '    '    strLnNum = strLnNum + 1
'    '    'Loop

'    '    'Populate_POS_INV_TRN()


'    'End If
'    objDataReader1.Close()
'    objCommand.Dispose()

'    ConnSQLClose()


'    If (Not desc Is Nothing) Then
'        result = SendTransShipmentToRMS(desc)
'    End If
'End Sub