<%@ Control Language="vb" AutoEventWireup="false" Codebehind="Header.ascx.vb" Inherits="SSTransfer.Header" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server">
</LINK>
<table cellSpacing="0" cellPadding="4" width="100%" class="sHeader" id="Table1">
	<tr>
		<td width="1%" vAlign="center"><asp:Image id="imgCompany" runat="server"></asp:Image></td>
		<td align="right" height="100%">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0" id="Table2">
				<tr>
					<td width="1%" valign="bottom">
						<asp:label id="lblStore" runat="server" CssClass="sHeaderStoreNumber" EnableViewState="False"></asp:label>
					</td>
					<td valign="center" align="middle">
						<asp:label id="lblHeaderTitle" runat="server" CssClass="sHeaderTitle" EnableViewState="False"></asp:label>
					</td>
					<td width="1%" vAlign="top" align="right">
						<table class="sHeaderLogin" cellSpacing="0" cellPadding="0" width="180" id="Table3">
							<tr>
								<td nowrap align="middle">&nbsp;<asp:label id="lblEmpName" runat="server" EnableViewState="False"></asp:label>&nbsp;</td>
							</tr>
							<tr>
								<td align="middle"><asp:linkbutton id="lbLogoff" runat="server" EnableViewState="False">log off</asp:linkbutton></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table cellSpacing="0" cellPadding="2" width="100%" class="sHeaderLinkbuttonTable" id="Table4">
	<tr>
		<td id="tdHome" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbHome" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">Info</asp:linkbutton></td>
		<td id="tdDebug" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbDebug" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">Debug</asp:linkbutton></td>
		<td id="tdUPS" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbUPS" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">UPS</asp:linkbutton></td>
		<td id="tdTransfer" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbTransfer" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">Transfer</asp:linkbutton></td>
		<td id="tdStore" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbStore" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">Store</asp:linkbutton></td>
		<td id="tdWeights" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbWeights" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">Weights</asp:linkbutton></td>
		<td id="tdNewTrans" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbNewTrans" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">New Transfer</asp:linkbutton></td>
		<td id="tdCheckIn" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbCheckIn" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">Check In</asp:linkbutton></td>
		<td id="tdSearch" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbSearch" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">Search</asp:linkbutton></td>
		<td id="tdMaint" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbMaint" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">Maint</asp:linkbutton></td>
		<td id="tdProfileList" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbProfileList" CssClass="sHeaderLinkbutton" runat="server" EnableViewState="False">Profiles</asp:linkbutton></td>
		<td id="tdProfileAdmin" nowrap align="middle" width="100" runat="server"><asp:LinkButton ID="lbProfileAdmin" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">Profile Admin</asp:LinkButton></td>
		<td id="tdICSearch" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbICSearch" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">Search</asp:linkbutton></td>
		<td id="tdExit" noWrap align="middle" width="100" runat="server"><asp:linkbutton id="lbExit" runat="server" CssClass="sHeaderLinkbutton" EnableViewState="False">Home\Exit</asp:linkbutton></td>
		<td align="right">&nbsp;</td>
	</tr>
</table>
