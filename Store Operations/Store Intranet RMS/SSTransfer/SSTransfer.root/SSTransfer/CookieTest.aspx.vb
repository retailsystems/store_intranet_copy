Public Class CookieTest
    Inherits System.Web.UI.Page
    Protected WithEvents lbStore5001 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbStore5002 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbStore5003 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbStore5004 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents RunSStransfer As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Request("Run") = 1 Then
            RunSStransfer.Visible = True
        Else
            RunSStransfer.Visible = False
        End If

        Dim loop1, loop2 As Integer
        Dim arr1(), arr2() As String
        Dim MyCookieColl As HttpCookieCollection
        Dim MyCookie As HttpCookie

        MyCookieColl = Request.Cookies
        ' Capture all cookie names into a string array.
        arr1 = MyCookieColl.AllKeys
        ' Grab individual cookie objects by cookie name     
        For loop1 = 0 To arr1.GetUpperBound(0)
            MyCookie = MyCookieColl(arr1(loop1))
            Response.Write("Cookie: " & MyCookie.Name & "<br>")
            Response.Write("Expires: " & MyCookie.Expires & "<br>")
            Response.Write("Secure:" & MyCookie.Secure & "<br>")
            Response.Write("Domain:" & MyCookie.Domain & "<br>")
            Response.Write("Path:" & MyCookie.Path & "<br>")

            ' Grab all values for single cookie into an object array.
            arr2 = MyCookie.Values.AllKeys
            ' Loop through cookie value collection and print all values.
            For loop2 = 0 To arr2.GetUpperBound(0)
                Response.Write("Value " & CStr(loop2) + ": " & arr2(loop2) & "<br>")
            Next loop2

        Next loop1
        Response.Write("[" & Request.Cookies("StoreNo").Value & "]")
    End Sub

    Private Sub lbStore5001_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbStore5001.Click
        Response.Cookies("StoreNo").Value = 5001
        Response.Cookies("StoreNo").Expires = "#1/1/2004#"
        Response.Redirect("CookieTest.aspx?Run=1")
    End Sub

    Private Sub lbStore5002_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbStore5002.Click
        Response.Cookies("StoreNo").Value = 5002
        Response.Cookies("StoreNo").Expires = "#1/1/2004#"
        Response.Redirect("CookieTest.aspx?Run=1")
    End Sub

    Private Sub lbStore5003_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbStore5003.Click
        Response.Cookies("StoreNo").Value = 5003
        Response.Cookies("StoreNo").Expires = "#1/1/2004#"
        Response.Redirect("CookieTest.aspx?Run=1")
    End Sub

    Private Sub lbStore5004_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbStore5004.Click
        Response.Cookies("StoreNo").Value = 5004
        Response.Cookies("StoreNo").Expires = "#1/1/2004#"
        Response.Redirect("CookieTest.aspx?Run=1")
    End Sub

End Class
