Public Class CustInfo
    Inherits System.Web.UI.Page
    Protected WithEvents btnUpdate As System.Web.UI.WebControls.Button
    Protected WithEvents lblFName As System.Web.UI.WebControls.Label
    Protected WithEvents txtFName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblLName As System.Web.UI.WebControls.Label
    Protected WithEvents txtLName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblStrAddress As System.Web.UI.WebControls.Label
    Protected WithEvents txtStrAddress As System.Web.UI.WebControls.TextBox
    Protected WithEvents trCustNumber As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblState As System.Web.UI.WebControls.Label
    Protected WithEvents lblCity As System.Web.UI.WebControls.Label
    Protected WithEvents txtCity As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtState As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblZip As System.Web.UI.WebControls.Label
    Protected WithEvents txtZip As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents lblWorkPhone As System.Web.UI.WebControls.Label
    Protected WithEvents txtWorkPhone As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnRetrieve As System.Web.UI.WebControls.Button
    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
    Protected WithEvents btnAddNew As System.Web.UI.WebControls.Button
    Protected WithEvents btnFClose As System.Web.UI.WebControls.Button
    Protected WithEvents lblNotes As System.Web.UI.WebControls.Label
    Protected WithEvents trNotes As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents txtNotes As System.Web.UI.WebControls.TextBox
    Protected WithEvents HCustId As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents HCustName As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblCustomerList As System.Web.UI.WebControls.Label
    Protected WithEvents trListCustomers As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trCustomerInfo As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblCustomerInfo As System.Web.UI.WebControls.Label
    Protected WithEvents trCustomerName As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trstrAddress As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trCityStateZip As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trMessage As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trWorkPhone As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents TrCloseSubmitbtns As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents dgCustomerList As System.Web.UI.WebControls.DataGrid
    Protected WithEvents test As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents phCloseWindow As System.Web.UI.WebControls.PlaceHolder
    Protected WithEvents phAssign As System.Web.UI.WebControls.PlaceHolder
    Protected WithEvents lblHomePhone As System.Web.UI.WebControls.Label
    'Protected WithEvents txtHomePhone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAreaCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPrefix As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtLast4 As System.Web.UI.WebControls.TextBox


    Private customerid As Integer    'Value passed in from Transferbox.ascx
    Private customeridX As Integer
    Public globalHomePhone As String 'Home Phone that user entered.  Used for New Customer purposes.'(GetCustomerInfo()) and CheckResults())
    Public strUserCID As String      'CID that user entered.  Used for New customer purposes.
    Private strID As Integer         'This is the autonumber from the customer table. CID. Used to submit to Dennis
    Public strDash, strParen1, strParen2 As String  'Used for validating tele numbers entered by user.
    Public strFirstName As String
    Public strLastName As String
    Public strAddress As String
    Public strCity As String
    Public strState As String
    Public strZip As String
    Public strHPhone As String
    Public strWPhone As String
    Public strNotes As String
    'Public strStore As String        'Store number from cookie
    Public intStore As Integer        'Store number from cookie
    Public parmCust_Id As SqlClient.SqlParameter
    Public parmFirstName As SqlClient.SqlParameter
    Public parmLastName As SqlClient.SqlParameter
    Public parmAddress As SqlClient.SqlParameter
    Public parmCity As SqlClient.SqlParameter
    Public parmState As SqlClient.SqlParameter
    Public parmZip As SqlClient.SqlParameter
    Public parmHPhone As SqlClient.SqlParameter
    Public parmWPhone As SqlClient.SqlParameter
    Public parmNotes As SqlClient.SqlParameter
    Public parmStore As SqlClient.SqlParameter
    Public parmID As SqlClient.SqlParameter
    Public NullCheck As Boolean
    Protected WithEvents lblOParen As System.Web.UI.WebControls.Label
    Protected WithEvents lblEParen As System.Web.UI.WebControls.Label
    Protected WithEvents lblPDash As System.Web.UI.WebControls.Label
    Private objUserInfo As New UserInfo()
    'Public ReadOnly Property Cookies() As HttpCookieCollection
    'Public Event GetCustomer_ID(ByVal customer_id As Integer) 'Used to send the customer id to Dennis

#Region " Web Form Designer Generated Code "


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()

        intStore = objUserInfo.StoreNumber
        lblCustomerList.Visible = False
        If (Not Request.QueryString("CID") = "" And Not Request.QueryString("CID") = "0") Then
            customerid = CStr(Request.QueryString("CID"))  'getting autonumber from dennis
        End If

        If (Not Request.QueryString("CIDX") = "" And Not Request.QueryString("CIDX") = "0") Then
            customeridX = CStr(Request.QueryString("CIDX")) 'getting autonum from listbox
        End If

        If Not IsPostBack Then
            If customeridX > 0 Then
                'if autonumber was set, retrieve customer's info.
                Call Retrieve_Info()
                phAssign.Visible = "True"
            ElseIf customerid > 0 Then
                'user wants to see customer info.
                Call ReviewCustomer()
            Else
                Call Display_Form()
            End If
            lblMessage.Text = " "
        End If


    End Sub
    Private Sub ReviewCustomer()
        btnRetrieve.Visible = False
        btnAddNew.Visible = False
        btnUpdate.Visible = False


        strID = customerid
        ViewState("ID") = strID

        Dim conGetCustomer As System.Data.SqlClient.SqlConnection
        Dim cmdGetCustomer As System.Data.SqlClient.SqlCommand

        conGetCustomer = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdGetCustomer = New SqlClient.SqlCommand("Ret_Cust_Info", conGetCustomer)
        cmdGetCustomer.CommandType = CommandType.StoredProcedure

        cmdGetCustomer.Parameters.Add("@ci", customerid)  'Send function the CID passed from Transfer.ascx
        'Don't need store # b/c retrieving off of autonum
        'parmCust_Id = cmdGetCustomer.Parameters.Add("@custid", SqlDbType.VarChar)
        parmFirstName = cmdGetCustomer.Parameters.Add("@FirstName", SqlDbType.VarChar)
        parmLastName = cmdGetCustomer.Parameters.Add("@LastName", SqlDbType.VarChar)
        parmAddress = cmdGetCustomer.Parameters.Add("@Address", SqlDbType.VarChar)
        parmCity = cmdGetCustomer.Parameters.Add("@City", SqlDbType.VarChar)
        parmState = cmdGetCustomer.Parameters.Add("@State", SqlDbType.VarChar)
        parmZip = cmdGetCustomer.Parameters.Add("@Zip", SqlDbType.VarChar)
        parmHPhone = cmdGetCustomer.Parameters.Add("@HPhone", SqlDbType.VarChar)
        parmWPhone = cmdGetCustomer.Parameters.Add("@WPhone", SqlDbType.VarChar)
        parmNotes = cmdGetCustomer.Parameters.Add("@Notes", SqlDbType.VarChar)
        'parmCust_Id.Size = 10
        parmFirstName.Size = 20
        parmLastName.Size = 20
        parmAddress.Size = 40
        parmCity.Size = 20
        parmState.Size = 2
        parmZip.Size = 12
        parmHPhone.Size = 15
        parmWPhone.Size = 15
        parmNotes.Size = 40
        'parmCust_Id.Direction = ParameterDirection.Output
        parmFirstName.Direction = ParameterDirection.Output
        parmLastName.Direction = ParameterDirection.Output
        parmAddress.Direction = ParameterDirection.Output
        parmCity.Direction = ParameterDirection.Output
        parmState.Direction = ParameterDirection.Output
        parmZip.Direction = ParameterDirection.Output
        parmHPhone.Direction = ParameterDirection.Output
        parmWPhone.Direction = ParameterDirection.Output
        parmNotes.Direction = ParameterDirection.Output
        conGetCustomer.Open()
        cmdGetCustomer.ExecuteNonQuery()

        'Check to see if first name is null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@FirstName").Value)
        If NullCheck = True Then
            strFirstName = " "
        Else
            strFirstName = cmdGetCustomer.Parameters("@FirstName").Value
        End If

        'Test to see if Last Name is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@LastName").Value)
        If NullCheck = True Then
            strLastName = " "
        Else
            strLastName = cmdGetCustomer.Parameters("@LastName").Value
        End If

        'Test to see if Address is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@Address").Value)
        If NullCheck = True Then
            strAddress = " "
        Else
            strAddress = cmdGetCustomer.Parameters("@Address").Value
        End If

        'Test to see if City is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@City").Value)
        If NullCheck = True Then
            strCity = " "
        Else
            strCity = cmdGetCustomer.Parameters("@City").Value
        End If

        'Test to see if State is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@State").Value)
        If NullCheck = True Then
            strState = " "
        Else
            strState = cmdGetCustomer.Parameters("@State").Value
        End If

        'Test to see if Zip Code is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@Zip").Value)
        If NullCheck = True Then
            strZip = " "
        Else
            strZip = cmdGetCustomer.Parameters("@Zip").Value
        End If

        'Test to see if Home Phone number is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@HPhone").Value)
        If NullCheck = True Then
            strHPhone = " "
        Else
            strHPhone = cmdGetCustomer.Parameters("@HPhone").Value
        End If


        'Test to see if Work Phone Number is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@WPhone").Value)
        If NullCheck = True Then
            strWPhone = " "
        Else
            strWPhone = cmdGetCustomer.Parameters("@WPhone").Value
        End If

        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@Notes").Value)
        If NullCheck = True Then
            strNotes = " "
        Else
            strNotes = cmdGetCustomer.Parameters("@Notes").Value
        End If

        conGetCustomer.Close()
        'Fill in text boxes with customer info.
        txtFName.Text = strFirstName
        txtLName.Text = strLastName
        txtStrAddress.Text = strAddress
        txtCity.Text = strCity
        txtState.Text = strState
        txtZip.Text = strZip
        txtWorkPhone.Text = strWPhone
        txtNotes.Text = strNotes
        'txtHomePhone.Text = strHPhone
        strHPhone = Trim(strHPhone)
        txtAreaCode.Text = Left(strHPhone, 3)
        txtPreFix.Text = Mid(strHPhone, 4, 3)
        txtLast4.Text = Right(strHPhone, 4)

        'txtHomePhone.ReadOnly = True
        txtAreaCode.ReadOnly = True  'HomePhone
        txtPreFix.ReadOnly = True    'HomePhone
        txtLast4.ReadOnly = True     'HomePhone
        txtWorkPhone.ReadOnly = True
        txtNotes.ReadOnly = True
        txtLName.ReadOnly = True
        txtFName.ReadOnly = True
        txtStrAddress.ReadOnly = True
        txtZip.ReadOnly = True
        txtCity.ReadOnly = True
        txtState.ReadOnly = True

        HCustId.Value = strID
        HCustName.Value = txtFName.Text & " " & txtLName.Text

    End Sub

    'Retrieve _Info runs if a customerid was passed in.
    Private Sub Retrieve_Info()
        'This function retrieves the customer's info if a Customer ID was passed in from Transfer.ascx
        'OR if user selects customer from selection box.
        btnAddNew.Visible = False
        btnUpdate.Visible = True
        strID = customeridX        'storing autonumber in strID
        ViewState("ID") = strID   'storing autonumber in ViewState variable

        Dim conGetCustomer As System.Data.SqlClient.SqlConnection
        Dim cmdGetCustomer As System.Data.SqlClient.SqlCommand

        conGetCustomer = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdGetCustomer = New SqlClient.SqlCommand("Ret_Cust_Info", conGetCustomer)
        cmdGetCustomer.CommandType = CommandType.StoredProcedure

        cmdGetCustomer.Parameters.Add("@ci", customeridX)  'Send function the CID passed from Transfer.ascx
        'Don't need store # b/c retrieving off of autonum
        'parmCust_Id = cmdGetCustomer.Parameters.Add("@custid", SqlDbType.VarChar)
        parmFirstName = cmdGetCustomer.Parameters.Add("@FirstName", SqlDbType.VarChar)
        parmLastName = cmdGetCustomer.Parameters.Add("@LastName", SqlDbType.VarChar)
        parmAddress = cmdGetCustomer.Parameters.Add("@Address", SqlDbType.VarChar)
        parmCity = cmdGetCustomer.Parameters.Add("@City", SqlDbType.VarChar)
        parmState = cmdGetCustomer.Parameters.Add("@State", SqlDbType.VarChar)
        parmZip = cmdGetCustomer.Parameters.Add("@Zip", SqlDbType.VarChar)
        parmHPhone = cmdGetCustomer.Parameters.Add("@HPhone", SqlDbType.VarChar)
        parmWPhone = cmdGetCustomer.Parameters.Add("@WPhone", SqlDbType.VarChar)
        parmNotes = cmdGetCustomer.Parameters.Add("@Notes", SqlDbType.VarChar)
        'parmCust_Id.Size = 10
        parmFirstName.Size = 20
        parmLastName.Size = 20
        parmAddress.Size = 40
        parmCity.Size = 20
        parmState.Size = 2
        parmZip.Size = 12
        parmHPhone.Size = 15
        parmWPhone.Size = 15
        parmNotes.Size = 40
        'parmCust_Id.Direction = ParameterDirection.Output
        parmFirstName.Direction = ParameterDirection.Output
        parmLastName.Direction = ParameterDirection.Output
        parmAddress.Direction = ParameterDirection.Output
        parmCity.Direction = ParameterDirection.Output
        parmState.Direction = ParameterDirection.Output
        parmZip.Direction = ParameterDirection.Output
        parmHPhone.Direction = ParameterDirection.Output
        parmWPhone.Direction = ParameterDirection.Output
        parmNotes.Direction = ParameterDirection.Output
        conGetCustomer.Open()
        cmdGetCustomer.ExecuteNonQuery()

        'Check to see if first name is null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@FirstName").Value)
        If NullCheck = True Then
            strFirstName = " "
        Else
            strFirstName = cmdGetCustomer.Parameters("@FirstName").Value
        End If

        'Test to see if Last Name is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@LastName").Value)
        If NullCheck = True Then
            strLastName = " "
        Else
            strLastName = cmdGetCustomer.Parameters("@LastName").Value
        End If

        'Test to see if Address is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@Address").Value)
        If NullCheck = True Then
            strAddress = " "
        Else
            strAddress = cmdGetCustomer.Parameters("@Address").Value
        End If

        'Test to see if City is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@City").Value)
        If NullCheck = True Then
            strCity = " "
        Else
            strCity = cmdGetCustomer.Parameters("@City").Value
        End If

        'Test to see if State is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@State").Value)
        If NullCheck = True Then
            strState = " "
        Else
            strState = cmdGetCustomer.Parameters("@State").Value
        End If

        'Test to see if Zip Code is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@Zip").Value)
        If NullCheck = True Then
            strZip = " "
        Else
            strZip = cmdGetCustomer.Parameters("@Zip").Value
        End If

        'Test to see if Home Phone number is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@HPhone").Value)
        If NullCheck = True Then
            strHPhone = " "
        Else
            strHPhone = cmdGetCustomer.Parameters("@HPhone").Value
        End If


        'Test to see if Work Phone Number is Null
        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@WPhone").Value)
        If NullCheck = True Then
            strWPhone = " "
        Else
            strWPhone = cmdGetCustomer.Parameters("@WPhone").Value
        End If

        NullCheck = IsDBNull(cmdGetCustomer.Parameters("@Notes").Value)
        If NullCheck = True Then
            strNotes = " "
        Else
            strNotes = cmdGetCustomer.Parameters("@Notes").Value
        End If

        conGetCustomer.Close()
        'Fill in text boxes with customer info.
        txtFName.Text = strFirstName
        txtLName.Text = strLastName
        txtStrAddress.Text = strAddress
        txtCity.Text = strCity
        txtState.Text = strState
        txtZip.Text = strZip
        txtWorkPhone.Text = strWPhone
        txtNotes.Text = strNotes
        'txtHomePhone.Text = strHPhone
        strHPhone = Trim(strHPhone)
        txtAreaCode.Text = Left(strHPhone, 3)
        txtPreFix.Text = Mid(strHPhone, 4, 3)
        txtLast4.Text = Right(strHPhone, 4)

        HCustId.Value = strID
        HCustName.Value = txtFName.Text & " " & txtLName.Text
    End Sub
    'Display_Form runs if a customerid wasn't passed.
    Private Sub Display_Form()
        'Displays form if there is no cid value from transfer.ascx
        btnAddNew.Visible = True
        btnRetrieve.Visible = True
        btnFClose.Visible = True
        btnUpdate.Visible = False
        lblCustomerList.Visible = False
    End Sub
    Private Sub btnRetrieve_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetrieve.Click
        'Validating user input.  If okay, calls GetCustomerInfo procedure.
        Dim ACCheck As Boolean
        Dim PrefixCheck As Boolean
        Dim Last4Check As Boolean
        ACCheck = IsNumeric(txtAreaCode.Text)
        PrefixCheck = IsNumeric(txtPreFix.Text)
        Last4Check = IsNumeric(txtLast4.Text)

        If Len(txtAreaCode.Text) < 3 Then
            'Check length of entry
            lblMessage.Text = "Please enter an area code for the home telephone number."
        ElseIf (Len(txtPreFix.Text) < 3 Or Len(txtLast4.Text) < 4) Then
            'Check length of entry
            lblMessage.Text = "The home telephone number is invalid.  Please check your entry."
        ElseIf (ACCheck = False Or PrefixCheck = False Or Last4Check = False) Then
            'Check length of entry
            lblMessage.Text = "The home telephone number is invalid.  Please check your entry."
        Else
            'everything is valid entry.  Call procedure to list customers.
            Call ListCustomers()
        End If
        'Dim strHomePhone As String
        'strHomePhone = Trim(txtHomePhone.Text)
        'strDash = "-"
        'strParen1 = "("
        'strParen2 = ")"
        'If (strHomePhone.Length = 10 And InStr(strHomePhone, strDash) = 0 And InStr(strHomePhone, strParen1) = 0 And InStr(strHomePhone, strParen2) = 0) Then
        'Call ListCustomers()
        'Else
        'lblMessage.Text = "Please enter only numbers.  Don't use ()'s, or -'s.  Customer Number should be area code plus home telephone number."
        'End If

        ''Retrieves customer info by customer id number (Home Tele #)

    End Sub
    Private Sub btnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Dim strFName, strLName As String
        strFName = Trim(txtFName.Text)
        strLName = Trim(txtLName.Text)
        If (strLName = "" Or strFName = "") Then
            'Make sure user entered a first and last name
            lblMessage.Text = "First Name and Last Name are both required."
        Else
            lblMessage.Text = ""
            Call Check_Custid()  'validate customer id textbox only has numbers not -' or ()'s.
        End If
        'If data is okay, it will call the Insert_Customer procedure
    End Sub
    Private Sub Check_Custid()
        'Validate user input.  Make sure custid doesn't have (), or Dashes.  If so, make a message to user.
        'If okay, calls Insert_Customer Procedure.
        Dim ACCheck As Boolean
        Dim PrefixCheck As Boolean
        Dim Last4Check As Boolean
        ACCheck = IsNumeric(txtAreaCode.Text)
        PrefixCheck = IsNumeric(txtPreFix.Text)
        Last4Check = IsNumeric(txtLast4.Text)

        If Len(txtAreaCode.Text) = 0 And Len(txtPreFix.Text) = 0 And Len(txtLast4.Text) = 0 Then
            'If user didn't type in telephone number, still insert customer.  otherwise, validate their entry.
            Call Insert_Customer()
        ElseIf Len(txtAreaCode.Text) < 3 Then
            'Check length of entry
            lblMessage.Text = "Please enter an area code for the home telephone number."
        ElseIf (Len(txtPreFix.Text) < 3 Or Len(txtLast4.Text) < 4) Then
            'Check length of entry
            lblMessage.Text = "The home telephone number is invalid.  Please check your entry."
        ElseIf (ACCheck = False Or PrefixCheck = False Or Last4Check = False) Then
            'Check length of entry
            lblMessage.Text = "The home telephone number is invalid.  Please check your entry."
        Else
            'everything is valid entry.  Call procedure to list customers.
            Call Insert_Customer()
        End If
        'Dim strHomePhone As String
        'strDash = "-"
        'strParen1 = "("
        'strParen2 = ")"
        'strHomePhone = Trim(txtHomePhone.Text)
        'If (strHomePhone.Length = 10 And InStr(strHomePhone, strDash) = 0 And InStr(strHomePhone, strParen1) = 0 And InStr(strHomePhone, strParen2) = 0) Then
        'Call Insert_Customer()
        'ElseIf strHomePhone = " " Or strHomePhone = "" Then
        'Call Insert_Customer()
        'Else
        'lblMessage.Text = "Please enter only numbers.  Don't use ()'s, or -'s. Customer Number should be area code plue home telephone number."
        'End If
    End Sub
    Private Sub Insert_Customer()
        'Lets user enter a new customer.  Must verify that the customers telephone number is unique though
        'when submitting new customer.  If that telephone number already existed, when they retrieved the first time
        'they should have just updated or changed the existing information.
        Dim Created As Date 'Date and Time customer was inserted for first time
        Dim Modified As Date  'Date and Time customer was modified
        Created = Now         'Get Date and Time
        Modified = Created    'Assign Created to Modified so they have the exact same information upon creation

        'Dim PhoneLength As Integer  Why do I need this?
        'PhoneLength = txtHomePhone.Text.Length
        'Dim intHPhone As Integer
        'intHPhone = txtAreaCode.Text & txtPreFix.Text & txtLast4.Text
        'PhoneLength = intHPhone


        ''intStore = 1 'use for testing

        strFirstName = txtFName.Text
        strLastName = txtLName.Text
        strAddress = txtStrAddress.Text
        strCity = txtCity.Text
        strState = txtState.Text
        strZip = txtZip.Text
        'strHPhone = txtHomePhone.Text   'customer number entered will be inserted into Home telephone number
        strHPhone = Trim(txtAreaCode.Text & txtPreFix.Text & txtLast4.Text)
        strWPhone = txtWorkPhone.Text
        strNotes = txtNotes.Text

        Dim conInsertCustomer As System.Data.SqlClient.SqlConnection
        Dim cmdInsertCustomer As System.Data.SqlClient.SqlCommand
        conInsertCustomer = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdInsertCustomer = New SqlClient.SqlCommand("New_Customer_Insert", conInsertCustomer)
        cmdInsertCustomer.CommandType = CommandType.StoredProcedure
        cmdInsertCustomer.Parameters.Add("@FName", strFirstName)
        cmdInsertCustomer.Parameters.Add("@LName", strLastName)
        cmdInsertCustomer.Parameters.Add("@Address", strAddress)
        cmdInsertCustomer.Parameters.Add("@City", strCity)
        cmdInsertCustomer.Parameters.Add("@State", strState)
        cmdInsertCustomer.Parameters.Add("@Zip", strZip)
        cmdInsertCustomer.Parameters.Add("@HPhone", strHPhone)
        cmdInsertCustomer.Parameters.Add("@WPhone", strWPhone)
        cmdInsertCustomer.Parameters.Add("@Notes", strNotes)
        cmdInsertCustomer.Parameters.Add("@Store", intStore)
        cmdInsertCustomer.Parameters.Add("@Created", Created)
        cmdInsertCustomer.Parameters.Add("@Modified", Modified)

        parmID = cmdInsertCustomer.Parameters.Add("@ID", SqlDbType.VarChar)
        parmID.Size = 15
        parmID.Direction = ParameterDirection.Output

        conInsertCustomer.Open()
        cmdInsertCustomer.ExecuteNonQuery()
        strID = cmdInsertCustomer.Parameters("@ID").Value
        ViewState("ID") = strID
        conInsertCustomer.Close()

        HCustId.Value = strID
        HCustName.Value = txtFName.Text & " " & txtLName.Text
        btnAddNew.Visible = False
        phAssign.Visible = "True"
        lblMessage.Text = "Your customer has been added.  You can now Assign your customer to the transfer."
    End Sub
    Private Sub ListCustomers()
        'This executes a stored procedure to get all the customers with the telephone number that the 
        'user entered from that store.  Then list those customers in dgCustomers.

        ''intStore = 1   'User for testing
        Dim strHomePhone As String
        strHomePhone = txtAreaCode.Text & txtPreFix.Text & txtLast4.Text
        globalHomePhone = Trim(strHomePhone)
        'globalHomePhone = Trim(txtHomePhone.Text)
        Dim strStoredProcedure As String
        Dim conListCustomer As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim ListCustomerDataReader As SqlClient.SqlDataReader
        conListCustomer.Open()
        'strStoredProcedure = "ListCustomers 8545693652, '188' "
        strStoredProcedure = "ListCustomers " & globalHomePhone & "," & intStore & " "
        Dim cmdListCustomer As New SqlClient.SqlCommand(strStoredProcedure, conListCustomer)
        ListCustomerDataReader = cmdListCustomer.ExecuteReader(CommandBehavior.CloseConnection)

        Dim count As Integer
        count = 0
        Do While ListCustomerDataReader.Read
            'count number of customers with telephone number entered
            count = count + 1
        Loop
        conListCustomer.Close()
        '************************************************************************************************
        If count = 1 Then
            'Fill in customer info. in textboxes
            Dim conGetCustomer As System.Data.SqlClient.SqlConnection
            Dim cmdGetCustomer As System.Data.SqlClient.SqlCommand

            conGetCustomer = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            cmdGetCustomer = New SqlClient.SqlCommand("Get_Cust_Info", conGetCustomer)
            cmdGetCustomer.CommandType = CommandType.StoredProcedure

            cmdGetCustomer.Parameters.Add("@ci", globalHomePhone)
            cmdGetCustomer.Parameters.Add("@Store", intStore)
            parmCust_Id = cmdGetCustomer.Parameters.Add("@custid", SqlDbType.VarChar)
            parmFirstName = cmdGetCustomer.Parameters.Add("@FirstName", SqlDbType.VarChar)
            parmLastName = cmdGetCustomer.Parameters.Add("@LastName", SqlDbType.VarChar)
            parmAddress = cmdGetCustomer.Parameters.Add("@Address", SqlDbType.VarChar)
            parmCity = cmdGetCustomer.Parameters.Add("@City", SqlDbType.VarChar)
            parmState = cmdGetCustomer.Parameters.Add("@State", SqlDbType.VarChar)
            parmZip = cmdGetCustomer.Parameters.Add("@Zip", SqlDbType.VarChar)
            parmHPhone = cmdGetCustomer.Parameters.Add("@HPhone", SqlDbType.VarChar)
            parmWPhone = cmdGetCustomer.Parameters.Add("@WPhone", SqlDbType.VarChar)
            parmNotes = cmdGetCustomer.Parameters.Add("@Notes", SqlDbType.VarChar)
            parmCust_Id.Size = 10
            parmFirstName.Size = 20
            parmLastName.Size = 20
            parmAddress.Size = 40
            parmCity.Size = 20
            parmState.Size = 2
            parmZip.Size = 12
            parmHPhone.Size = 15
            parmWPhone.Size = 15
            parmNotes.Size = 40
            parmCust_Id.Direction = ParameterDirection.Output
            parmFirstName.Direction = ParameterDirection.Output
            parmLastName.Direction = ParameterDirection.Output
            parmAddress.Direction = ParameterDirection.Output
            parmCity.Direction = ParameterDirection.Output
            parmState.Direction = ParameterDirection.Output
            parmZip.Direction = ParameterDirection.Output
            parmHPhone.Direction = ParameterDirection.Output
            parmWPhone.Direction = ParameterDirection.Output
            parmNotes.Direction = ParameterDirection.Output
            conGetCustomer.Open()
            cmdGetCustomer.ExecuteNonQuery()
            'Check to see if first name is null
            NullCheck = IsDBNull(cmdGetCustomer.Parameters("@FirstName").Value)
            If NullCheck = True Then
                strFirstName = " "
            Else
                strFirstName = cmdGetCustomer.Parameters("@FirstName").Value
            End If

            'Test to see if Last Name is Null
            NullCheck = IsDBNull(cmdGetCustomer.Parameters("@LastName").Value)
            If NullCheck = True Then
                strLastName = " "
            Else
                strLastName = cmdGetCustomer.Parameters("@LastName").Value
            End If

            'Test to see if Address is Null
            NullCheck = IsDBNull(cmdGetCustomer.Parameters("@Address").Value)
            If NullCheck = True Then
                strAddress = " "
            Else
                strAddress = cmdGetCustomer.Parameters("@Address").Value
            End If

            'Test to see if City is Null
            NullCheck = IsDBNull(cmdGetCustomer.Parameters("@City").Value)
            If NullCheck = True Then
                strCity = " "
            Else
                strCity = cmdGetCustomer.Parameters("@City").Value
            End If

            'Test to see if State is Null
            NullCheck = IsDBNull(cmdGetCustomer.Parameters("@State").Value)
            If NullCheck = True Then
                strState = " "
            Else
                strState = cmdGetCustomer.Parameters("@State").Value
            End If

            'Test to see if Zip Code is Null
            NullCheck = IsDBNull(cmdGetCustomer.Parameters("@Zip").Value)
            If NullCheck = True Then
                strZip = " "
            Else
                strZip = cmdGetCustomer.Parameters("@Zip").Value
            End If

            'Test to see if Home Phone number is Null
            NullCheck = IsDBNull(cmdGetCustomer.Parameters("@HPhone").Value)
            If NullCheck = True Then
                strHPhone = " "
            Else
                strHPhone = cmdGetCustomer.Parameters("@HPhone").Value
            End If


            'Test to see if Work Phone Number is Null
            NullCheck = IsDBNull(cmdGetCustomer.Parameters("@WPhone").Value)
            If NullCheck = True Then
                strWPhone = " "
            Else
                strWPhone = cmdGetCustomer.Parameters("@WPhone").Value
            End If

            NullCheck = IsDBNull(cmdGetCustomer.Parameters("@Notes").Value)
            If NullCheck = True Then
                strNotes = " "
            Else
                strNotes = cmdGetCustomer.Parameters("@Notes").Value
            End If

            conGetCustomer.Close()
            'Fill in text boxes with customer info.
            txtFName.Text = strFirstName
            txtLName.Text = strLastName
            txtStrAddress.Text = strAddress
            txtCity.Text = strCity
            txtState.Text = strState
            txtZip.Text = strZip
            txtWorkPhone.Text = strWPhone
            txtNotes.Text = strNotes
            'txtHomePhone.Text = strHPhone
            strHPhone = Trim(strHPhone)
            txtAreaCode.Text = Left(strHPhone, 3)
            txtPreFix.Text = Mid(strHPhone, 4, 3)
            txtLast4.Text = Right(strHPhone, 4)
            strID = cmdGetCustomer.Parameters("@custid").Value
            ViewState("ID") = strID 'see if this fixes update info problem

            HCustId.Value = strID
            HCustName.Value = txtFName.Text & " " & txtLName.Text
            phAssign.Visible = "true"
            btnUpdate.Visible = "True"
            lblCustomerList.Visible = False
        ElseIf count > 1 Then
            'Fill listbox with customer names so user can choose correct customer
            lblCustomerList.Visible = True
            Dim strSProcedure As String
            Dim conLCustomer As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            Dim LCDataReader As SqlClient.SqlDataReader
            conLCustomer.Open()
            strSProcedure = "ListCustomers " & globalHomePhone & ",'" & intStore & "' "
            Dim cmdLCustomer As New SqlClient.SqlCommand(strSProcedure, conLCustomer)
            LCDataReader = cmdLCustomer.ExecuteReader(CommandBehavior.CloseConnection)
            dgCustomerList.DataSource = LCDataReader
            dgCustomerList.DataBind()
        End If

        btnAddNew.Visible = True
        lblMessage.Visible = True
        lblMessage.Text = "To add a new customer: fill out the customer information and click on the Add Customer button"
    End Sub
    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        'This button makes updates to table when user has changed customer information.  
        Call Update_Info() 'This event updates user info and raises the event.
    End Sub
    Private Sub Update_Info()
        'This updates any changes made to the customer's information.
        'Validate updated info before making the changes in database.
        Dim ACCheck As Boolean
        Dim PrefixCheck As Boolean
        Dim Last4Check As Boolean
        ACCheck = IsNumeric(txtAreaCode.Text)
        PrefixCheck = IsNumeric(txtPreFix.Text)
        Last4Check = IsNumeric(txtLast4.Text)

        If Len(txtAreaCode.Text) < 3 Then
            'Check length of entry
            lblMessage.Text = "Please enter an area code for the home telephone number."
        ElseIf (Len(txtPreFix.Text) < 3 Or Len(txtLast4.Text) < 4) Then
            'Check length of entry
            lblMessage.Text = "The home telephone number is invalid.  Please check your entry."
        ElseIf (ACCheck = False Or PrefixCheck = False Or Last4Check = False) Then
            'Check length of entry
            lblMessage.Text = "The home telephone number is invalid.  Please check your entry."
        Else
            'everything is valid entry. 
            '*********************************************
        Dim Modified As Date
        Modified = Now    'Get modified date and time
        If ((txtFName.Text <> "" And txtFName.Text <> " ") And (txtLName.Text <> "" And txtLName.Text <> " ")) Then
            strID = ViewState("ID")
            strFirstName = txtFName.Text
            strLastName = txtLName.Text
            strAddress = txtStrAddress.Text
            strCity = txtCity.Text
            strState = txtState.Text
            strZip = txtZip.Text
            'strHPhone = txtHomePhone.Text   'customer number entered will be inserted into Home telephone number
            strHPhone = Trim(txtAreaCode.Text & txtPreFix.Text & txtLast4.Text)
            strWPhone = txtWorkPhone.Text
            strNotes = txtNotes.Text
            Dim conUpdateCustomer As System.Data.SqlClient.SqlConnection
            Dim cmdUpdateCustomer As System.Data.SqlClient.SqlCommand
            Dim parmCust_Id As SqlClient.SqlParameter


            conUpdateCustomer = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            cmdUpdateCustomer = New SqlClient.SqlCommand("Update_Customer_Info", conUpdateCustomer)
            cmdUpdateCustomer.CommandType = CommandType.StoredProcedure

            cmdUpdateCustomer.Parameters.Add("@custid", strID)
            cmdUpdateCustomer.Parameters.Add("@FName", strFirstName)
            cmdUpdateCustomer.Parameters.Add("@LName", strLastName)
            cmdUpdateCustomer.Parameters.Add("@Address", strAddress)
            cmdUpdateCustomer.Parameters.Add("@City", strCity)
            cmdUpdateCustomer.Parameters.Add("@State", strState)
            cmdUpdateCustomer.Parameters.Add("@Zip", strZip)
            cmdUpdateCustomer.Parameters.Add("@HPhone", strHPhone)
            cmdUpdateCustomer.Parameters.Add("@WPhone", strWPhone)
            cmdUpdateCustomer.Parameters.Add("@Notes", strNotes)
            cmdUpdateCustomer.Parameters.Add("@Modified", Modified)
            conUpdateCustomer.Open()
            cmdUpdateCustomer.ExecuteNonQuery()
            conUpdateCustomer.Close()
            lblMessage.Text = ""
            '***********************************************************
            'Raise event and close window
            'Close web form
            HCustId.Value = strID
            HCustName.Value = txtFName.Text & " " & txtLName.Text
            btnUpdate.Visible = True
        Else
            lblMessage.Text = "There must be a first and last name."
            End If
        End If
    End Sub
    Private Sub btnFClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFClose.Click

        phCloseWindow.Visible = True

    End Sub
End Class
