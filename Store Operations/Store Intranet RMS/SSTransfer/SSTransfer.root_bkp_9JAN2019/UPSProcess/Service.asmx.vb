﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Net
Imports System.IO
Imports System.Security.Authentication

<System.Web.Services.WebService(Namespace:="http://HotTopic.SSTransfer.UPSProcess/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class UPSProcessService
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function CreateShipmentLabel(ByVal URL As String, ByVal strXMLRequest As String, ByRef objUpsReponse As Object) As Boolean

        Dim objXMLRequest As HttpWebRequest
        'Dim strErrorCode As String
        'Dim strErrorDescription As String
        'Dim strMinimumRetrySeconds As String
        'Dim strResponseStatusCode As String
        Dim booReturn As Boolean = 0
        'Dim IOStream As System.IO.StreamWriter
        Dim objXMLResponse As HttpWebResponse

        Try

            ServicePointManager.SecurityProtocol = DirectCast(3072, System.Net.SecurityProtocolType)
            objXMLRequest = CType(WebRequest.Create(URL), HttpWebRequest)

            'Configure HttpWebRequest object
            objXMLRequest.ContentType = "application/x-www-form-urlencoded"
            objXMLRequest.Method = "POST"
            objXMLRequest.ContentLength = Len(strXMLRequest)
            objXMLRequest.Timeout = ConfigurationSettings.AppSettings("UPSTimeOut")
            objXMLRequest.KeepAlive = False
            objXMLRequest.Connection = ConfigurationSettings.AppSettings("UPSHTTPConnections")

            Using IOStream As New System.IO.StreamWriter(objXMLRequest.GetRequestStream())
                IOStream.Write(strXMLRequest)
                IOStream.Flush()
            End Using

            objXMLResponse = CType(objXMLRequest.GetResponse(), HttpWebResponse)

            'IOStream = New System.IO.StreamWriter(objXMLRequest.GetRequestStream())

            'IOStream.Write(strXMLRequest)
            'IOStream.Flush()

            'Save Response from UPS
            'objXMLResponse = CType(objXMLRequest.GetResponse(), HttpWebResponse)
            'IOStream.Close()

            'Parse through response
            If objXMLResponse.StatusCode <> HttpStatusCode.OK Then
                objXMLResponse.Close()
                Throw New Exception(objXMLResponse.StatusCode.ToString() + vbNewLine + objXMLResponse.StatusDescription.ToString())
            Else
                Using streamreader As New StreamReader(objXMLResponse.GetResponseStream(), System.Text.Encoding.UTF8)
                    objUpsReponse = CType(streamreader.ReadToEnd(), Object)
                End Using
                booReturn = True
            End If

            objXMLResponse.Close()

        Catch ex As Exception
            'System.Diagnostics.EventLog.WriteEntry("UPSAPIFailure", ex.ToString())
            'LogXMLError(m_strErrorSeverity, strErrorCode, ex.Message, strMinimumRetrySeconds, String.Empty, strXMLRequest, strXMLRequestType)
            'objXMLResponse.Close()
            objUpsReponse = CType(ex.ToString(), Object)
            booReturn = False
        End Try
        Return booReturn
    End Function
    <WebMethod()>
    Public Function TrackShipment(ByVal UpsXmlPostURL As String, ByVal _XmlReq As String, ByRef HttpPostFailCnt As Int32, ByRef svcErrorMsg As String) As String
        'Dim blnSuccess As Boolean
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHttpWebResponse As HttpWebResponse
        Dim objStreamReader As StreamReader
        Dim objStreamWriter As StreamWriter
        'Dim _XmlReq As String = "<?xml version='1.0'?><AccessRequest xml:lang='en-US'><AccessLicenseNumber>AD097444C1CD2A66</AccessLicenseNumber><UserId>HTUPSAPI</UserId><Password>ProteusControl2016</Password></AccessRequest><?xml version='1.0'?><TrackRequest xml:lang='en-US'><Request><TransactionReference><CustomerContext>HTUPSAPI</CustomerContext><XpciVersion>1.0001</XpciVersion></TransactionReference><RequestAction>Track</RequestAction><RequestOption>activity</RequestOption></Request><TrackingNumber>1Z758W850313835838</TrackingNumber></TrackRequest>"
        Dim strXMLResponse As String = ""

        Try
            ServicePointManager.SecurityProtocol = DirectCast(3072, System.Net.SecurityProtocolType)
            'If ServiceProvider = "UPS" Then
            ' objHttpWebRequest = CType(WebRequest.Create(UpsXmlPostURL), HttpWebRequest)
            'Else
            'ElseIf ServiceProvider = "FedEx" Then
            'objHttpWebRequest = CType(WebRequest.Create(""), HttpWebRequest)
            'Throw New Exception(ServiceProvider & " - ServiceProvider is not supported in UPSProcessService::TrackShipment method")
            'End If

            objHttpWebRequest = CType(WebRequest.Create(UpsXmlPostURL), HttpWebRequest)

            'Configure HttpWebRequest object
            With objHttpWebRequest
                .ContentType = "application/xml" ' "application/x-www-form-urlencoded"
                .Method = "POST"
                .ContentLength = Len(_XmlReq)
                '.Timeout = 100000 
                .KeepAlive = True
                '.Connection = " "
            End With

            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())

            objStreamWriter.Write(_XmlReq)
            objStreamWriter.Flush()

            'Get Response from UPS
            objHttpWebResponse = CType(objHttpWebRequest.GetResponse(), HttpWebResponse)

            objStreamWriter.Close()
            'Parse through response
            If objHttpWebResponse.StatusCode <> HttpStatusCode.OK Then
                HttpPostFailCnt += 1
                svcErrorMsg = "Missing UPS/FedEx XML Response"
                'Throw New Exception("Missing UPS/FedEx XML Response")
                'UpsXmlException("Missing UPS/FedEx XML Response")
            Else
                objStreamReader = New StreamReader(objHttpWebResponse.GetResponseStream(), Encoding.UTF8)
                strXMLResponse = objStreamReader.ReadToEnd()
                'SuccessCnt += 1
            End If
        Catch ex As Exception
            HttpPostFailCnt += 1
            svcErrorMsg = ex.ToString()
            'Throw New Exception(ex.ToString()) 'UpsXmlException(ex.Message)
        End Try
        Return strXMLResponse

    End Function
End Class