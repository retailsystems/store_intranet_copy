<%@ Page Language="vb" AutoEventWireup="false" Codebehind="GenerateUPS.aspx.vb" Inherits="SSTransfer.GenerateUPS" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>GenerateUPS</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
	</HEAD>
	<body class="sBody" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form2" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="4" width="100%">
				<tr>
					<td vAlign="top" width="98%" height="98%"><font face="arial" size="4"><b>UPS Internet 
								Shipping: View/Print Label</b></font>
						<OL>
							<li>
								<font face="arial" size="2"><b>Print the label:</b> Press the Print button at the 
									bottom of the screen to print the label below.</font><br>
								<br>
							<li>
								<font face="arial" size="2"><b>Fold the printed label at the line.</b> Place the 
									label in a UPS Shipping Pouch. If you do not have a<br>
									pouch, affix the folded label using clear plastic shipping tape over the entire 
									label. </font>
								<br>
								<br>
							<LI>
								<font face="arial" size="2"><b>Getting Your Shipment to UPS<br>
									</b>
									<br>
									<b>Customers with a Daily Pickup</b>
							</LI>
							<UL type="circle">
								<li>
									Your driver will pickup your shipment(s) as usual.
								</li>
							</UL>
						</OL>
						<P>&nbsp;</P>
						<P>&nbsp;</P>
						<P>&nbsp;</P>
						</FONT>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td vAlign="bottom" colSpan="2" height="1%"><font face="arial" size="4">FOLD HERE</font>
						<hr width="100%" color="#FFFFFF">
						<br>
						<br>
						<br>
					</td>
				</tr>
				<tr>
					<td colSpan="2" height="1%"><asp:image id="ShippingLabel" Runat="server" Height="400" Width="700"></asp:image></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
