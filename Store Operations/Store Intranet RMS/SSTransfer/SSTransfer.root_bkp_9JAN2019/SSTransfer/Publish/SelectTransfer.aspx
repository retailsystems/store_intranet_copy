<%@ Register TagPrefix="uc1" TagName="ScanUPS" Src="UserControls/ScanUPS.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SelectTransfer.aspx.vb" Inherits="SSTransfer.SelectTransfer" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SearchBox" Src="UserControls/SearchBox.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Login</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server" />
	</HEAD>
	<body class="sBody" id="pageBody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0" runat="server">
		<form id="Form1" onsubmit="return checkSubmit();" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td>
						<table height="1%" cellSpacing="0" cellPadding="4" width="100%">
							<tr>
								<td align="middle"><uc1:scanups id="ucScanUPS" runat="server"></uc1:scanups></td>
							</tr>
						</table>
						<hr width="100%" class="sHr">
						<table cellSpacing="0" cellPadding="4" width="100%" border="0">
							<tr>
								<td class="sInfo"><asp:label id="lblExpectedArrivals" runat="server">Expected Arrivals</asp:label></td>
							</tr>
						</table>
						<table cellSpacing="0" cellPadding="5" width="100%" border="0">
							<tr>
								<td>
									<table cellSpacing="0" cellPadding="5" width="100%" class="sDialogTable">
										<tr>
											<td vAlign="top" align="middle">
												<div style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 360px" class="sDatagrid">
													<table cellSpacing="0" cellPadding="2" width="100%" border="0">
														<tr>
															<td><asp:datagrid id="grdArrivals" CssClass="sDatagrid" runat="server" AutoGenerateColumns="False" EnableViewState="False" Width="100%" CellPadding="2" CellSpacing="1" AllowSorting="True" OnSortCommand="SortCommand_Arrivals">
																	<AlternatingItemStyle cssclass="sDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:HyperLinkColumn SortExpression="1" DataTextField="Box_Id" HeaderText="Transfer #" DataNavigateUrlField="Box_Id" datanavigateurlformatstring="CheckInTransfer.aspx?BID={0}">
																			<HeaderStyle CssClass="sDatagridHeader"></HeaderStyle>
																		</asp:HyperLinkColumn>
																		<asp:TemplateColumn HeaderStyle-Width="120" HeaderText="Type">
																			<HeaderStyle CssClass="sDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:Label Runat="server" CssClass="sDatagrid" ID="Label1">
																					<%# GetBoxTransferTypes(Container.dataitem("Box_Id")) %>
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:BoundColumn SortExpression="3" DataField="Sending_Store_Name" HeaderText="Sent From">
																			<HeaderStyle CssClass="sDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderText="Tracking #">
																			<HeaderStyle CssClass="sDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:HyperLink Target=_blank CssClass="sDatagrid" ID="Hyperlink1" Runat=server NavigateUrl='<%# Replace(Container.dataitem("TrackingURL"),"{0}", Container.dataitem("Tracking_Num")) %>'>
																					<%# Container.dataitem("Tracking_Num") %>
																				</asp:HyperLink>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:BoundColumn DataField="Status_Desc" HeaderText="Status">
																			<HeaderStyle CssClass="sDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn SortExpression="5" DataFormatString="{0:d}" DataField="Shipment_Date" HeaderText="Ship Date">
																			<HeaderStyle CssClass="sDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
