<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Transfer.aspx.vb" Inherits="SSTransfer.Transfer1" smartNavigation="False"%>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Admin - Transfer</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Hottopic.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
	</HEAD>
	<body class="sBody" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmDebug" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1%"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="center" align="middle">
						<table height="100%" cellSpacing="0" cellPadding="4" width="100%">
							<tr>
								<td vAlign="top" width="100%"><font face="arial" color="#ff0000" size="2"><b>Item Type 
											Config</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td align="middle">
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 220px">
													<table cellSpacing="0" cellPadding="2" width="100%" border="0">
														<tr>
															<td><asp:datagrid id="grdItemTypeConfig" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="false" Width="100%" CellPadding="2" CellSpacing="1" AllowSorting="False" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="Xfer_Id" Visible="false"></asp:BoundColumn>
																		<asp:BoundColumn DataField="Config_Type" HeaderText="Config Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Xfer_Cd" HeaderText="Item Trans Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="TType" HeaderText="Transfer Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="SendingStore" HeaderText="Sending Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="SendingGroup" HeaderText="Sending Group">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ReceivingStore" HeaderText="Receiving Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ReceivingGroup" HeaderText="Receiving Group">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Delete">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:CheckBox Runat="server" ID="chkDeleteItemTypeConfig"></asp:CheckBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right">
												<input type="button" value="Add Item Type" onclick="javasvript:window.open('AddForms_Transfer/AddItemTypeConfig.aspx','ItemType','center=1,scroll=0,status=n0,Width=650,Height=200');" class="sButton">&nbsp;
												<asp:button id="btnDeleteItemType" runat="server" EnableViewState="False" CssClass="sButton" Text="Delete"></asp:button>
											</td>
										</tr>
									</table>
									<font face="arial" color="#ff0000" size="2"><b>Route Config</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td>
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 220px">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td><asp:datagrid id="dgRouteConfig" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="false" Width="100%" CellPadding="2" CellSpacing="1" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="Transfer_Config_Id" Visible="false"></asp:BoundColumn>
																		<asp:BoundColumn DataField="Config_Type" HeaderText="Config Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="TType" HeaderText="Transfer Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="SendingStore" HeaderText="Sending Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="SendingGroup" HeaderText="Sending Group">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ReceivingStore" HeaderText="Receiving Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ReceivingGroup" HeaderText="Receiving Group">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Start_Date" HeaderText="Start">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="End_Date" HeaderText="End">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Delete">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:CheckBox Runat="server" ID="chkDeleteRouteConfig"></asp:CheckBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right">
												<input type="button" value="Add Route Config" onclick="javasvript:window.open('AddForms_Transfer/AddRouteConfig.aspx','RouteConfig','center=1,scroll=0,status=0,Width=650,Height=250,resizable=0');" class="sButton">&nbsp;
												<asp:button id="btnDeleteTransferRoute" runat="server" EnableViewState="False" CssClass="sButton" Text="Delete"></asp:button>
											</td>
										</tr>
									</table>
									<font face="arial" color="#ff0000" size="2"><b>Box Includes Config</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td>
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 220px">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td><asp:datagrid id="dgBoxIncludesConfig" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="false" Width="100%" CellPadding="2" CellSpacing="1" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="Config_Id" Visible="false"></asp:BoundColumn>
																		<asp:BoundColumn DataField="Config_Type" HeaderText="Config Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Includes_Desc" HeaderText="Box Include Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="TType" HeaderText="Transfer Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="SendingStore" HeaderText="Sending Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="SendingGroup" HeaderText="Sending Group">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ReceivingStore" HeaderText="Receiving Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ReceivingGroup" HeaderText="Receiving Group">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Delete">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:CheckBox Runat="server" ID="chkDeleteBoxIncludeConfig"></asp:CheckBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right">
												<input type="button" value="Add Box Include" onclick="javasvript:window.open('AddForms_Transfer/AddBoxIncludesConfig.aspx','BoxIncludeConfig','center=1,scroll=0,status=0,Width=650,Height=250,resizable=0');" class="sButton">&nbsp;
												<asp:button id="btnDeleteBoxInclude" runat="server" EnableViewState="False" CssClass="sButton" Text="Delete"></asp:button>
											</td>
										</tr>
									</table>
									<font face="arial" color="#ff0000" size="2"><b>Default Receiving Store</b></font>
									<table class="sInfoTable" cellSpacing="0" cellPadding="0" width="100%">
										<tr>
											<td>
												<div class="sInfoDatagrid" style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 220px">
													<table cellSpacing="0" cellPadding="2" width="100%">
														<tr>
															<td><asp:datagrid id="dgDefaultStore" runat="server" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="false" Width="100%" CellPadding="2" CellSpacing="1" CssClass="sInfoDatagrid">
																	<AlternatingItemStyle cssclass="sInfoDatagridAlternatingItem"></AlternatingItemStyle>
																	<Columns>
																		<asp:BoundColumn DataField="Receiving_Default_Id" Visible="false"></asp:BoundColumn>
																		<asp:BoundColumn DataField="DefaultStore" HeaderText="Default Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="TType" HeaderText="Transfer Type">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="SendingStore" HeaderText="Sending Store">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="SendingGroup" HeaderText="Sending Group">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																		</asp:BoundColumn>
																		<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Delete">
																			<HeaderStyle cssclass="sInfoDatagridHeader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:CheckBox Runat="server" ID="chkDeleteDefaultStore"></asp:CheckBox>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																</asp:datagrid></td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td align="right">
												<input type="button" value="Add Default Store" onclick="javasvript:window.open('AddForms_Transfer/AddDefaultStore.aspx','DefaultReceivingStore','center=1,scroll=0,status=0,Width=650,Height=250,resizable=0');" class="sButton">&nbsp;
												<asp:button id="btnDeleteDefaultStore" runat="server" EnableViewState="False" CssClass="sButton" Text="Delete"></asp:button>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
