<%@ Register TagPrefix="uc1" TagName="TransferBox" Src="UserControls/TransferBox.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewTransferForm.aspx.vb"
    Inherits="SSTransfer.NewTransferForm" SmartNavigation="False" EnableViewState="True"
    Trace="False" %>

<%@ Register TagPrefix="uc1" TagName="TransferHeader" Src="UserControls/TransferHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ScanItem" Src="UserControls/ScanItem.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Transfer Home</title>
    <meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
    <meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script language="JavaScript" src="Javascript/TrapKeyPress.js"></script>
    <script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
    <script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
    <script language="JavaScript" src="Javascript/jquery-3.3.1.min.js"></script>
    <link id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css"
        rel="stylesheet" runat="server"></link>
    <style type="text/css">
        .style1
        {
            width: 1227px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setWidth();
        });
        function setWidth() {
            $('#footerTable').width($('#divTransferBox').width());
            var typeHeader =$('.typeHeader')[0];
            var imgHeader = $('.imgHeader')[0];
            var col =$('#lastColumn');
            $(col).width($(typeHeader).width() + $(imgHeader).width() +5);
            var totalcol = $('#totalColumn');
            var shippedQtyHeader = $('.qtyShipped')[0];
            $(totalcol).width($(shippedQtyHeader).width());
            //$(col).html(($(typeHeader).width() - $(imgHeader).width()).toFixed(2));
        }
        
    </script>
</head>
<body onresize="setWidth()" class="sBody" id="pageBody" bottommargin="0" leftmargin="0" topmargin="0" scroll="no"
    rightmargin="0" runat="server">
    <form id="frmTransferHome" onsubmit="return checkSubmit();" method="post" runat="server">
    <table height="100%" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td height="1">
                <uc1:header id="layoutHeader" runat="server"></uc1:header>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <table cellspacing="0" cellpadding="4" width="100%">
                    <tr>
                        <td align="center">
                            <uc1:transferheader id="ucTransferHeader" runat="server"></uc1:transferheader>
                        </td>
                    </tr>
                </table>
                <hr class="sHr" width="100%">
                <table class="sScanItemDarkBG" cellspacing="0" cellpadding="4" width="100%">
                    <tr>
                        <td align="center">
                            <uc1:scanitem id="ucScanItem" runat="server"></uc1:scanitem>
                        </td>
                    </tr>
                </table>
                <hr class="sHr" width="100%">
                <table cellspacing="0" cellpadding="4" width="100%" border="0">
                    <tr>
                        <td align="left">
                            <div id="divASDLegend" runat="server">
                                <table cellspacing="0" cellpadding="4" border="0">
                                    <tr>
                                        <td>
                                            <font class="sNewTransFormTitle">Items that should go in the Monday Mail Pack:</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="sNormalBig">
                                            � Media older than 60 days
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="sNormalBig">
                                            � Bankcard Draft envelopes (including signed credit card slips and deposit slips
                                            and log)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="sNormalBig">
                                            � HR Accordion (only if there is&nbsp;paperwork inside)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="sNormalBig">
                                            � Expense Reports (including concert and misc. employee expense reports) with all
                                            the receipts attached
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="sNormalBig">
                                            � Mail or other items for anyone at HQ
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="sNormalBig">
                                            � Mail to other stores (may take up to 1 month to arrive at the receiving store)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="sNormalBig">
                                            � Do NOT send any gift cards
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divTransferBox" style="overflow: auto; width: 100%; max-height: 200px" runat="server">
                                <uc1:transferbox id="ucTransferBox" runat="server"></uc1:transferbox>
                            </div>
                            <table id="footerTable" style="background-color: #dcddde;border-top: 1px solid;">
                                <tbody>
                                    <tr>
                                   
                                    <td align="right"> <asp:label id="lblShippedQuantity" 
                                                runat="server" text="Total Quantity:" cssclass="sScanItemBlackLabel"></asp:label></td>
                                        <td align="center" id="totalColumn" style="border-left:1px solid gray;border-right:1px solid gray;">
                                           
                                            <asp:label id="lblQuantityTotal" 
                                                runat="server" text="0" cssclass="sScanItemBlackLabel"></asp:label>
                                        </td>
                                        <td  id="lastColumn">
                                        </td>
                                    </div>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                <%--                         <table cellSpacing="0" cellPadding="4" width="100%">
                          <tr>
                             <td>
                                <asp:Label ID="lblShippedQuantity" style="float:left;margin-top:5px;MAX-HEIGHT: 200px;height :100%;" runat="server" Text="Total Quantity:" cssclass="sScanItemBlackLabel"></asp:Label>
                                <asp:Label ID="lblQuantityTotal" style="float:left;margin-top:5px;MAX-HEIGHT: 200px;height:100%;" runat="server" Text="0" cssclass="sScanItemBlackLabel"></asp:Label>
                             </td>
                          </tr>
                         </table>--%>
                <table cellspacing="0" cellpadding="4" width="100%">
                    <tr>
                        <td align="center">
                            <asp:button id="btnVoid" runat="server" visible="False" text="Void" enableviewstate="False"
                                cssclass="sButton"></asp:button>
                            <asp:button id="btnUpdate" runat="server" visible="False" text="Update" enableviewstate="False"
                                cssclass="sButton"></asp:button>
                            &nbsp;
                            <asp:button id="btnCancel" runat="server" visible="False" text="Cancel" enableviewstate="False"
                                cssclass="sButton"></asp:button>
                            <asp:button id="btnEdit" runat="server" text="Edit" enableviewstate="False" cssclass="sButton"></asp:button>
                            &nbsp;
                            <asp:button id="btnSave" runat="server" text="Save for Later" enableviewstate="False"
                                cssclass="sButton"></asp:button>
                            &nbsp;
                            <asp:button id="btnCancelTransfer" runat="server" text="Cancel" enableviewstate="False"
                                cssclass="sButton"></asp:button>
                            &nbsp;
                            <asp:button id="btnSubmit" runat="server" text="Complete" enableviewstate="False"
                                cssclass="sButton"></asp:button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="1">
                <uc1:footer id="Footer1" runat="server"></uc1:footer>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
