<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ViewUPSAccounts.aspx.vb" Inherits="SSTransfer.ViewUPSAccounts"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>UPS Accounts</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
	</HEAD>
	<body class="sBody" id="pageBody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0" runat="server">
		<form id="frmAddGroup" onsubmit="return checkSubmit();" method="post" runat="server">
			<table class="sInfoTable" height="100%" cellSpacing="0" cellPadding="1" width="100%">
				<tr>
					<td class="sDialogHeader" align="middle" colSpan="2" height="1%"><asp:label id="lblHeader" runat="server" EnableViewState="False"></asp:label></td>
				</tr>
				<tr>
					<td align="middle" height="99%">
						<table width="100%">
							<tr>
								<td><asp:dropdownlist id="ddlStore" Width="100%" Runat="server" DataValueField="StoreNum" DataTextField="StoreName"></asp:dropdownlist></td>
								<td align="right" width="1%"><asp:button id="btnAddStore" CssClass="sButton" Runat="server" Text="Add Store"></asp:button></td>
							</tr>
						</table>
						<br>
						<table cellSpacing="0" cellPadding="2" width="100%">
							<tr>
								<td colSpan="2"><asp:label id="lblStores" runat="server" CssClass="Plain">Stores:</asp:label></td>
							</tr>
							<tr>
								<td colSpan="2"><asp:listbox id="lbStores" Width="100%" Rows="18" Runat="server" DataValueField="StoreNum" DataTextField="StoreName"></asp:listbox></td>
							</tr>
						</table>
						<br>
						<table width="100%">
							<tr>
								<td align="left"><input class="sButton" onclick="javascript:window.close();" type="button" value="Cancel">
								</td>
								<td align="right"><asp:button id="btnDeleteStore" CssClass="sButton" Runat="server" Text="Delete Store(s)"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
