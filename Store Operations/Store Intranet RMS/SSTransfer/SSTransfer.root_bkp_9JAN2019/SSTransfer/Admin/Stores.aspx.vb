
Public Class Stores
    Inherits System.Web.UI.Page

    Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents grdGroups As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnDeleteGroup As System.Web.UI.WebControls.Button
    Protected WithEvents dgStores As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnDeleteStore As System.Web.UI.WebControls.Button

    Private m_objUserInfo As New UserInfo()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strLegend As String
        Dim objSecurity As New Security(ConfigurationSettings.AppSettings("strSecurityConn"), ConfigurationSettings.AppSettings("ProjectId"))
        
        m_objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        'If employee is logged on update last Trans
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
        End If

        If Not objSecurity.ModuleAuthorized(m_objUserInfo.EmpId, "STORE", "STORE.ASPX") Then Response.Redirect("Unauthorized.aspx")

        'Sets both header title and highlights section button
        ucHeader.lblTitle = "SSTransfer Admin"
		ucHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Admin)
		ucHeader.CurrentPage(SSTransfer.Header.PageName.Store)

        GetStoreGroups()
        GetStoresFromGroup(Request("GroupId"))

    End Sub

    Public Function GetGroupId() As String

        Return Request("GroupId")

    End Function

    Private Sub GetStoreGroups()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetStoreGroups"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        grdGroups.DataSource = objDataReader
        grdGroups.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub GetStoresFromGroup(ByVal GroupId As Integer)

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetStoresFromGroup " & GroupId

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        dgStores.DataSource = objDataReader
        dgStores.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub btnDeleteGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteGroup.Click

        Dim I As Integer
        Dim dgi As DataGridItem
        Dim chkDeleteGroup As CheckBox
        Dim strTemp As String
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        For I = 0 To grdGroups.Items.Count - 1
            dgi = grdGroups.Items(I)
            chkDeleteGroup = CType(dgi.FindControl("chkDeleteGroup"), CheckBox)

            If chkDeleteGroup.Checked Then
                strTemp += dgi.Cells(0).Text & ","
            End If

        Next

        If Len(strTemp) > 0 Then
            strTemp = Mid(strTemp, 1, strTemp.Length - 1)

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spDeleteStoreGroupType '" & strTemp & "' "

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            GetStoreGroups()
            GetStoresFromGroup(Request("GroupId"))

        End If


    End Sub

    Private Sub btnDeleteStore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteStore.Click

        Dim I As Integer
        Dim dgi As DataGridItem
        Dim chkDeleteStore As CheckBox
        Dim strTemp As String
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        For I = 0 To dgStores.Items.Count - 1
            dgi = dgStores.Items(I)
            chkDeleteStore = CType(dgi.FindControl("chkDeleteStore"), CheckBox)

            If chkDeleteStore.Checked Then
                strTemp += dgi.Cells(0).Text & ","
            End If

        Next

        If Len(strTemp) > 0 Then
            strTemp = Mid(strTemp, 1, strTemp.Length - 1)

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spDeleteStoreGroup '" & strTemp & "' "

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            GetStoreGroups()
            GetStoresFromGroup(Request("GroupId"))

        End If


    End Sub

    Private Sub grdGroups_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdGroups.ItemCreated

        If e.Item.DataItem Is Nothing Then Exit Sub

        Dim tmpStoreGroupId As Integer = DataBinder.Eval(e.Item.DataItem, "StoreGroup_Id")

        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            If tmpStoreGroupId = Request("GroupId") Then
                e.Item.BackColor = System.Drawing.Color.FromName("#FF0000")
            End If
        End If
    End Sub

End Class
