Public Class AddUPSDefaultShipment
    Inherits System.Web.UI.Page

    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
    Protected WithEvents lblPackage As System.Web.UI.WebControls.Label
    Protected WithEvents ddlPackage As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblService As System.Web.UI.WebControls.Label
    Protected WithEvents ddlService As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblTType As System.Web.UI.WebControls.Label
    Protected WithEvents ddlTType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblSendingStore As System.Web.UI.WebControls.Label
    Protected WithEvents ddlSendingStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblReceivingStore As System.Web.UI.WebControls.Label
    Protected WithEvents ddlReceivingStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnAddDefaultShipment As System.Web.UI.WebControls.Button
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        If Not Page.IsPostBack Then
            FillUPSServiceCode()
            FillUPSPackageCode()
            FillTType()
            FillRStores()
            FillSStores()
        End If

    End Sub

    Private Sub FillRStores()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("asFillTStore", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlReceivingStore.DataSource = objDataReader
        ddlReceivingStore.DataBind()

        objItem.Text = "<All Stores>"
        objItem.Value = "NULL"
        ddlReceivingStore.Items.Insert(0, objItem)

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub FillSStores()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("asFillTStore", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlSendingStore.DataSource = objDataReader
        ddlSendingStore.DataBind()

        objItem.Text = "<All Stores>"
        objItem.Value = "NULL"
        ddlSendingStore.Items.Insert(0, objItem)

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub btnAddDefaultShipment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddDefaultShipment.Click

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spInsertUPSDefaultShipment '" & ddlPackage.SelectedItem.Value & "','" & ddlService.SelectedItem.Value & "'," & ddlTType.SelectedItem.Value & "," & ddlSendingStore.SelectedItem.Value & "," & ddlReceivingStore.SelectedItem.Value

        objCommand.CommandText = strStoredProcedure

        objCommand.ExecuteNonQuery()
        objCommand.Dispose()

        pageBody.Attributes.Add("onload", "window.opener.location=window.opener.location;")

    End Sub


    Private Sub FillUPSServiceCode()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spGetUPSServiceCodes NULL,NULL,NULL,NULL,NULL", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlService.DataSource = objDataReader
        ddlService.DataBind()

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub FillUPSPackageCode()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spGetUPSPackageTypes NULL,NULL,NULL", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlPackage.DataSource = objDataReader
        ddlPackage.DataBind()

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub FillTType()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spGetTType", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlTType.DataSource = objDataReader
        ddlTType.DataBind()

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub


End Class
