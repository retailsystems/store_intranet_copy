Public Class NewTransferForm
    Inherits System.Web.UI.Page
    Protected WithEvents btnScanItem As System.Web.UI.WebControls.Button
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents layoutHeader As Header
    Protected WithEvents ucTransferBox As TransferBox
    Protected WithEvents ucScanItem As ScanItem
    Protected WithEvents btnEdit As System.Web.UI.WebControls.Button
    Protected WithEvents btnUpdate As System.Web.UI.WebControls.Button
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents btnVoid As System.Web.UI.WebControls.Button
    Protected WithEvents ucTransferHeader As TransferHeader
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divTransferBox As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnCancelTransfer As System.Web.UI.WebControls.Button
    Protected WithEvents divASDLegend As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblQuantityTotal As System.Web.UI.WebControls.Label
    Protected WithEvents lblShippedQuantity As System.Web.UI.WebControls.Label
    Private objUserInfo As New UserInfo() 'User Info Object
    Private objTransferLock As New TransferLock() ' Locking Object
    Protected WithEvents btnDDS As System.Web.UI.WebControls.Button

    Private m_TType As Int32 ' Return/Store to Store

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Get employee info from Session /  cookies
        objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        'If employee is logged on to the system then their locks are kept alive
        If objUserInfo.EmpOnline Then
            objUserInfo.KeepLogonAlive()
            objTransferLock.KeepLocksAlive(objUserInfo.EmpId)
        Else
            Response.Redirect("SessionEnd.aspx?Mode=1")
        End If

        'If store number not set then redirect to set store page
        If objUserInfo.StoreNumber = -1 Or objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

        'Clear error pop up
        pageBody.Attributes.Add("onload", "")

        'Set confirm popups
        btnCancelTransfer.Attributes.Add("onclick", "return confirm('Are you sure you want to cancel the current transfer?')")
        btnVoid.Attributes.Add("onclick", "return confirm('Are you sure you want to void the current transfer?')")

        'Hide transfer box columns that don't apply to sending a transfer
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyReq
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyShipped_Edit
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyReceived_Edit
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyReceived

        'Get querystring variables from URL 
        m_TType = Request.QueryString("TType")

        'Set header properties
		layoutHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Transfer)
		layoutHeader.CurrentPage(SSTransfer.Header.PageName.NewTransfer)
        layoutHeader.DisableButtons()
        Select Case m_TType
            Case 1
                layoutHeader.lblTitle = "Store to Store"
            Case 2
                layoutHeader.lblTitle = "Return to DC/HQ"
            Case 3
                layoutHeader.lblTitle = "HQ Mail Pack"
                lblQuantityTotal.Visible = False
                lblShippedQuantity.Visible = False
            Case 4
                layoutHeader.lblTitle = "DDS Return"
        End Select


        'Set scan Item properties
        Select Case m_TType
            Case 1
                ucScanItem.SetForm = "SENDING"
                divASDLegend.Visible = False
                ucScanItem.RAVisible = False
                ucTransferHeader.ShowRStore = True

            Case 2
                ucScanItem.SetForm = "SENDING"
                divASDLegend.Visible = False
                ucScanItem.RAVisible = False
                ucTransferHeader.ShowDDLRStore = True
            Case 3
                ucScanItem.SKUVisible = False
                ucScanItem.CustomerInfoVisible = False
                ucScanItem.TransferSelectionVisible = False
                btnEdit.Visible = False
                btnSave.Visible = False
                divASDLegend.Visible = True
                divTransferBox.Visible = False
                ucScanItem.RAVisible = False
                ucTransferHeader.ShowRStore = True
            Case 4
                ucScanItem.SKUVisible = False
                ucScanItem.CustomerInfoVisible = False
                ucScanItem.TransferSelectionVisible = False
                btnEdit.Visible = False
                btnSave.Visible = False
                divASDLegend.Visible = False
                divTransferBox.Visible = False
                ucScanItem.RAVisible = True
                divASDLegend.Visible = False
                btnSave.Visible = False
                ucTransferHeader.ShowRStore = True
            Case Else
                ucScanItem.SetForm = "SENDING"
                divASDLegend.Visible = False
                ucScanItem.RAVisible = False
                ucTransferHeader.ShowRStore = True

                'ucScanItem.CustomerInfoVisible = True
        End Select

        ucScanItem.SetTType = m_TType

        'Set Transfer Header Type (Returns\Store To Store)
        ucTransferHeader.TType = m_TType


        If Not Page.IsPostBack Then

            If Len(Request("BID")) > 0 Then
                ucTransferHeader.BoxId = Request("BID")
                ucTransferBox.Box_Id = Request("BID")
                ucTransferHeader.SStore = objUserInfo.StoreNumber
                'ucTransferHeader.EditRStore = True
                ucTransferHeader.LoadData()

                'Redirect if status has changed
                If ucTransferHeader.BoxStatus <> 3 And ucTransferHeader.BoxStatus <> 6 Then
                    Response.Redirect("ViewTransfer.aspx?L=1&BID=" & Request("BID") & "&RET=default.aspx")
                End If

                If Not objTransferLock.LockTransfer(Request("BID"), objUserInfo.EmpId, Session.SessionID) Then
                    Response.Redirect("ViewTransfer.aspx?L=1&BID=" & Request("BID") & "&RET=default.aspx")
                End If

            Else
                ucTransferHeader.CreateNewHeader(objUserInfo.StoreNumber, 1, objUserInfo.EmpId, objUserInfo.EmpFullName)
                ucTransferBox.Box_Id = ucTransferHeader.BoxId
                ucTransferBox.Sending_Store = ucTransferHeader.SStore
                'ucTransferHeader.EditRStore = True
                ucTransferHeader.LoadData()
            End If

            'ucTransferHeader.EditRStore = True
            ' ucTransferHeader.LoadData()

        End If

       
        ucTransferBox.LoadData()
        'If Len(Request("BID")) > 0 Then

        ' ucTransferHeader.BoxId = Request("BID")
        '   ucTransferHeader.LoadData()

        'Redirect if status has changed
        '  If ucTransferHeader.BoxStatus <> 3 And ucTransferHeader.BoxStatus <> 6 Then
        '         Response.Redirect("ViewTransfer.aspx?BID=" & Request("BID"))
        '     End If

        '        If Not objTransferLock.LockTransfer(Request("BID"), objUserInfo.EmpId, Session.SessionID) Then
        '           Response.Redirect("ViewTransfer.aspx?BID=" & Request("BID"))
        '       End If

        '  End If

        If DisplayVoid() Then
            btnVoid.Visible = True
        End If

        If ViewState("Edit") = True Then

            ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyShipped
            ucTransferBox.ShowColumn = TransferBox.BoxColumnName.QtyShipped_Edit

            ucScanItem.EnableScan(False)

            btnSave.Visible = False
            btnSubmit.Visible = False
            btnEdit.Visible = False
            btnCancelTransfer.Visible = False

            btnUpdate.Visible = True
            btnCancel.Visible = True

        End If

        ucTransferHeader.GetData()

        ucScanItem.ReceivingStore = ucTransferHeader.RStore
        ucScanItem.SendingStore = ucTransferHeader.SStore

        If ucTransferHeader.RStore <= 0 Then
            ucTransferHeader.EnableBoxIncludes = False
            ucScanItem.EnableScan(False)
        Else
            ucTransferHeader.EnableBoxIncludes = True
            ucScanItem.EnableScan(True)
        End If

        If Len(Request("BID")) > 0 Then
            ucTransferHeader.EnableRStore = False
        End If
        lblQuantityTotal.Text = ucTransferBox.GetQuantityTotal()
    End Sub

    'Change Transer status to Void
    Private Sub btnVoid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoid.Click

        ucTransferHeader.EmpId = objUserInfo.EmpId
        ucTransferHeader.EmpFullName = objUserInfo.EmpFullName
        ucTransferHeader.BoxStatus = 7
        ucTransferHeader.Save()
        Response.Redirect("default.aspx")

    End Sub



    Sub ucScanItem_OnReceivedSKU(ByVal SKU_Num As String, ByVal Xfer_Transfer_Cd As Byte, ByVal Xfer_Transfer_Desc As String, ByVal Xfer_Cust_Id As Int32) Handles ucScanItem.OnReceivedSKU

        If Xfer_Transfer_Cd <> 0 Then

            If Xfer_Transfer_Cd = 3 And Xfer_Cust_Id = 0 Then
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Customer Name\Phone Required.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            Else
                If ucTransferBox.ItemCount > 0 Then
                    If ucTransferBox.IsXferInBox(5) Then
                        If Xfer_Transfer_Cd <> 5 Then
                            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please send RTV items only on this transfer") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                        Else
                            ucTransferBox.Box_Id = ucTransferHeader.BoxId
                            ucTransferBox.Sending_Store = ucTransferHeader.SStore
                            ucTransferBox.AddItemToBox(Xfer_Transfer_Cd, SKU_Num, Xfer_Cust_Id, 0, Xfer_Transfer_Desc)
                            ucTransferBox.RebindData()
                        End If
                    Else
                        If Xfer_Transfer_Cd = 5 Then
                            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please create a separate transfer for RTV items") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                        Else
                            ucTransferBox.Box_Id = ucTransferHeader.BoxId
                            ucTransferBox.Sending_Store = ucTransferHeader.SStore
                            ucTransferBox.AddItemToBox(Xfer_Transfer_Cd, SKU_Num, Xfer_Cust_Id, 0, Xfer_Transfer_Desc)
                            ucTransferBox.RebindData()
                        End If
                    End If
                Else
                    ucTransferBox.Box_Id = ucTransferHeader.BoxId
                    ucTransferBox.Sending_Store = ucTransferHeader.SStore
                    ucTransferBox.AddItemToBox(Xfer_Transfer_Cd, SKU_Num, Xfer_Cust_Id, 0, Xfer_Transfer_Desc)
                    ucTransferBox.RebindData()
                End If

            End If

        Else
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please select a Transfer Type.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
        End If

        If ucTransferBox.ItemCount > 0 Or Len(Request("BID")) > 0 Then
            ucTransferHeader.EnableRStore = False
        End If
        lblQuantityTotal.Text = ucTransferBox.GetQuantityTotal()
    End Sub

    Sub ucTransferBox_SKU(ByVal SKUNumber As String, ByVal ErrNumber As Byte, ByVal ErrDesc As String) Handles ucTransferBox.InValidSKU

        pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(ErrDesc) & "','Error','center=1;scroll=0;status=no;dialogWidth=380px;dialogHeight=200px;');")

    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If IsPageVaild() Then
            ucTransferHeader.BoxStatus = 6 'Saved Transfer
            ucTransferHeader.SStore = objUserInfo.StoreNumber
            ucTransferHeader.EmpId = objUserInfo.EmpId
            ucTransferHeader.EmpFullName = objUserInfo.EmpFullName
            ucTransferHeader.Save()
            ucTransferBox.Box_Id = ucTransferHeader.BoxId
            ucTransferBox.Save()
            ucTransferBox.InsertICStatus()
            ucTransferBox.ClearDataset()
            'objTransferLock.UnlockTransfer(ucTransferHeader.BoxId)
            Response.Redirect("CreateNewTrans.aspx")
        End If

    End Sub


    Private Function IsPageVaild() As Boolean

        Dim booValid As Boolean = True
        Dim strErrorMsg As String
        Dim strTemp As String
        Dim clsStore As New Store(objUserInfo.StoreNumber)

        If (Not ucTransferHeader.IsMerchandiseSeleted And ucTransferBox.ItemCount > 0) Then
            booValid = False
            strErrorMsg = strErrorMsg & "<br>Merchandise checkbox not checked."
            If Not ValidAmt() Then
                booValid = False
                strErrorMsg = strErrorMsg & "<br>" & "Total Amount can not exceed $" & ConfigurationSettings.AppSettings("mnyMaxTransferAmt")
            End If
        End If

        If (ucTransferHeader.IsMerchandiseSeleted And ucTransferBox.ItemCount <= 0) Then
            booValid = False
            strErrorMsg = strErrorMsg & "<br>Merchandise not included."
        End If

        If ucTransferHeader.IsBoxIncludesEmpty And ucTransferBox.ItemCount <= 0 Then
            booValid = False
            strErrorMsg = strErrorMsg & "<br>Box Includes is required."
        End If

        If ucTransferHeader.RStore = 0 Then
            booValid = False
            strErrorMsg = "Receiving Store Required."
        Else
            ' If Not clsStore.IsValidReceivingStore(ucTransferHeader.RStore, m_TType) Then
            '  booValid = False
            '  strErrorMsg = clsStore.ErrMsg
            ' Else

            '  strTemp = ucTransferHeader.GetInValidBoxInclude
            '  If Len(strTemp) > 0 Then
            '   booValid = False
            '    strErrorMsg = strErrorMsg & "<br>You cannot send the following:<br>" & strTemp & "<br><br>"
            ' End If
            ' End If
        End If

        If Not booValid Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(strErrorMsg) & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=200px;');")
        End If

        Return booValid

    End Function

    Private Function RStoreIsValid() As Boolean

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spIsValidStore", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim RStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@RStore", SqlDbType.SmallInt)
        Dim SStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@SStore", SqlDbType.SmallInt)
        Dim TTypeParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@TType", SqlDbType.TinyInt)
        Dim IsValidParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@IsValid", SqlDbType.Bit)

        RStoreParam.Direction = ParameterDirection.Input
        SStoreParam.Direction = ParameterDirection.Input
        TTypeParam.Direction = ParameterDirection.Input
        IsValidParam.Direction = ParameterDirection.Output

        RStoreParam.Value = ucTransferHeader.RStore
        SStoreParam.Value = objUserInfo.StoreNumber
        TTypeParam.Value = m_TType

        objCommand.ExecuteNonQuery()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return IsValidParam.Value

    End Function

    Private Sub CompleteTransfer()

        Dim strTotalWeight As String
        Dim booDDSValid As Boolean = True
        Dim decTransferWeight As Decimal

        If IsPageVaild() Then

            If m_TType = 4 Then

                Dim objDDSReturn As New DDSReturn(ConfigurationSettings.AppSettings("strDDSReturnConn"))

                If objDDSReturn.GetRAData(ucScanItem.RA, objUserInfo.StoreNumber) Then
                    decTransferWeight = objDDSReturn.RATransferWeight
                    booDDSValid = True
                Else
                    booDDSValid = False
                End If

            Else
                decTransferWeight = ucTransferBox.GetTotalWeight + ucTransferHeader.GetBoxIncludesLBS
                'Added below code to take care of UPS package total to 1 LB by vijay 
                If (decTransferWeight = 0) Then
                    decTransferWeight = System.Convert.ToDecimal(ConfigurationSettings.AppSettings("DefaultUPSPackageWeight"))
                End If
            End If

            If booDDSValid Then
                ucTransferHeader.RANum = ucScanItem.RA
                ucTransferHeader.BoxStatus = 6 'Packing
                ucTransferHeader.SStore = objUserInfo.StoreNumber
                ucTransferHeader.ShipmentDate = Now
                ucTransferHeader.EmpId = objUserInfo.EmpId
                ucTransferHeader.EmpFullName = objUserInfo.EmpFullName
                ucTransferHeader.Save()
                ucTransferBox.Box_Id = ucTransferHeader.BoxId
                ucTransferBox.Save()
                ucTransferBox.InsertICStatus()
                strTotalWeight = decTransferWeight
                ucTransferBox.ClearDataset()

                'objTransferLock.UnlockTransfer(ucTransferHeader.BoxId)
                Response.Redirect("SelectShipping.aspx?LBS=" & strTotalWeight & "&TType=" & m_TType & "&UPSOn=1&Err=0&BID=" & ucTransferHeader.BoxId & "&RA=" & ucScanItem.RA)
                'Response.Redirect("TransferProcessing.aspx?BID=" & ucTransferHeader.BoxId)
            Else
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("You have entered an incorrect RA number.  Please try again or contact the purchasing department to confirm the RA number.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")

            End If

        End If

    End Sub


    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        CompleteTransfer()

    End Sub

    Private Function ValidAmt() As Boolean

        If ucTransferBox.GetTotalAmt > ConfigurationSettings.AppSettings("mnyMaxTransferAmt") Then
            Return False
        Else
            Return True
        End If

    End Function

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyShipped
        ucTransferBox.ShowColumn = TransferBox.BoxColumnName.QtyShipped_Edit

        ucScanItem.EnableScan(False)

        btnSave.Visible = False
        btnSubmit.Visible = False
        btnEdit.Visible = False
        btnCancelTransfer.Visible = False

        btnUpdate.Visible = True
        btnCancel.Visible = True

        ViewState("Edit") = True
        ucTransferBox.RebindData()

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        ucScanItem.EnableScan(True)

        btnSave.Visible = True
        btnSubmit.Visible = True
        btnEdit.Visible = True
        btnCancelTransfer.Visible = True

        btnUpdate.Visible = False
        btnCancel.Visible = False

        ViewState("Edit") = False
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyShipped_Edit
        ucTransferBox.ShowColumn = TransferBox.BoxColumnName.QtyShipped

    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        ucTransferBox.UpdateEdit()
        lblQuantityTotal.Text = ucTransferBox.GetQuantityTotal()
        If ucTransferBox.ItemCount <= 0 And Not Len(Request("BID")) > 0 Then
            ucTransferHeader.EnableRStore = True
        End If

        ucScanItem.EnableScan(True)

        btnSave.Visible = True
        btnSubmit.Visible = True
        btnEdit.Visible = True
        btnCancelTransfer.Visible = True

        If DisplayVoid() Then
            btnVoid.Visible = True
        End If

        btnUpdate.Visible = False
        btnCancel.Visible = False

        ViewState("Edit") = False
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyShipped_Edit
        ucTransferBox.ShowColumn = TransferBox.BoxColumnName.QtyShipped

    End Sub


    Private Sub btnCancelTransfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelTransfer.Click
        Response.Redirect("default.aspx")
    End Sub

    Private Sub ucScanItem_OnReceivedRA() Handles ucScanItem.OnReceivedRA

        CompleteTransfer()

    End Sub

    Private Function DisplayVoid() As Boolean

        If ucTransferHeader.BoxStatus = 6 And _
        (objUserInfo.IsStoreManager Or objUserInfo.IsFullTimeAssistantManager _
         Or objUserInfo.IsPartTimeAssistantManager Or objUserInfo.IsDistrictManager) _
         And Len(Request("BID")) > 0 Then

            Return True

        Else

            Return False

        End If


    End Function

    Private Sub ucTransferHeader_OnReceivedRStore() Handles ucTransferHeader.OnReceivedRStore

        Dim clsTransfer As New Transfer()
        Dim ErrMsg As String

        ErrMsg = "The current policy does not permit packages to be shipped from Store #" & ucTransferHeader.SStore & " to Store #" & ucTransferHeader.RStore & ". If you believe you have received this message in error, please contact the Help Desk."

        ucScanItem.GetTransferTypes()
      
        If clsTransfer.IsTransferRestricted(ucTransferHeader.SStore, ucTransferHeader.RStore, m_TType) Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(ErrMsg) & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=200px;');")
            ucTransferHeader.ClearRStore()
            ucTransferHeader.EnableBoxIncludes = False
            ucScanItem.EnableScan(False)   
        End If

    End Sub

    Public Sub New()

    End Sub
End Class

   