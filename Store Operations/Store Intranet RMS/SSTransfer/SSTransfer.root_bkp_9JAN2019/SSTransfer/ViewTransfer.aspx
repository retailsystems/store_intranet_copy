<%@ Register TagPrefix="uc1" TagName="TransferBox" Src="UserControls/TransferBox.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewTransfer.aspx.vb"
    Inherits="SSTransfer.ViewTransfer" %>

<%@ Register TagPrefix="uc1" TagName="TransferHeader" Src="UserControls/TransferHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Transfer Home</title>
    <meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
    <meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
    <script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
    <link id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css"
        rel="stylesheet" runat="server" />
    <script language="JavaScript" src="Javascript/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            setWidth();
        });
        function setWidth() {
            $('#footerTable').width($('#divTransferBox').width());
            var typeHeader = $('.typeHeader')[0];
            var imgHeader = $('.imgHeader')[0];
            var col = $('#lastColumn');
            $(col).width($(typeHeader).width() + $(imgHeader).width() + 5);
            var totalcol = $('#totalColumn');
            var td1 = $('#totalColumnShipped');
            var shippedQtyHeader = $('.qtyReceived')[0];
            $(totalcol).width($(shippedQtyHeader).width());
            var shippedQtyHeader = $('.qtyShipped')[0];
            $(td1).width($(shippedQtyHeader).width());
            //$(col).html(($(typeHeader).width() - $(imgHeader).width()).toFixed(2));
        }
        
    </script>
</head>
<body onresize="setWidth()" id="pagebody" runat="server" bottommargin="0" leftmargin="0" topmargin="0"
    scroll="no" rightmargin="0">
    <form id="frmTransferHome" method="post" runat="server">
    <table height="100%" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td height="1">
                <uc1:header id="layoutHeader" runat="server"></uc1:header>
            </td>
        </tr>
        <tr>
            <td valign="top" align="middle">
                <table cellspacing="0" cellpadding="4" width="100%" border="0">
                    <tr>
                        <td align="middle">
                            <uc1:transferheader id="ucTransferHeader" runat="server"></uc1:transferheader>
                        </td>
                    </tr>
                </table>
                <hr width="100%" class="sHr">
                <table cellspacing="0" cellpadding="4" width="100%" border="0">
                    <tr>
                        <td align="left">
                            <div id="divTransferBox" style="overflow: auto; width: 100%; max-height: 200px">
                                <uc1:transferbox id="ucTransferBox" runat="server"></uc1:transferbox>
                            </div>
                            <table id="footerTable" style="background-color: #dcddde; border-top: 1px solid;">
                                <tbody>
                                    <tr>
                                        <td align="right">
                                            <asp:label id="lblShippedQuantity" runat="server" text="Total Quantity:" cssclass="sScanItemBlackLabel"></asp:label>
                                        </td>
                                        <td align="center" id="totalColumnShipped" style="border-left: 1px solid gray; border-right: 1px solid gray;">
                                            <asp:label id="lblShippedQuantityTotal" runat="server" text="0" cssclass="sScanItemBlackLabel"></asp:label>
                                           
                                        </td>
                                        <td align="center" id="totalColumn" style="border-left: 1px solid gray; border-right: 1px solid gray;">
                                            <asp:label id="lblQuantityTotal" runat="server" text="0" cssclass="sScanItemBlackLabel"></asp:label>
                                           
                                        </td>
                                          

                                        <td id="lastColumn">
                                        </td>
                                       
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:button id="btnReturn" runat="server" text="Return" cssclass="sButton"></asp:button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="1">
                <uc1:footer id="Footer1" runat="server"></uc1:footer>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
