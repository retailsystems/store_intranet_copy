<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LogOff.aspx.vb" Inherits="SSTransfer.LogOff" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Log Off</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
	</HEAD>
	<body bottomMargin="0" vLink="black" aLink="black" link="black" leftMargin="0" topMargin="0" rightMargin="0" scroll="yes">
		<form id="Form1" method="post" runat="server" onsubmit="return checkSubmit();">
			<table width="100%" height="100%" borderColor="#D60C8C" cellSpacing="0" borderColorDark="#D60C8C" cellPadding="1" borderColorLight="#D60C8C" border="3">
				<tr>
					<td height="1%" align="middle" bgColor="#FF9ACE" colSpan="2"><asp:label id="lblHeader" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="medium" Font-Bold="True">Debug Info</asp:label></td>
				</tr>
				<tr>
					<td height="99%" align="middle" bgColor="#FFFFFF">
						<table width="100%" height="100%" cellspacing="0" cellpadding="4">
							<tr>
								<td width="100%" valign="top">
									<br>
									<table cellSpacing="0" cellPadding="2" width="100%" border="0">
										<tr>
											<td>
												<asp:datagrid AllowSorting="False" id="grdEmpOnline" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="black" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="false" BorderWidth="0" AutoGenerateColumns="False" BackColor="#FFFFFF">
													<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
													<Columns>
														<asp:BoundColumn DataField="StoreNum" HeaderText="Store">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="Emp_Id" HeaderText="Emp Id">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Emp Name">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
															<ItemTemplate>
																<asp:Label Runat="server" ID="fullname" ForeColor="black" Font-Name="arial" Font-Size="x-small">
																	<%# Container.dataitem("NameFirst").tostring.trim & " " & Container.dataitem("NameLast").tostring.trim  %>
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:BoundColumn DataField="Created_Date" HeaderText="Last Trans">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Log Off">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
															<ItemTemplate>
																<a href='LogOff.aspx?Mode=1&EmpId=<%# Container.dataitem("Emp_Id").tostring %>'><font size="2" face="arial" color="#D60C8C">
																		<b>Log-Off</b></font></a>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
												</asp:datagrid>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
