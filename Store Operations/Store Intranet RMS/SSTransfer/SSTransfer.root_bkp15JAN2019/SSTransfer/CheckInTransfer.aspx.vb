
Public Class CheckInTransfer
    Inherits System.Web.UI.Page

    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents btnAcceptAll As System.Web.UI.WebControls.Button
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents layoutHeader As Header
    Protected WithEvents ucScanItem As ScanItem
    Protected WithEvents ucTransferBox As TransferBox
    Protected WithEvents btnEdit As System.Web.UI.WebControls.Button
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents btnUpdate As System.Web.UI.WebControls.Button
    Protected WithEvents ucTransferHeader As TransferHeader
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnCancelCheckIn As System.Web.UI.WebControls.Button
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblQuantityTotal As System.Web.UI.WebControls.Label
    Protected WithEvents lblShippedQuantityTotal As System.Web.UI.WebControls.Label

    Private m_objUserInfo As New UserInfo
    Private m_objLockTransfer As New TransferLock
    'Private m_objTransfer As New Transfer(Request.QueryString("BID"))


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        'Get employee data
        m_objUserInfo.GetEmployeeInfo()

        'If employee is logged on to the system then their locks are kept alive
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
            m_objLockTransfer.KeepLocksAlive(m_objUserInfo.EmpId)
        Else
            Response.Redirect("SessionEnd.aspx?Mode=1")
        End If

        'If store number not set then redirect to set store page
        If m_objUserInfo.StoreNumber = -1 Or m_objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

        'Clear error popup box
        pageBody.Attributes.Add("onload", "")
        btnCancelCheckIn.Attributes.Add("onclick", "return confirm('Are you sure you want to cancel the current transfer?')")

        layoutHeader.lblTitle = "Check In"
		layoutHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Transfer)
		layoutHeader.CurrentPage(SSTransfer.Header.PageName.CheckIn)
        layoutHeader.DisableButtons()


        ucScanItem.SetForm = "RECEIVING"
        ucScanItem.TransferSelectionVisible = False
        ucScanItem.CustomerInfoVisible = False
        ucScanItem.RAVisible = False

        ucScanItem.SetTType = 1 'Store to Store
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyShipped_Edit
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyReceived_Edit
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyReq

        ucTransferHeader.EnableBoxIncludes = False
        ucTransferBox.Box_Id = Request.QueryString("BID")
        ucTransferHeader.BoxId = Request.QueryString("BID")

        ucScanItem.CustomerInfoVisible = False

        ucTransferBox.LoadData()
        ucTransferHeader.LoadData()
        lblQuantityTotal.Text = ucTransferBox.GetTotalReceived()
        lblShippedQuantityTotal.Text = ucTransferBox.GetQuantityTotal()
        SetCompleteConfirm()


        'If Not m_objTransfer.IsTransferRestricted(ucTransferHeader.SStore, ucTransferHeader.RStore, ucTransferHeader.TType, "R") Then
        ' Response.Redirect("ViewTransfer.aspx?L=0&BID=" & Request("BID") & "&RET=default.aspx" & "&Msg=" & Server.UrlEncode("Transfer Check-In is no longer supported using this system."))
        ' End If

        If (ucTransferHeader.BoxStatus <> 2 And ucTransferHeader.BoxStatus <> 13) And ucTransferHeader.BoxStatus <> 3 Then
            Response.Redirect("ViewTransfer.aspx?L=1&BID=" & Request("BID") & "&RET=default.aspx")
        End If

        If Not m_objLockTransfer.LockTransfer(Request("BID"), m_objUserInfo.EmpId, Session.SessionID) Then
            Response.Redirect("ViewTransfer.aspx?L=1&BID=" & Request("BID") & "&RET=default.aspx")
        End If



    End Sub

    'Private Sub ucTransferBox_DiscrepancyGenerated(ByVal disAmount As Decimal, ByVal disQuant As Int32) Handles ucTransferBox.DiscrepancyGenerated
    'ucTransferHeader.DiscrepancyAmount = disAmount
    'ucTransferHeader.DiscrepancyQuantity = disQuant
    'ucTransferHeader.DisplayDiscrepancy()
    'End Sub


    'Capture SKU received event from scan item user control
    'Update quantity received
    Private Sub ucScanItem_OnReceivedSKU(ByVal SKU_Num As String, ByVal Xfer_Transfer_Cd As Byte, ByVal Xfer_Transfer_Desc As String, ByVal Xfer_Cust_Id As Int32) Handles ucScanItem.OnReceivedSKU

        'Update Item Quantity 
        ucTransferBox.UpdateQtyReceived(Xfer_Transfer_Cd, SKU_Num)
        ucTransferBox.RebindData()
        SetCompleteConfirm()
        lblQuantityTotal.Text = ucTransferBox.GetTotalReceived()
        lblShippedQuantityTotal.Text = ucTransferBox.GetQuantityTotal()
    End Sub

    'Set client browser to display warning message if user does not check in at least one item.
    Private Sub SetCompleteConfirm()

        If ucTransferBox.IsDiscrepancy Then
            btnSubmit.Attributes.Add("onclick", "return confirm('The sending quantities do not match the receiving quantities. Are you sure that you wish to complete the transfer?')") 'BW 01-20-04 fixed wording
        Else
            btnSubmit.Attributes.Add("onclick", "")
        End If

    End Sub

    'Event is captured when SKU is invalid
    Sub ucTransferBox_SKU(ByVal SKUNumber As String, ByVal ErrNumber As Byte, ByVal ErrDesc As String) Handles ucTransferBox.InValidSKU

        'Display error msg to user
        pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(ErrDesc) & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")

    End Sub

    'Event captured when user selects the edit button  
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        'Dsplay edit columns
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyReceived
        ucTransferBox.ShowColumn = TransferBox.BoxColumnName.QtyReceived_Edit
        'ucTransferBox.ShowColumn = 0

        'disable item scanning
        ucScanItem.EnableScan(False)

        'disable all buttons not associated with editing
        btnAcceptAll.Visible = False
        btnSave.Visible = False
        btnCancelCheckIn.Visible = False
        btnSubmit.Visible = False
        btnEdit.Visible = False

        btnUpdate.Visible = True
        btnCancel.Visible = True

    End Sub

    'Event is captured when user selects the cancel button
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        'ucTransferBox.HideColumn = 0
        ucScanItem.EnableScan(True)

        btnAcceptAll.Visible = True
        btnSave.Visible = True
        btnCancelCheckIn.Visible = True
        btnSubmit.Visible = True
        btnEdit.Visible = True

        btnUpdate.Visible = False
        btnCancel.Visible = False

    End Sub

    'Event is captured when users selects the update button
    'When update is selected all values in the edit columns are saved to the dataset
    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        'ucTransferBox.HideColumn = 0
        ucTransferBox.DeleteUnassignedOnly = True
        ucTransferBox.UpdateEdit()
        lblQuantityTotal.Text = ucTransferBox.GetTotalReceived()
        lblShippedQuantityTotal.Text = ucTransferBox.GetQuantityTotal()
        ucScanItem.EnableScan(True)

        btnAcceptAll.Visible = True
        btnSave.Visible = True
        btnCancelCheckIn.Visible = True
        btnSubmit.Visible = True
        btnEdit.Visible = True

        btnUpdate.Visible = False
        btnCancel.Visible = False

        SetCompleteConfirm()

    End Sub

    'Event is captured when Accept All button is selected
    'Accept All will update quantity received to match quantity sent
    Private Sub btnAcceptAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAcceptAll.Click

        ucTransferBox.AcceptAllReceived()
        SetCompleteConfirm()

    End Sub

    'Event is captured when user clicks the save button
    'Save changes box status to saved and stores dataset information to the database
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        ucTransferHeader.BoxStatus = 3 'Saved Unpacking
        ucTransferHeader.EmpId = m_objUserInfo.EmpId
        ucTransferHeader.EmpFullName = m_objUserInfo.EmpFullName
        ucTransferHeader.Save()
        ucTransferBox.Save()

        m_objLockTransfer.UnlockTransfer(ucTransferHeader.BoxId)

        Response.Redirect("SelectTransfer.aspx")

    End Sub


    'Event is captured when submit button is selected by user
    'Box status is changed to received and dataset information is stored in the database
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        'Dim objStore As New Store(ucTransferHeader.RStore)

        ucTransferHeader.BoxStatus = 4 'received
        ucTransferHeader.ReceivedDate = Now()
        ucTransferHeader.EmpId = m_objUserInfo.EmpId
        ucTransferHeader.EmpFullName = m_objUserInfo.EmpFullName

        ucTransferBox.GenerateDiscrepancy()
        ucTransferHeader.Save()
        ucTransferBox.Save()

        'If GERS automation is true then process in GERS
        If ConfigurationSettings.AppSettings("EnableAutoGERS") = True And ucTransferHeader.GetGERSAutoStatus <> True And GERSTransactionsEnabled() Then
            Dim objGERSUpdate As New GersInventoryUpdate

            objGERSUpdate.strDBConnection = ConfigurationSettings.AppSettings("strSQLConnection")

            objGERSUpdate.GetBoxHeader(ucTransferBox.Box_Id, ConfigurationSettings.AppSettings("EnableGERSMOS"))
            'objGERSUpdate.FlagGERSMovement()
            ucTransferBox.UpdateICStatus(TransferBox.eGERSSTatusType.Processed)
        Else
            ucTransferBox.UpdateICStatus(TransferBox.eGERSSTatusType.Open)
        End If

        Dim objUPS As New UPS_XML_Shipping

        objUPS.DeleteShippingLabels(ucTransferHeader.BoxId, Server.MapPath("UPSLabels"))

        m_objLockTransfer.UnlockTransfer(ucTransferHeader.BoxId)

        Response.Redirect("SelectTransfer.aspx")

    End Sub
    '# Before changed to ORMS
    ''Event is captured when submit button is selected by user
    ''Box status is changed to received and dataset information is stored in the database
    'Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

    '    'Dim objStore As New Store(ucTransferHeader.RStore)

    '    ucTransferHeader.BoxStatus = 4 'received
    '    ucTransferHeader.ReceivedDate = Now()
    '    ucTransferHeader.EmpId = m_objUserInfo.EmpId
    '    ucTransferHeader.EmpFullName = m_objUserInfo.EmpFullName

    '    ucTransferBox.GenerateDiscrepancy()
    '    ucTransferHeader.Save()
    '    ucTransferBox.Save()

    '    'If GERS automation is true then process in GERS
    '    If ConfigurationSettings.AppSettings("EnableAutoGERS") = True And ucTransferHeader.GetGERSAutoStatus <> True And GERSTransactionsEnabled() Then
    '        Dim objGERSUpdate As New GersInventoryUpdate()

    '        objGERSUpdate.strDBConnection = ConfigurationSettings.AppSettings("strSQLConnection")

    '        objGERSUpdate.GetBoxHeader(ucTransferBox.Box_Id, ConfigurationSettings.AppSettings("EnableGERSMOS"))
    '        objGERSUpdate.FlagGERSMovement()
    '        ucTransferBox.UpdateICStatus(TransferBox.eGERSSTatusType.Processed)
    '    Else
    '        ucTransferBox.UpdateICStatus(TransferBox.eGERSSTatusType.Open)
    '    End If

    '    Dim objUPS As New UPS_XML_Shipping()

    '    objUPS.DeleteShippingLabels(ucTransferHeader.BoxId, Server.MapPath("UPSLabels"))

    '    m_objLockTransfer.UnlockTransfer(ucTransferHeader.BoxId)

    '    Response.Redirect("SelectTransfer.aspx")

    'End Sub

    Private Function GERSTransactionsEnabled() As Boolean

        Dim objStore As New Store(ucTransferHeader.RStore)

        Select Case ucTransferHeader.TType
            Case 1
                If objStore.IsInternetStore And UCase(ConfigurationSettings.AppSettings("EnableGERSMOS_StoI")) = "TRUE" Then
                    Return True
                ElseIf Not objStore.IsInternetStore Then
                    Return True
                Else
                    Return False
                End If
            Case 2
                If UCase(ConfigurationSettings.AppSettings("EnableGERSMOS_StoDC")) = "TRUE" Then
                    Return True
                Else
                    Return False
                End If
            Case Else
                Return True
        End Select

    End Function

    'Event cancels the check in process when user selects cancel button
    Private Sub btnCancelCheckIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelCheckIn.Click
        Response.Redirect("default.aspx")
    End Sub

End Class
