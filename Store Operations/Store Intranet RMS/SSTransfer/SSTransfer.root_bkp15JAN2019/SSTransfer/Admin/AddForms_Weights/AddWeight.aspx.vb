Public Class AddWeight
    Inherits System.Web.UI.Page
    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
    Protected WithEvents lblWeightType As System.Web.UI.WebControls.Label
    Protected WithEvents ddlWeightType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblWeightTypeCode As System.Web.UI.WebControls.Label
    Protected WithEvents txtWeightTypeCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblLBS As System.Web.UI.WebControls.Label
    Protected WithEvents txtLBS As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnAddWeight As System.Web.UI.WebControls.Button
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        If Not Page.IsPostBack Then
            FillWeightType()
        End If

    End Sub

    Private Sub FillWeightType()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spGetAllWeightTypes", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        ddlWeightType.DataSource = objDataReader
        ddlWeightType.DataBind()

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Function IsValidForm() As Boolean

        Dim booReturn As Boolean = True

        If Len(txtLBS.Text) > 0 Then
            If Not IsNumeric(txtLBS.Text) Then
                booReturn = False
                lblError.Text += "[LBS] is invalid. "
            End If
        End If

        lblError.Visible = Not booReturn

        Return booReturn

    End Function

    Private Sub btnAddWeight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddWeight.Click

        If IsValidForm() Then

            Dim strStoredProcedure As String
            Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spInsertUPSUnitWeight " & ddlWeightType.SelectedItem.Value & ",'" & Replace(txtWeightTypeCode.Text, "'", "''") & "'," & txtLBS.Text

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()
            objCommand.Dispose()

            pageBody.Attributes.Add("onload", "window.opener.location=window.opener.location;")

        End If

    End Sub

End Class
