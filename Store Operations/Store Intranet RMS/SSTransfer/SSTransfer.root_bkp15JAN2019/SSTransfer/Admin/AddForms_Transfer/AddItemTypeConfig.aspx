<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AddItemTypeConfig.aspx.vb" Inherits="SSTransfer.AddItemTypeConfig"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Add Item Type Config</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
	</HEAD>
	<body class="sBody" id="pageBody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0" runat="server">
		<form id="frmAddUPSUserAccount" method="post" runat="server" onsubmit="return checkSubmit();">
			<table width="100%" class="sInfoTable" height="100%" cellSpacing="0" cellPadding="1">
				<tr>
					<td height="1%" align="middle" class="sDialogHeader" colSpan="2"><asp:label id="lblHeader" runat="server" EnableViewState="False">Add Item Type Config</asp:label></td>
				</tr>
				<tr>
					<td height="99%" align="middle">
						<table cellSpacing="0" cellPadding="2" width="100%">
							<tr>
								<td nowrap width="1%">
									<asp:label id="lblConfigType" runat="server" CssClass="Plain">Config Type:</asp:label>
								</td>
								<td>
									<asp:dropdownlist DataTextField="Description" DataValueField="Config_Type" id="ddlConfigType" Width="100%" Runat="server"></asp:dropdownlist>
								</td>
								<td nowrap width="1%">
									<asp:label id="lblItemTransType" runat="server" CssClass="Plain">Item Trans Type:</asp:label>
								</td>
								<td>
									<asp:DropDownList ID="ddlItemTransType" Runat="server" Width="100%"></asp:DropDownList>
								</td>
								<td nowrap width="1%">
									<asp:label id="lblTType" runat="server" CssClass="Plain">Transfer Type:</asp:label>
								</td>
								<td>
									<asp:dropdownlist DataTextField="Description" DataValueField="TType" id="ddlTType" Width="100%" Runat="server"></asp:dropdownlist>
								</td>
							</tr>
							<tr>
								<td nowrap width="1%">
									<asp:label id="lblSendingStore" runat="server" CssClass="Plain">Sending Store:</asp:label>
								</td>
								<td colspan="2">
									<asp:dropdownlist DataTextField="StoreName" DataValueField="StoreNum" id="ddlSendingStore" Width="100%" Runat="server"></asp:dropdownlist>
								</td>
								<td nowrap width="1%">
									<asp:label id="lblSendingGroup" runat="server" CssClass="Plain">Sending Group:</asp:label>
								</td>
								<td colspan="2">
									<asp:dropdownlist DataTextField="Description" DataValueField="StoreGroup_Id" id="ddlSendingGroup" Width="100%" Runat="server"></asp:dropdownlist>
								</td>
							</tr>
							<tr>
								<td nowrap width="1%">
									<asp:label id="lblReceivingStore" runat="server" CssClass="Plain">Reciving Store:</asp:label>
								</td>
								<td colspan="2">
									<asp:dropdownlist DataTextField="StoreName" DataValueField="StoreNum" id="ddlReceivingStore" Width="100%" Runat="server"></asp:dropdownlist>
								</td>
								<td nowrap width="1%">
									<asp:label id="lblReceivingGroup" runat="server" CssClass="Plain">Reciving Group:</asp:label>
								</td>
								<td colspan="2">
									<asp:dropdownlist DataTextField="Description" DataValueField="StoreGroup_Id" id="ddlReceivingGroup" Width="100%" Runat="server"></asp:dropdownlist>
								</td>
							</tr>
						</table>
						<br>
						<table width="100%">
							<tr>
								<td align="left">
									<input type="button" value="Cancel" onclick="javascript:window.close();" class="sButton">
								</td>
								<td align="right">
									<asp:Button ID="btnAddItemTypeConfig" Runat="Server" CssClass="sButton" Text="Add Item Type Config"></asp:Button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
