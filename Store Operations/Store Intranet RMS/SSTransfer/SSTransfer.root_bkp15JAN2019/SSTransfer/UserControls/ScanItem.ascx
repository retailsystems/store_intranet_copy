<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ScanItem.ascx.vb" Inherits="SSTransfer.ScanItem" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server">
</LINK>
<table cellSpacing="0" cellPadding="2" width="100%" border="0">
	<tr>
		<td class="sScanItemDarkBG" id="tdTransferType" noWrap width="1%" runat="server"><asp:label id="lblTransferType" runat="server" EnableViewState="False" CssClass="sScanItemBlackLabel">Transfer Type:</asp:label>&nbsp;
			<asp:dropdownlist id="ddlTransferType" runat="server" AutoPostBack="True" CssClass="Plain"></asp:dropdownlist>&nbsp;
		</td>
		<td id="tdCustomer" class="sScanItemLightBG" noWrap runat="server"><asp:label id="lblCustomerName" runat="server">Name:</asp:label>&nbsp;
			<asp:textbox id="txtCustomerName" runat="server" Columns="15"></asp:textbox>&nbsp;
			<asp:label id="Label1" runat="server">Phone:</asp:label>&nbsp;
			<asp:textbox id="txtCustomerPhone" runat="server" Columns="15"></asp:textbox><input id="txtHCustId" readOnly type="hidden" name="txtHCustId" runat="server">
			&nbsp;
		</td>
		<td class="sScanItemDarkBG" id="tdSKU" noWrap width="97%" runat="server"><asp:label id="lblSkuNumber" runat="server">SKU:</asp:label>&nbsp;
			<asp:textbox id="txtSkuNumber" runat="server" Columns="11" MaxLength="12"></asp:textbox><asp:button id="btnScanItem" runat="server" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" Text=" "></asp:button></td>
		<td class="sScanItemDarkBG" id="tdDDS" noWrap width="97%" runat="server"><asp:label id="lblDDS" runat="server">RA:</asp:label>&nbsp;
			<asp:textbox id="txtDDS" runat="server" Columns="11" MaxLength="12"></asp:textbox><asp:button id="btnScanRA" runat="server" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" Text=" "></asp:button></td>
		<td class="sScanItemDarkBG" noWrap width="1%">&nbsp;</td>
	</tr>
</table>
<SCRIPT language="javascript">
		<!--	
		function clickButton() 
		{
			if (event.keyCode == 13) 
			{
				document.forms[0].ucScanItem_btnScanItem.click();
				return false;
			}
		}
		-->
</SCRIPT>

<asp:placeholder id="phFocusName" runat="server" EnableViewState="False" Visible="false">
	<SCRIPT language="javascript">
		<!--
		document.forms[0].ucScanItem_txtCustomerName.focus();
		-->
	</SCRIPT>
</asp:placeholder><asp:placeholder id="phFocusPhone" runat="server" EnableViewState="False" Visible="false">
	<SCRIPT language="javascript">
		<!--
		document.forms[0].ucScanItem_txtCustomerPhone.focus();
		-->
	</SCRIPT>
</asp:placeholder><asp:placeholder id="phFocusSKU" runat="server" EnableViewState="False" Visible="false">
	<SCRIPT language="javascript">
		<!--
		document.forms[0].ucScanItem_txtSkuNumber.focus();
		-->
	</SCRIPT>
</asp:placeholder>
<asp:placeholder id="phFocusDDS" runat="server" EnableViewState="False" Visible="false">
	<SCRIPT language="javascript">
		<!--
		document.forms[0].ucScanItem_txtDDS.focus();
		
		function clickRAButton() 
		{
			if (event.keyCode == 13) 
			{
				document.forms[0].ucScanItem_btnDDS.click();
				return false;
			}
		}
		-->
	</SCRIPT>
</asp:placeholder>
