Public MustInherit Class ScanUPS
    Inherits System.Web.UI.UserControl
    Protected WithEvents lblUPSNumber As System.Web.UI.WebControls.Label
    Protected WithEvents ddlParcel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents txtTrackingNumber As System.Web.UI.WebControls.TextBox
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

    Public Event OnReceivedTrackNum(ByVal TrackingNum As String, ByVal Parcel As Int32)
    Public Event InValidTrackNum(ByVal strError As String)

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtTrackingNumber.Attributes.Add("onkeypress", "return clickButton()")
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        If Not Page.IsPostBack Then

            FillParcelDropdown()

        End If

    End Sub

    Private Sub FillParcelDropdown()

        'Dim FirstItem As Boolean = True
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        'Dim objCommand As New SqlClient.SqlCommand("spGetParcelServices ", objConnection)
        Dim objCommand As New SqlClient.SqlCommand("spGetParcelServices " & CInt(System.Web.HttpContext.Current.Request.Cookies("StoreNo").Value), objConnection)

        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader()

        While objDataReader.Read()

            'If FirstItem Then
            '    Dim FirstListItem As New ListItem()
            '    FirstListItem.Text = "Select a parcel service"
            '    FirstListItem.Value = "0"

            '    ddlParcel.Items.Add(FirstListItem)

            '    FirstItem = False
            'End If

            Dim newListItem As New ListItem()
            newListItem.Text = objDataReader("ParcelCompany")
            newListItem.Value = objDataReader("ParcelService_Cd")

            ddlParcel.Items.Add(newListItem)

        End While

        ddlParcel.SelectedIndex = 1

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub


    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If Not txtTrackingNumber.Text = Nothing And Not ddlParcel.SelectedItem.Value = 0 Then
            RaiseEvent OnReceivedTrackNum(Replace(txtTrackingNumber.Text, "'", "''"), ddlParcel.SelectedItem.Value)
        ElseIf Not txtTrackingNumber.Text = Nothing And ddlParcel.SelectedItem.Value = 0 Then
            RaiseEvent InValidTrackNum("Please select a parcel service.")
        End If

        txtTrackingNumber.Text = ""

    End Sub

End Class
