<%@ Page Language="vb" AutoEventWireup="false" Codebehind="default.aspx.vb" Inherits="SSTransfer._default" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Customer Information</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
	</HEAD>
	<body bottomMargin="0" vLink="black" aLink="black" link="black" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server" onsubmit="return checkSubmit();">
			<table width="100%" height="100%" borderColor="#D60C8C" cellSpacing="0" borderColorDark="#D60C8C" cellPadding="1" borderColorLight="#D60C8C" border="3">
				<tr>
					<td height="1%" align="middle" bgColor="#FF9ACE" colSpan="2"><asp:label id="lblHeader" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="medium" Font-Bold="True">Debug Info</asp:label></td>
				</tr>
				<tr>
					<td height="99%" align="middle" bgColor="#FFFFFF">
						<table width="100%" height="100%" cellspacing="0" cellpadding="4">
							<tr>
								<td width="100%" valign="top">
									<br>
									<table cellSpacing="0" cellPadding="2" width="100%" border="0">
										<tr>
											<td>
												<asp:datagrid AllowSorting="False" id="grdEmpOnline" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="black" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="false" BorderWidth="0" AutoGenerateColumns="False" BackColor="#FFFFFF">
													<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
													<Columns>
														<asp:BoundColumn DataField="StoreNum" HeaderText="Store">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="Emp_Id" HeaderText="Emp Id">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Emp Name">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
															<ItemTemplate>
																<asp:Label Runat="server" ID="fullname" ForeColor="black" Font-Name="arial" Font-Size="x-small">
																	<%# Container.dataitem("NameFirst").tostring.trim & " " & Container.dataitem("NameLast").tostring.trim  %>
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:BoundColumn DataField="CurrentPage" HeaderText="Page">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="Created_Date" HeaderText="Last Trans">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Log Off">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
															<ItemTemplate>
																<a href='default.aspx?Mode=1&EmpId=<%# Container.dataitem("Emp_Id").tostring %>'><font size="2" face="arial" color="#D60C8C">
																		<b>Log-Off</b></font></a>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
												</asp:datagrid>
											</td>
										</tr>
									</table>
									<br>
									<table width="100%" cellpadding="2" cellspacing="0">
										<tr>
											<td>
												<asp:DataGrid id="dgXMLErrorLog" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="black" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="true" BorderWidth="0" AutoGenerateColumns="False" BackColor="#FFFFFF">
													<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
													<Columns>
														<asp:BoundColumn DataField="Box_Id" HeaderText="Transfer #">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="XMLRequest_Type" HeaderText="Request Type">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="ErrorSeverity" HeaderText="Error Severity">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="ErrorCode" HeaderText="Error Code">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="ErrorDescription" HeaderText="Error Description">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="XML Response" ItemStyle-HorizontalAlign="center">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
															<ItemTemplate>
																<a href='ViewXML.aspx?Type=1&EID=<%# Container.dataitem("UPS_XMLError_Id") %>'>
																	<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" Font-Bold="True" ForeColor="#D60C8C" ID="Label4">VIEW</asp:Label>
																</a>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="XML Request" ItemStyle-HorizontalAlign="center">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
															<ItemTemplate>
																<a href='ViewXML.aspx?Type=2&EID=<%# Container.dataitem("UPS_XMLError_Id") %>'>
																	<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" Font-Bold="True" ForeColor="#D60C8C" ID="Label5">VIEW</asp:Label>
																</a>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:BoundColumn DataField="Created_Date" HeaderText="Error Date">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
													</Columns>
												</asp:DataGrid>
											</td>
										</tr>
									</table>
									<br>
									<table width="100%" cellpadding="2" cellspacing="0">
										<tr>
											<td>
												<asp:DataGrid id="dgAppErrors" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="black" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="true" BorderWidth="0" AutoGenerateColumns="False" BackColor="#FFFFFF">
													<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
													<Columns>
														<asp:BoundColumn DataField="App_Error_Id" HeaderText="Error Id">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderText="Error Message">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
															<ItemTemplate>
																<a href='viewAppError.aspx?AppError=<%# Container.dataitem("App_Error_Id").tostring %>'>
																	<font size="2" face="arial" color="#D60C8C"><b>View</b></font></a>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:BoundColumn DataField="Relevent_Data" HeaderText="Relevent Data">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="Created_Date" HeaderText="Created Date">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
													</Columns>
												</asp:DataGrid>
											</td>
										</tr>
									</table>
									<br>
									<table cellSpacing="2" cellPadding="4" border="0" width="100%">
										<tr>
											<td>
												<asp:DataGrid id="dgUPSVoid" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="black" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="true" BorderWidth="0" AutoGenerateColumns="False" BackColor="#FFFFFF" Visible="False">
													<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
													<Columns>
														<asp:BoundColumn DataField="Box_Id" HeaderText="Transfer #">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="Status_Desc" HeaderText="Transfer Status">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Tracking_Number">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
															<ItemTemplate>
																<a target=_blank href='<%# Replace(Container.dataitem("TrackingURL").tostring,"{0}",Container.dataitem("Tracking_Number").tostring) %>'>
																	<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" ForeColor="black" ID="Label3">
																		<%# Container.dataitem("Tracking_Number").tostring %>
																	</asp:Label>
																</a>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:BoundColumn DataField="RequestDate" HeaderText="Label Request Date">
															<HeaderStyle Font-Bold="True" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Elapsed Time">
															<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="#D60C8C" BackColor="#FDD9EC"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" ForeColor='black' ID="Label2">
																	<%# DateDiff(DateInterval.Hour, Container.dataitem("RequestDate"), Now) & " Hrs" %>
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
												</asp:DataGrid>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
