<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Error.aspx.vb" Inherits="SSTransfer._Error" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server" />
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server" onsubmit="return checkSubmit();">
			<table width="100%" height="100%" cellSpacing="0" cellPadding="1" class="sDialogTable">
				<tr>
					<td height="1%" align="middle" class="sDialogHeader" colSpan="2"><asp:label id="lblHeader" runat="server" EnableViewState="False">Error!</asp:label></td>
				</tr>
				<tr>
					<td height="99%" align="middle">
						<asp:Label id="lblErrDesc" cssclass="sInfo" runat="server"></asp:Label>
						<br>
						<br>
						<input type="button" value="Close" onclick="javascript:window.close();" class="sButton">
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
