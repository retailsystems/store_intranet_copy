<%@ Register TagPrefix="uc1" TagName="TransferBox" Src="UserControls/TransferBox.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ViewTransfer.aspx.vb" Inherits="SSTransfer.ViewTransfer" %>
<%@ Register TagPrefix="uc1" TagName="TransferHeader" Src="UserControls/TransferHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Transfer Home</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server"/>
	</HEAD>
	<body id="pagebody" runat="server" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="frmTransferHome" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1"><uc1:header id="layoutHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="top" align="middle">
						<table cellSpacing="0" cellPadding="4" width="100%" border="0">
							<tr>
								<td align="middle"><uc1:transferheader id="ucTransferHeader" runat="server"></uc1:transferheader></td>
							</tr>
						</table>
						<hr width="100%" class="sHr">
						<table cellSpacing="0" cellPadding="4" width="100%" border="0">
							<tr>
								<td align="middle">
									<div style="OVERFLOW: auto; WIDTH: 100%; HEIGHT: 250px"><uc1:transferbox id="ucTransferBox" runat="server"></uc1:transferbox></div>
								</td>
							</tr>
							<tr>
								<td align="right">
									<asp:button id="btnReturn" runat="server" Text="Return" CssClass="sButton"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
