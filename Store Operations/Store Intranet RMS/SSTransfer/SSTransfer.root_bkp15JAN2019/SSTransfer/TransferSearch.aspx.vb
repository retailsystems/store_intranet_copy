Public Class TransferSearch
    Inherits System.Web.UI.Page
    Protected WithEvents ucHeader As Header
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents txtTransNum As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtUPS As System.Web.UI.WebControls.TextBox
    Protected WithEvents rblTransfer As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents txtFDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnTransSearch As System.Web.UI.WebControls.Button
    Protected WithEvents btnUPSSearch As System.Web.UI.WebControls.Button
    Protected WithEvents lblFDateEX As System.Web.UI.WebControls.Label
    Protected WithEvents lblTDateEX As System.Web.UI.WebControls.Label
    Protected WithEvents btnDate As System.Web.UI.WebControls.Button
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

    Private objUserInfo As New UserInfo()
    Public strSQL2 As String
    Public intStore As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()

        'If employee is logged on to the system then update last trans
        If objUserInfo.EmpOnline Then
            objUserInfo.KeepLogonAlive()
        End If
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        pageBody.Attributes.Add("onload", "")
        'Put user code to initialize the page here
        ucHeader.lblTitle = "Search"
		ucHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Transfer)
		ucHeader.CurrentPage(SSTransfer.Header.PageName.Search)
        intStore = objUserInfo.StoreNumber  'store cookie

        lblFDateEX.Text = "(Format Example: " & Format(Today, "M/d/yy") & ")"
        lblTDateEX.Text = "(Format Example: " & Format(Today, "M/d/yy") & ")"

    End Sub

    Private Sub btnTransSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTransSearch.Click
        'Search by Transfer Number
        Dim strReturn
        strReturn = "TransferSearch.aspx"
        If txtTransNum.Text <> "" Then
            If Len(txtTransNum.Text) > 4 Then
                'adding on where statement if user enters a transfer(box id) #
                Dim strTransfer As String
                strTransfer = Trim(txtTransNum.Text)
                strTransfer = Replace(strTransfer, "'", "''")
                'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Box_Id = '" & strTransfer & "' ) "
                'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2) & "&RTN=" & strReturn)  Original
                Response.Redirect("AdvancedResults.aspx?ABID=" & strTransfer & "&SPRTN=" & strReturn)
            Else
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("The Transfer Number you entered is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                'lblError.Text = "The Transfer Number you entered is invalid."
            End If
        ElseIf txtTransNum.Text = "" Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter a Transfer Number.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            'lblError.Text = "Please enter a Transfer Number."
        End If

        'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2))
    End Sub
    Private Sub btnUPSSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUPSSearch.Click
        'Search by UPS Number
        Dim strReturn
        strReturn = "TransferSearch.aspx"
        If txtUPS.Text <> "" And txtUPS.Text.Length <= 18 Then
            'adding on where statement if user enters a UPS #
            Dim strUPS As String
            strUPS = Trim(txtUPS.Text)
            strUPS = Replace(strUPS, "'", "''")
            'strSQL2 = "AND (Box_Xfer_Hdr.Tracking_Num = '" & strUPS & "') "
            'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2) & "&RTN=" & strReturn)
            Response.Redirect("AdvancedResults.aspx?UPS=" & strUPS & "&SPRTN=" & strReturn)
        ElseIf txtUPS.Text = "" Then
            'lblError.Text = "Please enter a UPS number."
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter a UPS number.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            'ElseIf txtUPS.Text <> "" And txtUPS.Text.Length < 18 Then
            'lblError.Text = "UPS Number invalid.  Please make sure you have typed in your UPS Number correctly."
        End If
        'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2))
    End Sub
    Private Sub btnDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDate.Click
        'Search by date for shipped or received transfers.
        Dim FromDate As String
        Dim ThruDate As String
        Dim strReturn
        strReturn = "TransferSearch.aspx"
        If rblTransfer.SelectedItem.Value = "Received" Then
            'Search From Store's Received Date when they are shipping to this store
            If txtTDate.Text = "" And txtFDate.Text = "" Then
                'lblError.Text = "Please enter in a date range to search by."
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter in a date range to search by.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            ElseIf (txtFDate.Text = "" And txtTDate.Text <> "") Then
                Dim TestDate As Boolean
                ThruDate = Trim(txtTDate.Text)
                ThruDate = Replace(ThruDate, "'", "''")
                TestDate = IsDate(ThruDate)
                If TestDate = True Then
                    Dim testThruDate As Date
                    testThruDate = ThruDate
                    Dim WDate As Date
                    WDate = #1/1/1753#
                    If Date.Compare(WDate, testThruDate) <= 0 Then
                        'make sure date isn't before 1/1/1753
                        FromDate = Now.AddMonths(-12).ToShortDateString
                        ThruDate = testThruDate & " 23:59:59:999"
                        'strSQL2 = "AND (Box_Xfer_Hdr.Receiving_Store_Cd IN (" & (intStore) & ")) "
                        'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Received_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Received_Date <='" & ThruDate & "' ) "
                        'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2) & "&RTN=" & strReturn)
                        Response.Redirect("AdvancedResults.aspx?ST=" & "Simple" & "&SF=" & "Received" & "&TStore=" & intStore & "&FDate=" & FromDate & "&TDate=" & ThruDate & "&SPRTN=" & strReturn)
                    Else
                        pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your thru date should be past 1/1/1753.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                    End If
                Else
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your thru date is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                End If
            ElseIf (txtFDate.Text <> "" And txtTDate.Text = "") Then
                Dim TestDate As Boolean
                FromDate = Trim(txtFDate.Text)
                FromDate = Replace(FromDate, "'", "''")
                TestDate = IsDate(FromDate)
                If TestDate = True Then
                    Dim WDate As Date
                    Dim TestFromDate As Date
                    TestFromDate = FromDate
                    WDate = #1/1/1753#
                    If Date.Compare(WDate, TestFromDate) <= 0 Then
                        ThruDate = Now
                        FromDate = TestFromDate
                        'strSQL2 = "AND (Box_Xfer_Hdr.Receiving_Store_Cd IN (" & (intStore) & ")) "
                        'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Received_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Received_Date <='" & ThruDate & "' ) "
                        'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2) & "&RTN=" & strReturn)
                        Response.Redirect("AdvancedResults.aspx?ST=" & "Simple" & "&SF=" & "Received" & "&TStore=" & intStore & "&FDate=" & FromDate & "&TDate=" & ThruDate & "&SPRTN=" & strReturn)
                    Else
                        pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your from date should be past 1/1/1753.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                    End If
                Else
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your from date is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                End If
            ElseIf (txtFDate.Text <> "" And txtTDate.Text <> "") Then
                'Dim FromDate As String
                'Dim ThruDate As String
                FromDate = Trim(txtFDate.Text)    'assigning info to FromDate
                ThruDate = Trim(txtTDate.Text)    'assigning info to ThruDate
                FromDate = Replace(FromDate, "'", "''")
                ThruDate = Replace(ThruDate, "'", "''")
                Dim FromDateCheck As Boolean
                Dim ThruDateCheck As Boolean
                FromDateCheck = IsDate(FromDate)
                ThruDateCheck = IsDate(ThruDate)
                If (FromDateCheck = True And ThruDateCheck = True) Then
                    Dim TestFromDate As Date
                    Dim TestThruDate As Date
                    TestFromDate = FromDate
                    TestThruDate = ThruDate
                    Dim WDate As Date
                    WDate = #1/1/1753#
                    If Date.Compare(WDate, TestFromDate) <= 0 Then
                        If Date.Compare(WDate, TestThruDate) <= 0 Then
                            If Date.Compare(TestFromDate, TestThruDate) > 0 Then
                                'switching date around so they are in correct order.
                                Dim NewThruDate As Date
                                NewThruDate = TestFromDate
                                FromDate = TestThruDate
                                ThruDate = NewThruDate
                                ThruDate = ThruDate & " 23:59:59:999"
                                'strSQL2 = "AND (Box_Xfer_Hdr.Receiving_Store_Cd IN (" & (intStore) & ")) "
                                'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Received_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Received_Date <='" & ThruDate & "' ) "
                                'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2) & "&RTN=" & strReturn)
                                Response.Redirect("AdvancedResults.aspx?ST=" & "Simple" & "&SF=" & "Received" & "&TStore=" & intStore & "&FDate=" & FromDate & "&TDate=" & ThruDate & "&SPRTN=" & strReturn)
                            ElseIf Date.Compare(TestFromDate, TestThruDate) <= 0 Then
                                'already in correct order
                                FromDate = TestFromDate
                                ThruDate = TestThruDate & " 23:59:59:999"
                                'strSQL2 = "AND (Box_Xfer_Hdr.Receiving_Store_Cd IN (" & (intStore) & ")) "
                                'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Received_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Received_Date <='" & ThruDate & "' ) "
                                'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2) & "&RTN=" & strReturn)
                                Response.Redirect("AdvancedResults.aspx?ST=" & "Simple" & "&SF=" & "Received" & "&TStore=" & intStore & "&FDate=" & FromDate & "&TDate=" & ThruDate & "&SPRTN=" & strReturn)
                            End If 'end comparing of dates
                        Else
                            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your thru date should be past 1/1/1753.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                        End If 'Thru date compare with 1/1/1753
                    Else
                        pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your from date should be past 1/1/1753.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                    End If 'From Date compare with 1/1/1753
                ElseIf FromDateCheck = False Then
                    'lblError.Text = "Your date range is invalid."
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your from date is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                ElseIf ThruDatecheck = False Then
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your thru date is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                End If 'isdate compare()
            End If 'end if textboxes empty check
        ElseIf rblTransfer.SelectedItem.Value = "Shipped" Then
            'Search this store's shipped date when they are shipping elsewhere
            If txtTDate.Text = "" And txtFDate.Text = "" Then
                lblError.Text = "Please enter in a date range to search by."
            ElseIf (txtFDate.Text = "" And txtTDate.Text <> "") Then
                'lblError.Text = "Please fill in the From Date."
                Dim TestDate As Boolean
                ThruDate = Trim(txtTDate.Text)
                ThruDate = Replace(txtTDate.Text, "'", "''")
                TestDate = IsDate(ThruDate)
                If TestDate = True Then
                    Dim TestThruDate As Date
                    TestThruDate = ThruDate
                    Dim WDate As Date
                    WDate = #1/1/1753#
                    If Date.Compare(WDate, TestThruDate) <= 0 Then
                        'make sure date isn't before 1/1/1753
                        FromDate = Now.AddMonths(-12).ToShortDateString
                        ThruDate = TestThruDate & " 23:59:59:999"
                        'strSQL2 = "AND (Box_Xfer_Hdr.Sending_Store_Cd IN (" & (intStore) & ")) "
                        'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & ThruDate & "' ) "
                        'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2) & "&RTN=" & strReturn)
                        Response.Redirect("AdvancedResults.aspx?ST=" & "Simple" & "&SF=" & "Shipped" & "&FStore=" & intStore & "&FDate=" & FromDate & "&TDate=" & ThruDate & "&SPRTN=" & strReturn)
                    Else
                        pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your thru date should be past 1/1/1753.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                    End If
                Else
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your thru date is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                End If
            ElseIf (txtFDate.Text <> "" And txtTDate.Text = "") Then
                Dim TestDate As Boolean
                FromDate = Trim(txtFDate.Text)
                FromDate = Replace(FromDate, "'", "''")
                TestDate = IsDate(FromDate)
                If TestDate = True Then
                    Dim TestFromDate As Date
                    TestFromDate = FromDate
                    Dim WDate As Date
                    WDate = #1/1/1753#
                    If Date.Compare(WDate, TestFromDate) <= 0 Then
                        'make sure date isn't before 1/1/1753
                        ThruDate = Now
                        FromDate = TestFromDate
                        'strSQL2 = "AND (Box_Xfer_Hdr.Sending_Store_Cd IN (" & (intStore) & ")) "
                        'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & ThruDate & "' ) "
                        'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2) & "&RTN=" & strReturn)
                        Response.Redirect("AdvancedResults.aspx?ST=" & "Simple" & "&SF=" & "Shipped" & "&FStore=" & intStore & "&FDate=" & FromDate & "&TDate=" & ThruDate & "&SPRTN=" & strReturn)
                    Else
                        pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your from date should be past 1/1/1753.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                    End If
                Else
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your from date is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                End If
            ElseIf (txtFDate.Text <> "" And txtTDate.Text <> "") Then
                FromDate = Trim(txtFDate.Text)    'assigning info to FromDate
                ThruDate = Trim(txtTDate.Text)    'assigning info to ThruDate
                FromDate = Replace(FromDate, "'", "''")
                ThruDate = Replace(ThruDate, "'", "''")
                Dim FromDateCheck As Boolean
                Dim ThruDateCheck As Boolean
                FromDateCheck = IsDate(FromDate)
                ThruDateCheck = IsDate(ThruDate)
                If (FromDateCheck = True And ThruDateCheck = True) Then
                    Dim TestFromDate As Date
                    Dim TestThruDate As Date
                    TestFromDate = FromDate
                    TestThruDate = ThruDate
                    Dim WDate As Date
                    WDate = #1/1/1753#
                    If Date.Compare(WDate, TestFromDate) <= 0 Then
                        If Date.Compare(WDate, TestThruDate) <= 0 Then
                            If Date.Compare(TestFromDate, TestThruDate) > 0 Then
                                'switching date around so they are in correct order.
                                Dim NewThruDate As Date
                                NewThruDate = TestFromDate
                                FromDate = TestThruDate
                                ThruDate = NewThruDate
                                ThruDate = ThruDate & " 23:59:59:999"
                                'strSQL2 = "AND (Box_Xfer_Hdr.Sending_Store_Cd IN (" & (intStore) & ")) "
                                'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & ThruDate & "' ) "
                                'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2) & "&RTN=" & strReturn)
                                Response.Redirect("AdvancedResults.aspx?ST=" & "Simple" & "&SF=" & "Shipped" & "&FStore=" & intStore & "&FDate=" & FromDate & "&TDate=" & ThruDate & "&SPRTN=" & strReturn)
                            ElseIf Date.Compare(TestFromDate, TestThruDate) <= 0 Then
                                'already in correct order
                                FromDate = TestFromDate
                                ThruDate = TestThruDate & " 23:59:59:999"
                                'strSQL2 = "AND (Box_Xfer_Hdr.Sending_Store_Cd IN (" & (intStore) & ")) "
                                'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & ThruDate & "' ) "
                                'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2) & "&RTN=" & strReturn)
                                Response.Redirect("AdvancedResults.aspx?ST=" & "Simple" & "&SF=" & "Shipped" & "&FStore=" & intStore & "&FDate=" & FromDate & "&TDate=" & ThruDate & "&SPRTN=" & strReturn)
                            End If 'end comparing of dates
                        Else
                            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your thru date should be past 1/1/1753.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                        End If
                    Else
                        pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your from date should be past 1/1/1753.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                    End If
                ElseIf FromDateCheck = False Then
                    'lblError.Text = "Your date range is invalid."
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your from date is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                ElseIf ThruDateCheck = False Then
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Your thru date is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                End If
            End If
        End If  'end rbl selection

        '******************************************************************************************************

    End Sub
    Private Sub txtTransNum_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTransNum.TextChanged

    End Sub
    Private Sub txtUPS_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUPS.TextChanged

    End Sub
    Private Sub rblTransfer_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rblTransfer.SelectedIndexChanged

    End Sub
    Private Sub txtFDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFDate.TextChanged

    End Sub
    Private Sub txtTDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTDate.TextChanged

    End Sub
End Class
