Public Class AdvancedSearch
    Inherits System.Web.UI.Page
    Protected WithEvents lbStores As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblIncludes As System.Web.UI.WebControls.Label
    Protected WithEvents lbIncludes As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblDate As System.Web.UI.WebControls.Label
    Protected WithEvents lblTransferType As System.Web.UI.WebControls.Label
    Protected WithEvents lbTransferType As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblStatus As System.Web.UI.WebControls.Label
    Protected WithEvents lbStatus As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblFrom As System.Web.UI.WebControls.Label
    Protected WithEvents lblthru As System.Web.UI.WebControls.Label
    Protected WithEvents btnSearch As System.Web.UI.WebControls.Button
    Protected WithEvents dgResults As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblStore As System.Web.UI.WebControls.Label
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents txtFDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSearchFor As System.Web.UI.WebControls.Label
    Protected WithEvents lblSKU As System.Web.UI.WebControls.Label
    Protected WithEvents txtSKU As System.Web.UI.WebControls.TextBox
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ucHeader As Header
    Protected WithEvents btnClear As System.Web.UI.WebControls.Button
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Public intStore As Integer     'store number from cookie
    Public strSQL As String       'Holds search string
    Public strSQL2 As String      'Holds WHERE search statements
    Public FromDate As String
    Public ThruDate As String
    Public strSKU As String       'String for SKU criteria
    'Public FromDate As Date
    'Public ThruDate As Date
    Protected WithEvents rblSearchOptions As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lblFExample As System.Web.UI.WebControls.Label
    Protected WithEvents lblTExample As System.Web.UI.WebControls.Label
    Private objUserInfo As New UserInfo()
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()

        'If store number not set then redirect to set store page
        If objUserInfo.StoreNumber = -1 Then
            Response.Redirect("SetStore.aspx")
        End If
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ucHeader.lblTitle = "Advanced Search"
		ucHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Transfer)
		ucHeader.CurrentPage(SSTransfer.Header.PageName.Search)
        pageBody.Attributes.Add("onload", "")
        If Not IsPostBack Then
            Call FillStore()           'Fills Store listbox
            Call FillTransferType()    'Fills Transfer Types listbox
            Call FillStatus()          'Fills Status Types listbox
            Call FillIncludes()        'Fills Box Includes listbox

        End If

        intStore = objUserInfo.StoreNumber
        'intStore = "1"
        '****************************************************************************************

        lblFExample.Text = "(Format Example: " & Format(Today, "M/d/yy") & ")"
        lblTExample.Text = "(Format Example: " & Format(Today, "M/d/yy") & ")"

    End Sub

    Private Sub FillStore()
        'Fills Store listbox 
        Dim strStoreProcedure As String
        strStoreProcedure = "asFillTStore"
        Dim conStore As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        conStore.Open()
        Dim cmdStore As New SqlClient.SqlCommand(strStoreProcedure, conStore)
        Dim dtrStore As SqlClient.SqlDataReader
        dtrStore = cmdStore.ExecuteReader()
        lbStores.DataSource = dtrStore
        lbStores.DataTextField = "StoreName"
        lbStores.DataValueField = "StoreNum"
        lbStores.DataBind()
        dtrStore.Close()
        conStore.Close()
        cmdStore.Dispose()
    End Sub
    Private Sub FillTransferType()
        'Fills transfer type listbox
        'Dim strTTypeProcedure As String
        'strTTypeProcedure = "asFillTransferType"
        'Dim conTType As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        'conTType.Open()
        'Dim cmdTType As New SqlClient.SqlCommand(strTTypeProcedure, conTType)
        'Dim dtrTType As SqlClient.SqlDataReader
        'dtrTType = cmdTType.ExecuteReader()
        'lbTransferType.DataSource = dtrTType
        'lbTransferType.DataTextField = "Xfer_Desc"
        'lbTransferType.DataValueField = "Xfer_Cd"
        'lbTransferType.DataBind()
        'dtrTType.Close()
        'conTType.Close()
        Dim strTT As String
        strTT = "Select Xfer_Cd,Xfer_Desc FROM Xfer_Type Where Active=1 AND Xfer_Cd IN (3,4)"
        Dim conTT As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        conTT.Open()
        Dim cmdTT As New SqlClient.SqlCommand(strTT, conTT)
        Dim dtrTT As SqlClient.SqlDataReader
        dtrTT = cmdTT.ExecuteReader()
        lbTransferType.DataSource = dtrTT
        lbTransferType.DataTextField = "Xfer_Desc"
        lbTransferType.DataValueField = "Xfer_Cd"
        lbTransferType.DataBind()
        dtrTT.Close()
        conTT.Close()
        cmdTT.Dispose()
    End Sub
    Private Sub FillStatus()
        If rblSearchOptions.SelectedItem.Value = "Expected" Then
            lblStatus.Visible = False
            lbStatus.Visible = False
        ElseIf rblSearchOptions.SelectedItem.Value = "Sent" Then
            lblStatus.Visible = False
            lbStatus.Visible = False
            'lblStatus.Visible = True
            'lbStatus.Visible = True
            'Dim strSelect As String
            'strSelect = "Select Status_cd,Status_Desc FROM Box_Status_Type Where Active=1 AND Status_Cd IN (2,3,4)"
            'Dim conStatus As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            'conStatus.Open()
            'Dim cmdStatus As New SqlClient.SqlCommand(strSelect, conStatus)
            'Dim dtrStatus As SqlClient.SqlDataReader
            'dtrStatus = cmdStatus.ExecuteReader()
            'lbStatus.DataSource = dtrStatus
            'lbStatus.DataTextField = "Status_Desc"
            'lbStatus.DataValueField = "Status_Cd"
            'lbStatus.DataBind()
            'dtrStatus.Close()
            'conStatus.Close()
        ElseIf rblSearchOptions.SelectedItem.Value = "Received" Then
            lblStatus.Visible = True
            lbStatus.Visible = True
            Dim strSelect As String
            strSelect = "Select Status_cd,Status_Desc FROM Box_Status_Type Where Active=1 AND Status_Cd IN (3,4)"
            Dim conStatus As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            conStatus.Open()
            Dim cmdStatus As New SqlClient.SqlCommand(strSelect, conStatus)
            Dim dtrStatus As SqlClient.SqlDataReader
            dtrStatus = cmdStatus.ExecuteReader()
            lbStatus.DataSource = dtrStatus
            lbStatus.DataTextField = "Status_Desc"
            lbStatus.DataValueField = "Status_Cd"
            lbStatus.DataBind()
            dtrStatus.Close()
            conStatus.Close()
            cmdStatus.Dispose()
        End If
    End Sub
    Private Sub FillIncludes()
        'Fills Box Includes listbox
        Dim strIncludesProcedure As String
        strIncludesProcedure = "asFillIncludes"
        Dim conIncludes As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        conIncludes.Open()
        Dim cmdIncludes As New SqlClient.SqlCommand(strIncludesProcedure, conIncludes)
        Dim dtrIncludes As SqlClient.SqlDataReader
        dtrIncludes = cmdIncludes.ExecuteReader()
        lbIncludes.DataSource = dtrIncludes
        lbIncludes.DataTextField = "Includes_Desc"
        lbIncludes.DataValueField = "Box_Includes_Cd"
        lbIncludes.DataBind()
        dtrIncludes.Close()
        conIncludes.Close()
        cmdIncludes.Dispose()
    End Sub
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblDate.Visible = True
        lblthru.Visible = True
        lblFrom.Visible = True
        Call ValidateUserInput()

    End Sub
    Private Sub ValidateUserInput()
        lblError.Text = ""   'clean out lblError
        '************************************************************************************************************
        'DATE VALIDATION
        If (txtFDate.Text <> "" And txtTDate.Text = "") Then
            FromDate = Trim(txtFDate.Text)    'assigning info to FromDate
            FromDate = Replace(FromDate, "'", "''")
            Dim DateCheck As Boolean    'Checking to make user entered valid info into date box
            DateCheck = IsDate(FromDate)
            If DateCheck = False Then
                'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("The date information you entered is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                lblError.Text = "The from date you entered is invalid."
                lblDate.Visible = False
                lblthru.Visible = False
                lblFrom.Visible = False
            Else
                Dim TestFromDate As Date
                TestFromDate = FromDate
                Dim WDate As Date
                WDate = #1/1/1753#
                If Date.Compare(WDate, TestFromDate) <= 0 Then
                    FromDate = TestFromDate

                Else
                    lblError.Text = "Your from date should be past 1/1/1753."

                End If
            End If

        ElseIf (txtTDate.Text <> "" And txtFDate.Text = "") Then
            ThruDate = Trim(txtTDate.Text)    'assigning info to ThruDate
            ThruDate = Replace(ThruDate, "'", "''")
            Dim DateCheck As Boolean    'Checking to make user entered valid info into date box
            DateCheck = IsDate(ThruDate)
            If DateCheck = False Then
                'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("The date information you entered is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                lblError.Text = "The thru date you entered is invalid."
            Else
                Dim TestThruDate As Date
                TestThruDate = ThruDate
                Dim WDate As Date
                WDate = #1/1/1753#
                If Date.Compare(WDate, TestThruDate) <= 0 Then
                    ThruDate = TestThruDate
                Else
                    lblError.Text = "Your thru date should be past 1/1/1753."
                End If
            End If

        ElseIf (txtFDate.Text <> "" And txtTDate.Text <> "") Then
            FromDate = Trim(txtFDate.Text)    'assigning info to FromDate
            ThruDate = Trim(txtTDate.Text)    'assigning info to ThruDate
            FromDate = Replace(FromDate, "'", "''")
            ThruDate = Replace(ThruDate, "'", "''")
            Dim FromDateCheck As Boolean
            Dim ThruDateCheck As Boolean
            FromDateCheck = IsDate(FromDate)
            ThruDateCheck = IsDate(ThruDate)
            If (FromDateCheck = True And ThruDateCheck = True) Then
                Dim testFromDate As Date
                Dim testThruDate As Date
                testFromDate = FromDate
                testThruDate = ThruDate
                Dim WDate As Date
                WDate = #1/1/1753#
                If Date.Compare(WDate, TestFromDate) <= 0 Then
                    If Date.Compare(WDate, TestThruDate) <= 0 Then
                        If Date.Compare(TestFromDate, Now.ToShortDateString) > 0 Then
                            'Not allowing searching with future dates.
                            lblError.Text = "Cannot search with dates past the current date."
                            'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Cannot search with dates past the current date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                        ElseIf Date.Compare(TestThruDate, Now.ToShortDateString) > 0 Then
                            'Not allowing searching with future dates.
                            'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Cannot search with dates past the current date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                            lblError.Text = "Cannot search with dates past the current date."
                        ElseIf Date.Compare(TestFromDate, TestThruDate) > 0 Then
                            'switching date around so they are in correct order.
                            Dim NewThruDate As Date
                            NewThruDate = TestFromDate
                            FromDate = TestThruDate
                            ThruDate = NewThruDate
                            lblError.Text = ""
                            Call Search()
                        ElseIf Date.Compare(TestFromDate, TestThruDate) < 0 Then
                            FromDate = TestFromDate
                            ThruDate = TestThruDate
                            lblError.Text = ""
                            Call Search()
                        End If
                    Else
                        lblError.Text = "Your thru date should be past 1/1/1753."
                    End If
                Else
                    lblError.Text = "Your from date should be past 1/1/1753."
                End If
            ElseIf FromDateCheck = False Then  'one of the dates is not a valid date
                lblError.Text = "The from date you entered is invalid."
                'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("The date information you entered is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            ElseIf ThruDateCheck = False Then
                lblError.Text = "The thru date you entered is invalid."
            End If
        End If

        If lbStores.SelectedIndex <> -1 Then
            'Then stores are selected.  Make sure they didn't highlight enough to crash application.
            Dim NumSelected As Integer              'Counter
            NumSelected = 0                         'Initialize counter

            Dim IStore As ListItem
            For Each IStore In lbStores.Items
                If IStore.Selected = True Then
                    NumSelected = NumSelected + 1   'if x=1 just do intStatus=si.value else, concatonate()
                End If
            Next

            If NumSelected > 300 Then
                lblError.Text = "Please select fewer stores to search, or don't select any stores to search for all stores."
            End If
        End If

        If (Trim(txtSKU.Text) <> "" And Trim(txtSKU.Text <> Nothing)) Then
            Dim intLength As Integer
            Dim tempSKU As String
            tempSKU = Trim(txtSKU.Text)
            tempSKU = Replace(tempSKU, "'", "''")
            intLength = tempSKU.Length

            If intLength = 10 Or intLength = 9 Then
                'Check if SKU is a real valid SKU
                If intLength = 9 Then
                    Dim ItemNo As String
                    Dim ItemSize As String
                    ItemNo = Left(tempSKU, 6)
                    ItemNo = Replace(ItemNo, "'", "''")
                    ItemSize = Right(tempSKU, 3)
                    ItemSize = Replace(ItemSize, "'", "''")

                    If IsNumeric(ItemNo) And IsNumeric(ItemSize) Then
                        strSKU = ItemNo & "-" & ItemSize
                    Else
                        lblError.Text = "Invalid SKU entered."
                        lblError.Visible = True
                    End If

                    'SKU = ItemNo & "-" & ItemSize
                ElseIf intLength = 10 Then
                    Dim ItemNo As String
                    Dim ItemSize As String
                    ItemNo = Left(tempSKU, 6)
                    ItemNo = Replace(ItemNo, "'", "''")
                    ItemSize = Right(tempSKU, 3)

                    If IsNumeric(ItemNo) And IsNumeric(ItemSize) Then
                        'tempSKU = Replace(tempSKU, "'", "''")
                        strSKU = tempSKU
                    Else
                        lblError.Text = "Invalid SKU entered."
                        lblError.Visible = True
                    End If
                End If
            Else
                'not a valid sku (not enough digits entered in)
                lblError.Text = "Invalid SKU entered."
                lblError.Visible = True
            End If
        End If

        If (lblError.Text = "") Then
            Call Search()
        End If
        '*********************************************************************************************************
    End Sub
    Private Sub Search()

        Dim strSearchFor As String
        '*************************************************************************************************************
        'Get Transfer Type selections from user
        Dim TransType As String  'sent into SQL statement for Transfer Type selections
        If lbTransferType.SelectedIndex <> -1 Then
            Dim intTType As Integer
            Dim intTTypeX As String
            'Dim TransType As String  'sent into SQL statement for Transfer Type selections
            Dim x As Integer 'Counter
            x = 0           'initialize to zero
            Dim li As ListItem


            For Each li In lbTransferType.Items
                If li.Selected = True Then
                    x = x + 1   'if x=1 just do intTType=li.value else, concatonate()
                End If
            Next

            If x = 1 Then   'Only one selection by user
                For Each li In lbTransferType.Items
                    If li.Selected = True Then
                        intTType = li.Value
                    End If
                Next
            Else            'Multiple selections by user
                For Each li In lbTransferType.Items
                    If li.Selected = True Then
                        intTType = li.Value
                        intTTypeX = intTType & "," & intTTypeX   'separate selections with commas
                    End If
                Next
            End If

            If x > 1 Then   'take off ending comma at end of selections
                Dim Lposition As Integer
                Dim position As Integer
                Lposition = (Len(intTTypeX)) - 1
                position = Len(intTTypeX)
                TransType = Left$(intTTypeX, Lposition) & Replace(intTTypeX, ",", "", position)
            Else
                TransType = intTType   'single selection, so no string manipulation
            End If
            'send in selections to SQL statement
            'strSQL2 = strSQL2 & "AND (Box_Xfer_Item.Xfer_Type_Cd IN (" & (TransType) & ")) "

        End If
        '******************************************************************************************
        'Get status selected by user and insert into SQL (from List box)
        Dim strStatusType As String  'entry for SQL statement
        If (rblSearchOptions.SelectedItem.Value = "Sent") Or (rblSearchOptions.SelectedItem.Value = "Expected") Or (rblSearchOptions.SelectedItem.Value = "Received") Then
            If lbStatus.SelectedIndex <> -1 Then
                Dim intStatus As Integer
                Dim strSType As String
                'Dim strStatusType As String  'entry for SQL statement
                Dim Z As Integer             'Counter
                Z = 0                        'Initialize counter

                Dim si As ListItem
                For Each si In lbStatus.Items
                    If si.Selected = True Then
                        Z = Z + 1   'if x=1 just do intStatus=si.value else, concatonate()
                    End If
                Next

                If Z = 1 Then   'Only one selection by user
                    For Each si In lbStatus.Items
                        If si.Selected = True Then
                            intStatus = si.Value
                        End If
                    Next
                Else            'Multiple selections by user
                    For Each si In lbStatus.Items
                        If si.Selected = True Then
                            intStatus = si.Value
                            strSType = intStatus & "," & strSType   'separate selections with commas
                        End If
                    Next
                End If

                If Z > 1 Then   'take off ending comma at end of selections
                    Dim Sposition As Integer
                    Dim position As Integer
                    Sposition = (Len(strSType)) - 1
                    position = Len(strSType)
                    strStatusType = Left$(strSType, Sposition) & Replace(strSType, ",", "", position)
                Else
                    strStatusType = intStatus   'single selection, so no string manipulation
                End If
                'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Current_Box_Status_Cd IN (" & (strStatusType) & ")) "
            End If
        ElseIf (rblSearchOptions.SelectedItem.Value = "Void") Then

            'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Current_Box_Status_Cd IN (7)) "
        End If

        '**************************************************************************************************
        'If user is searching sent boxes, check if they want to search with any box includes
        'If (rblSearchOptions.SelectedItem.Value = "Sent" And lbIncludes.SelectedIndex <> -1) Then
        Dim strBoxIncludes As String  'entry for SQL statement
        If (rblSearchOptions.SelectedItem.Value = "Sent" And lbIncludes.SelectedIndex <> -1) Or (rblSearchOptions.SelectedItem.Value = "Void" And lbIncludes.SelectedIndex <> -1) Then
            Dim intIncludes As Integer
            Dim strBIncludes As String
            'Dim strBoxIncludes As String  'entry for SQL statement
            Dim Y As Integer             'Counter
            Y = 0                        'Initialize counter

            Dim BI As ListItem
            For Each BI In lbIncludes.Items
                If BI.Selected = True Then
                    Y = Y + 1   'if x=1 just do intStatus=si.value else, concatonate()
                End If
            Next

            If Y = 1 Then   'Only one selection by user
                For Each BI In lbIncludes.Items
                    If BI.Selected = True Then
                        intIncludes = BI.Value
                    End If
                Next
            Else            'Multiple selections by user
                For Each BI In lbIncludes.Items
                    If BI.Selected = True Then
                        intIncludes = BI.Value
                        strBIncludes = intIncludes & "," & strBIncludes   'separate selections with commas
                    End If
                Next
            End If

            If Y > 1 Then   'take off ending comma at end of selections
                Dim Bposition As Integer
                Dim position As Integer
                Bposition = (Len(strBIncludes)) - 1
                position = Len(strBIncludes)
                strBoxIncludes = Left$(strBIncludes, Bposition) & Replace(strBIncludes, ",", "", position)
            Else
                strBoxIncludes = intIncludes   'single selection, so no string manipulation
            End If
            'strSQL2 = strSQL2 & "AND (Box_To_Include.Box_Includes_Cd IN (" & (strBoxIncludes) & ")) "

        End If
        '**************************************************************************************************
        'Depending on the search mode choses, set sending or receiving store as the store # that is 
        'doing the search (using store cookie)
        Dim strFromStore As String
        Dim strToStore As String
        If (rblSearchOptions.SelectedItem.Value = "Received") Then
            strSearchFor = "Received"
            strToStore = intStore  'This Store received
            'strSQL2 = strSQL2 & "AND ((Box_Xfer_Hdr.Receiving_Store_Cd IN (" & (intStore) & ")) AND Box_Status_Type.Status_Desc <> 'Voided') "
        ElseIf (rblSearchOptions.SelectedItem.Value = "Sent") Then
            strSearchFor = "Shipped"
            strFromStore = intStore 'This store sent
            'strSQL2 = strSQL2 & "AND ((Box_Xfer_Hdr.Sending_Store_Cd IN (" & (intStore) & ")) AND Box_Status_Type.Status_Desc <> 'Voided') "
        ElseIf (rblSearchOptions.SelectedItem.Value = "Void") Then
            strSearchFor = "Voided"
            strFromStore = intStore 'This store sent
            'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Sending_Store_Cd IN (" & (intStore) & ")) "
        ElseIf (rblSearchOptions.SelectedItem.Value = "Expected") Then
            'make sure to get boxes that haven't been received yet.
            strSearchFor = "Expected"
            strToStore = intStore    'This store received
            'strSQL2 = strSQL2 & "AND ((Box_Xfer_Hdr.Receiving_Store_Cd IN (" & (intStore) & ")) AND Box_Xfer_Hdr.Received_Date IS NULL) AND Box_Status_Type.Status_Desc <> 'Voided' "
        End If
        '**************************************************************************************************
        'SQL statement for list box of stores.
        'If user is searching for Sent Boxes: Received will be their selections in store listbox
        'If user is searching for Received or Expecting Boxes: sending stores will be their selections in store listbox

        If ((rblSearchOptions.SelectedItem.Value = "Received" Or rblSearchOptions.SelectedItem.Value = "Expected") And (lbStores.SelectedIndex <> -1)) Then
            Dim intStores As Integer
            Dim intStores2 As String
            Dim strStoreSearch As String  'entry for SQL statement
            Dim W As Integer              'Counter
            W = 0                         'Initialize counter

            Dim IStore As ListItem
            For Each IStore In lbStores.Items
                If IStore.Selected = True Then
                    W = W + 1   'if x=1 just do intStatus=si.value else, concatonate()
                End If
            Next

            If W = 1 Then   'Only one selection by user
                For Each IStore In lbStores.Items
                    If IStore.Selected = True Then
                        intStores = IStore.Value
                    End If
                Next
            Else            'Multiple selections by user
                For Each IStore In lbStores.Items
                    If IStore.Selected = True Then
                        intStores = IStore.Value
                        intStores2 = intStores & "," & intStores2   'separate selections with commas
                    End If
                Next
            End If

            If W > 1 Then   'take off ending comma at end of selections
                Dim Wposition As Integer
                Dim position As Integer
                Wposition = (Len(intStores2)) - 1
                position = Len(intStores2)
                strStoreSearch = Left$(intStores2, Wposition) & Replace(intStores2, ",", "", position)
            Else
                strStoreSearch = intStores   'single selection, so no string manipulation
            End If
            'Searching From Stores selected by user.  To Store will be store that is doing the searching.
            strFromStore = strStoreSearch
            'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Sending_Store_Cd IN (" & (strStoreSearch) & ")) "

        ElseIf (rblSearchOptions.SelectedItem.Value = "Sent" And lbStores.SelectedIndex <> -1) Or (rblSearchOptions.SelectedItem.Value = "Void" And lbStores.SelectedIndex <> -1) Then
            Dim intStores As Integer
            Dim intStores2 As String
            Dim strStoreSearch As String  'entry for SQL statement
            Dim W As Integer              'Counter
            W = 0                         'Initialize counter

            Dim IStore As ListItem
            For Each IStore In lbStores.Items
                If IStore.Selected = True Then
                    W = W + 1   'if x=1 just do intStatus=si.value else, concatonate()
                End If
            Next

            If W = 1 Then   'Only one selection by user
                For Each IStore In lbStores.Items
                    If IStore.Selected = True Then
                        intStores = IStore.Value
                    End If
                Next
            Else            'Multiple selections by user
                For Each IStore In lbStores.Items
                    If IStore.Selected = True Then
                        intStores = IStore.Value
                        intStores2 = intStores & "," & intStores2   'separate selections with commas
                    End If
                Next
            End If

            If W > 1 Then   'take off ending comma at end of selections
                Dim Wposition As Integer
                Dim position As Integer
                Wposition = (Len(intStores2)) - 1
                position = Len(intStores2)
                strStoreSearch = Left$(intStores2, Wposition) & Replace(intStores2, ",", "", position)
            Else
                strStoreSearch = intStores   'single selection, so no string manipulation
            End If
            'Searching To Stores selected by user.  From Store will be store that is doing the searching.
            strToStore = strStoreSearch
            'strSQL2 = strSQL2 & "AND (Box_Xfer_Hdr.Receiving_Store_Cd IN (" & (strStoreSearch) & ")) "

        End If
        '**************************************************************************************************
        'SQL for Date where stmt
        'FromDate = txtFMonth.Text & "-" & txtFDay.Text & "-" & txtFYear.Text
        'ThruDate = txtTMonth.Text & "-" & txtTDay.Text & "-" & txtTYear.Text
        If (txtFDate.Text <> "" And txtTDate.Text <> "") Then
            'If user has entered all date information (From Date and Thru Date)
            If rblSearchOptions.SelectedItem.Value = "Received" Then
                'Search received date column
                ThruDate = ThruDate & " 23:59:59:999"
                FromDate = FromDate
                'strSQL2 = strSQL2 & "AND (Received_Date >= '" & FromDate & "' AND Received_Date <= '" & ThruDate & "') "
            ElseIf (rblSearchOptions.SelectedItem.Value = "Sent") Or (rblSearchOptions.SelectedItem.Value = "Void") Then
                'search shipped date column
                ThruDate = ThruDate & " 23:59:59:999"
                FromDate = FromDate
                'strSQL2 = strSQL2 & "AND (Shipment_Date >= '" & FromDate & "' AND Shipment_Date <= '" & ThruDate & "') "
            End If
        ElseIf (txtFDate.Text <> "" And txtTDate.Text = "") Then
            'If user entered From Date and no Thru Date
            ThruDate = Now.ToShortDateString
            ThruDate = ThruDate & " 23:59:59:999"
            FromDate = FromDate
            If rblSearchOptions.SelectedItem.Value = "Received" Then
                'Search received date column
                'strSQL2 = strSQL2 & "AND (Received_Date >= '" & FromDate & "' AND Received_Date <= '" & ThruDate & "') "
            ElseIf (rblSearchOptions.SelectedItem.Value = "Sent") Or (rblSearchOptions.SelectedItem.Value = "Void") Then
                'search shipped date column
                'strSQL2 = strSQL2 & "AND (Shipment_Date >= '" & FromDate & "' AND Shipment_Date <= '" & ThruDate & "') "
            End If
        ElseIf (txtFDate.Text = "" And txtTDate.Text <> "") Then
            'If user entered Thru Date and no From Date
            FromDate = Now.AddMonths(-12).ToShortDateString
            ThruDate = ThruDate & " 23:59:59:999"
            If rblSearchOptions.SelectedItem.Value = "Received" Then
                'Search received date column
                'strSQL2 = strSQL2 & "AND (Received_Date >= '" & FromDate & "' AND Received_Date <= '" & ThruDate & "') "
            ElseIf (rblSearchOptions.SelectedItem.Value = "Sent") Or (rblSearchOptions.SelectedItem.Value = "Void") Then
                'search shipped date column
                'strSQL2 = strSQL2 & "AND (Shipment_Date >='" & FromDate & "' AND Shipment_Date <= '" & ThruDate & "') "
            End If
        ElseIf (rblSearchOptions.SelectedItem.Value = "Expected") Then
            'strSQL2 = strSQL2 & "AND ((Box_xfer_hdr.Shipment_Date IS NOT NULL ) AND (Box_xfer_hdr.Received_Date IS NULL)) "
        End If

        Dim strReturn
        strReturn = "AdvancedSearch.aspx"
        'Response.Redirect("AdvancedResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL2) & "&SPRTN=" & strReturn)
        Response.Redirect("AdvancedResults.aspx?ST=" & "Advanced" & "&SF=" & strSearchFor & "&BS=" & strStatusType & "&TT=" & TransType & "&BI=" & strBoxIncludes & "&FStore=" & strFromStore & "&TStore=" & strToStore & "&FDate=" & FromDate & "&TDate=" & ThruDate & "&SKU=" & strSKU & "&SPRTN=" & strReturn)
    End Sub
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        'Clears out all entries and selections
        txtFDate.Text = ""
        txtTDate.Text = ""
        lbStores.SelectedIndex = -1
        lbIncludes.SelectedIndex = -1
        lbStatus.SelectedIndex = -1
        lbTransferType.SelectedIndex = -1
        lblError.Text = ""
    End Sub
    Private Sub rblSearchOptions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rblSearchOptions.SelectedIndexChanged
        'This handles user selection changes for search by (expected arrivals, received, sent boxes)
        'Sets the appropriate label text values and makes Box Includes visible pending on selection.
        If rblSearchOptions.SelectedItem.Value = "Expected" Then
            lbTransferType.Visible = True
            lblTransferType.Visible = True
            lbStatus.Visible = False
            lblStatus.Visible = False
            lblStore.Text = "From Store:"
            lbIncludes.Visible = "false"
            lblIncludes.Visible = "false"
            lbIncludes.SelectedIndex = -1
            lblDate.Visible = False
            lblthru.Visible = False
            txtTDate.Visible = False
            txtFDate.Visible = False
            lblFExample.Visible = False
            lblTExample.Visible = False
            lblFrom.Visible = False
            Dim strTT As String
            strTT = "Select Xfer_Cd,Xfer_Desc FROM Xfer_Type Where Active=1 AND Xfer_Cd IN (3,4)"
            Dim conTT As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            conTT.Open()
            Dim cmdTT As New SqlClient.SqlCommand(strTT, conTT)
            Dim dtrTT As SqlClient.SqlDataReader
            dtrTT = cmdTT.ExecuteReader()
            lbTransferType.DataSource = dtrTT
            lbTransferType.DataTextField = "Xfer_Desc"
            lbTransferType.DataValueField = "Xfer_Cd"
            lbTransferType.DataBind()
            dtrTT.Close()
            conTT.Close()
            cmdTT.Dispose()
        ElseIf rblSearchOptions.SelectedItem.Value = "Sent" Then
            lblStore.Text = "To Store:"
            lblDate.Text = "Sent Date:"
            lbIncludes.Visible = "true"
            lblIncludes.Visible = "True"
            lblDate.Visible = True
            lblthru.Visible = True
            txtTDate.Visible = True
            txtFDate.Visible = True
            lblFExample.Visible = True
            lblTExample.Visible = True
            lblFrom.Visible = True
            lblStatus.Visible = False
            lbStatus.Visible = False
            Dim strSelect As String
            strSelect = "Select Status_cd,Status_Desc FROM Box_Status_Type Where Active=1 AND Status_Cd IN (2,3,4)"
            Dim conStatus As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            conStatus.Open()
            Dim cmdStatus As New SqlClient.SqlCommand(strSelect, conStatus)
            Dim dtrStatus As SqlClient.SqlDataReader
            dtrStatus = cmdStatus.ExecuteReader()
            lbStatus.DataSource = dtrStatus
            lbStatus.DataTextField = "Status_Desc"
            lbStatus.DataValueField = "Status_Cd"
            lbStatus.DataBind()
            dtrStatus.Close()
            conStatus.Close()
            cmdStatus.Dispose()
            Dim strTT As String
            strTT = "Select Xfer_Cd,Xfer_Desc FROM Xfer_Type Where Active=1 "
            Dim conTT As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            conTT.Open()
            Dim cmdTT As New SqlClient.SqlCommand(strTT, conTT)
            Dim dtrTT As SqlClient.SqlDataReader
            dtrTT = cmdTT.ExecuteReader()
            lbTransferType.DataSource = dtrTT
            lbTransferType.DataTextField = "Xfer_Desc"
            lbTransferType.DataValueField = "Xfer_Cd"
            lbTransferType.DataBind()
            dtrTT.Close()
            conTT.Close()
            cmdTT.Dispose()
        ElseIf rblSearchOptions.SelectedItem.Value = "Received" Then
            lblStore.Text = "From Store:"
            lblDate.Text = "Received Date:"
            lbIncludes.Visible = "False"
            lblIncludes.Visible = "False"
            lbIncludes.SelectedIndex = -1
            lblDate.Visible = True
            lblthru.Visible = True
            txtTDate.Visible = True
            txtFDate.Visible = True
            lblFExample.Visible = True
            lblTExample.Visible = True
            lblFrom.Visible = True
            lblStatus.Visible = True
            lbStatus.Visible = True
            Dim strSelect As String
            strSelect = "Select Status_cd,Status_Desc FROM Box_Status_Type Where Active=1 AND Status_Cd IN (3,4)"
            Dim conStatus As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            conStatus.Open()
            Dim cmdStatus As New SqlClient.SqlCommand(strSelect, conStatus)
            Dim dtrStatus As SqlClient.SqlDataReader
            dtrStatus = cmdStatus.ExecuteReader()
            lbStatus.DataSource = dtrStatus
            lbStatus.DataTextField = "Status_Desc"
            lbStatus.DataValueField = "Status_Cd"
            lbStatus.DataBind()
            dtrStatus.Close()
            conStatus.Close()
            cmdStatus.Dispose()
            Dim strTT As String
            strTT = "Select Xfer_Cd,Xfer_Desc FROM Xfer_Type Where Active=1 AND Xfer_Cd IN (3,4)"
            Dim conTT As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            conTT.Open()
            Dim cmdTT As New SqlClient.SqlCommand(strTT, conTT)
            Dim dtrTT As SqlClient.SqlDataReader
            dtrTT = cmdTT.ExecuteReader()
            lbTransferType.DataSource = dtrTT
            lbTransferType.DataTextField = "Xfer_Desc"
            lbTransferType.DataValueField = "Xfer_Cd"
            lbTransferType.DataBind()
            dtrTT.Close()
            conTT.Close()
            cmdTT.Dispose()
        ElseIf rblSearchOptions.SelectedItem.Value = "Void" Then
            lblStore.Text = "To Store:"
            lblDate.Text = "Sent Date:"
            lbIncludes.Visible = "true"
            lblIncludes.Visible = "True"
            lblDate.Visible = True
            lblthru.Visible = True
            txtTDate.Visible = True
            txtFDate.Visible = True
            lblFExample.Visible = True
            lblTExample.Visible = True
            lblFrom.Visible = True
            lblStatus.Visible = False
            lbStatus.Visible = False
            Dim strTT As String
            strTT = "Select Xfer_Cd,Xfer_Desc FROM Xfer_Type Where Active=1 "
            Dim conTT As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            conTT.Open()
            Dim cmdTT As New SqlClient.SqlCommand(strTT, conTT)
            Dim dtrTT As SqlClient.SqlDataReader
            dtrTT = cmdTT.ExecuteReader()
            lbTransferType.DataSource = dtrTT
            lbTransferType.DataTextField = "Xfer_Desc"
            lbTransferType.DataValueField = "Xfer_Cd"
            lbTransferType.DataBind()
            dtrTT.Close()
            conTT.Close()
            cmdTT.Dispose()
        End If
    End Sub
End Class
