Imports System.Net
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Text
Imports System.IO

Public Class UPS_XML_Shipping

    Private m_strErrorSeverity As String

    Private m_BoxId As String
    Private m_SendingAddress1 As String
    Private m_SendingAddress2 As String
    Private m_SendingAddress3 As String
    Private m_SendingCity As String
    Private m_SendingProvince As String
    Private m_SendingName As String
    Private m_SendingPostalCode As String
    Private m_SendingCountry As String
    Private m_SendingShipperNumber As String
    Private m_ReceivingAddress1 As String
    Private m_ReceivingAddress2 As String
    Private m_ReceivingAddress3 As String
    Private m_ReceivingCity As String
    Private m_ReceivingProvince As String
    Private m_ReceivingName As String
    Private m_ReceivingPostalCode As String
    Private m_ReceivingCountry As String
    Private m_TType As Int32
    Private m_ReceivingPhone As String
    Private m_SendingPhone As String

    Private m_TotalCharges_Value As String
    Private m_TotalCharges_Currency As String
    Private m_ShipmentConfirm_Request As String
    Private m_ShipmentConfirm_Response As String
    Private m_ShipmentAccept_Request As String
    Private m_ShipmentAccept_Response As String

    Private m_ServiceCode As String
    Private m_PackageType As String
    Private m_ShipmentDigest As String
    Private m_ShipmentIdentificationNumber As String
    Private m_TrackingNumber As String
    Private m_ShippingLabel As String
    Private m_ImageDir As String
    Private m_ImageFileName As String
    Private m_intTrackingInfoId As Int32
    Private m_strLBS As String
    Private m_strRANum As String

    'Error XML error message accuired from UPS intergration server
    Public Event XMLErrorMsg(ByVal ErrorMsg As String)

    Public Sub New()

    End Sub

    'DDS RA Shipping number
    Property RANum()
        Get
            Return m_strRANum
        End Get
        Set(ByVal Value)
            m_strRANum = Value
        End Set
    End Property

    'Total montery value of shipping label
    ReadOnly Property TotalCharges_Value()
        Get
            Return m_TotalCharges_Value
        End Get
    End Property

    'UPS currency time. See UPS XML documentation for more information
    ReadOnly Property TotalCharges_Currency()
        Get
            Return m_TotalCharges_Currency
        End Get
    End Property

    'Full XML Accept request sent to UPS intergration servers 
    Property ShipmentAccept_Request()
        Get
            Return m_ShipmentAccept_Request
        End Get
        Set(ByVal Value)
            m_ShipmentAccept_Request = Value
        End Set
    End Property

    'Full XML Accept response sent from UPS intergration servers
    ReadOnly Property ShipmentAccept_Response()
        Get
            Return m_ShipmentAccept_Response
        End Get
    End Property

    'Full XML Confirm Request sent to UPS intergration servers
    Property ShipmentConfirm_Request()
        Get
            Return m_ShipmentConfirm_Request
        End Get
        Set(ByVal Value)
            ShipmentConfirm_Request = Value
        End Set
    End Property

    'Full XML Confirm response sent from UPS intergration servers
    ReadOnly Property ShipmentConfirm_Response()
        Get
            Return m_ShipmentConfirm_Response
        End Get
    End Property

    'XML error severity defined by UPS. See XML UPS documentation from further details 
    ReadOnly Property ErrorSeverity()
        Get
            Return m_strErrorSeverity
        End Get
    End Property

    'Box Id associated to Shipping request
    WriteOnly Property BoxId()
        Set(ByVal Value)
            m_BoxId = Value
        End Set
    End Property

    'Phone number of receiver
    WriteOnly Property ReceivingPhone()
        Set(ByVal Value)
            m_ReceivingPhone = Value
        End Set
    End Property

    'Phone number of sender
    WriteOnly Property SendingPhone()
        Set(ByVal Value)
            m_SendingPhone = Value
        End Set
    End Property

    'Estimated package weight
    WriteOnly Property PackageWeight()
        Set(ByVal Value)
            m_strLBS = Value
        End Set
    End Property

    'Transfer Shipment type (ie Returns,StoreToStore,Monday Mail Pack)
    WriteOnly Property TType()
        Set(ByVal Value)
            m_TType = Value
        End Set
    End Property

    'SQL UPS Tracking Id
    WriteOnly Property TrackingInfoId()
        Set(ByVal Value)
            m_intTrackingInfoId = Value
        End Set
    End Property

    'UPS Service Code - See UPS Documentation for further details
    WriteOnly Property ServiceCode()
        Set(ByVal Value)
            m_ServiceCode = Value
        End Set
    End Property

    'UPS Package Code - See UPS Documentation for furhter detials
    WriteOnly Property PackageType()
        Set(ByVal Value)
            m_PackageType = Value
        End Set
    End Property

    'Sender Name
    WriteOnly Property SendingName()
        Set(ByVal Value)
            m_SendingName = Value
        End Set
    End Property

    'Physical file name for shipping label image received from UPS
    ReadOnly Property ImageFileName()
        Get
            Return m_ImageFileName
        End Get
    End Property

    'Physical directory location for shipping label image received from UPS
    WriteOnly Property ImageDir()
        Set(ByVal Value)
            m_ImageDir = Value
        End Set
    End Property

    'Tracking number received from UPS after sending Accept request
    ReadOnly Property TrackingNum()
        Get
            Return m_TrackingNumber
        End Get
    End Property

    'Sender Province
    WriteOnly Property SendingProvince()
        Set(ByVal Value)
            m_SendingProvince = Value
        End Set
    End Property

    'Identification number generated by Shipment Confirm request. - See XML UPS documentation for further details
    Property ShipmentId()
        Get
            Return m_ShipmentIdentificationNumber
        End Get
        Set(ByVal Value)
            m_ShipmentIdentificationNumber = Value
        End Set
    End Property

    'Sender address 1
    WriteOnly Property SendingAddress1()
        Set(ByVal Value)
            m_SendingAddress1 = Value
        End Set
    End Property

    'Sender Address 2
    WriteOnly Property SendingAddress2()
        Set(ByVal Value)
            m_SendingAddress2 = Value
        End Set
    End Property

    'Sender Address 3
    WriteOnly Property SendingAddress3()
        Set(ByVal Value)
            m_SendingAddress3 = Value
        End Set
    End Property

    'Sender City
    WriteOnly Property SendingCity()
        Set(ByVal Value)
            m_SendingCity = Value
        End Set
    End Property

    'Sender Country
    WriteOnly Property SendingCountry()
        Set(ByVal Value)
            m_SendingCountry = Value
        End Set
    End Property

    'Sender Postal Code
    WriteOnly Property SendingPostalCode()
        Set(ByVal Value)
            m_SendingPostalCode = Value
        End Set
    End Property

    'Sender UPS Account Number
    WriteOnly Property SendingShipperNumber()
        Set(ByVal Value)
            m_SendingShipperNumber = Value
        End Set
    End Property

    'Receiver Name
    WriteOnly Property ReceivingName()
        Set(ByVal Value)
            m_ReceivingName = Value
        End Set
    End Property

    'Receiver Province
    WriteOnly Property ReceivingProvince()
        Set(ByVal Value)
            m_ReceivingProvince = Value
        End Set
    End Property

    'Receiver Adderss 1
    WriteOnly Property ReceivingAddress1()
        Set(ByVal Value)
            m_ReceivingAddress1 = Value
        End Set
    End Property

    'Receiver Address 2
    WriteOnly Property ReceivingAddress2()
        Set(ByVal Value)
            m_ReceivingAddress2 = Value
        End Set
    End Property

    'Receiver Address 3
    WriteOnly Property ReceivingAddress3()
        Set(ByVal Value)
            m_ReceivingAddress3 = Value
        End Set
    End Property

    'Receiver City
    WriteOnly Property ReceivingCity()
        Set(ByVal Value)
            m_ReceivingCity = Value
        End Set
    End Property

    'Receiver Country
    WriteOnly Property ReceivingCountry()
        Set(ByVal Value)
            m_ReceivingCountry = Value
        End Set
    End Property

    'Receiver Postal Code
    WriteOnly Property ReceivingPostalCode()
        Set(ByVal Value)
            m_ReceivingPostalCode = Value
        End Set
    End Property

    'Delete Shipping label image form hard disk
    Public Sub DeleteShippingLabels(ByVal Box_Id As String, ByVal Dir As String)

        Dim fInfo As FileInfo

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxTrackingNumbers", objConnection)
        Dim objDataReader As SqlClient.SqlDataReader
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)

        parBoxId.Direction = ParameterDirection.Input

        parBoxId.Value = Box_Id

        objDataReader = objCommand.ExecuteReader

        Do While objDataReader.Read()
            fInfo = New FileInfo(Dir & "\ShippingLabel_" & objDataReader("Tracking_Number") & ".gif")
            fInfo.Delete()
        Loop

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()


    End Sub

    Public Sub New(ByVal BoxId As String, ByVal SendingAddress1 As String, ByVal SendingAddress2 As String, ByVal SendingAddress3 As String, ByVal SendingCity As String, ByVal SendingProvince As String, _
    ByVal SendingName As String, ByVal SendingPostalCode As String, ByVal SendingCountry As String, ByVal SendingShipperNumber As String, _
    ByVal ReceivingAddress1 As String, ByVal ReceivingAddress2 As String, ByVal ReceivingAddress3 As String, ByVal ReceivingCity As String, ByVal ReceivingProvince As String, ByVal ReceivingName As String, _
    ByVal ReceivingPostalCode As String, ByVal ReceivingCountry As String, ByVal ImageDir As String, ByVal PackageWeight As String)

        m_BoxId = BoxId
        m_SendingCity = SendingCity
        m_SendingProvince = SendingName
        m_SendingName = SendingName
        m_SendingPostalCode = SendingPostalCode
        m_SendingCountry = SendingCountry
        m_SendingShipperNumber = SendingShipperNumber

        m_ReceivingCity = ReceivingCity
        m_ReceivingProvince = ReceivingProvince
        m_ReceivingName = ReceivingName
        m_ReceivingPostalCode = ReceivingPostalCode
        m_ReceivingCountry = ReceivingCountry

        m_ImageDir = ImageDir

        m_SendingAddress1 = SendingAddress1
        m_SendingAddress2 = SendingAddress2
        m_SendingAddress3 = SendingAddress3

        m_ReceivingAddress1 = ReceivingAddress1
        m_ReceivingAddress2 = ReceivingAddress2
        m_ReceivingAddress3 = ReceivingAddress3

        m_strLBS = PackageWeight

    End Sub

    'Send Void Shipping Request to UPS and mark tracking record with the appropriate void response from UPS
    Public Function SendVoidShipping() As Boolean

        Dim booVoided As Boolean
        Dim strStoredProcedure As String

        'Send Void response to UPS
        booVoided = SendResponse(ConfigurationSettings.AppSettings("XMLShipping_ShipVoid"), GenerateAccessRequest() & GenerateVoidShipping(), "VoidShipment")

        If booVoided Then

            strStoredProcedure = "spVoidUPS " & m_intTrackingInfoId & ",1,'" & Now() & "' "

            'Save void status
            Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            objConnection.Open()

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()
            objConnection.Dispose()

        End If

        Return booVoided

    End Function

    'Send Shipping Confirm Response to UPS intergration servers
    Public Function SendShippingConfirm() As Boolean

        Return SendResponse(ConfigurationSettings.AppSettings("XMLShipping_ShipConfirm"), GenerateAccessRequest() & GenerateShippingConfirm(), "ShipmentConfirm")

    End Function

    'Send Shipping Accept Request to UPS intergration servers
    Public Function SendShippingAccept() As Boolean

        Dim booReturn As Boolean

        If SendResponse(ConfigurationSettings.AppSettings("XMLShipping_ShipAccept"), GenerateAccessRequest() & GenerateShippingAccept(), "ShipmentAccept") Then
            booReturn = SaveShippingInfo()
        Else
            booReturn = False
        End If

        Return booReturn

    End Function

    'Generate XML text required by UPS for Confirmation response. - See UPS documention for furhter details
    Private Function GenerateShippingConfirm() As String

        Dim objIOStream As New System.IO.MemoryStream()
        Dim objXMLWriter As XmlTextWriter = Nothing
        Dim strPhone As String

        objXMLWriter = New XmlTextWriter(objIOStream, Encoding.UTF8)
        objXMLWriter.Formatting = Formatting.Indented
        objXMLWriter.WriteStartDocument(True)
        objXMLWriter.WriteStartElement("ShipmentConfirmRequest")

        objXMLWriter.WriteStartElement("Request")
        objXMLWriter.WriteStartElement("TransactionReference")
        objXMLWriter.WriteElementString("CustomerContext", "ShipConfirmUS")
        objXMLWriter.WriteElementString("XpciVersion", "1.0001")
        objXMLWriter.WriteEndElement() 'End TransactionReference
        objXMLWriter.WriteElementString("RequestAction", "ShipConfirm")
        objXMLWriter.WriteElementString("RequestOption", "nonvalidate")
        objXMLWriter.WriteEndElement() 'End Request

        objXMLWriter.WriteStartElement("LabelSpecification")
        objXMLWriter.WriteStartElement("LabelPrintMethod")
        objXMLWriter.WriteElementString("Code", "GIF")
        objXMLWriter.WriteEndElement() 'End LabelPrintMethod
        objXMLWriter.WriteElementString("HTTPUserAgent", "Mozilla/5.0")
        objXMLWriter.WriteStartElement("LabelStockSize")
        objXMLWriter.WriteElementString("Height", "4")
        objXMLWriter.WriteElementString("Width", "6")
        objXMLWriter.WriteEndElement() 'End LabelStockSize
        objXMLWriter.WriteStartElement("LabelImageFormat")
        objXMLWriter.WriteElementString("Code", "GIF")
        objXMLWriter.WriteEndElement() 'End LabelImageFormat
        objXMLWriter.WriteEndElement() 'End LabelSpecification

        objXMLWriter.WriteStartElement("Shipment")

        objXMLWriter.WriteStartElement("Shipper")
        objXMLWriter.WriteElementString("Name", m_SendingName.ToString.Trim)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Required For Non-US Store Requirements
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        objXMLWriter.WriteElementString("AttentionName", "Store Manager")

        If Len(strPhone) > 0 Then
            strPhone = Replace(m_ReceivingPhone.ToString.Trim, "(", "")
            strPhone = Replace(strPhone.ToString.Trim, ")", "")
            strPhone = Replace(strPhone.ToString.Trim, "-", "")
            strPhone = Replace(strPhone.ToString.Trim, ".", "")
            strPhone = Replace(strPhone.ToString.Trim, " ", "")
            objXMLWriter.WriteElementString("PhoneNumber", strPhone)
            'objXMLWriter.WriteStartElement("PhoneNumber")
            'objXMLWriter.WriteStartElement("StructuredPhoneNumber")
            'objXMLWriter.WriteElementString("PhoneCountryCode", )
            'objXMLWriter.WriteElementString("PhoneDialPlanNumber", Mid(strPhone, ))
            'objXMLWriter.WriteElementString("PhoneLineNumber", )
            'objXMLWriter.WriteEndElement() 'End StructuredPhoneNumber
            'objXMLWriter.WriteEndElement() 'End PhoneNumber
        End If

        objXMLWriter.WriteElementString("ShipperNumber", m_SendingShipperNumber.ToString.Trim)
        objXMLWriter.WriteStartElement("Address")

        If Len(m_SendingAddress1) > 0 Then
            objXMLWriter.WriteElementString("AddressLine1", m_SendingAddress1.ToString.Trim)
        End If
        If Len(m_SendingAddress2) > 0 Then
            objXMLWriter.WriteElementString("AddressLine2", m_SendingAddress2.ToString.Trim)
        End If
        If Len(m_SendingAddress3) > 0 Then
            objXMLWriter.WriteElementString("AddressLine3", m_SendingAddress3.ToString.Trim)
        End If

        objXMLWriter.WriteElementString("City", m_SendingCity.ToString.Trim)

        If UCase(m_SendingCountry.ToString.Trim) <> "PR" Then
            objXMLWriter.WriteElementString("StateProvinceCode", m_SendingProvince.ToString.Trim)
        End If

        objXMLWriter.WriteElementString("PostalCode", m_SendingPostalCode.ToString.Trim)
        objXMLWriter.WriteElementString("CountryCode", m_SendingCountry.ToString.Trim)
        objXMLWriter.WriteEndElement() 'End Address
        objXMLWriter.WriteEndElement() 'End Shipper

        objXMLWriter.WriteStartElement("ShipTo")
        objXMLWriter.WriteElementString("CompanyName", m_ReceivingName.ToString.Trim)


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Required For Non-US Store Requirements
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If m_TType = 3 Then
            objXMLWriter.WriteElementString("AttentionName", "Office Services")
        ElseIf m_TType = 4 Or m_TType = 2 Then
            objXMLWriter.WriteElementString("AttentionName", "Returns")
        ElseIf m_TType = 1 Then
            objXMLWriter.WriteElementString("AttentionName", "Store Manager")
        End If

        If Len(strPhone) > 0 Then
            strPhone = Replace(m_ReceivingPhone.ToString.Trim, "(", "")
            strPhone = Replace(strPhone.ToString.Trim, ")", "")
            strPhone = Replace(strPhone.ToString.Trim, "-", "")
            strPhone = Replace(strPhone.ToString.Trim, ".", "")
            strPhone = Replace(strPhone.ToString.Trim, " ", "")
            objXMLWriter.WriteElementString("PhoneNumber", strPhone)
            'objXMLWriter.WriteStartElement("PhoneNumber")
            'objXMLWriter.WriteStartElement("StructuredPhoneNumber")
            'objXMLWriter.WriteElementString("PhoneCountryCode", )
            'objXMLWriter.WriteElementString("PhoneDialPlanNumber", Mid(strPhone, ))
            'objXMLWriter.WriteElementString("PhoneLineNumber", )
            'objXMLWriter.WriteEndElement() 'End StructuredPhoneNumber
            'objXMLWriter.WriteEndElement() 'End PhoneNumber
        End If

        objXMLWriter.WriteStartElement("Address")
        If Len(m_ReceivingAddress1) > 0 Then
            objXMLWriter.WriteElementString("AddressLine1", m_ReceivingAddress1.ToString.Trim)
        End If
        If Len(m_ReceivingAddress2) > 0 Then
            objXMLWriter.WriteElementString("AddressLine2", m_ReceivingAddress2.ToString.Trim)
        End If
        If Len(m_ReceivingAddress3) > 0 Then
            objXMLWriter.WriteElementString("AddressLine3", m_ReceivingAddress3.ToString.Trim)
        End If
        objXMLWriter.WriteElementString("City", m_ReceivingCity.ToString.Trim)

        If UCase(m_ReceivingCountry.ToString.Trim) <> "PR" Then
            objXMLWriter.WriteElementString("StateProvinceCode", m_ReceivingProvince.ToString.Trim)
        End If

        objXMLWriter.WriteElementString("PostalCode", m_ReceivingPostalCode.ToString.Trim)
        objXMLWriter.WriteElementString("CountryCode", m_ReceivingCountry.ToString.Trim)
        objXMLWriter.WriteEndElement() 'End Address
        objXMLWriter.WriteEndElement() 'End ShipTo

        objXMLWriter.WriteStartElement("PaymentInformation")
        objXMLWriter.WriteStartElement("Prepaid")
        objXMLWriter.WriteStartElement("BillShipper")
        objXMLWriter.WriteElementString("AccountNumber", m_SendingShipperNumber.ToString.Trim)
        objXMLWriter.WriteEndElement() 'End BillShipper
        objXMLWriter.WriteEndElement() 'End PrePaid
        objXMLWriter.WriteEndElement() 'End PaymentInformation

        objXMLWriter.WriteStartElement("Service")
        objXMLWriter.WriteElementString("Code", m_ServiceCode.ToString.Trim)
        objXMLWriter.WriteEndElement() 'End Service

        objXMLWriter.WriteStartElement("Package")
        objXMLWriter.WriteStartElement("PackagingType")
        objXMLWriter.WriteElementString("Code", m_PackageType.ToString.Trim)
        objXMLWriter.WriteEndElement() 'End PackingType

        If m_PackageType.Trim <> "01" Then
            objXMLWriter.WriteStartElement("PackageWeight")
            objXMLWriter.WriteStartElement("UnitOfMeasurement")
            objXMLWriter.WriteElementString("Code", "LBS")
            objXMLWriter.WriteEndElement() 'End UnitOfMeasurement
            objXMLWriter.WriteElementString("Weight", m_strLBS.ToString)
            objXMLWriter.WriteEndElement() 'End Weight
        End If

        objXMLWriter.WriteStartElement("ReferenceNumber")
        objXMLWriter.WriteElementString("Code", "")

        If m_TType = 1 Then
            objXMLWriter.WriteElementString("Value", "STS")
        ElseIf m_TType = 2 Then
            objXMLWriter.WriteElementString("Value", "STDC")
        ElseIf m_TType = 3 Then
            objXMLWriter.WriteElementString("Value", "MMP")
        ElseIf m_TType = 4 Then
            objXMLWriter.WriteElementString("Value", "DDS " & m_strRANum)
        End If

        objXMLWriter.WriteEndElement() 'End ReferenceNumber

        objXMLWriter.WriteEndElement() 'End Package

        objXMLWriter.WriteEndElement() 'End Shipment

        objXMLWriter.WriteEndElement() 'End ShipmentConfirmRequest
        objXMLWriter.WriteEndDocument()

        objXMLWriter.Flush()

        objIOStream.Position = 0
        Dim r As New StreamReader(objIOStream)

        Return r.ReadToEnd

    End Function

    'Generate XML text required by UPS for Accept response. - See UPS documention for furhter details
    Private Function GenerateShippingAccept() As String

        Dim objIOStream As New System.IO.MemoryStream()
        Dim objXMLWriter As XmlTextWriter = Nothing

        objXMLWriter = New XmlTextWriter(objIOStream, Encoding.UTF8)
        objXMLWriter.Formatting = Formatting.Indented
        objXMLWriter.WriteStartDocument(True)
        objXMLWriter.WriteStartElement("ShipmentAcceptRequest")
        objXMLWriter.WriteStartElement("Request")
        objXMLWriter.WriteStartElement("TransactionReference")
        objXMLWriter.WriteElementString("CustomerContext", m_BoxId)
        objXMLWriter.WriteElementString("XpciVersion", "1.0001")
        objXMLWriter.WriteEndElement() 'End TransactionReference
        objXMLWriter.WriteElementString("RequestAction", "ShipAccept")
        objXMLWriter.WriteEndElement() 'End Request
        objXMLWriter.WriteElementString("ShipmentDigest", m_ShipmentDigest.ToString.Trim)
        objXMLWriter.WriteEndElement() 'End ShipmentAcceptRequest
        objXMLWriter.WriteEndDocument()

        objXMLWriter.Flush()

        objIOStream.Position = 0
        Dim r As New StreamReader(objIOStream)

        Return r.ReadToEnd

    End Function

    'Generate XML text required by UPS for Void response. - See UPS documention for furhter details
    Private Function GenerateVoidShipping() As String

        Dim objIOStream As New System.IO.MemoryStream()
        Dim objXMLWriter As XmlTextWriter = Nothing

        objXMLWriter = New XmlTextWriter(objIOStream, Encoding.UTF8)
        objXMLWriter.Formatting = Formatting.Indented
        objXMLWriter.WriteStartDocument(True)
        objXMLWriter.WriteStartElement("VoidShipmentRequest")
        objXMLWriter.WriteStartElement("Request")
        objXMLWriter.WriteStartElement("TransactionReference")
        objXMLWriter.WriteElementString("CustomerContext", "Void " & m_BoxId)
        objXMLWriter.WriteElementString("XpciVersion", "1.0001")
        objXMLWriter.WriteEndElement() 'End TransactionReference

        objXMLWriter.WriteElementString("RequestAction", "Void")
        objXMLWriter.WriteElementString("RequestOption", 1)

        objXMLWriter.WriteEndElement() 'End Request
        objXMLWriter.WriteElementString("ShipmentIdentificationNumber", m_ShipmentIdentificationNumber.ToString.Trim)
        objXMLWriter.WriteEndElement() 'End VoidShipmentRequest
        objXMLWriter.WriteEndDocument()
        objXMLWriter.Flush()

        objIOStream.Position = 0
        Dim r As New StreamReader(objIOStream)

        Return r.ReadToEnd

    End Function

    'Generate XML Access Request. This XML text needs to be sent along with every request sent over to UPS - See UPS documention for furhter details
    Private Function GenerateAccessRequest() As String

        Dim objIOStream As New System.IO.MemoryStream()
        Dim objXMLWriter As XmlTextWriter = Nothing

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetMYUPSAccountInfo", objConnection)

        objCommand.CommandType = CommandType.StoredProcedure

        Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@BoxId", SqlDbType.VarChar, 15)
        Dim parUserId As SqlClient.SqlParameter = objCommand.Parameters.Add("@UserId", SqlDbType.VarChar, 15)
        Dim parUserPassword As SqlClient.SqlParameter = objCommand.Parameters.Add("@UserPassword", SqlDbType.VarChar, 25)

        parBoxId.Direction = ParameterDirection.Input
        parUserId.Direction = ParameterDirection.Output
        parUserPassword.Direction = ParameterDirection.Output

        parBoxId.Value = m_BoxId

        objCommand.ExecuteNonQuery()

        objXMLWriter = New XmlTextWriter(objIOStream, Encoding.UTF8)
        objXMLWriter.Formatting = Formatting.Indented
        objXMLWriter.WriteStartDocument(True)
        objXMLWriter.WriteStartElement("AccessRequest")
        'objXMLWriter.WriteElementString("AccessLicenseNumber", "2B81D024C7D27224")
        'objXMLWriter.WriteElementString("AccessLicenseNumber", "AD097444C1CD2A66")
        objXMLWriter.WriteElementString("AccessLicenseNumber", ConfigurationSettings.AppSettings("AccessLicenseNumber"))
        'objXMLWriter.WriteElementString("UserId", "HTUPSAPI")
        'objXMLWriter.WriteElementString("Password", "ProteusControl2016")
        objXMLWriter.WriteElementString("UserId", parUserId.Value.ToString)
        objXMLWriter.WriteElementString("Password", parUserPassword.Value.ToString)
        objXMLWriter.WriteEndElement()
        objXMLWriter.WriteEndDocument()
        objXMLWriter.Flush()

        objIOStream.Position = 0

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Dim r As New StreamReader(objIOStream)

        Return r.ReadToEnd

    End Function

    'Send XML response to UPS via HttpWebResponse
    'This function is replaced with the proxy that we are calling from different server
    Private Function SendResponse_old(ByVal URL As String, ByVal strXMLRequest As String, ByVal strXMLRequestType As String) As Boolean

        Dim objXMLRequest As HttpWebRequest
        Dim strErrorCode As String
        Dim strErrorDescription As String
        Dim strMinimumRetrySeconds As String
        Dim strResponseStatusCode As String
        Dim booReturn As Boolean = 0
        Dim IOStream As System.IO.StreamWriter
        Dim objXMLResponse As HttpWebResponse

        Try

            objXMLRequest = CType(WebRequest.Create(URL), HttpWebRequest)

            'Configure HttpWebRequest object
            objXMLRequest.ContentType = "application/x-www-form-urlencoded"
            objXMLRequest.Method = "POST"
            objXMLRequest.ContentLength = Len(strXMLRequest)
            objXMLRequest.Timeout = ConfigurationSettings.AppSettings("UPSTimeOut")
            objXMLRequest.KeepAlive = False
            objXMLRequest.Connection = ConfigurationSettings.AppSettings("UPSHTTPConnections")

            IOStream = New System.IO.StreamWriter(objXMLRequest.GetRequestStream())

            IOStream.Write(strXMLRequest)
            IOStream.Flush()

            'Save Response from UPS
            objXMLResponse = CType(objXMLRequest.GetResponse(), HttpWebResponse)

            IOStream.Close()

            'Parse through response
            If objXMLResponse.StatusCode <> HttpStatusCode.OK Then
                LogXMLError("High", "0", "Missing UPS XML Response", "", "", strXMLRequest, strXMLRequestType)
                booReturn = 0
                objXMLResponse.Close()
            Else
                Dim IStream = New System.IO.StreamReader(objXMLResponse.GetResponseStream(), System.Text.Encoding.UTF8)
                Dim strXMLResponse As String = IStream.ReadToEnd()
                Dim objXMLTextReader As New System.Xml.XmlTextReader(strXMLResponse, XmlNodeType.Document, Nothing)

                objXMLResponse.Close()

                SaveXMLResponseRequest(strXMLRequest, strXMLResponse, strXMLRequestType)

                Dim objXMLPathDoc As New System.Xml.XPath.XPathDocument(objXMLTextReader)
                Dim objXMLPathNavigator As System.Xml.XPath.XPathNavigator = objXMLPathDoc.CreateNavigator()

                Dim objXMLNode As System.Xml.XPath.XPathNodeIterator = objXMLPathNavigator.Select("//ShipmentConfirmResponse/ShipmentCharges/TotalCharges/MonetaryValue/text()")
                If objXMLNode.MoveNext Then m_TotalCharges_Value = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ShipmentConfirmResponse/ShipmentCharges/TotalCharges/CurrencyCode/text()")
                If objXMLNode.MoveNext Then m_TotalCharges_Currency = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ResponseStatusCode/text()")
                If objXMLNode.MoveNext Then strResponseStatusCode = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ErrorSeverity/text()")
                If objXMLNode.MoveNext Then m_strErrorSeverity = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ErrorCode/text()")
                If objXMLNode.MoveNext Then strErrorCode = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ErrorDescription/text()")
                If objXMLNode.MoveNext Then strErrorDescription = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ImumretrySeconds/text()")
                If objXMLNode.MoveNext Then strMinimumRetrySeconds = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ShipmentDigest/text()")
                If objXMLNode.MoveNext Then m_ShipmentDigest = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ShipmentIdentificationNumber/text()")
                If objXMLNode.MoveNext Then m_ShipmentIdentificationNumber = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//TrackingNumber/text()")
                If objXMLNode.MoveNext Then m_TrackingNumber = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//GraphicImage/text()")
                If objXMLNode.MoveNext Then m_ShippingLabel = objXMLNode.Current.Value()

                'objXMLNode = objXMLPathNavigator.Select("//Error/text()")
                If strResponseStatusCode = 0 Then
                    LogXMLError(m_strErrorSeverity, strErrorCode, strErrorDescription, strMinimumRetrySeconds, strXMLResponse, strXMLRequest, strXMLRequestType)
                    RaiseEvent XMLErrorMsg(strErrorDescription)
                End If

                booReturn = strResponseStatusCode

                objXMLResponse.Close()

            End If

        Catch ex As Exception
            System.Diagnostics.EventLog.WriteEntry("UPSAPIFailure", ex.ToString())
            LogXMLError(m_strErrorSeverity, strErrorCode, ex.Message, strMinimumRetrySeconds, String.Empty, strXMLRequest, strXMLRequestType)
            'objXMLResponse.Close()
            booReturn = False

        End Try

        Return booReturn

    End Function
    'Send XML response to UPS via HttpWebResponse
    Private Function SendResponse(ByVal URL As String, ByVal strXMLRequest As String, ByVal strXMLRequestType As String) As Boolean

        Dim objXMLRequest As HttpWebRequest
        Dim strErrorCode As String
        Dim strErrorDescription As String
        Dim strMinimumRetrySeconds As String
        Dim strResponseStatusCode As String
        Dim booReturn As Boolean = 0
        Dim IOStream As System.IO.StreamWriter
        Dim objXMLResponse As HttpWebResponse
        Dim objResponse As Object

        Try

            'objXMLRequest = CType(WebRequest.Create(URL), HttpWebRequest)

            ''Configure HttpWebRequest object
            'objXMLRequest.ContentType = "application/x-www-form-urlencoded"
            'objXMLRequest.Method = "POST"
            'objXMLRequest.ContentLength = Len(strXMLRequest)
            'objXMLRequest.Timeout = ConfigurationSettings.AppSettings("UPSTimeOut")
            'objXMLRequest.KeepAlive = False
            'objXMLRequest.Connection = ConfigurationSettings.AppSettings("UPSHTTPConnections")

            'IOStream = New System.IO.StreamWriter(objXMLRequest.GetRequestStream())

            'IOStream.Write(strXMLRequest)
            'IOStream.Flush()

            ''Save Response from UPS
            'objXMLResponse = CType(objXMLRequest.GetResponse(), HttpWebResponse)

            'IOStream.Close()

            Dim svc As New UPSProcess.UPSProcessService
            Dim strSuccessStatus As Boolean = svc.CreateShipmentLabel(URL, strXMLRequest, objResponse)
            svc.Dispose()

            'Parse through response
            If strSuccessStatus = False Then
                Dim strXMLResponse As String = CType(objResponse, String)
                LogXMLError("High", "0", "Missing UPS XML Response", "", strXMLResponse, strXMLRequest, strXMLRequestType)
                booReturn = 0
                Return booReturn
            Else
                ' Dim IStream = New System.IO.StreamReader(objXMLResponse.GetResponseStream(), System.Text.Encoding.UTF8)
                'Dim strXMLResponse As String = IStream.ReadToEnd()
                Dim strXMLResponse As String = CType(objResponse, String)
                Dim objXMLTextReader As New System.Xml.XmlTextReader(strXMLResponse, XmlNodeType.Document, Nothing)

                'objXMLResponse.Close()

                SaveXMLResponseRequest(strXMLRequest, strXMLResponse, strXMLRequestType)

                Dim objXMLPathDoc As New System.Xml.XPath.XPathDocument(objXMLTextReader)
                Dim objXMLPathNavigator As System.Xml.XPath.XPathNavigator = objXMLPathDoc.CreateNavigator()

                Dim objXMLNode As System.Xml.XPath.XPathNodeIterator = objXMLPathNavigator.Select("//ShipmentConfirmResponse/ShipmentCharges/TotalCharges/MonetaryValue/text()")
                If objXMLNode.MoveNext Then m_TotalCharges_Value = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ShipmentConfirmResponse/ShipmentCharges/TotalCharges/CurrencyCode/text()")
                If objXMLNode.MoveNext Then m_TotalCharges_Currency = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ResponseStatusCode/text()")
                If objXMLNode.MoveNext Then strResponseStatusCode = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ErrorSeverity/text()")
                If objXMLNode.MoveNext Then m_strErrorSeverity = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ErrorCode/text()")
                If objXMLNode.MoveNext Then strErrorCode = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ErrorDescription/text()")
                If objXMLNode.MoveNext Then strErrorDescription = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ImumretrySeconds/text()")
                If objXMLNode.MoveNext Then strMinimumRetrySeconds = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ShipmentDigest/text()")
                If objXMLNode.MoveNext Then m_ShipmentDigest = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//ShipmentIdentificationNumber/text()")
                If objXMLNode.MoveNext Then m_ShipmentIdentificationNumber = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//TrackingNumber/text()")
                If objXMLNode.MoveNext Then m_TrackingNumber = objXMLNode.Current.Value()

                objXMLNode = objXMLPathNavigator.Select("//GraphicImage/text()")
                If objXMLNode.MoveNext Then m_ShippingLabel = objXMLNode.Current.Value()

                'objXMLNode = objXMLPathNavigator.Select("//Error/text()")
                If strResponseStatusCode = 0 Then
                    LogXMLError(m_strErrorSeverity, strErrorCode, strErrorDescription, strMinimumRetrySeconds, strXMLResponse, strXMLRequest, strXMLRequestType)
                    RaiseEvent XMLErrorMsg(strErrorDescription)
                End If

                booReturn = strResponseStatusCode

                ' objXMLResponse.Close()

            End If

        Catch ex As Exception
            System.Diagnostics.EventLog.WriteEntry("UPSAPIFailure", ex.ToString())
            LogXMLError(m_strErrorSeverity, strErrorCode, ex.Message, strMinimumRetrySeconds, String.Empty, strXMLRequest, strXMLRequestType)
            'objXMLResponse.Close()
            booReturn = False

        End Try

        Return booReturn

    End Function

    'Save entire XML response/request in memory
    Private Function SaveXMLResponseRequest(ByVal strXMLRequest As String, ByVal strXMLResponse As String, ByVal strXMLRequestType As String)

        Select Case strXMLRequestType

            Case "ShipmentConfirm"
                m_ShipmentConfirm_Response = strXMLResponse
                m_ShipmentConfirm_Request = strXMLRequest

            Case "ShipmentAccept"
                m_ShipmentAccept_Response = strXMLResponse
                m_ShipmentAccept_Request = strXMLRequest

        End Select

    End Function

    'Save Shipping label to hard drive
    Private Function SaveShippingInfo() As Boolean

        Try
            Dim bytShippingLabel As Byte() = Convert.FromBase64String(m_ShippingLabel)
            Dim fstream As New FileStream(m_ImageDir & "\ShippingLabel_" & m_TrackingNumber & ".gif", FileMode.OpenOrCreate)
            Dim bytstream As New BinaryWriter(fstream)

            bytstream.Write(bytShippingLabel)
            bytstream.Flush()
            bytstream.Close()
            fstream.Close()

            m_ImageFileName = "ShippingLabel_" & m_TrackingNumber & ".gif"

            Return True
        Catch
            Return False
        End Try

    End Function

    'Log XML errors generated during UPS communication
    Private Sub LogXMLError(ByVal ErrorSeverity As String, ByVal ErrorCode As String, ByVal ErrorDescription As String, ByVal MinimumRetrySeconds As String, ByVal XMLResponse As String, ByVal XMLRequest As String, ByVal XMLRequest_Type As String)

        Dim strSQL As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objSQLDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        strSQL = "INSERT INTO UPS_XML_Errors(Box_Id,XMLRequest_Type,ErrorSeverity,ErrorCode,ErrorDescription,MinimumRetrySeconds,Created_Date,XMLResponse,XMLRequest) "
        strSQL = strSQL & "Values('" & m_BoxId & "','" & XMLRequest_Type & "','" & Replace(ErrorSeverity, "'", "''") & "','" & Replace(ErrorCode, "'", "''") & "','" & Replace(ErrorDescription, "'", "''") & "','" & MinimumRetrySeconds & "','" & Now() & "','" & Replace(XMLResponse, "'", "''") & "','" & Replace(XMLRequest, "'", "''") & "') "

        Dim objACommand As New SqlClient.SqlCommand(strSQL, objConnection)
        objACommand.ExecuteNonQuery()

        objACommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

End Class
