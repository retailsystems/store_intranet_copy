<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AdvancedResults.aspx.vb" Inherits="SSTransfer.AdvancedResults" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Search Results</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server" />
	</HEAD>
	<body class="sBody" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="frmTransferHome" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="4" width="100%"  border="0">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="top" align="middle">
						<table height="100%" cellSpacing="0" cellPadding="4" width="100%" border="0">
							<tr>
								<td><asp:Label ID="lblResults" Runat="server" CssClass="sScanItemBlackLabel"></asp:Label></td>
							</tr>
							<tr>
								<td valign="top" align="middle">
									<div style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 380px" class="sDatagrid">
										<table class="sDatagridTable" cellSpacing="0" cellPadding="2" width="100%">
											<tr>
												<td>
													<asp:datagrid CssClass="sDatagrid" id="dgResults" OnSortCommand="Sort" AllowSorting="True" height="100%" AutoGenerateColumns="False" Runat="server" Width="100%" CellSpacing="1" CellPadding="2">
														<AlternatingItemStyle CssClass="sDatagridAlternatingItem"></AlternatingItemStyle>
														<ItemStyle CssClass="sDatagridItem"></ItemStyle>
														<HeaderStyle CssClass="sDatagridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn HeaderStyle-Width="12%" HeaderText="Transfer #" SortExpression="Box_Xfer_Hdr.Box_Id">
																<HeaderStyle CssClass="sDatagridHeader" HorizontalAlign="Left"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left"></ItemStyle>
																<ItemTemplate>
																	<a target=_self href='ViewTransfer.aspx?BID=<%# container.dataitem("Box_Id") %>&RET=<%# GetReturnQuery() %> '>
																		<asp:Label Runat="server" ID="sdf">
																			<%# container.dataitem("Box_Id") %>
																		</asp:Label>
																	</a>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Tracking #" SortExpression="Tracking_Num">
																<HeaderStyle CssClass="sDatagridHeader"></HeaderStyle>
																<ItemTemplate>
																	<asp:HyperLink Target=_blank ID=TL1 Runat=server NavigateUrl='<%# Replace(Container.dataitem("TrackingURL"),"{0}", Container.dataitem("Tracking_Num")) %>'>
																		<%# Container.dataitem("Tracking_Num") %>
																	</asp:HyperLink>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="Sending_Store" SortExpression="Sending_Store" HeaderText="From Store"></asp:BoundColumn>
															<asp:BoundColumn DataField="Receiving_Store" SortExpression="Receiving_Store" HeaderText="To Store"></asp:BoundColumn>
															<asp:BoundColumn DataField="Status_Desc" SortExpression="Box_Status_Type.Status_Desc" HeaderText="Current Status"></asp:BoundColumn>
															<asp:BoundColumn DataField="Shipment_Date" dataformatstring="{0:d}" SortExpression="Box_Xfer_Hdr.Shipment_Date" HeaderText="Shipped"></asp:BoundColumn>
															<asp:BoundColumn DataField="Received_Date" dataformatstring="{0:d}" SortExpression="Box_Xfer_Hdr.Received_Date" HeaderText="Received"></asp:BoundColumn>
															<asp:TemplateColumn HeaderStyle-Width="5%" HeaderText="Manifest">
																<HeaderStyle CssClass="sDatagridHeader" HorizontalAlign="center"></HeaderStyle>
																<ItemStyle HorizontalAlign="center"></ItemStyle>
																<ItemTemplate>
																	<asp:PlaceHolder Runat=server Visible='<%# Container.DataItem("ManifestDisplay") %>' ID="plManifest">
																		<a target=_blank href='GenerateManifest.aspx?BID=<%# container.dataitem("Box_Id") %>'>
																			<asp:Image Runat="server" ID="imgManifest" ImageUrl='Images\Printed.gif'></asp:Image>
																		</a>
																	</asp:PlaceHolder>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid>
												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td></td>
							</tr>
							<tr>
								<td noWrap align="right"><asp:button id="btnReturn" Runat="server" Text="Return" CssClass="sButton"></asp:button>&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<TR>
					<TD height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></TD>
				</TR>
			</table>
		</form>
	</body>
</HTML>
