Public Class SelectShipping
    Inherits System.Web.UI.Page

    Protected WithEvents lblInstructions As System.Web.UI.WebControls.Label
    Protected WithEvents lblRetry As System.Web.UI.WebControls.Label
    Protected WithEvents ucHeader As Header
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ddlPackageType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlService As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlParcel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlPackage As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblHelpLabel As System.Web.UI.WebControls.Label
    Protected WithEvents lblParcelInfo As System.Web.UI.WebControls.Label
    Protected WithEvents rbUPS As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rbLabel As System.Web.UI.WebControls.RadioButton
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents rbOther As System.Web.UI.WebControls.RadioButton
    Protected WithEvents ddlOther As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtShippingDM As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtHandDMApproval As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtApproval As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblApproval As System.Web.UI.WebControls.Label
    Protected WithEvents trUPS As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trLabel As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trOther As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tbOther As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tbUPS As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblabel As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

    Private m_booUPSOn As Boolean
    Private m_strRANum As String
    Private m_bytErr As Byte
    Private m_strBoxId As String
    Private m_TType As Int32
    Private m_strLBS As String
    Private m_intReceivingStore As Integer
    Private m_intSendingStore As Integer

    Private m_objTransferLock As New TransferLock()
    Private m_objUserInfo As New UserInfo()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Clear error popups
        pageBody.Attributes.Add("onload", "")
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        'Get UserInfo
        m_objUserInfo.GetEmployeeInfo()

        'If employee is logged on to the system then their locks are kept alive
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
            m_objTransferLock.KeepLocksAlive(m_objUserInfo.EmpId)
        Else
            Response.Redirect("SessionEnd.aspx?Mode=1")
        End If

        'If store number not set then redirect to set store page
        If m_objUserInfo.StoreNumber = -1 Or m_objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

        'Set header properties
        ucHeader.lblTitle = "Shipping Label"
		ucHeader.CurrentMode(SSTransfer.Header.HeaderGroup.Transfer)
		ucHeader.CurrentPage(SSTransfer.Header.PageName.NewTransfer)
        ucHeader.DisableButtons()

        'Get variables from the querystring
        m_strBoxId = Request("BID")

        'Get querystring variables
        m_bytErr = Request("Err") 'Error returned from generating UPS XML shipping label
        m_booUPSOn = Request("UPSOn") 'Enable/Disable XML UPS XML shipping option
        m_TType = Request("TType") ' Return/Store to Store
        m_strLBS = Request("LBS")
        m_strRANum = Request("RA")

        'Get sending and receiving stores (need to do this on every page load as the
        'member variables don't persist)
        Dim objTransfer As New Transfer(m_strBoxId)

        m_intReceivingStore = objTransfer.ReceivingStore()
        m_intSendingStore = objTransfer.SendingStore()

        'If page is not post back then fill drop down lists
        If Not Page.IsPostBack Then

            FillParcelDropdown() 'fill manual shipping label parcel service dropdown
            FillUPSPackageDropdown() 'fill ups package type dropdown

            'If m_TType = 3 Then
            '  ddlPackage.SelectedIndex = ddlPackage.Items.IndexOf(ddlPackage.Items.FindByValue("04"))
            '  End If


            SetShipmentDefaults(True, False)
            FillUPSServiceDropdown() 'Fill ups service type dropdown
            SetShipmentDefaults(False, True)

            ' If m_TType = 3 And ddlPackage.SelectedIndex = ddlPackage.Items.IndexOf(ddlPackage.Items.FindByValue("04")) Then
            '     ddlService.SelectedIndex = ddlService.Items.IndexOf(ddlService.Items.FindByValue("02"))
            ' Else
            '      ddlService.SelectedIndex = ddlService.Items.IndexOf(ddlService.Items.FindByValue("03"))
            '  End If

        End If

        'Disable all UPS XML shipping options on hard error
        If Not m_booUPSOn And Not rbOther.Checked Then
            rbLabel.Checked = True
            ScanLabel_Selected()
        End If

        'display error results to user
        Select Case m_bytErr
            Case 1 'Transient Error
                lblHelpLabel.Text = "Carrier's servers are temporary unavailable.<br>Please try again in a few minutes."
            Case 2 'Hard Error
                lblHelpLabel.Text = "An error has occurred while trying to generate a label.<br>Please contact support."
            Case Else
                lblHelpLabel.Text = "Please select one of the following choices."
        End Select

    End Sub

    Private Sub SetShipmentDefaults(ByVal Package As Boolean, ByVal Service As Boolean)

        Dim FirstItem As Boolean = True
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open() 'open DB connection

        Dim objCommand As New SqlClient.SqlCommand("spGetUPSShipmentDefaults " & m_TType & "," & m_intSendingStore & "," & m_intReceivingStore, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader() 'fill datareader

        While objDataReader.Read()

            If Package Then ddlPackage.SelectedIndex = ddlPackage.Items.IndexOf(ddlPackage.Items.FindByValue(objDataReader("PackageCode")))
            If Service Then ddlService.SelectedIndex = ddlService.Items.IndexOf(ddlService.Items.FindByValue(objDataReader("ServiceCode")))

        End While

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub


    'Fills dropdown for UPS XML service types(ie next day, 2-day,etc)
    Private Sub FillUPSServiceDropdown()

        Dim FirstItem As Boolean = True
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open() 'open DB connection

        Dim objCommand As New SqlClient.SqlCommand("spGetUPSServiceCodes '" & ddlPackage.SelectedItem.Value & "'," & m_TType & "," & m_intSendingStore & "," & m_intReceivingStore & ",'" & m_objUserInfo.EmpJobCode & "'", objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader() 'fill datareader

        ddlService.Items.Clear()

        Dim FirstListItem As New ListItem()
        FirstListItem.Text = "Select a service"
        FirstListItem.Value = "00"

        ddlService.Items.Add(FirstListItem)

        'Fill service dropdown from DB
        While objDataReader.Read()

            Dim newListItem As New ListItem()

            newListItem.Text = objDataReader("Description")
            newListItem.Value = objDataReader("Code")

            If objDataReader("DM_Approval") Then
                'If objDataReader("DM_Approval") And (Not m_objUserInfo.IsLead And Not m_objUserInfo.IsAssociate And Not m_objUserInfo.IsKeyholder) Then
                ddlService.Items.Add(newListItem)
                'ElseIf ddlPackage.SelectedItem.Value = "04" Then 'Add all services for Monday Mail Pak
                ' ddlService.Items.Add(newListItem)
            ElseIf Not objDataReader("DM_Approval") Then
                ddlService.Items.Add(newListItem)
            End If

        End While

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Fills Parcel company to dropdown for manual shipping label input option
    Private Sub FillParcelDropdown()

        Dim FirstItem As Boolean = True
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spGetParcelServices " & CInt(System.Web.HttpContext.Current.Request.Cookies("StoreNo").Value), objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader()

        'Dim FirstListItem As New ListItem()
        'FirstListItem.Text = "Select a parcel service"
        'FirstListItem.Value = "00"

        'ddlParcel.Items.Add(FirstListItem)

        'Gather data from datareader
        While objDataReader.Read()

            Dim newListItem As New ListItem()
            newListItem.Text = objDataReader("ParcelCompany")
            newListItem.Value = objDataReader("ParcelService_Cd")

            ddlParcel.Items.Add(newListItem)

        End While

        ddlParcel.SelectedIndex = 1

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Fills dropdown with UPS package types associated to UPS XML shipping
    Private Sub FillUPSPackageDropdown()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spGetUPSPackageTypes " & m_TType & "," & m_intSendingStore & "," & m_intReceivingStore, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader()
        ddlPackage.DataTextField = "ALT_Description" 'Hottopic's own description for UPS package types
        ddlPackage.DataValueField = "Code"
        ddlPackage.DataSource = objDataReader
        ddlPackage.DataBind()

        'Dim newListItem As New ListItem()
        'newListItem.Text = "Select a package type"
        'newListItem.Value = "00"
        'ddlPackage.Items.Insert(0, newListItem)

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Private Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    'If rbUPS.Checked Then
    'Response.Redirect("UPSProcessing.aspx?BID=" & m_strBoxId)
    'ElseIf rbOther.Checked Then
    'Response.Redirect("UPSManuelInput.aspx?BID=" & m_strBoxId)
    'ElseIf rbSave.Checked Then

    'Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
    'objConnection.Open()

    'Dim objCommand As New SqlClient.SqlCommand("spUpdateBoxHdrStatus", objConnection)
    'Dim objDataReader As SqlClient.SqlDataReader
    'objCommand.CommandType = CommandType.StoredProcedure

    'Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@BID", SqlDbType.VarChar, 14)
    'Dim parBoxStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@Status", SqlDbType.TinyInt)
    'Dim parModifiedDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@Modified", SqlDbType.DateTime)

    'parBoxId.Direction = ParameterDirection.Input
    'parBoxStatus.Direction = ParameterDirection.Input
    'parModifiedDate.Direction = ParameterDirection.Input

    'parBoxId.Value = m_strBoxId
    'parBoxStatus.Value = 6 'packing
    'parModifiedDate.Value = Now()

    'objCommand.ExecuteNonQuery()

    'objConnection.Close()
    'Response.Redirect("default.aspx")

    'End If

    'End Sub

    'Event generated when a user changes a parcel type via the parcel dropdown list
    Private Sub ddlParcel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlParcel.SelectedIndexChanged

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spGetParcelServiceNotes " & ddlParcel.SelectedItem.Value, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader() 'fill datareader

        'Display parcel service constrants
        If objDataReader.Read Then
            lblParcelInfo.Text = objDataReader("Notes")
        End If

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'If Check to see if selected UPS Service Code needs DM Approval
    Private Sub ddlService_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlService.SelectedIndexChanged

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetUPSPackageTypeDMApproval '" & ddlService.SelectedItem.Value & "'", objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader()

        txtApproval.Visible = False
        lblApproval.Visible = False

        'Display approval text if DM_Approval is True
        If objDataReader.Read Then

            If objDataReader("DM_Approval") And m_TType <> 3 Then
                txtApproval.Visible = True
                lblApproval.Visible = True
            End If

        End If

        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Change box status to packing
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer(m_strBoxId) 'Unlock box_id

        Response.Redirect("default.aspx") 'Direct to info page

    End Sub

    'Change dropdown options when user changes an UPS package type
    Private Sub ddlPackage_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPackage.SelectedIndexChanged

        Select Case ddlPackage.SelectedItem.Value

            Case "00" 'package type not selected
                'ddlService.Items.Clear()
                FillUPSServiceDropdown()

                'ddlService.SelectedIndex = 0
                ddlService.Enabled = False

                lblApproval.Visible = False
                txtApproval.Visible = False

                'Case "01" 'UPS letter
                'ddlService.Items.Remove(ddlService.Items.FindByValue("03"))
                'ddlService.SelectedIndex = 0

                'txtApproval.Visible = False
                'lblApproval.Visible = False
                'ddlService.Enabled = True

            Case "04" 'UPS Pak
                'ddlService.SelectedIndex = ddlService.Items.IndexOf(ddlService.Items.FindByValue("02"))

                FillUPSServiceDropdown()
                txtApproval.Visible = False
                lblApproval.Visible = False
                'ddlService.Enabled = True

            Case Else
                'ddlService.Items.Clear()
                FillUPSServiceDropdown()

                txtApproval.Visible = False
                lblApproval.Visible = False
                ddlService.Enabled = True

        End Select

    End Sub

    'Process user selections
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim strError As String
        Dim booError As Boolean = False

        If rbLabel.Checked Then 'Manual label

            'If parcel service not selected then display popup error to user
            If ddlParcel.SelectedItem.Value = 0 Then
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Parcel Service Required.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            Else 'redirect to manual label input screen
                Response.Redirect("ScanLabel.aspx?LBS=" & m_strLBS & "&TType=" & m_TType & "&BID=" & m_strBoxId & "&PARCEL=" & ddlParcel.SelectedItem.Value & "&Approval=" & Server.UrlEncode(txtHandDMApproval.Text) & "&RA=" & m_strRANum)
            End If

        ElseIf rbUPS.Checked Then 'Generate UPS label via XML

            'If package type not selected then display error to user
            If ddlPackage.SelectedItem.Value = "00" Then
                booError = True
                strError = "Package Type Required.<br>"
            End If

            'If service type not selected display error to user
            If ddlService.SelectedItem.Value = "00" Then
                booError = True
                strError = strError & "Service Type Required.<br>"
            End If

            If txtApproval.Visible Then
                If Len(txtApproval.Text) <= 0 Then
                    booError = True
                    strError = strError & "DM Approval Required.<br>"
                End If
            End If

            'If error was generated display popoup else generate UPS label
            If booError Then
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(strError) & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            ElseIf ddlService.SelectedItem.Value >= "50" Then 'Fedex
                Response.Redirect("FedexProcessing.aspx?LBS=" & m_strLBS & "&TType=" & m_TType & "&BID=" & m_strBoxId & "&PAK=" & ddlPackage.SelectedItem.Value & "&SER=" & ddlService.SelectedItem.Value & "&Approval=" & Server.UrlEncode(txtApproval.Text) & "&RA=" & m_strRANum)
            Else 'UPS
                Response.Redirect("UPSProcessing.aspx?LBS=" & m_strLBS & "&TType=" & m_TType & "&BID=" & m_strBoxId & "&PAK=" & ddlPackage.SelectedItem.Value & "&SER=" & ddlService.SelectedItem.Value & "&Approval=" & Server.UrlEncode(txtApproval.Text) & "&RA=" & m_strRANum)
            End If

        ElseIf rbOther.Checked Then 'Generate Set to HandCarry

            If txtHandDMApproval.Visible Then
                If Len(txtHandDMApproval.Text) <= 0 Then
                    booError = True
                    strError = strError & "DM Approval Required.<br>"
                End If
            End If

            If booError Then
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(strError) & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            Else
                If InsertHandCarry() Then
                    ChangeBoxStatus()
                    UpdateBoxHistory(2)

                    'Output OMS/WMS Interface file
                    Dim objTransfer As New Transfer(m_strBoxId)
                    Dim objStore As New Store(objTransfer.ReceivingStore)

                    If ConfigurationSettings.AppSettings("RASNFlatFileOn") = "1" And m_TType = 2 Then
                        Dim objBizTalkInterface As New BizTalkInterface(Replace(ConfigurationSettings.AppSettings("WMS_RASNFlatFileDir"), "~DC~", objTransfer.ReceivingStore))
                        objBizTalkInterface.CreateFlatFile(m_strBoxId) ' Create WMS Flat File
                    ElseIf ConfigurationSettings.AppSettings("OMSFlatFileOn") = "1" And objStore.IsInternetStore() Then
                        Dim objBizTalkInterface As New BizTalkInterface(ConfigurationSettings.AppSettings("OMS_ASNFlatFileDir"))
                        objBizTalkInterface.CreateFlatFile(m_strBoxId) ' Create WMS Flat File
                    End If

                    If m_TType = 3 Then
                        Response.Redirect("CreateNewTrans.aspx")
                    ElseIf m_TType = 4 Then
                        Dim objDDSReturn As New DDSReturn(ConfigurationSettings.AppSettings("strDDSReturnConn"))

                        objDDSReturn.UpdateRAShipped(m_strRANum, m_objUserInfo.StoreNumber, m_strBoxId, True, Now())

                        Response.Redirect("CreateNewTrans.aspx")
                    Else
                        Response.Redirect("TransferProcessing.aspx?TType=" & m_TType & "&BID=" & m_strBoxId)
                    End If
                End If
            End If

        End If

    End Sub

    'This function sets box tracking info to tracking
    Private Function InsertHandCarry() As Boolean

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spInsertBoxTrackingInfo", objConnection)

        objCommand.CommandType = CommandType.StoredProcedure

        Dim prmBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@BoxId", SqlDbType.VarChar, 14)
        Dim prmParcelService As SqlClient.SqlParameter = objCommand.Parameters.Add("@ParcelService", SqlDbType.SmallInt)
        Dim prmShipmentId As SqlClient.SqlParameter = objCommand.Parameters.Add("@ShipmentId", SqlDbType.VarChar, 18)
        Dim prmTrackingNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@TrackingNum", SqlDbType.VarChar, 30)
        Dim prmXMLShipping As SqlClient.SqlParameter = objCommand.Parameters.Add("@XMLShipping", SqlDbType.Bit)
        'Dim prmBoxStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@BoxStatus", SqlDbType.TinyInt)
        Dim prmCreated As SqlClient.SqlParameter = objCommand.Parameters.Add("@Created", SqlDbType.DateTime)
        Dim prmModified As SqlClient.SqlParameter = objCommand.Parameters.Add("@Modified", SqlDbType.DateTime)
        Dim prmTrackingInfoId As SqlClient.SqlParameter = objCommand.Parameters.Add("@TrackingInfo_Id", SqlDbType.Int)
        Dim prmApproved_By As SqlClient.SqlParameter = objCommand.Parameters.Add("@Approved_By", SqlDbType.VarChar, 35)
        Dim prmUPS_PackageType_Code As SqlClient.SqlParameter = objCommand.Parameters.Add("@UPS_PackageType_Code", SqlDbType.VarChar, 2)
        Dim prmUPS_Service_Code As SqlClient.SqlParameter = objCommand.Parameters.Add("@UPS_Service_Code", SqlDbType.VarChar, 2)


        'Set parameter directions
        prmBoxId.Direction = ParameterDirection.Input
        prmParcelService.Direction = ParameterDirection.Input
        prmShipmentId.Direction = ParameterDirection.Input
        prmTrackingNum.Direction = ParameterDirection.Input
        prmXMLShipping.Direction = ParameterDirection.Input
        'prmBoxStatus.Direction = ParameterDirection.Input
        prmCreated.Direction = ParameterDirection.Input
        prmModified.Direction = ParameterDirection.Input
        prmApproved_By.Direction = ParameterDirection.Input
        prmUPS_PackageType_Code.Direction = ParameterDirection.Input
        prmUPS_Service_Code.Direction = ParameterDirection.Input
        prmTrackingInfoId.Direction = ParameterDirection.Output

        'Set parameter values
        prmBoxId.Value = m_strBoxId
        prmTrackingNum.Value = "[ Hand Carry ]" ' tracking number
        prmXMLShipping.Value = 0 'mark XML generated shipping number to false
        'prmBoxStatus.Value = 2 'Shipped
        prmParcelService.Value = 0 'No parcel service
        prmShipmentId.Value = DBNull.Value ' XML required field not needed for manual input labels

        prmUPS_PackageType_Code.Value = DBNull.Value
        prmUPS_Service_Code.Value = DBNull.Value
        prmApproved_By.Value = txtHandDMApproval.Text

        Dim strDate = Now()

        'set timestamps
        prmCreated.Value = strDate
        prmModified.Value = strDate

        objCommand.ExecuteNonQuery() 'execute query

        If prmTrackingInfoId.Value = -1 Then
            Return False
        Else
            Return True
        End If

        'clean up connection/command objects
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Function

    'Change box status to shipped for the current box
    Private Sub ChangeBoxStatus()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateBoxHdrStatus", objConnection)
        Dim objDataReader As SqlClient.SqlDataReader
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@BID", SqlDbType.VarChar, 14)
        Dim parBoxStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@Status", SqlDbType.TinyInt)
        Dim parModifiedDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@Modified", SqlDbType.DateTime)

        parBoxId.Direction = ParameterDirection.Input
        parBoxStatus.Direction = ParameterDirection.Input
        parModifiedDate.Direction = ParameterDirection.Input

        parBoxId.Value = m_strBoxId
        parBoxStatus.Value = 2 'shipped
        parModifiedDate.Value = Now()

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Update box history
    Private Sub UpdateBoxHistory(ByVal intBoxStatus As Int32)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateBoxHistory '" & m_strBoxId & "'," & intBoxStatus & ",'" & m_objUserInfo.EmpId & "','" & Replace(m_objUserInfo.EmpFullName, "'", "''") & "','" & Now() & "' ", objConnection)

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Set interface
    Private Sub ScanLabel_Selected()

        ddlParcel.Enabled = True
        ddlService.Enabled = False
        ddlOther.Enabled = False

        txtHandDMApproval.Enabled = False
        txtHandDMApproval.Text = ""

        txtApproval.Visible = False
        lblApproval.Visible = False

        ddlPackage.Enabled = False
        'ddlPackage.SelectedIndex = 0
        'ddlService.SelectedIndex = 0
        FillUPSServiceDropdown()

        trOther.Attributes("class") = "sTableHighlight"
        trUPS.Attributes("class") = "sTableHighlight"
        trLabel.Attributes("class") = "sSelectShipDarkBG"

        tbOther.Attributes("class") = "sInfoTable"
        tbUPS.Attributes("class") = "sInfoTable"
        tblabel.Attributes("class") = "sDialogTable"

        'trOther.Attributes("bgcolor") = "#FF9ACE"
        'trUPS.Attributes("bgcolor") = "#FF9ACE"
        'trLabel.Attributes("bgcolor") = "#D60C8C"

        'tbOther.Attributes("bordercolor") = "#FF9ACE"
        'tbUPS.Attributes("bordercolor") = "#FF9ACE"
        'tblabel.Attributes("bordercolor") = "#D60C8C"

    End Sub

    'Set interface
    Private Sub UPS_Selected()
        ddlParcel.Enabled = False
        'ddlParcel.SelectedIndex = 0
        'ddlPackage.SelectedIndex = 0
        ddlPackage.Enabled = True
        'ddlService.SelectedIndex = 0
        ddlService.Enabled = True
        ddlOther.Enabled = False
        txtApproval.Visible = False
        txtApproval.Text = ""
        lblApproval.Visible = False
        txtHandDMApproval.Enabled = False
        txtHandDMApproval.Text = ""
        lblParcelInfo.Text = ""

        FillUPSServiceDropdown()

        trOther.Attributes("class") = "sTableHighlight"
        trUPS.Attributes("class") = "sSelectShipDarkBG"
        trLabel.Attributes("class") = "sTableHighlight"

        tbOther.Attributes("class") = "sInfoTable"
        tbUPS.Attributes("class") = "sDialogTable"
        tblabel.Attributes("class") = "sInfoTable"

        'trOther.Attributes("bgcolor") = "#FF9ACE"
        'trUPS.Attributes("bgcolor") = "#D60C8C"
        'trLabel.Attributes("bgcolor") = "#FF9ACE"

        'tbOther.Attributes("bordercolor") = "#FF9ACE"
        'tbUPS.Attributes("bordercolor") = "#D60C8C"
        'tblabel.Attributes("bordercolor") = "#FF9ACE"
    End Sub

    'Set Interface
    Private Sub Other_Selected()
        ddlParcel.Enabled = False
        'ddlParcel.SelectedIndex = 0
        'ddlPackage.SelectedIndex = 0
        txtHandDMApproval.Enabled = True
        ddlOther.Enabled = True
        'ddlService.SelectedIndex = 0
        ddlService.Enabled = True
        txtApproval.Visible = False
        lblApproval.Visible = False
        txtApproval.Text = ""
        ddlPackage.Enabled = False
        ddlService.Enabled = False

        FillUPSServiceDropdown()

        trOther.Attributes("class") = "sSelectShipDarkBG"
        trUPS.Attributes("class") = "sTableHighlight"
        trLabel.Attributes("class") = "sTableHighlight"

        tbOther.Attributes("class") = "sDialogTable"
        tbUPS.Attributes("class") = "sInfoTable"
        tblabel.Attributes("class") = "sInfoTable"

        'trOther.Attributes("bgcolor") = "#D60C8C"
        'trUPS.Attributes("bgcolor") = "#FF9ACE"
        'trLabel.Attributes("bgcolor") = "#FF9ACE"

        'tbOther.Attributes("bordercolor") = "#D60C8C"
        'tbUPS.Attributes("bordercolor") = "#FF9ACE"
        'tblabel.Attributes("bordercolor") = "#FF9ACE"
    End Sub

    'enable options associated to manual label group
    Private Sub rbLabel_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbLabel.CheckedChanged

        ScanLabel_Selected()

    End Sub

    'enable options associated to UPS XML label group
    Private Sub rbUPS_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbUPS.CheckedChanged
        UPS_Selected()
    End Sub

    Private Sub rbOther_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbOther.CheckedChanged
        Other_Selected()
    End Sub

    Private Sub btnSubmit_Load(sender As Object, e As System.EventArgs) Handles btnSubmit.Load

    End Sub
End Class
