Public Class UPSProcessing
    Inherits System.Web.UI.Page
    Protected WithEvents lblNotice As System.Web.UI.WebControls.Label
    Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private m_TType As Int32 'Return/Store to Store
    Private m_UPSServiceCode As String
    Private m_UPSPackageType As String
    Private m_strApprovedBy As String
    Private m_strBoxId As String
    Private m_objUserInfo As New UserInfo() 'User Info
    Private m_objTransferLock As New TransferLock()
    Private m_strLBS As String
    Private m_strRANum As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Get querystring variables
        m_TType = Request.QueryString("TType") 'Return or Store to Store
        m_UPSServiceCode = Request.QueryString("SER")
        m_UPSPackageType = Request.QueryString("PAK")
        m_strApprovedBy = Request.QueryString("Approval")
        m_strBoxId = Request.QueryString("BID")
        m_strLBS = Request.QueryString("LBS")
        m_strRANum = Request.QueryString("RA")

        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        'Get employee info
        m_objUserInfo.GetEmployeeInfo()

        'If store number not set then redirect to set store page
        If m_objUserInfo.StoreNumber = -1 Or m_objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

        'If employee is logged on to the system then their locks are kept alive
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
            m_objTransferLock.KeepLocksAlive(m_objUserInfo.EmpId)
        Else
            Response.Redirect("SessionEnd.aspx?Mode=1")
        End If

        'Set header properties
        ucHeader.CurrentMode(Header.HeaderGroup.Transfer)
        ucHeader.CurrentPage(Header.PageName.NewTransfer)
        ucHeader.DisableButtons()

        'Redirect after 1 second
        Response.AddHeader("refresh", "1;URL=PrintUPS.aspx?LBS=" & m_strLBS & "&TType=" & m_TType & "&BID=" & m_strBoxId & "&PAK=" & m_UPSPackageType & "&SER=" & m_UPSServiceCode & "&Approval=" & Server.UrlEncode(m_strApprovedBy) & "&RA=" & m_strRANum)

    End Sub

End Class
