Public Class EmployeeLogOff
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Private objUserInfo As New UserInfo()

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ucHeader.lblTitle = "Session End"
        ucHeader.CurrentMode(Request("Mode"))
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        lblError.Text = "Please log off before you exit the program."

    End Sub

End Class
