Public Class SentFrom
    Inherits System.Web.UI.Page
    Protected WithEvents tgSentFrom As Transfers
    Protected WithEvents layoutHeader As Header
    Protected WithEvents ucSearchBox As FilterBox

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim objStoreInfo As New Transfer.StoreInfo(ConfigurationSettings.AppSettings("strSQLConnection"))

        tgSentFrom.SetGridType = Transfers.TransferGridType.SentFrom
        tgSentFrom.GenerateReport()

        layoutHeader.lblTitle.Text = "Sent From 0003 Irvine"


    End Sub

End Class
