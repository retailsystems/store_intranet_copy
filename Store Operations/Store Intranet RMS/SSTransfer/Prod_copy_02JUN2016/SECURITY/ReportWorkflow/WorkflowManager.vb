' Static Model
Imports HotTopic.RD.Entity
Imports HotTopic.RD.Services
Imports HotTopic.RD.ReportFiles
Imports System.Data.SqlClient

Namespace HotTopic.RD.ReportWorkflow



    Public Class WorkflowManager

        Public Shared Sub Approved(ByVal rptSubmission As Submission, ByVal userId As String)
            Try
                'first, move files from Staging to LocationPath
                'TODO: COPY for now; change to MOVE when ready

                Dim locPath As String = rptSubmission.LocationPath
                Dim stagePath As String
                stagePath = rptSubmission.StagingPath
                If Not locPath.EndsWith("\") Then
                    locPath += "\"
                End If
                If Not stagePath.EndsWith("\") Then
                    stagePath += "\"
                End If
                'MOD 7/16/03 by sri -- stagepath FiscalWeekEndingDate subfolder path added  for upload type.
                'If rptSubmission.InputType = FileInputType.Upload Then
                '    stagePath += rptSubmission.FiscalWeekEndingDate.ToString("MMddyyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo) + "\"
                'End If
                'JDS 7/22/2003: for uploads, remove fw from stagepath; use submissionid as filename instead
                Dim searchPattern As String
                If rptSubmission.InputType = FileInputType.Upload Then
                    searchPattern = rptSubmission.ReportSubmissionId & "_*.*"
                ElseIf rptSubmission.InputType = FileInputType.MDReport Then
                    searchPattern = rptSubmission.DefaultFileName & "*" & "_" & rptSubmission.ReportSubmissionId & "*.*"
                Else
                    searchPattern = rptSubmission.DefaultFileName & "*" '+ "." + rptSubmission.FileFormat
                End If

                locPath += rptSubmission.FiscalYear.ToString() _
                    + "\" + rptSubmission.FiscalWeek.ToString() _
                    + "\" + rptSubmission.ReportId.ToString()
                Try
                    'SRI 7/22/03 Sub Company Owner Fix copy only Sub Company Owner matched files
                    If rptSubmission.InputType = FileInputType.System And rptSubmission.CompanySubOwners.Count > 0 Then
                        Dim cSubOwner As SubCompanyOwner
                        Dim ownerID As String
                        For Each cSubOwner In rptSubmission.CompanySubOwners
                            ownerID = cSubOwner.OwnerID
                            If rptSubmission.ContentScope = ContentScopeType.Store Then
                                ownerID = ownerID.Trim.PadLeft(4, "0")
                            End If
                            searchPattern = rptSubmission.DefaultFileName & "*" & ownerID & "*"
                            FileSystemManager.CopyFiles(stagePath, locPath, searchPattern)
                        Next
                        'reset the search pattern
                        searchPattern = rptSubmission.DefaultFileName & "*"
                    Else
                        FileSystemManager.CopyFiles(stagePath, locPath, searchPattern)
                    End If

                Catch ex As Exception
                    'cleanup any files already copied
                    FileSystemManager.DeleteFiles(locPath)
                    Throw ex
                End Try

                'then , save each file record in the ReportFile table
                Dim fileName As String
                Dim fileNames As ArrayList = FileSystemManager.ListFileNames(locPath, searchPattern)
                For Each fileName In fileNames

                    'init File...
                    Dim rptFile As File = New File(rptSubmission)
                    Dim ownerId As String
                    '**************************************
                    'MOD 7/21/03 by sri -- Input type UPLOAD get companyid from ReportCompanyOwner
                    'Assumption --> Input type UPLOAD are having scope Company.
                    '**************************************
                    If rptSubmission.InputType = FileInputType.Upload Then
                        'check for SubCompanyOwners
                        If rptSubmission.CompanySubOwners.Count > 0 Then
                            Dim cSubOwner As SubCompanyOwner

                            For Each cSubOwner In rptSubmission.CompanySubOwners
                                rptFile = New File(rptSubmission)
                                With rptFile
                                    .FileName = fileName
                                    .ArchivedFlag = False
                                    .ReportSubmissionId = rptSubmission.ReportSubmissionId
                                    .OwnerId = cSubOwner.OwnerID
                                    'save ReportFile
                                    FileManager.Save(rptFile, userId)
                                End With
                            Next
                        Else
                            Dim cOwner As CompanyOwner
                            For Each cOwner In rptSubmission.CompanyOwners
                                ownerId = cOwner.CompanyId
                                rptFile = New File(rptSubmission)
                                With rptFile
                                    .FileName = fileName
                                    .ArchivedFlag = False
                                    .ReportSubmissionId = rptSubmission.ReportSubmissionId
                                    .OwnerId = ownerId
                                    'save ReportFile
                                    FileManager.Save(rptFile, userId)
                                End With
                            Next
                        End If


                    Else
                        If fileName.ToLower.StartsWith(rptFile.DefaultFileName.ToLower) Then
                            'MOD 7/16/03 by sri -- stripping non numeric characters from file to get owenerid
                            Dim regExFileStr As String

                            regExFileStr = fileName.Substring(rptFile.DefaultFileName.Length, fileName.IndexOf(".") - rptFile.DefaultFileName.Length)
                            ' MOD 12/22/2003 FOR MD Reports handle submission id in file name 
                            If rptSubmission.InputType = FileInputType.MDReport Then
                                regExFileStr = regExFileStr.Replace("_" & rptSubmission.ReportSubmissionId, "")
                            End If
                            Dim DigitRegEx As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex("(?<number>\d+)")
                            Dim Mc As System.Text.RegularExpressions.MatchCollection = DigitRegEx.Matches(regExFileStr)
                            If Mc.Count > 0 Then
                                ownerId = Mc(0).Value
                            End If
                            'ownerId = System.Text.RegularExpressions.Regex.Replace(ownerId, "[^0-9]", "")
                            ownerId = Trim(ownerId)
                            '*******************************************************
                            If IsNumeric(ownerId) Then
                                With rptFile
                                    .FileName = fileName
                                    .ArchivedFlag = False
                                    .ReportSubmissionId = rptSubmission.ReportSubmissionId
                                    If .ContentScope = ContentScopeType.Store Or .ContentScope = ContentScopeType.Company Then
                                        '.OwnerId = Convert.ToInt16(fileName.Substring(rptFile.DefaultFileName.Length, fileName.IndexOf(".") - rptFile.DefaultFileName.Length))
                                        .OwnerId = Convert.ToInt16(ownerId)
                                    Else
                                        '.OwnerId = fileName.Substring(rptFile.DefaultFileName.Length, fileName.IndexOf(".") - rptFile.DefaultFileName.Length).Trim()
                                        .OwnerId = ownerId
                                    End If
                                End With
                                FileManager.Save(rptFile, userId)
                                ''verify filename
                                'If fileName.ToLower.StartsWith(rptFile.DefaultFileName.ToLower) Then
                                '    'save ReportFile
                                '    

                                'Else
                                '    'JDS 6/30/2003: just skip the file
                                '    'Throw New Exception("Invalid file name found in staging folder.")
                                'End If
                            End If
                        End If
                    End If

                Next

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub ApproveReport(ByVal rptApproval As Approval, ByVal userId As String, ByVal UserRoleID As Integer)
            Try
                With rptApproval
                    Dim arParms() As SqlParameter = New SqlParameter(5) {}

                    arParms(0) = New SqlParameter("@ReportSubmissionID", SqlDbType.Int)
                    arParms(0).Value = .ReportSubmissionId
                    arParms(1) = New SqlParameter("@EmployeeId", SqlDbType.Int)
                    arParms(1).Value = .ReportApprover.EmployeeId
                    arParms(2) = New SqlParameter("@status", SqlDbType.Int)
                    arParms(2).Value = .Status
                    arParms(3) = New SqlParameter("@userId", SqlDbType.VarChar, 30)
                    arParms(3).Value = userId
                    arParms(4) = New SqlParameter("@userRoleId", SqlDbType.VarChar, 30)
                    arParms(4).Value = UserRoleID
                    arParms(5) = New SqlParameter("@Comment", SqlDbType.NText)
                    arParms(5).Value = .Comment

                    SQLDataManager.GetInstance().Execute("dbo.RSP_UpdateSubmissionApproval", arParms)
                End With

            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        Public Shared Sub Notify(ByVal rptSubmission As Submission, ByVal SmtpServer As String, ByVal SmtpSender As String, ByVal appUrl As String, ByVal mailBody As String, Optional ByVal fromEmployeeId As Int32 = 0)
            Try
                Dim emailMgr As EmailManager = New EmailManager(SmtpServer)
                Dim mailSubject As String
                If rptSubmission.Status = ReportStatus.Cancelled Then
                    mailSubject = "Report Distro - Report Cancellation Notification"
                Else
                    mailSubject = "Report Distro - New Report Submission Notification"
                End If

                With rptSubmission
                    mailBody = mailBody.Replace("%SubmissionID%", .ReportSubmissionId)
                    mailBody = mailBody.Replace("%Title%", .Title)
                    mailBody = mailBody.Replace("%FiscalDate%", .FiscalWeekEndingDate)
                    mailBody = mailBody.Replace("%AppUrl%", appUrl)
                End With


                emailMgr.Format = Web.Mail.MailFormat.Html
                emailMgr.Sender = SmtpSender

                'recipients
                Dim apprs As ArrayList = rptSubmission.Approvers
                If apprs Is Nothing Then
                    Throw New Exception("No approvers.")
                Else
                    Dim appr As Approver
                    Dim routeOrder As Int32
                    For Each appr In apprs
                        If rptSubmission.ApprovalType = ReportApprovalType.One Then
                            emailMgr.AddRecipient(appr.EmailAddress)
                        Else
                            If fromEmployeeId = 0 Then
                                emailMgr.Recipient = appr.EmailAddress
                                Exit For
                            Else
                                'check who last approver and get next
                                If appr.EmployeeId = fromEmployeeId Then
                                    routeOrder = appr.RouteOrder + 1
                                End If
                                If appr.RouteOrder = routeOrder Then 'if next
                                    emailMgr.Recipient = appr.EmailAddress
                                    Exit For
                                End If
                            End If
                        End If
                    Next
                End If

                emailMgr.Subject = mailSubject
                'make sure to send only if there's a recipient
                If Not emailMgr.Recipient Is Nothing Then
                    If emailMgr.Recipient <> "" Then
                        emailMgr.Send(mailBody)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub NotifyAdmin(ByVal rptSubmission As Submission, ByVal SmtpServer As String, ByVal SmtpSender As String, ByVal appUrl As String, ByVal mailBody As String, ByVal AdminDt As DataTable)
            Try
                Dim emailMgr As EmailManager = New EmailManager(SmtpServer)
                Dim mailSubject As String
                mailSubject = "Report Distro - Report Declined Alert"
                With rptSubmission
                    mailBody = mailBody.Replace("%SubmissionID%", .ReportSubmissionId)
                    mailBody = mailBody.Replace("%Title%", .Title)
                    mailBody = mailBody.Replace("%FiscalDate%", .FiscalWeekEndingDate)
                    mailBody = mailBody.Replace("%AppUrl%", appUrl)
                End With


                emailMgr.Format = Web.Mail.MailFormat.Html
                emailMgr.Sender = SmtpSender

                'recipients
                Dim dr As DataRow
                Dim i As Int16
                For i = 0 To AdminDt.Rows.Count - 1
                    If i = 0 Then
                        emailMgr.Recipient = AdminDt.Rows(i).Item("EmailAddress")
                    Else
                        emailMgr.AddRecipient(AdminDt.Rows(i).Item("EmailAddress"))
                    End If
                Next

                emailMgr.Subject = mailSubject
                'make sure to send only if there's a recipient
                If Not emailMgr.Recipient Is Nothing Then
                    If emailMgr.Recipient <> "" Then
                        emailMgr.Send(mailBody)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ' Polymorphic: lists for submitter (non-approved); lists for approver (pending approval)
        ' Note: Need a user table specifying type of access privileges
        'Public Overridable Function List() As SubmissionDataSet

        'End Function
        'ToDo:implement data permission based on role
        Public Shared Function List(ByVal EmployeeID As String, ByVal UserID As String, ByVal UserRoleID As Integer) As DataSet
            'lists current submissions
            ' Set report parameters (2 input) 
            Dim arParms() As SqlParameter = New SqlParameter(2) {}
            arParms(0) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 30)
            arParms(0).Value = EmployeeID
            arParms(1) = New SqlParameter("@UserID", SqlDbType.VarChar, 30)
            arParms(1).Value = UserID
            arParms(2) = New SqlParameter("@UserRoleID", SqlDbType.Int)
            arParms(2).Value = UserRoleID
            Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_ListCurrentSubmissions", arParms)
            Return ds

        End Function
        Public Shared Function ListPendingApprovals(ByVal EmployeeId As Integer, ByVal UserRoleId As Integer) As DataSet
            'lists current submissions
            ' Set report parameters (1 input) 
            Dim arParms() As SqlParameter = New SqlParameter(1) {}
            arParms(0) = New SqlParameter("@employeeId", SqlDbType.VarChar, 30)
            arParms(0).Value = EmployeeId
            arParms(1) = New SqlParameter("@UserRoleID", SqlDbType.Int)
            arParms(1).Value = UserRoleId
            Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_GetApprovalPending", arParms)
            Return ds

        End Function
        Public Shared Function ListApproverNotes(ByVal ReportSubmissionID As Long) As DataSet
            Dim arParms() As SqlParameter = New SqlParameter(0) {}
            arParms(0) = New SqlParameter("@ReportSubmissionID", SqlDbType.Int)
            arParms(0).Value = ReportSubmissionID
            Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_GetApproverNotes", arParms)
            Return ds
        End Function
        Public Overridable Function ListApprovals(ByVal rptSubmissionID As Int32) As ApprovalDataset

        End Function

    End Class ' END CLASS DEFINITION WorkflowManager

End Namespace ' HotTopic.RD.ReportWorkflow

