if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sfn_GetControlsByParentId]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[sfn_GetControlsByParentId]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


CREATE Function dbo.sfn_GetControlsByParentId (@controlId smallint) 
RETURNS @ControlTable TABLE 
	(
	[ControlId] [smallint] NOT NULL ,
	[ControlName] [varchar] (50) NOT NULL ,
	[ControlType] [smallint] NOT NULL ,
	[ControlDesc] [varchar] (255) NULL ,
	[ParentControlId] [smallint] NULL ,
	[SortOrder] [smallint] NOT NULL
	)
AS
BEGIN
/******************************************************************************
**		File: sfn_GetControlsByParentId.sql
**		Name: sfn_GetControlsByParentId
**		Desc: This function returns child controls for given parent
**
**		This template can be customized:
**              
**		Return values: Table
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		
**	
**		Auth: Jeric Saez
**		Date: 7/1/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    
*******************************************************************************/
	
	
   DECLARE @RowsAdded int
   -- table variable to hold accumulated results
   DECLARE @controls TABLE 
	(
	[ControlId] [smallint] NOT NULL ,
	[ControlName] [varchar] (50) NOT NULL ,
	[ControlType] [smallint] NOT NULL ,
	[ControlDesc] [varchar] (255) NULL ,
	[ParentControlId] [smallint] NULL ,
	[SortOrder] [smallint] NOT NULL ,
    [processed] tinyint default 0
	)
	-- initialize @controls with direct child controls of the given parent control
	INSERT	@controls
	SELECT	ControlId
			,ControlName
			,ControlType
			,ControlDesc
			,ParentControlId
			,SortOrder
			,0
	FROM	Control
	WHERE	ParentControlId = @controlId

	SET @RowsAdded = @@rowcount
	
	-- While new controls were added in the previous iteration
	WHILE @RowsAdded > 0
	BEGIN
		/*Mark all control records whose direct child controls are going to be 
	found in this iteration with processed=1.*/
		UPDATE @controls
		SET processed = 1
		WHERE processed = 0
		-- Insert controls who report to controls marked 1.
		INSERT @controls
		SELECT	C.ControlId
				,C.ControlName
				,C.ControlType
				,C.ControlDesc
				,C.ParentControlId
				,C.SortOrder
				,0
		FROM	Control C, @controls r
		WHERE	C.ParentControlId=r.ControlId and r.processed = 1
		SET @RowsAdded = @@rowcount
		/*Mark all control records whose direct reports have been found
	in this iteration.*/
		UPDATE @controls
		SET processed = 2
		WHERE processed = 1
	END
	   
	-- copy to the result of the function the required columns
	INSERT @ControlTable
	SELECT	ControlId
			,ControlName
			,ControlType
			,ControlDesc
			,ParentControlId
			,SortOrder
	FROM @controls
	
	RETURN
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

