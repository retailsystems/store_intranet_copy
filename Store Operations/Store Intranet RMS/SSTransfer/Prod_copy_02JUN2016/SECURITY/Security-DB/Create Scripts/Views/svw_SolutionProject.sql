IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'svw_SolutionProject')
	BEGIN
		PRINT 'Dropping View svw_SolutionProject'
		DROP  View svw_SolutionProject
	END
GO

/******************************************************************************
**		File: svw_SolutionProject.sql
**		Name: svw_SolutionProject
**		Desc: 
**
**		This template can be customized:
**              
**
**		Auth: Jeric Saez
**		Date: 7/1/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

PRINT 'Creating View svw_SolutionProject'
GO
CREATE View svw_SolutionProject
as

	SELECT	SP.SolutionId
			,S.SolutionName
			,S.SolutionUrl
			,S.SolutionDesc
			,SP.ProjectId
			,P.ProjectName
			,P.ProjectUrl
			,P.ProjectDesc
			,P.DefaultProjectCSSClass
			,SP.SortOrder
	FROM	SolutionProject SP INNER JOIN
			Solution S ON SP.SolutionId  = S.SolutionId INNER JOIN
			Project P ON SP.ProjectId = P.ProjectId

GO


GRANT SELECT ON svw_SolutionProject TO AppUser

GO
