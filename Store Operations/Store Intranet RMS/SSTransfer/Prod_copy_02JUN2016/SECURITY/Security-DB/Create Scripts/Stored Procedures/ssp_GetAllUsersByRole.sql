IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'ssp_GetAllUsersByRole')
	BEGIN
		PRINT 'Dropping Procedure ssp_GetAllUsersByRole'
		DROP  Procedure  ssp_GetAllUsersByRole
	END

GO

PRINT 'Creating Procedure ssp_GetAllUsersByRole'
GO
CREATE Procedure ssp_GetAllUsersByRole
	@UserRoleID int
	,@ProjectID int
AS

/******************************************************************************
**		File: 
**		Name: ssp_GetAllUsersByRole
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:  SecurityManager.vb 
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 8/14/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT     EmployeeID, ltrim(rtrim(FirstName)) +' ' +ltrim(rtrim(LastName)) as FullName, EmailAddress
FROM         UserProfile usr 
JOIN UserProfileRole upf ON usr.UserID = upf.UserID AND ProjectID = @ProjectID AND UserRoleID = @UserRoleID




GO

GRANT EXEC ON ssp_GetAllUsersByRole TO AppUser

GO
