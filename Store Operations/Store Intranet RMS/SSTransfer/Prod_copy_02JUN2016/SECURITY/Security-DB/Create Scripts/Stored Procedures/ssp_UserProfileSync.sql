IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'ssp_UserProfileSync')
	BEGIN
		PRINT 'Dropping Procedure ssp_UserProfileSync'
		DROP  Procedure  ssp_UserProfileSync
	END

GO

PRINT 'Creating Procedure ssp_UserProfileSync'
GO
CREATE Procedure ssp_UserProfileSync
	/* Param List */
AS

/******************************************************************************
**		File: ssp_UserProfileSync.sql
**		Name: ssp_UserProfileSync
**		Desc: This procdeure sync UserProofile table with taUltimateEmployee Table
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 9/9/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
DECLARE @taEmployeeID varchar(10), @usrEmployeeID varchar(10),@taRegion_CD varchar(3), @usrRegion_CD varchar(3)
DECLARE @taDistrictID varchar(3), @usrDistrictID varchar(3)
DECLARE @taStoreNum int, @usrStoreNum int
DECLARE @taJobCode varchar(20), @usrJobCode varchar(20)
PRINT 'START Sync Regional Mangers'
DECLARE RmCur CURSOR FOR 
	SELECT DISTINCT 
		ta.EmployeeID
		,Region_CD
		,JobCode
		 from hottopic2.dbo.taUltimateEmployee ta
		INNER JOIN hottopic2_Dev.dbo.RegOfficers rm ON ta.EmployeeID = rm.EmpId	
OPEN RmCur

FETCH NEXT FROM RmCur INTO @taEmployeeID,@taRegion_CD,@taJobCode
WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @usrEmployeeID = null
		SELECT @usrEmployeeID = EmployeeID, @usrRegion_CD = RegionID, @usrJobCode= JobCode
			 FROM UserProfile WHERE EmployeeID = @taEmployeeID
		--PRINT @usrEmployeeID + ' ' + @usrRegion_CD + ' ' + @taRegion_CD
		IF (@usrEmployeeID IS NOT NULL )
			BEGIN
				IF (@usrRegion_CD <> @taRegion_CD OR @taJobCode <> @usrJobCode)
				BEGIN
					PRINT '	Updating EmployeeID :' + @taEmployeeID   
					UPDATE UserProfile SET 
						RegionID = @taRegion_CD
						,JobCode = @taJobCode
						WHERE EmployeeID = @taEmployeeID	
				END				
			END
		ELSE
			BEGIN
				PRINT '	Adding New EmployeeID :' + @taEmployeeID
				insert into UserProfile 
				(
				EmployeeID
				,[Password]
				,LastName
				,FirstName
				,JobCode
				,StoreNum
				,DistrictID
				,RegionID
				,CompanyID
				,ActiveFlag
				,NonIndividualFlag
				,EmailAddress
				,InsertBy
				,InsertDate
				)	

				select 
				ta.EmployeeID
				,'hottopic'
				,ltrim(rtrim(ta.NameLast))
				,ltrim(rtrim(ta.NameFirst))
				,ta.JobCode
				,null
				,null
				,@taRegion_CD
				,null
				,1
				,0
				,Substring(ta.NameFirst,1,1)+ltrim(rtrim(ta.NameLast))+'@hottopic.com'
				,'system'
				,getdate()
				from hottopic2_Dev.dbo.taUltimateEmployee ta				
				WHERE ta.EmployeeID = @taEmployeeID
				
			END
		FETCH NEXT FROM RmCur INTO @taEmployeeID,@taRegion_CD,@taJobCode
	END
CLOSE RmCur
DEALLOCATE RmCur
PRINT 'END Sync Regional Mangers'
--*******************************************************************
PRINT 'START Sync Divisional Mangers'
--*******************************************************************
DECLARE DmCur CURSOR FOR 
	SELECT DISTINCT 
		ta.EmployeeID
		,sd.RegionID
		,JobCode
		,dm.District_cd		
		 from hottopic2.dbo.taUltimateEmployee ta
		INNER JOIN hottopic2_Dev.dbo.DMOfficers dm ON ta.EmployeeID = dm.EmpId
		INNER JOIN ReportDistro_Dev.dbo.StoreDistrict sd ON sd.DistrictID = dm.District_cd AND EffectivityEndDate is NULL	
OPEN DmCur

FETCH NEXT FROM DmCur INTO @taEmployeeID,@taRegion_CD,@taJobCode,@taDistrictID
WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @usrEmployeeID = null
		SELECT @usrEmployeeID = EmployeeID, @usrRegion_CD = RegionID, @usrDistrictID = DistrictID,@usrJobCode= JobCode
			 FROM UserProfile WHERE EmployeeID = @taEmployeeID
		--PRINT @usrEmployeeID + ' ' + @usrRegion_CD + ' ' + @taRegion_CD
		IF (@usrEmployeeID IS NOT NULL )
			BEGIN
				IF (@usrRegion_CD <> @taRegion_CD OR @taJobCode <> @usrJobCode OR @taDistrictID <> @usrDistrictID )
				BEGIN
					PRINT '	Updating EmployeeID :' + @taEmployeeID
					UPDATE UserProfile SET 
						RegionID = @taRegion_CD
						,JobCode = @taJobCode
						,DistrictID = @taDistrictID
						WHERE EmployeeID = @taEmployeeID	
				END				
			END
		ELSE
			BEGIN
				PRINT '	Adding New EmployeeID :' + @taEmployeeID
				insert into UserProfile 
				(
				EmployeeID
				,[Password]
				,LastName
				,FirstName
				,JobCode
				,StoreNum
				,DistrictID
				,RegionID
				,CompanyID
				,ActiveFlag
				,NonIndividualFlag
				,EmailAddress
				,InsertBy
				,InsertDate
				)	

				select 
				ta.EmployeeID
				,'hottopic'
				,ltrim(rtrim(ta.NameLast))
				,ltrim(rtrim(ta.NameFirst))
				,ta.JobCode
				,null
				,@taDistrictID
				,@taRegion_CD
				,null
				,1
				,0
				,Substring(ta.NameFirst,1,1)+ltrim(rtrim(ta.NameLast))+'@hottopic.com'
				,'system'
				,getdate()
				from hottopic2_Dev.dbo.taUltimateEmployee ta				
				WHERE ta.EmployeeID = @taEmployeeID
				
			END
		FETCH NEXT FROM DmCur INTO @taEmployeeID,@taRegion_CD,@taJobCode,@taDistrictID
	END
CLOSE DmCur
DEALLOCATE DmCur
--*******************************************************************
PRINT 'END Sync Divisional Mangers'
--*******************************************************************

--*******************************************************************
PRINT 'START Sync Store Users'
--*******************************************************************
DECLARE StoreCur CURSOR FOR 
	SELECT DISTINCT 
		ta.EmployeeID
		,sd.RegionID
		,JobCode
		,sd.DistrictID
		,sd.StoreNum			
		FROM hottopic2_Dev.dbo.taUltimateEmployee ta
		INNER JOIN ReportDistro_Dev.dbo.StoreDistrict sd ON sd.StoreNum = ta.Location AND EffectivityEndDate is NULL	
		WHERE jobcode IN 
			(Select JobCode From TempJobCodevsRole TJR JOIN UserRole UR
				ON TJR.UserRoleID = UR.UserRoleID AND USerRoleName = 'Store')
				AND ta.EmployeeID not in(Select EmployeeID FROM UserProfile ) 
OPEN StoreCur

FETCH NEXT FROM StoreCur INTO @taEmployeeID, @taRegion_CD, @taJobCode, @taDistrictID, @taStoreNum
WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @usrEmployeeID = null
		SELECT @usrEmployeeID = EmployeeID, @usrRegion_CD = RegionID, @usrDistrictID = DistrictID
			 ,@usrJobCode= JobCode, @usrStoreNum = StoreNum
			 FROM UserProfile WHERE EmployeeID = @taEmployeeID
		--PRINT @usrEmployeeID + ' ' + @usrRegion_CD + ' ' + @taRegion_CD
		IF (@usrEmployeeID IS NOT NULL )
			BEGIN
				IF (@usrRegion_CD <> @taRegion_CD OR @taJobCode <> @usrJobCode OR @taDistrictID <> @usrDistrictID OR @usrStoreNum <> @taStoreNum)
				BEGIN
					PRINT '	Updating EmployeeID :' + @taEmployeeID
					UPDATE UserProfile SET 
						RegionID = @taRegion_CD
						,JobCode = @taJobCode
						,DistrictID = @taDistrictID
						,StoreNum = @taStoreNum
						WHERE EmployeeID = @taEmployeeID	
				END				
			END
		ELSE
			BEGIN
				PRINT '	Adding New EmployeeID :' + @taEmployeeID
				insert into UserProfile 
				(
				EmployeeID
				,[Password]
				,LastName
				,FirstName
				,JobCode
				,StoreNum
				,DistrictID
				,RegionID
				,CompanyID
				,ActiveFlag
				,NonIndividualFlag
				,EmailAddress
				,InsertBy
				,InsertDate
				)	

				select 
				ta.EmployeeID
				,'hottopic'
				,ltrim(rtrim(ta.NameLast))
				,ltrim(rtrim(ta.NameFirst))
				,ta.JobCode
				,@taStoreNum
				,@taDistrictID
				,@taRegion_CD
				,null
				,1
				,0
				,Substring(ta.NameFirst,1,1)+ltrim(rtrim(ta.NameLast))+'@hottopic.com'
				,'system'
				,getdate()
				from hottopic2_Dev.dbo.taUltimateEmployee ta				
				WHERE ta.EmployeeID = @taEmployeeID
				
			END
		FETCH NEXT FROM StoreCur INTO @taEmployeeID, @taRegion_CD, @taJobCode, @taDistrictID, @taStoreNum
	END
CLOSE StoreCur
DEALLOCATE StoreCur
--*******************************************************************
PRINT 'END Sync Store Users'
--*******************************************************************

--*******************************************************************
PRINT 'Copying New HQ Users'
--*******************************************************************
insert into UserProfile 
	(
	EmployeeID
	,[Password]
	,LastName
	,FirstName
	,JobCode
	,StoreNum
	,DistrictID
	,RegionID
	,CompanyID
	,ActiveFlag
	,NonIndividualFlag
	,EmailAddress
	,InsertBy
	,InsertDate
	)	

	select distinct
	ta.EmployeeID
	,'hottopic'
	,ltrim(rtrim(ta.NameLast))
	,ltrim(rtrim(ta.NameFirst))
	,ta.JobCode
	,ta.location
	,0
	,0
	,1
	,1
	,0
	,Substring(ta.NameFirst,1,1)+ltrim(rtrim(ta.NameLast))+'@hottopic.com'
	,'system'
	,getdate()
	 from hottopic2_Dev.dbo.taUltimateEmployee ta	
	 WHERE location in(8000,8001,4490,9000) 
	 AND EmployeeID not in(select EmployeeID FROM UserProfile)
	 

--SET CompanyID 1-HT , 2 -Torrid

UPDATE userprofile set companyid = 2 WHERE companyid IS Null AND regionId between 20 and 39
UPDATE userprofile set companyid = 1 WHERE companyid IS Null AND  regionId < 20
UPDATE userprofile set password = 'torrid' where password = 'hottopic' AND companyid = 2

--UPDATE UserProfileRole TABLE For ReportDistro Access

INSERT INTO userProfileRole SELECT userid, 2, UserRoleID,null 
	from UserProfile UP JOIN TempJobCodevsRole TJR ON UP.jobcode = TJR.JobCode
	AND TJR.JobCode Not in (Select JobCode From TempJobCodevsRole TJR JOIN UserRole UR
			ON TJR.UserRoleID = UR.UserRoleID AND USerRoleName = 'IT')
	WHERE UserID not in(SELECT userid FROM userProfileRole WHERE ProjectID = 2)


GO

GRANT EXEC ON ssp_UserProfileSync TO AppUser

GO
