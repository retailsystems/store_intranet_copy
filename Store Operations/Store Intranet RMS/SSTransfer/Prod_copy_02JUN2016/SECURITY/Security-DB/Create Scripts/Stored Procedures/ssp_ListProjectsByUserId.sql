IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'ssp_ListControlsByModuleId')
	BEGIN
		PRINT 'Dropping Procedure ssp_ListControlsByModuleId'
		DROP  Procedure  ssp_ListControlsByModuleId
	END

GO

PRINT 'Creating Procedure ssp_ListControlsByModuleId'
GO
CREATE Procedure ssp_ListControlsByModuleId
	@moduleId	smallint
AS

/******************************************************************************
**		File: ssp_ListControlsByModuleId.sql
**		Name: ssp_ListControlsByModuleId
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Jeric Saez
**		Date: 7/1/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    7/3/2003		Sri Bajjuri			Inner Join to table "Permission" to prevent cross join
**    7/3/2003		Jeric Saez			Add inherited controls
*******************************************************************************/

	SET NOCOUNT ON
	
	/* get all "natural" controls */
	SELECT	ModuleId
			,ModuleName
			,ModuleUrl
			,ModuleDesc
			,ModuleControlOrder
			,ControlId
			,ControlName
			,PM.ControlUrl
			,ControlType
			,ControlDesc
			,ParentControlId
			,ControlOrder			
			,PR.UserRoleId
			,InheritedModuleId
	INTO	#tmpControls
	FROM	svw_ProjectModuleControl PM INNER JOIN 
			ProjectRole PR ON PM.ProjectId = PR.ProjectId INNER JOIN
			Permission P ON (P.UserRoleID = PR.UserRoleID AND PM.ModuleControlID = P.ModuleControlID)
	WHERE	PM.ModuleId =@moduleId
	
	
	/* get all "inherited" controls */
	INSERT	#tmpControls
	SELECT	T.ModuleId
			,T.ModuleName
			,T.ModuleUrl
			,T.ModuleDesc
			,PM.ModuleControlOrder
			,PM.ControlId
			,PM.ControlName
			,PM.ControlUrl
			,PM.ControlType
			,PM.ControlDesc
			,PM.ParentControlId
			,PM.ControlOrder			
			,PR.UserRoleId
			,T.InheritedModuleId
	FROM	#tmpControls T INNER JOIN
			svw_ProjectModuleControl PM ON (T.InheritedModuleId = PM.ModuleId AND PM.InheritableFlag = 1) INNER JOIN 
			ProjectRole PR ON PM.ProjectId = PR.ProjectId INNER JOIN
			Permission P ON (P.UserRoleID = PR.UserRoleID AND PM.ModuleControlID = P.ModuleControlID)
	
	SELECT	DISTINCT *
	FROM	#tmpControls
	ORDER BY UserRoleId, InheritedModuleId, ModuleControlOrder


GO

GRANT EXEC ON ssp_ListControlsByModuleId TO AppUser

GO
 