IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'ssp_LoginUser')
	BEGIN
		PRINT 'Dropping Procedure ssp_LoginUser'
		DROP  Procedure  ssp_LoginUser
	END

GO

PRINT 'Creating Procedure ssp_LoginUser'
GO
CREATE Procedure ssp_LoginUser
	@employeeId		varchar(10)
	,@password		varchar(15)
	,@retValue int OUTPUT
AS

/******************************************************************************
**		File: ssp_LoginUser.sql
**		Name: ssp_LoginUser
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Jeric Saez
**		Date: 7/1/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

	SET NOCOUNT ON
	
	DECLARE	@rows int
	IF EXISTS(SELECT EmployeeId FROM UserProfile WHERE	EmployeeId = @employeeId
								AND		Password = @password
								AND		ActiveFlag = 1)
	
	SELECT	UserId
			,EmployeeId
			,Password
			,LastName
			,MiddleName
			,FirstName
			,JobCode
			,StoreNum
			,DistrictId
			,RegionId
			,NonIndividualFlag
			,InsertBy
			,InsertDate
			,UpdateBy
			,UpdateDate
	FROM	UserProfile 
	WHERE	EmployeeId = @employeeId
	AND		Password = @password
	AND		ActiveFlag = 1
	
	SELECT	@rows = @@ROWCOUNT
	
	IF (@rows > 0)
	BEGIN
		--RETURN 0 --login success
		set @retValue = 0
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT * FROM UserProfile WHERE EmployeeId = @employeeId)
			--RETURN -1 --wrong password
			set @retValue = -1
		ELSE
			--RETURN -2 --wrong employeeid
			set @retValue = -2
	END
			

GO

GRANT EXEC ON ssp_LoginUser TO AppUser

GO
