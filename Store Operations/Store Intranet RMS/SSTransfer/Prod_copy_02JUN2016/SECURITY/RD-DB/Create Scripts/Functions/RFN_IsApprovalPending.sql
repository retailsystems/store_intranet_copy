if exists (select * from dbo.sysobjects where id = object_id(N'RFN_IsApprovalPending') and xtype in (N'FN', N'IF', N'TF'))
drop function RFN_IsApprovalPending
GO


CREATE FUNCTION RFN_IsApprovalPending
	(
	@ReportSubmissionID int, @ReportID int, @EmployeeID int, @allApproveFlag int
	)
RETURNS bit
AS
/******************************************************************************
**		File: RFN_IsApprovalPending.sql
**		Name: RFN_IsApprovalPending
**		Desc:  This function returns file approval pending status 
**
**		This template can be customized:
**              
**		Return values: Table
** 
**		Called by:   
**              
**		Parameters:
**		Input									Output
**     ----------								-----------
**		@ReportSubmissionID,
**		@ReportID,
**		@EmployeeID
**	
**		Auth: Sri Bajjuri
**		Date: 6/13/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**     4/16/04		Sri Bajjuri			Change the approvaltype data type from bit to int.
*******************************************************************************/
	BEGIN
		declare @isPending bit, @ReportApproverID int, @RouteOrder int, @prevApproverID int
		set @isPending = 0
		-- get routeorder 
			select  @RouteOrder = RouteOrder
					from ReportApprover where ReportID = @ReportID and EmployeeId = @EmployeeID
					
		if @allApproveFlag = 3 
			-- all approvers approval required in the predefined route order
		BEGIN
			
			if not exists(select ReportSubmissionID from ReportSubmissionApproval where ReportSubmissionID= @ReportSubmissionID
								and  EmployeeId = @EmployeeID and status > 1)
				begin
				if @RouteOrder > 1 
					-- see if other approvers  approved the file.
					-- file status 1-pending , 2-approved , 3 -declined
					begin
						select @prevApproverID = EmployeeID from ReportApprover where ReportID = @ReportID
							and RouteOrder = (select max(RouteOrder) from ReportApprover where RouteOrder<@RouteOrder and  ReportID = @ReportID)
						
							if exists(select ReportSubmissionID from ReportSubmissionApproval where ReportSubmissionID= @ReportSubmissionID
											and EmployeeId = @prevApproverID and status = 2)
							select @isPending = 1							
					end
				else					
							select @isPending = 1
				end
			END
		ELSE
			if not exists(select ReportSubmissionID from ReportSubmissionApproval where ReportSubmissionID= @ReportSubmissionID
									and EmployeeId = @EmployeeID and status > 1)
							select @isPending = 1
	RETURN @isPending
	END
GO

GRANT EXEC ON RFN_IsApprovalPending TO RdsApp

GO
