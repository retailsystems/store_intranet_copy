if exists (select * from dbo.sysobjects where id = object_id(N'RFN_CdStoreCheck') and xtype in (N'FN', N'IF', N'TF'))
drop function RFN_CdStoreCheck
GO

CREATE FUNCTION RFN_CdStoreCheck
	(
	 @CdStoreFlag bit, @ReportCdStoreFlag bit
	)
RETURNS bit
AS
/******************************************************************************
**		File: RFN_CdStoreCheck.udf
**		Name: RFN_CdStoreCheck
**		Desc:  This function returns file approval pending status 
**
**		This template can be customized:
**              
**		Return values: Table
** 
**		Called by:   
**              
**		Parameters:
**		Input									Output
**     ----------								-----------
**		@ReportSubmissionID,
**		@ReportID,
**		@EmployeeID
**	
**		Auth: Sri Bajjuri
**		Date: 6/13/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    
*******************************************************************************/
	BEGIN
		declare @cdCheckOk bit
		set @cdCheckOk = 0
		if  @ReportCdStoreFlag = 1 
			if @CdStoreFlag = 1 
				set @cdCheckOk = 1
			else
				set @cdCheckOk = 0
		else
			set @cdCheckOk = 1
			
		return @cdCheckOk
	END

GO

GRANT EXEC ON RFN_CdStoreCheck TO RdsApp

GO
