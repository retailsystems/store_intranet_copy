if exists (select * from dbo.sysobjects where id = object_id(N'RFN_GetUserScope') and xtype in (N'FN', N'IF', N'TF'))
drop function RFN_GetUserScope
GO

CREATE Function RFN_GetUserScope ( @employeeId varchar(30)) 
returns @UserTable table 
	(
	OwnerID varchar(5),
	ContentScopeType int,
	employeeId int
	)
AS
begin
/******************************************************************************
**		File: RFN_GetUserScope.sql
**		Name: RFN_GetUserScope
**		
**
**		This template can be customized:
**              
**		Return values: Table
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@array
**	
**		Auth: Sri Bajjuri
**		Date: 6/27/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    
*******************************************************************************/
	declare @regionId varchar(5), @districtId varchar(3), @store varchar(5)
	select @regionId=Region,@districtId=district, @store = isNull(store,'') from RV_UserMst where user_login = @employeeId
	if len(@regionId) > 0
		begin
		insert into @UserTable  values(@regionId, 2,@employeeId )		
		end
	 if len(@districtId) > 0
		begin
		insert into @UserTable  values(@districtId, 3,@employeeId )		
		end
	else
		insert into @UserTable  values(@store, 3,@employeeId )	
	return
end

GO

GRANT SELECT ON RFN_GetUserScope TO Public

GO
 