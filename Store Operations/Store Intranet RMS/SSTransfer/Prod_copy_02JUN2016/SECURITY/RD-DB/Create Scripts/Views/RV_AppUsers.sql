IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'RV_AppUsers')
	BEGIN
		PRINT 'Dropping View RV_AppUsers'
		DROP  View RV_AppUsers
	END
GO

/******************************************************************************
**		File: 
**		Name: RV_AppUsers
**		Desc: 
**
**		This template can be customized:
**              
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

PRINT 'Creating View RV_AppUsers'
GO
CREATE View RV_AppUsers
as

	SELECT     *
FROM       HTSecurity_DEV.dbo.UserProfile 


GO


GRANT SELECT ON RV_AppUsers TO RdsApp

GO
