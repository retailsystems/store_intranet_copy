IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'RV_FiscalCalender')
	BEGIN
		PRINT 'Dropping View RV_FiscalCalender'
		DROP  View RV_FiscalCalender
	END
GO

/******************************************************************************
**		File: 
**		Name: RV_FiscalCalender
**		Desc: 
**
**		This template can be customized:
**              
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

PRINT 'Creating View RV_FiscalCalender'
GO
CREATE View RV_FiscalCalender
as

SELECT YR, BEG_DT, END_DT, PER_NUM, WK_NUM
FROM     FiscalCalender


GO


GRANT SELECT ON RV_FiscalCalender TO RdsApp

GO
