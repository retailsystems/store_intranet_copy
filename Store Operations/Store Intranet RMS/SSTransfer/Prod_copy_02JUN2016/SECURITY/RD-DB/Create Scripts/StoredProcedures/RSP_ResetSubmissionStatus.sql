IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_ResetSubmissionStatus')
	BEGIN
		PRINT 'Dropping Procedure RSP_ResetSubmissionStatus'
		DROP  Procedure  RSP_ResetSubmissionStatus
	END

GO

PRINT 'Creating Procedure RSP_ResetSubmissionStatus'
GO
CREATE Procedure RSP_ResetSubmissionStatus
	@ReportSubmissionID int
	,@status int
AS

/******************************************************************************
**		File: RSP_ResetSubmissionStatus.sql
**		Name: RSP_ResetSubmissionStatus
**		Desc: This procedure updates the status of reportSubmission.
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   RSP_UpdateSubmissionApproval
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportSubmissionID
**		Auth: Sri Bajjuri
**		Date: 7/7/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    7/14/2003		Sri					Calling 'RSP_ArchiveNthSubmission' if ExpirationType = 2 i.e Nth Report.
*******************************************************************************/
declare @approvalType int,  @cnt int, @approverCnt int, @reportId int
declare @ExpirationType int, @ArchiveNth int, @submissionStatus int

-- approvalType 1-None, 2- Any One 3- all
-- Approval Status 1-Pending, 2-Approved, 3-Declined , 4-Archived
	SELECT @approvalType=approvalType, @reportId=r.ReportID, @ExpirationType=ExpirationType ,@ArchiveNth = ArchiveNth FROM Report r 
		INNER JOIN ReportSubmission rs ON r.ReportId  = rs.ReportID and rs.ReportSubmissionID=@ReportSubmissionID
	if @approvalType = 2
		BEGIN
		IF EXISTS(SELECT ReportSubmissionID From ReportSubmissionApproval WHERE ReportSubmissionID=@ReportSubmissionID and Status = @status) 
			UPDATE ReportSubmission set Status = @status where ReportSubmissionID=@ReportSubmissionID
		
		END
	if @approvalType = 3
		BEGIN
			IF @status = 2 
			BEGIN
					-- check if all report Approvers approved the submission
					SELECT @approverCnt = count(EmployeeID) FROM ReportApprover WHERE ReportId = @reportId
					SELECT @cnt = count(EmployeeID) FROM ReportSubmissionApproval WHERE ReportSubmissionID = @ReportSubmissionID AND Status =@status
					IF @cnt=@approverCnt
						UPDATE ReportSubmission set Status = @status where ReportSubmissionID=@ReportSubmissionID
			END
			ELSE
			
				IF EXISTS(SELECT ReportSubmissionID From ReportSubmissionApproval WHERE ReportSubmissionID=@ReportSubmissionID and Status = @status) 
					UPDATE ReportSubmission set Status = @status where ReportSubmissionID=@ReportSubmissionID
				
			
		END
	
		if @@ERROR <> 0
			BEGIN							
				RAISERROR('RSP_ResetSubmissionStatus: Error updating ReportSubmission Status', 16, 1)
				RETURN -1
			END
		IF @status = 2
		BEGIN
			SELECT @submissionStatus=status  FROM ReportSubmission WHERE ReportSubmissionID=@ReportSubmissionID
			IF @submissionStatus =2 AND @ExpirationType = 2 AND @ArchiveNth > 0
				BEGIN
				--Print 'calling RSP_ArchiveNthSubmission'
					EXEC RSP_ArchiveNthSubmission @reportId,@ArchiveNth
					if @@ERROR <> 0
					BEGIN							
						RAISERROR('RSP_ResetSubmissionStatus: Error Archiving ReportSubmission ', 16, 1)
						RETURN -1
					END
				END
		END
		RETURN 0
GO

GRANT EXEC ON RSP_ResetSubmissionStatus TO RdsApp

GO
