IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetMdReportTypeData')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetMdReportTypeData'
		DROP  Procedure  RSP_GetMdReportTypeData
	END

GO

PRINT 'Creating Procedure RSP_GetMdReportTypeData'
GO
CREATE Procedure RSP_GetMdReportTypeData
	/* Param List */
AS

/******************************************************************************
**		File: RSP_GetMdReportTypeData.sql
**		Name: RSP_GetMdReportTypeData
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT MdReportTypeID, MdReportName, SplInstructions 
	FROM MDReportTypes ORDER BY MdReportTypeID


GO

GRANT EXEC ON RSP_GetMdReportTypeData TO RdsApp

GO
