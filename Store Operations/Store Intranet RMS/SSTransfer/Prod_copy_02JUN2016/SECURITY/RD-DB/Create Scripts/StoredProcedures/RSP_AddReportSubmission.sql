IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_AddReportSubmission')
	BEGIN
		PRINT 'Dropping Procedure RSP_AddReportSubmission'
		DROP  Procedure  RSP_AddReportSubmission
	END

GO

PRINT 'Creating Procedure RSP_AddReportSubmission'
GO
CREATE Procedure RSP_AddReportSubmission
	@ReportSubmissionID int OUTPUT,
	@ReportID int,
	@ReportDate datetime,
	@Status int,
	@UserId varchar(20),
	@StagingPath varchar(255),
	@LocationPath varchar(255),
	@EffectivityStartDate datetime,
	@Comment varchar(2000),
	@EffectivityEndDate datetime,
	@FiscalYear int = null,
	@FiscalWeek int = null,
	@MDReportType int = null,
	@MDEffectiveDate datetime =null,
	@MDPcNum varchar(50) = null
	
AS

/******************************************************************************
**		File: RSP_AddReportSubmission.sql
**		Name: RSP_AddReportSubmission
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------						-----------
**										ReportSubmissionID
**										errCode
**										errMsg
**		Auth: Sri bajjuri
**		Date: 6/13/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------		-------------------------------------------
**    7/25/2003     Sri Bajjuri		Calling RSP_ArchiveNthSubmission
*******************************************************************************/

DECLARE	@FWEDate	smalldatetime
declare @ExpirationType int, @ArchiveNth int
SELECT  @ExpirationType=ExpirationType ,@ArchiveNth = ArchiveNth FROM Report r WHERE ReportID = @ReportID
		
IF (@FiscalYear IS NULL)
BEGIN
	SELECT	@FiscalYear = YR
			,@FiscalWeek = WK_NUM
			,@FWEDate = END_DT
	FROM	RV_FiscalCalender
	WHERE	@ReportDate BETWEEN BEG_DT AND END_DT
END

IF EXISTS(SELECT * FROM ReportSubmission RS INNER JOIN Report R ON RS.ReportId = R.ReportId
			WHERE  RS.ReportID = @ReportID				
			AND	RS.FiscalYear = @FiscalYear
			AND	RS.FiscalWeek = @FiscalWeek
			AND	RS.Status != 5 /*not cancelled*/
			AND R.AllowMultiFlag = 0)
BEGIN
	RAISERROR ('Duplicate report for fiscal week.', 16, 3)
	RETURN -1
END

if @ReportSubmissionID > 0	
	--Update Existing Report
	BEGIN
	update ReportSubmission
		set 
				ReportID = @ReportID,				
				FiscalYear = @FiscalYear,
				FiscalWeek = @FiscalWeek,
				FiscalWeekEndingDate = @FWEDate,
				ReportDate = @ReportDate,
				StagingPath = @StagingPath,
				LocationPath = @LocationPath,				
				EffectivityStartDate = @EffectivityStartDate,
				EffectivityEndDate = @EffectivityEndDate,
				Comment = @Comment,
				Status = @Status,	
				MDReportType = @MDReportType,
				MDEffectiveDate = @MDEffectiveDate	,
				MDPcNum = @MDPcNum,
				UpdateBy =@UserId,				
				UpdateDate= getdate()
				where ReportSubmissionID = @ReportSubmissionID
		
					if @@error != 0 
					begin						
						RAISERROR ('Failed to Update Report Submission: rsp_AddReportSubmission.', 16, 3)
						RETURN -1
					end	
		
	END
else
	BEGIN
	
		INSERT INTO ReportSubmission 
				(				
				ReportID,
				FiscalYear,
				FiscalWeek,
				FiscalWeekEndingDate,
				ReportDate,
				StagingPath,
				LocationPath,				
				EffectivityStartDate,
				EffectivityEndDate,
				Comment,
				Status,	
				MDReportType,
				MDEffectiveDate,
				MDPcNum,			
				InsertBy,
				InsertDate
				)
		values(
				@ReportID,				
				@FiscalYear,
				@FiscalWeek ,
				@FWEDate ,
				@ReportDate ,
				@StagingPath,
				@LocationPath,				
				@EffectivityStartDate ,
				@EffectivityEndDate ,
				@Comment ,
				@Status ,	
				@MDReportType,
				@MDEffectiveDate,	
				@MDPcNum,		
				@UserId,
				getdate()
			)
		
		if @@error = 0 
			begin
				select @ReportSubmissionID = @@identity
			end
		else
			begin
				RAISERROR ('Failed to Add Report Submission: rsp_AddReportSubmission.', 16, 3)
				RETURN -1
			end
		
		
	END
		
			IF @Status =2 AND @ExpirationType = 2 AND @ArchiveNth > 0
				BEGIN
				--Print 'calling RSP_ArchiveNthSubmission'
					EXEC RSP_ArchiveNthSubmission @reportId,@ArchiveNth
					if @@ERROR <> 0
					BEGIN							
						RAISERROR('RSP_ResetSubmissionStatus: Error Archiving ReportSubmission ', 16, 1)
						RETURN -1
					END
			END
		

GO

GRANT EXEC ON RSP_AddReportSubmission TO RdsApp

GO
