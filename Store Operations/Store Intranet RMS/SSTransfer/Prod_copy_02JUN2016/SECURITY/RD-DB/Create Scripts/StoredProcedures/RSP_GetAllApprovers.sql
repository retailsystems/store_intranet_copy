IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetAllApprovers')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetAllApprovers'
		DROP  Procedure  RSP_GetAllApprovers
	END

GO

PRINT 'Creating Procedure RSP_GetAllApprovers'
GO
CREATE Procedure RSP_GetAllApprovers
	/* Param List */
AS

/******************************************************************************
**		File: RSP_GetAllApprovers.sql
**		Name: RSP_GetAllApprovers
**		Desc: This returns empID,name of all approvers.
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 6/17/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
SELECT     EmployeeID, ltrim(rtrim(NameFirst)) +' ' +ltrim(rtrim(NameLast)) as FullName
FROM         HotTopic2_Dev.dbo.taUltimateEmployee
WHERE     (JobCode LIKE 'DM')



GO

GRANT EXEC ON RSP_GetAllApprovers TO RdsApp

GO
