IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetApprovalPending')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetApprovalPending'
		DROP  Procedure  RSP_GetApprovalPending
	END

GO

PRINT 'Creating Procedure RSP_GetApprovalPending'
GO
CREATE Procedure RSP_GetApprovalPending
	@EmployeeID int
	,@UserRoleID int
	
AS

/******************************************************************************
**		File: RSP_GetApprovalPending.sql
**		Name: RSP_GetApprovalPending
**		Desc: returns all approval pending reports for logged in approver
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 6/13/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

-- get all pending reports which requires all approvers approval
-- TODO fix me userrole hard coding
if @UserRoleID = 1
	select Distinct ReportSubmissionID,rv.ReportID,ReportTitle,FiscalWeekEndingDate,
		ReportDate,LookUpText,Status,Comment from RV_CurrentSubmissions rv 
		INNER JOIN LookUp ON lookUpValue = status and LookUpType ='ReportStatus'		
		where Status =1 
else

select ReportSubmissionID,rv.ReportID,ReportTitle,FiscalWeekEndingDate,
		ReportDate,LookUpText,Status,Comment from RV_CurrentSubmissions rv 
		INNER JOIN LookUp ON lookUpValue = status and LookUpType ='ReportStatus'
		INNER JOIN ReportApprover ra ON rv.ReportID = ra.ReportID and ra.EmployeeID = @EmployeeID
		where Status =1 and dbo.RFN_isApprovalPending(rv.ReportSubmissionID,rv.ReportID,@EmployeeID,approvalType) = 1




GO

GRANT EXEC ON RSP_GetApprovalPending TO RdsApp

GO
