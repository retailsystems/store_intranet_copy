IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetUserInfo')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetUserInfo'
		DROP  Procedure  RSP_GetUserInfo
	END

GO

PRINT 'Creating Procedure RSP_GetUserInfo'
GO
CREATE Procedure RSP_GetUserInfo
	@EmployeeId varchar(30)
	/* Param List */
AS

/******************************************************************************
**		File: RSP_GetUserInfo.sql
**		Name: RSP_GetUserInfo
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 6/27/2003
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------		-------------------------------------------
**    
*******************************************************************************/
if exists (select User_Login from RV_UserMst where User_Login = @EmployeeId)
	select User_Login as UserID, Name_First as FirstName, Name_Last as LastName, isNull(Store,0) as StoreNum, Job_Class as UserRole,
			OwnerID, ContentScopeType
		from RV_UserMst JOIN dbo.RFN_GetUserScope(@EmployeeId) fn
		ON fn.EmployeeId = RV_UserMst.User_Login
		 where User_Login = @EmployeeId
else
	select EmployeeID as UserID, NameFirst as FirstName, NameLast as LastName, isNull(Location,0) as StoreNum, JobCode as UserRole,isNull(Location,0) as OwnerID,4 as ContentScopeType
		from RV_taUltimateEmployee where EmployeeID = @EmployeeId

GO

GRANT EXEC ON RSP_GetUserInfo TO RdsApp

GO
