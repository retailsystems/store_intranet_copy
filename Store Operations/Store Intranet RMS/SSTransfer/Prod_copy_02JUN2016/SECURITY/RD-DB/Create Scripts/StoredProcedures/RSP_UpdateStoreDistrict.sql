SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_UpdateStoreDistrict]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_UpdateStoreDistrict]
GO

CREATE PROCEDURE dbo.RSP_UpdateStoreDistrict
	@StoreNum int,	
	@RegionID varchar(5),
	@DistrictID varchar(5),
	@CompanyName varchar(30),
	@deleted bit,
	@CdStoreFlag bit

/******************************************************************************
**		File: RSP_UpdateStoreDistrict.sql
**		Name: RSP_UpdateStoreDistrict
**		Desc:  This storedprocedure  Add/Update StoreDistrict  information 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:  trigger STORE_TriggerReportStoreDistrict from HotTopic2_dev, Torrid database.
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportID
**	
**		Auth: Sri Bajjuri
**		Date: 6/11/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    8/5/2003		Sri Bajjuri			Update CDstoreFlag.
**		9/16/2003   Sri Bajjuri			Update (HTSecurity...dbo.UserProfile) store users with new regionID/DistrictID 
*******************************************************************************/
AS
	SET NOCOUNT ON
	declare @errCode int, @CompanyID int, @curRegionID int, @curDistrictID int
	
	select @errCode  = 0
	select @CompanyID = CompanyID from  Company where CompanyName =@CompanyName
	if @deleted = 1
		begin
		update StoreDistrict
					set EffectivityEndDate = getDate()					
					where  StoreNum = @StoreNum and EffectivityEndDate is null
		if @@error <> 0
					select @errCode  = -1
		end
	else
	BEGIN
	if exists(select StoreNum from StoreDistrict where StoreNum = @StoreNum)
		begin
			
			select @curDistrictID = 0
			select @curRegionID = 0
			select @curDistrictID = DistrictID, @curRegionID = RegionID from StoreDistrict where  StoreNum = @StoreNum and EffectivityEndDate is null
			if (@curDistrictID <>  @DistrictId or @curRegionID <> @regionID)
				begin
				-- set the expirydate to old record
				update StoreDistrict
					set EffectivityEndDate = getDate()					
					where  StoreNum = @StoreNum and EffectivityEndDate is null
				-- add new record	
				insert into StoreDistrict (CompanyID,RegionID,DistrictId,StoreNum,EffectivityStartDate,CdStoreFlag)
				values(@CompanyID,@regionID,@DistrictId,@StoreNum,getDate(),@CdStoreFlag)
				
				-- 9/16/2003 update store users with new district/region alignment
				UPDATE HTSecurity_Dev.dbo.UserProfile 
					SET DistrictID =  @DistrictId
						,RegionID = @regionID
					WHERE StoreNum = @StoreNum
					if @@error <> 0
					select @errCode  = -1
				end
			ELSE
				BEGIN
					update StoreDistrict
					set CdStoreFlag = @CdStoreFlag
					where  StoreNum = @StoreNum and EffectivityEndDate is null
				END
		end
	else
		begin
			
			insert into StoreDistrict (CompanyID,RegionID,DistrictId,StoreNum,EffectivityStartDate)
				values(@CompanyID,@regionID,@DistrictId,@StoreNum,getDate())
			-- 9/16/2003 update store users with new district/region alignment
				UPDATE HTSecurity_Dev.dbo.UserProfile 
					SET DistrictID =  @DistrictId
						,RegionID = @regionID
					WHERE StoreNum = @StoreNum	
				if @@error <> 0
					select @errCode  = -1
				
		end
	END
	return @errCode
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT EXEC ON dbo.RSP_UpdateStoreDistrict TO RdsApp

GO