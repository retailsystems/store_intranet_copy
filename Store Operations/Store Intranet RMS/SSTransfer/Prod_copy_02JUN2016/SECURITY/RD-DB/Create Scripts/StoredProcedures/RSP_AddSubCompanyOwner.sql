IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_AddSubCompanyOwner')
	BEGIN
		PRINT 'Dropping Procedure RSP_AddSubCompanyOwner'
		DROP  Procedure  RSP_AddSubCompanyOwner
	END

GO

PRINT 'Creating Procedure RSP_AddSubCompanyOwner'
GO
CREATE Procedure RSP_AddSubCompanyOwner
	@ReportID int
	,@CompanyID int
	,@OwnerId varchar(10)
	
AS

/******************************************************************************
**		File: 
**		Name: RSP_AddSubCompanyOwner
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
IF NOT EXISTS (SELECT ReportID FROM ReportSubCompanyOwner 
						WHERE ReportID = @ReportID
						AND CompanyID = @CompanyID 
						AND OwnerID = @OwnerID)
	INSERT INTO ReportSubCompanyOwner
		(
		ReportID
		,CompanyID
		,OwnerID
		)
	VALUES
		(
		@ReportID
		,@CompanyID
		,@OwnerID
		)

	IF @@ERROR != 0 
		BEGIN
			RAISERROR ('Failed to Add ReportSubCompanyOwner', 16, 3)
			RETURN -1
		END
	RETURN 0
GO

GRANT EXEC ON RSP_AddSubCompanyOwner TO RdsApp

GO
