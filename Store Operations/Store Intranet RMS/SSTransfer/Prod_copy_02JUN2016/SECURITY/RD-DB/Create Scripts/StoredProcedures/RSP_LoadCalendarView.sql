IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_LoadCalendarView')
	BEGIN
		PRINT 'Dropping Procedure RSP_LoadCalendarView'
		DROP  Procedure  RSP_LoadCalendarView
	END

GO

PRINT 'Creating Procedure RSP_LoadCalendarView'
GO
CREATE Procedure RSP_LoadCalendarView
	@employeeId varchar(30)
	,@JobCode varchar(30)
	,@BeginDate datetime
	,@EndDate datetime
	
AS

/******************************************************************************
**		File: 
**		Name: RSP_LoadCalendarView
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 7/24/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
SET NOCOUNT ON 
begin

		Select distinct  r.ReportID,r.ReportTitle, rs.FiscalWeekEndingDate,rs.ReportDate, r.SortOrder,rs.ReportSubmissionId, rs.Comment
				from ReportFile rf join ReportSubmission rs on rs.ReportSubmissionId = rf.ReportSubmissionID AND FiscalWeekEndingDate BETWEEN @BeginDate AND @EndDate
				join Report r on rs.ReportID = r.ReportID
				--Join ReportJobCodeOwner rjo ON r.ReportID = rjo.ReportID and JobCode = @JobCode
				Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
				join dbo.RFN_GetStoresByUser(@employeeId,@JobCode) fn on fn.contentScopeType = rf.contentScopeType and fn.OwnerID = rf.OwnerID 
				JOIN ReportCompanyOwner rco ON rco.ReportID = r.ReportID AND rco.CompanyID = fn.CompanyID AND dbo.RFN_CdStoreCheck(fn.CdStoreFlag, rco.CdStoresFlag) = 1
				where rs.Status in(2,4) AND (fn.EffectivityEndDate is null OR fn.EffectivityEndDate >= rs.FiscalWeekEndingDate )				 					
				and EffectivityStartDate <= getDate()
				AND dbo.RFN_JobCodeOwnerCheck(r.ReportID,@JobCode) = 1
				order by rs.FiscalWeekEndingDate,r.SortOrder
end

GO

GRANT EXEC ON RSP_LoadCalendarView TO RdsApp

GO
