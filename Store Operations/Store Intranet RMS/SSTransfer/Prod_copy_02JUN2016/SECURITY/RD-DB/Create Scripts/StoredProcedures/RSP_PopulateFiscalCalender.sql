IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_PopulateFiscalCalender')
	BEGIN
		PRINT 'Dropping Procedure RSP_PopulateFiscalCalender'
		DROP  Procedure  RSP_PopulateFiscalCalender
	END

GO

PRINT 'Creating Procedure RSP_PopulateFiscalCalender'
GO
CREATE Procedure RSP_PopulateFiscalCalender
	/* Param List */
AS

/******************************************************************************
**		File: RSP_PopulateFiscalCalender.sql
**		Name: RSP_PopulateFiscalCalender
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by: JOB RDSJOB_PopulateFiscalCalender  
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 7/25/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
DECLARE @MaxBegDt DATETIME


Select @MaxBegDt = MAX(BEG_DT) FROM FiscalCalender
IF @MaxBegDt IS NULL
	SELECT @MaxBegDt = '1/1/1900'
	
CREATE TABLE #TempCal
	(
		YR int
		,BEG_DT datetime
		,END_DT datetime
		,PER_NUM int
		,WK_NUM int
	)	
INSERT INTO 
	#TempCal 
		(
		YR
		,BEG_DT
		,END_DT
		,PER_NUM
		,WK_NUM
		)
SELECT     YR, BEG_DT, DATEADD(day, 6, BEG_DT) AS END_DT, PER_NUM, WK_NUM
FROM         OPENQUERY(gers, 'select * from sls_per_dt') 


INSERT INTO 
	FiscalCalender 
		(
		YR
		,BEG_DT
		,END_DT
		,PER_NUM
		,WK_NUM
		)
	SELECT 
	    YR
	    ,BEG_DT
	    ,DATEADD(day, 6, BEG_DT)
	    ,PER_NUM
	    ,WK_NUM
	    FROM #TempCal WHERE BEG_DT > @MaxBegDt

DROP TABLE #TempCal

GO

GRANT EXEC ON RSP_PopulateFiscalCalender TO RdsApp

GO
