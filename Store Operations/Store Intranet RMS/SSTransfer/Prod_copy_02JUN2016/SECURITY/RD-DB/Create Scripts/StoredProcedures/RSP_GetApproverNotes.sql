IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetApproverNotes')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetApproverNotes'
		DROP  Procedure  RSP_GetApproverNotes
	END

GO

PRINT 'Creating Procedure RSP_GetApproverNotes'
GO
CREATE Procedure RSP_GetApproverNotes
	@ReportSubmissionID as int
AS

/******************************************************************************
**		File: RSP_GetApproverNotes.sql
**		Name: RSP_GetApproverNotes
**		Desc: 
**
**		This template can be customized:
**              
**		Return values: Dataset
** 
**		Called by:   WorkFlowManager.vb
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 7/8/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

Select RSA.Comment, RSA.InsertDate, (FirstName + ' ' + LastName) as FullName
	 From ReportSubmissionApproval RSA INNER JOIN RV_AppUsers USR
	ON RSA.EmployeeID = USR.EmployeeID WHERE RSA.ReportSubmissionID = @ReportSubmissionID
	order by 2 desc


GO

GRANT EXEC ON RSP_GetApproverNotes TO RdsApp

GO
