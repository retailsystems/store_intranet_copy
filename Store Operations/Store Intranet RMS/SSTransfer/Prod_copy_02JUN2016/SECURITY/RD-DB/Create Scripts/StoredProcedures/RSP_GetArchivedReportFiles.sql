IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetArchivedReportFiles')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetArchivedReportFiles'
		DROP  Procedure  RSP_GetArchivedReportFiles
	END

GO

PRINT 'Creating Procedure RSP_GetArchivedReportFiles'
GO
CREATE Procedure RSP_GetArchivedReportFiles
	@StoreNum int
AS

/******************************************************************************
**		File: RSP_GetArchivedReportFiles.sql
**		Name: RSP_GetArchivedReportFiles
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 6/13/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

Select rf.* from RV_ArchivedReportFile rf inner join ReportFileStoreOwner rfo
		on rf.ReportFileID = rfo.ReportFileID and rfo.StoreNum = @storeNum



GO

GRANT EXEC ON RSP_GetArchivedReportFiles TO RdsApp

GO
