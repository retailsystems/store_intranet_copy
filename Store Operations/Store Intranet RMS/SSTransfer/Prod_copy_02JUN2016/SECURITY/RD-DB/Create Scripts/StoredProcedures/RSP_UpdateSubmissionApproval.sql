SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_UpdateSubmissionApproval]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_UpdateSubmissionApproval]
GO

CREATE PROCEDURE dbo.RSP_UpdateSubmissionApproval
	@ReportSubmissionID int,
	@EmployeeId int,
	@status int,	
	@userId varchar(30),
	@userRoleId int,
	@Comment ntext

/******************************************************************************
**		File: RSP_UpdateSubmissionApproval.sql
**		Name: RSP_UpdateSubmissionApproval
**		Desc:  This storedprocedure  Add/Update ReportSubmissionApproval information 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportID
**	
**		Auth: Sri Bajjuri
**		Date: 6/11/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
AS
	SET NOCOUNT ON
	declare @errCode int, @isAdmin bit , @reportId int
	select @errCode  = 0
	select @isAdmin = dbo.RFN_IsAdminRole(@userRoleId)
	select @reportId = ReportID from ReportSubmission where ReportSubmissionID = @ReportSubmissionID
	
	if @isAdmin = 1
		
		
		BEGIN
			/* handle cancellations */
			if @status = 5
				BEGIN
					insert into ReportSubmissionApproval 
					(
					ReportSubmissionID,
					EmployeeId,
					Status,
					Comment,
					InsertBy,
					InsertDate
					)
					values
					(
					@ReportSubmissionID,
					@EmployeeId,
					@Status,
					@Comment,
					@userId,
					getDate()
					)		
					
					UPDATE ReportSubmission 
						SET
						Status = @Status
						,UpdateBy = @userId
						,UpdateDate = getDate()
						WHERE ReportSubmissionID = @ReportSubmissionID
					if @@ERROR <> 0
						BEGIN							
							RAISERROR('RSP_UpdateSubmissionApproval: Error while cancelling  Submission', 16, 1)
							RETURN -1
						END
				END
			ELSE			
			insert into ReportSubmissionApproval 
				(
				ReportSubmissionID,
				EmployeeId,
				Status,
				Comment,
				InsertBy,
				InsertDate
				)
				select 
				@ReportSubmissionID,
				EmployeeId,
				@Status,
				@Comment,
				@userId,
				getDate()
				FROM ReportApprover WHERE ReportId = @reportId and EmployeeId not in(select EmployeeId from ReportSubmissionApproval where ReportSubmissionID = @ReportSubmissionID )
		END
	ELSE
		BEGIN
	/*if exists(select ReportSubmissionID from ReportSubmissionApproval where ReportSubmissionID = @ReportSubmissionID and EmployeeId = @EmployeeId)
		 Do nothing
		begin
			update ReportSubmissionApproval
				set Status = @Status,
					Comment = @Comment, 
					UpdateBy = @userId,
					UpdateDate = getDate()
					where ReportSubmissionID = @ReportSubmissionID and EmployeeId = @EmployeeId
					if @@ERROR <> 0
						BEGIN							
							RAISERROR('RSP_UpdateSubmissionApproval: Error updating to SubmissionApproval', 16, 1)
							RETURN -1
						END
		end
		
	else*/
		
			insert into ReportSubmissionApproval 
				(
				ReportSubmissionID,
				EmployeeId,
				Status,
				Comment,
				InsertBy,
				InsertDate
				)
				values
				(
				@ReportSubmissionID,
				@EmployeeId,
				@Status,
				@Comment,
				@userId,
				getDate()
				)				
	END			
				if @@ERROR <> 0
						BEGIN							
							RAISERROR('RSP_UpdateSubmissionApproval: Error updating to SubmissionApproval', 16, 1)
							RETURN -1
						END
				
		
	
		--if @status = 2
		--BEGIN
		-- CALL RSP_ResetSubmissionStatus to Update Report Submission Status
		EXEC RSP_ResetSubmissionStatus @ReportSubmissionID,@Status
		if @@ERROR <> 0
			BEGIN							
				RAISERROR('RSP_UpdateSubmissionApproval: Error updating to SubmissionApproval', 16, 1)
				RETURN -1
			END
		--END
	RETURN 0
GO

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXEC ON RSP_UpdateSubmissionApproval TO RdsApp

GO
