IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_DeleteReportSubmissionById')
	BEGIN
		PRINT 'Dropping Procedure RSP_DeleteReportSubmissionById'
		DROP  Procedure  RSP_DeleteReportSubmissionById
	END

GO

PRINT 'Creating Procedure RSP_DeleteReportSubmissionById'
GO
CREATE Procedure RSP_DeleteReportSubmissionById
	@reportSubmissionId int
AS

/******************************************************************************
**		File: RSP_DeleteReportSubmissionById.sql
**		Name: RSP_DeleteReportSubmissionById
**		Desc: Deletes ReportSubmission record given the Id.
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Jeric Saez
**		Date: 7/8/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

	SET NOCOUNT ON
	
	DELETE	ReportSubmission
	WHERE	ReportSubmissionId = @reportSubmissionId


GO

GRANT EXEC ON RSP_DeleteReportSubmissionById TO RdsApp

GO
