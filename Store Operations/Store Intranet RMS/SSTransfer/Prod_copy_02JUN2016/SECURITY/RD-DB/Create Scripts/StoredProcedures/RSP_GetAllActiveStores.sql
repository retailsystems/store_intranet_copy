IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetAllActiveStores')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetAllActiveStores'
		DROP  Procedure  RSP_GetAllActiveStores
	END

GO

PRINT 'Creating Procedure RSP_GetAllActiveStores'
GO
CREATE Procedure RSP_GetAllActiveStores
	/* Param List */
	@CompanyID int
AS

/******************************************************************************
**		File: RSP_GetAllActiveStores.sql
**		Name: RSP_GetAllActiveStores
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri		
**		Date: 10/07/2004
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
IF (@CompanyID = 1 OR @CompanyID = 2)
	Select StoreNum, Region as RegionID, District as DistrictID 
	FROM RV_StoreInfo
	WHERE (Deleted IS NULL OR Deleted=0) 
	AND CompanyID = @CompanyID AND StagingStore <> 1 AND InterNetStore <> 1
	AND Region NOT in(0,90) AND District not in (0,900)	
	ORDER BY Region,District,StoreNum
ELSE 
	Select StoreNum, Region as RegionID, District as DistrictID 
	FROM RV_StoreInfo
	WHERE (Deleted IS NULL OR Deleted=0) 
	AND StagingStore <> 1 AND InterNetStore <> 1
	AND Region NOT in(0,90) AND District not in (0,900)	
	ORDER BY Region,District,StoreNum

GO

GRANT EXEC ON RSP_GetAllActiveStores TO PUBLIC

GO
