IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_AddCompanyOwner')
	BEGIN
		PRINT 'Dropping Procedure RSP_AddCompanyOwner'
		DROP  Procedure  RSP_AddCompanyOwner
	END

GO

PRINT 'Creating Procedure RSP_AddCompanyOwner'
GO
CREATE Procedure RSP_AddCompanyOwner
	@ReportId int,
	@CompanyId int,
	@cdStoresFlag bit
	
	/* Param List */
AS

/******************************************************************************
**		File: RSP_AddCompanyOwner.sql
**		Name: RSP_AddCompanyOwner.sql
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 6/20/03
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/


	begin
	if not exists (select companyId  from ReportCompanyOwner where companyId = @CompanyId and ReportId = @reportID)
		insert into ReportCompanyOwner values(@reportID,@CompanyId,@cdStoresFlag)
	else
		update ReportCompanyOwner set CdStoresFlag = @cdStoresFlag where companyId = @CompanyId and ReportId = @reportID	
	
	end
	if @@error != 0 
					begin						
						RAISERROR ('Failed to add/update company owners', 16, 3)
						RETURN -1
					end	

GO

GRANT EXEC ON RSP_AddCompanyOwner TO RdsApp

GO
