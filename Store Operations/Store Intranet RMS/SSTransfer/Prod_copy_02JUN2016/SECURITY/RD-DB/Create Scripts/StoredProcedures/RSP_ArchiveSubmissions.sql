IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_ArchiveSubmissions')
	BEGIN
		PRINT 'Dropping Procedure RSP_ArchiveSubmissions'
		DROP  Procedure  RSP_ArchiveSubmissions
	END

GO

PRINT 'Creating Procedure RSP_ArchiveSubmissions'
GO
CREATE Procedure RSP_ArchiveSubmissions
	/* Param List */
AS

/******************************************************************************
**		File: RSP_ArchiveSubmissions.sql
**		Name: RSP_ArchiveSubmissions
**		Desc: This archives report submissions of expiration type 'Expiry Date'
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:  Sql Job 
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 7/11/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    7/31/03		Sri Bajjuri			Archive only approved files
*******************************************************************************/
Update ReportFile 
	SET ArchivedFlag=1 
	,UpdateDate = getDate()
	WHERE ReportSubmissionID in(
		Select ReportSubmissionID From ReportSubmission
		WHERE EffectivityEndDate <= getDate()
		AND Status = 2 AND  
		ReportID IN(SELECT ReportID From Report WHERE ExpirationType = 1) 
	)
UPDATE ReportSubmission 
	SET Status = 4 
	WHERE EffectivityEndDate <= getDate()
		AND Status = 2 AND  
		ReportID IN(SELECT ReportID From Report WHERE ExpirationType = 1) 

if @@error <> 0 
			begin
				RAISERROR ('Failed to Archive Files.:RSP_ArchiveSubmissions', 16, 3)
				RETURN -1
			end
		
GO

GRANT EXEC ON RSP_ArchiveSubmissions TO RdsApp

GO
