IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetAllSubCompanyOwners')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetAllSubCompanyOwners'
		DROP  Procedure  RSP_GetAllSubCompanyOwners
	END

GO

PRINT 'Creating Procedure RSP_GetAllSubCompanyOwners'
GO
CREATE Procedure RSP_GetAllSubCompanyOwners
	@ReportID int
AS

/******************************************************************************
**		File: RSP_GetAllSubCompanyOwners.sql
**		Name: RSP_GetAllSubCompanyOwners
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
DECLARE @ContentScopeType int
SELECT @ContentScopeType = ContentScopeType FROM Report WHERE ReportID = @ReportID
IF @ContentScopeType = 2
	SELECT DISTINCT RegionID as OwnerID,rco.CompanyID, ('Region '+ RegionID) as OwnerName
		,C.CompanyName
		FROM StoreDistrict sd
		JOIN ReportCompanyOwner rco ON sd.CompanyID = rco.CompanyID 
			AND rco.ReportID = @ReportID 
			AND ((rco.CDStoresFlag = 0) OR (sd.CDStoreFlag = rco.CDStoresFlag))
		INNER JOIN Company C ON sd.CompanyID = C.CompanyID
		WHERE EffectivityStartDate <= getDate() AND (EffectivityEndDate > getDate() OR EffectivityEndDate is null)
ELSE IF @ContentScopeType = 3
	SELECT DISTINCT DistrictID as OwnerID,rco.CompanyID, ('District '+ DistrictID) as OwnerName
		,C.CompanyName
		FROM StoreDistrict sd
		JOIN ReportCompanyOwner rco ON sd.CompanyID = rco.CompanyID 
			AND rco.ReportID = @ReportID 
			AND ((rco.CDStoresFlag = 0) OR (sd.CDStoreFlag = rco.CDStoresFlag))
		INNER JOIN Company C ON sd.CompanyID = C.CompanyID
		WHERE EffectivityStartDate <= getDate() AND (EffectivityEndDate > getDate() OR EffectivityEndDate is null)	
ELSE IF @ContentScopeType = 4
	SELECT DISTINCT StoreNum as OwnerID,rco.CompanyID, ('Store '+ convert(varchar,StoreNum)) as OwnerName 
		,C.CompanyName
		FROM StoreDistrict sd
		JOIN ReportCompanyOwner rco ON sd.CompanyID = rco.CompanyID 
			AND rco.ReportID = @ReportID 
			AND ((rco.CDStoresFlag = 0) OR (sd.CDStoreFlag = rco.CDStoresFlag))
		INNER JOIN Company C ON sd.CompanyID = C.CompanyID
		WHERE EffectivityStartDate <= getDate() AND (EffectivityEndDate > getDate() OR EffectivityEndDate is null)		
ELSE
	RETURN -1

GO

GRANT EXEC ON RSP_GetAllSubCompanyOwners TO RdsApp

GO
