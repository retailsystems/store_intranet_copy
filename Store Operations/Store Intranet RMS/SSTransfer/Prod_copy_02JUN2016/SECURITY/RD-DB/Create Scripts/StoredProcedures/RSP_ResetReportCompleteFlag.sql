IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_ResetReportCompleteFlag')
	BEGIN
		PRINT 'Dropping Procedure RSP_ResetReportCompleteFlag'
		DROP  Procedure  RSP_ResetReportCompleteFlag
	END

GO

PRINT 'Creating Procedure RSP_ResetReportCompleteFlag'
GO
CREATE Procedure RSP_ResetReportCompleteFlag
@ReportID as int
	/* Param List */
AS

/******************************************************************************
**		File: RSP_ResetReportCompleteFlag.sql
**		Name: RSP_ResetReportCompleteFlag
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
declare @ApprovalType int, @blnComplete bit
set @blnComplete = 1
select @ApprovalType = ApprovalType from Report Where ReportID = @ReportID
/*
if not exists(select top 1 reportid from ReportJobCodeOwner where  ReportID = @ReportID)
		select @blnComplete = 0
else 
*/
if @ApprovalType > 1 
	if not exists(select top 1 employeeID from ReportApprover where  ReportID = @ReportID)
		select @blnComplete = 0

	
update Report set SetupCompleteFlag = @blnComplete where  ReportID = @ReportID
if @@error != 0 
					begin						
						RAISERROR ('RSP_ResetReportCompleteFlag:Failed to update report complete status.', 16, 3)
						RETURN -1
					end	
RETURN 0

GO

GRANT EXEC ON RSP_ResetReportCompleteFlag TO RdsApp

GO
