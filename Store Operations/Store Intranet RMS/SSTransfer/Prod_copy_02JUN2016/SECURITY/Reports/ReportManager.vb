' Static Model
Imports HotTopic.RD.Entity
Imports HotTopic.RD.Reports
Imports HotTopic.RD.Services
Imports System.Data.SqlClient
Namespace HotTopic.RD.Reports
    '*******************************************************************************
    '**		Change History
    '*******************************************************************************
    '**		Date:		Author:				Description:
    '**		6/17/03		Sri bajjuri			new method AddApprovers added.
    '**    
    '*******************************************************************************/



    Public Class ReportManager

        Public Shared Sub Save(ByVal rpt As Report, ByVal userID As String)
            Try
                ' Set report parameters (18 input, 1 output) 
                Dim arParms() As SqlParameter = New SqlParameter(19) {}
                With rpt
                    arParms(0) = New SqlParameter("@reportId", SqlDbType.Int, 4, ParameterDirection.InputOutput)
                    arParms(0).Value = .ReportId
                    arParms(1) = New SqlParameter("@ReportTitle", SqlDbType.VarChar, 30)
                    arParms(1).Value = .Title
                    arParms(2) = New SqlParameter("@ReportDesc", SqlDbType.VarChar, 255)
                    arParms(2).Value = .Description
                    arParms(3) = New SqlParameter("@InputType", SqlDbType.Int)
                    arParms(3).Value = .InputType
                    arParms(4) = New SqlParameter("@StagingPath", SqlDbType.VarChar, 255)
                    arParms(4).Value = .StagingPath
                    arParms(5) = New SqlParameter("@LocationPath", SqlDbType.VarChar, 255)
                    arParms(5).Value = .LocationPath
                    arParms(6) = New SqlParameter("@ExpirationType", SqlDbType.Int)
                    arParms(6).Value = .ExpirationType
                    arParms(7) = New SqlParameter("@SortOrder", SqlDbType.Int)
                    arParms(7).Value = .SortOrder
                    arParms(8) = New SqlParameter("@DefaultFile", SqlDbType.VarChar, 50)
                    arParms(8).Value = .DefaultFileName
                    arParms(9) = New SqlParameter("@UserId", SqlDbType.VarChar, 20)
                    arParms(9).Value = userID
                    arParms(10) = New SqlParameter("@Scope", SqlDbType.Int)
                    arParms(10).Value = .ContentScope
                    arParms(11) = New SqlParameter("@FileIdentifier", SqlDbType.VarChar, 50)
                    arParms(11).Value = .DefaultFileIdentifier
                    arParms(12) = New SqlParameter("@FileFormat", SqlDbType.VarChar, 50)
                    arParms(12).Value = .FileFormat
                    arParms(13) = New SqlParameter("@ArchiveNth", SqlDbType.Int)
                    arParms(13).Value = UtilityManager.IntegerToNull(.ArchiveNth)
                    arParms(14) = New SqlParameter("@MaxFileSize", SqlDbType.Int)
                    arParms(14).Value = .MaxFileSize
                    arParms(15) = New SqlParameter("@ApproverEmail", SqlDbType.VarChar, 50)
                    arParms(15).Value = .ApproverGroupEmail
                    arParms(16) = New SqlParameter("@ApprovalType", SqlDbType.Int)
                    arParms(16).Value = .ApprovalType
                    arParms(17) = New SqlParameter("@ReadOnlyFlag", SqlDbType.Bit)
                    arParms(17).Value = .ReadOnlyFlag
                    arParms(18) = New SqlParameter("@NewReportID", SqlDbType.Int)
                    arParms(18).Value = 0
                    arParms(18).Direction = ParameterDirection.Output
                    arParms(19) = New SqlParameter("@AllowMultiFlag", SqlDbType.Bit)
                    arParms(19).Value = .AllowMultiFlag


                End With
                SQLDataManager.GetInstance().Execute("dbo.RSP_UpdateReport", arParms)
                rpt.ReportId = UtilityManager.NullToInteger(arParms(18).Value)
                If rpt.ReportId > 0 Then
                    ' add company owners
                    Dim i As Integer = 0
                    Dim arParms1() As SqlParameter = New SqlParameter(2) {}
                    For i = 0 To rpt.CompanyOwners.Count - 1
                        Dim _company As New CompanyOwner()
                        _company = rpt.CompanyOwners(i)
                        With _company
                            arParms1(0) = New SqlParameter("@ReportId", SqlDbType.Int)
                            arParms1(0).Value = rpt.ReportId
                            arParms1(1) = New SqlParameter("@CompanyId", SqlDbType.Int)
                            arParms1(1).Value = .CompanyId
                            arParms1(2) = New SqlParameter("@CDStoresFlag", SqlDbType.Bit)
                            arParms1(2).Value = .CDStoresFlag
                            SQLDataManager.GetInstance().Execute("dbo.RSP_AddCompanyOwner", arParms1)
                        End With
                    Next
                End If
           
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub

        ' Gets a report and associated approvers and owners
        Public Shared Function GetReport(ByVal reportId As Int32) As Report
            Try
                Dim rpt As Report = New Report()
                rpt.ReportId = reportId
                EntityManager.GetReportById(rpt, True)
                Return rpt
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Function

        ' Lists all reports in the Report table.
        Public Shared Function List() As DataSet
            'Dim rds As ReportDataSet = New ReportDataSet()
            Dim ds As DataSet
            ds = SQLDataManager.GetInstance().GetDataSet("dbo.RSP_ListReports")

            Return ds
        End Function

        Public Shared Sub Submit(ByVal rptSubmission As Submission, ByVal userID As String)
            Try
                ' Set report parameters (12 input) 
                Dim arParms() As SqlParameter = New SqlParameter(12) {}
                With rptSubmission
                    arParms(0) = New SqlParameter("@ReportSubmissionID", SqlDbType.Int)
                    arParms(0).Direction = ParameterDirection.InputOutput
                    arParms(0).Value = .ReportSubmissionId
                    arParms(1) = New SqlParameter("@reportId", SqlDbType.Int)
                    arParms(1).Value = .ReportId
                    arParms(2) = New SqlParameter("@ReportDate", SqlDbType.DateTime)
                    arParms(2).Value = .ReportDate
                    arParms(3) = New SqlParameter("@Status", SqlDbType.Int)
                    arParms(3).Value = .Status
                    arParms(4) = New SqlParameter("@UserId", SqlDbType.VarChar, 30)
                    arParms(4).Value = userID
                    arParms(5) = New SqlParameter("@StagingPath", SqlDbType.VarChar, 255)
                    arParms(5).Value = .StagingPath
                    arParms(6) = New SqlParameter("@LocationPath", SqlDbType.VarChar, 255)
                    arParms(6).Value = .LocationPath
                    arParms(7) = New SqlParameter("@EffectivityStartDate", SqlDbType.DateTime)
                    arParms(7).Value = .EffectivityStartDate
                    arParms(8) = New SqlParameter("@Comment", SqlDbType.VarChar, 2000)
                    arParms(8).Value = .Comment
                    arParms(9) = New SqlParameter("@EffectivityEndDate", SqlDbType.DateTime)
                    arParms(9).Value = UtilityManager.DateToNull(.EffectivityEndDate)
                    arParms(10) = New SqlParameter("@MDReportType", SqlDbType.Int)
                    arParms(10).Value = UtilityManager.IntegerToNull(.MDReportType)
                    arParms(11) = New SqlParameter("@MDEffectiveDate", SqlDbType.DateTime)
                    arParms(11).Value = UtilityManager.DateToNull(.MDEffectiveDate)
                    arParms(12) = New SqlParameter("@MDPcNum", SqlDbType.VarChar, 50)
                    arParms(12).Value = UtilityManager.StringToNull(.MDPcNum)

                End With
                SQLDataManager.GetInstance().Execute("dbo.RSP_AddReportSubmission", arParms)
                rptSubmission.ReportSubmissionId = arParms(0).Value
                EntityManager.GetReportSubmissionById(rptSubmission)
                Try
                    'put file into staging if upload
                    If rptSubmission.InputType = FileInputType.Upload Then
                        'JDS 7/22/2003: remove; will just use submissionid to uniquely identify file uploaded
                        'Dim stagingPath As String = rptSubmission.StagingPath + rptSubmission.FiscalWeekEndingDate.ToString("MMddyyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo) + "\"
                        'Dim fileName As String = rptSubmission.FileSubmitted.Name.Replace(rptSubmission.FileSubmitted.Name.Substring(0, rptSubmission.FileSubmitted.Name.LastIndexOf(".") + 1), rptSubmission.ReportSubmissionId)
                        Dim fileName As String = rptSubmission.ReportSubmissionId & "_" & rptSubmission.FileSubmitted.Name
                        FileSystemManager.EnsureExists(rptSubmission.StagingPath)
                        'FileSystemManager.DeleteFiles(rptSubmission.StagingPath)
                        FileSystemManager.SaveFile(rptSubmission.StagingPath + fileName, rptSubmission.FileSubmitted.Length, rptSubmission.FileSubmitted.FileStream)
                    End If
                 
                Catch ex As Exception
                    'rollback tran; delete ReportSubmission
                    arParms = New SqlParameter(0) {}
                    arParms(0) = New SqlParameter("@reportSubmissionId", SqlDbType.Int)
                    arParms(0).Value = rptSubmission.ReportSubmissionId
                    SQLDataManager.GetInstance().Execute("dbo.RSP_DeleteReportSubmissionById", arParms)
                    Throw ex
                End Try
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Public Shared Sub GetMDMasterDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByVal companyId As Int16)
            'Markdown master report
            Try
                'Create Store Dataset
                Dim Cn As New OracleDataManager()
                Dim strSql As String

                Dim effectiveDate, pc_tp, prcSql, whereSql As String
                Dim companyFilter As Int16 ' 1- HotTopic , 2 - Torrid, 0 - Both

                prcSql = ""
                Select Case rptSubmission.MDReportType
                    Case MDReportType.Regular
                        pc_tp = "MD"
                        prcSql = " AND PRC.RET_PRC > .01 AND PRC.RET_PRC LIKE '%.98' "
                    Case MDReportType.Further
                        pc_tp = "MD"
                        prcSql = " AND PRC.RET_PRC > .01 AND PRC.RET_PRC NOT LIKE '%.98' "
                    Case MDReportType.MOS
                        pc_tp = "MD"
                        prcSql = " AND PRC.RET_PRC = .01  "
                    Case MDReportType.MDAll
                        pc_tp = "MD"
                        prcSql = ""
                    Case MDReportType.MarkUp
                        pc_tp = "MU"
                        prcSql = ""
                    Case MDReportType.MDCancel
                        pc_tp = "MDC"
                        prcSql = ""
                    Case MDReportType.MUCancel
                        pc_tp = "MUC"
                        prcSql = ""
                End Select


                If rptSubmission.MDPcNum <> "" AndAlso IsNumeric(rptSubmission.MDPcNum) Then
                    whereSql = " WHERE PC_NUM = '" & rptSubmission.MDPcNum & "' "
                ElseIf IsDate(rptSubmission.MDEffectiveDate) Then
                    effectiveDate = rptSubmission.MDEffectiveDate.Day & "-" & MonthName(rptSubmission.MDEffectiveDate.Month, True).ToLower & "-" & rptSubmission.MDEffectiveDate.Year
                    whereSql = " WHERE BEG_DT = '" & effectiveDate & "' "
                Else
                    Throw New Exception("MDReport:Invalid MD report request")
                End If

                Select Case companyId
                    Case 1 ' Hot Topic
                        whereSql &= " AND DEPT.DIV_CD = 1"
                    Case 2 ' Torrid
                        whereSql &= " AND DEPT.DIV_CD = 5"
                End Select
                'Get MD Dataset
                strSql = "SELECT PRC.SKU_NUM,PRC.ITM_CD,PRC.RET_PRC,PRC.PREV_PERM_RET, PRC.PC_NUM, PRC.BEG_DT " & _
                        " ,PRC.PC_TP,GM_ITM.DEPT_CD,CLASS.CLASS_CD,CLASS.DES AS CLASS_DESC" & _
                        " , GM_ITM.DES1 AS ITEM_DESC,GM_SKU.SIZE_CD, LPAD(GM_ITM.VE_CD,7) AS VE_CD " & _
                        " FROM GM_PRC PRC, CLASS, GM_ITM, GM_SKU, DEPT" & _
                        whereSql & " AND pc_tp ='" & pc_tp & "' " & prcSql & _
                        " AND PC_NUM = (SELECT MAX(PC_NUM) FROM GM_PRC t1 WHERE PRC.SKU_NUM = t1.SKU_NUM AND PRC.ITM_CD = t1.ITM_CD)" & _
                        " AND PRC.ITM_CD = GM_ITM.ITM_CD " & _
                        " AND GM_ITM.DEPT_CD = DEPT.DEPT_CD " & _
                        " AND GM_ITM.CLASS_CD = CLASS.CLASS_CD" & _
                        " AND GM_SKU.SKU_NUM = PRC.SKU_NUM" & _
                        " ORDER BY GM_ITM.DEPT_CD,CLASS.CLASS_CD,VE_CD ,PRC.ITM_CD,PRC.SKU_NUM"
                'Throw New Exception("MDReport:" & strSql)
                mdDs = Cn.ExecuteQuery(EnumRSTypes.DataSet, strSql)

                Cn.CloseConnection()
                If mdDs.Tables(0).Rows.Count = 0 Then
                    Throw New Exception("MDReport:No records found")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub GetMDWHDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByVal companyId As Int16)
            'Markdown ware house report
            Try
                'Create Store Dataset
                Dim Cn As New OracleDataManager()
                Dim strSql As String

                Dim effectiveDate, pc_tp, prcSql, whereSql As String

                Select Case rptSubmission.MDReportType
                    Case MDReportType.Regular
                        pc_tp = "MD"
                        prcSql = " AND PRC.RET_PRC > .01 AND PRC.RET_PRC LIKE '%.98' "
                    Case MDReportType.Further
                        pc_tp = "MD"
                        prcSql = " AND PRC.RET_PRC > .01 AND PRC.RET_PRC NOT LIKE '%.98' "
                    Case MDReportType.MOS
                        pc_tp = "MD"
                        prcSql = " AND PRC.RET_PRC = .01  "
                    Case MDReportType.MDAll
                        pc_tp = "MD"
                        prcSql = ""
                    Case MDReportType.MarkUp
                        pc_tp = "MU"
                        prcSql = ""
                    Case MDReportType.MDCancel
                        pc_tp = "MDC"
                        prcSql = ""
                    Case MDReportType.MUCancel
                        pc_tp = "MUC"
                        prcSql = ""
                End Select


                If rptSubmission.MDPcNum <> "" AndAlso IsNumeric(rptSubmission.MDPcNum) Then
                    whereSql = " WHERE PC_NUM = '" & rptSubmission.MDPcNum & "' "
                ElseIf IsDate(rptSubmission.MDEffectiveDate) Then
                    effectiveDate = rptSubmission.MDEffectiveDate.Day & "-" & MonthName(rptSubmission.MDEffectiveDate.Month, True).ToLower & "-" & rptSubmission.MDEffectiveDate.Year
                    whereSql = " WHERE BEG_DT = '" & effectiveDate & "' "
                Else
                    Throw New Exception("MDReport:Invalid MD report request")
                End If
                Select Case companyId
                    Case 1 ' Hot Topic
                        whereSql &= " AND DEPT.DIV_CD = 1"
                    Case 2 ' Torrid
                        whereSql &= " AND DEPT.DIV_CD = 5"
                End Select
                'Get MD Dataset
                strSql = "SELECT PRC.SKU_NUM,PRC.ITM_CD,PRC.RET_PRC,PRC.PREV_PERM_RET, PRC.PC_NUM, PRC.BEG_DT, PRC.PC_TP " & _
                        " ,GM_ITM.DEPT_CD,CLASS.CLASS_CD,CLASS.DES AS CLASS_DESC,INV.STORE_CD,INV.LOC_CD, INV.LOC_TP" & _
                        " ,INV.OH_QTY, GM_ITM.DES1 AS ITEM_DESC,GM_SKU.SIZE_CD, VE_NAME, LPAD(AP.VE.VE_CD,7) AS VE_CD" & _
                        " FROM GM_PRC PRC, CLASS, GM_INV_LOC_V INV, GM_ITM, GM_SKU, AP.VE, DEPT" & _
                        whereSql & " AND pc_tp ='" & pc_tp & "' " & prcSql & _
                        " AND INV.STORE_CD = '9999' " & _
                        " AND PC_NUM = (SELECT MAX(PC_NUM) FROM GM_PRC t1 WHERE PRC.SKU_NUM = t1.SKU_NUM AND PRC.ITM_CD = t1.ITM_CD)" & _
                        " AND PRC.ITM_CD = GM_ITM.ITM_CD " & _
                        " AND GM_ITM.CLASS_CD = CLASS.CLASS_CD" & _
                        " AND GM_ITM.DEPT_CD = DEPT.DEPT_CD " & _
                        " AND INV.SKU_NUM = PRC.SKU_NUM AND INV.OH_QTY > 0" & _
                        " AND GM_SKU.SKU_NUM = PRC.SKU_NUM" & _
                        " AND GM_ITM.VE_CD = AP.VE.VE_CD" & _
                        " ORDER BY INV.LOC_TP,INV.LOC_CD,GM_ITM.DEPT_CD,CLASS.CLASS_CD,VE_CD,PRC.ITM_CD,PRC.SKU_NUM"
                mdDs = Cn.ExecuteQuery(EnumRSTypes.DataSet, strSql)
                'Throw New Exception(strSql)
                Cn.CloseConnection()
                If mdDs.Tables(0).Rows.Count = 0 Then
                    Throw New Exception("MDReport:No records found")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'SRI 12/18/03 MD Reports
        Public Shared Sub GetMDReportDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByRef storeDs As DataSet)
            Try
                'Create Store Dataset
                Dim Cn As New OracleDataManager()
                Dim strSql As String

                Dim effectiveDate, pc_tp, prcSql, whereSql As String
                Dim companyFilter As Int16 ' 1- HotTopic , 2 - Torrid, 0 - Both
                Dim cOwner As CompanyOwner
                If rptSubmission.CompanyOwners.Count = 1 Then
                    cOwner = rptSubmission.CompanyOwners(0)
                    companyFilter = cOwner.CompanyId
                Else
                    companyFilter = 0
                End If

                Select Case rptSubmission.MDReportType
                    Case MDReportType.Regular
                        pc_tp = "MD"
                        prcSql = " AND PRC.RET_PRC > .01 AND PRC.RET_PRC LIKE '%.98' "
                    Case MDReportType.Further
                        pc_tp = "MD"
                        ' MOD 4/16/04 Exclude price changes ending $$.99
                        'prcSql = " AND PRC.RET_PRC > .01 AND PRC.RET_PRC NOT LIKE '%.98' "
                        prcSql = " AND PRC.RET_PRC > .01 AND PRC.RET_PRC NOT LIKE '%.98' AND PRC.RET_PRC NOT LIKE  '%.99' "
                    Case MDReportType.MOS
                        pc_tp = "MD"
                        prcSql = " AND PRC.RET_PRC = .01  "
                    Case MDReportType.MDAll
                        pc_tp = "MD"
                        prcSql = ""
                    Case MDReportType.MarkUp
                        pc_tp = "MU"
                        prcSql = ""
                    Case MDReportType.MDCancel
                        pc_tp = "MDC"
                        prcSql = ""
                    Case MDReportType.MUCancel
                        pc_tp = "MUC"
                        prcSql = ""
                End Select


                If rptSubmission.MDPcNum <> "" AndAlso IsNumeric(rptSubmission.MDPcNum) Then
                    whereSql = " WHERE PC_NUM = '" & rptSubmission.MDPcNum & "' "
                    '6/7/04 5PM quick fix roll this back
                    '6/7/04 6PM quick fix rolled back
                    'prcSql = " AND PRC.RET_PRC LIKE '%.99' "
                ElseIf IsDate(rptSubmission.MDEffectiveDate) Then
                    effectiveDate = rptSubmission.MDEffectiveDate.Day & "-" & MonthName(rptSubmission.MDEffectiveDate.Month, True).ToLower & "-" & rptSubmission.MDEffectiveDate.Year
                    whereSql = " WHERE BEG_DT = '" & effectiveDate & "' "
                Else
                    Throw New Exception("MDReport:Invalid MD report request")
                End If
                ' ht, torrid dept filter
                If companyFilter > 0 Then
                    Select Case companyFilter
                        Case 1 ' Hot Topic
                            whereSql &= " AND DEPT.DIV_CD = 1"
                        Case 2 ' Torrid
                            whereSql &= " AND DEPT.DIV_CD = 5"
                    End Select
                End If

                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@CompanyFilter", SqlDbType.Int)
                arParms(0).Value = companyFilter
                storeDs = SQLDataManager.GetInstance().GetDataSet("dbo.RSP_ListStores", arParms)
                '" WHERE BEG_DT = '" & effectiveDate & "' AND pc_tp ='" & pc_tp & "' " & prcSql & _
                'Get MD Dataset
                ' MOD #10/04/2004 Exclude layaway inventory (layaway loc_cd is 'HOLD') from MD report
                strSql = "SELECT PRC.SKU_NUM,PRC.ITM_CD,PRC.RET_PRC,PRC.PREV_PERM_RET, PRC.PC_NUM, PRC.BEG_DT " & _
                        " ,GM_ITM.DEPT_CD,CLASS.CLASS_CD,CLASS.DES AS CLASS_DESC,to_number(INV.STORE_CD) AS STORE_CD" & _
                        " ,INV.OH_QTY, GM_ITM.DES1 AS ITEM_DESC,GM_SKU.SIZE_CD, VE_NAME, LPAD(AP.VE.VE_CD,7) AS VE_CD" & _
                        " FROM GM_PRC PRC, CLASS, GM_INV_LOC_V INV, GM_ITM, GM_SKU, AP.VE, DEPT" & _
                        whereSql & " AND pc_tp ='" & pc_tp & "' " & prcSql & _
                        " AND PC_NUM = (SELECT MAX(PC_NUM) FROM GM_PRC t1 WHERE PRC.SKU_NUM = t1.SKU_NUM AND PRC.ITM_CD = t1.ITM_CD)" & _
                        " AND PRC.ITM_CD = GM_ITM.ITM_CD " & _
                        " AND GM_ITM.CLASS_CD = CLASS.CLASS_CD" & _
                        " AND GM_ITM.DEPT_CD = DEPT.DEPT_CD " & _
                        " AND INV.SKU_NUM = PRC.SKU_NUM AND INV.OH_QTY > 0 AND loc_cd <> 'HOLD' " & _
                        " AND GM_SKU.SKU_NUM = PRC.SKU_NUM" & _
                        " AND GM_ITM.VE_CD = AP.VE.VE_CD" & _
                        " ORDER BY STORE_CD,GM_ITM.DEPT_CD,CLASS.CLASS_CD,VE_CD,PRC.ITM_CD,PRC.SKU_NUM"
                mdDs = Cn.ExecuteQuery(EnumRSTypes.DataSet, strSql)
                'Throw New Exception(strSql)
                Cn.CloseConnection()
                If mdDs.Tables(0).Rows.Count = 0 Then
                    Throw New Exception("MDReport:No records found")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub DeleteReportSubmissionById(ByVal rptSubmissionId As Integer)
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@reportSubmissionId", SqlDbType.Int)
                arParms(0).Value = rptSubmissionId
                SQLDataManager.GetInstance().Execute("dbo.RSP_DeleteReportSubmissionById", arParms)
            Catch ex As Exception

            End Try
        End Sub
        'SRI 8/19/03 Update Submission Comment
        Public Shared Sub UpdateReportSubmissionComment(ByVal rptSubmission As Submission, ByVal userID As String)
            Try
                ' Set report parameters (3 input) 
                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                With rptSubmission
                    arParms(0) = New SqlParameter("@ReportSubmissionID", SqlDbType.Int)
                    arParms(0).Value = .ReportSubmissionId
                    arParms(1) = New SqlParameter("@UserId", SqlDbType.VarChar, 30)
                    arParms(1).Value = userID
                    arParms(2) = New SqlParameter("@Comment", SqlDbType.VarChar, 2000)
                    arParms(2).Value = .Comment
                End With
                SQLDataManager.GetInstance().Execute("dbo.RSP_UpdateSubmissionComment", arParms)

            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Public Shared Sub SaveJobCodeOwners(ByVal reportId As Integer, ByVal companyId As Integer, ByVal jobCode As String, ByVal userID As String, Optional ByVal oldJobCode As String = "")
            Try
                'oldJobCode is required for updates
                Dim arParms() As SqlParameter = New SqlParameter(4) {}
                arParms(0) = New SqlParameter("@reportId", SqlDbType.Int)
                arParms(0).Value = reportId
                arParms(1) = New SqlParameter("@companyID", SqlDbType.Int)
                arParms(1).Value = companyId
                arParms(2) = New SqlParameter("@jobcode", SqlDbType.VarChar, 30)
                arParms(2).Value = jobCode
                arParms(3) = New SqlParameter("@UserId", SqlDbType.VarChar, 30)
                arParms(3).Value = userID
                arParms(4) = New SqlParameter("@oldJobcode", SqlDbType.VarChar, 30)
                arParms(4).Value = oldJobCode

                SQLDataManager.GetInstance().Execute("dbo.RSP_AddJobCodeOwner", arParms)
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        Public Shared Sub DeleteJobCodeOwner(ByVal reportId As Integer, ByVal companyId As Integer, ByVal jobCode As String)
            Try
                'oldJobCode is required for updates
                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                arParms(0) = New SqlParameter("@reportId", SqlDbType.Int)
                arParms(0).Value = reportId
                arParms(1) = New SqlParameter("@companyID", SqlDbType.Int)
                arParms(1).Value = companyId
                arParms(2) = New SqlParameter("@jobcode", SqlDbType.VarChar, 30)
                arParms(2).Value = jobCode
                SQLDataManager.GetInstance().Execute("dbo.RSP_DeleteJobCodeOwner", arParms)
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        Public Shared Sub DeleteSubCompanyOwners(ByVal reportId As Integer)
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@ReportId", SqlDbType.Int)
                arParms(0).Value = reportId
                SQLDataManager.GetInstance().Execute("RSP_DeleteSubCompanyOwnersByReport", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub SaveApprovers(ByVal reportId As Integer, ByVal approvers As ArrayList, ByVal userID As String)

            'delimApprovers is a "/" seperated list of approvers
            Dim delimApprovers As String
            Dim app As Approver

            For Each app In approvers
                If Len(delimApprovers) > 0 Then
                    delimApprovers += "/"
                End If
                delimApprovers += app.EmployeeId
            Next

            ' Set report parameters (3 input) 
            Try
                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                arParms(0) = New SqlParameter("@reportId", SqlDbType.Int)
                arParms(0).Value = reportId
                arParms(1) = New SqlParameter("@approverStr", SqlDbType.VarChar, 2000)
                arParms(1).Value = delimApprovers
                arParms(2) = New SqlParameter("@SubmitBy", SqlDbType.VarChar, 30)
                arParms(2).Value = userID

                SQLDataManager.GetInstance().Execute("dbo.RSP_AddApprovers", arParms)
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        Public Shared Function SaveSubCompanyOwners(ByVal reportId As Integer, ByVal SubCompanyOwners As ArrayList, ByVal userID As String)
            Try
                'Delete existing sub co.owners
                DeleteSubCompanyOwners(reportId)

                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                Dim subCoOwner As SubCompanyOwner
                For Each subCoOwner In SubCompanyOwners

                    arParms(0) = New SqlParameter("@ReportId", SqlDbType.Int)
                    arParms(0).Value = reportId
                    arParms(1) = New SqlParameter("@CompanyId", SqlDbType.Int)
                    arParms(1).Value = subCoOwner.CompanyId
                    arParms(2) = New SqlParameter("@OwnerId", SqlDbType.VarChar, 10)
                    arParms(2).Value = subCoOwner.OwnerID
                    SQLDataManager.GetInstance().Execute("RSP_AddSubCompanyOwner", arParms)
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetAllApprovers() As DataSet
            Dim ds As DataSet = SQLSecurityManager.GetInstance().GetDataSet("ssp_GetAllApprovers")
            Return ds
        End Function
        Public Shared Function ListDmStores(ByVal EmployeeID As String, ByVal UserRoleID As Integer) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(1) {}
                arParms(0) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 20)
                arParms(0).Value = EmployeeID
                arParms(1) = New SqlParameter("@UserRoleID", SqlDbType.Int)
                arParms(1).Value = UserRoleID
                Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_GetDMStores", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListAllActiveStores(ByVal CompanyID As Int16) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@CompanyID", SqlDbType.Int)
                arParms(0).Value = CompanyID '1-HT 2 - torrid

                Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_GetAllActiveStores", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListMLUDistricts(ByVal EmployeeID As String) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 20)
                arParms(0).Value = EmployeeID

                Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_GetMLUDistricts", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListAllSubCompanyOwners(ByVal ReportId As Integer) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@ReportId", SqlDbType.Int)
                arParms(0).Value = ReportId
                Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_GetAllSubCompanyOwners", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Shared Function GetAllReportTitles(Optional ByVal inputType As FileInputType = 0) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@inputType", SqlDbType.SmallInt)
                arParms(0).Value = UtilityManager.IntegerToNull(inputType)

                Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("dbo.RSP_GetReportTitles", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class ' END CLASS DEFINITION ReportManager

End Namespace ' HotTopic.RD.Reports

