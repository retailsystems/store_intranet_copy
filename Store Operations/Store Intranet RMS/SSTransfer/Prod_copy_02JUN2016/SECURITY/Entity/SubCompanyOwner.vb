' Static Model

Namespace HotTopic.RD.Entity

    Public Class SubCompanyOwner
        Private _ownerId As String
        Private _ownerName As String
        Private _companyId As Int32
        Private _companyName As String
        Public Property CompanyId() As Long
            Get
                Return _companyId
            End Get

            Set(ByVal Value As Long)
                _companyId = Value
            End Set
        End Property

        Public Property OwnerID() As String
            Get
                Return _ownerId
            End Get

            Set(ByVal Value As String)
                _ownerId = Value
            End Set
        End Property

        Public Property OwnerName() As String
            Get
                Return _ownerName
            End Get

            Set(ByVal Value As String)
                _ownerName = Value
            End Set
        End Property
        Public Property CompanyName() As String
            Get
                Return _companyName
            End Get

            Set(ByVal Value As String)
                _companyName = Value
            End Set
        End Property
    End Class
End Namespace ' HotTopic.RD.Entity