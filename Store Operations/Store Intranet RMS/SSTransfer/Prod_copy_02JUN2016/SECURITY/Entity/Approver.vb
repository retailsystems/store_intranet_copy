' Static Model
Namespace HotTopic.RD.Entity


    Public Class Approver

        Private _employeeId As String
        Private _lastName As String
        Private _firstName As String
        Private _routeOrder As Int16
        Private _emailAddress As String

        Public Sub New(ByVal employeeId As String)
            _employeeId = employeeId
        End Sub

        Public ReadOnly Property EmployeeId() As String
            Get
                Return _employeeId
            End Get
        End Property

        Public Property LastName() As String
            Get
                Return _lastName
            End Get

            Set(ByVal Value As String)
                _lastName = Value
            End Set
        End Property

        Public Property FirstName() As String
            Get
                Return _firstName
            End Get

            Set(ByVal Value As String)
                _firstName = Value
            End Set
        End Property

        Public Property RouteOrder() As Int16
            Get
                Return _routeOrder
            End Get

            Set(ByVal Value As Int16)
                _routeOrder = Value
            End Set
        End Property

        Public Property EmailAddress() As String
            Get
                Return _emailAddress
            End Get

            Set(ByVal Value As String)
                _emailAddress = Value
            End Set
        End Property

    End Class ' END CLASS DEFINITION Approver

End Namespace ' HotTopic.RD.Entity

