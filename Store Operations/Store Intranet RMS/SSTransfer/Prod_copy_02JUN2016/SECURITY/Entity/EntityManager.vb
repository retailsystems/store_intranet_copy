Imports System.Data.SqlClient

Imports HotTopic.RD.Services

Namespace HotTopic.RD.Entity
    Public Class EntityManager

        Public Shared Sub GetReportSubmissionById(ByRef submission As Submission)
            Dim dr As SqlDataReader
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@ReportSubmissionId", SqlDbType.Int)
                arParms(0).Value = submission.ReportSubmissionId
                dr = SQLDataManager.GetInstance().GetDataReader("RSP_GetReportSubmissionById", arParms)
                While dr.Read

                    With submission
                        .ReportId = dr.GetInt32(1)
                        .Title = dr.GetString(2)
                        .FiscalYear = dr.GetInt16(3)
                        .FiscalWeek = dr.GetInt16(4)
                        .ReportDate = dr.GetDateTime(5)
                        .StagingPath = dr.GetString(6)
                        .LocationPath = dr.GetString(7)
                        .EffectivityStartDate = dr.GetDateTime(8)
                        If Not dr.IsDBNull(9) Then
                            .EffectivityEndDate = dr.GetDateTime(9)
                        End If
                        If Not dr.IsDBNull(10) Then
                            .Comment = dr.GetString(10)
                        End If
                        .Status = dr.GetInt16(11)
                        .FiscalWeekEndingDate = dr.GetDateTime(12)
                        If Not dr.IsDBNull(13) Then
                            .MDReportType = dr.GetInt16(13)
                        End If
                        If Not dr.IsDBNull(14) Then
                            .MDEffectiveDate = dr.GetDateTime(14)
                        End If
                        If Not dr.IsDBNull(15) Then
                            .MDPcNum = dr.GetString(15)
                        End If
                    End With
                End While

            Catch ex As Exception
                ' exception handling code.
                Throw ex
            Finally
                If Not dr Is Nothing Then
                    dr.Close()
                End If
            End Try
        End Sub

        Public Shared Sub GetReportById(ByRef rpt As Report, Optional ByVal IsDeep As Boolean = False)
            If IsDeep Then
                DeepGetReportById(rpt)
            Else
                ShallowGetReportById(rpt)

            End If

        End Sub

        Private Shared Function ShallowGetReportById(ByRef rpt As Report) As Report
            Dim dr As SqlDataReader
            Try

                Dim arParms() As SqlParameter = New SqlParameter(1) {}
                arParms(0) = New SqlParameter("@reportId", SqlDbType.Int)
                arParms(0).Value = rpt.reportId
                arParms(1) = New SqlParameter("@reportOnly", SqlDbType.Bit)
                arParms(1).Value = True

                dr = SQLDataManager.GetInstance().GetDataReader("RSP_GetReportById", arParms)
                'Dim rpt As Report
                While dr.Read
                    'rpt = New Report()
                    With rpt
                        '.ReportId = dr.GetInt32(0)
                        .Title = dr.GetString(1)
                        If Not dr.IsDBNull(2) Then
                            .Description = dr.GetString(2)
                        End If
                        .InputType = dr.GetInt16(3)
                        .IsFolderFlag = dr.GetBoolean(4)
                        .StagingPath = dr.GetString(5)
                        .LocationPath = dr.GetString(6)
                        .ContentScope = dr.GetInt16(7)
                        If Not dr.IsDBNull(8) Then
                            .DefaultFileName = dr.GetString(8)
                        End If
                        If Not dr.IsDBNull(9) Then
                            .DefaultFileIdentifier = dr.GetString(9)
                        End If
                        If Not dr.IsDBNull(10) Then
                            .FileFormat = dr.GetString(10)
                        End If
                        .ExpirationType = dr.GetInt16(11)
                        If Not dr.IsDBNull(12) Then
                            .ArchiveNth = dr.GetInt32(12)
                        End If
                        If Not dr.IsDBNull(13) Then
                            .MaxFileSize = dr.GetInt32(13)
                        End If
                        If Not dr.IsDBNull(14) Then
                            .SortOrder = dr.GetInt32(14)
                        End If
                        .ApproverGroupEmail = dr.GetString(15)
                        .ApprovalType = dr.GetInt16(16)
                        .ReadOnlyFlag = dr.GetBoolean(17)
                        .AllowMultiFlag = dr.GetBoolean(23)
                    End With
                End While
                Return rpt
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            Finally
                If Not dr Is Nothing Then
                    dr.Close()
                End If
            End Try
        End Function

        Private Shared Function DeepGetReportById(ByRef rpt As Report) As Report
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@reportId", SqlDbType.Int)
                arParms(0).Value = rpt.ReportId
                Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("dbo.RSP_GetReportByID", arParms)
                If ds.Tables(0).Rows.Count = 0 Then
                    Return Nothing
                End If
                'instantiate objects
                'Dim rpt As Report = New Report()
                With rpt
                    .ReportId = Convert.ToInt32(ds.Tables(0).Rows(0)("ReportId"))
                    .Title = Convert.ToString(ds.Tables(0).Rows(0)("ReportTitle"))
                    .Description = UtilityManager.NullToString(ds.Tables(0).Rows(0)("ReportDesc"))
                    .InputType = Convert.ToInt16(ds.Tables(0).Rows(0)("InputType"))
                    .IsFolderFlag = Convert.ToBoolean(ds.Tables(0).Rows(0)("IsFolderFlag"))
                    .StagingPath = Convert.ToString(ds.Tables(0).Rows(0)("StagingPath"))
                    .LocationPath = Convert.ToString(ds.Tables(0).Rows(0)("LocationPath"))
                    .ContentScope = UtilityManager.NullToInteger(ds.Tables(0).Rows(0)("ContentScopeType"))
                    .DefaultFileName = UtilityManager.NullToString(ds.Tables(0).Rows(0)("DefaultFileName"))
                    .DefaultFileIdentifier = UtilityManager.NullToString(ds.Tables(0).Rows(0)("DefaultFileIdentifier"))
                    .FileFormat = UtilityManager.NullToString(ds.Tables(0).Rows(0)("FileFormat"))
                    .ExpirationType = Convert.ToInt16(ds.Tables(0).Rows(0)("ExpirationType"))
                    .ArchiveNth = UtilityManager.NullToInteger(ds.Tables(0).Rows(0)("ArchiveNth"))
                    .MaxFileSize = UtilityManager.NullToInteger(ds.Tables(0).Rows(0)("MaxFileSize"))
                    .SortOrder = UtilityManager.NullToInteger(ds.Tables(0).Rows(0)("SortOrder"))
                    .ApproverGroupEmail = UtilityManager.NullToString(ds.Tables(0).Rows(0)("ApproverGroupEmail"))
                    .ApprovalType = Convert.ToInt16(ds.Tables(0).Rows(0)("ApprovalType"))
                    .ReadOnlyFlag = Convert.ToBoolean(ds.Tables(0).Rows(0)("ReadOnlyFlag"))
                    .AllowMultiFlag = Convert.ToBoolean(ds.Tables(0).Rows(0)("AllowMultiFlag"))
                End With
                'check for companyowners
                Dim coOwners As ArrayList = New ArrayList(ds.Tables(1).Rows.Count)
                If ds.Tables(1).Rows.Count > 0 Then
                    Dim row As DataRow
                    For Each row In ds.Tables(1).Rows
                        Dim coOwner As CompanyOwner = New CompanyOwner()
                        With coOwner
                            .CompanyId = Convert.ToInt32(row("CompanyId"))
                            .CompanyName = Convert.ToString(row("CompanyName"))
                            .CDStoresFlag = Convert.ToBoolean(row("CDStoresFlag"))
                        End With
                        coOwners.Add(coOwner)
                    Next
                End If
                rpt.CompanyOwners = coOwners
                'check for approvers
                Dim approvers As ArrayList = New ArrayList(ds.Tables(2).Rows.Count)
                If ds.Tables(2).Rows.Count > 0 Then
                    Dim row As DataRow
                    For Each row In ds.Tables(2).Rows
                        Dim app As Approver = New Approver(Convert.ToString(row("EmployeeId")))
                        With app
                            .FirstName = UtilityManager.NullToString(row("FirstName"))
                            .LastName = UtilityManager.NullToString(row("LastName"))
                            .RouteOrder = UtilityManager.NullToInteger(row("RouteOrder"))
                            .EmailAddress = row("EmailAddress")
                        End With
                        approvers.Add(app)
                    Next
                End If
                rpt.Approvers = approvers
                'check for jobcodeowners
                Dim jcOwners As ArrayList = New ArrayList(ds.Tables(3).Rows.Count)
                If ds.Tables(3).Rows.Count > 0 Then
                    Dim row As DataRow
                    For Each row In ds.Tables(3).Rows
                        Dim jcOwner As JobCodeOwner = New JobCodeOwner()
                        With jcOwner
                            .CompanyId = Convert.ToInt32(row("CompanyId"))
                            .JobCode = Convert.ToString(row("jobcode"))
                            .CompanyName = Convert.ToString(row("CompanyName"))
                        End With
                        jcOwners.Add(jcOwner)
                    Next
                End If
                rpt.JobCodeOwners = jcOwners
                '7/21/2003
                'check for companySubOwners
                Dim subCoOwners As ArrayList = New ArrayList(ds.Tables(4).Rows.Count)
                If ds.Tables(4).Rows.Count > 0 Then
                    Dim row As DataRow
                    For Each row In ds.Tables(4).Rows
                        Dim subCoOwner As SubCompanyOwner = New SubCompanyOwner()
                        With subCoOwner
                            .CompanyId = Convert.ToInt32(row("CompanyId"))
                            .OwnerID = Convert.ToString(row("ownerID"))
                            .OwnerName = Convert.ToString(row("OwnerName"))
                            .CompanyName = Convert.ToString(row("CompanyName"))
                        End With
                        subCoOwners.Add(subCoOwner)
                    Next
                End If
                rpt.CompanySubOwners = subCoOwners
                Return rpt
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try

        End Function
        'Private Shared Function GetReportData(ByVal reportId As Int32) As SqlDataReader
        'End Function
    End Class

    Public Enum ReportStatus
        Pending = 1
        Approved
        Declined
        Archived
        Cancelled
    End Enum

    Public Enum ReportApprovalType
        None = 1
        One
        All
    End Enum

    Public Enum FileIdentifier
        StoreID = 4
        DistrictID = 3
        RegionID = 2
    End Enum

    Public Enum ContentScopeType
        Company = 1
        Region
        District
        Store
    End Enum
    Public Enum StoreOwner
        HotTopic = 1
        Torrid
        CDStores
        AllStores
    End Enum
    Public Enum FileInputType
        System = 1
        Upload
        MDReport
    End Enum
    Public Enum UserRole
        Admin = 1
        Approver = 2
        HQ = 3
        DM = 4
        Store = 5
        RM = 6
        MLU = 12 '5/23/04 New role market level users
    End Enum
    Public Enum ExpType
        ExpDate = 1
        NthReport
    End Enum
    Public Enum MDReportType
        Regular = 1
        Further = 2
        MarkUp = 3
        MDCancel = 4
        MUCancel = 5
        MOS = 6 'mark out of stock
        MDAll = 7 'all mark downs
    End Enum
End Namespace

