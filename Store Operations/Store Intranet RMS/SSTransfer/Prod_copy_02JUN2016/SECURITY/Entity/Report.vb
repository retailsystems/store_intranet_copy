' Static Model

Namespace HotTopic.RD.Entity


    Public Class Report

        Private _jobCodeOwners As System.Collections.ArrayList
        Private _approvers As System.Collections.ArrayList
        Private _coOwners As System.Collections.ArrayList
        Private _coSubOwners As System.Collections.ArrayList
        Private _reportId As Integer
        Private _title As String
        Private _description As String
        Private _inputType As FileInputType
        Private _isFolderFlag As Boolean
        Private _defaultFileName As String
        Private _defaultFileIdentifier As Integer
        Private _maxFileSize As Int32
        Private _fileFormat As String
        Private _sortOrder As Integer
        Private _expirationType As ExpType
        Private _archiveNth As Integer
        Private _approverGroupEmail As String
        Private _approvalType As ReportApprovalType
        Private _readOnlyFlag As Boolean
        Protected _stagingPath As String
        Protected _locationPath As String
        Protected _contentScope As ContentScopeType
        Private _allowMultiFlag As Boolean

        Public Sub New()

        End Sub

        Public Sub New(ByVal reportId As Int32, Optional ByVal IsDeep As Boolean = False)
            _reportId = reportId
            EntityManager.GetReportById(Me, IsDeep)
        End Sub

        Public Property ReportId() As Int32
            Get
                Return _reportId
            End Get

            Set(ByVal Value As Int32)
                _reportId = Value
            End Set
        End Property

        Public Property CompanyOwners() As ArrayList
            Get
                Return _coOwners
            End Get
            Set(ByVal Value As ArrayList)
                _coOwners = Value
            End Set
        End Property

        Public Property Approvers() As ArrayList
            Get
                Return _approvers
            End Get
            Set(ByVal Value As ArrayList)
                _approvers = Value
            End Set
        End Property

        Public Property JobCodeOwners() As ArrayList
            Get
                Return _jobCodeOwners
            End Get
            Set(ByVal Value As ArrayList)
                _jobCodeOwners = Value
            End Set
        End Property
        Public Property CompanySubOwners() As ArrayList
            Get
                Return _coSubOwners
            End Get
            Set(ByVal Value As ArrayList)
                _coSubOwners = Value
            End Set
        End Property
        Public Property Title() As String
            Get
                Return _title
            End Get

            Set(ByVal Value As String)
                _title = Value
            End Set
        End Property

        Public Property Description() As String
            Get
                Return _description
            End Get

            Set(ByVal Value As String)
                _description = Value
            End Set
        End Property

        Public Property InputType() As FileInputType
            Get
                Return _inputType
            End Get

            Set(ByVal Value As FileInputType)
                _inputType = Value
            End Set
        End Property

        Public Property StagingPath() As String
            Get
                Return _stagingPath
            End Get

            Set(ByVal Value As String)
                If Not Value.EndsWith("\") Then
                    Value += "\"
                End If
                _stagingPath = Value
            End Set
        End Property

        Public Property LocationPath() As String
            Get
                Return _locationPath
            End Get

            Set(ByVal Value As String)
                If Not Value.EndsWith("\") Then
                    Value += "\"
                End If
                _locationPath = Value
            End Set
        End Property

        Public Property IsFolderFlag() As Boolean
            Get
                Return _isFolderFlag
            End Get

            Set(ByVal Value As Boolean)
                _isFolderFlag = Value
            End Set
        End Property

        Public Property DefaultFileName() As String
            Get
                Return _defaultFileName
            End Get

            Set(ByVal Value As String)
                _defaultFileName = Value
            End Set
        End Property

        Public Property DefaultFileIdentifier() As Integer
            Get
                Return _defaultFileIdentifier
            End Get

            Set(ByVal Value As Integer)
                _defaultFileIdentifier = Value
            End Set
        End Property

        Public Property MaxFileSize() As Int32
            Get
                Return _maxFileSize
            End Get

            Set(ByVal Value As Int32)
                _maxFileSize = Value
            End Set
        End Property

        Public Property FileFormat() As String
            Get
                Return _fileFormat
            End Get

            Set(ByVal Value As String)
                _fileFormat = Value
            End Set
        End Property

        Public Property SortOrder() As Integer
            Get
                Return _sortOrder
            End Get

            Set(ByVal Value As Integer)
                _sortOrder = Value
            End Set
        End Property

        Public Property ExpirationType() As ExpType
            Get
                Return _expirationType
            End Get

            Set(ByVal Value As ExpType)
                _expirationType = Value
            End Set
        End Property

        Public Property ArchiveNth() As Integer
            Get
                Return _archiveNth
            End Get

            Set(ByVal Value As Integer)
                _archiveNth = Value
            End Set
        End Property

        Public Property ApproverGroupEmail() As String
            Get
                Return _approverGroupEmail
            End Get

            Set(ByVal Value As String)
                _approverGroupEmail = Value
            End Set
        End Property

        Public Property ApprovalType() As ReportApprovalType
            Get
                Return _approvalType
            End Get

            Set(ByVal Value As ReportApprovalType)
                _approvalType = Value
            End Set
        End Property

        Public Property ReadOnlyFlag() As Boolean
            Get
                Return _readOnlyFlag
            End Get

            Set(ByVal Value As Boolean)
                _readOnlyFlag = Value
            End Set
        End Property

        Public Property AllowMultiFlag() As Boolean
            Get
                Return _allowMultiFlag
            End Get

            Set(ByVal Value As Boolean)
                _allowMultiFlag = Value
            End Set
        End Property

        Public Property ContentScope() As ContentScopeType
            Get
                Return _contentScope
            End Get

            Set(ByVal Value As ContentScopeType)
                _contentScope = Value
            End Set
        End Property

    End Class ' END CLASS DEFINITION Report

End Namespace ' HotTopic.RD.Entity

