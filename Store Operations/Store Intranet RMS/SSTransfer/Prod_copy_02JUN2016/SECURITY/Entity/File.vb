' Static Model

Namespace HotTopic.RD.Entity


    Public Class File
        Inherits Submission

        Private _rptFileId As Int32
        Private _fileName As String
        Private _archivedFlag As Boolean
        Private _ownerId As String

        Public Sub New()

        End Sub

        Public Sub New(ByVal rptSubmission As Submission)
            MyBase.New(rptSubmission)
        End Sub

        Public Property ReportFileId() As Int32
            Get
                Return _rptFileId
            End Get

            Set(ByVal Value As Int32)
                _rptFileId = Value
            End Set
        End Property

        Public Property OwnerId() As String
            Get
                Return _ownerId
            End Get
            Set(ByVal Value As String)
                _ownerId = Value
            End Set
        End Property

        Public Shadows Property ContentScope() As ContentScopeType
            Get
                Return _contentScope
            End Get
            Set(ByVal Value As ContentScopeType)
                _contentScope = Value
            End Set
        End Property

        Public Property FileName() As String
            Get
                Return _fileName
            End Get

            Set(ByVal Value As String)
                _fileName = Value
            End Set
        End Property

        Public Property ArchivedFlag() As Boolean
            Get
                Return _archivedFlag
            End Get

            Set(ByVal Value As Boolean)
                _archivedFlag = Value
            End Set
        End Property

    End Class ' END CLASS DEFINITION File

End Namespace ' HotTopic.RD.Entity

