' Static Model

Namespace HotTopic.RD.Entity


    Public Class Submission
        Inherits Report

        Private _submissionId As Int32
        Private _fiscalYear As Int16
        Private _fiscalWeek As Int16
        Private _reportDate As Date
        Private _fiscalWeekEndDate As Date
        Private _effStartDate As Date
        Private _effEndDate As Date
        Private _comment As String
        Private _status As ReportStatus
        Private _newFlag As Boolean
        Private _fileSubmitted As SubmittedFile
        Private _mDReportType As MDReportType
        Private _mdEffectiveDate As Date
        Private _mdPcNum As String

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal reportId As Int32, Optional ByVal IsDeep As Boolean = False)
            MyBase.New(reportId, IsDeep)
        End Sub

        Public Sub New(ByVal reportId As Int32, ByVal submisionId As Int32, Optional ByVal IsDeep As Boolean = False)
            MyBase.New(reportId, IsDeep)
            _submissionId = submisionId
            EntityManager.GetReportSubmissionById(Me)
        End Sub

        Public Sub New(ByVal rptSubmission As Submission)
            Initialize(rptSubmission)
        End Sub

        Public Property ReportSubmissionId() As Int32
            Get
                Return _submissionId
            End Get

            Set(ByVal Value As Int32)
                _submissionId = Value
            End Set
        End Property

        Public Property FiscalYear() As Int16
            Get
                Return _fiscalYear
            End Get

            Set(ByVal Value As Int16)
                _fiscalYear = Value
            End Set
        End Property

        Public Property FiscalWeek() As Int16
            Get
                Return _fiscalWeek
            End Get

            Set(ByVal Value As Int16)
                _fiscalWeek = Value
            End Set
        End Property

        Public Property ReportDate() As Date
            Get
                Return _reportDate
            End Get

            Set(ByVal Value As Date)
                _reportDate = Value
            End Set
        End Property

        Public Property FiscalWeekEndingDate() As Date
            Get
                Return _fiscalWeekEndDate
            End Get

            Set(ByVal Value As Date)
                _fiscalWeekEndDate = Value
            End Set
        End Property

        Public Shadows Property StagingPath() As String
            Get
                Return _stagingPath
            End Get

            Set(ByVal Value As String)
                If Not Value.EndsWith("\") Then
                    Value += "\"
                End If
                _stagingPath = Value
            End Set
        End Property

        Public Shadows Property LocationPath() As String
            Get
                Return _locationPath
            End Get

            Set(ByVal Value As String)
                If Not Value.EndsWith("\") Then
                    Value += "\"
                End If
                _locationPath = Value
            End Set
        End Property

        Public Property EffectivityStartDate() As Date
            Get
                Return _effStartDate
            End Get

            Set(ByVal Value As Date)
                _effStartDate = Value
            End Set
        End Property

        Public Property EffectivityEndDate() As Date
            Get
                Return _effEndDate
            End Get

            Set(ByVal Value As Date)
                _effEndDate = Value
            End Set
        End Property

        Public Property Comment() As String
            Get
                Return _comment
            End Get

            Set(ByVal Value As String)
                _comment = Value
            End Set
        End Property

        Public Property Status() As ReportStatus
            Get
                Return _status
            End Get

            Set(ByVal Value As ReportStatus)
                _status = Value
            End Set
        End Property

        Public Property NewFlag() As Boolean
            Get
                Return _newFlag
            End Get

            Set(ByVal Value As Boolean)
                _newFlag = Value
            End Set
        End Property

        Public Property FileSubmitted() As SubmittedFile
            Get
                Return _fileSubmitted
            End Get
            Set(ByVal Value As SubmittedFile)
                _fileSubmitted = Value
            End Set
        End Property
        Public Property MDReportType() As MDReportType
            Get
                Return _mDReportType
            End Get

            Set(ByVal Value As MDReportType)
                _mDReportType = Value
            End Set
        End Property

        Public Property MDEffectiveDate() As Date
            Get
                Return _mdEffectiveDate
            End Get

            Set(ByVal Value As Date)
                _mdEffectiveDate = Value
            End Set
        End Property
        Public Property MDPcNum() As String
            Get
                Return _mdPcNum
            End Get

            Set(ByVal Value As String)
                _mdPcNum = Value
            End Set
        End Property


        Private Sub Initialize(ByVal rptSubmission As Submission)
            Me.ApprovalType = rptSubmission.ApprovalType
            Me.ApproverGroupEmail = rptSubmission.ApproverGroupEmail
            Me.Approvers = rptSubmission.Approvers
            Me.ArchiveNth = rptSubmission.ArchiveNth
            Me.Comment = rptSubmission.Comment
            Me.CompanyOwners = rptSubmission.CompanyOwners
            Me.ContentScope = rptSubmission.ContentScope
            Me.DefaultFileIdentifier = rptSubmission.DefaultFileIdentifier
            Me.DefaultFileName = rptSubmission.DefaultFileName
            Me.Description = rptSubmission.Description
            Me.EffectivityEndDate = rptSubmission.EffectivityEndDate
            Me.EffectivityStartDate = rptSubmission.EffectivityStartDate
            Me.ExpirationType = rptSubmission.ExpirationType
            Me.FileFormat = rptSubmission.FileFormat
            Me.FiscalWeek = rptSubmission.FiscalWeek
            Me.FiscalYear = rptSubmission.FiscalYear
            Me.InputType = rptSubmission.InputType
            Me.IsFolderFlag = rptSubmission.IsFolderFlag
            Me.JobCodeOwners = rptSubmission.JobCodeOwners
            Me.LocationPath = rptSubmission.LocationPath
            Me.MaxFileSize = rptSubmission.MaxFileSize
            Me.NewFlag = rptSubmission.NewFlag
            Me.ReadOnlyFlag = rptSubmission.ReadOnlyFlag
            Me.ReportDate = rptSubmission.ReportDate
            Me.ReportId = rptSubmission.ReportId
            Me.ReportSubmissionId = rptSubmission.ReportSubmissionId
            Me.SortOrder = rptSubmission.SortOrder
            Me.StagingPath = rptSubmission.StagingPath
            Me.Status = rptSubmission.Status
            Me.Title = rptSubmission.Title
            Me.MDReportType = rptSubmission.MDReportType
            Me.MDEffectiveDate = rptSubmission.MDEffectiveDate
            'TODO: other inherited attributes from Report...
        End Sub

    End Class ' END CLASS DEFINITION Submission

    Public Structure SubmittedFile
        Public Name As String
        Public Length As Int32
        Public FileStream As System.IO.Stream

    End Structure
End Namespace ' HotTopic.RD.Entity

