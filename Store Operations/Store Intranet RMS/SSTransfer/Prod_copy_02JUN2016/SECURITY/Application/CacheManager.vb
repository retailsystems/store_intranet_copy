' Static Model
Imports System.Web
Imports System.Web.Caching

Imports HotTopic.RD.Entity
Imports HotTopic.RD.Services
Imports HotTopic.RD.Security
Imports System.Data.SqlClient
Namespace HotTopic.RD.Application



	' Responsible for getting and caching lookup data
	Public Class CacheManager

        Private _context As HttpContext

        Friend Sub New(ByVal context As HttpContext)
            _context = context
        End Sub

        'dependency - i.e. file that lookup data will depend on re refreshing
        Public Function ListLookups(ByVal dependency As String) As DataSet
            Dim lookUpDs As DataSet

            If IsNothing(_context.Cache("LookUps")) Then
                lookUpDs = SQLDataManager.GetInstance().GetDataSet("RSP_GetLookUpData")
                '_context.Cache("LookUps") = lookUpDs
                _context.Cache.Insert("LookUps", lookUpDs, _
                    New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
            End If
            lookUpDs = _context.Cache("LookUps")

            Return lookUpDs

        End Function
        Public Function ListMDReportTypes(ByVal dependency As String) As DataSet
            Dim MDReportTypeDs As DataSet

            If IsNothing(_context.Cache("MDReportTypes")) Then
                MDReportTypeDs = SQLDataManager.GetInstance().GetDataSet("RSP_GetMdReportTypeData")
                '_context.Cache("LookUps") = lookUpDs
                _context.Cache.Insert("MDReportTypes", MDReportTypeDs, _
                    New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
            End If
            MDReportTypeDs = _context.Cache("MDReportTypes")

            Return MDReportTypeDs

        End Function

        Public Function GetMenu(ByVal dependency As String, ByVal userRoleId As Int16, ByVal moduleId As Int16) As String
            Dim controlType As Int16 = 1 'menu = 1?
            Dim dv As DataView = GetControlView(dependency, moduleId)
            dv.Sort = "UserRoleId, ControlType" 'for given role, give all Menu types
            'Dim foundRows As DataRowView() = dv.FindRows(New Object() {userRoleId, controlType})

            '***********************************************************************
            '                            MENU 
            ' Cache menuString per UserRole as reportdistro doesn't have page specific menu.
            '***********************************************************************
            Dim cacheName As String
            cacheName = "CachedMenu" & userRoleId
            If IsNothing(_context.Cache(cacheName)) Then
                'If IsNothing(_context.Session("MenuStr")) Then
                dv.RowFilter = "UserRoleId=" & userRoleId & " and ControlType=1 and ParentControlId =0"
                Dim menuHeader, menuTree As String
                menuHeader = "<table width='750' cellspacing='0' cellpadding='0' border='0' bgcolor='#246178' ><tr><td> " & _
                              "<table border='0' cellpadding='0' cellspacing='0' ID='Table5' class='white79'>" & _
                              "<tr><td bgcolor='#246178'></td>" & _
                              "<td align='center' bgcolor='#246178' class='white79' width='80' id='Main1' >" & _
                              "<a href='../Main/Main.aspx'   onFocus='this.blur();' style='text-decoration: none; color: #FFFFFF'>" & _
                              "<b>Main</b></a></td>"


                'TODO: loop thru found rows and build menu
                'build headerMenu
                Dim dvChild As DataView
                Dim dr, drChild As DataRow
                Dim drv, drvChild As DataRowView
                dvChild = dv
                For Each drv In dv
                    dr = drv.Row
                    dvChild.RowFilter = "UserRoleId=" & userRoleId & " and ControlType=1 and ParentControlId =" & dr("ControlID")
                    'hide the root node if no child nodes found
                    If dvChild.Count > 0 Then
                        menuHeader += "<td bgcolor='#000000' width=1></td>" & _
                                   "<td align='center' bgcolor='#246178' class='white79' width='130' id='MOD_" & dr("ControlId") & "' onmouseover=""setMenu('MOD_" & dr("ControlId") & "', 'MENU_" & dr("ControlId") & "')"">" & _
                                   "<a href='#'   onFocus='this.blur()' style='text-decoration: none; color: #FFFFFF'><b>" & dr("ControlName") & "</b></a></td><td bgcolor='#000000' width=1></td>"
                        'build treeNode
                        menuTree += "<table border='0' cellpadding=0  cellspacing=0  bgcolor=#548597 class='menu' id='MENU_" & dr("ControlId") & "' width='150'  onmouseout='hideMenu()' >" & _
                                    "<tr><td width=5></td><td><table border='0' cellpadding=0  cellspacing=0 width=100% >"
                        For Each drvChild In dvChild
                            drChild = drvChild.Row
                            menuTree += "<tr><td nowrap height=15 class='dl' id='td1' onmouseover=""this.style.background='#000000'"" onmouseout=""this.style.background='#548597'"">" & _
                                        "&nbsp;&nbsp;<a href='../" & drChild("ControlUrl") & "'   class='dl' title=''>" & drChild("ControlName") & "</a></td></tr>" & _
                                        "<tr><td><img src='../" & "Images/wspacer.gif' border='0' width='100%' height='1px'></td></tr>"

                        Next
                        menuTree += "</table></td><td width=5></td></tr></table>"
                    End If
                Next
                menuHeader += "</tr></table></td></tr></table>"
                _context.Cache.Insert(cacheName, menuHeader & menuTree, _
                   New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
                '_context.Session("MenuStr") = menuHeader & menuTree
            End If
            Return _context.Cache(cacheName)
            'Return menuHeader & menuTree
            'Return _context.Session("MenuStr")
            '***********************************************************************
            '                            MENU 
            '***********************************************************************
        End Function

        Public Function ListActions(ByVal dependency As String, ByVal userRoleId As Int16, ByVal moduleId As Int16) As DataRowView()
            Dim controlType As Int16 = 0 'non-menu = 0?
            Dim dv As DataView = GetControlView(dependency, moduleId)
            dv.Sort = "UserRoleId, ControlType" 'for given role, give all Menu types
            Return dv.FindRows(New Object() {userRoleId, controlType})
        End Function

        Public Function GetModuleId(ByVal dependency As String, ByVal userRoleId As Int16, ByVal projectId As Int16, ByVal moduleName As String) As Int16
            Dim dt As DataTable
            Dim cacheName As String = projectId.ToString() + "Modules"
            If IsNothing(_context.Cache(cacheName)) Then
                dt = SecurityManager.ListModulesByProjectId(projectId)
                _context.Cache.Insert(cacheName, dt, _
                    New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)


            Else
                dt = _context.Cache(cacheName)
            End If
            Dim dv As DataView = New DataView(dt)
            dv.Sort = "ModuleName, UserRoleId"
            Dim rowIndex As Int32 = dv.Find(New Object() {moduleName, userRoleId})
            If rowIndex = -1 Then
                Return 0 'no module or no access
            Else
                Return Convert.ToInt16(dv(rowIndex)("ModuleId"))
            End If
        End Function

        Private Function GetControlView(ByVal dependency As String, ByVal moduleId As Int16) As DataView
            Dim dt As DataTable
            Dim cacheName As String = moduleId.ToString() + "Controls"
            If IsNothing(_context.Cache(cacheName)) Then
                dt = SecurityManager.ListControlsByModuleId(moduleId)
                _context.Cache.Insert(cacheName, dt, _
                    New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
            Else
                dt = _context.Cache(cacheName)
            End If
            Dim dv As DataView = New DataView(dt)
            Return dv
        End Function
        Public Function GetFiscalMonthDateRange(ByVal dependency As String, ByRef fm As FiscalMonth)
            Try
                Dim cacheName As String = fm.MonthNum & "_" & fm.Year
                If IsNothing(_context.Cache(cacheName)) Then
                    Dim arParms() As SqlParameter = New SqlParameter(3) {}

                    arParms(0) = New SqlParameter("@MonthNum", SqlDbType.Int)
                    arParms(0).Value = fm.MonthNum
                    arParms(1) = New SqlParameter("@FiscalYear", SqlDbType.Int)
                    arParms(1).Value = fm.Year
                    arParms(2) = New SqlParameter("@BeginDate", SqlDbType.DateTime)
                    arParms(2).Direction = ParameterDirection.Output
                    arParms(3) = New SqlParameter("@EndDate", SqlDbType.DateTime)
                    arParms(3).Direction = ParameterDirection.Output
                    SQLDataManager.GetInstance().Execute("RSP_GetFiscalMonthBeginEndDates", arParms)

                    With fm
                        .BeginDate = arParms(2).Value
                        .EndDate = arParms(3).Value
                    End With
                    _context.Cache.Insert(cacheName, fm, _
                    New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
                Else
                    fm = _context.Cache(cacheName)
                End If
            Catch ex As SqlException
                Throw New RDAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'dependency - i.e. file that lookup data will depend on re refreshing
        Public Function ListUsersByRoleId(ByVal dependency As String, ByVal ProjectID As Int16) As DataTable
            Try
                Dim dt As New DataTable()

                If IsNothing(_context.Cache("UserDT")) Then
                    dt = SecurityManager.ListUsersByRoleId(UserRole.Admin, ProjectID)
                    _context.Cache.Insert("UserDT", dt, _
                        New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
                End If
                dt = _context.Cache("UserDT")

                Return dt
            Catch ex As SqlException
                Throw New RDAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class ' END CLASS DEFINITION CacheManager
    <Serializable()> _
    Public Structure FiscalMonth
        Public MonthNum As Int16
        Public Year As Int16
        Public BeginDate As Date
        Public EndDate As Date
    End Structure
End Namespace ' HotTopic.RD.Application

