' Static Model

Namespace HotTopic.RD.Services


	Public Class UtilityManager

		Public Shared Function InsertApos (ByVal InStr As String) As String
			
		End Function

        Public Shared Function NullToInteger(ByVal Value As Object) As Int32
            If Convert.IsDBNull(Value) Then
                Return 0
            Else
                Return Convert.ToInt32(Value)
            End If
        End Function

        Public Shared Function NullToString(ByVal Value As Object) As String
            If Convert.IsDBNull(Value) Then
                Return ""
            Else
                Return Convert.ToString(Value)
            End If
        End Function

        Public Shared Function NullToDate(ByVal Value As Object) As Date
            If Convert.IsDBNull(Value) Then
                Return #12:00:00 AM#
            Else
                Return Value
            End If
        End Function

        Public Shared Function DateToNull(ByVal Value As Date) As Object
            If Value = #12:00:00 AM# Then
                Return Convert.DBNull
            Else
                Return Value
            End If
        End Function

        Public Shared Function IntegerToNull(ByVal Value As Int32) As Object
            If Value = 0 Then
                Return Convert.DBNull
            Else
                Return Value
            End If
        End Function
        Public Shared Function StringToNull(ByVal Value As String) As Object
            If Trim(Value) = "" Then
                Return Convert.DBNull
            Else
                Return Trim(Value)
            End If
        End Function

        Public Shared Function RemoveLeadingZero(ByVal inStr As String) As Integer
            inStr = Trim(inStr)
            If inStr = "" Then
                Return 0
            Else

                Dim chArr As String
                Dim i As Integer = 0
                For i = 0 To inStr.Length - 1
                    If inStr.Chars(i) = "0" Then
                        chArr += "0"
                    End If
                Next
                'inStr.TrimStart(chArr)
                inStr.Replace(chArr, "")
                inStr = Trim(inStr)
                If IsNumeric(inStr) Then
                    Return CInt(inStr)
                Else
                    Return 0
                End If
            End If
        End Function

    End Class ' END CLASS DEFINITION UtilityManager

End Namespace ' HotTopic.RD.Services

