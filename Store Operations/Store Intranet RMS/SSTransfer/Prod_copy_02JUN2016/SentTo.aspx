<%@ Register TagPrefix="uc1" TagName="TransferGrid" Src="UserControls/TransferGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TransferFilter" Src="UserControls/TransferFilter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SearchBox" Src="UserControls/SearchBox.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SentTo.aspx.vb" Inherits="SSTransfer.SentTo"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Transfer Home</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" alink="white" vlink="white" link="white">
		<form id="frmTransferHome" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="4" width="100%" bgColor="#000000" border="0">
				<tr>
					<td height="1"><uc1:header id="layoutHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="top" align="middle">
						<table cellSpacing="0" cellPadding="5" width="100%" border="0">
							<tr>
								<td>
									<uc1:SearchBox id="ucSearchBox" runat="server"></uc1:SearchBox>
								</td>
							</tr>
						</table>
						<hr color="#990000" width="100%">
						<table cellSpacing="0" cellPadding="5" width="100%" border="0">
							<tr>
								<td align="middle"><uc1:transferfilter id="TransferFilter" runat="server"></uc1:transferfilter></td>
							</tr>
							<tr>
								<td align="middle"><uc1:transfergrid id="tgSentTo" runat="server"></uc1:transfergrid></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:Footer id="Footer1" runat="server"></uc1:Footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
