Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class TransferToPDF

    Private objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
    Private objORAConnection As New OleDbConnection(ConfigurationSettings.AppSettings("strORAConnection"))
    Private strBoxId As String
    Private strStoreId As String

    Public Enum PDFReportType As Byte
        StoreToStore = 1
        Damages = 2
        RTV = 3
        Backstock = 4
        Manifest = 5
    End Enum

    Public Sub New(ByVal InBoxId As String, ByVal InStoreCd As Int16)
        strBoxId = InBoxId
        strStoreId = InStoreCd

        objSQLConnection.Open()
        objORAConnection.Open()
    End Sub

    Public Sub Close()
        objSQLConnection.Close()
        objORAConnection.Close()
    End Sub

    Public Function CreatePDFReport(ByVal bytReportType As PDFReportType, ByVal strDir As String) As Boolean

        Dim objDataSet As New DataSet()
        Dim strSQL As String
        Dim strFilter As String
        Dim strReportType As String
        Dim strFileName As String

        Dim objSQLCommand As New SqlCommand()
        Dim objSQLDataAdapter As New SqlDataAdapter()

        Dim objORACommand As New OleDbCommand()
        Dim objORADataAdapter As New OleDbDataAdapter()

        Dim I As Int32
        Dim SKUNumbers As String
        Dim objRelation As DataRelation

        Dim strStoredProcedure As String

        Select Case bytReportType
            Case 1
                strFilter = "3,4"
                strReportType = "StoreToStore"
            Case 2
                strFilter = "1"
                strReportType = "Damages"
            Case 3
                strFilter = "5"
                strReportType = "RTV"
            Case 4
                strFilter = "2"
                strReportType = "BackStock"
            Case 5
                strFilter = "1,2,3,4,5"
                strReportType = "Manifest"
        End Select

        strSQL = "SELECT     Box_Xfer_Item.*, Xfer_Type.Xfer_Desc, Customer_Info.First_Name + ' ' + Customer_Info.Last_Name as Cust_Name, "
        strSQL = strSQL & "              Customer_Info.Home_Phone as Cust_Phone "
        strSQL = strSQL & "FROM         Box_Xfer_Item INNER JOIN "
        strSQL = strSQL & "              Xfer_Type ON Box_Xfer_Item.Xfer_Type_Cd = Xfer_Type.Xfer_Cd LEFT OUTER JOIN "
        strSQL = strSQL & "              Customer_Info ON Box_Xfer_Item.Cust_Id = Customer_Info.Cust_Id "
        strSQL = strSQL & "WHERE Box_Xfer_Item.Box_Id = '" & strBoxId & "' And Box_Xfer_Item.Xfer_Type_Cd In (" & strFilter & ") "
        strSQL = strSQL & "ORDER BY Xfer_Type.Xfer_Desc, Box_Xfer_Item.SKU_Num "

        objSQLCommand.Connection = objSQLConnection
        objSQLCommand.CommandText = strSQL
        objSQLDataAdapter.SelectCommand = objSQLCommand
        objSQLDataAdapter.FillSchema(objDataSet, SchemaType.Mapped)

        objSQLDataAdapter.Fill(objDataSet, "BoxItems")

        objDataSet.Tables("BoxItems").Columns.Add("VE_CD", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("SIZE_CD", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("CURR", GetType(Decimal))
        objDataSet.Tables("BoxItems").Columns.Add("DES1", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("DEPT_CD", GetType(String))
        objDataSet.Tables("BoxItems").Columns.Add("ITM_CD", GetType(String))

        Dim BoxItemskeys(3) As DataColumn

        BoxItemskeys(0) = objDataSet.Tables("BoxItems").Columns("Box_Id")
        BoxItemskeys(1) = objDataSet.Tables("BoxItems").Columns("SKU_Num")
        BoxItemskeys(2) = objDataSet.Tables("BoxItems").Columns("Xfer_Type_Cd")
        BoxItemskeys(3) = objDataSet.Tables("BoxItems").Columns("Cust_Id")

        objDataSet.Tables("BoxItems").PrimaryKey = BoxItemskeys

        If objDataSet.Tables("BoxItems").Rows.Count > 0 Then

            For I = 0 To objDataSet.Tables("BoxItems").Rows.Count - 1
                SKUNumbers = SKUNumbers & "'" & objDataSet.Tables("BoxItems").Rows(I).Item("SKU_Num") & "',"
            Next I

            SKUNumbers = Mid(SKUNumbers, 1, SKUNumbers.Length - 1)

            strSQL = " SELECT DISTINCT U.SKU_NUM, A.DES1, B.curr, U.SIZE_CD, A.DES1, A.VE_CD,A.DEPT_CD,A.ITM_CD "
            strSQL = strSQL & " FROM gm_itm A, GM_SKU U, "
            strSQL = strSQL & " (SELECT P1.sku_num SKU_NUM, P1.ret_prc CURR "
            strSQL = strSQL & " FROM gm_prc P1,  "
            strSQL = strSQL & " (SELECT sku_num, MAX(to_char(beg_dt, 'yyyymmdd')||to_char(ent_dt, 'yyyymmdd')||ent_time) last "
            strSQL = strSQL & "  FROM gm_prc  "
            strSQL = strSQL & "  WHERE sku_num in (" & SKUNumbers & ") AND beg_dt <= SYSDATE "
            strSQL = strSQL & "  GROUP BY sku_num ) P2 "
            strSQL = strSQL & "  WHERE P1.sku_num = P2.sku_num "
            strSQL = strSQL & "  AND TO_CHAR(P1.beg_dt, 'yyyymmdd')||TO_CHAR(P1.ent_dt,'yyyymmdd')||P1.ent_time = P2.last) B "
            strSQL = strSQL & "  WHERE A.itm_cd = SUBSTR(B.sku_num,1,6) AND (A.ITM_CD = U.ITM_CD) AND U.SKU_NUM IN (" & SKUNumbers & ") "

            objORACommand.Connection = objORAConnection
            objORACommand.CommandType = CommandType.Text
            objORACommand.CommandText = strSQL

            objORADataAdapter.SelectCommand = objORACommand
            objORADataAdapter.Fill(objDataSet, "ORAItemInfo")

            Dim keys(0) As DataColumn
            keys(0) = objDataSet.Tables("ORAItemInfo").Columns("SKU_Num")
            keys(0).Unique = True
            keys(0).AllowDBNull = False
            objDataSet.Tables("ORAItemInfo").PrimaryKey = keys

            AddORAItemInfo(objDataSet.Tables("BoxItems"), objDataSet.Tables("ORAItemInfo"))
            objDataSet.Tables("ORAItemInfo").Dispose()


            'Generate Report

            If bytReportType <> PDFReportType.Manifest Then
                GenerateTransferPDF(objDataSet, bytReportType, strDir, strReportType)
            Else
                GenerateManifestPDF(objDataSet, bytReportType, strDir)
            End If



        End If

        Return True

    End Function

    Private Sub GenerateTransferPDF(ByVal objDataSet As DataSet, ByVal bytReportType As PDFReportType, ByVal strDir As String, ByVal strReportType As String)

        Dim oRpt As New TransferForm()
        Dim strFileName As String
        Dim strStoredProcedure As String

        oRpt.SetDataSource(objDataSet.Tables("BoxItems"))

        Dim objDataReader As SqlClient.SqlDataReader
        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeader '" & strBoxId & "' ", objSQLConnection)

        objDataReader = objCommand.ExecuteReader()

        objDataReader.Read()

        Dim crParam As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition
        Dim crValues As New CrystalDecisions.Shared.ParameterValues()
        Dim crDisValue As New CrystalDecisions.Shared.ParameterDiscreteValue()

        For Each crParam In oRpt.DataDefinition.ParameterFields
            Select Case crParam.Name
                Case "FromStoreNumber"
                    crDisValue.Value = Mid(objDataReader("SStore"), 1, 4)
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "StoreName"
                    crDisValue.Value = Mid(objDataReader("SStore"), 5, Len(objDataReader("SStore")) - 5)
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "ToStoreNumber"
                    crDisValue.Value = Mid(objDataReader("RStore"), 1, 4)
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "ToStoreName"
                    crDisValue.Value = Mid(objDataReader("RStore"), 5, Len(objDataReader("RStore")) - 5)
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "TransferNumber"
                    crDisValue.Value = objDataReader("Box_Id")
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "FromStoreNumber"
                    crDisValue.Value = Mid(objDataReader("SStore"), 1, 4)
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "StoreName"
                    crDisValue.Value = Mid(objDataReader("SStore"), 5, Len(objDataReader("SStore")) - 5)
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "ToStoreNumber"
                    crDisValue.Value = Mid(objDataReader("RStore"), 1, 4)
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "ToStoreName"
                    crDisValue.Value = Mid(objDataReader("RStore"), 5, Len(objDataReader("RStore")) - 5)
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "TransferNumber"
                    crDisValue.Value = objDataReader("Box_Id")
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "StoreToStore"
                    If bytReportType = PDFReportType.StoreToStore Then
                        crDisValue.Value = "X"
                    Else
                        crDisValue.Value = " "
                    End If

                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "Damages"

                    If bytReportType = PDFReportType.Damages Then
                        crDisValue.Value = "X"
                    Else
                        crDisValue.Value = " "
                    End If

                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "RTV"
                    If bytReportType = PDFReportType.RTV Then
                        crDisValue.Value = "X"
                    Else
                        crDisValue.Value = " "
                    End If

                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "Backstock"
                    If bytReportType = PDFReportType.Backstock Then
                        crDisValue.Value = "X"
                    Else
                        crDisValue.Value = " "
                    End If

                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "DateShipped"
                    crDisValue.Value = objDataReader("Shipment_Date")
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "DateShipped"
                    crDisValue.Value = objDataReader("Shipment_Date")
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

            End Select
        Next

        objDataReader.Close()

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions()

        oRpt.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        oRpt.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        strFileName = strBoxId & "_" & strReportType & ".pdf"
        DiskOpts.DiskFileName = strDir & strFileName

        oRpt.ExportOptions.DestinationOptions = DiskOpts

        oRpt.Export()
        oRpt.Close()


        ' objSQLConnection.Close()

        strStoredProcedure = "spInsertPDFFile '" & strBoxId & "'," & bytReportType & ",'" & strFileName & "','" & strDir & strFileName & "','" & Now() & "' "

        ' objSQLConnection.Open()

        ' Dim objCommands As New SqlClient.SqlCommand(strStoredProcedure, objSQLConnection)
        objCommand.CommandText = strStoredProcedure
        objCommand.ExecuteNonQuery()

    End Sub

    Private Sub GenerateManifestPDF(ByVal objDataSet As DataSet, ByVal bytReportType As PDFReportType, ByVal strDir As String)

        Dim oRpt As New Manifest()
        Dim strFileName As String
        Dim strStoredProcedure As String

        oRpt.SetDataSource(objDataSet.Tables("BoxItems"))

        Dim objDataReader As SqlClient.SqlDataReader
        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeader '" & strBoxId & "' ", objSQLConnection)

        objDataReader = objCommand.ExecuteReader()

        objDataReader.Read()

        Dim crParam As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition
        Dim crValues As New CrystalDecisions.Shared.ParameterValues()
        Dim crDisValue As New CrystalDecisions.Shared.ParameterDiscreteValue()

        For Each crParam In oRpt.DataDefinition.ParameterFields
            Select Case crParam.Name
                Case "FromStoreNumber"
                    crDisValue.Value = Mid(objDataReader("SStore"), 1, 4)
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "StoreName"
                    crDisValue.Value = Mid(objDataReader("SStore"), 5, Len(objDataReader("SStore")) - 5)
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "ToStoreNumber"
                    crDisValue.Value = Mid(objDataReader("RStore"), 1, 4)
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "ToStoreName"
                    crDisValue.Value = Mid(objDataReader("RStore"), 5, Len(objDataReader("RStore")) - 5)
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "TransferNumber"
                    crDisValue.Value = objDataReader("Box_Id")
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

                Case "DateShipped"
                    crDisValue.Value = objDataReader("Shipment_Date")
                    crValues.Add(crDisValue)
                    crParam.ApplyCurrentValues(crValues)

            End Select
        Next

        objDataReader.Close()

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions()

        oRpt.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        oRpt.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        strFileName = strBoxId & "_Manifest.pdf"
        DiskOpts.DiskFileName = strDir & strFileName

        oRpt.ExportOptions.DestinationOptions = DiskOpts

        oRpt.Export()
        oRpt.Close()


        ' objSQLConnection.Close()

        strStoredProcedure = "spInsertPDFFile '" & strBoxId & "'," & bytReportType & ",'" & strFileName & "','" & strDir & strFileName & "','" & Now() & "' "

        ' objSQLConnection.Open()

        ' Dim objCommands As New SqlClient.SqlCommand(strStoredProcedure, objSQLConnection)
        objCommand.CommandText = strStoredProcedure
        objCommand.ExecuteNonQuery()

    End Sub

    Private Sub AddORAItemInfo(ByRef objTargetDataTable As DataTable, ByRef objSourceDataTable As DataTable)

        Dim CurrentDataRow As DataRow
        Dim FoundRow As DataRow
        Dim SKU_Num(0) As Object

        For Each CurrentDataRow In objTargetDataTable.Rows

            SKU_Num(0) = CurrentDataRow("SKU_Num")
            FoundRow = objSourceDataTable.Rows.Find(SKU_Num)

            If Not FoundRow Is Nothing Then
                CurrentDataRow("VE_CD") = FoundRow("VE_CD")
                CurrentDataRow("DES1") = FoundRow("DES1")
                CurrentDataRow("SIZE_CD") = FoundRow("SIZE_CD")
                CurrentDataRow("CURR") = FoundRow("CURR")
                CurrentDataRow("ITM_CD") = FoundRow("ITM_CD")
                CurrentDataRow("DEPT_CD") = FoundRow("DEPT_CD")
            End If

        Next

    End Sub

End Class
