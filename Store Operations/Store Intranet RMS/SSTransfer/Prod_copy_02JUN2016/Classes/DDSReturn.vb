Public Class DDSReturn

    Private strDDSReturnConn As String
    Private decRATransferWeight As Decimal
    Private booValidRANum As Boolean

    Sub New(ByVal DBConn As String)

        booValidRANum = False
        strDDSReturnConn = DBConn

    End Sub

    ReadOnly Property ValidRA()
        Get
            Return booValidRANum
        End Get
    End Property

    ReadOnly Property RATransferWeight()
        Get
            Return decRATransferWeight
        End Get
    End Property

    Public Function UpdateRAShipped(ByVal RANum As String, ByVal SendingStore As String, ByVal BoxId As String, ByVal Shipped As Boolean, ByVal Shipdate As Date)

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(strDDSReturnConn)

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("proc_UpdateShipped", objConnection)

        objCommand.CommandType = CommandType.StoredProcedure

        Dim prmRANum As SqlClient.SqlParameter = objCommand.Parameters.Add("@RA_Num", SqlDbType.VarChar)
        Dim prmStoreNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt)
        Dim prmBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)
        Dim prmShipped As SqlClient.SqlParameter = objCommand.Parameters.Add("@Shipped", SqlDbType.Bit)
        Dim prmShipDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@Ship_Date", SqlDbType.DateTime)

        prmRANum.Direction = ParameterDirection.Input
        prmStoreNum.Direction = ParameterDirection.Input

        prmRANum.Value = RANum
        prmStoreNum.Value = SendingStore
        prmBoxId.Value = BoxId
        prmShipped.Value = Shipped
        prmShipDate.Value = ShipDate

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Function


    Public Function GetRAData(ByVal RANum As String, ByVal SendingStore As Integer)

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(strDDSReturnConn)
        Dim objDataReader As SqlClient.SqlDataReader
        Dim decWeight As Decimal
        Dim booShipped As Boolean

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("proc_GetRATracking", objConnection)

        objCommand.CommandType = CommandType.StoredProcedure

        Dim prmRANum As SqlClient.SqlParameter = objCommand.Parameters.Add("@RA_Num", SqlDbType.VarChar)
        Dim prmStoreNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt)

        prmRANum.Direction = ParameterDirection.Input
        prmStoreNum.Direction = ParameterDirection.Input

        prmRANum.Value = RANum
        prmStoreNum.Value = SendingStore

        objDataReader = objCommand.ExecuteReader()

        If objDataReader.Read Then
            decRATransferWeight = objDataReader("shipment_weight")
            If Not IsDBNull(objDataReader("shipped")) Then
                booShipped = objDataReader("shipped")
                booValidRANum = Not booShipped
                Return Not booShipped
            Else
                Return True
            End If

        Else
            Return False
        End If

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Function


End Class
