Imports System.IO

Public Class BizTalkInterface

    'Outputlocation of ReturnASN flat file
    Private strFileOutputLocation As String
    Private strFlatFile As String

    Public Sub New(ByVal FileOutputLocation)
        strFileOutputLocation = FileOutputLocation
    End Sub

    Property FileOutputLocation()
        Get
            Return strFileOutputLocation
        End Get
        Set(ByVal Value)
            strFileOutputLocation = Value
        End Set
    End Property

    Property FlatFile()
        Get
            Return strFlatFile
        End Get
        Set(ByVal Value)
            strFlatFile = Value
        End Set
    End Property

    'Save Flatfile to disk
    Public Function CreateFlatFile(ByVal Box_Id As String)

        Dim fp As StreamWriter
        'STS custom error loging object
        Dim objErrorRecord As New ApplicationErrors(ConfigurationSettings.AppSettings("strSQLConnection"), _
                                                                      ConfigurationSettings.AppSettings("strErrorDBTable"), _
                                                                      ConfigurationSettings.AppSettings("strSMTPServer"), _
                                                                      ConfigurationSettings.AppSettings("strEmailTo"), _
                                                                      ConfigurationSettings.AppSettings("strEmailFrom"), _
                                                                      ConfigurationSettings.AppSettings("strEmailHeader"))
        'Generate header for flat file
        GetHeaderData(Box_Id)
        'Generate item details for flat file
        If GetItemData(Box_Id) Then

            'Save to disk
            Try
                fp = File.CreateText(System.Web.HttpContext.Current.Server.MapPath(strFileOutputLocation) & "rasn_" & Now.ToFileTime & Now.Millisecond & ".txt")
                fp.WriteLine(strFlatFile)
                fp.Close()
            Catch err As Exception

                'save error log
                objErrorRecord.AddRelevantInfo("Transfer: " & Box_Id.ToString)
                objErrorRecord.SaveError(False, True, "File Creation failed. Reason is as follows " & err.ToString())

            End Try

        End If

    End Function
    '# Added to interpret item for RMS changes.  10/02/2012 _LJ
    Private Function GetSKUNum(ByVal strSKU As String)

        If InStr(strSKU, "-") Then
            Return Mid(strSKU, strSKU.Length - 2, 3)
        Else
            Return strSKU
        End If

    End Function
    'Get item details for flat file 
    Private Function GetItemData(ByVal Box_Id As String) As Boolean

        'Open DB connection
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        'Used for required InboundASN SequenceNbr field
        Dim I As Integer = 1
        Dim booItemsExist As Boolean = False

        'open DB connection
        objConnection.Open()

        'Get all item details for every item in transfer
        Dim objCommand As New SqlClient.SqlCommand("spGetBoxItems", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = Box_Id

        objDataReader = objCommand.ExecuteReader()


        'Create item detail line items for WMS ReturnASN flat file
        Do While objDataReader.Read()

            booItemsExist = True
            Dim strSKUNum As String
            strSKUNum = GetSKUNum(objDataReader("SKU_Num"))

            strFlatFile = strFlatFile & vbCrLf
            'Header
            strFlatFile = strFlatFile & "BOX_XFER_ITEM:"
            'SequenceNbr <--> SequenceNbr
            strFlatFile = strFlatFile & I & "|"
            'Vendor <--> VendorID
            strFlatFile = strFlatFile & objDataReader("VE_CD").ToString & "|"
            'SKUSuffix <--> StyleSuffix
            ' strFlatFile = strFlatFile & Mid(objDataReader("SKU_Num"), Len(objDataReader("SKU_Num")) - 2, 3) & "|"
            strFlatFile = strFlatFile & strSKUNum & "|"
            'ItemQty <--> ShippedAsnQuantity/UnitsSHipped
            strFlatFile = strFlatFile & objDataReader("Item_Qty") & "|"
            'Company <--> Company
            strFlatFile = strFlatFile & objDataReader("Co").ToString & "|"
            'Division <--> Division
            strFlatFile = strFlatFile & objDataReader("Div").ToString & "|"
            'ItemNum <--> Style
            strFlatFile = strFlatFile & objDataReader("ITM_CD").ToString & "|"
            'CasesShipped <--> CasesShipped
            strFlatFile = strFlatFile & "1|"
            'PONum <--> PONbr
            strFlatFile = strFlatFile & "0|"

            'Remove trialing pipe
            strFlatFile = Mid(strFlatFile, 1, Len(strFlatFile) - 1)

            I = I + 1

        Loop

        'close connection
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return booItemsExist

    End Function

    'Generate Transfer header for WMS ReturnASN flat file
    Private Sub GetHeaderData(ByVal Box_Id As String)

        'Open DB Connection
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        'Open Connection
        objConnection.Open()

        'Get header information for current transfer
        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeader", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = Box_Id
        'execute stored procedure
        objDataReader = objCommand.ExecuteReader()
        'Dim StrBoxID As String
        'Dim StrComBoxID As String
        'Generate header for flat file
        If objDataReader.Read() Then

            'TrackingNum/BoxId <--> ShipmentNbr
            '# comment below out for ORMS change to send box ID (10 digit) to DC by Lan
            'If tracking Number is Hand Carry or does not exist then use boxid number
            'If Not IsDBNull(objDataReader("Tracking_Num")) Then
            '    'If objDataReader("Tracking_Num") = "[ Hand Carry ]" Then
            '    '    strFlatFile = "BOX_XFER_HDR:STS-" & objDataReader("Box_Id") & "|"
            '    'Else
            '    '    strFlatFile = "BOX_XFER_HDR:" & UCase(objDataReader("Tracking_Num")) & "|"
            '    'End If
            'Else 
            '#  ORMS change to send box ID (10 digit) to DC by Lan
            'StrBoxID = objDataReader("Box_Id")

            'StrComBoxID = StrBoxID.Substring(0, 4) + StrBoxID.Substring(6, 6)
            'strFlatFile = "BOX_XFER_HDR:STS-" & StrComBoxID & "|"

            strFlatFile = "BOX_XFER_HDR:STS-" & objDataReader("Box_Id") & "|"
            'End If
            'ASNOriginalType <--> ASNOriginalType
            Select Case objDataReader("TType")
                Case 1
                    strFlatFile = strFlatFile & "N|"
                Case 2
                    strFlatFile = strFlatFile & "R|"
            End Select
            'RecLocation <--> ToLocation
            strFlatFile = strFlatFile & objDataReader("Receiving_Store_Cd") & "|"
            'ASNType <--> ASNType
            strFlatFile = strFlatFile & "S|"
            'Id <--> Id 
            ' commment Inbound_ASN out and replace it by Fedex TrackingNo on 02/25/2013 by Lan 
            'strFlatFile = strFlatFile & "Inbound_ASN|"
            strFlatFile = strFlatFile & UCase(objDataReader("Tracking_Num")) & "|"
            'PurchasingUOM <--> PurchasingUOM
            strFlatFile = strFlatFile & "U|"
            'Sending Store <--> PoLineNbr
            strFlatFile = strFlatFile & objDataReader("Sending_Store_Cd") & "|"
            'Y <--> QcHoldUponReceipt
            strFlatFile = strFlatFile & "Y|"
            'Remove trialing pipe
            strFlatFile = Mid(strFlatFile, 1, Len(strFlatFile) - 1)

        End If

        'Close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

End Class
