<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ViewAppError.aspx.vb" Inherits="SSTransfer.ViewAppError" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Customer Information</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
	</HEAD>
	<body bgcolor="#FFFFFF" bottomMargin="0" vLink="black" aLink="black" link="black" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server" onsubmit="return checkSubmit();">
			<table width="100%" height="100%" borderColor="#D60C8C" cellSpacing="0" borderColorDark="#D60C8C" cellPadding="1" borderColorLight="#D60C8C" border="3">
				<tr>
					<td height="1%" align="middle" bgColor="#FF9ACE" colSpan="2"><asp:label id="lblHeader" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="medium" Font-Bold="True">Debug Info</asp:label></td>
				</tr>
				<tr>
					<td valign="top"><asp:label id="lblError" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="black" Font-Size="small"></asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
