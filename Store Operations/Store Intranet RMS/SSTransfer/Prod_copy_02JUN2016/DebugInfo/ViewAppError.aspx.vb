Public Class ViewAppError
    Inherits System.Web.UI.Page

    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
    Protected WithEvents lblError As System.Web.UI.WebControls.Label


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private m_intAppErrId As Int32

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        m_intAppErrId = Request("AppError")

        GetAppError()

    End Sub

    Private Sub GetAppError()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetAppErr " & m_intAppErrId, objConnection)

        objDataReader = objCommand.ExecuteReader()

        If objDataReader.Read() Then
            lblError.Text = objDataReader("Error_Message")
        End If

        objDataReader.Close()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

End Class
