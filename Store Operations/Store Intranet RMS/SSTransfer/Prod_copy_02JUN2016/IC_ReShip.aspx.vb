Public Class IC_ReShip
    Inherits System.Web.UI.Page
    Protected WithEvents lblShipDate_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblShipDate_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblUPS_Name As System.Web.UI.WebControls.Label
    Protected WithEvents hlUPS As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblTransferId_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblTransferId_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblEmp_name As System.Web.UI.WebControls.Label
    Protected WithEvents lblEmp_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblStatus_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblsensor As System.Web.UI.WebControls.Label
    Protected WithEvents lblInk_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblSStore_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblRStore_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblDisQuant_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblDisAmount_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblDisAmount_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblDisQuant_Name As System.Web.UI.WebControls.Label
    Protected WithEvents btnHistory As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents ddlError As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblStatus_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblSStore_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblRStore_Value As System.Web.UI.WebControls.Label
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ucHeader As Header
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblShipDate_Value_New As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents hlUPS_New As System.Web.UI.WebControls.HyperLink
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents lblTransferId_Value_New As System.Web.UI.WebControls.Label
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents lblEmp_Value_New As System.Web.UI.WebControls.Label
    Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    Protected WithEvents lblStatus_Value_New As System.Web.UI.WebControls.Label
    Protected WithEvents Label11 As System.Web.UI.WebControls.Label
    Protected WithEvents lblInk_Value_New As System.Web.UI.WebControls.Label
    Protected WithEvents Label13 As System.Web.UI.WebControls.Label
    Protected WithEvents lblSStore_Value_New As System.Web.UI.WebControls.Label
    Protected WithEvents Label15 As System.Web.UI.WebControls.Label
    Protected WithEvents lblRStore_Value_New As System.Web.UI.WebControls.Label
    Protected WithEvents lblCopyStatus As System.Web.UI.WebControls.Label
    Protected WithEvents lblRStore As System.Web.UI.WebControls.Label
    Protected WithEvents ddlRStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblSStore As System.Web.UI.WebControls.Label
    Protected WithEvents ddlSStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlAction As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button

    Private m_objUserInfo As New UserInfo()
    Private m_strBoxId As String
    Private m_intSStore As Int32
    Private m_intRStore As Int32
    '******************************************************************
    'These will be the variables passed to results pages to form querys.
    Public GBID As String
    Public FStore As String       'From Store
    Public FDis As String         'From District
    Public FReg As String         'From Region
    Public NoFInternet As String  'From Internet Store Checkbox
    Public TStore As String       'To Store
    Public TDis As String         'To District
    Public TReg As String         'To Region
    Public NoTInternet As String  'To Internet Store Checkbox
    Public GERS As String         'GERS Process Status
    Public Discrep As String      'Discrepancy status
    Public TType As String        'Transfer Type
    Public TStatus As String      'Transfer Status
    Public TDate As String        'To Date search
    Public FDate As String        'From Date Search
    Public DFQty As String        'Discrep from qty
    Public DTQty As String        'Discrep to qty
    Public DFAmt As String        'Discrep from amt
    Public DTAmt As String        'Discrep to amt
    Public TrackNum As String     'Tracking Number (UPS)
    Public TransNum As String     'Transfer Number
    '*********************************************************************

    Private Enum CopyErrorType
        SelectErr = 0
        EmployeeErr = 1
        CarrierDeliveryErr = 2
        IncorrectCarrierErr = 3
    End Enum

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        m_objUserInfo.GetEmployeeInfo()

        m_strBoxId = Request.QueryString("BID")
        GBID = Request.Params("GERSBID")
        FStore = Request.Params("FS")
        FDis = Request.Params("FDIS")
        FReg = Request.Params("FREG")
        TStore = Request.Params("TS")
        TDis = Request.Params("TDIS")
        TReg = Request.Params("TREG")
        NoFInternet = Request.Params("NFI")
        NoTInternet = Request.Params("NTI")
        GERS = Request.Params("G")
        Discrep = Request.Params("DS")     'Discrep Statuses
        TType = Request.Params("TT")       'Transfer Types
        TStatus = Request.Params("TranS")  'Transfer Statuses
        FDate = Request.Params("FD")
        TDate = Request.Params("TD")
        DFQty = Request.Params("DFQ")
        DTQty = Request.Params("DTQ")
        DFAmt = Request.Params("DFA")
        DTAmt = Request.Params("DTA")
        TrackNum = Request.Params("TKN")   'Tracking number
        TransNum = Request.Params("TSN")   'transfer number

        ucHeader.lblTitle = "Shipment Correction"
        ucHeader.CurrentPage(Header.PageName.IC_Search)
        ucHeader.CurrentMode(Header.HeaderGroup.IC)

        pageBody.Attributes.Add("onload", "")

        btnHistory.Attributes.Add("onclick", "javascript:window.showModalDialog('EmpHistory.aspx?BID=" & m_strBoxId & "&nocache=" & Now.ToString & "','Status History','center=1;scroll=0;status=no;dialogWidth=400px;dialogHeight=380px;');")

        If Not Page.IsPostBack Then
            GetHeaderData(m_strBoxId, False)

            FillStoreDDL(ddlSStore, "asFillTStore")
            FillStoreDDL(ddlRStore, "asFillTStore")

        End If


    End Sub

    Private Function CopyTransfer(ByVal BoxId As String, ByVal SStore As Int32, ByVal RStore As Int32, ByVal OldStatus As Int32, ByVal NewStatus As Int32, ByVal CopyTrackingLabel As Boolean, ByVal CopyICGERSStatus As Boolean) As String

        Dim TType As Int32
        'If lblRStore_Value_New.Text = "9999" Then
        If ddlRStore.SelectedItem.Text = "9999" Then
            TType = 2
        Else
            TType = 1
        End If

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spICReShip", objConnection)
        Dim objDataReader As SqlClient.SqlDataReader
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parOldStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@OldStatus", SqlDbType.TinyInt)
        Dim parSStore As SqlClient.SqlParameter = objCommand.Parameters.Add("@SStore", SqlDbType.SmallInt)
        Dim parRStore As SqlClient.SqlParameter = objCommand.Parameters.Add("@RStore", SqlDbType.SmallInt)
        Dim parNewStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@NewStatus", SqlDbType.TinyInt)
        Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@BoxId", SqlDbType.VarChar, 14)
        Dim parCreatedDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@Created_Date", SqlDbType.DateTime)
        Dim parCopyTrackingLabel As SqlClient.SqlParameter = objCommand.Parameters.Add("@CopyTrackingLabel", SqlDbType.Bit)
        Dim parCopyICGERSStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@CopyICGERSStatus", SqlDbType.Bit)
        Dim parEmpId As SqlClient.SqlParameter = objCommand.Parameters.Add("@EmpId", SqlDbType.VarChar, 5)
        Dim parEmpFullName As SqlClient.SqlParameter = objCommand.Parameters.Add("@EmpFullName", SqlDbType.VarChar, 100)
        Dim parTType As SqlClient.SqlParameter = objCommand.Parameters.Add("@TType", SqlDbType.SmallInt)
        Dim parNewBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@NewBoxId", SqlDbType.VarChar, 14)


        parOldStatus.Direction = ParameterDirection.Input
        parSStore.Direction = ParameterDirection.Input
        parRStore.Direction = ParameterDirection.Input
        parNewStatus.Direction = ParameterDirection.Input
        parBoxId.Direction = ParameterDirection.Input
        parCreatedDate.Direction = ParameterDirection.Input
        parCopyTrackingLabel.Direction = ParameterDirection.Input
        parCopyICGERSStatus.Direction = ParameterDirection.Input
        parEmpId.Direction = ParameterDirection.Input
        parEmpFullName.Direction = ParameterDirection.Input
        parTType.Direction = ParameterDirection.Input
        parNewBoxId.Direction = ParameterDirection.Output


        parOldStatus.Value = OldStatus
        parSStore.Value = SStore
        parRStore.Value = RStore
        parNewStatus.Value = NewStatus
        parBoxId.Value = BoxId
        parCreatedDate.Value = Now()
        parTType.Value = TType

        parCopyTrackingLabel.Value = CopyTrackingLabel
        parCopyICGERSStatus.Value = CopyICGERSStatus
        parEmpId.Value = m_objUserInfo.EmpId
        parEmpFullName.Value = m_objUserInfo.EmpFullName

        objCommand.ExecuteNonQuery()

        objConnection.Close()
        objCommand.Dispose()
        Return parNewBoxId.Value.ToString

    End Function
    Private Sub FillStoreDDL(ByRef StoreDDL As System.Web.UI.WebControls.DropDownList, ByVal strStoredProcedure As String)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        objDataReader = objCommand.ExecuteReader()

        StoreDDL.DataSource = objDataReader
        StoreDDL.DataBind()

        objItem.Text = "Select A Store"
        objItem.Value = 0

        StoreDDL.Items.Insert(0, objItem)

        objConnection.Close()
        objCommand.Dispose()
    End Sub
    Private Sub GetHeaderData(ByVal BoxId As String, ByVal booNew As Boolean)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeader", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = BoxId

        objDataReader = objCommand.ExecuteReader()

        If objDataReader.Read() Then

            If booNew Then
                lblTransferId_Value_New.Text = objDataReader("Box_Id").ToString

                If Not IsDBNull(objDataReader("Tracking_Num")) Then
                    hlUPS_New.Text = objDataReader("Tracking_Num")
                    hlUPS_New.NavigateUrl = Replace(objDataReader("TrackingURL"), "{0}", objDataReader("Tracking_Num"))
                End If

                lblStatus_Value_New.Text = objDataReader("Status_Desc").ToString
                lblEmp_Value_New.Text = objDataReader("Emp_FullName").ToString
                lblSStore_Value_New.Text = objDataReader("Sending_Store_Cd").ToString
                lblRStore_Value_New.Text = objDataReader("Receiving_Store_Cd").ToString
                lblInk_Value_New.Text = objDataReader("Tags").ToString

                If Not IsDBNull(objDataReader("Shipment_Date")) Then
                    lblShipDate_Value_New.Text = FormatDateTime(objDataReader("Shipment_Date"), DateFormat.ShortDate)
                End If
            Else
                lblTransferId_Value.Text = objDataReader("Box_Id").ToString

                If Not IsDBNull(objDataReader("Tracking_Num")) Then
                    hlUPS.Text = objDataReader("Tracking_Num")
                    hlUPS.NavigateUrl = Replace(objDataReader("TrackingURL"), "{0}", objDataReader("Tracking_Num"))
                End If

                lblStatus_Value.Text = objDataReader("Status_Desc").ToString
                lblEmp_Value.Text = objDataReader("Emp_FullName").ToString
                lblSStore_Value.Text = objDataReader("Sending_Store_Cd").ToString
                lblRStore_Value.Text = objDataReader("Receiving_Store_Cd").ToString
                lblInk_Value.Text = objDataReader("Tags").ToString

                If Not IsDBNull(objDataReader("Shipment_Date")) Then
                    lblShipDate_Value.Text = FormatDateTime(objDataReader("Shipment_Date"), DateFormat.ShortDate)
                End If
            End If

        End If

        objConnection.Close()
        objCommand.Dispose()
    End Sub
    Private Function IsPageValid()

        Dim booValid As Boolean = True
        Dim strErrorMsg As String

        If ddlRStore.Enabled And ddlRStore.SelectedItem.Value = 0 Then
            strErrorMsg = "Receiving Store required.<Br>"
            booValid = False
        End If

        If ddlSStore.Enabled And ddlSStore.SelectedItem.Value = 0 Then
            strErrorMsg = "Sending Store required.<Br>"
            booValid = False
        End If

        If ddlAction.Enabled And ddlAction.SelectedItem.Value = 0 Then
            strErrorMsg = strErrorMsg & "Action required."
            booValid = False
        End If

        If ddlError.SelectedItem.Value = 0 Then
            strErrorMsg = strErrorMsg & "Error Type required."
            booValid = False
        End If

        If Not booValid Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(strErrorMsg) & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
        End If

        Return booValid

    End Function
    Private Function VoidBoxTrackingInfo() As Boolean

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim booReturn As Boolean = True

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxTrackingInfo", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = m_strBoxId

        objDataReader = objCommand.ExecuteReader()

        If objDataReader.Read() Then

            If objDataReader("ParcelService_Cd") = 1 And objDataReader("XMLShipping") And objDataReader("Voided") = 0 Then
                booReturn = VoidShippingLabel(objDataReader("UPS_ShipmentId"), objDataReader("TrackingInfo_Id"))
            End If

        End If

        objConnection.Close()
        objCommand.Dispose()
        Return booReturn

    End Function
    Private Function VoidShippingLabel(ByVal strShipmentId As String, ByVal intTrackingInfoId As Int32) As Boolean

        Dim objUPS As New UPS_XML_Shipping()
        Dim strStoredProcedure As String
        Dim booReturn As Boolean = False

        objUPS.ShipmentId = strShipmentId
        objUPS.TrackingInfoId = intTrackingInfoId
        objUPS.BoxId = m_strBoxId

        booReturn = objUPS.SendVoidShipping

        Return booReturn

    End Function

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Dim strReturn As String
        strReturn = Request.Params("RTN2")
        Dim RTNPage
        RTNPage = "IC_ResultsTT"
        If InStr(1, strReturn, RTNPage) > 0 Then
            'Go back to IC_ResultsTT
            strReturn = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID
        Else
            'Go Back to IC_Result
            strReturn = "IC_Results.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum
        End If

        'Response.Redirect("IC_EditTransfer.aspx?BID=" & m_strBoxId & "&RTN2=" & strReturn)
        Response.Redirect("IC_EditTransfer.aspx?BID=" & m_strBoxId & "&RFC=" & strReturn)
    End Sub

    Private Sub FillAction(ByVal booKeep As Boolean, ByVal booReShip As Boolean)

        Dim objListItem_Select As New ListItem()
        Dim objListItem_Accept As New ListItem()
        Dim objListItem_ReShip As New ListItem()

        objListItem_Select.Text = "Select a action"
        objListItem_Select.Value = 0

        ddlAction.Items.Clear()
        ddlAction.Items.Add(objListItem_Select)

        If booKeep Then
            objListItem_Accept.Text = "Accept"
            objListItem_Accept.Value = 1
            ddlAction.Items.Add(objListItem_Accept)
        End If

        If booReShip Then
            objListItem_ReShip.Text = "Re-Ship"
            objListItem_ReShip.Value = 2
            ddlAction.Items.Add(objListItem_ReShip)
        End If

    End Sub
    Private Sub ddlError_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlError.SelectedIndexChanged

        Select Case ddlError.SelectedItem.Value
            Case CopyErrorType.SelectErr
                ddlAction.SelectedIndex = 0
                ddlAction.Enabled = False
                FillAction(True, True)
            Case CopyErrorType.EmployeeErr
                ddlAction.Enabled = True
                FillAction(True, True)
            Case CopyErrorType.CarrierDeliveryErr
                ddlAction.Enabled = True
                FillAction(True, True)
                'Case CopyErrorType.IncorrectCarrierErr
                '    ddlAction.Enabled = True
                '    FillAction(False, True)
        End Select

        ddlSStore.Enabled = False
        ddlRStore.Enabled = False

        ddlAction.SelectedIndex = 0
        ddlRStore.SelectedIndex = 0
        ddlSStore.SelectedIndex = 0

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim NewBoxId As String

        If IsPageValid() Then

            If IsProcessed(m_strBoxId) Then

                If ddlAction.SelectedItem.Value = 1 Then  'Code for keeping transfer

                    Select Case ddlError.SelectedItem.Value

                        Case CopyErrorType.EmployeeErr
                            NewBoxId = CopyTransfer(m_strBoxId, lblSStore_Value.Text.Trim, lblRStore_Value.Text.Trim, 9, 2, False, True)
                        Case CopyErrorType.CarrierDeliveryErr
                            NewBoxId = CopyTransfer(m_strBoxId, lblSStore_Value.Text.Trim, ddlRStore.SelectedItem.Value, 10, 2, 0, False)
                            'Case CopyErrorType.IncorrectCarrierErr
                            'NewBoxId = CopyTransfer(m_strBoxId, lblSStore_Value.Text.Trim, lblRStore_Value.Text.Trim, 11, 6, 0)

                            If VoidBoxTrackingInfo() Then
                                lblCopyStatus.Text = lblCopyStatus.Text & "Void Tracking Label...Successful.<br>"
                            Else
                                lblCopyStatus.Text = lblCopyStatus.Text & "<b>Void Tracking Label...Failure.</b><br>"
                            End If

                    End Select

                    GetHeaderData(NewBoxId, True)
                    lblCopyStatus.Text = lblCopyStatus.Text & "Transfer Copy...Successful.<br>"

                    GetHeaderData(m_strBoxId, False)

                ElseIf ddlAction.SelectedItem.Value = 2 Then   'Code for reshipping transfer

                    Select Case ddlError.SelectedItem.Value

                        Case CopyErrorType.EmployeeErr
                            NewBoxId = CopyTransfer(m_strBoxId, lblRStore_Value.Text.Trim, ddlRStore.SelectedItem.Value, 9, 6, 0, False)
                        Case CopyErrorType.CarrierDeliveryErr
                            NewBoxId = CopyTransfer(m_strBoxId, ddlSStore.SelectedItem.Value, lblRStore_Value.Text.Trim, 10, 6, 0, False)
                            'Case CopyErrorType.IncorrectCarrierErr
                            'NewBoxId = CopyTransfer(m_strBoxId, lblSStore_Value.Text.Trim, lblRStore_Value.Text.Trim, 11, 6, 0)

                            If VoidBoxTrackingInfo() Then
                                lblCopyStatus.Text = lblCopyStatus.Text & "Void Tracking Label...Successful.<br>"
                            Else
                                lblCopyStatus.Text = lblCopyStatus.Text & "<b>Void Tracking Label...Failure.</b><br>"
                            End If

                    End Select

                    GetHeaderData(NewBoxId, True)
                    lblCopyStatus.Text = lblCopyStatus.Text & "Transfer Copy...Successful.<br>"


                    GetHeaderData(m_strBoxId, False)

                End If

                btnSubmit.Enabled = False

            Else

                lblCopyStatus.Text = lblCopyStatus.Text & "Please process inventory movement before correcting shipments.<br>"

            End If

        End If

    End Sub

    Private Function IsProcessed(ByVal box_Id As String)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spIsBoxProcessed", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)
        Dim ProcessedParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Processed", SqlDbType.Bit)

        BoxIdParam.Direction = ParameterDirection.Input
        ProcessedParam.Direction = ParameterDirection.Output

        BoxIdParam.Value = box_Id

        objCommand.ExecuteNonQuery()

        objConnection.Close()
        objCommand.Dispose()

        Return ProcessedParam.Value

    End Function

    Private Sub ddlAction_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAction.SelectedIndexChanged

        Select Case ddlAction.SelectedItem.Value
            Case 0 'Select
                ddlSStore.SelectedIndex = 0
                ddlRStore.SelectedIndex = 0

                ddlSStore.Enabled = False
                ddlRStore.Enabled = False
            Case 1 'Keep
                ddlSStore.SelectedIndex = 0
                ddlRStore.SelectedIndex = 0

                If ddlError.SelectedItem.Value = CopyErrorType.EmployeeErr Then
                    ddlSStore.Enabled = False
                    ddlRStore.Enabled = False
                Else
                    ddlSStore.Enabled = False
                    ddlRStore.Enabled = True
                End If

            Case 2 'Re-Ship
                ddlSStore.SelectedIndex = 0
                ddlRStore.SelectedIndex = 0

                If ddlError.SelectedItem.Value = CopyErrorType.CarrierDeliveryErr Then
                    ddlSStore.Enabled = True
                    ddlRStore.Enabled = False
                Else
                    ddlSStore.Enabled = False
                    ddlRStore.Enabled = True
                End If

        End Select

    End Sub
End Class
