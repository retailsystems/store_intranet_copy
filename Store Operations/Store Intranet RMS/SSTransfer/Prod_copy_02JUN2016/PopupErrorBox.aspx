<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PopupErrorBox.aspx.vb" Inherits="SSTransfer.PopupErrorBox"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Error Message</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" alink="white" vlink="white" link="white">
		<form id="Form1" method="post" runat="server">
			<table borderColor="#990000" cellSpacing="0" borderColorDark="#990000" cellPadding="2" borderColorLight="#990000" border="3" width="100%">
				<tr>
					<td align="middle" bgColor="#440000" colSpan="2"><asp:label id="lblHeader" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="medium" Font-Bold="True">Error!</asp:label></td>
				</tr>
				<tr>
					<td bgColor="#000000">
						<table cellSpacing="0" cellPadding="3" border="0" width="100%">
							<tr>
								<td align="middle" bgColor="#000000">
									<asp:Label id="lblErrorMessage" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small"></asp:Label>
								</td>
							</tr>
							<tr>
								<td align="middle">
									<asp:Button id="btnClose" runat="server" Text="Close" BackColor="#440000" BorderColor="#990000" ForeColor="White" Font-Bold="True"></asp:Button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
