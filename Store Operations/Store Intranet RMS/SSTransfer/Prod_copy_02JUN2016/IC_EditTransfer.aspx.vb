Public Class IC_EditTransfer
    Inherits System.Web.UI.Page
    Protected WithEvents lblShipDate_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblShipDate_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblUPS_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblUPS_Value As System.Web.UI.WebControls.Label
    Protected WithEvents hlUPS As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblTransferId_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblTransferId_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblEmp_name As System.Web.UI.WebControls.Label
    Protected WithEvents lblEmp_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblVRStore As System.Web.UI.WebControls.Label
    Protected WithEvents lblStatus_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblStatus_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblInk_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblSStore_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblSStore_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblRStore_Name As System.Web.UI.WebControls.Label
    Protected WithEvents ddlRStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlGersStatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlDiscrepType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlorigGers As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlorigDiscrep As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlTType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblDisQuant_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblDisQuant_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblDisAmount_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblDisAmount_Value As System.Web.UI.WebControls.Label
    Protected WithEvents btnHistory As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents dgTransferBox As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgXferGroups As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ucHeader As Header
    Protected WithEvents lblsensor As System.Web.UI.WebControls.Label
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Protected WithEvents btnUpdate As System.Web.UI.WebControls.Button
    Protected WithEvents btnUpdateGERS As System.Web.UI.WebControls.Button
    Protected WithEvents lblNotes As System.Web.UI.WebControls.Label
    Protected WithEvents txtNotes As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtItemQty As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnVoid As System.Web.UI.WebControls.Button
    Protected WithEvents btnReShip As System.Web.UI.WebControls.Button
    Protected WithEvents btnEdit As System.Web.UI.WebControls.Button
    Private intRStore As Int32
    Private intSStore As Int32
    Public DiscrepQTY As Integer
    Public DiscrepAMT As Decimal
    Private m_strBoxId As String
    Public parmDQTY As SqlClient.SqlParameter
    Public parmDAMT As SqlClient.SqlParameter
    Public parmCheck As SqlClient.SqlParameter
    Public parmCheckNewTType As SqlClient.SqlParameter
    Public parmDiscrep As SqlClient.SqlParameter
    Public parmShipId As SqlClient.SqlParameter
    Public parmTrackID As SqlClient.SqlParameter
    Public parmParcelService As SqlClient.SqlParameter
    Public parmXML As SqlClient.SqlParameter
    Public parmVoid As SqlClient.SqlParameter
    Dim strReturn As String
    'Dim strReturn2 As String
    Public intReceivingStore As Int32  'assigned value when user changes receiving store value
    Private strNotes As String
    Public strHistoryNotes As String
    Public intdgXferTypes As Integer  'Count of items
    Public intdgXferItems As Integer  'Count of items
    Public intVoid As Integer
    Public ViewOnly As Boolean
    Private objUserInfo As New UserInfo()
    Protected WithEvents btnLost As System.Web.UI.WebControls.Button
    Protected WithEvents btnUpdateStoreNotes As System.Web.UI.WebControls.Button
    Protected WithEvents lblStoreNotes As System.Web.UI.WebControls.Label
    Protected WithEvents lblNewTransfer As System.Web.UI.WebControls.Label
    Protected WithEvents lblNewTransID As System.Web.UI.WebControls.Label
    Protected WithEvents lblOldTransfer As System.Web.UI.WebControls.Label
    Protected WithEvents lblOldTransID As System.Web.UI.WebControls.Label
    Protected WithEvents lblReShipInfo As System.Web.UI.WebControls.Label
    'Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Private objTransferLock As New TransferLock()
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    '******************************************************************
    'These will be the variables passed to results pages to form querys.
    Public GBID As String
    Public FStore As String       'From Store
    Public FDis As String         'From District
    Public FReg As String         'From Region
    Public NoFInternet As String  'From Internet Store Checkbox
    Public TStore As String       'To Store
    Public TDis As String         'To District
    Public TReg As String         'To Region
    Public NoTInternet As String  'To Internet Store Checkbox
    Public GERS As String         'GERS Process Status
    Public Discrep As String      'Discrepancy status
    Public TType As String        'Transfer Type
    Public TStatus As String      'Transfer Status
    Public TDate As String        'To Date search
    Public FDate As String        'From Date Search
    Public DFQty As String        'Discrep from qty
    Public DTQty As String        'Discrep to qty
    Public DFAmt As String        'Discrep from amt
    Public DTAmt As String        'Discrep to amt
    Public TrackNum As String     'Tracking Number (UPS)
    Public TransNum As String     'Transfer Number
    '*********************************************************************
    Public ShipID As String       'XML UPS Shipping ID
    Public TrackID As Integer     'Tracking ID # from box_xfer_tracking
    Public ParcelType As Int32    'Whether it is UPS or Not
    Public XML As Int32           'whether or not it was XML generated.
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objUserInfo.GetEmployeeInfo()
        If Not objUserInfo.EmpOnline Then
            Response.Redirect("SessionEnd.aspx?Mode=2")
        End If

        pageBody.Attributes.Add("onload", "")
        btnLost.Attributes.Add("onclick", "return confirm('Are you sure you want this transfer to have a status of Lost?')")
        btnVoid.Attributes.Add("onclick", "return confirm('Are you sure you want to Void this transfer?')")

        GBID = Request.Params("GERSBID")
        FStore = Request.Params("FS")
        FDis = Request.Params("FDIS")
        FReg = Request.Params("FREG")
        TStore = Request.Params("TS")
        TDis = Request.Params("TDIS")
        TReg = Request.Params("TREG")
        NoFInternet = Request.Params("NFI")
        NoTInternet = Request.Params("NTI")
        GERS = Request.Params("G")
        Discrep = Request.Params("DS")     'Discrep Statuses
        TType = Request.Params("TT")       'Transfer Types
        TStatus = Request.Params("TranS")  'Transfer Statuses
        FDate = Request.Params("FD")
        TDate = Request.Params("TD")
        DFQty = Request.Params("DFQ")
        DTQty = Request.Params("DTQ")
        DFAmt = Request.Params("DFA")
        DTAmt = Request.Params("DTA")
        TrackNum = Request.Params("TKN")   'Tracking number
        TransNum = Request.Params("TSN")   'transfer number

        ucHeader.lblTitle = "Inventory Control View Transfer"
        ucHeader.CurrentPage(Header.PageName.IC_Search)
        ucHeader.CurrentMode(Header.HeaderGroup.IC)

        m_strBoxId = Request("BID")  'assigning Transfer Number received from link.
        'strReturn = Request.Params("RTN")  'gets return string for IC_Results and IC_ResultsTT


        If Not IsPostBack Then
            If Request.Params("VO") = Nothing Or Request.Params("VO") = "" Then
                ViewOnly = True
                ViewState("VO") = True
            Else
                ViewOnly = Request.Params("VO")
                ViewState("VO") = Request.Params("VO")
            End If

            Call CheckViewOnly()
        End If
        GetBoxHeader()
        GetBoxItems()
        GetBoxXferGroupings()
    End Sub
    Private Sub CheckViewOnly()
        If ViewState("VO") = True Then
            'Not editable mode
            'Edit, Lost, and Re-Ship buttons visibility determined in GetHeaderData()
            'txtNotes.ReadOnly = True
            txtNotes.Visible = False
            btnUpdateStoreNotes.Visible = False
            lblVRStore.Visible = True
            ddlRStore.Visible = False
            btnVoid.Visible = False
            btnUpdate.Visible = False
            btnUpdateGERS.Visible = False
            btnReturn.Visible = True
            ucHeader.lblTitle = "Inventory Control View Transfer"

        Else
            'In editable mode
            btnUpdate.Visible = True
            btnUpdateGERS.Visible = True
            btnReturn.Visible = True
            'txtNotes.ReadOnly = False
            txtNotes.Visible = True
            btnUpdateStoreNotes.Visible = True
            lblVRStore.Visible = True 'allow change in IC_Re-Ship
            ddlRStore.Visible = False
            btnEdit.Visible = False 'in edit mode, don't need to see the button
            ucHeader.lblTitle = "Inventory Control Edit Transfer"

            If lblStatus_Value.Text = "Shipped" Then
                btnVoid.Visible = True
                btnLost.Visible = True
            Else
                btnVoid.Visible = False
                btnLost.Visible = False
            End If

            If lblStatus_Value.Text = "Shipped" Or lblStatus_Value.Text = "Received" Then
                'If Trim(lblStatus_Value.Text) = "Shipped" Or Trim(lblStatus_Value.Text) = "Received" Then
                btnReShip.Visible = True
            Else
                btnReShip.Visible = False
            End If

        End If

    End Sub
    Private Sub GetBoxHeader()
        'add functionality to employee history button.
        Call GetHeaderData()
        'btnHistory.Attributes.Add("onclick", "javascript:window.showModalDialog('EmpHistory.aspx?BID=" & m_strBoxId & "','Status History','center=1;scroll=0;status=no;dialogWidth=400px;dialogHeight=380px;');")
        btnHistory.Attributes.Add("onclick", "javascript:window.showModalDialog('EmpHistory.aspx?BID=" & m_strBoxId & "&nocache=" & Now.ToString & "','Status History','center=1;scroll=0;status=no;dialogWidth=400px;dialogHeight=380px;');")

    End Sub
    Private Sub GetBoxItems()
        'Gets SKU_NUM,received qty, shipped qty,xfer type, retail price
        Dim strSQL As String
        strSQL = "Select Box_Xfer_Item.Xfer_Type_Cd, Xfer_Type.Xfer_Desc, Box_Xfer_Item.SKU_NUM, Box_Xfer_Item.CURR, Box_Xfer_Item.VE_CD, Box_Xfer_Item.Size_CD, Box_Xfer_Item.DES1, Box_Xfer_Item.Item_Qty, Box_Xfer_Item.Received_Qty FROM Box_Xfer_Item Left Outer Join Xfer_Type On Xfer_Type.Xfer_Cd=Box_Xfer_Item.Xfer_Type_Cd "
        strSQL = strSQL & "WHERE Box_Xfer_Item.Box_Id='" & m_strBoxId & "' Order By Xfer_Desc "
        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()
        Dim cmdSearch As New SqlClient.SqlCommand(strSQL, conSearch)
        dtrSearch = cmdSearch.ExecuteReader
        dgTransferBox.DataSource = dtrSearch
        dgTransferBox.DataBind()
        dtrSearch.Close()
        conSearch.Close()
        cmdSearch.Dispose()
        intdgXferItems = dgTransferBox.Items.Count
        ViewState("XICount") = intdgXferItems
    End Sub
    Private Sub GetBoxXferGroupings()
        Dim strSQL As String
        strSQL = "Select IC_Box_Trans_Status.Xfer_Type_Cd, IC_Box_Trans_Status.Box_ID as Box_ID, Xfer_Type.Xfer_Desc as Xfer_Type,IC_Box_Trans_Status.GersStatus_cd,IC_Box_Trans_Status.Disc_Cd "
        strSQL = strSQL & "FROM IC_Box_Trans_Status Left outer Join Xfer_Type on IC_Box_Trans_Status.Xfer_Type_Cd=Xfer_Type.Xfer_Cd "
        strSQL = strSQL & " WHERE IC_Box_Trans_Status.Box_Id='" & m_strBoxId & "' "
        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()
        Dim cmdSearch As New SqlClient.SqlCommand(strSQL, conSearch)
        dtrSearch = cmdSearch.ExecuteReader
        dgXferGroups.DataSource = dtrSearch
        dgXferGroups.DataBind()
        dtrSearch.Close()
        conSearch.Close()
        cmdSearch.Dispose()
        intdgXferTypes = dgXferGroups.Items.Count
        ViewState("XGCount") = intdgXferTypes
    End Sub
    Protected Function GetBoxComments(ByVal Box_Id As String, ByVal Xfer_Type_Cd As Int32) As String
        Dim NullCheck As Boolean
        Dim strReturn As String
        Dim conGetComment As System.Data.SqlClient.SqlConnection
        Dim cmdGetComment As System.Data.SqlClient.SqlCommand
        Dim dtrGetComment As SqlClient.SqlDataReader
        Dim Test
        'Calling stored Procedure to retrieve comments.
        conGetComment = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        conGetComment.Open()
        cmdGetComment = New SqlClient.SqlCommand("spGetICComments", conGetComment)
        cmdGetComment.CommandType = CommandType.StoredProcedure
        cmdGetComment.Parameters.Add("@BID", Box_Id)
        cmdGetComment.Parameters.Add("@TransType", Xfer_Type_Cd)

        dtrGetComment = cmdGetComment.ExecuteReader

        If dtrGetComment.Read Then
            Test = (dtrGetComment("Comment"))
        End If

        NullCheck = IsDBNull(Test)
        'Checking to see if Comment exists.  Pending on yes or no, a diff't picture will show
        If NullCheck = True Then
            strReturn = "Images/Commentsblank.jpg"
        Else
            strReturn = "Images/CommentsExist.jpg"
        End If

        conGetComment.Close()
        dtrGetComment.Close()
        cmdGetComment.Dispose()
        Return strReturn

    End Function
    Protected Function GetCommentReturnQuery() As String
        'Provides return query from Comments page.
        Dim strCommentsReturn As String 'Return query for comments page.
        'Dim strPage As String
        'strPage = "IC_EditTransfer.aspx"
        'strCommentsReturn = strPage & "?BID=" & m_strBoxId
        strCommentsReturn = "IC_EditTransfer.aspx?BID=" & m_strBoxId
        Return strCommentsReturn
    End Function
    Protected Function GetReturnQuery() As String
        Dim strReturn2
        ''This is the querystring for the IC_Results & IC_ResultsTT pages.
        'strReturn2 = strReturn
        'If strReturn2 = Nothing Or strReturn2 = "" Then
        ''if coming back from IC_comments page.
        'strReturn2 = Request.Params("RTN2")
        'End If
        If Request.Params("RTN") <> Nothing And Request.Params("RTN") <> "" Then
            'Return to IC_Results
            strReturn2 = "IC_Results.aspx?" & "&FS=" & FStore & "&FDis=" & FDis & "&FReg=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TReg=" & TReg & "&TDis=" & TDis & " & NTI = " & NoTInternet & " & G = " & GERS & " & DS = " & Discrep & " & TT = " & TType & " & TranS = " & TStatus & " & FD = " & FDate & " & TD = " & TDate & " & DFQ = " & DFQty & " & DTQ = " & DTQty & " & DFA = " & DFAmt & " & DTA = " & DTAmt & " & TKN = " & TrackNum & " & TSN = " & TransNum
        Else
            'Return to IC_ResultsTT
            If GBID = Nothing Then
                strReturn2 = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum
            Else
                strReturn2 = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID
            End If
        End If
        Return (strReturn2)
    End Function
    Protected Function GetVOValue() As Boolean
        'This is for going between IC_Comments page and this page.  
        Dim ViewValue As Boolean
        ViewValue = ViewState("VO")
        Return ViewValue
    End Function
    Private Sub GetHeaderData()
        'This retrieves the Transfer Number, UPS, Sending Store Num, Receiving Store Num, Sending Store Name,  
        'Receiving Store Name,Current Box Status (cd and description),employee id,employee name, shipment date, 
        'received date, created date, modified date
        If lblStatus_Value.Text <> Nothing Or lblStatus_Value.Text <> "" Then
            strNotes = txtNotes.Text
            ViewState("StoreNotes") = strNotes
        End If

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        objConnection.Open()
        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeader", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input
        BoxIdParam.Value = m_strBoxId
        objDataReader = objCommand.ExecuteReader()

        If objDataReader.Read() Then
            If Not IsDBNull(objDataReader("Notes")) Then
                'txtNotes.Text = objDataReader("Notes")
                lblStoreNotes.Text = objDataReader("Notes")
                strNotes = objDataReader("Notes")
            End If

            If Not IsDBNull(objDataReader("Tracking_Num")) Then
                hlUPS.NavigateUrl = Replace(objDataReader("TrackingURL"), "{0}", objDataReader("Tracking_Num"))
            End If
            If Not IsDBNull(objDataReader("Tracking_Num")) Then
                lblUPS_Value.Text = objDataReader("Tracking_Num")
            End If

            If Not IsDBNull(objDataReader("Emp_FullName")) Then
                lblEmp_Value.Text = objDataReader("Emp_FullName")
            End If

            lblTransferId_Value.Text = objDataReader("Box_Id")
            lblStatus_Value.Text = objDataReader("Status_Desc")

            If (lblStatus_Value.Text = "Voided" Or lblStatus_Value.Text = "Employee Error" Or lblStatus_Value.Text = "Carrier Delivery Error" Or lblStatus_Value.Text = "Incorrect Carrier Error" Or lblStatus_Value.Text = "Packing") Then
                btnEdit.Visible = False
            Else
                If ViewState("VO") = False Then
                    btnEdit.Visible = False
                Else
                    btnEdit.Visible = True
                End If
            End If

            If (lblStatus_Value.Text = "Shipped" Or lblStatus_Value.Text = "Received") And ViewState("VO") = False Then
                btnReShip.Visible = True
            Else
                btnReShip.Visible = False
            End If

            If (lblStatus_Value.Text = "Shipped" And ViewState("VO") = False) Then
                btnLost.Visible = True
                btnVoid.Visible = True
            Else
                btnLost.Visible = False
                btnVoid.Visible = False
            End If

            intRStore = objDataReader("Receiving_Store_Cd")
            lblInk_Value.Text = GetInkTags(intRStore)
            lblSStore_Value.Text = Right("0000" & objDataReader("SStoreNum"), 4)
            intSStore = objDataReader("Sending_Store_Cd")
            lblDisQuant_Value.Text = GetDiscrepQty(m_strBoxId)
            lblDisAmount_Value.Text = GetDiscrepAmt(m_strBoxId)

            If Not IsDBNull(objDataReader("RStore")) Then
                intRStore = objDataReader("Receiving_Store_Cd")
                lblVRStore.Text = intRStore
                FillStoreDDL(ddlRStore, intRStore, "asFillTStore")
            End If


            If Not IsDBNull(objDataReader("Shipment_Date")) Then
                lblShipDate_Value.Text = FormatDateTime(objDataReader("Shipment_Date"), DateFormat.ShortDate)
            End If

        End If

        objConnection.Close()
        objDataReader.Close()
        objCommand.Dispose()

        lblNewTransID.Visible = False
        lblOldTransID.Visible = False
        lblOldTransfer.Visible = False
        lblNewTransfer.Visible = False
        lblReShipInfo.Visible = False
        Call CheckReShipInfo()

    End Sub
    Private Sub CheckReShipInfo()
        'This checks if this transfer is related in somehow to having been reshipped.  
        'If so, it provides that information in the box header.

        'This checks if the current transfer id is an old transfer id and has been re-shipped.
        Dim objConnection1 As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection1.Open()
        Dim objCommand1 As New SqlClient.SqlCommand("spICReShipNew", objConnection1)
        objCommand1.CommandType = CommandType.StoredProcedure
        Dim TransId As SqlClient.SqlParameter = objCommand1.Parameters.Add("@TransId", SqlDbType.VarChar, 14)
        Dim NewTransId As SqlClient.SqlParameter = objCommand1.Parameters.Add("@NewTransId", SqlDbType.VarChar, 14)

        TransId.Direction = ParameterDirection.Input
        NewTransId.Direction = ParameterDirection.Output
        TransId.Value = lblTransferId_Value.Text

        objCommand1.ExecuteNonQuery()
        objConnection1.Close()
        objCommand1.Dispose()
        If IsDBNull(NewTransId.Value) Then
            'Return ""
            lblNewTransID.Visible = False
            lblNewTransfer.Visible = False
        Else
            'Return TagsParam.Value
            lblNewTransfer.Visible = True
            lblNewTransID.Visible = True
            lblNewTransID.Text = NewTransId.Value
        End If

        'This checks if the current transfer is a new transfer id 
        'that is related to an old transfer id that was re-shipped.
        Dim objConnection2 As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection2.Open()
        Dim objCommand2 As New SqlClient.SqlCommand("spICReShipOld", objConnection2)
        objCommand2.CommandType = CommandType.StoredProcedure
        Dim TransId2 As SqlClient.SqlParameter = objCommand2.Parameters.Add("@TransId", SqlDbType.VarChar, 14)
        Dim NewTransId2 As SqlClient.SqlParameter = objCommand2.Parameters.Add("@NewTransId", SqlDbType.VarChar, 14)

        TransId2.Direction = ParameterDirection.Input
        NewTransId2.Direction = ParameterDirection.Output
        TransId2.Value = lblTransferId_Value.Text

        objCommand2.ExecuteNonQuery()
        objConnection2.Close()
        objCommand2.Dispose()
        If IsDBNull(NewTransId2.Value) Then
            'Return ""
            lblOldTransID.Visible = False
            lblOldTransfer.Visible = False
        Else
            'Return TagsParam.Value
            lblOldTransfer.Visible = True
            lblOldTransID.Visible = True
            lblOldTransID.Text = NewTransId2.Value
        End If

        'Checks to see whether or not to make the label ReShip Info visible or not.
        If lblOldTransID.Visible = True Or lblNewTransID.Visible = True Then
            lblReShipInfo.Visible = True
        Else
            lblReShipInfo.Visible = False
        End If

    End Sub
    Private Sub dgXferGroups_ItemDataBound(ByVal s As Object, ByVal e As DataGridItemEventArgs) Handles dgXferGroups.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim myGersDropDown As DropDownList
            Dim myDiscDropDown As DropDownList
            Dim myorigGers As DropDownList
            Dim myorigDiscrep As DropDownList
            Dim lblViewGers As Label
            Dim lblViewDiscrep As Label
            myDiscDropDown = CType(e.Item.FindControl("ddlDiscrepStatus"), DropDownList)
            myDiscDropDown.SelectedIndex = myDiscDropDown.Items.IndexOf(myDiscDropDown.Items.FindByValue(e.Item.DataItem("Disc_CD")))
            myorigDiscrep = CType(e.Item.FindControl("ddlorigDiscrep"), DropDownList)
            myorigDiscrep.SelectedIndex = myDiscDropDown.Items.IndexOf(myDiscDropDown.Items.FindByValue(e.Item.DataItem("Disc_CD")))
            myGersDropDown = CType(e.Item.FindControl("ddlGersStatus"), DropDownList)
            myGersDropDown.SelectedIndex = myGersDropDown.Items.IndexOf(myGersDropDown.Items.FindByValue(e.Item.DataItem("GERSSTATUS_CD")))
            myorigGers = CType(e.Item.FindControl("ddlorigGers"), DropDownList)
            myorigGers.SelectedIndex = myGersDropDown.Items.IndexOf(myGersDropDown.Items.FindByValue(e.Item.DataItem("GERSSTATUS_CD")))
            lblViewGers = CType(e.Item.FindControl("lblVGers"), Label)
            lblViewGers.Text = myGersDropDown.SelectedItem.Text
            lblViewDiscrep = CType(e.Item.FindControl("lblVDiscrep"), Label)
            lblViewDiscrep.Text = myDiscDropDown.SelectedItem.Text

            If ViewState("VO") = True Then
                lblViewDiscrep.Visible = True
                lblViewGers.Visible = True
                myGersDropDown.Visible = False
                myDiscDropDown.Visible = False
            Else
                lblViewDiscrep.Visible = False
                lblViewGers.Visible = False
                myGersDropDown.Visible = True
                myDiscDropDown.Visible = True
            End If

        End If
        ViewState("XTCount") = dgXferGroups.Items.Count
    End Sub
    Private Sub dgTransferBox_ItemDataBound(ByVal s As Object, ByVal e As DataGridItemEventArgs) Handles dgTransferBox.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim intTType As Integer
            Dim strSQLG As String
            Dim intGStatus As Integer
            Dim myDropDown As DropDownList
            Dim myOrig As DropDownList
            Dim QTYShipped As TextBox
            Dim QTYReceived As TextBox
            Dim lblTType As Label
            Dim lblshipqty As Label
            Dim lblrecqty As Label

            myDropDown = CType(e.Item.FindControl("ddlTType"), DropDownList)
            myDropDown.SelectedIndex = myDropDown.Items.IndexOf(myDropDown.Items.FindByValue(e.Item.DataItem("Xfer_Type_Cd")))
            myOrig = CType(e.Item.FindControl("ddlorig"), DropDownList)
            myOrig.SelectedIndex = myOrig.Items.IndexOf(myOrig.Items.FindByValue(e.Item.DataItem("Xfer_Type_Cd")))
            QTYShipped = CType(e.Item.FindControl("txtItemQty"), TextBox)
            lblshipqty = CType(e.Item.FindControl("lblorigshipqty"), Label)
            lblshipqty.Text = QTYShipped.Text  'assigning text to label
            lblshipqty.Visible = True
            QTYShipped.Visible = False
            QTYReceived = CType(e.Item.FindControl("txtReceivedQty"), TextBox)
            lblrecqty = CType(e.Item.FindControl("lblqtyreceived"), Label)
            lblrecqty.Text = QTYReceived.Text
            lblTType = CType(e.Item.FindControl("lblVType"), Label)
            lblTType.Text = myDropDown.SelectedItem.Text


            Dim conGetGers As New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            intTType = myDropDown.SelectedItem.Value
            strSQLG = "Select GersStatus_Cd From IC_Box_Trans_Status where Box_Id='" & m_strBoxId & "' AND Xfer_Type_Cd=" & intTType & " "
            Dim cmdSearch As New SqlClient.SqlCommand(strSQLG, conGetGers)
            conGetGers.Open()

            Dim dreader As SqlClient.SqlDataReader = cmdSearch.ExecuteReader
            'Return dreader
            While dreader.Read
                intGStatus = dreader("gersstatus_Cd")
            End While
            conGetGers.Close()
            dreader.Close()
            cmdSearch.Dispose()
            If ViewState("VO") = True Then
                'QTYShipped.ReadOnly = True
                QTYReceived.Visible = False
                lblrecqty.Visible = True
                lblTType.Visible = True
                myDropDown.Visible = False
            Else
                'If lblVRStore.Text = 9999 Or lblVRStore.Text = 5990 Then
                'QTYReceived.Visible = True
                'lblrecqty.Visible = False
                'Else
                'QTYReceived.Visible = False
                'lblrecqty.Visible = True
                'End If
                'QTYShipped.ReadOnly = False
                'QTYReceived.ReadOnly = False
                If lblStatus_Value.Text = "Received" Then
                    QTYReceived.Visible = True
                    lblrecqty.Visible = False
                    lblTType.Visible = False
                    myDropDown.Visible = True
                ElseIf lblStatus_Value.Text = "Shipped" And lblSStore_Value.Text <> "9999" Then
                    'If store ships WAN to NON_WAN, IC has to accept the quantities for them.
                    QTYReceived.Visible = True
                    lblrecqty.Visible = False
                    lblTType.Visible = True
                    myDropDown.Visible = False
                Else
                    QTYReceived.Visible = False
                    lblrecqty.Visible = True
                    lblTType.Visible = True
                    myDropDown.Visible = False
                End If


            End If

            If intGStatus <> 1 Then
                'Gers Status is not open so disable ddlTType and QTY boxes.
                myDropDown.Enabled = False
                'QTYShipped.Enabled = False
                QTYReceived.Enabled = False
            ElseIf intGStatus = 1 Then
                'Fill item's TType ddl with all TTypes that are open or not used in this transfer.
                Dim DInclude As String
                DInclude = "("
                Dim conDFillTType As System.Data.SqlClient.SqlConnection
                conDFillTType = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                strSQLG = "Select Xfer_Type_Cd From IC_Box_Trans_Status where Box_Id='" & m_strBoxId & "' AND GersStatus_Cd <> 1 "
                Dim cmdDFillTType As New SqlClient.SqlCommand(strSQLG, conDFillTType)
                conDFillTType.Open()
                Dim dtreader As SqlClient.SqlDataReader = cmdDFillTType.ExecuteReader

                Do While dtreader.Read()
                    myDropDown.Items.Remove(myDropDown.Items.FindByValue(dtreader("Xfer_Type_Cd")))
                Loop
                conDFillTType.Close()
                dtreader.Close()
                cmdDFillTType.Dispose()
            End If
        End If
        ViewState("XICount") = dgTransferBox.Items.Count
    End Sub
    Public Function FillGers() As SqlClient.SqlDataReader

        'This function fills the Gers DropDown Lists.
        Dim SQL As String
        Dim dtG As DataTable = New DataTable()
        SQL = "Select * From GersStatus_Type Where Active=1 "
        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
        conSearch.Open()
        Dim dreader As SqlClient.SqlDataReader = cmdSearch.ExecuteReader(CommandBehavior.CloseConnection)

        Return dreader
        conSearch.Close()
        dreader.Close()
        cmdSearch.Dispose()
    End Function
    Public Function FillDiscrep(ByVal Box_Id As String, ByVal Xfer_Type_Cd As Int32) As SqlClient.SqlDataReader
        'This function fills the Discrepancy DropDown Lists.
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()
        Dim objCommand As New SqlClient.SqlCommand("spCheckDiscrepFlag", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@BoxID", SqlDbType.VarChar, 14)
        Dim XferTypeParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@XferType", SqlDbType.SmallInt)
        Dim DFlagParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Flag", SqlDbType.SmallInt)
        BoxIdParam.Direction = ParameterDirection.Input
        XferTypeParam.Direction = ParameterDirection.Input
        DFlagParam.Direction = ParameterDirection.Output
        BoxIdParam.Value = Box_Id
        XferTypeParam.Value = Xfer_Type_Cd
        objCommand.ExecuteNonQuery()
        objConnection.Close()
        objCommand.Dispose()

        If IsDBNull(DFlagParam.Value) Then
            Dim SQL As String
            Dim dtG As DataTable = New DataTable()
            SQL = "Select * From Discrep_Type Where Active=1 "
            Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
            conSearch.Open()
            Dim dreader As SqlClient.SqlDataReader = cmdSearch.ExecuteReader(CommandBehavior.CloseConnection)
            Return dreader
            conSearch.Close()
            dreader.Close()
            cmdSearch.Dispose()
        ElseIf DFlagParam.Value = 0 Then
            'Return TagsParam.Value
            Dim SQL As String
            Dim dtG As DataTable = New DataTable()
            SQL = "Select * From Discrep_Type Where Active=1 and Disc_Cd <> 5 "
            Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
            conSearch.Open()
            Dim dreader As SqlClient.SqlDataReader = cmdSearch.ExecuteReader(CommandBehavior.CloseConnection)
            Return dreader
            conSearch.Close()
            dreader.Close()
            cmdSearch.Dispose()
        ElseIf DFlagParam.Value = 1 Then
            Dim SQL As String
            Dim dtG As DataTable = New DataTable()
            SQL = "Select * From Discrep_Type Where Active=1 and Disc_Cd <> 5 and Disc_Cd <> 1 "
            Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
            conSearch.Open()
            Dim dreader As SqlClient.SqlDataReader = cmdSearch.ExecuteReader(CommandBehavior.CloseConnection)
            Return dreader
            conSearch.Close()
            dreader.Close()
            cmdSearch.Dispose()
        End If

        'Dim SQL As String
        'Dim dtG As DataTable = New DataTable()
        'SQL = "Select * From Discrep_Type Where Active=1 "
        'Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        'Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
        'conSearch.Open()
        'Dim dreader As SqlClient.SqlDataReader = cmdSearch.ExecuteReader(CommandBehavior.CloseConnection)
        'Return dreader
        'conSearch.Close()
        'dreader.Close()
        'cmdSearch.Dispose()
    End Function
    Public Function FillType() As SqlClient.SqlDataReader
        'This function fills the Transfer Type Drop Down list

        Dim SQL As String
        Dim dtG As DataTable = New DataTable()
        SQL = "Select * From Xfer_Type Where Active=1 "
        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
        conSearch.Open()
        Dim dreader As SqlClient.SqlDataReader = cmdSearch.ExecuteReader(CommandBehavior.CloseConnection)
        Return dreader
        conSearch.Close()
        dreader.Close()
        cmdSearch.Dispose()
    End Function
    Private Sub FillStoreDDL(ByRef StoreDDL As System.Web.UI.WebControls.DropDownList, ByVal intSelected As Int32, ByVal strStoredProcedure As String)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

        StoreDDL.DataSource = objDataReader
        StoreDDL.DataBind()

        objItem.Text = "Select A Store"
        objItem.Value = 0

        StoreDDL.Items.Insert(0, objItem)
        StoreDDL.SelectedIndex = StoreDDL.Items.IndexOf(StoreDDL.Items.FindByValue(intSelected.ToString))

        objConnection.Close()
        objDataReader.Close()
        objCommand.Dispose()
    End Sub
    Private Function GetInkTags(ByVal InRStore As Int32) As String
        'This retrieves the ink tags used in the receiving store
        'This function is called in the GetHeader() sub 
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spICGetInkTags", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim RStoreIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Store_Cd", SqlDbType.SmallInt)
        Dim TagsParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Tags", SqlDbType.VarChar, 5)

        RStoreIdParam.Direction = ParameterDirection.Input
        TagsParam.Direction = ParameterDirection.Output
        RStoreIdParam.Value = InRStore

        objCommand.ExecuteNonQuery()

        objConnection.Close()
        objCommand.Dispose()
        If IsDBNull(TagsParam.Value) Then
            Return ""
        Else
            'Return TagsParam.Value.GetType.ToString
            Return TagsParam.Value
        End If
    End Function
    Private Function GetDiscrepQty(ByVal m_strBoxId As String) As Integer
        'Get discrepancy QTY for header
        Dim conGetQTY As System.Data.SqlClient.SqlConnection
        Dim cmdGetQTY As System.Data.SqlClient.SqlCommand
        conGetQTY = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdGetQTY = New SqlClient.SqlCommand("spGetDiscrepQTY", conGetQTY)
        cmdGetQTY.CommandType = CommandType.StoredProcedure
        cmdGetQTY.Parameters.Add("@BID", m_strBoxId)
        parmDQTY = cmdGetQTY.Parameters.Add("@DQTY", SqlDbType.VarChar)
        parmDQTY.Size = 40
        parmDQTY.Direction = ParameterDirection.Output
        conGetQTY.Open()
        cmdGetQTY.ExecuteNonQuery()
        DiscrepQTY = cmdGetQTY.Parameters("@DQTY").Value
        conGetQTY.Close()
        cmdGetQTY.Dispose()
        Return DiscrepQTY
    End Function
    Private Function GetDiscrepAmt(ByVal m_strBoxId As String) As Decimal
        'Get discrepancy QTY for header
        Dim conGetAMT As System.Data.SqlClient.SqlConnection
        Dim cmdGetAMT As System.Data.SqlClient.SqlCommand
        conGetAMT = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdGetAMT = New SqlClient.SqlCommand("spGetDiscrepAMT", conGetAMT)
        cmdGetAMT.CommandType = CommandType.StoredProcedure
        cmdGetAMT.Parameters.Add("@BID", m_strBoxId)
        parmDAMT = cmdGetAMT.Parameters.Add("@DAMT", SqlDbType.Money)
        parmDAMT.Size = 40
        parmDAMT.Direction = ParameterDirection.Output
        conGetAMT.Open()
        cmdGetAMT.ExecuteNonQuery()
        DiscrepAMT = Format((cmdGetAMT.Parameters("@DAMT").Value), "C")
        conGetAMT.Close()
        cmdGetAMT.Dispose()
        Return DiscrepAMT
    End Function
    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click

        ViewState("VO") = False
        objTransferLock.UnlockTransfer(m_strBoxId) 'Unlock box

        Dim strReturnResults As String
        strReturnResults = Request.Params("RFC")
        If strReturnResults <> Nothing And strReturnResults <> "" Then
            'if had to get return query from comments page.
            Dim RTNPage As String
            Dim strReturn2
            RTNPage = "IC_ResultsTT"
            strReturn2 = Request.Params("RFC") 'return query for getting from IC_EditTransfer to ResultsTT
            If InStr(1, strReturn2, RTNPage) > 0 Then
                'Go back to IC_ResultsTT
                strReturnResults = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID
            Else
                'Go Back to IC_Result
                strReturnResults = "IC_Results.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum
            End If
            Response.Redirect(strReturnResults)
        End If

        If Request.Params("RTN") <> Nothing And Request.Params("RTN") <> "" Then
            'Return to IC_Results
            Response.Redirect("IC_Results.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum)
        Else
            'Return to IC_ResultsTT
            If GBID = Nothing Then
                Response.Redirect("IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum)
            Else
                Response.Redirect("IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID)
            End If
        End If

    End Sub
    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        'Call UpdateReceivingStore()  'update receiving store number if needed.
        'Call GetBoxItems()

        Dim ddlTType As DropDownList 'user changeable Transfer Type DDL
        Dim ddlOrigTType As DropDownList  'orig value of Transfer Type DDL
        Dim txtQTYShip As TextBox    'QTY shipped
        Dim txtQTYRec As TextBox     'QTY received
        Dim txtorigQTYShip As TextBox    'Original QTY shipped
        Dim txtorigQTYRec As TextBox     'Original QTY received
        Dim intQTYShip As Integer
        Dim intQTYRec As Integer
        Dim intorigQTYShip As Integer  'Original QTY shipped
        Dim intorigQTYRec As Integer   'Original QTY received
        Dim intOrigTType As Integer
        Dim intNewTType As Integer
        Dim intDiscrepQty As Integer 'Discrepancy QTY
        Dim strSKU As String         'SKU
        Dim Price As Decimal         'Price of SKU (QTY of 1)
        Dim DiscrepAmt As Decimal    'Dollar amt of Discrepancy
        Dim I As Integer             'Counter



        'If dgTransferBox.Items.Count > 0 Then
        intdgXferItems = ViewState("XICount")

        For I = 0 To (intdgXferItems - 1)

            strSKU = dgTransferBox.Items(I).Cells(0).Text
            Price = CType(dgTransferBox.Items(I).Cells(3).Text, Decimal)
            ddlTType = CType(dgTransferBox.Items(I).FindControl("ddlTType"), DropDownList)
            ddlOrigTType = CType(dgTransferBox.Items(I).FindControl("ddlorig"), DropDownList)
            intOrigTType = ddlOrigTType.SelectedItem.Value
            intNewTType = ddlTType.SelectedItem.Value
            txtQTYShip = CType(dgTransferBox.Items(I).FindControl("txtItemQty"), TextBox)
            txtQTYRec = CType(dgTransferBox.Items(I).FindControl("txtReceivedQty"), TextBox)
            'txtorigQTYShip = CType(dgTransferBox.Items(I).FindControl("txtorigQS"), TextBox)
            txtorigQTYRec = CType(dgTransferBox.Items(I).FindControl("txtorigQR"), TextBox)


            'Update QTY's and Discrep QTY and AMT
            If (IsNumeric(txtQTYShip.Text) = True And IsNumeric(txtQTYRec.Text) = True) Then
                intQTYShip = CInt(txtQTYShip.Text)
                intQTYRec = CInt(txtQTYRec.Text)
                intorigQTYRec = CInt(txtorigQTYRec.Text)


                If (intQTYRec <> intorigQTYRec) Then
                    strHistoryNotes = "Received quantities were changed."
                    Call UpdateICHistory(m_strBoxId, strHistoryNotes)
                End If

                If (lblStatus_Value.Text = "Shipped" And lblVRStore.Text <> "9999" And intorigQTYRec = 0) Then
                    'Change box status to Received.
                    Dim ModifiedDate As DateTime
                    Dim BoxStatus As Int32
                    BoxStatus = 4
                    ModifiedDate = Now

                    Dim conUpdateStatus As System.Data.SqlClient.SqlConnection
                    Dim cmdUpdateStatus As System.Data.SqlClient.SqlCommand
                    conUpdateStatus = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                    cmdUpdateStatus = New SqlClient.SqlCommand("spUpdateBoxHdrStatus", conUpdateStatus)
                    cmdUpdateStatus.CommandType = CommandType.StoredProcedure
                    cmdUpdateStatus.Parameters.Add("@BID", m_strBoxId)
                    cmdUpdateStatus.Parameters.Add("@Status", BoxStatus)
                    cmdUpdateStatus.Parameters.Add("@Modified", ModifiedDate)
                    conUpdateStatus.Open()
                    cmdUpdateStatus.ExecuteNonQuery()
                    conUpdateStatus.Close()
                    cmdUpdateStatus.Dispose()

                    strHistoryNotes = "IC received the transfer on the store's behalf."
                    Call UpdateICHistory(m_strBoxId, strHistoryNotes)
                End If

                If (intQTYRec >= 0) Then
                    'Don't allow Negatives
                    intDiscrepQty = Math.Abs(intQTYShip - intQTYRec) 'Getting disrepancy qty
                    DiscrepAmt = (intDiscrepQty * Price)   'Calc Discrep $ Amt
                    Dim conUpdateDisc As System.Data.SqlClient.SqlConnection
                    Dim cmdUpdateDisc As System.Data.SqlClient.SqlCommand
                    conUpdateDisc = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                    cmdUpdateDisc = New SqlClient.SqlCommand("spICUPDiscrep", conUpdateDisc)
                    cmdUpdateDisc.CommandType = CommandType.StoredProcedure
                    cmdUpdateDisc.Parameters.Add("@BID", m_strBoxId)
                    cmdUpdateDisc.Parameters.Add("@XferCd", intOrigTType)
                    cmdUpdateDisc.Parameters.Add("@SKU", strSKU)
                    'cmdUpdateDisc.Parameters.Add("@IQTY", intQTYShip)
                    cmdUpdateDisc.Parameters.Add("@RecQTY", intQTYRec)
                    cmdUpdateDisc.Parameters.Add("@DiscrepQty", intDiscrepQty)
                    cmdUpdateDisc.Parameters.Add("@DiscrepAmt", DiscrepAmt)
                    conUpdateDisc.Open()
                    cmdUpdateDisc.ExecuteNonQuery()
                    conUpdateDisc.Close()
                    cmdUpdateDisc.Dispose()
                End If
            Else
            End If

            'Update Transfer Group Changes.
            If intOrigTType <> intNewTType Then
                'User Changed Transfer Type for this SKU
                Call UpdateTransferType(m_strBoxId, strSKU, intOrigTType, intNewTType)
            End If
        Next

        Call CheckViewOnly()
        GetBoxHeader()
        GetBoxItems()
        GetBoxXferGroupings()
        'End If
    End Sub
    Private Sub UpdateTransferType(ByVal BoxId As String, ByVal SKU As String, ByVal OrigTType As Integer, ByVal NewTType As Integer)
        'Update to New Transfer Type
        'NOTE: THIS DOES NOT CHANGE THE DISCREP STATUS.  IC WILL MANUALLY HAVE TO CHANGE THE DISCREP STATUS FOR TTYPE UPDATES.
        Dim strConfirm As String  'If blank, one sku for this Xfer Type.
        Dim intNewTTypeConfirm As Int32   'If blank, new TType not in box already, else this TType already exists.
        Dim NullCheck As Boolean
        Dim intDiscrep As Int32

        'Checking to see if there are other skus that in this box with this original TType
        Dim conGetXfer As System.Data.SqlClient.SqlConnection
        Dim cmdGetXfer As System.Data.SqlClient.SqlCommand
        conGetXfer = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdGetXfer = New SqlClient.SqlCommand("spCheckXferTypes", conGetXfer)
        cmdGetXfer.CommandType = CommandType.StoredProcedure
        cmdGetXfer.Parameters.Add("@BID", BoxId)
        cmdGetXfer.Parameters.Add("@XferCd", OrigTType)
        cmdGetXfer.Parameters.Add("@SKU", SKU)
        parmCheck = cmdGetXfer.Parameters.Add("@Check", SqlDbType.VarChar)
        parmCheck.Size = 12
        parmCheck.Direction = ParameterDirection.Output
        conGetXfer.Open()
        cmdGetXfer.ExecuteNonQuery()
        NullCheck = IsDBNull(cmdGetXfer.Parameters("@Check").Value)
        If NullCheck = True Then
            'This is the only sku for this TType.  Delete this Transfer Type and update to new TType
            strConfirm = ""
        Else
            'More skus for this TTYpe.  Just update this sku to new TType
            strConfirm = cmdGetXfer.Parameters("@Check").Value
        End If
        conGetXfer.Close()
        cmdGetXfer.Dispose()
        'This checks to see if the New TType already exists for this box.  If not, Insert new TType into this box.
        'If it does exist, if Gers is "Open" then just update this sku to that TType.
        Dim conGetNewXfer As System.Data.SqlClient.SqlConnection
        Dim cmdGetNewXfer As System.Data.SqlClient.SqlCommand
        conGetNewXfer = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdGetNewXfer = New SqlClient.SqlCommand("spICGetBoxTransferTypes", conGetNewXfer)
        cmdGetNewXfer.CommandType = CommandType.StoredProcedure
        cmdGetNewXfer.Parameters.Add("@BID", BoxId)
        cmdGetNewXfer.Parameters.Add("@TType", NewTType)
        parmCheckNewTType = cmdGetNewXfer.Parameters.Add("@Gers", SqlDbType.TinyInt)
        parmCheckNewTType.Size = 2
        parmCheckNewTType.Direction = ParameterDirection.Output
        conGetNewXfer.Open()
        cmdGetNewXfer.ExecuteNonQuery()
        NullCheck = IsDBNull(cmdGetNewXfer.Parameters("@Gers").Value)
        If NullCheck = True Then
            'This TType does not already exist for this box
            intNewTTypeConfirm = 0
        Else
            'This TType does already exist.  Check GERS status.
            intNewTTypeConfirm = cmdGetNewXfer.Parameters("@Gers").Value
        End If
        conGetNewXfer.Close()
        cmdGetNewXfer.Dispose()
        If (strConfirm = "" And intNewTTypeConfirm = 0) Then
            'Only SKU for Old Type
            'New TType does not already exist for this box

            strHistoryNotes = "A SKU's Transfer Type was changed."
            Call UpdateICHistory(m_strBoxId, strHistoryNotes)

            'This updates IC_Box_Trans_Status to new Transfer Type, sets Gers to "Open"
            Dim conUpdateOldTType As System.Data.SqlClient.SqlConnection
            Dim cmdUpdateOldTType As System.Data.SqlClient.SqlCommand
            conUpdateOldTType = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            cmdUpdateOldTType = New SqlClient.SqlCommand("spICUpdateOldTType", conUpdateOldTType)
            cmdUpdateOldTType.CommandType = CommandType.StoredProcedure
            cmdUpdateOldTType.Parameters.Add("@BID", m_strBoxId)
            cmdUpdateOldTType.Parameters.Add("@OldTType", OrigTType)
            cmdUpdateOldTType.Parameters.Add("@NewTType", NewTType)
            conUpdateOldTType.Open()
            cmdUpdateOldTType.ExecuteNonQuery()
            conUpdateOldTType.Close()
            cmdUpdateOldTType.Dispose()
            'This updates Box_Xfer_Item's SKU to the new TType
            Call UpdateTTypeItems(m_strBoxId, SKU, OrigTType, NewTType)

        ElseIf (strConfirm = "" And intNewTTypeConfirm = 1) Then
            'Only SKU for Old Type
            'New TType does already exist for this box

            strHistoryNotes = "A SKU's Transfer Type was changed."
            Call UpdateICHistory(m_strBoxId, strHistoryNotes)

            'This updates Box_Xfer_Item's SKU to the new TType
            Call UpdateTTypeItems(m_strBoxId, SKU, OrigTType, NewTType)

            'This the old Transfer Type that doesn't exist anymore.
            Dim conDelTType As System.Data.SqlClient.SqlConnection
            Dim cmdDelTType As System.Data.SqlClient.SqlCommand
            conDelTType = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            cmdDelTType = New SqlClient.SqlCommand("spICDelOldTType", conDelTType)
            cmdDelTType.CommandType = CommandType.StoredProcedure
            cmdDelTType.Parameters.Add("@BID", m_strBoxId)
            cmdDelTType.Parameters.Add("@OldTType", OrigTType)
            conDelTType.Open()
            cmdDelTType.ExecuteNonQuery()
            conDelTType.Close()
            cmdDelTType.Dispose()
        ElseIf (strConfirm <> "" And intNewTTypeConfirm = 0) Then
            'Not only SKU for Old Type
            'New TType does not already exist for this box

            strHistoryNotes = "A SKU's Transfer Type was changed."
            Call UpdateICHistory(m_strBoxId, strHistoryNotes)

            'This updates Box_Xfer_Item's SKU to the new TType
            Call UpdateTTypeItems(m_strBoxId, SKU, OrigTType, NewTType)

            'Get Discrep Status, so can insert into new TType
            Dim conGetDisc As System.Data.SqlClient.SqlConnection
            Dim cmdGetDisc As System.Data.SqlClient.SqlCommand
            conGetDisc = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            cmdGetDisc = New SqlClient.SqlCommand("spICGetDiscrep", conGetDisc)
            cmdGetDisc.CommandType = CommandType.StoredProcedure
            cmdGetDisc.Parameters.Add("@BID", BoxId)
            cmdGetDisc.Parameters.Add("@XferCd", OrigTType)
            parmDiscrep = cmdGetDisc.Parameters.Add("@Disc", SqlDbType.TinyInt)
            parmDiscrep.Size = 1
            parmDiscrep.Direction = ParameterDirection.Output
            conGetDisc.Open()
            cmdGetDisc.ExecuteNonQuery()
            NullCheck = IsDBNull(cmdGetDisc.Parameters("@Disc").Value)
            If NullCheck = True Then
                'This is the only sku for this TType.  Delete this Transfer Type and update to new TType
                intDiscrep = 0
            Else
                'More skus for this TTYpe.  Just update this sku to new TType
                intDiscrep = cmdGetDisc.Parameters("@Disc").Value
            End If
            conGetDisc.Close()
            cmdGetDisc.Dispose()
            'This inserts new TType for this box into IC_Box_Trans_Status, sets Gers to "Open"
            Dim conInsertTType As System.Data.SqlClient.SqlConnection
            Dim cmdInsertTType As System.Data.SqlClient.SqlCommand
            conInsertTType = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            cmdInsertTType = New SqlClient.SqlCommand("spICInsertTType", conInsertTType)
            cmdInsertTType.CommandType = CommandType.StoredProcedure
            cmdInsertTType.Parameters.Add("@BID", m_strBoxId)
            cmdInsertTType.Parameters.Add("@NewTType", NewTType)
            cmdInsertTType.Parameters.Add("@Disc", intDiscrep)
            conInsertTType.Open()
            cmdInsertTType.ExecuteNonQuery()
            conInsertTType.Close()
            cmdInsertTType.Dispose()
        ElseIf (strConfirm <> "" And intNewTTypeConfirm = 1) Then
            'Not only SKU for Old Type
            'New TType does already exist for this box

            strHistoryNotes = "A SKU's Transfer Type was changed."
            Call UpdateICHistory(m_strBoxId, strHistoryNotes)

            'This updates Box_Xfer_Item's SKU to the new TType
            Call UpdateTTypeItems(m_strBoxId, SKU, OrigTType, NewTType)
        End If
        'Call CheckViewOnly()
        'GetBoxHeader()
        'GetBoxItems()
        'GetBoxXferGroupings()
    End Sub
    Private Sub UpdateTTypeItems(ByVal BoxId As String, ByVal SKU As String, ByVal OrigTType As Integer, ByVal NewTType As Integer)
        'This updates Box_Xfer_Item's SKU to the new TType
        Dim conUpdateItemTType As System.Data.SqlClient.SqlConnection
        Dim cmdUpdateItemTType As System.Data.SqlClient.SqlCommand
        conUpdateItemTType = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdUpdateItemTType = New SqlClient.SqlCommand("spICUpdateTTypeItem", conUpdateItemTType)
        cmdUpdateItemTType.CommandType = CommandType.StoredProcedure
        cmdUpdateItemTType.Parameters.Add("@BID", m_strBoxId)
        cmdUpdateItemTType.Parameters.Add("@SKU", SKU)
        cmdUpdateItemTType.Parameters.Add("@OldTType", OrigTType)
        cmdUpdateItemTType.Parameters.Add("@NewTType", NewTType)
        conUpdateItemTType.Open()
        cmdUpdateItemTType.ExecuteNonQuery()
        conUpdateItemTType.Close()
        cmdUpdateItemTType.Dispose()
        Dim Notes As String
        Notes = "Transfer Type was changed."
        UpdateICHistory(m_strBoxId, Notes)
    End Sub
    Private Sub btnUpdateGERS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateGERS.Click

        'Call UpdateReceivingStore() 'update receiving store number if needed.
        Dim I As Integer
        Dim strTType As String
        Dim ddlGers As DropDownList
        Dim ddlDiscrep As DropDownList
        Dim ddlorigGers As DropDownList
        Dim ddlorigDiscrep As DropDownList
        Dim intGersStatus As Integer
        Dim strGers As String
        Dim intDiscrep As Integer
        Dim intorigGers As Integer
        Dim intorigDiscrep As Integer
        Dim parmXferCd As SqlClient.SqlParameter
        Dim intXferCd As Int32

        'GetBoxXferGroupings()

        'If dgXferGroups.Items.Count > 0 Then
        intdgXferTypes = ViewState("XTCount")
        For I = 0 To (intdgXferTypes - 1)

            strTType = dgXferGroups.Items(I).Cells(0).Text
            ddlGers = CType(dgXferGroups.Items(I).FindControl("ddlGersStatus"), DropDownList)
            ddlDiscrep = CType(dgXferGroups.Items(I).FindControl("ddlDiscrepStatus"), DropDownList)
            ddlorigGers = CType(dgXferGroups.Items(I).FindControl("ddlorigGers"), DropDownList)
            ddlorigDiscrep = CType(dgXferGroups.Items(I).FindControl("ddlorigDiscrep"), DropDownList)
            intGersStatus = ddlGers.SelectedItem.Value
            strGers = ddlGers.SelectedItem.Text
            intDiscrep = ddlDiscrep.SelectedItem.Value
            intorigGers = ddlorigGers.SelectedItem.Value
            intorigDiscrep = ddlorigDiscrep.SelectedItem.Value

            If intGersStatus <> intorigGers Then
                strHistoryNotes = "A change was made to a GERS Status."
                Call UpdateICHistory(m_strBoxId, strHistoryNotes)
            End If

            If intDiscrep <> intorigDiscrep Then
                strHistoryNotes = "A change was made to a Discrepancy Status"
                Call UpdateICHistory(m_strBoxId, strHistoryNotes)
            End If

            'Get the Transfer Type CD (for the Update subprocedure below) by querying against Xfer Description
            Dim conRetTTypeCD As System.Data.SqlClient.SqlConnection
            Dim cmdRetTTypeCD As System.Data.SqlClient.SqlCommand
            conRetTTypeCD = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            cmdRetTTypeCD = New SqlClient.SqlCommand("spRetrieveTransType", conRetTTypeCD)
            cmdRetTTypeCD.CommandType = CommandType.StoredProcedure
            cmdRetTTypeCD.Parameters.Add("@TType", strTType)
            parmXferCd = cmdRetTTypeCD.Parameters.Add("@XferCd", SqlDbType.TinyInt)
            parmXferCd.Size = 1
            parmXferCd.Direction = ParameterDirection.Output
            conRetTTypeCD.Open()
            cmdRetTTypeCD.ExecuteNonQuery()
            intXferCd = cmdRetTTypeCD.Parameters("@XferCd").Value
            conRetTTypeCD.Close()
            cmdRetTTypeCD.Dispose()
            Dim conUpdateGD As System.Data.SqlClient.SqlConnection
            Dim cmdUpdateGD As System.Data.SqlClient.SqlCommand
            conUpdateGD = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            cmdUpdateGD = New SqlClient.SqlCommand("spICUPGersDiscrep", conUpdateGD)
            cmdUpdateGD.CommandType = CommandType.StoredProcedure
            cmdUpdateGD.Parameters.Add("@BID", m_strBoxId)
            cmdUpdateGD.Parameters.Add("@TType", intXferCd)
            cmdUpdateGD.Parameters.Add("@GERS", intGersStatus)
            cmdUpdateGD.Parameters.Add("@Discrep", intDiscrep)
            conUpdateGD.Open()
            cmdUpdateGD.ExecuteNonQuery()
            conUpdateGD.Close()
            cmdUpdateGD.Dispose()
        Next
        Call CheckViewOnly()
        GetBoxHeader()
        GetBoxItems()
        GetBoxXferGroupings()

    End Sub
    'Private Sub UpdateReceivingStore()
    '    'Update Receiving Store if Changed.
    '    If (intReceivingStore <> Nothing Or intReceivingStore <> 0) Then
    '        Dim conUpdateRecStore As System.Data.SqlClient.SqlConnection
    '        Dim cmdUpdateRecStore As System.Data.SqlClient.SqlCommand
    '        conUpdateRecStore = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
    '        cmdUpdateRecStore = New SqlClient.SqlCommand("spUpdateRecStore", conUpdateRecStore)
    '        cmdUpdateRecStore.CommandType = CommandType.StoredProcedure
    '        cmdUpdateRecStore.Parameters.Add("@BID", m_strBoxId)
    '        cmdUpdateRecStore.Parameters.Add("@RStore", intReceivingStore)
    '        conUpdateRecStore.Open()
    '        cmdUpdateRecStore.ExecuteNonQuery()
    '        conUpdateRecStore.Close()
    '    End If
    'End Sub
    Private Sub ddlRStore_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlRStore.SelectedIndexChanged
        'User has changed receiving store number.
        If (lblStatus_Value.Text <> "New Transfer" And lblStatus_Value.Text <> "Unpacking" And lblStatus_Value.Text <> "Packing") Then
            intReceivingStore = ddlRStore.SelectedItem.Value
            strHistoryNotes = "Receiving Store Number has been changed."
            Call UpdateICHistory(m_strBoxId, strHistoryNotes)
        End If
    End Sub
    Private Sub txtNotes_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNotes.TextChanged
        'strNotes = txtNotes.Text
        'Dim Modified As DateTime
        'Modified = Now
        'Call UpdateNotes(m_strBoxId, strNotes, Modified)
        'strHistoryNotes = "The store's notes were modified."
        'Call UpdateICHistory(m_strBoxId, strHistoryNotes)

        'This is all done in the btnUpdateStoreNotes_Click
    End Sub
    Private Sub UpdateNotes(ByVal BoxId As String, ByVal Notes As String, ByVal Modified As DateTime)
        'This updates the box's header notes when they have been changed.
        Dim conUpdateNotes As System.Data.SqlClient.SqlConnection
        Dim cmdUpdateNotes As System.Data.SqlClient.SqlCommand
        conUpdateNotes = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdUpdateNotes = New SqlClient.SqlCommand("spUpdateBoxNotes", conUpdateNotes)
        cmdUpdateNotes.CommandType = CommandType.StoredProcedure
        cmdUpdateNotes.Parameters.Add("@BID", BoxId)
        cmdUpdateNotes.Parameters.Add("@Notes", Notes)
        cmdUpdateNotes.Parameters.Add("@Modified", Modified)
        conUpdateNotes.Open()
        cmdUpdateNotes.ExecuteNonQuery()
        conUpdateNotes.Close()
        cmdUpdateNotes.Dispose()
    End Sub
    Private Sub UpdateICHistory(ByVal BoxId As String, ByVal Note As String)
        'This updates the IC History
        Dim CreatedDate As DateTime
        Dim EmpId As String
        CreatedDate = Now
        EmpId = Trim(objUserInfo.EmpId)

        Dim conUpdateICHistory As System.Data.SqlClient.SqlConnection
        Dim cmdUpdateICHistory As System.Data.SqlClient.SqlCommand
        conUpdateICHistory = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdUpdateICHistory = New SqlClient.SqlCommand("spICInsertHistory", conUpdateICHistory)
        cmdUpdateICHistory.CommandType = CommandType.StoredProcedure
        cmdUpdateICHistory.Parameters.Add("@BID", BoxId)
        cmdUpdateICHistory.Parameters.Add("@EID", EmpId)
        cmdUpdateICHistory.Parameters.Add("@NOTE", Note)
        cmdUpdateICHistory.Parameters.Add("@DATE", CreatedDate)
        conUpdateICHistory.Open()
        cmdUpdateICHistory.ExecuteNonQuery()
        conUpdateICHistory.Close()
        cmdUpdateICHistory.Dispose()
    End Sub
    Private Sub btnVoid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        'Response.Redirect("IC_VoidConfirm.aspx?BID=" & m_strBoxId & "&RTN=" & strReturn)
        Call VoidBox()
        'need to send return string to go back to either IC_Results or IC_ResultsTT
    End Sub
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If Not objTransferLock.LockTransfer(Request("BID"), objUserInfo.EmpId, Session.SessionID) Then
            ViewOnly = True
            ViewState("VO") = True
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("This transfer has been locked by someone else.") & "&Title=Transfer Locked" & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            GetBoxHeader()
            GetBoxItems()
            GetBoxXferGroupings()
            Call CheckViewOnly()
        Else
            ViewOnly = False
            ViewState("VO") = False
            GetBoxHeader()
            GetBoxItems()
            GetBoxXferGroupings()
            Call CheckViewOnly()
        End If
    End Sub
    Private Sub btnReShip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReShip.Click
        'Response.Redirect("IC_ReShip.aspx?BID=" & m_strBoxId & "&RTN2=" & strReturn2)
        Dim strReturn2
        If Request.Params("RTN") <> Nothing And Request.Params("RTN") <> "" Then
            'Return to IC_Results
            strReturn2 = "IC_Results.aspx?" & "&FS=" & FStore & "&FDis=" & FDis & "&FReg=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TReg=" & TReg & "&TDis=" & TDis & " & NTI = " & NoTInternet & " & G = " & GERS & " & DS = " & Discrep & " & TT = " & TType & " & TranS = " & TStatus & " & FD = " & FDate & " & TD = " & TDate & " & DFQ = " & DFQty & " & DTQ = " & DTQty & " & DFA = " & DFAmt & " & DTA = " & DTAmt & " & TKN = " & TrackNum & " & TSN = " & TransNum
        Else
            'Return to IC_ResultsTT
            If GBID = Nothing Then
                strReturn2 = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum
            Else
                strReturn2 = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID
            End If
        End If
        Response.Redirect("IC_ReShip.aspx?BID=" & m_strBoxId & "&RTN2=" & strReturn2)
    End Sub
    Private Sub VoidBox()
        'IC wants to void box
        Dim intStatus
        Dim strempid As String
        Dim Modified
        Dim NullCheck As Boolean
        Modified = Now
        Dim strBox As String
        Dim strUser As String
        Dim intVoided As Int32  'This shows if void was successful
        strBox = Request.Params("BID")
        intStatus = 7


        If Not objUserInfo.EmpId Is Nothing Then
            strUser = objUserInfo.EmpFullName
            strempid = objUserInfo.EmpId
        Else
            'Nobody is logged in.
        End If

        '**************************************************************
        'Get Info to send Void Request to UPS
        Dim conGetVoidingInfo As System.Data.SqlClient.SqlConnection
        Dim cmdGetVoidingInfo As System.Data.SqlClient.SqlCommand
        conGetVoidingInfo = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdGetVoidingInfo = New SqlClient.SqlCommand("spICGETVOIDINFO", conGetVoidingInfo)
        cmdGetVoidingInfo.CommandType = CommandType.StoredProcedure
        cmdGetVoidingInfo.Parameters.Add("@BID", m_strBoxId)
        parmShipId = cmdGetVoidingInfo.Parameters.Add("@ShipID", SqlDbType.VarChar)
        parmTrackID = cmdGetVoidingInfo.Parameters.Add("@TrackID", SqlDbType.Int)
        parmParcelService = cmdGetVoidingInfo.Parameters.Add("@ParcelType", SqlDbType.SmallInt)
        parmXML = cmdGetVoidingInfo.Parameters.Add("@XML", SqlDbType.SmallInt)
        parmShipId.Size = 18
        parmTrackID.Size = 4
        parmParcelService.Size = 2
        parmXML.Size = 1
        parmShipId.Direction = ParameterDirection.Output
        parmTrackID.Direction = ParameterDirection.Output
        parmParcelService.Direction = ParameterDirection.Output
        parmXML.Direction = ParameterDirection.Output
        conGetVoidingInfo.Open()
        cmdGetVoidingInfo.ExecuteNonQuery()

        NullCheck = IsDBNull(cmdGetVoidingInfo.Parameters("@ShipID").Value)
        If NullCheck = True Then
            ShipID = ""
        Else
            ShipID = cmdGetVoidingInfo.Parameters("@ShipID").Value
        End If

        NullCheck = IsDBNull(cmdGetVoidingInfo.Parameters("@TrackID").Value)
        If NullCheck = True Then
            TrackID = 0
        Else
            TrackID = cmdGetVoidingInfo.Parameters("@TrackID").Value
        End If

        NullCheck = IsDBNull(cmdGetVoidingInfo.Parameters("@ParcelType").Value)
        If NullCheck = True Then
            ParcelType = 0
        Else
            ParcelType = cmdGetVoidingInfo.Parameters("@ParcelType").Value
        End If

        NullCheck = IsDBNull(cmdGetVoidingInfo.Parameters("@XML").Value)
        If NullCheck = True Then
            XML = 0
        Else
            XML = cmdGetVoidingInfo.Parameters("@XML").Value
        End If

        conGetVoidingInfo.Close()
        cmdGetVoidingInfo.Dispose()
        '**************************************************************

        If (ParcelType = 1 And XML = 1) Then
            'Send Void Request to UPS
            VoidShippingLabel()
            '****************************************************************
            'Check if Void was successful
            Dim conCheckVoid As System.Data.SqlClient.SqlConnection
            Dim cmdCheckVoid As System.Data.SqlClient.SqlCommand
            conCheckVoid = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            cmdCheckVoid = New SqlClient.SqlCommand("spICCHECKVOID", conCheckVoid)
            cmdCheckVoid.CommandType = CommandType.StoredProcedure
            cmdCheckVoid.Parameters.Add("@BID", m_strBoxId)
            cmdCheckVoid.Parameters.Add("@TrackID", TrackID)
            parmVoid = cmdCheckVoid.Parameters.Add("@Void", SqlDbType.SmallInt)
            parmVoid.Size = 2
            parmVoid.Direction = ParameterDirection.Output
            conCheckVoid.Open()
            cmdCheckVoid.ExecuteNonQuery()
            intVoided = cmdCheckVoid.Parameters("@Void").Value

            If intVoided = 1 Then
                'Shipping label was successfully voided.
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("UPS shipping label successfully voided.") & "&Title=Void Successful" & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                '****************************************************************
                'Update Box Status to VOID
                Call VoidBoxUpdate(strBox, intStatus, strempid, strUser, Modified)
                '***********************************************************
                'Update IC History NOTES
                Dim Notes As String
                Notes = "This box was voided.  Box's shipping label was voided."
                UpdateICHistory(strBox, Notes)
                '******************************
                Call CheckViewOnly()
                GetBoxHeader()
                GetBoxItems()
                GetBoxXferGroupings()
            ElseIf intVoided = -1 Then
                'Void failed.
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Void of UPS shipping label failed.  This could be due to shipping label being older than 24 hours.  Please call UPS.") & "&Title=Warning!" & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                '****************************************************************
                'Update Box Status to VOID
                Call VoidBoxUpdate(strBox, intStatus, strempid, strUser, Modified)
                '***********************************************************
                'Update IC History NOTES
                Dim Notes As String
                Notes = "Box was voided. IC void shipping label attempt failed."
                UpdateICHistory(strBox, Notes)
                '******************************
                Call CheckViewOnly()
                GetBoxHeader()
                GetBoxItems()
                GetBoxXferGroupings()
            End If  'end of end if of void was successfully voided or not

        ElseIf (ParcelType = 1 And XML = 0) Then
            'IC needs to call UPS b/c this wasn't a generated label.  It was a pre-existing label
            'This still voids the box itself

            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Call UPS to Void the shipping label.  The label used was a pre-existing label.") & "&Title=Warning!" & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            'Update Box Status to VOID
            Call VoidBoxUpdate(strBox, intStatus, strempid, strUser, Modified)
            '************************
            'Update IC Notes.
            Dim Notes As String
            Notes = "This box was voided.  UPS shipping label was a pre-existing label."
            UpdateICHistory(strBox, Notes)
            '******************************
            Call CheckViewOnly()
            GetBoxHeader()
            GetBoxItems()
            GetBoxXferGroupings()

        ElseIf (ParcelType <> 1) Then
            'The parcel carrier isn't UPS.  IC will have to call whatever delivery carrier was used.
            'This still voids the box.

            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("UPS is not the carrier for this transfer.  Please call the appropriate carrier to void the shipping label.") & "&Title=Warning!" & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")

            'Update Box Status to VOID
            Call VoidBoxUpdate(strBox, intStatus, strempid, strUser, Modified)
            '************************
            'Update IC Notes.
            Dim Notes As String
            Notes = "This box was voided.  Shipping label was not a UPS label."
            UpdateICHistory(strBox, Notes)
            '*********************
            Call CheckViewOnly()
            GetBoxHeader()
            GetBoxItems()
            GetBoxXferGroupings()
        End If  'End if of voiding transfer
    End Sub
    Private Sub VoidBoxUpdate(ByVal strBox As String, ByVal intStatus As Integer, ByVal strempid As String, ByVal strUser As String, ByVal Modified As DateTime)
        'This updates the Box History and Box_Xfer_Hdr when a box is voided.
        Dim conUpdateHistory As System.Data.SqlClient.SqlConnection
        Dim cmdUpdateHistory As System.Data.SqlClient.SqlCommand
        conUpdateHistory = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdUpdateHistory = New SqlClient.SqlCommand("spUpdateBoxHistory", conUpdateHistory)
        cmdUpdateHistory.CommandType = CommandType.StoredProcedure
        cmdUpdateHistory.Parameters.Add("@Box_Id", strBox)
        cmdUpdateHistory.Parameters.Add("@Status", intStatus)
        cmdUpdateHistory.Parameters.Add("@EmpId", strempid)
        cmdUpdateHistory.Parameters.Add("@EmpFullName", strUser)
        cmdUpdateHistory.Parameters.Add("@UpdateTime", Modified)
        conUpdateHistory.Open()
        cmdUpdateHistory.ExecuteNonQuery()
        conUpdateHistory.Close()
        cmdUpdateHistory.Dispose()
        Dim conUpdateBoxHdr As System.Data.SqlClient.SqlConnection
        Dim cmdUpdateBoxHdr As System.Data.SqlClient.SqlCommand
        conUpdateBoxHdr = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdUpdateBoxHdr = New SqlClient.SqlCommand("spUpdateBoxHdrStatus", conUpdateBoxHdr)
        cmdUpdateBoxHdr.CommandType = CommandType.StoredProcedure
        cmdUpdateBoxHdr.Parameters.Add("@BID", strBox)
        cmdUpdateBoxHdr.Parameters.Add("@Status", intStatus)
        cmdUpdateBoxHdr.Parameters.Add("@Modified", Modified)
        conUpdateBoxHdr.Open()
        cmdUpdateBoxHdr.ExecuteNonQuery()
        conUpdateBoxHdr.Close()
        cmdUpdateBoxHdr.Dispose()
        'view status becomes viewonly
        viewstate("VO") = True
    End Sub
    Private Function VoidShippingLabel()

        Dim objUPS As New UPS_XML_Shipping()

        'objUPS.ShipmentId = m_strShipmentId 'XML UPS Shipment Id
        'objUPS.TrackingInfoId = m_intTrackingInfoId
        objUPS.ShipmentId = ShipID 'XML UPS Shipment Id
        objUPS.TrackingInfoId = TrackID
        objUPS.BoxId = m_strBoxId

        'Send XML Void Request
        objUPS.SendVoidShipping()

    End Function
    Private Sub btnLost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLost.Click
        'Updates box status to lost.
        Dim strNotesLost As String
        Dim conUpdateBoxStatus As System.Data.SqlClient.SqlConnection
        Dim cmdUpdateBoxStatus As System.Data.SqlClient.SqlCommand
        conUpdateBoxStatus = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdUpdateBoxStatus = New SqlClient.SqlCommand("spUpdateBoxHdrStatus", conUpdateBoxStatus)
        cmdUpdateBoxStatus.CommandType = CommandType.StoredProcedure
        cmdUpdateBoxStatus.Parameters.Add("@BID", m_strBoxId)
        cmdUpdateBoxStatus.Parameters.Add("@Status", 8)
        cmdUpdateBoxStatus.Parameters.Add("@Modified", Now())
        conUpdateBoxStatus.Open()
        cmdUpdateBoxStatus.ExecuteNonQuery()
        conUpdateBoxStatus.Close()
        cmdUpdateBoxStatus.Dispose()
        strNotesLost = "Box Status changed to Lost"
        Call UpdateICHistory(m_strBoxId, strNotesLost)
        Call GetBoxHeader()
    End Sub
    Private Sub btnUpdateStoreNotes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateStoreNotes.Click
        'strNotes = txtNotes.Text
        Dim origNotes As String
        Dim updatedNotes As String
        origNotes = lblStoreNotes.Text
        strNotes = ViewState("StoreNotes")
        updatedNotes = origNotes & strNotes
        Dim Modified As DateTime
        Modified = Now
        'Call UpdateNotes(m_strBoxId, strNotes, Modified)
        Call UpdateNotes(m_strBoxId, updatedNotes, Modified)

        strHistoryNotes = "The store's notes were modified."
        Call UpdateICHistory(m_strBoxId, strHistoryNotes)

        Call GetBoxHeader()
        txtNotes.Text = ""
    End Sub
End Class
