<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UpdateConfirm.aspx.vb" Inherits="SSTransfer_IC.UpdateConfirm"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>UpdateConfirm</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="yes" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td noWrap height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td><table width="100%" height="100%">
							<tr valign="top">
								<td><asp:Label Runat="server" ID="lblUpdated" Font-Size="X-Small" Font-Names="Arial" ForeColor="White"></asp:Label>
									<br>
									<br>
									<asp:Label Runat="server" ID="lblChecked" Font-Size="X-Small" Font-Names="Arial" ForeColor="White"></asp:Label>
									<br>
									<br>
									<asp:Label Runat="server" ID="lblLocked" Font-Size="X-Small" Font-Names="Arial" ForeColor="White"></asp:Label>
									<br>
									<asp:Label Runat="server" ID="lblTransfersLocked" Font-Size="X-Small" Font-Names="Arial" ForeColor="White"></asp:Label>
									<br>
									<br>
									<asp:Label Runat="server" ID="lblBadCriteria" Font-Size="X-Small" Font-Names="Arial" ForeColor="White"></asp:Label>
									<br>
									<br>
									<asp:Label Runat="server" ID="lblNoBatchNum" Font-Size="X-Small" Font-Names="Arial" ForeColor="White"></asp:Label>
								    
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td noWrap height="1"><uc1:footer id="ucFooter" runat="server" Font-Size="X-Small" Font-Names="Arial" ForeColor="White"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
