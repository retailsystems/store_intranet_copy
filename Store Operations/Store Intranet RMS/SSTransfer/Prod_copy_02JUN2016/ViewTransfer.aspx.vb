Public Class ViewTransfer
    Inherits System.Web.UI.Page

    Protected WithEvents layoutHeader As Header
    Protected WithEvents ucSearchBox As FilterBox
    Protected WithEvents ucTransferBox As TransferBox
    Protected WithEvents ucTransferHeader As TransferHeader
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

    Private m_objUserInfo As New UserInfo()
    Private m_strReturn As String
    Private m_booLocked As Boolean = False

    'RETURNING VARIABLES FOR ADVANCEDRESULTS.ASPX
    Public strTransfer As String      'Transfer Number
    Public strUPS As String           'UPS Info
    Public strSearchType As String    'Simple Search or Advanced Search
    Public strSearchFor As String     'Void, Expected Arrivals, Shipped Boxes, Received Boxes
    Public strFromStore As String     'From Store
    Public strToStore As String       'To Store
    Public strFromDate As String      'From Date
    Public strThruDate As String      'Thru Date
    Public strTransferTypes As String 'Transfer Types user selected in advanced search.
    Public strBoxStatus As String     'Box Statuses
    Public strBoxIncludes As String   'Box Includes
    Public strAdvReturn As String     'Advanced Search Results Return String (TransferSearch or AdvancedSearch)
    Public strSKU As String           'Advanced Search-SKU

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        'Fill User Object with employee/store data
        m_objUserInfo.GetEmployeeInfo()

        'If store number not set then redirect to set store page
        If m_objUserInfo.StoreNumber = -1 Or m_objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

        'If employee is logged on to the system then update last trans
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
        End If

        'Get QueryString variables
        m_strReturn = Request("RET")
        If Not IsNothing(Request("L")) Then
            m_booLocked = Request("L")
        End If

        'Set header properties
        layoutHeader.lblTitle = "View Transfer"
        layoutHeader.CurrentMode(Header.HeaderGroup.Transfer)
        layoutHeader.CurrentPage(Header.PageName.Home)

        'Set transfer box 
        ucTransferBox.ViewSnapShot = True 'Display prices of items when sent
        ucTransferBox.Box_Id = Request.QueryString("BID")
        'ucTransferBox.GenDiscrepancy = True
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyShipped_Edit
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyReceived_Edit

        'Set transfer header
        ucTransferHeader.BoxIncludesVisible = False
        ucTransferHeader.DisableNotes = True
        ucTransferHeader.BoxId = Request.QueryString("BID")
        ucTransferHeader.ShowUPSLink = True

        'Lock Notification
        '''''If m_booLocked Then
        '''''    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Title=" & Server.UrlEncode("Info") & "&Err=" & Server.UrlEncode("Transfer " & Request.QueryString("BID") & " is currently in use.<br>Please try later.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
        '''''End If

        If Len(Request.QueryString("Msg")) > 0 Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Title=" & Server.UrlEncode("Info") & "&Err=" & Server.UrlEncode(Request.QueryString("Msg")) & "','Notification','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
        End If

        ucTransferHeader.LoadData()
        ucTransferBox.LoadData()

        strTransfer = Request.Params("ABID")
        strUPS = Request.Params("UPS")
        strSearchType = Request.Params("ST")
        strSearchFor = Request.Params("SF")
        strFromStore = Request.Params("FStore")
        strToStore = Request.Params("TStore")
        strFromDate = Request.Params("FDate")
        strThruDate = Request.Params("TDate")
        strTransferTypes = Request.Params("TT")
        strBoxStatus = Request.Params("BS")
        strBoxIncludes = Request.Params("BI")
        strAdvReturn = Request.Params("SPRTN")  'Return String From AdvancedResults
        strSKU = Request.Params("SKU")
    End Sub
    'Display discrepancy in the box header
    'Private Sub ucTransferBox_DiscrepancyGenerated(ByVal DisAmount As Decimal, ByVal DisQuant As Int32) Handles ucTransferBox.DiscrepancyGenerated

    'ucTransferHeader.DiscrepancyAmount = DisAmount
    'ucTransferHeader.DiscrepancyQuantity = DisQuant
    'ucTransferHeader.DisplayDiscrepancy()

    'End Sub

    'Return to previous screen

    'return user back to previous page
    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        If (strAdvReturn <> Nothing And strAdvReturn <> "") Then
            'Returning to AdvancedResults
            m_strReturn = m_strReturn & "&ABID=" & strTransfer & "&UPS=" & strUPS & "&ST=" & strSearchType & "&SF=" & strSearchFor & "&FStore=" & strFromStore & "&TStore=" & strToStore & "&FDate=" & strFromDate & "&TDate=" & strThruDate & "&TT=" & strTransferTypes & "&BI=" & strBoxIncludes & "&BS=" & strBoxStatus & "&SKU=" & strSKU & "&SPRTN=" & strAdvReturn
            Response.Redirect(m_strReturn)
        Else
            Response.Redirect(m_strReturn)
        End If
    End Sub

End Class
