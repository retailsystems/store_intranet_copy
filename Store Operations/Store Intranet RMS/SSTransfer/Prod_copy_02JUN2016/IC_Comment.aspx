<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_Comment.aspx.vb" Inherits="SSTransfer.IC_Comment"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>IC Comment</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="yes" rightMargin="0">
		<form id="frmICComment" method="post" runat="server" onsubmit="return checkSubmit();">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<TBODY>
					<tr>
						<td noWrap height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
					</tr>
					<tr valign="top" height="1%">
						<td nowrap><br>
							<asp:Label runat="server" id="lblComments" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"></asp:Label></td>
					</tr>
					<tr valign="top" height="1%">
						<td nowrap><asp:TextBox Runat="server" ID="txtComments" width="100%" TextMode="MultiLine" Rows="5" tabIndex="1"></asp:TextBox></td>
					</tr>
					<tr valign="top">
						<td nowrap align="right"><br>
							<asp:Button Runat="server" ID="btnReturn" text="Return" BorderColor="#990000" BackColor="#440000" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True" ForeColor="White" Visible="True" tabIndex="2"></asp:Button>&nbsp;<asp:Button ID="btnSave" Runat="server" Text="Save Comments" BorderColor="#990000" BackColor="#440000" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True" ForeColor="White" tabIndex="3"></asp:Button></td>
					</tr>
					<tr>
						<td noWrap height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
					</tr>
				</TBODY>
			</table>
		</form>
		</TR></TBODY></TABLE></FORM>
	</body>
</HTML>
