Imports System.Data.OleDb

Public Class WebForm1
    Inherits System.Web.UI.Page


    'Search Parameters

    'connStrings 
    Dim connHeader As System.Data.SqlClient.SqlConnection
    Dim connOra As System.Data.OleDb.OleDbConnection

    'local variables to store  :: check type in dbase
    'used to do parameter checks on the field
    Dim gStoNum As Integer
    Dim gStoName As String
    Dim gAddr As String
    Dim gCity As String
    Dim gState As String
    Dim gZip As String
    Dim gPhone1 As String
    Dim gPhone2 As String
    Dim gEmail As String
    Dim gOpen As String
    Dim gDim As String
    Dim gRegion As String
    Dim gDist As String
    Dim gStoMgr As String
    Dim gAM1 As String
    Dim gAM2 As String
    Dim gAM3 As String
    Dim gAM4 As String
    Dim gAM5 As String
    Dim gTags As String
    Dim gSimon As String
    Dim gCoName As String
    Dim gShipNumb As String

    Dim GoSearch As Boolean 'Counter 

    'Flags to identify which search fields usrs have filled
    Dim SA_Num As Integer
    Dim SA_Name As Integer
    Dim SA_State As Integer
    Dim SA_Email As Integer
    Dim SA_OpenDate As Integer
    Dim SA_Region As Integer
    Dim SA_Dist As Integer
    Dim SA_Tags As Integer
    Dim SA_Simon As Integer
    Dim SA_City As Integer
    Dim SA_Mgr As Integer
    Dim SA_Size As Integer

    'Display Objects
    Protected WithEvents txtStoreName As System.Web.UI.WebControls.TextBox


    Protected WithEvents dgSearch As System.Web.UI.WebControls.DataGrid
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl

    Public FromDate As String
    Public ThruDate As String


    'Data passed in by POST
    Private pStoreID As Integer
    Protected WithEvents headStoreSrch As System.Web.UI.WebControls.Label
    Protected WithEvents lblStoNum As System.Web.UI.WebControls.Label
    Protected WithEvents txtSrchStoNum As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSrchOpenDate As System.Web.UI.WebControls.Label
    Protected WithEvents txtFDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblTo2 As System.Web.UI.WebControls.Label
    Protected WithEvents txtTDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblStoName As System.Web.UI.WebControls.Label
    Protected WithEvents txtSrchStoName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSrchRegion As System.Web.UI.WebControls.Label
    Protected WithEvents cboSrchRegion As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbSrchState As System.Web.UI.WebControls.Label
    Protected WithEvents cboSrchState As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblSrchDist As System.Web.UI.WebControls.Label
    Protected WithEvents cboSrchDist As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblCity1 As System.Web.UI.WebControls.Label
    Protected WithEvents cboCity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblMgr As System.Web.UI.WebControls.Label
    Protected WithEvents cboMgr As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents txtSize As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblTo As System.Web.UI.WebControls.Label
    Protected WithEvents txtSize2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEmpty As System.Web.UI.WebControls.Label
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents lblNoResults As System.Web.UI.WebControls.Label
    Protected WithEvents btnSearch As System.Web.UI.WebControls.Button
    Protected WithEvents btnClear As System.Web.UI.WebControls.Button
    Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
    Protected WithEvents lblSrchEmail As System.Web.UI.WebControls.Label
    Protected WithEvents txtSrchEmail As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSrchTags As System.Web.UI.WebControls.Label
    Protected WithEvents txtSrchTag As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSrchSimon As System.Web.UI.WebControls.Label
    Protected WithEvents txtSrchSimon As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblShipNumb As System.Web.UI.WebControls.Label
    Protected WithEvents txtShipNumb As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSrchRes As System.Web.UI.WebControls.Label
    Protected WithEvents lblLogin As System.Web.UI.WebControls.Label
    Protected WithEvents txtUserID As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents lblLoginMsg As System.Web.UI.WebControls.Label
    Protected WithEvents headStore As System.Web.UI.WebControls.Label
    Protected WithEvents lblSaved As System.Web.UI.WebControls.Label
    Protected WithEvents lblRecordDeleted As System.Web.UI.WebControls.Label
    Protected WithEvents lblStoreNum As System.Web.UI.WebControls.Label
    Protected WithEvents txtStoNum As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbtStoSize As System.Web.UI.WebControls.Label
    Protected WithEvents txtStoDim As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblStoreName As System.Web.UI.WebControls.Label
    Protected WithEvents txtStoName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblRegion As System.Web.UI.WebControls.Label
    Protected WithEvents cboRegion As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblStoAddr As System.Web.UI.WebControls.Label
    Protected WithEvents txtAddr As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblDist As System.Web.UI.WebControls.Label
    Protected WithEvents cboDistrict As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblCity As System.Web.UI.WebControls.Label
    Protected WithEvents txtCity As System.Web.UI.WebControls.TextBox
    Protected WithEvents labManager As System.Web.UI.WebControls.Label
    Protected WithEvents txtStoMgr As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblState As System.Web.UI.WebControls.Label
    Protected WithEvents cboState As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblAsstMgr1 As System.Web.UI.WebControls.Label
    Protected WithEvents txtAsstMgr1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblZip As System.Web.UI.WebControls.Label
    Protected WithEvents txtZip As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblAsstMgr2 As System.Web.UI.WebControls.Label
    Protected WithEvents txtAsstMgr2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblPhone1 As System.Web.UI.WebControls.Label
    Protected WithEvents txtPhone1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblAsstMgr3 As System.Web.UI.WebControls.Label
    Protected WithEvents txtAsstMgr3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblPhone2 As System.Web.UI.WebControls.Label
    Protected WithEvents txtPhone2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblAsstMgr4 As System.Web.UI.WebControls.Label
    Protected WithEvents txtAsstMgr4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEMail As System.Web.UI.WebControls.Label
    Protected WithEvents txtEmail As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblAsstMgr5 As System.Web.UI.WebControls.Label
    Protected WithEvents txtAsstMgr5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblOpenDate As System.Web.UI.WebControls.Label
    Protected WithEvents txtOpenDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblTags As System.Web.UI.WebControls.Label
    Protected WithEvents txtTags As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSimon As System.Web.UI.WebControls.Label
    Protected WithEvents txtSimon As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblCoName As System.Web.UI.WebControls.Label
    Protected WithEvents txtCoName As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnModify As System.Web.UI.WebControls.Button
    Protected WithEvents btnDelete As System.Web.UI.WebControls.Button
    Protected WithEvents btnOK As System.Web.UI.WebControls.Button
    Dim myDataTable As DataTable

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'get any var passed into this page
        pStoreID = Request.QueryString("pStoreID")
        pageBody.Attributes.Add("onload", "")

        If Not IsPostBack Then
            'populate pull downs
            build_DDL_State()
            build_DDL_Region()
            build_DDL_District()
            build_DDL_City()
            build_DDL_Mgr()
        End If

        'Init Settings
        Hide_Messages()
        SearchArea_Hide()
        DisplayArea_Hide()
        Login_Hide()

        If pStoreID <> Nothing Then
            actSearchOne(pStoreID)
        End If

    End Sub

    '-----------------------------------------------------------------------------
    'tf
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub Login_Hide()

        lblLogin.Visible = False
        txtUserID.Visible = False
        btnSubmit.Visible = False
        btnCancel.Visible = False
        lblLoginMsg.Visible = False

    End Sub

    '-----------------------------------------------------------------------------
    'tf
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub Login_Show()

        lblLogin.Visible = True
        txtUserID.Visible = True
        btnSubmit.Visible = True
        btnCancel.Visible = True

    End Sub

    '-----------------------------------------------------------------------------
    'Searches and assigns the values to the text fields
    '
    '
    '-----------------------------------------------------------------------------


    Public Sub actSearchOne(ByVal pStoreID)

        ConnHeaderOpen()

        'commands
        Dim cmdHeader As System.Data.SqlClient.SqlCommand

        'parameters
        Dim pStoNum As SqlClient.SqlParameter
        Dim pStoName As SqlClient.SqlParameter
        Dim pAddr As SqlClient.SqlParameter
        Dim pCity As SqlClient.SqlParameter
        Dim pState As SqlClient.SqlParameter
        Dim pZip As SqlClient.SqlParameter
        Dim pPhone1 As SqlClient.SqlParameter
        Dim pPhone2 As SqlClient.SqlParameter
        Dim pEmail As SqlClient.SqlParameter
        Dim pOpen As SqlClient.SqlParameter
        Dim pDim As SqlClient.SqlParameter
        Dim pReg As SqlClient.SqlParameter
        Dim pDist As SqlClient.SqlParameter
        Dim pGM As SqlClient.SqlParameter
        Dim pAM1 As SqlClient.SqlParameter
        Dim pAM2 As SqlClient.SqlParameter
        Dim pAM3 As SqlClient.SqlParameter
        Dim pAM4 As SqlClient.SqlParameter
        Dim pAM5 As SqlClient.SqlParameter
        Dim pTags As SqlClient.SqlParameter
        Dim pSimon As SqlClient.SqlParameter
        Dim pCoName As SqlClient.SqlParameter
        Dim pShipNumb As SqlClient.SqlParameter


        'inputs (1) 
        cmdHeader = New SqlClient.SqlCommand("spFindStoreData", connHeader)
        cmdHeader.CommandType = CommandType.StoredProcedure
        cmdHeader.Parameters.Add("@StoreID", pStoreID)

        'outputs
        pStoNum = cmdHeader.Parameters.Add("@SIDOut", SqlDbType.VarChar)
        pStoName = cmdHeader.Parameters.Add("@SNameOut", SqlDbType.VarChar)
        pAddr = cmdHeader.Parameters.Add("@SAddrOut", SqlDbType.VarChar)
        pCity = cmdHeader.Parameters.Add("@SCityOut", SqlDbType.VarChar)
        pState = cmdHeader.Parameters.Add("@SStateOut", SqlDbType.VarChar)
        pZip = cmdHeader.Parameters.Add("@SZipOut", SqlDbType.VarChar)
        pPhone1 = cmdHeader.Parameters.Add("@SPhone1Out", SqlDbType.VarChar)
        pPhone2 = cmdHeader.Parameters.Add("@SPhone2Out", SqlDbType.VarChar)
        pEmail = cmdHeader.Parameters.Add("@SEmailOut", SqlDbType.VarChar)
        pOpen = cmdHeader.Parameters.Add("@SOpenOut", SqlDbType.VarChar)
        pDim = cmdHeader.Parameters.Add("@SDimOut", SqlDbType.VarChar)
        pReg = cmdHeader.Parameters.Add("@SRegionOut", SqlDbType.VarChar)
        pDist = cmdHeader.Parameters.Add("@SDistrictOut", SqlDbType.VarChar)

        pGM = cmdHeader.Parameters.Add("@SMgrOut", SqlDbType.VarChar)
        pAM1 = cmdHeader.Parameters.Add("@SAM1Out", SqlDbType.VarChar)
        pAM2 = cmdHeader.Parameters.Add("@SAM2Out", SqlDbType.VarChar)
        pAM3 = cmdHeader.Parameters.Add("@SAM3Out", SqlDbType.VarChar)
        pAM4 = cmdHeader.Parameters.Add("@SAM4Out", SqlDbType.VarChar)
        pAM5 = cmdHeader.Parameters.Add("@SAM5Out", SqlDbType.VarChar)
        pTags = cmdHeader.Parameters.Add("@STagsOut", SqlDbType.VarChar)
        pSimon = cmdHeader.Parameters.Add("@SSimonOut", SqlDbType.VarChar)
        pCoName = cmdHeader.Parameters.Add("@SCoNameOut", SqlDbType.VarChar)
        pShipNumb = cmdHeader.Parameters.Add("@SShipNumbOut", SqlDbType.VarChar)

        'Declare Size
        pStoNum.Size = 100
        pStoName.Size = 40
        pAddr.Size = 60
        pCity.Size = 100
        pState.Size = 30
        pZip.Size = 6
        pPhone1.Size = 15
        pPhone2.Size = 15
        pEmail.Size = 40
        pOpen.Size = 15
        pDim.Size = 10
        pReg.Size = 10
        pDist.Size = 10
        pGM.Size = 40
        pAM1.Size = 40
        pAM2.Size = 40
        pAM3.Size = 40
        pAM4.Size = 40
        pAM5.Size = 40
        pTags.Size = 1
        pSimon.Size = 1
        pCoName.Size = 25
        pShipNumb.Size = 10

        'Declare Direction
        pStoNum.Direction = ParameterDirection.Output '
        pStoName.Direction = ParameterDirection.Output '
        pAddr.Direction = ParameterDirection.Output '
        pCity.Direction = ParameterDirection.Output
        pState.Direction = ParameterDirection.Output
        pZip.Direction = ParameterDirection.Output
        pPhone1.Direction = ParameterDirection.Output
        pPhone2.Direction = ParameterDirection.Output
        pEmail.Direction = ParameterDirection.Output
        pOpen.Direction = ParameterDirection.Output
        pDim.Direction = ParameterDirection.Output
        pReg.Direction = ParameterDirection.Output '*
        pDist.Direction = ParameterDirection.Output '*
        pGM.Direction = ParameterDirection.Output
        pAM1.Direction = ParameterDirection.Output
        pAM2.Direction = ParameterDirection.Output
        pAM3.Direction = ParameterDirection.Output
        pAM4.Direction = ParameterDirection.Output
        pAM5.Direction = ParameterDirection.Output
        pTags.Direction = ParameterDirection.Output
        pSimon.Direction = ParameterDirection.Output
        pCoName.Direction = ParameterDirection.Output
        pShipNumb.Direction = ParameterDirection.Output

        'execute
        cmdHeader.ExecuteNonQuery()

        'Build the State Drop Down in Display Area
        Call build_DDL_DisplayState()
        Call build_DDL_DisplayCity()
        Call build_DDL_DisplayMgr()
        Call build_DDL_DisplayRegion()
        Call build_DDL_DisplayDistrict()


        '#
        If IsDBNull(pStoNum.Value) Then
            txtStoNum.Text = ""
        Else
            txtStoNum.Text = pStoNum.Value
        End If

        'Name(2)
        If IsDBNull(pStoName.Value) Then
            txtStoName.Text = ""
        Else
            txtStoName.Text = pStoName.Value
        End If

        'Addr (3)
        If IsDBNull(pAddr.Value) Then
            txtAddr.Text = ""
        Else
            txtAddr.Text = pAddr.Value
        End If

        'City (4)
        If IsDBNull(pCity.Value) Then
            txtCity.Text = ""
        Else
            txtCity.Text = pCity.Value
        End If

        'State (5)
        If IsDBNull(pState.Value) Then
            cboState.SelectedIndex = 0
        Else
            cboState.SelectedIndex = cboState.Items.IndexOf(cboState.Items.FindByValue(pState.Value))
        End If

        'Zip (6) 
        If IsDBNull(pZip.Value) Then
            txtZip.Text = ""
        Else
            txtZip.Text = pZip.Value
        End If

        'Phone1 (7) 
        If IsDBNull(pPhone1.Value) Then
            txtPhone1.Text = ""
        Else
            txtPhone1.Text = pPhone1.Value
        End If

        'Phone2 (8) 
        If IsDBNull(pPhone2.Value) Then
            txtPhone2.Text = ""
        Else
            txtPhone2.Text = pPhone2.Value
        End If

        'Email (9) 
        If IsDBNull(pEmail.Value) Then
            txtEmail.Text = ""
        Else
            txtEmail.Text = pEmail.Value
        End If

        'Open (10) 
        If IsDBNull(pOpen.Value) Then
            txtOpenDate.Text = ""
        Else
            txtOpenDate.Text = pOpen.Value
        End If

        'Dim (11)
        If IsDBNull(pDim.Value) Then
            txtStoDim.Text = ""
        Else
            txtStoDim.Text = pDim.Value
        End If

        'Reg (12) 
        If IsDBNull(pReg.Value) Then
            cboRegion.SelectedIndex = 0
        Else
            cboRegion.SelectedIndex = cboRegion.Items.IndexOf(cboRegion.Items.FindByValue(pReg.Value))
        End If

        'District (13) 
        If IsDBNull(pDist.Value) Then
            cboDistrict.SelectedIndex = 0
        Else
            cboDistrict.SelectedIndex = cboDistrict.Items.IndexOf(cboDistrict.Items.FindByValue(pDist.Value))
        End If

        'GM (14) 
        If IsDBNull(pGM.Value) Then
            txtStoMgr.Text = ""
        Else
            txtStoMgr.Text = pGM.Value
        End If

        'AM1 (15) 
        If IsDBNull(pAM1.Value) Then
            txtAsstMgr1.Text = ""
        Else
            txtAsstMgr1.Text = pAM1.Value
        End If

        'AM2 (16)
        If IsDBNull(pAM2.Value) Then
            txtAsstMgr2.Text = ""
        Else
            txtAsstMgr2.Text = pAM2.Value
        End If

        'AM3 (17) 
        If IsDBNull(pAM3.Value) Then
            txtAsstMgr3.Text = ""
        Else
            txtAsstMgr3.Text = pAM3.Value
        End If

        'AM4 (18) 
        If IsDBNull(pAM4.Value) Then
            txtAsstMgr4.Text = ""
        Else
            txtAsstMgr4.Text = pAM4.Value
        End If

        'AM5 (19) 
        If IsDBNull(pAM5.Value) Then
            txtAsstMgr5.Text = ""
        Else
            txtAsstMgr5.Text = pAM5.Value
        End If

        'Tags (20) 
        If IsDBNull(pTags.Value) Then
            txtTags.Text = ""
        Else
            txtTags.Text = pTags.Value
        End If

        'Simon (21) 
        If IsDBNull(pSimon.Value) Then
            txtSimon.Text = ""
        Else
            txtSimon.Text = pSimon.Value
        End If

        'Company Name (22) 
        If IsDBNull(pCoName.Value) Then
            txtCoName.Text = ""
        Else
            txtCoName.Text = pCoName.Value
        End If

        'Shipper Number (23) 
        If IsDBNull(pShipNumb.Value) Then
            txtShipNumb.Text = ""
        Else
            txtShipNumb.Text = pShipNumb.Value
        End If

        'Show display area
        DisplayArea_Show()
        'close
        ConnHeaderClose()

    End Sub 'end actSearchOne  

    Private Sub dgSearch_PageIndexChanged(ByVal Source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgSearch.PageIndexChanged

        dgSearch.CurrentPageIndex = e.NewPageIndex

        Dim strSearch As String
        strSearch = ViewState("strSrch")

        Call actSearch(strSearch)

        dgSearch.DataBind()

    End Sub


    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub Hide_Messages()
        lblNoResults.Visible = False
        lblSaved.Visible = False
        lblRecordDeleted.Visible = False
        lblEmpty.Visible = False
        lblError.Visible = False

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub SearchArea_Hide()
        dgSearch.Visible = False
        lblSrchRes.Visible = False
    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub SearchArea_Show()
        dgSearch.Visible = True
        lblSrchRes.Visible = True
    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub ValidateDate()
        Dim FromDateCheck As Boolean
        Dim ThruDateCheck As Boolean
        Dim FromDateForm As Date
        Dim ThruDateForm As Date

        'DATE VALIDATION

        If (txtFDate.Text <> "" And txtTDate.Text <> "") Then
            FromDate = txtFDate.Text    'assigning info to FromDate
            ThruDate = txtTDate.Text    'assigning info to ThruDate
            FromDateForm = CDate(txtFDate.Text)
            ThruDateForm = CDate(txtTDate.Text)
            FromDateCheck = IsDate(FromDate)
            ThruDateCheck = IsDate(ThruDate)
            SA_OpenDate = 1
            GoSearch = True
            If (FromDateCheck = True And ThruDateCheck = True) Then
                If FromDateForm > ThruDateForm Then
                    'Not allowing searching with future dates.
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("From date cannot be later than Through date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                    GoSearch = 0
                    SA_OpenDate = 0
                ElseIf Date.Compare(ThruDate, Now.ToShortDateString) > 0 Then
                    'Not allowing searching with future dates.
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Cannot search with dates past the current date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                    GoSearch = 0
                    SA_OpenDate = 0
                ElseIf Date.Compare(FromDate, ThruDate) > 0 Then
                    'switching date around so they are in correct order.
                    Dim NewThruDate As Date
                    NewThruDate = FromDate
                    FromDate = ThruDate
                    ThruDate = NewThruDate
                    lblError.Text = ""
                End If
            Else  'one of the dates is not a valid date
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("The date information you entered is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                GoSearch = 0
                SA_OpenDate = 0
            End If
            'ElseIf (lblError.Text = "") Then 'everything passed date validation.  Call Search procedure
            'lblError.Text = ""
        End If
    End Sub
    Private Sub DisplayArea_Hide()

        lblStoAddr.Visible = False
        lblState.Visible = False
        lblZip.Visible = False
        lblOpenDate.Visible = False
        btnDelete.Visible = False
        btnModify.Visible = False
        txtAddr.Visible = False
        txtZip.Visible = False
        cboState.Visible = False
        txtOpenDate.Visible = False
        txtStoNum.Visible = False
        txtStoName.Visible = False
        lblPhone1.Visible = False
        lblPhone2.Visible = False
        lblEMail.Visible = False
        txtPhone1.Visible = False
        txtPhone2.Visible = False
        txtEmail.Visible = False
        txtStoDim.Visible = False
        lblRegion.Visible = False
        cboRegion.Visible = False
        lbtStoSize.Visible = False
        lblDist.Visible = False
        labManager.Visible = False
        lblAsstMgr1.Visible = False
        lblAsstMgr2.Visible = False
        cboDistrict.Visible = False
        lblAsstMgr3.Visible = False
        txtStoMgr.Visible = False
        txtAsstMgr1.Visible = False
        txtAsstMgr2.Visible = False
        txtAsstMgr3.Visible = False
        txtAsstMgr4.Visible = False
        lblAsstMgr4.Visible = False
        txtAsstMgr5.Visible = False
        lblAsstMgr5.Visible = False
        lblTags.Visible = False
        txtTags.Visible = False
        lblSimon.Visible = False
        txtSimon.Visible = False
        lblStoreNum.Visible = False
        lblStoreName.Visible = False

        lblCoName.Visible = False
        txtCoName.Visible = False
        lblShipNumb.Visible = False
        txtShipNumb.Visible = False

        lblCity.Visible = False
        txtCity.Visible = False

        headStore.Visible = False

        btnOK.Visible = False

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub DisplayArea_Show()

        lblStoAddr.Visible = True
        lblState.Visible = True
        lblZip.Visible = True
        lblOpenDate.Visible = True
        ' btnDelete.Visible = True
        'btnModify.Visible = True
        txtAddr.Visible = True
        txtZip.Visible = True
        cboState.Visible = True
        txtOpenDate.Visible = True
        txtStoNum.Visible = True
        txtStoName.Visible = True
        lblPhone1.Visible = True
        lblPhone2.Visible = True
        lblEMail.Visible = True
        txtPhone1.Visible = True
        txtPhone2.Visible = True
        txtEmail.Visible = True
        txtStoDim.Visible = True
        lblRegion.Visible = True
        cboRegion.Visible = True
        lbtStoSize.Visible = True
        lblDist.Visible = True
        labManager.Visible = True
        lblAsstMgr1.Visible = True
        lblAsstMgr2.Visible = True
        cboDistrict.Visible = True
        lblAsstMgr3.Visible = True
        txtStoMgr.Visible = True
        txtAsstMgr1.Visible = True
        txtAsstMgr2.Visible = True
        txtAsstMgr3.Visible = True
        txtAsstMgr4.Visible = True
        lblAsstMgr4.Visible = True
        txtAsstMgr5.Visible = True
        lblAsstMgr5.Visible = True
        lblTags.Visible = True
        txtTags.Visible = True
        lblSimon.Visible = True
        txtSimon.Visible = True

        txtCoName.Visible = True
        lblCoName.Visible = True

        lblStoreNum.Visible = True
        lblStoreName.Visible = True

        '     lblShipNumb1.Visible = True
        '       txtShipNumb.Visible = True

        lblCity.Visible = True
        txtCity.Visible = True

        headStore.Visible = True

        btnOK.Visible = False

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim StoNum As Integer
        Dim StoName As String
        Dim StoState As String
        Dim StoEmail As String
        Dim StoOpenDate As String
        Dim StoRegion As String
        Dim StoDist As String
        Dim StoTag As String
        Dim StoSimon As String
        Dim StoCity As String
        Dim StoMgr As String
        Dim StoSize As String
        Dim StoSize2 As String

        Dim Item As Integer 'Identifies which Vars user enters

        'Hide msgs
        lblNoResults.Visible = False

        'Set Header
        headStore.Text = "Store Data"

        'Hide Display Area
        DisplayArea_Hide()

        'Flag
        GoSearch = False

        '-----------------------------------------------------------------------------
        ' Input Validation happens here
        '
        '-----------------------------------------------------------------------------

        'Check if both fields are used
        'Check if both fields are used
        If txtSrchStoNum.Text = Nothing And txtSrchStoName.Text = Nothing And txtSrchEmail.Text = Nothing And _
        (txtFDate.Text = Nothing Or txtTDate.Text = Nothing) And txtSrchSimon.Text = Nothing And txtSrchTag.Text = Nothing And _
        (txtSize.Text = Nothing Or txtSize2.Text = Nothing) And _
        cboSrchRegion.SelectedItem.Value = Nothing And _
        cboCity.SelectedItem.Value = Nothing And _
        cboMgr.SelectedItem.Value = Nothing And _
        cboSrchState.SelectedItem.Value = Nothing And cboSrchDist.SelectedItem.Value = Nothing Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("No Search Data entered. Please fill in at least one search field.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            GoSearch = 0
        End If

        'StoreNum (1) 
        If txtSrchStoNum.Text <> Nothing Then
            StoNum = ParseQuotes(txtSrchStoNum.Text)

            SA_Num = 1
            GoSearch = True
        End If

        'StoreName (2) 
        If txtSrchStoName.Text <> Nothing Then
            StoName = ParseQuotes(txtSrchStoName.Text)
            StoName = "%" & StoName & "%"

            SA_Name = 1
            GoSearch = True
        End If

        'Email (3)
        If txtSrchEmail.Text <> Nothing Then
            StoEmail = ParseQuotes(txtSrchEmail.Text)
            StoEmail = "%" & StoEmail & "%"

            SA_Email = 1
            GoSearch = True
        End If

        'Simon (5) ''
        If txtSrchSimon.Text <> Nothing Then
            StoSimon = ParseQuotes(txtSrchSimon.Text)
            StoSimon = "%" & StoSimon & "%"

            SA_Simon = 1
            GoSearch = True
        End If

        'Tag(6) 
        If txtSrchTag.Text <> Nothing Then
            StoTag = ParseQuotes(txtSrchTag.Text)
            StoTag = "%" & StoTag & "%"

            SA_Tags = 1
            GoSearch = True
        End If

        'State(7)
        If cboSrchState.SelectedItem.Value <> Nothing Then
            StoState = ParseQuotes(cboSrchState.SelectedItem.Value)
            'StoState = "%" & StoState & "%"

            SA_State = 1
            GoSearch = True
        End If

        'District(8) 
        If cboSrchDist.SelectedItem.Value <> Nothing Then
            StoDist = ParseQuotes(cboSrchDist.SelectedItem.Value)
            'StoDist = "%" & StoDist & "%"

            SA_Dist = 1
            GoSearch = True
        End If

        'Region (9)
        If cboSrchRegion.SelectedItem.Value <> Nothing Then
            StoRegion = ParseQuotes(cboSrchRegion.SelectedItem.Value)
            'StoRegion = "%" & StoRegion & "%"

            SA_Region = 1
            GoSearch = True
        End If

        'City(10)
        If cboCity.SelectedItem.Value <> Nothing Then
            StoCity = ParseQuotes(cboCity.SelectedItem.Value)
            'StoState = "%" & StoState & "%"

            SA_City = 1
            GoSearch = True
        End If

        'Manager(11)
        If cboMgr.SelectedItem.Value <> Nothing Then
            StoMgr = ParseQuotes(cboMgr.SelectedItem.Value)
            'StoState = "%" & StoState & "%"

            SA_Mgr = 1
            GoSearch = True
        End If

        'Size(12) 
        If txtSize.Text <> Nothing Then
            StoSize = ParseQuotes(txtSize.Text)
            '     StoSize = "%" & StoSize & "%"
            SA_Size = 1
            GoSearch = True
        End If

        'Size2(13) 
        If txtSize2.Text <> Nothing Then
            StoSize2 = ParseQuotes(txtSize2.Text)
            '     StoSize2 = "%" & StoSize2 & "%"
            SA_Size = 1
            GoSearch = True
        End If

        If txtFDate.Text = Nothing And txtTDate.Text <> Nothing Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter From Date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            GoSearch = 0
        Else
            If txtFDate.Text <> Nothing And txtTDate.Text = Nothing Then
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter To Date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                GoSearch = 0
            End If
        End If

        If txtSize.Text = Nothing And txtSize2.Text <> Nothing Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter beginning range for sq. feet.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            GoSearch = 0
        Else
            If txtSize.Text <> Nothing And txtSize2.Text = Nothing Then
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter ending range for sq. feet.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                GoSearch = 0
            End If
        End If

        'OpenDate (4)
        If txtFDate.Text <> Nothing Or txtTDate.Text <> Nothing Then
            ValidateDate()
        End If

        'If all fields are not empty
        If GoSearch = True Then
            actSearch(StoNum, StoName, StoState, StoEmail, StoOpenDate, StoRegion, StoDist, StoTag, StoSimon, StoCity, StoMgr, StoSize, StoSize2)
            'actSearchVars(StoNum, StoName, StoState, StoEmail, StoOpenDate, StoRegion, StoDist, StoTag, StoSimon)
            Call SearchArea_Show()
        End If

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub actSearch(ByVal StoNum, ByVal StoName, ByVal StoState, ByVal StoEmail, ByVal StoOpenDate, _
    ByVal StoRegion, ByVal StoDist, ByVal StoTag, ByVal StoSimon, ByVal StoCity, ByVal StoMgr, ByVal StoSize, ByVal StoSize2)

        ConnHeaderOpen()

        'commands
        Dim cmdHeader As System.Data.SqlClient.SqlCommand
        Dim CommandString As String = "SELECT  StoreNum AS 'Store Number', StoreName AS 'Store Name', Email, City, State, Region, District, Tags, Simon, OpenDate, Manager, Dimension, Address, Phone1 FROM STORE WHERE 'x' = 'x'"

        If SA_Num = 1 Then 'QA'd
            CommandString = CommandString & " AND StoreNum = " & StoNum
        End If

        If SA_Name = 1 Then 'QA'd
            CommandString = CommandString & " AND StoreName LIKE '" & StoName & "'"
        End If

        If SA_Email = 1 Then
            CommandString = CommandString & " AND Email LIKE '" & StoEmail & "'"
        End If

        If SA_State = 1 Then
            CommandString = CommandString & " AND State LIKE '" & StoState & "'"
        End If

        If SA_OpenDate = 1 Then
            CommandString = CommandString & " AND OpenDate BETWEEN '" & FromDate & "' AND '" & ThruDate & "'"
        End If

        If SA_Region = 1 Then 'QA'd
            CommandString = CommandString & " AND Region LIKE '" & StoRegion & "'"
        End If

        If SA_Dist = 1 Then 'QA'd
            CommandString = CommandString & " AND District = " & StoDist
        End If

        If SA_Tags = 1 Then
            CommandString = CommandString & " AND Tags = " & StoTag
        End If

        If SA_Simon = 1 Then
            CommandString = CommandString & " AND Simon = " & StoSimon
        End If

        If SA_City = 1 Then
            CommandString = CommandString & " AND City = '" & StoCity & "'"
        End If

        If SA_Mgr = 1 Then
            CommandString = CommandString & " AND Manager = '" & StoMgr & "'"
        End If

        If SA_Size = 1 Then
            CommandString = CommandString & " AND Dimension BETWEEN " & StoSize & " AND " & StoSize2
        End If


        'Execution 
        'cmdHeader = New SqlClient.SqlCommand(CommandString, connHeader)
        'cmdHeader.ExecuteNonQuery()


        ''Binding to DataGrid
        'Dim ListStores As System.Data.SqlClient.SqlDataReader
        'ListStores = cmdHeader.ExecuteReader()
        'dgSearch.DataSource = ListStores
        'dgSearch.DataBind()

        ''Close Conn
        'ConnHeaderClose()

        '************************************************
        'SKU Search Example
        'Dim connectionString As String
        'connectionString = "Provider=MSDAORA.1; Data Source=genret; User ID=hottopic; Password=hottopic;"
        'Dim myDataAdapter As New System.Data.OleDb.OleDbDataAdapter(CommandString, connectionString)
        'Dim myDataSet As New DataSet()
        'myDataAdapter.Fill(myDataSet, "Items")
        'myDataTable = myDataSet.Tables(0)
        'dgResults.DataSource = myDataTable

        Dim myDataAdapter As New System.Data.SqlClient.SqlDataAdapter(CommandString, connHeader)
        Dim myDataSet As New DataSet()
        myDataAdapter.Fill(myDataSet, "Items")
        myDataTable = myDataSet.Tables(0)
        dgSearch.DataSource = myDataTable
        dgSearch.DataBind()

    End Sub


    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnHeaderOpen()

        'connection
        connHeader = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        connHeader.Open()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnHeaderClose()

        connHeader.Close()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub actSearchVars(ByVal StoNum As Integer, ByVal StoName As String, ByVal StoState As String, _
    ByVal StoEmail As String, ByVal StoOpenDate As String, ByVal StoRegion As String, ByVal StoDist As String, _
    ByVal StoTag As String, ByVal StoSimon As String, ByVal StoCity As String, ByVal StoMgr As String, _
    ByVal StoSize As String, ByVal StoSize2 As String)

        'command
        Dim cmdHeader As System.Data.SqlClient.SqlCommand 'Ssvr 
        Dim cmdHeaderCount As System.Data.SqlClient.SqlCommand 'Ssvr 

        'parameters
        Dim pStoNum As SqlClient.SqlParameter
        Dim pStoName As SqlClient.SqlParameter
        Dim pAddr As SqlClient.SqlParameter
        Dim pCity As SqlClient.SqlParameter
        Dim pState As SqlClient.SqlParameter
        Dim pZip As SqlClient.SqlParameter
        Dim pPhone1 As SqlClient.SqlParameter
        Dim pPhone2 As SqlClient.SqlParameter
        Dim pEmail As SqlClient.SqlParameter
        Dim pOpen As SqlClient.SqlParameter
        Dim pDim As SqlClient.SqlParameter
        Dim pReg As SqlClient.SqlParameter
        Dim pDist As SqlClient.SqlParameter
        Dim pGM As SqlClient.SqlParameter
        Dim pAM1d As SqlClient.SqlParameter
        Dim pAM2 As SqlClient.SqlParameter
        Dim pAM3 As SqlClient.SqlParameter
        Dim pAM4 As SqlClient.SqlParameter
        Dim pAM5 As SqlClient.SqlParameter
        Dim pTags As SqlClient.SqlParameter
        Dim pSimon As SqlClient.SqlParameter
        Dim pCoName As SqlClient.SqlParameter
        Dim pShipNumb As SqlClient.SqlParameter
        'connection
        connHeader = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        connHeader.Open()

        'Check the inputs and assign good parameters on null
        If StoNum = Nothing Then
            StoNum = 0
        End If

        If StoName = Nothing Then
            StoName = " "
        End If

        'inputs (1) 
        cmdHeader = New SqlClient.SqlCommand("spFindStore", connHeader)
        cmdHeader.CommandType = CommandType.StoredProcedure
        cmdHeader.Parameters.Add("@StoreNum", StoNum)
        cmdHeader.Parameters.Add("@StoreName", StoName)

        'Outputs 
        pStoNum = cmdHeader.Parameters.Add("@sNum", SqlDbType.VarChar)
        pStoName = cmdHeader.Parameters.Add("@sName", SqlDbType.VarChar)
        pAddr = cmdHeader.Parameters.Add("@Address", SqlDbType.VarChar)
        pCity = cmdHeader.Parameters.Add("@City", SqlDbType.VarChar)
        pState = cmdHeader.Parameters.Add("@State", SqlDbType.VarChar)
        pZip = cmdHeader.Parameters.Add("@Zip", SqlDbType.VarChar)
        pPhone1 = cmdHeader.Parameters.Add("@Phone1", SqlDbType.VarChar)
        pPhone2 = cmdHeader.Parameters.Add("@Phone2", SqlDbType.VarChar)
        pEmail = cmdHeader.Parameters.Add("@Email", SqlDbType.VarChar)
        pOpen = cmdHeader.Parameters.Add("@Open", SqlDbType.VarChar)
        pDim = cmdHeader.Parameters.Add("@Dim", SqlDbType.VarChar)
        pReg = cmdHeader.Parameters.Add("@Region", SqlDbType.VarChar)
        pDist = cmdHeader.Parameters.Add("@Dist", SqlDbType.VarChar)
        pGM = cmdHeader.Parameters.Add("@Mgr", SqlDbType.VarChar)
        pAM1d = cmdHeader.Parameters.Add("@Am1", SqlDbType.VarChar)
        pAM2 = cmdHeader.Parameters.Add("@Am2", SqlDbType.VarChar)
        pAM3 = cmdHeader.Parameters.Add("@Am3", SqlDbType.VarChar)
        pAM4 = cmdHeader.Parameters.Add("@Am4", SqlDbType.VarChar)
        pAM5 = cmdHeader.Parameters.Add("@Am5", SqlDbType.VarChar)
        pTags = cmdHeader.Parameters.Add("@Tags", SqlDbType.VarChar)
        pSimon = cmdHeader.Parameters.Add("@Simon", SqlDbType.VarChar)
        pCoName = cmdHeader.Parameters.Add("@CoName", SqlDbType.VarChar)
        pShipNumb = cmdHeader.Parameters.Add("@ShipNumb", SqlDbType.VarChar)

        'Declare Size
        pStoNum.Size = 100
        pStoName.Size = 40
        pAddr.Size = 60
        pCity.Size = 30
        pState.Size = 30
        pZip.Size = 6
        pPhone1.Size = 15
        pPhone2.Size = 15
        pEmail.Size = 40
        pOpen.Size = 15
        pDim.Size = 10
        pReg.Size = 10
        pDist.Size = 10
        pGM.Size = 40
        pAM1d.Size = 40
        pAM2.Size = 40
        pAM3.Size = 40
        pAM4.Size = 40
        pAM5.Size = 40
        pTags.Size = 1
        pSimon.Size = 1
        pCoName.Size = 25
        pShipNumb.Size = 10


        'Declare Direction
        pStoNum.Direction = ParameterDirection.Output
        pStoName.Direction = ParameterDirection.Output
        pAddr.Direction = ParameterDirection.Output
        pCity.Direction = ParameterDirection.Output
        pState.Direction = ParameterDirection.Output
        pZip.Direction = ParameterDirection.Output
        pPhone1.Direction = ParameterDirection.Output
        pPhone2.Direction = ParameterDirection.Output
        pEmail.Direction = ParameterDirection.Output
        pOpen.Direction = ParameterDirection.Output
        pDim.Direction = ParameterDirection.Output
        pReg.Direction = ParameterDirection.Output
        pDist.Direction = ParameterDirection.Output
        pGM.Direction = ParameterDirection.Output
        pAM1d.Direction = ParameterDirection.Output
        pAM2.Direction = ParameterDirection.Output
        pAM3.Direction = ParameterDirection.Output
        pAM4.Direction = ParameterDirection.Output
        pAM5.Direction = ParameterDirection.Output
        pTags.Direction = ParameterDirection.Output
        pSimon.Direction = ParameterDirection.Output
        pCoName.Direction = ParameterDirection.Output


        '++++++
        pShipNumb.Direction = ParameterDirection.Output
        '++++ Query Count +++++

        'Inputs (2) 
        cmdHeaderCount = New SqlClient.SqlCommand("spFindStoreCount", connHeader)
        cmdHeaderCount.CommandType = CommandType.StoredProcedure
        cmdHeaderCount.Parameters.Add("@StoreNum", StoNum)
        cmdHeaderCount.Parameters.Add("@StoreName", StoName)

        cmdHeaderCount.Parameters.Add("@StoState", StoState)
        cmdHeaderCount.Parameters.Add("@StoEmail", StoEmail)
        cmdHeaderCount.Parameters.Add("@StoOpen", StoOpenDate)
        cmdHeaderCount.Parameters.Add("@StoRegion", StoRegion)
        cmdHeaderCount.Parameters.Add("@StoDist", StoDist)
        cmdHeaderCount.Parameters.Add("@StoTag", StoTag)
        cmdHeaderCount.Parameters.Add("@StoSimon", StoSimon)
        cmdHeaderCount.Parameters.Add("@StoCity", StoCity)
        cmdHeaderCount.Parameters.Add("@StoMgr", StoMgr)
        cmdHeaderCount.Parameters.Add("@StoSize", StoSize)
        cmdHeaderCount.Parameters.Add("@StoSize2", StoSize2)

        'Outputs (2) 
        'Dim pCount As SqlClient.SqlParameter
        'pCount = cmdHeaderCount.Parameters.Add("@count", SqlDbType.VarChar)

        'Direction(2) 
        'pCount.Direction = ParameterDirection.Output

        'Size(2) 
        'pCount.Size = 10

        'Execute both extract data and count data -------------------

        cmdHeader.ExecuteNonQuery()
        cmdHeaderCount.ExecuteNonQuery()

        '------------------------------------------------------------

        'Bind Search results to display
        Dim ListStores As System.Data.SqlClient.SqlDataReader
        ListStores = cmdHeaderCount.ExecuteReader()
        dgSearch.DataSource = ListStores
        dgSearch.DataBind()

        'Dim Count As Integer 'TEST AREA ONLY 
        'Count = cmdHeaderCount.Parameters("@Count").Value
        'Count = pCount.Value

        'If IsDBNull(pStoNum.Value) Then
        'Count = pStoNum.Value()
        'End If

        connHeader.Close()

        dgSearch.Visible = True

    End Sub 'End actSearchVars

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub DataToLocal()

        gStoNum = txtStoNum.Text
        gStoName = txtStoName.Text
        gAddr = txtAddr.Text
        gState = cboState.SelectedItem.Text
        gZip = txtZip.Text
        gPhone1 = txtPhone1.Text
        gPhone2 = txtPhone2.Text
        gEmail = txtEmail.Text
        gOpen = txtOpenDate.Text
        gDim = txtStoDim.Text
        'gRegion = cboRegion.SelectedItem.Text
        'gDist = cboDistrict.SelectedItem.Text
        gStoMgr = txtStoMgr.Text
        gAM1 = txtAsstMgr1.Text
        gAM2 = txtAsstMgr2.Text
        gAM3 = txtAsstMgr3.Text
        gAM4 = txtAsstMgr4.Text
        gAM5 = txtAsstMgr5.Text
        gTags = txtTags.Text
        gSimon = txtSimon.Text
        gCoName = txtCoName.Text
        gShipNumb = txtShipNumb.Text

    End Sub


    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_State()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(State) FROM STORE"

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboSrchState.DataSource = dataTable.DefaultView
        cboSrchState.DataTextField = "State"
        cboSrchState.DataValueField = "State"
        cboSrchState.DataBind()
        cboSrchState.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub

    Private Sub build_DDL_City()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(City) FROM STORE"

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboCity.DataSource = dataTable.DefaultView
        cboCity.DataTextField = "City"
        cboCity.DataValueField = "City"
        cboCity.DataBind()
        cboCity.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub

    Private Sub build_DDL_City_by_State()

        ConnHeaderOpen()

        'helen
        cboCity.SelectedIndex = 0

        'commands
        Dim CommandString As String = "SELECT DISTINCT(City) FROM STORE WHERE State LIKE '" & cboSrchState.SelectedItem.Text & "'"

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboCity.DataSource = dataTable.DefaultView
        cboCity.DataTextField = "City"
        cboCity.DataValueField = "City"
        cboCity.DataBind()
        cboCity.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub

    Private Sub build_DDL_Mgr()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(Manager) FROM STORE"

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboMgr.DataSource = dataTable.DefaultView
        cboMgr.DataTextField = "Manager"
        cboMgr.DataValueField = "Manager"
        cboMgr.DataBind()
        cboMgr.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub
    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_DisplayState()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(State) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboState.DataSource = dataTable.DefaultView
        cboState.DataTextField = "State"
        cboState.DataValueField = "State"
        cboState.DataBind()
        cboState.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub

    Private Sub build_DDL_DisplayCity()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(City) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboCity.DataSource = dataTable.DefaultView
        cboCity.DataTextField = "City"
        cboCity.DataValueField = "City"
        cboCity.DataBind()
        cboCity.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub


    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_DisplayMgr()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(Manager) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboMgr.DataSource = dataTable.DefaultView
        cboMgr.DataTextField = "Manager"
        cboMgr.DataValueField = "Manager"
        cboMgr.DataBind()
        cboMgr.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub


    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_Region()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(Region) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboSrchRegion.DataSource = dataTable.DefaultView
        cboSrchRegion.DataTextField = "Region"
        cboSrchRegion.DataBind()
        cboSrchRegion.Items.Insert(0, "")


        ConnHeaderClose()

    End Sub


    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_DisplayRegion()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(Region) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboRegion.DataSource = dataTable.DefaultView
        cboRegion.DataTextField = "Region"
        cboRegion.DataBind()
        cboRegion.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub


    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_DisplayDistrict()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(District) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboDistrict.DataSource = dataTable.DefaultView
        cboDistrict.DataTextField = "District"
        cboDistrict.DataBind()
        cboDistrict.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_District()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(District) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboSrchDist.DataSource = dataTable.DefaultView
        cboSrchDist.DataTextField = "District"
        cboSrchDist.DataBind()
        cboSrchDist.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub actInsertVars()

        ConnHeaderOpen()

        'Txfer Data 
        DataToLocal()

        'Commands()
        Dim cmdHeader As System.Data.SqlClient.SqlCommand 'Ssvr 

        'Inputs
        cmdHeader = New SqlClient.SqlCommand("spInsertStoreRow", connHeader)
        cmdHeader.CommandType = CommandType.StoredProcedure
        cmdHeader.Parameters.Add("@StoNum", gStoNum)
        cmdHeader.Parameters.Add("@StoName", gStoName)
        cmdHeader.Parameters.Add("@Addr", gAddr)
        cmdHeader.Parameters.Add("@City", gCity)
        cmdHeader.Parameters.Add("@State", gState)
        cmdHeader.Parameters.Add("@Zip", gZip)
        cmdHeader.Parameters.Add("@Phone1", gPhone1)
        cmdHeader.Parameters.Add("@Phone2", gPhone2)
        cmdHeader.Parameters.Add("@Email", gEmail)
        cmdHeader.Parameters.Add("@OpenDate", gOpen)
        cmdHeader.Parameters.Add("@Dimension", gDim)
        cmdHeader.Parameters.Add("@Region", gRegion)
        cmdHeader.Parameters.Add("@District", gRegion)
        cmdHeader.Parameters.Add("@Manager", gStoMgr)
        cmdHeader.Parameters.Add("@AsstMgr1", gAM1)
        cmdHeader.Parameters.Add("@AsstMgr2", gAM2)
        cmdHeader.Parameters.Add("@AsstMgr3", gAM3)
        cmdHeader.Parameters.Add("@AsstMgr4", gAM4)
        cmdHeader.Parameters.Add("@AsstMgr5", gAM5)
        cmdHeader.Parameters.Add("@Tags", gTags)
        cmdHeader.Parameters.Add("@Simon", gSimon)
        cmdHeader.Parameters.Add("@CoName", gCoName)
        cmdHeader.Parameters.Add("@ShipNumb", gShipNumb)
        cmdHeader.ExecuteNonQuery()

        connHeader.Close()

        'User messages here.

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Function ParseQuotes(ByVal ParseText As String) As String
        Dim Offset As Integer, X As Integer

        ParseText = Trim$(ParseText) 'optional but it is a good place to do it
        X = InStr(ParseText, "'")
        If X > 0 Then
            Offset = 1
            Do While X > 0
                ParseText = Left$(ParseText, X) & Mid$(ParseText, X)

                'copies the single quote twice
                Offset = X + 2     'get past both quotes for search to resume
                X = InStr(Offset, ParseText, "'")
            Loop
        End If

        ParseQuotes = ParseText

    End Function

    '-----------------------------------------------------------------------------
    'Show the login area when byn clicked
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Login_Show()

    End Sub

    '-----------------------------------------------------------------------------
    'Hide the login area when btn clicked
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        txtUserID.Text = Nothing
        Login_Hide()

    End Sub

    '-----------------------------------------------------------------------------
    ' Actions taken when Login Submit button clicked 
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim fEmpId As Integer
        Dim TfEmpID As String

        TfEmpID = Trim(txtUserID.Text)

        'Input Validation :: If Non-numeric entered
        If Not IsNumeric(TfEmpID) Then

            'set field to none
            txtUserID.Text = Nothing

            'show err msg
            lblLoginMsg.Visible = True

            'fill err msg
            lblLoginMsg.Text = "Incorrect Input Type. Please enter Employee ID"

            'login show
            Login_Show()

        Else
            'If Numeric Value entered
            'Assign the Values 
            fEmpId = CInt(TfEmpID)

            'Hide the login 
            Login_Hide()

            'Check the return val
            Dim Chk As Integer
            Chk = OK_User(fEmpId)

            If Chk > 0 Then
                Show_AddStore()
                txtUserID.Text = Nothing
            Else
                Login_Show()
                txtUserID.Text = Nothing
                lblLoginMsg.Visible = True
                lblLoginMsg.Text = "Employee ID not found. Please enter another Employee ID."
            End If

        End If

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnOra_Open()

        connOra = New System.Data.OleDb.OleDbConnection("Provider=MSDAORA.1;Password=hottopic;User ID=hottopic;Data Source=genret")
        connOra.Open()

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnOra_Close()

        connOra.Close()

    End Sub

    '-----------------------------------------------------------------------------
    ' Connects to Database and checks if the user exists, returns value if exists
    '
    '
    '-----------------------------------------------------------------------------

    Public Function OK_User(ByVal fEmpID) As Integer

        Dim Boole As Integer

        ConnOra_Open()

        'returns the count number of employees found for that
        'employeeid
        Dim cmdOKUser As System.Data.OleDb.OleDbCommand
        cmdOKUser = New OleDbCommand("fxnValidEmp", connOra)
        cmdOKUser.CommandType = CommandType.StoredProcedure

        Dim prmOUT As OleDbParameter
        Dim prmIN As OleDbParameter

        prmOUT = cmdOKUser.CreateParameter()
        prmOUT.Direction = ParameterDirection.ReturnValue
        prmOUT.OleDbType = OleDbType.Integer
        prmOUT.Size = 100

        prmIN = cmdOKUser.CreateParameter()
        prmIN.Direction = ParameterDirection.Input
        prmIN.OleDbType = OleDbType.Integer
        prmIN.Size = 100
        prmIN.Value = fEmpID

        cmdOKUser.Parameters.Add(prmOUT)
        cmdOKUser.Parameters.Add(prmIN)

        cmdOKUser.ExecuteNonQuery()

        ''Function :)
        OK_User = prmOUT.Value

        ConnOra_Close()

    End Function

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub Show_AddStore()

        'Set Header
        headStore.Text = "Add New Store"

        'clear all the cells
        ClearCells()

        'build the pull downs
        build_DDL_DisplayDistrict()
        build_DDL_DisplayRegion()
        build_DDL_DisplayState()
        build_DDL_DisplayCity()
        build_DDL_DisplayMgr()

        'Show the area
        DisplayArea_Show()

        '..but not the Delete Button
        btnDelete.Visible = False

    End Sub

    '-----------------------------------------------------------------------------
    'clears the cells in the store display area
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub ClearCells()

        'Num (1)
        txtSrchStoNum.Text = ""
        'Name(2)
        txtSrchStoName.Text = ""
        'Addr (3)
        txtAddr.Text = ""
        'City (4)
        txtCity.Text = ""
        cboCity.SelectedIndex = 0
        'State (5)
        cboState.SelectedIndex = 0
        'Region 
        cboSrchRegion.SelectedIndex = 0
        'District 
        cboSrchDist.SelectedIndex = 0
        'Zip (6) 
        txtZip.Text = ""
        'Phone1 (7) 
        txtPhone1.Text = ""
        'Phone2 (8) 
        txtPhone2.Text = ""
        'Email (9) 
        txtEmail.Text = ""
        'Date range (10) 
        txtOpenDate.Text = ""
        txtFDate.Text = ""
        txtTDate.Text = ""
        'Dim (11)
        txtStoDim.Text = ""
        'State
        cboSrchState.SelectedIndex = 0
        'Reg (12) 
        cboRegion.SelectedIndex = 0
        'District (13) 
        cboSrchDist.SelectedIndex = 0
        'GM (14) 
        cboMgr.SelectedIndex = 0
        txtStoMgr.Text = ""
        'AM1 (15) 
        txtAsstMgr1.Text = ""
        'AM2 (16)
        txtAsstMgr2.Text = ""
        'AM3 (17) 
        txtAsstMgr3.Text = ""
        'AM4 (18) 
        txtAsstMgr4.Text = ""
        'AM5 (19) 
        txtAsstMgr5.Text = ""
        'Tags (20) 
        txtTags.Text = ""
        'Simon (21) 
        txtSimon.Text = ""
        'Company Name (22) 
        txtCoName.Text = ""
        'Shipper Number (23) 
        txtShipNumb.Text = ""
        'Size range
        txtSize.Text = ""
        txtSize2.Text = ""

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        btnDelete.Visible = False
        btnModify.Visible = False
        ' btnOK.Visible = True

        lblSaved.Visible = True
        lblSaved.Text = "You are about to Delete Store " & txtStoNum.Text & ". " & vbCrLf & "Click OK to delete."

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim StoNum As String
        Dim StoNumF As Integer

        StoNum = Trim(txtStoNum.Text)
        StoNumF = CInt(StoNum)

        ActDelete(StoNumF)

        DisplayArea_Hide()
        btnOK.Visible = False

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub ActDelete(ByVal StoNumF)

        'open    
        ConnHeaderOpen()

        'commands
        Dim cmdHeader As System.Data.SqlClient.SqlCommand

        'inputs
        cmdHeader = New SqlClient.SqlCommand("spDeleteStore", connHeader)
        cmdHeader.CommandType = CommandType.StoredProcedure
        cmdHeader.Parameters.Add("@StoreNum", StoNumF)

        'exec
        cmdHeader.ExecuteNonQuery()

        'close
        ConnHeaderClose()

        lblNoResults.Text = "Store " & StoNumF & " successfully deleted."
        lblNoResults.Visible = True

        'Should Implment Timer here

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


        actInsertVars()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click

        DisplayArea_Hide()

        dgSearch.Visible = False

        ClearCells()

        build_DDL_City()

    End Sub

    Private Sub cboSrchState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSrchState.SelectedIndexChanged
        If cboSrchState.SelectedIndex = 0 Then
            Call build_DDL_DisplayCity()
        Else
            Call build_DDL_City_by_State()
        End If
    End Sub

    Private Sub actSearch(ByVal CommandString)

        ConnHeaderOpen()

        'commands
        Dim cmdHeader As System.Data.SqlClient.SqlCommand

        'Execution 
        cmdHeader = New SqlClient.SqlCommand(CommandString, connHeader)
        cmdHeader.ExecuteNonQuery()

        With dgSearch
            .AllowPaging = True
            .PagerStyle.Mode = PagerMode.NumericPages
            .PagerStyle.PageButtonCount = 5
            .PageSize = 5
        End With

        'Binding to DataGrid - Working - OLD 
        'Dim ListStores As System.Data.SqlClient.SqlDataReader
        'ListStores = cmdHeader.ExecuteReader()
        'dgSearch.DataSource = ListStores
        'dgSearch.DataBind()

        'New 
        'dec datatable in global 
        Dim myDataAdapter As New System.Data.SqlClient.SqlDataAdapter(CommandString, connHeader)
        Dim myDataSet As New DataSet()

        myDataAdapter.Fill(myDataSet, "Stores")
        myDataTable = myDataSet.Tables(0)
        dgSearch.DataSource = myDataTable
        dgSearch.DataBind()

        If Not Page.IsPostBack Then
            dgSearch.DataBind()
        End If

        'Close Conn
        ConnHeaderClose()

    End Sub

End Class
