<%@ Page Language="vb" AutoEventWireup="false" Codebehind="StoreDetail.aspx.vb" Inherits="SSTransfer.StoreDetail"%>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Store Detail</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript">
			<!--
			javascript:window.history.forward(1);
			//-->
		</script>
	</HEAD>
	<body bgColor="black" leftMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD height="1" style="WIDTH: 748px"><uc1:header id="ucHeader" runat="server" Visible="False" EnableViewState="False"></uc1:header></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 749px">
						<TABLE id="tblDisplay" style="BORDER-TOP-WIDTH: thin; BORDER-LEFT-WIDTH: thin; BORDER-LEFT-COLOR: #cc0033; BORDER-BOTTOM-WIDTH: thin; BORDER-BOTTOM-COLOR: #cc0033; BORDER-TOP-COLOR: #cc0033; BORDER-RIGHT-WIDTH: thin; BORDER-RIGHT-COLOR: #cc0033" cellSpacing="0" cellPadding="0" width="100%" align="left" border="0">
							<TR>
								<TD style="WIDTH: 148px" align="middle"></TD>
								<TD style="WIDTH: 173px"><SPAN><asp:label id="lblSaved" runat="server" Font-Bold="True" ForeColor="Red" BackColor="Transparent" Font-Names="Arial" Font-Size="Smaller">Record Saved.</asp:label></SPAN></TD>
								<TD style="WIDTH: 137px" align="left"></TD>
								<TD style="WIDTH: 282px"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px; HEIGHT: 25px" align="middle">
									<asp:button id="btnNewSearch" runat="server" BackColor="#440000" ForeColor="White" Font-Bold="True" Width="101px" BorderColor="#990000" Text="New Search" Enabled="False" Visible="False"></asp:button></TD>
								<TD style="WIDTH: 173px; HEIGHT: 25px"><SPAN title="Store Detail"><asp:label id="lblRecordDeleted" runat="server" Font-Bold="True" ForeColor="Red" BackColor="Transparent" Font-Names="Arial" Font-Size="Smaller">Record Deleted.</asp:label></SPAN></TD>
								<TD style="WIDTH: 137px; HEIGHT: 25px" align="left">&nbsp;</TD>
								<TD style="WIDTH: 282px; HEIGHT: 25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:Button id="btnReturn" runat="server" Font-Size="Smaller" BackColor="#440000" ForeColor="White" Font-Bold="True" Width="79px" BorderColor="#990000" Text="Return"></asp:Button></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px" align="right"><asp:label id="lblStoreNum" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">Store Number :</asp:label></TD>
								<TD style="WIDTH: 173px" align="left"><asp:textbox id="txtStoNum" tabIndex="1" runat="server" BackColor="White" Width="60px" MaxLength="4" Height="20px"></asp:textbox></TD>
								<TD style="WIDTH: 137px" align="right"><asp:label id="lbtStoSize" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small"> Sq. Feet :</asp:label></TD>
								<TD style="WIDTH: 282px"><asp:textbox id="txtStoDim" tabIndex="11" runat="server" Width="60px" Height="20px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px" align="right"><asp:label id="lblStoreName" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">Store Name :</asp:label></TD>
								<TD style="WIDTH: 173px"><asp:textbox id="txtStoName" tabIndex="2" runat="server" Width="200px" Height="20px"></asp:textbox></TD>
								<TD style="WIDTH: 137px" align="right"><asp:label id="lblRegion" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">Region :</asp:label></TD>
								<TD style="WIDTH: 282px"><asp:dropdownlist id="cboRegion" tabIndex="12" runat="server" Width="60px" Height="20px"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px" align="right"><asp:label id="lblStoAddr" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">Address :</asp:label></TD>
								<TD style="WIDTH: 173px"><asp:textbox id="txtAddr" tabIndex="3" runat="server" Width="200px" Height="20px"></asp:textbox></TD>
								<TD style="WIDTH: 137px" align="right"><asp:label id="lblDist" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">District :</asp:label></TD>
								<TD style="WIDTH: 282px"><asp:dropdownlist id="cboDistrict" tabIndex="13" runat="server" Width="60px" Height="20px"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px; HEIGHT: 23px" align="right"><asp:label id="lblCity" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">City :</asp:label></TD>
								<TD style="WIDTH: 173px; HEIGHT: 23px"><asp:textbox id="txtCity" tabIndex="4" runat="server" Width="200px" Height="20px"></asp:textbox></TD>
								<TD style="WIDTH: 137px; HEIGHT: 23px" align="right"><asp:label id="labManager" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small"> Manager :</asp:label></TD>
								<TD style="WIDTH: 282px; HEIGHT: 23px"><asp:textbox id="txtStoMgr" tabIndex="14" runat="server" Width="200px" Height="20px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px; HEIGHT: 13px" align="right"><asp:label id="lblState" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">State :</asp:label></TD>
								<TD style="WIDTH: 173px; HEIGHT: 13px"><asp:dropdownlist id="cboState" tabIndex="5" runat="server" Width="60px" Height="20px"></asp:dropdownlist></TD>
								<TD style="WIDTH: 137px; HEIGHT: 13px" align="right"><asp:label id="lblAsstMgr1" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small"> AM :</asp:label></TD>
								<TD style="WIDTH: 282px; HEIGHT: 13px"><asp:textbox id="txtAsstMgr1" tabIndex="15" runat="server" Width="200px" Height="20px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px; HEIGHT: 10px" align="right"><asp:label id="lblZip" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">Zip :</asp:label></TD>
								<TD style="WIDTH: 173px; HEIGHT: 10px"><asp:textbox id="txtZip" tabIndex="6" runat="server" Width="60px" MaxLength="5" Height="20px"></asp:textbox></TD>
								<TD style="WIDTH: 137px; HEIGHT: 10px" align="right"><asp:label id="lblAsstMgr2" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small"> AM :</asp:label></TD>
								<TD style="WIDTH: 282px; HEIGHT: 10px"><asp:textbox id="txtAsstMgr2" tabIndex="16" runat="server" Width="200px" Height="20px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px; HEIGHT: 21px" align="right"><asp:label id="lblPhone1" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">Phone 1:</asp:label>&nbsp;
									<asp:label id="lblP1Format" runat="server" Font-Size="XX-Small" Font-Names="Arial" BackColor="Transparent" ForeColor="White" Font-Bold="True">(xxx)xxx-xxxx</asp:label></TD>
								<TD style="WIDTH: 173px; HEIGHT: 21px"><asp:textbox id="txtPhone1" tabIndex="7" runat="server" Width="200px" MaxLength="13" Height="20px"></asp:textbox></TD>
								<TD style="WIDTH: 137px; HEIGHT: 21px" align="right"><asp:label id="lblAsstMgr3" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small"> AM :</asp:label></TD>
								<TD style="WIDTH: 282px; HEIGHT: 21px"><asp:textbox id="txtAsstMgr3" tabIndex="17" runat="server" Width="200px" Height="20px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px; HEIGHT: 18px" align="right"><asp:label id="lblPhone2" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">Phone 2:</asp:label>&nbsp;
									<asp:label id="lblP2Format" runat="server" Font-Size="XX-Small" Font-Names="Arial" BackColor="Transparent" ForeColor="White" Font-Bold="True">(xxx)xxx-xxxx</asp:label></TD>
								<TD style="WIDTH: 173px; HEIGHT: 18px"><asp:textbox id="txtPhone2" tabIndex="8" runat="server" Width="200px" MaxLength="13" Height="20px"></asp:textbox></TD>
								<TD style="WIDTH: 137px; HEIGHT: 18px" align="right"><asp:label id="lblAsstMgr4" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small"> AM :</asp:label></TD>
								<TD style="WIDTH: 282px; HEIGHT: 20px"><asp:textbox id="txtAsstMgr4" tabIndex="18" runat="server" Width="200px" Height="20px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px; HEIGHT: 30px" align="right"><asp:label id="lblEMail" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">E-Mail :</asp:label></TD>
								<TD style="WIDTH: 173px; HEIGHT: 30px"><asp:textbox id="txtEmail" tabIndex="9" runat="server" Width="200px" Height="20px"></asp:textbox></TD>
								<TD style="WIDTH: 137px; HEIGHT: 30px" align="right"><asp:label id="lblAsstMgr5" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small"> AM :</asp:label></TD>
								<TD style="WIDTH: 282px; HEIGHT: 30px"><asp:textbox id="txtAsstMgr5" tabIndex="19" runat="server" Width="200px" Height="20px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px; HEIGHT: 14px" align="right"><asp:label id="lblOpenDate" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">Open Date :</asp:label></TD>
								<TD style="WIDTH: 173px; HEIGHT: 14px"><asp:textbox id="txtOpenDate" tabIndex="10" runat="server" Width="200px" Height="20px"></asp:textbox></TD>
								<TD style="WIDTH: 137px; HEIGHT: 14px" align="right"><asp:label id="lblTags" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">Tags :</asp:label></TD>
								<TD style="WIDTH: 282px; HEIGHT: 14px"><asp:textbox id="txtTags" tabIndex="20" runat="server" Width="60px" MaxLength="1" Height="20px"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label id="lblSimon" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">Simon:</asp:label><asp:textbox id="txtSimon" tabIndex="21" runat="server" Width="60px" MaxLength="1" Height="20px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px; HEIGHT: 29px" align="right"></TD>
								<TD style="WIDTH: 173px; HEIGHT: 29px"></TD>
								<TD style="WIDTH: 137px; HEIGHT: 29px" align="right">
									<asp:label id="lblCoName" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="X-Small">Company Name :</asp:label></TD>
								<TD style="WIDTH: 282px; HEIGHT: 29px">
									<asp:textbox id="txtCoName" tabIndex="22" runat="server" Width="200px" Height="20px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px; HEIGHT: 29px" align="right"></TD>
								<TD style="WIDTH: 173px; HEIGHT: 29px"></TD>
								<TD style="WIDTH: 137px; HEIGHT: 29px" align="right">
									<asp:label id="lblShipNumb" runat="server" Font-Bold="True" ForeColor="White" BackColor="Transparent" Font-Names="Arial" Font-Size="Smaller" Enabled="False" Visible="False">Shipper Number:</asp:label></TD>
								<TD style="WIDTH: 282px; HEIGHT: 29px">
									<asp:textbox id="txtShipNumb" tabIndex="22" runat="server" Width="200px" Visible="False" Height="20px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px" align="right">
									<asp:button id="btnOK" runat="server" BackColor="#440000" ForeColor="White" Font-Bold="True" Width="130px" BorderColor="#990000" Text="Yes. I'm sure."></asp:button></TD>
								<TD style="WIDTH: 173px"><asp:button id="btnModify" runat="server" Font-Bold="True" ForeColor="White" BackColor="#440000" Text="Update Record" BorderColor="#990000" Width="126px"></asp:button></TD>
								<TD style="WIDTH: 137px" align="right"><asp:button id="btnDelete" runat="server" Font-Bold="True" ForeColor="White" BackColor="#440000" Text="Delete Record" BorderColor="#990000" Width="138px" Font-Size="Smaller"></asp:button></TD>
								<TD style="WIDTH: 282px"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 148px" align="right"></TD>
								<TD style="WIDTH: 173px"></TD>
								<TD style="WIDTH: 137px" align="right"></TD>
								<TD style="WIDTH: 282px"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD height="1" style="WIDTH: 748px"><uc1:footer id="ucFooter" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
