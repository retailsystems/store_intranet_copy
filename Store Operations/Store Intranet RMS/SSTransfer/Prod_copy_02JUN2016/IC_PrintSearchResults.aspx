<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_PrintSearchResults.aspx.vb" Inherits="SSTransfer.IC_PrintSearchResults"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Print Search Results</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" scroll="yes">
		<form id="frmPrintSearchResults" method="post" runat="server" onsubmit="return checkSubmit();">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td nowrap>
						<asp:DataGrid ID="dgSearchResults" Runat="server" Width="100%" AutoGenerateColumns="False">
							<AlternatingItemStyle ForeColor="Black" BackColor="Transparent"></AlternatingItemStyle>
							<ItemStyle Font-Size="XX-Small"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Font-Names="arial" ForeColor="Black" BackColor="Transparent"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="Box_Id" HeaderText="Transfer Number"></asp:BoundColumn>
								<asp:BoundColumn HeaderText="From Store" DataField="Sending_Store"></asp:BoundColumn>
								<asp:BoundColumn HeaderText="To Store" DataField="Receiving_Store"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Type">
									<HeaderStyle ForeColor="Black"></HeaderStyle>
									<ItemTemplate>
										<asp:Label Runat="server" Font-Name="arial" Font-Size="xx-small" ForeColor="Black" Font-Bold="False" ID="lblTType">
											<%# GetBoxTransferTypesSD(Container.dataitem("Box_Id")) %>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="Shipment_Date" dataformatstring="{0:d}" ReadOnly="True" HeaderText="Date"></asp:BoundColumn>
								<asp:BoundColumn DataField="Status_Desc" HeaderText="Box Status"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
				<tr>
					<td align="right" nowrap><INPUT type="button" value="Print" onclick="window.print()">&nbsp;<asp:button runat="server" ID="btnReturn" Text="Return"></asp:button></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
