<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_NameProfile.aspx.vb" Inherits="SSTransfer.IC_NameProfile"%>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>IC_NameProfile</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="../Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="../Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="../Javascript/AllowOneSubmit.js" language="JavaScript"></script>
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="yes" rightMargin="0">
		<form id="frmNameProfile" method="post" runat="server" onsubmit="return checkSubmit();">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<TBODY>
					<tr>
						<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
					</tr>
					<tr>
						<td vAlign="top">
							<table cellSpacing="0" cellPadding="4" width="100%" bgColor="#000000" border="0">
								<tr>
									<td noWrap width="1%"><asp:label id="lblProfileType" Runat="server" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" text="Profile Type" Height="23px" Visible="False">Profile Type:</asp:label></td>
									<td noWrap width="40%"><asp:radiobuttonlist id="rblProfileType" Runat="server" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" RepeatDirection="Horizontal" CellSpacing="1" CellPadding="1" width="100%" Visible="False" tabIndex="1">
											<asp:ListItem Value="Global">Global</asp:ListItem>
											<asp:ListItem Value="Personal" Selected="True">Personal</asp:ListItem>
										</asp:radiobuttonlist></td>
									<td noWrap></td>
								</tr>
								<tr>
									<td noWrap width="1%"><asp:label id="lblProfileName" Runat="server" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" text="Profile Name:">Profile Name:</asp:label></td>
									<td noWrap width="40%"><asp:textbox id="txtProfileName" Runat="server" width="100%" tabIndex="2"></asp:textbox></td>
									<td noWrap></td>
								</tr>
								<tr>
									<td vAlign="top" noWrap width="1%"><asp:label id="lblProDescrip" Runat="server" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" text="Profile Description:">Short Profile Description:</asp:label></td>
									<td noWrap width="40%"><asp:textbox id="txtProDescrip" Runat="server" Rows="1" width="100%" tabIndex="3"></asp:textbox></td>
									<td noWrap></td>
								</tr>
								<tr>
									<td noWrap width="1%"></td>
									<td noWrap align="right" width="40%"><asp:button id="btnSave" Runat="server" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" Text="Save" BackColor="#440000" BorderColor="#990000" tabIndex="4"></asp:button></td>
									<td noWrap></td>
								</tr>
								<tr>
									<td nowrap colspan="2"><asp:Label ID="lblError" width="100%" Runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True" ForeColor="White"></asp:Label></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
					</tr>
				</TBODY>
			</table>
		</form>
		</TR></TBODY></TABLE></FORM>
	</body>
</HTML>
