Public Class ICProfileList
    Inherits System.Web.UI.Page
    Protected WithEvents grdPersonal As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblPersonal As System.Web.UI.WebControls.Label
    Protected WithEvents lblGlobal As System.Web.UI.WebControls.Label
    Protected WithEvents dgGlobal As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ucHeader As Header
    Private objUserInfo As New UserInfo()
    Dim strUser As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()
        If Not objUserInfo.EmpOnline Then
            Response.Redirect("../SessionEnd.aspx?Mode=2")
        End If
        ucHeader.CurrentPage(Header.PageName.IC_Profiles)
        ucHeader.CurrentMode(Header.HeaderGroup.IC)

        If Not IsPostBack Then
            If Not objUserInfo.EmpId Is Nothing Then
                strUser = objUserInfo.EmpFullName
            End If
        End If
        Call FillPersonalProfileList()
        Call FillGlobalProfileList()
    End Sub
    Private Sub FillPersonalProfileList()
        'Grabs all profiles made by person logged in
        Dim strProfiles
        strProfiles = "Select * From IC_SavedProfile Where Created_By= '" & strUser & "' "
        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()

        Dim cmdSearch As New SqlClient.SqlCommand(strProfiles, conSearch)
        dtrSearch = cmdSearch.ExecuteReader

        grdPersonal.DataSource = dtrSearch
        grdPersonal.DataBind()
        dtrSearch.Close()
        conSearch.Close()
        cmdSearch.Dispose()
    End Sub
    Private Sub FillGlobalProfileList()
        'Grabs all profiles made by people other than person logged in.
        Dim strPubProfiles
        strPubProfiles = "Select * From IC_SavedProfile Where Created_By <> '" & strUser & "' "
        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()

        Dim cmdSearch As New SqlClient.SqlCommand(strPubProfiles, conSearch)
        dtrSearch = cmdSearch.ExecuteReader

        dgGlobal.DataSource = dtrSearch
        dgGlobal.DataBind()
        dtrSearch.Close()
        conSearch.Close()
        cmdSearch.Dispose()
    End Sub
End Class
