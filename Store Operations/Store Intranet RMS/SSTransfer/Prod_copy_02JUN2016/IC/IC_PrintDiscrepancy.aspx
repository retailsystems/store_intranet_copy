<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_PrintDiscrepancy.aspx.vb" Inherits="SSTransfer.IC_PrintDiscrepancy"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Print Search Results</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script src="../Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="../Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<script src="../Javascript/DisableClientBack.js" language="JavaScript"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" scroll="yes">
		<form id="frmPrintSearchResultsTT" method="post" runat="server" onsubmit="return checkSubmit();">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td nowrap>
						<asp:DataGrid ID="dgSearchResults" Runat="server" Width="100%" AutoGenerateColumns="False">
							<ItemStyle Font-Size="XX-Small"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Font-Names="arial" ForeColor="Black" BackColor="White"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="Box_Id" HeaderText="Transfer #"></asp:BoundColumn>
								<asp:BoundColumn HeaderText="From Store" DataField="Sending_Store"></asp:BoundColumn>
								<asp:BoundColumn HeaderText="To Store" DataField="Receiving_Store"></asp:BoundColumn>
								<asp:BoundColumn DataField="SKU_NUM" HeaderText="SKU"></asp:BoundColumn>
								<asp:BoundColumn DataField="Xfer_Desc" HeaderText="Type"></asp:BoundColumn>
								<asp:BoundColumn DataField="Shipment_Date" dataformatstring="{0:d}" HeaderText="Date" ReadOnly="True"></asp:BoundColumn>
								<asp:BoundColumn DataField="Status_Desc" HeaderText="Box Status"></asp:BoundColumn>
								<asp:BoundColumn DataField="GERSSTATUS_DESC" HeaderText="GERS Process Status"></asp:BoundColumn>
								<asp:BoundColumn DataField="Disc_DESC" HeaderText="Disc Status"></asp:BoundColumn>
								<asp:BoundColumn DataField="Shipped_Qty" HeaderText="Shipped Qty"></asp:BoundColumn>
								<asp:BoundColumn DataField="Received_Qty" HeaderText="Received Qty"></asp:BoundColumn>
								<asp:BoundColumn DataField="Discrepancy" HeaderText="Disc Qty"></asp:BoundColumn>
								<asp:BoundColumn DataField="Discrep_Amt" HeaderText="Disc Amt"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
				<tr>
					<td align="right" nowrap><INPUT type="button" value="Print" onclick="window.print()">&nbsp;<asp:button ID="btnReturn" Runat="server" text="Return"></asp:button></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
