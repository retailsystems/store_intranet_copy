<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_ReShip.aspx.vb" Inherits="SSTransfer.IC_ReShip"%>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>IC Re-Ship</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="../Javascript/AllowOneSubmit.js"></script>
	</HEAD>
	<body id="pageBody" bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="no" rightMargin="0" runat="server">
		<form id="frmTransferHome" onsubmit="return checkSubmit();" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="2" width="100%" bgColor="#000000" border="0">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="top" align="middle">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td noWrap><asp:label id="lblShipDate_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Ship Date:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblShipDate_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td noWrap><asp:label id="lblUPS_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Tracking #:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:hyperlink id="hlUPS" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Visible="True" Target="_blank" NavigateUrl="">
										<asp:Label runat="server" ForeColor="White" ID="lblUPS_Value" Font-Size="X-Small" Font-Names="Arial"></asp:Label>
									</asp:hyperlink></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblTransferId_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Transfer ID:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblTransferId_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td noWrap width="1%"><asp:label id="lblEmp_name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Employee:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblEmp_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label>&nbsp;<input id="btnHistory" style="FONT-WEIGHT: bold; BORDER-LEFT-COLOR: #990000; BORDER-BOTTOM-COLOR: #990000; COLOR: white; BORDER-TOP-COLOR: #990000; FONT-FAMILY: Arial; HEIGHT: 20px; BACKGROUND-COLOR: #440000; BORDER-RIGHT-COLOR: #990000" tabIndex="1" type="button" value="H" name="btnHistory" runat="server"></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblStatus_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Status:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblStatus_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td noWrap width="1%"><asp:label id="lblsensor" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Sensor Tags:</asp:label>&nbsp;&nbsp;</td>
								<td vAlign="center"><asp:label id="lblInk_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblSStore_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Sending Store:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblSStore_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td noWrap width="1%"><asp:label id="lblRStore_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Receiving Store:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblRStore_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
							</tr>
							<tr>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap colSpan="4"></td>
							</tr>
						</table>
						<hr width="100%" color="#990000">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td noWrap><asp:label id="Label1" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Ship Date:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblShipDate_Value_New" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td noWrap><asp:label id="Label3" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Tracking #:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:hyperlink id="hlUPS_New" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Visible="True" Target="_blank" NavigateUrl="">
										<asp:Label runat="server" ForeColor="White" ID="Label4" Font-Size="X-Small" Font-Names="Arial"></asp:Label>
									</asp:hyperlink></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="Label5" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Transfer ID:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblTransferId_Value_New" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td noWrap width="1%"><asp:label id="Label7" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Employee:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblEmp_Value_New" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="Label9" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Status:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblStatus_Value_New" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td noWrap width="1%"><asp:label id="Label11" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Sensor Tags:</asp:label>&nbsp;&nbsp;</td>
								<td vAlign="center"><asp:label id="lblInk_Value_New" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="Label13" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Sending Store:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblSStore_Value_New" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td noWrap width="1%"><asp:label id="Label15" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Receiving Store:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblRStore_Value_New" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
							</tr>
							<tr>
								<td noWrap></td>
							</tr>
							<tr>
								<td colSpan="4"></td>
							</tr>
						</table>
						<hr width="100%" color="#990000">
						<br>
						<table cellSpacing="0" cellPadding="3" width="100%" border="0">
							<tr>
								<td vAlign="top" align="middle">
									<table borderColor="#990000" cellSpacing="0" cellPadding="2" width="100%" border="1">
										<tr>
											<td bgColor="#990000" colSpan="2"><font face="arial" color="#ffffff" size="small">Correct 
													Transfer</font></td>
										</tr>
										<tr>
											<td style="WIDTH: 322px" align="middle" width="322">
												<table cellSpacing="2" cellPadding="0" border="0">
													<tr>
														<td noWrap><font face="arial" color="white" size="2">Error Type:</font>&nbsp;</td>
														<td><asp:dropdownlist id="ddlError" runat="server" AutoPostBack="True">
																<asp:ListItem Value="0">Select an error type</asp:ListItem>
																<asp:ListItem Value="1">Employee Error</asp:ListItem>
																<asp:ListItem Value="2">Carrier Delivery Error</asp:ListItem>
															</asp:dropdownlist></td>
													</tr>
													<tr>
														<td noWrap><font face="arial" color="white" size="2">Action:</font>&nbsp;</td>
														<td><asp:dropdownlist id="ddlAction" runat="server" Width="100%" AutoPostBack="True" Enabled="False">
																<asp:ListItem Value="0">Select an action</asp:ListItem>
															</asp:dropdownlist></td>
													</tr>
													<tr>
														<td><asp:label id="lblRStore" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Receiving Store:</asp:label></td>
														<td><asp:dropdownlist id="ddlRStore" runat="server" Width="100%" Enabled="False" DataValueField="StoreNum" DataTextField="StoreNum"></asp:dropdownlist></td>
													</tr>
													<tr>
														<td><asp:label id="lblSStore" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial">Sending Store:</asp:label></td>
														<td><asp:dropdownlist id="ddlSStore" runat="server" Width="100%" Enabled="False" DataValueField="StoreNum" DataTextField="StoreNum"></asp:dropdownlist></td>
													</tr>
												</table>
											</td>
											<td vAlign="center" align="middle"><asp:label id="lblCopyStatus" runat="server" ForeColor="White" Font-Names="Arial"></asp:label></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<br>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td align="middle"><asp:button id="btnSubmit" runat="server" ForeColor="White" Font-Names="Arial" BorderColor="#990000" Font-Bold="True" Text="Copy Transfer" BackColor="#440000"></asp:button>&nbsp;
									<asp:button id="btnReturn" runat="server" ForeColor="White" Font-Names="Arial" BorderColor="#990000" Font-Bold="True" Text="Return" BackColor="#440000"></asp:button>&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
