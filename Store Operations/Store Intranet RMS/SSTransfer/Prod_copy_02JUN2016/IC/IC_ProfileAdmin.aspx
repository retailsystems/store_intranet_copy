<%@ Register TagPrefix="uc1" TagName="Footer" Src="../Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Layout/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_ProfileAdmin.aspx.vb" Inherits="SSTransfer.IC_ProfileAdmin"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Profile Administration</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="../Javascript/DisableClientBack.js"></script>
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="yes" rightMargin="0">
		<form id="frmproadmin" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td noWrap height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr vAlign="top">
					<td noWrap>
						<asp:label id="lblAdminCode" Runat="server" Font-Size="xsmall" ForeColor="White" Font-Name="arial" Font-Bold="True">Admin Code:</asp:label>&nbsp;<asp:textbox id="txtAdminCode" Runat="server" TextMode="Password" MaxLength="5"></asp:textbox>&nbsp;<asp:button id="btnAdminCode" BackColor="#440000" Runat="server" ForeColor="white" Font-Name="arial" Font-Bold="True" Text="Submit" BorderColor="#990000"></asp:button></td>
				</tr>
				<tr>
					<td noWrap><asp:label id="lblError" Width="100%" Runat="server" Font-Size="xsmall" ForeColor="white" Font-Name="arial" Font-Bold="True"></asp:label></td>
				</tr>
				<tr vAlign="top">
					<td vAlign="top" noWrap>
						<div style="OVERFLOW-Y: auto; SCROLLBAR-HIGHLIGHT-COLOR: #990000; WIDTH: 100%; SCROLLBAR-ARROW-COLOR: #ffffff; SCROLLBAR-BASE-COLOR: #440000; HEIGHT: 310px"><asp:datagrid id="dgProfiles" GridLines="None" BorderStyle="None" CellPadding="2" CellSpacing="1" BackColor="#111111" Width="100%" AutoGenerateColumns="False" Runat="server" Font-Size="X-Small" Font-Names="Arial">
								<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
								<ItemStyle Font-Size="X-Small" Font-Names="Arial" ForeColor="White"></ItemStyle>
								<HeaderStyle Font-Size="X-Small" Font-Names="arial" Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
								<Columns>
									<asp:BoundColumn HeaderText="Profile Name" DataField="Profile_Name"></asp:BoundColumn>
									<asp:BoundColumn HeaderText="Profile Description" DataField="Profile_Desc"></asp:BoundColumn>
									<asp:BoundColumn HeaderText="Created By" DataField="Created_By"></asp:BoundColumn>
									<asp:BoundColumn HeaderText="Created Date" DataField="Created_Date"></asp:BoundColumn>
									<asp:BoundColumn HeaderText="Modified Date" DataField="Modified_Date"></asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Delete">
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" Width="5%" BackColor="#440000"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<a target=_self href='IC_ProfileAdmin.aspx?PID=<%# container.dataitem("Profile_Id") %>'>
												<asp:Image Runat="server" ID="imgDelete" ImageUrl='..\Images\skull2.gif'></asp:Image></a>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></div>
					</td>
				</tr>
				<tr>
					<td noWrap height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
		<script language="javascript">
		document.forms[0].txtAdminCode.focus();	
		</script>
	</body>
</HTML>
