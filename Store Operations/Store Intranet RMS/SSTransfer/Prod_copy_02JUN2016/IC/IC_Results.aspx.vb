Public Class IC_Results
    Inherits System.Web.UI.Page
    Protected WithEvents ucHeader As Header
    Protected WithEvents lblView As System.Web.UI.WebControls.Label
    Protected WithEvents ddlView As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dgTTResults As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgTest As System.Web.UI.WebControls.DataGrid
    Private objUserInfo As New UserInfo()
    Public SQL As String
    Public strSearch As String
    Public strBox As String
    'Public intVoid
    Public BOXID As String
    Protected WithEvents lblResults As System.Web.UI.WebControls.Label
    Protected WithEvents ddlOptions As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnExecute As System.Web.UI.WebControls.Button
    Dim strUser As String
    '******************************************************************
    'These will be the variables passed to results pages to form querys.
    Public FStore As String       'From Store
    Public FDis As String         'From District
    Public FReg As String         'From Region
    Public NoFInternet As String  'From Internet Store Checkbox
    Public TStore As String       'To Store
    Public TDis As String         'To District
    Public TReg As String         'To Region
    Public NoTInternet As String  'To Internet Store Checkbox
    Public GERS As String         'GERS Process Status
    Public Discrep As String      'Discrepancy status
    Public TType As String        'Transfer Type
    Public TStatus As String      'Transfer Status
    Public TDate As String        'To Date search
    Public FDate As String        'From Date Search
    Public DFQty As String        'Discrep from qty
    Public DTQty As String        'Discrep to qty
    Public DFAmt As String        'Discrep from amt
    Public DTAmt As String        'Discrep to amt
    Public TrackNum As String     'Tracking Number (UPS)
    Public TransNum As String     'Transfer Number
    '*********************************************************************

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()
        If Not objUserInfo.EmpOnline Then
            Response.Redirect("../SessionEnd.aspx?Mode=2")
        End If
        'Put user code to initialize the page here
        ucHeader.lblTitle = "Inventory Control Search Results"
        ucHeader.CurrentPage(Header.PageName.IC_Search)
        ucHeader.CurrentMode(Header.HeaderGroup.IC)

        'strSearch = Request.Params("SQL2")
        FStore = Request.Params("FS")
        FDis = Request.Params("FDIS")
        FReg = Request.Params("FREG")
        TStore = Request.Params("TS")
        TDis = Request.Params("TDIS")
        TReg = Request.Params("TREG")
        NoFInternet = Request.Params("NFI")
        NoTInternet = Request.Params("NTI")
        GERS = Request.Params("G")
        Discrep = Request.Params("DS")     'Discrep Statuses
        TType = Request.Params("TT")       'Transfer Types
        TStatus = Request.Params("TranS")  'Transfer Statuses
        FDate = Request.Params("FD")
        TDate = Request.Params("TD")
        DFQty = Request.Params("DFQ")
        DTQty = Request.Params("DTQ")
        DFAmt = Request.Params("DFA")
        DTAmt = Request.Params("DTA")
        TrackNum = Request.Params("TKN")   'Tracking number
        TransNum = Request.Params("TSN")   'transfer number
        If Not IsPostBack Then
            Call AssignstrSearch()
            Call ExecSearch()
        End If
    End Sub
    Public Function GetSearchString() As String
        Return Server.UrlEncode(strSearch)
    End Function
    Private Sub AssignstrSearch()
        'This creates the WHERE portion of the query passed on the variable passed.

        If (FStore = "ALL") Then
            'Nothing selected so search ALL STORES
            strSearch = strSearch & "AND ((Box_Xfer_Hdr.Sending_Store_Cd >= 5001 AND Box_Xfer_Hdr.Sending_Store_Cd <= 5900) OR (Box_Xfer_Hdr.Sending_Store_Cd = 5990)) "
        ElseIf (FStore <> "" And FStore <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Sending_Store_Cd=" & FStore & " "
        ElseIf (FDis <> "" And FDis <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Sending_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE District = '" & FDis & "') "
        ElseIf (FReg <> "" And FReg <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Sending_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE Region = " & FReg & ") "
        End If

        If (NoFInternet <> "unchecked") Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Sending_Store_Cd <> 5990 "
        End If

        If (TStore = "ALL") Then
            'Nothing selected so search ALL STORES
            strSearch = strSearch & "AND ((Box_Xfer_Hdr.Receiving_Store_Cd >= 5001 AND Box_Xfer_Hdr.Receiving_Store_Cd <= 5900) OR Box_Xfer_Hdr.Receiving_Store_Cd = 9999 OR Box_Xfer_Hdr.Receiving_store_Cd=5990) "
        ElseIf (TStore <> "" And TStore <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Receiving_Store_Cd=" & TStore & " "
        ElseIf (TDis <> "" And TDis <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Receiving_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE District = " & TDis & ") "
        ElseIf (TReg <> "" And TReg <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Receiving_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE Region = " & TReg & ") "
        End If

        If (NoTInternet <> "unchecked") Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Receiving_Store_Cd <> 5990 "
        End If

        If (GERS <> "" And GERS <> Nothing) Then
            'strSearch = strSearch & "AND (IC_Box_Trans_Status.GERSStatus_Cd=" & (GERS) & ") "
            strSearch = strSearch & "AND (IC_Box_Trans_Status.GERSStatus_Cd IN (" & (GERS) & ")) "
        End If

        If (Discrep <> "" And Discrep <> Nothing) Then
            strSearch = strSearch & "AND (IC_Box_Trans_Status.Disc_Cd IN (" & (Discrep) & ")) "
        End If

        If (TType <> "" And TType <> Nothing) Then
            strSearch = strSearch & "AND (IC_Box_Trans_Status.Xfer_Type_Cd IN (" & (TType) & ")) "
        End If

        If (TStatus <> "" And TStatus <> Nothing) Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Current_Box_Status_Cd IN (" & (TStatus) & ")) "
        End If

        If (DFQty <> "" And DFQty <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Item.Discrepancy >= " & DFQty & " "
        End If

        If (DTQty <> "" And DTQty <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Item.Discrepancy <= " & DTQty & " "
        End If

        If (DFAmt <> "" And DFAmt <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Item.Discrep_Amt >= " & DFAmt & " "
        End If

        If (DTAmt <> "" And DTAmt <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Item.Discrep_Amt <= " & DTAmt & " "
        End If

        If (FDate <> "" And FDate <> Nothing And TDate <> "" And TDate <> Nothing) Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & TDate & "' ) "
        End If

        If (TrackNum <> "" And TrackNum <> Nothing) Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Tracking_Num=('" & (TrackNum) & "')) "
        End If

        If (TransNum <> "" And TransNum <> Nothing) Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Box_Id=('" & (TransNum) & "')) "
        End If
    End Sub
    Private Sub ExecSearch()
        'execute search query
        SQL = "SELECT Distinct Box_Xfer_Hdr.Shipment_Date AS Shipment_Date, Box_Xfer_Hdr.Current_Box_Status_Cd as Current_Box_Status_Cd, Box_Status_Type.Status_Desc as Status_Desc, IC_Box_Trans_Status.Box_Id as Box_Id, "
        SQL = SQL & "Box_Xfer_Hdr.Sending_Store_Cd as Sending_Store, Box_Xfer_Hdr.Receiving_Store_Cd as Receiving_Store "
        SQL = SQL & "FROM Box_Xfer_Hdr LefT OUTER JOIN "
        SQL = SQL & "Box_Xfer_Item ON Box_Xfer_Hdr.Box_Id = Box_Xfer_Item.Box_Id LEFT OUTER JOIN "
        SQL = SQL & "IC_Box_Trans_Status ON Box_Xfer_Hdr.Box_Id = IC_Box_Trans_Status.Box_Id "
        SQL = SQL & "Left Outer Join Box_Status_Type ON Box_xfer_Hdr.Current_Box_Status_Cd=Box_Status_Type.Status_Cd "
        SQL = SQL & "Left Outer Join GERSSTatus_Type ON IC_Box_Trans_Status.GERSStatus_Cd=GERSStatus_Type.GERSStatus_Cd  "
        SQL = SQL & "Left Outer Join Discrep_Type ON IC_Box_Trans_Status.Disc_Cd=Discrep_Type.Disc_Cd "
        SQL = SQL & "Left OUter JOin Box_To_Include ON IC_Box_Trans_Status.Box_Id=Box_To_Include.Box_Id "
        SQL = SQL & "WHere Box_To_Include.Box_Includes_Cd='6' "
        If Len(strSearch) > 0 Then
            'SQL = SQL & " WHERE" & Mid(strSearch, 4, Len(strSearch) - 3)
            SQL = SQL & strSearch
        End If

        SQL = SQL & "Order By Box_Id DESC"

        Call CountResults()     
    End Sub
    Public Sub CountResults()
        'Count number of results
        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()

        Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
        dtrSearch = cmdSearch.ExecuteReader
        Dim count As Integer
        count = 0
        Do While dtrSearch.Read
            count = count + 1
        Loop
        ViewState("numDGResults") = count
        If count > 0 Then
            Call FillDataGrid()
        Else
            lblResults.Text = "No Results Found"
        End If
        dtrSearch.Close()
        conSearch.Close()
        cmdSearch.Dispose()
    End Sub
    Public Sub Sort(ByVal s As Object, ByVal e As DataGridSortCommandEventArgs)
        Call AssignstrSearch()
        SQL = "SELECT Distinct Box_Xfer_Hdr.Shipment_Date AS Shipment_Date, Box_Xfer_Hdr.Current_Box_Status_Cd as Current_Box_Status_Cd, Box_Status_Type.Status_Desc as Status_Desc, IC_Box_Trans_Status.Box_Id as Box_Id, "
        SQL = SQL & "Box_Xfer_Hdr.Sending_Store_Cd as Sending_Store, Box_Xfer_Hdr.Receiving_Store_Cd as Receiving_Store "
        SQL = SQL & "FROM Box_Xfer_Hdr LefT OUTER JOIN "
        SQL = SQL & "Box_Xfer_Item ON Box_Xfer_Hdr.Box_Id = Box_Xfer_Item.Box_Id LEFT OUTER JOIN "
        SQL = SQL & "IC_Box_Trans_Status ON Box_Xfer_Hdr.Box_Id = IC_Box_Trans_Status.Box_Id "
        SQL = SQL & "Left Outer Join Box_Status_Type ON Box_xfer_Hdr.Current_Box_Status_Cd=Box_Status_Type.Status_Cd "
        SQL = SQL & "Left Outer Join GERSSTatus_Type ON IC_Box_Trans_Status.GERSStatus_Cd=GERSStatus_Type.GERSStatus_Cd  "
        SQL = SQL & "Left Outer Join Discrep_Type ON IC_Box_Trans_Status.Disc_Cd=Discrep_Type.Disc_Cd "
        SQL = SQL & "Left OUter JOin Box_To_Include ON IC_Box_Trans_Status.Box_Id=Box_To_Include.Box_Id "
        SQL = SQL & "WHere Box_To_Include.Box_Includes_Cd='6' "
        If Len(strSearch) > 0 Then
            'SQL = SQL & " WHERE" & Mid(strSearch, 4, Len(strSearch) - 3)
            SQL = SQL & strSearch
        End If
        SQL = SQL & " Order By " & e.SortExpression & " DESC"
        FillDataGrid()
    End Sub
    Private Sub FillDataGrid()
        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()
        Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
        dtrSearch = cmdSearch.ExecuteReader
        dgTTResults.DataSource = dtrSearch
        dgTTResults.DataBind()
        dtrSearch.Close()
        conSearch.Close()
        cmdSearch.Dispose()
    End Sub
    Protected Function GetBoxTransferTypesSD(ByVal Box_Id As String) As String
        'Get Short Description
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim strReturn As String

        objConnection.Open()

        strStoredProcedure = "spGetBoxTransferTypes '" & Box_Id & "' "

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader()

        Do While objDataReader.Read
            strReturn = strReturn & objDataReader("Xfer_Short_Desc") & ","
        Loop
        ViewState("BOXID") = Box_Id
        objConnection.Close()
        objDataReader.Close()
        objCommand.Dispose()
        If Len(strReturn) > 0 Then
            strReturn = Mid(strReturn, 1, Len(strReturn) - 1)
        Else
            strReturn = ""
        End If

        Return strReturn

    End Function
    Private Sub ddlView_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlView.SelectedIndexChanged
        'User has changed view type
        If ddlView.SelectedItem.Value = "TransferType" Then
            'Show Columns: Transfer#,Trans Type, date, box status, gers status, descrep status, comment
            'Order by transfer type
            'redirect to IC_ResultsTT.aspx
            Dim intCount As Integer
            intCount = ViewState("numDGResults")

            If intCount > 1 Then
                'strSearch = Request.Params("SQL2")
                'Response.Redirect("IC_ResultsTT.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSearch))
                Response.Redirect("IC_ResultsTT.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum)
            ElseIf intCount = 1 Then
                BOXID = ViewState("BOXID")
                'strSearch = Request.Params("SQL2")
                'Response.Redirect("IC_ResultsTT.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSearch) & "&GERSBID=" & BOXID)
                Response.Redirect("IC_ResultsTT.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & BOXID)
            End If

        ElseIf ddlView.SelectedItem.Value = "Transfer" Then
            'Show Columns: Transfer#,Trans Type, date, box status, gers status, descrep status, gers print out, void button
            'Order by Transfer Number

        End If
    End Sub
    'Private Sub btnPResults_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    'Go to IC_PrintSearchResults.aspx  (user wants to print search results)
    'strSearch = Request.Params("SQL2")
    'Dim strPage As String
    'strPage = "IC_Results.aspx"
    'Response.Redirect("IC_PrintSearchResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSearch) & "&RPage=" & strPage)
    'End Sub
    Public Function GetRTNEditTransfer()
        'This function gets the return string for when the user goes to the IC_EditTransfer page.  When they hit
        'return on that page it will return to this page.
        Dim strRTN
        'strRTN = "IC_Results.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSearch)
        'strRTN = "IC_ResultsTT.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum
        strRTN = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum
        Return strRTN
    End Function
    Private Sub btnExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExecute.Click
        If ddlOptions.SelectedItem.Value = "Print Search Results" Then
            'Go to IC_PrintSearchResults.aspx  (user wants to print search results)
            'strSearch = Request.Params("SQL2")
            'Dim strPage As String
            'strPage = "IC_Results.aspx"
            'Response.Redirect("IC_PrintSearchResults.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSearch) & "&RPage=" & strPage)
            Response.Redirect("IC_PrintSearchResults.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum)

            'ElseIf ddlOptions.SelectedItem.Value = "Check-In DC" Then
            '    'mark RTVs, Defectives, CTBs as Recorded (box status), Processed (in GERS), 
            '    'set discrep status to ND,discrep amt to zero, and update qty received only when box status is shipped.
            '    Dim i As Integer
            '    Dim dgi As DataGridItem
            '    Dim chkExcept As CheckBox

            '    For i = 0 To dgTTResults.Items.Count - 1
            '        dgi = dgTTResults.Items(i)
            '        chkExcept = CType(dgi.FindControl("chkEItem"), CheckBox)
            '        If chkExcept.Checked Then
            '            'Do Nothing
            '        Else
            '            'If box status is shipped, and transfer type is (RTV or CTB or Defective) then update info.
            '            Dim PBox_Id As String
            '            Dim PItemTransType As DataGridItem
            '            Dim PToStore As String
            '            Dim PTransType As String  'Transfer Type
            '            Dim PBoxStatus As String  'Box Current Status
            '            Dim PGersStatus As String 'Gers Status
            '            Dim intGERSCd As Int32
            '            Dim intXferCd As Int32
            '            Dim parmXferCd As SqlClient.SqlParameter
            '            PBox_Id = CType(dgi.FindControl("lblTN"), Label).Text  'Gets Box Id

            '            PItemTransType = dgTTResults.Items(i)
            '            PToStore = PItemTransType.Cells(2).Text
            '            PTransType = PItemTransType.Cells(3).Text   'Gets Transfer Type
            '            PBoxStatus = PItemTransType.Cells(5).Text   'Gets Box Current Status

            '            'Check GERS Status CD
            '            Dim conFindGERS As System.Data.SqlClient.SqlConnection
            '            Dim cmdFindGERS As System.Data.SqlClient.SqlCommand
            '            conFindGERS = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            '            cmdFindGERS = New SqlClient.SqlCommand("spRetrieveGERSStatus", conFindGERS)
            '            cmdFindGERS.CommandType = CommandType.StoredProcedure
            '            cmdFindGERS.Parameters.Add("@BID", PBox_Id)
            '            parmXferCd = cmdFindGERS.Parameters.Add("@GERSCd", SqlDbType.TinyInt)
            '            parmXferCd.Size = 1
            '            parmXferCd.Direction = ParameterDirection.Output
            '            conFindGERS.Open()
            '            cmdFindGERS.ExecuteNonQuery()
            '            intGERSCd = cmdFindGERS.Parameters("@GERSCd").Value
            '            conFindGERS.Close()
            '            If intGERSCd = 1 Then
            '                PGersStatus = "Open"
            '            End If

            '            If PBoxStatus = "Shipped" And PGersStatus = "Open" Then
            '                'Check Transfer Type CD
            '                'Dim conFindTTCD As System.Data.SqlClient.SqlConnection
            '                'Dim cmdFindTTCD As System.Data.SqlClient.SqlCommand
            '                'conFindTTCD = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            '                'cmdFindTTCD = New SqlClient.SqlCommand("spRetrieveTransType", conFindTTCD)
            '                'cmdFindTTCD.CommandType = CommandType.StoredProcedure
            '                'cmdFindTTCD.Parameters.Add("@TType", PTransType)
            '                'parmXferCd = cmdFindTTCD.Parameters.Add("@XferCd", SqlDbType.TinyInt)
            '                'parmXferCd.Size = 1
            '                'parmXferCd.Direction = ParameterDirection.Output
            '                'conFindTTCD.Open()
            '                'cmdFindTTCD.ExecuteNonQuery()
            '                'intXferCd = cmdFindTTCD.Parameters("@XferCd").Value
            '                'conFindTTCD.Close()

            '                'If intXferCd = 1 Or intXferCd = 2 Or intXferCd = 5 Then
            '                If PToStore = "9999" Then
            '                    'To update info, transfer type must be RTV, Defective, or CTB
            '                    Dim intBoxStatus As Int32
            '                    Dim intGersStatus As Int32
            '                    Dim intDiscrepStatus As Int32
            '                    Dim strEmpName As String
            '                    Dim strEmpNum As String
            '                    Dim ModifiedDate As Date
            '                    Dim strSQL As String
            '                    intBoxStatus = 5  'Set to Recorded
            '                    intGersStatus = 3  'Set to Processed(Recorded)
            '                    intDiscrepStatus = 1 'No Discrepancy
            '                    strEmpName = objUserInfo.EmpFullName
            '                    strEmpNum = objUserInfo.EmpId
            '                    ModifiedDate = Now

            '                    'Update Box_Xfer_Hdr
            '                    Dim conUpdateHdr As System.Data.SqlClient.SqlConnection
            '                    Dim cmdUpdateHdr As System.Data.SqlClient.SqlCommand
            '                    conUpdateHdr = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            '                    cmdUpdateHdr = New SqlClient.SqlCommand("spDCUpdateBoxHeader", conUpdateHdr)
            '                    cmdUpdateHdr.CommandType = CommandType.StoredProcedure
            '                    cmdUpdateHdr.Parameters.Add("@Box_Id", PBox_Id)
            '                    cmdUpdateHdr.Parameters.Add("@Current_Box_Status", intBoxStatus)
            '                    cmdUpdateHdr.Parameters.Add("@Received_Date", ModifiedDate)
            '                    cmdUpdateHdr.Parameters.Add("@Modified_Date", ModifiedDate)
            '                    conUpdateHdr.Open()
            '                    cmdUpdateHdr.ExecuteNonQuery()
            '                    conUpdateHdr.Close()

            '                    'Update IC_Box_trans_Status Table
            '                    Dim conUpdateICBoxTransStatus As System.Data.SqlClient.SqlConnection
            '                    Dim cmdUpdateICBoxTransStatus As System.Data.SqlClient.SqlCommand
            '                    conUpdateICBoxTransStatus = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            '                    cmdUpdateICBoxTransStatus = New SqlClient.SqlCommand("spDCUpdateICTrans", conUpdateICBoxTransStatus)
            '                    cmdUpdateICBoxTransStatus.CommandType = CommandType.StoredProcedure
            '                    cmdUpdateICBoxTransStatus.Parameters.Add("@Box_Id", PBox_Id)
            '                    cmdUpdateICBoxTransStatus.Parameters.Add("@GersStatus", intGersStatus)
            '                    cmdUpdateICBoxTransStatus.Parameters.Add("@DiscrepStatus", intDiscrepStatus)
            '                    conUpdateICBoxTransStatus.Open()
            '                    cmdUpdateICBoxTransStatus.ExecuteNonQuery()
            '                    conUpdateICBoxTransStatus.Close()

            '                    'Update Box_Xfer_Items
            '                    strSQL = "Select Box_Id,Xfer_Type_Cd,Sku_Num,Item_Qty,Received_Qty,Discrepancy,Discrep_Amt From Box_Xfer_Item Where Box_Id='" & PBox_Id & "' "
            '                    Dim conUpdateItems As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            '                    Dim dtrUpdateItems As SqlClient.SqlDataReader
            '                    conUpdateItems.Open()
            '                    Dim cmdUpdateItems As New SqlClient.SqlCommand(strSQL, conUpdateItems)
            '                    dtrUpdateItems = cmdUpdateItems.ExecuteReader

            '                    'Set discrep values and received qty
            '                    Dim strSku As String
            '                    Dim intItemQty As Integer
            '                    Dim intRecQty As Integer
            '                    Dim intDiscrepQty As Integer
            '                    Dim DiscrepAmt As Integer
            '                    DiscrepAmt = 0 'Set discrepancy dollar amt to zero
            '                    intDiscrepQty = 0 'Set discrepancy qty to zero
            '                    Do While dtrUpdateItems.Read
            '                        intXferCd = dtrUpdateItems("Xfer_Type_Cd")
            '                        strSku = dtrUpdateItems("Sku_Num")
            '                        intItemQty = CInt(dtrUpdateItems("Item_Qty"))
            '                        intRecQty = CInt(dtrUpdateItems("Received_Qty"))

            '                        Dim conUpItems As System.Data.SqlClient.SqlConnection
            '                        Dim cmdUpItems As System.Data.SqlClient.SqlCommand
            '                        conUpItems = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            '                        cmdUpItems = New SqlClient.SqlCommand("spDCUpdateBoxItems", conUpItems)
            '                        cmdUpItems.CommandType = CommandType.StoredProcedure
            '                        cmdUpItems.Parameters.Add("@Box_Id", PBox_Id)
            '                        cmdUpItems.Parameters.Add("@Xfer_Cd", intXferCd)
            '                        cmdUpItems.Parameters.Add("@Sku", strSku)
            '                        cmdUpItems.Parameters.Add("@IQty", intItemQty)
            '                        cmdUpItems.Parameters.Add("@RQty", intItemQty)  'set received qty to the same as item qty.  No Discrepancy
            '                        cmdUpItems.Parameters.Add("@DiscrepQty", intDiscrepQty)
            '                        cmdUpItems.Parameters.Add("@DiscrepAmt", DiscrepAmt)
            '                        conUpItems.Open()
            '                        cmdUpItems.ExecuteNonQuery()
            '                        conUpItems.Close()

            '                    Loop  'end do while loop for updating discrepancies
            '                    dtrUpdateItems.Close()
            '                    conUpdateItems.Close()

            '                    'Update Box History Table
            '                    Dim conUpdateHistory As System.Data.SqlClient.SqlConnection
            '                    Dim cmdUpdateHistory As System.Data.SqlClient.SqlCommand
            '                    conUpdateHistory = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            '                    cmdUpdateHistory = New SqlClient.SqlCommand("spUpdateBoxHistory", conUpdateHistory)
            '                    cmdUpdateHistory.CommandType = CommandType.StoredProcedure
            '                    cmdUpdateHistory.Parameters.Add("@Box_Id", PBox_Id)
            '                    cmdUpdateHistory.Parameters.Add("@Status", intBoxStatus)
            '                    cmdUpdateHistory.Parameters.Add("@EmpId", strEmpNum)
            '                    cmdUpdateHistory.Parameters.Add("@EmpFullName", strEmpName)
            '                    cmdUpdateHistory.Parameters.Add("@UpdateTime", ModifiedDate)
            '                    conUpdateHistory.Open()
            '                    cmdUpdateHistory.ExecuteNonQuery()
            '                    conUpdateHistory.Close()

            '                End If 'end if for checking transfer type
            '            End If  'end if for checking box status is shipped
            '        End If 'end if for checked box

            '    Next 'end of for loop
        End If
    End Sub
    Private Sub dgTTResults_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgTTResults.SelectedIndexChanged

    End Sub
End Class
