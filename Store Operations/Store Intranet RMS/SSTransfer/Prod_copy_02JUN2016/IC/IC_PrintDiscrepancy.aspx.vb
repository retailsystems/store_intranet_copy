Public Class IC_PrintDiscrepancy
    Inherits System.Web.UI.Page
    Protected WithEvents dgSearchResults As System.Web.UI.WebControls.DataGrid
    Public SQL As String
    Public strSearch As String
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Public GBID As String
    Private objUserInfo As New UserInfo()
    'Public strRPage As String 'The page to return to
    '*****************************************************************
    'These will be the variables passed to results pages to form querys.
    Public FStore As String       'From Store
    Public FDis As String         'From District
    Public FReg As String         'From Region
    Public NoFInternet As String  'From Internet Store Checkbox
    Public TStore As String       'To Store
    Public TDis As String         'To District
    Public TReg As String         'To Region
    Public NoTInternet As String  'To Internet Store Checkbox
    Public GERS As String         'GERS Process Status
    Public Discrep As String      'Discrepancy status
    Public TType As String        'Transfer Type
    Public TStatus As String      'Transfer Status
    Public TDate As String        'To Date search
    Public FDate As String        'From Date Search
    Public DFQty As String        'Discrep from qty
    Public DTQty As String        'Discrep to qty
    Public DFAmt As String        'Discrep from amt
    Public DTAmt As String        'Discrep to amt
    Public TrackNum As String     'Tracking Number (UPS)
    Public TransNum As String     'Transfer Number
    '*****************************************************************

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        objUserInfo.GetEmployeeInfo()
        If Not objUserInfo.EmpOnline Then
            Response.Redirect("../SessionEnd.aspx?Mode=2")
        End If
        'strRPage = Request.Params("RPage")
        GBID = Request.Params("GERSBID")
        FStore = Request.Params("FS")
        FDis = Request.Params("FDIS")
        FReg = Request.Params("FREG")
        TStore = Request.Params("TS")
        TDis = Request.Params("TDIS")
        TReg = Request.Params("TREG")
        NoFInternet = Request.Params("NFI")
        NoTInternet = Request.Params("NTI")
        GERS = Request.Params("G")
        Discrep = Request.Params("DS")     'Discrep Statuses
        TType = Request.Params("TT")       'Transfer Types
        TStatus = Request.Params("TranS")  'Transfer Statuses
        FDate = Request.Params("FD")
        TDate = Request.Params("TD")
        DFQty = Request.Params("DFQ")
        DTQty = Request.Params("DTQ")
        DFAmt = Request.Params("DFA")
        DTAmt = Request.Params("DTA")
        TrackNum = Request.Params("TKN")   'Tracking number
        TransNum = Request.Params("TSN")   'transfer number

        'strSearch = Request.Params("SQL2")
        If Not IsPostBack Then
            If GBID = Nothing Then
                Call AssignstrSearch()
                Call ExecSearch()
            Else
                'Gets results from particular transfer # in order to edit GERS status
                SQL = "SELECT Distinct Box_Xfer_Hdr.Shipment_Date AS Shipment_Date, Box_Xfer_Hdr.Current_Box_Status_Cd as Current_Box_Status_Cd, Box_Status_Type.Status_Desc as Status_Desc,IC_Box_Trans_Status.Xfer_Type_Cd as Xfer_Type_Cd, IC_Box_Trans_Status.Box_Id as Box_Id, "
                SQL = SQL & "Xfer_Type.Xfer_Desc as Xfer_Desc, IC_Box_Trans_Status.GERSStatus_Cd, GERSStatus_Type.GERSStatus_Desc as GERSStatus_Desc,IC_Box_Trans_Status.Disc_Cd, Discrep_Type.Disc_Desc as Disc_Desc, Box_Xfer_Hdr.Sending_Store_Cd as Sending_Store, Box_Xfer_Hdr.Receiving_Store_Cd as Receiving_Store, "
                SQL = SQL & "Box_Xfer_Item.SKu_Num as SKU_NUM, Box_Xfer_Item.Item_Qty as Shipped_Qty, Box_Xfer_Item.Received_Qty, Box_xfer_Item.Discrepancy, (CONVERT (varchar(10),Box_Xfer_Item.Discrep_Amt)) as Discrep_Amt "
                SQL = SQL & "FROM Box_Xfer_Hdr LefT OUTER JOIN "
                SQL = SQL & "Box_Xfer_Item ON Box_Xfer_Hdr.Box_Id = Box_Xfer_Item.Box_Id LEFT OUTER JOIN "
                SQL = SQL & "IC_Box_Trans_Status ON Box_Xfer_Hdr.Box_Id = IC_Box_Trans_Status.Box_Id "
                SQL = SQL & "Left Outer Join Box_Status_Type ON Box_xfer_Hdr.Current_Box_Status_Cd=Box_Status_Type.Status_Cd "
                SQL = SQL & "Left Outer Join Xfer_Type ON IC_Box_Trans_Status.Xfer_Type_Cd=Xfer_Type.Xfer_Cd "
                SQL = SQL & "Left Outer Join GERSSTatus_Type ON IC_Box_Trans_Status.GERSStatus_Cd=GERSStatus_Type.GERSStatus_Cd  "
                SQL = SQL & "Left Outer Join Discrep_Type ON IC_Box_Trans_Status.Disc_Cd=Discrep_Type.Disc_Cd "
                SQL = SQL & "Left OUter JOin Box_To_Include ON IC_Box_Trans_Status.Box_Id=Box_To_Include.Box_Id "
                SQL = SQL & "WHERE Box_To_Include.Box_Includes_Cd='6' AND Box_Xfer_Hdr.Box_Id='" & GBID & "' "

                Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                Dim dtrSearch As SqlClient.SqlDataReader
                conSearch.Open()

                Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
                dtrSearch = cmdSearch.ExecuteReader

                dgSearchResults.DataSource = dtrSearch
                dgSearchResults.DataBind()
                dtrSearch.Close()
                conSearch.Close()
                cmdSearch.Dispose()
            End If
        End If
    End Sub
    Private Sub AssignstrSearch()
        'This creates the WHERE portion of the query passed on the variable passed.

        If (FStore = "ALL") Then
            'Nothing selected so search ALL STORES
            strSearch = strSearch & "AND ((Box_Xfer_Hdr.Sending_Store_Cd >= 5001 AND Box_Xfer_Hdr.Sending_Store_Cd <= 5900) OR (Box_Xfer_Hdr.Sending_Store_Cd = 5990)) "
        ElseIf (FStore <> "" And FStore <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Sending_Store_Cd=" & FStore & " "
        ElseIf (FDis <> "" And FDis <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Sending_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE District = '" & FDis & "') "
        ElseIf (FReg <> "" And FReg <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Sending_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE Region = " & FReg & ") "
        End If

        If (NoFInternet <> "unchecked") Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Sending_Store_Cd <> 5990 "
        End If

        If (TStore = "ALL") Then
            'Nothing selected so search ALL STORES
            strSearch = strSearch & "AND ((Box_Xfer_Hdr.Receiving_Store_Cd >= 5001 AND Box_Xfer_Hdr.Receiving_Store_Cd <= 5900) OR Box_Xfer_Hdr.Receiving_Store_Cd = 9999 OR Box_Xfer_Hdr.Receiving_Store_Cd = 5990) "
        ElseIf (TStore <> "" And TStore <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Receiving_Store_Cd=" & TStore & " "
        ElseIf (TDis <> "" And TDis <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Receiving_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE District = " & TDis & ") "
        ElseIf (TReg <> "" And TReg <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Receiving_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE Region = " & TReg & ") "
        End If

        If (NoTInternet <> "unchecked") Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Receiving_Store_Cd <> 5990 "
        End If

        If (GERS <> "" And GERS <> Nothing) Then
            'strSearch = strSearch & "AND (IC_Box_Trans_Status.GERSStatus_Cd=" & (GERS) & ") "
            strSearch = strSearch & "AND (IC_Box_Trans_Status.GERSStatus_Cd IN (" & (GERS) & ")) "
        End If

        If (Discrep <> "" And Discrep <> Nothing) Then
            strSearch = strSearch & "AND (IC_Box_Trans_Status.Disc_Cd IN (" & (Discrep) & ")) "
        End If

        If (TType <> "" And TType <> Nothing) Then
            strSearch = strSearch & "AND (IC_Box_Trans_Status.Xfer_Type_Cd IN (" & (TType) & ")) "
        End If

        If (TStatus <> "" And TStatus <> Nothing) Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Current_Box_Status_Cd IN (" & (TStatus) & ")) "
        End If

        If (DFQty <> "" And DFQty <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Item.Discrepancy >= " & DFQty & " "
        End If

        If (DTQty <> "" And DTQty <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Item.Discrepancy <= " & DTQty & " "
        End If

        If (DFAmt <> "" And DFAmt <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Item.Discrep_Amt >= " & DFAmt & " "
        End If

        If (DTAmt <> "" And DTAmt <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Item.Discrep_Amt <= " & DTAmt & " "
        End If

        If (FDate <> "" And FDate <> Nothing And TDate <> "" And TDate <> Nothing) Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & TDate & "' ) "
        End If

        If (TrackNum <> "" And TrackNum <> Nothing) Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Tracking_Num=('" & (TrackNum) & "')) "
        End If

        If (TransNum <> "" And TransNum <> Nothing) Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Box_Id=('" & (TransNum) & "')) "
        End If
    End Sub
    Private Sub ExecSearch()
        'execute search query
        SQL = "SELECT Distinct Box_Xfer_Hdr.Shipment_Date AS Shipment_Date, Box_Xfer_Hdr.Current_Box_Status_Cd as Current_Box_Status_Cd, Box_Status_Type.Status_Desc as Status_Desc,IC_Box_Trans_Status.Xfer_Type_Cd as Xfer_Type_Cd, IC_Box_Trans_Status.Box_Id as Box_Id, "
        SQL = SQL & "Xfer_Type.Xfer_Desc as Xfer_Desc, IC_Box_Trans_Status.GERSStatus_Cd, GERSStatus_Type.GERSStatus_Desc as GERSStatus_Desc,IC_Box_Trans_Status.Disc_Cd, Discrep_Type.Disc_Desc as Disc_Desc, Box_Xfer_Hdr.Sending_Store_Cd as Sending_Store, Box_Xfer_Hdr.Receiving_Store_Cd as Receiving_Store, "
        SQL = SQL & "Box_Xfer_Item.SKu_Num as SKU_NUM, Box_Xfer_Item.Item_Qty as Shipped_Qty, Box_Xfer_Item.Received_Qty as Received_Qty, Box_xfer_Item.Discrepancy as Discrepancy, (CONVERT (varchar(10),Box_Xfer_Item.Discrep_Amt)) as Discrep_Amt "
        SQL = SQL & "FROM Box_Xfer_Hdr LefT OUTER JOIN "
        SQL = SQL & "Box_Xfer_Item ON Box_Xfer_Hdr.Box_Id = Box_Xfer_Item.Box_Id LEFT OUTER JOIN "
        SQL = SQL & "IC_Box_Trans_Status ON Box_Xfer_Hdr.Box_Id = IC_Box_Trans_Status.Box_Id "
        SQL = SQL & "Left Outer Join Box_Status_Type ON Box_xfer_Hdr.Current_Box_Status_Cd=Box_Status_Type.Status_Cd "
        SQL = SQL & "Left Outer Join Xfer_Type ON IC_Box_Trans_Status.Xfer_Type_Cd=Xfer_Type.Xfer_Cd "
        SQL = SQL & "Left Outer Join GERSSTatus_Type ON IC_Box_Trans_Status.GERSStatus_Cd=GERSStatus_Type.GERSStatus_Cd  "
        SQL = SQL & "Left Outer Join Discrep_Type ON IC_Box_Trans_Status.Disc_Cd=Discrep_Type.Disc_Cd "
        SQL = SQL & "Left OUter JOin Box_To_Include ON IC_Box_Trans_Status.Box_Id=Box_To_Include.Box_Id "
        SQL = SQL & "WHERE Box_To_Include.Box_Includes_Cd='6' "
        If Len(strSearch) > 0 Then
            'SQL = SQL & " WHERE" & Mid(strSearch, 4, Len(strSearch) - 3)
            SQL = SQL & strSearch
        End If
        SQL = SQL & "Order By Xfer_Type_Cd, Box_Id "
        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()

        Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
        dtrSearch = cmdSearch.ExecuteReader

        dgSearchResults.DataSource = dtrSearch
        dgSearchResults.DataBind()
        dtrSearch.Close()
        conSearch.Close()
        cmdSearch.Dispose()
    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        If GBID = Nothing Then
            'Dim strReturn As String
            'strReturn = strRPage & "?SQL2=" & strSearch
            'Response.Redirect(strReturn)
            Response.Redirect("IC_ResultsTT.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum)
        Else
            'Dim strReturn As String
            'strReturn = strRPage & "?GERSBID=" & GBID
            'Response.Redirect(strReturn)
            Response.Redirect("IC_ResultsTT.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID)
        End If
    End Sub
End Class
