Imports System.Text
Imports System.Net.Sockets

Public Class IC_ResultsTT
    Inherits System.Web.UI.Page
    Protected WithEvents ucHeader As Header
    Protected WithEvents lblView As System.Web.UI.WebControls.Label
    Protected WithEvents ddlView As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dgTTResults As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgTest As System.Web.UI.WebControls.DataGrid
    Private objUserInfo As New UserInfo()
    Public SQL As String
    Public strSearch As String
    Public strBox As String
    Public intVoid
    Public GBID As String   'retrieving TT for Gers Status Viewing purposes
    Protected WithEvents btnPGers As System.Web.UI.WebControls.Button
    Protected WithEvents btnPResults As System.Web.UI.WebControls.Button
    Protected WithEvents btnPDiscrep As System.Web.UI.WebControls.Button
    Protected WithEvents ddlOptions As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnExecute As System.Web.UI.WebControls.Button
    Protected WithEvents lblND As System.Web.UI.WebControls.Label
    Protected WithEvents lblNI As System.Web.UI.WebControls.Label
    Protected WithEvents lblIA As System.Web.UI.WebControls.Label
    Protected WithEvents lblIN As System.Web.UI.WebControls.Label
    Protected WithEvents lblNR As System.Web.UI.WebControls.Label
    Dim strUser As String
    '*****************************************************************
    'These will be the variables passed to results pages to form querys.
    Public FStore As String       'From Store
    Public FDis As String         'From District
    Public FReg As String         'From Region
    Public NoFInternet As String  'From Internet Store Checkbox
    Public TStore As String       'To Store
    Public TDis As String         'To District
    Public TReg As String         'To Region
    Public NoTInternet As String  'To Internet Store Checkbox
    Public GERS As String         'GERS Process Status
    Public Discrep As String      'Discrepancy status
    Public TType As String        'Transfer Type
    Public TStatus As String      'Transfer Status
    Public TDate As String        'To Date search
    Public FDate As String        'From Date Search
    Public DFQty As String        'Discrep from qty
    Public DTQty As String        'Discrep to qty
    Public DFAmt As String        'Discrep from amt
    Public DTAmt As String        'Discrep to amt
    Public TrackNum As String     'Tracking Number (UPS)
    Public TransNum As String     'Transfer Number
    '*****************************************************************

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()
        If Not objUserInfo.EmpOnline Then
            Response.Redirect("../SessionEnd.aspx?Mode=2")
        End If
        ucHeader.lblTitle = "Inventory Control Search Results"
        ucHeader.CurrentPage(Header.PageName.IC_Search)
        ucHeader.CurrentMode(Header.HeaderGroup.IC)

        'strSearch = Request.Params("SQL2")
        GBID = Request.Params("GERSBID")
        FStore = Request.Params("FS")
        FDis = Request.Params("FDIS")
        FReg = Request.Params("FREG")
        TStore = Request.Params("TS")
        TDis = Request.Params("TDIS")
        TReg = Request.Params("TREG")
        NoFInternet = Request.Params("NFI")
        NoTInternet = Request.Params("NTI")
        GERS = Request.Params("G")
        Discrep = Request.Params("DS")     'Discrep Statuses
        TType = Request.Params("TT")       'Transfer Types
        TStatus = Request.Params("TranS")  'Transfer Statuses
        FDate = Request.Params("FD")
        TDate = Request.Params("TD")
        DFQty = Request.Params("DFQ")
        DTQty = Request.Params("DTQ")
        DFAmt = Request.Params("DFA")
        DTAmt = Request.Params("DTA")
        TrackNum = Request.Params("TKN")   'Tracking number
        TransNum = Request.Params("TSN")   'transfer number

        If Not IsPostBack Then
            If GBID = Nothing Then
                Call AssignstrSearch()
                Call ExecSearch()
            Else
                'Get Gers Status's for this particular transfer Number
                Call GetGersResults()
            End If
        End If
    End Sub
    Private Sub AssignstrSearch()
        'This creates the WHERE portion of the query passed on the variable passed.

        If (FStore = "ALL") Then
            'Nothing selected so search ALL STORES
            strSearch = strSearch & "AND ((Box_Xfer_Hdr.Sending_Store_Cd >= 5001 AND Box_Xfer_Hdr.Sending_Store_Cd <= 5900) OR (Box_Xfer_Hdr.Sending_Store_Cd = 5990)) "
        ElseIf (FStore <> "" And FStore <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Sending_Store_Cd=" & FStore & " "
        ElseIf (FDis <> "" And FDis <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Sending_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE District = '" & FDis & "') "
        ElseIf (FReg <> "" And FReg <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Sending_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE Region = " & FReg & ") "
        End If

        If (NoFInternet <> "unchecked") Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Sending_Store_Cd <> 5990 "
        End If

        If (TStore = "ALL") Then
            'Nothing selected so search ALL STORES
            strSearch = strSearch & "AND ((Box_Xfer_Hdr.Receiving_Store_Cd >= 5001 AND Box_Xfer_Hdr.Receiving_Store_Cd <= 5900) OR Box_Xfer_Hdr.Receiving_Store_Cd = 9999 OR Box_Xfer_Hdr.Receiving_Store_Cd = 5990) "
        ElseIf (TStore <> "" And TStore <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Receiving_Store_Cd=" & TStore & " "
        ElseIf (TDis <> "" And TDis <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Receiving_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE District = " & TDis & ") "
        ElseIf (TReg <> "" And TReg <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Receiving_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE Region = " & TReg & ") "
        End If

        If (NoTInternet <> "unchecked") Then
            strSearch = strSearch & "AND Box_Xfer_Hdr.Receiving_Store_Cd <> 5990 "
        End If

        If (GERS <> "" And GERS <> Nothing) Then
            'strSearch = strSearch & "AND (IC_Box_Trans_Status.GERSStatus_Cd=" & (GERS) & ") "
            strSearch = strSearch & "AND (IC_Box_Trans_Status.GERSStatus_Cd IN (" & (GERS) & ")) "
        End If

        If (Discrep <> "" And Discrep <> Nothing) Then
            strSearch = strSearch & "AND (IC_Box_Trans_Status.Disc_Cd IN (" & (Discrep) & ")) "
        End If

        If (TType <> "" And TType <> Nothing) Then
            strSearch = strSearch & "AND (IC_Box_Trans_Status.Xfer_Type_Cd IN (" & (TType) & ")) "
        End If

        If (TStatus <> "" And TStatus <> Nothing) Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Current_Box_Status_Cd IN (" & (TStatus) & ")) "
        End If

        If (DFQty <> "" And DFQty <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Item.Discrepancy >= " & DFQty & " "
        End If

        If (DTQty <> "" And DTQty <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Item.Discrepancy <= " & DTQty & " "
        End If

        If (DFAmt <> "" And DFAmt <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Item.Discrep_Amt >= " & DFAmt & " "
        End If

        If (DTAmt <> "" And DTAmt <> Nothing) Then
            strSearch = strSearch & "AND Box_Xfer_Item.Discrep_Amt <= " & DTAmt & " "
        End If

        If (FDate <> "" And FDate <> Nothing And TDate <> "" And TDate <> Nothing) Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & TDate & "' ) "
        End If

        If (TrackNum <> "" And TrackNum <> Nothing) Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Tracking_Num=('" & (TrackNum) & "')) "
        End If

        If (TransNum <> "" And TransNum <> Nothing) Then
            strSearch = strSearch & "AND (Box_Xfer_Hdr.Box_Id=('" & (TransNum) & "')) "
        End If
    End Sub
    Private Sub GetGersResults()
        'Gets results from particular transfer # in order to edit GERS status
        SQL = "SELECT Distinct Box_Xfer_Hdr.Shipment_Date as Shipment_Date, Box_Xfer_Hdr.Current_Box_Status_Cd as Current_Box_Status_Cd, Box_Status_Type.Status_Desc as Status_Desc,IC_Box_Trans_Status.Xfer_Type_Cd as Xfer_Type_Cd, IC_Box_Trans_Status.Box_Id as Box_Id, "
        SQL = SQL & "Xfer_Type.Xfer_Desc as Xfer_Desc, IC_Box_Trans_Status.GERSStatus_Cd, GERSStatus_Type.GERSStatus_Desc as GERSStatus_Desc,IC_Box_Trans_Status.Disc_Cd, Discrep_Type.Disc_Desc as Disc_Desc, Box_Xfer_Hdr.Sending_Store_Cd as Sending_Store, Box_Xfer_Hdr.Receiving_Store_Cd as Receiving_Store "
        SQL = SQL & "FROM Box_Xfer_Hdr LefT OUTER JOIN "
        SQL = SQL & "Box_Xfer_Item ON Box_Xfer_Hdr.Box_Id = Box_Xfer_Item.Box_Id LEFT OUTER JOIN "
        SQL = SQL & "IC_Box_Trans_Status ON Box_Xfer_Hdr.Box_Id = IC_Box_Trans_Status.Box_Id "
        SQL = SQL & "Left Outer Join Box_Status_Type ON Box_xfer_Hdr.Current_Box_Status_Cd=Box_Status_Type.Status_Cd "
        SQL = SQL & "Left Outer Join Xfer_Type ON IC_Box_Trans_Status.Xfer_Type_Cd=Xfer_Type.Xfer_Cd "
        SQL = SQL & "Left Outer Join GERSSTatus_Type ON IC_Box_Trans_Status.GERSStatus_Cd=GERSStatus_Type.GERSStatus_Cd  "
        SQL = SQL & "Left Outer Join Discrep_Type ON IC_Box_Trans_Status.Disc_Cd=Discrep_Type.Disc_Cd "
        SQL = SQL & "Left OUter JOin Box_To_Include ON IC_Box_Trans_Status.Box_Id=Box_To_Include.Box_Id "
        SQL = SQL & "WHERE Box_To_Include.Box_Includes_Cd='6' AND Box_Xfer_Hdr.Box_Id='" & GBID & "' "

        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()

        Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
        dtrSearch = cmdSearch.ExecuteReader

        dgTTResults.DataSource = dtrSearch
        dgTTResults.DataBind()
        dtrSearch.Close()
        conSearch.Close()
        cmdSearch.Dispose()

    End Sub
    Private Sub ExecSearch()
        'execute search query
        SQL = "SELECT Distinct Box_Xfer_Hdr.Shipment_Date as Shipment_Date, Box_Xfer_Hdr.Current_Box_Status_Cd as Current_Box_Status_Cd, Box_Status_Type.Status_Desc as Status_Desc,IC_Box_Trans_Status.Xfer_Type_Cd as Xfer_Type_Cd, IC_Box_Trans_Status.Box_Id as Box_Id, "
        SQL = SQL & "Xfer_Type.Xfer_Desc as Xfer_Desc, IC_Box_Trans_Status.GERSStatus_Cd, GERSStatus_Type.GERSStatus_Desc as GERSStatus_Desc,IC_Box_Trans_Status.Disc_Cd, Discrep_Type.Disc_Desc as Disc_Desc, Box_Xfer_Hdr.Sending_Store_Cd as Sending_Store, Box_Xfer_Hdr.Receiving_Store_Cd as Receiving_Store "
        SQL = SQL & "FROM Box_Xfer_Hdr LefT OUTER JOIN "
        SQL = SQL & "Box_Xfer_Item ON Box_Xfer_Hdr.Box_Id = Box_Xfer_Item.Box_Id LEFT OUTER JOIN "
        SQL = SQL & "IC_Box_Trans_Status ON Box_Xfer_Hdr.Box_Id = IC_Box_Trans_Status.Box_Id "
        SQL = SQL & "Left Outer Join Box_Status_Type ON Box_xfer_Hdr.Current_Box_Status_Cd=Box_Status_Type.Status_Cd "
        SQL = SQL & "Left Outer Join Xfer_Type ON IC_Box_Trans_Status.Xfer_Type_Cd=Xfer_Type.Xfer_Cd "
        SQL = SQL & "Left Outer Join GERSSTatus_Type ON IC_Box_Trans_Status.GERSStatus_Cd=GERSStatus_Type.GERSStatus_Cd  "
        SQL = SQL & "Left Outer Join Discrep_Type ON IC_Box_Trans_Status.Disc_Cd=Discrep_Type.Disc_Cd "
        SQL = SQL & "Left OUter JOin Box_To_Include ON IC_Box_Trans_Status.Box_Id=Box_To_Include.Box_Id "
        SQL = SQL & "WHERE Box_To_Include.Box_Includes_Cd='6' "
        If Len(strSearch) > 0 Then
            'SQL = SQL & " WHERE" & Mid(strSearch, 4, Len(strSearch) - 3)
            SQL = SQL & strSearch
        End If
        SQL = SQL & "Order By Xfer_Desc, Box_Id "

        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()

        Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
        dtrSearch = cmdSearch.ExecuteReader

        dgTTResults.DataSource = dtrSearch
        dgTTResults.DataBind()
        dtrSearch.Close()
        conSearch.Close()
        cmdSearch.Dispose()


    End Sub
    Protected Function GetBoxTransferTypesSD(ByVal Box_Id As String) As String
        'Get Short Description
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim strReturn As String

        objConnection.Open()

        strStoredProcedure = "spGetBoxTransferTypes '" & Box_Id & "' "

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader()

        Do While objDataReader.Read
            strReturn = strReturn & objDataReader("Xfer_Short_Desc") & ","
        Loop

        objConnection.Close()
        objDataReader.Close()
        objCommand.Dispose()
        If Len(strReturn) > 0 Then
            strReturn = Mid(strReturn, 1, Len(strReturn) - 1)
        Else
            strReturn = ""
        End If

        Return strReturn

    End Function
    Protected Function GetBoxComments(ByVal Box_Id As String, ByVal Xfer_Type_Cd As Int32) As String
        'Get Short Description
        Dim NullCheck As Boolean

        Dim strReturn As String

        Dim conGetComment As System.Data.SqlClient.SqlConnection
        Dim cmdGetComment As System.Data.SqlClient.SqlCommand
        Dim dtrGetComment As SqlClient.SqlDataReader

        conGetComment = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        conGetComment.Open()
        cmdGetComment = New SqlClient.SqlCommand("spGetICComments", conGetComment)
        cmdGetComment.CommandType = CommandType.StoredProcedure
        cmdGetComment.Parameters.Add("@BID", Box_Id)
        cmdGetComment.Parameters.Add("@TransType", Xfer_Type_Cd)
        dtrGetComment = cmdGetComment.ExecuteReader
      
        Dim Test
        If dtrGetComment.Read Then
            Test = (dtrGetComment("Comment"))
        End If

        NullCheck = IsDBNull(Test)
        If NullCheck = True Then
            'strReturn = "Images/CommentsExist.jpg"
            strReturn = "../Images/Commentsblank.jpg"
        Else
            'strReturn = "Images/Commentsblank.jpg"
            strReturn = "../Images/CommentsExist.jpg"
        End If


        conGetComment.Close()
        dtrGetComment.Close()
        cmdGetComment.Dispose()
        Return strReturn
        '**********************************************************************************
        'Dim strStoredProcedure As String
        'Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        'Dim strReturn As String

        'objConnection.Open()

        'strStoredProcedure = "spGetBoxTransferTypes '" & Box_Id & "' "

        'Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        'Dim objDataReader As SqlClient.SqlDataReader

        'objDataReader = objCommand.ExecuteReader()

        'Do While objDataReader.Read
        'strReturn = strReturn & objDataReader("Xfer_Short_Desc") & ","
        'Loop

        'objConnection.Close()

        'If Len(strReturn) > 0 Then
        'strReturn = Mid(strReturn, 1, Len(strReturn) - 1)
        'Else
        'strReturn = ""
        ' End If
    End Function
    Protected Function GetReturnQuery() As String
        'Provides return query from Comments page.
        Dim strReturn As String 'Return query for comments page.
        'Dim strPage As String
        'strPage = "IC_ResultsTT.aspx"

        If GBID = Nothing Then
            'Not searching for Gers Status, do normal query
            'strReturn = "IC_ResultsTT.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSearch)
            'strReturn = "IC_ResultsTT.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum
            strReturn = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum
        Else
            'want query on particular box.
            'strReturn = strPage & "?GERSBID=" & GBID
            strReturn = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID
        End If

        Return strReturn
    End Function
    Public Function GetRTNEditTransfer()
        'This function gets the return string for when the user goes to the IC_EditTransfer page.  When they hit
        'return on that page it will return to this page.
        Dim strRTN
        If GBID = Nothing Then
            'strRTN = "IC_ResultsTT.aspx?SQL2=" & strSearch
            'strRTN = "IC_ResultsTT.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSearch)
            strRTN = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum
        Else
            'strRTN = "IC_ResultsTT.aspx?GERSBID=" & GBID
            strRTN = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID
        End If

        Return strRTN
    End Function
    Private Sub VoidBox()
        'IC wants to void box
        Dim intStatus
        Dim strempid As String
        Dim Modified
        Modified = Now
        strBox = Request.Params("BID")
        intStatus = 7

        If Not objUserInfo.EmpId Is Nothing Then
            strUser = objUserInfo.EmpFullName
            strempid = objUserInfo.EmpId
        End If

        Dim conUpdateHistory As System.Data.SqlClient.SqlConnection
        Dim cmdUpdateHistory As System.Data.SqlClient.SqlCommand
        conUpdateHistory = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdUpdateHistory = New SqlClient.SqlCommand("spUpdateBoxHistory", conUpdateHistory)
        cmdUpdateHistory.CommandType = CommandType.StoredProcedure
        cmdUpdateHistory.Parameters.Add("@Box_Id", strBox)
        cmdUpdateHistory.Parameters.Add("@Status", intStatus)
        cmdUpdateHistory.Parameters.Add("@EmpId", strempid)
        cmdUpdateHistory.Parameters.Add("@EmpFullName", strUser)
        cmdUpdateHistory.Parameters.Add("@UpdateTime", Modified)
        conUpdateHistory.Open()
        cmdUpdateHistory.ExecuteNonQuery()
        conUpdateHistory.Close()
        cmdUpdateHistory.Dispose()
        Dim conUpdateBoxHdr As System.Data.SqlClient.SqlConnection
        Dim cmdUpdateBoxHdr As System.Data.SqlClient.SqlCommand
        conUpdateBoxHdr = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdUpdateBoxHdr = New SqlClient.SqlCommand("spUpdateBoxHdrStatus", conUpdateBoxHdr)
        cmdUpdateBoxHdr.CommandType = CommandType.StoredProcedure
        cmdUpdateBoxHdr.Parameters.Add("@BID", strBox)
        cmdUpdateBoxHdr.Parameters.Add("@Status", intStatus)
        cmdUpdateBoxHdr.Parameters.Add("@Modified", Modified)
        conUpdateBoxHdr.Open()
        cmdUpdateBoxHdr.ExecuteNonQuery()
        conUpdateBoxHdr.Close()
        cmdUpdateBoxHdr.Dispose()
    End Sub
    Private Sub ddlView_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlView.SelectedIndexChanged
        'User has changed view type
        If ddlView.SelectedItem.Value = "TransferType" Then
            'Show Columns: Transfer#,Trans Type, date, box status, gers status, descrep status, comment
            'Order by transfer type

        ElseIf ddlView.SelectedItem.Value = "Transfer" Then
            'Redirect to IC_Results
            'strSearch = Request.Params("SQL2")
            'Response.Redirect("IC_Results.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSearch))
            Response.Redirect("IC_Results.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum)
        End If
    End Sub
    Private Sub btnExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExecute.Click
        If ddlOptions.SelectedItem.Value = "Print Search Results" Then
            'Dim strPage As String
            'strPage = "IC_ResultsTT.aspx"
            GBID = Request.Params("GERSBID")
            If GBID = Nothing Then
                'Not searching for Gers Status, do normal query
                'strSearch = Request.Params("SQL2")
                'Response.Redirect("IC_PrintSearchResultsTT.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSearch) & "&RPage=" & strPage)
                Response.Redirect("IC_PrintSearchResultsTT.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum)
            Else
                'want query on particular box.
                'Response.Redirect("IC_PrintSearchResultsTT.aspx?GERSBID=" & System.Web.HttpUtility.UrlEncode(GBID) & "&RPage=" & strPage)
                Response.Redirect("IC_PrintSearchResultsTT.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID)
            End If
        ElseIf ddlOptions.SelectedItem.Value = "Print GERS" Then
            Dim i As Integer
            Dim dgi As DataGridItem
            Dim chkExcept As CheckBox

            For i = 0 To dgTTResults.Items.Count - 1
                dgi = dgTTResults.Items(i)
                chkExcept = CType(dgi.FindControl("chkEItem"), CheckBox)
                If chkExcept.Checked Then
                    'Don't change status

                Else
                    'Checkbox is not checked, change gers status to print Queue (4)
                    Dim PBox_Id As String
                    Dim PItemTransType As DataGridItem
                    Dim PTransType As String
                    Dim PBoxStatus As String
                    Dim PToStore As String
                    Dim intXferCd As Int32
                    Dim parmXferCd As SqlClient.SqlParameter
                    PBox_Id = CType(dgi.FindControl("lblTN"), Label).Text  'Gets Box Id

                    PItemTransType = dgTTResults.Items(i)
                    PToStore = PItemTransType.Cells(2).Text    'Get To Store
                    PTransType = PItemTransType.Cells(3).Text  'Gets Transfer Type
                    PBoxStatus = PItemTransType.Cells(5).Text  'Gets Box Current Status

                    'If (PBoxStatus = "Received" Or PBoxStatus = "Shipped") Then
                    If ((PBoxStatus = "Received" Or PBoxStatus = "Shipped") And PToStore <> "9999") Then
                        'If PBoxStatus isn't "Received" or "Shipped" then don't print out and update status.

                        'Get Transfer Type CD
                        Dim conFindTTCD As System.Data.SqlClient.SqlConnection
                        Dim cmdFindTTCD As System.Data.SqlClient.SqlCommand
                        conFindTTCD = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                        cmdFindTTCD = New SqlClient.SqlCommand("spRetrieveTransType", conFindTTCD)
                        cmdFindTTCD.CommandType = CommandType.StoredProcedure
                        cmdFindTTCD.Parameters.Add("@TType", PTransType)
                        parmXferCd = cmdFindTTCD.Parameters.Add("@XferCd", SqlDbType.TinyInt)
                        parmXferCd.Size = 1
                        parmXferCd.Direction = ParameterDirection.Output
                        conFindTTCD.Open()
                        cmdFindTTCD.ExecuteNonQuery()
                        intXferCd = cmdFindTTCD.Parameters("@XferCd").Value
                        conFindTTCD.Close()
                        cmdFindTTCD.Dispose()
                        'Update GersStatus in IC_Box_Trans_Status Table to Print Queue
                        Dim conUpdateGersStatus As System.Data.SqlClient.SqlConnection
                        Dim cmdUpdateGersStatus As System.Data.SqlClient.SqlCommand
                        conUpdateGersStatus = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                        cmdUpdateGersStatus = New SqlClient.SqlCommand("spUpdateGersPrintQueue", conUpdateGersStatus)
                        cmdUpdateGersStatus.CommandType = CommandType.StoredProcedure
                        cmdUpdateGersStatus.Parameters.Add("@BID", PBox_Id)
                        cmdUpdateGersStatus.Parameters.Add("@TType", intXferCd)
                        conUpdateGersStatus.Open()
                        cmdUpdateGersStatus.ExecuteNonQuery()
                        conUpdateGersStatus.Close()
                        cmdUpdateGersStatus.Dispose()

                        Dim Notes As String
                        Notes = "This has been printed to GERS."
                        Call UpdateICHistory(PBox_Id, Notes)
                        'Response.Redirect("IC_ProfileList.aspx")
                    End If

                End If
            Next
            Response.Redirect("IC_ProfileList.aspx")
        ElseIf ddlOptions.SelectedItem.Value = "Print Discrepancy" Then
            'Finished
            'Dim strPage As String
            'strPage = "IC_ResultsTT.aspx"
            GBID = Request.Params("GERSBID")

            If GBID = Nothing Then
                'Not searching for Gers Status, do normal query
                'strSearch = Request.Params("SQL2")
                'Response.Redirect("IC_PrintDiscrepancy.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSearch) & "&RPage=" & strPage)
                Response.Redirect("IC_PrintDiscrepancy.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum)
            Else
                'want query on particular box.
                'strSearch = Request.Params("SQL2")
                'Response.Redirect("IC_PrintDiscrepancy.aspx?GERSBID=" & System.Web.HttpUtility.UrlEncode(GBID) & "&RPage=" & strPage)
                Response.Redirect("IC_PrintDiscrepancy.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID)
            End If

        ElseIf ddlOptions.SelectedItem.Value = "Update GERS" Then
            Dim i As Integer
            Dim dgi As DataGridItem
            Dim chkExcept As CheckBox

            For i = 0 To dgTTResults.Items.Count - 1
                dgi = dgTTResults.Items(i)
                chkExcept = CType(dgi.FindControl("chkEItem"), CheckBox)
                If chkExcept.Checked Then
                    'Do Nothing
                Else
                    'Update status to Processed('Recorded') if transfer has been received and printed.
                    Dim PBox_Id As String
                    Dim PItemTransType As DataGridItem
                    Dim PTransType As String
                    Dim PToStore As String
                    Dim PBoxStatus As String
                    Dim PGersStatus As String
                    Dim intXferCd As Int32
                    Dim parmXferCd As SqlClient.SqlParameter
                    PBox_Id = CType(dgi.FindControl("lblTN"), Label).Text  'Gets Box Id

                    PItemTransType = dgTTResults.Items(i)
                    PToStore = PItemTransType.Cells(2).Text    'Get To Store
                    PTransType = PItemTransType.Cells(3).Text  'Gets Transfer Type
                    PBoxStatus = PItemTransType.Cells(5).Text  'Gets Box Current Status
                    PGersStatus = PItemTransType.Cells(6).Text  'Gets Gers Status

                    'If PBoxStatus = "Received" And PGersStatus = "Printed" Then
                    If (PBoxStatus = "Received" And PGersStatus = "Printed" And PToStore <> "9999") Then
                        'If PBoxStatus isn't "Received" then don't update gers status.
                        'DC gets updated differently than using this function.

                        'Get Transfer Type CD
                        Dim conFindTTCD As System.Data.SqlClient.SqlConnection
                        Dim cmdFindTTCD As System.Data.SqlClient.SqlCommand
                        conFindTTCD = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                        cmdFindTTCD = New SqlClient.SqlCommand("spRetrieveTransType", conFindTTCD)
                        cmdFindTTCD.CommandType = CommandType.StoredProcedure
                        cmdFindTTCD.Parameters.Add("@TType", PTransType)
                        parmXferCd = cmdFindTTCD.Parameters.Add("@XferCd", SqlDbType.TinyInt)
                        parmXferCd.Size = 1
                        parmXferCd.Direction = ParameterDirection.Output
                        conFindTTCD.Open()
                        cmdFindTTCD.ExecuteNonQuery()
                        intXferCd = cmdFindTTCD.Parameters("@XferCd").Value
                        conFindTTCD.Close()
                        cmdFindTTCD.Dispose()
                        'Update GersStatus in IC_Box_Trans_Status Table to Processed(Recorded)
                        Dim conUpdateGersStatus As System.Data.SqlClient.SqlConnection
                        Dim cmdUpdateGersStatus As System.Data.SqlClient.SqlCommand
                        conUpdateGersStatus = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                        cmdUpdateGersStatus = New SqlClient.SqlCommand("spUpdateRecordGers", conUpdateGersStatus)
                        cmdUpdateGersStatus.CommandType = CommandType.StoredProcedure
                        cmdUpdateGersStatus.Parameters.Add("@BID", PBox_Id)
                        cmdUpdateGersStatus.Parameters.Add("@TType", intXferCd)
                        conUpdateGersStatus.Open()
                        cmdUpdateGersStatus.ExecuteNonQuery()
                        conUpdateGersStatus.Close()
                        cmdUpdateGersStatus.Dispose()
                        'Response.Redirect("IC_ProfileList.aspx")
                        Dim Notes As String
                        Notes = "GERS has been updated."
                        Call UpdateICHistory(PBox_Id, Notes)
                    End If
                End If
            Next  'End For Loop
            Response.Redirect("IC_ProfileList.aspx")
        ElseIf ddlOptions.SelectedItem.Value = "Check-In DC" Then
            'mark RTVs, Defectives, CTBs as Recorded (box status), Processed (in GERS), 
            'set discrep status to ND,discrep amt to zero, and update qty received only when box status is shipped.
            Dim i As Integer
            Dim dgi As DataGridItem
            Dim chkExcept As CheckBox

            For i = 0 To dgTTResults.Items.Count - 1
                dgi = dgTTResults.Items(i)
                chkExcept = CType(dgi.FindControl("chkEItem"), CheckBox)
                If chkExcept.Checked Then
                    'Do Nothing
                Else
                    'If box status is shipped, and transfer type is (RTV or CTB or Defective or STW) then update info.
                    Dim PBox_Id As String
                    Dim PItemTransType As DataGridItem
                    Dim PTransType As String  'Transfer Type
                    Dim PBoxStatus As String  'Box Current Status
                    Dim PGersStatus As String 'Gers Status
                    Dim intXferCd As Int32
                    Dim parmXferCd As SqlClient.SqlParameter
                    PBox_Id = CType(dgi.FindControl("lblTN"), Label).Text  'Gets Box Id

                    PItemTransType = dgTTResults.Items(i)
                    PTransType = PItemTransType.Cells(3).Text   'Gets Transfer Type
                    PBoxStatus = PItemTransType.Cells(5).Text   'Gets Box Current Status
                    PGersStatus = PItemTransType.Cells(6).Text  'Gets Gers Status

                    If PBoxStatus = "Shipped" And PGersStatus = "Open" Then
                        'Check Transfer Type CD
                        Dim conFindTTCD As System.Data.SqlClient.SqlConnection
                        Dim cmdFindTTCD As System.Data.SqlClient.SqlCommand
                        conFindTTCD = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                        cmdFindTTCD = New SqlClient.SqlCommand("spRetrieveTransType", conFindTTCD)
                        cmdFindTTCD.CommandType = CommandType.StoredProcedure
                        cmdFindTTCD.Parameters.Add("@TType", PTransType)
                        parmXferCd = cmdFindTTCD.Parameters.Add("@XferCd", SqlDbType.TinyInt)
                        parmXferCd.Size = 1
                        parmXferCd.Direction = ParameterDirection.Output
                        conFindTTCD.Open()
                        cmdFindTTCD.ExecuteNonQuery()
                        intXferCd = cmdFindTTCD.Parameters("@XferCd").Value
                        conFindTTCD.Close()
                        cmdFindTTCD.Dispose()
                        If intXferCd = 1 Or intXferCd = 2 Or intXferCd = 5 Or intXferCd = 7 Then
                            'To update info, transfer type must be RTV, Defective, or CTB, or STW
                            Dim intBoxStatus As Int32
                            Dim intGersStatus As Int32
                            Dim intDiscrepStatus As Int32
                            Dim strEmpName As String
                            Dim strEmpNum As String
                            Dim ModifiedDate As Date
                            Dim strSQL As String
                            intBoxStatus = 4  'Set to Received
                            intGersStatus = 3  'Set to Processed(Recorded)
                            intDiscrepStatus = 1 'No Discrepancy
                            strEmpName = objUserInfo.EmpFullName
                            strEmpNum = objUserInfo.EmpId
                            ModifiedDate = Now

                            'Update Box_Xfer_Hdr (updating box status to received.)
                            Dim conUpdateHdr As System.Data.SqlClient.SqlConnection
                            Dim cmdUpdateHdr As System.Data.SqlClient.SqlCommand
                            conUpdateHdr = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                            cmdUpdateHdr = New SqlClient.SqlCommand("spDCUpdateBoxHeader", conUpdateHdr)
                            cmdUpdateHdr.CommandType = CommandType.StoredProcedure
                            cmdUpdateHdr.Parameters.Add("@Box_Id", PBox_Id)
                            cmdUpdateHdr.Parameters.Add("@Current_Box_Status", intBoxStatus)
                            cmdUpdateHdr.Parameters.Add("@Received_Date", ModifiedDate)
                            cmdUpdateHdr.Parameters.Add("@Modified_Date", ModifiedDate)
                            conUpdateHdr.Open()
                            cmdUpdateHdr.ExecuteNonQuery()
                            conUpdateHdr.Close()
                            cmdUpdateHdr.Dispose()
                            'Update IC_Box_trans_Status Table where the box id and xfer type are the same.
                            Dim conUpdateICBoxTransStatus As System.Data.SqlClient.SqlConnection
                            Dim cmdUpdateICBoxTransStatus As System.Data.SqlClient.SqlCommand
                            conUpdateICBoxTransStatus = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                            cmdUpdateICBoxTransStatus = New SqlClient.SqlCommand("spDCUpdateICTrans", conUpdateICBoxTransStatus)
                            cmdUpdateICBoxTransStatus.CommandType = CommandType.StoredProcedure
                            cmdUpdateICBoxTransStatus.Parameters.Add("@Box_Id", PBox_Id)
                            cmdUpdateICBoxTransStatus.Parameters.Add("@XferType", intXferCd)
                            cmdUpdateICBoxTransStatus.Parameters.Add("@GersStatus", intGersStatus)
                            cmdUpdateICBoxTransStatus.Parameters.Add("@DiscrepStatus", intDiscrepStatus)
                            conUpdateICBoxTransStatus.Open()
                            cmdUpdateICBoxTransStatus.ExecuteNonQuery()
                            conUpdateICBoxTransStatus.Close()
                            cmdUpdateICBoxTransStatus.Dispose()
                            'Update Box_Xfer_Items
                            strSQL = "Select Box_Id,Xfer_Type_Cd,Sku_Num,Item_Qty,Received_Qty,Discrepancy,Discrep_Amt From Box_Xfer_Item Where Box_Id='" & PBox_Id & "' and Xfer_Type_Cd='" & intXferCd & "' "
                            Dim conUpdateItems As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                            Dim dtrUpdateItems As SqlClient.SqlDataReader
                            conUpdateItems.Open()
                            Dim cmdUpdateItems As New SqlClient.SqlCommand(strSQL, conUpdateItems)
                            dtrUpdateItems = cmdUpdateItems.ExecuteReader

                            'Set discrep values and received qty
                            Dim strSku As String
                            Dim intItemQty As Integer
                            Dim intRecQty As Integer
                            Dim intDiscrepQty As Integer
                            Dim DiscrepAmt As Integer
                            DiscrepAmt = 0 'Set discrepancy dollar amt to zero
                            intDiscrepQty = 0 'Set discrepancy qty to zero
                            Do While dtrUpdateItems.Read
                                intXferCd = dtrUpdateItems("Xfer_Type_Cd")
                                strSku = dtrUpdateItems("Sku_Num")
                                intItemQty = CInt(dtrUpdateItems("Item_Qty"))
                                intRecQty = CInt(dtrUpdateItems("Received_Qty"))

                                Dim conUpItems As System.Data.SqlClient.SqlConnection
                                Dim cmdUpItems As System.Data.SqlClient.SqlCommand
                                conUpItems = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                                cmdUpItems = New SqlClient.SqlCommand("spDCUpdateBoxItems", conUpItems)
                                cmdUpItems.CommandType = CommandType.StoredProcedure
                                cmdUpItems.Parameters.Add("@Box_Id", PBox_Id)
                                cmdUpItems.Parameters.Add("@Xfer_Cd", intXferCd)
                                cmdUpItems.Parameters.Add("@Sku", strSku)
                                cmdUpItems.Parameters.Add("@IQty", intItemQty)
                                cmdUpItems.Parameters.Add("@RQty", intItemQty)  'set received qty to the same as item qty.  No Discrepancy
                                cmdUpItems.Parameters.Add("@DiscrepQty", intDiscrepQty)
                                cmdUpItems.Parameters.Add("@DiscrepAmt", DiscrepAmt)
                                conUpItems.Open()
                                cmdUpItems.ExecuteNonQuery()
                                conUpItems.Close()
                                cmdUpItems.Dispose()
                            Loop  'end do while loop for updating discrepancies
                            dtrUpdateItems.Close()
                            conUpdateItems.Close()
                            cmdUpdateItems.Dispose()
                            'Update Box History Table
                            Dim conUpdateHistory As System.Data.SqlClient.SqlConnection
                            Dim cmdUpdateHistory As System.Data.SqlClient.SqlCommand
                            conUpdateHistory = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                            cmdUpdateHistory = New SqlClient.SqlCommand("spUpdateBoxHistory", conUpdateHistory)
                            cmdUpdateHistory.CommandType = CommandType.StoredProcedure
                            cmdUpdateHistory.Parameters.Add("@Box_Id", PBox_Id)
                            cmdUpdateHistory.Parameters.Add("@Status", intBoxStatus)
                            cmdUpdateHistory.Parameters.Add("@EmpId", strEmpNum)
                            cmdUpdateHistory.Parameters.Add("@EmpFullName", strEmpName)
                            cmdUpdateHistory.Parameters.Add("@UpdateTime", ModifiedDate)
                            conUpdateHistory.Open()
                            cmdUpdateHistory.ExecuteNonQuery()
                            conUpdateHistory.Close()
                            cmdUpdateHistory.Dispose()

                            Dim Notes As String
                            Notes = "IC has checked this in."
                            Call UpdateICHistory(PBox_Id, Notes)
                        End If 'end if for checking transfer type
                    End If  'end if for checking box status is shipped
                End If 'end if for checked box

            Next 'end of for loop

        End If
        Response.Redirect("IC_ProfileList.aspx")
    End Sub
    Private Sub UpdateICHistory(ByVal BoxId As String, ByVal Note As String)
        'This updates the IC History
        Dim CreatedDate As DateTime
        Dim EmpId As String
        CreatedDate = Now
        EmpId = Trim(objUserInfo.EmpId)

        Dim conUpdateICHistory As System.Data.SqlClient.SqlConnection
        Dim cmdUpdateICHistory As System.Data.SqlClient.SqlCommand
        conUpdateICHistory = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdUpdateICHistory = New SqlClient.SqlCommand("spICInsertHistory", conUpdateICHistory)
        cmdUpdateICHistory.CommandType = CommandType.StoredProcedure
        cmdUpdateICHistory.Parameters.Add("@BID", BoxId)
        cmdUpdateICHistory.Parameters.Add("@EID", EmpId)
        cmdUpdateICHistory.Parameters.Add("@NOTE", Note)
        cmdUpdateICHistory.Parameters.Add("@DATE", CreatedDate)
        conUpdateICHistory.Open()
        cmdUpdateICHistory.ExecuteNonQuery()
        conUpdateICHistory.Close()
        cmdUpdateICHistory.Dispose()
    End Sub
    Private Sub ddlOptions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlOptions.SelectedIndexChanged

    End Sub
End Class
