Public Class IC_DCMaint
    Inherits System.Web.UI.Page
    Protected WithEvents ucHeader As Header
    Protected WithEvents lblTrans As System.Web.UI.WebControls.Label
    Protected WithEvents txtTransNum As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Private objUserInfo As New UserInfo()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()

        'Put user code to initialize the page here
        ucHeader.lblTitle = "Inventory Control DC Maintenance"
        'ucHeader.CurrentPage(Header.PageName.DCMaint)
        ucHeader.CurrentMode(Header.HeaderGroup.IC)

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'Check first to make sure that transfer number exists.
        'Then update to received, recorded, accept all quantities of merchandise, set discrep to ND
        lblError.Text = ""
        Dim strTransNum As String
        Dim strSQL As String
        Dim lenTransNum As Integer
        strTransNum = Trim(txtTransNum.Text)
        lenTransNum = Len(strTransNum)

        If lenTransNum <> 0 Then
            strSQL = "Select Box_Id From Box_Xfer_Hdr Where Box_Id='" & strTransNum & "' AND (Receiving_Store_Cd = '9999' OR Receiving_Store_Cd='5990') "
            Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            Dim dtrSearch As SqlClient.SqlDataReader
            conSearch.Open()
            Dim cmdSearch As New SqlClient.SqlCommand(strSQL, conSearch)
            dtrSearch = cmdSearch.ExecuteReader
            Dim count As Integer
            count = 0
            'If it is a valid transfer to DC or Internet counter will not be zero.
            Do While dtrSearch.Read
                count = count + 1
            Loop
            dtrSearch.Close()
            conSearch.Close()
            cmdSearch.Dispose()
            If count > 0 Then
                'Check certain status's to see if okay to allow update
                strSQL = "Select current_box_Status_cd From Box_Xfer_Hdr Where Box_Id='" & strTransNum & "' "
                Dim conCheckBoxStatus As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                Dim dtrCheckBoxStatus As SqlClient.SqlDataReader
                conCheckBoxStatus.Open()
                Dim cmdCheckBoxStatus As New SqlClient.SqlCommand(strSQL, conCheckBoxStatus)
                dtrCheckBoxStatus = cmdCheckBoxStatus.ExecuteReader
                Dim intCurrentBoxStatus As Integer
                Do While dtrCheckBoxStatus.Read
                    intCurrentBoxStatus = dtrCheckBoxStatus("Current_Box_Status_cd")
                Loop
                dtrCheckBoxStatus.Close()
                conCheckBoxStatus.Close()
                cmdCheckBoxStatus.Dispose()
                If (intCurrentBoxStatus <> 1 And intCurrentBoxStatus <> 6) Then

                    'Now check Gers Status.  If open, let update occur.
                    strSQL = "Select gersstatus_cd From IC_Box_Trans_Status Where Box_Id='" & strTransNum & "' "
                    Dim conCheckGers As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                    Dim dtrCheckGers As SqlClient.SqlDataReader
                    conCheckGers.Open()
                    Dim cmdCheckGers As New SqlClient.SqlCommand(strSQL, conCheckGers)
                    dtrCheckGers = cmdCheckGers.ExecuteReader
                    Dim intGersStatusCD As Integer
                    Dim blnUpdate As Boolean
                    Do While dtrCheckGers.Read
                        intGersStatusCD = dtrCheckGers("gersstatus_cd")
                        If intGersStatusCD = 1 Then
                            blnUpdate = True
                        End If
                    Loop
                    dtrCheckGers.Close()
                    conCheckGers.Close()
                    cmdCheckGers.Dispose()
                    If blnUpdate = True Then
                        'Proceed with update
                        '****************************************************************************************
                        'Update info needed.
                        Dim intBoxStatus As Int32
                        Dim intGersStatus As Int32
                        Dim intDiscrepStatus As Int32
                        Dim strEmpName As String
                        Dim strEmpNum As String
                        Dim ModifiedDate As Date
                        intBoxStatus = 5  'Set to Recorded
                        intGersStatus = 3  'Set to Processed(Recorded)
                        intDiscrepStatus = 1 'No Discrepancy
                        strEmpName = objUserInfo.EmpFullName
                        strEmpNum = objUserInfo.EmpId
                        ModifiedDate = Now

                        'Update Box_Xfer_Hdr
                        Dim conUpdateHdr As System.Data.SqlClient.SqlConnection
                        Dim cmdUpdateHdr As System.Data.SqlClient.SqlCommand
                        conUpdateHdr = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                        cmdUpdateHdr = New SqlClient.SqlCommand("spDCUpdateBoxHeader", conUpdateHdr)
                        cmdUpdateHdr.CommandType = CommandType.StoredProcedure
                        cmdUpdateHdr.Parameters.Add("@Box_Id", strTransNum)
                        cmdUpdateHdr.Parameters.Add("@Current_Box_Status", intBoxStatus)
                        cmdUpdateHdr.Parameters.Add("@Received_Date", ModifiedDate)
                        cmdUpdateHdr.Parameters.Add("@Modified_Date", ModifiedDate)
                        conUpdateHdr.Open()
                        cmdUpdateHdr.ExecuteNonQuery()
                        conUpdateHdr.Close()
                        cmdUpdateHdr.Dispose()
                        'Update IC_Box_trans_Status Table
                        Dim conUpdateICBoxTransStatus As System.Data.SqlClient.SqlConnection
                        Dim cmdUpdateICBoxTransStatus As System.Data.SqlClient.SqlCommand
                        conUpdateICBoxTransStatus = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                        cmdUpdateICBoxTransStatus = New SqlClient.SqlCommand("spDCUpdateICTrans", conUpdateICBoxTransStatus)
                        cmdUpdateICBoxTransStatus.CommandType = CommandType.StoredProcedure
                        cmdUpdateICBoxTransStatus.Parameters.Add("@Box_Id", strTransNum)
                        cmdUpdateICBoxTransStatus.Parameters.Add("@GersStatus", intGersStatus)
                        cmdUpdateICBoxTransStatus.Parameters.Add("@DiscrepStatus", intDiscrepStatus)
                        conUpdateICBoxTransStatus.Open()
                        cmdUpdateICBoxTransStatus.ExecuteNonQuery()
                        conUpdateICBoxTransStatus.Close()
                        cmdUpdateICBoxTransStatus.Dispose()
                        'Update Box_Xfer_Items
                        strSQL = "Select Box_Id,Xfer_Type_Cd,Sku_Num,Item_Qty,Received_Qty,Discrepancy,Discrep_Amt From Box_Xfer_Item Where Box_Id='" & strTransNum & "' "
                        Dim conUpdateItems As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                        Dim dtrUpdateItems As SqlClient.SqlDataReader
                        conUpdateItems.Open()
                        Dim cmdUpdateItems As New SqlClient.SqlCommand(strSQL, conUpdateItems)
                        dtrUpdateItems = cmdUpdateItems.ExecuteReader

                        'Set discrep values and received qty
                        Dim intXferCd As Integer
                        Dim strSku As String
                        Dim intItemQty As Integer
                        Dim intRecQty As Integer
                        Dim intDiscrepQty As Integer
                        Dim DiscrepAmt As Integer
                        DiscrepAmt = 0 'Set discrepancy dollar amt to zero
                        intDiscrepQty = 0 'Set discrepancy qty to zero
                        Do While dtrUpdateItems.Read
                            intXferCd = dtrUpdateItems("Xfer_Type_Cd")
                            strSku = dtrUpdateItems("Sku_Num")
                            intItemQty = CInt(dtrUpdateItems("Item_Qty"))
                            intRecQty = CInt(dtrUpdateItems("Received_Qty"))

                            Dim conUpItems As System.Data.SqlClient.SqlConnection
                            Dim cmdUpItems As System.Data.SqlClient.SqlCommand
                            conUpItems = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                            cmdUpItems = New SqlClient.SqlCommand("spDCUpdateBoxItems", conUpItems)
                            cmdUpItems.CommandType = CommandType.StoredProcedure
                            cmdUpItems.Parameters.Add("@Box_Id", strTransNum)
                            cmdUpItems.Parameters.Add("@Xfer_Cd", intXferCd)
                            cmdUpItems.Parameters.Add("@Sku", strSku)
                            cmdUpItems.Parameters.Add("@IQty", intItemQty)
                            cmdUpItems.Parameters.Add("@RQty", intItemQty)  'set received qty to the same as item qty.  No Discrepancy
                            cmdUpItems.Parameters.Add("@DiscrepQty", intDiscrepQty)
                            cmdUpItems.Parameters.Add("@DiscrepAmt", DiscrepAmt)
                            conUpItems.Open()
                            cmdUpItems.ExecuteNonQuery()
                            conUpItems.Close()
                            cmdUpItems.Dispose()
                        Loop
                        dtrUpdateItems.Close()
                        conUpdateItems.Close()
                        cmdUpdateItems.Dispose()

                        'Update Box History Table
                        Dim conUpdateHistory As System.Data.SqlClient.SqlConnection
                        Dim cmdUpdateHistory As System.Data.SqlClient.SqlCommand
                        conUpdateHistory = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
                        cmdUpdateHistory = New SqlClient.SqlCommand("spUpdateBoxHistory", conUpdateHistory)
                        cmdUpdateHistory.CommandType = CommandType.StoredProcedure
                        cmdUpdateHistory.Parameters.Add("@Box_Id", strTransNum)
                        cmdUpdateHistory.Parameters.Add("@Status", intBoxStatus)
                        cmdUpdateHistory.Parameters.Add("@EmpId", strEmpNum)
                        cmdUpdateHistory.Parameters.Add("@EmpFullName", strEmpName)
                        cmdUpdateHistory.Parameters.Add("@UpdateTime", ModifiedDate)
                        conUpdateHistory.Open()
                        cmdUpdateHistory.ExecuteNonQuery()
                        conUpdateHistory.Close()
                        cmdUpdateHistory.Dispose()
                        'lblError.Text = "Transfer Number " & strTransNum & " has been recorded."
                        lblError.Text = "Transfer Number " & strTransNum & " has been processed."
                        lblError.Visible = True
                    Else
                        lblError.Text = "This transfer's Gers Status must be open in order to update."
                        lblError.Visible = True

                    End If 'End of Checking of Gers Status
                Else
                    lblError.Text = "This transfer must be shipped before you can update."
                    lblError.Visible = True
                End If 'End checking of current box status
            Else
                'This is not a valid transfer number to DC
                lblError.Visible = True
                lblError.Text = "You have entered an invalid transfer number."
            End If  'end counter if stmt
        Else
            'user left textbox blank
            lblError.Visible = True
            lblError.Text = "Please enter a transfer number."
        End If 'end txt length if stmt

    End Sub
    Private Sub txtTransNum_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTransNum.TextChanged

    End Sub
End Class
