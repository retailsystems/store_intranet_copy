Imports System

Public Class IC_Search
    Inherits System.Web.UI.Page
    Protected WithEvents lstTransStatus As System.Web.UI.WebControls.ListBox
    Protected WithEvents lstTransType As System.Web.UI.WebControls.ListBox
    Protected WithEvents txtUPS As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDateF As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDateT As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTransferNum As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnClear As System.Web.UI.WebControls.Button
    Protected WithEvents btnSaveProfile As System.Web.UI.WebControls.Button
    Protected WithEvents btnSearch As System.Web.UI.WebControls.Button
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents btnUpdateProfile As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteProfile As System.Web.UI.WebControls.Button
    Protected WithEvents lblDelete As System.Web.UI.WebControls.Label
    Protected WithEvents btnDYes As System.Web.UI.WebControls.Button
    Protected WithEvents btnDNo As System.Web.UI.WebControls.Button
    Protected WithEvents ddlFRegion As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlFDistrict As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlFStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtdesamtF As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesamtT As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdiscrepF As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdiscrepT As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbDiscrepancy As System.Web.UI.WebControls.ListBox
    Protected WithEvents ddlTRegion As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlTDistrict As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlTStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlDateSearch As System.Web.UI.WebControls.DropDownList
    Protected WithEvents rblSearchDate As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents DYes As System.Web.UI.WebControls.Button
    Protected WithEvents lblProfileName As System.Web.UI.WebControls.Label
    Protected WithEvents txtProfileName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblProDescrip As System.Web.UI.WebControls.Label
    Protected WithEvents txtProDescrip As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents chkTInternetStore As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkFInternetStore As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lstGERS As System.Web.UI.WebControls.ListBox
    Protected WithEvents ucHeader As Header
    Public parmProID As SqlClient.SqlParameter
    Public ProfileName As String
    Public ProfileDescrip As String
    Public intRegion As Integer
    Public intDistrict As Integer
    Public intFStore As Integer
    Public intTStore As Integer
    Public intTDistrict As Integer
    Public intTRegion As Integer
    Public strUPS As String
    Public strTransferNum As String
    Public FromDate As Date
    Public ToDate As Date
    Public intDiscQtyFrom As Integer
    Public intDiscQtyTo As Integer
    Public DiscAmtFrom As Decimal
    Public DiscAmtTo As Decimal
    'Public intGersStatus As Integer
    Public strGersStatus As String
    Public strDiscepStatus As String
    Public strTransGroup As String
    Public strTransStatus As String
    Public blnContinue As Boolean
    Public Created As Date
    Public Modified As Date
    Public ProfileID As Integer
    Public strSQL As String
    Public SQL2 As String
    Private objUserInfo As New UserInfo()
    '*********************************************************************
    'These will be the variables passed to results pages to form querys.
    Public FStore As String       'From Store
    Public FDis As String         'From District
    Public FReg As String         'From Region
    Public NoFInternet As String  'From Internet Store Checkbox
    Public TStore As String       'To Store
    Public TDis As String         'To District
    Public TReg As String         'To Region
    Public NoTInternet As String  'To Internet Store Checkbox
    Public GERS As String         'GERS Process Status
    Public Discrep As String      'Discrepancy status
    Public TType As String        'Transfer Type
    Public TStatus As String      'Transfer Status
    Public TDate As String        'To Date search
    Public FDate As String        'From Date Search
    Public DFQty As String        'Discrep from qty
    Public DTQty As String        'Discrep to qty
    Public DFAmt As String        'Discrep from amt
    Public DTAmt As String        'Discrep to amt
    Public TrackNum As String     'Tracking Number (UPS)
    Public TransNum As String     'Transfer Number


    '*********************************************************************


    Public Creator As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()
        If Not objUserInfo.EmpOnline Then
            Response.Redirect("../SessionEnd.aspx?Mode=2")
        End If
        ucHeader.lblTitle = "Inventory Control Search"
        ucHeader.CurrentPage(Header.PageName.IC_Search)
        ucHeader.CurrentMode(Header.HeaderGroup.IC)
        pageBody.Attributes.Add("onload", "")
        If Not objUserInfo.EmpId Is Nothing Then
            Creator = objUserInfo.EmpFullName
        End If
        If Not IsPostBack Then
            rblSearchDate.SelectedIndex = 0
            ddlDateSearch.Enabled = False
            Dim PList As String
            PList = Request.QueryString("PROID")
            If PList = "" Then
                Call FillStore()
                Call FillRegion()
                Call FillDistrict()
                Call FillToStore()
                Call FillToDistrict()
                Call FillToRegion()
                Call FillGERS()
                Call FillTransType()
                Call FillTransStatus()
                Call FillDiscrepancy()
                lblError.Text = ""
                lstTransType.SelectedIndex = 0
                lstTransStatus.SelectedIndex = 0
                lbDiscrepancy.SelectedIndex = 0
                lstGERS.SelectedIndex = 0
            Else
                Call FillProfile()
            End If

        End If

    End Sub
    Private Sub FillToStore()
        'Fills Store combobox 
        Dim strStoreProcedure As String
        strStoreProcedure = "asFillTStore" 'order counts - keept this order
        Dim conStore As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim cmdStore As New SqlClient.SqlCommand(strStoreProcedure, conStore)
        Dim dtrStore As SqlClient.SqlDataReader

        Try
            conStore.Open()

            dtrStore = cmdStore.ExecuteReader()
            ddlTStore.DataSource = dtrStore
            ddlTStore.DataTextField = "StoreName"
            ddlTStore.DataValueField = "StoreNum"
            ddlTStore.DataBind()
        Catch e As Exception
            'redirect to the error page
        Finally
            ddlTStore.Items.Insert(0, "<ALL STORES>")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
        End Try
    End Sub
    Private Sub FillStore()
        'Fills Store combobox 
        Dim strStoreProcedure As String
        strStoreProcedure = "asFillStore" 'order counts - keept this order
        Dim conStore As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim cmdStore As New SqlClient.SqlCommand(strStoreProcedure, conStore)
        Dim dtrStore As SqlClient.SqlDataReader

        Try
            conStore.Open()

            dtrStore = cmdStore.ExecuteReader()
            ddlFStore.DataSource = dtrStore
            ddlFStore.DataTextField = "StoreName"
            ddlFStore.DataValueField = "StoreNum"
            ddlFStore.DataBind()
        Catch e As Exception
            'redirect to the error page
        Finally
            ddlFStore.Items.Insert(0, "<ALL STORES>")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
        End Try
    End Sub
    Private Sub FillToRegion()
        Dim conRegion As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim cmdRegion As New SqlClient.SqlCommand("SELECT DISTINCT Region AS Region FROM STORE WHERE ((StoreNum >= 5001 AND StoreNum <= 5900 AND Region IS NOT NULL) OR StoreNum = 5990) ORDER BY Region", conRegion)
        Dim dtrRegion As SqlClient.SqlDataReader

        Try
            conRegion.Open()

            dtrRegion = cmdRegion.ExecuteReader()
            ddlTRegion.DataSource = dtrRegion
            ddlTRegion.DataTextField = "Region"
            ddlTRegion.DataValueField = "Region"
            ddlTRegion.DataBind()
            ddlTRegion.Items.Insert(0, "<ALL REGIONS>")
        Catch e As Exception
            'redirect to the error page
        Finally
            dtrRegion.Close()
            conRegion.Close()
            cmdRegion.Dispose()
        End Try
        ddlTRegion.SelectedIndex = 0
    End Sub
    Private Sub FillRegion()
        Dim conRegion As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim cmdRegion As New SqlClient.SqlCommand("SELECT DISTINCT Region AS Region FROM STORE WHERE (StoreNum >= 5001 AND StoreNum <= 5900 AND Region IS NOT NULL) OR StoreNum=5990 ORDER BY Region", conRegion)
        Dim dtrRegion As SqlClient.SqlDataReader

        Try
            conRegion.Open()

            dtrRegion = cmdRegion.ExecuteReader()
            ddlFRegion.DataSource = dtrRegion
            ddlFRegion.DataTextField = "Region"
            ddlFRegion.DataValueField = "Region"
            ddlFRegion.DataBind()
            ddlFRegion.Items.Insert(0, "<ALL REGIONS>")
        Catch e As Exception
            'redirect to the error page
        Finally
            dtrRegion.Close()
            conRegion.Close()
            cmdRegion.Dispose()
        End Try
        ddlFRegion.SelectedIndex = 0

    End Sub
    Private Sub FillToDistrict()
        Dim conConnection As SqlClient.SqlConnection
        Dim cmdCommand As SqlClient.SqlCommand
        Dim dtrReader As SqlClient.SqlDataReader
        conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        conConnection.Open()
        cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE ((StoreNum >= 5001 AND StoreNum < 5900 AND District IS NOT NULL) OR StoreNum=5990) ", conConnection)
        dtrReader = cmdCommand.ExecuteReader()
        ddlTDistrict.DataSource = dtrReader
        ddlTDistrict.DataTextField = "District"
        ddlTDistrict.DataValueField = "District"
        ddlTDistrict.DataBind()
        ddlTDistrict.Items.Insert(0, "<ALL DISTRICTS>")

        dtrReader.Close()
        conConnection.Close()
        cmdCommand.Dispose()
    End Sub
    Private Sub FillDistrict()
        Dim conConnection As SqlClient.SqlConnection
        Dim cmdCommand As SqlClient.SqlCommand
        Dim dtrReader As SqlClient.SqlDataReader
        conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        conConnection.Open()
        cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE (StoreNum >= 5001 AND StoreNum < 5900 AND District IS NOT NULL) OR StoreNum=5990 ", conConnection)
        dtrReader = cmdCommand.ExecuteReader()
        ddlFDistrict.DataSource = dtrReader
        ddlFDistrict.DataTextField = "District"
        ddlFDistrict.DataValueField = "District"
        ddlFDistrict.DataBind()
        ddlFDistrict.Items.Insert(0, "<ALL DISTRICTS>")

        dtrReader.Close()
        conConnection.Close()
        cmdCommand.Dispose()
    End Sub
    Private Sub FillGERS()
        Dim conConnection As SqlClient.SqlConnection
        Dim cmdCommand As SqlClient.SqlCommand
        Dim dtrReader As SqlClient.SqlDataReader
        conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        conConnection.Open()
        cmdCommand = New SqlClient.SqlCommand("SELECT GERSStatus_Desc,GERSStatus_Cd FROM GERSStatus_Type WHERE Active=1 ", conConnection)
        dtrReader = cmdCommand.ExecuteReader()
        lstGERS.DataSource = dtrReader
        lstGERS.DataTextField = "GERSStatus_Desc"
        lstGERS.DataValueField = "GERSStatus_Cd"
        lstGERS.DataBind()
        lstGERS.Items.Insert(0, "<ALL GERS STATUSES>")

        dtrReader.Close()
        conConnection.Close()
        cmdCommand.Dispose()
    End Sub
    Private Sub FillTransType()
        Dim conConnection As SqlClient.SqlConnection
        Dim cmdCommand As SqlClient.SqlCommand
        Dim dtrReader As SqlClient.SqlDataReader
        conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        conConnection.Open()
        cmdCommand = New SqlClient.SqlCommand("SELECT Xfer_Desc,Xfer_Cd FROM Xfer_Type WHERE Active=1 ", conConnection)
        dtrReader = cmdCommand.ExecuteReader()
        lstTransType.DataSource = dtrReader
        lstTransType.DataTextField = "Xfer_Desc"
        lstTransType.DataValueField = "Xfer_Cd"
        lstTransType.DataBind()
        lstTransType.Items.Insert(0, "<ALL TRANSFER TYPES>")

        dtrReader.Close()
        conConnection.Close()
        cmdCommand.Dispose()
    End Sub
    Private Sub FillTransStatus()
        Dim conConnection As SqlClient.SqlConnection
        Dim cmdCommand As SqlClient.SqlCommand
        Dim dtrReader As SqlClient.SqlDataReader
        conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        conConnection.Open()
        cmdCommand = New SqlClient.SqlCommand("SELECT Status_Desc,Status_Cd FROM Box_Status_Type WHERE Active=1 ", conConnection)
        dtrReader = cmdCommand.ExecuteReader()
        lstTransStatus.DataSource = dtrReader
        lstTransStatus.DataTextField = "Status_Desc"
        lstTransStatus.DataValueField = "Status_Cd"
        lstTransStatus.DataBind()
        lstTransStatus.Items.Insert(0, "<ALL TRANSFER STATUSES>")

        dtrReader.Close()
        conConnection.Close()
        cmdCommand.Dispose()
    End Sub
    Private Sub FillDiscrepancy()
        Dim conConnection As SqlClient.SqlConnection
        Dim cmdCommand As SqlClient.SqlCommand
        Dim dtrReader As SqlClient.SqlDataReader
        conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        conConnection.Open()
        cmdCommand = New SqlClient.SqlCommand("SELECT Disc_Cd,Disc_Desc,Disc_LongDesc FROM Discrep_Type WHERE Active=1 ", conConnection)
        dtrReader = cmdCommand.ExecuteReader()
        lbDiscrepancy.DataSource = dtrReader
        'lbDiscrepancy.DataTextField = "Disc_Desc"
        lbDiscrepancy.DataTextField = "Disc_LongDesc"
        lbDiscrepancy.DataValueField = "Disc_Cd"
        lbDiscrepancy.DataBind()
        lbDiscrepancy.Items.Insert(0, "<ALL DISCREPANCY TYPES>")

        dtrReader.Close()
        conConnection.Close()
        cmdCommand.Dispose()
    End Sub
    Private Sub ddlFRegion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlFRegion.SelectedIndexChanged
        If ddlFRegion.SelectedIndex <> 0 Then
            'fill listbox with districts in the region selected
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE Region='" + ddlFRegion.SelectedItem.Value + "' ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlFDistrict.DataSource = dtrReader
            ddlFDistrict.DataTextField = "District"
            ddlFDistrict.DataValueField = "District"
            ddlFDistrict.DataBind()
            ddlFDistrict.Items.Insert(0, "<ALL DISTRICTS IN REGION " + ddlFRegion.SelectedItem.Value + ">")
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()

            'fill appropriate stores to districts in Region chosen.
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE Region='" + ddlFRegion.SelectedItem.Value + "' AND ((StoreNum >=5000 AND StoreNum <=5900) OR StoreNum=5990) ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlFStore.DataSource = dtrStore
            ddlFStore.DataTextField = "StoreName"
            ddlFStore.DataValueField = "StoreNum"
            ddlFStore.DataBind()
            ddlFStore.Items.Insert(0, "<ALL STORES IN REGION " + ddlFRegion.SelectedItem.Value + ">")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
        ElseIf ddlFRegion.SelectedIndex = 0 Then
            'fill listbox with all districts
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE StoreNum >=5000 AND StoreNum <=5900 AND District IS NOT NULL ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlFDistrict.DataSource = dtrReader
            ddlFDistrict.DataTextField = "District"
            ddlFDistrict.DataValueField = "District"
            ddlFDistrict.DataBind()
            ddlFDistrict.Items.Insert(0, "<ALL DISTRICTS>")
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()

            'fill with all stores 
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE StoreNum >=5000 AND StoreNum <=5900 ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlFStore.DataSource = dtrStore
            ddlFStore.DataTextField = "StoreName"
            ddlFStore.DataValueField = "StoreNum"
            ddlFStore.DataBind()
            ddlFStore.Items.Insert(0, "<ALL STORES>")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
        End If
    End Sub
    Private Sub ddlFDistrict_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlFDistrict.SelectedIndexChanged

        If ddlFDistrict.SelectedIndex <> 0 Then
            'fill appropriate stores to district chosen.
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE District='" + ddlFDistrict.SelectedItem.Value + "' AND ((StoreNum >=5000 AND StoreNum <=5900) OR StoreNum=5990) ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlFStore.DataSource = dtrStore
            ddlFStore.DataTextField = "StoreName"
            ddlFStore.DataValueField = "StoreNum"
            ddlFStore.DataBind()
            ddlFStore.Items.Insert(0, "<ALL STORES IN DISTRICT " + ddlFDistrict.SelectedItem.Value + ">")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
            'if user selects a district, make sure proper region selection shows
            Dim conRegion As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            Dim cmdRegion As New SqlClient.SqlCommand("SELECT DISTINCT Region AS Region FROM STORE WHERE District='" + ddlFDistrict.SelectedItem.Value + "' ORDER BY Region", conRegion)
            Dim dtrRegion As SqlClient.SqlDataReader
            conRegion.Open()
            dtrRegion = cmdRegion.ExecuteReader()
            ddlFRegion.DataSource = dtrRegion
            ddlFRegion.DataTextField = "Region"
            ddlFRegion.DataValueField = "Region"
            ddlFRegion.DataBind()
            ddlFRegion.Items.Insert(0, "<ALL REGIONS>")
            dtrRegion.Close()
            conRegion.Close()
            ddlFRegion.SelectedIndex = 1
            cmdRegion.Dispose()
            '*********************************************
            'testing fix
            ddlFDistrict.Items.RemoveAt(0)
            ddlFDistrict.Items.Insert(0, "<ALL DISTRICTS>")
            '*********************************************
        ElseIf ddlFDistrict.SelectedIndex = 0 Then
            'Fill in all regions, user selected all districts
            Dim conRegion As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            Dim cmdRegion As New SqlClient.SqlCommand("SELECT DISTINCT Region AS Region FROM STORE WHERE StoreNum >= 5001 AND StoreNum <= 5900 AND Region IS NOT NULL ORDER BY Region", conRegion)
            Dim dtrRegion As SqlClient.SqlDataReader
            conRegion.Open()
            dtrRegion = cmdRegion.ExecuteReader()
            ddlFRegion.DataSource = dtrRegion
            ddlFRegion.DataTextField = "Region"
            ddlFRegion.DataValueField = "Region"
            ddlFRegion.DataBind()
            ddlFRegion.Items.Insert(0, "<ALL REGIONS>")
            dtrRegion.Close()
            conRegion.Close()
            cmdRegion.Dispose()
            ddlFRegion.SelectedIndex = 0

            'fill with all stores 
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE StoreNum >=5000 AND StoreNum <=5900 ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlFStore.DataSource = dtrStore
            ddlFStore.DataTextField = "StoreName"
            ddlFStore.DataValueField = "StoreNum"
            ddlFStore.DataBind()
            ddlFStore.Items.Insert(0, "<ALL STORES>")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
        End If
    End Sub
    Private Sub ddlFStore_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlFStore.SelectedIndexChanged
        If ddlFStore.SelectedIndex <> 0 Then
            'fill appropriate region
            Dim conRegion As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            Dim cmdRegion As New SqlClient.SqlCommand("SELECT DISTINCT Region AS Region FROM STORE WHERE StoreNum='" + ddlFStore.SelectedItem.Value + "' ORDER BY Region", conRegion)
            Dim dtrRegion As SqlClient.SqlDataReader
            conRegion.Open()
            dtrRegion = cmdRegion.ExecuteReader()
            ddlFRegion.DataSource = dtrRegion
            ddlFRegion.DataTextField = "Region"
            ddlFRegion.DataValueField = "Region"
            ddlFRegion.DataBind()
            dtrRegion.Close()
            conRegion.Close()
            cmdRegion.Dispose()
            'fill listbox with districts
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE StoreNum='" + ddlFStore.SelectedItem.Value + "' ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlFDistrict.DataSource = dtrReader
            ddlFDistrict.DataTextField = "District"
            ddlFDistrict.DataValueField = "District"
            ddlFDistrict.DataBind()
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()
            '*********************************************
            'testing fix
            ddlFStore.Items.RemoveAt(0)
            ddlFStore.Items.Insert(0, "<ALL STORES>")
            '*********************************************
        ElseIf ddlFStore.SelectedIndex = 0 Then
            'Fill in all regions
            Dim conRegion As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            Dim cmdRegion As New SqlClient.SqlCommand("SELECT DISTINCT Region AS Region FROM STORE WHERE StoreNum >= 5001 AND StoreNum <= 5900 AND Region IS NOT NULL ORDER BY Region", conRegion)
            Dim dtrRegion As SqlClient.SqlDataReader
            conRegion.Open()
            dtrRegion = cmdRegion.ExecuteReader()
            ddlFRegion.DataSource = dtrRegion
            ddlFRegion.DataTextField = "Region"
            ddlFRegion.DataValueField = "Region"
            ddlFRegion.DataBind()
            ddlFRegion.Items.Insert(0, "<ALL REGIONS>")
            dtrRegion.Close()
            conRegion.Close()
            cmdRegion.Dispose()
            ddlFRegion.SelectedIndex = 0

            'fill listbox with all districts
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE StoreNum >=5000 AND StoreNum <=5900 AND District IS NOT NULL ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlFDistrict.DataSource = dtrReader
            ddlFDistrict.DataTextField = "District"
            ddlFDistrict.DataValueField = "District"
            ddlFDistrict.DataBind()
            ddlFDistrict.Items.Insert(0, "<ALL DISTRICTS>")
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()
        End If
    End Sub
    Private Sub ValidateInput()
        'Validate all user input boxes.
        lblError.Text = ""
        Dim strUPS
        strUPS = Trim(txtUPS.Text)
        'If (txtUPS.Text <> "" And strUPS.Length < 18) Then
        'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("UPS# needs to be 18 characters long.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
        'lblError.Text = "UPS# needs to be 18 characters long."
        'End If

        If (txtdiscrepF.Text <> "") Then
            Dim test As Boolean
            test = IsNumeric(txtdiscrepF.Text)
            If test = False Then
                'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("There should be no characters in the discrepancy quantity fields.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                lblError.Text = "There should be no characters in the discrepancy quantity fields."
            End If
        End If

        If (txtdiscrepT.Text <> "") Then
            Dim test As Boolean
            test = IsNumeric(txtdiscrepT.Text)
            If test = False Then
                'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("There should be no characters in the discrepancy quantity fields.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                lblError.Text = "There should be no characters in the discrepancy quantity fields."
            End If
        End If

        If (txtdesamtF.Text <> "") Then
            Dim test As Boolean
            test = IsNumeric(txtdesamtF.Text)
            If test = False Then
                'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("There should be no characters in the discrepancy quantity fields.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                lblError.Text = "There should be no characters in the discrepancy quantity fields."
            End If
        End If

        If (txtdesamtT.Text <> "") Then
            Dim test As Boolean
            test = IsNumeric(txtdesamtT.Text)
            If test = False Then
                'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("There should be no characters in the discrepancy quantity fields.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                lblError.Text = "There should be no characters in the discrepancy quantity fields."
            End If
        End If

        If (txtDateF.Text <> "") Then
            Dim Test As Boolean
            test = IsDate(txtDateF.Text)
            If test = False Then
                'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("You entered an invalid date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                lblError.Text = "You entered an invalid date."
            End If
        End If

        If (txtDateT.Text <> "") Then
            Dim Test As Boolean
            test = IsDate(txtDateT.Text)
            If test = False Then
                'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("You entered an invalid date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                lblError.Text = "You entered an invalid date."
            End If
        End If


    End Sub
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Call FillRegion()
        Call FillDistrict()
        Call FillStore()
        Call FillToRegion()
        Call FillToDistrict()
        Call FillToStore()
        Call FillGERS()
        Call FillTransType()
        Call FillTransStatus()
        Call FillDiscrepancy()

        ddlFRegion.SelectedIndex = -1
        ddlFDistrict.SelectedIndex = -1
        ddlFStore.SelectedIndex = -1
        ddlTRegion.SelectedIndex = -1
        ddlTDistrict.SelectedIndex = -1
        ddlTStore.SelectedIndex = -1
        ddlDateSearch.SelectedIndex = -1
        lbDiscrepancy.SelectedIndex = 0
        lstTransType.SelectedIndex = 0
        lstTransStatus.SelectedIndex = 0
        lstGERS.SelectedIndex = 0
        rblSearchDate.SelectedItem.Value = "Specified Date Range"
        txtdesamtF.Text = ""
        txtdesamtT.Text = ""
        txtdiscrepF.Text = ""
        txtdiscrepT.Text = ""
        txtUPS.Text = ""
        txtTransferNum.Text = ""
        txtDateF.Text = ""
        txtDateT.Text = ""
        lblError.Text = ""
        lblDelete.Visible = False
        btnDYes.Visible = False
        btnDNo.Visible = False
        lblProfileName.Visible = False
        txtProfileName.Text = ""
        txtProfileName.Visible = False
        lblProDescrip.Visible = False
        txtProDescrip.Text = ""
        txtProDescrip.Visible = False
        btnCancel.Visible = False
        btnSave.Visible = False
        btnSaveProfile.Visible = True
        btnUpdateProfile.Visible = False
        btnDeleteProfile.Visible = False
        chkFInternetStore.Checked = False
        chkTInternetStore.Checked = False

    End Sub
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblError.Text = ""
        Call ValidateInput()
        Call GetInfo()
        'Response.Redirect("IC_Results.aspx?SQL2=" & System.Web.HttpUtility.UrlEncode(strSQL))
        If blnContinue = True Then
        Response.Redirect("IC_Results.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum)
        End If
    End Sub
    Private Sub btnSaveProfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveProfile.Click
        lblError.Text = ""
        btnSave.Visible = True
        btnCancel.Visible = True
        lblProfileName.Visible = True
        txtProfileName.Visible = True
        lblProDescrip.Visible = True
        txtProDescrip.Visible = True
        btnSaveProfile.Visible = False
        '****************************************
        'going in btnSave code
        'Call ValidateInput()
        'Call GetInfo()
        'If blnContinue = True Then
        '    'execute stored procedure to insert new profile

        '    Created = Now        'gets current date and time
        '    Modified = Created
        '    '****************************************************************
        '    'Get Search Date Criteria for Profile
        '    Dim DateSearchType
        '    Dim DateRange
        '    DateSearchType = rblSearchDate.SelectedItem.Value

        '    If DateSearchType = "Selected Date Range" Then
        '        DateRange = ddlDateSearch.SelectedItem.Value
        '        ToDate = "#1/1/1979#"
        '        FromDate = "#1/1/1979#"

        '    ElseIf DateSearchType = "Specified Date Range" Then
        '        DateRange = "<Select A Range>"
        '        If txtDateT.Text = "" Then
        '            ToDate = Now.ToShortDateString
        '        End If

        '        If txtDateF.Text = "" Then
        '            FromDate = ((Now.AddMonths(-12)).ToShortDateString)   'Need to change
        '        End If
        '    End If


        '    '***********************************************************************
        '    Dim conInsertProfile As System.Data.SqlClient.SqlConnection
        '    Dim cmdInsertProfile As System.Data.SqlClient.SqlCommand
        '    conInsertProfile = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        '    cmdInsertProfile = New SqlClient.SqlCommand("spNew_Profile_Insert", conInsertProfile)
        '    cmdInsertProfile.CommandType = CommandType.StoredProcedure
        '    cmdInsertProfile.Parameters.Add("@DateSearchType", DateSearchType)
        '    cmdInsertProfile.Parameters.Add("@DateRange", DateRange)
        '    cmdInsertProfile.Parameters.Add("@shipdateF", FromDate)
        '    cmdInsertProfile.Parameters.Add("@shipdateT", ToDate)
        '    cmdInsertProfile.Parameters.Add("@FRegion", intRegion)
        '    cmdInsertProfile.Parameters.Add("@FDistrict", intDistrict)
        '    cmdInsertProfile.Parameters.Add("@FStore", intFStore)
        '    cmdInsertProfile.Parameters.Add("@TRegion", intTRegion)
        '    cmdInsertProfile.Parameters.Add("@TDistrict", intTDistrict)
        '    cmdInsertProfile.Parameters.Add("@TStore", intTStore)
        '    cmdInsertProfile.Parameters.Add("@TransNum", strTransferNum)
        '    cmdInsertProfile.Parameters.Add("@UPS", strUPS)
        '    cmdInsertProfile.Parameters.Add("@TransferGroup", strTransGroup)
        '    cmdInsertProfile.Parameters.Add("@TransferStatus", strTransStatus)
        '    cmdInsertProfile.Parameters.Add("@DiscrepQtyF", intDiscQtyFrom)
        '    cmdInsertProfile.Parameters.Add("@DiscrepQtyT", intDiscQtyTo)
        '    cmdInsertProfile.Parameters.Add("@DiscrepamtF", DiscAmtFrom)
        '    cmdInsertProfile.Parameters.Add("@DiscrepamtT", DiscAmtTo)
        '    cmdInsertProfile.Parameters.Add("@DiscrepStatus", strDiscepStatus)
        '    cmdInsertProfile.Parameters.Add("@Gers", intGersStatus)
        '    cmdInsertProfile.Parameters.Add("@Created", Created)
        '    cmdInsertProfile.Parameters.Add("@Modified", Modified)
        '    cmdInsertProfile.Parameters.Add("@Creator", Creator)

        '    parmProID = cmdInsertProfile.Parameters.Add("@PID", SqlDbType.Int)
        '    parmProID.Size = 20
        '    parmProID.Direction = ParameterDirection.Output

        '    conInsertProfile.Open()
        '    cmdInsertProfile.ExecuteNonQuery()
        '    conInsertProfile.Close()
        '    ProfileID = cmdInsertProfile.Parameters("@PID").Value   'getting user profile id number

        'Response.Redirect("IC_NameProfile.aspx?PID=" & System.Web.HttpUtility.UrlEncode(ProfileID))
        'End If
    End Sub
    Private Sub GetInfo()
        'get user selections to either search or save profile.
        If lblError.Text = "" Then
            blnContinue = True
            If ddlFRegion.SelectedIndex <> -1 Then
                If ddlFRegion.SelectedIndex = 0 Then
                    intRegion = ddlFRegion.SelectedIndex   'search all regions
                ElseIf ddlFRegion.SelectedIndex > 0 Then
                    intRegion = ddlFRegion.SelectedItem.Value
                End If
            End If

            If ddlFDistrict.SelectedIndex <> -1 Then
                If ddlFDistrict.SelectedIndex = 0 Then
                    intDistrict = ddlFDistrict.SelectedIndex  'search all districts or all districts in region
                ElseIf ddlFDistrict.SelectedIndex > 0 Then
                    intDistrict = ddlFDistrict.SelectedItem.Value
                End If
            End If

            If ddlFStore.SelectedIndex <> -1 Then
                If ddlFStore.SelectedIndex = 0 Then
                    intFStore = ddlFStore.SelectedIndex    'search all stores or all stores in a specific area.
                ElseIf ddlFStore.SelectedIndex > 0 Then
                    intFStore = ddlFStore.SelectedItem.Value
                End If
            End If

            'Dim intindexvalue
            'intindexvalue = ddlFStore.SelectedItem.Value

            If (ddlFStore.SelectedItem.Value = "<ALL STORES>") Then
                'Nothing selected so search ALL STORES
                'strSQL = strSQL & "AND ((Box_Xfer_Hdr.Sending_Store_Cd >= 5001 AND Box_Xfer_Hdr.Sending_Store_Cd <= 5900) OR (Box_Xfer_Hdr.Sending_Store_Cd = 5990)) "
                FStore = "ALL"
            ElseIf (ddlFStore.SelectedItem.Value = "<ALL STORES IN DISTRICT " + ddlFDistrict.SelectedItem.Value + ">") Then
                'strSQL = strSQL & "AND Box_Xfer_Hdr.Sending_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE District = '" + ddlFDistrict.SelectedItem.Value + "') "
                FDis = ddlFDistrict.SelectedItem.Value
            ElseIf (ddlFStore.SelectedItem.Value = "<ALL STORES IN REGION " + ddlFRegion.SelectedItem.Value + ">") Then
                'strSQL = strSQL & "AND Box_Xfer_Hdr.Sending_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE Region = " + ddlFRegion.SelectedItem.Value + ") "
                FReg = ddlFRegion.SelectedItem.Value
            Else
                'strSQL = strSQL & "AND Box_Xfer_Hdr.Sending_Store_Cd=" + ddlFStore.SelectedItem.Value + " "
                FStore = ddlFStore.SelectedItem.Value
            End If

            If chkFInternetStore.Checked = True Then
                'strSQL = strSQL & "AND Box_Xfer_Hdr.Sending_Store_Cd <> 5990 "
                NoFInternet = "checked"    'exclude internet store
            Else
                NoFInternet = "unchecked"  'include internet store
            End If



            '**************************************************************************************
            If ddlTRegion.SelectedIndex <> -1 Then
                If ddlTRegion.SelectedIndex = 0 Then
                    intTRegion = ddlTRegion.SelectedIndex   'search all regions
                ElseIf ddlTRegion.SelectedIndex > 0 Then
                    intTRegion = ddlTRegion.SelectedItem.Value
                End If
            End If

            If ddlTDistrict.SelectedIndex <> -1 Then
                If ddlTDistrict.SelectedIndex = 0 Then
                    intTDistrict = ddlTDistrict.SelectedIndex  'search all districts or all districts in region
                ElseIf ddlTDistrict.SelectedIndex > 0 Then
                    intTDistrict = ddlTDistrict.SelectedItem.Value
                End If
            End If

            If ddlTStore.SelectedIndex <> -1 Then
                If ddlTStore.SelectedIndex = 0 Then
                    intTStore = ddlTStore.SelectedIndex    'search all stores or all stores in a specific area.
                ElseIf ddlTStore.SelectedIndex > 0 Then
                    intTStore = ddlTStore.SelectedItem.Value
                End If
            End If


            If (ddlTStore.SelectedItem.Value = "<ALL STORES>") Then
                'Nothing selected so search ALL STORES
                'strSQL = strSQL & "AND ((Box_Xfer_Hdr.Receiving_Store_Cd >= 5001 AND Box_Xfer_Hdr.Receiving_Store_Cd <= 5900) OR Box_Xfer_Hdr.Receiving_Store_Cd = 9999) "
                TStore = "ALL"
            ElseIf (ddlTStore.SelectedItem.Value = "<ALL STORES IN DISTRICT " + ddlTDistrict.SelectedItem.Value + ">") Then
                'strSQL = strSQL & "AND Box_Xfer_Hdr.Receiving_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE District = " + ddlTDistrict.SelectedItem.Value + ") "
                TDis = ddlTDistrict.SelectedItem.Value
            ElseIf (ddlTStore.SelectedItem.Value = "<ALL STORES IN REGION " + ddlTRegion.SelectedItem.Value + ">") Then
                TReg = ddlTRegion.SelectedItem.Value
                'strSQL = strSQL & "AND Box_Xfer_Hdr.Receiving_Store_Cd IN (Select StoreNum FROM Torrid.dbo.STORE WHERE Region = " + ddlTRegion.SelectedItem.Value + ") "
            Else
                TStore = ddlTStore.SelectedItem.Value
                'strSQL = strSQL & "AND Box_Xfer_Hdr.Receiving_Store_Cd=" + ddlTStore.SelectedItem.Value + " "
            End If

            If chkTInternetStore.Checked = True Then
                'strSQL = strSQL & "AND Box_Xfer_Hdr.Receiving_Store_Cd <> 5990 "
                NoTInternet = "checked"    'exclude internet store
            Else
                NoTInternet = "unchecked"  'include internet store
            End If
            '**************************************************************************************
            'intGersStatus = lstGERS.SelectedIndex
            'If lstGERS.SelectedIndex > 0 Then
            '    Dim GStatus
            '    GStatus = lstGERS.SelectedItem.Value
            '    'strSQL = strSQL & "AND (IC_Box_Trans_Status.GERSStatus_Cd=" & (GStatus) & ") "
            '    GERS = GStatus
            'End If
            '************************************************************************************

            If lbDiscrepancy.SelectedIndex <> -1 Then
                Dim intDType As Integer
                Dim intDTypeX As String
                Dim AllDiscrep As Boolean
                'Dim DiscrepType As String  'sent into SQL statement for Discrepancy Type selections
                Dim x As Integer 'Counter
                x = 0           'initialize to zero
                Dim li As ListItem


                For Each li In lbDiscrepancy.Items
                    If li.Selected = True Then
                        x = x + 1   'if x=1 just do intDType=li.value else, concatonate()
                    End If
                Next

                If x = 1 Then   'Only one selection by user
                    For Each li In lbDiscrepancy.Items
                        If li.Selected = True Then
                            If li.Value = "<ALL DISCREPANCY TYPES>" Then
                                intDType = 0
                                AllDiscrep = True
                            Else
                                AllDiscrep = False
                                intDType = li.Value
                            End If
                        End If
                    Next
                Else            'Multiple selections by user
                    For Each li In lbDiscrepancy.Items
                        If li.Selected = True Then
                            If li.Value = "<ALL DISCREPANCY TYPES>" Then
                                AllDiscrep = True
                                intDType = 0
                            Else
                                intDType = li.Value
                                intDTypeX = intDType & "," & intDTypeX   'separate selections with commas
                            End If

                        End If
                    Next
                End If

                If x > 1 Then   'take off ending comma at end of selections
                    Dim Lposition As Integer
                    Dim position As Integer
                    Lposition = (Len(intDTypeX)) - 1
                    position = Len(intDTypeX)
                    strDiscepStatus = Left$(intDTypeX, Lposition) & Replace(intDTypeX, ",", "", position)
                Else
                    strDiscepStatus = intDType   'single selection, so no string manipulation
                End If

                'send in selections to SQL statement
                If AllDiscrep <> True Then
                    'strSQL = strSQL & "AND (IC_Box_Trans_Status.Disc_Cd IN (" & (strDiscepStatus) & ")) "
                    Discrep = strDiscepStatus
                Else
                    strDiscepStatus = "0"
                End If
            ElseIf lbDiscrepancy.SelectedIndex = -1 Then
                strDiscepStatus = ""
            End If
            '******************************************************************************************
            If lstTransType.SelectedIndex <> -1 Then
                Dim intTType As Integer
                Dim intTTypeX As String
                Dim ALLTTypes As Boolean

                Dim x As Integer 'Counter
                x = 0           'initialize to zero
                Dim li As ListItem


                For Each li In lstTransType.Items
                    If li.Selected = True Then
                        x = x + 1   'if x=1 just do intDType=li.value else, concatonate()
                    End If
                Next

                If x = 1 Then   'Only one selection by user
                    For Each li In lstTransType.Items
                        If li.Selected = True Then
                            If li.Value = "<ALL TRANSFER TYPES>" Then
                                intTType = 0
                                ALLTTypes = True
                            Else
                                intTType = li.Value
                                ALLTTypes = False
                            End If
                        End If
                    Next
                Else            'Multiple selections by user
                    For Each li In lstTransType.Items
                        If li.Selected = True Then
                            If li.Value = "<ALL TRANSFER TYPES>" Then
                                intTType = 0
                                ALLTTypes = True
                            Else
                                intTType = li.Value
                                intTTypeX = intTType & "," & intTTypeX   'separate selections with commas
                            End If
                        End If
                    Next
                End If

                If x > 1 Then   'take off ending comma at end of selections
                    Dim Lposition As Integer
                    Dim position As Integer
                    Lposition = (Len(intTTypeX)) - 1
                    position = Len(intTTypeX)
                    strTransGroup = Left$(intTTypeX, Lposition) & Replace(intTTypeX, ",", "", position)
                Else
                    strTransGroup = intTType   'single selection, so no string manipulation
                End If
                'send in selections to SQL statement
                If ALLTTypes = True Then
                    strTransGroup = "0"
                Else
                    'strSQL = strSQL & "AND (IC_Box_Trans_Status.Xfer_Type_Cd IN (" & (strTransGroup) & ")) "
                    TType = strTransGroup
                End If
            ElseIf lstTransType.SelectedIndex = -1 Then
                strTransGroup = ""
            End If
            '******************************************************************************************
            If lstGERS.SelectedIndex <> -1 Then
                Dim intGType As Integer
                Dim intGTypeX As String
                Dim AllGStatus As Boolean

                Dim x As Integer 'Counter
                x = 0           'initialize to zero
                Dim li As ListItem


                For Each li In lstGERS.Items
                    If li.Selected = True Then
                        x = x + 1   'if x=1 just do intDType=li.value else, concatonate()
                    End If
                Next

                If x = 1 Then   'Only one selection by user
                    For Each li In lstGERS.Items
                        If li.Selected = True Then
                            If li.Value = "<ALL GERS STATUSES>" Then
                                AllGStatus = True
                                intGType = 0
                            Else
                                intGType = li.Value
                                AllGStatus = False
                            End If

                        End If
                    Next
                Else            'Multiple selections by user
                    For Each li In lstGERS.Items
                        If li.Selected = True Then
                            If li.Value = "<ALL GERS STATUSES>" Then
                                intGType = 0
                                AllGStatus = True
                            Else
                                intGType = li.Value
                                intGTypeX = intGType & "," & intGTypeX   'separate selections with commas
                            End If
                        End If
                    Next
                End If

                If x > 1 Then   'take off ending comma at end of selections
                    Dim Lposition As Integer
                    Dim position As Integer
                    Lposition = (Len(intGTypeX)) - 1
                    position = Len(intGTypeX)
                    strGersStatus = Left$(intGTypeX, Lposition) & Replace(intGTypeX, ",", "", position)

                Else
                    strGersStatus = intGType   'single selection, so no string manipulation
                End If
                'send in selections to SQL statement
                If AllGStatus = False Then
                    'strSQL = strSQL & "AND (Box_Xfer_Hdr.Current_Box_Status_Cd IN (" & (strTransStatus) & ")) "
                    GERS = strGersStatus
                Else
                    strGersStatus = "0"
                End If
            ElseIf lstGERS.SelectedIndex = -1 Then
                strGersStatus = ""
            End If
            '******************************************************************************************
            If lstTransStatus.SelectedIndex <> -1 Then
                Dim intSType As Integer
                Dim intSTypeX As String
                Dim AllTStatus As Boolean
                'Dim DiscrepType As String  'sent into SQL statement for Transfer Type selections
                Dim x As Integer 'Counter
                x = 0           'initialize to zero
                Dim li As ListItem


                For Each li In lstTransStatus.Items
                    If li.Selected = True Then
                        x = x + 1   'if x=1 just do intDType=li.value else, concatonate()
                    End If
                Next

                If x = 1 Then   'Only one selection by user
                    For Each li In lstTransStatus.Items
                        If li.Selected = True Then
                            If li.Value = "<ALL TRANSFER STATUSES>" Then
                                AllTStatus = True
                                intSType = 0
                            Else
                                intSType = li.Value
                                AllTStatus = False
                            End If

                        End If
                    Next
                Else            'Multiple selections by user
                    For Each li In lstTransStatus.Items
                        If li.Selected = True Then
                            If li.Value = "<ALL TRANSFER STATUSES>" Then
                                intSType = 0
                                AllTStatus = True
                            Else
                                intSType = li.Value
                                intSTypeX = intSType & "," & intSTypeX   'separate selections with commas
                            End If
                        End If
                    Next
                End If

                If x > 1 Then   'take off ending comma at end of selections
                    Dim Lposition As Integer
                    Dim position As Integer
                    Lposition = (Len(intSTypeX)) - 1
                    position = Len(intSTypeX)
                    strTransStatus = Left$(intSTypeX, Lposition) & Replace(intSTypeX, ",", "", position)
                Else
                    strTransStatus = intSType   'single selection, so no string manipulation
                End If
                'send in selections to SQL statement
                If AllTStatus = False Then
                    'strSQL = strSQL & "AND (Box_Xfer_Hdr.Current_Box_Status_Cd IN (" & (strTransStatus) & ")) "
                    TStatus = strTransStatus
                Else
                    strTransStatus = "0"
                End If
            ElseIf lstTransStatus.SelectedIndex = -1 Then
                strTransStatus = ""
            End If
            '******************************************************************************************
            If rblSearchDate.SelectedItem.Value = "Specified Date Range" Then
                'Get date range
                If txtDateF.Text <> "" Then
                    FromDate = txtDateF.Text
                    'FromDate = CDate(ToDate)
                End If

                If txtDateT.Text <> "" Then
                    Dim strToDate As String
                    strToDate = txtDateT.Text & " 11:59:59 PM"
                    'ToDate = txtDateT.Text
                    ToDate = CDate(strToDate)

                End If
                If (txtDateF.Text <> "" And txtDateT.Text <> "") Then
                    'strSQL = strSQL & "AND (Datediff(d,'" & FromDate & "',Box_Xfer_Hdr.Shipment_Date)>=0 AND Datediff(d,'" & ToDate & "',Box_Xfer_Hdr.Shipment_Date)<=0) "
                    strSQL = strSQL & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & ToDate & "' ) "
                    'strSQL = strSQL & "AND ((CONVERT (char(10),Box_Xfer_Hdr.Shipment_Date,101)) >='" & FromDate & "' AND (CONVERT (char(10),Box_Xfer_Hdr.Shipment_Date,101)) <='" & ToDate & "' ) "
                ElseIf (txtDateF.Text <> "" And txtDateT.Text = "") Then
                    'ToDate = Now.ToShortDateString
                    ToDate = (Now.ToShortDateString) & " 11:59:59 PM"
                    'strSQL = strSQL & "AND (Datediff(d,'" & FromDate & "',Box_Xfer_Hdr.Shipment_Date)>=0 AND Datediff(d,'" & ToDate & "',Box_Xfer_Hdr.Shipment_Date)<=0) "
                    strSQL = strSQL & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & ToDate & "') "
                    'strSQL = strSQL & "AND ((CONVERT (char(10),Box_Xfer_Hdr.Shipment_Date,101)) >='" & FromDate & "' AND (CONVERT (char(10),Box_Xfer_Hdr.Shipment_Date,101)) <='" & ToDate & "') "

                ElseIf (txtDateF.Text = "" And txtDateT.Text <> "") Then
                    FromDate = ToDate.AddMonths(-12)  'Now minus 6 months
                    'strSQL = strSQL & "AND (Datediff(d,'" & FromDate & "',Box_Xfer_Hdr.Shipment_Date)>=0 AND Datediff(d,'" & ToDate & "',Box_Xfer_Hdr.Shipment_Date)<=0) "
                    strSQL = strSQL & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & ToDate & "') "
                    'strSQL = strSQL & "AND ((CONVERT (char(10),Box_Xfer_Hdr.Shipment_Date,101)) >='" & FromDate & "' AND (CONVERT (char(10),Box_Xfer_Hdr.Shipment_Date,101)) <='" & ToDate & "') "
                End If
            ElseIf rblSearchDate.SelectedItem.Value = "Selected Date Range" Then
                'get how many past days and search range.
                ToDate = Now.ToShortDateString & " 11:59:59 PM"
                If ddlDateSearch.SelectedItem.Value = "Past 10 Days" Then
                    FromDate = (Now.ToShortDateString)
                    FromDate = FromDate.AddDays(-10)
                    strSQL = strSQL & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & ToDate & "' ) "
                ElseIf ddlDateSearch.SelectedItem.Value = "Past 30 Days" Then
                    FromDate = (Now.ToShortDateString)
                    FromDate = FromDate.AddMonths(-1)
                    strSQL = strSQL & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & ToDate & "' ) "
                    'strSQL = strSQL & "AND ((CONVERT (char(10),Box_Xfer_Hdr.Shipment_Date,101)) >='" & FromDate & "' AND (CONVERT (char(10),Box_Xfer_Hdr.Shipment_Date,101)) <='" & ToDate & "' ) "
                ElseIf ddlDateSearch.SelectedItem.Value = "Past 60 Days" Then
                    FromDate = (Now.ToShortDateString)
                    FromDate = FromDate.AddMonths(-2)
                    strSQL = strSQL & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & ToDate & "' ) "
                    'strSQL = strSQL & "AND ((CONVERT (char(10),Box_Xfer_Hdr.Shipment_Date,101)) >='" & FromDate & "' AND (CONVERT (char(10),Box_Xfer_Hdr.Shipment_Date,101)) <='" & ToDate & "' ) "
                ElseIf ddlDateSearch.SelectedItem.Value = "Past 90 Days" Then
                    FromDate = (Now.ToShortDateString)
                    FromDate = FromDate.AddMonths(-3)
                    strSQL = strSQL & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & ToDate & "' ) "
                    'strSQL = strSQL & "AND ((CONVERT (char(10),Box_Xfer_Hdr.Shipment_Date,101)) >='" & FromDate & "' AND (CONVERT (char(10),Box_Xfer_Hdr.Shipment_Date,101)) <='" & ToDate & "' ) "
                ElseIf ddlDateSearch.SelectedItem.Value = "<ALL DATES>" Then
                    FromDate = (Now.ToShortDateString)
                    FromDate = FromDate.AddMonths(-12)
                    strSQL = strSQL & "AND (Box_Xfer_Hdr.Shipment_Date >='" & FromDate & "' AND Box_Xfer_Hdr.Shipment_Date <='" & ToDate & "' ) "
                End If

            End If

            If FromDate <> "12:00:00 AM" Then
                FDate = FromDate
            End If
            If ToDate <> "12:00:00 AM" Then
                TDate = ToDate
            End If
            '******************************************************************************************
            'Get discrepancy quantity amounts

            'If (txtdiscrepF.Text <> "" And txtdiscrepF.Text <> 0) Then
            If (txtdiscrepF.Text <> "") Then
                If (txtdiscrepF.Text <> 0) Then
                    intDiscQtyFrom = txtdiscrepF.Text
                    'strSQL = strSQL & "AND Box_Xfer_Item.Discrepancy >= " & intDiscQtyFrom & " "
                    DFQty = intDiscQtyFrom
                End If
            End If

            'If (txtdiscrepT.Text <> "" And txtdiscrepT.Text <> 0) Then
            If (txtdiscrepT.Text <> "") Then
                If (txtdiscrepT.Text <> 0) Then
                    intDiscQtyTo = txtdiscrepT.Text
                    'strSQL = strSQL & "AND Box_Xfer_Item.Discrepancy <= " & intDiscQtyTo & " "
                    DTQty = intDiscQtyTo
                End If
            End If
            '******************************************************************************************
            'Get discrepancy dollar amounts
            If txtdesamtF.Text <> "" Then
                DiscAmtFrom = txtdesamtF.Text
                'strSQL = strSQL & "AND Box_Xfer_Item.Discrep_Amt >= " & DiscAmtFrom & " "
                DFAmt = DiscAmtFrom
            End If

            If txtdesamtT.Text <> "" Then
                DiscAmtTo = txtdesamtT.Text
                'strSQL = strSQL & "AND Box_Xfer_Item.Discrep_Amt <= " & DiscAmtTo & " "
                DTAmt = DiscAmtTo
            End If
            '******************************************************************************************
            strUPS = Trim(txtUPS.Text)
            If strUPS <> "" Then
                'strSQL = strSQL & "AND (Box_Xfer_Hdr.Tracking_Num=('" & (strUPS) & "')) "
                TrackNum = strUPS
            End If
            strTransferNum = Trim(txtTransferNum.Text)
            If strTransferNum <> "" Then
                'strSQL = strSQL & "AND (Box_Xfer_Hdr.Box_Id=('" & (strTransferNum) & "')) "
                TransNum = strTransferNum
            End If
        Else
            blnContinue = False
        End If
    End Sub
    Private Sub FillProfile()
        'This sub procedure fills in the textboxes and search criteria for the profile selected.
        Dim parmCreator As SqlClient.SqlParameter
        Dim ProfileCreator As String
        Dim PList As String
        PList = Request.QueryString("PROID")
        'checking to see if person logged in is creator of profile
        Dim conGetCreator As System.Data.SqlClient.SqlConnection
        Dim cmdGetCreator As System.Data.SqlClient.SqlCommand
        conGetCreator = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdGetCreator = New SqlClient.SqlCommand("spGetCreator", conGetCreator)
        cmdGetCreator.CommandType = CommandType.StoredProcedure
        cmdGetCreator.Parameters.Add("@PID", PList)
        parmCreator = cmdGetCreator.Parameters.Add("@Creator", SqlDbType.VarChar)
        parmCreator.Size = 45
        parmCreator.Direction = ParameterDirection.Output
        conGetCreator.Open()
        cmdGetCreator.ExecuteNonQuery()
        conGetCreator.Close()

        ProfileCreator = cmdGetCreator.Parameters("@Creator").Value
        cmdGetCreator.Dispose()
        If ProfileCreator = Creator Then
            'person made this profile so they can delete and edit profile
            btnDeleteProfile.Visible = True
            btnUpdateProfile.Visible = True
            btnSaveProfile.Visible = False
        Else
            'user isn't creator of profile, so they can't delete or edit profile
            btnDeleteProfile.Visible = False
            btnSaveProfile.Visible = False
            btnUpdateProfile.Visible = False
        End If
        '********************************************************************************************
        Dim parmPType As SqlClient.SqlParameter
        Dim parmDateSearchType As SqlClient.SqlParameter
        Dim parmDateRange As SqlClient.SqlParameter
        Dim parmShipF As SqlClient.SqlParameter
        Dim parmShipT As SqlClient.SqlParameter
        Dim parmRegion As SqlClient.SqlParameter
        Dim parmDistrict As SqlClient.SqlParameter
        Dim parmStore As SqlClient.SqlParameter
        Dim parmEFInternet As SqlClient.SqlParameter
        Dim parmTRegion As SqlClient.SqlParameter
        Dim parmTDistrict As SqlClient.SqlParameter
        Dim parmTStore As SqlClient.SqlParameter
        Dim parmETInternet As SqlClient.SqlParameter
        Dim parmTransNum As SqlClient.SqlParameter
        Dim parmUPS As SqlClient.SqlParameter
        Dim parmTransGroup As SqlClient.SqlParameter
        Dim parmTransStatus As SqlClient.SqlParameter
        Dim parmDisQtyF As SqlClient.SqlParameter
        Dim parmDisQtyT As SqlClient.SqlParameter
        Dim parmDisAmtF As SqlClient.SqlParameter
        Dim parmDisAmtT As SqlClient.SqlParameter
        Dim parmDisStatus As SqlClient.SqlParameter
        Dim parmGers As SqlClient.SqlParameter

        Dim conGetProfile As System.Data.SqlClient.SqlConnection
        Dim cmdGetProfile As System.Data.SqlClient.SqlCommand
        conGetProfile = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdGetProfile = New SqlClient.SqlCommand("asRetrieveProfile", conGetProfile)
        cmdGetProfile.CommandType = CommandType.StoredProcedure

        cmdGetProfile.Parameters.Add("@PID", PList)
        parmPType = cmdGetProfile.Parameters.Add("@ProfileType", SqlDbType.VarChar)
        parmCreator = cmdGetProfile.Parameters.Add("@Creator", SqlDbType.VarChar)
        parmDateSearchType = cmdGetProfile.Parameters.Add("@DateSearchType", SqlDbType.VarChar)
        parmDateRange = cmdGetProfile.Parameters.Add("@DateRange", SqlDbType.VarChar)
        parmShipF = cmdGetProfile.Parameters.Add("@shipdateF", SqlDbType.VarChar)
        parmShipT = cmdGetProfile.Parameters.Add("@shipdateT", SqlDbType.VarChar)
        parmRegion = cmdGetProfile.Parameters.Add("@FRegion", SqlDbType.Int)
        parmDistrict = cmdGetProfile.Parameters.Add("@FDistrict", SqlDbType.Int)
        parmStore = cmdGetProfile.Parameters.Add("@FStore", SqlDbType.Int)
        parmEFInternet = cmdGetProfile.Parameters.Add("@EFInternet", SqlDbType.TinyInt)
        parmTRegion = cmdGetProfile.Parameters.Add("@TRegion", SqlDbType.Int)
        parmTDistrict = cmdGetProfile.Parameters.Add("@TDistrict", SqlDbType.Int)
        parmTStore = cmdGetProfile.Parameters.Add("@TStore", SqlDbType.Int)
        parmETInternet = cmdGetProfile.Parameters.Add("@ETInternet", SqlDbType.TinyInt)
        parmTransNum = cmdGetProfile.Parameters.Add("@TransNum", SqlDbType.VarChar)
        parmUPS = cmdGetProfile.Parameters.Add("@UPS", SqlDbType.VarChar)
        parmTransGroup = cmdGetProfile.Parameters.Add("@TransferGroup", SqlDbType.VarChar)
        parmTransStatus = cmdGetProfile.Parameters.Add("@TransferStatus", SqlDbType.VarChar)
        parmDisQtyF = cmdGetProfile.Parameters.Add("@DiscrepQtyF", SqlDbType.Int)
        parmDisQtyT = cmdGetProfile.Parameters.Add("@DiscrepQtyT", SqlDbType.Int)
        parmDisAmtF = cmdGetProfile.Parameters.Add("@DiscrepamtF", SqlDbType.Decimal)
        parmDisAmtT = cmdGetProfile.Parameters.Add("@DiscrepamtT", SqlDbType.Decimal)
        parmDisStatus = cmdGetProfile.Parameters.Add("@DiscrepStatus", SqlDbType.VarChar)
        parmGers = cmdGetProfile.Parameters.Add("@Gers", SqlDbType.VarChar)
        'parmGers = cmdGetProfile.Parameters.Add("@Gers", SqlDbType.Int)
        parmPType.Size = 50
        parmCreator.Size = 20
        parmDateSearchType.Size = 50
        parmDateRange.Size = 50
        parmShipF.Size = 20
        parmShipT.Size = 20
        parmRegion.Size = 4
        parmDistrict.Size = 4
        parmStore.Size = 4
        parmEFInternet.Size = 1
        parmTRegion.Size = 4
        parmTDistrict.Size = 4
        parmTStore.Size = 4
        parmETInternet.Size = 1
        parmTransNum.Size = 15
        parmUPS.Size = 30
        parmTransGroup.Size = 100
        parmTransStatus.Size = 100
        parmDisQtyF.Size = 4
        parmDisQtyT.Size = 4
        parmDisAmtF.Size = 20
        parmDisAmtT.Size = 20
        parmDisStatus.Size = 100
        parmGers.Size = 100
        parmPType.Direction = ParameterDirection.Output
        parmDateSearchType.Direction = ParameterDirection.Output
        parmDateRange.Direction = ParameterDirection.Output
        parmCreator.Direction = ParameterDirection.Output
        parmShipF.Direction = ParameterDirection.Output
        parmShipT.Direction = ParameterDirection.Output
        parmRegion.Direction = ParameterDirection.Output
        parmDistrict.Direction = ParameterDirection.Output
        parmStore.Direction = ParameterDirection.Output
        parmEFInternet.Direction = ParameterDirection.Output
        parmTRegion.Direction = ParameterDirection.Output
        parmTDistrict.Direction = ParameterDirection.Output
        parmTStore.Direction = ParameterDirection.Output
        parmETInternet.Direction = ParameterDirection.Output
        parmTransNum.Direction = ParameterDirection.Output
        parmUPS.Direction = ParameterDirection.Output
        parmTransGroup.Direction = ParameterDirection.Output
        parmTransStatus.Direction = ParameterDirection.Output
        parmDisQtyF.Direction = ParameterDirection.Output
        parmDisQtyT.Direction = ParameterDirection.Output
        parmDisAmtF.Direction = ParameterDirection.Output
        parmDisAmtT.Direction = ParameterDirection.Output
        parmDisStatus.Direction = ParameterDirection.Output
        parmGers.Direction = ParameterDirection.Output
        conGetProfile.Open()
        cmdGetProfile.ExecuteNonQuery()
        conGetProfile.Close()


        'Fill in user's profile information/selections
        '**************************************************************
        'fill in textboxes user information
        Dim NullCheck As Boolean
        txtUPS.Text = Trim(cmdGetProfile.Parameters("@UPS").Value)
        txtTransferNum.Text = Trim(cmdGetProfile.Parameters("@TransNum").Value)

        Dim DQtyF As Integer
        Dim DQtyT As Integer
        DQtyF = cmdGetProfile.Parameters("@DiscrepQtyF").Value
        DQtyT = cmdGetProfile.Parameters("@DiscrepQtyT").Value
        If DQtyF = 0 And DQtyT = 0 Then
            txtdiscrepF.Text = ""
            txtdiscrepT.Text = ""
        Else
            txtdiscrepF.Text = DQtyF
            txtdiscrepT.Text = DQtyT
        End If
        'check to see if descrep amount from is null
        NullCheck = IsDBNull(cmdGetProfile.Parameters("@DiscrepamtF").Value)
        If NullCheck = True Then
            txtdesamtF.Text = ""
        Else
            txtdesamtF.Text = cmdGetProfile.Parameters("@DiscrepamtF").Value
        End If

        'check to see if descrep amount to is null
        NullCheck = IsDBNull(cmdGetProfile.Parameters("@DiscrepamtT").Value)
        If NullCheck = True Then
            txtdesamtT.Text = ""
        Else
            txtdesamtT.Text = cmdGetProfile.Parameters("@DiscrepamtT").Value
        End If
        '***********************************************************************
        'Retrieve Date Search info
        Dim DateSearchType = (cmdGetProfile.Parameters("@DateSearchType").Value)
        Dim DateRange = (cmdGetProfile.Parameters("@DateRange").Value)
        If DateSearchType = "Selected Date Range" Then
            rblSearchDate.SelectedIndex = rblSearchDate.Items.IndexOf(rblSearchDate.Items.FindByValue(DateSearchType.ToString))
            If DateRange = "Past 10 Days" Then
                ddlDateSearch.SelectedIndex = ddlDateSearch.Items.IndexOf(ddlDateSearch.Items.FindByValue(DateRange.ToString))
            ElseIf DateRange = "Past 30 Days" Then
                ddlDateSearch.SelectedIndex = ddlDateSearch.Items.IndexOf(ddlDateSearch.Items.FindByValue(DateRange.ToString))
                'ddlDateSearch.SelectedItem.Value = DateRange
            ElseIf DateRange = "Past 60 Days" Then
                ddlDateSearch.SelectedIndex = ddlDateSearch.Items.IndexOf(ddlDateSearch.Items.FindByValue(DateRange.ToString))
                'ddlDateSearch.SelectedItem.Value = DateRange
            ElseIf DateRange = "Past 90 Days" Then
                ddlDateSearch.SelectedIndex = ddlDateSearch.Items.IndexOf(ddlDateSearch.Items.FindByValue(DateRange.ToString))
                'ddlDateSearch.SelectedItem.Value = DateRange
            ElseIf DateRange = "<ALL DATES>" Then
                ddlDateSearch.SelectedIndex = ddlDateSearch.Items.IndexOf(ddlDateSearch.Items.FindByValue(DateRange.ToString))
            End If
            txtDateF.Enabled = False
            txtDateT.Enabled = False
            ddlDateSearch.Enabled = True
            txtDateF.Text = ""
            txtDateT.Text = ""
        ElseIf DateSearchType = "Specified Date Range" Then
            'rblSearchDate.SelectedItem.Value = "Specified Date Range"
            rblSearchDate.SelectedIndex = rblSearchDate.Items.IndexOf(rblSearchDate.Items.FindByValue(DateSearchType.ToString))
            txtDateF.Text = FormatDateTime(cmdGetProfile.Parameters("@shipdateF").Value, DateFormat.ShortDate)
            txtDateT.Text = FormatDateTime(cmdGetProfile.Parameters("@shipdateT").Value, DateFormat.ShortDate)
            txtDateF.Enabled = True
            txtDateT.Enabled = True
            ddlDateSearch.Enabled = False
            If txtDateF.Text = "1/1/1979" And txtDateT.Text = "1/1/1979" Then
                txtDateF.Text = ""
                txtDateT.Text = ""
            End If
            ddlDateSearch.SelectedIndex = ddlDateSearch.Items.IndexOf(ddlDateSearch.Items.FindByValue(DateRange.ToString))

        End If
        '**************************************************************************
        ''Fill in dropdown selections.  Region, store, district,Gers
        'Dim Gers As String
        'Gers = cmdGetProfile.Parameters("@Gers").Value
        'Call FillGERS()
        ''ddlGERS.SelectedIndex = ddlGERS.Items.IndexOf(ddlGERS.Items.FindByValue(Gers.ToString))
        'lstGERS.SelectedIndex = Gers

        '**************************************************************************
        'Fill From Store DDl's
        Dim Store As Integer
        Store = cmdGetProfile.Parameters("@FStore").Value
        Dim intDistrict As Integer
        intDistrict = cmdGetProfile.Parameters("@FDistrict").Value
        Dim intRegion As Integer
        intRegion = cmdGetProfile.Parameters("@FRegion").Value
        If Store <> 0 And intDistrict = 0 And intRegion = 0 Then    'if user selected a store, fill in region and district according to store
            Dim District As Integer
            Dim Region As Integer
            Dim conGetDR As System.Data.SqlClient.SqlConnection
            Dim cmdGetDR As System.Data.SqlClient.SqlCommand
            conGetDR = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            cmdGetDR = New SqlClient.SqlCommand("asGetDR", conGetDR)
            cmdGetDR.CommandType = CommandType.StoredProcedure
            cmdGetDR.Parameters.Add("@FStore", Store)
            parmRegion = cmdGetDR.Parameters.Add("@FRegion", SqlDbType.Int)
            parmDistrict = cmdGetDR.Parameters.Add("@FDistrict", SqlDbType.Int)
            parmRegion.Size = 4
            parmDistrict.Size = 4
            parmRegion.Direction = ParameterDirection.Output
            parmDistrict.Direction = ParameterDirection.Output
            conGetDR.Open()
            cmdGetDR.ExecuteNonQuery()
            conGetDR.Close()

            Region = cmdGetDR.Parameters("@FRegion").Value
            District = cmdGetDR.Parameters("@FDistrict").Value
            cmdGetDR.Dispose()
            Call FillRegion()
            ddlFRegion.SelectedIndex = ddlFRegion.Items.IndexOf(ddlFRegion.Items.FindByValue(Region.ToString))
            'Fill districts according to region
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE Region='" & Region & "' ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlFDistrict.DataSource = dtrReader
            ddlFDistrict.DataTextField = "District"
            ddlFDistrict.DataValueField = "District"
            ddlFDistrict.DataBind()
            ddlFDistrict.Items.Insert(0, "<ALL DISTRICTS IN REGION " + ddlFRegion.SelectedItem.Value + " >")
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()
            ddlFDistrict.SelectedIndex = ddlFDistrict.Items.IndexOf(ddlFDistrict.Items.FindByValue(District.ToString))

            'Fill stores in specified District
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE District='" & District & "' AND StoreNum >=5000 AND StoreNum <=5900 ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlFStore.DataSource = dtrStore
            ddlFStore.DataTextField = "StoreName"
            ddlFStore.DataValueField = "StoreNum"
            ddlFStore.DataBind()
            ddlFStore.Items.Insert(0, "<ALL STORES IN DISTRICT " + ddlFDistrict.SelectedItem.Value + ">")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
            ddlFStore.SelectedIndex = ddlFStore.Items.IndexOf(ddlFStore.Items.FindByValue(Store.ToString))
        ElseIf Store = 0 And intDistrict <> 0 And intRegion <> 0 Then  'user selected  a district
            Dim Region As Integer
            Dim conGetR As System.Data.SqlClient.SqlConnection
            Dim cmdGetR As System.Data.SqlClient.SqlCommand
            conGetR = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            cmdGetR = New SqlClient.SqlCommand("asGetR", conGetR)
            cmdGetR.CommandType = CommandType.StoredProcedure
            cmdGetR.Parameters.Add("@FDistrict", intDistrict)
            parmRegion = cmdGetR.Parameters.Add("@FRegion", SqlDbType.Int)
            parmRegion.Size = 4
            parmRegion.Direction = ParameterDirection.Output
            conGetR.Open()
            cmdGetR.ExecuteNonQuery()
            conGetR.Close()

            Region = cmdGetR.Parameters("@FRegion").Value
            cmdGetR.Dispose()
            Call FillRegion()
            ddlFRegion.SelectedIndex = ddlFRegion.Items.IndexOf(ddlFRegion.Items.FindByValue(Region.ToString))

            'Fill districts according to region
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE Region='" & Region & "' ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlFDistrict.DataSource = dtrReader
            ddlFDistrict.DataTextField = "District"
            ddlFDistrict.DataValueField = "District"
            ddlFDistrict.DataBind()
            ddlFDistrict.Items.Insert(0, "<ALL DISTRICTS IN REGION " + ddlFRegion.SelectedItem.Value + " >")
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()
            ddlFDistrict.SelectedIndex = ddlFDistrict.Items.IndexOf(ddlFDistrict.Items.FindByValue(intDistrict.ToString))

            'Fill stores in specified District
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE District='" & intDistrict & "' AND StoreNum >=5000 AND StoreNum <=5900 ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlFStore.DataSource = dtrStore
            ddlFStore.DataTextField = "StoreName"
            ddlFStore.DataValueField = "StoreNum"
            ddlFStore.DataBind()
            ddlFStore.Items.Insert(0, "<ALL STORES IN DISTRICT " + ddlFDistrict.SelectedItem.Value + ">")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
            ddlFStore.SelectedIndex = ddlFStore.Items.IndexOf(ddlFStore.Items.FindByValue(Store.ToString))
        ElseIf Store = 0 And intDistrict = 0 And intRegion <> 0 Then  'User selects region only
            FillRegion()
            ddlFRegion.SelectedIndex = ddlFRegion.Items.IndexOf(ddlFRegion.Items.FindByValue(intRegion.ToString))

            'Fill districts according to region
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE Region='" & intRegion & "' ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlFDistrict.DataSource = dtrReader
            ddlFDistrict.DataTextField = "District"
            ddlFDistrict.DataValueField = "District"
            ddlFDistrict.DataBind()
            ddlFDistrict.Items.Insert(0, "<ALL DISTRICTS IN REGION " + ddlFRegion.SelectedItem.Value + " >")
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()
            ddlFDistrict.SelectedIndex = 0

            'Fill stores in specified District
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE Region='" & intRegion & "' AND StoreNum >=5000 AND StoreNum <=5900 ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlFStore.DataSource = dtrStore
            ddlFStore.DataTextField = "StoreName"
            ddlFStore.DataValueField = "StoreNum"
            ddlFStore.DataBind()
            ddlFStore.Items.Insert(0, "<ALL STORES IN REGION " + ddlFRegion.SelectedItem.Value + ">")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
            ddlFStore.SelectedIndex = 0
        ElseIf Store = 0 And intDistrict = 0 And intRegion = 0 Then  'user doesn't select anything
            Call FillRegion()
            Call FillDistrict()
            Call FillStore()
        End If

        If cmdGetProfile.Parameters("@EFInternet").Value = 1 Then
            chkFInternetStore.Checked = True
        Else
            chkFInternetStore.Checked = False
        End If
        '****************************************************************************
        'Fill To Store ddl's
        Dim TStore As Integer
        TStore = cmdGetProfile.Parameters("@TStore").Value
        Dim intTDistrict As Integer
        intTDistrict = cmdGetProfile.Parameters("@TDistrict").Value
        Dim intTRegion As Integer
        intTRegion = cmdGetProfile.Parameters("@TRegion").Value
        If TStore <> 0 And intTDistrict = 0 And intTRegion = 0 Then    'if user selected a store, fill in region and district according to store
            Dim TDistrict As Integer
            Dim TRegion As Integer
            Dim conGetDR As System.Data.SqlClient.SqlConnection
            Dim cmdGetDR As System.Data.SqlClient.SqlCommand
            conGetDR = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            cmdGetDR = New SqlClient.SqlCommand("asGetToDR", conGetDR)
            cmdGetDR.CommandType = CommandType.StoredProcedure
            cmdGetDR.Parameters.Add("@TStore", TStore)
            parmTRegion = cmdGetDR.Parameters.Add("@TRegion", SqlDbType.Int)
            parmTDistrict = cmdGetDR.Parameters.Add("@TDistrict", SqlDbType.Int)
            parmTRegion.Size = 4
            parmTDistrict.Size = 4
            parmTRegion.Direction = ParameterDirection.Output
            parmTDistrict.Direction = ParameterDirection.Output
            conGetDR.Open()
            cmdGetDR.ExecuteNonQuery()
            conGetDR.Close()

            TRegion = cmdGetDR.Parameters("@TRegion").Value
            TDistrict = cmdGetDR.Parameters("@TDistrict").Value
            cmdGetDR.Dispose()
            Call FillToRegion()
            ddlTRegion.SelectedIndex = ddlTRegion.Items.IndexOf(ddlTRegion.Items.FindByValue(TRegion.ToString))
            'Fill districts according to region
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE Region='" & TRegion & "' ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlTDistrict.DataSource = dtrReader
            ddlTDistrict.DataTextField = "District"
            ddlTDistrict.DataValueField = "District"
            ddlTDistrict.DataBind()
            ddlTDistrict.Items.Insert(0, "<ALL DISTRICTS IN REGION " + ddlTRegion.SelectedItem.Value + " >")
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()
            ddlTDistrict.SelectedIndex = ddlTDistrict.Items.IndexOf(ddlTDistrict.Items.FindByValue(TDistrict.ToString))

            'Fill stores in specified District
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE District='" & TDistrict & "' AND StoreNum >=5000 AND StoreNum <=5900 ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlTStore.DataSource = dtrStore
            ddlTStore.DataTextField = "StoreName"
            ddlTStore.DataValueField = "StoreNum"
            ddlTStore.DataBind()
            ddlTStore.Items.Insert(0, "<ALL STORES IN DISTRICT " + ddlTDistrict.SelectedItem.Value + ">")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
            ddlTStore.SelectedIndex = ddlTStore.Items.IndexOf(ddlTStore.Items.FindByValue(TStore.ToString))
        ElseIf TStore = 0 And intTDistrict <> 0 And intTRegion <> 0 Then  'user selected  a district
            Dim TRegion As Integer
            Dim conGetR As System.Data.SqlClient.SqlConnection
            Dim cmdGetR As System.Data.SqlClient.SqlCommand
            conGetR = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            cmdGetR = New SqlClient.SqlCommand("asGetToR", conGetR)
            cmdGetR.CommandType = CommandType.StoredProcedure
            cmdGetR.Parameters.Add("@TDistrict", intTDistrict)
            parmTRegion = cmdGetR.Parameters.Add("@TRegion", SqlDbType.Int)
            parmTRegion.Size = 4
            parmTRegion.Direction = ParameterDirection.Output
            conGetR.Open()
            cmdGetR.ExecuteNonQuery()
            conGetR.Close()

            TRegion = cmdGetR.Parameters("@TRegion").Value
            cmdGetR.Dispose()
            Call FillToRegion()
            ddlTRegion.SelectedIndex = ddlTRegion.Items.IndexOf(ddlTRegion.Items.FindByValue(TRegion.ToString))

            'Fill districts according to region
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE Region='" & TRegion & "' ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlTDistrict.DataSource = dtrReader
            ddlTDistrict.DataTextField = "District"
            ddlTDistrict.DataValueField = "District"
            ddlTDistrict.DataBind()
            ddlTDistrict.Items.Insert(0, "<ALL DISTRICTS IN REGION " + ddlTRegion.SelectedItem.Value + " >")
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()
            ddlTDistrict.SelectedIndex = ddlTDistrict.Items.IndexOf(ddlTDistrict.Items.FindByValue(intTDistrict.ToString))

            'Fill stores in specified District
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE District='" & intTDistrict & "' AND StoreNum >=5000 AND StoreNum <=5900 ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlTStore.DataSource = dtrStore
            ddlTStore.DataTextField = "StoreName"
            ddlTStore.DataValueField = "StoreNum"
            ddlTStore.DataBind()
            ddlTStore.Items.Insert(0, "<ALL STORES IN DISTRICT " + ddlTDistrict.SelectedItem.Value + ">")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
            ddlTStore.SelectedIndex = ddlTStore.Items.IndexOf(ddlTStore.Items.FindByValue(TStore.ToString))
        ElseIf TStore = 0 And intTDistrict = 0 And intTRegion <> 0 Then  'User selects region only
            FillToRegion()
            ddlTRegion.SelectedIndex = ddlTRegion.Items.IndexOf(ddlTRegion.Items.FindByValue(intTRegion.ToString))

            'Fill districts according to region
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE Region='" & intRegion & "' ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlTDistrict.DataSource = dtrReader
            ddlTDistrict.DataTextField = "District"
            ddlTDistrict.DataValueField = "District"
            ddlTDistrict.DataBind()
            ddlTDistrict.Items.Insert(0, "<ALL DISTRICTS IN REGION " + ddlTRegion.SelectedItem.Value + " >")
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()
            ddlTDistrict.SelectedIndex = 0

            'Fill stores in specified District
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE Region='" & intTRegion & "' AND StoreNum >=5000 AND StoreNum <=5900 ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlTStore.DataSource = dtrStore
            ddlTStore.DataTextField = "StoreName"
            ddlTStore.DataValueField = "StoreNum"
            ddlTStore.DataBind()
            ddlTStore.Items.Insert(0, "<ALL STORES IN REGION " + ddlTRegion.SelectedItem.Value + ">")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
            ddlTStore.SelectedIndex = 0
        ElseIf TStore = 0 And intTDistrict = 0 And intTRegion = 0 Then  'user doesn't select anything
            Call FillToRegion()
            Call FillToDistrict()
            Call FillToStore()
        End If

        If cmdGetProfile.Parameters("@ETInternet").Value = 1 Then
            chkTInternetStore.Checked = True
        Else
            chkTInternetStore.Checked = False
        End If
        '***********************************************************************
        'Fill in user selections for listboxes

        'Listbox for Discrepancy
        Dim strDiscrepancy As String
        Dim lenDiscrepancy As Integer
        strDiscrepancy = Trim(cmdGetProfile.Parameters("@DiscrepStatus").Value)
        lenDiscrepancy = Len(strDiscrepancy)

        Dim strD As String
        If lenDiscrepancy > 1 Then
            Call FillDiscrepancy()
            Dim x As Integer
            x = 1
            'Dim strD As String
            Do While x <= lenDiscrepancy
                If x = 1 Then
                    strD = Left$(strDiscrepancy, x)
                    lbDiscrepancy.Items(lbDiscrepancy.Items.IndexOf(lbDiscrepancy.Items.FindByValue(strD.ToString))).Selected = True
                    x = x + 2
                ElseIf x > 1 Then
                    strD = Mid$(strDiscrepancy, x, 1)
                    lbDiscrepancy.Items(lbDiscrepancy.Items.IndexOf(lbDiscrepancy.Items.FindByValue(strD.ToString))).Selected = True
                    x = x + 2
                End If
            Loop
            'lbDiscrepancy.SelectedIndex = lbDiscrepancy.Items.IndexOf(lbDiscrepancy.Items.FindByValue(strD.ToString))
        ElseIf lenDiscrepancy = 1 Then
            Call FillDiscrepancy()
            If strDiscrepancy = "0" Then
                lbDiscrepancy.SelectedIndex = 0
            Else
                lbDiscrepancy.SelectedIndex = lbDiscrepancy.Items.IndexOf(lbDiscrepancy.Items.FindByValue(strDiscrepancy.ToString))
            End If

        ElseIf lenDiscrepancy = 0 Then
            Call FillDiscrepancy()
        End If

        'Listbox Transfer Type
        Dim strTransType As String
        Dim lenTransType As Integer
        strTransType = Trim(cmdGetProfile.Parameters("@TransferGroup").Value)
        lenTransType = Len(strTransType)
        Dim strT As String
        If lenTransType > 1 Then
            Call FillTransType()
            Dim x As Integer
            x = 1
            Do While x <= lenTransType
                If x = 1 Then
                    strT = Left$(strTransType, x)
                    lstTransType.Items(lstTransType.Items.IndexOf(lstTransType.Items.FindByValue(strT.ToString))).Selected = True
                    x = x + 2
                ElseIf x > 1 Then
                    strT = Mid$(strTransType, x, 1)
                    lstTransType.Items(lstTransType.Items.IndexOf(lstTransType.Items.FindByValue(strT.ToString))).Selected = True
                    x = x + 2
                End If
            Loop
        ElseIf lenTransType = 1 Then
            Call FillTransType()
            If strTransType = "0" Then
                lstTransType.SelectedIndex = 0
            Else
                lstTransType.SelectedIndex = lstTransType.Items.IndexOf(lstTransType.Items.FindByValue(strTransType.ToString))
            End If
        ElseIf lenTransType = 0 Then
            Call FillTransType()
        End If

        'Listbox Transfer Status
        Dim strTransStatus As String
        Dim lenTransStatus As Integer
        strTransStatus = Trim(cmdGetProfile.Parameters("@TransferStatus").Value)
        lenTransStatus = Len(strTransStatus)
        Dim strS As String
        If lenTransStatus > 1 Then
            Call FillTransStatus()
            Dim x As Integer
            x = 1
            Do While x <= lenTransStatus
                If x = 1 Then
                    strS = Left$(strTransStatus, x)
                    lstTransStatus.Items(lstTransStatus.Items.IndexOf(lstTransStatus.Items.FindByValue(strS.ToString))).Selected = True
                    x = x + 2
                ElseIf x > 1 Then
                    strS = Mid$(strTransStatus, x, 1)
                    lstTransStatus.Items(lstTransStatus.Items.IndexOf(lstTransStatus.Items.FindByValue(strS.ToString))).Selected = True
                    x = x + 2
                End If
            Loop
        ElseIf lenTransStatus = 1 Then
            Call FillTransStatus()
            If strTransStatus = "0" Then
                lstTransStatus.SelectedIndex = 0
            Else
                lstTransStatus.SelectedIndex = lstTransStatus.Items.IndexOf(lstTransStatus.Items.FindByValue(strTransStatus.ToString))
            End If
        ElseIf lenTransStatus = 0 Then
            Call FillTransStatus()
        End If

        'Listbox GERS Process Status
        Dim strGERSStatus As String
        Dim lenGERSStatus As Integer
        strGERSStatus = Trim(cmdGetProfile.Parameters("@GERS").Value)
        lenGERSStatus = Len(strGERSStatus)
        Dim strG As String
        If lenGERSStatus > 1 Then
            Call FillGERS()
            Dim x As Integer
            x = 1
            Do While x <= lenGERSStatus
                If x = 1 Then
                    strG = Left$(strGERSStatus, x)
                    lstGERS.Items(lstGERS.Items.IndexOf(lstGERS.Items.FindByValue(strG.ToString))).Selected = True
                    x = x + 2
                ElseIf x > 1 Then
                    strG = Mid$(strGERSStatus, x, 1)
                    lstGERS.Items(lstGERS.Items.IndexOf(lstGERS.Items.FindByValue(strG.ToString))).Selected = True
                    x = x + 2
                End If
            Loop
        ElseIf lenGERSStatus = 1 Then
            Call FillGERS()
            If strGERSStatus = "0" Then
                lstGERS.SelectedIndex = 0
            Else
                lstGERS.SelectedIndex = lstGERS.Items.IndexOf(lstGERS.Items.FindByValue(strGERSStatus.ToString))
            End If
        ElseIf lenGERSStatus = 0 Then
            Call FillGERS()
        End If
        cmdGetProfile.Dispose()
    End Sub
    Private Sub btnUpdateProfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateProfile.Click
        'This updates the users profile through a stored procedure
        'need PList  (profile id)
        Dim PList As String
        PList = Request.QueryString("PROID")
        Modified = Now
        Call GetInfo()

        '****************************************************************
        'Get Search Date Criteria for Profile
        Dim DateSearchType
        Dim DateRange
        DateSearchType = rblSearchDate.SelectedItem.Value

        If DateSearchType = "Selected Date Range" Then
            DateRange = ddlDateSearch.SelectedItem.Value
            ToDate = "#1/1/1979#"
            FromDate = "#1/1/1979#"

        ElseIf DateSearchType = "Specified Date Range" Then
            DateRange = "<Select A Range>"
            If txtDateT.Text = "" Then
                ToDate = Now.ToShortDateString
            End If

            If txtDateF.Text = "" Then
                FromDate = ((Now.AddMonths(-12)).ToShortDateString)   'Need to change
            End If
        End If

        Dim ExcludeFInternet As Int32
        Dim ExcludeTInternet As Int32
        If chkFInternetStore.Checked = True Then
            ExcludeFInternet = 1
        Else
            ExcludeFInternet = 0
        End If

        If chkTInternetStore.Checked = True Then
            ExcludeTInternet = 1
        Else
            ExcludeTInternet = 0
        End If

        If intFStore <> 0 Then
            intRegion = 0
            intDistrict = 0
        End If

        If intTStore <> 0 Then
            intTRegion = 0
            intTDistrict = 0
        End If
        '***********************************************************************

        Dim conUpdateProfile As System.Data.SqlClient.SqlConnection
        Dim cmdUpdateProfile As System.Data.SqlClient.SqlCommand
        conUpdateProfile = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdUpdateProfile = New SqlClient.SqlCommand("spUpdate_Profile", conUpdateProfile)
        cmdUpdateProfile.CommandType = CommandType.StoredProcedure
        cmdUpdateProfile.Parameters.Add("@PID", PList)
        cmdUpdateProfile.Parameters.Add("@DateSearchType", DateSearchType)
        cmdUpdateProfile.Parameters.Add("@DateRange", DateRange)
        cmdUpdateProfile.Parameters.Add("@shipdateF", FromDate)
        cmdUpdateProfile.Parameters.Add("@shipdateT", ToDate)
        cmdUpdateProfile.Parameters.Add("@FRegion", intRegion)
        cmdUpdateProfile.Parameters.Add("@FDistrict", intDistrict)
        cmdUpdateProfile.Parameters.Add("@FStore", intFStore)
        cmdUpdateProfile.Parameters.Add("@EFInternet", ExcludeFInternet)
        cmdUpdateProfile.Parameters.Add("@TRegion", intTRegion)
        cmdUpdateProfile.Parameters.Add("@TDistrict", intTDistrict)
        cmdUpdateProfile.Parameters.Add("@TStore", intTStore)
        cmdUpdateProfile.Parameters.Add("@ETInternet", ExcludeTInternet)
        cmdUpdateProfile.Parameters.Add("@TransNum", strTransferNum)
        cmdUpdateProfile.Parameters.Add("@UPS", strUPS)
        cmdUpdateProfile.Parameters.Add("@TransferGroup", strTransGroup)
        cmdUpdateProfile.Parameters.Add("@TransferStatus", strTransStatus)
        cmdUpdateProfile.Parameters.Add("@DiscrepQtyF", intDiscQtyFrom)
        cmdUpdateProfile.Parameters.Add("@DiscrepQtyT", intDiscQtyTo)
        cmdUpdateProfile.Parameters.Add("@DiscrepamtF", DiscAmtFrom)
        cmdUpdateProfile.Parameters.Add("@DiscrepamtT", DiscAmtTo)
        cmdUpdateProfile.Parameters.Add("@DiscrepStatus", strDiscepStatus)
        cmdUpdateProfile.Parameters.Add("@Gers", strGersStatus)
        cmdUpdateProfile.Parameters.Add("@Modified", Modified)

        conUpdateProfile.Open()
        cmdUpdateProfile.ExecuteNonQuery()
        conUpdateProfile.Close()
        cmdUpdateProfile.Dispose()
        Response.Redirect("IC_ProfileList.aspx")
        'Response.Redirect("IC_NameProfile.aspx?PID=" & System.Web.HttpUtility.UrlEncode(PList))
    End Sub
    Private Sub btnDeleteProfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteProfile.Click
        'asks user if they are sure they want to delete their profile
        lblDelete.Visible = True
        btnDYes.Visible = True
        btnDNo.Visible = True
    End Sub
    Private Sub btnDYes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDYes.Click
        'Deletes profile (only if user was the one who created the report.)
        Dim PList As String
        PList = Request.QueryString("PROID")

        Dim conDeleteProfile As System.Data.SqlClient.SqlConnection
        Dim cmdDeleteProfile As System.Data.SqlClient.SqlCommand
        conDeleteProfile = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdDeleteProfile = New SqlClient.SqlCommand("spDeleteProfile", conDeleteProfile)
        cmdDeleteProfile.CommandType = CommandType.StoredProcedure
        cmdDeleteProfile.Parameters.Add("@PID", PList)
        conDeleteProfile.Open()
        cmdDeleteProfile.ExecuteNonQuery()
        conDeleteProfile.Close()
        cmdDeleteProfile.Dispose()
        Response.Redirect("IC_ProfileList.aspx")
    End Sub
    Private Sub btnDNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDNo.Click
        'User doesn't really want to delete user Profile
        lblDelete.Visible = False
        btnDYes.Visible = False
        btnDNo.Visible = False
    End Sub
    Private Sub ddlTRegion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTRegion.SelectedIndexChanged
        If ddlTRegion.SelectedIndex <> 0 Then
            'fill listbox with districts in the region selected
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE Region='" + ddlTRegion.SelectedItem.Value + "' ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlTDistrict.DataSource = dtrReader
            ddlTDistrict.DataTextField = "District"
            ddlTDistrict.DataValueField = "District"
            ddlTDistrict.DataBind()
            ddlTDistrict.Items.Insert(0, "<ALL DISTRICTS IN REGION " + ddlTRegion.SelectedItem.Value + " >")
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()

            'fill appropriate stores to districts in Region chosen.
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE Region='" + ddlTRegion.SelectedItem.Value + "' AND ((StoreNum >=5000 AND StoreNum <=5900) OR StoreNum=5990) ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlTStore.DataSource = dtrStore
            ddlTStore.DataTextField = "StoreName"
            ddlTStore.DataValueField = "StoreNum"
            ddlTStore.DataBind()
            ddlTStore.Items.Insert(0, "<ALL STORES IN REGION " + ddlTRegion.SelectedItem.Value + ">")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
        ElseIf ddlTRegion.SelectedIndex = 0 Then
            'fill listbox with all districts
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE StoreNum >=5000 AND StoreNum <=5900 AND District IS NOT NULL ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlTDistrict.DataSource = dtrReader
            ddlTDistrict.DataTextField = "District"
            ddlTDistrict.DataValueField = "District"
            ddlTDistrict.DataBind()
            ddlTDistrict.Items.Insert(0, "<ALL DISTRICTS>")
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()

            'fill with all stores 
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE StoreNum >=5000 AND StoreNum <=5900 ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlTStore.DataSource = dtrStore
            ddlTStore.DataTextField = "StoreName"
            ddlTStore.DataValueField = "StoreNum"
            ddlTStore.DataBind()
            ddlTStore.Items.Insert(0, "<ALL STORES>")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
        End If
    End Sub
    Private Sub ddlTDistrict_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTDistrict.SelectedIndexChanged
        If ddlTDistrict.SelectedIndex <> 0 Then
            'fill appropriate stores to district chosen.
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE District='" + ddlTDistrict.SelectedItem.Value + "' AND ((StoreNum >=5000 AND StoreNum <=5900) OR StoreNum=5990) ", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlTStore.DataSource = dtrStore
            ddlTStore.DataTextField = "StoreName"
            ddlTStore.DataValueField = "StoreNum"
            ddlTStore.DataBind()
            ddlTStore.Items.Insert(0, "<ALL STORES IN DISTRICT " + ddlTDistrict.SelectedItem.Value + ">")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
            'if user selects a district, make sure proper region selection shows
            Dim conRegion As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            Dim cmdRegion As New SqlClient.SqlCommand("SELECT DISTINCT Region AS Region FROM STORE WHERE District='" + ddlTDistrict.SelectedItem.Value + "' ORDER BY Region", conRegion)
            Dim dtrRegion As SqlClient.SqlDataReader
            conRegion.Open()
            dtrRegion = cmdRegion.ExecuteReader()
            ddlTRegion.DataSource = dtrRegion
            ddlTRegion.DataTextField = "Region"
            ddlTRegion.DataValueField = "Region"
            ddlTRegion.DataBind()
            ddlTRegion.Items.Insert(0, "<ALL REGIONS>")
            dtrRegion.Close()
            conRegion.Close()
            cmdRegion.Dispose()
            ddlTRegion.SelectedIndex = 1
            '*********************************************
            'testing fix
            ddlTDistrict.Items.RemoveAt(0)
            ddlTDistrict.Items.Insert(0, "<ALL DISTRICTS>")
            '*********************************************
        ElseIf ddlTDistrict.SelectedIndex = 0 Then
            'Fill in all regions, user selected all districts
            Dim conRegion As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            Dim cmdRegion As New SqlClient.SqlCommand("SELECT DISTINCT Region AS Region FROM STORE WHERE StoreNum >= 5001 AND StoreNum <= 5900 AND Region IS NOT NULL ORDER BY Region", conRegion)
            Dim dtrRegion As SqlClient.SqlDataReader
            conRegion.Open()
            dtrRegion = cmdRegion.ExecuteReader()
            ddlTRegion.DataSource = dtrRegion
            ddlTRegion.DataTextField = "Region"
            ddlTRegion.DataValueField = "Region"
            ddlTRegion.DataBind()
            ddlTRegion.Items.Insert(0, "<ALL REGIONS>")
            dtrRegion.Close()
            conRegion.Close()
            cmdRegion.Dispose()
            ddlTRegion.SelectedIndex = 0

            'fill with all stores 
            Dim conStore As SqlClient.SqlConnection
            Dim cmdStore As SqlClient.SqlCommand
            Dim dtrStore As SqlClient.SqlDataReader
            conStore = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conStore.Open()
            cmdStore = New SqlClient.SqlCommand("SELECT Distinct StoreNum,StoreName FROM Store WHERE StoreNum >=5000 AND StoreNum <=5900", conStore)
            dtrStore = cmdStore.ExecuteReader()
            ddlTStore.DataSource = dtrStore
            ddlTStore.DataTextField = "StoreName"
            ddlTStore.DataValueField = "StoreNum"
            ddlTStore.DataBind()
            ddlTStore.Items.Insert(0, "<ALL STORES>")
            dtrStore.Close()
            conStore.Close()
            cmdStore.Dispose()
        End If
    End Sub
    Private Sub ddlTStore_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTStore.SelectedIndexChanged
        If ddlTStore.SelectedIndex <> 0 Then
            'fill appropriate region
            Dim conRegion As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            Dim cmdRegion As New SqlClient.SqlCommand("SELECT DISTINCT Region AS Region FROM STORE WHERE StoreNum='" + ddlTStore.SelectedItem.Value + "' ORDER BY Region", conRegion)
            Dim dtrRegion As SqlClient.SqlDataReader
            conRegion.Open()
            dtrRegion = cmdRegion.ExecuteReader()
            ddlTRegion.DataSource = dtrRegion
            ddlTRegion.DataTextField = "Region"
            ddlTRegion.DataValueField = "Region"
            ddlTRegion.DataBind()
            dtrRegion.Close()
            conRegion.Close()
            cmdRegion.Dispose()
            'fill listbox with districts
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE StoreNum='" + ddlTStore.SelectedItem.Value + "' ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlTDistrict.DataSource = dtrReader
            ddlTDistrict.DataTextField = "District"
            ddlTDistrict.DataValueField = "District"
            ddlTDistrict.DataBind()
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()
            '*********************************************
            'testing fix
            ddlTStore.Items.RemoveAt(0)
            ddlTStore.Items.Insert(0, "<ALL STORES>")
            '*********************************************
        ElseIf ddlTStore.SelectedIndex = 0 Then
            'Fill in all regions
            Dim conRegion As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            Dim cmdRegion As New SqlClient.SqlCommand("SELECT DISTINCT Region AS Region FROM STORE WHERE ((StoreNum >= 5001 AND StoreNum <= 5900 AND Region IS NOT NULL) OR StoreNum=5990) ORDER BY Region", conRegion)
            Dim dtrRegion As SqlClient.SqlDataReader
            conRegion.Open()
            dtrRegion = cmdRegion.ExecuteReader()
            ddlTRegion.DataSource = dtrRegion
            ddlTRegion.DataTextField = "Region"
            ddlTRegion.DataValueField = "Region"
            ddlTRegion.DataBind()
            ddlTRegion.Items.Insert(0, "<ALL REGIONS>")
            dtrRegion.Close()
            conRegion.Close()
            cmdRegion.Dispose()
            ddlTRegion.SelectedIndex = 0

            'fill listbox with all districts
            Dim conConnection As SqlClient.SqlConnection
            Dim cmdCommand As SqlClient.SqlCommand
            Dim dtrReader As SqlClient.SqlDataReader
            conConnection = New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
            conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT Distinct District AS District FROM Store WHERE StoreNum >=5000 AND StoreNum <=5900 AND District IS NOT NULL ", conConnection)
            dtrReader = cmdCommand.ExecuteReader()
            ddlTDistrict.DataSource = dtrReader
            ddlTDistrict.DataTextField = "District"
            ddlTDistrict.DataValueField = "District"
            ddlTDistrict.DataBind()
            ddlTDistrict.Items.Insert(0, "<ALL DISTRICTS>")
            dtrReader.Close()
            conConnection.Close()
            cmdCommand.Dispose()
        End If
    End Sub
    Private Sub rblSearchDate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rblSearchDate.SelectedIndexChanged
        'User changed method of Date Search
        If rblSearchDate.SelectedItem.Value = "Specified Date Range" Then
            txtDateT.Enabled = True
            txtDateF.Enabled = True
            ddlDateSearch.Enabled = False

        ElseIf rblSearchDate.SelectedItem.Value = "Selected Date Range" Then
            txtDateT.Enabled = False
            txtDateF.Enabled = False
            ddlDateSearch.Enabled = True
        End If
    End Sub
    Private Sub ValidateProfileInput()
        'Validate profile name and description
        ProfileName = txtProfileName.Text
        ProfileDescrip = txtProDescrip.Text

        If (txtProfileName.Text = "" And txtProDescrip.Text <> "") Then
            'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter a name for your profile.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            lblError.Text = "Please enter a name for your profile."
        ElseIf (txtProfileName.Text <> "" And txtProDescrip.Text = "") Then
            'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter a profile description.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            lblError.Text = "Please enter a profile description."
        ElseIf (txtProfileName.Text = "" And txtProDescrip.Text = "") Then
            'pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter a name and description for your profile.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            lblError.Text = "Please enter a name and description for your profile."
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Save profile,name,and description.
        Call ValidateInput()
        Call ValidateProfileInput()
        Call GetInfo()
        If blnContinue = True Then
            'execute stored procedure to insert new profile

            Created = Now        'gets current date and time
            Modified = Created
            '****************************************************************
            'Get Search Date Criteria for Profile
            Dim DateSearchType
            Dim DateRange
            DateSearchType = rblSearchDate.SelectedItem.Value

            If DateSearchType = "Selected Date Range" Then
                DateRange = ddlDateSearch.SelectedItem.Value
                ToDate = "#1/1/1979#"
                FromDate = "#1/1/1979#"

            ElseIf DateSearchType = "Specified Date Range" Then
                DateRange = "<Select A Range>"
                If txtDateT.Text = "" Then
                    'ToDate = Now.ToShortDateString
                    ToDate = "#1/1/1979#"
                End If

                If txtDateF.Text = "" Then
                    'FromDate = ((Now.AddMonths(-12)).ToShortDateString)
                    FromDate = "#1/1/1979#"
                End If
            End If

            Dim ExcludeFInternet As Int32
            Dim ExcludeTInternet As Int32
            If chkFInternetStore.Checked = True Then
                ExcludeFInternet = 1
            Else
                ExcludeFInternet = 0
            End If

            If chkTInternetStore.Checked = True Then
                ExcludeTInternet = 1
            Else
                ExcludeTInternet = 0
            End If

            '***********************************************************************
            Dim conInsertProfile As System.Data.SqlClient.SqlConnection
            Dim cmdInsertProfile As System.Data.SqlClient.SqlCommand
            conInsertProfile = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            cmdInsertProfile = New SqlClient.SqlCommand("spNew_Profile_Insert", conInsertProfile)
            cmdInsertProfile.CommandType = CommandType.StoredProcedure
            cmdInsertProfile.Parameters.Add("@DateSearchType", DateSearchType)
            cmdInsertProfile.Parameters.Add("@DateRange", DateRange)
            cmdInsertProfile.Parameters.Add("@shipdateF", FromDate)
            cmdInsertProfile.Parameters.Add("@shipdateT", ToDate)
            cmdInsertProfile.Parameters.Add("@FRegion", intRegion)
            cmdInsertProfile.Parameters.Add("@FDistrict", intDistrict)
            cmdInsertProfile.Parameters.Add("@FStore", intFStore)
            cmdInsertProfile.Parameters.Add("@EFInternet", ExcludeFInternet)
            cmdInsertProfile.Parameters.Add("@TRegion", intTRegion)
            cmdInsertProfile.Parameters.Add("@TDistrict", intTDistrict)
            cmdInsertProfile.Parameters.Add("@TStore", intTStore)
            cmdInsertProfile.Parameters.Add("@ETInternet", ExcludeTInternet)
            cmdInsertProfile.Parameters.Add("@TransNum", strTransferNum)
            cmdInsertProfile.Parameters.Add("@UPS", strUPS)
            cmdInsertProfile.Parameters.Add("@TransferGroup", strTransGroup)
            cmdInsertProfile.Parameters.Add("@TransferStatus", strTransStatus)
            cmdInsertProfile.Parameters.Add("@DiscrepQtyF", intDiscQtyFrom)
            cmdInsertProfile.Parameters.Add("@DiscrepQtyT", intDiscQtyTo)
            cmdInsertProfile.Parameters.Add("@DiscrepamtF", DiscAmtFrom)
            cmdInsertProfile.Parameters.Add("@DiscrepamtT", DiscAmtTo)
            cmdInsertProfile.Parameters.Add("@DiscrepStatus", strDiscepStatus)
            'cmdInsertProfile.Parameters.Add("@Gers", intGersStatus)
            cmdInsertProfile.Parameters.Add("@Gers", strGersStatus)
            cmdInsertProfile.Parameters.Add("@Created", Created)
            cmdInsertProfile.Parameters.Add("@Modified", Modified)
            cmdInsertProfile.Parameters.Add("@Creator", Creator)
            cmdInsertProfile.Parameters.Add("@ProfileName", ProfileName)
            cmdInsertProfile.Parameters.Add("@ProDescrip", ProfileDescrip)

            parmProID = cmdInsertProfile.Parameters.Add("@PID", SqlDbType.Int)
            parmProID.Size = 20
            parmProID.Direction = ParameterDirection.Output

            conInsertProfile.Open()
            cmdInsertProfile.ExecuteNonQuery()
            conInsertProfile.Close()

            ProfileID = cmdInsertProfile.Parameters("@PID").Value   'getting user profile id number
            cmdInsertProfile.Dispose()
            Response.Redirect("IC_ProfileList.aspx")
        End If
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'cancel saving of profile
        lblProfileName.Visible = False
        txtProfileName.Visible = False
        lblProDescrip.Visible = False
        txtProDescrip.Visible = False
        btnCancel.Visible = False
        btnSave.Visible = False
        btnSaveProfile.Visible = True
        lblError.Text = ""
    End Sub
    Private Sub txtProfileName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProfileName.TextChanged
        ProfileName = txtProfileName.Text
    End Sub
    Private Sub txtProDescrip_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProDescrip.TextChanged
        ProfileDescrip = txtProDescrip.Text
    End Sub
    Private Sub chkFInternetStore_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFInternetStore.CheckedChanged

    End Sub
    Private Sub chkTInternetStore_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTInternetStore.CheckedChanged

    End Sub
End Class
