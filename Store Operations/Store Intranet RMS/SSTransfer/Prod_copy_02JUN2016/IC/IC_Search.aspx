<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_Search.aspx.vb" Inherits="SSTransfer.IC_Search"%>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>IC Search</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="../Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<script src="../Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="../Javascript/TrapKeyPress.js" language="JavaScript"></script>
	</HEAD>
	<body id="pageBody" runat="server" bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="yes" rightMargin="0">
		<form id="frmICSearch" method="post" runat="server" onsubmit="return checkSubmit();">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="top" align="right"><br>
						<table cellSpacing="0" cellPadding="4" width="100%" bgColor="#000000" border="0">
							<tr>
								<td vAlign="top" width="50%">
									<table cellSpacing="0" cellPadding="2" width="100%" bgColor="#000000" border="0">
										<tr>
											<td noWrap bgColor="#990000"><font face="arial" color="white" size="2">From Store 
													Criteria</font></td>
										</tr>
										<tr>
											<td bgColor="#990000">
												<table cellSpacing="0" cellPadding="2" width="100%" bgColor="#000000" border="0">
													<tr>
														<td width="1%"><FONT face="Arial" color="#ffffff" size="2">Region:</FONT></td>
														<td><asp:dropdownlist id="ddlFRegion" tabIndex="1" runat="server" AutoPostBack="True" Width="100%"></asp:dropdownlist></td>
													</tr>
													<tr>
														<td width="1%"><FONT face="Arial" color="#ffffff" size="2">District:</FONT></td>
														<td><asp:dropdownlist id="ddlFDistrict" tabIndex="2" runat="server" AutoPostBack="True" Width="100%"></asp:dropdownlist></td>
													</tr>
													<tr>
														<td noWrap width="1%"><FONT face="Arial" color="#ffffff" size="2">Store #:</FONT></td>
														<td><asp:dropdownlist id="ddlFStore" tabIndex="3" runat="server" AutoPostBack="True" Width="100%"></asp:dropdownlist></td>
													</tr>
													<tr>
														<td noWrap width="1%"><FONT face="Arial" color="#ffffff" size="2">Exclude Internet 
																Store:</FONT></td>
														<td><asp:CheckBox Runat="server" ID="chkFInternetStore"></asp:CheckBox></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<br>
									<table cellSpacing="0" cellPadding="2" width="100%" bgColor="#000000" border="0">
										<tr>
											<td noWrap bgColor="#990000"><font face="arial" color="white" size="2">To Store 
													Criteria</font></td>
										</tr>
										<tr>
											<td bgColor="#990000">
												<table cellSpacing="0" cellPadding="2" width="100%" bgColor="#000000" border="0">
													<tr>
														<td width="1%"><FONT face="Arial" color="#ffffff" size="2">Region:</FONT></td>
														<td><asp:dropdownlist id="ddlTRegion" tabIndex="4" runat="server" AutoPostBack="True" Width="100%"></asp:dropdownlist></td>
													</tr>
													<tr>
														<td width="1%"><FONT face="Arial" color="#ffffff" size="2">District:</FONT></td>
														<td><asp:dropdownlist id="ddlTDistrict" tabIndex="5" runat="server" AutoPostBack="True" Width="100%"></asp:dropdownlist></td>
													</tr>
													<tr>
														<td noWrap width="1%"><FONT face="Arial" color="#ffffff" size="2">Store #:</FONT></td>
														<td><asp:dropdownlist id="ddlTStore" tabIndex="6" runat="server" AutoPostBack="True" Width="100%"></asp:dropdownlist></td>
													</tr>
													<tr>
														<td noWrap width="1%"><FONT face="Arial" color="#ffffff" size="2">Exclude Internet 
																Store:</FONT></td>
														<td><asp:CheckBox Runat="server" ID="chkTInternetStore"></asp:CheckBox></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<br>
									<table cellSpacing="0" cellPadding="2" width="100%" bgColor="#000000" border="0">
										<tr>
											<td noWrap bgColor="#990000"><font face="arial" color="white" size="2">GERS Process 
													Criteria</font></td>
										</tr>
										<tr>
											<td bgColor="#990000" colSpan="2">
												<table cellSpacing="0" cellPadding="2" width="100%" bgColor="#000000" border="0">
													<tr>
														<td><FONT face="Arial" color="#ffffff" size="2">GERS Process&nbsp;Status:</FONT></td>
													</tr>
													<tr>
														<td><asp:listbox id="lstGERS" tabIndex="7" runat="server" width="100%" SelectionMode="Multiple"></asp:listbox></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td vAlign="top" width="50%">
									<table cellSpacing="0" cellPadding="2" width="100%" bgColor="#000000" border="0">
										<tr>
											<td noWrap bgColor="#990000"><font face="arial" color="white" size="2">Transfer 
													Criteria</font></td>
										<tr>
											<td bgColor="#990000" colSpan="2">
												<table cellSpacing="0" cellPadding="2" width="100%" bgColor="#000000" border="0">
													<tr>
														<td noWrap><FONT face="Arial" color="#ffffff" size="2">Search Shipping Date By:</FONT></td>
														<td noWrap><asp:radiobuttonlist id="rblSearchDate" AutoPostBack="True" Runat="server" ForeColor="White" Font-Names="Arial" Font-Size="X-Small" RepeatDirection="Horizontal" tabIndex="8">
																<asp:ListItem Value="Specified Date Range">Specified Date Range</asp:ListItem>
																<asp:ListItem Value="Selected Date Range">Selected Date Range</asp:ListItem>
															</asp:radiobuttonlist></td>
													</tr>
													<tr>
														<td noWrap width="1%"><FONT face="Arial" color="#ffffff" size="2">Shipping Date:</FONT></td>
														<td noWrap><asp:textbox id="txtDateF" tabIndex="9" runat="server" Columns="9"></asp:textbox><font face="arial" color="white" size="2">&nbsp;To&nbsp;</font>
															<asp:textbox id="txtDateT" tabIndex="10" runat="server" Columns="9"></asp:textbox>&nbsp;
															<asp:dropdownlist id="ddlDateSearch" Runat="server" tabIndex="11">
																<asp:ListItem Value="Select A Range">Select A Range</asp:ListItem>
																<asp:ListItem Value="All Dates">
																	<ALL DATES>
																</asp:ListItem>
																<asp:ListItem Value="Past 10 Days">Past 10 Days</asp:ListItem>
																<asp:ListItem Value="Past 30 Days">Past 30 Days</asp:ListItem>
																<asp:ListItem Value="Past 60 Days">Past 60 Days</asp:ListItem>
																<asp:ListItem Value="Past 90 Days">Past 90 Days</asp:ListItem>
															</asp:dropdownlist></td>
													</tr>
													<tr>
														<td width="1%"><FONT face="Arial" color="#ffffff" size="2">Transfer #:</FONT></td>
														<td><asp:textbox id="txtTransferNum" tabIndex="12" runat="server" Width="100%"></asp:textbox></td>
													</tr>
													<tr>
														<td><FONT face="Arial" color="#ffffff" size="2">Tracking&nbsp;#:</FONT></td>
														<td><asp:textbox id="txtUPS" tabIndex="13" runat="server" width="100%" MaxLength="18"></asp:textbox></td>
													</tr>
													<tr>
														<td colSpan="2">
															<table cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
																<tr>
																	<td><font face="arial" color="white" size="2">Transfer Type:</font></td>
																	<td><font face="arial" color="white" size="2">Transfer Status:</font></td>
																</tr>
																<tr width="100%">
																	<td width="50%"><asp:listbox id="lstTransType" tabIndex="14" runat="server" Width="100%" SelectionMode="Multiple" Rows="3"></asp:listbox></td>
																	<td width="50%"><asp:listbox id="lstTransStatus" tabIndex="15" runat="server" Width="100%" SelectionMode="Multiple" Rows="3"></asp:listbox></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<br>
									<table cellSpacing="0" cellPadding="2" width="100%" bgColor="#000000" border="0">
										<tr>
											<td noWrap bgColor="#990000"><font face="arial" color="white" size="2">Discrepancy 
													Criteria</font></td>
										</tr>
										<tr bgColor="#990000">
											<td>
												<table cellSpacing="0" cellPadding="2" width="100%" bgColor="#000000" border="0">
													<tr>
														<td noWrap width="1%"><FONT face="Arial" color="#ffffff" size="2">Discrepancy Amount:</FONT></td>
														<td><asp:textbox id="txtdesamtF" tabIndex="16" runat="server" Columns="9"></asp:textbox><font face="arial" color="white" size="2">&nbsp;To&nbsp;</font>
															<asp:textbox id="txtdesamtT" tabIndex="17" runat="server" Columns="9"></asp:textbox></td>
													</tr>
													<tr>
														<td noWrap width="1%"><FONT face="Arial" color="#ffffff" size="2">Discrepancy Quantity:</FONT></td>
														<td><asp:textbox id="txtdiscrepF" tabIndex="18" runat="server" Columns="9"></asp:textbox><font face="arial" color="white" size="2">&nbsp;To&nbsp;</font>
															<asp:textbox id="txtdiscrepT" tabIndex="19" runat="server" Columns="9"></asp:textbox></td>
													</tr>
													<tr>
														<td colSpan="2">
															<table cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
																<tr>
																	<td><font face="arial" color="white" size="2">Discrepancy Status:</font></td>
																</tr>
																<tr>
																	<td><asp:listbox id="lbDiscrepancy" tabIndex="20" runat="server" width="100%" SelectionMode="Multiple" Rows="3"></asp:listbox></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table cellSpacing="0" cellPadding="1" width="100%" border="0">
							<tr align="right">
								<td align="right" colSpan="2"><asp:button id="btnDeleteProfile" Visible="False" Runat="server" Text="Delete Profile" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" BackColor="#440000" BorderColor="#990000" tabIndex="21"></asp:button>&nbsp;
									<asp:button id="btnUpdateProfile" Visible="False" Runat="server" Text="Update Profile" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" BackColor="#440000" BorderColor="#990000" tabIndex="22"></asp:button>&nbsp;
									<asp:button id="btnSaveProfile" tabIndex="23" Runat="server" Text="Save Profile" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" BackColor="#440000" BorderColor="#990000"></asp:button>&nbsp;
									<asp:Button ID="btnSearch" Runat="server" Text="Search" TabIndex="24" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" BackColor="#440000" BorderColor="#990000"></asp:Button>&nbsp;<asp:Button Runat="server" ID="btnClear" Text="Clear" TabIndex="25" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" BackColor="#440000" BorderColor="#990000"></asp:Button></td>
							</tr>
							<tr>
								<td nowrap align="right"><asp:Label ID="lblDelete" Runat="server" Visible="False" ForeColor="Red" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Do you really want to delete this profile?</asp:Label>&nbsp;<asp:Button Runat="server" ID="btnDYes" Text="Yes" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" BackColor="#440000" BorderColor="#990000" Visible="False" tabIndex="26"></asp:Button>&nbsp;<asp:Button Runat="server" ID="btnDNo" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" BackColor="#440000" BorderColor="#990000" Text="No" Visible="False" tabIndex="27"></asp:Button>&nbsp;</td>
							</tr>
							<tr>
								<td noWrap colSpan="2"><asp:label id="lblError" width="100%" Runat="server" ForeColor="White" Font-Names="Arial" Font-Size="Small"></asp:label></td>
							</tr>
						</table>
						<asp:label id="lblProfileName" Runat="server" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" text="Profile Name:" Visible="False">Profile Name:</asp:label><asp:textbox id="txtProfileName" Runat="server" Visible="False" MaxLength="30"></asp:textbox>&nbsp;<asp:label id="lblProDescrip" Runat="server" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" text="Profile Description:" Visible="False">Short Profile Description:</asp:label><asp:textbox id="txtProDescrip" Runat="server" Rows="1" Visible="False"></asp:textbox>&nbsp;<asp:button id="btnSave" Runat="server" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" Text="Save" BackColor="#440000" BorderColor="#990000" Visible="False"></asp:button>&nbsp;<asp:button id="btnCancel" Runat="server" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" Text="Cancel" BackColor="#440000" BorderColor="#990000" Visible="False"></asp:button>
					</td>
				</tr>
				<tr>
					<td colSpan="2"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
