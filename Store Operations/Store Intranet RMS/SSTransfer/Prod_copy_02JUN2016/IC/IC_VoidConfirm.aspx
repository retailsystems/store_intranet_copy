<%@ Register TagPrefix="uc1" TagName="Header" Src="../Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../Layout/Footer.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_VoidConfirm.aspx.vb" Inherits="SSTransfer.IC_VoidConfirm"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Void Confirmation</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="../Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="../Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="../Javascript/AllowOneSubmit.js" language="JavaScript"></script>
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="yes" rightMargin="0">
		<form id="frmVConfirmation" method="post" runat="server" onsubmit="return checkSubmit();">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<TBODY>
					<tr>
						<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
					</tr>
					<tr>
						<td vAlign="top">
							<table cellSpacing="0" cellPadding="4" width="100%" bgColor="#000000" border="0">
								<tr>
									<td></td>
								</tr>
								<tr>
									<td noWrap align="middle"><asp:label id="lblVoidConfirm" Font-Names="Arial" Font-Size="Medium" Font-Bold="True" BackColor="Black" ForeColor="White" Runat="server">Do you really want to void this transfer?</asp:label>&nbsp;
										<asp:button id="btnYes" Font-Names="Arial" Font-Size="Small" Font-Bold="True" BackColor="#440000" ForeColor="White" Runat="server" BorderColor="#990000" Text="Yes"></asp:button>&nbsp;
										<asp:button id="btnNo" Font-Names="Arial" Font-Size="Small" Font-Bold="True" BackColor="#440000" ForeColor="White" Runat="server" BorderColor="#990000" Text="No"></asp:button></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
					</tr>
				</TBODY>
			</table>
		</form>
		</TR></TBODY></TABLE></FORM>
	</body>
</HTML>
