<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_ResultsTT.aspx.vb" Inherits="SSTransfer.IC_ResultsTT"%>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../Layout/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>IC Results</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="../Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="../Javascript/AllowOneSubmit.js"></script>
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="frmICResultsTT" method="post" runat="server" onsubmit="return checkSubmit();">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<TBODY>
					<tr>
						<td noWrap colSpan="3" height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
					</tr>
					<tr>
					</tr>
					<tr vAlign="top" height="1%">
						<td noWrap><asp:label id="lblView" Font-Bold="True" ForeColor="White" Runat="server" Font-Size="X-Small" Font-Names="Arial">View</asp:label>&nbsp;
							<asp:dropdownlist id="ddlView" Runat="server" AutoPostBack="True">
								<asp:ListItem Value="TransferType" Selected="True">By Transfer Type</asp:ListItem>
								<asp:ListItem Value="Transfer">By Transfer</asp:ListItem>
							</asp:dropdownlist></td>
						<td noWrap></td>
						<td noWrap align="right"><asp:Label ID="lblNR" Runat="server" Font-Size="XX-Small" Font-Names="Arial" ForeColor="White">NR-Not Received.</asp:Label><asp:label id="lblND" ForeColor="White" Runat="server" Font-Size="XX-Small" Font-Names="Arial">ND-No Discrepancy</asp:label>&nbsp;
							<asp:label id="lblNI" ForeColor="White" Runat="server" Font-Size="XX-Small" Font-Names="Arial">NI-Not Investigated</asp:label>&nbsp;
							<asp:label id="lblIA" ForeColor="White" Runat="server" Font-Size="XX-Small" Font-Names="Arial">IA-Investigated/Adjusted</asp:label>&nbsp;
							<asp:label id="lblIN" ForeColor="WHite" Runat="server" Font-Size="XX-Small" Font-Names="Arial">IN-Investigated/Not Adjusted</asp:label>&nbsp;
						</td>
					</tr>
					<tr vAlign="top">
						<td noWrap colSpan="3"><br>
							<div style="OVERFLOW-Y: auto; SCROLLBAR-HIGHLIGHT-COLOR: #990000; WIDTH: 100%; SCROLLBAR-ARROW-COLOR: #ffffff; SCROLLBAR-BASE-COLOR: #440000; HEIGHT: 410px">
								<table cellSpacing="0" cellPadding="2" width="100%" border="0">
									<tr>
										<td>
											<asp:datagrid id="dgTTResults" Runat="server" height="100%" Font-Size="X-Small" Font-Names="Arial" GridLines="None" BorderStyle="None" CellPadding="2" CellSpacing="1" BackColor="#111111" Width="100%" AutoGenerateColumns="False">
												<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
												<ItemStyle Font-Size="X-Small" Font-Names="Arial" ForeColor="White"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Font-Names="arial" Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn HeaderText="Transfer Number">
														<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="White" Width="12%" BackColor="#440000"></HeaderStyle>
														<ItemStyle HorizontalAlign="Left"></ItemStyle>
														<ItemTemplate>
															<a href='IC_EditTransfer.aspx?BID=<%# Container.dataitem("Box_Id") %>&RTNTT=<%# GetRTNEditTransfer() %>'>
																<asp:Label Runat="server" ID="lblTN" Text='<%# Container.dataitem("Box_Id") %>'>
																</asp:Label></a>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn HeaderText="From Store" DataField="Sending_Store"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="To Store" DataField="Receiving_Store"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Transfer Type" DataField="Xfer_Desc"></asp:BoundColumn>
													<asp:BoundColumn DataField="Shipment_Date" dataformatstring="{0:d}" HeaderText="Shipment Date" ReadOnly="True"></asp:BoundColumn>
													<asp:BoundColumn DataField="Status_Desc" HeaderText="Box Status"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="GERS Status" DataField="GERSStatus_Desc"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Discrep. " DataField="Disc_Desc"></asp:BoundColumn>
													<asp:TemplateColumn HeaderText="Comments">
														<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" Width="5%" BackColor="#440000"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<a href='IC_Comment.aspx?BID=<%# Container.dataitem("Box_Id") %>&XT=<%# Container.dataitem("Xfer_Type_Cd") %>&RTNRESULTSTT=<%# GetReturnQuery() %>'>
																<asp:Image Runat="server" ID="Image1" ImageUrl='<%# GetBoxComments(Container.dataitem("Box_Id"),Container.dataitem("Xfer_Type_Cd")) %>'>
																</asp:Image></a>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn Visible="True" HeaderText="E">
														<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:checkbox ID="chkEItem" Runat="server"></asp:checkbox>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
											</asp:datagrid></td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td noWrap align="right" colSpan="3"><asp:dropdownlist id="ddlOptions" Runat="server">
								<asp:ListItem Value="<OPTIONS>"></asp:ListItem>
								<asp:ListItem Value="Print Discrepancy"></asp:ListItem>
								<asp:ListItem Value="Print GERS"></asp:ListItem>
								<asp:ListItem Value="Print Search Results"></asp:ListItem>
								<asp:ListItem Value="Update GERS"></asp:ListItem>
								<asp:ListItem Value="Check-In DC"></asp:ListItem>
							</asp:dropdownlist>&nbsp;<asp:button id="btnExecute" Font-Bold="True" ForeColor="White" Runat="server" Font-Size="X-Small" Font-Names="Arial" BackColor="#440000" Text="Execute" BorderColor="#990000"></asp:button></td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td noWrap colSpan="3" height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
					</tr>
				</TBODY>
			</table>
		</form>
		</TR></TBODY></TABLE></FORM>
	</body>
</HTML>
