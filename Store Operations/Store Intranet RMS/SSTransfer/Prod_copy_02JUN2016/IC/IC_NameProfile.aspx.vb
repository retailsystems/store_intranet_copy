Public Class IC_NameProfile
    Inherits System.Web.UI.Page
    Protected WithEvents ucHeader As Header
    Protected WithEvents lblProfileType As System.Web.UI.WebControls.Label
    Protected WithEvents txtProfileName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtProDescrip As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblProfileName As System.Web.UI.WebControls.Label
    Protected WithEvents lblProDescrip As System.Web.UI.WebControls.Label
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents rblProfileType As System.Web.UI.WebControls.RadioButtonList
    'Public ProfileType As String
    Public ProfileName As String
    Public ProfileDescrip As String
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Public intPID As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        ucHeader.lblTitle = "Save Profile"
        ucHeader.CurrentPage(Header.PageName.IC_Profiles)
        ucHeader.CurrentMode(Header.HeaderGroup.IC)
        intPID = Request.Params("PID")
        'ProfileType = "Personal"
        If Not IsPostBack Then
            txtProfileName.Text = ""
            txtProDescrip.Text = ""
            lblError.Text = ""
            Call FillInfo()
        End If
    End Sub
    Private Sub FillInfo()
        'Fill info from previously saved profile.
        Dim NullCheck As Boolean
        Dim parmProName As SqlClient.SqlParameter
        Dim parmProDesc As SqlClient.SqlParameter
        'Dim parmProType As SqlClient.SqlParameter

        Dim conGetPro As System.Data.SqlClient.SqlConnection
        Dim cmdGetPro As System.Data.SqlClient.SqlCommand
        conGetPro = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdGetPro = New SqlClient.SqlCommand("spFillProName", conGetPro)
        cmdGetPro.CommandType = CommandType.StoredProcedure
        cmdGetPro.Parameters.Add("@PID", intPID)
        parmProName = cmdGetPro.Parameters.Add("@ProName", SqlDbType.VarChar)
        parmProDesc = cmdGetPro.Parameters.Add("@ProDesc", SqlDbType.VarChar)
        'parmProType = cmdGetPro.Parameters.Add("@ProType", SqlDbType.VarChar)
        parmProName.Size = 50
        parmProDesc.Size = 150
        'parmProType.Size = 50
        parmProName.Direction = ParameterDirection.Output
        parmProDesc.Direction = ParameterDirection.Output
        'parmProType.Direction = ParameterDirection.Output
        conGetPro.Open()
        cmdGetPro.ExecuteNonQuery()
        conGetPro.Close()

        NullCheck = IsDBNull(cmdGetPro.Parameters("@ProName").Value)
        If NullCheck = True Then
            txtProfileName.Text = ""
        Else
            txtProfileName.Text = cmdGetPro.Parameters("@ProName").Value
        End If

        NullCheck = IsDBNull(cmdGetPro.Parameters("@ProDesc").Value)
        If NullCheck = True Then
            txtProDescrip.Text = ""
        Else
            txtProDescrip.Text = cmdGetPro.Parameters("@ProDesc").Value
        End If

        ' NullCheck = IsDBNull(cmdGetPro.Parameters("@ProType").Value)
        'If NullCheck = True Then
            'txtProType.Text = ""
        'rblProfileType.SelectedItem.Value = "Personal"
        'ProfileType = "Personal"
        'Else
            'txtProType.Text = cmdGetPro.Parameters("@ProType").Value
            'cmdGetPro.Parameters("@ProType").Value
        'ProfileType = cmdGetPro.Parameters("@ProType").Value
        ' rblProfileType.SelectedIndex = rblProfileType.Items.IndexOf(rblProfileType.Items.FindByValue(ProfileType.ToString))

        'End If
        cmdGetPro.Dispose()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ProfileName = txtProfileName.Text
        ProfileDescrip = txtProDescrip.Text
      
        If (txtProfileName.Text <> "" And txtProDescrip.Text <> "") Then
            Dim conInsertProfile As System.Data.SqlClient.SqlConnection
            Dim cmdInsertProfile As System.Data.SqlClient.SqlCommand
            conInsertProfile = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            cmdInsertProfile = New SqlClient.SqlCommand("spSaveProfile", conInsertProfile)
            cmdInsertProfile.CommandType = CommandType.StoredProcedure
            cmdInsertProfile.Parameters.Add("@ProfileName", ProfileName)
            cmdInsertProfile.Parameters.Add("@ProDescrip", ProfileDescrip)
            'cmdInsertProfile.Parameters.Add("@ProfileType", ProfileType)
            cmdInsertProfile.Parameters.Add("@PID", intPID)
            conInsertProfile.Open()
            cmdInsertProfile.ExecuteNonQuery()
            conInsertProfile.Close()
            cmdInsertProfile.Dispose()
            Response.Redirect("IC_ProfileList.aspx")
        ElseIf (txtProfileName.Text = "" And txtProDescrip.Text <> "") Then
            lblError.Text = "Please enter a name for your profile."
        ElseIf (txtProfileName.Text <> "" And txtProDescrip.Text = "") Then
            lblError.Text = "Please enter a profile description."
        ElseIf (txtProfileName.Text = "" And txtProDescrip.Text = "") Then
            lblError.Text = "Please enter a name for your profile."
        End If

    End Sub
    
    Private Sub txtProfileName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProfileName.TextChanged
        ProfileName = txtProfileName.Text
    End Sub
    Private Sub txtProDescrip_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProDescrip.TextChanged
        ProfileDescrip = txtProDescrip.Text
    End Sub
End Class
