<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_DCMaint.aspx.vb" Inherits="SSTransfer.IC_DCMaint"%>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../Layout/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>IC DC Maintenance</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="../Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="../Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="../Javascript/AllowOneSubmit.js"></script>
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="yes" rightMargin="0">
		<form id="frmICDCMaint" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td noWrap colSpan="3" height="1" width="100%"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
				</tr>
				<tr valign="top">
					<td nowrap colSpan="3" height="1%"><asp:Label Runat="server" id="lblTrans" Visible="True" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="Small">Transfer Number:</asp:Label>&nbsp;<asp:TextBox Runat="server" ID="txtTransNum" tabIndex="1"></asp:TextBox>&nbsp;<asp:Button Runat="server" ID="btnSubmit" Text="Submit" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small" BackColor="#440000" BorderColor="#990000" tabIndex="2"></asp:Button></td>
				</tr>
				<tr valign="top" width="100%">
					<td nowrap width="100%" colSpan="3"><br>
						<asp:Label Runat="server" ID="lblError" Width="100%" Font-Size="Small" Font-Names="Arial" Font-Bold="True" ForeColor="Red"></asp:Label></td>
				</tr>
				</TR>
				<tr>
					<td noWrap colSpan="3" height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
