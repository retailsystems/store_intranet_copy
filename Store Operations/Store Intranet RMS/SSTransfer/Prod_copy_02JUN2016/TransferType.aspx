<%@ Page Language="vb" AutoEventWireup="false" Codebehind="TransferType.aspx.vb" Inherits="SSTransfer.TransferType"%>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Transfer Home</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" alink="white" vlink="white" link="white">
		<form id="frmTransferHome" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgcolor="#000000" border="0">
				<tr>
					<td height="1"><uc1:Header id="layoutHeader" runat="server"></uc1:Header></td>
				</tr>
				<tr>
					<td align="middle">
						<a href="NewTransferForm.aspx?TType=1"><font size="2" color="red" face="arial"><b>STORE TO STORE</b></font></a><br><br>
						<a href="NewTransferForm.aspx?TType=2"><font size="2" color="red" face="arial"><b>RETURN</b></font></a>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:Footer id="Footer1" runat="server"></uc1:Footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>