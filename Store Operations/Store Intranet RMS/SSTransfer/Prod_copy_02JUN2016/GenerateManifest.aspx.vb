Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Export.Pdf
Imports DataDynamics.ActiveReports.Document.Margins

Public Class ActiveManifest
    Inherits System.Web.UI.Page

    Private m_BoxId As String
    Private m_intTType As Integer
    Private m_booWAN As Boolean

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        m_BoxId = Request("BID")
        'm_intTType = Request("TType")
        'm_booNONWAN = Request("NONWAN")

        GetHeader()

        If m_intTType = 1 Then 'Store to Store transfer
            StreamSTOSPDF()
        ElseIf m_intTType = 2 Then 'Returns
            StreamReturnsPDF()
        End If

    End Sub

    Private Sub StreamSTOSPDF()

        Dim rpt As New StoreToStore(m_BoxId, m_booWAN, ConfigurationSettings.AppSettings("ManifestImage"))

        rpt.PageSettings.Margins.Left = 0.5
        rpt.PageSettings.Margins.Right = 0.5
        rpt.PageSettings.Margins.Bottom = 0.5
        rpt.PageSettings.Margins.Top = 0.5

        rpt.Document.Printer.PrinterName = ""

        Try
            rpt.Run()
        Catch eRunReport As Exception
            ' Failure running report, just report the error to the user:
            Response.Clear()
            Response.Write("<h1>Error running report:</h1>")
            Response.Write(eRunReport.ToString())
            Exit Sub
        End Try

        ' Buffer this pages output until the PDF is complete.
        Response.Buffer = True

        ' Clear any part of this page that might have already been buffered for output.
        Response.ClearContent()
        ' Clear any headers that might have already been buffered (such as the content type for an HTML page)
        Response.ClearHeaders()
        ' Tell the browser and the "network" that this resulting data of this page should be be cached since this could be a dynamic report that changes upon each request.
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        ' Tell the browser this is a PDF document so it will use an appropriate viewer.
        Response.ContentType = "application/pdf"

        Response.AddHeader("content-disposition", "inline; filename=Manifest.pdf")

        ' Create the PDF export object
        Dim pdf As PdfExport = New PdfExport()

        ' Create a new memory stream that will hold the pdf output
        Dim memStream As System.IO.MemoryStream = New System.IO.MemoryStream()
        ' Export the report to PDF:
        pdf.Export(rpt.Document, memStream)
        ' Write the PDF stream out
        Response.BinaryWrite(memStream.ToArray())

        ' Send all buffered content to the client
        Response.End()

    End Sub

    Private Sub GetHeader()

        Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim strStoredProcedure As String
        Dim objDataReader As SqlClient.SqlDataReader

        objSQLConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeader '" & m_BoxId & "' ", objSQLConnection)

        objDataReader = objCommand.ExecuteReader()

        objDataReader.Read()

        m_intTType = objDataReader("TType")
        m_booWAN = objDataReader("ReceivingStoreWAN")

        objCommand.Dispose()
        objDataReader.Close()
        objSQLConnection.Close()
        objSQLConnection.Dispose()

    End Sub

    Private Sub StreamReturnsPDF()

        Dim rpt As New Returns(m_BoxId, ConfigurationSettings.AppSettings("ManifestImage"))

        rpt.PageSettings.Margins.Left = 0.5
        rpt.PageSettings.Margins.Right = 0.5
        rpt.PageSettings.Margins.Bottom = 0.5
        rpt.PageSettings.Margins.Top = 0.5

        Try
            rpt.Run()
        Catch eRunReport As Exception
            ' Failure running report, just report the error to the user:
            Response.Clear()
            Response.Write("<h1>Error running report:</h1>")
            Response.Write(eRunReport.ToString())
            Exit Sub
        End Try

        ' Buffer this pages output until the PDF is complete.
        Response.Buffer = True

        ' Clear any part of this page that might have already been buffered for output.
        Response.ClearContent()
        ' Clear any headers that might have already been buffered (such as the content type for an HTML page)
        Response.ClearHeaders()
        ' Tell the browser and the "network" that this resulting data of this page should be be cached since this could be a dynamic report that changes upon each request.
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        ' Tell the browser this is a PDF document so it will use an appropriate viewer.
        Response.ContentType = "application/pdf"

        ' Create the PDF export object
        Dim pdf As PdfExport = New PdfExport()

        ' Create a new memory stream that will hold the pdf output
        Dim memStream As System.IO.MemoryStream = New System.IO.MemoryStream()
        ' Export the report to PDF:
        pdf.Export(rpt.Document, memStream)
        ' Write the PDF stream out
        Response.BinaryWrite(memStream.ToArray())

        ' Send all buffered content to the client
        Response.End()

    End Sub


End Class
