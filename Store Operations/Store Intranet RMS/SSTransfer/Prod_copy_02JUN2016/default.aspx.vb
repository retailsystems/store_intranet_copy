Public Class WebForm_TransferHome
    Inherits System.Web.UI.Page

    Protected WithEvents layoutHeader As Header
    'Protected WithEvents TabStripv As Microsoft.Web.UI.WebControls.TabStrip
    'Protected WithEvents TabStripView As Microsoft.Web.UI.WebControls.MultiPage
    'Protected WithEvents ucSearchBox As FilterBox
    Protected WithEvents grdArrivals As DataGrid
    Protected WithEvents grdShipped As DataGrid
    Protected WithEvents grdSavedTransfers As DataGrid
    Protected WithEvents grdPrintQueue As DataGrid
    'Protected WithEvents tsGrids As Microsoft.Web.UI.WebControls.TabStrip
    Protected WithEvents lblHelpLabel As Label
    Protected WithEvents lblLegend As Label
    Protected WithEvents dgSavedCheckIns As DataGrid

    Protected WithEvents phEA As PlaceHolder
    Protected WithEvents phRS As PlaceHolder
    Protected WithEvents phP As PlaceHolder
    Protected WithEvents phU As PlaceHolder
    Protected WithEvents phPS As PlaceHolder

    Protected WithEvents TDEA As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDRS As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDP As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDU As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDPS As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents EAFont As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents RSFont As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents PFont As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents UFont As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents PSFont As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

    Private m_objUserInfo As New UserInfo()
    Private m_intSelectedTab As Int16
    Private m_bytSortArrival As Byte = 0
    Private m_bytSortShipped As Byte = 0
    Private m_bytSortSavedTransfers As Byte = 0
    Private m_bytSortSavedCheckIns As Byte = 0
    Private m_bytSortPrintQueue As Byte = 0

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strLegend As String

        m_objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        'If employee is logged on update last Trans
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
        End If

        'If store number not set then redirect to set store page
        If m_objUserInfo.StoreNumber = -1 Or m_objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

        'Get variables from quesrystring
        m_intSelectedTab = Request("Tab")

        If m_intSelectedTab.ToString.Length <= 0 Or m_intSelectedTab = Nothing Then
            m_intSelectedTab = 1 'Expected Arrivals
        End If

        'Sets both header title and highlights section button
        layoutHeader.lblTitle = "Manage Store Transfers"
        layoutHeader.CurrentMode(Header.HeaderGroup.Transfer)
        layoutHeader.CurrentPage(Header.PageName.Home)

        'If Not Page.IsPostBack Then

        'Fills the datagrids on the home page
        'If ViewState("Filter") = "" Then

        'lblLegend.Text = GetTransferTypeLegend()
        ' Else
        'FillDataGrids(1, ViewState("Filter")) ' Display filtered results
        ' End If

        'End If

        'Mark selected tab
        Select Case m_intSelectedTab

            Case 1 'Expected Arrivals
                'TDEA.Attributes("bgcolor") = "#FF9ACE"
                'EAFont.Attributes("color") = "#000000"
                TDEA.Attributes("class") = "sInfoTabMouseOver"
                'EAFont.Attributes("class") = "sInfoTab"
                phEA.Visible = True
                FillDataGrids(1, "") 'Display all results
                TDEA.Attributes("onmouseout") = ""

            Case 2 'Recently Shipped
                'TDRS.Attributes("bgcolor") = "#FF9ACE"
                'RSFont.Attributes("color") = "#000000"
                TDRS.Attributes("class") = "sInfoTabMouseOver"
                'RSFont.Attributes("class") = "sInfoTab"
                phRS.Visible = True
                FillDataGrids(2, "") 'Display all results
                TDRS.Attributes("onmouseout") = ""

            Case 3 'Packing
                'TDP.Attributes("bgcolor") = "#FF9ACE"
                'PFont.Attributes("color") = "#000000"
                TDP.Attributes("class") = "sInfoTabMouseOver"
                'PFont.Attributes("class") = "sInfoTab"
                phP.Visible = True
                FillDataGrids(3, "") 'Display all results
                TDP.Attributes("onmouseout") = ""

            Case 4 'Unpacking
                'TDU.Attributes("bgcolor") = "#FF9ACE"
                'UFont.Attributes("color") = "#000000"
                TDU.Attributes("class") = "sInfoTabMouseOver"
                'UFont.Attributes("class") = "sInfoTab"
                phU.Visible = True
                FillDataGrids(4, "") 'Display all results
                TDU.Attributes("onmouseout") = ""

            Case 5 'Print Shipped
                'TDPS.Attributes("bgcolor") = "#FF9ACE"
                'PSFont.Attributes("color") = "#000000"
                TDPS.Attributes("class") = "sInfoTabMouseOver"
                'PSFont.Attributes("class") = "sInfoTab"
                phPS.Visible = True
                FillDataGrids(5, "") 'Display all results
                TDPS.Attributes("onmouseout") = ""

            Case Else 'Expected Arrivals
                'TDEA.Attributes("bgcolor") = "#FF9ACE"
                'EAFont.Attributes("color") = "#000000"
                TDEA.Attributes("class") = "sInfoTabMouseOver"
                EAFont.Attributes("class") = "sInfoTab"
                phEA.Visible = True
                FillDataGrids(1, "") 'Display all results
                TDEA.Attributes("onmouseout") = ""

        End Select

        Select Case m_intSelectedTab
            Case 1
                lblHelpLabel.Text = "All incoming transfers sent to this store. Use Search to locate older Transfers."
            Case 2
                lblHelpLabel.Text = "All non-received transfers sent from this Store. Use Search to locate older Transfers."
            Case 3
                lblHelpLabel.Text = "New Transfers originating at this store that were saved for later. These transfers have a status of Packing."
            Case 4
                lblHelpLabel.Text = "Transfers sent to your store that were saved for later. These transfers have a status of Unpacking."
            Case 5
                lblHelpLabel.Text = "Select a Transfer Manifest to print by clicking on the icon. Use Search button to locate older Transfer Manifests"
        End Select

        'Add Legend to infobar
        lblHelpLabel.Text = lblHelpLabel.Text

    End Sub

    'This function returns 
    Private Function GetTransferTypeLegend() As String

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim strReturn As String

        objConnection.Open() 'open connection

        strStoredProcedure = "spGetTransferTypeLegend"

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader() 'fill datareader

        Do While objDataReader.Read
            strReturn = strReturn & "<B>" & objDataReader("Xfer_Short_Desc") & "</b> - " & objDataReader("Xfer_Desc") & "&nbsp;&nbsp;&nbsp;"
        Loop

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        If Len(strReturn) > 0 Then
            strReturn = Mid(strReturn, 1, Len(strReturn) - 1)
        Else
            strReturn = ""
        End If

        Return strReturn


    End Function

    'Rebinds sorted data to the expected arrivals datagrid
    Public Sub SortCommand_Arrivals(ByVal Source As Object, ByVal E As DataGridSortCommandEventArgs)

        ' m_bytSortArrival = E.SortExpression

        'This block of code sorts both filtered and non-filtered data
        'If ViewState("Filter") <> "" Then
        ' FillDataGrids(1, ViewState("Filter"))
        ' Else
        ' FillDataGrids(0, "")
        ' End If

    End Sub

    'Rebinds sorted data to the Recently shipped datagrid
    Public Sub SortCommand_Shipped(ByVal Source As Object, ByVal E As DataGridSortCommandEventArgs)

        'm_bytSortShipped = E.SortExpression

        'This block of code sorts both filtered and non-filtered data
        ' If ViewState("Filter") <> "" Then
        'FillDataGrids(1, ViewState("Filter"))
        ' Else
        'FillDataGrids(0, "")
        'End If

    End Sub

    'Rebinds sorted data to the packing datagrid
    Public Sub SortCommand_SavedTransfers(ByVal Source As Object, ByVal E As DataGridSortCommandEventArgs)

        ' m_bytSortSavedTransfers = E.SortExpression

        'This block of code sorts both filtered and non-filtered data
        'If ViewState("Filter") <> "" Then
        'FillDataGrids(1, ViewState("Filter"))
        'Else
        'FillDataGrids(0, "")
        'End If

    End Sub

    'Rebinds sorted data to the unpacking datagrids
    Public Sub SortCommand_SavedCheckIns(ByVal Source As Object, ByVal E As DataGridSortCommandEventArgs)

        'm_bytSortSavedCheckIns = E.SortExpression

        'This block of code sorts both filtered and non-filtered data
        'If ViewState("Filter") <> "" Then
        ' FillDataGrids(1, ViewState("Filter"))
        ' Else
        ' FillDataGrids(0, "")
        ' End If

    End Sub

    Public Function GetTrackingURL(ByVal TrackingURL As String, ByVal TrackingNum As String) As String

        If TrackingNum.Length > 0 Then
            Return Replace(TrackingURL, "{0}", TrackingNum)
        Else
            Return ""
        End If

    End Function

    'Rebinds sorted data to the print history datagrid
    Public Sub SortCommand_PrintQueue(ByVal Source As Object, ByVal E As DataGridSortCommandEventArgs)

        'm_bytSortPrintQueue = E.SortExpression

        'This block of code sorts both filtered and non-filtered data
        ' If ViewState("Filter") <> "" Then
        'FillDataGrids(1, ViewState("Filter"))
        'Else
        ' FillDataGrids(0, "")
        'End If

    End Sub

    Public Function LabelImage(ByVal ParcelService As String, ByVal TrackingNumber As String)
        If ParcelService = "1" Then
            Return "<img border=""0"" id=""imgUPS"" src=""Images\ups.gif"" onclick=""javascript:window.open('GenerateUPS.aspx?Label=" & Server.UrlEncode("UPSLabels\ShippingLabel_") & TrackingNumber & ".gif','UPS','toolbar=1,center=1,scrollbar=no,status=no,width=700,height=400');"">"
        ElseIf ParcelService = "2" Then
            Return "<img border=""0"" id=""imgUPS"" src=""Images\fedex.gif"" onclick=""javascript:window.open('" & Server.UrlEncode("UPSLabels\ShippingLabel_") & TrackingNumber & ".pdf','FedEx','toolbar=1,center=1,scrollbar=no,status=no,width=700,height=400');"">"
        Else
            Return Nothing
        End If
    End Function

    'Arguments: 
    '   booFilterOn - True/False value used to select the correct stored procedure
    '   strFilterString - String value used to filter data grids
    'Function:
    '   This sub gathers the necessary data from the database and then binds the information to the 4 data grids displayed on the
    '   home page. This sub has code in place which effects the way data is bound to the data grids depending on the
    '   clients IE browser version. If the client has IE version 6 or grater, then all of the grids are filled. 
    '   If the client does not have IE 6 or higher, then only the selected data grid is filled. Data binding is treated differently
    '   due to the functionality of the Tab Strip object.
    Private Sub FillDataGrids(ByVal booFilterOn As Boolean, ByVal strFilterString As String)

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        ' Dim bc As HttpBrowserCapabilities
        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        'bc = Request.Browser 'Gets clients browser information

        objConnection.Open()

        'Fills Expected Arrivals data grid
        If (m_intSelectedTab = 1) Then

            'If booFilterOn Then
            ' strStoredProcedure = "spGetFilterArrivals " & m_objUserInfo.StoreNumber & ",'" & strFilterString & "'," & m_bytSortArrival
            'Else
            strStoredProcedure = "spGetArrivals " & m_objUserInfo.StoreNumber & "," & m_bytSortArrival
            'End If

            objCommand.CommandText = strStoredProcedure

            objDataReader = objCommand.ExecuteReader()

            grdArrivals.DataSource = objDataReader
            grdArrivals.DataBind()
            objCommand.Dispose()
            objDataReader.Close()

        End If

        'Fills Recently Shipped data grid
        If (m_intSelectedTab = 2) Then

            'If booFilterOn Then
            'strStoredProcedure = "spGetFilterShipped " & m_objUserInfo.StoreNumber & ",'" & strFilterString & "'," & m_bytSortShipped
            'Else
            strStoredProcedure = "spGetShipped " & m_objUserInfo.StoreNumber & "," & m_bytSortShipped
            'End If

            objCommand.CommandText = strStoredProcedure

            objDataReader = objCommand.ExecuteReader()

            grdShipped.DataSource = objDataReader
            grdShipped.DataBind()
            objCommand.Dispose()
            objDataReader.Close()

        End If

        'Fills Packing data grid
        If (m_intSelectedTab = 3) Then

            'If booFilterOn Then
            'strStoredProcedure = "spGetFilterSavedTrans " & m_objUserInfo.StoreNumber & ",'" & strFilterString & "'," & m_bytSortSavedTransfers
            'Else
            strStoredProcedure = "spGetSavedTransfers " & m_objUserInfo.StoreNumber & "," & m_bytSortSavedTransfers
            'End If

            objCommand.CommandText = strStoredProcedure
            objDataReader = objCommand.ExecuteReader()

            grdSavedTransfers.DataSource = objDataReader
            grdSavedTransfers.DataBind()
            objCommand.Dispose()
            objDataReader.Close()

        End If

        'Fills Unpacking data grid
        If (m_intSelectedTab = 4) Then

            'If booFilterOn Then
            'strStoredProcedure = "spGetFilterSavedCheckIn " & m_objUserInfo.StoreNumber & ",'" & strFilterString & "'," & m_bytSortSavedCheckIns
            'Else
            strStoredProcedure = "spGetSavedCheckIn " & m_objUserInfo.StoreNumber & "," & m_bytSortSavedCheckIns
            'End If

            objCommand.CommandText = strStoredProcedure
            objDataReader = objCommand.ExecuteReader()

            dgSavedCheckIns.DataSource = objDataReader
            dgSavedCheckIns.DataBind()
            objCommand.Dispose()
            objDataReader.Close()

        End If

        'Fills Print history data grid
        If (m_intSelectedTab = 5) Then

            'If booFilterOn Then
            'strStoredProcedure = "spGetFilterBoxPrinted " & m_objUserInfo.StoreNumber & ",'" & strFilterString & "'," & m_bytSortPrintQueue
            'Else
            strStoredProcedure = "spGetBoxPrinted " & m_objUserInfo.StoreNumber & "," & m_bytSortPrintQueue
            'End If

            objCommand.CommandText = strStoredProcedure
            objDataReader = objCommand.ExecuteReader()

            grdPrintQueue.DataSource = objDataReader
            grdPrintQueue.DataBind()

            objCommand.Dispose()
            objDataReader.Close()

        End If

        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Arguments:
    '   Box_Id - The box id
    'Function:
    '   This function returns abbreviations of all the transfer types associated to the selected box
    Protected Function GetBoxTransferTypes(ByVal Box_Id As String) As String

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim strReturn As String

        objConnection.Open()

        strStoredProcedure = "spGetBoxTransferTypes '" & Box_Id & "' "

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader()

        Do While objDataReader.Read
            strReturn = strReturn & objDataReader("Xfer_Short_Desc") & ","
        Loop

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        If Len(strReturn) > 0 Then
            strReturn = Mid(strReturn, 1, Len(strReturn) - 1)
        Else
            strReturn = ""
        End If

        Return strReturn

    End Function

    'This event is fired when the filter button is pressed(Filter Off)
    ' Sub ucSearchBox_OnFilterOff() Handles ucSearchBox.OnFilterOff

    ' FillDataGrids(0, "") 'Binds unfiltered data to all of the data grids
    ' ViewState("Filter") = ""

    ' End Sub

    'This event is fired when the filter button is pressed(Filter On)
    ' Sub ucSearchBox_OnFilterOn(ByVal FilterString As String) Handles ucSearchBox.OnFilterOn

    '  FillDataGrids(1, FilterString) 'Binds filtered data to all of the grids
    '   ViewState("Filter") = FilterString

    ' End Sub

    'Function:
    'This functions re binds data to the selected data grid only if the client is not using IE version 6 or higher
    'Private Sub tsGrids_SelectedIndexChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsGrids.SelectedIndexChange

    'Dim bc As HttpBrowserCapabilities

    'bc = Request.Browser

    'If bc.Version < 6 Then
    'FillDataGrids(0, "")
    ' End If

    'End Sub

End Class
