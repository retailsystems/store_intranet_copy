
Imports System.Data.OleDb

Public Class StoreLogin

    Inherits System.Web.UI.Page

    'Objects
    Protected WithEvents lblLoginMsg As System.Web.UI.WebControls.Label
    Protected WithEvents dgVendors As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ddlDept As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnDispVend As System.Web.UI.WebControls.Button
    Protected WithEvents btnAddDept As System.Web.UI.WebControls.Button
    Protected WithEvents btnDelDept As System.Web.UI.WebControls.Button
    Protected WithEvents btnOk As System.Web.UI.WebControls.Button
    Protected WithEvents txtRemVend As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnRemVend As System.Web.UI.WebControls.Button
    Protected WithEvents txtAddVend As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnAddVend As System.Web.UI.WebControls.Button
    Protected WithEvents btnStAddVend As System.Web.UI.WebControls.Button
    Protected WithEvents lblVendAction As System.Web.UI.WebControls.Label
    Protected WithEvents btnStDelVend As System.Web.UI.WebControls.Button
    Protected WithEvents txtAddDept As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblAddDept As System.Web.UI.WebControls.Label
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents btnAddAllVend As System.Web.UI.WebControls.Button
    Protected WithEvents btnDelAllVend As System.Web.UI.WebControls.Button
    Public intDept As Integer

    'connstrings
    Dim connHeader As System.Data.SqlClient.SqlConnection
    Dim connOra As System.Data.OleDb.OleDbConnection

    'globals
    Dim gDept As Integer
    Dim gIndex As Integer

    Dim gToDelete As Integer
    Dim AddVar, AddVend, RemVend, Go As Integer

    'New 
    Dim myDataTable As DataTable
    Dim gVend As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Go = 1

        lblLoginMsg.Visible = False
        btnOk.Visible = False
        dgVendors.Visible = False

        txtRemVend.Visible = False
        txtAddVend.Visible = False
        btnRemVend.Visible = False
        btnAddVend.Visible = False
        lblVendAction.Visible = False

        btnStAddVend.Visible = False
        btnStDelVend.Visible = False
        btnAddAllVend.Visible = False
        btnDelAllVend.Visible = False

        txtAddDept.Visible = False
        lblAddDept.Visible = False
        btnCancel.Visible = False

        'Take a slice of the Department Selected on Last Iteration, index and value
        GetDept()

        'build the pull down, pull down remembers the last item selected
        'buildDeptPD()
        If Not IsPostBack Then
            buildDeptPD()
        End If
        'ddlDept.SelectedIndex = gIndex

    End Sub

    '-----------------------------------------------------------------------------
    ' Action when Cancel Button 
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click


        If btnOk.Text = "Add Dept" Then
            CloseAddControls()
        End If


        If btnOk.Text = "Delete" Then
            CloseDelControls()
        End If

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub GetDept()

        If Not ddlDept.SelectedIndex < 0 Then

            If ddlDept.SelectedIndex = -1 Then 'ensures that initiated for the page load
                ddlDept.SelectedIndex = 0
            End If

            gDept = CInt(Left(ddlDept.SelectedItem.Text, 5))
            gIndex = ddlDept.SelectedIndex

        End If

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub actAddRow(ByVal AddVar)

        ' ------------------ ORA : Check if Dept Num is valid in ORA -------------------------- 

        ConnOra_Open()

        Dim strSQL As String
        strSQL = "SELECT count(dept_cd) AS COUNT FROM DEPT WHERE dept_cd = " & AddVar

        ''Execute
        Dim cmdOraDD As System.Data.OleDb.OleDbCommand
        cmdOraDD = New OleDbCommand(strSQL, connOra)
        cmdOraDD.CommandType = CommandType.Text
        cmdOraDD.ExecuteNonQuery()

        'Instantiate Data Reader
        Dim drOraDD As System.Data.OleDb.OleDbDataReader
        drOraDD = cmdOraDD.ExecuteReader

        'Count # of variables found
        Dim Count As Integer

        Do While drOraDD.Read
            Count = drOraDD("COUNT")
        Loop

        ConnOra_Close()

        'If dept exists, Insert variable in SSrv 
        If Count > 0 Then

            ' ---------------- SSvr : Check if Dept Num pre-exists in SQL ---------------------- 

            ConnSql_Open()

            'Check if the Department is in the SQL Table - to prevent duplication :: sp completed
            Dim cmdCheck As System.Data.SqlClient.SqlCommand
            cmdCheck = New SqlClient.SqlCommand("spDeptCount", connHeader)
            cmdCheck.CommandType = CommandType.StoredProcedure
            cmdCheck.Parameters.Add("@Dept", AddVar)
            cmdCheck.ExecuteNonQuery()

            'Instantiate Data Reader
            Dim Ext As System.Data.SqlClient.SqlDataReader
            Ext = cmdCheck.ExecuteReader

            'Count # of variables found
            Dim Cnt

            Do While Ext.Read
                Cnt = Ext("COUNT")
            Loop

            'Do While Ext.Read
            'Cnt = Cnt + 1
            'Loop

            ConnSql_Close()

            ' ---------------- SSvr : Add the Department Number  ----------------------------- 

            If Cnt = 0 Then
                Dim lengthDept As Integer
                Dim NeededLength As Integer
                Dim NeededZeros As Integer
                Dim strDept As String
                strDept = Trim(txtAddDept.Text)
                NeededLength = 4
                lengthDept = Len(Trim(txtAddDept.Text))
                NeededZeros = NeededLength - lengthDept
                Dim DeptNum As String
                If NeededZeros = 1 Then
                    DeptNum = "0" & strDept
                ElseIf NeededZeros = 2 Then
                    DeptNum = "00" & strDept
                ElseIf NeededZeros = 3 Then
                    DeptNum = "000" & strDept
                End If
                ConnSql_Open()

                'Insert the Dept to the Table :: sp completed
                Dim cmdInsert As System.Data.SqlClient.SqlCommand
                cmdInsert = New SqlClient.SqlCommand("spInsertDept", connHeader)
                cmdInsert.CommandType = CommandType.StoredProcedure
                'cmdInsert.Parameters.Add("@Dept", AddVar)
                cmdInsert.Parameters.Add("@Dept", DeptNum)
                cmdInsert.ExecuteNonQuery()

                CloseAddControls()

                'rebuild the pull down now
                buildDeptPD()

                lblLoginMsg.Text = "Department Number " & AddVar & " successfully ADDED."
                lblLoginMsg.Visible = True

                ConnSql_Close()

            Else

                lblLoginMsg.Text = "That Department Number already exists. Please Try again."
                lblLoginMsg.Visible = True

            End If 'Insert

        Else

            'Else Err message 
            lblLoginMsg.Text = "You have entered an Invalid Department Number. Please Try again."
            lblLoginMsg.Visible = True

        End If

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub actDeleteRow(ByVal gDept)

        ConnSql_Open()

        Dim cmdDel As System.Data.SqlClient.SqlCommand
        cmdDel = New SqlClient.SqlCommand("spDelDept", connHeader)
        cmdDel.CommandType = CommandType.StoredProcedure
        cmdDel.Parameters.Add("@Dept", gDept)
        cmdDel.ExecuteNonQuery()

        buildDeptPD()

        lblLoginMsg.Text = "Department " & gDept & " was deleted."
        lblLoginMsg.Visible = True

        'ensures the Delete is not auto performed on next page load
        btnOk.Text = "Default"

        ConnSql_Close()

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub buildDeptPD()

        Dim I As Integer
        Dim Cont As String

        ' ------------------------------------- SQL SRV ------------------------------------------
        ConnSql_Open()

        'Instantiate Command
        Dim cmdDropDown As System.Data.SqlClient.SqlCommand
        cmdDropDown = New SqlClient.SqlCommand("spGetDeptID", connHeader)
        cmdDropDown.CommandType = CommandType.StoredProcedure
        cmdDropDown.ExecuteNonQuery()

        'Instantiate Data Reader
        Dim drDeptID As System.Data.SqlClient.SqlDataReader
        drDeptID = cmdDropDown.ExecuteReader()

        'Assemble the DeptID String to be used in ORA later 
        Cont = "( "
        Do While drDeptID.Read
            Cont = Cont & drDeptID("Dept_Cd") & " , "
        Loop
        Cont = Cont & " 0 )" '0 included to buffer the last comma

        ConnSql_Close() 'Close SQL Queries

        ' ------------------------------------- ORA ---------------------------------------------

        ConnOra_Open()

        'declare strings
        Dim strSQL As String
        strSQL = "SELECT DEPT_CD || ' ' || DES AS DEPART FROM DEPT WHERE DEPT_CD IN " & Cont
        strSQL = strSQL & "Order By Dept_Cd"

        Dim daOraDD As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter(strSQL, connOra)

        ''Execute
        Dim cmdOraDD As System.Data.OleDb.OleDbCommand
        cmdOraDD = New OleDbCommand(strSQL, connOra)
        cmdOraDD.CommandType = CommandType.Text
        cmdOraDD.ExecuteNonQuery()

        'Instantiate Data Reader
        Dim drOraDD As System.Data.OleDb.OleDbDataReader
        drOraDD = cmdOraDD.ExecuteReader


        'Bind Data Reader to Pull Down 
        ddlDept.DataSource = drOraDD
        ddlDept.DataTextField = "DEPART"
        ddlDept.DataBind()

        ConnOra_Close()

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnSql_Open()

        'connection
        connHeader = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        connHeader.Open()

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnSql_Close()

        connHeader.Close()

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnOra_Open()

        'connects to hottopic in GERS
        connOra = New System.Data.OleDb.OleDbConnection("Provider=MSDAORA.1;Password=hottopic;User ID=hottopic;Data Source=genret")
        connOra.Open()

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnOra_Close()

        connOra.Close()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub Login_Hide()

        lblLoginMsg.Visible = False

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub Login_Show()


    End Sub


    '-----------------------------------------------------------------------------
    ' Actions taken when Show Vendor Buttons is clicked 
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)






    End Sub

    '-----------------------------------------------------------------------------
    ' Connects to Database and checks if the user exists, returns value if exists
    ' Ora Connection with Sp
    '
    '-----------------------------------------------------------------------------

    Private Function OK_User(ByVal fEmpID) As Integer

        Dim Boole As Integer

        ConnOra_Open()

        'returns the count number of employees found for that
        'employeeid
        Dim cmdOKUser As System.Data.OleDb.OleDbCommand
        cmdOKUser = New OleDbCommand("fxnValidEmp", connOra)
        cmdOKUser.CommandType = CommandType.StoredProcedure

        Dim prmOUT As OleDbParameter
        Dim prmIN As OleDbParameter

        prmOUT = cmdOKUser.CreateParameter()
        prmOUT.Direction = ParameterDirection.ReturnValue
        prmOUT.OleDbType = OleDbType.Integer
        prmOUT.Size = 100

        prmIN = cmdOKUser.CreateParameter()
        prmIN.Direction = ParameterDirection.Input
        prmIN.OleDbType = OleDbType.Integer
        prmIN.Size = 100
        prmIN.Value = fEmpID

        cmdOKUser.Parameters.Add(prmOUT)
        cmdOKUser.Parameters.Add(prmIN)

        cmdOKUser.ExecuteNonQuery()

        'Function :)
        OK_User = prmOUT.Value

        ConnOra_Close()

    End Function

    '-----------------------------------------------------------------------------
    'Displays Vendor Info per Department 
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnDispVend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDispVend.Click
        dgVendors.CurrentPageIndex = 0
        Dim Dpt As Integer
        Dim Vend As String

        ViewState("gDeptAddTo") = gDept

        Dpt = gDept

        ' ------------------------------------- SQL SRV ------------------------------------------
        ' get all Vendors Associated with the Dept passed in  

        ConnSql_Open()

        'Instantiate Command
        Dim cmdDropDown As System.Data.SqlClient.SqlCommand
        cmdDropDown = New SqlClient.SqlCommand("spGetVendors", connHeader)
        cmdDropDown.CommandType = CommandType.StoredProcedure
        cmdDropDown.Parameters.Add("@Dpt", Dpt)
        cmdDropDown.ExecuteNonQuery()

        'Instantiate Data Reader
        Dim drDeptID As System.Data.SqlClient.SqlDataReader
        drDeptID = cmdDropDown.ExecuteReader()

        'Assemble the DeptID String to be used in ORA later 
        Vend = "( '"
        Do While drDeptID.Read
            Vend = Vend & drDeptID("Vendor_Cd") & "' , '"
        Loop
        Vend = Vend & " 0' )" '0 included to buffer the last comma


        ViewState("vsVend") = Vend
        ViewState("vsDpt") = Dpt
       

        ConnSql_Close() 'Close SQL Queries

        ' ------------------------------------- ORA  ------------------------------------------

        actDispVend(Vend)

    End Sub


    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub actDispVend(ByVal Vend)

        Dim strSQL As String
       
            ConnOra_Open()
            strSQL = "SELECT VE_CD, VE_NAME FROM VE WHERE VE_CD IN" & Vend



        With dgVendors
            .AllowPaging = True
            .PagerStyle.Mode = PagerMode.NumericPages
            .PagerStyle.PageButtonCount = 5
            .PageSize = 5
        End With

        'NEW...
        'dec datatable in global 
        Dim myDataAdapter As New System.Data.OleDb.OleDbDataAdapter(strSQL, connOra)
        Dim myDataSet As New DataSet()

        myDataAdapter.Fill(myDataSet, "Vendors")
        myDataTable = myDataSet.Tables(0)
        dgVendors.DataSource = myDataTable
        dgVendors.DataBind()

        If Not Page.IsPostBack Then
            dgVendors.DataBind()
        End If

        dgVendors.Visible = True


        ' -----------------------------------------
        'OLD Data - Works

        'Bind Data Reader to Pull Down 
        'dgVendors.DataSource = drOraDD
        'dgVendors.DataBind()
        'dgVendors.Visible = True

        'If Datareader is empty, show err message
        'Dim CntChk
        'CntChk = 0
        'Do While drOraDD.Read
        'CntChk = CntChk + 1
        'Loop

        ''If CntChk = 0 Then
        ''lblLoginMsg.Text = "No Vendors listed for this Department."
        ''lblLoginMsg.Visible = True
        ''dgVendors.Visible = False
        ''End If

        lblLoginMsg.Text = "Vendor Listings for Department " & ViewState("vsDpt") & ":"
        lblLoginMsg.Visible = True

        btnStAddVend.Visible = True
        btnStDelVend.Visible = True
        btnAddAllVend.Visible = True
        btnDelAllVend.Visible = True

        ConnOra_Close()

    End Sub


    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub dgVendors_PageIndexChanged(ByVal Source As Object, _
    ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgVendors.PageIndexChanged

        dgVendors.CurrentPageIndex = e.NewPageIndex

        Dim strSearch As String
        Dim strDept As String
        strSearch = ViewState("vsVend")


        actDispVend(strSearch)
        dgVendors.DataBind()

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnRemVend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemVend.Click

        Dim localDept As Integer
        Dim localVend As Integer

        'Dept to delete from
        localDept = ViewState("gDeptAddTo")

        If txtRemVend.Text = "" Then

            lblLoginMsg.Text = "Empty Field Detected. Please try again."
            lblLoginMsg.Visible = True

            lblVendAction.Text = "Enter Vendor to delete from Dept: " & localDept
            lblVendAction.Visible = True

            txtRemVend.Visible = True
            btnRemVend.Visible = True

        Else

            If Not IsNumeric(txtRemVend.Text) Then

                lblLoginMsg.Text = "Format Error Detected. Please try again."
                lblLoginMsg.Visible = True

                lblVendAction.Text = "Enter Vendor to delete from Dept: " & localDept
                lblVendAction.Visible = True

                txtRemVend.Visible = True
                btnRemVend.Visible = True

            Else  'Check if the Association Exists between Vendor and Dept

                ConnSql_Open()

                RemVend = Trim(CInt(txtRemVend.Text))

                'Check if those associations already exist. 
                Dim cmdDropDown As System.Data.SqlClient.SqlCommand
                cmdDropDown = New SqlClient.SqlCommand("spCheckAssoc", connHeader)
                cmdDropDown.CommandType = CommandType.StoredProcedure

                cmdDropDown.Parameters.Add("@Vnd", RemVend)
                cmdDropDown.Parameters.Add("@Dpt", localDept)

                cmdDropDown.ExecuteNonQuery()

                'Instantiate Data Reader
                Dim drDeptID As System.Data.SqlClient.SqlDataReader
                drDeptID = cmdDropDown.ExecuteReader()

                Dim Chk As Integer

                Do While drDeptID.Read
                    Chk = drDeptID("COUNT")
                Loop

                ConnSql_Close()

                If Chk = 1 Then 'IE Association(Exists)

                    ConnSql_Open()

                    'Check if those associations already exist. 
                    Dim cmdDelAssoc As System.Data.SqlClient.SqlCommand
                    cmdDelAssoc = New SqlClient.SqlCommand("spDelAssoc", connHeader)
                    cmdDelAssoc.CommandType = CommandType.StoredProcedure

                    cmdDelAssoc.Parameters.Add("@Vnd", RemVend)
                    cmdDelAssoc.Parameters.Add("@Dpt", localDept)

                    cmdDelAssoc.ExecuteNonQuery()

                    lblVendAction.Text = "The Association between Dept " & localDept & "  and Vendor " & RemVend & " was DELETED."
                    lblVendAction.Visible = True

                Else
                    lblVendAction.Text = "The Association between Dept " & localDept & "  and Vendor " & RemVend & " does not exist."
                    lblVendAction.Visible = True
                End If

                ConnSql_Close()

            End If



        End If



    End Sub

    '-----------------------------------------------------------------------------
    ' Initate the Add Vendor Process
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnStAddVend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStAddVend.Click

        btnStAddVend.Visible = False
        btnStDelVend.Visible = False
        btnAddAllVend.Visible = False
        btnDelAllVend.Visible = False

        lblVendAction.Visible = True
        txtAddVend.Visible = True
        btnAddVend.Visible = True

        lblVendAction.Text = "Enter Vendor to Add for Dept " & ViewState("gDeptAddTo")

    End Sub


    Private Sub btnAddVend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddVend.Click

        ' ------------------------- ORA check if Vendor number is valid -------------------------

        'Need to check the value of gDept here!!!

        If txtAddVend.Text = "" Then
            lblVendAction.Visible = True
            txtAddVend.Visible = True
            btnAddVend.Visible = True

            lblLoginMsg.Text = "Empty Field Detected. Please try again."
            lblLoginMsg.Visible = True
        Else

            ConnOra_Open()

            'Get Vendor Number Entered
            AddVend = Trim(CInt(txtAddVend.Text))

            Dim strSQL As String
            strSQL = "SELECT count(ve_cd) AS COUNT FROM VE WHERE ve_cd = '" & AddVend & "'"

            'Execute
            Dim cmdOraDD As System.Data.OleDb.OleDbCommand
            cmdOraDD = New OleDbCommand(strSQL, connOra)
            cmdOraDD.CommandType = CommandType.Text
            cmdOraDD.ExecuteNonQuery()

            'Instantiate Data Reader
            Dim drAV As System.Data.OleDb.OleDbDataReader
            drAV = cmdOraDD.ExecuteReader

            'Count # of variables found
            Dim Count As Integer

            'Check Value of Count
            Do While drAV.Read
                Count = drAV("COUNT")
            Loop

            ConnOra_Close()

            'If dept exists, Check if Vendor is already associated with that Dept.  
            If Count > 0 Then

                ' ------------------------- SQL Check if Vend-Dept Assoc pre-exists --------------

                ConnSql_Open()

                'Instantiate Command
                Dim cmdDropDown As System.Data.SqlClient.SqlCommand
                cmdDropDown = New SqlClient.SqlCommand("spCheckAssoc", connHeader)
                cmdDropDown.CommandType = CommandType.StoredProcedure
                cmdDropDown.Parameters.Add("@Vnd", AddVend)
                cmdDropDown.Parameters.Add("@Dpt", gDept)
                cmdDropDown.ExecuteNonQuery()

                'Instantiate Data Reader
                Dim drDeptID As System.Data.SqlClient.SqlDataReader
                drDeptID = cmdDropDown.ExecuteReader()

                Dim Chk As Integer

                Do While drDeptID.Read
                    Chk = drDeptID("COUNT")
                Loop

                ConnSql_Close()

                ' ------------------------- Insert Association to Table -------------------------

                If Chk < 1 Then  'Should be 0, ie this is a new association

                    ConnSql_Open()

                    'Dim DeptAddTo As Integer
                    Dim DeptAddTo As String
                    DeptAddTo = ViewState("gDeptAddTo")
                    'Check if there needs to be leading zeros, if so, add those in.
                    Dim lengthDept As Integer
                    Dim NeededLength As Integer
                    Dim NeededZeros As Integer
                    lengthDept = Len(Trim(DeptAddTo))
                    NeededLength = 4
                    NeededZeros = NeededLength - lengthDept
                    Dim DeptNum As String
                    If NeededZeros = 1 Then
                        DeptNum = "0" & DeptAddTo
                    ElseIf NeededZeros = 2 Then
                        DeptNum = "00" & DeptAddTo
                    ElseIf NeededZeros = 3 Then
                        DeptNum = "000" & DeptAddTo
                    End If
                    'Insert the Association 
                    Dim cmdIns As System.Data.SqlClient.SqlCommand
                    cmdIns = New SqlClient.SqlCommand("spInsertAssoc", connHeader)
                    cmdIns.CommandType = CommandType.StoredProcedure
                    cmdIns.Parameters.Add("@Vnd", AddVend)
                    cmdIns.Parameters.Add("@Dpt", DeptNum)
                    cmdIns.ExecuteNonQuery()

                    lblLoginMsg.Text = "Association of Dept: " & DeptAddTo & " to Vendor: " & AddVend & " successfully ADDED."
                    lblLoginMsg.Visible = True

                    ConnSql_Close()
                ElseIf Chk = 1 Then  'association already exists.
                    Dim DeptAddTo As String
                    DeptAddTo = ViewState("gDeptAddTo")
                    lblLoginMsg.Text = "Association of Dept: " & DeptAddTo & " to Vendor: " & AddVend & " already exists. "
                    lblLoginMsg.Visible = True

                End If

            End If

        End If

    End Sub

    '-----------------------------------------------------------------------------
    ' Start the Delete Vendor Procedure
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnStDelVend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStDelVend.Click

        btnStAddVend.Visible = False
        btnStDelVend.Visible = False
        btnAddAllVend.Visible = False
        btnDelAllVend.Visible = False

        lblVendAction.Text = "Enter Vendor to Dissociate from Dept:  " & ViewState("gDeptAddTo")
        lblVendAction.Visible = True

        txtRemVend.Visible = True
        btnRemVend.Visible = True

    End Sub

    '-----------------------------------------------------------------------------
    ' Open Add Controls 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub OpenAddControls()

        lblAddDept.Visible = True
        txtAddDept.Visible = True
        btnCancel.Visible = True
        btnOk.Visible = True
        btnOk.Text = "Add Dept"

    End Sub

    '-----------------------------------------------------------------------------
    ' Open Add Controls 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub CloseAddControls()

        lblLoginMsg.Visible = False
        lblAddDept.Visible = False
        txtAddDept.Visible = False
        txtAddDept.Text = ""

        btnCancel.Visible = False
        btnOk.Visible = False
        btnOk.Text = "Default"

    End Sub

    '-----------------------------------------------------------------------------
    ' Adding a Dept to Department
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnAddDept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddDept.Click

        OpenAddControls()

    End Sub

    '-----------------------------------------------------------------------------
    ' Deleting a Dept from Department
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnDelDept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelDept.Click

        OpenDelControls()

    End Sub

    '-----------------------------------------------------------------------------
    ' Open Delete Controls
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub OpenDelControls()

        lblAddDept.Visible = False
        txtAddDept.Visible = False

        btnCancel.Visible = True

        btnOk.Visible = True
        btnOk.Text = "Delete"

        lblLoginMsg.Text = "You are about to delete Department " & gDept & " and all Vendors associated with it. Click Delete to continue."
        lblLoginMsg.Visible = True

        ViewState("gToDel") = gDept

    End Sub


    '-----------------------------------------------------------------------------
    ' Open Delete Controls
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub CloseDelControls()

        lblLoginMsg.Visible = False

        btnOk.Visible = False
        btnOk.Text = "Default"

        btnCancel.Visible = False

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click


        If btnOk.Text = "Delete" Then
            gToDelete = ViewState("gToDel")
            actDeleteRow(gToDelete) 'input validation is built in this function
        End If


        If btnOk.Text = "Add Dept" Then
            OpenAddControls()

            'check
            If txtAddDept.Text <> "" Then
                If IsNumeric(txtAddDept.Text) Then
                    
                    AddVar = Trim(CInt(txtAddDept.Text))

                    Go = 0
                Else
                    lblLoginMsg.Text = "Wrong Format detected. Pls try again."
                    lblLoginMsg.Visible = True
                End If
            Else
                lblLoginMsg.Text = "Empty Field detected. Pls try again."
                lblLoginMsg.Visible = True
            End If

        End If 'btnOk 

        If Go = 0 Then
            actAddRow(AddVar)
            'CloseAddControls()
        End If

    End Sub

    Private Sub btnAddAllVend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAllVend.Click
        'Add all Vendors to table
        Dim strDpt As String
        Dim strOraDpt As String
        Dim strSQL As String
        Dim lenStrDpt As Integer
        Dim intneedzeros As Integer
        intDept = ViewState("gDeptAddTo")
        strDpt = CStr(intDept)
        lenStrDpt = Len(strDpt)
        intneedzeros = 4 - lenStrDpt
        If intneedzeros = 3 Then
            strOraDpt = "000" & strDpt
        ElseIf intneedzeros = 2 Then
            strOraDpt = "00" & strDpt
        ElseIf intneedzeros = 1 Then
            strOraDpt = "0" & strDpt
        ElseIf intneedzeros = 0 Then
            strOraDpt = strDpt
        End If

        'Delete entire Dept, then re-insert Dept with zero in Vendor field
        ConnSql_Open()
        Dim cmdDelAssoc As System.Data.SqlClient.SqlCommand
        cmdDelAssoc = New SqlClient.SqlCommand("spDelAllAssoc", connHeader)
        cmdDelAssoc.CommandType = CommandType.StoredProcedure
        cmdDelAssoc.Parameters.Add("@Dpt", intDept)
        cmdDelAssoc.ExecuteNonQuery()

        ConnSql_Close()

        'Re-insert Dept with zero in Vendor field
        ConnSql_Open()
        Dim cmdInsert As System.Data.SqlClient.SqlCommand
        cmdInsert = New SqlClient.SqlCommand("spInsertDept", connHeader)
        cmdInsert.CommandType = CommandType.StoredProcedure
        'cmdInsert.Parameters.Add("@Dept", intDept)
        cmdInsert.Parameters.Add("@Dept", strOraDpt)
        cmdInsert.ExecuteNonQuery()
        ConnSql_Close()

        'Get all vendors with that dept from Gers
        strSQL = "SELECT distinct VE.VE_CD, VE.VE_NAME, GM_ITM.Dept_Cd FROM VE, GM_ITM WHERE GM_ITM.Dept_Cd='" & strOraDpt & "' AND GM_ITM.VE_CD=VE.VE_CD"
        ConnOra_Open()
        Dim cmdOraDD As System.Data.OleDb.OleDbCommand
        cmdOraDD = New OleDbCommand(strSQL, connOra)
        cmdOraDD.CommandType = CommandType.Text
        cmdOraDD.ExecuteNonQuery()

        'Instantiate Data Reader
        Dim drAV As System.Data.OleDb.OleDbDataReader
        drAV = cmdOraDD.ExecuteReader

        Dim strVendor As String
        Dim intVendor As Integer
        Do While drAV.Read
            'Loop through vendors and insert into table.
            strVendor = drAV("VE_CD")
            intVendor = CInt(strVendor)
            ConnSql_Open()
            Dim cmdIns As System.Data.SqlClient.SqlCommand
            cmdIns = New SqlClient.SqlCommand("spInsertAssoc", connHeader)
            cmdIns.CommandType = CommandType.StoredProcedure
            cmdIns.Parameters.Add("@Vnd", intVendor)
            cmdIns.Parameters.Add("@Dpt", strOraDpt)
            cmdIns.ExecuteNonQuery()
            ConnSql_Close()
        Loop
        ConnOra_Close()
        lblVendAction.Visible = "True"
        lblVendAction.Text = "All Vendors have been associated to Dept" & intDept


        btnStAddVend.Visible = False
        btnStDelVend.Visible = False
        btnAddAllVend.Visible = False
        btnDelAllVend.Visible = False
    End Sub

    Private Sub btnDelAllVend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelAllVend.Click
        'Delete all Vendors in table
        Dim strSQL As String
        intDept = ViewState("gDeptAddTo")

        'Delete entire Dept, then re-insert Dept with zero in Vendor field
        ConnSql_Open()
        Dim cmdDelAssoc As System.Data.SqlClient.SqlCommand
        cmdDelAssoc = New SqlClient.SqlCommand("spDelAllAssoc", connHeader)
        cmdDelAssoc.CommandType = CommandType.StoredProcedure
        cmdDelAssoc.Parameters.Add("@Dpt", intDept)
        cmdDelAssoc.ExecuteNonQuery()

        ConnSql_Close()

        'Re-insert Dept with zero in Vendor field
        ConnSql_Open()
        Dim cmdInsert As System.Data.SqlClient.SqlCommand
        cmdInsert = New SqlClient.SqlCommand("spInsertDept", connHeader)
        cmdInsert.CommandType = CommandType.StoredProcedure
        cmdInsert.Parameters.Add("@Dept", intDept)
        cmdInsert.ExecuteNonQuery()
        ConnSql_Close()

        lblVendAction.Visible = "True"
        lblVendAction.Text = "All Vendors are no longer associated to Dept" & intDept


        btnStAddVend.Visible = False
        btnStDelVend.Visible = False
        btnAddAllVend.Visible = False
        btnDelAllVend.Visible = False
    End Sub
End Class
