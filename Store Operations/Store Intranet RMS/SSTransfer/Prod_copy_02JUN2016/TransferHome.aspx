<%@ Register TagPrefix="uc1" TagName="SearchBox" Src="UserControls/SearchBox.ascx" %>
<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="TransferHome.aspx.vb" Inherits="SSTransfer.WebForm_TransferHome" smartNavigation="False"%>
<%@ Register TagPrefix="ucLayout" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="ucLayout" TagName="Footer" Src="Layout/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Transfer Home ccc</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="frmTransferHome" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td height="1"><uclayout:header id="layoutHeader" runat="server"></uclayout:header></td>
				</tr>
				</TD></TR>
				<tr>
					<td vAlign="top" align="middle">
						<table cellSpacing="0" cellPadding="5" width="100%" border="0">
							<tr>
								<td><uc1:searchbox id="ucSearchBox" runat="server"></uc1:searchbox></td>
							</tr>
						</table>
						<hr width="100%" color="#990000">
						<table cellSpacing="0" cellPadding="5" width="100%" border="0">
							<tr>
								<td>
									<table cellSpacing="0" cellPadding="0" width="100%" border="0">
										<tr>
											<td><iewc:tabstrip id="tsGrids" runat="server" ForeColor="White" Font-Size="Small" Font-Names="Arial" BorderColor="#990000" TabDefaultStyle="background-color:#440000;font-family:arial;font-weight:bold;font-size:x-small;color:#999999;width:120;height:21;text-align:center" TabHoverStyle="background-color:#990000;color:#FFFFFF;" TabSelectedStyle="background-color:#990000;color:#FFFFFF" targetId="TabStripView">
													<iewc:Tab Text="Expected Arrivals"></iewc:Tab>
													<iewc:TabSeparator />
													<iewc:Tab Text="Recently Shipped"></iewc:Tab>
													<iewc:TabSeparator />
													<iewc:Tab Text="Packing"></iewc:Tab>
													<iewc:TabSeparator />
													<iewc:Tab Text="Unpacking"></iewc:Tab>
													<iewc:TabSeparator />
													<iewc:Tab Text="Print History"></iewc:Tab>
												</iewc:tabstrip></td>
										</tr>
									</table>
									<table borderColor="#990000" cellSpacing="0" cellPadding="0" width="100%" border="1">
										<tr>
											<td align="middle"><iewc:multipage id="TabStripView" style="BORDER-RIGHT: #000000 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #000000 1px solid; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: medium none; PADDING-TOP: 5px; BORDER-BOTTOM: #000000 1px solid" runat="server">
													<iewc:PageView>
														<div style="OVERFLOW-Y: auto;  WIDTH: 100%; HEIGHT: 280px;scrollbar-base-color:#440000;scrollbar-arrow-color:#ffffff;scrollbar-highlight-color:#990000;">
															<table cellSpacing="0" cellPadding="2" width="100%" border="0">
																<tr>
																	<td>
																		<asp:datagrid OnSortCommand="SortCommand_Arrivals" AllowSorting="True" id="grdArrivals" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="White" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="true" BorderWidth="0" AutoGenerateColumns="False" BackColor="#111111">
																			<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
																			<Columns>
																				<asp:HyperLinkColumn SortExpression="1" DataTextField="Box_Id" HeaderText="Transfer #" DataNavigateUrlField="Box_Id" datanavigateurlformatstring="ViewTransfer.aspx?BID={0}">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:HyperLinkColumn>
																				<asp:TemplateColumn HeaderStyle-Width="120" HeaderText="Type">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																					<ItemTemplate>
																						<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" ForeColor="white">
																							<%# GetBoxTransferTypes(Container.dataitem("Box_Id")) %>
																						</asp:Label>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn SortExpression="3" DataField="Sending_Store_Name" HeaderText="Sent Form">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:HyperLinkColumn SortExpression="4" DataTextField="UPS_Tracking_Num" HeaderText="UPS #" DataNavigateUrlField="UPS_Tracking_Num" Target="_blank" datanavigateurlformatstring="http://wwwapps.ups.com/etracking/tracking.cgi?tracknums_displayed=1&TypeOfInquiryNumber=T&HTMLVersion=4.0&InquiryNumber1={0}">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:HyperLinkColumn>
																				<asp:BoundColumn DataField="Status_Desc" HeaderText="Status">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn SortExpression="5" DataFormatString="{0:d}" DataField="Shipment_Date" HeaderText="Ship Date">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																			</Columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</div>
													</iewc:PageView>
													<iewc:PageView>
														<div style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 280px;scrollbar-base-color:#440000;scrollbar-arrow-color:#ffffff;scrollbar-highlight-color:#990000;">
															<table cellSpacing="0" cellPadding="2" width="100%" border="0">
																<tr>
																	<td>
																		<asp:datagrid OnSortCommand="SortCommand_Shipped" AllowSorting="True" id="grdShipped" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="White" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="true" BorderWidth="0" AutoGenerateColumns="False" BackColor="#111111">
																			<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
																			<Columns>
																				<asp:HyperLinkColumn SortExpression="1" DataTextField="Box_Id" HeaderText="Transfer #" DataNavigateUrlField="Box_Id" datanavigateurlformatstring="ViewTransfer.aspx?BID={0}">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:HyperLinkColumn>
																				<asp:TemplateColumn HeaderStyle-Width="120" HeaderText="Type">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																					<ItemTemplate>
																						<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" ForeColor="white" ID="Label1">
																							<%# GetBoxTransferTypes(Container.dataitem("Box_Id")) %>
																						</asp:Label>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn SortExpression="3" DataField="Receiving_Store_Name" HeaderText="Sent To">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:HyperLinkColumn SortExpression="4" DataTextField="UPS_Tracking_Num" HeaderText="UPS #" DataNavigateUrlField="UPS_Tracking_Num" Target="_blank" datanavigateurlformatstring="http://wwwapps.ups.com/etracking/tracking.cgi?tracknums_displayed=1&TypeOfInquiryNumber=T&HTMLVersion=4.0&InquiryNumber1={0}">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:HyperLinkColumn>
																				<asp:BoundColumn SortExpression="5" DataField="Status_Desc" HeaderText="Status">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn SortExpression="6" DataFormatString="{0:d}" DataField="Shipment_Date" HeaderText="Ship Date">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																			</Columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</div>
													</iewc:PageView>
													<iewc:PageView>
														<div style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 280px;scrollbar-base-color:#440000;scrollbar-arrow-color:#ffffff;scrollbar-highlight-color:#990000;">
															<table cellSpacing="0" cellPadding="2" width="100%" border="0">
																<tr>
																	<td>
																		<asp:datagrid OnSortCommand="SortCommand_SavedTransfers" AllowSorting="True" id="grdSavedTransfers" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="White" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="true" BorderWidth="0" AutoGenerateColumns="False" BackColor="#111111">
																			<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
																			<Columns>
																				<asp:TemplateColumn SortExpression="1" HeaderText="Transfer #">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																					<ItemTemplate>
																						<a href='NewTransferForm.aspx?BID=<%# Container.dataitem("Box_Id") %>&TType=<%# Container.dataitem("TType") %>'>
																							<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" ForeColor="white" ID="Label4">
																								<%# Container.dataitem("Box_Id") %>
																							</asp:Label>
																						</a>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Type">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																					<ItemTemplate>
																						<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" ForeColor="white" ID="Label2">
																							<%# GetBoxTransferTypes(Container.dataitem("Box_Id")) %>
																						</asp:Label>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn SortExpression="3" DataField="Receiving_Store_Name" HeaderText="Sent To">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn SortExpression="4" DataField="Employee" HeaderText="Employee">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn SortExpression="5" DataField="Status_Desc" HeaderText="Status">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn SortExpression="6" DataFormatString="{0:d}" DataField="Modified_Date" HeaderText="Saved Date">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																			</Columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</div>
													</iewc:PageView>
													<iewc:PageView>
														<div style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 280px;scrollbar-base-color:#440000;scrollbar-arrow-color:#ffffff;scrollbar-highlight-color:#990000;">
															<table cellSpacing="0" cellPadding="2" width="100%" border="0">
																<tr>
																	<td>
																		<asp:datagrid OnSortCommand="SortCommand_SavedCheckIns" AllowSorting="True" id="dgSavedCheckIns" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="White" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="true" BorderWidth="0" AutoGenerateColumns="False" BackColor="#111111">
																			<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
																			<Columns>
																				<asp:HyperLinkColumn SortExpression="1" DataTextField="Box_Id" HeaderText="Transfer #" DataNavigateUrlField="Box_Id" datanavigateurlformatstring="CheckInTransfer.aspx?BID={0}">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:HyperLinkColumn>
																				<asp:TemplateColumn HeaderStyle-Width="120" HeaderText="Type">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																					<ItemTemplate>
																						<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" ForeColor="white" ID="Label3">
																							<%# GetBoxTransferTypes(Container.dataitem("Box_Id")) %>
																						</asp:Label>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn SortExpression="3" DataField="Sending_Store_Name" HeaderText="Sent From">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:HyperLinkColumn SortExpression="4" DataTextField="UPS_Tracking_Num" HeaderText="UPS #" DataNavigateUrlField="UPS_Tracking_Num" Target="_blank" datanavigateurlformatstring="http://wwwapps.ups.com/etracking/tracking.cgi?tracknums_displayed=1&TypeOfInquiryNumber=T&HTMLVersion=4.0&InquiryNumber1={0}">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:HyperLinkColumn>
																				<asp:BoundColumn SortExpression="5" DataField="Status_Desc" HeaderText="Status">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn SortExpression="6" DataFormatString="{0:d}" DataField="Shipment_Date" HeaderText="Ship Date">
																					<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																			</Columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</div>
													</iewc:PageView>
													<iewc:PageView>
														<div style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 280px;scrollbar-base-color:#440000;scrollbar-arrow-color:#ffffff;scrollbar-highlight-color:#990000;">
															<table cellSpacing="0" cellPadding="2" width="100%" border="0">
																<tr>
																	<td>
																		<asp:datagrid OnSortCommand="SortCommand_PrintQueue" AllowSorting="True" id="grdPrintQueue" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="White" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="true" BorderWidth="0" AutoGenerateColumns="False" BackColor="#111111">
																			<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
																			<Columns>
																				<asp:HyperLinkColumn ItemStyle-Wrap="False" HeaderStyle-Width="15%" SortExpression="1" DataTextFormatString="{0:d}" DataTextField="Box_Id" HeaderText="Transfer #" DataNavigateUrlField="Box_Id" datanavigateurlformatstring="ViewTransfer.aspx?BID={0}">
																					<HeaderStyle Wrap="False" Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:HyperLinkColumn>
																				<asp:BoundColumn ItemStyle-Wrap="False" HeaderStyle-Width="15%" SortExpression="2" DataFormatString="{0:d}" DataField="Shipment_Date" HeaderText="Shipment Date">
																					<HeaderStyle Wrap="False" Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:TemplateColumn HeaderStyle-Width="65%">
																					<HeaderStyle HorizontalAlign="center" Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																					<ItemStyle HorizontalAlign="center"></ItemStyle>
																					<ItemTemplate>
																						&nbsp;
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderStyle-Width="5%" HeaderText="Manifest">
																					<HeaderStyle HorizontalAlign="center" Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
																					<ItemStyle HorizontalAlign="center"></ItemStyle>
																					<ItemTemplate>
																						<a target=_blank href='GenerateManifest.aspx?BID=<%# container.dataitem("Box_Id") %>'>
																							<asp:Image Runat=server ID="imgManifest" ImageUrl='Images\Printed.gif'>
																							</asp:Image></a>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																			</Columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</div>
													</iewc:PageView>
												</iewc:multipage></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uclayout:footer id="layoutFooter" runat="server"></uclayout:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
