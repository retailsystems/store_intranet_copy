<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_ProfileList.aspx.vb" Inherits="SSTransfer.ICProfileList"%>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Profile List</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="yes" rightMargin="0">
		<form id="frmTransferHome" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="top">
						<div style="OVERFLOW-Y: auto; SCROLLBAR-HIGHLIGHT-COLOR: #990000; WIDTH: 100%; SCROLLBAR-ARROW-COLOR: #ffffff; SCROLLBAR-BASE-COLOR: #440000; HEIGHT: 225px">
							<table cellSpacing="0" cellPadding="2" width="100%" border="0">
								<br>
								<tr>
									<td><asp:label id="lblPersonal" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Runat="server" Font-Bold="True">My Profiles</asp:label><asp:datagrid id="grdPersonal" runat="server" BackColor="#111111" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="False" Width="100%" CellPadding="2" CellSpacing="1" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" AllowSorting="True">
											<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
											<Columns>
												<asp:HyperLinkColumn SortExpression="Profile_Name" DataTextField="Profile_Name" HeaderText="Profile Name" DataNavigateUrlField="Profile_Id" datanavigateurlformatstring="IC_Search.aspx?PROID={0}">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:HyperLinkColumn>
												<asp:BoundColumn SortExpression="Profile_Desc" DataField="Profile_Desc" HeaderText="Description">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn SortExpression="Created_By" DataField="Created_By" HeaderText="Created By">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn SortExpression="Created_Date" DataField="Created_Date" HeaderText="Date Created">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn SortExpression="Modified_Date" DataField="Modified_Date" HeaderText="Modified Date">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:BoundColumn>
											</Columns>
										</asp:datagrid></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td vAlign="top">
						<div style="OVERFLOW-Y: auto; SCROLLBAR-HIGHLIGHT-COLOR: #990000; WIDTH: 100%; SCROLLBAR-ARROW-COLOR: #ffffff; SCROLLBAR-BASE-COLOR: #440000; HEIGHT: 225px">
							<table cellSpacing="0" cellPadding="2" width="100%" border="0">
								<tr>
									<td><asp:label id="lblGlobal" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Runat="server" Font-Bold="True">Other People's Profiles</asp:label><asp:datagrid id="dgGlobal" runat="server" BackColor="#111111" AutoGenerateColumns="False" BorderWidth="0" EnableViewState="False" Width="100%" CellPadding="2" CellSpacing="1" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" AllowSorting="True">
											<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
											<Columns>
												<asp:HyperLinkColumn SortExpression="Profile_Name" DataTextField="Profile_Name" HeaderText="Profile Name" DataNavigateUrlField="Profile_Id" datanavigateurlformatstring="IC_Search.aspx?PROID={0}">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:HyperLinkColumn>
												<asp:BoundColumn SortExpression="Profile_Desc" DataField="Profile_Desc" HeaderText="Description">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn SortExpression="Created_By" DataField="Created_By" HeaderText="Created By">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn SortExpression="Created_Date" DataField="Created_Date" HeaderText="Date Created">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn SortExpression="Modified_Date" DataField="Modified_Date" HeaderText="Modified Date">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:BoundColumn>
											</Columns>
										</asp:datagrid></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:Footer id="ucFooter" runat="server"></uc1:Footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
