<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UPSVoid.aspx.vb" Inherits="SSTransfer.UPSVoid"%>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Login</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="frmTransferHome" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td align="middle" valign="top">
						<br>
						<div style="OVERFLOW-Y: auto;  WIDTH: 100%; HEIGHT: 280px;scrollbar-base-color:#440000;scrollbar-arrow-color:#ffffff;scrollbar-highlight-color:#990000;">
							<table cellSpacing="2" cellPadding="4" border="0" width="100%">
								<tr>
									<td>
										<asp:DataGrid id="dgUPSVoid" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="White" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="true" BorderWidth="0" AutoGenerateColumns="False" BackColor="#111111">
											<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
											<Columns>
												<asp:BoundColumn DataField="Box_Id" HeaderText="Transfer #">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Status_Desc" HeaderText="Transfer Status">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Tracking_Number">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
													<ItemTemplate>
														<a target=_blank href='<%# Replace(Container.dataitem("TrackingURL").tostring,"{0}",Container.dataitem("Tracking_Number").tostring) %>'>
															<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" ForeColor="white" ID="Label3">
																<%# Container.dataitem("Tracking_Number").tostring %>
															</asp:Label>
														</a>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="RequestDate" HeaderText="Label Request Date">
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Elapsed Time" ItemStyle-HorizontalAlign="center">
													<HeaderStyle HorizontalAlign="center" Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
													<ItemTemplate>
														<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" ForeColor='White' ID="Label2">
															<%# DateDiff(DateInterval.Hour, Container.dataitem("RequestDate"), Now) & " Hrs" %>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Void Label" ItemStyle-HorizontalAlign="center">
													<HeaderStyle Font-Bold="True" HorizontalAlign="center" ForeColor="White" BackColor="#440000"></HeaderStyle>
													<ItemTemplate>
														<a href='UPSVoid.aspx?SID=<%# Server.URLEncode(Container.dataitem("UPS_ShipmentId").tostring) %>&TID=<%# Server.URLEncode(Container.dataitem("TrackingInfo_Id").tostring) %>&BID=<%# Server.URLEncode(Container.dataitem("Box_Id").tostring) %>'>
															<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" Font-Bold="true" ForeColor="red" ID="Label1">Void</asp:Label>
														</a>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="Voided" HeaderText="Void Status" ItemStyle-HorizontalAlign="center">
													<HeaderStyle Font-Bold="True" HorizontalAlign="center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												</asp:BoundColumn>
											</Columns>
										</asp:DataGrid>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
