<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="StoreResults.aspx.vb" Inherits="SSTransfer.StoreResults"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WebForm1</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript">
			<!--
			javascript:window.history.forward(1);
			//-->
		</script>
	</HEAD>
	<body bgColor="black" leftMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<P>
				<TABLE id="Main" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></TD>
					</TR>
					<TR>
						<TD><asp:button id="btnNewSearch" runat="server" BackColor="#440000" ForeColor="White" Font-Bold="True" Width="101px" BorderColor="#990000" Text="New Search"></asp:button></TD>
					</TR>
					<TR valign="top">
						<TD>
							<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD align="middle"><asp:datagrid id="dgSearch" runat="server" BackColor="Transparent" ForeColor="GhostWhite" Font-Bold="True" Width="100%" BorderColor="Red" BorderStyle="Solid" BorderWidth="0px" AutoGenerateColumns="False" PageSize="8" Font-Names="Arial" Font-Size="Smaller" AllowPaging="True" AllowSorting="True">
											<SelectedItemStyle ForeColor="Transparent" BorderStyle="Solid"></SelectedItemStyle>
											<AlternatingItemStyle BackColor="DimGray"></AlternatingItemStyle>
											<HeaderStyle BackColor="#804040"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="Store Number">
													<ItemTemplate>
														<a href="StoreDetail.aspx?Empty=0&pStoreID=<%# container.dataitem("Store Number")%>"><font color="Red" size="2" face="arial"><%# container.dataitem("Store Number") %></font></a>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="Store Name" HeaderText="Store Name"></asp:BoundColumn>
												<asp:BoundColumn DataField="City" HeaderText="City"></asp:BoundColumn>
												<asp:BoundColumn DataField="State" HeaderText="State"></asp:BoundColumn>
												<asp:BoundColumn DataField="Region" HeaderText="Region"></asp:BoundColumn>
											</Columns>
											<PagerStyle ForeColor="White" PageButtonCount="5" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></TD>
					</TR>
				</TABLE>
				<BR>
				<BR>
				<BR>
				<BR>
				<BR>
				<BR>
			</P>
		</form>
	</body>
</HTML>
