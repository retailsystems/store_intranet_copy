Public Class ScanLabel
    Inherits System.Web.UI.Page
    Protected WithEvents lblInstruction As System.Web.UI.WebControls.Label
    Protected WithEvents lblTrackNum As System.Web.UI.WebControls.Label
    Protected WithEvents btnAssign As System.Web.UI.WebControls.Button
    Protected WithEvents ucHeader As Header
    Protected WithEvents btnClear As System.Web.UI.WebControls.Button
    Protected WithEvents txtTrackNum As System.Web.UI.WebControls.TextBox
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

    Private m_objUserInfo As New UserInfo() ' user object
    Private m_ParcelService As String 'select parcel service
    Private m_BoxId As String 'box id
    Private m_TType As Int32 'return/store to store
    Private m_strApprovedBy As String
    Private m_objTransferLock As New TransferLock()
    Private m_strLBS As String
    Private m_strRANum As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Get user info from session and cookies
        m_objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        'If employee is logged on to the system then their locks are kept alive
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
            m_objTransferLock.KeepLocksAlive(m_objUserInfo.EmpId)
        Else
            Response.Redirect("SessionEnd.aspx?Mode=1")
        End If

        'If store number not set then redirect to set store page
        If m_objUserInfo.StoreNumber = -1 Or m_objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

        'clear previous popoup error messages
        pageBody.Attributes.Add("onload", "")

        'capture return key for tracking number input
        txtTrackNum.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){document.forms[0].btnSubmit.click()};")

        'get querystring variables from URL
        m_BoxId = Request.QueryString("BID") 'box id
        m_ParcelService = Request.QueryString("PARCEL") 'parcel service provider
        m_TType = Request.QueryString("TType") 'return/store to store
        m_strApprovedBy = Request.QueryString("Approval")
        m_strLBS = Request.QueryString("LBS")
        m_strRANum = Request.QueryString("RA")

        'set header properties
        ucHeader.lblTitle = "Shipping Label"
        ucHeader.CurrentMode(Header.HeaderGroup.Transfer)
        ucHeader.CurrentPage(Header.PageName.NewTransfer)
        ucHeader.DisableButtons()

    End Sub

   

    'Assigns tracking number entered by user to box if valid 
    Private Sub btnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssign.Click

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("spInsertBoxTrackingInfo", objConnection)

        objCommand.CommandType = CommandType.StoredProcedure

        Dim prmBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@BoxId", SqlDbType.VarChar, 14)
        Dim prmParcelService As SqlClient.SqlParameter = objCommand.Parameters.Add("@ParcelService", SqlDbType.SmallInt)
        Dim prmShipmentId As SqlClient.SqlParameter = objCommand.Parameters.Add("@ShipmentId", SqlDbType.VarChar, 18)
        Dim prmTrackingNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@TrackingNum", SqlDbType.VarChar, 30)
        Dim prmXMLShipping As SqlClient.SqlParameter = objCommand.Parameters.Add("@XMLShipping", SqlDbType.Bit)
        'Dim prmBoxStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@BoxStatus", SqlDbType.TinyInt)
        Dim prmCreated As SqlClient.SqlParameter = objCommand.Parameters.Add("@Created", SqlDbType.DateTime)
        Dim prmModified As SqlClient.SqlParameter = objCommand.Parameters.Add("@Modified", SqlDbType.DateTime)
        Dim prmTrackingInfoId As SqlClient.SqlParameter = objCommand.Parameters.Add("@TrackingInfo_Id", SqlDbType.Int)
        Dim prmApproved_By As SqlClient.SqlParameter = objCommand.Parameters.Add("@Approved_By", SqlDbType.VarChar, 35)
        Dim prmUPS_PackageType_Code As SqlClient.SqlParameter = objCommand.Parameters.Add("@UPS_PackageType_Code", SqlDbType.VarChar, 2)
        Dim prmUPS_Service_Code As SqlClient.SqlParameter = objCommand.Parameters.Add("@UPS_Service_Code", SqlDbType.VarChar, 2)

        'Set parameter directions
        prmBoxId.Direction = ParameterDirection.Input
        prmParcelService.Direction = ParameterDirection.Input
        prmShipmentId.Direction = ParameterDirection.Input
        prmTrackingNum.Direction = ParameterDirection.Input
        prmXMLShipping.Direction = ParameterDirection.Input
        'prmBoxStatus.Direction = ParameterDirection.Input
        prmCreated.Direction = ParameterDirection.Input
        prmModified.Direction = ParameterDirection.Input
        prmApproved_By.Direction = ParameterDirection.Input
        prmUPS_PackageType_Code.Direction = ParameterDirection.Input
        prmUPS_Service_Code.Direction = ParameterDirection.Input
        prmTrackingInfoId.Direction = ParameterDirection.Output

        'Set parameter values
        prmBoxId.Value = m_BoxId
        prmTrackingNum.Value = Replace(lblTrackNum.Text.Trim, " ", "") ' tracking number
        prmXMLShipping.Value = 0 'mark XML generated shipping number to false
        'prmBoxStatus.Value = 2 'Shipped
        prmParcelService.Value = m_ParcelService 'set parcel service
        prmShipmentId.Value = DBNull.Value ' XML required field not needed for manual input labels

        prmUPS_PackageType_Code.Value = DBNull.Value
        prmUPS_Service_Code.Value = DBNull.Value
        prmApproved_By.Value = m_strApprovedBy

        Dim strDate = Now()

        'set timestamps
        prmCreated.Value = strDate
        prmModified.Value = strDate

        objCommand.ExecuteNonQuery() 'execute query

        'clean up connection/command objects
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        'if storedprocedure returns an err then display generated message
        If prmTrackingInfoId.Value = -1 Then 'popup error box to user
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Tracking Number Has Already Been Used.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
        Else 'save tracking info
            ChangeBoxStatus() 'change box status to shipped
            UpdateBoxHistory(2) 'log shipped history
            UnlockTransfer() 'unlock box id

            'Output OMS/WMS Interface file
            Dim objTransfer As New Transfer(m_BoxId)
            Dim objStore As New Store(objTransfer.ReceivingStore)

            If ConfigurationSettings.AppSettings("RASNFlatFileOn") = "1" And m_TType = 2 Then
                Dim objBizTalkInterface As New BizTalkInterface(Replace(ConfigurationSettings.AppSettings("WMS_RASNFlatFileDir"), "~DC~", objTransfer.ReceivingStore))
                objBizTalkInterface.CreateFlatFile(m_BoxId) ' Create WMS Flat File
            ElseIf ConfigurationSettings.AppSettings("OMSFlatFileOn") = "1" And objStore.IsInternetStore() Then
                Dim objBizTalkInterface As New BizTalkInterface(ConfigurationSettings.AppSettings("OMS_ASNFlatFileDir"))
                objBizTalkInterface.CreateFlatFile(m_BoxId) ' Create WMS Flat File
            End If

            If m_TType = 3 Then
                Response.Redirect("CreateNewTrans.aspx")
            ElseIf m_TType = 4 Then
                Dim objDDSReturn As New DDSReturn(ConfigurationSettings.AppSettings("strDDSReturnConn"))

                objDDSReturn.UpdateRAShipped(m_strRANum, m_objUserInfo.StoreNumber, m_BoxId, True, Now())

                Response.Redirect("CreateNewTrans.aspx")
            Else
                Response.Redirect("TransferProcessing.aspx?TType=" & m_TType & "&BID=" & m_BoxId) 'generate manifest
            End If

            'Response.Redirect("CreateNewTrans.aspx")
        End If

    End Sub

    'Unlock box id
    Private Sub UnlockTransfer()

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer(m_BoxId)

        objTransferLock = Nothing

    End Sub

    'Update box history
    Private Sub UpdateBoxHistory(ByVal intBoxStatus As Int32)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateBoxHistory '" & m_BoxId & "'," & intBoxStatus & ",'" & m_objUserInfo.EmpId & "','" & Replace(m_objUserInfo.EmpFullName, "'", "''") & "','" & Now() & "' ", objConnection)

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'change box status to shipped
    Private Sub ChangeBoxStatus()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateBoxHdrStatus", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@BID", SqlDbType.VarChar, 14)
        Dim parBoxStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@Status", SqlDbType.TinyInt)
        Dim parModifiedDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@Modified", SqlDbType.DateTime)

        'Set parameter directions
        parBoxId.Direction = ParameterDirection.Input
        parBoxStatus.Direction = ParameterDirection.Input
        parModifiedDate.Direction = ParameterDirection.Input

        'Set parameter values
        parBoxId.Value = m_BoxId
        parBoxStatus.Value = 2 'shipped
        parModifiedDate.Value = Now()

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    'Clear tracking number
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        btnClear.Visible = False
        btnAssign.Visible = False
        lblTrackNum.Text = ""
        txtTrackNum.Text = ""
        txtTrackNum.Visible = True
        btnSubmit.Visible = True
    End Sub

    'go back to label selection screen
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("SelectShipping.aspx?LBS=" & m_strLBS & "&BID=" & m_BoxId & "&TType=" & m_TType & "&UPSOn=1&Err=0")
    End Sub

    'Assign tacking number
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click


        If Len(txtTrackNum.Text) > 0 Then
            txtTrackNum.Text = UCase(txtTrackNum.Text)
            lblTrackNum.Text = txtTrackNum.Text
            txtTrackNum.Visible = False
            btnClear.Visible = True
            btnAssign.Visible = True
            btnSubmit.Visible = False

        Else

            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Tracking Number can not be blank.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")

        End If

    End Sub

End Class
