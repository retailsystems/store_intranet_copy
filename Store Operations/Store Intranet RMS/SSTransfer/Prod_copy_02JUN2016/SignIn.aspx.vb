Imports System.Data
Imports System.Data.OleDb

Public Class SignIn
    Inherits System.Web.UI.Page
    Protected WithEvents txtEmployeeId As System.Web.UI.WebControls.TextBox
    Protected WithEvents radReturn As System.Web.UI.WebControls.RadioButton
    Protected WithEvents radStore As System.Web.UI.WebControls.RadioButton
    Protected WithEvents ph As System.Web.UI.WebControls.PlaceHolder
    Protected WithEvents layoutHeader As Header
    Protected WithEvents trTranType As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents btnSubmitBlank As System.Web.UI.WebControls.Button
    Protected WithEvents trTranTypeSpace As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trEmpId As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents trEmpIdBlank As System.Web.UI.HtmlControls.HtmlTableRow

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        layoutHeader.lblTitle = "Sign In"

        If Request.QueryString("TTypeOn") = 0 Then
            trTranType.Visible = False
            trTranTypeSpace.Visible = False
        End If

        If Not IsNothing(Request.Cookies("EmpId")) And Request.QueryString("TTypeOn") = 1 Then
            If Request.Cookies("EmpId").Value = "" Then
                trEmpIdBlank.Visible = False
            Else
                trEmpIdBlank.Visible = True
                trEmpId.Visible = False
            End If

        Else
            trEmpIdBlank.Visible = False
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim strSQL As String
        Dim objORACommand As New OleDbCommand()
        Dim objORAConnection As New OleDbConnection(ConfigurationSettings.AppSettings("strORAConnection"))
        Dim objORADataReader As OleDbDataReader
        Dim objSQLDataReader As SqlClient.SqlDataReader

        objORAConnection.Open()

        strSQL = "SELECT U.EMPLOYEEID, U.NAMEFIRST, U.NAMELAST "
        strSQL = strSQL & "FROM TAULTIMATEEMPLOYEE U "
        strSQL = strSQL & "WHERE U.EMPLOYEEID = '" & txtEmployeeId.Text & "' "

        objORACommand.Connection = objORAConnection
        objORACommand.CommandText = strSQL
        objORADataReader = objORACommand.ExecuteReader()

        If objORADataReader.Read Then

            Response.Cookies("EmpId").Value = txtEmployeeId.Text
            Response.Cookies("EmpFirstName").Value = objORADataReader("NAMEFIRST")
            Response.Cookies("EmpLastName").Value = objORADataReader("NAMELAST")

            If radStore.Checked Then
                Page.Response.Redirect(Request.QueryString("Page") & "?TType=1&Nav=" & Request("Nav"))
            Else
                Page.Response.Redirect(Request.QueryString("Page") & "?TType=2&Nav=" & Request("Nav"))
            End If

        End If

    End Sub

    Private Sub btnSubmitBlank_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmitBlank.Click

        If radStore.Checked Then
            Page.Response.Redirect(Request.QueryString("Page") & "?TType=1&Nav=" & Request("Nav"))
        Else
            Page.Response.Redirect(Request.QueryString("Page") & "?TType=2&Nav=" & Request("Nav"))
        End If

    End Sub

End Class
