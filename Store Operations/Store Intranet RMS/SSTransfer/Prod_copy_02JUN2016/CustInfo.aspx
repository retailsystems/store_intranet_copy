<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CustInfo.aspx.vb" Inherits="SSTransfer.CustInfo"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Customer Information</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<!--<script language="javascript">
		window.focus();
		frmCustInfo.txtHomePhone.focus();
		</script>-->
		<asp:placeholder id="phCloseWindow" visible="false" Runat="server">
			<SCRIPT language="javascript">
			parent.closepopup();		
			</SCRIPT>
		</asp:placeholder>
	</HEAD>
	<body scroll="no" bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmCustInfo" method="post" runat="server">
			<table style="WIDTH: 100%; HEIGHT: 100%" borderColor="#990000" cellSpacing="0" borderColorDark="#990000" cellPadding="1" width="761" borderColorLight="#990000" border="3">
				<tr>
					<td align="middle" bgColor="#440000" colSpan="2"><asp:label id="lblHeader" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="medium" Font-Bold="True">Customer Information</asp:label></td>
				</tr>
				<tr>
					<td align="middle" bgColor="#000000">
						<table cellSpacing="0" cellPadding="1" border="0">
							<tr id="trCustNumber" runat="server">
								<td noWrap><asp:label id="lblHomePhone" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">Home Phone:</asp:label></td>
								<td noWrap><asp:Label Runat="server" ID="lblOParen" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="medium" font-bold="True" Height="20px">(</asp:Label>&nbsp;<asp:textbox id="txtAreaCode" runat="server" Width="35px" MaxLength="3" TabIndex="1"></asp:textbox>&nbsp;<asp:Label Runat="server" ID="lblEParen" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="medium" font-bold="True" Height="20px">)</asp:Label>&nbsp;<asp:TextBox Runat="server" ID="txtPrefix" Width="35px" MaxLength="3"></asp:TextBox>&nbsp;<asp:Label Runat="server" ID="lblPDash" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="medium" font-bold="True">-</asp:Label>&nbsp;<asp:TextBox Runat="server" ID="txtLast4" Width="45px" MaxLength="4"></asp:TextBox>&nbsp;&nbsp;<asp:button id="btnRetrieve" runat="server" ForeColor="White" Font-Bold="True" Width="134px" Text="Retrieve Customer" BackColor="#440000" BorderColor="#990000" TabIndex="2"></asp:button></td>
							</tr>
							<tr id="trListCustomers" runat="server" width="100%">
								<td vAlign="top" noWrap><asp:label id="lblCustomerList" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-size="x-small" font-bold="True" Height="22px">List of Customers:</asp:label></td>
								<td vAlign="top" noWrap borderColor="gray" borderColorLight="navy" align="left" bgColor="#000000" colSpan="2">
								<asp:datagrid id="dgCustomerList" runat="server" Font-Names="Arial" ForeColor="Black" Font-Size="X-Small" Width="100%" BackColor="White" BorderColor="white" Height="40px" CellSpacing="0" CellPadding="0"  ItemStyle-Height="15px" ShowHeader="False" AutoGenerateColumns="False" BorderStyle="solid" GridLines="None" TabIndex="3">
										<Columns>
											<asp:TemplateColumn>
												<ItemTemplate>
													<a href="CustInfo.aspx?CIDX=<%# container.dataitem("ID")%>"><font color="Black" size="2" face="arial"><%# container.dataitem("FirstName") & " " & container.dataitem("LastName")%></font></a>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></td>
							</tr>
							<tr id="trCustomerInfo" runat="server">
								<td noWrap><asp:label id="lblCustomerInfo" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="X-Small" font-bold="True" Font-Underline="False">Customer Information:</asp:label></td>
							</tr>
							<tr id="trCustomerName" runat="server">
								<td noWrap><asp:label id="lblFName" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">First Name:</asp:label></td>
								<td noWrap><asp:textbox id="txtFName" runat="server" Width="200px" TabIndex="4"></asp:textbox>
									&nbsp;<asp:label id="lblLName" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">Last Name:</asp:label>
									&nbsp;<asp:textbox id="txtLName" runat="server" Width="185px" TabIndex="5"></asp:textbox></td>
							</tr>
							<tr id="trstrAddress" runat="server">
								<td noWrap><asp:label id="lblStrAddress" runat="server" visible="False" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">Street Address:</asp:label></td>
								<td noWrap><asp:textbox id="txtStrAddress" runat="server" Width="472px" visible="False" TabIndex="6"></asp:textbox></td>
							</tr>
							<tr id="trCityStateZip" runat="server">
								<td noWrap><asp:label id="lblCity" runat="server" Font-Names="Arial" visible="False" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">City:</asp:label></td>
								<td noWrap><asp:textbox id="txtCity" runat="server" Width="212px" visible="False" TabIndex="7" ></asp:textbox>
									&nbsp;<asp:label id="lblState" runat="server" Font-Names="Arial" visible="False" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">State:</asp:label>
									&nbsp;<asp:textbox id="txtState" runat="server" Width="43px" visible="False" MaxLength="2" TabIndex="8"></asp:textbox>
									&nbsp;<asp:label id="lblZip" runat="server" Font-Names="Arial" visible="False" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">Zip:</asp:label>
									&nbsp;<asp:textbox id="txtZip" runat="server" Width="103px" visible="False" MaxLength="12" TabIndex="9"></asp:textbox></td>
							</tr>
							<tr id="trWorkPhone" runat="server">
								<td noWrap><asp:label id="lblWorkPhone" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">Work Phone:</asp:label></td>
								<td noWrap><asp:textbox id="txtWorkPhone" runat="server" Width="200px" MaxLength="10" TabIndex="10"></asp:textbox></td>
							</tr>
							<tr id="trNotes" runat="server">
								<td noWrap style="HEIGHT: 71px"><asp:label id="lblNotes" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">Notes:</asp:label></td>
								<td noWrap style="HEIGHT: 71px"><asp:textbox id="txtNotes" runat="server" Width="474px" TextMode="MultiLine" Wrap="True" Height="66px" TabIndex="11"></asp:textbox></td>
							</tr>
							<tr id="trMessage" runat="server">
								<td colSpan="2"><asp:label id="lblMessage" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True" Width="620px" visible="true"></asp:label></td>
							</tr>
							<tr id="TrCloseSubmitbtns" runat="server">
								<td noWrap colSpan="2" align="right"><input id="HCustId" type="hidden" runat="server" NAME="HCustId">
									<input id="HCustName" type="hidden" runat="server" NAME="HCustName">&nbsp;
									<asp:button id="btnAddNew" runat="server" ForeColor="White" Font-Bold="True" Width="106px" Text="Add Customer" BackColor="#440000" BorderColor="#990000" Visible="False" TabIndex="12"></asp:button>&nbsp;
									<asp:button id="btnUpdate" runat="server" ForeColor="White" Font-Bold="True" Width="71px" Text="Update" BackColor="#440000" BorderColor="#990000" Visible="False" TabIndex="13"></asp:button>&nbsp;
									<asp:PlaceHolder ID="phAssign" Runat="server" Visible="False"><input id="btntest" style="BORDER-TOP-WIDTH: 1px; FONT-WEIGHT: bold; BORDER-LEFT-WIDTH: 1px; BORDER-LEFT-COLOR: #990000; BORDER-BOTTOM-WIDTH: 1px; BORDER-BOTTOM-COLOR: #990000; WIDTH: 61px; COLOR: white; BORDER-TOP-COLOR: #990000; FONT-FAMILY: Arial; HEIGHT: 24px; BACKGROUND-COLOR: #440000; BORDER-RIGHT-WIDTH: 1px; BORDER-RIGHT-COLOR: #990000" onclick="javascript:parent.setCustId(HCustId.value,HCustName.value);window.close();" type="button" value="Assign" tabindex="14"></asp:PlaceHolder>
									&nbsp;<asp:button id="btnFClose" runat="server" ForeColor="White" Font-Bold="True" Width="69px" Text="Close" BackColor="#440000" BorderColor="#990000" TabIndex="15"></asp:button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<script language="javascript">
		window.focus();
		frmCustInfo.txtHomePhone.focus();
		</script>
		</form>
	
	</body>
</HTML>
