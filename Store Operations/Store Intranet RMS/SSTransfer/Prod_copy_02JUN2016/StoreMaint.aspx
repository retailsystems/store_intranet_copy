<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="StoreMaint.aspx.vb" Inherits="SSTransfer.WebForm1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Store Search</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript">
			<!--
			javascript:window.history.forward(1);
			//-->
		</script>
	</HEAD>
	<body id="pageBody" bgColor="black" leftMargin="0" topMargin="0" scroll="no" runat="server">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD><uc1:header id="ucHeader" runat="server"></uc1:header></TD>
					</TD></TR>
			</TABLE>
			<TABLE id="Table1" style="BORDER-TOP-WIDTH: thin; BORDER-LEFT-WIDTH: thin; BORDER-LEFT-COLOR: #cc3366; BORDER-BOTTOM-WIDTH: thin; BORDER-BOTTOM-COLOR: #cc3366; WIDTH: 100%; BORDER-TOP-COLOR: #cc3366; HEIGHT: 172px; BORDER-RIGHT-WIDTH: thin; BORDER-RIGHT-COLOR: #cc3366" cellSpacing="0" cellPadding="2" width="994" align="left" border="0">
				<TR>
					<TD style="WIDTH: 131px" align="left"><asp:label id="headStoreSrch" runat="server" BackColor="Transparent" ForeColor="Red" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True" Font-Italic="True">Store Search</asp:label></TD>
					<TD style="WIDTH: 262px"></TD>
					<TD style="WIDTH: 123px" align="right"><SPAN></SPAN></TD>
					<TD style="WIDTH: 282px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 131px" align="right"><asp:label id="lblStoNum" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Store Number:</asp:label></TD>
					<TD style="WIDTH: 262px"><asp:textbox id="txtSrchStoNum" runat="server" Width="60px"></asp:textbox></TD>
					<TD style="WIDTH: 123px" align="right"><SPAN><SPAN><asp:label id="lblSrchOpenDate" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Open Date:</asp:label></SPAN></SPAN></TD>
					<TD style="WIDTH: 282px"><asp:textbox id="txtFDate" runat="server" Width="60px"></asp:textbox>&nbsp;&nbsp;<asp:label id="lblTo2" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">To  </asp:label>&nbsp;&nbsp;<asp:textbox id="txtTDate" runat="server" Width="60px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 131px; HEIGHT: 28px" align="right"><asp:label id="lblStoName" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Store Name:</asp:label></TD>
					<TD style="WIDTH: 262px; HEIGHT: 28px"><asp:textbox id="txtSrchStoName" runat="server" Width="153px"></asp:textbox></TD>
					<TD style="WIDTH: 123px; HEIGHT: 28px" align="right"><SPAN><asp:label id="lblSrchRegion" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Region :</asp:label></SPAN></TD>
					<TD style="WIDTH: 282px; HEIGHT: 28px"><asp:dropdownlist id="cboSrchRegion" runat="server" Width="60px"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 131px; HEIGHT: 13px" align="right"><asp:label id="lbSrchState" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">State:</asp:label></TD>
					<TD style="WIDTH: 262px; HEIGHT: 13px"><asp:dropdownlist id="cboSrchState" runat="server" Width="60px" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD style="WIDTH: 123px; HEIGHT: 13px" align="right"><asp:label id="lblSrchDist" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">District :</asp:label></TD>
					<TD style="WIDTH: 282px; HEIGHT: 13px"><asp:dropdownlist id="cboSrchDist" runat="server" Width="60px"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 131px; HEIGHT: 19px" align="right"><asp:label id="lblCity1" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">City:</asp:label></TD>
					<TD style="WIDTH: 262px; HEIGHT: 19px"><asp:dropdownlist id="cboCity" runat="server" Width="153px"></asp:dropdownlist></TD>
					<TD style="WIDTH: 123px; HEIGHT: 19px" align="right"><asp:label id="lblMgr" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Manager :</asp:label></TD>
					<TD style="WIDTH: 282px; HEIGHT: 19px"><asp:dropdownlist id="cboMgr" runat="server" Width="153px"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 131px; HEIGHT: 19px" align="right"></TD>
					<TD style="WIDTH: 262px; HEIGHT: 19px"></TD>
					<TD style="WIDTH: 123px; HEIGHT: 19px" align="right"><asp:label id="Label2" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Sq. Feet :</asp:label>&nbsp;</TD>
					<TD style="WIDTH: 282px; HEIGHT: 19px"><asp:textbox id="txtSize" runat="server" Width="60px"></asp:textbox>&nbsp;&nbsp;
						<asp:label id="lblTo" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">To  </asp:label><asp:textbox id="txtSize2" runat="server" Width="60px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 131px; HEIGHT: 19px" align="right"></TD>
					<TD style="WIDTH: 262px; HEIGHT: 19px"><asp:label id="lblEmpty" runat="server" BackColor="Transparent" ForeColor="Red" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Generic</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label id="lblError" runat="server" BackColor="Transparent" ForeColor="Red" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Error</asp:label></TD>
					<TD style="WIDTH: 123px; HEIGHT: 19px" align="middle"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 131px; HEIGHT: 20px" align="right"></TD>
					<TD style="WIDTH: 262px; HEIGHT: 20px"><asp:label id="lblNoResults" runat="server" BackColor="Transparent" ForeColor="Red" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">No Record(s) Found.</asp:label></TD>
					<TD style="WIDTH: 123px; HEIGHT: 20px"><asp:button id="btnSearch" runat="server" BackColor="#440000" ForeColor="White" Font-Bold="True" Width="93px" BorderColor="#990000" Text="Search"></asp:button></TD>
					<TD style="WIDTH: 282px; HEIGHT: 20px"><asp:button id="btnClear" runat="server" BackColor="#440000" ForeColor="White" Font-Bold="True" Width="95px" BorderColor="#990000" Text="Clear Fields"></asp:button><SPAN></SPAN>&nbsp;&nbsp;<asp:button id="btnAdd" runat="server" BackColor="#440000" ForeColor="White" Font-Bold="True" Width="95px" BorderColor="#990000" Text="Add Store"></asp:button></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 131px; HEIGHT: 20px" align="right"><asp:label id="lblSrchEmail" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True" Enabled="False" Visible="False">Email:</asp:label></TD>
					<TD style="WIDTH: 262px; HEIGHT: 20px"><asp:textbox id="txtSrchEmail" runat="server" Width="268px" Enabled="False" Visible="False"></asp:textbox></TD>
					<TD style="WIDTH: 123px; HEIGHT: 20px"></TD>
					<TD style="WIDTH: 282px; HEIGHT: 20px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 131px; HEIGHT: 20px" align="right"></TD>
					<TD style="WIDTH: 262px; HEIGHT: 20px"></TD>
					<TD style="WIDTH: 123px; HEIGHT: 20px"><asp:label id="lblSrchTags" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True" Enabled="False" Visible="False">Tag :</asp:label><asp:textbox id="txtSrchTag" runat="server" Width="19px" Enabled="False" Visible="False"></asp:textbox><asp:label id="lblSrchSimon" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True" Enabled="False" Visible="False">Simon :</asp:label><asp:textbox id="txtSrchSimon" runat="server" Width="19px" Enabled="False" Visible="False"></asp:textbox></TD>
					<TD style="WIDTH: 282px; HEIGHT: 20px"><asp:label id="lblShipNumb" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True" Enabled="False" Visible="False">Shipper Number :</asp:label><asp:textbox id="txtShipNumb" runat="server" Width="194px" Enabled="False" Visible="False"></asp:textbox></TD>
				</TR>
			</TABLE>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
			<P>
				<TABLE id="Table3" style="WIDTH: 145px; HEIGHT: 29px" cellSpacing="0" cellPadding="0" width="145" border="0">
					<TR>
						<TD align="middle"><asp:label id="lblSrchRes" runat="server" BackColor="Transparent" ForeColor="Red" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True" Font-Italic="True" Width="199px">Store Search : Search Results</asp:label></TD>
					</TR>
				</TABLE>
			</P>
			<P></P>
			<P></P>
			<P></P>
			<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
				<TBODY>
					<TR>
						<TD align="middle"><asp:datagrid id="dgSearch" runat="server" BackColor="Transparent" ForeColor="GhostWhite" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True" Width="100%" BorderColor="Red" PageSize="5" BorderStyle="Solid" BorderWidth="0px" AutoGenerateColumns="False" AllowPaging="True">
								<SelectedItemStyle ForeColor="Transparent" BorderStyle="Solid"></SelectedItemStyle>
								<AlternatingItemStyle BackColor="DimGray"></AlternatingItemStyle>
								<HeaderStyle BackColor="#804040"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText=" ">
										<ItemTemplate>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:HyperLinkColumn DataNavigateUrlField="Store Number" DataNavigateUrlFormatString="javascript:var win=open('StoreDetail.aspx?pStoreID={0}','_blank','status=yes,width=790,height=400, scrollbars=no,resizeable=True')" DataTextField="Store Number" HeaderText="Store Number"></asp:HyperLinkColumn>
									<asp:BoundColumn DataField="Store Name" HeaderText="Store Name"></asp:BoundColumn>
									<asp:BoundColumn DataField="Address" HeaderText="Address"></asp:BoundColumn>
									<asp:BoundColumn DataField="City" HeaderText="City"></asp:BoundColumn>
									<asp:BoundColumn DataField="State" HeaderText="State"></asp:BoundColumn>
									<asp:BoundColumn DataField="Phone1" HeaderText="Phone Number"></asp:BoundColumn>
									<asp:BoundColumn DataField="Dimension" HeaderText="Sq. Feet"></asp:BoundColumn>
									<asp:BoundColumn DataField="Manager" HeaderText="Manager"></asp:BoundColumn>
								</Columns>
								<PagerStyle NextPageText="" PrevPageText="" BorderStyle="None" ForeColor="White" BackColor="Black" PageButtonCount="5" Mode="NumericPages"></PagerStyle>
							</asp:datagrid></TD>
					</TR>
				</TBODY>
			</TABLE>
			<BR>
			<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
				<TBODY>
					<TR>
						<TD style="WIDTH: 17px" align="middle"></TD>
						<TD><asp:label id="lblLogin" runat="server" BackColor="Transparent" ForeColor="Red" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True" Font-Italic="True">Login</asp:label></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 17px"></TD>
						<TD><asp:textbox id="txtUserID" runat="server" Width="174px"></asp:textbox>&nbsp;<SPAN></SPAN><asp:button id="btnSubmit" runat="server" Width="93px" Text="Submit"></asp:button>&nbsp;
							<asp:button id="btnCancel" runat="server" Width="93px" Text="Cancel"></asp:button></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 17px"></TD>
						<TD><asp:label id="lblLoginMsg" runat="server" BackColor="Transparent" ForeColor="Red" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Generic.</asp:label></TD>
					</TR>
				</TBODY>
			</TABLE>
			<P></P>
			<TABLE id="tblDisplay" style="BORDER-TOP-WIDTH: thin; BORDER-LEFT-WIDTH: thin; BORDER-LEFT-COLOR: #cc0033; BORDER-BOTTOM-WIDTH: thin; BORDER-BOTTOM-COLOR: #cc0033; BORDER-TOP-COLOR: #cc0033; BORDER-RIGHT-WIDTH: thin; BORDER-RIGHT-COLOR: #cc0033" cellSpacing="0" cellPadding="0" width="100%" align="left" border="0">
				<TBODY>
					<TR>
						<TD style="WIDTH: 139px" align="middle"><asp:label id="headStore" runat="server" BackColor="Transparent" ForeColor="Red" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True" Font-Italic="True" Width="125px">Store Data</asp:label></TD>
						<TD style="WIDTH: 272px"></TD>
						<TD style="WIDTH: 137px" align="right"></TD>
						<TD style="WIDTH: 282px"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px"></TD>
						<TD style="WIDTH: 272px"><SPAN><asp:label id="lblSaved" runat="server" BackColor="Transparent" ForeColor="Red" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Record Saved.</asp:label>an</SPAN>
							<asp:label id="lblRecordDeleted" runat="server" BackColor="Transparent" ForeColor="Red" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Record Deleted.</asp:label></TD>
						<TD style="WIDTH: 137px" align="right"></TD>
						<TD style="WIDTH: 282px"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px" align="right"><asp:label id="lblStoreNum" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Store Number:</asp:label></TD>
						<TD style="WIDTH: 272px"><asp:textbox id="txtStoNum" runat="server" Font-Bold="True" Width="60px" Enabled="False" ReadOnly="True"></asp:textbox></TD>
						<TD style="WIDTH: 137px" align="right"><asp:label id="lbtStoSize" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Store Size:</asp:label></TD>
						<TD style="WIDTH: 282px"><asp:textbox id="txtStoDim" runat="server" Font-Bold="True" Width="60px" Enabled="False"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px" align="right"><asp:label id="lblStoreName" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Store Name:</asp:label></TD>
						<TD style="WIDTH: 272px"><asp:textbox id="txtStoName" runat="server" Font-Bold="True" Width="200px" Enabled="False" Wrap="False"></asp:textbox></TD>
						<TD style="WIDTH: 137px" align="right"><asp:label id="lblRegion" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Region:</asp:label></TD>
						<TD style="WIDTH: 282px"><asp:dropdownlist id="cboRegion" runat="server" Font-Bold="True" Width="60px" Enabled="False"></asp:dropdownlist></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px; HEIGHT: 23px" align="right"><asp:label id="lblStoAddr" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Address:</asp:label></TD>
						<TD style="WIDTH: 272px; HEIGHT: 23px"><asp:textbox id="txtAddr" runat="server" Font-Bold="True" Width="200px" Enabled="False" Wrap="False"></asp:textbox></TD>
						<TD style="WIDTH: 137px; HEIGHT: 23px" align="right"><asp:label id="lblDist" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">District:</asp:label></TD>
						<TD style="WIDTH: 282px; HEIGHT: 23px"><asp:dropdownlist id="cboDistrict" runat="server" Font-Bold="True" Width="60px" Enabled="False"></asp:dropdownlist></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px" align="right"><asp:label id="lblCity" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">City:</asp:label></TD>
						<TD style="WIDTH: 272px"><asp:textbox id="txtCity" runat="server" Font-Bold="True" Width="200px" Enabled="False" Wrap="False"></asp:textbox></TD>
						<TD style="WIDTH: 137px" align="right"><asp:label id="labManager" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Store Manager:</asp:label></TD>
						<TD style="WIDTH: 282px"><asp:textbox id="txtStoMgr" runat="server" Font-Bold="True" Width="200px" Enabled="False" Wrap="False"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px" align="right"><asp:label id="lblState" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">State:</asp:label></TD>
						<TD style="WIDTH: 272px"><asp:dropdownlist id="cboState" runat="server" Font-Bold="True" Width="60px" Enabled="False"></asp:dropdownlist></TD>
						<TD style="WIDTH: 137px" align="right"><asp:label id="lblAsstMgr1" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Asst. Manager:</asp:label></TD>
						<TD style="WIDTH: 282px"><asp:textbox id="txtAsstMgr1" runat="server" Font-Bold="True" Width="200px" Enabled="False" Wrap="False"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px" align="right"><asp:label id="lblZip" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Zip:</asp:label></TD>
						<TD style="WIDTH: 272px"><asp:textbox id="txtZip" runat="server" Font-Bold="True" Width="60px" Enabled="False"></asp:textbox></TD>
						<TD style="WIDTH: 137px" align="right"><asp:label id="lblAsstMgr2" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Asst. Manager :</asp:label></TD>
						<TD style="WIDTH: 282px"><asp:textbox id="txtAsstMgr2" runat="server" Font-Bold="True" Width="200px" Enabled="False" Wrap="False"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px" align="right"><asp:label id="lblPhone1" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Phone 1:</asp:label></TD>
						<TD style="WIDTH: 272px"><asp:textbox id="txtPhone1" runat="server" Font-Bold="True" Width="200px" Enabled="False" Wrap="False"></asp:textbox></TD>
						<TD style="WIDTH: 137px" align="right"><asp:label id="lblAsstMgr3" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Asst. Manager :</asp:label></TD>
						<TD style="WIDTH: 282px"><asp:textbox id="txtAsstMgr3" runat="server" Font-Bold="True" Width="200px" Enabled="False" Wrap="False"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px" align="right"><asp:label id="lblPhone2" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Phone 2:</asp:label></TD>
						<TD style="WIDTH: 272px"><asp:textbox id="txtPhone2" runat="server" Font-Bold="True" Width="200px" Wrap="False"></asp:textbox></TD>
						<TD style="WIDTH: 137px" align="right"><asp:label id="lblAsstMgr4" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Asst. Manager :</asp:label></TD>
						<TD style="WIDTH: 282px"><asp:textbox id="txtAsstMgr4" runat="server" Font-Bold="True" Width="200px" Enabled="False" Wrap="False"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px; HEIGHT: 23px" align="right"><asp:label id="lblEMail" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">E-Mail:</asp:label></TD>
						<TD style="WIDTH: 272px; HEIGHT: 23px"><asp:textbox id="txtEmail" runat="server" Font-Bold="True" Width="200px" Enabled="False" Wrap="False"></asp:textbox></TD>
						<TD style="WIDTH: 137px; HEIGHT: 23px" align="right"><asp:label id="lblAsstMgr5" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Asst. Manager :</asp:label></TD>
						<TD style="WIDTH: 282px; HEIGHT: 23px"><asp:textbox id="txtAsstMgr5" runat="server" Width="200px" Enabled="False" Wrap="False"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px" align="right"><asp:label id="lblOpenDate" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Open Date:</asp:label></TD>
						<TD style="WIDTH: 272px"><asp:textbox id="txtOpenDate" runat="server" Font-Bold="True" Width="200px" Enabled="False" Wrap="False"></asp:textbox></TD>
						<TD style="WIDTH: 137px" align="right"><asp:label id="lblTags" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Tags:</asp:label></TD>
						<TD style="WIDTH: 282px"><asp:textbox id="txtTags" runat="server" Font-Bold="True" Width="60px" Enabled="False"></asp:textbox>&nbsp;&nbsp;&nbsp; 
							&nbsp;<asp:label id="lblSimon" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Simon:</asp:label>&nbsp;&nbsp;&nbsp;<asp:textbox id="txtSimon" runat="server" Font-Bold="True" Width="62px" Enabled="False"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px" align="right"></TD>
						<TD style="WIDTH: 272px"></TD>
						<TD style="WIDTH: 137px" align="right"><asp:label id="lblCoName" runat="server" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller" Font-Bold="True">Company Name :</asp:label></TD>
						<TD style="WIDTH: 282px"><asp:textbox id="txtCoName" runat="server" Font-Bold="True" Width="200px" Enabled="False"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px; HEIGHT: 31px" align="right"></TD>
						<TD style="WIDTH: 272px; HEIGHT: 31px"></TD>
						<TD style="WIDTH: 137px; HEIGHT: 31px" align="right"></TD>
						<TD style="WIDTH: 282px; HEIGHT: 31px"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 139px" align="right"></TD>
						<TD style="WIDTH: 272px"></TD>
						<TD style="WIDTH: 137px" align="right"><asp:button id="btnModify" runat="server" Width="93px" Text="Save Record" Enabled="False" Visible="False"></asp:button></TD>
						<TD style="WIDTH: 282px"><SPAN>Span</SPAN>
							<asp:button id="btnDelete" runat="server" Width="99px" Text="Delete Record" Enabled="False" Visible="False"></asp:button><SPAN>Span</SPAN>
							<asp:button id="btnOK" runat="server" Width="99px" Text="OK" Enabled="False" Visible="False"></asp:button></TD>
					</TR>
				</TBODY>
			</TABLE>
		</form>
	</body>
</HTML>
