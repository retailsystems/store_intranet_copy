Imports System.Net
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Text
Imports System.IO

Public Class UPSTEST
    Inherits System.Web.UI.Page
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim bc As HttpBrowserCapabilities
        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        bc = Request.Browser

        objConnection.Open()


        strStoredProcedure = "SELECT XMLRequest From UPS_XML_Errors where XMLRequest is not null"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        objDataReader.Read()
        Label1.Text = objDataReader("XMLRequest")
        objDataReader.Close()


    End Sub

End Class
