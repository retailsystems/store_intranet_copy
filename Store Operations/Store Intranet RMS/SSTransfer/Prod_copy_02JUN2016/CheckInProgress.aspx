<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CheckInProgress.aspx.vb" Inherits="SSTransfer.CheckInProgress"%>
<%@ Register TagPrefix="uc1" TagName="ScanItem" Src="UserControls/ScanItem.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TransferBox" Src="UserControls/TransferBox.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TransferDetail" Src="UserControls/TransferDetail.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Transfer Home</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" alink="white" vlink="white" link="white">
		<form id="frmTransferHome" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="4" width="100%" bgcolor="#000000" border="0">
				<tr>
					<td height="1"><uc1:Header id="layoutHeader" runat="server"></uc1:Header></td>
				</tr>
				<tr>
					<td align="middle" valign="top">
						<table cellSpacing="0" cellPadding="2" width="98%" border="0">
							<tr>
								<td align="middle">
									<uc1:TransferDetail id="TransferDetail1" runat="server"></uc1:TransferDetail>
								</td>
							</tr>
						</table>
						<hr color="#990000" width="100%">
						<br>
						<table cellSpacing="0" cellPadding="2" width="98%" border="0">
							<tr>
								<td align="middle">&nbsp;
									<uc1:ScanItem id="ucScanItem" runat="server"></uc1:ScanItem>
								</td>
							</tr>
						</table>
						<br>
						<table cellSpacing="0" cellPadding="2" width="98%" border="0">
							<tr>
								<td><font face="arial" color="white" size="3"><b>Scanned</b></font></td>
							</tr>
						</table>
						<table cellSpacing="0" cellPadding="2" width="98%" border="0">
							<tr>
								<td>
									<div style="OVERFLOW:auto; WIDTH:100%; HEIGHT:100px">
										<uc1:TransferBox id="TransferBox1" runat="server"></uc1:TransferBox>
									</div>
								</td>
							</tr>
						</table>
						<br>
						<table cellSpacing="0" cellPadding="2" width="98%" border="0">
							<tr>
								<td align="middle">
									<asp:Button id="btnAcceptAll" runat="server" Text="Accept All" BackColor="#440000" BorderColor="#990000" ForeColor="White" Font-Bold="True"></asp:Button>&nbsp;
									<asp:Button id="btnScanComplete" runat="server" Text="Scan Complete" BackColor="#440000" BorderColor="#990000" ForeColor="White" Font-Bold="True"></asp:Button>&nbsp;
									<asp:Button id="btnSave" runat="server" Text="Save for Later" BackColor="#440000" BorderColor="#990000" ForeColor="White" Font-Bold="True"></asp:Button>&nbsp;
									<asp:Button id="btnNextBox" runat="server" Text="Next Box" BackColor="#440000" BorderColor="#990000" ForeColor="White" Font-Bold="True"></asp:Button>&nbsp;
									<asp:Button id="btnSubmit" runat="server" Text="Submit" BackColor="#440000" BorderColor="#990000" ForeColor="White" Font-Bold="True"></asp:Button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:Footer id="Footer1" runat="server"></uc1:Footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
