
Imports System.Data.OleDb


Public Class StoreLogin

    Inherits System.Web.UI.Page

    'connStrings 
    Dim connHeader As System.Data.SqlClient.SqlConnection
    Protected WithEvents txtUserID As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblLoginMsg As System.Web.UI.WebControls.Label
    Dim connOra As System.Data.OleDb.OleDbConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblLoginMsg.Text = "An Employee ID is requested for this action. Please enter your ID."

    End Sub


    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnOra_Open()

        connOra = New System.Data.OleDb.OleDbConnection("Provider=MSDAORA.1;Password=hottopic;User ID=hottopic;Data Source=genret")
        connOra.Open()

    End Sub

    '-----------------------------------------------------------------------------
    ' 
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnOra_Close()

        connOra.Close()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub Login_Hide()

        txtUserID.Visible = False
        btnSubmit.Visible = False
        lblLoginMsg.Visible = False

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub Login_Show()

        txtUserID.Visible = True
        btnSubmit.Visible = True

    End Sub


    '-----------------------------------------------------------------------------
    ' Actions taken when Login Submit button clicked 
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim fEmpId As Integer
        Dim TfEmpID As String

        TfEmpID = Trim(txtUserID.Text)

        'Input Validation :: If Non-numeric entered
        If Not IsNumeric(TfEmpID) Then

            'set field to none
            txtUserID.Text = Nothing

            'show err msg
            lblLoginMsg.Visible = True

            'fill err msg
            lblLoginMsg.Text = "Incorrect Input Type. Please enter Employee ID"

        Else

            'If Numeric Value entered
            'Assign the Values 
            fEmpId = CInt(TfEmpID)

            'Check the return val
            Dim Chk As Integer
            Chk = OK_User(fEmpId)

            If Chk > 0 Then
                Response.Redirect("StoreDetail.aspx?Empty=1")
                txtUserID.Text = Nothing
            Else
                Login_Show()
                txtUserID.Text = Nothing
                lblLoginMsg.Visible = True
                lblLoginMsg.Text = "Employee ID not found. Please enter another Employee ID."
            End If

        End If

    End Sub


    '-----------------------------------------------------------------------------
    ' Connects to Database and checks if the user exists, returns value if exists
    '
    '
    '-----------------------------------------------------------------------------

    Private Function OK_User(ByVal fEmpID) As Integer

        Dim Boole As Integer

        ConnOra_Open()

        'returns the count number of employees found for that
        'employeeid
        Dim cmdOKUser As System.Data.OleDb.OleDbCommand
        cmdOKUser = New OleDbCommand("fxnValidEmp", connOra)
        cmdOKUser.CommandType = CommandType.StoredProcedure

        Dim prmOUT As OleDbParameter
        Dim prmIN As OleDbParameter

        prmOUT = cmdOKUser.CreateParameter()
        prmOUT.Direction = ParameterDirection.ReturnValue
        prmOUT.OleDbType = OleDbType.Integer
        prmOUT.Size = 100

        prmIN = cmdOKUser.CreateParameter()
        prmIN.Direction = ParameterDirection.Input
        prmIN.OleDbType = OleDbType.Integer
        prmIN.Size = 100
        prmIN.Value = fEmpID

        cmdOKUser.Parameters.Add(prmOUT)
        cmdOKUser.Parameters.Add(prmIN)

        cmdOKUser.ExecuteNonQuery()

        'Function :)
        OK_User = prmOUT.Value

        ConnOra_Close()

    End Function


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class
