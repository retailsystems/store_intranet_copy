Imports System
Imports System.Data
Imports System.Data.OleDb

Namespace Transfer
    Public Class StoreInfo

        Dim strConnString As String

        Public Sub New(ByVal strDSN As String)

            strConnString = strDSN

        End Sub

        Public Function GetStoreInfo(ByVal StoreNumber As String) As String

            Dim strSQL As String
            Dim strReturn As String

            strSQL = "Select * "
            strSQL = strSQL & "From tblStores "
            strSQL = strSQL & "Where Store_Id= '" & StoreNumber & "' "

            Dim objConnection As New OleDbConnection(strConnString)
            objConnection.Open()

            Dim objCommand As New OleDbCommand(strSQL, objConnection)
            Dim objDataReader As OleDbDataReader

            objDataReader = objCommand.ExecuteReader()

            objDataReader.Read()
            strReturn = objDataReader("Store_Id") & " " & objDataReader("Store_Description")

            Return strReturn

        End Function


    End Class

    Public Class Transfers

        Dim strConnString As String

        Public Sub New(ByVal strDSN As String)

            strConnString = strDSN

        End Sub

        Public Function GetExpectedArrivals() As OleDbDataReader

            Dim strSQL As String

            strSQL = "Select '..\ViewTransfer.aspx' as Url, Ship_Date, Transfer_Type, Sent_From, Sent_To, UPS_No, Transfer_No, Status "
            strSQL = strSQL & "From tblTransfers "
            strSQL = strSQL & "Where Sent_To_Id = '0003' "

            Dim objConnection As New OleDbConnection(strConnString)
            objConnection.Open()

            Dim objCommand As New OleDbCommand(strSQL, objConnection)
            Dim objDataReader As OleDbDataReader

            objDataReader = objCommand.ExecuteReader()

            Return objDataReader

        End Function

        Public Function GetArrivals() As OleDbDataReader

            Dim strSQL As String

            strSQL = "Select '..\CheckInProgress.aspx' as Url, Ship_Date, Transfer_Type, Sent_From, Sent_To, UPS_No, Transfer_No, Status "
            strSQL = strSQL & "From tblTransfers "
            strSQL = strSQL & "Where Sent_To_Id = '0003' "

            Dim objConnection As New OleDbConnection(strConnString)
            objConnection.Open()

            Dim objCommand As New OleDbCommand(strSQL, objConnection)
            Dim objDataReader As OleDbDataReader

            objDataReader = objCommand.ExecuteReader()

            Return objDataReader

        End Function

        Public Function GetShippedTo() As OleDbDataReader

            Dim strSQL As String

            strSQL = "Select '..\ViewTransfer.aspx' as Url, Ship_Date, Transfer_Type, Sent_From, Sent_To, UPS_No, Transfer_No, Status "
            strSQL = strSQL & "From tblTransfers "
            strSQL = strSQL & "Where Sent_To_Id = '0003' "

            Dim objConnection As New OleDbConnection(strConnString)
            objConnection.Open()

            Dim objCommand As New OleDbCommand(strSQL, objConnection)
            Dim objDataReader As OleDbDataReader

            objDataReader = objCommand.ExecuteReader()

            Return objDataReader

        End Function

        Public Function GetShippedFrom() As OleDbDataReader

            Dim strSQL As String

            strSQL = "Select '..\ViewTransfer.aspx' as Url, Ship_Date, Transfer_Type, Sent_From, Sent_To, UPS_No, Transfer_No, Status "
            strSQL = strSQL & "From tblTransfers "
            strSQL = strSQL & "Where Sent_From_Id = '0003' "

            Dim objConnection As New OleDbConnection(strConnString)
            objConnection.Open()

            Dim objCommand As New OleDbCommand(strSQL, objConnection)
            Dim objDataReader As OleDbDataReader

            objDataReader = objCommand.ExecuteReader()

            Return objDataReader

        End Function

        Public Function GetRecentlyShipped() As OleDbDataReader

            Dim strSQL As String

            strSQL = "Select '..\ViewTransfer.aspx' as Url, Ship_Date, Transfer_Type, Sent_From, Sent_To, UPS_No, Transfer_No, Status "
            strSQL = strSQL & "From tblTransfers "
            strSQL = strSQL & "Where Sent_From_Id = '0003' "

            Dim objConnection As New OleDbConnection(strConnString)
            objConnection.Open()

            Dim objCommand As New OleDbCommand(strSQL, objConnection)
            Dim objDataReader As OleDbDataReader

            objDataReader = objCommand.ExecuteReader()

            Return objDataReader

        End Function

    End Class

    Public Class BoxItems

        Dim strConnString As String

        Public Sub New(ByVal strDSN As String)

            strConnString = strDSN

        End Sub

        Public Function GetBoxItems() As OleDbDataReader

            Dim strSQL As String

            strSQL = "Select * "
            strSQL = strSQL & "From tblBoxes INNER JOIN "
            strSQL = strSQL & "          tblItems ON tblBoxes.SKU = tblItems.SKU "

            Dim objConnection As New OleDbConnection(strConnString)
            objConnection.Open()

            Dim objCommand As New OleDbCommand(strSQL, objConnection)
            Dim objDataReader As OleDbDataReader

            objDataReader = objCommand.ExecuteReader()

            Return objDataReader

        End Function

    End Class

End Namespace