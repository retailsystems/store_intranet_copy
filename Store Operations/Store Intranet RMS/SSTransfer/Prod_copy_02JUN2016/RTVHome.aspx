<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RTVHome.aspx.vb" Inherits="SSTransfer.StoreLogin"%>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Valid Defectives Maintenance</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript">
			<!--
			javascript:window.history.forward(1);
			//-->
		</script>
	</HEAD>
	<body bgColor="black" leftMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></TD>
				</TR>
				<TR vAlign="top">
					<TD>
						<TABLE id="Table4" height="200" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD style="WIDTH: 17px; HEIGHT: 2px" align="middle"></TD>
								<TD style="HEIGHT: 22px"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 17px; HEIGHT: 2px"></TD>
								<TD style="HEIGHT: 26px"><asp:dropdownlist id="ddlDept" runat="server" Width="216px"></asp:dropdownlist>&nbsp;<asp:button id="btnDispVend" runat="server" Width="152px" Height="24px" Text="Display Dept Vendors" Font-Bold="True" BackColor="#440000" ForeColor="White" BorderColor="#990000"></asp:button>&nbsp;<asp:button id="btnAddDept" runat="server" Width="96px" Height="24px" Text="Add Dept" Font-Bold="True" BackColor="#440000" ForeColor="White" BorderColor="#990000"></asp:button>&nbsp;<asp:button id="btnDelDept" runat="server" Width="113px" Height="24px" Text="Remove Dept" Font-Bold="True" BackColor="#440000" ForeColor="White" BorderColor="#990000"></asp:button></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 17px"></TD>
								<TD><asp:label id="lblAddDept" runat="server" Font-Bold="True" BackColor="Transparent" ForeColor="White" ToolTip=" " Font-Names="Arial" Font-Size="Smaller">Enter Dept Number to Add:</asp:label>&nbsp;&nbsp;
									<asp:textbox id="txtAddDept" runat="server" Width="110px"></asp:textbox>&nbsp;<asp:button id="btnOk" runat="server" Width="88px" Height="24px" Text="Submit" Font-Bold="True" BackColor="#440000" ForeColor="White" BorderColor="#990000"></asp:button>&nbsp;<asp:button id="btnCancel" runat="server" Width="88px" Height="24px" Text="Cancel" Font-Bold="True" BackColor="#440000" ForeColor="White" BorderColor="#990000"></asp:button></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 17px"></TD>
								<TD vAlign="top"><asp:label id="lblVendAction" runat="server" Font-Bold="True" BackColor="Transparent" ForeColor="White" Font-Names="Arial" Font-Size="Smaller">Enter Vendor by Number: </asp:label>&nbsp;&nbsp;<asp:textbox id="txtRemVend" runat="server" Width="71px" MaxLength="5"></asp:textbox>&nbsp;
									<asp:button id="btnRemVend" runat="server" Width="205px" Height="24px" Text="Remove Vendor Association" Font-Bold="True" BackColor="#440000" ForeColor="White" BorderColor="#990000"></asp:button><asp:textbox id="txtAddVend" runat="server" Width="65px" MaxLength="5"></asp:textbox>&nbsp;
									<asp:button id="btnAddVend" runat="server" Width="192px" Height="24px" Text="Add Vendor Association" Font-Bold="True" BackColor="#440000" ForeColor="White" BorderColor="#990000"></asp:button><BR>
									<asp:label id="lblLoginMsg" runat="server" Font-Bold="True" BackColor="Transparent" ForeColor="Red" Font-Names="Arial" Font-Size="Smaller">Generic.</asp:label></TD>
							</TR>
							<TR>
								<td></td>
								<TD><asp:datagrid id="dgVendors" runat="server" Width="539px" Font-Bold="True" BackColor="Transparent" ForeColor="White" BorderColor="Black" Font-Names="Arial" Font-Size="Smaller" CellPadding="0" AllowPaging="True" PageSize="5" BorderStyle="Solid" AutoGenerateColumns="False">
										<AlternatingItemStyle BorderColor="Transparent" VerticalAlign="Top" BackColor="DimGray"></AlternatingItemStyle>
										<HeaderStyle Font-Size="X-Small" Font-Names="arial" Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="VE_CD" HeaderText="Vendor Code"></asp:BoundColumn>
											<asp:BoundColumn DataField="VE_NAME" HeaderText="Vendor Name"></asp:BoundColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 17px"></TD>
								<TD><asp:button id="btnStAddVend" runat="server" Width="146px" Height="24px" Text="Add Vendor to Dept" Font-Bold="True" BackColor="#440000" ForeColor="White" BorderColor="#990000"></asp:button>&nbsp;&nbsp;
									<asp:button id="btnStDelVend" runat="server" Width="185px" Height="24px" Text="Delete Vendor from Dept" Font-Bold="True" BackColor="#440000" ForeColor="White" BorderColor="#990000"></asp:button>&nbsp;&nbsp;
									<asp:button id="btnAddAllVend" Text="Add All Vendors" Font-Bold="True" BackColor="#440000" ForeColor="White" BorderColor="#990000" Runat="server"></asp:button>&nbsp;&nbsp;
									<asp:button id="btnDelAllVend" Font-Bold="True" BackColor="#440000" ForeColor="White" BorderColor="#990000" Runat="server" text="Delete All Vendors"></asp:button></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 17px"></TD>
								<TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
