<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ScanItem_old.aspx.vb" Inherits="SSTransfer.ScanItem_Old"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Scan Item</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" alink="white" vlink="white" link="white">
		<form id="Form1" method="post" runat="server">
			<table borderColor="#990000" cellSpacing="0" borderColorDark="#990000" cellPadding="2" borderColorLight="#990000" border="3" width="350">
				<tr>
					<td align="middle" bgColor="#440000" colSpan="2"><asp:label id="lblHeader" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="medium" Font-Bold="True">Scan Item</asp:label></td>
				</tr>
				<tr>
					<td bgColor="#000000">
						<table cellSpacing="0" cellPadding="3" border="0">
							<tr id="trTransferType_Opt1" runat="server">
								<td noWrap><asp:label id="lblTransferType" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">Transfer Type:</asp:label></td>
								<td noWrap><asp:radiobutton id="radBuyerRTV" runat="server" ForeColor="#ffffff" Font-Size="x-small" Font-Name="Arial" text="Buyer Initiated RTV" groupname="TransferType"></asp:radiobutton></td>
							</tr>
							<tr id="trTransferType_Opt2" runat="server">
								<td>&nbsp;</td>
								<td noWrap><asp:radiobutton id="radStoreRTV" runat="server" ForeColor="#ffffff" Font-Size="x-small" Font-Name="Arial" text="Store Initiated RTV" groupname="TransferType"></asp:radiobutton></td>
							</tr>
							<tr id="trTransferType_Opt3" runat="server">
								<td>&nbsp;</td>
								<td noWrap><asp:radiobutton id="radCTB" runat="server" ForeColor="#ffffff" Font-Size="x-small" Font-Name="Arial" text="CTB" groupname="TransferType"></asp:radiobutton></td>
							</tr>
							<tr id="trTransferType_Opt4" runat="server">
								<td>&nbsp;</td>
								<td noWrap><asp:radiobutton id="radAnalystTransfer" runat="server" ForeColor="#ffffff" Font-Size="x-small" Font-Name="Arial" text="Analyst Requested Store Transfer" groupname="TransferType"></asp:radiobutton></td>
							</tr>
							<tr id="trTransferType_Opt5" runat="server">
								<td>&nbsp;</td>
								<td noWrap><asp:radiobutton id="radCustomerTransfer" runat="server" ForeColor="#ffffff" Font-Size="x-small" Font-Name="Arial" text="Customer Requested Store Transfer" groupname="TransferType"></asp:radiobutton></td>
							</tr>
							<tr id="trRTVReason_1" runat="server">
								<td noWrap><asp:label id="lblRTVReason" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">RTV Reason:</asp:label></td>
								<td noWrap><asp:dropdownlist id="ddlRTVReason" runat="server">
										<asp:ListItem Value="0">Defective</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr id="trRTVReason_2" runat="server">
								<td>&nbsp;</td>
								<td noWrap><asp:label id="lblRTVReasonHint" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small">(Use only for Store Initiated 
										RTV)</asp:label></td>
							</tr>
							<tr>
								<td noWrap><asp:label id="lblScanAllItems" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">Scan All Items?</asp:label></td>
								<td noWrap><asp:radiobutton id="radAllItemYes" runat="server" ForeColor="#ffffff" Font-Size="x-small" Font-Name="Arial" text="Yes" groupname="ScanAll"></asp:radiobutton><asp:radiobutton id="radAllItemNo" runat="server" ForeColor="#ffffff" Font-Size="x-small" Font-Name="Arial" text="No" groupname="ScanAll"></asp:radiobutton></td>
							</tr>
							<tr>
								<td noWrap><asp:label id="lblScanItem" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">Scan Item:</asp:label></td>
								<td noWrap><asp:textbox id="txtScanItem" runat="server"></asp:textbox></td>
							</tr>
							<tr>
								<td align="middle" colspan="2">
									<asp:Button id="btnSubmit" runat="server" Text="Submit" ForeColor="White" BackColor="#440000" BorderColor="#990000" Font-Bold="True"></asp:Button>&nbsp;
									<input style="FONT-WEIGHT:bold;BORDER-LEFT-COLOR:#990000;BORDER-BOTTOM-COLOR:#990000;COLOR:white;BORDER-TOP-COLOR:#990000;BACKGROUND-COLOR:#440000;BORDER-RIGHT-COLOR:#990000" onclick="window.close()" type="button" value="Close">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
