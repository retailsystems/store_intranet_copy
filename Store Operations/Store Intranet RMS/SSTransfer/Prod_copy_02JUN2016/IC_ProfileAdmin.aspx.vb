Public Class IC_ProfileAdmin
    Inherits System.Web.UI.Page
    Protected WithEvents ucHeader As Header
    Private objUserInfo As New UserInfo()
    Protected WithEvents lblAdminCode As System.Web.UI.WebControls.Label
    Protected WithEvents txtAdminCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnAdminCode As System.Web.UI.WebControls.Button
    Protected WithEvents dgProfiles As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Public strAdminCode As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objUserInfo.GetEmployeeInfo()
        If Not objUserInfo.EmpOnline Then
            Response.Redirect("SessionEnd.aspx?Mode=2")
        End If
        'Put user code to initialize the page here
        ucHeader.lblTitle = "Inventory Control Profile Administration"
        ucHeader.CurrentPage(Header.PageName.ProfileAdmin)
        ucHeader.CurrentMode(Header.HeaderGroup.IC)

        Dim TestDelete As String
        'If there is a PID passed, then execute the delete profile sub procedure
        TestDelete = Request.QueryString("PID")
        If Len(TestDelete) > 0 Then
            Call DeleteProfile()
        End If

        strAdminCode = Request.Params("Admin")
        If strAdminCode = "yes" Then
            btnAdminCode.Visible = False
            txtAdminCode.Visible = False
            lblAdminCode.Visible = False
            lblError.Visible = False
            Call FillProfileGrid()
        End If

    End Sub
    Private Sub FillProfileGrid()
        'Fill datagrid with all the profiles
        Dim strPubProfiles
        strPubProfiles = "Select * From IC_SavedProfile Order By Created_By "
        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()

        Dim cmdSearch As New SqlClient.SqlCommand(strPubProfiles, conSearch)
        dtrSearch = cmdSearch.ExecuteReader

        dgProfiles.DataSource = dtrSearch
        dgProfiles.DataBind()
        dtrSearch.Close()
        conSearch.Close()
        cmdSearch.Dispose()
    End Sub
    Private Sub DeleteProfile()
        'If user is valid to do profile deletion, then delete
        Dim ProfileNum As String
        ProfileNum = Request.QueryString("PID")

        Dim conDeleteProfile As System.Data.SqlClient.SqlConnection
        Dim cmdDeleteProfile As System.Data.SqlClient.SqlCommand
        conDeleteProfile = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdDeleteProfile = New SqlClient.SqlCommand("spDeleteProfile", conDeleteProfile)
        cmdDeleteProfile.CommandType = CommandType.StoredProcedure
        cmdDeleteProfile.Parameters.Add("@PID", ProfileNum)
        conDeleteProfile.Open()
        cmdDeleteProfile.ExecuteNonQuery()
        conDeleteProfile.Close()
        cmdDeleteProfile.Dispose()
        Response.Redirect("IC_ProfileAdmin.aspx?Admin=yes")
        'Else if not valid to do deletion, then give error message.

        'End If
    End Sub
    Private Sub btnAdminCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdminCode.Click

        If txtAdminCode.Text = 18305 Then
            'allow deletion of profiles
            btnAdminCode.Visible = False
            txtAdminCode.Visible = False
            lblAdminCode.Visible = False
            lblError.Visible = False
            Call FillProfileGrid()
        Else
            'don't allow deletion of profiles
            btnAdminCode.Visible = True
            txtAdminCode.Visible = True
            lblAdminCode.Visible = True
            lblError.Visible = True
            lblError.Text = "The code you entered was incorrect.  Please re-enter your code."
        End If
    End Sub
    Private Sub txtAdminCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAdminCode.TextChanged

    End Sub
End Class
