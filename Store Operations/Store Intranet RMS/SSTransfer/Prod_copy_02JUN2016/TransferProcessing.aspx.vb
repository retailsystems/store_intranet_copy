Public Class TransferProcessing
    Inherits System.Web.UI.Page

    Protected WithEvents lblNotice As System.Web.UI.WebControls.Label
    Protected WithEvents ddlSetStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSetStore As System.Web.UI.WebControls.Button
    Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private m_TType As Int32 'Return/Store to Store
    Private m_objUserInfo As New UserInfo()
    Private m_objTransferLock As New TransferLock()

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Get querystring info 
        m_TType = Request.QueryString("TType") 'Return or Store to Store
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        m_objUserInfo.GetEmployeeInfo()

        'If store number not set then redirect to set store page
        If m_objUserInfo.StoreNumber = -1 Or m_objUserInfo.StoreNumber = 0 Then
            Response.Redirect("SetStore.aspx")
        End If

        'If employee is logged on to the system then their locks are kept alive
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
            m_objTransferLock.KeepLocksAlive(m_objUserInfo.EmpId)
        Else
            Response.Redirect("SessionEnd.aspx?Mode=1")
        End If

        'Set header properties
        ucHeader.CurrentMode(Header.HeaderGroup.Transfer)
        ucHeader.CurrentPage(Header.PageName.NewTransfer)
        ucHeader.DisableButtons()

        'Redirect afer 1 second
        Response.AddHeader("refresh", "1;URL=PrintTransfer.aspx?TType=" & m_TType & "&BID=" & Request.QueryString("BID"))

    End Sub

End Class
