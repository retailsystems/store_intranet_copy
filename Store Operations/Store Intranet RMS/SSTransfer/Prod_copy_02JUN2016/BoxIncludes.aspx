<%@ Page Language="vb" AutoEventWireup="false" Codebehind="BoxIncludes.aspx.vb" Inherits="SSTransfer_IC.BoxIncludes"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Box Includes</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" onsubmit="return checkSubmit();" method="post" runat="server">
			<table borderColor="#990000" height="100%" cellSpacing="0" borderColorDark="#990000" cellPadding="4" width="100%" bgColor="#000000" borderColorLight="#990000" border="3">
				<tr>
					<td align="middle" bgColor="#440000" colSpan="2" height="1%"><asp:label id="lblHeader" runat="server" Font-Bold="True" Font-Size="medium" ForeColor="White" EnableViewState="False" Font-Names="Arial">Box Includes</asp:label></td>
				</tr>
				<tr>
					<td vAlign="top" align="middle" bgColor="#000000">
						<div style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; OVERFLOW-Y: auto; SCROLLBAR-HIGHLIGHT-COLOR: #990000; BORDER-LEFT: #990000 1px solid; WIDTH: 100%; SCROLLBAR-ARROW-COLOR: #ffffff; BORDER-BOTTOM: #990000 1px solid; SCROLLBAR-BASE-COLOR: #440000; HEIGHT: 130px"><asp:checkboxlist id="cblBoxIncludes" runat="server" Font-Size="X-Small" ForeColor="White" Font-Names="Arial" CellPadding="0" CellSpacing="0" BorderStyle="None"></asp:checkboxlist><asp:label id="lblBoxIncludes" runat="server" Font-Size="X-Small" ForeColor="White" EnableViewState="False" Font-Names="Arial" Visible="False"></asp:label></div>
					</td>
				</tr>
				<tr>
					<td vAlign="top" align="middle" bgColor="#000000"><input style="FONT-WEIGHT: bold; BORDER-LEFT-COLOR: #990000; BORDER-BOTTOM-COLOR: #990000; COLOR: white; BORDER-TOP-COLOR: #990000; FONT-FAMILY: Arial; BACKGROUND-COLOR: #440000; BORDER-RIGHT-COLOR: #990000" onclick="javascript:window.close();" type="button" value="Close">
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
