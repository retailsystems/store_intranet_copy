if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spGetUPSPackageTypes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spGetUPSPackageTypes]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO





CREATE PROCEDURE dbo.spGetUPSPackageTypes
(
	@TType tinyint = null,
	@SendingStore smallint=null,
	@ReceivingStore smallint =null
)
AS
	
	IF(  ISNULL(@TType,0) = 0  and ISNULL(@SendingStore,0) = 0 and ISNULL(@ReceivingStore,0) = 0)
		BEGIN
			SELECT     *
			FROM UPS_Package_Type
			ORDER BY Sort_Order
			
		END
	ELSE

		BEGIN
			SELECT     *
			FROM UPS_Package_Type
			WHERE Code NOT IN
				(SELECT PackageCode
			       FROM UPS_Package_Filter
			       WHERE FilterType = 'E' AND ((TType = @TType AND SendingStore IS NULL AND ReceivingStore IS NULL) OR
						(TType = @TType AND SendingStore = @SendingStore AND ReceivingStore IS NULL) OR
			            (TType = @TType AND SendingStore = @SendingStore AND ReceivingStore = @ReceivingStore) OR
			            (TType = @TType AND SendingStore IS NULL AND ReceivingStore = @ReceivingStore)))
			 
			UNION
			SELECT     *
			FROM UPS_Package_Type
			WHERE Code IN
				(SELECT PackageCode
			       FROM UPS_Package_Filter
			       WHERE FilterType = 'I' AND ((TType = @TType AND SendingStore IS NULL AND ReceivingStore IS NULL) OR
						(TType = @TType AND SendingStore = @SendingStore AND ReceivingStore IS NULL) OR
			            (TType = @TType AND SendingStore = @SendingStore AND ReceivingStore = @ReceivingStore) OR
			            (TType = @TType AND SendingStore IS NULL AND ReceivingStore = @ReceivingStore)))
			ORDER BY Sort_Order	
		END	
			
	RETURN

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

