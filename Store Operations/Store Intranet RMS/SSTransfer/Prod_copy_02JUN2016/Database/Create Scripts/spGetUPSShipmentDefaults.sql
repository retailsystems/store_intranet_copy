if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spGetUPSShipmentDefaults]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spGetUPSShipmentDefaults]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


CREATE PROCEDURE dbo.spGetUPSShipmentDefaults
(
	@TType tinyint,
	@SendingStore smallint,
	@ReceivingStore smallint
)
AS
	SELECT TOP 1 PackageCode, ServiceCode FROM (
	SELECT PackageCode, ServiceCode, SendingStore, ReceivingStore From UPS_Shipment_Default Where TType = @TType and SendingStore = @SendingStore and ReceivingStore = @ReceivingStore
	UNION
	SELECT PackageCode, ServiceCode, SendingStore, ReceivingStore From UPS_Shipment_Default Where TType = @TType and SendingStore = @SendingStore and ReceivingStore is null  
	UNION
	SELECT PackageCode, ServiceCode, SendingStore, ReceivingStore From UPS_Shipment_Default Where TType = @TType and SendingStore is null and ReceivingStore = @ReceivingStore 
	UNION		
	SELECT PackageCode, ServiceCode, SendingStore, ReceivingStore From UPS_Shipment_Default Where TType = @TType and SendingStore is null and ReceivingStore is null
	) SD
	ORDER BY SendingStore DESC, ReceivingStore DESC
	RETURN

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

