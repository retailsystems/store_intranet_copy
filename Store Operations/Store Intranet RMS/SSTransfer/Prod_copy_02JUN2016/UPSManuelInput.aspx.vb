Public Class UPSManuelInput
    Inherits System.Web.UI.Page
    Protected WithEvents lblInstruction As System.Web.UI.WebControls.Label
    Protected WithEvents lblTrackNum As System.Web.UI.WebControls.Label
    Protected WithEvents btnAssign As System.Web.UI.WebControls.Button
    Protected WithEvents ucHeader As Header
    Protected WithEvents btnClear As System.Web.UI.WebControls.Button
    Protected WithEvents txtTrackNum As System.Web.UI.WebControls.TextBox
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl

    Private objUserInfo As New UserInfo()
    Private m_BoxId As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()

        m_BoxId = Request("BID")

        ucHeader.lblTitle = "Shipping Label"
        ucHeader.CurrentMode(Header.HeaderGroup.Transfer)
        ucHeader.CurrentPage(Header.PageName.NewTransfer)

        If txtTrackNum.Text.Length > 9 Then
            lblTrackNum.Text = txtTrackNum.Text
            txtTrackNum.Visible = False
            btnClear.Visible = True
            btnAssign.Visible = True
        Else
            txtTrackNum.Text = ""
        End If

    End Sub

    Private Sub btnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssign.Click

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spInsertBoxTrackingInfo", objConnection)

        objCommand.CommandType = CommandType.StoredProcedure

        Dim prmBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@BoxId", SqlDbType.VarChar, 14)
        Dim prmParcelService As SqlClient.SqlParameter = objCommand.Parameters.Add("@ParcelService", SqlDbType.SmallInt)
        Dim prmShipmentId As SqlClient.SqlParameter = objCommand.Parameters.Add("@ShipmentId", SqlDbType.VarChar, 18)
        Dim prmTrackingNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@TrackingNum", SqlDbType.VarChar, 30)
        Dim prmXMLShipping As SqlClient.SqlParameter = objCommand.Parameters.Add("@XMLShipping", SqlDbType.Bit)
        'Dim prmBoxStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@BoxStatus", SqlDbType.TinyInt)
        Dim prmCreated As SqlClient.SqlParameter = objCommand.Parameters.Add("@Created", SqlDbType.DateTime)
        Dim prmModified As SqlClient.SqlParameter = objCommand.Parameters.Add("@Modified", SqlDbType.DateTime)
        Dim prmTrackingInfoId As SqlClient.SqlParameter = objCommand.Parameters.Add("@TrackingInfo_Id", SqlDbType.Int)

        prmBoxId.Direction = ParameterDirection.Input
        prmParcelService.Direction = ParameterDirection.Input
        prmShipmentId.Direction = ParameterDirection.Input
        prmTrackingNum.Direction = ParameterDirection.Input
        prmXMLShipping.Direction = ParameterDirection.Input
        'prmBoxStatus.Direction = ParameterDirection.Input
        prmCreated.Direction = ParameterDirection.Input
        prmModified.Direction = ParameterDirection.Input
        prmTrackingInfoId.Direction = ParameterDirection.Output

        prmBoxId.Value = m_BoxId
        prmTrackingNum.Value = lblTrackNum.Text
        prmXMLShipping.Value = 0
        'prmBoxStatus.Value = 2 'Shipped
        prmParcelService.Value = 1 'UPS
        prmShipmentId.Value = DBNull.Value

        Dim strDate = Now()

        prmCreated.Value = strDate
        prmModified.Value = strDate

        objCommand.ExecuteNonQuery()

        objConnection.Close()

        If prmTrackingInfoId.Value = -1 Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Tracking Number Has Already Been Used.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
        Else
            Response.Redirect("TransferProcessing.aspx?BID=" & m_BoxId)
            'Response.Redirect("CreateNewTrans.aspx")
        End If

    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        btnClear.Visible = False
        btnAssign.Visible = False
        lblTrackNum.Text = ""
        txtTrackNum.Text = ""
        txtTrackNum.Visible = True
    End Sub

End Class
