Public Class UPSVoid
    Inherits System.Web.UI.Page
    Protected WithEvents dgUPSVoid As System.Web.UI.WebControls.DataGrid

    Private m_strShipmentId As String
    Private m_intTrackingInfoId As Int32
    Private m_strBoxId As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Len(Request("TID")) > 0 Then
            m_strShipmentId = Request("SID") 'UPS XML Shipping Id
            m_intTrackingInfoId = Request("TID") ' UPS Tracking Number
            m_strBoxId = Request("BID")
            VoidShippingLabel()
        End If

        FillDatagrid()

    End Sub

    Private Sub FillDatagrid()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        Dim objCommand As New SqlClient.SqlCommand("spGetUPSVoid", objConnection)

        objConnection.Open()

        objDataReader = objCommand.ExecuteReader()

        dgUPSVoid.DataSource = objDataReader
        dgUPSVoid.DataBind()

        objCommand.Dispose()
        objDataReader.Close()
        objConnection.Close()

    End Sub

    'Sends a xml void request to UPS
    Private Function VoidShippingLabel()

        Dim objUPS As New UPS_XML_Shipping()

        objUPS.ShipmentId = m_strShipmentId 'XML UPS Shipment Id
        objUPS.TrackingInfoId = m_intTrackingInfoId
        objUPS.BoxId = m_strBoxId

        'Send XML Void Request
        objUPS.SendVoidShipping()

    End Function

End Class
