Public Class ViewXML
    Inherits System.Web.UI.Page
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Dim strSQL As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        strSQL = "SELECT * FROM UPS_XML_Errors WHERE UPS_XMLError_Id = " & Request("EID")

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strSQL, objConnection)

        objDataReader = objCommand.ExecuteReader()

        If objDataReader.Read() Then
            If Request("Type") = 1 Then
                TextBox1.Text = objDataReader("XMLResponse")
            Else
                TextBox1.Text = objDataReader("XMLRequest")
            End If
        End If

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()
        objDataReader.Close()

    End Sub

End Class
