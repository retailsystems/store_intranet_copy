Public Class Transfer1
    Inherits System.Web.UI.Page

    Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents grdItemTypeConfig As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnAddItemType As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteItemType As System.Web.UI.WebControls.Button
    Protected WithEvents dgRouteConfig As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnAddTransferRoute As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteTransferRoute As System.Web.UI.WebControls.Button
    Protected WithEvents dgBoxIncludesConfig As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnAddBoxIncludeType As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteBoxIncludeType As System.Web.UI.WebControls.Button
    Protected WithEvents btnAddDefaultStore As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteDefaultStore As System.Web.UI.WebControls.Button
    Protected WithEvents dgDefaultStore As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnAddBoxInclude As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteBoxInclude As System.Web.UI.WebControls.Button

    Private m_objUserInfo As New UserInfo()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strLegend As String
        Dim objSecurity As New Security(ConfigurationSettings.AppSettings("strSecurityConn"), ConfigurationSettings.AppSettings("ProjectId"))

        m_objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        'If employee is logged on update last Trans
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
        End If

        If Not objSecurity.ModuleAuthorized(m_objUserInfo.EmpId, "TRANSFER", "TRANSFER.ASPX") Then Response.Redirect("Unauthorized.aspx")

        'Sets both header title and highlights section button
        ucHeader.lblTitle = "SSTransfer Admin"
        ucHeader.CurrentMode(Header.HeaderGroup.Admin)
        ucHeader.CurrentPage(Header.PageName.Transfer)

        GetItemTypeConfig()
        GetRouteConfig()
        GetBoxIncludeConfig()
        GetDefaultReceivingStoreConfig()

    End Sub

    Private Sub GetItemTypeConfig()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetItemTypeConfig"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        grdItemTypeConfig.DataSource = objDataReader
        grdItemTypeConfig.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub GetRouteConfig()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetRouteConfig"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        dgRouteConfig.DataSource = objDataReader
        dgRouteConfig.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub GetBoxIncludeConfig()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetBoxIncludeConfig"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        dgBoxIncludesConfig.DataSource = objDataReader
        dgBoxIncludesConfig.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub GetDefaultReceivingStoreConfig()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetDefaultReceivingStoreConfig"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        dgDefaultStore.DataSource = objDataReader
        dgDefaultStore.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub btnDeleteItemType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteItemType.Click

        Dim I As Integer
        Dim dgi As DataGridItem
        Dim chkDeleteItemTypeConfig As CheckBox
        Dim strTemp As String
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        For I = 0 To grdItemTypeConfig.Items.Count - 1
            dgi = grdItemTypeConfig.Items(I)
            chkDeleteItemTypeConfig = CType(dgi.FindControl("chkDeleteItemTypeConfig"), CheckBox)

            If chkDeleteItemTypeConfig.Checked Then
                strTemp += dgi.Cells(0).Text & ","
            End If

        Next

        If Len(strTemp) > 0 Then
            strTemp = Mid(strTemp, 1, strTemp.Length - 1)

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spDeleteItemTypeConfig '" & strTemp & "' "

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            GetItemTypeConfig()

        End If

    End Sub

    Private Sub btnDeleteBoxInclude_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteBoxInclude.Click

        Dim I As Integer
        Dim dgi As DataGridItem
        Dim chkDeleteBoxIncludeConfig As CheckBox
        Dim strTemp As String
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        For I = 0 To dgBoxIncludesConfig.Items.Count - 1
            dgi = dgBoxIncludesConfig.Items(I)
            chkDeleteBoxIncludeConfig = CType(dgi.FindControl("chkDeleteBoxIncludeConfig"), CheckBox)

            If chkDeleteBoxIncludeConfig.Checked Then
                strTemp += dgi.Cells(0).Text & ","
            End If

        Next

        If Len(strTemp) > 0 Then
            strTemp = Mid(strTemp, 1, strTemp.Length - 1)

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spDeleteBoxIncludeConfig '" & strTemp & "' "

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            GetBoxIncludeConfig()

        End If

    End Sub

    Private Sub btnDeleteTransferRoute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteTransferRoute.Click

        Dim I As Integer
        Dim dgi As DataGridItem
        Dim chkDeleteRouteConfig As CheckBox
        Dim strTemp As String
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        For I = 0 To dgRouteConfig.Items.Count - 1
            dgi = dgRouteConfig.Items(I)
            chkDeleteRouteConfig = CType(dgi.FindControl("chkDeleteRouteConfig"), CheckBox)

            If chkDeleteRouteConfig.Checked Then
                strTemp += dgi.Cells(0).Text & ","
            End If

        Next

        If Len(strTemp) > 0 Then
            strTemp = Mid(strTemp, 1, strTemp.Length - 1)

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spDeleteRouteConfig '" & strTemp & "' "

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            GetRouteConfig()

        End If

    End Sub

    Private Sub btnDeleteDefaultStore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteDefaultStore.Click

        Dim I As Integer
        Dim dgi As DataGridItem
        Dim chkDeleteDefaultStore As CheckBox
        Dim strTemp As String
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        For I = 0 To dgDefaultStore.Items.Count - 1
            dgi = dgDefaultStore.Items(I)
            chkDeleteDefaultStore = CType(dgi.FindControl("chkDeleteDefaultStore"), CheckBox)

            If chkDeleteDefaultStore.Checked Then
                strTemp += dgi.Cells(0).Text & ","
            End If

        Next

        If Len(strTemp) > 0 Then
            strTemp = Mid(strTemp, 1, strTemp.Length - 1)

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spDeleteDefaultReceivingStore '" & strTemp & "' "

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            GetDefaultReceivingStoreConfig()

        End If

    End Sub

End Class
