Public Class AddGroup
    Inherits System.Web.UI.Page
    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
    Protected WithEvents lblGroupDescription As System.Web.UI.WebControls.Label
    Protected WithEvents txtGroupDescription As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblStores As System.Web.UI.WebControls.Label
    Protected WithEvents lbStores As System.Web.UI.WebControls.ListBox
    Protected WithEvents btnAddGroup As System.Web.UI.WebControls.Button

    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        If Not Page.IsPostBack Then
            FillStores()
        End If

    End Sub

    Private Sub FillStores()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open() 'open connection

        Dim objCommand As New SqlClient.SqlCommand("asFillTStore", objConnection)

        objDataReader = objCommand.ExecuteReader() 'fill data reader

        'Bind data to dropdown list
        lbStores.DataSource = objDataReader
        lbStores.DataBind()

        'close connections
        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub btnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddGroup.Click

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spInsertGroup '" & Replace(txtGroupDescription.Text, "'", "''") & "','" & GetSelectedItems() & "'"

        objCommand.CommandText = strStoredProcedure

        objCommand.ExecuteNonQuery()
        objCommand.Dispose()

        pageBody.Attributes.Add("onload", "window.opener.location=window.opener.location;")

    End Sub

    Private Function GetSelectedItems() As String

        Dim I As Integer
        Dim strTemp As String

        For I = 0 To lbStores.Items.Count - 1
            If lbStores.Items(I).Selected Then
                strTemp = strTemp & lbStores.Items(I).Value & ","
            End If
        Next

        If Len(strTemp) > 0 Then
            Return Mid(strTemp, 1, Len(strTemp) - 1)
        Else
            Return strTemp
        End If

    End Function

End Class
