Public Class Weights
    Inherits System.Web.UI.Page

    Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnUpdate As System.Web.UI.WebControls.Button
    Protected WithEvents grdBoxIncludeWeight As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnAddMerchandiseWeight As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteMerchandiseWeight As System.Web.UI.WebControls.Button
    Protected WithEvents dgMerchandiseWeights As System.Web.UI.WebControls.DataGrid

    Private m_objUserInfo As New UserInfo()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strLegend As String
        Dim objSecurity As New Security(ConfigurationSettings.AppSettings("strSecurityConn"), ConfigurationSettings.AppSettings("ProjectId"))

        m_objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        'If employee is logged on update last Trans
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
        End If

        If Not objSecurity.ModuleAuthorized(m_objUserInfo.EmpId, "WEIGHTS", "WEIGHTS.ASPX") Then Response.Redirect("Unauthorized.aspx")

        'Sets both header title and highlights section button
        ucHeader.lblTitle = "SSTransfer Admin"
        ucHeader.CurrentMode(Header.HeaderGroup.Admin)
        ucHeader.CurrentPage(Header.PageName.Weights)

        GetBoxIncludeWeights()
        GetMerchandiseWeights()

    End Sub

    Private Sub GetBoxIncludeWeights()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetBoxIncludeWeights"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        grdBoxIncludeWeight.DataSource = objDataReader
        grdBoxIncludeWeight.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub GetMerchandiseWeights()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetMerchandiseWeights"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        dgMerchandiseWeights.DataSource = objDataReader
        dgMerchandiseWeights.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub btnDeleteMerchandiseWeight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteMerchandiseWeight.Click

        Dim I As Integer
        Dim dgi As DataGridItem
        Dim chkDeleteWeight As CheckBox
        Dim strTemp As String
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        For I = 0 To dgMerchandiseWeights.Items.Count - 1
            dgi = dgMerchandiseWeights.Items(I)
            chkDeleteWeight = CType(dgi.FindControl("chkDeleteWeight"), CheckBox)

            If chkDeleteWeight.Checked Then
                strTemp += dgi.Cells(0).Text & ","
            End If

        Next

        If Len(strTemp) > 0 Then
            strTemp = Mid(strTemp, 1, strTemp.Length - 1)

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spDeleteWeights '" & strTemp & "' "

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            GetMerchandiseWeights()

        End If

    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        Dim I As Integer
        Dim dgi As DataGridItem
        Dim txtWeight As TextBox
        Dim strTemp As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        objConnection.Open()
        Dim objCommand As New SqlClient.SqlCommand()
        objCommand.Connection = objConnection

        For I = 0 To grdBoxIncludeWeight.Items.Count - 1

            dgi = grdBoxIncludeWeight.Items(I)
            txtWeight = CType(dgi.FindControl("txtWeight"), TextBox)

            If IsNumeric(txtWeight.Text) Then
                strTemp = "spUpdateBoxIncludeWeight " & dgi.Cells(0).Text & "," & txtWeight.Text

                objCommand.CommandText = strTemp
                objCommand.ExecuteNonQuery()

            End If

        Next

        objCommand.Dispose()
        objConnection.Close()
        GetBoxIncludeWeights()

    End Sub

End Class
