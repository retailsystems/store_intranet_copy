Public Class UPS
    Inherits System.Web.UI.Page

    Protected WithEvents ucHeader As Header
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnAddDefualtShipment As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteDefualtShipment As System.Web.UI.WebControls.Button
    Protected WithEvents grdUPSUserAccounts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgUPSDefaultShipment As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgUPSPackage As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnAddPackageConfig As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeletePackageConfig As System.Web.UI.WebControls.Button
    Protected WithEvents grdUPSService As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnAddServiceConfig As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteServiceConfig As System.Web.UI.WebControls.Button
    Protected WithEvents btnDeleteUPSAccount As System.Web.UI.WebControls.Button

    Private m_objUserInfo As New UserInfo()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strLegend As String
        Dim objSecurity As New Security(ConfigurationSettings.AppSettings("strSecurityConn"), ConfigurationSettings.AppSettings("ProjectId"))

        m_objUserInfo.GetEmployeeInfo()
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        'If employee is logged on update last Trans
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
        End If

        If Not objSecurity.ModuleAuthorized(m_objUserInfo.EmpId, "UPS", "UPS.ASPX") Then Response.Redirect("Unauthorized.aspx")


        'Sets both header title and highlights section button
        ucHeader.lblTitle = "SSTransfer Admin"
        ucHeader.CurrentMode(Header.HeaderGroup.Admin)
        ucHeader.CurrentPage(Header.PageName.UPS)


        GetUPSUserAccounts()
        GetUPSPackage()
        GetUPSService()
        GetUPSDefaultShipment()


    End Sub

    Private Sub GetUPSUserAccounts()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetUPSUserAccounts"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        grdUPSUserAccounts.DataSource = objDataReader
        grdUPSUserAccounts.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub GetUPSDefaultShipment()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetUPSDefaultShipment"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        dgUPSDefaultShipment.DataSource = objDataReader
        dgUPSDefaultShipment.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub GetUPSPackage()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetUPSPackageConfig"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        dgUPSPackage.DataSource = objDataReader
        dgUPSPackage.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub GetUPSService()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        strStoredProcedure = "spGetUPSServiceConfig"

        objCommand.CommandText = strStoredProcedure

        objDataReader = objCommand.ExecuteReader()

        grdUPSService.DataSource = objDataReader
        grdUPSService.DataBind()
        objCommand.Dispose()
        objDataReader.Close()

    End Sub

    Private Sub btnDeleteUPSAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteUPSAccount.Click

        Dim I As Integer
        Dim dgi As DataGridItem
        Dim chkDeleteUPSuserAccount As CheckBox
        Dim strTemp As String
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        For I = 0 To grdUPSUserAccounts.Items.Count - 1
            dgi = grdUPSUserAccounts.Items(I)
            chkDeleteUPSuserAccount = CType(dgi.FindControl("chkDeleteUPSuserAccount"), CheckBox)

            If chkDeleteUPSuserAccount.Checked Then
                strTemp += dgi.Cells(0).Text & ","
            End If

        Next

        If Len(strTemp) > 0 Then
            strTemp = Mid(strTemp, 1, strTemp.Length - 1)

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spDeleteUPSUserAccount '" & strTemp & "' "

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            GetUPSUserAccounts()

        End If

    End Sub

    Private Sub btnDeleteDefualtShipment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteDefualtShipment.Click

        Dim I As Integer
        Dim dgi As DataGridItem
        Dim chkDeleteUPSDefaultShipment As CheckBox
        Dim strTemp As String
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        For I = 0 To dgUPSDefaultShipment.Items.Count - 1
            dgi = dgUPSDefaultShipment.Items(I)
            chkDeleteUPSDefaultShipment = CType(dgi.FindControl("chkDeleteUPSDefaultShipment"), CheckBox)

            If chkDeleteUPSDefaultShipment.Checked Then
                strTemp += dgi.Cells(0).Text & ","
            End If

        Next

        If Len(strTemp) > 0 Then
            strTemp = Mid(strTemp, 1, strTemp.Length - 1)

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spDeleteUPSDefaultShipment '" & strTemp & "' "

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            GetUPSDefaultShipment()

        End If


    End Sub

    Private Sub btnDeletePackageConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeletePackageConfig.Click

        Dim I As Integer
        Dim dgi As DataGridItem
        Dim chkDeleteUPSPackage As CheckBox
        Dim strTemp As String
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        For I = 0 To dgUPSPackage.Items.Count - 1
            dgi = dgUPSPackage.Items(I)
            chkDeleteUPSPackage = CType(dgi.FindControl("chkDeleteUPSPackage"), CheckBox)

            If chkDeleteUPSPackage.Checked Then
                strTemp += dgi.Cells(0).Text & ","
            End If

        Next

        If Len(strTemp) > 0 Then
            strTemp = Mid(strTemp, 1, strTemp.Length - 1)

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spDeleteUPSPackageFilter '" & strTemp & "' "

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            GetUPSPackage()

        End If

    End Sub

    Private Sub btnDeleteServiceConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteServiceConfig.Click

        Dim I As Integer
        Dim dgi As DataGridItem
        Dim chkDeleteUPSService As CheckBox
        Dim strTemp As String
        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        For I = 0 To grdUPSService.Items.Count - 1
            dgi = grdUPSService.Items(I)
            chkDeleteUPSService = CType(dgi.FindControl("chkDeleteUPSService"), CheckBox)

            If chkDeleteUPSService.Checked Then
                strTemp += dgi.Cells(0).Text & ","
            End If

        Next

        If Len(strTemp) > 0 Then
            strTemp = Mid(strTemp, 1, strTemp.Length - 1)

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            strStoredProcedure = "spDeleteUPSServiceFilter '" & strTemp & "' "

            objCommand.CommandText = strStoredProcedure

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            GetUPSService()

        End If

    End Sub

End Class
