Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class StoreToStore
Inherits ActiveReport

    Private m_objDataSet As New DataSet()
    Private m_BoxId As String
    Private m_RowCount As Int32
    Private m_ONWAN As Boolean
    Private m_TopPic As PicType

    Public Enum PicType
        Hottopic = 1
        Torrid = 2
    End Enum

    Public Sub New(ByVal BoxId As String, ByVal ONWAN As Boolean, ByVal TopPic As PicType)
        MyBase.New()
        m_BoxId = BoxId
        m_TopPic = TopPic
        m_ONWAN = ONWAN
        InitializeReport()
    End Sub

#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As ReportHeader = Nothing
    Private WithEvents Detail As Detail = Nothing
    Private WithEvents ReportFooter As ReportFooter = Nothing
	Private srSending As SubReport = Nothing
	Private srReceiving As SubReport = Nothing
	Private srIC As SubReport = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "SSTransfer.StoreToStore.rpx")
		Me.ReportHeader = CType(Me.Sections("ReportHeader"),DataDynamics.ActiveReports.ReportHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.ReportFooter = CType(Me.Sections("ReportFooter"),DataDynamics.ActiveReports.ReportFooter)
		Me.srSending = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.SubReport)
		Me.srReceiving = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.SubReport)
		Me.srIC = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.SubReport)
	End Sub

#End Region

    Private Sub Returns_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Me.srReceiving.Report = New Full_Manifest(m_BoxId, "Receiving Store", m_TopPic)
        Me.srSending.Report = New Full_Manifest(m_BoxId, "Sending Store", m_TopPic)

        If Not m_ONWAN Then
            Me.srIC.Report = New Full_Manifest(m_BoxId, "IC", m_TopPic)
            Me.srIC.Visible = True
        End If

    End Sub

End Class
