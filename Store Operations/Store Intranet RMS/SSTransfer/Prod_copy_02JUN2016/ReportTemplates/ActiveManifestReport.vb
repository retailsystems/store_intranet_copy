Imports System
Imports System.Data
'Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class ActiveManifestReport
    Inherits ActiveReport


    Private m_objDataSet As New DataSet()
    Private m_BoxId As String
    Private m_RowCount As Int32

    Public Sub New(ByVal BoxId As String)
        MyBase.New()
        m_BoxId = BoxId
        InitializeReport()
    End Sub

#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As PageHeader = Nothing
    Private WithEvents Detail As Detail = Nothing
    Private WithEvents PageFooter As PageFooter = Nothing
	Private Shape11 As Shape = Nothing
	Private Shape10 As Shape = Nothing
	Private Shape16 As Shape = Nothing
	Private Shape1 As Shape = Nothing
	Private Shape15 As Shape = Nothing
	Private txtFromStoreName As TextBox = Nothing
	Private txtToStoreNum As TextBox = Nothing
	Private txtToStoreName As TextBox = Nothing
	Private lblVendor As Label = Nothing
	Private lblDept As Label = Nothing
	Private lblSKU As Label = Nothing
	Private lblTransferType As Label = Nothing
	Private lblCust As Label = Nothing
	Private lblQuantShipped As Label = Nothing
	Private lblQuantReq As Label = Nothing
	Private lblDescription As Label = Nothing
	Private lblRetail As Label = Nothing
	Private lblSize As Label = Nothing
	Private lblStyle As Label = Nothing
	Private Shape3 As Shape = Nothing
	Private lblTransfer As Label = Nothing
	Private lblFromStoreTop As Label = Nothing
	Private Picture1 As Picture = Nothing
	Private Shape4 As Shape = Nothing
	Private Shape5 As Shape = Nothing
	Private Shape6 As Shape = Nothing
	Private Shape7 As Shape = Nothing
	Private Shape8 As Shape = Nothing
	Private Shape9 As Shape = Nothing
	Private Shape12 As Shape = Nothing
	Private Shape13 As Shape = Nothing
	Private lblFrom As Label = Nothing
	Private lblFromStore As Label = Nothing
	Private lblStoreName As Label = Nothing
	Private lblTo As Label = Nothing
	Private lblToStoreNum As Label = Nothing
	Private lblToStoreName As Label = Nothing
	Private Line2 As Line = Nothing
	Private Line1 As Line = Nothing
	Private txtStoreNumTop As TextBox = Nothing
	Private txtTransfer As TextBox = Nothing
	Private lblManifest As Label = Nothing
	Private Shape14 As Shape = Nothing
	Private txtFromStoreNum As TextBox = Nothing
	Private txtCopy As TextBox = Nothing
	Private Label1 As Label = Nothing
	Private txtTrackingNum As TextBox = Nothing
	Private lblShipdate As Label = Nothing
	Private txtShipmentDate As TextBox = Nothing
	Private lblCarrier As Label = Nothing
	Private txtCarrier As TextBox = Nothing
	Private txtCustName As TextBox = Nothing
	Private Shape26 As Shape = Nothing
	Private txtPhone As TextBox = Nothing
	Private Shape27 As Shape = Nothing
	Private Shape25 As Shape = Nothing
	Private Shape24 As Shape = Nothing
	Private Shape17 As Shape = Nothing
	Private Shape18 As Shape = Nothing
	Private Shape19 As Shape = Nothing
	Private Shape20 As Shape = Nothing
	Private Shape21 As Shape = Nothing
	Private Shape22 As Shape = Nothing
	Private Shape23 As Shape = Nothing
	Private txtTT As TextBox = Nothing
	Private txtQS As TextBox = Nothing
	Private txtDesc As TextBox = Nothing
	Private txtRetail As TextBox = Nothing
	Private txtSize As TextBox = Nothing
	Private txtItem As TextBox = Nothing
	Private txtVendor As TextBox = Nothing
	Private txtDept As TextBox = Nothing
	Private txtSKU As TextBox = Nothing
	Private lblOfficeOnly As Label = Nothing
	Private Shape28 As Shape = Nothing
	Private lblOf As Label = Nothing
	Private lblPage As Label = Nothing
	Private lblSendingInstructions As Label = Nothing
	Private txtPageTotal As TextBox = Nothing
	Private TextBox1 As TextBox = Nothing
	Private lblBoxIncludes As Label = Nothing
	Private txtBoxIncludes As TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "SSTransfer.ActiveManifestReport.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Shape11 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.Shape10 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Shape)
		Me.Shape16 = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Shape)
		Me.Shape1 = CType(Me.PageHeader.Controls(3),DataDynamics.ActiveReports.Shape)
		Me.Shape15 = CType(Me.PageHeader.Controls(4),DataDynamics.ActiveReports.Shape)
		Me.txtFromStoreName = CType(Me.PageHeader.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtToStoreNum = CType(Me.PageHeader.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtToStoreName = CType(Me.PageHeader.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.lblVendor = CType(Me.PageHeader.Controls(8),DataDynamics.ActiveReports.Label)
		Me.lblDept = CType(Me.PageHeader.Controls(9),DataDynamics.ActiveReports.Label)
		Me.lblSKU = CType(Me.PageHeader.Controls(10),DataDynamics.ActiveReports.Label)
		Me.lblTransferType = CType(Me.PageHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblCust = CType(Me.PageHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblQuantShipped = CType(Me.PageHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblQuantReq = CType(Me.PageHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblDescription = CType(Me.PageHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblRetail = CType(Me.PageHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblSize = CType(Me.PageHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.lblStyle = CType(Me.PageHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.Shape3 = CType(Me.PageHeader.Controls(19),DataDynamics.ActiveReports.Shape)
		Me.lblTransfer = CType(Me.PageHeader.Controls(20),DataDynamics.ActiveReports.Label)
		Me.lblFromStoreTop = CType(Me.PageHeader.Controls(21),DataDynamics.ActiveReports.Label)
		Me.Picture1 = CType(Me.PageHeader.Controls(22),DataDynamics.ActiveReports.Picture)
		Me.Shape4 = CType(Me.PageHeader.Controls(23),DataDynamics.ActiveReports.Shape)
		Me.Shape5 = CType(Me.PageHeader.Controls(24),DataDynamics.ActiveReports.Shape)
		Me.Shape6 = CType(Me.PageHeader.Controls(25),DataDynamics.ActiveReports.Shape)
		Me.Shape7 = CType(Me.PageHeader.Controls(26),DataDynamics.ActiveReports.Shape)
		Me.Shape8 = CType(Me.PageHeader.Controls(27),DataDynamics.ActiveReports.Shape)
		Me.Shape9 = CType(Me.PageHeader.Controls(28),DataDynamics.ActiveReports.Shape)
		Me.Shape12 = CType(Me.PageHeader.Controls(29),DataDynamics.ActiveReports.Shape)
		Me.Shape13 = CType(Me.PageHeader.Controls(30),DataDynamics.ActiveReports.Shape)
		Me.lblFrom = CType(Me.PageHeader.Controls(31),DataDynamics.ActiveReports.Label)
		Me.lblFromStore = CType(Me.PageHeader.Controls(32),DataDynamics.ActiveReports.Label)
		Me.lblStoreName = CType(Me.PageHeader.Controls(33),DataDynamics.ActiveReports.Label)
		Me.lblTo = CType(Me.PageHeader.Controls(34),DataDynamics.ActiveReports.Label)
		Me.lblToStoreNum = CType(Me.PageHeader.Controls(35),DataDynamics.ActiveReports.Label)
		Me.lblToStoreName = CType(Me.PageHeader.Controls(36),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.PageHeader.Controls(37),DataDynamics.ActiveReports.Line)
		Me.Line1 = CType(Me.PageHeader.Controls(38),DataDynamics.ActiveReports.Line)
		Me.txtStoreNumTop = CType(Me.PageHeader.Controls(39),DataDynamics.ActiveReports.TextBox)
		Me.txtTransfer = CType(Me.PageHeader.Controls(40),DataDynamics.ActiveReports.TextBox)
		Me.lblManifest = CType(Me.PageHeader.Controls(41),DataDynamics.ActiveReports.Label)
		Me.Shape14 = CType(Me.PageHeader.Controls(42),DataDynamics.ActiveReports.Shape)
		Me.txtFromStoreNum = CType(Me.PageHeader.Controls(43),DataDynamics.ActiveReports.TextBox)
		Me.txtCopy = CType(Me.PageHeader.Controls(44),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.PageHeader.Controls(45),DataDynamics.ActiveReports.Label)
		Me.txtTrackingNum = CType(Me.PageHeader.Controls(46),DataDynamics.ActiveReports.TextBox)
		Me.lblShipdate = CType(Me.PageHeader.Controls(47),DataDynamics.ActiveReports.Label)
		Me.txtShipmentDate = CType(Me.PageHeader.Controls(48),DataDynamics.ActiveReports.TextBox)
		Me.lblCarrier = CType(Me.PageHeader.Controls(49),DataDynamics.ActiveReports.Label)
		Me.txtCarrier = CType(Me.PageHeader.Controls(50),DataDynamics.ActiveReports.TextBox)
		Me.txtCustName = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.TextBox)
		Me.Shape26 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.Shape)
		Me.txtPhone = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.Shape27 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.Shape)
		Me.Shape25 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Shape)
		Me.Shape24 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.Shape)
		Me.Shape17 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.Shape18 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.Shape)
		Me.Shape19 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Shape)
		Me.Shape20 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.Shape)
		Me.Shape21 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.Shape)
		Me.Shape22 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.Shape)
		Me.Shape23 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.Shape)
		Me.txtTT = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.TextBox)
		Me.txtQS = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.TextBox)
		Me.txtDesc = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.txtRetail = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.txtSize = CType(Me.Detail.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.txtItem = CType(Me.Detail.Controls(18),DataDynamics.ActiveReports.TextBox)
		Me.txtVendor = CType(Me.Detail.Controls(19),DataDynamics.ActiveReports.TextBox)
		Me.txtDept = CType(Me.Detail.Controls(20),DataDynamics.ActiveReports.TextBox)
		Me.txtSKU = CType(Me.Detail.Controls(21),DataDynamics.ActiveReports.TextBox)
		Me.lblOfficeOnly = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Shape28 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Shape)
		Me.lblOf = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblPage = CType(Me.PageFooter.Controls(3),DataDynamics.ActiveReports.Label)
		Me.lblSendingInstructions = CType(Me.PageFooter.Controls(4),DataDynamics.ActiveReports.Label)
		Me.txtPageTotal = CType(Me.PageFooter.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.TextBox1 = CType(Me.PageFooter.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.lblBoxIncludes = CType(Me.PageFooter.Controls(7),DataDynamics.ActiveReports.Label)
		Me.txtBoxIncludes = CType(Me.PageFooter.Controls(8),DataDynamics.ActiveReports.TextBox)
	End Sub

#End Region

    Private Sub ActiveManifestReport_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.DataInitialize

        Me.Fields.Add("SKU_Num")
        Me.Fields.Add("Dept_Cd")
        Me.Fields.Add("VE_CD")
        Me.Fields.Add("ITM_CD")
        Me.Fields.Add("SIZE_CD")
        Me.Fields.Add("CURR")
        Me.Fields.Add("DES1")
        Me.Fields.Add("ITEM_Qty")
        Me.Fields.Add("Cust_Phone")
        Me.Fields.Add("Cust_Name")
        Me.Fields.Add("Xfer_Short_Desc")

    End Sub

    Private Sub ActiveManifestReport_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        CreateDataSet()

    End Sub

    Private Sub CreateDataSet()

        Dim strSQL As String
        Dim strFilter As String
        Dim strReportType As String
        Dim strFileName As String
        Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        'Dim objORAConnection As New OleDbConnection(ConfigurationSettings.AppSettings("strORAConnection"))

        Dim objSQLCommand As New SqlCommand()
        Dim objSQLDataAdapter As New SqlDataAdapter()

        'Dim objORACommand As New OleDbCommand()
        ' Dim objORADataAdapter As New OleDbDataAdapter()

        Dim I As Int32
        Dim SKUNumbers As String
        Dim objRelation As DataRelation

        Dim strStoredProcedure As String

        strSQL = "SELECT     Box_Xfer_Item.*, Xfer_Type.Xfer_Desc,Xfer_Type.Xfer_Short_Desc,Customer_Info.FullName as Cust_Name, "
        strSQL = strSQL & "              Customer_Info.Phone as Cust_Phone "
        strSQL = strSQL & "FROM         Box_Xfer_Item INNER JOIN "
        strSQL = strSQL & "              Xfer_Type ON Box_Xfer_Item.Xfer_Type_Cd = Xfer_Type.Xfer_Cd LEFT OUTER JOIN "
        strSQL = strSQL & "              Customer_Info ON Box_Xfer_Item.Cust_Id = Customer_Info.Cust_Id "
        strSQL = strSQL & "WHERE Box_Xfer_Item.Box_Id = '" & m_BoxId & "' "
        strSQL = strSQL & "ORDER BY Xfer_Type.Xfer_Desc, Box_Xfer_Item.SKU_Num "

        objSQLConnection.Open()
        objSQLCommand.Connection = objSQLConnection
        objSQLCommand.CommandText = strSQL
        objSQLDataAdapter.SelectCommand = objSQLCommand
        objSQLDataAdapter.FillSchema(m_objDataSet, SchemaType.Mapped)
        objSQLDataAdapter.Fill(m_objDataSet, "BoxItems")
        objSQLConnection.Close()

        'm_objDataSet.Tables("BoxItems").Columns.Add("VE_CD", GetType(String))
        'm_objDataSet.Tables("BoxItems").Columns.Add("SIZE_CD", GetType(String))
        'm_objDataSet.Tables("BoxItems").Columns.Add("CURR", GetType(Decimal))
        'm_objDataSet.Tables("BoxItems").Columns.Add("DES1", GetType(String))
        'm_objDataSet.Tables("BoxItems").Columns.Add("DEPT_CD", GetType(String))
        'm_objDataSet.Tables("BoxItems").Columns.Add("ITM_CD", GetType(String))

        'Dim BoxItemskeys(3) As DataColumn

        'BoxItemskeys(0) = m_objDataSet.Tables("BoxItems").Columns("Box_Id")
        'BoxItemskeys(1) = m_objDataSet.Tables("BoxItems").Columns("SKU_Num")
        'BoxItemskeys(2) = m_objDataSet.Tables("BoxItems").Columns("Xfer_Type_Cd")
        'BoxItemskeys(3) = m_objDataSet.Tables("BoxItems").Columns("Cust_Id")

        'm_objDataSet.Tables("BoxItems").PrimaryKey = BoxItemskeys

        'If m_objDataSet.Tables("BoxItems").Rows.Count = 0 Then
        'SKUNumbers = "'0'"
        ' Else
        'For I = 0 To m_objDataSet.Tables("BoxItems").Rows.Count - 1
        'SKUNumbers = SKUNumbers & "'" & m_objDataSet.Tables("BoxItems").Rows(I).Item("SKU_Num") & "',"
        'Next I

        'SKUNumbers = Mid(SKUNumbers, 1, SKUNumbers.Length - 1)
        ' End If

        'strSQL = " SELECT DISTINCT U.SKU_NUM, A.DES1, B.curr, U.SIZE_CD, A.DES1, A.VE_CD,A.DEPT_CD,A.ITM_CD "
        'strSQL = strSQL & " FROM gm_itm A, GM_SKU U, "
        'strSQL = strSQL & " (SELECT P1.sku_num SKU_NUM, P1.ret_prc CURR "
        'strSQL = strSQL & " FROM gm_prc P1,  "
        'strSQL = strSQL & " (SELECT sku_num, MAX(to_char(beg_dt, 'yyyymmdd')||to_char(ent_dt, 'yyyymmdd')||ent_time) last "
        ' strSQL = strSQL & "  FROM gm_prc  "
        ' strSQL = strSQL & "  WHERE sku_num in (" & SKUNumbers & ") AND beg_dt <= SYSDATE "
        ' strSQL = strSQL & "  GROUP BY sku_num ) P2 "
        ' strSQL = strSQL & "  WHERE P1.sku_num = P2.sku_num "
        'strSQL = strSQL & "  AND TO_CHAR(P1.beg_dt, 'yyyymmdd')||TO_CHAR(P1.ent_dt,'yyyymmdd')||P1.ent_time = P2.last) B "
        ' strSQL = strSQL & "  WHERE A.itm_cd = SUBSTR(B.sku_num,1,6) AND (A.ITM_CD = U.ITM_CD) AND U.SKU_NUM IN (" & SKUNumbers & ") "

        ' objORAConnection.Open()
        ' objORACommand.Connection = objORAConnection
        ' objORACommand.CommandType = CommandType.Text
        ' objORACommand.CommandText = strSQL

        'objORADataAdapter.SelectCommand = objORACommand
        'objORADataAdapter.Fill(m_objDataSet, "ORAItemInfo")
        ' objORAConnection.Close()

        'Dim keys(0) As DataColumn
        'keys(0) = m_objDataSet.Tables("ORAItemInfo").Columns("SKU_Num")
        'keys(0).Unique = True
        'keys(0).AllowDBNull = False
        'm_objDataSet.Tables("ORAItemInfo").PrimaryKey = keys

        'AddORAItemInfo(m_objDataSet.Tables("BoxItems"), m_objDataSet.Tables("ORAItemInfo"))
        ' m_objDataSet.Tables("ORAItemInfo").Dispose()


    End Sub

    Private Sub AddORAItemInfo(ByRef objTargetDataTable As DataTable, ByRef objSourceDataTable As DataTable)

        Dim CurrentDataRow As DataRow
        Dim FoundRow As DataRow
        Dim SKU_Num(0) As Object

        For Each CurrentDataRow In objTargetDataTable.Rows

            SKU_Num(0) = CurrentDataRow("SKU_Num")
            FoundRow = objSourceDataTable.Rows.Find(SKU_Num)

            If Not FoundRow Is Nothing Then
                CurrentDataRow("VE_CD") = FoundRow("VE_CD")
                CurrentDataRow("DES1") = FoundRow("DES1")
                CurrentDataRow("SIZE_CD") = FoundRow("SIZE_CD")
                CurrentDataRow("CURR") = FoundRow("CURR")
                CurrentDataRow("ITM_CD") = FoundRow("ITM_CD")
                CurrentDataRow("DEPT_CD") = FoundRow("DEPT_CD")
                CurrentDataRow("ITM_CD") = FoundRow("ITM_CD")
            End If

        Next

    End Sub

    Private Sub ActiveManifestReport_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles MyBase.FetchData
        Try

            If m_objDataSet.Tables("BoxItems").Rows.Count > m_RowCount Then
                Me.Fields("SKU_Num").Value = m_objDataSet.Tables("BoxItems").Rows(m_RowCount)("SKU_Num")
                Me.Fields("DEPT_CD").Value = m_objDataSet.Tables("BoxItems").Rows(m_RowCount)("DEPT_CD")
                Me.Fields("VE_CD").Value = m_objDataSet.Tables("BoxItems").Rows(m_RowCount)("VE_CD")
                Me.Fields("ITM_CD").Value = m_objDataSet.Tables("BoxItems").Rows(m_RowCount)("ITM_CD")
                Me.Fields("SIZE_CD").Value = m_objDataSet.Tables("BoxItems").Rows(m_RowCount)("SIZE_CD")
                Me.Fields("CURR").Value = m_objDataSet.Tables("BoxItems").Rows(m_RowCount)("CURR")
                Me.Fields("DES1").Value = m_objDataSet.Tables("BoxItems").Rows(m_RowCount)("DES1")
                Me.Fields("ITEM_Qty").Value = m_objDataSet.Tables("BoxItems").Rows(m_RowCount)("ITEM_Qty")
                Me.Fields("Cust_Phone").Value = m_objDataSet.Tables("BoxItems").Rows(m_RowCount)("Cust_Phone")
                Me.Fields("Cust_Name").Value = m_objDataSet.Tables("BoxItems").Rows(m_RowCount)("Cust_Name")
                Me.Fields("Xfer_Short_Desc").Value = m_objDataSet.Tables("BoxItems").Rows(m_RowCount)("Xfer_Short_Desc")

                m_RowCount = m_RowCount + 1

                eArgs.EOF = False
            Else
                eArgs.EOF = True
            End If

        Catch
            eArgs.EOF = True
        End Try

    End Sub

    Private Sub ActiveManifestReport_PageStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PageStart

        Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim strFileName As String
        Dim strStoredProcedure As String
        Dim objDataReader As SqlClient.SqlDataReader
        Dim strBoxInculdes As String
        Dim intTType As Int32
        Dim strCopies As String

        objSQLConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeader '" & m_BoxId & "' ", objSQLConnection)

        objDataReader = objCommand.ExecuteReader()

        objDataReader.Read()

        Me.txtFromStoreName.Text = objDataReader("SStore").ToString
        Me.txtFromStoreNum.Text = objDataReader("Sending_Store_Cd").ToString
        Me.txtStoreNumTop.Text = objDataReader("Sending_Store_Cd").ToString
        Me.txtTransfer.Text = objDataReader("Box_Id").ToString
        Me.txtToStoreNum.Text = objDataReader("Receiving_Store_Cd").ToString
        Me.txtToStoreName.Text = objDataReader("RStore").ToString
        Me.txtTrackingNum.Text = objDataReader("Tracking_Num").ToString
        Me.txtShipmentDate.Text = String.Format("{0:d}", objDataReader("Shipment_Date"))
        Me.txtCarrier.Text = objDataReader("ParcelCompany").ToString

        intTType = objDataReader("TType")

        objDataReader.Close()

        objCommand.CommandText = "spGetBoxIncludes '" & m_BoxId & "' "

        objDataReader = objCommand.ExecuteReader()

        Do While objDataReader.Read()
            If objDataReader("Checked") Then
                strBoxInculdes = strBoxInculdes & objDataReader("Includes_Desc") & ", "
            End If
        Loop

        If strBoxInculdes.Length > 0 Then
            strBoxInculdes = Mid(strBoxInculdes, 1, strBoxInculdes.Length - 2)
        End If

        Me.txtBoxIncludes.Text = strBoxInculdes

        objDataReader.Close()

        strCopies = "Sending Store "

        If intTType = 1 Then 'Store Transfer

            strCopies = strCopies & "| Receiving Store "

        ElseIf intTType = 2 Then 'Returns

            strCopies = strCopies & "| Returns "

            objCommand.CommandText = "spGetBoxTransferTypes '" & m_BoxId & "' "
            objDataReader = objCommand.ExecuteReader()

            Do While objDataReader.Read()

                strCopies = strCopies & "| " & objDataReader("Xfer_Desc") & " "

            Loop

        End If

        Me.txtCopy.Text = strCopies

        objCommand.Dispose()
        objDataReader.Close()
        objSQLConnection.Close()

    End Sub


End Class
