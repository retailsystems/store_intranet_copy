Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class Full_Manifest
    Inherits ActiveReport

    Private m_BoxId As String
    Private m_strTitle As String
    Private m_Page As Integer
    Private m_TopPic As PicType

    Public Enum PicType
        Hottopic = 1
        Torrid = 2
    End Enum

    Public Sub New(ByVal BoxId As String, ByVal strTitle As String, ByVal TopPic As PicType)
        MyBase.New()
        m_BoxId = BoxId
        m_TopPic = TopPic
        m_strTitle = strTitle
        InitializeReport()
    End Sub

#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents ghHeader As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents gfHeader As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
	Private Shape28 As DataDynamics.ActiveReports.Shape = Nothing
	Private PicHottopic As DataDynamics.ActiveReports.Picture = Nothing
	Private lblQuantReq As DataDynamics.ActiveReports.Label = Nothing
	Private lblQuantShipped As DataDynamics.ActiveReports.Label = Nothing
	Private Shape11 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape10 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape16 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape15 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtFromStoreName As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtToStoreNum As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtToStoreName As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblVendor As DataDynamics.ActiveReports.Label = Nothing
	Private lblDept As DataDynamics.ActiveReports.Label = Nothing
	Private lblSKU As DataDynamics.ActiveReports.Label = Nothing
	Private lblTransferType As DataDynamics.ActiveReports.Label = Nothing
	Private lblCust As DataDynamics.ActiveReports.Label = Nothing
	Private lblDescription As DataDynamics.ActiveReports.Label = Nothing
	Private lblRetail As DataDynamics.ActiveReports.Label = Nothing
	Private lblSize As DataDynamics.ActiveReports.Label = Nothing
	Private lblStyle As DataDynamics.ActiveReports.Label = Nothing
	Private Shape3 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblTransfer As DataDynamics.ActiveReports.Label = Nothing
	Private lblFromStoreTop As DataDynamics.ActiveReports.Label = Nothing
	Private Shape4 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape5 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape6 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape7 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape8 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape9 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape12 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape13 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblFrom As DataDynamics.ActiveReports.Label = Nothing
	Private lblFromStore As DataDynamics.ActiveReports.Label = Nothing
	Private lblStoreName As DataDynamics.ActiveReports.Label = Nothing
	Private lblTo As DataDynamics.ActiveReports.Label = Nothing
	Private lblToStoreNum As DataDynamics.ActiveReports.Label = Nothing
	Private lblToStoreName As DataDynamics.ActiveReports.Label = Nothing
	Private Line2 As DataDynamics.ActiveReports.Line = Nothing
	Private Line1 As DataDynamics.ActiveReports.Line = Nothing
	Private txtStoreNumTop As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTransfer As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblManifest As DataDynamics.ActiveReports.Label = Nothing
	Private Shape14 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtFromStoreNum As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label1 As DataDynamics.ActiveReports.Label = Nothing
	Private txtTrackingNum As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblShipdate As DataDynamics.ActiveReports.Label = Nothing
	Private txtShipmentDate As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblCarrier As DataDynamics.ActiveReports.Label = Nothing
	Private txtCarrier As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtCopy As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtBoxIncludes As DataDynamics.ActiveReports.TextBox = Nothing
	Private lblPage As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label2 As DataDynamics.ActiveReports.Label = Nothing
	Private lblApprovedBy As DataDynamics.ActiveReports.Label = Nothing
	Private txtApprovedBy As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape1 As DataDynamics.ActiveReports.Shape = Nothing
	Private lblBoxIncludes As DataDynamics.ActiveReports.Label = Nothing
	Private txtNotes As DataDynamics.ActiveReports.TextBox = Nothing
	Private Label3 As DataDynamics.ActiveReports.Label = Nothing
	Private Line3 As DataDynamics.ActiveReports.Line = Nothing
	Private PicTorrid As DataDynamics.ActiveReports.Picture = Nothing
	Private Shape24 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtSKU As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDept As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtVendor As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtItem As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtSize As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtRetail As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtDesc As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtQS As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtTT As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape23 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape22 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape21 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape20 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape19 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape18 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape17 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape25 As DataDynamics.ActiveReports.Shape = Nothing
	Private Shape27 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtPhone As DataDynamics.ActiveReports.TextBox = Nothing
	Private Shape26 As DataDynamics.ActiveReports.Shape = Nothing
	Private txtCustName As DataDynamics.ActiveReports.TextBox = Nothing
	Private txtQR As DataDynamics.ActiveReports.TextBox = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "SSTransfer.Full_Manifest.rpx")
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.ghHeader = CType(Me.Sections("ghHeader"),DataDynamics.ActiveReports.GroupHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.gfHeader = CType(Me.Sections("gfHeader"),DataDynamics.ActiveReports.GroupFooter)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Shape28 = CType(Me.ghHeader.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.PicHottopic = CType(Me.ghHeader.Controls(1),DataDynamics.ActiveReports.Picture)
		Me.lblQuantReq = CType(Me.ghHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.lblQuantShipped = CType(Me.ghHeader.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Shape11 = CType(Me.ghHeader.Controls(4),DataDynamics.ActiveReports.Shape)
		Me.Shape10 = CType(Me.ghHeader.Controls(5),DataDynamics.ActiveReports.Shape)
		Me.Shape16 = CType(Me.ghHeader.Controls(6),DataDynamics.ActiveReports.Shape)
		Me.Shape15 = CType(Me.ghHeader.Controls(7),DataDynamics.ActiveReports.Shape)
		Me.txtFromStoreName = CType(Me.ghHeader.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtToStoreNum = CType(Me.ghHeader.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.txtToStoreName = CType(Me.ghHeader.Controls(10),DataDynamics.ActiveReports.TextBox)
		Me.lblVendor = CType(Me.ghHeader.Controls(11),DataDynamics.ActiveReports.Label)
		Me.lblDept = CType(Me.ghHeader.Controls(12),DataDynamics.ActiveReports.Label)
		Me.lblSKU = CType(Me.ghHeader.Controls(13),DataDynamics.ActiveReports.Label)
		Me.lblTransferType = CType(Me.ghHeader.Controls(14),DataDynamics.ActiveReports.Label)
		Me.lblCust = CType(Me.ghHeader.Controls(15),DataDynamics.ActiveReports.Label)
		Me.lblDescription = CType(Me.ghHeader.Controls(16),DataDynamics.ActiveReports.Label)
		Me.lblRetail = CType(Me.ghHeader.Controls(17),DataDynamics.ActiveReports.Label)
		Me.lblSize = CType(Me.ghHeader.Controls(18),DataDynamics.ActiveReports.Label)
		Me.lblStyle = CType(Me.ghHeader.Controls(19),DataDynamics.ActiveReports.Label)
		Me.Shape3 = CType(Me.ghHeader.Controls(20),DataDynamics.ActiveReports.Shape)
		Me.lblTransfer = CType(Me.ghHeader.Controls(21),DataDynamics.ActiveReports.Label)
		Me.lblFromStoreTop = CType(Me.ghHeader.Controls(22),DataDynamics.ActiveReports.Label)
		Me.Shape4 = CType(Me.ghHeader.Controls(23),DataDynamics.ActiveReports.Shape)
		Me.Shape5 = CType(Me.ghHeader.Controls(24),DataDynamics.ActiveReports.Shape)
		Me.Shape6 = CType(Me.ghHeader.Controls(25),DataDynamics.ActiveReports.Shape)
		Me.Shape7 = CType(Me.ghHeader.Controls(26),DataDynamics.ActiveReports.Shape)
		Me.Shape8 = CType(Me.ghHeader.Controls(27),DataDynamics.ActiveReports.Shape)
		Me.Shape9 = CType(Me.ghHeader.Controls(28),DataDynamics.ActiveReports.Shape)
		Me.Shape12 = CType(Me.ghHeader.Controls(29),DataDynamics.ActiveReports.Shape)
		Me.Shape13 = CType(Me.ghHeader.Controls(30),DataDynamics.ActiveReports.Shape)
		Me.lblFrom = CType(Me.ghHeader.Controls(31),DataDynamics.ActiveReports.Label)
		Me.lblFromStore = CType(Me.ghHeader.Controls(32),DataDynamics.ActiveReports.Label)
		Me.lblStoreName = CType(Me.ghHeader.Controls(33),DataDynamics.ActiveReports.Label)
		Me.lblTo = CType(Me.ghHeader.Controls(34),DataDynamics.ActiveReports.Label)
		Me.lblToStoreNum = CType(Me.ghHeader.Controls(35),DataDynamics.ActiveReports.Label)
		Me.lblToStoreName = CType(Me.ghHeader.Controls(36),DataDynamics.ActiveReports.Label)
		Me.Line2 = CType(Me.ghHeader.Controls(37),DataDynamics.ActiveReports.Line)
		Me.Line1 = CType(Me.ghHeader.Controls(38),DataDynamics.ActiveReports.Line)
		Me.txtStoreNumTop = CType(Me.ghHeader.Controls(39),DataDynamics.ActiveReports.TextBox)
		Me.txtTransfer = CType(Me.ghHeader.Controls(40),DataDynamics.ActiveReports.TextBox)
		Me.lblManifest = CType(Me.ghHeader.Controls(41),DataDynamics.ActiveReports.Label)
		Me.Shape14 = CType(Me.ghHeader.Controls(42),DataDynamics.ActiveReports.Shape)
		Me.txtFromStoreNum = CType(Me.ghHeader.Controls(43),DataDynamics.ActiveReports.TextBox)
		Me.Label1 = CType(Me.ghHeader.Controls(44),DataDynamics.ActiveReports.Label)
		Me.txtTrackingNum = CType(Me.ghHeader.Controls(45),DataDynamics.ActiveReports.TextBox)
		Me.lblShipdate = CType(Me.ghHeader.Controls(46),DataDynamics.ActiveReports.Label)
		Me.txtShipmentDate = CType(Me.ghHeader.Controls(47),DataDynamics.ActiveReports.TextBox)
		Me.lblCarrier = CType(Me.ghHeader.Controls(48),DataDynamics.ActiveReports.Label)
		Me.txtCarrier = CType(Me.ghHeader.Controls(49),DataDynamics.ActiveReports.TextBox)
		Me.txtCopy = CType(Me.ghHeader.Controls(50),DataDynamics.ActiveReports.TextBox)
		Me.txtBoxIncludes = CType(Me.ghHeader.Controls(51),DataDynamics.ActiveReports.TextBox)
		Me.lblPage = CType(Me.ghHeader.Controls(52),DataDynamics.ActiveReports.TextBox)
		Me.Label2 = CType(Me.ghHeader.Controls(53),DataDynamics.ActiveReports.Label)
		Me.lblApprovedBy = CType(Me.ghHeader.Controls(54),DataDynamics.ActiveReports.Label)
		Me.txtApprovedBy = CType(Me.ghHeader.Controls(55),DataDynamics.ActiveReports.TextBox)
		Me.Shape1 = CType(Me.ghHeader.Controls(56),DataDynamics.ActiveReports.Shape)
		Me.lblBoxIncludes = CType(Me.ghHeader.Controls(57),DataDynamics.ActiveReports.Label)
		Me.txtNotes = CType(Me.ghHeader.Controls(58),DataDynamics.ActiveReports.TextBox)
		Me.Label3 = CType(Me.ghHeader.Controls(59),DataDynamics.ActiveReports.Label)
		Me.Line3 = CType(Me.ghHeader.Controls(60),DataDynamics.ActiveReports.Line)
		Me.PicTorrid = CType(Me.ghHeader.Controls(61),DataDynamics.ActiveReports.Picture)
		Me.Shape24 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.Shape)
		Me.txtSKU = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.TextBox)
		Me.txtDept = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.TextBox)
		Me.txtVendor = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.TextBox)
		Me.txtItem = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.TextBox)
		Me.txtSize = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.TextBox)
		Me.txtRetail = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.TextBox)
		Me.txtDesc = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.TextBox)
		Me.txtQS = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.TextBox)
		Me.txtTT = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.TextBox)
		Me.Shape23 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.Shape)
		Me.Shape22 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.Shape)
		Me.Shape21 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.Shape)
		Me.Shape20 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.Shape)
		Me.Shape19 = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.Shape)
		Me.Shape18 = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.Shape)
		Me.Shape17 = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.Shape)
		Me.Shape25 = CType(Me.Detail.Controls(17),DataDynamics.ActiveReports.Shape)
		Me.Shape27 = CType(Me.Detail.Controls(18),DataDynamics.ActiveReports.Shape)
		Me.txtPhone = CType(Me.Detail.Controls(19),DataDynamics.ActiveReports.TextBox)
		Me.Shape26 = CType(Me.Detail.Controls(20),DataDynamics.ActiveReports.Shape)
		Me.txtCustName = CType(Me.Detail.Controls(21),DataDynamics.ActiveReports.TextBox)
		Me.txtQR = CType(Me.Detail.Controls(22),DataDynamics.ActiveReports.TextBox)
	End Sub

#End Region

    Private Sub Full_Manifest_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.DataInitialize

        Me.Fields.Add("SKU_Num")
        Me.Fields.Add("Dept_Cd")
        Me.Fields.Add("VE_CD")
        Me.Fields.Add("ITM_CD")
        Me.Fields.Add("SIZE_CD")
        Me.Fields.Add("CURR")
        Me.Fields.Add("DES1")
        Me.Fields.Add("ITEM_Qty")
        Me.Fields.Add("Received_Qty")
        Me.Fields.Add("Cust_Phone")
        Me.Fields.Add("Cust_Name")
        Me.Fields.Add("Xfer_Short_Desc")

    End Sub

    'Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format

    ' Me.Fields("SKU_Num").Value = Me.Fields("SKU_Num").Value
    ' Me.Fields("DEPT_CD").Value = Me.Fields("DEPT_CD").Value
    'Me.Fields("VE_CD").Value = Me.Fields("VE_CD").Value
    'Me.Fields("ITM_CD").Value = Me.Fields("ITM_CD").Value
    'Me.Fields("SIZE_CD").Value = Me.Fields("SIZE_CD").Value
    'Me.Fields("CURR").Value = Me.Fields("CURR").Value
    'Me.Fields("DES1").Value = Me.Fields("DES1").Value
    'Me.Fields("ITEM_Qty").Value = Me.Fields("ITEM_Qty").Value
    'Me.Fields("Cust_Phone").Value = Me.Fields("Cust_Phone").Value
    'Me.Fields("Cust_Name").Value = Me.Fields("Cust_Name").Value
    'Me.Fields("Xfer_Short_Desc").Value = Me.Fields("Xfer_Short_Desc").Value

    ' End Sub

    Private Sub Full_Manifest_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        If m_TopPic = PicType.Hottopic Then
            Me.PicHottopic.Visible = True
        ElseIf m_TopPic = PicType.Torrid Then
            Me.PicTorrid.Visible = True
        End If

        CreateDataSet()
        GetHeader()

    End Sub

    Private Sub ghHeader_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles ghHeader.BeforePrint
        m_Page = m_Page + 1
        Me.lblPage.Value = m_Page
    End Sub

    Private Sub GetHeader()

        Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim strFileName As String
        Dim strStoredProcedure As String
        Dim objDataReader As SqlClient.SqlDataReader
        Dim strBoxInculdes As String
        Dim intTType As Int32
        Dim booONWAN As Boolean

        objSQLConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeader '" & m_BoxId & "' ", objSQLConnection)

        objDataReader = objCommand.ExecuteReader()

        objDataReader.Read()

        Me.txtFromStoreName.Text = objDataReader("SStore").ToString
        Me.txtFromStoreNum.Text = objDataReader("Sending_Store_Cd").ToString
        Me.txtStoreNumTop.Text = objDataReader("Sending_Store_Cd").ToString
        Me.txtTransfer.Text = objDataReader("Box_Id").ToString
        Me.txtToStoreNum.Text = objDataReader("Receiving_Store_Cd").ToString
        Me.txtToStoreName.Text = objDataReader("RStore").ToString
        Me.txtTrackingNum.Text = objDataReader("Tracking_Num").ToString
        Me.txtShipmentDate.Text = String.Format("{0:d}", objDataReader("Shipment_Date"))
        Me.txtCarrier.Text = objDataReader("ParcelCompany").ToString
        Me.txtCopy.Text = m_strTitle
        Me.txtNotes.Text = objDataReader("Notes").ToString

        If objDataReader("Approved_By").ToString.Trim.Length > 0 Then
            Me.txtApprovedBy.Text = objDataReader("Approved_By").ToString
        End If

        intTType = objDataReader("TType")
        booONWAN = objDataReader("ReceivingStoreWAN")

        objCommand.CommandText = "spGetBoxIncludes '" & m_BoxId & "'," & intTType & "," & objDataReader("Receiving_Store_Cd").ToString & "," & objDataReader("Sending_Store_Cd").ToString

        objDataReader.Close()

        objDataReader = objCommand.ExecuteReader()

        Do While objDataReader.Read()
            If objDataReader("Checked") Then
                strBoxInculdes = strBoxInculdes & objDataReader("Includes_Desc") & ", "
            End If
        Loop

        If strBoxInculdes.Length > 0 Then
            strBoxInculdes = Mid(strBoxInculdes, 1, strBoxInculdes.Length - 2)
        End If

        Me.txtBoxIncludes.Text = strBoxInculdes

        objCommand.Dispose()
        objDataReader.Close()
        objSQLConnection.Close()

    End Sub

    Private Sub CreateDataSet()

        Dim m_ds = New DataDynamics.ActiveReports.DataSources.SqlDBDataSource()
        Dim strSQL As String

        m_ds.ConnectionString = ConfigurationSettings.AppSettings("strSQLConnection")

        strSQL = "SELECT     Box_Xfer_Item.*, Xfer_Type.Xfer_Desc,Xfer_Type.Xfer_Short_Desc,Customer_Info.FullName as Cust_Name, "
        strSQL = strSQL & "              Customer_Info.Phone as Cust_Phone "
        strSQL = strSQL & "FROM         Box_Xfer_Item INNER JOIN "
        strSQL = strSQL & "              Xfer_Type ON Box_Xfer_Item.Xfer_Type_Cd = Xfer_Type.Xfer_Cd LEFT OUTER JOIN "
        strSQL = strSQL & "              Customer_Info ON Box_Xfer_Item.Cust_Id = Customer_Info.Cust_Id "
        strSQL = strSQL & "WHERE Box_Xfer_Item.Box_Id = '" & m_BoxId & "' "
        strSQL = strSQL & "ORDER BY Xfer_Type.Xfer_Desc, Box_Xfer_Item.Dept_Cd, Box_Xfer_Item.SKU_Num "

        m_ds.SQL = strSQL
        Me.DataSource = m_ds

    End Sub


End Class
