<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SignIn.aspx.vb" Inherits="SSTransfer.SignIn"%>
<%@ Register TagPrefix="uc1" TagName="Navigation" Src="UserControls/Navigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Transfer Home</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmTransferHome" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td height="1"><uc1:header id="layoutHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td align="middle">
						<table cellSpacing="2" cellPadding="3" border="0">
							<tr id="trTranType" runat="server">
								<td align="middle" colSpan="3"><asp:radiobutton id="radReturn" tabIndex="3" runat="server" Font-Size="Small" Font-Names="Arial" Font-Bold="True" Text="Return" ForeColor="Red" GroupName="grpTranType" Checked="True"></asp:radiobutton>&nbsp;&nbsp;
									<asp:radiobutton id="radStore" tabIndex="4" runat="server" Font-Size="Small" Font-Names="Arial" Font-Bold="True" Text="Store to Store" ForeColor="Red" GroupName="grpTranType"></asp:radiobutton></td>
							</tr>
							<tr id="trTranTypeSpace" runat="server">
								<td colSpan="3">&nbsp;</td>
							</tr>
							<tr id="trEmpId" runat="server">
								<td><font face="Arial" color="white" size="2"><b>Employee Id:</b></font>
								</td>
								<td><asp:textbox id="txtEmployeeId" tabIndex="1" runat="server" TextMode="Password"></asp:textbox></td>
								<td><asp:button id="btnSubmit" tabIndex="2" runat="server" Font-Bold="True" Text="Submit" ForeColor="White" BorderColor="#990000" BackColor="#440000"></asp:button></td>
							</tr>
							<tr id="trEmpIdBlank" runat="server">
								<td align="middle" colSpan="3"><asp:button id="btnSubmitBlank" tabIndex="2" runat="server" Font-Bold="True" Text="Submit" ForeColor="White" BorderColor="#990000" BackColor="#440000"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
