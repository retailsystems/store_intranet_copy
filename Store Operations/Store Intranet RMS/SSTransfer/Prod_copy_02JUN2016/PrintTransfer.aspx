<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PrintTransfer.aspx.vb" Inherits="SSTransfer.PrintTransfer" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Transfer Home</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/Torrid.css" type="text/css" rel="stylesheet" runat="server"/>
	</HEAD>
	<body class="sBody" id="pagebody" runat="server" scroll="no" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmTransferHome" method="post" runat="server" onsubmit="return checkSubmit();">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1%"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td height="1%">
						<table cellSpacing="0" cellPadding="2" width="100%" border="0">
							<tr>
								<td width="1%"><img src="images\info.gif" border="0"></td>
								<td valign="center"><asp:Label Runat="server" ID="lblHelpLabel" cssclass="sPrintTrans"></asp:Label></td>
							</tr>
						</table>
						<hr width="100%" class="sHr">
					</td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="4" width="100%">
							<tr>
								<td height="1%" align="right">
									<asp:button id="btnDone" runat="server" Text="Done" CssClass="sButton"></asp:button>
								</td>
							</tr>
							<tr>
								<td height="99%">
									<iframe runat="server" id="pdfIFrame" src='' width="100%" height="100%"></iframe>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1%"><uc1:Footer id="ucFooter" runat="server"></uc1:Footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
