<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_EditTransfer.aspx.vb" Inherits="SSTransfer.IC_EditTransfer"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>IC Edit Transfer</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
	</HEAD>
	<body id="pageBody" runat="server" bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="yes" rightMargin="0">
		<form id="frmTransferHome" onsubmit="return checkSubmit();" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="top">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><asp:label id="lblShipDate_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Ship Date:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblShipDate_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td><asp:label id="lblUPS_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Tracking Number:</asp:label></td>
								<td><asp:hyperlink id="hlUPS" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Visible="True" Target="_blank" NavigateUrl="">
										<asp:Label runat="server" ForeColor="White" ID="lblUPS_Value" Font-Size="X-Small" Font-Names="Arial"></asp:Label>
									</asp:hyperlink></td>
								<td><asp:Label ID="lblReShipInfo" Runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Re-Ship Information</asp:Label></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblTransferId_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Transfer ID:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblTransferId_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td noWrap width="1%"><asp:label id="lblEmp_name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Employee:</asp:label></td>
								<td><asp:label id="lblEmp_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label><input id="btnHistory" style="FONT-WEIGHT: bold; BORDER-LEFT-COLOR: #990000; BORDER-BOTTOM-COLOR: #990000; COLOR: white; BORDER-TOP-COLOR: #990000; FONT-FAMILY: Arial; HEIGHT: 20px; BACKGROUND-COLOR: #440000; BORDER-RIGHT-COLOR: #990000" tabIndex="1" type="button" value="H" name="btnHistory" runat="server"></td>
								<td><asp:Label ID="lblNewTransfer" Runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">New Transfer Id:</asp:Label></td>
								<td><asp:Label ID="lblNewTransID" Runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"></asp:Label></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblStatus_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Box Status:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblStatus_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td noWrap width="1%"><asp:label id="lblsensor" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Sensor Tags:</asp:label></td>
								<td vAlign="center"><asp:label id="lblInk_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td><asp:Label ID="lblOldTransfer" Runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Old Transfer Id:</asp:Label></td>
								<td><asp:Label ID="lblOldTransID" Runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"></asp:Label></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblSStore_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Sending Store:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblSStore_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td noWrap width="1%"><asp:label id="lblRStore_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Receiving Store:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:dropdownlist id="ddlRStore" tabIndex="2" runat="server" ForeColor="White" BackColor="Black" DataValueField="StoreNum" DataTextField="StoreNum"></asp:dropdownlist><asp:label id="lblVRStore" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Visible="False" Runat="server"></asp:label></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblDisQuant_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Discrepancy Quant:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblDisQuant_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
								<td noWrap width="1%"><asp:label id="lblDisAmount_Name" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Discrepancy Amount:</asp:label>&nbsp;&nbsp;</td>
								<td><asp:label id="lblDisAmount_Value" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial"></asp:label></td>
							</tr>
							<tr>
								<td noWrap><asp:label id="lblNotes" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Runat="server" Font-Bold="True">Shipment Notes:</asp:label></td>
							</tr>
							<tr>
								<td noWrap colSpan="6"><asp:Label Runat="server" ID="lblStoreNotes" Width="99%" Font-Names="Arial" Font-Size="X-Small" ForeColor="White"></asp:Label></td>
							</tr>
							<tr>
								<td noWrap colSpan="6"><asp:textbox id="txtNotes" runat="server" Width="99%" Height="50px" TextMode="MultiLine" BorderColor="#990000"></asp:textbox></td>
							</tr>
							<tr>
								<td nowrap colspan="6" align="right"><br>
									<asp:button id="btnEdit" tabIndex="12" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" BackColor="#440000" Runat="server" BorderColor="#990000" Font-Bold="True" Text="Edit"></asp:button>&nbsp;<asp:Button Runat="server" ID="btnLost" Text="Lost" BorderColor="#990000" Font-Bold="True" tabIndex="13" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" BackColor="#440000"></asp:Button>&nbsp;<asp:button id="btnReShip" tabIndex="14" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" BackColor="#440000" Runat="server" BorderColor="#990000" Font-Bold="True" Text="Re-Ship"></asp:button>&nbsp;<asp:button id="btnVoid" tabIndex="15" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" Visible="False" BackColor="#440000" Runat="server" BorderColor="#990000" Font-Bold="True" Text="Void"></asp:button>&nbsp;<asp:Button Runat="server" ID="btnUpdateStoreNotes" Text="Update Notes" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" BorderColor="#990000" Font-Bold="True" tabIndex="16" BackColor="#440000"></asp:Button>&nbsp;<asp:button id="btnReturn" tabIndex="17" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" BackColor="#440000" Runat="server" BorderColor="#990000" Font-Bold="True" Text="Return"></asp:button></td>
							</tr>
						</table>
						<hr width="100%" color="#990000">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><asp:datagrid id="dgXferGroups" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" BackColor="#111111" Width="100%" AllowSorting="True" CellSpacing="1" CellPadding="2" EnableViewState="False" BorderWidth="0" AutoGenerateColumns="False">
										<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
										<Columns>
											<asp:BoundColumn DataField="Xfer_Type" HeaderText="Grouping">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn headerText="GERS Process Status">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:DropDownList Runat="server" TabIndex="3" ID="ddlGersStatus" DataSource="<%# FillGers() %>" DataTextField="GERSSTATUS_DESC" DataValueField="GERSSTATUS_CD">
													</asp:DropDownList>
													<asp:Label Runat="server" ID="lblVGers" Font-Names="Arial" Font-Size="X-Small" ForeColor="White"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headerText="GERS Status" Visible="False">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:DropDownList Runat="server" ID="ddlorigGers" DataSource="<%# FillGers() %>" DataTextField="GERSSTATUS_DESC" DataValueField="GERSSTATUS_CD">
													</asp:DropDownList>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headerText="Discrep Status">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:DropDownList Runat="server" ID="ddlDiscrepStatus" TabIndex="4" DataSource='<%# FillDiscrep(Container.dataitem("Box_Id"),Container.dataitem("Xfer_Type_Cd")) %>' DataTextField="Disc_LongDESC" DataValueField="Disc_CD">
													</asp:DropDownList>
													<asp:Label Runat="server" ID="lblVDiscrep" Font-Names="Arial" Font-Size="X-Small" ForeColor="White"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headerText="Discrep Status" Visible="false">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:DropDownList Runat="server" ID="ddlorigDiscrep" DataSource='<%# FillDiscrep(Container.dataitem("Box_Id"),Container.dataitem("Xfer_Type_Cd")) %>' DataTextField="Disc_LongDESC" DataValueField="Disc_CD">
													</asp:DropDownList>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Comments">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" Width="5%" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<a href='IC_Comment.aspx?BID=<%# Container.dataitem("Box_Id") %>&XT=<%# Container.dataitem("Xfer_Type_Cd") %>&IET=<%# GetCommentReturnQuery() %>&RFC=<%# GetReturnQuery() %>&VO=<%# GetVOValue() %>'>
														<asp:Image TabIndex="5" Runat="server" ID="Image1" ImageUrl='<%# GetBoxComments(Container.dataitem("Box_Id"),Container.dataitem("Xfer_Type_Cd")) %>'>
														</asp:Image></a>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></td>
							</tr>
							<tr>
								<td noWrap align="right" width="100%"><asp:button id="btnUpdateGERS" tabIndex="6" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" BackColor="#440000" Runat="server" BorderColor="#990000" Font-Bold="True" Text="Update GERS"></asp:button></td>
							</tr>
						</table>
						<hr width="100%" color="#990000">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><asp:datagrid id="dgTransferBox" runat="server" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" BackColor="#111111" Width="100%" AllowSorting="True" CellSpacing="1" CellPadding="2" EnableViewState="False" BorderWidth="0" AutoGenerateColumns="False">
										<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
										<Columns>
											<asp:BoundColumn DataField="SKU_NUM" HeaderText="SKU">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="VE_CD" HeaderText="Vendor">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="SIZE_CD" HeaderText="Size">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="CURR" HeaderText="Retail" DataFormatString="{0:C}">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DES1" HeaderText="Description">
												<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn headerText="Type">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:DropDownList Runat="server" TabIndex="7" ID="ddlTType" DataSource='<%# FillType() %>' DataTextField="Xfer_DESC" DataValueField="Xfer_CD">
													</asp:DropDownList>
													<asp:Label Runat="server" ID="lblVType" Font-Names="Arial" Font-Size="X-Small" ForeColor="White"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headerText="OrigType" Visible="False">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:DropDownList Runat="server" ID="ddlorig" DataSource='<%# FillType() %>' DataTextField="Xfer_DESC" DataValueField="Xfer_CD">
													</asp:DropDownList>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headerText="Qty. Shipped">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:textbox BorderStyle="Solid" BorderWidth="2" BorderColor="#440000" ID="txtItemQty" text='<%# Container.DataItem("Item_Qty") %>' Runat="server" Width="40" EnableViewState="false">
													</asp:textbox>
													<asp:Label Runat="server" ID="lblorigshipqty" Font-Names="Arial" Font-Size="X-Small" ForeColor="White"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headerText="Qty. Received">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox BorderStyle="Solid" BorderWidth="2" TabIndex="9" BorderColor="#440000" MaxLength="3" ID="txtReceivedQty" text='<%# Container.DataItem("Received_Qty") %>' Runat="server" Width="40" EnableViewState="false">
													</asp:TextBox>
													<asp:Label Runat="server" ID="lblqtyreceived" Font-Names="Arial" Font-Size="X-Small" ForeColor="White"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headerText="Qty. Received" Visible="False">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox BorderStyle="Solid" BorderWidth="2" BorderColor="#440000" MaxLength="3" ID="txtorigQR" text='<%# Container.DataItem("Received_Qty") %>' Runat="server" Width="40" EnableViewState="false">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></td>
							<tr>
								<td noWrap align="right" width="100%"><asp:button id="btnUpdate" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" BackColor="#440000" Runat="server" BorderColor="#990000" Font-Bold="True" Text="Update Items"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
