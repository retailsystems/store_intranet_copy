Public Class WebForm_TransferHome
    Inherits System.Web.UI.Page

    Protected WithEvents layoutHeader As Header
    Protected WithEvents TabStripv As Microsoft.Web.UI.WebControls.TabStrip
    Protected WithEvents TabStripView As Microsoft.Web.UI.WebControls.MultiPage
    Protected WithEvents ucSearchBox As FilterBox
    Protected WithEvents grdArrivals As DataGrid
    Protected WithEvents grdShipped As DataGrid
    Protected WithEvents grdSavedTransfers As DataGrid
    Protected WithEvents grdPrintQueue As DataGrid
    Protected WithEvents tsGrids As Microsoft.Web.UI.WebControls.TabStrip
    Protected WithEvents dgSavedCheckIns As DataGrid

    Private objUserInfo As New UserInfo()
    Private bytSortArrival As Byte = 0
    Private bytSortShipped As Byte = 0
    Private bytSortSavedTransfers As Byte = 0
    Private bytSortSavedCheckIns As Byte = 0
    Private bytSortPrintQueue As Byte = 0

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.SetInOut(Page.Request, Page.Response)
        layoutHeader.lblTitle = "Manage Store Transfers"
        layoutHeader.CurrentPage(Header.PageName.Home)

        If Not Page.IsPostBack Then

            If ViewState("Filter") = "" Then
                FillDataGrids(0, ViewState("Filter"))
            Else
                FillDataGrids(1, "")
            End If

        End If

    End Sub

    Sub SortCommand_Arrivals(ByVal Source As Object, ByVal E As DataGridSortCommandEventArgs)
        bytSortArrival = E.SortExpression
        If ViewState("Filter") <> "" Then
            FillDataGrids(1, ViewState("Filter"))
        Else
            FillDataGrids(0, "")
        End If

    End Sub

    Sub SortCommand_Shipped(ByVal Source As Object, ByVal E As DataGridSortCommandEventArgs)
        bytSortShipped = E.SortExpression
        If ViewState("Filter") <> "" Then
            FillDataGrids(1, ViewState("Filter"))
        Else
            FillDataGrids(0, "")
        End If
    End Sub

    Sub SortCommand_SavedTransfers(ByVal Source As Object, ByVal E As DataGridSortCommandEventArgs)
        bytSortSavedTransfers = E.SortExpression
        If ViewState("Filter") <> "" Then
            FillDataGrids(1, ViewState("Filter"))
        Else
            FillDataGrids(0, "")
        End If
    End Sub

    Sub SortCommand_SavedCheckIns(ByVal Source As Object, ByVal E As DataGridSortCommandEventArgs)
        bytSortSavedCheckIns = E.SortExpression
        If ViewState("Filter") <> "" Then
            FillDataGrids(1, ViewState("Filter"))
        Else
            FillDataGrids(0, "")
        End If
    End Sub

    Sub SortCommand_PrintQueue(ByVal Source As Object, ByVal E As DataGridSortCommandEventArgs)
        bytSortPrintQueue = E.SortExpression
        If ViewState("Filter") <> "" Then
            FillDataGrids(1, ViewState("Filter"))
        Else
            FillDataGrids(0, "")
        End If
    End Sub

    Private Sub FillDataGrids(ByVal booFilterOn As Boolean, ByVal strFilterString As String)

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim bc As HttpBrowserCapabilities
        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

        bc = Request.Browser

        objConnection.Open()

        If (bc.Version < 6 And tsGrids.SelectedIndex = 0) Or (bc.Version >= 6) Then

            If booFilterOn Then
                strStoredProcedure = "spGetFilterArrivals " & objUserInfo.StoreNumber & ",'" & strFilterString & "'," & bytSortArrival
            Else
                strStoredProcedure = "spGetArrivals " & objUserInfo.StoreNumber & "," & bytSortArrival
            End If

            objCommand.CommandText = strStoredProcedure

            objDataReader = objCommand.ExecuteReader()

            grdArrivals.DataSource = objDataReader
            grdArrivals.DataBind()
            objCommand.Dispose()
            objDataReader.Close()
        End If

        If (bc.Version < 6 And tsGrids.SelectedIndex = 1) Or (bc.Version >= 6) Then

            If booFilterOn Then
                strStoredProcedure = "spGetFilterShipped " & objUserInfo.StoreNumber & ",'" & strFilterString & "'," & bytSortShipped
            Else
                strStoredProcedure = "spGetShipped " & objUserInfo.StoreNumber & "," & bytSortShipped
            End If

            objCommand.CommandText = strStoredProcedure

            objDataReader = objCommand.ExecuteReader()

            grdShipped.DataSource = objDataReader
            grdShipped.DataBind()
            objCommand.Dispose()
            objDataReader.Close()
        End If

        If (bc.Version < 6 And tsGrids.SelectedIndex = 2) Or (bc.Version >= 6) Then

            If booFilterOn Then
                strStoredProcedure = "spGetFilterSavedTrans " & objUserInfo.StoreNumber & ",'" & strFilterString & "'," & bytSortSavedTransfers
            Else
                strStoredProcedure = "spGetSavedTransfers " & objUserInfo.StoreNumber & "," & bytSortSavedTransfers
            End If

            objCommand.CommandText = strStoredProcedure
            objDataReader = objCommand.ExecuteReader()

            grdSavedTransfers.DataSource = objDataReader
            grdSavedTransfers.DataBind()
            objCommand.Dispose()
            objDataReader.Close()
        End If

        If (bc.Version < 6 And tsGrids.SelectedIndex = 3) Or (bc.Version >= 6) Then

            If booFilterOn Then
                strStoredProcedure = "spGetFilterSavedCheckIn " & objUserInfo.StoreNumber & ",'" & strFilterString & "'," & bytSortSavedCheckIns
            Else
                strStoredProcedure = "spGetSavedCheckIn " & objUserInfo.StoreNumber & "," & bytSortSavedCheckIns
            End If

            objCommand.CommandText = strStoredProcedure
            objDataReader = objCommand.ExecuteReader()

            dgSavedCheckIns.DataSource = objDataReader
            dgSavedCheckIns.DataBind()
            objCommand.Dispose()
            objDataReader.Close()
        End If

        If (bc.Version < 6 And tsGrids.SelectedIndex = 4) Or (bc.Version >= 6) Then

            If booFilterOn Then
                strStoredProcedure = "spGetFilterBoxPrinted " & objUserInfo.StoreNumber & ",'" & strFilterString & "'," & bytSortPrintQueue
            Else
                strStoredProcedure = "spGetBoxPrinted " & objUserInfo.StoreNumber & "," & bytSortPrintQueue
            End If

            objCommand.CommandText = strStoredProcedure
            objDataReader = objCommand.ExecuteReader()

            grdPrintQueue.DataSource = objDataReader
            grdPrintQueue.DataBind()
        End If

        objConnection.Close()

    End Sub


    Protected Function GetBoxTransferTypes(ByVal Box_Id As String) As String

        'Dim strStoredProcedure As String
        'Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        'Dim strReturn As String

        'objConnection.Open()

        'strStoredProcedure = "spGetBoxTransferTypes '" & Box_Id & "' "

        'Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        'Dim objDataReader As SqlClient.SqlDataReader

        'objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

        'Do While objDataReader.Read
        'strReturn = strReturn & objDataReader("Xfer_Short_Desc") & ","
        'Loop

        'If Len(strReturn) > 0 Then
        'strReturn = Mid(strReturn, 1, Len(strReturn) - 1)
        'Else
        'strReturn = ""
        'End If

        'Return strReturn
        Return "test"
    End Function

    Sub ucSearchBox_OnFilterOff() Handles ucSearchBox.OnFilterOff


        FillDataGrids(0, "")
        ViewState("Filter") = ""

    End Sub

    Sub ucSearchBox_OnFilterOn(ByVal FilterString As String) Handles ucSearchBox.OnFilterOn

        FillDataGrids(1, FilterString)
        ViewState("Filter") = FilterString

    End Sub

    Protected Function GetImageURL(ByVal strFileName As String) As String
        If strFileName = "" Then
            Return "images\blank.gif"
        Else
            Return "images\printed.gif"
        End If
    End Function

    Sub ucSearchBox_OnNewTransfer() Handles ucSearchBox.OnNewTransfer
        Response.Redirect("SignIn.aspx?TTypeOn=1&Page=NewTransferForm.aspx")
    End Sub

    Sub ucSearchBox_OnCheckIn() Handles ucSearchBox.OnCheckIn
        Response.Redirect("SignIn.aspx?TTypeOn=0&Page=SelectTransfer.aspx")
    End Sub

    Private Sub tsGrids_SelectedIndexChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsGrids.SelectedIndexChange

        Dim bc As HttpBrowserCapabilities

        bc = Request.Browser

        If bc.Version < 6 Then
            FillDataGrids(0, "")
        End If

    End Sub

End Class
