Imports System.Data
Imports System.Data.SqlClient

Public MustInherit Class Transfers

    Inherits System.Web.UI.UserControl
    Protected WithEvents grdExpectedArrivals As System.Web.UI.WebControls.DataGrid
    Private Display As TransferGridType
    Private HrefLink As String
    Private Store As String
    Private FilterText As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
    Protected WithEvents lblGridTitle As System.Web.UI.WebControls.Label

#End Region

    Public Enum TransferGridType
        Expected = 1
        Shipped = 2
        Arrivals = 5
    End Enum

    Protected Function FillTransferDropDown(ByVal Box_Id As Int32) As SqlClient.SqlDataReader

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        strStoredProcedure = "GetBoxTransferTypes " & Box_Id

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

        FillTransferDropDown = objDataReader

    End Function

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        
        If Not Page.IsPostBack Then

            FillDataGrid()

        ElseIf Not ViewState("FilterText") Is Nothing Then

            FilterText = ViewState("FilterText")

        End If

    End Sub

    Public Sub FillDataGrid()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Store = "0001"

        Select Case Display
            Case TransferGridType.Expected
                grdExpectedArrivals.Columns(3).Visible = False

                strStoredProcedure = GetStoredProcedure()

                Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
                Dim objDataReader As SqlClient.SqlDataReader

                objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

                grdExpectedArrivals.DataSource = objDataReader
                grdExpectedArrivals.DataBind()

            Case TransferGridType.Shipped
                grdExpectedArrivals.Columns(2).Visible = False

                strStoredProcedure = "GetShipped '" & Store & "' "

                Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
                Dim objDataReader As SqlClient.SqlDataReader

                objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

                grdExpectedArrivals.DataSource = objDataReader
                grdExpectedArrivals.DataBind()

            Case TransferGridType.Arrivals

                strStoredProcedure = "spGetArrivals '" & Store & "' "

                Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
                Dim objDataReader As SqlClient.SqlDataReader

                objDataReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

                grdExpectedArrivals.DataSource = objDataReader
                grdExpectedArrivals.DataBind()

            Case Else

        End Select

        objConnection.Close()

    End Sub

    Private Function GetStoredProcedure() As String

        Dim Box_Id As Int32
        Dim Shipment_Date As String

        If FilterText Is Nothing Then
            Return "spExpectedArrivals '" & Store & "' "
        Else
            If FilterText.Length > 0 Then
                If IsDate(FilterText) Then
                    Shipment_Date = FilterText
                End If

                If IsNumeric(FilterText) Then
                    Box_Id = FilterText
                End If

                Return "spGetFilterArrivals '" & Store & "','" & FilterText & "','" & FilterText & "'," & Box_Id & ",'" & Shipment_Date & "','" & FilterText & "','" & FilterText & "' "
            Else
                Return "spExpectedArrivals '" & Store & "' "
            End If
        End If

    End Function


    Public Property SetStore()
        Get
            Return Store
        End Get
        Set(ByVal Value)
            Store = Value
        End Set
    End Property

    WriteOnly Property SetFilterText() As String
        Set(ByVal Value As String)
            FilterText = Value
        End Set
    End Property

    Protected Sub SortRows(ByVal objSender As Object, ByVal objArgs As DataGridSortCommandEventArgs)

        Dim strSortOrder As String

        strSortOrder = objArgs.SortExpression.ToString

    End Sub

    Public Property SetGridType() As TransferGridType
        Get
            SetGridType = Display
        End Get
        Set(ByVal Value As TransferGridType)
            Display = Value
        End Set
    End Property

End Class
