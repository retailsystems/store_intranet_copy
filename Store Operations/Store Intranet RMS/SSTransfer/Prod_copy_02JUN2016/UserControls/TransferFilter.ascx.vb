Public MustInherit Class TransferFilter
    Inherits System.Web.UI.UserControl
    Protected WithEvents chkShipped As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkAnalyst As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkUnpacking As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkCustomer As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkReceived As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkStore As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkRecorded As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkBuyer As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnLastWeek As System.Web.UI.WebControls.Button
    Protected WithEvents btnLastMonth As System.Web.UI.WebControls.Button
    Protected WithEvents btnLastSixMonth As System.Web.UI.WebControls.Button
    Protected WithEvents chkCTB As System.Web.UI.WebControls.CheckBox

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

End Class
