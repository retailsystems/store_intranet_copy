Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Public MustInherit Class TransferHeader
    Inherits System.Web.UI.UserControl

    Protected WithEvents lblTransferId_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblTransferId_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblUPS_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblUPS_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblStatus_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblStatus_Value As System.Web.UI.WebControls.Label
    Protected WithEvents ddlStatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblEmp_name As System.Web.UI.WebControls.Label
    Protected WithEvents lblEmp_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblSStore_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblSStore_Value As System.Web.UI.WebControls.Label
    Protected WithEvents ddlSStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblRStore_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblRStore_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblShipDate_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblShipDate_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblBoxIncludes_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lstBoxIncludes As System.Web.UI.WebControls.ListBox
    Protected WithEvents hlUPS As System.Web.UI.WebControls.HyperLink
    Protected WithEvents btnHistory As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cblBoxIncludes As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents lblDisQuant_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblDisQuant_Value As System.Web.UI.WebControls.Label
    Protected WithEvents lblDisAmount_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblDisAmount_Value As System.Web.UI.WebControls.Label
    Protected WithEvents trDiscrepancy As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblInk_Value As System.Web.UI.WebControls.Label
    Protected WithEvents cvBoxIncludes As System.Web.UI.WebControls.CustomValidator
    Protected WithEvents lblNotes As System.Web.UI.WebControls.Label
    Protected WithEvents txtNotes As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblBoxIncludes As System.Web.UI.WebControls.Label
    Protected WithEvents lblSensor As System.Web.UI.WebControls.Label
    Protected WithEvents txtRStore As System.Web.UI.WebControls.TextBox
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblRANum_Name As System.Web.UI.WebControls.Label
    Protected WithEvents lblRANum_Value As System.Web.UI.WebControls.Label
    Protected WithEvents ddlRStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents phFocusRStore As System.Web.UI.WebControls.PlaceHolder

    Private strBoxId As String
    Private strUPS As String
    Private strEmpId As String
    Private strEmpFullName As String
    Private intSStore As Int32
    Private intRStore As Int32
    Private bytBoxStatus As Byte
    Private strShipmentDate As String
    Private strReceivedDate As String
    Private decDisAmount As Decimal
    Private strNotes As String
    Private strRANum As String
    Private intDisQuant As Int32
    Protected WithEvents btnStoreChange As System.Web.UI.WebControls.Button

    Private tinyTType As Byte = 0
    Public Event OnReceivedRStore()


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))

        'capture return key for tracking number input
        txtRStore.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){document.forms[0].ucTransferHeader_btnStoreChange.click()};")

        If Page.IsPostBack Then
            GetState()
            GetControlSelection()
        End If

        If Not IsNumeric(txtRStore.Text) Then
            phFocusRStore.Visible = txtRStore.Visible
        End If

        FillBoxIncludes()

    End Sub

    Public Sub GetData()
        GetState()
        GetControlSelection()
    End Sub

    Public Sub LoadData()

        trDiscrepancy.Visible = False
        btnHistory.Attributes.Add("onclick", "javascript:window.showModalDialog('EmpHistory.aspx?BID=" & strBoxId & "&nocache=" & Now.ToString & "','Status History','center=1;scroll=0;status=no;dialogWidth=400px;dialogHeight=380px;');")


        If Not strBoxId Is Nothing Then GetHeaderData()

        'If Not Page.IsPostBack Then
        'FillBoxIncludes()
        'End If

        If tinyTType = 2 Then

            'txtRStore.Visible = False
            'ddlRStore.Visible = True

            FillStoreDDL(ddlRStore, intRStore, intSStore)

            If Len(Trim(strBoxId)) = 0 Then ddlRStore.SelectedIndex = ddlRStore.Items.IndexOf(ddlRStore.Items.FindByValue(GetDefaultReceivingStore(intSStore, tinyTType)))
        ElseIf tinyTType = 3 Then
            If Len(Trim(strBoxId)) = 0 Then txtRStore.Text = GetDefaultReceivingStore(intSStore, tinyTType)
            txtRStore.ReadOnly = True

            ' txtRStore.Visible = True
            ' ddlRStore.Visible = False

        ElseIf tinyTType = 4 Then
            If Len(Trim(strBoxId)) = 0 Then txtRStore.Text = GetDefaultReceivingStore(intSStore, tinyTType)
            txtRStore.ReadOnly = True

            ' txtRStore.Visible = True
            ' ddlRStore.Visible = False

        ElseIf tinyTType = 1 Then
            If Len(Trim(strBoxId)) = 0 Then txtRStore.Text = GetDefaultReceivingStore(intSStore, tinyTType)

            ' txtRStore.Visible = True
            ' ddlRStore.Visible = False

        End If

        If ddlStatus.Visible Then
            FillBoxStatus()
        End If

        SaveState()

    End Sub

    Property TType()
        Set(ByVal Value)
            tinyTType = Value
        End Set
        Get
            Return tinyTType
        End Get
    End Property

    WriteOnly Property ReceivedDate()
        Set(ByVal Value)
            strReceivedDate = Value
        End Set
    End Property

    'Public Sub ServerValidation(ByVal source As Object, ByVal args As ServerValidateEventArgs)

    'If (ddlRStore.SelectedIndex = 0) Then
    'args.IsValid = False
    'Else
    'args.IsValid = True
    'End If

    'End Sub

    'Return GERS Status
    Public Function GetGERSAutoStatus()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetGERSAutoStatus", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parBoxId As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)
        Dim parGERSStatus As SqlClient.SqlParameter = objCommand.Parameters.Add("@GERSStatus", SqlDbType.Bit)

        parBoxId.Direction = ParameterDirection.Input
        parGERSStatus.Direction = ParameterDirection.Output

        parBoxId.Value = strBoxId

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return parGERSStatus.Value

    End Function

    Public Function IsBoxIncludesEmpty() As Boolean

        Dim i As Integer
        Dim itemselected As Boolean = False

        For i = 0 To cblBoxIncludes.Items.Count - 1

            If cblBoxIncludes.Items(i).Selected Then
                itemselected = True
                Exit For
            End If
        Next

        Return Not itemselected

    End Function

    Public Function IsMerchandiseSeleted() As Boolean

        Dim i As Integer

        For i = 0 To cblBoxIncludes.Items.Count - 1
            If cblBoxIncludes.Items(i).Value = 6 Then
                If cblBoxIncludes.Items(i).Selected Then
                    Return True
                Else
                    Return False
                End If
                Exit For
            End If
        Next

    End Function

    Public Function GetBoxIncludesLBS() As Decimal

        Dim i As Integer
        Dim strSQL As String
        Dim tmpDecimal As Decimal = 0.0

        For i = 0 To cblBoxIncludes.Items.Count - 1

            If cblBoxIncludes.Items(i).Selected Then
                strSQL = strSQL & cblBoxIncludes.Items(i).Value & ","
            End If

        Next

        If Len(strSQL) > 0 Then

            Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            Dim objDataReader As SqlClient.SqlDataReader

            strSQL = Mid(strSQL, 1, Len(strSQL) - 1)
            strSQL = "SELECT Sum(Item_LBS) TotalLBS FROM Box_Includes_Type WHERE Box_Includes_Cd In (" & strSQL & ")"
            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strSQL, objConnection)

            objDataReader = objCommand.ExecuteReader()

            If objDataReader.Read() Then
                tmpDecimal = objDataReader("TotalLBS")
            End If

            objCommand.Dispose()
            objConnection.Close()
            objDataReader.Close()
            objConnection.Dispose()

        End If

        Return tmpDecimal

    End Function

    Public Sub BoxIncludesValidation(ByVal source As Object, ByVal args As ServerValidateEventArgs)

        If (cblBoxIncludes.SelectedIndex = -1) Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If

    End Sub


    Private Sub GetControlSelection()

        If ddlRStore.Visible Then intRStore = ddlRStore.SelectedItem.Value
        If ddlSStore.Visible Then intSStore = ddlSStore.SelectedItem.Value
        If txtRStore.Visible And IsNumeric(txtRStore.Text) Then
            intRStore = txtRStore.Text
        ElseIf txtRStore.Visible And Not IsNumeric(txtRStore.Text) Then
            intRStore = -1
        End If

        strNotes = txtNotes.Text

    End Sub


    Private Sub GetState()

        If strBoxId Is Nothing Then strBoxId = ViewState("BH_BoxId")
        If strNotes Is Nothing Then strNotes = ViewState("BH_Notes")
        If strUPS Is Nothing Then strUPS = ViewState("BH_UPS")
        If strEmpId Is Nothing Then strEmpId = ViewState("BH_EmpId")
        If intSStore = 0 Then intSStore = ViewState("BH_SStore")
        If intRStore = 0 Then intRStore = ViewState("BH_RStore")
        If bytBoxStatus = 0 Then bytBoxStatus = ViewState("BH_BoxStatus")
        If strShipmentDate Is Nothing Then strShipmentDate = ViewState("BH_ShipDate")
        If strEmpFullName Is Nothing Then strEmpFullName = ViewState("BH_EmpFullName")
        If strRANum Is Nothing Then strRANum = ViewState("BH_RANum")
        If tinyTType = 0 Then tinyTType = ViewState("BH_tinyTType")

    End Sub

    Public Sub DisplayDiscrepancy()

        lblDisAmount_Value.Text = FormatCurrency(decDisAmount, 2)
        lblDisQuant_Value.Text = intDisQuant

        trDiscrepancy.Visible = True

    End Sub

    Private Sub SaveState()

        ViewState("BH_BoxId") = strBoxId
        ViewState("BH_Notes") = strNotes
        ViewState("BH_UPS") = strUPS
        ViewState("BH_EmpId") = strEmpId
        ViewState("BH_SStore") = intSStore
        ViewState("BH_RStore") = intRStore
        ViewState("BH_BoxStatus") = bytBoxStatus
        ViewState("BH_ShipDate") = strShipmentDate
        ViewState("BH_EmpFullName") = strEmpFullName
        ViewState("BH_RANum") = strRANum
        ViewState("BH_tinyTType") = tinyTType

    End Sub

    Private Sub GetHeaderData()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHeader", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar)

        BoxIdParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = strBoxId

        objDataReader = objCommand.ExecuteReader()

        If objDataReader.Read() Then

            If Not IsDBNull(objDataReader("Notes")) Then

                If Len(txtNotes.Text) > 0 Then
                    strNotes = txtNotes.Text
                Else
                    txtNotes.Text = objDataReader("Notes")
                    strNotes = objDataReader("Notes")
                End If

            End If

            If Not IsDBNull(objDataReader("Tracking_Num")) Then
                hlUPS.NavigateUrl = Replace(objDataReader("TrackingURL").ToString, "{0}", objDataReader("Tracking_Num").ToString)
            End If

            lblTransferId_Value.Text = objDataReader("Box_Id")

            If Not IsDBNull(objDataReader("Tracking_Num")) Then
                lblUPS_Value.Text = objDataReader("Tracking_Num")
                strUPS = objDataReader("Tracking_Num")
            End If

            lblStatus_Value.Text = objDataReader("Status_Desc")
            bytBoxStatus = objDataReader("Current_Box_Status_Cd")
            strEmpId = objDataReader("Emp_Id")

            If Not IsDBNull(objDataReader("Emp_FullName")) Then
                lblEmp_Value.Text = objDataReader("Emp_FullName")
            End If

            txtRStore.Text = Right("0000" & objDataReader("RStoreNum"), 4)
            lblSStore_Value.Text = Right("0000" & objDataReader("SStoreNum"), 4)
            intSStore = objDataReader("Sending_Store_Cd")

            If Not IsDBNull(objDataReader("RStore")) Then
                lblRStore_Value.Text = objDataReader("RStore")
                intRStore = objDataReader("Receiving_Store_Cd")
            End If

            GetInkTags(intRStore, intSStore)

            If Not IsDBNull(objDataReader("Shipment_Date")) Then
                lblShipDate_Value.Text = FormatDateTime(objDataReader("Shipment_Date"), DateFormat.ShortDate)
                strShipmentDate = objDataReader("Shipment_Date")
            End If

            If Not IsDBNull(objDataReader("DDS_RANum")) Then
                lblRANum_Value.Text = objDataReader("DDS_RANum")
                strRANum = objDataReader("DDS_RANum")
            End If

            tinyTType = objDataReader("TType")

        End If

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Function GetDefaultReceivingStore(ByVal SendingStore As Integer, ByVal TType As Int16) As String

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetDefaultReceivingStore", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim TTypeParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@TType", SqlDbType.TinyInt)
        Dim SStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@SendingStore", SqlDbType.SmallInt)
        Dim RStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt)

        TTypeParam.Direction = ParameterDirection.Input
        SStoreParam.Direction = ParameterDirection.Input
        RStoreParam.Direction = ParameterDirection.Output

        TTypeParam.Value = TType
        SStoreParam.Value = SendingStore

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return RStoreParam.Value.ToString

    End Function

    Private Sub GetInkTags(ByVal InRStore As Integer, ByVal InSStore As Integer)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetInkTags", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim RStoreIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Store_Cd", SqlDbType.SmallInt)
        Dim TagsParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Tags", SqlDbType.VarChar, 5)
        Dim SStoreIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Store_Sending_Cd", SqlDbType.SmallInt)
        Dim MisMatchParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Mismatch", SqlDbType.Bit)

        RStoreIdParam.Direction = ParameterDirection.Input
        TagsParam.Direction = ParameterDirection.Output
        SStoreIdParam.Direction = ParameterDirection.Input
        MisMatchParam.Direction = ParameterDirection.Output

        RStoreIdParam.Value = InRStore
        SStoreIdParam.Value = InSStore

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        lblInk_Value.Text = TagsParam.Value.ToString

        'If MisMatchParam.Value = 1 Then
        '    lblInk_Value.ForeColor = System.Drawing.Color.FromArgb(214, 12, 140) '#D60C8C
        '    lblSensor.ForeColor = System.Drawing.Color.FromArgb(214, 12, 140) '#D60C8C
        'Else
        '    lblInk_Value.ForeColor = System.Drawing.Color.Black
        '    lblSensor.ForeColor = System.Drawing.Color.Black
        'End If

    End Sub

    Private Sub FillBoxStatus()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxStatuses", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        objDataReader = objCommand.ExecuteReader()

        ddlStatus.DataSource = objDataReader
        ddlStatus.DataBind()

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub FillStoreDDL(ByRef StoreDDL As System.Web.UI.WebControls.DropDownList, ByVal intSelected As Int32, ByVal intSStore As Integer)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetStoreNumbers " & tinyTType & "," & intSStore, objConnection)

        objDataReader = objCommand.ExecuteReader()

        StoreDDL.DataSource = objDataReader
        StoreDDL.DataBind()

        objItem.Text = "Select A Store"
        objItem.Value = 0

        StoreDDL.Items.Insert(0, objItem)
        StoreDDL.SelectedIndex = StoreDDL.Items.IndexOf(StoreDDL.Items.FindByValue(intSelected.ToString))

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub GetReceivingStoreName()
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim storename As String = String.Empty
        Dim returnvalue As Integer

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetStoreName", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        objCommand.Parameters.Add("@StoreNum", intRStore)
        objCommand.Parameters.Add("@StoreName", SqlDbType.VarChar)

        objCommand.Parameters("@StoreName").Direction = ParameterDirection.Output
        objCommand.Parameters("@StoreName").Size = 40

        returnvalue = objCommand.ExecuteNonQuery()
        lblRStore_Value.Text = objCommand.Parameters("@StoreName").Value.ToString()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()
    End Sub

    Private Sub FillBoxIncludes()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim cblPreviousBoxIncludes As New CheckBoxList
        Dim I As Integer

        objConnection.Open()

        strStoredProcedure = "spGetBoxIncludes '" & strBoxId & "'," & tinyTType & "," & intRStore & "," & intSStore

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader()

        For I = 0 To cblBoxIncludes.Items.Count - 1
            cblPreviousBoxIncludes.Items.Add(cblBoxIncludes.Items(I))
        Next

        cblBoxIncludes.Items.Clear()

        While objDataReader.Read()

            Dim newListItem As New ListItem

            newListItem.Text = objDataReader("Includes_Desc")
            newListItem.Value = objDataReader("Box_Includes_Cd")

            If objDataReader("Checked") <> 0 Then
                newListItem.Selected = True
                lblBoxIncludes.Text = lblBoxIncludes.Text & "&nbsp;&nbsp;" & objDataReader("Includes_Desc") & "<br>"
            End If

            cblBoxIncludes.Items.Add(newListItem)

        End While

        For I = 0 To cblBoxIncludes.Items.Count - 1

            If Not cblPreviousBoxIncludes.Items.FindByValue(cblBoxIncludes.Items(I).Value) Is Nothing Then
                If cblPreviousBoxIncludes.Items.FindByValue(cblBoxIncludes.Items(I).Value).Selected Then
                    cblBoxIncludes.Items(I).Selected = True
                End If
            End If

        Next

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Public Function GetInValidBoxInclude() As String

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim I As Integer
        Dim strReturn As String

        objConnection.Open()

        strStoredProcedure = "spGetBoxIncludes_Exclude " & tinyTType & "," & RStore & "," & SStore

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader()

        While objDataReader.Read()

            If Not cblBoxIncludes.Items.FindByValue(objDataReader("Box_Includes_Cd")) Is Nothing Then
                If cblBoxIncludes.Items.FindByValue(objDataReader("Box_Includes_Cd")).Selected Then
                    strReturn = strReturn & objDataReader("Includes_Desc") & ","
                End If
            End If

        End While

        If Len(strReturn) > 0 Then
            strReturn = Mid(strReturn, 1, Len(strReturn) - 1)
        End If

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return strReturn

    End Function

    Public Sub CreateNewHeader(ByVal InSStore As Int32, ByVal InBoxStatus As Byte, ByVal InEmpId As String, ByVal InEmpName As String)

        'Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        'objConnection.Open()

        ' Dim objCommand As New SqlClient.SqlCommand("spGetStoreName", objConnection)
        ' objCommand.CommandType = CommandType.StoredProcedure

        ' Dim SStoreIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt)
        ' Dim SNameParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreName", SqlDbType.VarChar, 40)

        '  SStoreIdParam.Direction = ParameterDirection.Input
        ' SNameParam.Direction = ParameterDirection.Output
        '  SStoreIdParam.Value = InSStore

        'objCommand.ExecuteNonQuery()

        'objConnection.Close()

        lblStatus_Value.Text = "New Transfer"
        bytBoxStatus = InBoxStatus
        strEmpId = InEmpId
        lblEmp_Value.Text = InEmpName
        strEmpFullName = InEmpName
        lblSStore_Value.Text = InSStore
        intSStore = InSStore

    End Sub

    Public Sub DeleteHeader()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spDeleteBoxHeader '" & strBoxId & "' ", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Private Sub UpdateHeader()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateBoxHeader", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)
        Dim SStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Sending_Store_Cd", SqlDbType.SmallInt)
        Dim RStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Receiving_Store_Cd", SqlDbType.SmallInt)
        Dim StatusParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Current_Box_Status", SqlDbType.TinyInt)
        Dim EmpIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Emp_Id", SqlDbType.VarChar, 5)
        Dim EmpFullNameParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Emp_FullName", SqlDbType.VarChar, 65)
        Dim NotesParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Notes", SqlDbType.Text)
        Dim ShipDateParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Shipment_Date", SqlDbType.DateTime)
        Dim ReceivedDateParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Received_Date", SqlDbType.DateTime)
        Dim Modified_Date As SqlClient.SqlParameter = objCommand.Parameters.Add("@Modified_Date", SqlDbType.DateTime)
        Dim RANumParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@DDS_RANum", SqlDbType.VarChar, 25)

        BoxIdParam.Direction = ParameterDirection.Input
        SStoreParam.Direction = ParameterDirection.Input
        RStoreParam.Direction = ParameterDirection.Input
        StatusParam.Direction = ParameterDirection.Input
        EmpIdParam.Direction = ParameterDirection.Input
        EmpFullNameParam.Direction = ParameterDirection.Input
        ShipDateParam.Direction = ParameterDirection.Input
        ReceivedDateParam.Direction = ParameterDirection.Input
        Modified_Date.Direction = ParameterDirection.Input
        NotesParam.Direction = ParameterDirection.Input
        RANumParam.Direction = ParameterDirection.Input

        BoxIdParam.Value = strBoxId
        SStoreParam.Value = intSStore
        RStoreParam.Value = intRStore
        StatusParam.Value = bytBoxStatus
        EmpIdParam.Value = strEmpId
        NotesParam.Value = strNotes
        EmpFullNameParam.Value = strEmpFullName
        ShipDateParam.Value = strShipmentDate
        ReceivedDateParam.Value = strReceivedDate
        RANumParam.Value = strRANum
        Modified_Date.Value = Now()

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        UpdateBoxHistory()
        SaveBoxIncludes()

    End Sub

    Public Sub Save()

        If strBoxId Is Nothing Then
            SaveHeader()
        Else
            UpdateHeader()
        End If

    End Sub

    Private Sub SaveHeader()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spInsertBoxHeader", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim SStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@SendingStore", SqlDbType.SmallInt)
        Dim RStoreParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@ReceivingStore", SqlDbType.SmallInt)
        Dim StatusParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Status", SqlDbType.TinyInt)
        Dim EmpIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Emp_Id", SqlDbType.VarChar, 5)
        Dim EmpFullNameParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Emp_FullName", SqlDbType.VarChar, 65)
        Dim ShipDateParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Shipment_Date", SqlDbType.DateTime)
        Dim CreateDateParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Created_Date", SqlDbType.DateTime)
        Dim BoxIdParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Box_Id", SqlDbType.VarChar, 14)
        Dim TTypeParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@TType", SqlDbType.TinyInt)
        Dim NotesParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@Notes", SqlDbType.Text)
        Dim RANumParam As SqlClient.SqlParameter = objCommand.Parameters.Add("@DDS_RANum", SqlDbType.VarChar, 25)


        SStoreParam.Direction = ParameterDirection.Input
        RStoreParam.Direction = ParameterDirection.Input
        StatusParam.Direction = ParameterDirection.Input
        EmpIdParam.Direction = ParameterDirection.Input
        EmpFullNameParam.Direction = ParameterDirection.Input
        ShipDateParam.Direction = ParameterDirection.Input
        CreateDateParam.Direction = ParameterDirection.Input
        BoxIdParam.Direction = ParameterDirection.Output
        TTypeParam.Direction = ParameterDirection.Input
        NotesParam.Direction = ParameterDirection.Input
        RANumParam.Direction = ParameterDirection.Input


        SStoreParam.Value = intSStore
        RStoreParam.Value = intRStore
        StatusParam.Value = bytBoxStatus
        EmpIdParam.Value = strEmpId
        EmpFullNameParam.Value = strEmpFullName
        ShipDateParam.Value = strShipmentDate
        TTypeParam.Value = tinyTType
        NotesParam.Value = strNotes
        CreateDateParam.Value = Now()
        RANumParam.Value = strRANum

        BoxIdParam.Direction = ParameterDirection.Output

        objCommand.ExecuteNonQuery()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()
        strBoxId = BoxIdParam.Value

        UpdateBoxHistory()
        SaveBoxIncludes()

    End Sub

    Private Sub SaveBoxIncludes()

        Dim i As Integer
        Dim strInsert As String
        Dim strSQL As String

        For i = 0 To cblBoxIncludes.Items.Count - 1
            If cblBoxIncludes.Items(i).Selected Then
                strInsert = strInsert & cblBoxIncludes.Items(i).Value & ","
            End If
        Next

        If Not strInsert = Nothing Then
            strInsert = Mid(strInsert, 1, strInsert.Length - 1)

            strSQL = "DELETE FROM Box_To_Include WHERE Box_Id = '" & strBoxId & "'; "
            strSQL = strSQL & "INSERT INTO Box_To_Include(Box_Id,Box_Includes_Cd) "
            strSQL = strSQL & "SELECT '" & strBoxId & "',Box_Includes_Cd FROM BOX_Includes_Type Where Box_Includes_Cd In (" & strInsert & ") "

            Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand(strSQL, objConnection)

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()
            objConnection.Dispose()
        End If

    End Sub

    Private Sub UpdateBoxHistory()

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spUpdateBoxHistory '" & strBoxId & "'," & bytBoxStatus & ",'" & strEmpId & "','" & Replace(strEmpFullName, "'", "''") & "','" & Now() & "' ", objConnection)

        objDataReader = objCommand.ExecuteReader()

        ddlStatus.DataSource = objDataReader
        ddlStatus.DataBind()

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

    Public Sub ClearRStore()

        txtRStore.Text = ""
        lblRStore_Value.Text = ""
        ddlRStore.SelectedIndex = 0
        intRStore = 0

    End Sub

    WriteOnly Property BoxIncludesVisible()
        Set(ByVal Value)
            If Value Then
                cblBoxIncludes.Visible = True
                lblBoxIncludes.Visible = False
            Else
                cblBoxIncludes.Visible = False
                lblBoxIncludes.Visible = True
            End If
        End Set
    End Property

    WriteOnly Property DisableNotes()
        Set(ByVal Value)
            txtNotes.ReadOnly = Value
            'txtNotes.ForeColor = Color.Black
            'txtNotes.BackColor = Color.White
        End Set
    End Property

    Property BoxStatus()
        Get
            Return bytBoxStatus
        End Get
        Set(ByVal Value)
            bytBoxStatus = Value
        End Set
    End Property

    WriteOnly Property EnableBoxIncludes()
        Set(ByVal Value)
            cblBoxIncludes.Enabled = Value
        End Set
    End Property

    WriteOnly Property EnableRStore()
        Set(ByVal Value)
            txtRStore.Enabled = Value
            ddlRStore.Enabled = Value
        End Set
    End Property



    Property ShipmentDate()
        Get
            Return strShipmentDate
        End Get
        Set(ByVal Value)
            strShipmentDate = Value
        End Set
    End Property

    Property DiscrepancyAmount()
        Get
            Return decDisAmount
        End Get
        Set(ByVal Value)
            decDisAmount = Value
        End Set
    End Property

    Property DiscrepancyQuantity()
        Get
            Return intDisQuant
        End Get
        Set(ByVal Value)
            intDisQuant = Value
        End Set
    End Property

    Property SStore()
        Get
            Return intSStore
        End Get
        Set(ByVal Value)
            intSStore = Value
        End Set
    End Property

    Property EmpId()
        Get
            Return strEmpId
        End Get
        Set(ByVal Value)
            strEmpId = Value
        End Set
    End Property

    Property EmpFullName()
        Get
            Return strEmpFullName
        End Get
        Set(ByVal Value)
            strEmpFullName = Value
        End Set
    End Property

    Property BoxId()
        Get
            Return strBoxId
        End Get
        Set(ByVal Value)
            strBoxId = Value
        End Set
    End Property

    Property RStore()
        Get
            Return intRStore
        End Get
        Set(ByVal Value)
            intRStore = Value
        End Set
    End Property

    Property RANum()
        Get
            Return strRANum
        End Get
        Set(ByVal Value)
            strRANum = Value
        End Set
    End Property

    WriteOnly Property ShowUPSLink()
        Set(ByVal Value)
            lblUPS_Value.Visible = True
            hlUPS.Visible = True
        End Set
    End Property

    WriteOnly Property ShowDDLRStore()
        Set(ByVal Value)
            ddlRStore.Visible = Value
        End Set
    End Property

    WriteOnly Property ShowRStore()
        Set(ByVal Value)
            txtRStore.Visible = Value
            lblRStore_Value.Visible = Not Value
        End Set
    End Property

    'WriteOnly Property EditDDLRStore()
    '    Set(ByVal Value)
    '        If Value = True Then
    '            lblRStore_Value.Visible = False
    '            ddlRStore.Visible = True
    '            txtRStore.Visible = False
    '        End If
    '    End Set
    'End Property

    'WriteOnly Property EditRStore()
    '    Set(ByVal Value)
    '        If Value = True Then
    '            lblRStore_Value.Visible = False
    '            txtRStore.Visible = True
    '            ddlRStore.Visible = False
    '        End If
    '    End Set
    'End Property

    'WriteOnly Property EditBoxIncludes()
    '    Set(ByVal Value)
    '        If Value = True Then
    '            lstBoxIncludes.Enabled = True
    '        End If
    '    End Set
    'End Property

    'Private Sub ddlRStore_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlRStore.SelectedIndexChanged
    'GetInkTags(ddlRStore.SelectedItem.Value, intSStore)
    'FillBoxIncludes()
    'End Sub

    Private Sub btnStoreChange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStoreChange.Click
        GetReceivingStoreName()
        FillBoxIncludes()
        RaiseEvent OnReceivedRStore()
    End Sub

    Private Sub ddlRStore_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlRStore.SelectedIndexChanged
        FillBoxIncludes()
        RaiseEvent OnReceivedRStore()
    End Sub

End Class
