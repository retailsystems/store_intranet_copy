<%@ Control Language="vb" AutoEventWireup="false" Codebehind="TransferGrid.ascx.vb" Inherits="SSTransfer.Transfers" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table cellSpacing="0" cellPadding="2" width="100%" border="0">
	<tr>
		<td>
			<asp:datagrid AllowSorting="True" OnSortCommand="SortRows" id="grdExpectedArrivals" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="White" CellSpacing="1" CellPadding="2" Width="100%" EnableViewState="False" BorderWidth="0" AutoGenerateColumns="False" BackColor="#111111">
				<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
				<Columns>
					<asp:HyperLinkColumn SortExpression="Shipment_Date" DataTextFormatString="{0:d}" DataTextField="Shipment_Date" HeaderText="Ship Date" DataNavigateUrlField="URL">
						<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
					</asp:HyperLinkColumn>
					<asp:TemplateColumn HeaderStyle-Width="120" HeaderText="Type">
						<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
						<ItemTemplate>
							<asp:DropDownList Width="100%" ID="ddlTransferType" DataTextField="Xfer_Desc" DataSource='<%# FillTransferDropDown(Container.dataitem("Box_Id")) %>' runat="Server">
							</asp:DropDownList>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn SortExpression="Sending_Store_Name" DataField="Sending_Store_Name" HeaderText="Sent Form">
						<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn SortExpression="Receiving_Store_Name" DataField="Receiving_Store_Name" HeaderText="Sent To">
						<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn SortExpression="UPS_Tracking_Num" DataField="UPS_Tracking_Num" HeaderText="UPS #">
						<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn SortExpression="Box_Id" DataField="Box_Id" HeaderText="Transfer #">
						<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn SortExpression="Status_Desc" DataField="Status_Desc" HeaderText="Status">
						<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
					</asp:BoundColumn>
				</Columns>
			</asp:datagrid>
		</td>
	</tr>
</table>
