Public MustInherit Class Navigation
    Inherits System.Web.UI.UserControl
    Protected WithEvents lbHome As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbNewTrans As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbCheckIn As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdHome As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdNewTrans As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdCheckIn As System.Web.UI.HtmlControls.HtmlTableCell

    Private strHome As String
    Private strTrnas As String
    Private strCheck As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case Request("Nav")
            Case 1
                tdHome.BgColor = "#990000"
  
            Case 2

                tdNewTrans.BgColor = "#990000"

            Case 3

                tdCheckIn.BgColor = "#990000"
            Case Else
                tdHome.BgColor = "#990000"

        End Select
    End Sub

    Private Sub lbHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbHome.Click
        Response.Redirect("TransferHome.aspx?Nav=1")
    End Sub

    Private Sub lbNewTrans_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbNewTrans.Click
        Response.Redirect("SignIn.aspx?TTypeOn=1&Page=NewTransferForm.aspx&Nav=2")
    End Sub

    Private Sub lbCheckIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbCheckIn.Click
        Response.Redirect("SignIn.aspx?TTypeOn=0&Page=SelectTransfer.aspx&Nav=3")
    End Sub
End Class
