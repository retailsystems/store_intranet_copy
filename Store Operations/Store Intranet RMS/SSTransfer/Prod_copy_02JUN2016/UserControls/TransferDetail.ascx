<%@ Control Language="vb" AutoEventWireup="false" Codebehind="TransferDetail.ascx.vb" Inherits="SSTransfer.TransferDetail" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<TABLE style="WIDTH: 984px; HEIGHT: 134px" cellSpacing="0" cellPadding="0" width="984" bgColor="#000000" border="0">
	<tr>
		<td style="WIDTH: 15.09%; HEIGHT: 34px" align="left"><FONT color="#ffffff">
				<P align="left"><asp:label id="lblBoxID" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">Transfer ID</asp:label></P>
			</FONT>
		</td>
		<TD style="WIDTH: 15%; HEIGHT: 34px"><asp:label id="labTxtBoxId" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">txtBoxID</asp:label><asp:textbox id="txtBoxId" runat="server" Width="79px"></asp:textbox></TD>
		<TD style="WIDTH: 15%; HEIGHT: 34px">
			<P align="left"><asp:label id="lblUPSTrackingNum" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server" Width="138px">UPS Number:</asp:label></P>
		</TD>
		<td style="WIDTH: 40%; HEIGHT: 34px"><FONT><asp:label id="labTxtUPSNumber" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">txtUPSNumber</asp:label><asp:textbox id="txtUPSNumber" runat="server" Width="136px"></asp:textbox></FONT></td>
		<td style="WIDTH: 137px; HEIGHT: 115px" align="middle" rowSpan="4"><asp:label id="lblBoxItems" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">Box Includes:</asp:label><asp:listbox id="listBoxIncludes" runat="server" Width="157px" Height="74px" SelectionMode="Multiple" BackColor="White"></asp:listbox></td>
	</tr>
	<tr>
		<td style="WIDTH: 15.09%; HEIGHT: 29px" align="left"><FONT color="#ffffff">
				<P align="left"><asp:label id="lblStatus" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">Status:</asp:label></P>
			</FONT>
		</td>
		<td style="WIDTH: 15%; HEIGHT: 29px"><FONT color="#ffffff"><asp:label id="labTxtStatus" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">txtStatus</asp:label><asp:textbox id="txtStatus" runat="server" Width="138px"></asp:textbox></FONT></td>
		<td style="WIDTH: 15%; HEIGHT: 29px" align="left">
			<P align="left"><asp:label id="lblSendingEmp" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server" Width="159px">Employee:</asp:label></P>
		</td>
		<td style="WIDTH: 40%; HEIGHT: 29px"><FONT color="#ffffff"><asp:label id="labTxtSendingEmp" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">txtSendingEmp</asp:label><asp:textbox id="txtSendingEmp" runat="server" Width="244px"></asp:textbox></FONT></td>
	</tr>
	<tr>
		<td style="WIDTH: 15.09%; HEIGHT: 34px" align="left"><FONT color="#ffffff">
				<P align="left"><asp:label id="lblSending" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">Sending Store:</asp:label></P>
			</FONT>
		</td>
		<td style="WIDTH: 15%; HEIGHT: 34px"><FONT color="#ffffff"><asp:label id="labTxtSendingStore" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">txtSendingStore</asp:label><asp:textbox id="txtSendingStore" runat="server" Width="70px"></asp:textbox></FONT></td>
		<td style="WIDTH: 15%; HEIGHT: 34px" align="left">
			<P align="left"><asp:label id="lblRecevingStore" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">Receiving Store:</asp:label></P>
		</td>
		<td style="WIDTH: 40%; HEIGHT: 34px"><FONT color="#ffffff"><asp:label id="labTxtRecStore" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">txtReceivingStore</asp:label><asp:dropdownlist id="cboReceivingStore" runat="server" Width="249px"></asp:dropdownlist></FONT></td>
	</tr>
	<TR>
		<TD style="WIDTH: 14.87%; HEIGHT: 27px"><FONT color="#ffffff">
				<P align="left"><asp:label id="lblShipmentDate" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">Shipment Date:</asp:label></P>
			</FONT>
		</TD>
		<TD style="WIDTH: 15%; HEIGHT: 27px"><asp:label id="labTxtShipmentDate" ForeColor="White" Font-Size="X-Small" Font-Names="Arial" runat="server">txtShipmentDate</asp:label><asp:textbox id="txtShipmentDate" runat="server" Width="138px"></asp:textbox></TD>
		<TD style="WIDTH: 15%; HEIGHT: 27px">
			<P align="left">&nbsp;</P>
		</TD>
		<TD style="WIDTH: 40%; HEIGHT: 27px"></TD>
	</TR>
</TABLE>
