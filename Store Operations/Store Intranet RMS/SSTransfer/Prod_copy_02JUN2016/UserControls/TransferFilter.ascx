<%@ Control Language="vb" AutoEventWireup="false" Codebehind="TransferFilter.ascx.vb" Inherits="SSTransfer.TransferFilter" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
		<td width="20%">&nbsp;</td>
		<td nowrap><font face="arial" color="red" size="x-small"><b>Show Transfers with a Status 
					of:</b></font>
		</td>
		<td nowrap><font face="arial" color="red" size="x-small"><b>Show Transfers of Type:</b></font>
		</td>
		<td width="5%">&nbsp;</td>
	</tr>
	<tr>
		<td width="20%">&nbsp;</td>
		<td nowrap><asp:checkbox id="chkShipped" ForeColor="#ffffff" Text="Shipped" Font-Size="X-Small" Font-Names="Arial" runat="server"></asp:checkbox></td>
		<td nowrap><asp:checkbox id="chkAnalyst" ForeColor="#ffffff" Text="Analyst Requested Store Transfer" Font-Size="X-Small" Font-Names="Arial" runat="server"></asp:checkbox></td>
		<td width="5%">&nbsp;</td>
	</tr>
	<tr>
		<td width="20%">&nbsp;</td>
		<td nowrap><asp:checkbox id="chkUnpacking" ForeColor="#ffffff" Text="Unpacking" Font-Size="X-Small" Font-Names="Arial" runat="server"></asp:checkbox></td>
		<td nowrap><asp:checkbox id="chkCustomer" ForeColor="#ffffff" Text="Customer Requesting Store Transfer" Font-Size="X-Small" Font-Names="Arial" runat="server"></asp:checkbox></td>
		<td width="5%">&nbsp;</td>
	</tr>
	<tr>
		<td width="20%">&nbsp;</td>
		<td nowrap><asp:checkbox id="chkReceived" ForeColor="#ffffff" Text="Received" Font-Size="X-Small" Font-Names="Arial" runat="server"></asp:checkbox></td>
		<td nowrap><asp:checkbox id="chkStore" ForeColor="#ffffff" Text="Store Initiated RTB" Font-Size="X-Small" Font-Names="Arial" runat="server"></asp:checkbox></td>
		<td width="5%">&nbsp;</td>
	</tr>
	<tr>
		<td width="20%">&nbsp;</td>
		<td nowrap><asp:checkbox id="chkRecorded" ForeColor="#ffffff" Text="Recorded" Font-Size="X-Small" Font-Names="Arial" runat="server"></asp:checkbox></td>
		<td nowrap><asp:checkbox id="chkBuyer" ForeColor="#ffffff" Text="Buyer Initiated" Font-Size="X-Small" Font-Names="Arial" runat="server"></asp:checkbox></td>
		<td width="5%">&nbsp;</td>
	</tr>
	<tr>
		<td width="20%">&nbsp;</td>
		<td nowrap>&nbsp;
		</td>
		<td nowrap><asp:checkbox id="chkCTB" ForeColor="#ffffff" Text="CTB" Font-Size="X-Small" Font-Names="Arial" runat="server"></asp:checkbox></td>
		<td width="5%">&nbsp;</td>
	</tr>
	<tr>
		<td width="20%">&nbsp;</td>
		<td colSpan="2">&nbsp;</td>
		<td width="5%">&nbsp;</td>
	</tr>
	<tr>
		<td align="middle" colSpan="4"><asp:button id="btnLastWeek" ForeColor="White" Text="Last Week" runat="server" BackColor="#440000" BorderColor="#990000" Font-Bold="True"></asp:button>&nbsp;
			<asp:button id="btnLastMonth" ForeColor="White" Text="Last Month" runat="server" BackColor="#440000" BorderColor="#990000" Font-Bold="True"></asp:button>&nbsp;
			<asp:button id="btnLastSixMonth" ForeColor="White" Text="Last Six Month" runat="server" BackColor="#440000" BorderColor="#990000" Font-Bold="True"></asp:button></td>
	</tr>
</table>
