
Imports System.Data.OleDb

Public MustInherit Class TransferDetail

    Inherits System.Web.UI.UserControl
    Protected WithEvents lblStatus As System.Web.UI.WebControls.Label
    Protected WithEvents lblSending As System.Web.UI.WebControls.Label
    Protected WithEvents lblSendingEmp As System.Web.UI.WebControls.Label
    Protected WithEvents lblShipmentDate As System.Web.UI.WebControls.Label
    Protected WithEvents txtShipmentDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblRecevingStore As System.Web.UI.WebControls.Label
    Protected WithEvents trBoxAlso As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents txtUPSNumber As System.Web.UI.WebControls.TextBox
    Protected WithEvents cboReceivingStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents labTxtStatus As System.Web.UI.WebControls.Label
    Protected WithEvents labTxtSendingStore As System.Web.UI.WebControls.Label
    Protected WithEvents labTxtShipmentDate As System.Web.UI.WebControls.Label
    Protected WithEvents lblUPSTrackingNum As System.Web.UI.WebControls.Label
    Protected WithEvents lblBoxID As System.Web.UI.WebControls.Label
    Protected WithEvents labTxtBoxId As System.Web.UI.WebControls.Label
    Protected WithEvents labTxtUPSNumber As System.Web.UI.WebControls.Label

    Protected WithEvents txtBoxId As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSendingEmp As System.Web.UI.WebControls.TextBox
    Protected WithEvents labTxtRecStore As System.Web.UI.WebControls.Label
    Protected WithEvents txtStatus As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSendingStore As System.Web.UI.WebControls.TextBox
    Protected WithEvents labTxtSendingEmp As System.Web.UI.WebControls.Label

    Protected WithEvents listBoxIncludes As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblBoxInc As System.Web.UI.WebControls.Label
    Protected WithEvents lblBoxItems As System.Web.UI.WebControls.Label

    'Global variables 
    Public gloVarBoxID As Integer 'Visual
    Public gloStatus As String 'Visual
    Public gloSendingStore As String 'Visual
    Public gloUPSTracking As String 'Visual
    Public gloShipDate As String  'Visual
    Public gloSendEmpID As String 'Visual
    Public gloRecStore As String 'Visual
    Public gloBoxIncludes As String 'Visual   

    Public gloReadOnly As Integer    'Flag
    Public gloTemp As String         'Used for Transfer
    Public gloHeaderCount As Integer 'Implicit Non Visual
    Public gloCreatedDate As String 'Non Visual

    'Global Markers
    Public cmdGetStoreNumMark As Integer

    'Declare Connection Public 
    Public connHeader As System.Data.SqlClient.SqlConnection 'SSvr - General Info
    Public connOra As System.Data.OleDb.OleDbConnection 'Ora - Emp Name

    Private BoxNum As Integer
    Private EmpId As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '************************ GET STORE NUMBER FROM COOKIE ***********************************

        Dim loop1, loop2 As Integer
        Dim arr1(), arr2() As String
        Dim MyCookieColl As HttpCookieCollection
        Dim MyCookie As HttpCookie

        MyCookieColl = Request.Cookies

        'Capture all cookie names into a string array.
        arr1 = MyCookieColl.AllKeys

        'Grab individual cookie objects by cookie name     
        For loop1 = 0 To arr1.GetUpperBound(0)
            MyCookie = MyCookieColl(arr1(loop1))
            arr2 = MyCookie.Values.AllKeys ' Grab all values for single cookie into an object array.
        Next loop1

        'Response.Write("[" & Request.Cookies("StoreNo").Value & "]")
        'gloSendingStore = Request.Cookies("StoreNo").Value
        gloSendingStore = "0001"
        'gloSendEmpID = Request.Cookies("EmpId").Value   '**** Ask Dennis about why can't extract this ***

        '----
        'Dim loop1, loop2 As Integer
        'Dim arr1(), arr2() As String
        'Dim MyCookieColl As HttpCookieCollection
        'Dim MyCookie As HttpCookie

        'MyCookieColl = Request.Cookies
        ' Capture all cookie names into a string array.
        'arr1 = MyCookieColl.AllKeys
        ' Grab individual cookie objects by cookie name     
        'For loop1 = 0 To arr1.GetUpperBound(0)
        'MyCookie = MyCookieColl(arr1(loop1))
        'Response.Write("Cookie: " & MyCookie.Name & "<br>")
        'Response.Write("Expires: " & MyCookie.Expires & "<br>")
        'Response.Write("Secure:" & MyCookie.Secure & "<br>")

        ' Grab all values for single cookie into an object array.
        'arr2 = MyCookie.Values.AllKeys
        ' Loop through cookie value collection and print all values.
        'For loop2 = 0 To arr2.GetUpperBound(0)
        'Response.Write("Value " & CStr(loop2) + ": " & arr2(loop2) & "<br>")
        'Next loop2

        'Next loop1
        'Response.Write("[" & Request.Cookies("StoreNo").Value & "]")

        '**********************************************************************************

        'VARS FROM USER 
        If gloVarBoxID <> Nothing Then
            ViewState("vsBoxID") = gloVarBoxID
            ViewState("backUpBoxId") = gloVarBoxID 'for local PageLoad purpose
            BoxNum = ViewState("vsBoxID")
        Else
            gloVarBoxID = Nothing
            BoxNum = gloVarBoxID
        End If

        If gloStatus <> Nothing Then
            ViewState("vsStatusName") = gloStatus
        End If

        If gloSendingStore <> Nothing Then
            ViewState("vsSendingStore") = gloSendingStore
        End If

        If gloShipDate <> Nothing Then
            ViewState("vsShipDate") = gloShipDate
        End If

        If gloReadOnly <> Nothing Then
            ViewState("vsReadOnly") = gloReadOnly '0 => R only,  1 => R + W  
        End If

        '**** This combination working **********
        BoxNum = Nothing
        'BoxNum = 53
        gloSendEmpID = 17651     'should get from cookie
        '****************************************

        ViewState("cookEmpId") = gloSendEmpID           'transfer cookie val to ViewState 'ref NewBox()
        EmpId = gloSendEmpID                            'transfer EmpId from cookie to local EmpId 'ref GetHeaderInfo

        If Not IsPostBack Then                          'First time Page is loaded
            If ViewState("vsBoxID") = Nothing Then      'viewstate = dne  'goes here on refresh browser button
                'Call GetHeaderInfo(BoxNum, EmpId)     'When BoxNum = Null - FIRST LOAD
            Else                                        'viewstate = exist
                gloTemp = ViewState("vsBoxID")          'When BoxNum = Not Null - FIRST LOAD 
                DisplayOldRecord(gloTemp)
            End If
        Else                                            'Subsequent Times Page is loaded
            If ViewState("vsBoxID") = Nothing Then
                gloTemp = ViewState("backUpBoxId")      'When BoxNum = Not Null - ON REFRESH
            Else
                gloTemp = ViewState("vsBoxID")
            End If
            Call DisplayOldRecord(gloTemp)              'When BoxNum = Null - ON REFRESH
        End If

    End Sub

    WriteOnly Property BoxAlsoIncludes()
        Set(ByVal Value)
            'trBoxAlso.Visible = Value
        End Set
    End Property

    WriteOnly Property Discrepancy()
        Set(ByVal Value)
            'trDiscrepancy.Visible = Value
        End Set
    End Property

    WriteOnly Property Test()
        Set(ByVal Value)
        End Set
    End Property

    '------------------------------------------------------
    'Function: Display BoxHdr Info - New Box
    '
    '
    '------------------------------------------------------

    Public Sub DisplayNewRecord()

        'Appearance Labels Controls - standard 
        labTxtBoxId.Visible = False
        labTxtStatus.Visible = False
        labTxtSendingStore.Visible = False
        labTxtUPSNumber.Visible = False
        labTxtShipmentDate.Visible = False
        labTxtSendingEmp.Visible = False
        labTxtRecStore.Visible = False

        'Appearance Fields Controls - standard
        txtBoxId.Visible = False
        txtStatus.Visible = False
        txtSendingStore.Visible = False
        txtUPSNumber.Visible = False
        txtShipmentDate.Visible = False
        txtSendingEmp.Visible = False
        cboReceivingStore.Visible = False

        'Actual Values get set here
        labTxtBoxId.Visible = True
        labTxtBoxId.Text = ViewState("vsBoxID")

        'Manually Set
        labTxtStatus.Visible = True
        labTxtStatus.Text = ViewState("vsStatusName")

        'Grab from Cookie
        labTxtSendingStore.Visible = True
        labTxtSendingStore.Text = gloSendingStore

        'TXFER over for future reads - Need to do it here because these ViewStates are not set elsewhere
        ViewState("vsSendingStore") = gloSendingStore

        'Manually Set 
        labTxtShipmentDate.Visible = True
        labTxtShipmentDate.Text = Today()

        'TXFER over for future reads  - Need to do it here because these ViewStates are not set elsewhere
        ViewState("vsShipDate") = Today()

        'UPS Number 
        txtUPSNumber.Visible = False 'to be filled later - Not set to anything now

        'Set the created Date
        ViewState("createDate") = Today

        'Emp Number 
        labTxtSendingEmp.Visible = True
        labTxtSendingEmp.Text = ViewState("vsEmpName")


        'Receiving Store 
        ' If ViewState("vsReadOnly") = 0 Then '0 => R only 
        cboReceivingStore.Visible = True
        'ViewState("vsRecStore") = cboReceivingStore.SelectedItem.Value
        'End If

        'If ViewState("vsReadOnly") = 1 Then '0 => R only 
        'cboReceivingStore.Visible = False
        'labTxtRecStore.Visible = True
        'labTxtRecStore.Text = ViewState("vs ")
        'End If


    End Sub 'End Disp New Record

    '------------------------------------------------------
    'Function: Display BoxHdr Info - Old Record
    '
    '
    '------------------------------------------------------
    Public Sub DisplayOldRecord(ByVal glotemp)

        Dim Temp As String

        'Appearance Labels Controls - standard 
        labTxtBoxId.Visible = False
        labTxtStatus.Visible = False
        labTxtSendingStore.Visible = False
        labTxtUPSNumber.Visible = False
        labTxtShipmentDate.Visible = False
        labTxtSendingEmp.Visible = False
        labTxtRecStore.Visible = False

        'Appearance Fields Controls - standard
        txtBoxId.Visible = False
        txtStatus.Visible = False
        txtSendingStore.Visible = False
        txtUPSNumber.Visible = False
        txtShipmentDate.Visible = False
        txtSendingEmp.Visible = False
        cboReceivingStore.Visible = False

        'Actual Values get set here
        labTxtBoxId.Visible = True
        labTxtBoxId.Text = glotemp

        labTxtStatus.Visible = True
        labTxtStatus.Text = ViewState("vsStatusName") 'ok on refresh 

        labTxtSendingStore.Visible = True
        labTxtSendingStore.Text = ViewState("vsSendingStore")

        labTxtShipmentDate.Visible = True
        labTxtShipmentDate.Text = ViewState("vsShipDate")

        labTxtUPSNumber.Visible = True
        labTxtUPSNumber.Text = ViewState("vsUPSTracking")

        labTxtSendingEmp.Visible = True
        labTxtSendingEmp.Text = ViewState("vsEmpName") 'ok on refresh

        cboReceivingStore.Visible = True

    End Sub 'End disp old record

    '------------------------------------------------------
    'Function: Find the Employee Name based on the EmpID
    'provided.
    '
    'fxInputs : EmpID
    'spInputs : -> EmpID 
    'spOutputs : EmpLast & EmpFirst concatenated 
    'fxOutputs : None - Assigne spOutput to Global Var
    '------------------------------------------------------

    Public Sub FindEmployee(ByVal EmpId As Integer)

        Dim id As Integer
        'Declare command string 
        Dim objORADataAdapter As New OleDb.OleDbDataAdapter()

        'Link the conn
        connOra = New System.Data.OleDb.OleDbConnection("Provider=MSDAORA.1;Password=hottopic;User ID=hottopic;Data Source=genret")
        connOra.Open()

        'Commands
        Dim cmdFindEmpName As System.Data.OleDb.OleDbCommand
        cmdFindEmpName = New OleDbCommand("spFindEmp", connOra)
        cmdFindEmpName.CommandType = CommandType.StoredProcedure

        Dim prmPara As OleDbParameter
        Dim prmIn As OleDbParameter

        prmPara = cmdFindEmpName.CreateParameter()
        prmPara.Direction = ParameterDirection.ReturnValue
        prmPara.OleDbType = OleDbType.VarChar
        prmPara.Size = 200

        prmIn = cmdFindEmpName.CreateParameter()
        prmIn.Value = EmpId 'passed in
        prmIn.Direction = ParameterDirection.Input
        prmIn.OleDbType = OleDbType.VarChar
        prmIn.Size = 5

        cmdFindEmpName.Parameters.Add(prmPara)
        cmdFindEmpName.Parameters.Add(prmIn)

        cmdFindEmpName.ExecuteNonQuery()

        ViewState("vsEmpName") = prmPara.Value()

        connOra.Close()

    End Sub 'End Find Employee

    '------------------------------------------------------
    'Function FindBoxID: Find the Newest BoxID for New Record 
    'Box_Xfer_Hdr
    '
    'fxInputs : None
    'spInputs : None 
    'spOutputs : BoxId - max value 
    'fxOutputs : None - var assigned to global
    '------------------------------------------------------

    Public Sub FindBoxID()

        Dim EraseMe As String

        'declare command string
        Dim cmdFindBoxID As System.Data.SqlClient.SqlCommand 'SSvr

        'Exec Command String 
        cmdFindBoxID = New SqlClient.SqlCommand("spFindBoxId", connHeader)
        cmdFindBoxID.CommandType = CommandType.StoredProcedure

        'Handlers
        Dim prmNewBoxId As SqlClient.SqlParameter
        prmNewBoxId = cmdFindBoxID.Parameters.Add("@Max", SqlDbType.VarChar)
        prmNewBoxId.Size = 100
        prmNewBoxId.Direction = ParameterDirection.Output

        'Execute
        cmdFindBoxID.ExecuteNonQuery()

        'Assign outputs to globalVar
        'gloVarBoxID = CStr(cmdFindBoxID.Parameters("@Max").Value)
        'ViewState("vsBoxID") = gloVarBoxID

        ' ViewState("vsBoxID") = CStr(cmdFindBoxID.Parameters("@Max").Value)
        'ViewState("backUpBoxId") = CStr(cmdFindBoxID.Parameters("@Max").Value)
        'EraseMe = CStr(cmdFindBoxID.Parameters("@Max").Value)

        ViewState("vsBoxID") = prmNewBoxId.Value
        ViewState("backUpBoxId") = prmNewBoxId.Value

    End Sub 'End FindBoxId

    '------------------------------------------------------
    'Function: Set the Temp Record - Insert a Temp Record
    'to dbase. To extract Autogen BoxID later
    '
    'fxInput - None
    'spInput - None 
    'spOutput - None
    'fxOutput - None
    '------------------------------------------------------

    Public Sub setTempRecord()

        'declare the command string
        Dim cmdCreateRow As System.Data.SqlClient.SqlCommand 'SSvr

        'Exec command string    
        cmdCreateRow = New SqlClient.SqlCommand("spCreateRow", connHeader)
        cmdCreateRow.CommandType = CommandType.StoredProcedure

        'Execute
        cmdCreateRow.ExecuteNonQuery()

    End Sub 'End setTempRecord

    '------------------------------------------------------
    'Function: Delete the Temp Record - when user does not 
    'commit the transaction
    '
    'fxInput - None
    'spInput - None 
    'spOutput - None
    'fxOutput - None
    '------------------------------------------------------

    Public Sub deleteTempRecord()

        'declare command string
        Dim cmdDeleteRow As System.Data.SqlClient.SqlCommand ' SSvr

        'Exec command string 
        cmdDeleteRow = New SqlClient.SqlCommand("spDeleteRow", connHeader)
        cmdDeleteRow.CommandType = CommandType.StoredProcedure

        'Execute 
        cmdDeleteRow.ExecuteNonQuery()

    End Sub 'End deleteTempRecord

    '------------------------------------------------------
    'Function: Delete the Temp Record - when user does not 
    'commit the transaction. 
    '
    'fxInput - BoxID, need to pass in an existing BoxID 
    'spInput - None 
    'spOutput - None
    'fxOutput - None
    '------------------------------------------------------

    Public Sub deleteTempRecordRow(ByVal BoxID)

        'declare command string
        Dim cmdDeleteRecordRow As System.Data.SqlClient.SqlCommand ' SSvr

        'declare parameter
        Dim prmBoxID As SqlClient.SqlParameter

        'Exec command string 
        cmdDeleteRecordRow = New SqlClient.SqlCommand("spDeleteRowInput", connHeader)
        cmdDeleteRecordRow.CommandType = CommandType.StoredProcedure
        cmdDeleteRecordRow.Parameters.Add("@BoxId", BoxID)

        'Execute 
        cmdDeleteRecordRow.ExecuteNonQuery()

    End Sub 'End deleteTempRecordRow

    '------------------------------------------------------
    'Function ConvertStatus
    '
    'Convert the Status ID to a Text Version
    '
    'fxInput - StatusID
    'spInput - StatusID
    'spOutput - None
    'fxOutput - None
    '------------------------------------------------------

    Public Sub ConvertStatus(ByVal StatusID)

        'declare command string
        Dim cmdConStatus As System.Data.SqlClient.SqlCommand 'SSvr

        'Exec Command String 
        cmdConStatus = New SqlClient.SqlCommand("spConStatus", connHeader)
        cmdConStatus.CommandType = CommandType.StoredProcedure

        'Handlers - Input 
        Dim prmIn As SqlClient.SqlParameter
        prmIn = cmdConStatus.Parameters.Add("@Status_ID", SqlDbType.VarChar)
        prmIn.Size = 100
        prmIn.Value = StatusID
        prmIn.Direction = ParameterDirection.Input

        'Handlers - Output 
        Dim prmOut As SqlClient.SqlParameter
        prmOut = cmdConStatus.Parameters.Add("@Status_Desc", SqlDbType.VarChar)
        prmOut.Size = 100
        prmOut.Direction = ParameterDirection.Output

        'Execute Query 
        cmdConStatus.ExecuteNonQuery()

        'Assign output 
        ViewState("vsStatusName") = CStr(cmdConStatus.Parameters("@Status_Desc").Value)

    End Sub 'End ConvertStatus  

    '-------------------------------------------------------
    'On Load, object needs to run Stored Proc and Populate
    'its fields 
    'CALLS: Sp to call : spFillBoxHdr - General Info 
    '       Sp to call : spFillBoxHdrCount - Count of return rows
    '        
    'CONNECTIONS: both sSvr and Ora open connection 
    '
    '
    'INPUT: BoxNum
    '
    '
    'OUTPUT:
    '
    '
    'CONDITIONS: 1) proc does not check for existence of box hdr
    '
    '
    '-------------------------------------------------------

    Public Sub GetHeaderInfo(ByVal BoxNum As Integer, ByVal EmpId As Integer)

        'Translate a pass in into an integer when passing to SQL 
        If BoxNum = Nothing Then
            BoxNum = 0
        End If

        'Declare All Commands
        Dim cmdHeader As System.Data.SqlClient.SqlCommand 'SSvr - Get General Data 
        Dim cmdHeaderCount As System.Data.SqlClient.SqlCommand ' SSvr - Count the # of records found
        Dim cmdGetStoreNumbers As System.Data.SqlClient.SqlCommand 'SSvr- Store Num - Get all store #'s
        Dim cmdGetBoxIncludes As System.Data.SqlClient.SqlCommand 'SSvr- Box Includes - All records
        Dim cmdGetBoxIncludesSpecific As System.Data.SqlClient.SqlCommand 'SSvr- Box Includes - Specific

        'Declare Data Readers
        Dim dtrGetStoreNumbers As System.Data.SqlClient.SqlDataReader 'SSvr - Store Num - Display all store #'s
        Dim dtrGetBoxIncludes As System.Data.SqlClient.SqlDataReader 'SSvr - Box Includes
        Dim dtrGetBoxesIncludesSpecific As System.Data.SqlClient.SqlDataReader 'SSvr - Box Includes Specific

        'Declare Parameters connHeader (1) 
        Dim prmStatus As SqlClient.SqlParameter
        Dim prmSendingStore As SqlClient.SqlParameter
        Dim prmSendingEmp As SqlClient.SqlParameter
        Dim prmShipmentDate As SqlClient.SqlParameter
        Dim prmRecStoreCD As SqlClient.SqlParameter
        Dim prmUPSTrack As SqlClient.SqlParameter
        Dim prmRecDate As SqlClient.SqlParameter

        'Declare Parameters connHeaderCount (1) 
        Dim prmHeaderCount As SqlClient.SqlParameter

        'Declare Parameters connGenretHeader (2)  
        Dim prmStoreName As SqlClient.SqlParameter
        Dim prmStoreNum As SqlClient.SqlParameter

        'Box Includes Drop Down
        Dim DropDownList1 As System.Web.UI.WebControls.DropDownList

        'Connect to sSvr dbConn
        connHeader = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        connHeader.Open()

        'Declare Local Containers
        Dim labStatusText As String
        Dim labSendingStoreText As String
        Dim labSendingEmpText As String

        'Execute the Sp to extract; pass the BoxType in  (1)
        cmdHeader = New SqlClient.SqlCommand("spFillBoxHdr", connHeader)
        cmdHeader.CommandType = CommandType.StoredProcedure
        cmdHeader.Parameters.Add("@Box_Id", BoxNum)

        'Execute the Sp to count resultset; pass the BoxType in (1)
        cmdHeaderCount = New SqlClient.SqlCommand("spFillBoxHdrCount", connHeader)
        cmdHeaderCount.CommandType = CommandType.StoredProcedure
        cmdHeaderCount.Parameters.Add("@Box_Id", BoxNum)

        'Execute the SP; no inputs (2)
        cmdGetStoreNumbers = New SqlClient.SqlCommand("spGetStoreNumbers", connHeader)
        cmdGetStoreNumbers.CommandType = CommandType.StoredProcedure

        'Execute the SP; no inputs (3)
        cmdGetBoxIncludes = New SqlClient.SqlCommand("spGetBoxIncludes", connHeader)
        cmdGetBoxIncludes.CommandType = CommandType.StoredProcedure

        'Execute the SP; no inputs (4) - listbox gets the box includes items specific to that box
        'cmdGetBoxIncludesSpecific = New SqlClient.SqlCommand("spGetBoxIncludesSpecific", connHeader)
        'cmdGetBoxIncludesSpecific.CommandType = CommandType.StoredProcedure

        'Link the result variables to Parameters (general) (1)
        prmSendingStore = cmdHeader.Parameters.Add("@Sending_Store_Cd", SqlDbType.VarChar)
        prmStatus = cmdHeader.Parameters.Add("@Current_Box_Status_Cd", SqlDbType.VarChar)
        prmSendingEmp = cmdHeader.Parameters.Add("@Emp_Id", SqlDbType.VarChar)
        prmShipmentDate = cmdHeader.Parameters.Add("@Shipment_Date", SqlDbType.VarChar)
        prmRecStoreCD = cmdHeader.Parameters.Add("@Receiving_Store_Cd", SqlDbType.VarChar)
        prmUPSTrack = cmdHeader.Parameters.Add("@UPS_Tracking_Num", SqlDbType.VarChar)
        prmRecDate = cmdHeader.Parameters.Add("@Received_Date", SqlDbType.VarChar)

        'Link the result variables to Parameters (general) (1)
        prmHeaderCount = cmdHeaderCount.Parameters.Add("@Count", SqlDbType.VarChar)

        'Link the result variables to Parameters (drop down box)
        Dim prmStore_Name As SqlClient.SqlParameter
        Dim prmStore_Num As SqlClient.SqlParameter

        'Assign Size to Parameters (1)
        prmStatus.Size = 10
        prmSendingStore.Size = 10
        prmSendingEmp.Size = 10
        prmShipmentDate.Size = 10
        prmRecStoreCD.Size = 10
        prmUPSTrack.Size = 10
        prmRecDate.Size = 10

        'Assign Size to Parameters (1)
        prmHeaderCount.Size = 10

        'Assign direction to Parameters (1) 
        prmStatus.Direction = ParameterDirection.Output
        prmSendingStore.Direction = ParameterDirection.Output
        prmSendingEmp.Direction = ParameterDirection.Output
        prmShipmentDate.Direction = ParameterDirection.Output
        prmRecStoreCD.Direction = ParameterDirection.Output
        prmUPSTrack.Direction = ParameterDirection.Output
        prmRecDate.Direction = ParameterDirection.Output

        'Assign direction to Parameters (1)
        prmHeaderCount.Direction = ParameterDirection.Output

        'Open and Execute
        cmdHeader.ExecuteNonQuery()
        cmdHeaderCount.ExecuteNonQuery()

        'Status - Get from FirstQuery
        Dim StatusID As Integer

        'Default Status of 1: Packing 
        If IsDBNull(prmStatus.Value) Then
            StatusID = 1
        Else
            StatusID = prmStatus.Value
        End If

        'How many records found?
        gloHeaderCount = CStr(cmdHeaderCount.Parameters("@Count").Value)

        'If query returned Null, then this is a ** NEW **  RECORD
        If gloHeaderCount = 0 Then

            Call FindEmployee(EmpId)       'Get the Employee Name from EmpID  '* set to VS 
            Call ConvertStatus(StatusID)    'Convert Status ID to Status Text
            Call setTempRecord()            'Add a Temp Row to table '* Nothing Set 
            Call FindBoxID()                'Get the Temp Row ID to table '* set to VS

            'Assign values of datareader :: Pull Down Data
            dtrGetStoreNumbers = cmdGetStoreNumbers.ExecuteReader()
            cboReceivingStore.DataSource = dtrGetStoreNumbers
            cboReceivingStore.DataTextField = "Store_Name_Alias"
            cboReceivingStore.DataValueField = "Store_Cd"
            cboReceivingStore.DataBind()
            dtrGetStoreNumbers.Close()

            dtrGetBoxIncludes = cmdGetBoxIncludes.ExecuteReader()
            listBoxIncludes.DataSource = dtrGetBoxIncludes
            listBoxIncludes.DataTextField = "Includes_Desc"
            listBoxIncludes.DataValueField = "Box_Includes_Cd"
            listBoxIncludes.DataBind()
            listBoxIncludes.SelectionMode = ListSelectionMode.Multiple
            dtrGetBoxIncludes.Close()

            Call DisplayNewRecord()         'Display Control in New Record Style
            Call AssignGlobal()

        Else

            'If this is an old record

            Call FindEmployee(EmpId)       'Get the Employee Name from EmpID() '* set to VS

            Call ConvertStatus(StatusID)   'Convert Status ID to Status Text

            'Assign the variables to globals
            ViewState("vsBoxID") = CStr(cmdHeader.Parameters("@Box_Id").Value)

            Dim gloTemp1 As String
            gloTemp1 = ViewState("vsBoxID")

            'ViewState("vsStatus") = CStr(cmdHeader.Parameters("@Current_Box_Status_Cd").Value)
            'vsStatusName Set in calling GetHeaderInfo 

            ViewState("vsSendingStore") = cmdHeader.Parameters("@Sending_Store_Cd").Value
            ViewState("vsUPSTracking") = CStr(cmdHeader.Parameters("@UPS_Tracking_Num").Value)
            ViewState("vsShipDate") = CStr(cmdHeader.Parameters("@Shipment_Date").Value)

            'ViewState("vsSendEmpID") = CStr(cmdHeader.Parameters("@Emp_Id").Value)
            'vsEmpName Set in FindEmployee

            ViewState("vsRecStore") = CStr(cmdHeader.Parameters("@Receiving_Store_Cd").Value)

            'Assign values of datareader 
            dtrGetStoreNumbers = cmdGetStoreNumbers.ExecuteReader()
            cboReceivingStore.DataSource = dtrGetStoreNumbers
            cboReceivingStore.DataTextField = "Store_Name_Alias"
            cboReceivingStore.DataValueField = "Store_Cd"
            cboReceivingStore.DataBind()
            dtrGetStoreNumbers.Close()

            dtrGetBoxIncludes = cmdGetBoxIncludes.ExecuteReader()
            listBoxIncludes.DataSource = dtrGetBoxIncludes
            listBoxIncludes.DataTextField = "Includes_Desc"
            listBoxIncludes.DataValueField = "Box_Includes_Cd"
            listBoxIncludes.DataBind()
            dtrGetBoxIncludes.Close()

            Call DisplayOldRecord(gloTemp1)
            Call AssignGlobal()             'Assign the Viewstates to Locals

        End If

        'close connection
        connHeader.Close()

    End Sub 'End GetHeaderInfo

    '---------------------------------------------------- 
    'Saves the Box Information to Box_Xfer_Hdr
    '
    '
    '---------------------------------------------------- 

    Public Sub SaveBox()

        Call AssignGlobal() 'Assign ViewState -> Globals
        Call InsertRecord() 'Insert the records to the database

    End Sub


    '---------------------------------------------------- 
    'SQL Insert Record  - func ok
    '
    '
    '---------------------------------------------------- 

    Public Sub InsertRecord()

        'Vars used 
        '1 gloVarBoxID'
        '2 gloSendingStore'
        '3 gloStatus'
        '4 gloSendEmpID
        '5 gloShipDate'
        '6 gloRecStore
        '7 gloCreatedDate

        connHeader = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        connHeader.Open()

        'Exec Command String
        Dim cmdInsert As System.Data.SqlClient.SqlCommand
        cmdInsert = New SqlClient.SqlCommand("spInsertRow", connHeader)
        cmdInsert.CommandType = CommandType.StoredProcedure

        'Handlers (1)
        Dim prmBoxID As SqlClient.SqlParameter
        prmBoxID = cmdInsert.Parameters.Add("@BoxID", SqlDbType.VarChar)
        prmBoxID.Size = 20
        prmBoxID.Value = gloVarBoxID
        prmBoxID.Direction = ParameterDirection.Input

        '(2)
        Dim prmSStore As SqlClient.SqlParameter
        prmSStore = cmdInsert.Parameters.Add("@SStore", SqlDbType.VarChar)
        prmSStore.Size = 20
        prmSStore.Value = gloSendingStore
        prmSStore.Direction = ParameterDirection.Input

        '(3)
        Dim prmStatus As SqlClient.SqlParameter
        prmStatus = cmdInsert.Parameters.Add("@Status", SqlDbType.VarChar)
        prmStatus.Size = 20
        prmStatus.Value = gloStatus
        prmStatus.Direction = ParameterDirection.Input

        '(4)
        Dim prmEmpID As SqlClient.SqlParameter
        prmEmpID = cmdInsert.Parameters.Add("@EmpID", SqlDbType.VarChar)
        prmEmpID.Size = 20
        prmEmpID.Value = gloSendEmpID
        prmEmpID.Direction = ParameterDirection.Input

        '(5)
        Dim prmShipDate As SqlClient.SqlParameter
        prmShipDate = cmdInsert.Parameters.Add("@ShipDate", SqlDbType.VarChar)
        prmShipDate.Size = 20
        prmShipDate.Value = gloShipDate
        prmShipDate.Direction = ParameterDirection.Input

        '(6)
        Dim prmRecStore As SqlClient.SqlParameter
        prmRecStore = cmdInsert.Parameters.Add("@RecStore", SqlDbType.VarChar)
        prmRecStore.Size = 20
        prmRecStore.Value = gloRecStore
        prmRecStore.Direction = ParameterDirection.Input

        '(7)
        Dim prmCDate As SqlClient.SqlParameter
        prmCDate = cmdInsert.Parameters.Add("@CDate", SqlDbType.VarChar)
        prmCDate.Size = 20
        prmCDate.Value = gloCreatedDate
        prmCDate.Direction = ParameterDirection.Input

        cmdInsert.ExecuteNonQuery()

        connHeader.Close()

    End Sub

    '---------------------------------------------------- 
    'assign Viewstate to global - place at end of program 
    'Get Snapshot of screen data - func ok 
    '
    '
    '---------------------------------------------------- 
    Public Sub AssignGlobal()
        Dim tStatus As String

        gloVarBoxID = ViewState("vsBoxID") 'format ok

        tStatus = ViewState("vsStatusName")
        ToStatusID(tStatus)
        'gloStatus var set by ToStatusID()

        gloSendingStore = ViewState("vsSendingStore")
        gloShipDate = ViewState("vsShipDate")
        'gloSendEmpID      :: use this glo - already in Numeric format
        gloRecStore = cboReceivingStore.SelectedItem.Value 'format ok 
        gloCreatedDate = ViewState("createDate")

    End Sub

    '---------------------------------------------------------
    'Sets gloStatus and translates string back to Integer - func ok 
    '
    '
    '---------------------------------------------------------
    Public Sub ToStatusID(ByVal tStatus)

        connHeader = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        connHeader.Open()

        'Exec Command String
        Dim cmdConvertID As System.Data.SqlClient.SqlCommand
        cmdConvertID = New SqlClient.SqlCommand("spToIntStatus", connHeader)
        cmdConvertID.CommandType = CommandType.StoredProcedure

        'Handlers - Input 
        Dim prmConvertID As SqlClient.SqlParameter
        prmConvertID = cmdConvertID.Parameters.Add("@Status", SqlDbType.VarChar)
        prmConvertID.Size = 20
        prmConvertID.Value = tStatus
        prmConvertID.Direction = ParameterDirection.Input

        'Output
        Dim prmOut As SqlClient.SqlParameter
        prmOut = cmdConvertID.Parameters.Add("@StatCode", SqlDbType.VarChar)
        prmOut.Size = 20
        prmOut.Direction = ParameterDirection.Output

        'Execute SQL
        cmdConvertID.ExecuteNonQuery()

        'Set gloStatus to an Integer
        gloStatus = prmOut.Value

        connHeader.Close()

    End Sub

    Public Sub NewBox()

        Dim BoxNumber As Integer
        Dim EmployeeID As Integer

        BoxNumber = Nothing
        'EmployeeID = ViewState("cookEmpId") 'taken from the Cookie
        EmployeeID = gloSendEmpID            'explicit call - TEMP 

        GetHeaderInfo(BoxNumber, EmployeeID)

    End Sub

    Public Sub DeleteBox()

        Dim BoxNumber As Integer

        BoxNumber = ViewState("vsBoxID")
        deleteTempRecordRow(BoxNumber)

    End Sub

End Class



