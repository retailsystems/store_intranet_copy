Public Class IC_VoidConfirm
    Inherits System.Web.UI.Page
    Protected WithEvents lblVoidConfirm As System.Web.UI.WebControls.Label
    Protected WithEvents btnYes As System.Web.UI.WebControls.Button
    Protected WithEvents btnNo As System.Web.UI.WebControls.Button
    'Public strSearch As String
    Public strBox As String
    Public strReturn As String
    Public VOValue As Boolean
    Protected WithEvents ucHeader As Header

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'strSearch = Request.Params("SQL2")
        ucHeader.lblTitle = "Void Confirmation"
        ucHeader.CurrentMode(Header.HeaderGroup.IC)
        ucHeader.CurrentPage(Header.PageName.IC_Search)
        strBox = Request.Params("BID")
        strReturn = Request.Params("RTN") 'This tells IC_EditTransfer the return SQL string for IC_Results or IC_ResultsTT
    End Sub

    Private Sub btnYes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnYes.Click
        'Dim Void = 1
        VOValue = False
        Response.Redirect("IC_EditTransfer.aspx?Void=1&BID=" & strBox & "&RTN=" & strReturn & "&VO=" & VOValue)
        'Response.Redirect(strReturn & "?Void=1&BID=" & strBox)
    End Sub

    Private Sub btnNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNo.Click
        VOValue = False
        Response.Redirect("IC_EditTransfer.aspx?BID=" & strBox & "&RTN=" & strReturn & "&VO=" & VOValue)
        'Response.Redirect(strReturn & "?BID=" & strBox)
    End Sub
End Class
