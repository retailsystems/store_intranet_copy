Public Class EmpHistory
    Inherits System.Web.UI.Page
    Protected WithEvents grdEmpHistory As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objDataReader As SqlClient.SqlDataReader
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetBoxHistory '" & Request.QueryString("BID") & "' ", objConnection)

        objDataReader = objCommand.ExecuteReader()

        grdEmpHistory.DataSource = objDataReader
        grdEmpHistory.DataBind()

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

    End Sub

End Class
