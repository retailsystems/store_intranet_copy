Public Class ScanItem_Old
    Inherits System.Web.UI.Page
    Protected WithEvents radStoreRTV As System.Web.UI.WebControls.RadioButton
    Protected WithEvents radCTB As System.Web.UI.WebControls.RadioButton
    Protected WithEvents radAnalystTransfer As System.Web.UI.WebControls.RadioButton
    Protected WithEvents radCustomerTransfer As System.Web.UI.WebControls.RadioButton
    Protected WithEvents ddlRTVReason As System.Web.UI.WebControls.DropDownList
    Protected WithEvents radAllItemYes As System.Web.UI.WebControls.RadioButton
    Protected WithEvents radAllItemNo As System.Web.UI.WebControls.RadioButton
    Protected WithEvents txtScanItem As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
    Protected WithEvents lblTransferType As System.Web.UI.WebControls.Label
    Protected WithEvents lblRTVReason As System.Web.UI.WebControls.Label
    Protected WithEvents lblRTVReasonHint As System.Web.UI.WebControls.Label
    Protected WithEvents lblScanAllItems As System.Web.UI.WebControls.Label
    Protected WithEvents lblScanItem As System.Web.UI.WebControls.Label
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnClose As System.Web.UI.WebControls.Button
    Protected WithEvents radBuyerRTV As System.Web.UI.WebControls.RadioButton
    Protected WithEvents trTransferType_Opt1 As System.Web.UI.Control
    Protected WithEvents trTransferType_Opt2 As System.Web.UI.Control
    Protected WithEvents trTransferType_Opt3 As System.Web.UI.Control
    Protected WithEvents trTransferType_Opt4 As System.Web.UI.Control
    Protected WithEvents trTransferType_Opt5 As System.Web.UI.Control
    Protected WithEvents trRTVReason_1 As System.Web.UI.Control
    Protected WithEvents trRTVReason_2 As System.Web.UI.Control

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Page.Request.QueryString("Mode") = "CheckIn" Then

            Me.TransferType = False
            Me.RTVReason = False

        End If

    End Sub

    WriteOnly Property TransferType()
        Set(ByVal Value)
            trTransferType_Opt1.Visible = Value
            trTransferType_Opt2.Visible = Value
            trTransferType_Opt3.Visible = Value
            trTransferType_Opt4.Visible = Value
            trTransferType_Opt5.Visible = Value
        End Set
    End Property

    WriteOnly Property RTVReason()
        Set(ByVal Value)
            trRTVReason_1.Visible = Value
            trRTVReason_2.Visible = Value
        End Set
    End Property

End Class
