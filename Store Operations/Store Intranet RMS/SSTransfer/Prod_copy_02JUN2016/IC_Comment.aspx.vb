Public Class IC_Comment
    Inherits System.Web.UI.Page
    Protected WithEvents lblComments As System.Web.UI.WebControls.Label
    Protected WithEvents txtComments As System.Web.UI.WebControls.TextBox
    Public strBox
    Public strSType
    Public strComment
    Public strUComment
    Protected WithEvents ucHeader As Header
    Public strReturn As String
    'Public strReturn1 As String
    'Public strReturn2 As String
    Public ViewValue As Boolean 'For going back to IC_EditTransfer Page.
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Public parmComment As SqlClient.SqlParameter
    Private objUserInfo As New UserInfo()
    Private objTransferLock As New TransferLock()
    '*****************************************************************
    'These will be the variables passed to results pages to form querys.
    'For returning to IC_ResultsTT
    Public GBID As String   'retrieving TT for Gers Status Viewing purposes
    Public FStore As String       'From Store
    Public FDis As String         'From District
    Public FReg As String         'From Region
    Public NoFInternet As String  'From Internet Store Checkbox
    Public TStore As String       'To Store
    Public TDis As String         'To District
    Public TReg As String         'To Region
    Public NoTInternet As String  'To Internet Store Checkbox
    Public GERS As String         'GERS Process Status
    Public Discrep As String      'Discrepancy status
    Public TType As String        'Transfer Type
    Public TStatus As String      'Transfer Status
    Public TDate As String        'To Date search
    Public FDate As String        'From Date Search
    Public DFQty As String        'Discrep from qty
    Public DTQty As String        'Discrep to qty
    Public DFAmt As String        'Discrep from amt
    Public DTAmt As String        'Discrep to amt
    Public TrackNum As String     'Tracking Number (UPS)
    Public TransNum As String     'Transfer Number
    '*****************************************************************
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        objUserInfo.GetEmployeeInfo()
        If Not objUserInfo.EmpOnline Then
            Response.Redirect("SessionEnd.aspx?Mode=2")
        End If

        ucHeader.lblTitle = "Inventory Control Comments"
        ucHeader.CurrentPage(Header.PageName.IC_Search)
        ucHeader.CurrentMode(Header.HeaderGroup.IC)

        GBID = Request.Params("GERSBID")
        FStore = Request.Params("FS")
        FDis = Request.Params("FDIS")
        FReg = Request.Params("FREG")
        TStore = Request.Params("TS")
        TDis = Request.Params("TDIS")
        TReg = Request.Params("TREG")
        NoFInternet = Request.Params("NFI")
        NoTInternet = Request.Params("NTI")
        GERS = Request.Params("G")
        Discrep = Request.Params("DS")     'Discrep Statuses
        TType = Request.Params("TT")       'Transfer Types
        TStatus = Request.Params("TranS")  'Transfer Statuses
        FDate = Request.Params("FD")
        TDate = Request.Params("TD")
        DFQty = Request.Params("DFQ")
        DTQty = Request.Params("DTQ")
        DFAmt = Request.Params("DFA")
        DTAmt = Request.Params("DTA")
        TrackNum = Request.Params("TKN")   'Tracking number
        TransNum = Request.Params("TSN")   'transfer number

        If Not IsPostBack Then
            strBox = Request.Params("BID")
            strSType = Request.Params("XT")

            If Request.Params("RTNRESULTSTT") = Nothing Or Request.Params("RTNRESULTSTT") = "" Then
                ViewValue = Request.Params("VO")
            End If

            lblComments.Text = "Comments on Transfer Number: " & strBox & " "
            Call CheckBoxStatus()
            'Call RetrieveComments()
        End If
    End Sub
    Private Sub RetrieveComments()
        'Retrieve any comments that already exist and bind to textbox.
        Dim NullCheck As Boolean
        Dim conGetComment As System.Data.SqlClient.SqlConnection
        Dim cmdGetComment As System.Data.SqlClient.SqlCommand
        Dim dtrGetComment As SqlClient.SqlDataReader

        conGetComment = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        cmdGetComment = New SqlClient.SqlCommand("spGetICComments", conGetComment)
        cmdGetComment.CommandType = CommandType.StoredProcedure
        cmdGetComment.Parameters.Add("@BID", strBox)
        cmdGetComment.Parameters.Add("@TransType", strSType)
        conGetComment.Open()
        dtrGetComment = cmdGetComment.ExecuteReader

        If dtrGetComment.Read() Then
            NullCheck = IsDBNull(dtrGetComment("Comment"))
            If NullCheck = True Then
                txtComments.Text = ""
            Else
                txtComments.Text = dtrGetComment("Comment")
            End If

        End If
        dtrGetComment.Close()
        conGetComment.Close()
        cmdGetComment.Dispose()
    End Sub
    Private Sub CheckBoxStatus()
        'Check box current status.  If status is packing, unpacking, 
        'saved, or new transfer then disable the text box.  Else enable the textbox
        Dim SQL
        Dim BoxStatus
        SQL = "SELECT Current_Box_Status_Cd FROM Box_Xfer_Hdr WHERE Box_Id= '" & strBox & "' "

        Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim dtrSearch As SqlClient.SqlDataReader
        conSearch.Open()
        Dim cmdSearch As New SqlClient.SqlCommand(SQL, conSearch)
        dtrSearch = cmdSearch.ExecuteReader
        If dtrSearch.Read Then
            BoxStatus = dtrSearch("Current_Box_Status_Cd")
        End If
        conSearch.Close()
        dtrSearch.Close()
        cmdSearch.Dispose()
        If Not objTransferLock.LockTransfer(Request("BID"), objUserInfo.EmpId, Session.SessionID) Then
            Call RetrieveComments()
            txtComments.ReadOnly = True
            btnSave.Visible = False
        ElseIf BoxStatus = 1 Or BoxStatus = 5 Or BoxStatus = 6 Then
            'can't edit comments if box status is new transfer, packing, or recorded.
            txtComments.ReadOnly = True
            btnSave.Visible = False
        Else
            txtComments.ReadOnly = False
            btnSave.Visible = True
            Call RetrieveComments()
        End If
    End Sub
    Private Sub txtComments_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtComments.TextChanged
        'User has changed text.  Assign text to strUComment
        strUComment = txtComments.Text
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        strUComment = txtComments.Text
        strBox = Request.Params("BID")
        strSType = Request.Params("XT")
        If strUComment <> "" Then
            Dim conSaveComment As System.Data.SqlClient.SqlConnection
            Dim cmdSaveComment As System.Data.SqlClient.SqlCommand
            conSaveComment = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
            cmdSaveComment = New SqlClient.SqlCommand("spSaveICComment", conSaveComment)
            cmdSaveComment.CommandType = CommandType.StoredProcedure
            cmdSaveComment.Parameters.Add("@BID", strBox)
            cmdSaveComment.Parameters.Add("@TransType", strSType)
            cmdSaveComment.Parameters.Add("@Comments", strUComment)
            conSaveComment.Open()
            cmdSaveComment.ExecuteNonQuery()
            conSaveComment.Close()
            cmdSaveComment.Dispose()
        End If
        'Return to page this was called from
        'ViewValue = Request.Params("VO")
        'strReturn2 = Request.Params("RTN2")
        'strReturn1 = Request.Params("RTN")
        'strReturn = strReturn1 & "&RTN2=" & strReturn2 & "&VO=" & ViewValue
        'Response.Redirect(strReturn)
        If Request.Params("RTNRESULTSTT") = Nothing Or Request.Params("RTNRESULTSTT") = "" Then
            'Returning to IC_EditTransfer Page
            Dim strReturn1
            Dim strReturn2
            Dim RTNPage
            RTNPage = "IC_ResultsTT"
            strReturn2 = Request.Params("RFC") 'return query for getting from IC_EditTransfer to ResultsTT
            If InStr(1, strReturn2, RTNPage) > 0 Then
                'Go back to IC_ResultsTT
                strReturn2 = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID
            Else
                'Go Back to IC_Result
                strReturn2 = "IC_Results.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum
            End If
            ViewValue = Request.Params("VO")    'View mode for IC_EditTransfer
            'strReturn2 = Request.Params("RFC")  'return query for getting from IC_EditTransfer to ResultsTT
            strReturn1 = Request.Params("IET")  'return query for getting back to IC_EditTransfer
            strReturn = strReturn1 & "&RFC=" & strReturn2 & "&VO=" & ViewValue
            Response.Redirect(strReturn)
        Else
            'Returning to IC_ResultsTT Page
            'strReturn = Request.Params("RTNRESULTSTT")
            'Response.Redirect(strReturn)
            If GBID = Nothing Then
                Response.Redirect("IC_ResultsTT.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum)
            Else
                Response.Redirect("IC_ResultsTT.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID)
            End If
        End If

    End Sub
    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        'Return to previous page.
        'ViewValue = Request.Params("VO")
        'strReturn2 = Request.Params("RTN2")
        'strReturn1 = Request.Params("RTN")
        'strReturn = strReturn1 & "&RTN2=" & strReturn2 & "&VO=" & ViewValue
        'Response.Redirect(strReturn)
        If Request.Params("RTNRESULTSTT") = Nothing Or Request.Params("RTNRESULTSTT") = "" Then
            'Returning to IC_EditTransfer Page
            Dim strReturn1
            Dim strReturn2
            Dim RTNPage As String
            RTNPage = "IC_ResultsTT"
            strReturn2 = Request.Params("RFC") 'return query for getting from IC_EditTransfer to ResultsTT
            If InStr(1, strReturn2, RTNPage) > 0 Then
                'Go back to IC_ResultsTT
                strReturn2 = "IC_ResultsTT.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID
            Else
                'Go Back to IC_Result
                strReturn2 = "IC_Results.aspx?" & "&FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum
            End If
            ViewValue = Request.Params("VO")   'View mode for IC_EditTransfer
            'strReturn2 = Request.Params("RFC") 'return query for getting from IC_EditTransfer to ResultsTT
            strReturn1 = Request.Params("IET") 'return query for getting back to IC_EditTransfer
            strReturn = strReturn1 & "&RFC=" & strReturn2 & "&VO=" & ViewValue
            Response.Redirect(strReturn)
        Else
            'Returning to IC_ResultsTT Page
            'strReturn = Request.Params("RTNRESULTSTT")
            'Response.Redirect(strReturn)
            If GBID = Nothing Then
                Response.Redirect("IC_ResultsTT.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum)
            Else
                Response.Redirect("IC_ResultsTT.aspx?FS=" & FStore & "&FDIS=" & FDis & "&FREG=" & FReg & "&NFI=" & NoFInternet & "&TS=" & TStore & "&TREG=" & TReg & "&TDIS=" & TDis & "&NTI=" & NoTInternet & "&G=" & GERS & "&DS=" & Discrep & "&TT=" & TType & "&TranS=" & TStatus & "&FD=" & FDate & "&TD=" & TDate & "&DFQ=" & DFQty & "&DTQ=" & DTQty & "&DFA=" & DFAmt & "&DTA=" & DTAmt & "&TKN=" & TrackNum & "&TSN=" & TransNum & "&GERSBID=" & GBID)
            End If
        End If

    End Sub
End Class
