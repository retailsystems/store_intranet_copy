
Imports System.Data.OleDb


Public Class StoreResults
    Inherits System.Web.UI.Page
    Protected WithEvents dgSearch As System.Web.UI.WebControls.DataGrid

    'connStrings 
    Dim connHeader As System.Data.SqlClient.SqlConnection
    Dim connOra As System.Data.OleDb.OleDbConnection

    'global Search String 
    Dim strSearch As String
    Protected WithEvents btnNewSearch As System.Web.UI.WebControls.Button

    'New - see actSearch
    Dim myDataTable As DataTable

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            strSearch = Request.Params("SQL")

            'New
            ViewState("strSrch") = strSearch

            If strSearch <> "" Then
                Call actSearch(strSearch)
            End If
        End If

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnHeaderOpen()

        'connection
        connHeader = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        connHeader.Open()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnHeaderClose()

        connHeader.Close()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub actSearch(ByVal CommandString)

        ConnHeaderOpen()

        'commands
        Dim cmdHeader As System.Data.SqlClient.SqlCommand

        'Execution 
        cmdHeader = New SqlClient.SqlCommand(CommandString, connHeader)
        cmdHeader.ExecuteNonQuery()

        With dgSearch
            .AllowPaging = True
            .PagerStyle.Mode = PagerMode.NumericPages
            .PagerStyle.PageButtonCount = 5
            .PageSize = 5
        End With

        'Binding to DataGrid - Working - OLD 
        'Dim ListStores As System.Data.SqlClient.SqlDataReader
        'ListStores = cmdHeader.ExecuteReader()
        'dgSearch.DataSource = ListStores
        'dgSearch.DataBind()

        'New 
        'dec datatable in global 
        Dim myDataAdapter As New System.Data.SqlClient.SqlDataAdapter(CommandString, connHeader)
        Dim myDataSet As New DataSet()

        myDataAdapter.Fill(myDataSet, "Stores")
        myDataTable = myDataSet.Tables(0)
        dgSearch.DataSource = myDataTable
        dgSearch.DataBind()

        If Not Page.IsPostBack Then
            dgSearch.DataBind()
        End If

        'Close Conn
        ConnHeaderClose()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub dgSearch_PageIndexChanged(ByVal Source As Object, _
    ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgSearch.PageIndexChanged

        dgSearch.CurrentPageIndex = e.NewPageIndex

        Dim strSearch As String
        strSearch = ViewState("strSrch")

        Call actSearch(strSearch)

        dgSearch.DataBind()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnNewSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Response.Redirect("StoreSearch.aspx")

    End Sub

End Class
