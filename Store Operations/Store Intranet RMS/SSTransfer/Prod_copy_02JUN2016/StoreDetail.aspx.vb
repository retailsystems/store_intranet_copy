
Imports System.Data.OleDb

Public Class StoreDetail

    Inherits System.Web.UI.Page

    'connStrings 
    Dim connHeader As System.Data.SqlClient.SqlConnection
    Dim connOra As System.Data.OleDb.OleDbConnection

    'Data passed in by POST
    Private pStoreID As Integer
    Public pEmp As Integer

    'Local Buckets for Store Data
    Dim gStoNum As Integer '1
    Dim gStoName As String '2
    Dim gAddr As String '3
    Dim gState As String '4 - nulling out on extract
    Dim gZip As String '5
    Dim gPhone1 As String '6
    Dim gPhone2 As String '7
    Dim gEmail As String '8
    Dim gOpen As Date '9
    Dim gDim As Integer '10
    Dim gRegion As Integer '11 - nulling out on extract
    Dim gDist As Integer '12 - nulling out on extract
    Dim gStoMgr As String '13
    Dim gAM1 As String '14
    Dim gAM2 As String '15
    Dim gAM3 As String '16
    Dim gAM4 As String '17
    Dim gAM5 As String '18
    Dim gTags As String '19
    Dim gSimon As String '20
    Dim gCity As String '21
    Dim gCoName As String '20
    Dim gShipNumb As String '21

    'Local Buckets for Initial Data Pull - update
    Dim eStoNum As Integer '1
    Dim eStoName As String '2
    Dim eAddr As String '3
    Dim eState As String '4 - nulling out on extract
    Dim eZip As String '5
    Dim ePhone1 As String '6
    Dim ePhone2 As String '7
    Dim eEmail As String '8
    Dim eOpen As Date '9
    Dim eDim As Integer '10
    Dim eRegion As Integer '11 - nulling out on extract
    Dim eDist As Integer '12 - nulling out on extract
    Dim eStoMgr As String '13
    Dim eAM1 As String '14
    Dim eAM2 As String '15
    Dim eAM3 As String '16
    Dim eAM4 As String '17
    Dim eAM5 As String '18
    Dim eTags As String '19
    Dim eSimon As String '20
    Dim eCity As String '21
    Dim eCoName As String '21
    Dim eShipNumb As String '21


    Protected WithEvents btnNewSearch As System.Web.UI.WebControls.Button
    Protected WithEvents lblSaved As System.Web.UI.WebControls.Label
    Protected WithEvents lblRecordDeleted As System.Web.UI.WebControls.Label
    Protected WithEvents btnOK As System.Web.UI.WebControls.Button
    Protected WithEvents lblStoreNum As System.Web.UI.WebControls.Label
    Protected WithEvents txtStoNum As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbtStoSize As System.Web.UI.WebControls.Label
    Protected WithEvents txtStoDim As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblStoreName As System.Web.UI.WebControls.Label
    Protected WithEvents txtStoName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblRegion As System.Web.UI.WebControls.Label
    Protected WithEvents cboRegion As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblStoAddr As System.Web.UI.WebControls.Label
    Protected WithEvents txtAddr As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblDist As System.Web.UI.WebControls.Label
    Protected WithEvents cboDistrict As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblCity As System.Web.UI.WebControls.Label
    Protected WithEvents txtCity As System.Web.UI.WebControls.TextBox
    Protected WithEvents labManager As System.Web.UI.WebControls.Label
    Protected WithEvents txtStoMgr As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblState As System.Web.UI.WebControls.Label
    Protected WithEvents cboState As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblAsstMgr1 As System.Web.UI.WebControls.Label
    Protected WithEvents txtAsstMgr1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblZip As System.Web.UI.WebControls.Label
    Protected WithEvents txtZip As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblAsstMgr2 As System.Web.UI.WebControls.Label
    Protected WithEvents txtAsstMgr2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblPhone1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblAsstMgr3 As System.Web.UI.WebControls.Label
    Protected WithEvents txtAsstMgr3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblPhone2 As System.Web.UI.WebControls.Label
    Protected WithEvents txtPhone2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblAsstMgr4 As System.Web.UI.WebControls.Label
    Protected WithEvents txtAsstMgr4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEMail As System.Web.UI.WebControls.Label
    Protected WithEvents txtEmail As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblAsstMgr5 As System.Web.UI.WebControls.Label
    Protected WithEvents txtAsstMgr5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblOpenDate As System.Web.UI.WebControls.Label
    Protected WithEvents txtOpenDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblDateFormat As System.Web.UI.WebControls.Label
    Protected WithEvents lblTags As System.Web.UI.WebControls.Label
    Protected WithEvents txtTags As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSimon As System.Web.UI.WebControls.Label
    Protected WithEvents txtSimon As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnModify As System.Web.UI.WebControls.Button
    Protected WithEvents btnDelete As System.Web.UI.WebControls.Button
    Protected WithEvents txtPhone1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblP1Format As System.Web.UI.WebControls.Label
    Protected WithEvents lblP2Format As System.Web.UI.WebControls.Label

    'Protected WithEvents lblCoName As System.Web.UI.WebControls.Label
    'Protected WithEvents txtCoName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblShipNumb As System.Web.UI.WebControls.Label
    Protected WithEvents txtShipNumb As System.Web.UI.WebControls.TextBox


    'Abort Flag
    Dim gloAbort As Integer

    'Local Buckets for Store Data
    Dim gErrStoNum As Integer '1
    Dim gErrStoName As Integer '2
    Dim gErrAddr As Integer '3
    Dim gErrState As Integer  '4 - nulling out on extract
    Dim gErrZip As Integer  '5
    Dim gErrPhone1 As Integer  '6
    Dim gErrPhone2 As Integer  '7
    Dim gErrEmail As Integer '8
    Dim gErrOpen As Integer  '9
    Dim gErrDim As Integer  '10
    Dim gErrRegion As Integer '11 - nulling out on extract
    Dim gErrDist As Integer '12 - nulling out on extract
    Dim gErrStoMgr As Integer  '13
    Dim gErrAM1 As Integer  '14
    Dim gErrAM2 As Integer  '15
    Dim gErrAM3 As Integer  '16
    Dim gErrAM4 As Integer  '17
    Dim gErrAM5 As Integer  '18
    Dim gErrTags As Integer  '19
    Dim gErrSimon As Integer  '20
    Dim gErrCity As Integer '21
    Dim gErrCoName As Integer  '20
    Dim gErrShipNumb As Integer '21

    Dim gCount As Integer
    Protected WithEvents lblCoName As System.Web.UI.WebControls.Label
    Protected WithEvents txtCoName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblShipNumbb As System.Web.UI.WebControls.Label
    Protected WithEvents txtShipNumbb As System.Web.UI.WebControls.TextBox
    Dim gCt As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblSaved.Visible = False
        lblSaved.Text = "Default"
        lblRecordDeleted.Visible = False
        lblRecordDeleted.Text = "Default Text"

        ExtractFields()

        'get any var passed into this page
        pStoreID = Request.QueryString("pStoreID")

        'Display an old record
        If gCt = 0 Then
            If pStoreID <> Nothing Then
                actSearchOne(pStoreID)
            End If
        End If

        pEmp = Request.QueryString("Empty")

        If pEmp = 1 Then 'Adding New Store

            btnDelete.Visible = False
            btnOK.Visible = False
            btnModify.Text = "Add Record"

            If eRegion = Nothing Then
                build_DDL_DisplayRegion()
            End If

            If eState = Nothing Then
                build_DDL_DisplayState()
            End If

            If eDist = Nothing Then
                build_DDL_DisplayDistrict()
            End If

        Else 'Modify a Store

            btnDelete.Visible = True
            btnOK.Visible = False
            btnModify.Text = "Modify Record"

        End If

    End Sub

    '-----------------------------------------------------------------------------
    'Extract Fields - Each time the page is loaded, data is pulled so that it can be
    'used in the update store action 
    '
    '-----------------------------------------------------------------------------

    Public Sub ExtractFields()

        Dim Count As Integer

        '(1) Num - Must Integer
        If txtStoNum.Text <> "" Then
            eStoNum = Trim(CInt(txtStoNum.Text))
            Count = Count + 1
        End If


        '(2) Name - Must String
        If txtStoName.Text <> "" Then
            eStoName = Trim(CStr(txtStoName.Text))
            Count = Count + 1
        End If


        '(3) Addr - Must String
        If txtAddr.Text <> "" Then
            eAddr = Trim(CStr(txtAddr.Text))
            Count = Count + 1
        End If


        '(4) State - Must String
        If Not cboState.SelectedIndex <= 0 Then
            eState = Trim(CStr(cboState.SelectedItem.Text))
            Count = Count + 1
        End If


        '(5) Zip - Must String
        If txtZip.Text <> "" Then
            eZip = CStr(txtZip.Text)
            Count = Count + 1
        End If


        '(6) Must String
        If txtPhone1.Text <> "" Then
            ePhone1 = CStr(txtPhone1.Text)
            Count = Count + 1
        End If


        '(7) Must String
        If txtPhone2.Text <> "" Then
            ePhone2 = CStr(txtPhone2.Text)
            Count = Count + 1
        End If

        '(8) Must String
        If txtEmail.Text <> "" Then
            eEmail = CStr(txtEmail.Text)
            Count = Count + 1
        End If

        '(9) Must Date

        If txtOpenDate.Text <> "" Then
            If IsDate(txtOpenDate.Text) Then
                eOpen = CDate(txtOpenDate.Text)
                Count = Count + 1
            End If
        End If


        '(10) Must Integer   
        If txtStoDim.Text <> "" Then
            eDim = CInt(txtStoDim.Text)
            Count = Count + 1
        End If


        '(11) Region Integer
        If Not cboRegion.SelectedIndex <= 0 Then
            eRegion = Trim(CInt(cboRegion.SelectedItem.Text))
            Count = Count + 1
        End If


        '(12) Dist Integer
        If Not cboDistrict.SelectedIndex <= 0 Then
            eDist = Trim(CInt(cboDistrict.SelectedItem.Text))
            Count = Count + 1
        End If


        '(13) Must String
        If txtStoMgr.Text <> "" Then
            eStoMgr = CStr(txtStoMgr.Text)
            Count = Count + 1
        End If


        '(14) Must String
        If txtAsstMgr1.Text <> "" Then
            eAM1 = CStr(txtAsstMgr1.Text)
            Count = Count + 1
        End If


        '(15) Must String
        If txtAsstMgr2.Text <> "" Then
            eAM2 = CStr(txtAsstMgr2.Text)
            Count = Count + 1
        End If


        '(16) Must String
        If txtAsstMgr3.Text <> "" Then
            eAM3 = CStr(txtAsstMgr3.Text)
            Count = Count + 1
        End If


        '(17) Must String
        If txtAsstMgr4.Text <> "" Then
            eAM4 = CStr(txtAsstMgr4.Text)
            Count = Count + 1
        End If


        '(18) Must String
        If txtAsstMgr5.Text <> "" Then
            eAM5 = CStr(txtAsstMgr5.Text)
            Count = Count + 1
        End If


        '(19) Must String
        If txtTags.Text <> "" Then
            eTags = CStr(txtTags.Text)
            Count = Count + 1
        End If


        '(20) Must String
        If txtSimon.Text <> "" Then
            eSimon = CStr(txtSimon.Text)
            Count = Count + 1
        End If


        '(21) Must String
        If txtCity.Text <> "" Then
            eCity = CStr(txtCity.Text)
            Count = Count + 1
        End If

        '(22) Must String
        If txtCoName.Text <> "" Then
            eCoName = CStr(txtCoName.Text)
            Count = Count + 1
        End If

        '(23) Must String
        If txtShipNumb.Text <> "" Then
            eShipNumb = CStr(txtShipNumb.Text)
            Count = Count + 1
        End If

        gCt = Count

    End Sub


    '-----------------------------------------------------------------------------
    'clears the cells in the store display area
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub ClearCells()

        'Num (1)
        txtStoNum.Text = Nothing

        'Name(2)
        txtStoName.Text = ""

        'Addr (3)
        txtAddr.Text = ""

        'City (4)
        txtCity.Text = ""

        'State (5)
        cboState.SelectedIndex = 0

        'Zip (6) 
        txtZip.Text = ""

        'Phone1 (7) 
        txtPhone1.Text = ""

        'Phone2 (8) 
        txtPhone2.Text = ""

        'Email (9) 
        txtEmail.Text = ""

        'Open (10) 
        txtOpenDate.Text = ""

        'Dim (11)
        txtStoDim.Text = ""

        'Reg (12) 
        cboRegion.SelectedIndex = 0

        'District (13) 
        cboDistrict.SelectedIndex = 0

        'GM (14) 
        txtStoMgr.Text = ""

        'AM1 (15) 
        txtAsstMgr1.Text = ""

        'AM2 (16)
        txtAsstMgr2.Text = ""

        'AM3 (17) 
        txtAsstMgr3.Text = ""

        'AM4 (18) 
        txtAsstMgr4.Text = ""

        'AM5 (19) 
        txtAsstMgr5.Text = ""

        'Tags (20) 
        txtTags.Text = ""

        'Simon (21) 
        txtSimon.Text = ""

        'Company Name (22) 
        txtCoName.Text = ""

        'Shipper Number (23) 
        txtShipNumb.Text = ""



    End Sub
    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------


    Private Sub btnNewSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewSearch.Click

        Response.Redirect("StoreSearch.aspx")

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_DisplayRegion()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(Region) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboRegion.DataSource = dataTable.DefaultView
        cboRegion.DataTextField = "Region"
        cboRegion.DataBind()

        cboRegion.Enabled() = False

        ConnHeaderClose()

    End Sub


    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_DisplayDistrict()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(District) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboDistrict.DataSource = dataTable.DefaultView
        cboDistrict.DataTextField = "District"
        cboDistrict.DataBind()

        cboDistrict.Enabled = False

        ConnHeaderClose()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_DisplayState()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(State) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboState.DataSource = dataTable.DefaultView
        cboState.DataTextField = "State"
        cboState.DataValueField = "State"
        cboState.DataBind()
        cboState.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnHeaderOpen()

        'connection
        connHeader = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        connHeader.Open()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnHeaderClose()

        connHeader.Close()

    End Sub

    '-----------------------------------------------------------------------------
    'Searches and assigns the values to the text fields
    '
    '
    '-----------------------------------------------------------------------------


    Public Sub actSearchOne(ByVal pStoreID)

        ConnHeaderOpen()

        'commands
        Dim cmdHeader As System.Data.SqlClient.SqlCommand

        'parameters
        Dim pStoNum As SqlClient.SqlParameter
        Dim pStoName As SqlClient.SqlParameter
        Dim pAddr As SqlClient.SqlParameter
        Dim pCity As SqlClient.SqlParameter
        Dim pState As SqlClient.SqlParameter
        Dim pZip As SqlClient.SqlParameter
        Dim pPhone1 As SqlClient.SqlParameter
        Dim pPhone2 As SqlClient.SqlParameter
        Dim pEmail As SqlClient.SqlParameter
        Dim pOpen As SqlClient.SqlParameter
        Dim pDim As SqlClient.SqlParameter
        Dim pReg As SqlClient.SqlParameter
        Dim pDist As SqlClient.SqlParameter
        Dim pGM As SqlClient.SqlParameter
        Dim pAM1 As SqlClient.SqlParameter
        Dim pAM2 As SqlClient.SqlParameter
        Dim pAM3 As SqlClient.SqlParameter
        Dim pAM4 As SqlClient.SqlParameter
        Dim pAM5 As SqlClient.SqlParameter
        Dim pTags As SqlClient.SqlParameter
        Dim pSimon As SqlClient.SqlParameter
        Dim pCoName As SqlClient.SqlParameter
        Dim pShipNumb As SqlClient.SqlParameter

        'inputs (1) 
        cmdHeader = New SqlClient.SqlCommand("spFindStoreData", connHeader)
        cmdHeader.CommandType = CommandType.StoredProcedure
        cmdHeader.Parameters.Add("@StoreID", pStoreID)

        'outputs
        pStoNum = cmdHeader.Parameters.Add("@SIDOut", SqlDbType.VarChar)
        pStoName = cmdHeader.Parameters.Add("@SNameOut", SqlDbType.VarChar)
        pAddr = cmdHeader.Parameters.Add("@SAddrOut", SqlDbType.VarChar)
        pCity = cmdHeader.Parameters.Add("@SCityOut", SqlDbType.VarChar)
        pState = cmdHeader.Parameters.Add("@SStateOut", SqlDbType.VarChar)
        pZip = cmdHeader.Parameters.Add("@SZipOut", SqlDbType.VarChar)
        pPhone1 = cmdHeader.Parameters.Add("@SPhone1Out", SqlDbType.VarChar)
        pPhone2 = cmdHeader.Parameters.Add("@SPhone2Out", SqlDbType.VarChar)
        pEmail = cmdHeader.Parameters.Add("@SEmailOut", SqlDbType.VarChar)
        pOpen = cmdHeader.Parameters.Add("@SOpenOut", SqlDbType.VarChar)
        pDim = cmdHeader.Parameters.Add("@SDimOut", SqlDbType.VarChar)
        pReg = cmdHeader.Parameters.Add("@SRegionOut", SqlDbType.VarChar)
        pDist = cmdHeader.Parameters.Add("@SDistrictOut", SqlDbType.VarChar)

        pGM = cmdHeader.Parameters.Add("@SMgrOut", SqlDbType.VarChar)
        pAM1 = cmdHeader.Parameters.Add("@SAM1Out", SqlDbType.VarChar)
        pAM2 = cmdHeader.Parameters.Add("@SAM2Out", SqlDbType.VarChar)
        pAM3 = cmdHeader.Parameters.Add("@SAM3Out", SqlDbType.VarChar)
        pAM4 = cmdHeader.Parameters.Add("@SAM4Out", SqlDbType.VarChar)
        pAM5 = cmdHeader.Parameters.Add("@SAM5Out", SqlDbType.VarChar)
        pTags = cmdHeader.Parameters.Add("@STagsOut", SqlDbType.VarChar)
        pSimon = cmdHeader.Parameters.Add("@SSimonOut", SqlDbType.VarChar)
        pCoName = cmdHeader.Parameters.Add("@SCoNameOut", SqlDbType.VarChar)
        pShipNumb = cmdHeader.Parameters.Add("@SShipNumbOut", SqlDbType.VarChar)

        'Declare Size
        pStoNum.Size = 100
        pStoName.Size = 50
        pAddr.Size = 100
        pCity.Size = 300
        pState.Size = 100
        pZip.Size = 50
        pPhone1.Size = 50
        pPhone2.Size = 50
        pEmail.Size = 50
        pOpen.Size = 100
        pDim.Size = 50
        pReg.Size = 10
        pDist.Size = 10
        pGM.Size = 50
        pAM1.Size = 50
        pAM2.Size = 50
        pAM3.Size = 50
        pAM4.Size = 50
        pAM5.Size = 50
        pTags.Size = 1
        pSimon.Size = 1
        pCoName.Size = 25
        pShipNumb.Size = 10

        'Declare Direction
        pStoNum.Direction = ParameterDirection.Output
        pStoName.Direction = ParameterDirection.Output
        pAddr.Direction = ParameterDirection.Output
        pCity.Direction = ParameterDirection.Output
        pState.Direction = ParameterDirection.Output
        pZip.Direction = ParameterDirection.Output
        pPhone1.Direction = ParameterDirection.Output
        pPhone2.Direction = ParameterDirection.Output
        pEmail.Direction = ParameterDirection.Output
        pOpen.Direction = ParameterDirection.Output
        pDim.Direction = ParameterDirection.Output
        pReg.Direction = ParameterDirection.Output
        pDist.Direction = ParameterDirection.Output
        pGM.Direction = ParameterDirection.Output
        pAM1.Direction = ParameterDirection.Output
        pAM2.Direction = ParameterDirection.Output
        pAM3.Direction = ParameterDirection.Output
        pAM4.Direction = ParameterDirection.Output
        pAM5.Direction = ParameterDirection.Output
        pTags.Direction = ParameterDirection.Output
        pSimon.Direction = ParameterDirection.Output
        pCoName.Direction = ParameterDirection.Output
        pShipNumb.Direction = ParameterDirection.Output

        'execute
        cmdHeader.ExecuteNonQuery()


        'Build the State Drop Down in Display Area
        Call build_DDL_DisplayState()
        Call build_DDL_DisplayRegion()
        Call build_DDL_DisplayDistrict()


        '#
        If IsDBNull(pStoNum.Value) Then
            txtStoNum.Text = ""
        Else
            txtStoNum.Text = pStoNum.Value
        End If

        'Name(2)
        If IsDBNull(pStoName.Value) Then
            txtStoName.Text = ""
        Else
            txtStoName.Text = pStoName.Value
        End If

        'Addr (3)
        If IsDBNull(pAddr.Value) Then
            txtAddr.Text = ""
        Else
            txtAddr.Text = pAddr.Value
        End If

        'City (4)
        If IsDBNull(pCity.Value) Then
            txtCity.Text = ""
        Else
            txtCity.Text = pCity.Value
        End If

        'State (5)
        If IsDBNull(pState.Value) Then
            cboState.SelectedIndex = 0
        Else
            cboState.SelectedIndex = cboState.Items.IndexOf(cboState.Items.FindByValue(pState.Value))
        End If

        'Zip (6) 
        If IsDBNull(pZip.Value) Then
            txtZip.Text = ""
        Else
            txtZip.Text = pZip.Value
        End If

        'Phone1 (7) 
        If IsDBNull(pPhone1.Value) Then
            txtPhone1.Text = ""
        Else
            txtPhone1.Text = pPhone1.Value
        End If

        'Phone2 (8) 
        If IsDBNull(pPhone2.Value) Then
            txtPhone2.Text = ""
        Else
            txtPhone2.Text = pPhone2.Value
        End If

        'Email (9) 
        If IsDBNull(pEmail.Value) Then
            txtEmail.Text = ""
        Else
            txtEmail.Text = pEmail.Value
        End If

        'Open (10) 
        If IsDBNull(pOpen.Value) Then
            txtOpenDate.Text = ""
        Else
            txtOpenDate.Text = Left(pOpen.Value, 12)
        End If

        'Dim (11)
        If IsDBNull(pDim.Value) Then
            txtStoDim.Text = ""
        Else
            txtStoDim.Text = pDim.Value
        End If

        'Reg (12) 
        If IsDBNull(pReg.Value) Then
            cboRegion.SelectedIndex = 0
        Else
            cboRegion.SelectedIndex = cboRegion.Items.IndexOf(cboRegion.Items.FindByValue(pReg.Value))
        End If

        'District (13) 
        If IsDBNull(pDist.Value) Then
            cboDistrict.SelectedIndex = 0
        Else
            cboDistrict.SelectedIndex = cboDistrict.Items.IndexOf(cboDistrict.Items.FindByValue(pDist.Value))
        End If

        'GM (14) 
        If IsDBNull(pGM.Value) Then
            txtStoMgr.Text = ""
        Else
            txtStoMgr.Text = pGM.Value
        End If

        'AM1 (15) 
        If IsDBNull(pAM1.Value) Then
            txtAsstMgr1.Text = ""
        Else
            txtAsstMgr1.Text = pAM1.Value
        End If

        'AM2 (16)
        If IsDBNull(pAM2.Value) Then
            txtAsstMgr2.Text = ""
        Else
            txtAsstMgr2.Text = pAM2.Value
        End If

        'AM3 (17) 
        If IsDBNull(pAM3.Value) Then
            txtAsstMgr3.Text = ""
        Else
            txtAsstMgr3.Text = pAM3.Value
        End If

        'AM4 (18) 
        If IsDBNull(pAM4.Value) Then
            txtAsstMgr4.Text = ""
        Else
            txtAsstMgr4.Text = pAM4.Value
        End If

        'AM5 (19) 
        If IsDBNull(pAM5.Value) Then
            txtAsstMgr5.Text = ""
        Else
            txtAsstMgr5.Text = pAM5.Value
        End If

        'Tags (20) 
        If IsDBNull(pTags.Value) Then
            txtTags.Text = ""
        Else
            txtTags.Text = pTags.Value
        End If

        'Simon (21) 
        If IsDBNull(pSimon.Value) Then
            txtSimon.Text = ""
        Else
            txtSimon.Text = pSimon.Value
        End If

        'Company Name (22) 
        If IsDBNull(pCoName.Value) Then
            txtCoName.Text = ""
        Else
            txtCoName.Text = pCoName.Value
        End If

        'Shipper Number (23) 
        If IsDBNull(pShipNumb.Value) Then
            txtShipNumb.Text = ""
        Else
            txtShipNumb.Text = pShipNumb.Value
        End If
        'close
        ConnHeaderClose()

    End Sub 'end actSearchOne   

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub DisplayArea_Hide()

        lblStoAddr.Visible = False
        lblState.Visible = False
        lblZip.Visible = False
        lblOpenDate.Visible = False
        btnDelete.Visible = False
        btnModify.Visible = False
        txtAddr.Visible = False
        txtZip.Visible = False
        cboState.Visible = False
        txtOpenDate.Visible = False
        txtStoNum.Visible = False
        txtStoName.Visible = False
        lblPhone1.Visible = False
        lblPhone2.Visible = False
        lblEMail.Visible = False
        txtPhone1.Visible = False
        txtPhone2.Visible = False
        txtEmail.Visible = False
        txtStoDim.Visible = False
        lblRegion.Visible = False
        cboRegion.Visible = False
        lbtStoSize.Visible = False
        lblDist.Visible = False
        labManager.Visible = False
        lblAsstMgr1.Visible = False
        lblAsstMgr2.Visible = False
        cboDistrict.Visible = False
        lblAsstMgr3.Visible = False
        txtStoMgr.Visible = False
        txtAsstMgr1.Visible = False
        txtAsstMgr2.Visible = False
        txtAsstMgr3.Visible = False
        txtAsstMgr4.Visible = False
        lblAsstMgr4.Visible = False
        txtAsstMgr5.Visible = False
        lblAsstMgr5.Visible = False
        lblTags.Visible = False
        txtTags.Visible = False
        lblSimon.Visible = False
        txtSimon.Visible = False
        lblStoreNum.Visible = False
        lblStoreName.Visible = False

        lblCoName.Visible = False
        txtCoName.Visible = False
        lblShipNumb.Visible = False
        lblShipNumb.Visible = False

        lblCity.Visible = False
        txtCity.Visible = False

        lblDateFormat.Visible = False

        lblP1Format.Visible = False
        lblP2Format.Visible = False

    End Sub

    '-----------------------------------------------------------------------------
    'Transfers Data from Interface to Local Variables
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub DataToLocal()

        'flag .. all go
        gloAbort = 0

        'set the default (white) color of the display area
        DefaultDisplay()

        'reset error flags
        ResetErrs()



        '(1) Num - Must Integer
        If txtStoNum.Text = "" Then
            gloAbort = 1
            gErrStoNum = 1
        Else
            If Not IsNumeric(txtStoNum.Text) Then
                gloAbort = 1
                gErrStoNum = 1
            Else
                gStoNum = CInt(txtStoNum.Text)
            End If
        End If


        '(2) Name - Must String
        If txtStoName.Text = "" Then
            gloAbort = 1
            gErrStoName = 1
        Else
            If IsNumeric(txtStoName.Text) Then
                gloAbort = 1
                gErrStoName = 1
            Else
                gStoName = CStr(txtStoName.Text)
            End If
        End If


        '(3) Addr - Must String
        If txtAddr.Text = "" Then
            'If IsDBNull(txtAddr.Text) Then
            gloAbort = 1
            gErrAddr = 1
        Else
            If IsNumeric(txtAddr.Text) Then
                gloAbort = 1
                gErrAddr = 1
            Else
                gAddr = CStr(txtAddr.Text)
            End If
        End If


        'DataToLocal is not called until after PageLoad
        'hence combo boxes are never filled. Need to get
        'from eState - when the extraction is first called

        '(4) State - Must String
        If eState = "" Then
            gloAbort = 1
            gErrState = 1
        Else
            If IsNumeric(eState) Then
                gloAbort = 1
                gErrState = 1
            Else
                gState = eState
            End If
        End If


        '(4) State - Must String
        'If IsDBNull(cboState.SelectedItem.Value) Then
        '    gloAbort = 1
        '    gErrState = 1
        'Else
        '    If IsNumeric(cboState.SelectedItem.Text) Then
        '        gloAbort = 1
        '        gErrState = 1
        '    Else
        '        gState = CStr(cboState.SelectedItem.Text)
        '    End If
        'End If


        '(5) Zip - Must String
        If txtZip.Text = "" Then
            gloAbort = 1
            gErrZip = 1
        Else
            If Not IsNumeric(txtZip.Text) Then
                gloAbort = 1
                gErrZip = 1
            Else
                gZip = CStr(txtZip.Text)
            End If
        End If

        '(6) Must String
        If txtPhone1.Text = "" Then
            gloAbort = 1
            gErrPhone1 = 1
        Else
            If IsNumeric(txtPhone1.Text) Then
                gloAbort = 1
                gErrPhone1 = 1
            Else
                gPhone1 = CStr(txtPhone1.Text)
            End If
        End If

        '(7) Must String
        If txtPhone2.Text = "" Then
            gloAbort = 1
            gErrPhone2 = 1
        Else
            If IsNumeric(txtPhone2.Text) Then
                gloAbort = 1
                gErrPhone2 = 1
            Else
                gPhone2 = CStr(txtPhone2.Text)
            End If
        End If

        '(8) Must String
        If txtEmail.Text = "" Then
            gloAbort = 1
            gErrEmail = 1
        Else
            If IsNumeric(txtEmail.Text) Then
                gloAbort = 1
                gErrEmail = 1
            Else
                gEmail = CStr(txtEmail.Text)
            End If
        End If

        '(9) Must Date
        If txtOpenDate.Text = "" Then
            gloAbort = 1
            gErrOpen = 1
        Else
            If Not IsDate(txtOpenDate.Text) Then
                gloAbort = 1
                gErrOpen = 1
            Else

                gOpen = CDate(txtOpenDate.Text)
            End If
        End If


        '(10) Must Integer   'Can be Null
        If Not IsNumeric(txtStoDim.Text) Then
            gloAbort = 1
            gErrDim = 1
        Else
            gDim = CInt(txtStoDim.Text)
        End If


        '(11) Region Integer
        If eRegion = Nothing Then
            gloAbort = 1
            gErrRegion = 1
        Else
            If Not IsNumeric(eRegion) Then
                gloAbort = 1
                gErrRegion = 1
            Else
                gRegion = eRegion
            End If
        End If


        '(12) Dist Integer
        If eDist = Nothing Then
            gloAbort = 1
            gErrDist = 1
        Else
            If Not IsNumeric(eRegion) Then
                gloAbort = 1
                gErrDist = 1
            Else
                gDist = eDist
            End If
        End If


        '(13) Must String
        If IsDBNull(txtStoMgr.Text) Then
            gloAbort = 1
        Else
            If IsNumeric(txtStoMgr.Text) Then
                gloAbort = 1
                gErrStoMgr = 1
            Else
                gStoMgr = CStr(txtStoMgr.Text)
            End If
        End If


        '(14) Must String
        If IsNumeric(txtAsstMgr1.Text) Then
            gloAbort = 1
            gErrAM1 = 1
        Else
            gAM1 = CStr(txtAsstMgr1.Text)
        End If



        '(15) Must String
        If IsNumeric(txtAsstMgr2.Text) Then
            gloAbort = 1
            gErrAM2 = 1
        Else
            gAM2 = CStr(txtAsstMgr2.Text)
        End If


        '(16) Must String
        If IsNumeric(txtAsstMgr3.Text) Then
            gloAbort = 1
            gErrAM3 = 1
        Else
            gAM3 = CStr(txtAsstMgr3.Text)
        End If

        '(17) Must String
        If IsNumeric(txtAsstMgr4.Text) Then
            gloAbort = 1
            gErrAM4 = 1
        Else
            gAM4 = CStr(txtAsstMgr4.Text)
        End If

        '(18) Must String
        If IsNumeric(txtAsstMgr5.Text) Then
            gloAbort = 1
            gErrAM5 = 1
        Else
            gAM5 = CStr(txtAsstMgr5.Text)
        End If

        '(19) Must String
        If IsNumeric(txtTags.Text) Then
            gloAbort = 1
            gErrTags = 1
        Else
            gTags = CStr(txtTags.Text)
        End If

        '(20) Must String
        If IsNumeric(txtSimon.Text) Then
            gloAbort = 1
            gErrSimon = 1
        Else
            gSimon = CStr(txtSimon.Text)
        End If


        '(21) Must String
        If txtCity.Text = "" Then
            gloAbort = 1
            gErrCity = 1
        Else
            If IsNumeric(txtCity.Text) Then
                gloAbort = 1
                gErrCity = 1
            Else
                gCity = CStr(txtCity.Text)
            End If
        End If

        '(22) Must String
        If txtCoName.Text = "" Then
            gloAbort = 1
            gErrCoName = 1
        Else
            If IsNumeric(txtCoName.Text) Then
                gloAbort = 1
                gErrCoName = 1
            Else
                gCoName = CStr(txtCoName.Text)
            End If
        End If

        '(22) Must String
        If txtShipNumbb.Text = "" Then
            gloAbort = 1
            gErrShipNumb = 1
        Else
            If IsNumeric(txtShipNumb.Text) Then
                gloAbort = 1
                gErrShipNumb = 1
            Else
                gShipNumb = CStr(txtShipNumb.Text)
            End If
        End If
        DisplayErrs()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub DisplayErrs()


        If gErrStoNum = 1 Then
            txtStoNum.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrStoName = 1 Then
            txtStoName.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrAddr = 1 Then
            txtAddr.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrState = 1 Then
            cboState.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrZip = 1 Then
            txtZip.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrPhone1 = 1 Then
            txtPhone1.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrPhone2 = 1 Then
            txtPhone2.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrEmail = 1 Then
            txtEmail.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrOpen = 1 Then
            txtOpenDate.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrDim = 1 Then
            txtStoDim.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrRegion = 1 Then
            cboRegion.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrDist = 1 Then
            cboDistrict.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrStoMgr = 1 Then
            txtStoMgr.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrAM1 = 1 Then
            txtAsstMgr1.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrAM2 = 1 Then
            txtAsstMgr2.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrAM3 = 1 Then
            txtAsstMgr3.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrAM4 = 1 Then
            txtAsstMgr4.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrAM5 = 1 Then
            txtAsstMgr5.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrTags = 1 Then
            txtTags.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrSimon = 1 Then
            txtSimon.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrCoName = 1 Then
            txtCoName.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrShipNumb = 1 Then
            txtShipNumb.BackColor = System.Drawing.Color.Bisque
        End If

        If gErrCity = 1 Then
            txtCity.BackColor = System.Drawing.Color.Bisque
        End If

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ResetErrs()


        gloAbort = 0
        gCount = 0


        gErrStoNum = 0
        gErrStoName = 0
        gErrAddr = 0
        gErrState = 0
        gErrZip = 0
        gErrPhone1 = 0
        gErrPhone2 = 0
        gErrEmail = 0
        gErrOpen = 0
        gErrDim = 0
        gErrRegion = 0
        gErrDist = 0
        gErrStoMgr = 0
        gErrAM1 = 0
        gErrAM2 = 0
        gErrAM3 = 0
        gErrAM4 = 0
        gErrAM5 = 0
        gErrTags = 0
        gErrSimon = 0
        gErrCity = 0
        gCoName = 0
        gShipNumb = 0
    End Sub


    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub DefaultDisplay()

        txtStoNum.BackColor = System.Drawing.Color.White
        txtStoName.BackColor = System.Drawing.Color.White
        txtAddr.BackColor = System.Drawing.Color.White
        txtZip.BackColor = System.Drawing.Color.White
        txtPhone1.BackColor = System.Drawing.Color.White
        txtPhone2.BackColor = System.Drawing.Color.White
        txtEmail.BackColor = System.Drawing.Color.White
        txtOpenDate.BackColor = System.Drawing.Color.White
        txtStoDim.BackColor = System.Drawing.Color.White
        txtStoMgr.BackColor = System.Drawing.Color.White
        txtAsstMgr1.BackColor = System.Drawing.Color.White
        txtAsstMgr2.BackColor = System.Drawing.Color.White
        txtAsstMgr3.BackColor = System.Drawing.Color.White
        txtAsstMgr4.BackColor = System.Drawing.Color.White
        txtAsstMgr5.BackColor = System.Drawing.Color.White
        txtTags.BackColor = System.Drawing.Color.White
        txtSimon.BackColor = System.Drawing.Color.White
        txtCity.BackColor = System.Drawing.Color.White

        txtCoName.BackColor = System.Drawing.Color.White
        txtShipNumb.BackColor = System.Drawing.Color.White

        cboState.BackColor = System.Drawing.Color.White
        cboRegion.BackColor = System.Drawing.Color.White
        cboDistrict.BackColor = System.Drawing.Color.White

    End Sub
    '-----------------------------------------------------------------------------
    'Prelim Delete the record
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim Store As String

        DisplayArea_Hide()


        Store = Trim(txtStoNum.Text)

        lblSaved.Visible = True
        lblSaved.Text = "You will delete Store# " & Store & ". Are you sure?"
        btnOK.Visible = True

    End Sub

    '-----------------------------------------------------------------------------
    'Final Delete the record
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click

        Dim StoNum As String
        Dim StoNumF As Integer

        StoNum = Trim(txtStoNum.Text)
        StoNumF = CInt(StoNum)

        ActDelete(StoNumF)

        ClearCells()

        DisplayArea_Hide()
        btnOK.Visible = False

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub ActDelete(ByVal StoNumF)

        'open    
        ConnHeaderOpen()

        'commands
        Dim cmdHeader As System.Data.SqlClient.SqlCommand

        'inputs
        cmdHeader = New SqlClient.SqlCommand("spDeleteStore", connHeader)
        cmdHeader.CommandType = CommandType.StoredProcedure
        cmdHeader.Parameters.Add("@StoreNum", StoNumF)

        'exec
        cmdHeader.ExecuteNonQuery()

        lblSaved.Visible = True
        lblSaved.Text = "STORE " & StoNumF & " has been Deleted. "

        'close
        ConnHeaderClose()

    End Sub

    '-----------------------------------------------------------------------------
    'Passes Store Number in and checks if the record already exists
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub CheckDup(ByVal gStoNum)

        ConnHeaderOpen()

        'Commands()
        Dim cmdHeader As System.Data.SqlClient.SqlCommand

        'Vars
        Dim Count As SqlClient.SqlParameter

        'Specs
        cmdHeader = New SqlClient.SqlCommand("spCountStore", connHeader)
        cmdHeader.CommandType = CommandType.StoredProcedure
        cmdHeader.Parameters.Add("@StoNum", gStoNum)

        'Outputs
        Count = cmdHeader.Parameters.Add("@Count", SqlDbType.VarChar)
        Count.Size = 10
        Count.Direction = ParameterDirection.Output

        'Execute 
        cmdHeader.ExecuteNonQuery()

        gCount = Count.Value

        If gCount <> 0 Then
            lblSaved.Text = "A record with this number already exists."
            lblSaved.Visible = True
            gloAbort = 1 'kill the add process
        End If

        ConnHeaderClose()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub actInsertVars()

        'Txfer Data 
        DataToLocal()

        'Commands()
        Dim cmdHeader As System.Data.SqlClient.SqlCommand

        'If all fields are valid 
        If gloAbort = 0 Then

            'check if this store number already exists
            CheckDup(gStoNum)

        End If

        ConnHeaderOpen()

        'If all fields are valid
        If gloAbort = 0 Then

            'Inputs
            cmdHeader = New SqlClient.SqlCommand("spInsertStoreRow", connHeader)
            cmdHeader.CommandType = CommandType.StoredProcedure
            cmdHeader.Parameters.Add("@StoNum", gStoNum)
            cmdHeader.Parameters.Add("@StoName", gStoName)
            cmdHeader.Parameters.Add("@Addr", gAddr)
            cmdHeader.Parameters.Add("@City", gCity)
            cmdHeader.Parameters.Add("@State", gState)
            cmdHeader.Parameters.Add("@Zip", gZip)
            cmdHeader.Parameters.Add("@Phone1", gPhone1)
            cmdHeader.Parameters.Add("@Phone2", gPhone2)
            cmdHeader.Parameters.Add("@Email", gEmail)
            cmdHeader.Parameters.Add("@OpenDate", gOpen)
            cmdHeader.Parameters.Add("@Dimension", gDim)
            cmdHeader.Parameters.Add("@Region", gRegion)
            cmdHeader.Parameters.Add("@District", gDist)
            cmdHeader.Parameters.Add("@Manager", gStoMgr)
            cmdHeader.Parameters.Add("@AsstMgr1", gAM1)
            cmdHeader.Parameters.Add("@AsstMgr2", gAM2)
            cmdHeader.Parameters.Add("@AsstMgr3", gAM3)
            cmdHeader.Parameters.Add("@AsstMgr4", gAM4)
            cmdHeader.Parameters.Add("@AsstMgr5", gAM5)
            cmdHeader.Parameters.Add("@Tags", gTags)
            cmdHeader.Parameters.Add("@Simon", gSimon)
            cmdHeader.Parameters.Add("@CoName", gCoName)
            cmdHeader.Parameters.Add("@ShipNumb", gShipNumb)

            cmdHeader.ExecuteNonQuery()

            'Do if some inputs are Invalid
            lblSaved.Text = "This record has been SUCCESSFULLY ADDED."
            lblSaved.Visible = True

        Else  'Messaging

            If gCount > 0 Then
                lblSaved.Text = "A Store with this Store Number already exists. Add Store cancelled."
                lblSaved.Visible = True
            Else
                lblSaved.Text = "Formatting Error/Empty Fields Detected."
                lblSaved.Visible = True
            End If

        End If

        connHeader.Close()


    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub UpdateTable()

        'Txfer Data 
        DataToLocal() 'Err checking built in

        'Commands()
        Dim cmdHeader As System.Data.SqlClient.SqlCommand

        ConnHeaderOpen()

        If gCt = 13 Then

            'Inputs
            cmdHeader = New SqlClient.SqlCommand("spUpdateRow", connHeader)
            cmdHeader.CommandType = CommandType.StoredProcedure
            cmdHeader.Parameters.Add("@StoNum", eStoNum)
            cmdHeader.Parameters.Add("@StoName", eStoName)
            cmdHeader.Parameters.Add("@Addr", eAddr)
            cmdHeader.Parameters.Add("@City", eCity)
            cmdHeader.Parameters.Add("@State", eState)
            cmdHeader.Parameters.Add("@Zip", eZip)
            cmdHeader.Parameters.Add("@Phone1", ePhone1)
            cmdHeader.Parameters.Add("@Phone2", ePhone2)
            cmdHeader.Parameters.Add("@Email", eEmail)
            cmdHeader.Parameters.Add("@OpenDate", eOpen)
            cmdHeader.Parameters.Add("@Dimension", eDim)
            cmdHeader.Parameters.Add("@Region", eRegion)
            cmdHeader.Parameters.Add("@District", eDist)
            cmdHeader.Parameters.Add("@Manager", eStoMgr)
            cmdHeader.Parameters.Add("@AsstMgr1", eAM1)
            cmdHeader.Parameters.Add("@AsstMgr2", eAM2)
            cmdHeader.Parameters.Add("@AsstMgr3", eAM3)
            cmdHeader.Parameters.Add("@AsstMgr4", eAM4)
            cmdHeader.Parameters.Add("@AsstMgr5", eAM5)
            cmdHeader.Parameters.Add("@Tags", eTags)
            cmdHeader.Parameters.Add("@Simon", eSimon)
            cmdHeader.Parameters.Add("@CoName", eCoName)
            cmdHeader.Parameters.Add("@ShipNumb", eShipNumb)


            cmdHeader.ExecuteNonQuery()

            lblSaved.Text = "This record has been SUCCESSFULLY UPDATED."
            lblSaved.Visible = True

        Else

            lblSaved.Text = "Formatting/Empty Cells found on UPDATE Action. Update Cancelled."
            lblSaved.Visible = True

        End If

        connHeader.Close()

    End Sub

    '-----------------------------------------------------------------------------
    ' This is either the Add or Save Record button - forked based on flag passed in
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModify.Click

        If btnModify.Text = "Add Record" Then

            'check if all inputs are valid 
            If gloAbort = 0 Then

                'insert record
                actInsertVars()

            Else
                lblSaved.Text = "Formatting Error/Empty Fields Detected."
                lblSaved.Visible = True
            End If
        End If



        If btnModify.Text = "Modify Record" Then 'If this is an "Update Record" 
            If gloAbort = 0 Then

                'update record
                UpdateTable()

            Else
                lblSaved.Text = "Formatting Error/Empty Fields Detected."
                lblSaved.Visible = True
            End If
        End If


    End Sub

    Private Sub txtAsstMgr2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAsstMgr2.TextChanged

    End Sub

    Private Sub cboRegion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRegion.SelectedIndexChanged

    End Sub
End Class
