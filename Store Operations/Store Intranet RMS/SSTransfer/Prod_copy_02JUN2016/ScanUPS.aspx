<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ScanUPS.aspx.vb" Inherits="SSTransfer.ScanUPS"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Scan Item</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" alink="white" vlink="white" link="white">
		<form id="Form1" method="post" runat="server">
			<table borderColor="#990000" cellSpacing="0" borderColorDark="#990000" cellPadding="2" borderColorLight="#990000" border="3" width="350">
				<tr>
					<td align="middle" bgColor="#440000" colSpan="2"><asp:label id="lblHeader" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="medium" Font-Bold="True">Scan UPS</asp:label></td>
				</tr>
				<tr>
					<td bgColor="#000000">
						<table cellSpacing="0" cellPadding="3" border="0" width="100%">
							<tr>
								<td noWrap><asp:label id="lblScanItem" runat="server" Font-Names="Arial" EnableViewState="False" ForeColor="White" Font-Size="x-small" font-bold="True">Scan UPS:</asp:label></td>
								<td noWrap><asp:textbox id="txtScanItem" runat="server"></asp:textbox></td>
							</tr>
							<tr>
								<td align="middle" colspan="2">
									<asp:Button id="btnSubmit" runat="server" Text="Submit" ForeColor="White" BackColor="#440000" BorderColor="#990000" Font-Bold="True"></asp:Button>&nbsp;
									<input style="FONT-WEIGHT:bold;BORDER-LEFT-COLOR:#990000;BORDER-BOTTOM-COLOR:#990000;COLOR:white;BORDER-TOP-COLOR:#990000;BACKGROUND-COLOR:#440000;BORDER-RIGHT-COLOR:#990000" onclick="window.close()" type="button" value="Close">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
