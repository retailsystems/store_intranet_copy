<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IC_Results.aspx.vb" Inherits="SSTransfer.IC_Results"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>IC Results</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
	</HEAD>
	<body bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="frmICResults" onsubmit="return checkSubmit();" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<TBODY>
					<tr>
						<td noWrap colSpan="3" height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
					</tr>
					<tr>
					</tr>
					<tr vAlign="top" height="1%">
						<td noWrap><asp:label id="lblView" Font-Bold="True" ForeColor="White" Runat="server" Font-Size="X-Small" Font-Names="Arial">View</asp:label>&nbsp;
							<asp:dropdownlist id="ddlView" Runat="server" AutoPostBack="True">
								<asp:ListItem Value="TransferType">By Transfer Type</asp:ListItem>
								<asp:ListItem Value="Transfer" Selected="True">By Transfer</asp:ListItem>
							</asp:dropdownlist></td>
					</tr>
					<tr vAlign="top">
						<td noWrap><br>
							<asp:label id="lblResults" Font-Bold="True" ForeColor="white" Runat="server" Font-Size="small" Font-Name="arial"></asp:label></td>
					</tr>
					<tr vAlign="top">
						<td noWrap colSpan="3"><br>
							<div style="OVERFLOW-Y: auto; SCROLLBAR-HIGHLIGHT-COLOR: #990000; WIDTH: 100%;HEIGHT: 410px; SCROLLBAR-ARROW-COLOR: #ffffff; SCROLLBAR-BASE-COLOR: #440000;">
								<table cellSpacing="0" cellPadding="2" width="100%" border="0">
									<tr>
										<td>
											<asp:datagrid id="dgTTResults" Runat="server" Font-Size="X-Small" Font-Names="Arial" GridLines="None" BorderStyle="None" CellPadding="2" CellSpacing="1" BackColor="#111111" Width="100%" Height="100%" AutoGenerateColumns="False" AllowSorting="True" OnSortCommand="Sort">
												<AlternatingItemStyle BackColor="#333333"></AlternatingItemStyle>
												<ItemStyle Font-Size="X-Small" Font-Names="Arial" ForeColor="White"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Font-Names="arial" Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn HeaderText="Transfer Number" SortExpression="IC_Box_Trans_Status.Box_Id">
														<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="White" Width="12%" BackColor="#440000"></HeaderStyle>
														<ItemStyle HorizontalAlign="Left"></ItemStyle>
														<ItemTemplate>
															<a href='IC_EditTransfer.aspx?BID=<%# Container.dataitem("Box_Id") %>&RTN=<%# GetRTNEditTransfer() %>'>
																<asp:Label Runat="server" ID="lblTN" Text='<%# Container.dataitem("Box_Id") %>'>
																</asp:Label></a>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="Sending_Store" SortExpression="Box_Xfer_Hdr.Sending_Store_Cd" HeaderText="From Store"></asp:BoundColumn>
													<asp:BoundColumn DataField="Receiving_Store" SortExpression="Box_Xfer_Hdr.Receiving_Store_Cd" HeaderText="To Store"></asp:BoundColumn>
													<asp:TemplateColumn HeaderText="Type">
														<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#440000"></HeaderStyle>
														<ItemTemplate>
															<asp:Label Runat="server" Font-Name="arial" Font-Size="x-small" ForeColor="white" ID="lblTType">
																<%# GetBoxTransferTypesSD(Container.dataitem("Box_Id")) %>
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="Shipment_Date" SortExpression="Box_Xfer_Hdr.Shipment_Date" ReadOnly="True" HeaderText="Shipment Date" DataFormatString="{0:d}"></asp:BoundColumn>
													<asp:BoundColumn DataField="Status_Desc" SortExpression="Box_Status_Type.Status_Desc" HeaderText="Box Status"></asp:BoundColumn>
												</Columns>
											</asp:datagrid></td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td noWrap align="right"><asp:DropDownList Runat="server" ID="ddlOptions">
								<asp:ListItem Value="<OPTIONS>"></asp:ListItem>
								<asp:ListItem Value="Print Search Results"></asp:ListItem>
							</asp:DropDownList>
							<asp:button id="btnExecute" Font-Bold="True" ForeColor="White" Runat="server" Font-Size="X-Small" Font-Names="Arial" BackColor="#440000" BorderColor="#990000" Text="Execute"></asp:button></td>
					</tr>
					<tr>
						<td noWrap colSpan="3" height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
					</tr>
				</TBODY>
			</table>
		</form>
		</TR></TBODY></TABLE></FORM>
	</body>
</HTML>
