<%@ Register TagPrefix="uc1" TagName="SearchBox" Src="UserControls/SearchBox.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UPSManuelInput.aspx.vb" Inherits="SSTransfer.UPSManuelInput"%>
<%@ Register TagPrefix="uc1" TagName="ScanUPS" Src="UserControls/ScanUPS.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Login</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
	</HEAD>
	<body id="pageBody" runat="server" bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="Form1" method="post" runat="server" onsubmit="return checkSubmit();">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td vAlign="center">
						<table cellSpacing="0" cellPadding="4" width="100%" border="0">
							<tr>
								<td vAlign="center" align="middle"><IMG src="images\scan.gif" border="0"></td>
								<td vAlign="center" align="middle">
									<table cellSpacing="0" cellPadding="4" border="0">
										<tr>
											<td><asp:label id="lblInstruction" runat="server" Font-Names="Arial" Font-Bold="True" ForeColor="Red">Please use one of your pre-printed UPS labels by scanning in the tracking number.<br><br>You have selected the following tracking number:</asp:label></td>
										</tr>
										<tr>
											<td><asp:label id="lblTrackNum" runat="server" Font-Names="Arial" ForeColor="Red" Font-Size="large"></asp:label><asp:textbox id="txtTrackNum" runat="server" Width="380px"></asp:textbox></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td align="middle">
												<asp:button id="btnClear" runat="server" ForeColor="White" Visible="false" BackColor="#440000" Text="Clear" BorderColor="#990000"></asp:button>&nbsp;
												<asp:button id="btnAssign" runat="server" ForeColor="White" BackColor="#440000" Text="Assign Tracking Number" BorderColor="#990000" visible="false"></asp:button></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
		<script language="javascript">
			if(document.forms[0].txtTrackNum != null){
				document.forms[0].txtTrackNum.focus();
			}
		</script>
	</body>
</HTML>
