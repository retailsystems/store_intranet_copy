use hottopic2_dev
go

drop proc spphGetStoreFromSubnet
go
/*
Author:  Peter Ho
Date:    9.04.2003

Purpose: Get a store number from the requestor's ip (client)

Note: orig code from Dennis Prestia
*/
CREATE PROCEDURE spphGetStoreFromSubnet
  @StoreSubnet varchar(15),
  @StoreNum smallint OUTPUT
AS
  DECLARE @tmpReturn smallint

  SELECT @tmpReturn = StoreNum
  FROM  STORE
  WHERE  Subnet = @StoreSubnet

  IF(@tmpReturn IS NULL)
    SELECT @StoreNum = -1
  ELSE
    SELECT @StoreNum = @tmpReturn

Return
GO

 

declare @storenum smallint
set @storenum = -2
exec spphGetStoreFromSubnet '172.20.62.144',@storenum output
print @storenum

select * from store

select * from subnet


select * from store


