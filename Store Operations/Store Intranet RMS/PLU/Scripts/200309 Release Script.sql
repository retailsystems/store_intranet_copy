Use Hottopic2
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spphGetStoreFromSubnet]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spphGetStoreFromSubnet]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/*
Author:  Peter Ho
Date:    9.04.2003

Purpose: Get a store number from the requestor's ip (client)

Note: orig code from Dennis Prestia
*/
CREATE PROCEDURE spphGetStoreFromSubnet
  @StoreSubnet varchar(15),
  @StoreNum smallint OUTPUT
AS
  DECLARE @tmpReturn smallint

  SELECT @tmpReturn = StoreNum
  FROM  STORE
  WHERE  Subnet = @StoreSubnet

  IF(@tmpReturn IS NULL)
    SELECT @StoreNum = -1
  ELSE
    SELECT @StoreNum = @tmpReturn

Return

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



Use Torrid
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spphGetStoreFromSubnet]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spphGetStoreFromSubnet]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/*
Author:  Peter Ho
Date:    9.04.2003

Purpose: Get a store number from the requestor's ip (client)

Note: orig code from Dennis Prestia
*/
CREATE PROCEDURE spphGetStoreFromSubnet
  @StoreSubnet varchar(15),
  @StoreNum smallint OUTPUT
AS
  DECLARE @tmpReturn smallint

  SELECT @tmpReturn = StoreNum
  FROM  STORE
  WHERE  Subnet = @StoreSubnet

  IF(@tmpReturn IS NULL)
    SELECT @StoreNum = -1
  ELSE
    SELECT @StoreNum = @tmpReturn

Return

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

