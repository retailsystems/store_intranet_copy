<%@ Page Language="vb" AutoEventWireup="false" Codebehind="view_details.aspx.vb" Inherits="PLU.WebForm3"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Item Detail</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<META http-equiv="CACHE-CONTROL" content="NO-CACHE">
		<META http-equiv="PRAGMA" content="NO-CACHE">
		<META http-equiv="EXPIRES" content="0">
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/hottopic.css" type="text/css" rel="stylesheet" runat="server" />
		<base target="_self" />
  </HEAD>
	<body id="pageBody" scroll="no" runat="server" MS_Positioning="FLOWLayout">
		<form id="Form1" method="post" runat="server">
			<iframe class="Header_Frame" id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0"
				src="http://172.16.125.2/Header/Header.aspx?mode=hottopic" frameBorder="0" width="100%"
				scrolling="no" runat="server"></iframe>
			<table height="100%" width="100%">
				<tr>
					<td noWrap><asp:datagrid id="dgDetails" runat="server" CssClass="DatagridSm" AutoGenerateColumns="False"
							EnableViewState="True" GridLines="None" Width="100%">
							<ItemStyle CssClass="BC111111sm"></ItemStyle>
							<AlternatingItemStyle CssClass="BC333333sm"></AlternatingItemStyle>
							<HeaderStyle CssClass="DATAGRID_Headersm"></HeaderStyle>
							<Columns>
								<asp:ButtonColumn runat="Server" ButtonType="LinkButton" HeaderText="SKU" DataTextField="item" 
 CommandName="SKUSelected"></asp:ButtonColumn>
								<asp:BoundColumn DataField="itmdescrip" HeaderText="Description"></asp:BoundColumn>
								<asp:BoundColumn DataField="size_cd" HeaderText="Size"></asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Price">
											<ItemTemplate><asp:Label Runat="server" ID="Label2" Text="--">
														</asp:Label></ItemTemplate>
											</asp:TemplateColumn>		
								<asp:BoundColumn DataField="subclass" HeaderText="Subclass"></asp:BoundColumn>
								<asp:BoundColumn DataField="subclass_desc" HeaderText="Sub Description"></asp:BoundColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td vAlign="top">
						<table width="100%">
							<tr>
								<td vAlign="top" align="left" width="60%">
									<table width="100%">
										<tr>
											<td colSpan="2">
												<table class="normal" width="100%">
													<tr>
														<td noWrap><asp:label id="lblSKU" EnableViewState="True" Runat="server"></asp:label>&nbsp;&nbsp;&nbsp;
															<asp:label id="lblsize" Runat="server"></asp:label></td>
														<td class="Header_Title" noWrap align="right">Product Locator</td>
													</tr>
													<tr>
														<td noWrap width="100%" colSpan="2"><asp:radiobuttonlist id="rblArea" CssClass="normal" EnableViewState="True" Width="100%" Runat="server"
																RepeatDirection="Horizontal" AutoPostBack="True">
																<asp:ListItem Value="All">All</asp:ListItem>
																<asp:ListItem Value="Region">My&nbsp;Region</asp:ListItem>
																<asp:ListItem Value="District" Selected="True">My&nbsp;District</asp:ListItem>
																<asp:ListItem Value="Internet">Internet</asp:ListItem>
																<asp:ListItem Value="Store">My&nbsp;Store</asp:ListItem>
															</asp:radiobuttonlist></td>
														<td>Store:</td>
														<td><asp:textbox id="txtStore" EnableViewState="True" Width="52px" Runat="server"></asp:textbox></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
														<td align="right"><asp:button id="btnSubmit" tabIndex="8" runat="server" CssClass="b1" EnableViewState="False"
																Text="Submit"></asp:button></td>
													</tr>
													<tr>
														<td noWrap width="100%" colSpan="2"><asp:label id="lblResults" runat="server" CssClass="red"></asp:label><asp:label id="lblhidden" Width="0" Runat="server" visible="false"></asp:label></td>
													</tr>
													<tr>
														<td noWrap colSpan="4"><asp:datagrid id="dgPL" CssClass="DatagridSm" AutoGenerateColumns="False" GridLines="None" Width="100%"
																Runat="server" AllowPaging="True">
																<AlternatingItemStyle CssClass="BC333333sm"></AlternatingItemStyle>
																<ItemStyle CssClass="BC111111sm"></ItemStyle>
																<HeaderStyle CssClass="DATAGRID_Headersm"></HeaderStyle>
																<Columns>
																	<asp:BoundColumn DataField="store" HeaderText="Store"></asp:BoundColumn>
																	<asp:TemplateColumn HeaderText="Qty">
																		<HeaderStyle BorderWidth="0px" CssClass="DATAGRID_Headersm"></HeaderStyle>
																		<ItemTemplate>
																			<asp:Label Runat="server" ID="lblQTY" Text='<%# Container.dataitem("available_qty") %>'>
																			</asp:Label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Ship Date">
																		<HeaderStyle BorderWidth="0px" CssClass="DATAGRID_Headersm"></HeaderStyle>
																	
																		<ItemTemplate>
																			<asp:Label Runat="server" ID="lblShipDate" Text='<%# Container.dataitem("lst_rcvd_dt") %>'>
																			</asp:Label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Store Name">
																		<HeaderStyle BorderWidth="0px" CssClass="DATAGRID_Headersm"></HeaderStyle>
																		<ItemTemplate>
																			<a href='mailto:<%# GETEMAIL(Container.dataitem("store")) %>'>
																				<asp:Label Runat="server" ID="lblstorename" Text='<%# Container.dataitem("store_name") %>'>
																				</asp:Label></a>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Phone">
																		<HeaderStyle BorderWidth="0px" CssClass="DATAGRID_Headersm"></HeaderStyle>
																		<ItemTemplate>
																			<asp:Label Runat="server" ID="lblPhone" Text='<%# Phone(Container.dataitem("store")) %>'>
																			</asp:Label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																</Columns>
															</asp:datagrid></td>
													</tr>
													<tr>
														<td vAlign="top" noWrap align="left"><br>
															<input class="b1" id="btnClose" onclick="JavaScript:window.close();" type="button" value="Close"
																Width="88px" Height="27px"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td vAlign="top" align="right" width="60%">
									<table width="100%">
										<tr>
											<td>
												<table width="100%">
													<tr>
														<td></td>
													</tr>
													<tr>
														<td></td>
													</tr>
													<tr>
														<td noWrap align="right"><br>
															<br>
															<asp:label id="lblPhoto" CssClass="Red" Width="119px" Runat="server" Height="240px" Visible="False"
																text="No Photo Available."></asp:label></td>
													</tr>
													<tr>
														<td noWrap align="right"><asp:image id="imgItem" runat="server" EnableViewState="False" BorderStyle="None" ImageAlign="right"></asp:image></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
