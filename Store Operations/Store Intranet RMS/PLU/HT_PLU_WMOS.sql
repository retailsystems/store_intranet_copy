DROP MATERIALIZED VIEW HOTTOPIC.HT_PLU_WMOS;
--
-- HT_PLU_WMOS  (Materialized View) 
--
CREATE MATERIALIZED VIEW HOTTOPIC.HT_PLU_WMOS 
TABLESPACE HT_TAB
PCTUSED    80
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             4M
            MINEXTENTS       1
            MAXEXTENTS       8192
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOCACHE
LOGGING
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE
--START WITH TO_DATE('17-Apr-2008 11:44:48','dd-mon-yyyy hh24:mi:ss')
--NEXT SYSDATE + 1  
WITH PRIMARY KEY
AS 
SELECT /*+ all_rows */ /* HT_PLU_WMOS  refreshes materialized view */
       i.subclass_cd,
       s.des subdesc,
       i.ve_cd,
       v.ve_name,
       i.dept_cd,
       d.des deptdesc,
       k.sku_num,
       u.upc_cd,
       i.des2 smythdesc,
       i.des1 itmdescrip,
       k.size_cd,
       hottopic.ht_get_ret_prc.get_sku_ret_prc (k.sku_num) curr,
       TRUNC (SYSDATE) AS beg_dt,
       i.itm_cd,
       z.udf010,
       z.udf011,
       z.udf013
  FROM gm_itm i,
       subclass s,
       gm_sku k,
       ve v,
       dept d,
       gm_itm_udf z,
       (--get sku and upc code
        SELECT a.sku_num sku_num, b.upc_cd upc_cd
          FROM gm_sku a, gm_sku2upc_cd b
         WHERE a.sku_num = b.sku_num(+) AND b.upc_cd NOT LIKE '00%') u
 WHERE i.itm_cd = k.itm_cd
   AND z.itm_cd(+) = i.itm_cd
   AND i.ve_cd = v.ve_cd
   AND i.subclass_cd = s.subclass_cd
   AND i.dept_cd = d.dept_cd
   AND k.sku_num = u.sku_num(+)
   AND (i.dept_cd >= '0001' AND i.dept_cd <= '0049' OR i.subclass_cd = '999991')
   AND (i.grid_flag = 'N' OR i.grid_flag = 'Y' AND k.sku_num NOT LIKE '%000')
   AND i.lst_rcv_dt IS NOT NULL
   AND (k.sku_tp is null OR k.sku_tp <> 'PPK');

COMMENT ON TABLE HOTTOPIC.HT_PLU_WMOS IS 'snapshot table for snapshot HOTTOPIC.HT_PLU_WMOS';

--
-- HT_PLU_WMOS_DEPT_SBCLS  (Index) 
--
CREATE INDEX HOTTOPIC.HT_PLU_WMOS_DEPT_SBCLS ON HOTTOPIC.HT_PLU_WMOS
(DEPT_CD, SUBCLASS_CD)
LOGGING
TABLESPACE HT_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             4M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

--
-- HT_PLU_WMOS_ITM  (Index) 
--
CREATE INDEX HOTTOPIC.HT_PLU_WMOS_ITM ON HOTTOPIC.HT_PLU_WMOS
(ITM_CD)
LOGGING
TABLESPACE HT_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             4M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

--
-- HT_PLU_WMOS_SKU_NUM  (Index) 
--
CREATE INDEX HOTTOPIC.HT_PLU_WMOS_SKU_NUM ON HOTTOPIC.HT_PLU_WMOS
(SKU_NUM)
LOGGING
TABLESPACE HT_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             4M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

GRANT SELECT ON HOTTOPIC.HT_PLU_WMOS TO HOTTOPIC_INQ_ROLE;