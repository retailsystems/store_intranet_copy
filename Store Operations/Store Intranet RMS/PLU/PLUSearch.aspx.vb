Option Explicit On 

Imports System.Data.OleDb

Partial Class WebForm1

    Inherits System.Web.UI.Page

    Protected WithEvents lblDeptNo As System.Web.UI.WebControls.Label
    Protected WithEvents lblsubclass As System.Web.UI.WebControls.Label
    Protected WithEvents lblItem As System.Web.UI.WebControls.Label
    Protected WithEvents lblDescription As System.Web.UI.WebControls.Label
    Protected WithEvents DataView1 As System.Data.DataView
    Protected WithEvents lblMOS As System.Web.UI.WebControls.Label

	Private mblnChangeCriteria As Boolean
	Private mblnVSCriteria As Boolean
	Private mstrDescrip As String
    Private mobjDataTable As DataTable
	Private mstrItemNum As String
	Private mstrSearch As String
	Private objDAL As New clsDAL()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim conConnection As SqlClient.SqlConnection
        'OleDbConnection
        Dim cmdCommand As SqlClient.SqlCommand 'OleDbCommand
        Dim dtrReader As SqlClient.SqlDataReader 'OleDbDataReader
        'Dim conSubConnection As OleDbConnection
        Dim cmdSubCommand As SqlClient.SqlCommand 'OleDbCommand
        Dim dtrSubReader As SqlClient.SqlDataReader 'OleDbDataReader

        'Set Store cookie for debug
        'Response.Cookies("StoreNo").Value = "4559"
        'Response.Cookies("StoreNo").Expires = #1/1/2100#

		'DP 4-09-04 Incident #1657 Fix
		ddlDept.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){event.returnValue=false;event.cancel = true;document.forms[0].btnSubmit.click();}")
		ddlSubclass.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){event.returnValue=false;event.cancel = true;document.forms[0].btnSubmit.click();}")
		'ddlCompany.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){event.returnValue=false;event.cancel = true;document.forms[0].btnSubmit.click();}")
		txtItem.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){event.returnValue=false;event.cancel = true;document.forms[0].btnSubmit.click();}")
		txtDescription.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){event.returnValue=false;event.cancel = true;document.forms[0].btnSubmit.click();}")
		rblvendor.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){event.returnValue=false;event.cancel = true;document.forms[0].btnSubmit.click();}")
		rblCLR.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){event.returnValue=false;event.cancel = true;document.forms[0].btnSubmit.click();}")

		mstrSearch = Replace(UCase(Request("txtSearchStr")), "'", "''")

		'fills in dept dropdown list
		'making results label not visible on startup 
		lblResults.Visible = False
		lblNoResult.Visible = False
		lblError.Visible = False
		pageBody.Attributes.Add("onload", "")
		mblnVSCriteria = ViewState("ddlChangeCriteria")

		If Not IsPostBack Then
			'initilize text boxes to nothing
			txtItem.Text = ""
			txtDescription.Text = ""
			txtVendor.Text = ""
			rblvendor.SelectedIndex = 0			'vendor number as default selection
            'rblMOS.SelectedIndex = 1			   'no as default selection
			rblCLR.SelectedIndex = IIf(mstrSearch > "", 0, 1)			  'DP 3/26/2004 no as default 'BW 05-11-04 unless from New Music
			lblNoResult.Visible = False
			dgResults.AllowPaging = True

			'Fills department drop down list
            conConnection = New SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("IntranetRMS").ConnectionString)
			conConnection.Open()
            cmdCommand = New SqlClient.SqlCommand("SELECT dept, convert(varchar, dept) + ':' + dept_desc as descrip, dept_desc, dept as n FROM dept_info Where " & GetDepartmentRangeClause() & " ORDER BY dept", conConnection)
            dtrReader = cmdCommand.ExecuteReader()

            With ddlDept
                .DataSource = dtrReader
                .DataTextField = "descrip"
                .DataValueField = "dept"
                .DataBind()
                .Items.Insert(0, "<NONE>")
            End With

            dtrReader.Close()
            'conConnection.Close()
            '****************************************************************************************
            'Fill subclass dropdown list
            'conSubConnection = New OleDbConnection(ConfigurationManager.ConnectionStrings("IntranetRMS").ConnectionString)
            'conSubConnection.Open()
            cmdSubCommand = New SqlClient.SqlCommand("SELECT subclass, convert(varchar, subclass)+':'+subclass_desc AS sdescrip, subclass_desc, dept FROM subclass_info WHERE subclass NOT LIKE '%9999' AND " & GetExcludedSubclassClause() & " AND " & GetDepartmentRangeClause() & " order by subclass, subclass_desc", conConnection)
            dtrSubReader = cmdSubCommand.ExecuteReader()

            With ddlSubclass
                .DataSource = dtrSubReader
                .DataTextField = "sdescrip"
                .DataValueField = "subclass"
                .DataBind()
                .Items.Insert(0, "<NONE>")
            End With

            dtrSubReader.Close()
            'conSubConnection.Close()

            ddlDept.SelectedIndex = 0
            ddlSubclass.SelectedIndex = 0

            If mstrSearch > "" Then
                Call GatherData()
            End If
        End If

        Dim strStoreNo As String
        strStoreNo = GetStore()

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationManager.AppSettings("Stylesheet"))
        If Not strStoreNo = "" Then
            ifmHeader.Attributes.Add("src", ConfigurationManager.AppSettings("Mode") & "&location=Store: " & strStoreNo.ToString.PadLeft(4, "0") & "&user=&title=SKU Search")
        Else
            ifmHeader.Attributes.Add("src", ConfigurationManager.AppSettings("Mode") & "&location=Store:&user=&title=SKU Search")
        End If

    End Sub

    Private Sub ddlDept_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlDept.SelectedIndexChanged

        Dim conSubConnection As SqlClient.SqlConnection 'OleDbConnection
        Dim cmdSubCommand As SqlClient.SqlCommand 'OleDbCommand
        Dim dtrSubReader As SqlClient.SqlDataReader 'OleDbDataReader

        conSubConnection = New SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("IntranetRMS").ConnectionString)
        conSubConnection.Open()

        'When user selects a dept from drop down list, the appropriate subclasses fill in its dropdown list
        If ddlDept.SelectedIndex = 0 Then
            'the user chooses not to select a dept then this gives them a list of all subclasses to choose from
            cmdSubCommand = New SqlClient.SqlCommand("SELECT subclass, convert(varchar, subclass)+':'+subclass_desc AS sdescrip, subclass_desc, dept FROM subclass_info WHERE subclass NOT LIKE '%9999' AND " & GetExcludedSubclassClause() & " AND " & GetDepartmentRangeClause() & " order by dept, subclass_desc", conSubConnection)
        Else
            'if the user does choose a dept, this lists only the subclasses belonging to that dept 
            cmdSubCommand = New SqlClient.SqlCommand("SELECT subclass, convert(varchar, subclass)+':'+subclass_desc AS sdescrip, subclass_desc, dept FROM subclass_info WHERE Dept ='" & ddlDept.SelectedItem.Value & "' AND subclass NOT LIKE '%9999' AND " & GetExcludedSubclassClause() & " ORDER BY subclass_desc", conSubConnection)
        End If

        dtrSubReader = cmdSubCommand.ExecuteReader()

        With ddlSubclass
            .DataSource = dtrSubReader
            .DataTextField = "sdescrip"
            .DataValueField = "Subclass"
            .DataBind()
            .Items.Insert(0, "<NONE>")
        End With

        dtrSubReader.Close()
        conSubConnection.Close()
        ViewState("ddlChangeCriteria") = True

    End Sub

    Private Sub txtItem_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtItem.TextChanged

        'stores what user types in for the item number.  Should only be 6 numbers long
        mstrItemNum = Replace(txtItem.Text, "'", "''")
        mblnChangeCriteria = True

    End Sub

    Private Sub txtDescription_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDescription.TextChanged

        'store what user types in for a description
        mstrDescrip = "%" & UCase(Replace(txtDescription.Text, "'", "''")) & "%"
        mblnChangeCriteria = True

    End Sub

    Private Function GetSKUNumber(ByVal InSKU As String) As String

        Dim tmpSKU As String

        If InSKU.Length > 9 Then
            If Mid(InSKU, 1, 2) = "00" Then
                tmpSKU = Mid(InSKU, 3, InSKU.Length - 2)
                Return Mid(tmpSKU, 1, 6) & "-" & Mid(tmpSKU, 7, 3)
            Else
                Return InSKU
            End If
        ElseIf Not InStr(InSKU, "-") And InSKU.Length = 9 Then
            Return Mid(InSKU, 1, 6) & "-" & Mid(InSKU, 7, 3)
        Else
            Return InSKU
        End If

    End Function

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim strItemLength As String
        Dim strItemNo As String
        Dim strItemSize As String
        Dim strCommandString As String
        Dim strStoreNo As String

        dgResults.CurrentPageIndex = 0   'Fixes current index problem.

        If (ddlDept.SelectedIndex > 0 Or ddlSubclass.SelectedIndex > 0 Or txtDescription.Text > "" Or txtItem.Text > "" Or txtVendor.Text > "") Then
            'Tests to see if user hit submit w/o entering any search criteria.

            strItemLength = Trim(txtItem.Text)  'Check if user entered in a SKU or Item Number

            strItemLength = GetSKUNumber(strItemLength)

            Select Case strItemLength.Length
                Case 10, 9
                    'pass SKU directly over to product locator
                    'Check if SKU is a real valid SKU
                    If strItemLength.Length = 9 Then
                        strItemNo = Left(strItemLength, 6)
                        strItemSize = Right(strItemLength, 3)
                        strItemLength = strItemNo & "-" & strItemSize
                    End If

                    strItemLength = Replace(strItemLength, "'", "''")
                    mstrItemNum = Left(strItemLength, 6)

                    strCommandString = "SELECT l.item as itm_cd, l.item_desc AS itmdescrip, round(l.unit_retail, 2) as curr , l.dept, l.subclass, l.subclass_desc"
                    strCommandString &= " FROM vw_item_info l"

                    strCommandString &= " Where l.item ='" & mstrItemNum & "'"
                    strCommandString &=  " AND store = " & GetStore()
                    'adding department filter - Michelle An 01/24/12
                    strCommandString &= " AND " & GetDepartmentRangeClauseL() & ""
                    dgResults.Visible = True

                    Call mFilterSKUs(strCommandString)

                    dgResults.DataSource = mobjDataTable

                    'check for no results
                    If mobjDataTable.Rows.Count = 0 Then
                        'Not a valid sku.  Don't send to product locator
                        lblResults.Visible = False
                        lblNoResult.Visible = True
                        lblNoResult.Text = "There are no results for this SKU. Please make sure you typed the SKU in correctly."

                        With dgResults
                            .AllowPaging = False
                            .DataSource = ""
                            .DataBind()
                            .Visible = False
                        End With

                        ViewState("ddlChangeCriteria") = False
                        mblnVSCriteria = ViewState("ddlChangeCriteria")
                    Else
                        'There are results so bind the data and send to product locator
                        With dgResults
                            .AllowPaging = False
                            .DataBind()
                        End With

                        strStoreNo = GetStore()

                        ViewState("ddlChangeCriteria") = False
                        mblnVSCriteria = ViewState("ddlChangeCriteria")
                        pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('view_details.aspx?STORE=" & strStoreNo & " &itm_cd= " & strItemLength & "','_blank','status=yes;dialogWidth=950px;dialogHeight=550px;scroll=0;resizeable=1;');")
                    End If

                Case 6, 0, 7, 8
                    'either an item or nothing.  get rest of search criteria
                    If rblvendor.SelectedItem.Value = "VNumber" And txtVendor.Text > "" Then
                        'Check if user entered a vendor number to search for, if so, is it valid?
                        If Trim(txtVendor.Text).Length > 6 Then
                            'invalid vendor number
                            lblError.Visible = True
                            lblError.Text = "Invalid vendor number."
                        Else
                            'valid vendor number, continue
                            ViewState("ddlChangeCriteria") = False
                            mblnVSCriteria = ViewState("ddlChangeCriteria")
                            Call GatherData()
                        End If
                    Else
                        ViewState("ddlChangeCriteria") = False
                        mblnVSCriteria = ViewState("ddlChangeCriteria")
                        Call GatherData()
                    End If

                Case Else
                    'invalid number entered as a SKU or Item number.
                    ViewState("ddlChangeCriteria") = False
                    mblnVSCriteria = ViewState("ddlChangeCriteria")
                    lblError.Visible = True
                    lblError.Text = "Invalid SKU or Item Number."
            End Select
        Else
            'User didn't type any search criteria
            ViewState("ddlChangeCriteria") = False
            mblnVSCriteria = ViewState("ddlChangeCriteria")
            Call NoSearch()
        End If

        txtItem.Text = ""

    End Sub

    Private Sub NoSearch()

        'This subprocedure handles the situation when a user hits submit but w/o search criteria.
        dgResults.Visible = False
        lblError.Visible = True
        lblError.Text = "You have not entered any search criteria."

    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click

        Dim conSubConnection As SqlClient.SqlConnection
        Dim cmdSubCommand As SqlClient.SqlCommand
        Dim dtrSubReader As SqlClient.SqlDataReader
        'Clears textboxes and makes the datagrid not visible
        'set drop down lists to "select a whatever"
        txtDescription.Text = ""
        txtItem.Text = ""
        txtVendor.Text = ""
        'rblMOS.SelectedIndex = 1
        rblCLR.SelectedIndex = 1      'DP 3/26/2004 no as default
        rblvendor.SelectedIndex = 0        'vendor number as default selection
        lblResults.Visible = False
        lblError.Visible = False
        lblError.Text = ""
        ddlDept.SelectedIndex = 0
        ViewState("ddlChangeCriteria") = False
        mblnVSCriteria = ViewState("ddlChangeCriteria")
        mblnChangeCriteria = False

        With dgResults
            .AllowPaging = False
            .DataSource = ""
            .DataBind()
            .Visible = False
        End With

        lblNoResult.Visible = False
        '****************************************************************************************
        'Fill subclass dropdown list
        conSubConnection = New SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("IntranetRMS").ConnectionString)
        conSubConnection.Open()
        cmdSubCommand = New SqlClient.SqlCommand("SELECT distinct subclass, Subclass+':'+subclass_desc AS sdescrip, subclass_desc, CONVERT(int, dept)dept FROM item_info WHERE subclass NOT LIKE '%9999' AND " & GetExcludedSubclassClause() & " AND " & GetDepartmentRangeClause() & " ORDER BY CONVERT(int, dept), subclass_desc", conSubConnection)
        dtrSubReader = cmdSubCommand.ExecuteReader()

        With ddlSubclass
            .DataSource = dtrSubReader
            .DataTextField = "sdescrip"
            .DataValueField = "subclass"
            .DataBind()
            .Items.Insert(0, "<NONE>")
        End With


        dtrSubReader.Close()
        conSubConnection.Close()
        ddlSubclass.SelectedIndex = 0

    End Sub

    Private Sub dgResults_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgResults.PageIndexChanged

        If Not mblnChangeCriteria And Not mblnVSCriteria Then
            dgResults.CurrentPageIndex = e.NewPageIndex
            Call GatherData()
            dgResults.DataBind()
        ElseIf mblnChangeCriteria Then
            dgResults.CurrentPageIndex = 0
            Call GatherData()
        ElseIf mblnVSCriteria Then
            dgResults.CurrentPageIndex = 0
            Call GatherData()
        End If

    End Sub

    Public Function GetStore() As String
        'Check both the store cookie and the IP-to-store lookup table
        'IP-to-store lookup value overrides cookie if both are found
        Dim strStoreNo As String

        If Not Request.Cookies("StoreNo") Is Nothing Then
            strStoreNo = Server.HtmlEncode(Request.Cookies("StoreNo").Value)
        End If

        Dim iStoreNumberFromDB As Integer = New PLU.StoreNumberHelper().GetStoreFromIP(Me.Request.ServerVariables("REMOTE_ADDR"))
        If iStoreNumberFromDB > 0 Then
            'Store number from IP lookup overrides cookie; need to format the number properly, though
            strStoreNo = iStoreNumberFromDB.ToString("0000")
        End If

        Return strStoreNo
    End Function

    Private Sub GatherData()

        Dim strCommandString As String
        Dim strSearchMethod As String

        'Executes search query for PLU
        lblResults.Visible = True
        dgResults.Visible = True
        mstrItemNum = Replace(txtItem.Text.Trim, "'", "''")
        mstrDescrip = "%" & UCase(Replace(Trim(txtDescription.Text), "'", "''")) & "%"

        strCommandString = "SELECT l.item as itm_cd, RTRIM(l.item_desc) AS itmdescrip, l.unit_retail as curr, l.dept as dept, l.subclass as subclass, l.subclass_desc as subclass_desc, l.dept_desc as dept_desc, l.item_level as itm_level"
        strCommandString &= ", l.item_parent FROM vw_item_info l"

        'adding department filter - Michelle An 01/24/12
        strCommandString &= " WHERE " & GetDepartmentRangeClauseL()

        'adding store to the filter
        strCommandString &= " and store =" & GetStore()

        If ddlDept.SelectedIndex > 0 Then
            strCommandString &= IIf(Val(strSearchMethod) = 0, " AND", " AND") & " l.dept ='" & ddlDept.SelectedItem.Value & "'"
            strSearchMethod = "1"
        Else
            strSearchMethod = "0"
        End If

        If ddlSubclass.SelectedIndex > 0 Then
            strCommandString &= IIf(Val(strSearchMethod) = 0, " AND", " AND") & " l.subclass ='" & ddlSubclass.SelectedItem.Value & "'"
            strSearchMethod &= "1"
        Else
            strSearchMethod &= "0"
        End If

        If Not mstrItemNum Is Nothing Then
            If mstrItemNum.Length > 6 Then
                strCommandString &= IIf(Val(strSearchMethod) = 0, " AND", " AND") & " (l.item_parent= '" & mstrItemNum & "' or l.item = '" & mstrItemNum & "') and Len(l.item)>6"
                strSearchMethod &= "1"
            Else
                strCommandString &= " and l.item = '" & mstrItemNum & "' and Len(l.item)=6"
                strSearchMethod &= "0"
            End If
        Else
            strCommandString &= " and l.item_parent is null"
            strSearchMethod &= "0"
        End If

        If txtDescription.Text > "" Then
            strCommandString &= IIf(Val(strSearchMethod) = 0, " AND", " AND") & " UPPER(l.item_desc) LIKE '" & mstrDescrip & "'"
            strSearchMethod &= "1"
        Else
            strSearchMethod &= "0"
        End If

        Select Case strSearchMethod
            Case "1000"
                strSearchMethod = "A" 'Search by Department only
            Case "0010"
                strSearchMethod = "B" 'Search by Item only
            Case "0001"
                strSearchMethod = "C" 'Search by Description
            Case "1111"
                strSearchMethod = "D" 'Search by Dept, Subclass, Item, and Description
            Case "1010"
                strSearchMethod = "E" 'Search by Dept and Item
            Case "1011"
                strSearchMethod = "F" 'Search by Dept, Item, and Descrip
            Case "1100"
                strSearchMethod = "G" 'Searches by Dept and Subclass
            Case "1101"
                strSearchMethod = "H" 'Search by Dept, Subclass, Description
            Case "0011"
                strSearchMethod = "I" 'Search by Item and Description
            Case "1110"
                strSearchMethod = "J" 'Search by Dept, Subclass, and Item
            Case "1001"
                strSearchMethod = "K" 'Search by Dept and Description
            Case "0100"
                strSearchMethod = "L" 'Search by Subclass
            Case "0110"
                strSearchMethod = "M" 'Search by Subclass and Item
            Case "0111"
                strSearchMethod = "N" 'Search by Subclass, Item, and Description
            Case "0101"
                strSearchMethod = "O" 'Search by Subclass and Description
            Case "0000"
                If mstrSearch > "" Then
                    strSearchMethod = "S" 'Search All by Search term

                    strCommandString &= " AND (UPPER(l.dept_desc) LIKE '%" & mstrSearch & "%'"
                    strCommandString &= " OR UPPER(l.subclass_desc) LIKE '%" & mstrSearch & "%'"

                    strCommandString &= " OR item ='" & mstrSearch & "'"
                    strCommandString &= " OR UPPER(l.item_desc) LIKE '%" & mstrSearch & "%'"
                End If
        End Select


        mblnChangeCriteria = False

        'adding zone (store branch) filter - Michelle An 10/24/12
        strCommandString &= " AND " & GetDivisionCodeClause()
        '*********************************************************************************
        '*********************************************************************************
        If strCommandString > "" Then
            'if strCommandString isn't empty (which means vendor input was okay), continue with search

            Select Case rblCLR.SelectedIndex
                Case 0
                    strCommandString &= " "
                Case 1
                    strCommandString &= " and l.clearance_ind = 'N'"
            End Select
            ' strCommandString &= " group by l.item, l.item_parent, l.item_level"
            '***************************************************************************
            'check to see if I need any order by statements added
            Select Case strSearchMethod
                Case "G", "H", "L", "O"
                    'strCommandString &= " Order By ve_cd"
                Case "A", "K"
                    strCommandString &= " Order By subclass"
                Case "C", "V"
                    strCommandString &= " Order By dept, subclass"
            End Select

            '******************************************************************************************
            'Try fixing error by only doing paging under certain cases.
            'If not searching with item, use paging.
            With dgResults
                Select Case strSearchMethod
                    Case "A", "C", "G", "H", "K", "L", "O", "S"
                        .AllowPaging = True
                    Case Else
                        If txtVendor.Text > "" Then
                            .AllowPaging = True
                        Else
                            'If searching with item number don't use paging
                            .AllowPaging = False
                        End If
                End Select

                'Set paging settings
                If .AllowPaging Then
                    .PagerStyle.Mode = PagerMode.NumericPages
                    .PagerStyle.PageButtonCount = 5
                    .PageSize = ConfigurationManager.AppSettings("ResultsPerPage")
                End If
            End With


            Call mFilterSKUs(strCommandString)

            If (strSearchMethod = "B" And (mobjDataTable.Rows.Count > 1 Or mobjDataTable.Rows.Count = 1)) Then
                'search for one item and returned more than 1 row - in this case, split the table into two, one with item and one with skus
                Dim newTable As DataTable

                Dim strStoreNo As String
                If (mobjDataTable.Rows.Count = 1 And IsDBNull(mobjDataTable.Rows(0).Item("item_parent")) = False) Then
                    strCommandString = GetItemQuery(mobjDataTable.Rows(0).Item("item_parent").ToString())
                    Call mFilterSKUs(strCommandString)
                    strStoreNo = GetStore()
                    ViewState("ddlChangeCriteria") = False
                    mblnVSCriteria = ViewState("ddlChangeCriteria")
                    pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('view_details.aspx?STORE=" & strStoreNo & " &itm_cd= " & mstrItemNum & "','_blank','status=yes;dialogWidth=950px;dialogHeight=550px;scroll=0;resizeable=1;');")

                Else
                    newTable = mobjDataTable.Clone
                    Dim row As DataRow
                    For Each row In mobjDataTable.Rows
                        If row.Item("itm_level") = "1" Then
                            newTable.ImportRow(row)
                            mobjDataTable.Rows.Clear()
                            mobjDataTable = newTable
                            Exit For
                        End If
                    Next
                End If




            End If

            dgResults.DataSource = mobjDataTable

            'check for no results
            If mobjDataTable.Rows.Count = 0 Then
                lblResults.Visible = False
                lblNoResult.Visible = True
                lblNoResult.Text = "There are no results for your search. "

                With dgResults
                    .AllowPaging = False
                    .DataSource = ""
                    .DataBind()
                    .Visible = False
                End With
            Else

                'There are results so bind the data.
                dgResults.DataBind()
            End If

        Else
            'strCommandString is empty.  Show vendor error message.
            lblResults.Visible = False
            lblError.Visible = True
        End If

        ViewState("ddlChangeCriteria") = False
        mblnVSCriteria = ViewState("ddlChangeCriteria")

    End Sub

    Private Function GetItemQuery(ByVal strItemNo As String) As String
        Dim newQuery As String
        newQuery = "SELECT l.item as itm_cd, rtrim(l.item_desc) AS itmdescrip, l.unit_retail as curr, l.dept as dept, l.subclass as subclass, l.subclass_desc as subclass_desc, l.dept_desc as dept_desc, item_level, tran_level, " & _
                   "l.item_parent FROM vw_item_info l " & _
                    "Where (l.item = '" & strItemNo & "') and Len(l.item)>6 "
        newQuery &= " and store=" & GetStore()
        Return newQuery
    End Function

    Private Sub mFilterSKUs(ByVal strCommandString As String)

        mobjDataTable = objDAL.SelectOracleSQL(strCommandString)

    End Sub

    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click

        Response.Redirect("../home.asp")

    End Sub

    Private Sub rblvendor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rblvendor.SelectedIndexChanged

        'If user enters anything in the txtVendor and selects search by vendor number or vendor name add
        'search query on to sql statement
        mblnChangeCriteria = True

    End Sub

    Private Sub txtVendor_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtVendor.TextChanged

        'If user types info into Vendor text box include query string.
        mblnChangeCriteria = True

    End Sub

    Private Sub ddlSubclass_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSubclass.SelectedIndexChanged

        ViewState("ddlChangeCriteria") = True

    End Sub

    'Private Sub rblMOS_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rblMOS.SelectedIndexChanged

    '    mblnChangeCriteria = True

    'End Sub

    Private Function GetExcludedSubclassClause() As String
        Dim strExcludedSubclassClause As String
        Dim arExcludedSubclasses As Array
        Dim bFirstSubclass As Boolean
        bFirstSubclass = True

        arExcludedSubclasses = ConfigurationManager.AppSettings("HiddenSubclasses").Split(",".ToCharArray)

        For Each strSubclass As String In arExcludedSubclasses
            If bFirstSubclass Then
                strExcludedSubclassClause &= " subclass <> '" & strSubclass.Trim() & "' "
                bFirstSubclass = False
            Else
                strExcludedSubclassClause &= " AND subclass <> '" & strSubclass.Trim() & "' "
            End If
        Next

        Return strExcludedSubclassClause

    End Function

    Private Function GetDepartmentRangeClause() As String
        Dim strDepartmentRangeClause As String = String.Empty
        Dim strMinDepartmentCode() As String = ConfigurationManager.AppSettings("MinDepartmentCode").ToString().Split(",")
        Dim strMaxDepartmentCode() As String = ConfigurationManager.AppSettings("MaxDepartmentCode").ToString().Split(",")
        Dim strDepartmentRangeClauseArray(strMinDepartmentCode.Length + 1) As String
        Dim intIndex As Integer = 0

        For intIndex = 0 To strMinDepartmentCode.Length - 1
            strDepartmentRangeClauseArray(intIndex) = "( convert(int, dept) >= '" & Integer.Parse(strMinDepartmentCode(intIndex).ToString()) & "' AND convert(int,dept) <= '" & Integer.Parse(strMaxDepartmentCode(intIndex).ToString()).ToString() & "')"
        Next intIndex

        For intIndex = 0 To strMinDepartmentCode.Length - 1
            If (intIndex = strMinDepartmentCode.Length - 1) Then
                strDepartmentRangeClause = strDepartmentRangeClause + strDepartmentRangeClauseArray(intIndex).ToString()
            Else
                strDepartmentRangeClause = strDepartmentRangeClause + strDepartmentRangeClauseArray(intIndex).ToString() + " OR "
            End If

        Next intIndex

        'strDepartmentRangeClause = "convert(int, dept) >= '" & Integer.Parse(ConfigurationManager.AppSettings("MinDepartmentCode")).ToString() & "' AND convert(int,dept) <= '" & Integer.Parse(ConfigurationManager.AppSettings("MaxDepartmentCode")).ToString() & "'"

        strDepartmentRangeClause = "(" + strDepartmentRangeClause + ")"

        Return strDepartmentRangeClause

    End Function
    Private Function GetDepartmentRangeClauseL() As String
        Dim strDepartmentRangeClause As String = String.Empty
        Dim strMinDepartmentCode() As String = ConfigurationManager.AppSettings("MinDepartmentCode").ToString().Split(",")
        Dim strMaxDepartmentCode() As String = ConfigurationManager.AppSettings("MaxDepartmentCode").ToString().Split(",")
        Dim strDepartmentRangeClauseArray(strMinDepartmentCode.Length + 1) As String
        Dim intIndex As Integer = 0

        For intIndex = 0 To strMinDepartmentCode.Length - 1
            strDepartmentRangeClauseArray(intIndex) = "( convert(int, dept) >= '" & Integer.Parse(strMinDepartmentCode(intIndex).ToString()) & "' AND convert(int,dept) <= '" & Integer.Parse(strMaxDepartmentCode(intIndex).ToString()).ToString() & "')"
        Next intIndex

        For intIndex = 0 To strMinDepartmentCode.Length - 1
            If (intIndex = strMinDepartmentCode.Length - 1) Then
                strDepartmentRangeClause = strDepartmentRangeClause + strDepartmentRangeClauseArray(intIndex).ToString()
            Else
                strDepartmentRangeClause = strDepartmentRangeClause + strDepartmentRangeClauseArray(intIndex).ToString() + " OR "
            End If

        Next intIndex

        strDepartmentRangeClause = "(" + strDepartmentRangeClause + ")"

        'strDepartmentRangeClause = "convert(int, dept) >= '" & Integer.Parse(ConfigurationManager.AppSettings("MinDepartmentCode")).ToString() & "' AND convert(int, dept) <= '" & Integer.Parse(ConfigurationManager.AppSettings("MaxDepartmentCode")).ToString() & "'"

        Return strDepartmentRangeClause

    End Function

    Private Function GetDivisionCodeClause() As String
        Dim strDivisionCodeClause As String
        strDivisionCodeClause = " zone_id in (" & ConfigurationManager.AppSettings("DivisionCode") & ") "
        Return strDivisionCodeClause
    End Function

    Private Sub btnSubmit_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.DataBinding

    End Sub
End Class