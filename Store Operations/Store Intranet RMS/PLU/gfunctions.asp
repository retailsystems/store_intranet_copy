<%
'This function alternates row colors for tables
Private Function gColorLines(intRow)

	If intRow mod 2 = 0 Then
		gColorLines = "#111111"	
	Else
		gColorLines = "#333333"	
	End If

End Function

'Original clearallcookies
'Private Sub gClearAllCookies()
	
	'Dim Cookie
	
	'For Each Cookie in Request.Cookies
	  
	 ' Response.Cookies(Cookie).Expires = "1/1/1984"
	  
	'Next

'End Sub

Private Sub gClearAllCookies()
'Checks to see if cookie is for the store number
'if the cookie is for the store number it does not erase the cookie
'if the cookie is not for the store number it does erase the cookie
  Dim Cookie
  
  
  For Each Cookie in Request.Cookies
      If Cookie="StoreNo" then
	    Response.Cookies("StoreNo").Expires="1/1/2038"
	  else
	  Response.Cookies(cookie).Expires="1/1/1984"
      End If
   Next
    
End Sub

Private Sub gClearCookieKeys(strCookieName)
	
	Dim Key
	
	For	Each Key In Request.Cookies(strCookieName)
	
		Response.Cookies(strCookieName)(Key) = ""

	Next
	
End Sub

'This function saves the current part qty inputs displayed on screen to the clients cookies
Private Sub gSaveCurrentList(strInputName,strCookie,booNumber)

	Dim item
	Dim lngPartNo
	
	For Each item in Request.Form
		
		If Mid(Item,1,(Len(strInputName))) = strInputName Then
			
			lngPartNo = Mid(Item,Len(strInputName)+2,Len(Item) - 5)
			If (Isnumeric(Request(Item)) And booNumber) Or Not booNumber Then
				Response.Cookies(strCookie)(lngPartNo) = Request(Item)
			End If
			
		End If
		
	Next
   
End Sub

'style information
Private Sub gGetStyles()
%>
	<STYLE TYPE="text/css">
	<!--
	INPUT.I1{
	background-color: #FFFFFF;
	font-family: Arial;
	color: #000000;
	border-style: solid;
	border-color: #990000;
	width: 40;
	}

	INPUT.I2{
	background-color: #440000;
	font-family: Arial;
	font-weight: bold;
	color: #FF0000;
	border-style: solid;
	border-color: #990000;
	width: 25;
	height: 20;
	}
	
	INPUT.I3{
	background-color: #FFFFFF;
	font-family: Arial;
	color: #000000;
	border-style: solid;
	border-color: #990000;
	}
	
	INPUT.B1{
	background-color: #440000;	
	color: #FFFFFF; 
	font-weight: bold; 
	border-style: solid; 
	border-color: #990000;

	}
	
	-->
	</STYLE>
<%
End Sub
%>