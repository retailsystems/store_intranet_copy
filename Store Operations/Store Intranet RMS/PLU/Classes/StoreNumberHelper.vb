Imports System.Text
Imports System.Data
Imports System.Data.SqlClient

Public Class StoreNumberHelper
  ''added by Peter Ho 9.4.2003
  ''note: orig code from Dennis Prestia.  made some mods.
  Public Function GetStoreFromIP(ByVal requestorIP As String) As Integer
    Dim StoreIP() As String
    Dim StoreSubnetMask() As String
    Dim strStoreSubnet As String
    Dim strTemp As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("Intranet").ConnectionString)

    StoreIP = Split(requestorIP, ".")

        StoreSubnetMask = Split(ConfigurationManager.AppSettings("strStoreSubnetMask"), ".")

    Dim subnetResolved As New StringBuilder(15)
    subnetResolved.Append((StoreIP(0) And StoreSubnetMask(0)).ToString())
    subnetResolved.Append(".")
    subnetResolved.Append((StoreIP(1) And StoreSubnetMask(1)).ToString())
    subnetResolved.Append(".")
    subnetResolved.Append((StoreIP(2) And StoreSubnetMask(2)).ToString())
    subnetResolved.Append(".")
    subnetResolved.Append((StoreIP(3) And StoreSubnetMask(3)).ToString())

    'strStoreSubnet = StoreIP(0) And StoreSubnetMask(0)
    'strStoreSubnet = strStoreSubnet & "."
    'strStoreSubnet = strStoreSubnet & (StoreIP(1) And StoreSubnetMask(1))
    'strStoreSubnet = strStoreSubnet & "."
    'strStoreSubnet = strStoreSubnet & (StoreIP(2) And StoreSubnetMask(2))
    'strStoreSubnet = strStoreSubnet & "."
    'strStoreSubnet = strStoreSubnet & (StoreIP(3) And StoreSubnetMask(3))

    Dim objCommand As New SqlClient.SqlCommand("spphGetStoreFromSubnet", objConnection)
    objCommand.CommandType = CommandType.StoredProcedure

    Dim parStoreSubnet As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreSubnet", SqlDbType.VarChar, 15)
    parStoreSubnet.Direction = ParameterDirection.Input
    parStoreSubnet.Value = subnetResolved.ToString()

    Dim parStoreNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt)
    parStoreNum.Direction = ParameterDirection.Output

    Dim retval As Integer

    Try
      objConnection.Open()
      objCommand.ExecuteNonQuery()
    Catch
      Return -1
    Finally
      ''objCommand.Dispose()

      objConnection.Close()
    End Try

    Return parStoreNum.Value
  End Function




End Class
