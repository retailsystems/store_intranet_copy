Option Explicit On 

Imports System.Data.OracleClient
Imports System.Data.SqlClient

Public Class clsDAL

    Private objCommand As SqlCommand
    'Private objOracleCommand As OracleCommand
    Private objOracleCommand As SqlCommand

	Public Function cnnConnection() As SqlConnection

        cnnConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Intranet").ConnectionString)

	End Function

	Private Function Execute() As Integer

		Dim intRecordsAffected As Integer

		With objCommand
			.Connection = cnnConnection()
			.Connection.Open()
			intRecordsAffected = .ExecuteNonQuery()
			.Connection.Close()
		End With

		Return intRecordsAffected

	End Function

    Public Function ExecuteSQL(ByVal vstrSQL As String) As Integer

        objCommand = New SqlCommand

        With objCommand
            .CommandText = vstrSQL
            .CommandType = CommandType.Text
        End With

        Return Execute()

    End Function

    Private Function GetDataTable() As DataTable

        Dim objSQL As SqlDataAdapter
        Dim objDataTable As New DataTable

        objCommand.Connection = cnnConnection()

        objSQL = New SqlDataAdapter(objCommand)
        objSQL.FillSchema(objDataTable, SchemaType.Source)
        objSQL.Fill(objDataTable)

        Return objDataTable

    End Function

    Public Function SelectSQL(ByVal vstrSQL As String) As DataTable

        objCommand = New SqlCommand

        With objCommand
            .CommandText = vstrSQL
            .CommandType = CommandType.Text
        End With

        Return GetDataTable()

    End Function

    Private Function cnnOracleConnection() As SqlConnection

        '   cnnOracleConnection = New OracleConnection(ConfigurationManager.ConnectionStrings("strRMSADO").ConnectionString)
        cnnOracleConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("IntranetRMS").ConnectionString)
    End Function

    Private Function ExecuteOracle() As Single

        Dim intRecordsAffected As Single

        With objOracleCommand
            .Connection = cnnOracleConnection()
            .Connection.Open()
            intRecordsAffected = .ExecuteNonQuery()
            .Connection.Close()
        End With

        Return intRecordsAffected

    End Function

    Private Function GetOracleDataTable() As DataTable

        Dim objSQL As SqlDataAdapter
        Dim objDataTable As New DataTable

        objOracleCommand.Connection = cnnOracleConnection()
        objSQL = New SqlDataAdapter(objOracleCommand)
        '  objSQL = New OracleDataAdapter(objOracleCommand)
        objSQL.Fill(objDataTable)
        cnnOracleConnection.Close()
        cnnOracleConnection.Dispose()
        Return objDataTable

    End Function

    Public Function SelectOracleSQL(ByVal vstrSQL As String) As DataTable

        objOracleCommand = New SqlCommand

        With objOracleCommand
            .CommandText = vstrSQL
            .CommandType = CommandType.Text
        End With

        Return GetOracleDataTable()

    End Function
    

End Class
