<!-- #include file="globalconst.asp" -->
<Script runat="server">
'Displays the top part of the main frame
Private Sub gShowHeader(byRef strTitle As String)
    Dim strStore 
    Dim strStoreNo
    
    'For testing cookies when running through debugger
	'Response.cookies("StoreNo").value=0188
	'Response.cookies("StoreNo").Expires="July 27, 2003"
	 
  If Not Request.Cookies("StoreNo") Is Nothing Then
    strStoreNo = Server.HtmlEncode(Request.Cookies("StoreNo").Value)
  End If
  ''added by peter.ho on 9.4.2003
  ''logic:
  ''  1. use orig logic for store number (passed in from querystring)
  ''  2. overwrite store number if found in db from client's ip addr
  Dim iStoreNumberFromDB As Integer = new PLU.StoreNumberHelper().GetStoreFromIP(Me.Request.ServerVariables("REMOTE_ADDR"))
  If iStoreNumberFromDB > 0 Then
    ''store number found in db from client's ip addr
    strStoreNo = iStoreNumberFromDB.ToString()
  End If
  ''end mod
  strStore = "Store: " & strStoreNo
	
	Response.Write ("<table border='0' cellspacing='0' cellpadding='0' width='100%'>")
	Response.Write ("<tr>")
	Response.Write ("<td> <A HREF='http://www.hottopic.com' target='_blank'><img src='Images/hdr_main2.gif'  border='0' alt='Hot Topic'></A></td>")
	'Response.Write "<td>  <A HREF='http://www.torrid.com'> <img src='Images/TorridBlk.jpg' border='0' alt='Torrid'></A></td>"
	Response.Write ("<td  colspan='2' align='right' valign='Top'></td><td  colspan='2'  align='right' valign='Top'>")
	Response.Write ("<table width='40%' border='1' bordercolor='#990000' cellspacing='0' cellpadding='0'><tr><td bgcolor='#440000' align='right'>")
	Response.Write ("<font size='2' face'Arial' color='Red'><b>&nbsp; </b></font>")
	Response.Write ("</td></tr>")
    'had to comment out in order to get to work.
	'If Len(Request.Cookies("User")("UserName")) <> 0 Then
		'Response.Write ("<tr onclick=""document.location='lstephens/hottopic/home.asp?Mode=Logoff';""><td bgcolor='#440000' align='center'>")
		'Response.Write ("<a href='home.asp?Mode=Logoff' style={text-decoration:none;} ><font size='2' face='" & gcMainFontFace & "' color='Red'><b>log off</b></font></a>")
		'Response.Write ("</td></tr>")
	'End If
	Response.Write ("</table>")
    Response.Write ("<tr></tr>")
	Response.Write ("<tr></tr>")
	Response.Write ("<tr></tr>")
	Response.Write ("<tr></tr>")
	Response.Write ("<tr><td colspan='2' align='left'><font face='Arial'><font size='+1' color='White'>" & strStore & "</font></td>")
	Response.Write ("<td colspan='2' align='right'><font face='Arial'><font size='+1' color='Red'>" & strTitle & "</font></td></tr>")
	Response.Write ("<tr>")
	Response.Write ("<td colspan='2'><hr color='#990000'></td>")
	Response.Write ("<td colspan='2'><hr color='#990000'></td>")
	Response.Write ("</tr>")
	Response.Write ("</table>")
End Sub
</Script>
