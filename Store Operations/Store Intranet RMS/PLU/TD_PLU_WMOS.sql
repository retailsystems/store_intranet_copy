DROP MATERIALIZED VIEW HOTTOPIC.TD_PLU_WMOS;
--
-- TD_PLU_WMOS  (Materialized View) 
--
CREATE MATERIALIZED VIEW HOTTOPIC.TD_PLU_WMOS 
TABLESPACE HT_TAB
PCTUSED    80
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             4M
            MINEXTENTS       1
            MAXEXTENTS       8192
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOCACHE
LOGGING
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE
START WITH TO_DATE('17-Apr-2008 11:44:48','dd-mon-yyyy hh24:mi:ss')
NEXT SYSDATE + 1  
WITH PRIMARY KEY
AS 
/* Formatted on 2008/04/16 12:40 (Formatter Plus v4.8.8) */
SELECT /*+ all_rows */ /* TD_PLU_WMOS  refreshes materialized view */
	I.subclass_cd,
	S.des subdesc,
	I.ve_cd,
	V.ve_name,
	I.dept_cd,
	D.des DEPTDESC,
	K.sku_num,
	U.upc_cd,
	I.des2 SMYTHDESC,
	I.des1 ITMDESCRIP,
	K.size_cd,
	hottopic.ht_get_ret_prc.get_sku_ret_prc(k.sku_num) curr,
	TRUNC(SYSDATE) AS beg_dt,
	I.itm_cd,
	Z.udf010,
	Z.udf011,
	Z.udf013
FROM gm_itm I,
	 subclass S,
	 gm_sku K,
	 ve V,
	 dept D,
	 gm_itm_udf Z,
  	 (--get sku and upc code
   	 		SELECT A.sku_num sku_num, B.upc_cd upc_cd
			FROM gm_sku A, gm_sku2upc_cd B
			WHERE A.sku_num = B.sku_num(+) AND B.upc_cd NOT LIKE '00%'
   	 ) U
WHERE I.itm_cd = K.itm_cd
AND Z.itm_cd(+) = I.itm_cd
AND I.ve_cd = V.ve_cd
AND I.subclass_cd = S.subclass_cd
AND I.dept_cd = D.dept_cd
AND K.sku_num = U.sku_num(+)
AND I.dept_cd >= '0050'
AND I.dept_cd < '0099'
AND (i.grid_flag = 'N' or i.grid_flag = 'Y' and k.sku_num not like '%000')
AND I.lst_rcv_dt IS NOT NULL
AND (k.sku_tp is null OR k.sku_tp <> 'PPK');

COMMENT ON TABLE HOTTOPIC.TD_PLU_WMOS IS 'snapshot table for snapshot HOTTOPIC.TD_PLU_WMOS';

--
-- TD_PLU_WMOS_DEPT_SBCLS  (Index) 
--
CREATE INDEX HOTTOPIC.TD_PLU_WMOS_DEPT_SBCLS ON HOTTOPIC.TD_PLU_WMOS
(DEPT_CD, SUBCLASS_CD)
LOGGING
TABLESPACE HT_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             4M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

--
-- TD_PLU_WMOS_ITM  (Index) 
--
CREATE INDEX HOTTOPIC.TD_PLU_WMOS_ITM ON HOTTOPIC.TD_PLU_WMOS
(ITM_CD)
LOGGING
TABLESPACE HT_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             4M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

--
-- TD_PLU_WMOS_SKU_NUM  (Index) 
--
CREATE INDEX HOTTOPIC.TD_PLU_WMOS_SKU_NUM ON HOTTOPIC.TD_PLU_WMOS
(SKU_NUM)
LOGGING
TABLESPACE HT_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             4M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

GRANT SELECT ON HOTTOPIC.TD_PLU_WMOS TO HOTTOPIC_INQ_ROLE;