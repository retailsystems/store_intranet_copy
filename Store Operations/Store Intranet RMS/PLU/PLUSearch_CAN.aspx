<%@Import Namespace="System.Data.OleDB"%>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PLUSearch.aspx.vb" Inherits="PLU.WebForm1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>SKU Search</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link id="lnkStyles" href="http://172.16.125.2/StyleSheets/hottopic.css" type="text/css" runat="server" rel="stylesheet" />
	</HEAD>
	<body id="pageBody" runat="server">
		<form id="frmPLU" method="post" runat="server">
			<iframe class="Header_Frame" id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0"
				src="http://172.16.125.2/Header/Header.aspx?mode=hottopic" frameBorder="0" width="100%"
				scrolling="no" runat="server"></iframe>
			<table height="100%" width="100%">
				<tr>
					<td vAlign="top" align="right"><br>
						<table width="100%">
							<tr>
								<td vAlign="top" width="50%">
									<table width="100%">
										<tr>
											<td>
												<table class="normal" width="100%">
													<tr width="100%">
														<td class="normal" noWrap width="45%">Dept.</td>
														<td align="left" width="55%"><asp:dropdownlist id="ddlDept" tabIndex="1" runat="server" width="100%" autopostback="true"></asp:dropdownlist></td>
													</tr>
													<tr width="100%">
														<td noWrap width="45%">Subclass</td>
														<td align="left" width="55%"><asp:dropdownlist id="ddlSubclass" tabIndex="3" runat="server" width="100%" autopostback="false"></asp:dropdownlist></td>
													</tr>
													<tr width="100%">
														<td vAlign="top" noWrap align="left" width="45%"><asp:radiobuttonlist id="rblvendor" tabIndex="5" runat="server" repeatdirection="Horizontal" cssclass="normal" Visible="False">
																<asp:listitem value="VNumber">Vendor No.</asp:listitem>
																<asp:listitem value="VName">Name</asp:listitem>
															</asp:radiobuttonlist></td>
														<td vAlign="bottom" align="left" width="55%"><asp:textbox id="txtVendor" tabIndex="6" runat="server" width="100%" maxlength="17" Visible="False"></asp:textbox></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td vAlign="top" width="50%">
									<table width="100%">
										<tr>
											<td colSpan="2">
												<table class="normal" width="100%">
													<tr>
														<td noWrap>Item or SKU</td>
														<td noWrap><asp:textbox id="txtItem" tabIndex="2" runat="server" width="50%" maxlength="12"></asp:textbox></td>
													</tr>
													<tr>
														<td noWrap width="1%">Item Description</td>
														<td noWrap><asp:textbox id="txtDescription" tabIndex="4" runat="server" width="100%" maxlength="23"></asp:textbox></td>
													</tr>
												
													<tr>
														<td noWrap width="1%">Include CLEARANCE?</td>
														<td noWrap><asp:radiobuttonlist id="rblCLR" tabIndex="7" runat="server" repeatdirection="Horizontal" cssclass="normal">
																<asp:listitem value="Yes">Yes</asp:listitem>
																<asp:listitem value="No" selected="True">No</asp:listitem>
															</asp:radiobuttonlist></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table width="100%">
							<tr align="right">
								<td align="right" colSpan="2"><asp:button id="btnSubmit" tabIndex="8" runat="server" cssclass="b1" enableviewstate="False"
										text="Submit"></asp:button>&nbsp;
									<asp:button id="btnClear" tabIndex="9" runat="server" cssclass="b1" enableviewstate="False"
										text="Clear"></asp:button>&nbsp;
									<asp:button id="btnHome" tabIndex="10" runat="server" cssclass="b1" enableviewstate="False"
										text="Home"></asp:button></td>
							</tr>
							<tr width="100%">
								<td noWrap align="left" width="100%"><asp:label id="lblResults" runat="server" width="100%" cssclass="red">Results</asp:label><asp:label id="lblNoResult" runat="server" width="100%" cssclass="red" visible="False"></asp:label><asp:label id="lblError" runat="server" width="100%" cssclass="red" visible="False"></asp:label></td>
							</tr>
							<tr>
								<td><asp:datagrid id="dgResults" runat="server" width="100%" cssclass="DataGridSm" gridlines="None"
										pagesize="5" autogeneratecolumns="False">
										<itemstyle cssclass="BC111111sm"></itemstyle>
										<alternatingitemstyle cssclass="BC333333sm"></alternatingitemstyle>
										<headerstyle cssclass="DATAGRID_Headersm"></headerstyle>
										<columns>
											<asp:boundcolumn datafield="itmdescrip" headertext="Description"></asp:boundcolumn>
											<asp:boundcolumn datafield="dept" headertext="Dept"></asp:boundcolumn>
											<asp:boundcolumn datafield="subclass" headertext="Subclass"></asp:boundcolumn>
											<asp:boundcolumn datafield="subclass_desc" headertext="Subclass Description"></asp:boundcolumn>
											
											<asp:templatecolumn headertext="ItemNum">
												<itemtemplate>
													<a href="javascript:void(0);" onclick="javascript:window.open('view_details.aspx?itm_cd=<%# Container.dataitem(1) %>&STORE=<%# GetStore() %>','_blank','status=yes,width=800,height=550,scrollbars=no,resizeable=1')">
														<asp:Label Runat="server" ID="Label1" Text='<%# Container.dataitem("itm_cd") %>'>
														</asp:Label></a>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:BoundColumn DataField="curr" HeaderText="Price" Visible="false"></asp:BoundColumn>	
											<asp:TemplateColumn HeaderText="Price">
											<ItemTemplate><asp:Label Runat="server" ID="Label2" Text="--">
														</asp:Label></ItemTemplate>
											</asp:TemplateColumn>										
										</columns>
									</asp:datagrid></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<% '*** Begin: create Return, Home buttons
       ' Response.Write ("<table border='0' cellspacing='0' cellpadding='0' width='750'>")
	   ' Response.Write ("<tr><td><hr color='" & gcPageLineColor & "'></td></tr>")
       ' Response.Write ("<td align='right' ><input class='B1' type='button' name='cmdReturn' color='white' value='Return' onClick='history.go(-1)';'>&nbsp;&nbsp;<input class='B1' type='button' value='Home' onClick='document.location=" & chr(34) & "Home.asp" & chr(34) & ";' id='button' name='button'></td></tr>")
	   ' Response.Write ("</table>")
        '*** End: create Return, Home buttons%>
		</form>
		<script language="javascript">
			<!--
			document.forms[0].txtItem.focus();
			-->
		</script>
	</body>
</HTML>
