<%@ Page Language="vb" AutoEventWireup="false" Codebehind="view_details.aspx.vb" Inherits="PLU.WebForm3"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Item Detail</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<META http-equiv="CACHE-CONTROL" content="NO-CACHE">
		<META http-equiv="PRAGMA" content="NO-CACHE">
		<META http-equiv="EXPIRES" content="0">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<base target="_self" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
  </HEAD>
	<body id="pageBody" scroll="no" runat="server" MS_Positioning="FLOWLayout">
		<form id="Form1" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server" />
			<div class="container home" role="main">
			<table height="100%" width="100%">
				<tr>
					<td noWrap><asp:datagrid id="dgDetails" runat="server" CssClass="table archtbl" AutoGenerateColumns="False"
							EnableViewState="True" GridLines="None" Width="100%">
							<ItemStyle CssClass="archtbltr"></ItemStyle>
							<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
							<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
							<Columns>
								<asp:ButtonColumn runat="Server" ButtonType="LinkButton" HeaderText="SKU" DataTextField="item" CommandName="SKUSelected"></asp:ButtonColumn>
								<asp:BoundColumn DataField="itmdescrip" HeaderText="Description"></asp:BoundColumn>
								<asp:BoundColumn DataField="size_cd" HeaderText="Size"></asp:BoundColumn>
								<asp:BoundColumn DataField="curr" HeaderText="Price"></asp:BoundColumn>
								<asp:BoundColumn DataField="subclass" HeaderText="Subclass"></asp:BoundColumn>
								<asp:BoundColumn DataField="subclass_desc" HeaderText="Sub Description"></asp:BoundColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td vAlign="top">
						<table width="100%">
							<tr>
								<td vAlign="top" align="left" width="60%">
									<table width="100%">
										<tr>
											<td colSpan="2">
												<table width="100%" style="font-size:11px">
													<tr>
														<td noWrap><asp:label id="lblSKU" EnableViewState="True" Runat="server"></asp:label>&nbsp;&nbsp;&nbsp;
															<asp:label id="lblsize" Runat="server"></asp:label></td>
														<td noWrap align="right">Product Locator</td>
													</tr>
													<tr>
														<td noWrap width="100%" colSpan="2"><asp:radiobuttonlist id="rblArea" EnableViewState="True" Width="100%" Runat="server"
																RepeatDirection="Horizontal" AutoPostBack="True" font-size="11px">
																<asp:ListItem Value="All">All</asp:ListItem>
																<asp:ListItem Value="Region">My&nbsp;Region</asp:ListItem>
																<asp:ListItem Value="District" Selected="True">My&nbsp;District</asp:ListItem>
																<asp:ListItem Value="Internet">Internet</asp:ListItem>
																<asp:ListItem Value="Store">My&nbsp;Store</asp:ListItem>
															</asp:radiobuttonlist></td>
														<td>Store:</td>
														<td><asp:textbox id="txtStore" EnableViewState="True" Width="52px" Runat="server"></asp:textbox></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
														<td align="right"><asp:button id="btnSubmit" tabIndex="8" runat="server" CssClass="btn btn-danger" EnableViewState="False"
																Text="Submit"></asp:button></td>
													</tr>
													<tr>
														<td noWrap width="100%" colSpan="2"><asp:label id="lblResults" runat="server"></asp:label><asp:label id="lblhidden" Width="0" Runat="server" visible="false"></asp:label></td>
													</tr>
													<tr>
														<td noWrap colSpan="4"><asp:datagrid id="dgPL" CssClass="table archtblf" AutoGenerateColumns="False" GridLines="None" Width="100%"
																Runat="server" AllowPaging="True" font-size="11px">
																<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
																<ItemStyle CssClass="archtbltr"></ItemStyle>
																<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
																<Columns>
																	<asp:BoundColumn DataField="store" HeaderText="Store"></asp:BoundColumn>
																	<asp:TemplateColumn HeaderText="Qty">
																		<HeaderStyle BorderWidth="0px" CssClass="archtblhdr"></HeaderStyle>
																		<ItemTemplate>
																			<asp:Label Runat="server" ID="lblQTY" Text='<%# Container.dataitem("available_qty") %>'>
																			</asp:Label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Ship Date">
																		<HeaderStyle BorderWidth="0px" CssClass="archtblhdr"></HeaderStyle>
																	
																		<ItemTemplate>
																			<asp:Label Runat="server" ID="lblShipDate" Text='<%# Container.dataitem("lst_rcvd_dt") %>'>
																			</asp:Label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Store Name">
																		<HeaderStyle BorderWidth="0px" CssClass="archtblhdr"></HeaderStyle>
																		<ItemTemplate>
																			<a href='mailto:<%# GETEMAIL(Container.dataitem("store")) %>'>
																				<asp:Label Runat="server" ID="lblstorename" Text='<%# Container.dataitem("store_name") %>'>
																				</asp:Label></a>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Phone">
																		<HeaderStyle BorderWidth="0px" CssClass="archtblhdr"></HeaderStyle>
																		<ItemTemplate>
																			<asp:Label Runat="server" ID="lblPhone" Text='<%# Phone(Container.dataitem("store")) %>'>
																			</asp:Label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																</Columns>
															</asp:datagrid></td>
													</tr>
													<tr>
														<td vAlign="top" noWrap align="left"><br>
															<input class="btn btn-danger" id="btnClose" onclick="JavaScript:window.close();" type="button" value="Close" width="88px" height="27px"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td vAlign="top" align="right" width="60%">
									<table width="100%">
										<tr>
											<td>
												<table width="100%">
													<tr>
														<td></td>
													</tr>
													<tr>
														<td></td>
													</tr>
													<tr>
														<td noWrap align="right"><br>
															<br>
															<asp:label id="lblPhoto" Width="119px" Runat="server" Height="240px" Visible="False"
																text="No Photo Available."></asp:label></td>
													</tr>
													<tr>
														<td noWrap align="right"><asp:image id="imgItem" runat="server" EnableViewState="False" BorderStyle="None" ImageAlign="right"></asp:image></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
