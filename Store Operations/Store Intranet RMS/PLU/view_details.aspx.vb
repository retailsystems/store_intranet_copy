Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports System.Configuration

Partial Class WebForm3

    Inherits System.Web.UI.Page

    Protected WithEvents txthidden As System.Web.UI.WebControls.TextBox

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public value As String
    Public Store As String
    Public str_sku As String
    Public SKU As String 'passed by search
    Public AreaChanged As Integer
    Public SKUChanged As Integer
    Public DPChanged As Integer
    Public blnNoSizeSku As Boolean
    Public NRows As Integer

    Private _rockStoreFilterSql As String = ""
    Private _newStoreFilterSql As String = ""

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Store = Request.QueryString("STORE")
        If Store = "" Then
            Response.Write("You have to set up a store to use this page.")
            Response.End()
        End If
        If Not IsPostBack Then
            Dim value As String
            value = Request.QueryString("itm_cd")
            value = Trim(value)
            If value = "" Then
                Response.Write("You have to pick an item to use this page.")
                Response.End()
            End If
        End If
        '  Store = "0553"  'Use for testing
        pageBody.Attributes("onload") = ""
        ClientScript.RegisterHiddenField("__EVENTTARGET", "btnSubmit")
        str_sku = ViewState("SKUSelected") 'Grabbing sku from viewstate in the case of user changed area to look in.
        Call Get_Detail_Info()
        ' MOD 5/17/05 Author: Sri Bajjuri Desc: HT Rock Store filter
        BuildRockStoreFilterStr()
        BuildNewStoreFilterStr()

        If Not IsPostBack Then
          
          
            Dim Size As String
            Dim DGPLTransType As DataGridItem
            If (dgDetails.Items.Count <> 0) Then
                DGPLTransType = dgDetails.Items(0)
                Size = DGPLTransType.Cells(2).Text
            Else
                DGPLTransType = Nothing
                Size = ""
            End If


            If Size = "&nbsp;" Then
                Size = ""
            End If

            If NRows = 1 Then
                str_sku = CType(dgDetails.Items(0).Cells(0).Controls(0), LinkButton).Text
                Call FillInventory(str_sku)
                If Size = "" Then
                    lblsize.Text = ""
                Else
                    lblsize.Text = "Size: " & Size
                End If
            ElseIf NRows > 1 Then
                dgPL.Visible = False
                lblResults.Visible = True
                lblResults.Text = "Please select a SKU to find product location."
            End If
        End If

        Dim strStoreNo As String

        strStoreNo = GetStore()

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationManager.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationManager.AppSettings("Mode") & "&location=Store: " & Val(strStoreNo) & "&user=&title=Item Detail")

    End Sub

    Private Sub dgDetails_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgDetails.PageIndexChanged
        ViewState("DPChanged") = 1
        dgDetails.CurrentPageIndex = e.NewPageIndex
        dgDetails.DataBind()
        'str_sku = CType(dgDetails.Items(0).Cells(0).Controls(0), LinkButton).Text
        'Call FillInventory(str_sku)
        str_sku = ""
        lblSKU.Visible = False
        lblsize.Visible = False
        dgPL.Visible = False
        lblResults.Visible = True
        lblResults.Text = "Please select a SKU to find product location."
    End Sub
    Private Sub Get_Detail_Info()
        Dim myDataTable As DataTable
        Dim commandString As String
        Dim connectionString As String

        With dgDetails
            .AllowPaging = True
            .PagerStyle.Mode = PagerMode.NumericPages
            .PagerStyle.PageButtonCount = 3
            .PageSize = 5
        End With

        connectionString = ConfigurationManager.ConnectionStrings("IntranetRMS").ConnectionString

        'Dim value As String  'item number passed from search page
        value = Request.QueryString("itm_cd")
        value = Trim(value)

        commandString = "SELECT l.item, l.item_desc AS itmdescrip, l.item_parent as parent, l.size as size_cd, "
        commandString &= " l.unit_retail as curr,"
        commandString &= " l.subclass as subclass, l.subclass_desc as subclass_desc FROM vw_item_info l"
        commandString &= " Where " & GetDivisionCodeClause_Item()

        If value.Length = 6 Then
            commandString &= " AND (l.item_parent = '" & value & "' ) and len(l.item)>6 "
        ElseIf value.Length >= 8 And value.Length <= 10 Then
            ' if it is a 000 sku, get rid of the zero
            'If value.Contains("-000") Then
            '    value = value.Substring(0, 6)
            'End If
            commandString &= " AND (l.item_parent = '" & value & "' or (l.item = '" & value & "' and l.tran_level = l.item_level))"
        End If
        commandString &= " and store =" & GetStore()
        commandString &= " ORDER BY l.item"

        Dim myDataAdapter As New System.Data.SqlClient.SqlDataAdapter(commandString, connectionString)
        Dim myDataSet As New DataSet()

        myDataAdapter.Fill(myDataSet, "Items")
        myDataTable = myDataSet.Tables(0)

        '******************************************************************************* 
        'Parse size 
        'Dim dtC As New DataColumn
        'dtC.DataType = System.Type.GetType("System.String")
        'dtC.AllowDBNull = True
        'dtC.Caption = "size_cd"
        'dtC.ColumnName = "size_cd"
        'dtC.DefaultValue = String.Empty
        'myDataTable.Columns.Add(dtC)
        'Dim s As Integer = 0
        NRows = myDataTable.Rows.Count
        'For s = 0 To NRows - 1
        '    ParseSize(myDataTable.Rows(s))
        'Next

        dgDetails.DataSource = myDataTable
        dgDetails.DataBind()
        '********************************************************************************
        'Test if images exists from Internet store
        Dim strTestUrlExists As String
        Dim picvalue As String
        Dim strUrl As String
        Dim strPic As String
        Dim strProductImage As String

        strPic = ViewState("Pic")
        strProductImage = ConfigurationManager.AppSettings("strProductImage")

        If (value.Length = 6) Then
            picvalue = Left(value, 6)
        ElseIf (myDataTable.Rows.Count <> 0) Then
            If IsDBNull(myDataTable.Rows(0)("parent")) = False Then
                picvalue = myDataTable.Rows(0)("parent").ToString
            End If
        Else
            picvalue = value
        End If
        strUrl = Replace(strProductImage, "[picvalue]", picvalue, , , CompareMethod.Text)
        'strUrl = "http://www.hottopic.com/assets/product_images/lg/" + picvalue + "_lg.jpg"
        'imgItem.ImageUrl = "http://www.hottopic.com/assets/product_images/lg/" + picvalue + "_lg.jpg"

        If strPic = Nothing Or strPic = "" Then
            'haven't checked for picture yet.
            strTestUrlExists = ReturnWebStatusCode(strUrl)            'Test if image exists for this item from Internet store
            If strTestUrlExists = "OK" Then
                imgItem.ImageUrl = strUrl
                imgItem.Visible = True
                lblPhoto.Visible = False
                ViewState("Pic") = "Y"
            Else
                ViewState("Pic") = "N"
                imgItem.Visible = False
                lblPhoto.Visible = True              'Give user messge that photo doesn't exist for that item.
            End If
        ElseIf strPic = "Y" Then
            'Picture does exist
            imgItem.ImageUrl = strUrl
            imgItem.Visible = True
            lblPhoto.Visible = False
            ViewState("Pic") = "Y"
        ElseIf strPic = "N" Then
            'Picture does not exist
            ViewState("Pic") = "N"
            imgItem.Visible = False
            lblPhoto.Visible = True           'Give user messge that photo doesn't exist for that item.
        End If

    End Sub


    Private Function ParseSize(ByRef row As DataRow) As DataRow
        Dim htSize As Hashtable = New Hashtable
        htSize.Add("diff_type_1", IIf(IsDBNull(row.Item("diff_type_1").ToString), "", row.Item("diff_type_1").ToString))
        htSize.Add("diff_type_2", IIf(IsDBNull(row.Item("diff_type_2").ToString), "", row.Item("diff_type_2").ToString))
        htSize.Add("diff_type_3", IIf(IsDBNull(row.Item("diff_type_3").ToString), "", row.Item("diff_type_3").ToString))
        htSize.Add("diff_type_4", IIf(IsDBNull(row.Item("diff_type_4").ToString), "", row.Item("diff_type_4").ToString))
        Dim sizeCol As String
        sizeCol = GetSizeColumn(htSize).ToString()

        If sizeCol = String.Empty Then
            row.Item("size_cd") = String.Empty

        Else

            row.Item("size_cd") = row.Item(sizeCol).ToString
        End If



        Return row
    End Function

    Private Function GetSizeColumn(ByVal htSize As Hashtable) As String
        Dim entry As DictionaryEntry
        Dim strColumn As String = String.Empty

        For Each entry In htSize
            If entry.Value.ToString.Trim.ToUpper = "SIZE" Then
                strColumn = "diff_" & entry.Key.ToString().Substring(entry.Key.ToString().Length - 1, 1)
                Exit For
            End If

        Next

        Return strColumn
    End Function


    Public Function ReturnWebStatusCode(ByVal inURL As String) As String
        Try
            Dim myRequest As System.Net.HttpWebRequest
            'create an http request and send our URL string.
            myRequest = CType(System.Net.HttpWebRequest.Create(inURL), System.Net.HttpWebRequest)
            'set some timeout value so we don't wait too long for a return.
            myRequest.Timeout = 8000
            'get the response sent back from the server.
            Dim myResponse As System.Net.HttpWebResponse = CType(myRequest.GetResponse(), System.Net.HttpWebResponse)
            myResponse.Close()
            If myResponse.StatusCode = System.Net.HttpStatusCode.OK Then
                ViewState("Pic") = "Y"
                Return "OK"
            Else
                ViewState("Pic") = "N"
                Return myResponse.StatusCode.ToString
            End If
        Catch ex As Exception
            'some other http error occurred.
            Return ex.Message
        End Try
    End Function
    Public Function GETEMAIL(ByVal store_cd As String) As String
        'Put together email address
        Dim Email As String
        Dim strStore As String
        strStore = store_cd.PadLeft(4, "0")
        If strStore = GetInternetStore() Then
            Email = "InternetFulfillment" + "@hottopic.com"
        Else
            Email = "s" + strStore + "@hottopic.com"
        End If

        Return Email

    End Function
    Public Function Phone(ByVal store_cd As String) As String
        'Put together email address
        Dim strPhone As String
        Dim intStore As Integer
        Dim test As Boolean
        intStore = CType(store_cd, Integer)

        Dim commandString As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("Intranet").ConnectionString)
        commandString = "select phone1 as phone from store where storenum = '" & intStore & "' "
        Dim objCommand As New SqlClient.SqlCommand(commandString, objConnection)
        objConnection.Open()
        Dim objDataReader As SqlClient.SqlDataReader
        objDataReader = objCommand.ExecuteReader()
        Do While objDataReader.Read
            test = IsDBNull(objDataReader("phone"))
            If test = False Then
                strPhone = objDataReader("phone")
            Else
                strPhone = " "
            End If
        Loop
        objConnection.Close()
        objDataReader.Close()
        objCommand.Dispose()

        Return strPhone
    End Function

    Public Function GetShipDate(ByVal strStoreCd As String, ByVal strShipDate As String) As String

        'If strStoreCd = GetInternetStore() Then

        '    Dim strSQL As String
        '    Dim objORACommand As New OleDbCommand
        '    Dim objORAConnection As New OleDbConnection(ConfigurationManager.ConnectionStrings("WMS").ConnectionString)
        '    Dim objORADataReader As OleDbDataReader
        '    Dim shipdate As String
        '    Dim schema As String = ConfigurationManager.ConnectionStrings("WMS_Schema").ConnectionString

        '    objORAConnection.Open()

        '    strSQL = "select max(outpt_pkt_hdr.ship_date) as ship_date  from " & schema & ".Item_Master," & schema & ".Carton_Hdr," & schema & ".Outpt_Pkt_Hdr " _
        '        & "where(ITEM_MASTER.SKU_ID = CARTON_HDR.SKU_ID And CARTON_HDR.PKT_CTRL_NBR = Outpt_Pkt_Hdr.PKT_CTRL_NBR) " _
        '        & "and outpt_pkt_hdr.shipto = '" & GetInternetStore() & "' and ITEM_MASTER.DSP_SKU = '" & str_sku & "' and CARTON_HDR.STAT_CODE = 90 "

        '    objORACommand.Connection = objORAConnection
        '    objORACommand.CommandText = strSQL
        '    objORADataReader = objORACommand.ExecuteReader()

        '    If objORADataReader.Read() Then
        '        If IsDBNull(objORADataReader("ship_date")) Then
        '            shipdate = ""
        '        Else
        '            shipdate = objORADataReader("ship_date")
        '        End If

        '        objORAConnection.Close()
        '        objORACommand.Dispose()
        '        objORADataReader.Close()

        '        Return shipdate
        '    Else
        '        shipdate = ""
        '    End If

        'Else
        '    Return strShipDate
        'End If
        Return strShipDate

    End Function

    Public Function GetInv(ByVal store_cd As String) As String
        'This function actually gets the inventory qty.  Fill Inventory gets the description, store name, etc.
        Dim commandString As String
        Dim strReturn As String

        'MOD 04/12/12 Michelle An
        ' Switched connection back to GERS for Internet stores
        'MOD 05/23/07 Davendar Kaur 
        ' Changed connection and query to pull 'on_hand_qty' from Oracle table from Blue Martini PRDBMS.hottopic.com

        'If store_cd = GetInternetStore() Then
        'Dim objORAConnection As New OleDbConnection(ConfigurationManager.ConnectionStrings("Internet").ConnectionString)
        'Dim objORACommand As New OleDbCommand
        'Dim objORADataReader As OleDbDataReader

        'objORAConnection.Open()

        'commandString = "select on_hand_qty as avail_qty from HT_CUSTOM.HT_INVENTORY where sku_code like '" & str_sku & "%' "
        'objORACommand.Connection = objORAConnection
        'objORACommand.CommandText = commandString

        'objORADataReader = objORACommand.ExecuteReader()
        'Do While objORADataReader.Read
        '    strReturn = objORADataReader("avail_qty")
        'Loop
        'objORAConnection.Close()
        'objORADataReader.Close()
        'objORACommand.Dispose()
        'If Len(strReturn) > 0 Then
        'Else
        '    strReturn = ""

        'End If
        'Return strReturn
        'Else

        commandString = "SELECT /*+ordered*/ "
        commandString &= "a_gm_inv_loc.avail_qty "
        commandString &= "FROM gm_inv_loc a_gm_inv_loc,gm_itm b,store d,pos_term p "
        commandString &= "WHERE SUBSTR(a_gm_inv_loc.sku_num,1,6)=b.itm_cd "
        commandString &= "AND " & GetDivisionCodeClause()
        commandString &= "AND a_gm_inv_loc.store_cd=d.store_cd "
        commandString &= "AND d.store_cd=p.store_cd "
        commandString &= "AND p.term_num = DECODE(d.store_cd, '5990','05','01')"
        commandString &= "AND (p.lst_trn_dt > (SYSDATE -5) AND p.lst_trn_dt < SYSDATE) "
        commandString &= "AND a_gm_inv_loc.sku_num='" & str_sku & "' "
        commandString &= "AND a_gm_inv_loc.LOC_CD = 'RECV' "
        commandString &= "AND a_gm_inv_loc.store_cd = '" & store_cd & "' AND a_gm_inv_loc.avail_qty > 0 "

        Dim myConnection As New OleDb.OleDbConnection(ConfigurationManager.ConnectionStrings("GERS_OLEDB").ConnectionString)
        Dim myCommand As New OleDb.OleDbCommand(commandString, myConnection)
        myConnection.Open()
        Dim myReader As OleDb.OleDbDataReader
        myReader = myCommand.ExecuteReader()
        Do While myReader.Read
            strReturn = myReader("avail_qty")
        Loop
        myConnection.Close()
        myReader.Close()
        myCommand.Dispose()

        If Len(strReturn) > 0 Then
            '    strReturn = Mid(strReturn, 1, Len(strReturn) - 1)
        Else
            strReturn = ""
        End If

        Return strReturn
        'End If



    End Function
    Private Function InternetStoreFilterSql() As String
        ' Queries BMS tables for current OH_INV and builds filter sql if there is no inventory
        Dim internetInv As String = GetInv(GetInternetStore())
        Dim filterSql As String = ""
        If internetInv = "" OrElse internetInv = "0" Then
            filterSql = " AND a_gm_inv_loc.store_cd <> '" & GetInternetStore() & "' "
        End If
        Return filterSql
    End Function


    Private Sub FillInventory(ByVal str_sku As String)
        'This fills the dgPL (Product Locator) datagrid.
        Dim myDataTable As DataTable
        Dim commandString As String
        ' Dim connectionString As String
        Dim objDAL As New clsDAL

        With dgPL
            .AllowPaging = True
            .PagerStyle.Mode = PagerMode.NumericPages
            .PagerStyle.PageButtonCount = 3
            .PageSize = 11
        End With

        Store = GetStore()

        lblSKU.Text = "SKU: " & str_sku
        If rblArea.SelectedItem.Value = "All" Then
            'Grab inventory for all stores in Hot Topic Company (Not Torrid)
            '--retrieving by dept_cd is faster than div_cd in this case.
            '-- check internet store OH_QTY
            txtStore.Text = ""
            commandString = "SELECT /*+ordered*/ "
            commandString &= "store, "
            commandString &= "available_qty, "
			commandString &= "convert(varchar,lst_rcvd_dt,101) as lst_rcvd_dt, "
            commandString &= "s.StoreName as store_name "
            commandString &= "FROM item_inv i join " & GetDataCodeClause() & ".dbo.store s on i.store = s.StoreNum"
            commandString &= " WHERE "
            commandString &= "i.item ='" & str_sku & "'"
            ' commandString &= "AND store in (select distinct store from hottopic2.storehttd_v_org_hier "
            commandString &= " "
            commandString &= "AND available_qty > 0 "
            commandString &= "ORDER BY available_qty DESC "

        ElseIf rblArea.SelectedItem.Value = "Region" Then
            'Grab inventory for this store's region
            txtStore.Text = ""
            commandString = "SELECT /*+ordered*/ "
            commandString &= "store, "
            commandString &= "available_qty, "
			commandString &= "convert(varchar,lst_rcvd_dt,101) as lst_rcvd_dt, "
            commandString &= "s.StoreName as store_name "
            commandString &= "FROM item_inv i join " & GetDataCodeClause() & ".dbo.store s on i.store = s.StoreNum"
            commandString &= " WHERE "
            commandString &= "i.item ='" & str_sku & "'"
            'commandString &= "AND store in (select distinct store from httd_v_org_hier "
            commandString &= " and s.region =(select region from " & GetDataCodeClause() & ".dbo.store where storeNum ='" & Integer.Parse(Store).ToString() & "' ) "
            commandString &= "AND available_qty > 0 "
            commandString &= "ORDER BY available_qty DESC "

        ElseIf rblArea.SelectedItem.Value = "District" Then
            'Grab inventory for this store's district
            txtStore.Text = ""
            commandString = "SELECT /*+ordered*/ "
            commandString &= "store, "
            commandString &= "available_qty, "
			commandString &= "convert(varchar,lst_rcvd_dt,101) as lst_rcvd_dt, "
            commandString &= "s.StoreName as store_name "
            commandString &= "FROM item_inv i join " & GetDataCodeClause() & ".dbo.store s on i.store = s.StoreNum"
            commandString &= " WHERE "
            commandString &= "i.item ='" & str_sku & "'"
            'commandString &= "AND store in (select distinct store from httd_v_org_hier "
            commandString &= " and s.district =(select district from " & GetDataCodeClause() & ".dbo.store where storeNum='" & Integer.Parse(Store).ToString() & "' ) "
            commandString &= "AND available_qty > 0 "
            commandString &= "ORDER BY available_qty DESC "

        ElseIf rblArea.SelectedItem.Value = "Internet" Then
            'Grab inventory for Internet Store
            txtStore.Text = ""
            commandString = "SELECT /*+ordered*/ "
            commandString &= "store, "
            commandString &= "available_qty, "
			commandString &= "convert(varchar,lst_rcvd_dt,101) as lst_rcvd_dt, "
            commandString &= "s.StoreName as store_name "
            commandString &= "FROM item_inv i join " & GetDataCodeClause() & ".dbo.store s on i.store = s.StoreNum"
            commandString &= " WHERE "
            commandString &= "i.item ='" & str_sku & "'"
            'commandString &= "AND store in (select distinct store from httd_v_org_hier "
            commandString &= " and store= '" & Integer.Parse(GetInternetStore()).ToString() & "' "
            commandString &= "AND available_qty > 0 "
            commandString &= "ORDER BY available_qty DESC "
            ' commandString &= InternetStoreFilterSql()

        ElseIf rblArea.SelectedItem.Value = "Store" Then
            'Grab inventory for only this store
            txtStore.Text = ""
            commandString = "SELECT /*+ordered*/ "
            commandString &= "store, "
            commandString &= "available_qty, "
			commandString &= "convert(varchar,lst_rcvd_dt,101) as lst_rcvd_dt, "
            commandString &= "s.StoreName as store_name "
            commandString &= "FROM item_inv i join " & GetDataCodeClause() & ".dbo.store s on i.store = s.StoreNum"
            commandString &= " WHERE "
            commandString &= "i.item ='" & str_sku & "'"
            'commandString &= "AND store in (select distinct store from httd_v_org_hier "
            commandString &= " and store='" & Integer.Parse(Store).ToString() & "' "
            commandString &= "AND available_qty > 0 "
            commandString &= "ORDER BY available_qty DESC "
        End If

        myDataTable = objDAL.SelectOracleSQL(commandString)

        If Len(_rockStoreFilterSql.Trim) > 0 Then
            myDataTable.DefaultView.RowFilter = _newStoreFilterSql & " And " & _rockStoreFilterSql
        Else
            myDataTable.DefaultView.RowFilter = _newStoreFilterSql
        End If


        '***************************************************************
        Dim nRows As Integer = myDataTable.DefaultView.Count
        If nRows = 0 Then
            lblResults.Visible = True
            lblResults.Text = "There is no inventory."
            'Make sure datagrid doesn't show up

            With dgPL
                .AllowPaging = False
            End With

            dgPL.DataSource = ""
            dgPL.DataBind()
            dgPL.Visible = False
            ViewState("AreaChanged") = 0
            ViewState("SKUChanged") = 0
            ViewState("DPChanged") = 0
        Else
            'There are results so bind the data.
            lblResults.Visible = False
            dgPL.Visible = True
            dgPL.DataSource = myDataTable
            AreaChanged = ViewState("AreaChanged")
            SKUChanged = ViewState("SKUChanged")
            DPChanged = ViewState("DPChanged")
            If AreaChanged = 1 Then
                dgPL.CurrentPageIndex = 0
                ViewState("AreaChanged") = 0
            ElseIf SKUChanged = 1 Then
                dgPL.CurrentPageIndex = 0
                ViewState("SKUChanged") = 0
            ElseIf DPChanged = 1 Then
                dgPL.CurrentPageIndex = 0
                ViewState("DPChanged") = 0
            End If
            dgPL.DataBind()
        End If

    End Sub

    Private Sub rblArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rblArea.SelectedIndexChanged
        'User changes area to search for inventory
        ViewState("AreaChanged") = 1

        If NRows = 1 Then
            str_sku = CType(dgDetails.Items(0).Cells(0).Controls(0), LinkButton).Text
            Call FillInventory(str_sku)
        ElseIf NRows > 1 Then
            If str_sku = "" Then
                lblResults.Visible = True
                lblResults.Text = "Please select a SKU to find product location."
            Else
                Call FillInventory(str_sku)
            End If
        End If

    End Sub
    Private Sub dgdetails_SKUSelected(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDetails.ItemCommand
        'If statement checks if it is the paging being executed or the linkbutton.
        dgPL.Visible = True
        lblResults.Visible = False
        lblsize.Visible = True
        lblSKU.Visible = True

        Dim Size As String
        Dim DGPLTransType As DataGridItem

        If CType(e.CommandSource, LinkButton).CommandName = "SKUSelected" Then
            DGPLTransType = dgDetails.Items(e.Item.ItemIndex)
            Size = DGPLTransType.Cells(2).Text
            lblsize.Text = "Size: " & Size

            str_sku = CType(dgDetails.Items(e.Item.ItemIndex).Cells(0).Controls(0), LinkButton).Text
            ViewState("SKUChanged") = 1
            ViewState("SKUSelected") = str_sku
            FillInventory(str_sku) 'refill product locator results. 
        End If
    End Sub
    Private Sub dgPL_PageIndexChanged(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgPL.PageIndexChanged
        ViewState("AreaChanged") = 0
        ViewState("SKUChanged") = 0
        dgPL.CurrentPageIndex = e.NewPageIndex
        If str_sku = "" Then
            'If user changes area on default sku shown in product locator.  (otherwise, str_sku is blank.)
            str_sku = CType(dgDetails.Items(0).Cells(0).Controls(0), LinkButton).Text
        End If
        Call FillInventory(str_sku)
        dgPL.DataBind()
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strStoreNum As String
        Dim commandstring As String
        Dim intLenStore As Integer
        Dim intMode As Integer
        Dim strMode As String
        Dim myDataTable As DataTable
        If NRows = 1 Then
            str_sku = CType(dgDetails.Items(0).Cells(0).Controls(0), LinkButton).Text
        End If
        'strStoreNum = CType(txtStore.Text.TrimStart("0"c), Integer)
        strStoreNum = txtStore.Text
        If strStoreNum <> "" Then
            strStoreNum = strStoreNum.PadLeft(4, "0"c)
            strMode = ConfigurationManager.AppSettings("Mode")
            intMode = InStr(strMode, "=", CompareMethod.Text)
            strMode = Mid(strMode, intMode + 1, 5)
            If Not IsNumeric(txtStore.Text) Then
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(" Store Number cannot be a non numeric value") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
                Exit Sub
            End If

            If Trim(UCase(strMode)) = "HOTTO" Then
                If strStoreNum < 5000 Then

                    txtStore.Text = ""
                    commandstring = "SELECT /*+ordered*/ "
                    commandstring &= "store, "
                    commandstring &= "available_qty, "
                    commandstring &= "lst_rcvd_dt, "
                    commandstring &= "s.StoreName as store_name "
                    commandstring &= "FROM item_inv i join hottopic2.dbo.store s on i.store = s.StoreNum"
                    commandstring &= " WHERE "
                    commandstring &= "item ='" & str_sku & "'"
                    ' commandstring &= "AND store in (select distinct store from httd_v_org_hier "
                    commandstring &= " and i.store='" & Integer.Parse(strStoreNum).ToString() & "' "
                    commandstring &= "AND available_qty > 0 "
                    commandstring &= "ORDER BY available_qty DESC "

                Else
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(" Only Hot Topic stores can be entered") & "','Error','center=1;scroll=0;status=no;dialogWidth=300px;dialogHeight=180px;');")
                    Exit Sub
                End If
            ElseIf Trim(UCase(strMode)) = "TORRI" Then
                If strStoreNum > 5000 Then
                    txtStore.Text = ""
                    commandstring = "SELECT /*+ordered*/ "
                    commandstring &= "store, "
                    commandstring &= "available_qty, "
                    commandstring &= "lst_rcvd_dt, "
                    commandstring &= "s.StoreName as store_name "
                    commandstring &= "FROM item_inv i join torrid.dbo.store s on i.store = s.StoreNum"
                    commandstring &= " WHERE "
                    commandstring &= "item ='" & str_sku & "'"
                    ' commandstring &= "AND store in (select distinct store from httd_v_org_hier "
                    commandstring &= " and i.store='" & Integer.Parse(strStoreNum).ToString() & "' "
                    commandstring &= "AND available_qty > 0 "
                    commandstring &= "ORDER BY available_qty DESC "
                Else
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(" Only Torrid stores can be entered") & "','Error','center=1;scroll=0;status=no;dialogWidth=300px;dialogHeight=180px;');")
                    Exit Sub
                End If
            ElseIf Trim(UCase(strMode)) = "BLACK" Then
                If strStoreNum > 8000 Then
                    txtStore.Text = ""
                    commandstring = "SELECT /*+ordered*/ "
                    commandstring &= "store, "
                    commandstring &= "available_qty, "
                    commandstring &= "lst_rcvd_dt, "
                    commandstring &= "s.StoreName as store_name "
                    commandstring &= "FROM item_inv i join blackheart.dbo.store s on i.store = s.StoreNum"
                    commandstring &= " WHERE "
                    commandstring &= "item ='" & str_sku & "' "
                    ' commandstring &= "AND store in (select distinct store from httd_v_org_hier "
                    commandstring &= "and i.store='" & Integer.Parse(strStoreNum).ToString() & "' "
                    commandstring &= "AND available_qty > 0 "
                    commandstring &= "ORDER BY available_qty DESC "
                Else
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(" Only Torrid stores can be entered") & "','Error','center=1;scroll=0;status=no;dialogWidth=300px;dialogHeight=180px;');")
                    Exit Sub
                End If
            ElseIf Trim(UCase(strMode)) = "LOVES" Then
                If strStoreNum > 9000 Then
                    txtStore.Text = ""
                    commandstring = "SELECT /*+ordered*/ "
                    commandstring &= "store, "
                    commandstring &= "available_qty, "
                    commandstring &= "lst_rcvd_dt, "
                    commandstring &= "s.StoreName as store_name "
                    commandstring &= "FROM item_inv i join lovesick.dbo.store s on i.store = s.StoreNum"
                    commandstring &= " WHERE "
                    commandstring &= "item ='" & str_sku & "' "
                    ' commandstring &= "AND store in (select distinct store from httd_v_org_hier "
                    commandstring &= "and i.store='" & Integer.Parse(strStoreNum).ToString() & "' "
                    commandstring &= "AND available_qty > 0 "
                    commandstring &= "ORDER BY available_qty DESC "
                Else
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(" Only Torrid stores can be entered") & "','Error','center=1;scroll=0;status=no;dialogWidth=300px;dialogHeight=180px;');")
                    Exit Sub
                End If

            End If
        Else
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(" Please enter a store number     ") & "','Error','center=1;scroll=0;status=no;dialogWidth=300px;dialogHeight=180px;');")
            Exit Sub
        End If
        Dim objDAL As New clsDAL
        myDataTable = objDAL.SelectOracleSQL(commandstring)


        If Len(_rockStoreFilterSql.Trim) > 0 Then
            myDataTable.DefaultView.RowFilter = _newStoreFilterSql & " And " & _rockStoreFilterSql
        Else
            myDataTable.DefaultView.RowFilter = _newStoreFilterSql
        End If

        Dim nRows1 As Integer = myDataTable.DefaultView.Count

        If nRows1 = 0 Then
            lblResults.Visible = True
            lblResults.Text = "There is no inventory."
            'Make sure datagrid doesn't show up

            With dgPL
                .AllowPaging = False
            End With

            dgPL.DataSource = ""
            dgPL.DataBind()
            dgPL.Visible = False
            ViewState("AreaChanged") = 0
            ViewState("SKUChanged") = 0
            ViewState("DPChanged") = 0
        Else
            'There are results so bind the data.
            lblResults.Visible = False
            dgPL.Visible = True
            dgPL.DataSource = myDataTable
            AreaChanged = ViewState("AreaChanged")
            SKUChanged = ViewState("SKUChanged")
            DPChanged = ViewState("DPChanged")
            dgPL.CurrentPageIndex = 0
            ViewState("AreaChanged") = 0

            dgPL.DataBind()
        End If
    End Sub

    Private Sub BuildNewStoreFilterStr()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("Intranet").ConnectionString)
        Dim strStores As String

        objConnection.Open() 'open connection

        strStoredProcedure = "spGetNewStoreFilter"

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader() 'fill datareader

        Do While objDataReader.Read
            strStores += " store <> " & objDataReader("StoreNum") & " AND "
        Loop

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        If Len(strStores) > 0 Then
            strStores = Mid(strStores, 1, Len(strStores) - 4)
        Else
            strStores = ""
        End If

        _newStoreFilterSql = ""
        If ConfigurationManager.AppSettings("NewStoreFilterFlag").ToString.ToUpper = "ON" Then
            If strStores <> "" Then
                _newStoreFilterSql = strStores
            End If
        End If
    End Sub


    Private Sub BuildRockStoreFilterStr()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("Intranet").ConnectionString)
        Dim strStores As String

        objConnection.Open() 'open connection

        strStoredProcedure = "spGetRockStores"

        Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        Dim objDataReader As SqlClient.SqlDataReader

        objDataReader = objCommand.ExecuteReader() 'fill datareader

        Do While objDataReader.Read
            strStores += " store <> " & objDataReader("StoreNum") & " AND "
        Loop

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        If Len(strStores) > 0 Then
            strStores = Mid(strStores, 1, Len(strStores) - 4)
        Else
            strStores = ""
        End If


        _rockStoreFilterSql = ""
        If Application("HTRockStoreFilterFlag").ToString.ToUpper = "ON" Then
            If strStores <> "" Then
                _rockStoreFilterSql = strStores
            End If
        End If
    End Sub

    Private Function GetDivisionCodeClause() As String
        Dim strDivisionCodeClause As String
        strDivisionCodeClause = " zone_id in (" & ConfigurationManager.AppSettings("DivisionCode") & ") "
        Return strDivisionCodeClause
    End Function

    Private Function GetDivisionCodeClause_Item() As String
        Dim strDivisionCodeClause As String
        strDivisionCodeClause = " zone_id in (" & ConfigurationManager.AppSettings("DivisionCode") & ") "
        Return strDivisionCodeClause
    End Function

    Private Function GetDataCodeClause() As String
        Dim strDataCodeClause As String
        Dim strZone As String
        strZone = ConfigurationManager.AppSettings("DivisionCode")
        If strZone = "'5'" Then
            strDataCodeClause = "torrid"
        ElseIf strZone = "'8'" Then
            strDataCodeClause = "blackheart"
        ElseIf strZone = "'9'" Then
            strDataCodeClause = "lovesick"
        Else
            strDataCodeClause = "hottopic2"
        End If

        Return strDataCodeClause
    End Function


    Private Function GetInternetStore() As String
        Return ConfigurationManager.AppSettings("InternetStore")
    End Function

    Private Function GetStore() As String
        'Check both the store cookie and the IP-to-store lookup table
        'IP-to-store lookup value overrides cookie if both are found
        Dim strStoreNo As String

        If Not Request.Cookies("StoreNo") Is Nothing Then
            strStoreNo = Server.HtmlEncode(Request.Cookies("StoreNo").Value)
        End If

        Dim iStoreNumberFromDB As Integer = New PLU.StoreNumberHelper().GetStoreFromIP(Me.Request.ServerVariables("REMOTE_ADDR"))
        If iStoreNumberFromDB > 0 Then
            'Store number from IP lookup overrides cookie; need to format the number properly, though
            strStoreNo = iStoreNumberFromDB.ToString("0000")
        End If

        Return strStoreNo
    End Function
End Class
