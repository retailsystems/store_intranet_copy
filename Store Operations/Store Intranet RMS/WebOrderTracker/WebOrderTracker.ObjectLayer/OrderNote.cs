﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebOrderTracker.ObjectLayer
{
	/// <summary>
	/// 
	/// </summary>
	public class OrderNote
	{
		/// <summary>
		/// Order Number [8 digit]
		/// </summary>
		public string OrderNumber { get; set; }
	}
}
