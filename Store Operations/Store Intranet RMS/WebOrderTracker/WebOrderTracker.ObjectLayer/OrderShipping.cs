﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebOrderTracker.ObjectLayer
{
	/// <summary>
	/// Order shipping information for each shipment
	/// </summary>
	public class OrderShipping
	{
		/// <summary>
		/// Order Number [8 digit]
		/// </summary>
		public string OrderNumber { get; set; }
		/// <summary>
		/// Full Order Number [12 digit]
		/// </summary>
		public string FullOrderNumber { get; set; }
		/// <summary>
		/// Big Status
		/// </summary>
		public string BigStatus { get; set; }
		/// <summary>
		/// Amount 1
		/// </summary>
		public float Amount1 { get; set; }
		/// <summary>
		/// Shipped Date
		/// </summary>
		public DateTime DateX { get; set; }
		/// <summary>
		/// Posted Date
		/// </summary>
		public DateTime PostedDate { get; set; }
		/// <summary>
		/// Amount 2
		/// </summary>
		public float Amount2 { get; set; }
	}
}
