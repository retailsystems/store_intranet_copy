﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebOrderTracker.ObjectLayer
{
	/// <summary>
	/// Order information for the entire order, across all shipments
	/// </summary>
	public class OrderHeader
	{
		/// <summary>Order Number [8 digit]</summary>
		public string OrderNumber { get; set; }
		/// <summary>Source</summary>
		public string Source { get; set; }
		/// <summary>Store Number</summary>
		public string StoreNumber { get; set; }
		/// <summary>Payment Method</summary>
		public string PayMethod { get; set; }
		/// <summary>Division</summary>
		public string Division { get; set; }
		/// <summary>Entry Date</summary>
		public DateTime EntryDate { get; set; }
		/// <summary>Product Dollars</summary>
		public float ProductDollars { get; set; }
		/// <summary>Customer First Name</summary>
		public string FirstName { get; set; }
		/// <summary>Customer Last Name</summary>
		public string LastName { get; set; }
		/// <summary>Customer Full Name</summary>
		public string FullName { get; set; }
		/// <summary>Customer Address Street</summary>
		public string Street { get; set; }
		/// <summary>Customer Address City</summary>
		public string City { get; set; }
		/// <summary>Customer Address State</summary>
		public string State { get; set; }
		/// <summary>Customer Address Zip</summary>
		public string Zip { get; set; }
		/// <summary>Customer Email Address</summary>
		public string Email { get; set; }
		/// <summary>Ship Source</summary>
		public string ShipSource { get; set; }
		/// <summary>Order Header Status</summary>
		public string Status { get; set; }
		/// <summary>Customer Phone Number</summary>
		public string Phone { get; set; }
		/// <summary>Last Shipment Date</summary>
		public DateTime LastShipment { get; set; }
		/// <summary>Number of Packages in Entire Order</summary>
		public short NumberOfPackages { get; set; }
	}
}
