﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebOrderTracker.ObjectLayer
{
	/// <summary>
	/// Order information for each line of the shipment
	/// </summary>
	public class OrderItem
	{
		/// <summary>
		/// Order Number [8 digit]
		/// </summary>
		public string OrderNumber { get; set; }
		/// <summary>
		/// Full Order Number [12 digit]
		/// </summary>
		public string FullOrderNumber { get; set; }
		/// <summary>
		/// Ship Date
		/// </summary>
		public DateTime ShipDate { get; set; }
		/// <summary>
		/// Ship Method
		/// </summary>
		public string ShipMethod { get; set; }
		/// <summary>
		/// Tax Rate
		/// </summary>
		public float TaxRate { get; set; }
		/// <summary>
		/// Line Number
		/// </summary>
		public short LineNumber { get; set; }
		/// <summary>
		/// EDP Number
		/// </summary>
		public int EDPNumber { get; set; }
		/// <summary>
		/// SKU Number
		/// </summary>
		public string SKU { get; set; }
		/// <summary>
		/// SKU Description
		/// </summary>
		public string Description { get; set; }
		/// <summary>
		/// Quantity
		/// </summary>
		public short Quantity { get; set; }
		/// <summary>
		/// Price
		/// </summary>
		public float Price { get; set; }
		/// <summary>
		/// Cost
		/// </summary>
		public float Cost { get; set; }
		/// <summary>
		/// Item Status
		/// </summary>
		public char ItemStatus { get; set; }
		/// <summary>
		/// Order Status
		/// </summary>
		public char OrderStatus { get; set; }
		/// <summary>
		/// Source
		/// </summary>
		public string Source { get; set; }
	}
}
