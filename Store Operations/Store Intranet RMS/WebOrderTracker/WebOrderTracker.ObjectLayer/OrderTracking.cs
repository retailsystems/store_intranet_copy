﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebOrderTracker.ObjectLayer
{
	/// <summary>
	/// Order Tracking information for each shipment
	/// </summary>
	public class OrderTracking
	{
		/// <summary>
		/// Order Number [8 digit]
		/// </summary>
		public string OrderNumber { get; set; }
		/// <summary>
		/// Full Order Number [12 digit]
		/// </summary>
		public string FullOrderNumber { get; set; }
		/// <summary>
		/// Tracking Number
		/// </summary>
		public string TrackingNumber { get; set; }
		/// <summary>
		/// Shipping Weight
		/// </summary>
		public float Weight { get; set; }
		/// <summary>
		/// Shipping Date
		/// </summary>
		public DateTime ShipDate { get; set; }
		/// <summary>
		/// Date Changed
		/// </summary>
		public DateTime DateChanged { get; set; }
		/// <summary>
		/// Actual Weight
		/// </summary>
		public float ActualWeight { get; set; }
		/// <summary>
		/// Phone
		/// </summary>
		public string Phone { get; set; }
	}
}
