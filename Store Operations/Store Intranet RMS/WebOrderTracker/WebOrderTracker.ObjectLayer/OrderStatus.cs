﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebOrderTracker.ObjectLayer
{
	/// <summary>
	/// Order Status information for each shipment
	/// </summary>
	public class OrderStatus
	{
		/// <summary>
		/// Order Number [8 digit]
		/// </summary>
		public string OrderNumber { get; set; }
		/// <summary>
		/// Full Order Number [12 digit]
		/// </summary>
		public string FullOrderNumber { get; set; }
		/// <summary>
		/// Tracking Number used to Receive Order
		/// </summary>
		public string TrackingNumber { get; set; }
		/// <summary>
		/// Order Number / Tracking Number Key
		/// </summary>
		public string OrderTrackingNumber { get; set; }
		/// <summary>
		/// Date Shipment was Received in Store
		/// </summary>
		public DateTime DateReceivedInStore { get; set; }
		/// <summary>
		/// Date Shipment was Picked Up by Customer
		/// </summary>
		public DateTime DateCustomerPickedUp { get; set; }
		/// <summary>
		/// Date Shipment was Returned to DC
		/// </summary>
		public DateTime DateReturnedToDC { get; set; }

	}
}
