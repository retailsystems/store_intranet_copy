<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SetStore.aspx.vb" Inherits="Home.frmSetStore" %>

<!doctype html>
<html>
<head>
    <title>Set Your Store</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
    <meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
    <link id="Link1" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link id="Link2" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
    <iframe class="Header_Frame" id="ifmHeader" marginwidth="0" hspace="0" marginheight="0"
        src="" frameborder="0" width="100%" scrolling="no"
        runat="server"></iframe>
    <div class="hderBanner">
        <img style="max-width: 100%" src="/Global/images/head_banner.jpg" />
    </div>
    <div class="container home" role="main">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <form id="frmSetStore" method="post" runat="server">
                    <h3>
                        <asp:Label ID="lblMessage" runat="server">Message</asp:Label>
                    </h3>
                    <div class="input-group">
                        <asp:Label ID="lblSetStore" CssClass="input-group-addon" runat="server">Set Store No:&nbsp;&nbsp;</asp:Label>
                        <asp:DropDownList ID="ddlStore" CssClass="form-control" runat="server" DataValueField="StoreNum"
                            DataTextField="StoreName">
                        </asp:DropDownList>
                    </div>
                    <br />
                    <div class="text-right">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-danger" Text="Submit"></asp:Button>&nbsp;&nbsp;
                        <asp:HyperLink ID="hypReturn" runat="server" NavigateUrl="Home.aspx" CssClass="btn btn-danger">Return</asp:HyperLink>&nbsp;&nbsp;
                        <asp:HyperLink ID="hypHome" runat="server" NavigateUrl="Home.aspx" CssClass="btn btn-danger">Home</asp:HyperLink>
                    </div>
                </form>
            </div>
            <div class="col-md-3">
            </div>
        </div>
        <div class="clearfix">
        </div>
    </div>
    <script src="/Global/js/jquery.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
</body>
</html>
