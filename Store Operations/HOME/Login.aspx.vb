Partial Class frmLogin

    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim objDAL As New clsDAL()
    Dim objDataRow As DataRow
    Dim objDataTable As New DataTable()
    Dim strHome As String
    Dim strSQL As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        strHome = Request("Home")

        Select Case strHome
            Case "D", "R"
                lblID.Text = "Employee ID"
            Case Else
                lblID.Text = "Login ID"
        End Select

        lblError.Visible = False
        objDAL.FocusScript(txtID)

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=Login")

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim lngUserID As Long
        Dim strEmpID As String

        If IsNumeric(txtID.Text) Then
            strEmpID = Val(txtID.Text).ToString.PadLeft(6, "0")
        Else
            strEmpID = txtID.Text
        End If

        strSQL = "SELECT UM.User_ID, UM.Store, UM.Job_Class"
        strSQL &= ", UE.NameFirst, UE.NameLast, UE.NamePreferred, UE.JobCode"
        strSQL &= ", C.Office_Location"
        strSQL &= " FROM User_Mst UM"
        strSQL &= " LEFT JOIN taUltimateEmployee UE ON UM.User_Login = UE.EmployeeID"
        strSQL &= " LEFT JOIN Contact C ON UM.User_Login = RTRIM(C.EmpID)"
        strSQL &= " WHERE UM.User_Login = '" & Replace(strEmpID, "'", "''") & "'"
        strSQL &= " AND UM.Password = '" & Replace(txtPassword.Text, "'", "''") & "'"
        strSQL &= " AND UM.Active_Fl = 1"
        objDataTable = objDAL.SelectSQL(strSQL)

        If objDataTable.Rows.Count > 0 Then
            objDataRow = objDataTable.Rows(0)
            lngUserID = objDataRow("User_ID")
            Response.Cookies("User")("UserName") = strEmpID
            Response.Cookies("User")("ID") = lngUserID
            Response.Cookies("User")("Store") = IIf(strHome = "D" And IsNumeric(objDataRow("Office_Location") & ""), Right("0000" & Trim(objDataRow("Office_Location") & ""), 4), Trim(objDataRow("Store") & ""))
            Response.Cookies("User")("Home") = strHome
            Response.Cookies("User")("Name") = StrConv(Trim(objDataRow(IIf(IsDBNull(objDataRow("NamePreferred")), "NameFirst", "NamePreferred")) & "") & " " & Trim(objDataRow("NameLast") & ""), VbStrConv.ProperCase)

            '  get the securities that match with the users Job_Class
            strSQL = "Select * from Security where Job_Class = '" & Trim(objDataRow("Job_Class")) & "'"

            If strHome = "D" Then
                Select Case Trim(objDataRow("JobCode") & "")
                    Case "DM", "DMCA", "DMPADE", "DMTOR"
                        'OK
                    Case Else
                        strSQL = ""
                End Select
            End If

            If strSQL > "" Then
                objDataTable = objDAL.SelectSQL(strSQL)

                For Each objDataRow In objDataTable.Rows
                    Response.Cookies("User")("" & Trim(objDataRow("Function")) & "") = Trim(objDataRow("Access"))
                Next
            End If

            'get the securities that match with the user_ID
            strSQL = "Select * from Security where User_ID = " & lngUserID
            objDataTable = objDAL.SelectSQL(strSQL)

            For Each objDataRow In objDataTable.Rows
                Response.Cookies("User")("" & Trim(objDataRow("Function")) & "") = Trim(objDataRow("Access"))
            Next

            Response.Redirect("Home.aspx")
        Else
            lblError.Text = "Login attempt failed. Please make sure the " & Replace(lblID.Text, ":", "") & " and Password are correct."
            lblError.Visible = True
            txtPassword.Text = ""
        End If

    End Sub

End Class
