Partial Class frmEmpLogin

    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim objDAL As New clsDAL()

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim objHttpCookie As HttpCookie
        Dim strLocation As String
        Dim strStoreNumber As String

        'get store number from current cookie setting
        objHttpCookie = Request.Cookies("StoreNo")

        If Not IsNothing(objHttpCookie) Then
            'make sure there are no spaces
            strStoreNumber = Trim(objHttpCookie.Value)
        End If

        If IsNumeric(strStoreNumber) Then
            strLocation = "Store: " & strStoreNumber.PadLeft(4, "0")
        Else
            Response.Redirect("EmpLoginError.aspx")
        End If

        objDAL.TextboxButton(txtEmpID, btnSubmit)
        objDAL.FocusScript(txtEmpID)

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=" & strLocation & "&user=&title=Store Employee Login")

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim strMode As String
        Dim strTarget As String

        If LCase(Right(ConfigurationSettings.AppSettings("Mode"), 6)) = "torrid" Then
            strMode = "Torrid"
        Else
            strMode = "HotTopic"
        End If

        Select Case UCase(Request("Target"))
            Case "DCSS"
                strTarget = "DCSS/Home.aspx"
            Case "DCSS2"
                strTarget = "DCSS2/Home.aspx"
            Case "REPORTS2"
                strTarget = "RptMain2/ReportMain.aspx"
            Case Else
                strTarget = "RptMain/ReportMain.aspx"
        End Select

        Response.Redirect("../" & strMode & "/" & strTarget & "?EmpID=" & txtEmpID.Text)

    End Sub

End Class
