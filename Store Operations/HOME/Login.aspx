<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Login.aspx.vb" Inherits="Home.frmLogin" %>

<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
    <meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
    <link id="Link1" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link id="Link2" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
    <iframe id="ifmHeader" runat="server" class="Header_Frame" src=""
        width="100%" marginheight="0" marginwidth="0" frameborder="0" hspace="0" scrolling="no">
    </iframe>
    <div class="hderBanner">
        <img style="max-width: 100%" src="/Global/images/head_banner.jpg" />
    </div>
    <div class="container home" role="main">
        <div class="row">
            <asp:Label ID="lblError" CssClass="alert-danger myalert" role="alert" runat="server">Login attempt failed. Please make sure the ID and Password are correct.</asp:Label>
            <div class="clearfix">
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
                <form id="frmLogin" method="post" runat="server">
                    <br />
                    <div class="input-group">
                        <asp:Label ID="lblID" runat="server" CssClass="input-group-addon">Login ID</asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtID" runat="server" MaxLength="8"></asp:TextBox>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon">Password</span>
                        <asp:TextBox CssClass="form-control" ID="txtPassword" runat="server" TextMode="Password"
                            MaxLength="8"></asp:TextBox>
                    </div>
                    <br />
                    <div class="text-right">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-danger" Text="Submit"></asp:Button>&nbsp;&nbsp;
                        <asp:HyperLink ID="hypHome" runat="server" CssClass="btn btn-danger" NavigateUrl="Home.aspx">Home</asp:HyperLink>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
            </div>
        </div>
        <div class="clearfix">
        </div>
    </div>
    <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
</body>
</html>
