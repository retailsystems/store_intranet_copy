<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LoginError.aspx.vb" Inherits="Home.frmLoginError" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>LoginError</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
    <meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
    <link id="Link1" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link id="Link2" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
    <iframe id="ifmHeader" runat="server" class="Header_Frame" src=""
        width="100%" marginheight="0" marginwidth="0" frameborder="0" hspace="0" scrolling="no">
    </iframe>
    <div class="hderBanner">
        <img style="max-width: 100%" src="/Global/images/head_banner.jpg" />
    </div>
    <div class="container home" role="main">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <form id="frmLoginError" method="post" runat="server">
                    <br />
                    <span class="alert-danger myalert">Your current Login status does not allow you to access
                        this section.
                        <br /><br />
                        <div class="text-right">
                            <asp:HyperLink ID="hypHome" runat="server" CssClass="btn btn-danger" NavigateUrl="Home.aspx">Back To Home</asp:HyperLink>
                        </div>
                    </span>
                </form>
            </div>
            <div class="col-md-3">
            </div>
        </div>
    </div>
    <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
</body>
</html>
