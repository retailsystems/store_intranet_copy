Option Explicit On 

Imports System.Data.SqlClient

Public Class clsDAL

    Private blnScript As Boolean
    Private objSqlCommand As SqlCommand
    Private mstrDatabase As String

    Private Function cnnConnection() As SqlConnection

        cnnConnection = New SqlConnection(ConfigurationSettings.AppSettings(ConfigurationSettings.AppSettings("Database_" & mstrDatabase)))

    End Function

    Private Function Execute() As String

        Dim intRowsAffected As Integer

        Try
            With objSqlCommand
                .Connection = cnnConnection()
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
            End With
        Catch
            Return "Error #" & Err.Number & ": " & Err.Description
        End Try

    End Function

    Public Function ExecuteSQL(ByVal vstrSQL As String, Optional ByVal vstrDatabase As String = "Main") As String

        objSqlCommand = New SqlCommand()
        mstrDatabase = vstrDatabase

        With objSqlCommand
            .CommandText = vstrSQL
            .CommandType = CommandType.Text
        End With

        Return Execute()

    End Function

    Private Function GetDataTable() As DataTable

        Dim objSqlDataAdapter As SqlDataAdapter
        Dim objDataTable As New DataTable()

        objSqlCommand.Connection = cnnConnection()

        objSqlDataAdapter = New SqlDataAdapter(objSqlCommand)
        objSqlDataAdapter.Fill(objDataTable)

        Return objDataTable

    End Function

    Public Function SelectSQL(ByVal vstrSQL As String, Optional ByVal vstrDatabase As String = "Main") As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = vstrDatabase

        With objSqlCommand
            .CommandText = vstrSQL
            .CommandType = CommandType.Text
        End With

        Return GetDataTable()

    End Function

    'Javascript section

    Private Sub ButtonScript(ByVal objPage As Page)

        Dim objControl As Control
        Dim objHtmlForm As HtmlForm

        If Not blnScript Then
            For Each objControl In objPage.Controls
                If TypeName(objControl) = "HtmlForm" Then
                    objHtmlForm = objControl
                    objHtmlForm.Attributes.Add("onkeydown", "CatchKeyPress(window.event.keyCode, window.event.srcElement);")
                End If
            Next

            With objPage.Response
                .Write("<script language='javascript'> " & vbCrLf)
                .Write("function CatchKeyPress(KeyCode, Sender) { " & vbCrLf)
                .Write("var btnToBeClicked = null; " & vbCrLf)
                .Write("RemoveEnterAndEscEvents(); " & vbCrLf)
                .Write("if(KeyCode == '13') { " & vbCrLf)
                .Write("var ButtonName = Sender.getAttribute('TargetButton'); " & vbCrLf)
                .Write("btnToBeClicked = document.getElementById(ButtonName); " & vbCrLf)
                .Write("if(btnToBeClicked) { " & vbCrLf)
                .Write("btnToBeClicked.click(); " & vbCrLf)
                .Write("} " & vbCrLf)

                .Write("else {" & vbCrLf)
                .Write("Sender = document.getElementById('" & objHtmlForm.ID & "'); " & vbCrLf)
                .Write("ButtonName = Sender.getAttribute('TargetButton'); " & vbCrLf)
                .Write("btnToBeClicked = document.getElementById(ButtonName); " & vbCrLf)
                .Write("if(btnToBeClicked) { " & vbCrLf)
                .Write("btnToBeClicked.click(); " & vbCrLf)
                .Write("} " & vbCrLf)
                .Write("} " & vbCrLf)

                .Write("} " & vbCrLf)
                .Write("} " & vbCrLf)

                .Write("function RemoveEnterAndEscEvents() { " & vbCrLf)
                .Write("if (event.keyCode == 13 || event.keyCode == 27) { " & vbCrLf)
                .Write("event.cancelBubble = true; event.returnValue = false; " & vbCrLf)
                .Write("} " & vbCrLf)
                .Write("} " & vbCrLf)
                .Write("</script> " & vbCrLf)
            End With

            blnScript = True
        End If

    End Sub

    Public Sub FocusScript(ByVal objControl As Control)

        objControl.Page.RegisterStartupScript("focus", "<SCRIPT language='javascript'>document.getElementById('" & objControl.ID & "').focus() </SCRIPT>")

    End Sub

    Public Sub TextboxButton(ByVal objTextBox As TextBox, ByVal objButton As Control)

        objTextBox.Attributes.Add("TargetButton", objButton.ID)

        Call ButtonScript(objTextBox.Page)

    End Sub

    Public Sub ClearCookies(ByRef Request As HttpRequest, ByRef Response As HttpResponse)

        Dim intCookie As Integer
        Dim objCookie As Object
        Dim strName As String

        For intCookie = Request.Cookies.Count - 1 To 0 Step -1
            objCookie = Request.Cookies(intCookie)

            Select Case TypeName(objCookie)
                Case "HttpCookie"
                    strName = objCookie.Name
                Case "String"
                    strName = objCookie
                Case Else
                    strName = ""
            End Select

            Select Case strName
                Case "ASP.NET_SessionId", "StoreNo", ""
                    'Do nothing
                Case Else
                    Response.Cookies(strName).Expires = "1/1/1984"
                    Request.Cookies.Remove(strName)
            End Select
        Next

    End Sub

    Public Function GetUser(ByVal Request As HttpRequest, ByVal response As HttpResponse) As String

        Dim strName As String
        Dim strUserName As String

        If IsNothing(Request.Cookies("User")) Then
            response.Cookies.Add(New HttpCookie("User"))
        End If

        strUserName = Request.Cookies("User")("UserName")
        strName = Request.Cookies("User")("Name")

        Return IIf(strName = "" Or Not IsNumeric(strUserName), strUserName, strName)

    End Function

    Public Function GetStoreFromIP(Optional ByVal vstrDatabase As String = "Main") As Integer

        Dim StoreIP() As String
        Dim StoreSubnetMask() As String
        Dim strStoreSubnet As String
        Dim strTemp As String
        Dim objConnection As SqlConnection

        mstrDatabase = vstrDatabase
        objConnection = cnnConnection()

        StoreIP = Split(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), ".")
        StoreSubnetMask = Split(ConfigurationSettings.AppSettings("strStoreSubnetMask"), ".")

        strStoreSubnet = StoreIP(0) And StoreSubnetMask(0)
        strStoreSubnet = strStoreSubnet & "."
        strStoreSubnet = strStoreSubnet & (StoreIP(1) And StoreSubnetMask(1))
        strStoreSubnet = strStoreSubnet & "."
        strStoreSubnet = strStoreSubnet & (StoreIP(2) And StoreSubnetMask(2))
        strStoreSubnet = strStoreSubnet & "."
        strStoreSubnet = strStoreSubnet & (StoreIP(3) And StoreSubnetMask(3))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetStoreFromSubnet", objConnection)

        objCommand.CommandType = CommandType.StoredProcedure

        Dim parStoreSubnet As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreSubnet", SqlDbType.VarChar, 15)
        Dim parStoreNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt)

        parStoreSubnet.Direction = ParameterDirection.Input
        parStoreNum.Direction = ParameterDirection.Output

        parStoreSubnet.Value = strStoreSubnet

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()
        objConnection.Dispose()

        Return parStoreNum.Value

    End Function

End Class
