Partial Class frmSetStore

    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim objDAL As New clsDAL()
    Dim objDataRow As DataRow
    Dim objDataTable As New DataTable()
    Dim strSQL As String
    Dim strStore As String
    Dim strUser As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim intInStr As Integer
        Dim objHttpCookie As HttpCookie
        Dim objListItem As ListItem
        Dim strCompany As String
        Dim strLocation As String
        Dim strMode As String

        strMode = ConfigurationSettings.AppSettings("Mode")
        intInStr = InStrRev(strMode, "=")
        strCompany = Mid(strMode, intInStr + 1)
        strUser = objDAL.GetUser(Request, Response)
        objHttpCookie = Request.Cookies("StoreNo")

        If Not IsNothing(objHttpCookie) Then
            strStore = objHttpCookie.Value

            If IsNumeric(strStore) Then
                strLocation = "Store: " & strStore.PadLeft(4, "0")
            End If
        End If

        lblMessage.Text = "This machine is " & IIf(strStore > "", "currently set up for Store No. " & Right("0000" & strStore, 4), "not currently set up for a store.")

        If ddlStore.Items.Count = 0 Then
            strSQL = "SELECT S.StoreNum, S.StoreName"
            strSQL &= " FROM STORE S INNER JOIN"
            strSQL &= " CompanyLocations CL ON S.StoreNum BETWEEN CL.LocationLo AND CL.LocationHi"
            strSQL &= " WHERE (CL.Company = '" & Replace(strCompany, "'", "''") & "')"
            strSQL &= " ORDER BY S.StoreNum"

            ddlStore.DataSource = objDAL.SelectSQL(strSQL)
            ddlStore.DataBind()
            objListItem = ddlStore.Items.FindByValue(Val(strStore))

            If Not IsNothing(objListItem) Then
                objListItem.Selected = True
            End If
        End If

        If Not IsPostBack And Not IsNothing(Request.UrlReferrer) Then
            hypReturn.NavigateUrl = Request.UrlReferrer.ToString
        End If

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", strMode & "&location=" & strLocation & "&user=" & strUser & "&title=Set Store")

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        strStore = ddlStore.SelectedItem.Value.PadLeft(4, "0")

        'set the cookie for the currently selected store
        With Response.Cookies("StoreNo")
            .Expires = DateAdd("yyyy", 10, Today)
            .Value = strStore
        End With

        lblMessage.Text = "Store No. " & strStore & " has been set for this computer."
        lblSetStore.Visible = False
        ddlStore.Visible = False
        btnSubmit.Visible = False

        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=Store: " & strStore & "&user=" & strUser & "&title=Set Store")

    End Sub

End Class
