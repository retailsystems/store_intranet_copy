<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Home.aspx.vb" Inherits="Home.frmHome" %>

<!doctype html>
<html>
<head>
    <title>Intranet Home</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
    <meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
    <link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->
</head>
<body id="home">
    <iframe id="ifmHeader" runat="server" src="" width="100%" marginheight="0" marginwidth="0" frameborder="0" hspace="0" scrolling="no">
    </iframe>
    <div class="hderBanner">
        <img style="max-width: 100%" src="/Global/images/head_banner.jpg" />
    </div>
    <div class="container home" role="main">
        <form id="frmHome" method="post" runat="server">
            <div class="row">
                <div class="col-md-3 sideNav well">
                    <asp:DataList ID="dltLinks" runat="server" CellPadding="0" CellSpacing="0" Width="100%">
                        <ItemTemplate>
                            <div id="divLink" runat="server">
                                <asp:HyperLink ID="hypLink" runat="server">HyperLink</asp:HyperLink>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
                <div class="col-md-9">
                    <div id="login" runat="server">
                        <h5>
                            Please enter your Employee ID to see DC Shipments to Store and Reports</h5>                        
                        <div class="input-group">
                            <span class="input-group-addon">Employee Number:</span>
                            <asp:TextBox ID="txtEmpID" CssClass="form-control" runat="server" MaxLength="8"></asp:TextBox>
                        </div>
                        <br />
                        <div class="text-right">
                            <input type="submit" name="btnSubmit" value="Submit" id="btnSubmit" class="btn btn-danger"
                                runat="server" />
                        </div>
                    </div>
                    <iframe id="calendar" runat="server" style="background-color:Transparent" src=""
                        width="100%" marginheight="0" marginwidth="0" frameborder="0" hspace="0" scrolling="auto" height="606"></iframe>
                    <iframe id="report" runat="server" src=""
                        width="100%" marginheight="0" marginwidth="0" frameborder="0" hspace="0" scrolling="auto" height="800"></iframe>
                </div>

                <script language='Javascript'>
					if(window.opener != null){
						if('<%=strHome%>'=='' && !window.opener.closed){
							window.close();
						};
					};
                </script>
            </div>
        </form>
        <div class="clearfix">
        </div>
    </div>
    <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
</body>
</html>
