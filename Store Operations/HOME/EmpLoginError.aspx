<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EmpLoginError.aspx.vb" Inherits="Home.EmpLoginError" %>

<!DOCTYPE html>
<html>
<head>
    <title>Login Error</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
    <meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
    <link id="Link1" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link id="Link2" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
    <iframe id="ifmHeader" runat="server" class="Header_Frame" src=""
        width="100%" marginheight="0" marginwidth="0" frameborder="0" hspace="0" scrolling="no">
    </iframe>
    <div class="hderBanner">
        <img style="max-width: 100%" src="/Global/images/head_banner.jpg" />
    </div>
    <div class="container home" role="main">
        <form id="frmEmpLoginError" method="post" runat="server">
            <div class="alert alert-warning" role="alert">
                You have not set your store number.
                <asp:HyperLink ID="hypSetStore" runat="server" NavigateUrl='SetStore.aspx'>
							Click here to set your store.
                </asp:HyperLink>
                <br>
                You will not be able to view your reports until your store number is set.<br>
            </div>
            <div class="text-right">
                <asp:HyperLink ID="hypReturn" runat="server" NavigateUrl="Home.aspx" CssClass="btn btn-danger">Return</asp:HyperLink>&nbsp;&nbsp;
                <asp:HyperLink ID="hypHome" runat="server" NavigateUrl="Home.aspx" CssClass="btn btn-danger">Home</asp:HyperLink>
            </div>
        </form>
    </div>
    <script src="/Global/js/jquery.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
</body>
</html>
