Partial Class frmHome

    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim objDAL As New clsDAL()
    Dim objDataRow As DataRow
    Dim objDataTable As New DataTable()
    Dim strSQL As String
    Public strHome As String
	Dim intStore As Int32

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strMode As String
        Dim strStore As String
        Dim strTitle As String
        Dim strUser As String

        strMode = UCase(Request("Mode"))

        If strMode = "LOGOFF" Then
            objDAL.ClearCookies(Request, Response)
        End If

        strUser = objDAL.GetUser(Request, Response)

        If strMode = "LOGOFF" Then
            strUser = ""
            strHome = ""
        Else
            strHome = Request.Cookies("User")("Home")
        End If

        intStore = objDAL.GetStoreFromIP

        If intStore = -1 And Not Request.Cookies("StoreNo") Is Nothing Then
            intStore = Request.Cookies("StoreNo").Value
        End If

        If intStore > 0 And strHome = "" Then
            strStore = "Store: " & intStore.ToString.PadLeft(4, "0")
        End If

        strSQL = "SELECT * FROM MenuHomes"
        strSQL &= " WHERE Home = '" & strHome & "'"
        objDataTable = objDAL.SelectSQL(strSQL)

        If objDataTable.Rows.Count > 0 Then
            objDataRow = objDataTable.Rows(0)

            If objDataRow("Security") > "" Then
                If Request.Cookies("User")(objDataRow("Security")) <> "Y" Then
                    Response.Redirect("LoginError.aspx")
                End If
            End If

            strTitle = objDataRow("HeaderTitle")
        End If

		'strSQL = "SELECT * FROM HomeMenu"
		'strSQL &= " WHERE Home = '" & strHome & "'"
		'strSQL &= " AND Deleted = 0"
		'strSQL &= " ORDER BY LinkOrder"
		strSQL = "SELECT HM.*, ISNULL(S.Simon, '') AS Simon"
		strSQL &= " FROM HomeMenu HM LEFT OUTER JOIN"
		strSQL &= " GiftCardLinks GCL ON GCL.HomeMenu_Security = HM.Security LEFT OUTER JOIN"
		strSQL &= " STORE S ON S.Simon = GCL.Store_Simon AND S.StoreNum = " & intStore
		strSQL &= " WHERE (HM.Home = '" & strHome & "') AND (HM.Deleted = 0)"
		strSQL &= " ORDER BY HM.LinkOrder"
		objDataTable = objDAL.SelectSQL(strSQL)

		dltLinks.DataSource = objDataTable
		dltLinks.DataBind()

		'Use custom Stylesheet & Header
		lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
		ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=" & strStore & "&user=" & strUser & "&title=" & strTitle)

		ShowLogin()
	End Sub

    Private Sub dltLinks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dltLinks.ItemDataBound

        Dim objDiv As HtmlGenericControl
        Dim objHyperlink As HyperLink
        Dim strNavigateURL As String

        With e.Item
            objDiv = .FindControl("divLink")
            objHyperlink = .FindControl("hypLink")

            If .DataItem("Security") > "" Then
                If .DataItem("Home") > "" Then
                    objHyperlink.Visible = (Request.Cookies("User")(.DataItem("Security")) = "Y")
                Else
                    objHyperlink.Visible = (.DataItem("Simon") > "")
                End If
            End If

            If .DataItem("AlignRight") Then
                objDiv.Attributes.Add("align", "Right")
                objHyperlink.CssClass = "Login"
            End If

            objHyperlink.Text = .DataItem("LinkText")
            strNavigateURL = .DataItem("NavigateUrl")

            strNavigateURL = Replace(strNavigateURL, "[ID]", Request.Cookies("User")("ID"), , , CompareMethod.Text)
            strNavigateURL = Replace(strNavigateURL, "[USERNAME]", Request.Cookies("User")("UserName"), , , CompareMethod.Text)

            objHyperlink.NavigateUrl = strNavigateURL

            If .DataItem("NewWindow") Then
                objHyperlink.Target = "_blank"
            End If

            If .DataItem("OnClick") > "" Then
                objHyperlink.Attributes.Add("onClick", .DataItem("OnClick"))
            End If
        End With

    End Sub

	Public Sub ShowLogin()
		' show login div
		login.Visible = True
		' hide calendar iframe
		calendar.Visible = False
		' hide reports iframe
		report.Visible = False
	End Sub

	Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.ServerClick
		calendar.Attributes("src") = ConfigurationSettings.AppSettings("Calendar") & "?empid=" & txtEmpID.Text.Trim()
		report.Attributes("src") = ConfigurationSettings.AppSettings("Report") & "?empid=" & txtEmpID.Text.Trim()

		login.Visible = False
		calendar.Visible = True
		report.Visible = True
	End Sub
End Class
