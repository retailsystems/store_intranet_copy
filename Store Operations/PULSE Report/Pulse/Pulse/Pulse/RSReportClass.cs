using System;
using System.IO;
using Pulse.casql05dev;

namespace Pulse
{
    abstract class RSReportClass
    {
        protected string reportServerURL;
        protected string reportPath;
        protected string deviceInfo;
        protected string reportFileType;
        protected string reportLocalPath;
        protected string reportArchivePath;
        protected string reportPriorWeekEndPath;
        protected string reportFormat;

        protected string reportFileName;
        protected string reportFullFilePath;
        protected string reportFullPriorWeekEndPath;
        protected string reportFullArchivePath;

        protected HTError.IHTError errorHandler;
        protected int logLevel;

        protected ReportExecutionService rs;
        protected ParameterValue[] parameters;

        protected byte[] result = null;
        protected string format;
        protected string historyID = null;

        protected DataSourceCredentials[] credentials = null;
        protected string showHideToggle = null;
        protected string encoding = "";
        protected string mimeType = "";
        protected Warning[] warnings = null;
        protected ParameterValue[] reportHistoryParameters = null;
        protected string[] streamIDs = null;

        protected ExecutionInfo execInfo;
        protected ExecutionHeader execHeader;
        protected string SessionId = "";
        protected string extension = "";

        public RSReportClass(string RSURL, string rsPath, string devInfo, string rptType, string rptLocPath, string rptPriorWkEndPath, string rptArcvPath, string rptFmt, HTError.IHTError errHandler, int logLvl)
        {
            rs = new ReportExecutionService();

            reportServerURL = RSURL;
            reportPath = rsPath + rptType;
            deviceInfo = devInfo;
            reportFileType = rptType;
            reportLocalPath = rptLocPath;
            reportPriorWeekEndPath = rptPriorWkEndPath;
            reportArchivePath = rptArcvPath;
            reportFormat = rptFmt;

            reportFullFilePath = reportLocalPath + reportFileType;
            if (!reportFullFilePath.EndsWith("\\")) reportFullFilePath += "\\";

            reportFullPriorWeekEndPath = reportPriorWeekEndPath + reportFileType;
            if (!reportFullPriorWeekEndPath.EndsWith("\\")) reportFullPriorWeekEndPath += "\\";

            reportArchivePath = reportArchivePath.Replace("%yr%", DateTime.Now.Year.ToString());

            reportFullArchivePath = reportArchivePath + reportFileType;
            if (!reportFullArchivePath.EndsWith("\\")) reportFullArchivePath += "\\";

            errorHandler = errHandler;
            logLevel = logLvl;
            
            execInfo = new ExecutionInfo();
            execHeader = new ExecutionHeader();
        }

        public virtual void Load()
        {
            rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
            rs.Url = reportServerURL;

            //no timeout let job scheduler handle the timeout
            rs.Timeout = -1;

            rs.ExecutionHeaderValue = execHeader;
            execInfo = rs.LoadReport(reportPath, historyID);
        }

        protected abstract ParameterValue[] GetParameterArray();
      
        public abstract void Execute();

        public abstract void SaveReportInExcel();

        protected abstract void ArchiveReports();

        /// <summary>
        /// Method used to rename a file
        /// </summary>
        /// <param name="path">Location of file</param>
        /// <param name="fileName">The name of the file</param>
        /// <param name="newName">Name file is going to be changed to</param>
        protected bool RenameFile(string path, string fileName, string newName)
        {
            bool bRet = false;
            try
            {
                if (!String.IsNullOrEmpty(path) && !String.IsNullOrEmpty(fileName))
                {
                    if (!path.EndsWith("\\")) path += "\\";
                    FileInfo fiInfo;
                    fiInfo = new FileInfo(path + fileName);
                    if (fiInfo.Exists)
                    {
                        fiInfo.MoveTo(path + newName);
                    }

                    bRet = true;
                }
                
                return bRet;
            }
            catch (Exception ex)
            {
                errorHandler.LogError(ex);

                return bRet;
            }
        }
    }
}
