using System;
using Pulse.casql05dev;
using System.Collections.Generic;
using System.Web.Services.Protocols;
using System.Text;

namespace Pulse
{
    class District : RSReportClass
    {
        private string _Alignment;
        private string _ReportDate;
        private string _DistrictNumber;
        private string _Division;

        public District(string RSURL, string rsPath, string devInfo, string rptType, string rptLocPath, string rptPriorWkEndPath, string rptArcvPath, string rptFmt, HTError.IHTError errHandler, int logLvl)
            : base(RSURL, rsPath, devInfo, rptType, rptLocPath, rptPriorWkEndPath, rptArcvPath, rptFmt, errHandler, logLvl)
        {
        }

        public string Alignment
        {
            set
            {
                _Alignment = value;
            }
            get
            {
                return _Alignment;
            }
        }

        public string ReportDate
        {
            set
            {
                _ReportDate = value;
            }
            get
            {
                return _ReportDate;
            }
        }

        public string DistrictNumber
        {
            set
            {
                _DistrictNumber = value;
            }
            get
            {
                return _DistrictNumber;
            }
        }

        public string Division
        {
            set
            {
                _Division = value;
            }
            get
            {
                return _Division;
            }
        }

        protected override ParameterValue[] GetParameterArray()
        {
            ParameterValue[] parameters = new ParameterValue[4];

            parameters[0] = new ParameterValue();
            parameters[0].Name = "p_Alignment";
            parameters[0].Value = _Alignment;

            parameters[1] = new ParameterValue();
            parameters[1].Name = "p_ReportDate";
            parameters[1].Value = _ReportDate;

            parameters[2] = new ParameterValue();
            parameters[2].Name = "p_Division";
            parameters[2].Value = _Division;

            parameters[3] = new ParameterValue();
            parameters[3].Name = "p_district";
            parameters[3].Value = _DistrictNumber;

            return parameters;
        }

        public override void Execute()
        {
            parameters = GetParameterArray();

            if (parameters == null)
            {
                throw new Exception("Report parameters have not been set.");
            }

            rs.SetExecutionParameters(parameters, "en-us");
            SessionId = rs.ExecutionHeaderValue.ExecutionID;

            try
            {
                result = rs.Render(reportFormat, deviceInfo, out extension, out mimeType, out encoding, out warnings, out streamIDs);
                execInfo = rs.GetExecutionInfo();
            }
            catch (SoapException e)
            {
                throw new Exception(e.Detail.InnerText);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void SaveReportInExcel()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        protected override void ArchiveReports()
        {
            throw new Exception("The method or operation is not implemented.");
        }

    }
}
