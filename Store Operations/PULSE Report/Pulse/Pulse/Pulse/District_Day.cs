using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Pulse
{
    class District_Day : District
    {
        private string ReportFileName
        {
            get { return "D" + DistrictNumber + "W.xls"; }
        }

        public District_Day(string RSURL, string rsPath, string devInfo, string rptType, string rptLocPath, string rptPriorWkEndPath, string rptArcvPath, string rptFmt, HTError.IHTError errHandler, int logLvl)
            : base(RSURL, rsPath, devInfo, rptType, rptLocPath, rptPriorWkEndPath, rptArcvPath, rptFmt, errHandler, logLvl)
        {

        }

        public override void SaveReportInExcel()
        {
            // Write the contents of the report to a file.
            try
            {
                ArchiveReports();

                DirectoryInfo diDestination = new DirectoryInfo(reportFullFilePath);

                //Check if the target directory exists, if not, create it.
                if (!Directory.Exists(diDestination.FullName))
                {
                    Directory.CreateDirectory(diDestination.FullName);
                }

                FileInfo fi = new FileInfo(reportFullFilePath + ReportFileName);
                if (fi.Exists) fi.Delete();
                fi = null;

                FileStream stream = File.Create(reportFullFilePath + ReportFileName, result.Length);
                errorHandler.LogDebug(ReportFileName + " created...");
                stream.Write(result, 0, result.Length);
                errorHandler.LogDebug("Results written to the file.");
                stream.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ReportFileName + " could not be created! Please check the reporting services server. " + ex);
            }
        }

        protected override void ArchiveReports()
        {
            string dateTime = Regex.Replace(DateTime.Now.ToShortDateString(), "/", ".");
            string rptNewFileName = "D" + DistrictNumber + "W-" + dateTime + ".xls";
            string rptPriorWeekEndFileName = "D" + DistrictNumber + "W._Archive.xls";
            FileInfo finfo; 
            bool successArchive = false;
            bool successPrior = false;

            try
            {
                finfo = new FileInfo(reportFullFilePath + ReportFileName);

                if (finfo.Exists)
                {
                    DirectoryInfo diDestination = new DirectoryInfo(reportArchivePath + "\\" + reportFileType);

                    //Check if the target directory exists, if not, create it.
                    if (!Directory.Exists(diDestination.FullName))
                    {
                        Directory.CreateDirectory(diDestination.FullName);
                    }

                    finfo = new FileInfo(reportFullArchivePath + rptNewFileName);

                    if (finfo.Exists) finfo.Delete();
                    finfo = null;

                    if(DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                    {
                        DirectoryInfo diInfo = new DirectoryInfo(reportFullPriorWeekEndPath);

                        //Check if the target directory exists, if not, create it.
                        if (!Directory.Exists(diInfo.FullName))
                        {
                            Directory.CreateDirectory(diInfo.FullName);
                        }

                        finfo = new FileInfo(reportFullPriorWeekEndPath + rptPriorWeekEndFileName);

                        if (finfo.Exists) finfo.Delete();
                        finfo = null;

                        File.Copy(reportFullFilePath + ReportFileName, reportFullPriorWeekEndPath + ReportFileName);
                        successPrior = RenameFile(reportFullPriorWeekEndPath, ReportFileName, rptPriorWeekEndFileName);
                    }

                    //Move to archive folder
                    File.Move(reportFullFilePath + ReportFileName, reportFullArchivePath + ReportFileName);
                    
                    successArchive = RenameFile(reportFullArchivePath, ReportFileName, rptNewFileName);

                    errorHandler.LogDebug(ReportFileName + " was renamed to " + rptNewFileName + " and archived.");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString() + " FILE: " + reportFullFilePath + ReportFileName);
            }
        }

    }
}
