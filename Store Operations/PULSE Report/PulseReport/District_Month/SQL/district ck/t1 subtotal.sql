declare @p_reportdate as datetime
declare @p_alignment as int
declare @p_curr_store  as int
declare @p_district as int
declare @p_region as varchar(4)
set @p_alignment =1
set @p_reportdate = '08-AUG-07'
set @p_region = '20'
set @p_district =201
set @p_curr_store = 5002
SELECT     distinct x_1.WEEK_NUM, x_1.QTR, x_1.YR, @p_reportdate AS TRN_DT, 
x_1.district, x_1.Gross_sales - ISNULL(x2.Gross_sales, 0.0) 
                      - ISNULL(x3.Gross_sales, 0.0) AS gross_sales, 
x_1.return_amount - ISNULL(x2.return_amount, 0.0) - ISNULL(x3.return_amount, 0.0) AS return_amount, 
x_1.trns_count - ISNULL(x2.trns_count, 0.0) - ISNULL(x3.trns_count, 0.0) AS trns_count, 
x_1.unit_count - ISNULL(x2.unit_count, 0.0) 
                      - ISNULL(x3.unit_count, 0.0) AS unit_count, 
NULL AS emp_name, (x_1.Gross_sales - ISNULL(x2.Gross_sales, 0.0) 
                      - ISNULL(x3.Gross_sales, 0.0) + x_1.return_amount - ISNULL(x2.return_amount, 0.0) - ISNULL(x3.return_amount, 0.0)) 
                      / x_1.trns_count - ISNULL(x2.trns_count, 0.0) - ISNULL(x3.trns_count, 0.0) AS ADT, 
(x_1.unit_count - ISNULL(x2.unit_count, 0.0) - ISNULL(x3.unit_count, 
                      0.0)) / x_1.trns_count - ISNULL(x2.trns_count, 0.0) - ISNULL(x3.trns_count, 0.0) AS UPT
FROM         (SELECT     a.WEEK_NUM, a.QTR, a.YR, @p_reportdate AS TRN_DT, @p_district district , SUM(a.GROSS_SALES) AS Gross_sales, SUM(a.RETURN_AMOUNT) 
                                              AS return_amount, SUM(a.TRNS_COUNT) AS trns_count, SUM(a.UNIT_COUNT) AS unit_count
                       FROM         pulse_summary AS a INNER JOIN
                                              Alignment AS b ON b.district  = @p_district and a.store_cd = b.store_cd  and b.alignment_cd = @p_alignment
                       WHERE      (a.TRN_DT = @p_reportdate)
                       GROUP BY  b.district, a.WEEK_NUM, a.QTR, a.YR) AS x_1 LEFT OUTER JOIN
                          (SELECT     a.WEEK_NUM, a.QTR, a.YR, @p_reportdate AS TRN_DT, @p_district district, SUM(a.GROSS_SALES) AS Gross_sales, 
                                                   SUM(a.RETURN_AMOUNT) AS return_amount, SUM(a.TRNS_COUNT) AS trns_count, SUM(a.UNIT_COUNT) AS unit_count 
                                                   
                            FROM          pulse_summary AS a INNER JOIN
                                                   Alignment AS b ON b.district  = @p_district and b.alignment_cd = @p_alignment
                            WHERE       (a.TRN_DT = @p_reportdate) AND
                                                   (b.district IN (@p_district)) AND (a.TRN_DT = @p_reportdate) AND ( (LEFT(a.SALES_ASSOCIATE_NUM, 2) = '99') OR (a.SALES_ASSOCIATE_NUM = '000000'))
                            GROUP BY  a.WEEK_NUM, a.QTR, a.YR, a.TRN_DT) AS x2 ON x2.district = x_1.district LEFT OUTER JOIN
                          (SELECT     a.WEEK_NUM, a.QTR, a.YR, @p_reportdate AS TRN_DT, @p_district district, SUM(a.GROSS_SALES) AS Gross_sales, 
                                                   SUM(a.RETURN_AMOUNT) AS return_amount, SUM(a.TRNS_COUNT) AS trns_count, SUM(a.UNIT_COUNT) AS unit_count
                                                   
                            FROM          pulse_summary AS a INNER JOIN
                                                   Alignment AS b ON b.district = @p_district and b.alignment_cd = @p_alignment
                            WHERE       (a.TRN_DT = @p_reportdate) AND (a.SALES_ASSOCIATE_NUM = 'UNASSIGNED')
                            GROUP BY  a.WEEK_NUM, a.QTR, a.YR, a.TRN_DT) AS x3 ON x3.district = x_1.district