declare @p_reportdate as datetime
declare @p_alignment as int
declare @p_district as int
declare @p_region as varchar(4)
--set @p_alignment = 2
set @p_reportdate = '14-APR-07'
set @p_district = 201 as int
--set @p_region = '20'
SELECT    distinct  a.WEEK_NUM, a.QTR, a.YR, @p_reportdate AS TRN_DT, @p_district district, 
sum(a.GROSS_SALES) gross_sales, 
sum(a.RETURN_AMOUNT) return_amount, 
sum(a.TRNS_COUNT) trns_count, 
sum(a.UNIT_COUNT) unit_count, 
                      '000' AS Expr1,  ISNULL((sum(a.GROSS_SALES )+ ISNULL(sum(a.RETURN_AMOUNT), 0.)) / sum(a.TRNS_COUNT), 0.0) AS ADT, 
                      ISNULL(sum(a.UNIT_COUNT) / sum(a.TRNS_COUNT), 0.0) AS UPT
FROM         tmp_summary AS a INNER JOIN
                      Alignment AS b ON b.district = @p_district and b.alignment_cd = @p_alignment --LEFT OUTER JOIN
                      --tmp_employee AS c ON c.EMPLOYEE = a.SALES_ASSOCIATE_NUM
WHERE     (a.SALES_ASSOCIATE_NUM = 'UNASSIGNED') AND (b.region IN (@p_district)) AND (a.TRN_DT = @p_reportdate)
group by week_num, qtr, yr