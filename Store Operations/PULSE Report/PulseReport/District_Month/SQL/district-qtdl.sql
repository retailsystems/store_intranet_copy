declare @p_reportdate as datetime
declare @p_alignment as int
declare @p_curr_store  as int
declare @p_district as int
declare @p_region as varchar(4)
set @p_alignment =1
set @p_reportdate = '30-SEP-07'
set @p_region = '20'
set @p_district =201
set @p_curr_store = 5002
SELECT   distinct  x_1.store_name,  x_1.store_cd,  x_1.district,  x_1.QTR, x_1.YR, @p_reportdate AS TRN_DT,
--total
x_1.Gross_sales AS gross_sales, 
x_1.return_amount  AS return_amount, 
x_1.trns_count  AS trns_count, 
x_1.unit_count  AS unit_count, 
--(x_1.Gross_sales  + x_1.return_amount)/x_1.trns_count   AS ADT,
Case
when x_1.trns_count <> 0. then (x_1.Gross_sales  + x_1.return_amount)/x_1.trns_count
else 0.
end as ADT, 
--x_1.unit_count  / x_1.trns_count AS UPT,
Case
when x_1.trns_count <> 0. then x_1.unit_count  / x_1.trns_count
else 0.
end as UPT, 
--house
--house
(ISNULL(x2.Gross_sales, 0.0))  AS gross_sales_hs, 
ISNULL(x2.return_amount, 0.0)  AS return_amount_hs, 
ISNULL(x2.trns_count, 0.0)  AS trns_count_hs, 
ISNULL(x2.unit_count, 0.0)  AS unit_count_hs, 
Case
when x2.trns_count <> 0. then (x2.Gross_sales  + x2.return_amount)/x2.trns_count
else 0.
end as ADT_hsn,
Case
when x2.trns_count <> 0. then x2.unit_count  / x2.trns_count
else 0.
end as UPT_hs, 
--unassigned
ISNULL(x3.Gross_sales, 0.0)  AS gross_sales_un, 
ISNULL(x3.return_amount, 0.0)  AS return_amount_un, 
ISNULL(x3.trns_count, 0.0)  AS trns_count_un, 
ISNULL(x3.unit_count, 0.0)  AS unit_count_un, 
--(ISNULL(x3.Gross_sales, 0.0) + ISNULL(x2.return_amount, 0.0) )/ ( ISNULL(x2.trns_count, 0.0) ) AS ADT_un, 
Case
when x3.trns_count <> 0. then (x3.Gross_sales  + x3.return_amount)/x3.trns_count
else 0.
end as ADT_un,
Case
when x3.trns_count <> 0. then x3.unit_count  / x3.trns_count
else 0.
end as UPT_un, 
--ISNULL(x3.unit_count, 0.0) /  ISNULL(x2.trns_count, 0.0)  AS UPT_un,
--subtotal
x_1.Gross_sales - ISNULL(x2.Gross_sales, 0.0) - ISNULL(x3.Gross_sales, 0.0) AS gross_sales_sub, 
x_1.return_amount - ISNULL(x2.return_amount, 0.0) - ISNULL(x3.return_amount, 0.0) AS return_amount_sub, 
x_1.trns_count - ISNULL(x2.trns_count, 0.0) - ISNULL(x3.trns_count, 0.0) AS trns_count_sub, 
x_1.unit_count - ISNULL(x2.unit_count, 0.0) - ISNULL(x3.unit_count, 0.0) AS unit_count_sub, 
0. ADT_sub,
0. UPT_sub
--(x_1.Gross_sales - ISNULL(x2.Gross_sales, 0.0) - ISNULL(x3.Gross_sales, 0.0) + x_1.return_amount - ISNULL(x2.return_amount, 0.0) 
-- - ISNULL(x3.return_amount, 0.0))/ (x_1.trns_count - ISNULL(x2.trns_count, 0.0) - ISNULL(x3.trns_count, 0.0)) AS ADT_sub, 
--Case
--when (x_1.trns_count-x2.trns_count - x3.trns_count) <> 0. then 1 --(x_1.Gross_sales - ISNULL(x2.Gross_sales, 0.0) - ISNULL(x3.Gross_sales, 0.0) + x_1.return_amount - ISNULL(x2.return_amount, 0.0) - ISNULL(x3.return_amount, 0.0))/ (x_1.trns_count - ISNULL(x2.trns_count, 0.0) - ISNULL(x3.trns_count, 0.0)) 
--else 0.
--end as ADT
--(x_1.unit_count - ISNULL(x2.unit_count, 0.0) - ISNULL(x3.unit_count,0.0)) / 
--(x_1.trns_count - ISNULL(x2.trns_count, 0.0) - ISNULL(x3.trns_count, 0.0)) AS UPT_sub
FROM         (SELECT    b.store_name, a.store_cd, a.QTR, a.YR, @p_reportdate AS TRN_DT, @p_district district , SUM(a.GROSS_SALES) AS Gross_sales, SUM(a.RETURN_AMOUNT) 
                                              AS return_amount, SUM(a.TRNS_COUNT) AS trns_count, SUM(a.UNIT_COUNT) AS unit_count
                       FROM         pulse_summary AS a INNER JOIN
                                              Alignment AS b ON b.district  = @p_district and a.store_cd = b.store_cd  and b.alignment_cd = @p_alignment
                       WHERE   (a.TRN_DT BETWEEN
                          (SELECT     MIN(C.DT) AS Expr1
                            FROM          Calendar AS C INNER JOIN
                                                       (SELECT     MERCH_QUARTER, MERCH_YEAR
                                                         FROM          Calendar
                                                         WHERE      (DT = CONVERT(VARCHAR, @p_reportdate, 101))) AS SUBTBL ON C.MERCH_QUARTER = SUBTBL.MERCH_QUARTER AND 
                                                   C.MERCH_YEAR = SUBTBL.MERCH_YEAR) AND @p_reportdate)
                       GROUP BY  b.store_name, a.store_cd,b.district,  a.QTR, a.YR) AS x_1 LEFT OUTER JOIN
                          (SELECT     b.store_name, a.store_cd,  a.QTR, a.YR, @p_reportdate AS TRN_DT, @p_district AS district, SUM(a.GROSS_SALES) AS gross_sales, 
                      SUM(a.RETURN_AMOUNT) AS return_amount, SUM(a.TRNS_COUNT) AS trns_count, SUM(a.UNIT_COUNT) AS unit_count, ISNULL((SUM(a.GROSS_SALES) + ISNULL(SUM(a.RETURN_AMOUNT), 0)) / SUM(a.TRNS_COUNT), 0.0) AS ADT, 
                      ISNULL(SUM(a.UNIT_COUNT) / SUM(a.TRNS_COUNT), 0.0) AS UPT
FROM         pulse_summary AS a INNER JOIN
                      Alignment AS b ON b.district  = @p_district and b.alignment_cd = @p_alignment LEFT OUTER JOIN
                     employee AS c ON c.EMPLOYEE = a.SALES_ASSOCIATE_NUM
WHERE    (a.TRN_DT BETWEEN
                          (SELECT     MIN(C.DT) AS Expr1
                            FROM          Calendar AS C INNER JOIN
                                                       (SELECT     MERCH_QUARTER, MERCH_YEAR
                                                         FROM          Calendar
                                                         WHERE      (DT = CONVERT(VARCHAR, @p_reportdate, 101))) AS SUBTBL ON C.MERCH_QUARTER = SUBTBL.MERCH_QUARTER AND 
                                                   C.MERCH_YEAR = SUBTBL.MERCH_YEAR) AND @p_reportdate)  AND ( (a.SALES_ASSOCIATE_NUM = '000000') or (SUBSTRING(a.SALES_ASSOCIATE_NUM, 1, 2) = '99'))
GROUP BY  a.QTR, a.YR,b.store_name, a.store_cd) AS x2 ON x2.district = x_1.district   and x2.store_cd= x_1.store_cd LEFT OUTER JOIN
                          (SELECT    distinct  b.store_name, a.store_cd,  a.QTR, a.YR, @p_reportdate AS TRN_DT, @p_district district, 
sum(a.GROSS_SALES) gross_sales, 
sum(a.RETURN_AMOUNT) return_amount, 
sum(a.TRNS_COUNT) trns_count, 
sum(a.UNIT_COUNT) unit_count, 
                      '000' AS Expr1,  ISNULL((sum(a.GROSS_SALES )+ ISNULL(sum(a.RETURN_AMOUNT), 0.)) / sum(a.TRNS_COUNT), 0.0) AS ADT, 
                      ISNULL(sum(a.UNIT_COUNT) / sum(a.TRNS_COUNT), 0.0) AS UPT
FROM         pulse_summary AS a INNER JOIN
                      Alignment AS b ON b.district = @p_district and b.alignment_cd = @p_alignment --LEFT OUTER JOIN
                      --tmp_employee AS c ON c.EMPLOYEE = a.SALES_ASSOCIATE_NUM
WHERE     (a.SALES_ASSOCIATE_NUM = 'UNASSIGNED') AND (b.district IN (@p_district)) AND (b.district IN (@p_district)) AND (a.TRN_DT BETWEEN
                          (SELECT     MIN(C.DT) AS Expr1
                            FROM          Calendar AS C INNER JOIN
                                                       (SELECT     MERCH_QUARTER, MERCH_YEAR
                                                         FROM          Calendar
                                                         WHERE      (DT = CONVERT(VARCHAR, @p_reportdate, 101))) AS SUBTBL ON C.MERCH_QUARTER = SUBTBL.MERCH_QUARTER AND 
                                                   C.MERCH_YEAR = SUBTBL.MERCH_YEAR) AND @p_reportdate)
group by  qtr, yr, b.store_name, a.store_cd) AS x3 ON x3.district = x_1.district and x3.district = x_1.district and x3.store_cd = x_1.store_cd
order by x_1.store_cd asc