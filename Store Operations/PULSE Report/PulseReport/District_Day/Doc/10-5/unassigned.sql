declare @p_reportdate as datetime
declare @p_alignment as int
declare @p_curr_store  as int
declare @p_district as int
declare @p_region as varchar(4)
set @p_alignment =1
set @p_reportdate = '10-APR-07'
set @p_region = '21'
set @p_district =205
set @p_curr_store = 5002
SELECT     a.WEEK_NUM, a.QTR, a.YR, @p_reportdate AS TRN_DT, @p_district district , SUM(a.GROSS_SALES) AS Gross_sales, SUM(a.RETURN_AMOUNT) 
                                              AS return_amount, SUM(a.TRNS_COUNT) AS trns_count, SUM(a.UNIT_COUNT) AS unit_count
                       FROM         pulse_summary AS a INNER JOIN
                                              Alignment AS b ON b.district  = @p_district and a.store_cd = b.store_cd  and b.alignment_cd = @p_alignment
                       WHERE      (a.TRN_DT = @p_reportdate) AND 
(a.SALES_ASSOCIATE_NUM = 'UNASSIGNED')
                       GROUP BY  b.district, a.WEEK_NUM, a.QTR, a.YR 