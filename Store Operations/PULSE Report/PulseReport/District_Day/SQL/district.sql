declare @p_reportdate as datetime
declare @p_alignment as int
declare @p_curr_store  as int
declare @p_district as int
declare @p_region as varchar(4)
set @p_alignment =1
set @p_reportdate = '08-AUG-07'
set @p_region = '20'
set @p_district =201
set @p_curr_store = 5002









SELECT   distinct  x_1.store_name,  x_1.store_cd,  x_1.district, x_1.WEEK_NUM, x_1.QTR, x_1.YR, @p_reportdate AS TRN_DT,
--total
x_1.Gross_sales AS gross_sales, 
x_1.return_amount  AS return_amount, 
x_1.trns_count  AS trns_count, 
x_1.unit_count  AS unit_count, 
--(x_1.Gross_sales  + x_1.return_amount)/x_1.trns_count   AS ADT,
Case
when x_1.trns_count <> 0. then (x_1.Gross_sales  + x_1.return_amount)/x_1.trns_count
else 0.
end as ADT, 
--x_1.unit_count  / x_1.trns_count AS UPT,
Case
when x_1.trns_count <> 0. then x_1.unit_count  / x_1.trns_count
else 0.
end as UPT, 
--house
ISNULL(x2.Gross_sales, 0.0)  AS gross_sales_hs, 
ISNULL(x2.return_amount, 0.0)  AS return_amount_hs, 
ISNULL(x2.trns_count, 0.0)  AS trns_count_hs, 
ISNULL(x2.unit_count, 0.0)  AS unit_count_hs, 
--(ISNULL(x2.Gross_sales, 0.0) + ISNULL(x2.return_amount, 0.0) )/ ( ISNULL(x2.trns_count, 0.0) ) AS ADT_hs, 
Case
when x2.trns_count <> 0. then (x2.Gross_sales  + x2.return_amount)/x2.trns_count
else 0.
end as ADT_hs,
--ISNULL(x2.unit_count, 0.0) /  ISNULL(x2.trns_count, 0.0)  AS UPT_hs,
Case
when x2.trns_count <> 0. then x2.unit_count  / x2.trns_count
else 0.
end as UPT_hs, 
--unassigned
ISNULL(x3.Gross_sales, 0.0)  AS gross_sales_un, 
ISNULL(x3.return_amount, 0.0)  AS return_amount_un, 
ISNULL(x3.trns_count, 0.0)  AS trns_count_un, 
ISNULL(x3.unit_count, 0.0)  AS unit_count_un, 
--(ISNULL(x3.Gross_sales, 0.0) + ISNULL(x2.return_amount, 0.0) )/ ( ISNULL(x2.trns_count, 0.0) ) AS ADT_un, 
Case
when x3.trns_count <> 0. then (x3.Gross_sales  + x3.return_amount)/x3.trns_count
else 0.
end as ADT_un,
Case
when x3.trns_count <> 0. then x3.unit_count  / x3.trns_count
else 0.
end as UPT_un, 
--ISNULL(x3.unit_count, 0.0) /  ISNULL(x2.trns_count, 0.0)  AS UPT_un,
--subtotal
x_1.Gross_sales - ISNULL(x2.Gross_sales, 0.0) - ISNULL(x3.Gross_sales, 0.0) AS gross_sales_sub, 
x_1.return_amount - ISNULL(x2.return_amount, 0.0) - ISNULL(x3.return_amount, 0.0) AS return_amount_sub, 
x_1.trns_count - ISNULL(x2.trns_count, 0.0) - ISNULL(x3.trns_count, 0.0) AS trns_count_sub, 
x_1.unit_count - ISNULL(x2.unit_count, 0.0) - ISNULL(x3.unit_count, 0.0) AS unit_count_sub, 
0. ADT_sub,
0. UPT_sub
--(x_1.Gross_sales - ISNULL(x2.Gross_sales, 0.0) - ISNULL(x3.Gross_sales, 0.0) + x_1.return_amount - ISNULL(x2.return_amount, 0.0) 
-- - ISNULL(x3.return_amount, 0.0))/ (x_1.trns_count - ISNULL(x2.trns_count, 0.0) - ISNULL(x3.trns_count, 0.0)) AS ADT_sub, 
--Case
--when (x_1.trns_count-x2.trns_count - x3.trns_count) <> 0. then 1 --(x_1.Gross_sales - ISNULL(x2.Gross_sales, 0.0) - ISNULL(x3.Gross_sales, 0.0) + x_1.return_amount - ISNULL(x2.return_amount, 0.0) - ISNULL(x3.return_amount, 0.0))/ (x_1.trns_count - ISNULL(x2.trns_count, 0.0) - ISNULL(x3.trns_count, 0.0)) 
--else 0.
--end as ADT
--(x_1.unit_count - ISNULL(x2.unit_count, 0.0) - ISNULL(x3.unit_count,0.0)) / 
--(x_1.trns_count - ISNULL(x2.trns_count, 0.0) - ISNULL(x3.trns_count, 0.0)) AS UPT_sub
FROM         (SELECT    b.store_name, a.store_cd, a.WEEK_NUM, a.QTR, a.YR, @p_reportdate AS TRN_DT, @p_district district , SUM(a.GROSS_SALES) AS Gross_sales, SUM(a.RETURN_AMOUNT) 
                                              AS return_amount, SUM(a.TRNS_COUNT) AS trns_count, SUM(a.UNIT_COUNT) AS unit_count
                       FROM         pulse_summary AS a INNER JOIN
                                              Alignment AS b ON b.district  = @p_district and a.store_cd = b.store_cd  and b.alignment_cd = @p_alignment
                       WHERE      (a.TRN_DT = @p_reportdate)
                       GROUP BY  b.store_name, a.store_cd,b.district, a.WEEK_NUM, a.QTR, a.YR, a.TRN_DT) AS x_1 LEFT OUTER JOIN
                          (SELECT    b.store_name, a.store_cd,  a.WEEK_NUM, a.QTR, a.YR, @p_reportdate AS TRN_DT, @p_district district, SUM(a.GROSS_SALES) AS Gross_sales, 
                                                   SUM(a.RETURN_AMOUNT) AS return_amount, SUM(a.TRNS_COUNT) AS trns_count, SUM(a.UNIT_COUNT) AS unit_count 
                                                   
                            FROM          pulse_summary AS a INNER JOIN
                                                   Alignment AS b ON b.district  = @p_district and b.alignment_cd = @p_alignment
                            WHERE                   (b.district IN (@p_district)) AND (a.TRN_DT = @p_reportdate) AND ( (LEFT(a.SALES_ASSOCIATE_NUM, 2) = '99') OR (a.SALES_ASSOCIATE_NUM = '000000'))
                            GROUP BY  b.store_name, a.store_cd, b.district, a.WEEK_NUM, a.QTR, a.YR, a.TRN_DT) AS x2 ON x2.district = x_1.district  and x2.district = x_1.district  and x2.store_cd= x_1.store_cd LEFT OUTER JOIN
                          (SELECT     a.store_cd, a.WEEK_NUM, a.QTR, a.YR, @p_reportdate AS TRN_DT, @p_district district, SUM(a.GROSS_SALES) AS Gross_sales, 
                                                   SUM(a.RETURN_AMOUNT) AS return_amount, SUM(a.TRNS_COUNT) AS trns_count, SUM(a.UNIT_COUNT) AS unit_count
                                                   
                            FROM         pulse_summary AS a INNER JOIN
                                                   Alignment AS b ON b.district = @p_district  and b.alignment_cd = @p_alignment
                            WHERE       (a.TRN_DT = @p_reportdate) AND (a.SALES_ASSOCIATE_NUM = 'UNASSIGNED')
                            GROUP BY a.store_cd,  b.district, a.WEEK_NUM, a.QTR, a.YR, a.TRN_DT) AS x3 ON x3.district = x_1.district and x3.district = x_1.district and x3.store_cd = x_1.store_cd
order by x_1.store_cd asc