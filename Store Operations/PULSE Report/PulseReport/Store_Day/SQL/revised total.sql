declare @p_reportdate as datetime
declare @p_alignment as int
declare @p_curr_store  as int
declare @p_district as int
declare @p_region as varchar(4)
set @p_alignment =1
set @p_reportdate = '10-APR-07'
set @p_region = '20'
set @p_district =201
set @p_curr_store = 5002
SELECT     a.WEEK_NUM, a.QTR, a.YR, a.TRN_DT, a.STORE_CD, 
                      (a.GROSS_SALES) AS Gross_sales, (a.RETURN_AMOUNT) AS return_amount, (a.TRNS_COUNT) AS trns_count, (a.UNIT_COUNT) 
                      AS unit_count, '0' AS sales_associate_num, b.STORE_NAME, NULL AS emp_name, '999999' AS weekend
FROM         pulse_summary  AS a INNER JOIN
                      Alignment AS b ON a.STORE_CD = b.STORE_CD and b.alignment_cd = 1
WHERE     (a.STORE_CD IN (@p_curr_store)) AND (a.TRN_DT = @p_reportdate)
--GROUP BY a.STORE_CD, a.WEEK_NUM, a.QTR, a.YR, a.TRN_DT, b.STORE_NAME