using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace PulseRSUtil
{
    class Chain_Day : Chain
    {
        private string rptFileName = "ChainW.xls";

        public Chain_Day(string RSURL, string rsPath, string devInfo, string rptType, string rptLocPath, string rptPriorWkEndPath, string rptArcvPath, string rptFmt, HTError.IHTError errHandler, int logLvl)
            : base(RSURL, rsPath, devInfo, rptType, rptLocPath, rptPriorWkEndPath, rptArcvPath, rptFmt, errHandler, logLvl)
        {

        }

        public override void SaveReportInExcel()
        {
            // Write the contents of the report to a file.
            try
            {
                ArchiveReports();

                DirectoryInfo diDestination = new DirectoryInfo(reportFullFilePath);

                //Check if the target directory exists, if not, create it.
                if (!Directory.Exists(diDestination.FullName))
                {
                    Directory.CreateDirectory(diDestination.FullName);
                }

                FileInfo fi = new FileInfo(reportFullFilePath + rptFileName);
                if (fi.Exists) fi.Delete();
                fi = null;

                FileStream stream = File.Create(reportFullFilePath + rptFileName, result.Length);
                errorHandler.LogDebug(rptFileName + " created...");
                stream.Write(result, 0, result.Length);
                errorHandler.LogDebug("Results written to the file.");
                stream.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override void ArchiveReports()
        {
            string dateTime = Regex.Replace(DateTime.Now.ToShortDateString(), "/", ".");
            string rptNewFileName = "ChainW-" + dateTime + ".xls";
            string rptPriorWeekEndFileName = "ChainW._Archive.xls";
            FileInfo finfo; 
            bool successArchive = false;
            bool successPrior = false;

            try
            {
                finfo = new FileInfo(reportFullFilePath + rptFileName);

                if (finfo.Exists)
                {
                    DirectoryInfo diDestination = new DirectoryInfo(reportArchivePath + "\\" + reportFileType);

                    //Check if the target directory exists, if not, create it.
                    if (!Directory.Exists(diDestination.FullName))
                    {
                        Directory.CreateDirectory(diDestination.FullName);
                    }

                    finfo = new FileInfo(reportFullArchivePath + rptNewFileName);

                    if (finfo.Exists) finfo.Delete();
                    finfo = null;

                    if(DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                    {
                        DirectoryInfo diInfo = new DirectoryInfo(reportFullPriorWeekEndPath);

                        //Check if the target directory exists, if not, create it.
                        if (!Directory.Exists(diInfo.FullName))
                        {
                            Directory.CreateDirectory(diInfo.FullName);
                        }

                        finfo = new FileInfo(reportFullPriorWeekEndPath + rptPriorWeekEndFileName);

                        if (finfo.Exists) finfo.Delete();
                        finfo = null;

                        File.Copy(reportFullFilePath + rptFileName, reportFullPriorWeekEndPath + rptFileName);
                        successPrior = RenameFile(reportFullPriorWeekEndPath, rptFileName, rptPriorWeekEndFileName);
                    }

                    //Move to archive folder
                    File.Move(reportFullFilePath + rptFileName, reportFullArchivePath + rptFileName);
                    
                    successArchive = RenameFile(reportFullArchivePath, rptFileName, rptNewFileName);

                    errorHandler.LogDebug(rptFileName + " was renamed to " + rptNewFileName + " and archived.");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString() + " FILE: " + reportFullFilePath + rptFileName);
            }
        }
    }
}
