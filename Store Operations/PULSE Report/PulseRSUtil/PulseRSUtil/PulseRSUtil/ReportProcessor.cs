using System;
using System.IO;
using PulseRSUtil.casql05dev;
using System.Web.Services.Protocols;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Dts.Runtime;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Smo.Agent;
using Microsoft.SqlServer.Management.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using SSIS = Microsoft.SqlServer.Dts.Runtime;


namespace PulseRSUtil
{
    class PulseReportProcessor
    {
        private static string _rptServer = "";
        private static string _sqlServerConnString = "";
        private static string _currStoreQuery = "";
        private static string _priorStoreQuery = "";
        private static string _currDistrictQuery = "";
        private static string _priorDistrictQuery = "";
        private static string _currRegionQuery = "";
        private static string _priorRegionQuery = "";
        private static string _currFiscalDayMonth = "";
        private static string _pkgPath = "";
        private static string _rptPkgConfigPath = "";
        private static int _pkgSSISExecution = 0;
        private static string _pkgCalSqnName = "";
        private static string _rsUrl = "";
        private static int _rptExecution = 0;
        private static string _rptPath = "";
        
        private static string _rptFormat = "";
        private static string _rptLocalPath = "";
        private static string _rptPriorWeekEndPath = "";
        private static string _rptArchivePath = "";
        private static string _rptDate = "";
        private static string _autoRunRptTypes = "";
        private static string _autoRunRptIDs = "";
        private static int _rptAlignment = 0;
        private static int _rptDivision = 0;
        private static int _logLevel = 0;
        private static string _productionLog = "";
        private static string _debugLog = "";
        private static string _emailServer = "";
        private static string _emailSender = "";
        private static string _emailRecipients = "";
        private static string _deviceInfo = "";

        private static SqlConnection _sqlConnection = null;

        private static HTError.IHTError errorHandler;

        /// <summary>
        /// Entry point for the when running the Pulse Reporting Service Untility
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //Get values initialized from configuration file
            InitializeApp();

            errorHandler = new HTError.ErrorHandler(_productionLog, _debugLog, _emailServer, _emailSender, _emailRecipients, (HTError.LogLevel)_logLevel);

            errorHandler.AlsoLogToConsole = true;
            errorHandler.ErrorEmailPriority = System.Net.Mail.MailPriority.High;
            errorHandler.SendMailOnError = true;
            errorHandler.SendMailOnWarning = true;

            errorHandler.LogNotify("Pulse process has started.");

            try
            {
                int cnt = 0;
                int rptTypeCD = 0;
                int rptAlign = 0;
                int rptGroupValue = 0;
                int division = 0;
                DateTime rptDt  = new DateTime(); //default value of 1/1/0001

                if (args.Length > 0)
                {
                    for (int i = 0; i < args.Length; i++)
                    {
                        if (args[i].Trim().StartsWith("RptType"))
                        {
                            try
                            {
                                cnt += 1;
                                rptTypeCD = int.Parse(args[i].Trim().Substring(args[i].Trim().IndexOf("=") + 1));
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Invalid RptType parameter, valid report types are 1-12, e.g. RptType=1");
                            }
                        }
                        if (args[i].Trim().StartsWith("RptAlign"))
                        {
                            try
                            {
                                cnt += 1;
                                rptAlign = int.Parse(args[i].Trim().Substring(args[i].Trim().IndexOf("=") + 1));
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Invalid RptAlign parameter, valid report types are 'current' or 'prior', e.g. RptAlign=current");
                            }
                        }
                        if (args[i].Trim().StartsWith("RptDt"))
                        {
                            try
                            {
                                cnt += 1;
                                rptDt = DateTime.Parse(args[i].Trim().Substring(args[i].Trim().IndexOf("=") + 1));
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Invalid Report date parameter, valid syntax is RptDt=MM/DD/YYYY");
                                
                            }
                        }
                        if (args[i].Trim().StartsWith("RptGroupValue"))
                        {
                            try
                            {
                                cnt += 1;
                                rptGroupValue = int.Parse(args[i].Trim().Substring(args[i].Trim().IndexOf("=") + 1));

                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Invalid RptGroupValue parameter, valid report types store, region or district number e.g. RptGroupValue=5002");
                            }
                        }
                        if (args[i].Trim().StartsWith("Division"))
                        {
                            try
                            {
                                cnt += 1;
                                division = int.Parse(args[i].Trim().Substring(args[i].Trim().IndexOf("=") + 1));

                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Invalid Division parameter, valid Division types are 1 or 5 e.g. Division=5");
                            }
                        }
                        if (args[i].Trim().StartsWith("SSISPackageExecution"))
                        {
                            try
                            {
                                cnt += 1;
                                _pkgSSISExecution = int.Parse(args[i].Trim().Substring(args[i].Trim().IndexOf("=") + 1));

                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Invalid Division SSISPackageExecution value, valid input Disable = 0, Enable = 1");
                            }
                        }
                        if (args[i].Trim().StartsWith("ReportExecution"))
                        {
                            try
                            {
                                cnt += 1;
                                _rptExecution = int.Parse(args[i].Trim().Substring(args[i].Trim().IndexOf("=") + 1));

                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Invalid Division ReportExecution value, valid input Disable = 0, Enable = 1");
                            }
                        }
                    }
                }
                if (cnt != args.Length)
                {
                    throw new Exception("Invalid parameters passed, valid paramters are [RptType],[RptAlign],[RptDt],[RptGroupValue],[Division],[SSISPackageExecution],[ReportExecution]");
                }

                //Run Pulse_Summary SSIS Package
                if (_pkgSSISExecution == 1)
                {
                    CallSSISPackages();
                }

                //Run Report(s)
                if (_rptExecution == 1)
                {
                    //Command line run
                    if (rptTypeCD > 0)
                    {
                        RunManualReportType(rptTypeCD, rptAlign, rptDt, rptGroupValue, division);
                    }
                    //Automated run
                    else
                    {
                        if (_rptDate == "")
                            rptDt = DateTime.Now.AddDays(-1);
                        else
                        {
                            int year;
                            int month;
                            int day;

                            string[] dateArray = _rptDate.Split('/');

                            if (IsNumeric(dateArray[2]) && dateArray[2].Length == 4)
                                year = Convert.ToInt32(dateArray[2]);
                            else
                                throw new Exception("Invalide date Input. valid syntax MM/DD/YYYY");

                            if (IsNumeric(dateArray[1]) && dateArray[1].Length == 2)
                                day = Convert.ToInt32(dateArray[1]);
                            else
                                throw new Exception("Invalide date Input. valid syntax MM/DD/YYYY");

                            if (IsNumeric(dateArray[0]) && dateArray[0].Length == 2)
                                month = Convert.ToInt32(dateArray[0]);
                            else
                                throw new Exception("Invalide date Input. valid syntax MM/DD/YYYY");

                            rptDt = new DateTime(year, month, day);
                        }

                        string[] autoRunRptIDArr = _autoRunRptIDs.Split('|');
                        if (autoRunRptIDArr.Length > 0)
                        {
                            for (int i = 0; i < autoRunRptIDArr.Length; i++)
                            {
                                rptTypeCD = int.Parse(autoRunRptIDArr[i]);
                                RunAutoReportType(rptTypeCD, rptDt, _rptDivision);
                            }
                        }
                    }
                }

            errorHandler.LogNotify("Pulse process has ended.");
            }
            catch (Exception ex)
            {
                errorHandler.LogError(ex);
            }
        }

        //Manual Run
        private static void RunManualReportType(int rptTypeCD, int rptAlign, DateTime rptDt, int rptGroupValue, int division)
        {
            RSReportClass report = GetReport(rptTypeCD);

            switch (GetGroup(rptTypeCD))
            {
                case ReportGroup.Store:
                    Store store = (Store)report;
                    store.ReportDate = rptDt.ToShortDateString();
                    store.StoreNumber = rptGroupValue.ToString();
                    store.Division = division.ToString();
                    break;
                case ReportGroup.District:
                    District district = (District)report;
                    district.Alignment = rptAlign.ToString();
                    district.ReportDate = rptDt.ToShortDateString();
                    district.DistrictNumber = rptGroupValue.ToString();
                    district.Division = division.ToString();
                    break;
                case ReportGroup.Region:
                    Region region = (Region)report;
                    region.Alignment = rptAlign.ToString();
                    region.ReportDate = rptDt.ToShortDateString();
                    region.RegionNumber = rptGroupValue.ToString();
                    region.Division = division.ToString();
                    break;
                default:
                    Chain chain = (Chain)report;
                    chain.Alignment = rptAlign.ToString();
                    chain.ReportDate = rptDt.ToShortDateString();
                    chain.Division = division.ToString();
                    break;

            }

            report.Load();
            report.Execute();
            report.SaveReportInExcel();
        }

        private enum ReportGroup
        {
            Store,
            District,
            Region,
            Chain
        }

        private static ReportGroup GetGroup(int iReportType)
        {
            if (iReportType >= 1 && iReportType <= 3)
                return ReportGroup.Store;
            else if (iReportType >= 4 && iReportType <= 6)
                return ReportGroup.District;
            else if (iReportType >= 7 && iReportType <= 9)
                return ReportGroup.Region;
            else
                return ReportGroup.Chain;
        }

        //private static ArrayList GetCurrentGroupValues(int iReportType, HTError.IHTError errorHandler)
        private static ArrayList GetGroupValues(int alignment, int iReportType)
        {
            if (alignment == 1)
            {
                if (iReportType >= 1 && iReportType <= 3)
                    return GetGroupList(_currStoreQuery);
                else if (iReportType >= 4 && iReportType <= 6)
                    return GetGroupList(_currDistrictQuery);
                else if (iReportType >= 7 && iReportType <= 9)
                    return GetGroupList(_currRegionQuery);
                else return null;
            }
            else
            {
                if (iReportType >= 1 && iReportType <= 3)
                    return GetGroupList(_priorStoreQuery);
                else if (iReportType >= 4 && iReportType <= 6)
                    return GetGroupList(_priorDistrictQuery);
                else if (iReportType >= 7 && iReportType <= 9)
                    return GetGroupList(_priorRegionQuery);
                else return null;
            }
        }

        //private static RSReportClass GetReport(int rptTypeCD, HTError.IHTError errorHandler)
        private static RSReportClass GetReport(int rptTypeCD)
        {
            RSReportClass report;

            switch (rptTypeCD)
            {
                case 1:
                    report = new Store_Day(_rsUrl, _rptPath, _deviceInfo, GetAutoRunRptType(rptTypeCD), _rptLocalPath, _rptPriorWeekEndPath, _rptArchivePath, _rptFormat, errorHandler, _logLevel);
                    break;
                case 2:
                    report = new Store_Month(_rsUrl, _rptPath, _deviceInfo, GetAutoRunRptType(rptTypeCD), _rptLocalPath, _rptPriorWeekEndPath, _rptArchivePath, _rptFormat, errorHandler, _logLevel);
                    break;
                case 3:
                    report = new Store_Qtr(_rsUrl, _rptPath, _deviceInfo, GetAutoRunRptType(rptTypeCD), _rptLocalPath, _rptPriorWeekEndPath, _rptArchivePath, _rptFormat, errorHandler, _logLevel);
                    break;
                case 4:
                    report = new District_Day(_rsUrl, _rptPath, _deviceInfo, GetAutoRunRptType(rptTypeCD), _rptLocalPath, _rptPriorWeekEndPath, _rptArchivePath, _rptFormat, errorHandler, _logLevel);
                    break;
                case 5:
                    report = new District_Month(_rsUrl, _rptPath, _deviceInfo, GetAutoRunRptType(rptTypeCD), _rptLocalPath, _rptPriorWeekEndPath, _rptArchivePath, _rptFormat, errorHandler, _logLevel);
                    break;
                case 6:
                    report = new District_Qtr(_rsUrl, _rptPath, _deviceInfo, GetAutoRunRptType(rptTypeCD), _rptLocalPath, _rptPriorWeekEndPath, _rptArchivePath, _rptFormat, errorHandler, _logLevel);
                    break;
                case 7:
                    report = new Region_Day(_rsUrl, _rptPath, _deviceInfo, GetAutoRunRptType(rptTypeCD), _rptLocalPath, _rptPriorWeekEndPath, _rptArchivePath, _rptFormat, errorHandler, _logLevel);
                    break;
                case 8:
                    report = new Region_Month(_rsUrl, _rptPath, _deviceInfo, GetAutoRunRptType(rptTypeCD), _rptLocalPath, _rptPriorWeekEndPath, _rptArchivePath, _rptFormat, errorHandler, _logLevel);
                    break;
                case 9:
                    report = new Region_Qtr(_rsUrl, _rptPath, _deviceInfo, GetAutoRunRptType(rptTypeCD), _rptLocalPath, _rptPriorWeekEndPath, _rptArchivePath, _rptFormat, errorHandler, _logLevel);
                    break;
                case 10:
                    report = new Chain_Day(_rsUrl, _rptPath, _deviceInfo, GetAutoRunRptType(rptTypeCD), _rptLocalPath, _rptPriorWeekEndPath, _rptArchivePath, _rptFormat, errorHandler, _logLevel);
                    break;
                case 11:
                    report = new Chain_Month(_rsUrl, _rptPath, _deviceInfo, GetAutoRunRptType(rptTypeCD), _rptLocalPath, _rptPriorWeekEndPath, _rptArchivePath, _rptFormat, errorHandler, _logLevel);
                    break;
                case 12:
                    report = new Chain_Qtr(_rsUrl, _rptPath, _deviceInfo, GetAutoRunRptType(rptTypeCD), _rptLocalPath, _rptPriorWeekEndPath, _rptArchivePath, _rptFormat, errorHandler, _logLevel);
                    break;
                default:
                    throw new Exception("Invalide rptTypeCD: " + rptTypeCD);
            }

            return report;
        }

        private static void RunAutoReportType(int rptTypeCD, DateTime rptDt, int division)
        {
            ArrayList groupList = new ArrayList();
            ArrayList fiscalDayMonth = new ArrayList();

            try
            {
                fiscalDayMonth = GetCurrentFiscalDayMonth();

                //Use prior alignment for the first day of the fiscal month
                if (fiscalDayMonth[0].ToString() == "SUNDAY" && fiscalDayMonth[1].ToString() == "1")
                {
                    groupList = GetGroupValues(2, rptTypeCD);

                    if(groupList != null)
                        LoopThroughGroupList(groupList, rptTypeCD, 2, rptDt, division);
                    else // -1 is a dummy report group type (is not used for Chain reports)
                        RunManualReportType(rptTypeCD, 2, rptDt, -1, division);
                }
                //Use current alignment
                else
                {
                    groupList = GetGroupValues(1, rptTypeCD);

                    if(groupList != null)
                        LoopThroughGroupList(groupList, rptTypeCD, 1, rptDt, division);
                    else // -1 is a dummy report group type (is not used for Chain reports)
                        RunManualReportType(rptTypeCD, 1, rptDt, -1, division);

                }
            }
            catch (Exception ex)
            {
                errorHandler.LogError(ex);
            }
        }

        private static void LoopThroughGroupList(ArrayList groupList, int rptTypeCD, int rptAlign, DateTime rptDt, int division)
        {
            foreach (string item in groupList)
            {
                try
                {
                    int rptGroupValue = Convert.ToInt32(item);

                    RunManualReportType(rptTypeCD, rptAlign, rptDt, rptGroupValue, division);
                }
                catch (Exception ex)
                {
                    errorHandler.LogError(ex);
                }
            }
        }
        
        private static void InitializeApp()
        {
            System.Configuration.AppSettingsReader settings = new System.Configuration.AppSettingsReader();

            _rptServer = (String)settings.GetValue("ReportServerName", _rptServer.GetType());
            _sqlServerConnString = (String)settings.GetValue("SQLServerConnString", _sqlServerConnString.GetType());
            _currStoreQuery = (String)settings.GetValue("CurrentStoreQuery", _currStoreQuery.GetType());
            _priorStoreQuery = (String)settings.GetValue("PriorStoreQuery", _priorStoreQuery.GetType());
            _currDistrictQuery = (String)settings.GetValue("CurrentDistrictQuery", _currDistrictQuery.GetType());
            _priorDistrictQuery = (String)settings.GetValue("PriorDistrictQuery", _priorDistrictQuery.GetType());
            _currRegionQuery = (String)settings.GetValue("CurrentRegionQuery", _currRegionQuery.GetType());
            _priorRegionQuery = (String)settings.GetValue("PriorRegionQuery", _priorRegionQuery.GetType());
            _currFiscalDayMonth = (String)settings.GetValue("CurrentFiscalDayMonth", _currFiscalDayMonth.GetType());
            _pkgPath = (String)settings.GetValue("ReportPackagePath", _pkgPath.GetType());
            _rptPkgConfigPath = (String)settings.GetValue("ReportPackageConfigPath", _rptPkgConfigPath.GetType());
            _pkgSSISExecution = (int)settings.GetValue("SSISPackageExecution", _rptExecution.GetType());
            _pkgCalSqnName = (String)settings.GetValue("CalendarSequenceName", _pkgCalSqnName.GetType());
            _rsUrl = (String)settings.GetValue("ReportServiceURL", _rsUrl.GetType());
            _rptExecution = (int)settings.GetValue("ReportExecution", _rptExecution.GetType());
            _rptPath = (String)settings.GetValue("ReportPath", _rptPath.GetType());
            _rptFormat = (String)settings.GetValue("RptFormat", _rptFormat.GetType());

            _rptLocalPath = (String)settings.GetValue("RptLocalFilePath", _rptLocalPath.GetType());
            _rptLocalPath = _rptLocalPath.TrimEnd('/');
            if (!_rptLocalPath.EndsWith("\\")) _rptLocalPath += "\\";

            _rptPriorWeekEndPath = (String)settings.GetValue("RptPriorWeekEndPath", _rptPriorWeekEndPath.GetType());
            _rptPriorWeekEndPath = _rptPriorWeekEndPath.TrimEnd('/');
            if (!_rptPriorWeekEndPath.EndsWith("\\")) _rptPriorWeekEndPath += "\\";

            _rptArchivePath = (String)settings.GetValue("RptArchiveFilePath", _rptArchivePath.GetType());
            _rptArchivePath = _rptArchivePath.TrimEnd('/');
            if (!_rptArchivePath.EndsWith("\\")) _rptArchivePath += "\\";

            _rptDate = (String)settings.GetValue("ReportDate", _rptDate.GetType());
            _autoRunRptTypes = (String)settings.GetValue("AutoRunRptTypes", _autoRunRptTypes.GetType());
            _autoRunRptIDs = (String)settings.GetValue("AutoRunRptIDs", _autoRunRptIDs.GetType());
            _rptAlignment = (int)settings.GetValue("ReportAlignment", _rptAlignment.GetType());
            _rptDivision = (int)settings.GetValue("ReportDivision", _rptDivision.GetType());

            _productionLog = (String)settings.GetValue("ProductionLog", _productionLog.GetType());
            _debugLog = (String)settings.GetValue("DebugLog", _debugLog.GetType());
            _logLevel = (int)settings.GetValue("LogLevel", _logLevel.GetType());

            _emailServer = (String)settings.GetValue("EmailServer", _emailServer.GetType());
            _emailSender = (String)settings.GetValue("Sender", _emailSender.GetType());
            _emailRecipients = (String)settings.GetValue("Recipients", _emailRecipients.GetType());

            _deviceInfo = (String)settings.GetValue("DeviceInfo", _deviceInfo.GetType());

            //convert device info to xml string
            if (_deviceInfo != "")
            {
                _deviceInfo = _deviceInfo.Replace("&gt;", ">");
                _deviceInfo = _deviceInfo.Replace("&lt;", "<");
            }
        }

        /// <summary>
        /// Call SSIS Packages to fill in current 7 weeks into PULSE_BASE table
        /// Update employee information from Lawson
        /// And populate the PULSE_SUMMARY tables and alignments
        /// </summary>
        private static void CallSSISPackages()
        {
            try
            {
                //Microsoft.SqlServer.Dts.Runtime. REFERENCE
                Application app = new Application();

                DTSExecResult pkgResults_Sql;

                Package pkgIn;

                //Connect to Pulse Summary SSIS Package
                pkgIn = app.LoadFromSqlServer(_pkgPath, _rptServer, "", "", null);

                //Use configuration file 
                pkgIn.ImportConfigurationFile(_rptPkgConfigPath);

                //Set errorHandler
                pkgIn.Variables["ERROR_HANDLER"].Value = errorHandler;

                errorHandler.LogDebug("Running SSIS Package");

                //Execute SSIS Package
                if (_pkgSSISExecution == 1)
                {
                    pkgResults_Sql = pkgIn.Execute();
                    errorHandler.LogDebug("Package Execution Status: " + pkgResults_Sql.ToString());

                    if (pkgResults_Sql == DTSExecResult.Failure || pkgResults_Sql == DTSExecResult.Canceled)
                    {
                        throw new Exception("Package Execution Status: " + pkgResults_Sql.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns report title description from the configuration file
        /// </summary>
        /// <param name="rptTypeCd"></param>
        /// <returns></returns>
        private static string GetAutoRunRptType(int rptTypeCd)
        {
            string rptTitle = "";
            string[] _rptTitleArray = _autoRunRptTypes.Split('|');
            for (int i = 0; i < _rptTitleArray.Length; i++)
            {
                if (_rptTitleArray[i].Trim().StartsWith(rptTypeCd + "#"))
                {
                    rptTitle = _rptTitleArray[i].Trim().Substring(_rptTitleArray[i].Trim().IndexOf("#") + 1);
                    break;
                }
            }
            return rptTitle;
        }

        private static bool IsNumeric(string s)
        {
            try
            {
                Int32.Parse(s);
            }
            catch
            {
                return false;
            }
            return true;
        }

        private static ArrayList GetGroupList(string groupTypeQuery)
        {
            ArrayList groupList = new ArrayList();
            SqlDataReader sqlReader = null;
            try
            {
                SQLServerConnection();

                SqlCommand sqlCommand = new SqlCommand(groupTypeQuery, _sqlConnection);
                sqlReader = sqlCommand.ExecuteReader();
                while (sqlReader.Read())
                {
                    if (!(sqlReader.IsDBNull(0)))
                    {
                        //Gets the first Column in the resulting query
                        errorHandler.LogDebug(sqlReader.GetString(0));
                        groupList.Add(sqlReader.GetString(0));
                    }
                }

                SQLServerDisconnection();

                return groupList;
            }
            catch (Exception ex)
            {
                errorHandler.LogError(ex);
                return null;
            }
        }

        private static ArrayList GetCurrentFiscalDayMonth()
        {
            ArrayList fiscalDayMonth = new ArrayList();
            SqlDataReader sqlReader = null;
            try
            {
                SQLServerConnection();

                SqlCommand sqlCommand = new SqlCommand(_currFiscalDayMonth, _sqlConnection);
                sqlReader = sqlCommand.ExecuteReader();
                while (sqlReader.Read())
                {
                    if (!(sqlReader.IsDBNull(0)))
                    {
                        //Gets the first Column in the resulting query
                        Console.WriteLine(sqlReader.GetString(0));
                        Console.WriteLine(sqlReader.GetValue(1));
                        fiscalDayMonth.Add(sqlReader.GetString(0));
                        fiscalDayMonth.Add(sqlReader.GetValue(1) + "");
                    }
                }

                SQLServerDisconnection();

                return fiscalDayMonth;
            }
            catch (Exception ex)
            {
                errorHandler.LogError(ex);
                return null;
            }
        } 

        private static void SQLServerConnection()
        {
            _sqlConnection = new SqlConnection(_sqlServerConnString);

            try
            {
                _sqlConnection.Open();
            }
            catch(Exception ex)
            {
                errorHandler.LogError(ex);
            }
        }

        private static void SQLServerDisconnection()
        {
            if (_sqlConnection != null)
            {
                _sqlConnection.Close();
                _sqlConnection = null;
            }
        }
    }
}
