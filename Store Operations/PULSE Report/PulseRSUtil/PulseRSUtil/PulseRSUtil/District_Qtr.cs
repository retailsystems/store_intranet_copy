using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace PulseRSUtil
{
    class District_Qtr : District
    {
        private string ReportFileName
        {
            get { return "D" + DistrictNumber + "Q.xls"; }
        }

        public District_Qtr(string RSURL, string rsPath, string devInfo, string rptType, string rptLocPath, string rptPriorWkEndPath, string rptArcvPath, string rptFmt, HTError.IHTError errHandler, int logLvl)
            : base(RSURL, rsPath, devInfo, rptType, rptLocPath, rptPriorWkEndPath, rptArcvPath, rptFmt, errHandler, logLvl)
        {
        }

        public override void SaveReportInExcel()
        {
            // Write the contents of the report to a file.
            try
            {
                ArchiveReports();

                DirectoryInfo diDestination = new DirectoryInfo(reportFullFilePath);

                //Check if the target directory exists, if not, create it.
                if (!Directory.Exists(diDestination.FullName))
                {
                    Directory.CreateDirectory(diDestination.FullName);
                }

                FileInfo fi = new FileInfo(reportFullFilePath + ReportFileName);
                if (fi.Exists) fi.Delete();
                fi = null;

                FileStream stream = File.Create(reportFullFilePath + ReportFileName, result.Length);
                errorHandler.LogDebug(ReportFileName + " created...");
                stream.Write(result, 0, result.Length);
                errorHandler.LogDebug("Results written to the file.");
                stream.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override void ArchiveReports()
        {
            string dateTime = Regex.Replace(DateTime.Now.ToShortDateString(), "/", ".");
            string rptNewFileName = "D" + DistrictNumber + "Q-" + dateTime + ".xls";
            FileInfo finfo; 
            bool success = false;

            try
            {
                finfo = new FileInfo(reportFullFilePath + ReportFileName);
                if (finfo.Exists)
                {
                    success = RenameFile(reportFullFilePath, ReportFileName, rptNewFileName);

                    DirectoryInfo diDestination = new DirectoryInfo(reportArchivePath + "\\" + reportFileType);

                    // Check if the target directory exists, if not, create it.
                    if (!Directory.Exists(diDestination.FullName))
                    {
                        Directory.CreateDirectory(diDestination.FullName);
                    }

                    finfo = new FileInfo(reportFullArchivePath + rptNewFileName);

                    if (finfo.Exists) finfo.Delete();
                    finfo = null;

                    File.Move(reportFullFilePath + rptNewFileName, reportFullArchivePath + rptNewFileName);

                    errorHandler.LogDebug(ReportFileName + " was renamed to " + rptNewFileName + " and archived.");
                }

            }
            catch (Exception ex)
            {
                errorHandler.LogError(ex);
            }
        }
    }
}
