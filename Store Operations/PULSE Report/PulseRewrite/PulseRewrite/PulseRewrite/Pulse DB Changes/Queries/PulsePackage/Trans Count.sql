﻿UPDATE Pulse_Sales_Summary
SET	GROSS_SALES_TRNS_COUNT = SUBTBL.GROSS_SALES_TRNS_COUNT,
	RETURN_TRNS_COUNT = SUBTBL.RETURN_TRNS_COUNT
FROM PULSE_SALES_SUMMARY P
INNER JOIN	
(
	SELECT 	A.WEEK_NUM,
			A.TRN_DT,
			A.STORE_CD,
			A.SALES_ASSOCIATE_NUM,	
			SUM(A.RET_TRNS) AS RETURN_TRNS_COUNT,
			SUM(A.SALE_TRNS) AS GROSS_SALES_TRNS_COUNT
	FROM
	(
		SELECT	WEEK_NUM,
				TRN_DT,
				STORE_CD,
				LN_TP,
				TRN_TP,
				TERM_NUM,
				TRN_NUM,
				VOID,
				CASE
					WHEN VOID = 'P' AND LN_TP = 'RET' AND TRN_TP = 'VPT' THEN 0
					WHEN LN_TP = 'RET' AND TRN_TP = 'SAL' THEN 0
					WHEN LN_TP = 'RET' THEN 1
					ELSE 0
				END AS RET_TRNS,
				CASE
					WHEN VOID = 'P' AND LN_TP = 'SAL' AND TRN_TP = 'VPT' THEN -1
					WHEN LN_TP = 'SAL' THEN 1
					ELSE 0
				END AS SALE_TRNS,
			SALES_ASSOCIATE_NUM
		FROM PULSE_BASE
		GROUP BY WEEK_NUM, TRN_DT, STORE_CD, TRN_TP, LN_TP, TERM_NUM, TRN_NUM, SALES_ASSOCIATE_NUM, VOID
	) A
	GROUP BY A.WEEK_NUM, A.TRN_DT, A.STORE_CD, A.SALES_ASSOCIATE_NUM
)SUBTBL
ON P.WEEK_NUM = SUBTBL.WEEK_NUM
AND P.TRN_DT = SUBTBL.TRN_DT
AND P.STORE_CD = SUBTBL.STORE_CD
AND P.SALES_ASSOCIATE_NUM = SUBTBL.SALES_ASSOCIATE_NUM
AND (ISNULL(P.GROSS_SALES_TRNS_COUNT,0) <> ISNULL(SUBTBL.GROSS_SALES_TRNS_COUNT, 0) OR ISNULL(P.RETURN_TRNS_COUNT, 0) <> ISNULL(SUBTBL.RETURN_TRNS_COUNT, 0))