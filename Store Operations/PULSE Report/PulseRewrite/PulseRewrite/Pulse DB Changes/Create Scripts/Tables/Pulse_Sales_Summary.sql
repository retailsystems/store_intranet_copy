﻿ 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Pulse_Sales_Summary') AND type in (N'U'))
BEGIN
	DROP TABLE dbo.Pulse_Sales_Summary
END
GO

CREATE TABLE [dbo].[Pulse_Sales_Summary](
	[WEEK_NUM] [numeric](2, 0) NOT NULL,
	[TRN_DT] [datetime] NOT NULL,
	[STORE_CD] [varchar](4) NOT NULL,
	[GROSS_SALES] [numeric](10, 2) NULL,
	[RETURN_AMOUNT] [numeric](10, 2) NULL,
	[GROSS_SALES_TRNS_COUNT] INT NULL,
	[GROSS_SALES_UNIT_COUNT] INT NULL,
	[RETURN_TRNS_COUNT] INT NULL,
	[RETURN_UNIT_COUNT] INT NULL,
	[SALES_ASSOCIATE_NUM] [varchar](15) NULL,
	[SALES_TYPE] INT NOT NULL	
) ON [PRIMARY]