
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'USP_LoadHours')
	BEGIN
		DROP Procedure dbo.USP_LoadHours
	END
GO

-- =============================================
-- Author:		Christine P. Dao
-- Create date: 07/16/2013
-- Description:	Loads Hours for report usage
-- =============================================
CREATE PROCEDURE [dbo].[USP_LoadHours]

AS
BEGIN

	--Delete Records
	delete from dbo.Report_Hours
	where [date] in (select distinct CONVERT(DATETIME, b.ClockDate, 101) as date from dbo.Labor_Base b);
	 

	--Insert Records
	insert into dbo.Report_Hours
	select	CONVERT(DATETIME, T.Clock_Date, 101) as Date,
			right('0000'+ rtrim(T.Store_Num), 4) as Store,
			T.Employee_No,
			/*LEFT(T.In_Time, 2) as In_Hour,
			LEFT(T.Out_Time, 2) as Out_Hour,
			SUBSTRING(T.In_Time, 3, 2) as In_Min,
			SUBSTRING(T.Out_Time, 3, 2) as Out_Min,
			LEFT(T.In_Time, 2) * 60 as In_Hour_To_Min,
			LEFT(T.Out_Time, 2) * 60 as Out_Hour_To_Min,
			(LEFT(T.In_Time, 2) * 60) + SUBSTRING(T.In_Time, 3, 2) as Total_In_Min,
			(LEFT(T.Out_Time, 2) * 60) + SUBSTRING(T.Out_Time, 3, 2) as Total_Out_Min,*/
			SUM(CAST(((LEFT(T.Out_Time, 2) * 60) + SUBSTRING(T.Out_Time, 3, 2)) - ((LEFT(T.In_Time, 2) * 60) + SUBSTRING(T.In_Time, 3, 2)) as FLOAT)/CAST(60 as FLOAT)) as Total_Hours
	from
	(	
		select  
		A.Store_Num AS  Store_Num, 
		A.ClockDate as Clock_Date,
		A.Employee_No AS Employee_No, 
		A.ClockTime as In_Time,
		B.ClockTime as Out_Time
		from 
		(select * from dbo.Labor_Base
		where ClockState = 1 
		) A  
		JOIN  
		(select * from dbo.Labor_Base 
		where ClockState  = -1 ) B 
		on  A.Employee_No = B.Employee_No and 
		A.Store_Num  = B.Store_Num  and
		A.ClockDate = B.ClockDate  and 
		A.ClockTime< B.ClockTime and 
		A.ID = (B.ID -1 ) 
	)T
	group by T.Clock_Date, T.Store_Num, T.Employee_No;

END