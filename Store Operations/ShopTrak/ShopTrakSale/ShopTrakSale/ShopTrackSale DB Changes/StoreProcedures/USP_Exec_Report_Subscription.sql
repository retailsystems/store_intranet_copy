USE [ReportServer]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'USP_Exec_Report_Subscription')
	BEGIN
		DROP Procedure dbo.USP_Exec_Report_Subscription
	END
GO

-- =============================================
-- Author:		Danny Lau
-- Create date: March 15 2007
-- Description:	Executes stored procedures that active report subscriptions
-- =============================================
create PROCEDURE [dbo].[USP_Exec_Report_Subscription]
(
	@P_Schedule_Name AS varchar(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE
	@Schedule_cd VARCHAR(100), 
	@SQL Varchar(200), 
	@time datetime,
	@time60 datetime,
	@time_max datetime,
	@status1  varchar(4), 
	@status2  varchar(4),
	@MaxTime  int,
	@Report_End_Date  datetime


select @MaxTime= 60
--select @Report_End_Date=string_def from t_report_strings where name = 'Channel_Report_End_Date'

DECLARE
	mycursor CURSOR for
	Select ScheduleID from reportServer.dbo.Schedule where Name = @P_Schedule_Name  --returns the schedule IDs in table format

OPEN mycursor
	Fetch next from mycursor into @Schedule_cd

WHILE @@FETCH_STATUS = 0
Begin
	Select @SQL = '
		Use [ReportServer]
		
		Exec ReportServer.dbo.AddEvent @EventType=''SharedSchedule'', @EventData='''+@Schedule_cd+ '''' 
	--executes one of the subscription schedules
	Exec (@SQL)

	select @time = getdate()
	select @time60 = dateadd(ss,60,getdate())
	
	while @time <@time60		--This loop adds 60 seconds after the above subscription has been run to avoid satisfying below loop prematurely
	begin
		select @time = getdate()
	end


	select @time_max = dateadd(mi,@MaxTime,getdate())
	select @time = getdate()
	select @status1 = 'NC'
	select @status2 = 'NC'


	--Loops to check if all the subscriptions that use the running schedule has completed
	While  Not((@status1 = 'Done' and @status2 = 'Done') or (@time >@time_max))
	begin


		Select @status1 = Left(min(LastStatus),4)  ,@status2 =Left(max(LastStatus),4)
		From [ReportServer].[dbo].[Schedule] inner join reportServer.dbo.Subscriptions on cast([ReportServer].[dbo].[Schedule].EventData as varchar(100)) = cast(reportServer.dbo.Subscriptions.MatchData as varchar(100))
		where reportServer.dbo.Schedule.ScheduleID = @Schedule_cd

		select @time = getdate()

	end

FETCH NEXT FROM mycursor
Into  @Schedule_cd

END
Close mycursor
DeAllocate mycursor
END

GO