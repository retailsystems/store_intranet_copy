SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'USP_LoadSales')
	BEGIN
		DROP Procedure dbo.USP_LoadSales
	END
GO

-- =============================================
-- Author:		Christine P. Dao
-- Create date: 07/16/2013
-- Description:	Loads Sales for report usage
-- =============================================
CREATE PROCEDURE [dbo].[USP_LoadSales]

AS
BEGIN

	--Delete Records
	delete from dbo.Report_Sales
	where [date] in (
		select distinct d.DT
		from
		(
			select  t.DT
			from dbo.Temp_Flash_No_Div s 
			join dbo.DimTime t on LOWER(t.DayNameOfWeek) = LOWER(LTRIM(RIGHT([Day],PATINDEX('%[0-9]%',REVERSE([Day]))-1)))
			and t.WeekNumOfMerchYear = CAST(SUBSTRING([Day], PATINDEX('%[0-9]%',[Day]), LEN([Day]) - LEN(RIGHT([Day],PATINDEX('%[0-9]%',REVERSE([Day]))-1)) - PATINDEX('%[0-9]%',[Day]) + 1) AS TINYINT)
			and t.MerchYear = RIGHT(s.[Year], 4)
			
			union all 
			
			select  t.DT
			from dbo.Temp_Flash_Sales s 
			join dbo.DimTime t on LOWER(t.DayNameOfWeek) = LOWER(LTRIM(RIGHT([Day],PATINDEX('%[0-9]%',REVERSE([Day]))-1)))
			and t.WeekNumOfMerchYear = CAST(SUBSTRING([Day], PATINDEX('%[0-9]%',[Day]), LEN([Day]) - LEN(RIGHT([Day],PATINDEX('%[0-9]%',REVERSE([Day]))-1)) - PATINDEX('%[0-9]%',[Day]) + 1) AS TINYINT)
			and t.MerchYear = RIGHT(s.[Year], 4)
		)d
	);

	--Insert Records
	WITH NoDiv_CTE(DT, Day, Store, Plan_Sales, Transactions)
	as
	(
		select  t.DT,
				s.[Day],
				RIGHT(s.Store, 4) as Store,
				s.Plan_Sales,
				s.Transactions
		from dbo.Temp_Flash_No_Div s 
		join dbo.DimTime t on LOWER(t.DayNameOfWeek) = LOWER(LTRIM(RIGHT([Day],PATINDEX('%[0-9]%',REVERSE([Day]))-1)))
		and t.WeekNumOfMerchYear = CAST(SUBSTRING([Day], PATINDEX('%[0-9]%',[Day]), LEN([Day]) - LEN(RIGHT([Day],PATINDEX('%[0-9]%',REVERSE([Day]))-1)) - PATINDEX('%[0-9]%',[Day]) + 1) AS TINYINT)
		and t.MerchYear = RIGHT(s.[Year], 4)
	),
	Sales_CTE(DT, Day, Store, POS_Sales, Kiosk_Sales, Units, Comp_TY, Comp_LY)
	as
	(
		select  t.DT,
				s.[Day],
				RIGHT(s.Store, 4) as Store,
				s.POS_Sales,
				s.Kiosk_Sales,
				s.Units,
				Comp_TY,
				s.Comp_LY
		from dbo.Temp_Flash_Sales s 
		join dbo.DimTime t on LOWER(t.DayNameOfWeek) = LOWER(LTRIM(RIGHT([Day],PATINDEX('%[0-9]%',REVERSE([Day]))-1)))
		and t.WeekNumOfMerchYear = CAST(SUBSTRING([Day], PATINDEX('%[0-9]%',[Day]), LEN([Day]) - LEN(RIGHT([Day],PATINDEX('%[0-9]%',REVERSE([Day]))-1)) - PATINDEX('%[0-9]%',[Day]) + 1) AS TINYINT)
		and t.MerchYear = RIGHT(s.[Year], 4)
		
	)
	insert into dbo.Report_Sales
	select	d.dt,
			d.Store,
			s.POS_Sales as Sales,
			n.Plan_Sales,
			s.Units,
			n.Transactions,
			s.Comp_TY,
			s.Comp_LY,
			0 as Channel_Key
	from
	(
		select distinct a.DT,
			a.Store
		from
		(
			select	DT,
					Store
			from NoDiv_CTE 

			union all

			select DT,
				   Store
			from Sales_CTE 
		)
		a
	)d 
	left join NoDiv_CTE n on d.DT = n.DT and d.Store = n.Store
	left join Sales_CTE s on d.DT = s.DT and d.Store = s.Store

	union all

	select	s.DT,
			s.Store,
			s.Kiosk_Sales as Sales,
			NULL as Plan_Sales,
			NULL as Units,
			NULL as Transactions,
			NULL as Comp_TY,
			NULL as Comp_LY,
			3 as Channel_Key
	from Sales_CTE s;

END