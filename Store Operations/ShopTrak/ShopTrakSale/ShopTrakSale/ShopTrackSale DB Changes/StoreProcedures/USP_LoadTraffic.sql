USE [shoppertraksale]
GO

-- =============================================
-- Author:		Christine P. Dao
-- Create date: 07/16/2013
-- Update date: 09/04/13 - Use a Merg instead of delete then insert
-- Description:	Loads Traffic for report usage
-- =============================================

ALTER PROCEDURE [dbo].[USP_LoadTraffic]
AS
BEGIN

--Insert and update records
merge dbo.Report_Traffic as target
using (
	select CONVERT(DATETIME, [Date], 101) as Date,
		   Store,
		   ExitCount as ExitCount,
		   Indicator
	from dbo.Temp_Traffic
	where ExitCount <> 0
) as source 
on target.Date = source.Date and target.Store = source.Store
when matched and target.ExitCount <> source.ExitCount
	then update set target.ExitCount = Source.ExitCount
when not matched then
	insert (Date, Store, ExitCount, Indicator)
	values (source.Date, Source.Store, Source.ExitCount, Source.Indicator);

END
 