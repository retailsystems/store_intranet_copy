USE [shoppertraksale]
GO
/****** Object:  StoredProcedure [dbo].[USP_Load_Report_Summary]    Script Date: 04/21/2014 17:14:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sri Bajjuri
-- Create date: 07/22/2013
-- Update date: Inlude Last Months Data
-- Description:	Data for the Scorecard report usage
-- =============================================

ALTER PROCEDURE [dbo].[USP_Load_Report_Summary]
(
	@CurrDt AS datetime
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE @WeekBegDt as datetime;
DECLARE @LYWTDBegDt as datetime;
DECLARE @LYWTDEndDt as datetime;
DECLARE @LWWeekBegDt as datetime;
DECLARE @LWWeekEndDt as datetime;
DECLARE @MTDBegDt as datetime;
DECLARE @LYNWBegDt as datetime;
DECLARE @LYNWEndDt as datetime;
DECLARE @LeastRptDt as datetime;
--DECLARE @CurrDt as datetime;
DECLARE @rptSales int, @rptComp int, @rptPlan int, @rptKiosk int, @rptSalesLW int, @rptTraffic int;
DECLARE @rptTrafficLW int, @rptTrans int, @rptCnv int, @rptDps int, @rptCar int, @rptADT int, @rptUPT int;
DECLARE @dpoWTD int,@dpoLYWTD int,@dpoLW int,@dpoMTD int,@dpoLYNW int,@dpoPeerWTD int, @dpoPeerMTD int,@dpoPeerVarWTD int,@dpoPeerVarMTD int;

SELECT @rptSales = dbo.fnGetDataOrderNum ('Sales'), @rptComp = dbo.fnGetDataOrderNum ('Comp %')
	, @rptPlan = dbo.fnGetDataOrderNum ('Plan %'), @rptKiosk = dbo.fnGetDataOrderNum ('Kiosk % of Sales')
	,@rptSalesLW = dbo.fnGetDataOrderNum('Sales vs LW Sales'), @rptTraffic = dbo.fnGetDataOrderNum('Traffic TW')
	,@rptTrafficLW = dbo.fnGetDataOrderNum('Traffic % to LW') ,@rptTrans = dbo.fnGetDataOrderNum('Transactions')
	, @rptCnv = dbo.fnGetDataOrderNum('Conversion'),@rptDps = dbo.fnGetDataOrderNum( '$ Per Shopper')
	, @rptCar = dbo.fnGetDataOrderNum('CAR'),@rptADT = dbo.fnGetDataOrderNum( 'ADT')
	, @rptUPT = dbo.fnGetDataOrderNum('UPT');

SELECT @dpoWTD = dbo.fnGetDataPeriodOrderNum ('WTD'), @dpoLYWTD = dbo.fnGetDataPeriodOrderNum ('LY WTD')
	,@dpoLW = dbo.fnGetDataPeriodOrderNum ('LW'), @dpoMTD = dbo.fnGetDataPeriodOrderNum ('MTD')
	,@dpoLYNW = dbo.fnGetDataPeriodOrderNum ('LY NW'), @dpoPeerWTD = dbo.fnGetDataPeriodOrderNum ('Peer WTD')
	,@dpoPeerMTD = dbo.fnGetDataPeriodOrderNum ('Peer MTD'),@dpoPeerVarWTD = dbo.fnGetDataPeriodOrderNum ('Var To Peer WTD')
	,@dpoPeerVarMTD = dbo.fnGetDataPeriodOrderNum ('Var to Peer MTD');


--SET @CurrDt = '07/06/2013'

-- Get Week Begin Date
select @WeekBegDt = WeekBegDt, @LWWeekBegDt = WeekBegDt - 7, @LWWeekEndDt = WeekBegDt - 1
from dbo.DimTime_Restated
where DT = @CurrDt;

--LY Dates
select @LYWTDBegDt = ly.WeekBegDt,@LYNWBegDt = ly.WeekBegDt + 7, @LYNWEndDt = ly.WeekBegDt + 13,@LYWTDEndDt = ly.DT
from dbo.DimTime_Restated ly
join 
(
	select DayNumOfMerchYear, MerchYear
	from dbo.DimTime_Restated
	where DT = @CurrDt
)ty on ty.DayNumOfMerchYear = ly.DayNumOfMerchYear and (ty.MerchYear - 1) =  ly.MerchYear;

--Beginning of Month
select @MTDBegDt = DT
from dbo.DimTime_Restated t
join
(
	select MerchMonthNumOfYear, MerchYear
	from dbo.DimTime_Restated
	where DT = @CurrDt
)m on t.MerchYear = m.MerchYear and t.MerchMonthNumOfYear = m.MerchMonthNumOfYear
where t.DayNumOfMerchMonth = 1;

--Get the least of the two dates
select @LeastRptDt = min(t.LeastDate)
from
(
	select @MTDBegDt as LeastDate
	union
	select @LWWeekBegDt as LeastDate
)t;

/*
select @WeekBegDt as WeekBegDt, @LYWTDBegDt as LYWeekBegDt, @LYWTDEndDt as LYWeekEndDt, 
@LWWeekBegDt as LWWeekBegDt, @LWWeekEndDt as LWWeekEndDt, @MTDBegDt as MTDBegDt,
@LYNWBegDt as LYNWBegDt, @LYNWEndDt as LYNWEndDt;
*/
--TRUNCATE TABLE dbo.Report_Store_Summary_New;
TRUNCATE TABLE dbo.Report_Store_Summary;
TRUNCATE TABLE dbo.Report_District_Summary;
TRUNCATE TABLE dbo.Report_Region_Summary;
TRUNCATE TABLE dbo.Report_Mall_Type_Summary;

;WITH SlsCTE (TRN_DT, STORE, SALES, PLAN_SALES, TRANSACTIONS, Comp_TY, Comp_LY
, CHANNEL_KEY, DayNumOfWeek, Division, District, ExitCount,Units,Labor_Hours) AS 
(
    SELECT [DATE] AS TRN_DT, S.Store, S.Sales, S.PLAN_SALES, S.TRANSACTIONS
    , S.Comp_TY, S.Comp_LY, S.CHANNEL_KEY, t.DayNumOfWeek, a.Division,a.District
    , ExitCount, s.Units,Labor_Hours
    FROM dbo.Report_Sales s
    JOIN dbo.DimTime_Restated t ON t.Dt = s.[DATE]
    JOIN dbo.Store_Alignment a on s.Store = a.Store
    LEFT OUTER JOIN 
    (
    SELECT [DATE] AS TRN_DT, Store, SUM(ExitCount) AS ExitCount 
    FROM dbo.Report_Traffic
    WHERE [DATE] BETWEEN  @LeastRptDt AND @CurrDt
	GROUP BY [DATE], Store 
    ) B ON s.[DATE] = B.TRN_DT AND s.Store = B.Store AND s.Channel_Key = 0
    LEFT OUTER JOIN (
    SELECT [DATE] AS TRN_DT, RIGHT('0000' + Store, 4) AS STORE, ROUND(SUM(Hours),4) as Labor_Hours 
    FROM dbo.Report_Hours
    WHERE [DATE] BETWEEN  @LeastRptDt AND @CurrDt
    GROUP BY [DATE], Store 
    ) C ON s.[DATE] = C.TRN_DT AND s.Store = C.Store AND s.Channel_Key = 0
    WHERE [DATE] BETWEEN  @LeastRptDt AND @CurrDt
    --WHERE S.[Date] BETWEEN  '06/02/2013' AND '06/19/2013'
    --AND a.Division = 1 and a.District = '001'
), LYSlsCTE (TRN_DT, STORE, SALES, PLAN_SALES, TRANSACTIONS, Comp_TY, Comp_LY
	, CHANNEL_KEY, DayNumOfWeek, Division, District,ExitCount,Units) AS 
(
    SELECT [DATE] AS TRN_DT, S.Store, S.Sales, S.PLAN_SALES, S.TRANSACTIONS
    , S.Comp_TY, S.Comp_LY, S.CHANNEL_KEY, t.DayNumOfWeek, a.Division,a.District,ExitCount, s.Units
    FROM dbo.Report_Sales s
    JOIN dbo.DimTime_Restated t ON t.Dt = s.[DATE]
    JOIN dbo.Store_Alignment a on s.Store = a.Store
     LEFT OUTER JOIN 
    (
    SELECT [DATE] AS TRN_DT, Store, SUM(ExitCount) AS ExitCount 
    FROM dbo.Report_Traffic
    WHERE [DATE] BETWEEN  @LYWTDBegDt AND @LYNWEndDt
	GROUP BY [DATE], Store 
    ) B ON s.[DATE] = B.TRN_DT AND s.Store = B.Store AND s.Channel_Key = 0
    WHERE [DATE] BETWEEN  @LYWTDBegDt AND @LYNWEndDt
    --WHERE S.[Date] BETWEEN  '06/02/2013' AND '06/19/2013'
    --AND a.Division = 1 and a.District = '001'
)
--insert into dbo.Report_Store_Summary_New (DataOrder,DataPeriodOrder,Division,District,Store,Data1,Data2)
insert into dbo.Report_Store_Summary (DataOrder,DataPeriodOrder,Division,District,Store,Data1,Data2)
----------------- SALES -------------------------------
select 	@rptSales as DataOrder,	s.DayNumOfWeek as DataPeriodOrder,
		s.Division,s.District,s.Store,s.Sales as Data1,	NULL AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
UNION --wtd
select 	@rptSales as DataOrder,	@dpoWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Sales) as Data1,NULL AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --ly wtd
select 	@rptSales as DataOrder,	@dpoLYWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Sales) as Data1,NULL AS Data2
from LYSlsCTE s 
where TRN_DT between @LYWTDBegDt and @LYWTDEndDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --lw
select 	@rptSales as DataOrder,	@dpoLW as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Sales) as Data1,NULL AS Data2
from SlsCTE s 
where TRN_DT between @LWWeekBegDt and @LWWeekEndDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --mtd
select 	@rptSales as DataOrder,	@dpoMTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Sales) as Data1,NULL AS Data2
from SlsCTE s 
where TRN_DT between @MTDBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --LY NW
select 	@rptSales as DataOrder,	@dpoLYNW as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Sales) as Data1,NULL AS Data2
from LYSlsCTE s 
where TRN_DT between @LYNWBegDt and @LYNWEndDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
--------------------- COMP % ---------------------------
UNION
select 	@rptComp as DataOrder,	s.DayNumOfWeek as DataPeriodOrder,
		s.Division,s.District,s.Store,s.Comp_TY as Data1,	s.Comp_LY AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
UNION --wtd
select 	@rptComp as DataOrder,	@dpoWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Comp_TY) as Data1,sum(s.Comp_LY) AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --ly wtd
select 	@rptComp as DataOrder,	@dpoLYWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Comp_TY) as Data1,sum(s.Comp_LY) AS Data2
from LYSlsCTE s 
where TRN_DT between @LYWTDBegDt and @LYWTDEndDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --lw
select 	@rptComp as DataOrder,	@dpoLW as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Comp_TY) as Data1,sum(s.Comp_LY) AS Data2
from SlsCTE s 
where TRN_DT between @LWWeekBegDt and @LWWeekEndDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --mtd
select 	@rptComp as DataOrder,	@dpoMTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Comp_TY) as Data1,sum(s.Comp_LY) AS Data2
from SlsCTE s 
where TRN_DT between @MTDBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
--------------------- PLAN ---------------------------
UNION --wtd
select 	@rptPlan as DataOrder,	@dpoWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.SALES) as Data1,sum(s.PLAN_SALES) AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --mtd
select 	@rptPlan as DataOrder,	@dpoMTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.SALES) as Data1,sum(s.PLAN_SALES) AS Data2
from SlsCTE s 
where TRN_DT between @MTDBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
--------------------- KIOSK ---------------------------
UNION
SELECT @rptKiosk as DataOrder,	@dpoWTD as DataPeriodOrder,
	Division,District,Store, SUM(Data1) AS Data1, SUM(Data2) AS Data2 FROM
(select 	s.Division,s.District,s.Store,s.Sales as Data1,	NULL AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 3
UNION
select 	s.Division,s.District,s.Store,null as Data1,s.Sales AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
) A
GROUP BY Division,District,Store
UNION --ly wtd
SELECT @rptKiosk as DataOrder,	@dpoLYWTD as DataPeriodOrder,
	Division,District,Store, SUM(Data1) AS Data1, SUM(Data2) AS Data2 FROM
(select 	s.Division,s.District,s.Store,s.Sales as Data1,	NULL AS Data2
from LYSlsCTE s 
where TRN_DT between @LYWTDBegDt and @LYWTDEndDt and s.Channel_Key = 3
UNION
select 	s.Division,s.District,s.Store,null as Data1,s.Sales AS Data2
from LYSlsCTE s 
where TRN_DT between @LYWTDBegDt and @LYWTDEndDt and s.Channel_Key = 0
) A
GROUP BY Division,District,Store

--------------------- SALES VS LW ---------------------------
UNION
SELECT @rptSalesLW as DataOrder,DataPeriodOrder,
	Division,District,Store, SUM(Data1) AS Data1, SUM(Data2) AS Data2 FROM
(select s.DayNumOfWeek as DataPeriodOrder,s.Division,s.District,s.Store,s.Sales as Data1,NULL AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
UNION
select s.DayNumOfWeek as DataPeriodOrder,s.Division,s.District,s.Store,NULL as Data1,s.Sales AS Data2
from SlsCTE s 
where TRN_DT between @LWWeekBegDt and @LWWeekEndDt and s.Channel_Key = 0
) A
GROUP BY DataPeriodOrder,Division,District,Store
UNION -- WTD
SELECT @rptSalesLW as DataOrder,@dpoWTD as DataPeriodOrder,
	Division,District,Store, SUM(Data1) AS Data1, SUM(Data2) AS Data2 FROM
(select s.Division,s.District,s.Store,s.Sales as Data1,NULL AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
UNION
select s.Division,s.District,s.Store,NULL as Data1,s.Sales AS Data2
from SlsCTE s 
where TRN_DT between @LWWeekBegDt and @LWWeekEndDt and s.Channel_Key = 0
) A
GROUP BY Division,District,Store
--------------------- TRAFFIC---------------------------
UNION
select 	@rptTraffic as DataOrder,	s.DayNumOfWeek as DataPeriodOrder,
		s.Division,s.District,s.Store,s.ExitCount as Data1,	NULL AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
UNION --wtd
select 	@rptTraffic as DataOrder,	@dpoWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.ExitCount) as Data1,NULL AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --ly wtd
select 	@rptTraffic as DataOrder,	@dpoLYWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.ExitCount) as Data1,NULL AS Data2
from LYSlsCTE s 
where TRN_DT between @LYWTDBegDt and @LYWTDEndDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --lw
select 	@rptTraffic as DataOrder,	@dpoLW as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.ExitCount) as Data1,NULL AS Data2
from SlsCTE s 
where TRN_DT between @LWWeekBegDt and @LWWeekEndDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
--------------------- TRAFFIC LW---------------------------
UNION
SELECT @rptTrafficLW as DataOrder,DataPeriodOrder,
	Division,District,Store, SUM(Data1) AS Data1, SUM(Data2) AS Data2 FROM
(select s.DayNumOfWeek as DataPeriodOrder,s.Division,s.District,s.Store,s.ExitCount as Data1,NULL AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
UNION
select s.DayNumOfWeek as DataPeriodOrder,s.Division,s.District,s.Store,NULL as Data1,s.ExitCount AS Data2
from SlsCTE s 
where TRN_DT between @LWWeekBegDt and @LWWeekEndDt and s.Channel_Key = 0
) A
GROUP BY DataPeriodOrder,Division,District,Store
UNION -- WTD
SELECT @rptTrafficLW as DataOrder,@dpoWTD as DataPeriodOrder,
	Division,District,Store, SUM(Data1) AS Data1, SUM(Data2) AS Data2 FROM
(select s.Division,s.District,s.Store,s.ExitCount as Data1,NULL AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
UNION
select s.Division,s.District,s.Store,NULL as Data1,s.ExitCount AS Data2
from SlsCTE s 
where TRN_DT between @LWWeekBegDt and @LWWeekEndDt and s.Channel_Key = 0
) A
GROUP BY Division,District,Store
--------------------- TRANSACTIONS---------------------------
UNION
select 	@rptTrans as DataOrder,	s.DayNumOfWeek as DataPeriodOrder,
		s.Division,s.District,s.Store,s.TRANSACTIONS as Data1,	NULL AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
UNION --wtd
select 	@rptTrans as DataOrder,	@dpoWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.TRANSACTIONS) as Data1,NULL AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --mtd
select 	@rptTrans as DataOrder,	@dpoMTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.TRANSACTIONS) as Data1,NULL AS Data2
from SlsCTE s 
where TRN_DT between @MTDBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
--------------------- CONVERSIONS---------------------------
UNION
select 	@rptCnv as DataOrder,	s.DayNumOfWeek as DataPeriodOrder,
		s.Division,s.District,s.Store,s.TRANSACTIONS as Data1,s.ExitCount AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
and ISNULL(s.ExitCount,0) > 0
UNION --wtd
select 	@rptCnv as DataOrder,	@dpoWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.TRANSACTIONS) as Data1,SUM(s.ExitCount) AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
and ISNULL(s.ExitCount,0) > 0
GROUP BY s.Division,s.District,s.Store
UNION --lw
select 	@rptCnv as DataOrder,	@dpoLW as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.TRANSACTIONS) as Data1,SUM(s.ExitCount) AS Data2
from SlsCTE s 
where TRN_DT between @LWWeekBegDt and @LWWeekEndDt and s.Channel_Key = 0
and ISNULL(s.ExitCount,0) > 0
GROUP BY s.Division,s.District,s.Store
UNION --mtd
select 	@rptCnv as DataOrder,	@dpoMTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.TRANSACTIONS) as Data1,SUM(s.ExitCount) AS Data2
from SlsCTE s 
where TRN_DT between @MTDBegDt and @CurrDt and s.Channel_Key = 0
and ISNULL(s.ExitCount,0) > 0
GROUP BY s.Division,s.District,s.Store
--------------------- $ Per Shopper (DPS)---------------------------
UNION --wtd
select 	@rptDps as DataOrder,	@dpoWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.SALES) as Data1,SUM(s.ExitCount) AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
--------------------- Customer to Associate Ratio (CAR) ---------------------------
UNION --wtd
select 	@rptCar as DataOrder,	@dpoWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.ExitCount) as Data1,sum(s.Labor_Hours) AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
--------------------- ADT ---------------------------
UNION
select 	@rptADT as DataOrder,	s.DayNumOfWeek as DataPeriodOrder,
		s.Division,s.District,s.Store,s.Sales as Data1,	s.TRANSACTIONS AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
UNION --wtd
select 	@rptADT as DataOrder,	@dpoWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Sales) as Data1,sum(s.TRANSACTIONS) AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --ly wtd
select 	@rptADT as DataOrder,	@dpoLYWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Sales) as Data1,sum(s.TRANSACTIONS) AS Data2
from LYSlsCTE s 
where TRN_DT between @LYWTDBegDt and @LYWTDEndDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --lw
select 	@rptADT as DataOrder,	@dpoLW as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Sales) as Data1,sum(s.TRANSACTIONS) AS Data2
from SlsCTE s 
where TRN_DT between @LWWeekBegDt and @LWWeekEndDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --mtd
select 	@rptADT as DataOrder,	@dpoMTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Sales) as Data1,sum(s.TRANSACTIONS) AS Data2
from SlsCTE s 
where TRN_DT between @MTDBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
--------------------- UPT ---------------------------
UNION
select 	@rptUPT as DataOrder,	s.DayNumOfWeek as DataPeriodOrder,
		s.Division,s.District,s.Store,s.Units as Data1,	s.TRANSACTIONS AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
UNION --wtd
select 	@rptUPT as DataOrder,	@dpoWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Units) as Data1,sum(s.TRANSACTIONS) AS Data2
from SlsCTE s 
where TRN_DT between @WeekBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --ly wtd
select 	@rptUPT as DataOrder,	@dpoLYWTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Units) as Data1,sum(s.TRANSACTIONS) AS Data2
from LYSlsCTE s 
where TRN_DT between @LYWTDBegDt and @LYWTDEndDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --lw
select 	@rptUPT as DataOrder,	@dpoLW as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Units) as Data1,sum(s.TRANSACTIONS) AS Data2
from SlsCTE s 
where TRN_DT between @LWWeekBegDt and @LWWeekEndDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
UNION --mtd
select 	@rptUPT as DataOrder,	@dpoMTD as DataPeriodOrder,
		s.Division,s.District,s.Store,sum(s.Units) as Data1,sum(s.TRANSACTIONS) AS Data2
from SlsCTE s 
where TRN_DT between @MTDBegDt and @CurrDt and s.Channel_Key = 0
GROUP BY s.Division,s.District,s.Store
;
------------------------- Load Peer Data  ----------------------------------
--WTD
INSERT INTO dbo.Report_Store_Summary
(DataOrder,DataPeriodOrder,Division,District,Store,Data1,Data2)
select F.DataOrder, @dpoPeerWTD as DataPeriodOrder,F.Division,F.District, F.Store
,ROUND(SUM(F1.Data1),2) AS Data1, ROUND(SUM(F1.Data2),2) as Data2
FROM dbo.Report_Store_Summary F
JOIN dbo.Store_Category S ON F.Store = S.Store
JOIN dbo.Store_Category S1 ON S.Peer_Group = S1.Peer_Group
JOIN dbo.Report_Store_Summary F1 ON F1.store = S1.Store AND F.Division = F1.Division
	AND F.DataOrder = F1.DataOrder AND F.DataPeriodOrder = F1.DataPeriodOrder
WHERE F.DataOrder in(@rptComp,@rptCnv,@rptADT,@rptUPT) AND F.DataPeriodOrder = @dpoWTD
GROUP BY F.DataOrder, F.DataPeriodOrder,F.Division,F.District, F.Store;

--MTD
INSERT INTO dbo.Report_Store_Summary
(DataOrder,DataPeriodOrder,Division,District,Store,Data1,Data2)
select F.DataOrder, @dpoPeerMTD as DataPeriodOrder,F.Division,F.District, F.Store
,ROUND(SUM(F1.Data1),2) AS Data1, ROUND(SUM(F1.Data2),2) as Data2
FROM dbo.Report_Store_Summary F
JOIN dbo.Store_Category S ON F.Store = S.Store
JOIN dbo.Store_Category S1 ON S.Peer_Group = S1.Peer_Group
JOIN dbo.Report_Store_Summary F1 ON F1.store = S1.Store AND F.Division = F1.Division
	AND F.DataOrder = F1.DataOrder AND F.DataPeriodOrder = F1.DataPeriodOrder
WHERE F.DataOrder in(@rptComp,@rptCnv,@rptADT,@rptUPT) AND F.DataPeriodOrder = @dpoMTD
GROUP BY F.DataOrder, F.DataPeriodOrder,F.Division,F.District, F.Store;

-- var peer wtd
INSERT INTO dbo.Report_Store_Summary
(DataOrder,DataPeriodOrder,Division,District,Store,Data1,Data2)
select F.DataOrder, @dpoPeerVarWTD as DataPeriodOrder,F.Division,F.District, F.Store
,CASE WHEN F.Data2 = 0 THEN NULL ELSE ROUND((F.Data1/F.Data2),4) END as Data1
,CASE WHEN F1.Data2 = 0 THEN NULL ELSE ROUND((F1.Data1/F1.Data2),4) END as Data2
FROM dbo.Report_Store_Summary F --wtd
--peer wtd
JOIN dbo.Report_Store_Summary F1 ON F1.store = F.Store AND F.Division = F1.Division and F.DataOrder = F1.DataOrder and F1.DataPeriodOrder = @dpoPeerWTD	
WHERE F.DataOrder in(@rptADT,@rptUPT) AND F.DataPeriodOrder = @dpoWTD;

-- var peer mtd
INSERT INTO dbo.Report_Store_Summary
(DataOrder,DataPeriodOrder,Division,District,Store,Data1,Data2)
select F.DataOrder, @dpoPeerVarMTD as DataPeriodOrder,F.Division,F.District, F.Store
,CASE WHEN F.Data2 = 0 THEN NULL ELSE ROUND((F.Data1/F.Data2),4) END as Data1
,CASE WHEN F1.Data2 = 0 THEN NULL ELSE ROUND((F1.Data1/F1.Data2),4) END as Data2
FROM dbo.Report_Store_Summary F --mtd
--peer mtd
JOIN dbo.Report_Store_Summary F1 ON F1.store = F.Store AND F.Division = F1.Division and F.DataOrder = F1.DataOrder and F1.DataPeriodOrder = @dpoPeerMTD	
WHERE F.DataOrder in(@rptADT,@rptUPT) AND F.DataPeriodOrder = @dpoMTD;

--populate district/region tables
INSERT INTO dbo.Report_District_Summary
(DataOrder,DataPeriodOrder,Division,Region,District,Data1,Data2)
select F.DataOrder, F.DataPeriodOrder,F.Division,S.Region,F.District
,ROUND(SUM(Data1),2) AS Data1, ROUND(SUM(Data2),2) as Data2
FROM dbo.Report_Store_Summary F
JOIN dbo.Store_Alignment S ON F.District = S.District and F.Store = S.Store
WHERE DataOrder <> @rptCnv
GROUP BY F.DataOrder, F.DataPeriodOrder,F.Division,S.Region,F.District
UNION
select F.DataOrder, F.DataPeriodOrder,F.Division,S.Region,F.District
,ROUND(SUM(Data1),2) AS Data1, ROUND(SUM(Data2),2) as Data2
FROM dbo.Report_Store_Summary F
JOIN dbo.Store_Alignment S ON F.District = S.District and F.Store = S.Store
WHERE DataOrder = @rptCnv AND ISNULL(Data2,0) > 0
GROUP BY F.DataOrder, F.DataPeriodOrder,F.Division,S.Region,F.District;

--populate division/region tables
INSERT INTO dbo.Report_Region_Summary
(DataOrder,DataPeriodOrder,Division,Region,Data1,Data2)
select F.DataOrder, F.DataPeriodOrder,F.Division,S.Region
,ROUND(SUM(Data1),2) AS Data1, ROUND(SUM(Data2),2) as Data2
FROM dbo.Report_District_Summary F
JOIN (select distinct Region, District from dbo.Store_Alignment) S ON F.Region = S.Region and F.District = S.District 
WHERE DataOrder <> @rptCnv
GROUP BY F.DataOrder, F.DataPeriodOrder,F.Division,S.Region
UNION
select F.DataOrder, F.DataPeriodOrder,F.Division,S.Region
,ROUND(SUM(Data1),2) AS Data1, ROUND(SUM(Data2),2) as Data2
FROM dbo.Report_District_Summary F
JOIN (select distinct Region, District from dbo.Store_Alignment) S ON F.Region = S.Region and F.District = S.District 
WHERE DataOrder = @rptCnv AND ISNULL(Data2,0) > 0
GROUP BY F.DataOrder, F.DataPeriodOrder,F.Division,S.Region
;

--populate division/mall type tables 
INSERT INTO dbo.Report_Mall_Type_Summary
select F.DataOrder, F.DataPeriodOrder,F.Division,C.Mall_Type
,ROUND(SUM(Data1),2) AS Data1, ROUND(SUM(Data2),2) as Data2
FROM dbo.Report_Store_Summary F
JOIN dbo.Store_Alignment S ON F.Store = S.Store and F.District = S.District 
JOIN dbo.Store_Category C ON F.Store = C.Store
WHERE DataOrder <> @rptCnv
GROUP BY F.DataOrder, F.DataPeriodOrder,F.Division,C.Mall_Type
UNION
select F.DataOrder, F.DataPeriodOrder,F.Division,C.Mall_Type
,ROUND(SUM(Data1),2) AS Data1, ROUND(SUM(Data2),2) as Data2
FROM dbo.Report_Store_Summary F
JOIN dbo.Store_Alignment S ON F.Store = S.Store and F.District = S.District 
JOIN dbo.Store_Category C ON F.Store = C.Store
WHERE DataOrder = @rptCnv AND ISNULL(Data2,0) > 0
GROUP BY F.DataOrder, F.DataPeriodOrder,F.Division,C.Mall_Type;
   
END

