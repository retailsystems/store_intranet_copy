use shoppertraksale
declare @reportdate smalldatetime = '8/20/2014'

-- Calculate the beginning of the week
declare @weekbegindate smalldatetime
select @weekbegindate = dt.WeekBegDt
from dbo.DimTime_Restated as dt with (nolock)
where dt.DT = @reportdate
-- Calculate the beginning of the month
declare @monthbegindate smalldatetime
select @monthbegindate = m.DT
from dbo.DimTime_Restated as m with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on m.MerchYear = dt.MerchYear
	and m.MerchMonthNumOfYear = dt.MerchMonthNumOfYear
	and m.DayNumOfMerchMonth = 1
where dt.DT = @reportdate
-- Calculate the beginning of the quarter
declare @quarterbegindate smalldatetime
select @quarterbegindate = min(q.DT)
from dbo.DimTime_Restated as q with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on q.MerchYear = dt.MerchYear
	and q.MerchQuarter = dt.MerchQuarter
where dt.DT = @reportdate
-- Calculate the beginning of the year
declare @yearbegindate smalldatetime
select @yearbegindate = y.DT
from dbo.DimTime_Restated as y with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on y.MerchYear = dt.MerchYear
	and y.DayNumOfMerchYear = 1
where dt.DT = @reportdate
-- Calculate last year's date
declare @lydate smalldatetime
select @lydate = ly.DT
from dbo.DimTime_Restated as ly with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on ly.MerchYear = dt.MerchYear - 1
	and ly.DayNumOfMerchYear = dt.DayNumOfMerchYear
where dt.DT = @reportdate
-- Calculate last year's beginning of the week
declare @lyweekbegindate smalldatetime
select @lyweekbegindate = dt.WeekBegDt
from dbo.DimTime_Restated as dt with (nolock)
where dt.DT = @lydate
-- Calculate last year's beginning of the month
declare @lymonthbegindate smalldatetime
select @lymonthbegindate = m.DT
from dbo.DimTime_Restated as m with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on m.MerchYear = dt.MerchYear
	and m.MerchMonthNumOfYear = dt.MerchMonthNumOfYear
	and m.DayNumOfMerchMonth = 1
where dt.DT = @lydate
-- Calculate last year's beginning of the quarter
declare @lyquarterbegindate smalldatetime
select @lyquarterbegindate = min(q.DT)
from dbo.DimTime_Restated as q with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on q.MerchYear = dt.MerchYear
	and q.MerchQuarter = dt.MerchQuarter
where dt.DT = @lydate
-- Calculate last year's beginning of the year
declare @lyyearbegindate smalldatetime
select @lyyearbegindate = y.DT
from dbo.DimTime_Restated as y with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on y.MerchYear = dt.MerchYear
	and y.DayNumOfMerchYear = 1
where dt.DT = @lydate;

select
@reportdate as '@reportdate',
@weekbegindate as '@weekbegindate',
@monthbegindate as '@monthbegindate',
@quarterbegindate as '@quarterbegindate',
@yearbegindate as '@yearbegindate',
@lydate as '@lydate',
@lyweekbegindate as '@lyweekbegindate',
@lymonthbegindate as '@lymonthbegindate',
@lyquarterbegindate as '@lyquarterbegindate',
@lyyearbegindate as '@lyyearbegindate'