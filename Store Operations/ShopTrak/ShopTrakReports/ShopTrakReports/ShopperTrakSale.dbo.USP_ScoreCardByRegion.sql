create procedure dbo.USP_ScoreCardByRegion
(
	@region varchar(2),
	@reportdate datetime
)
as
begin
-- Calculate the beginning of the week
declare @weekbegindate datetime
select @weekbegindate = dt.WeekBegDt
from dbo.DimTime_Restated as dt with (nolock)
where dt.DT = @reportdate
-- Calculate the beginning of the month
declare @monthbegindate datetime
select @monthbegindate = m.DT
from dbo.DimTime_Restated as m with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on m.MerchYear = dt.MerchYear
	and m.MerchMonthNumOfYear = dt.MerchMonthNumOfYear
	and m.DayNumOfMerchMonth = 1
where dt.DT = @reportdate
-- Calculate the beginning of the quarter
declare @quarterbegindate datetime
select @quarterbegindate = min(q.DT)
from dbo.DimTime_Restated as q with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on q.MerchYear = dt.MerchYear
	and q.MerchQuarter = dt.MerchQuarter
where dt.DT = @reportdate
-- Calculate the beginning of the year
declare @yearbegindate datetime
select @yearbegindate = y.DT
from dbo.DimTime_Restated as y with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on y.MerchYear = dt.MerchYear
	and y.DayNumOfMerchYear = 1
where dt.DT = @reportdate
-- Calculate last year's date
declare @lydate datetime
select @lydate = ly.DT
from dbo.DimTime_Restated as ly with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on ly.MerchYear = dt.MerchYear - 1
	and ly.DayNumOfMerchYear = dt.DayNumOfMerchYear
where dt.DT = @reportdate
-- Calculate last year's beginning of the week
declare @lyweekbegindate datetime
select @lyweekbegindate = dt.WeekBegDt
from dbo.DimTime_Restated as dt with (nolock)
where dt.DT = @lydate
-- Calculate last year's beginning of the month
declare @lymonthbegindate datetime
select @lymonthbegindate = m.DT
from dbo.DimTime_Restated as m with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on m.MerchYear = dt.MerchYear
	and m.MerchMonthNumOfYear = dt.MerchMonthNumOfYear
	and m.DayNumOfMerchMonth = 1
where dt.DT = @lydate
-- Calculate last year's beginning of the quarter
declare @lyquarterbegindate datetime
select @lyquarterbegindate = min(q.DT)
from dbo.DimTime_Restated as q with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on q.MerchYear = dt.MerchYear
	and q.MerchQuarter = dt.MerchQuarter
where dt.DT = @lydate
-- Calculate last year's beginning of the year
declare @lyyearbegindate datetime
select @lyyearbegindate = y.DT
from dbo.DimTime_Restated as y with (nolock)
inner join dbo.DimTime_Restated as dt with (nolock)
	on y.MerchYear = dt.MerchYear
	and y.DayNumOfMerchYear = 1
where dt.DT = @lydate;

-- Current Year Sales
with CurrentYearSales as
(
	-- Date Range: Day
	select
		1 as DateRange,
		Region_Description as Region,
		District_Description as District,
		a.Store + ' ' + a.Store_Name as Store,
		sum(s.Sales) as ActualSales,
		sum(isnull(s.Plan_Sales, 0)) as PlanSales,
		case sum(isnull(s.Plan_Sales, 0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Plan_Sales, 0)) end as VarToPlanPercent,
		sum(s.Sales) - sum(isnull(s.Plan_Sales, 0)) as VarToPlanDollar,
		case sum(s.Comp_LY) when 0 then 0 else sum(s.Comp_TY)/sum(s.Comp_LY) end as CompPercent,
		sum(s.Comp_TY) - sum(s.Comp_LY) as CompToLY,
		sum(s.Comp_TY) as CompTY,
		sum(s.Comp_LY) as CompLY,
		--sum(isnull(t.ExitCount,0)) as Traffic,
		--case sum(isnull(t.ExitCount,0)) when 0 then 0 else sum(isnull(s.Transactions,0))/cast(sum(isnull(t.ExitCount,0)) as float) end as Conversion,
		sum(isnull(s.Transactions,0)) as Transactions,
		--case sum(isnull(t.ExitCount,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(t.ExitCount,0)) end as DPS,
		--case sum(isnull(h.Hours,0)) when 0 then 0 else sum(isnull(t.ExitCount,0))/sum(isnull(h.Hours,0)) end as CAR,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Transactions,0)) end as ADT,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Units)/cast(sum(isnull(s.Transactions,0)) as float) end as UPT,
		sum(s.Units) as Units,
		sum(case s.Channel_Key when 3 then s.Sales else 0 end) as SaveTheSale,
		sum(isnull(s.TCC_Sales,0)) as TCCSales,
		sum(isnull(s.Loyalty_Sales,0)) as LoyaltySales
	from dbo.Report_Sales as s with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = s.Store
	inner join dbo.Region as r with (nolock)
		on r.Region_Number = a.Region
	inner join dbo.District as d with (nolock)
		on d.District_Number = a.District
	--left join dbo.Report_Traffic as t with (nolock)
	--	on t.Date = s.Date
	--	and t.Store = s.Store
	--left join dbo.Report_Hours as h with (nolock)
	--	on h.Date = s.Date
	--	and h.Store = s.Store
	where s.Date = @reportdate
	and a.Region = @region
	group by Region_Description, District_Description, a.Store + ' ' + a.Store_Name

	-- Date Range: WTD
	union
	select
		2 as DateRange,
		Region_Description as Region,
		District_Description as District,
		a.Store + ' ' + a.Store_Name as Store,
		sum(s.Sales) as ActualSales,
		sum(isnull(s.Plan_Sales, 0)) as PlanSales,
		case sum(isnull(s.Plan_Sales, 0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Plan_Sales, 0)) end as VarToPlanPercent,
		sum(s.Sales) - sum(isnull(s.Plan_Sales, 0)) as VarToPlanDollar,
		case sum(s.Comp_LY) when 0 then 0 else sum(s.Comp_TY)/sum(s.Comp_LY) end as CompPercent,
		sum(s.Comp_TY) - sum(s.Comp_LY) as CompToLY,
		sum(s.Comp_TY) as CompTY,
		sum(s.Comp_LY) as CompLY,
		--sum(isnull(t.ExitCount,0)) as Traffic,
		--case sum(isnull(t.ExitCount,0)) when 0 then 0 else sum(isnull(s.Transactions,0))/cast(sum(isnull(t.ExitCount,0)) as float) end as Conversion,
		sum(isnull(s.Transactions,0)) as Transactions,
		--case sum(isnull(t.ExitCount,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(t.ExitCount,0)) end as DPS,
		--case sum(isnull(h.Hours,0)) when 0 then 0 else sum(isnull(t.ExitCount,0))/sum(isnull(h.Hours,0)) end as CAR,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Transactions,0)) end as ADT,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Units)/cast(sum(isnull(s.Transactions,0)) as float) end as UPT,
		sum(s.Units) as Units,
		sum(case s.Channel_Key when 3 then s.Sales else 0 end) as SaveTheSale,
		sum(isnull(s.TCC_Sales,0)) as TCCSales,
		sum(isnull(s.Loyalty_Sales,0)) as LoyaltySales
	from dbo.Report_Sales as s with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = s.Store
	inner join dbo.Region as r with (nolock)
		on r.Region_Number = a.Region
	inner join dbo.District as d with (nolock)
		on d.District_Number = a.District
	--left join dbo.Report_Traffic as t with (nolock)
	--	on t.Date = s.Date
	--	and t.Store = s.Store
	--left join dbo.Report_Hours as h with (nolock)
	--	on h.Date = s.Date
	--	and h.Store = s.Store
	where s.Date between @weekbegindate and @reportdate
	and a.Region = @region
	group by Region_Description, District_Description, a.Store + ' ' + a.Store_Name

	-- Date Range: MTD
	union
	select
		3 as DateRange,
		Region_Description as Region,
		District_Description as District,
		a.Store + ' ' + a.Store_Name as Store,
		sum(s.Sales) as ActualSales,
		sum(isnull(s.Plan_Sales, 0)) as PlanSales,
		case sum(isnull(s.Plan_Sales, 0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Plan_Sales, 0)) end as VarToPlanPercent,
		sum(s.Sales) - sum(isnull(s.Plan_Sales, 0)) as VarToPlanDollar,
		case sum(s.Comp_LY) when 0 then 0 else sum(s.Comp_TY)/sum(s.Comp_LY) end as CompPercent,
		sum(s.Comp_TY) - sum(s.Comp_LY) as CompToLY,
		sum(s.Comp_TY) as CompTY,
		sum(s.Comp_LY) as CompLY,
		--sum(isnull(t.ExitCount,0)) as Traffic,
		--case sum(isnull(t.ExitCount,0)) when 0 then 0 else sum(isnull(s.Transactions,0))/cast(sum(isnull(t.ExitCount,0)) as float) end as Conversion,
		sum(isnull(s.Transactions,0)) as Transactions,
		--case sum(isnull(t.ExitCount,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(t.ExitCount,0)) end as DPS,
		--case sum(isnull(h.Hours,0)) when 0 then 0 else sum(isnull(t.ExitCount,0))/sum(isnull(h.Hours,0)) end as CAR,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Transactions,0)) end as ADT,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Units)/cast(sum(isnull(s.Transactions,0)) as float) end as UPT,
		sum(s.Units) as Units,
		sum(case s.Channel_Key when 3 then s.Sales else 0 end) as SaveTheSale,
		sum(isnull(s.TCC_Sales,0)) as TCCSales,
		sum(isnull(s.Loyalty_Sales,0)) as LoyaltySales
	from dbo.Report_Sales as s with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = s.Store
	inner join dbo.Region as r with (nolock)
		on r.Region_Number = a.Region
	inner join dbo.District as d with (nolock)
		on d.District_Number = a.District
	--left join dbo.Report_Traffic as t with (nolock)
	--	on t.Date = s.Date
	--	and t.Store = s.Store
	--left join dbo.Report_Hours as h with (nolock)
	--	on h.Date = s.Date
	--	and h.Store = s.Store
	where s.Date between @monthbegindate and @reportdate
	and a.Region = @region
	group by Region_Description, District_Description, a.Store + ' ' + a.Store_Name

	-- Date Range: QTD
	union
	select
		4 as DateRange,
		Region_Description as Region,
		District_Description as District,
		a.Store + ' ' + a.Store_Name as Store,
		sum(s.Sales) as ActualSales,
		sum(isnull(s.Plan_Sales, 0)) as PlanSales,
		case sum(isnull(s.Plan_Sales, 0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Plan_Sales, 0)) end as VarToPlanPercent,
		sum(s.Sales) - sum(isnull(s.Plan_Sales, 0)) as VarToPlanDollar,
		case sum(s.Comp_LY) when 0 then 0 else sum(s.Comp_TY)/sum(s.Comp_LY) end as CompPercent,
		sum(s.Comp_TY) - sum(s.Comp_LY) as CompToLY,
		sum(s.Comp_TY) as CompTY,
		sum(s.Comp_LY) as CompLY,
		--sum(isnull(t.ExitCount,0)) as Traffic,
		--case sum(isnull(t.ExitCount,0)) when 0 then 0 else sum(isnull(s.Transactions,0))/cast(sum(isnull(t.ExitCount,0)) as float) end as Conversion,
		sum(isnull(s.Transactions,0)) as Transactions,
		--case sum(isnull(t.ExitCount,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(t.ExitCount,0)) end as DPS,
		--case sum(isnull(h.Hours,0)) when 0 then 0 else sum(isnull(t.ExitCount,0))/sum(isnull(h.Hours,0)) end as CAR,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Transactions,0)) end as ADT,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Units)/cast(sum(isnull(s.Transactions,0)) as float) end as UPT,
		sum(s.Units) as Units,
		sum(case s.Channel_Key when 3 then s.Sales else 0 end) as SaveTheSale,
		sum(isnull(s.TCC_Sales,0)) as TCCSales,
		sum(isnull(s.Loyalty_Sales,0)) as LoyaltySales
	from dbo.Report_Sales as s with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = s.Store
	inner join dbo.Region as r with (nolock)
		on r.Region_Number = a.Region
	inner join dbo.District as d with (nolock)
		on d.District_Number = a.District
	--left join dbo.Report_Traffic as t with (nolock)
	--	on t.Date = s.Date
	--	and t.Store = s.Store
	--left join dbo.Report_Hours as h with (nolock)
	--	on h.Date = s.Date
	--	and h.Store = s.Store
	where s.Date between @quarterbegindate and @reportdate
	and a.Region = @region
	group by Region_Description, District_Description, a.Store + ' ' + a.Store_Name

	-- Date Range: YTD
	union
	select
		5 as DateRange,
		Region_Description as Region,
		District_Description as District,
		a.Store + ' ' + a.Store_Name as Store,
		sum(s.Sales) as ActualSales,
		sum(isnull(s.Plan_Sales, 0)) as PlanSales,
		case sum(isnull(s.Plan_Sales, 0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Plan_Sales, 0)) end as VarToPlanPercent,
		sum(s.Sales) - sum(isnull(s.Plan_Sales, 0)) as VarToPlanDollar,
		case sum(s.Comp_LY) when 0 then 0 else sum(s.Comp_TY)/sum(s.Comp_LY) end as CompPercent,
		sum(s.Comp_TY) - sum(s.Comp_LY) as CompToLY,
		sum(s.Comp_TY) as CompTY,
		sum(s.Comp_LY) as CompLY,
		--sum(isnull(t.ExitCount,0)) as Traffic,
		--case sum(isnull(t.ExitCount,0)) when 0 then 0 else sum(isnull(s.Transactions,0))/cast(sum(isnull(t.ExitCount,0)) as float) end as Conversion,
		sum(isnull(s.Transactions,0)) as Transactions,
		--case sum(isnull(t.ExitCount,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(t.ExitCount,0)) end as DPS,
		--case sum(isnull(h.Hours,0)) when 0 then 0 else sum(isnull(t.ExitCount,0))/sum(isnull(h.Hours,0)) end as CAR,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Transactions,0)) end as ADT,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Units)/cast(sum(isnull(s.Transactions,0)) as float) end as UPT,
		sum(s.Units) as Units,
		sum(case s.Channel_Key when 3 then s.Sales else 0 end) as SaveTheSale,
		sum(isnull(s.TCC_Sales,0)) as TCCSales,
		sum(isnull(s.Loyalty_Sales,0)) as LoyaltySales
	from dbo.Report_Sales as s with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = s.Store
	inner join dbo.Region as r with (nolock)
		on r.Region_Number = a.Region
	inner join dbo.District as d with (nolock)
		on d.District_Number = a.District
	--left join dbo.Report_Traffic as t with (nolock)
	--	on t.Date = s.Date
	--	and t.Store = s.Store
	--left join dbo.Report_Hours as h with (nolock)
	--	on h.Date = s.Date
	--	and h.Store = s.Store
	where s.Date between @yearbegindate and @reportdate
	and a.Region = @region
	group by Region_Description, District_Description, a.Store + ' ' + a.Store_Name
), --End Current Year Sales

-- Current Year Traffic
CurrentYearTraffic as
(
	-- Date Range: Day
	select
		1 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(t.ExitCount,0)) as ExitCount
	from dbo.Report_Traffic as t with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = t.Store
	where t.Date = @reportdate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name

	-- Date Range: WTD
	union
	select
		2 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(t.ExitCount,0)) as ExitCount
	from dbo.Report_Traffic as t with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = t.Store
	where t.Date between @weekbegindate and @reportdate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name

	-- Date Range: MTD
	union
	select
		3 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(t.ExitCount,0)) as ExitCount
	from dbo.Report_Traffic as t with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = t.Store
	where t.Date between @monthbegindate and @reportdate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name

	-- Date Range: QTD
	union
	select
		4 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(t.ExitCount,0)) as ExitCount
	from dbo.Report_Traffic as t with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = t.Store
	where t.Date between @quarterbegindate and @reportdate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name

	-- Date Range: YTD
	union
	select
		5 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(t.ExitCount,0)) as ExitCount
	from dbo.Report_Traffic as t with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = t.Store
	where t.Date between @yearbegindate and @reportdate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name
), -- End Current Year Traffic

-- Current Year Hours
CurrentYearHours as
(
	-- Date Range: Day
	select
		1 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(h.Hours,0)) as Hours
	from dbo.Report_Hours as h with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = h.Store
	where h.Date = @reportdate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name

	-- Date Range: WTD
	union
	select
		2 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(h.Hours,0)) as Hours
	from dbo.Report_Hours as h with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = h.Store
	where h.Date between @weekbegindate and @reportdate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name

	-- Date Range: MTD
	union
	select
		3 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(h.Hours,0)) as Hours
	from dbo.Report_Hours as h with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = h.Store
	where h.Date between @monthbegindate and @reportdate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name

	-- Date Range: QTD
	union
	select
		4 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(h.Hours,0)) as Hours
	from dbo.Report_Hours as h with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = h.Store
	where h.Date between @quarterbegindate and @reportdate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name

	-- Date Range: YTD
	union
	select
		5 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(h.Hours,0)) as Hours
	from dbo.Report_Hours as h with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = h.Store
	where h.Date between @yearbegindate and @reportdate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name
), -- End Current Year Hours

-- Last Year
LastYear as
(
	-- Date Range: Day
	select
		1 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(s.Sales,0)) as Sales,
		sum(isnull(s.Units,0)) as Units,
		sum(isnull(s.Transactions,0)) as Transactions,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Transactions,0)) end as ADT,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Units)/cast(sum(isnull(s.Transactions,0)) as float) end as UPT
	from dbo.Report_Sales as s with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = s.Store
	where s.Date = @lydate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name

	-- Date Range: WTD
	union
	select
		2 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(s.Sales,0)) as Sales,
		sum(isnull(s.Units,0)) as Units,
		sum(isnull(s.Transactions,0)) as Transactions,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Transactions,0)) end as ADT,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Units)/cast(sum(isnull(s.Transactions,0)) as float) end as UPT
	from dbo.Report_Sales as s with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = s.Store
	where s.Date between @lyweekbegindate and @lydate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name

	-- Date Range: MTD
	union
	select
		3 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(s.Sales,0)) as Sales,
		sum(isnull(s.Units,0)) as Units,
		sum(isnull(s.Transactions,0)) as Transactions,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Transactions,0)) end as ADT,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Units)/cast(sum(isnull(s.Transactions,0)) as float) end as UPT
	from dbo.Report_Sales as s with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = s.Store
	where s.Date between @lymonthbegindate and @lydate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name

	-- Date Range: QTD
	union
	select
		4 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(s.Sales,0)) as Sales,
		sum(isnull(s.Units,0)) as Units,
		sum(isnull(s.Transactions,0)) as Transactions,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Transactions,0)) end as ADT,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Units)/cast(sum(isnull(s.Transactions,0)) as float) end as UPT
	from dbo.Report_Sales as s with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = s.Store
	where s.Date between @lyquarterbegindate and @lydate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name

	-- Date Range: YTD
	union
	select
		5 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		sum(isnull(s.Sales,0)) as Sales,
		sum(isnull(s.Units,0)) as Units,
		sum(isnull(s.Transactions,0)) as Transactions,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Sales)/sum(isnull(s.Transactions,0)) end as ADT,
		case sum(isnull(s.Transactions,0)) when 0 then 0 else sum(s.Units)/cast(sum(isnull(s.Transactions,0)) as float) end as UPT
	from dbo.Report_Sales as s with (nolock)
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = s.Store
	where s.Date between @lyyearbegindate and @lydate
	and a.Region = @region
	group by a.Store + ' ' + a.Store_Name
), -- End Last Year

-- Peers
Peer as 
(
	-- Date Range: Day
	select
		1 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		c.Peer_Group,
		sum(isnull(rs.Sales,0)) as Sales,
		sum(isnull(rs.Transactions,0)) as Transactions,
		sum(isnull(rt.ExitCount,0)) as Traffic,
		case sum(cast(isnull(rt.ExitCount,0) as bigint)) when 0 then 0 
			else sum(isnull(rs.Transactions,0))/cast(sum(isnull(rt.ExitCount,0)) as float) end as Conversion,
		case sum(cast(isnull(rt.ExitCount,0) as bigint)) when 0 then 0 
			else sum(isnull(rs.Sales,0))/sum(isnull(rt.ExitCount,0)) end as DPS
	from dbo.store_category as c with (nolock)
	inner join dbo.Store_Category as p with (nolock)
		on p.Peer_Group = c.Peer_Group
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = c.Store
	inner join dbo.Report_Sales as rs with (nolock)
		on rs.Store = p.Store
	inner join dbo.Report_Traffic as rt with (nolock)
		on rt.Store = p.Store
	where a.Region = @region
	and rs.Date = @reportdate
	and rt.Date = @reportdate
	group by a.Store + ' ' + a.Store_Name, c.Peer_Group

	-- Date Range: WTD
	union
	select
		2 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		c.Peer_Group,
		sum(isnull(rs.Sales,0)) as Sales,
		sum(isnull(rs.Transactions,0)) as Transactions,
		sum(isnull(rt.ExitCount,0)) as Traffic,
		case sum(cast(isnull(rt.ExitCount,0) as bigint)) when 0 then 0 
			else sum(isnull(rs.Transactions,0))/cast(sum(isnull(rt.ExitCount,0)) as float) end as Conversion,
		case sum(cast(isnull(rt.ExitCount,0) as bigint)) when 0 then 0 
			else sum(isnull(rs.Sales,0))/sum(isnull(rt.ExitCount,0)) end as DPS
	from dbo.store_category as c with (nolock)
	inner join dbo.Store_Category as p with (nolock)
		on p.Peer_Group = c.Peer_Group
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = c.Store
	inner join dbo.Report_Sales as rs with (nolock)
		on rs.Store = p.Store
	inner join dbo.Report_Traffic as rt with (nolock)
		on rt.Store = p.Store
	where a.Region = @region
	and rs.Date between @weekbegindate and @reportdate
	and rt.Date between @weekbegindate and @reportdate
	group by a.Store + ' ' + a.Store_Name, c.Peer_Group

	-- Date Range: MTD
	union
	select
		3 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		c.Peer_Group,
		sum(isnull(rs.Sales,0)) as Sales,
		sum(isnull(rs.Transactions,0)) as Transactions,
		sum(isnull(rt.ExitCount,0)) as Traffic,
		case sum(cast(isnull(rt.ExitCount,0) as bigint)) when 0 then 0 
			else sum(isnull(rs.Transactions,0))/cast(sum(isnull(rt.ExitCount,0)) as float) end as Conversion,
		case sum(cast(isnull(rt.ExitCount,0) as bigint)) when 0 then 0 
			else sum(isnull(rs.Sales,0))/sum(isnull(rt.ExitCount,0)) end as DPS
	from dbo.store_category as c with (nolock)
	inner join dbo.Store_Category as p with (nolock)
		on p.Peer_Group = c.Peer_Group
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = c.Store
	inner join dbo.Report_Sales as rs with (nolock)
		on rs.Store = p.Store
	inner join dbo.Report_Traffic as rt with (nolock)
		on rt.Store = p.Store
	where a.Region = @region
	and rs.Date between @monthbegindate and @reportdate
	and rt.Date between @monthbegindate and @reportdate
	group by a.Store + ' ' + a.Store_Name, c.Peer_Group

	-- Date Range: QTD
	union
	select
		4 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		c.Peer_Group,
		sum(isnull(rs.Sales,0)) as Sales,
		sum(isnull(rs.Transactions,0)) as Transactions,
		sum(isnull(rt.ExitCount,0)) as Traffic,
		case sum(cast(isnull(rt.ExitCount,0) as bigint)) when 0 then 0 
			else sum(isnull(rs.Transactions,0))/cast(sum(isnull(rt.ExitCount,0)) as float) end as Conversion,
		case sum(cast(isnull(rt.ExitCount,0) as bigint)) when 0 then 0 
			else sum(isnull(rs.Sales,0))/sum(isnull(rt.ExitCount,0)) end as DPS
	from dbo.store_category as c with (nolock)
	inner join dbo.Store_Category as p with (nolock)
		on p.Peer_Group = c.Peer_Group
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = c.Store
	inner join dbo.Report_Sales as rs with (nolock)
		on rs.Store = p.Store
	inner join dbo.Report_Traffic as rt with (nolock)
		on rt.Store = p.Store
	where a.Region = @region
	and rs.Date between @quarterbegindate and @reportdate
	and rt.Date between @quarterbegindate and @reportdate
	group by a.Store + ' ' + a.Store_Name, c.Peer_Group

	-- Date Range: YTD
	union
	select
		5 as DateRange,
		a.Store + ' ' + a.Store_Name as Store,
		c.Peer_Group,
		sum(isnull(rs.Sales,0)) as Sales,
		sum(isnull(rs.Transactions,0)) as Transactions,
		sum(isnull(rt.ExitCount,0)) as Traffic,
		case sum(cast(isnull(rt.ExitCount,0) as bigint)) when 0 then 0 
			else sum(isnull(rs.Transactions,0))/cast(sum(isnull(rt.ExitCount,0)) as float) end as Conversion,
		case sum(cast(isnull(rt.ExitCount,0) as bigint)) when 0 then 0 
			else sum(isnull(rs.Sales,0))/sum(isnull(rt.ExitCount,0)) end as DPS
	from dbo.store_category as c with (nolock)
	inner join dbo.Store_Category as p with (nolock)
		on p.Peer_Group = c.Peer_Group
	inner join dbo.Store_Alignment as a with (nolock)
		on a.Store = c.Store
	inner join dbo.Report_Sales as rs with (nolock)
		on rs.Store = p.Store
	inner join dbo.Report_Traffic as rt with (nolock)
		on rt.Store = p.Store
	where a.Region = @region
	and rs.Date between @yearbegindate and @reportdate
	and rt.Date between @yearbegindate and @reportdate
	group by a.Store + ' ' + a.Store_Name, c.Peer_Group
) -- End Peers

-- Final Query
select
	cy.DateRange,
	cy.Region,
	cy.District,
	cy.Store,
	cy.ActualSales,
	cy.PlanSales,
	cy.VarToPlanPercent,
	cy.VarToPlanDollar,
	cy.CompPercent,
	cy.CompToLY,
	cy.CompTY,
	cy.CompLY,
	isnull(t.ExitCount,0) as Traffic,
	p.Traffic as TrafficPeer,
	case isnull(t.ExitCount,0) when 0 then 0 else isnull(cy.Transactions,0)/cast(isnull(t.ExitCount,0) as float) end as Conversion,
	p.Conversion as ConversionPeer,
	cy.Transactions,
	ly.Transactions as TransactionsLY,
	p.Transactions as TransactionsPeer,
	case isnull(t.ExitCount,0) when 0 then 0 else cy.ActualSales/isnull(t.ExitCount,0) end as DPS,
	p.DPS as DPSPeer,
	case isnull(h.Hours,0) when 0 then 0 else isnull(t.ExitCount,0)/isnull(h.Hours,0) end as CAR,
	isnull(h.Hours,0) as Hours,
	cy.ADT,
	ly.ADT as ADTLY,
	case ly.ADT when 0 then 0 else cy.ADT/ly.ADT - 1 end as ADTVarToLY,
	cy.UPT,
	ly.UPT as UPTLY,
	case ly.UPT when 0 then 0 else cy.UPT/ly.UPT - 1 end as UPTVarToLY,
	cy.Units,
	ly.Sales as SalesLY,
	ly.Units as UnitsLY,
	p.Sales as SalesPeer,
	cy.SaveTheSale,
	cy.LoyaltySales,
	cy.TCCSales
from CurrentYearSales as cy
left join CurrentYearTraffic as t
	on t.DateRange = cy.DateRange
	and t.Store = cy.Store
left join CurrentYearHours as h
	on h.DateRange = cy.DateRange
	and h.Store = cy.Store
left join LastYear as ly
	on ly.DateRange = cy.DateRange
	and ly.Store = cy.Store
left join Peer as p
	on p.DateRange = cy.DateRange
	and p.Store = cy.Store
order by cy.DateRange, cy.Region, cy.District, cy.Store
end

grant execute on dbo.USP_ScoreCardByRegion to R_TrafficScorecardRpt