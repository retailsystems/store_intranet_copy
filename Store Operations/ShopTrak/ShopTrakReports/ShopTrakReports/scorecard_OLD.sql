declare @division int = 1
declare @district varchar(3) = '001'

select
	r.District,
	r.Store,
	r.DataPeriodOrder,
	sum(case when r.DataOrder  1 then Data1 else 0 end) as 'Sales',
	sum(case r.DataOrder when 3  then Data1/Data2 else 0 end) as 'Comp %'
from dbo.Report_Store_Summary as r with (nolock)
where r.Division = @division
and r.District = @district
and r.DataPeriodOrder in (8,11)
group by r.District, r.Store, r.DataPeriodOrder
order by r.District, r.Store, r.DataPeriodOrder

-- select * from dbo.Data_Order

-- select * from dbo.Data_Period_Order