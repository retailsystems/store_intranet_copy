using System;
using System.Collections.Generic;
using System.Text;

namespace ManualTCCSettlement
{
    class TTranStorebyDateSumInfo
    {
        private Int64 m_i64SumStorebyDateTransAmt = 0;

        public Int64 I64SumStorebyDateTransAmt
        {
            get { return m_i64SumStorebyDateTransAmt; }
            set { m_i64SumStorebyDateTransAmt = value; }
        }
        private int m_iByDateStoreNum = 0;

        public int IByDateStoreNum
        {
            get { return m_iByDateStoreNum; }
            set { m_iByDateStoreNum = value; }
        }
        private string m_strTransDateFinal = null;

        public string StrTransDateFinal
        {
            get { return m_strTransDateFinal; }
            set { m_strTransDateFinal = value; }
        }

    }
}
