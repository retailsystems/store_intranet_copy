using System;
using System.Collections.Generic;
using System.Text;

namespace ManualTCCSettlement
{
    class TDLogCCInfo
    //public class TDLogCCInfo
    {
        private string m_tranNumTDCCInfo;
        private string m_lst4CCNumTDCCInfo;
        private string m_fullCCNumTDCCInfo;
        private string m_enterTypeNumTDCCInfo;
        private string m_colOne;
        private string m_colTwo;
        private string m_strCardEntry;

        public string TranNumTDCCInfo
        {
            get { return m_tranNumTDCCInfo; }
            set { m_tranNumTDCCInfo = value; }
        }
        public string Lst4CCNumTDCCInfo
        {
            get { return m_lst4CCNumTDCCInfo; }
            set { m_lst4CCNumTDCCInfo = value; }
        }
        public string EnterTypeNumTDCCInfo
        {
            get { return m_enterTypeNumTDCCInfo; }
            set { m_enterTypeNumTDCCInfo = value; }
        }
        public string ColOne
        {
            get { return m_colOne; }
            set { m_colOne = value; }
        }
        public string ColTwo
        {
            get { return m_colTwo; }
            set { m_colTwo = value; }
        }
        public string FullCCNumTDCCInfo()
        {
            ////m_errorHandler.LogDebug(m_colTwo);
            //try
            //{
            m_fullCCNumTDCCInfo = m_colTwo.Substring(52, 16);
            //}
            //catch (Exception ex)
            //{
            //    //m_errorHandler.LogError(ex);
            //}

            return m_fullCCNumTDCCInfo.ToString();
        }

        public string StrCardEntry()
        {
            m_strCardEntry = m_colTwo.Substring(m_colTwo.Length - 17, 1);
            return m_strCardEntry.ToString();
        }

    }
}
