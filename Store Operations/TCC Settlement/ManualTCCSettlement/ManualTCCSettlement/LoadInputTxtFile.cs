using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using System.Collections;

namespace ManualTCCSettlement
{
    class LoadInputTxtFile
    {
        HTError.IHTError m_errorHandler;
        public LoadInputTxtFile(HTError.IHTError RHtError)
        {
            m_errorHandler = RHtError;
        }

        public ArrayList Start()
        {
            //Open input master file
            ArrayList allFileLines = new ArrayList();                       

            string txtPathFile = FileHelper.BuildExcelFilePath();
            if (!File.Exists(txtPathFile))
            {
                m_errorHandler.SendMailOnError = true;
                m_errorHandler.LogError("Can not find Torrid Credit Card Settlement .csv file: " + txtPathFile + ".Please make sure it has been run SSIS package first.");

                System.Environment.Exit(1);
            }
            try
            {
                if (txtPathFile != null)
                {
                    System.IO.TextReader sTextFile = null;
                    sTextFile = new System.IO.StreamReader(txtPathFile);

                    string s_CCReadFile = String.Empty;
                    int iLineNum = 1;

                    //Capture the +ve, -ve transactions in Hash for verification                    
                    Hashtable htPOSTranType = new Hashtable();
                    Hashtable htNEGTranType = new Hashtable();
                    string[] strPOSTranTypes = ConfigurationManager.AppSettings["POSTranTypes"].ToString().Split(',');
                    string[] strNEGTranTypes = ConfigurationManager.AppSettings["NEGTranTypes"].ToString().Split(',');

                    foreach (string strPOSTranType in strPOSTranTypes)
                        htPOSTranType.Add(strPOSTranType, strPOSTranType);
                    foreach (string strNEGTranType in strNEGTranTypes)
                        htNEGTranType.Add(strNEGTranType, strNEGTranType);


                    //Loop through eachline
                    while ((s_CCReadFile = sTextFile.ReadLine()) != null)
                    {
                        string MainString = s_CCReadFile;

                        GetTxtInputInfo c_InputCC = new GetTxtInputInfo();
                        c_InputCC.WholeLine = s_CCReadFile;

                        if (MainString.StartsWith("StoreNum"))
                        {
                            c_InputCC.IsComment = true;
                        }
                        else
                        {
                            string[] myARSplit = MainString.Split(',');

                            c_InputCC.StrStoreNum = myARSplit[0].ToString();
                            c_InputCC.StrOutputTranDate = myARSplit[1].ToString();
                            c_InputCC.StrTranNum = myARSplit[2].ToString();
                            c_InputCC.StrTranAmt = myARSplit[3].ToString();
                            c_InputCC.StrTranTax = myARSplit[4].ToString();
                            c_InputCC.StrCCNumMask = myARSplit[5].ToString();
                            c_InputCC.StrAuth = myARSplit[6].ToString();
                            c_InputCC.StrTransType = myARSplit[7].ToString();

                            if (c_InputCC.StrTranAmt.Trim().Length == 0)
                            {
                                throw new Exception("Transaction Amount cannot be empty in line # " + iLineNum.ToString() + ". Please correct it and try again.");
                            }
                            if (c_InputCC.StrTransType.Trim().Length == 0)
                            {
                                throw new Exception("Transaction type cannot be empty in line # " + iLineNum.ToString() + ". Please correct it and try again.");
                            }
                            if (htPOSTranType.ContainsKey(c_InputCC.StrTransType) == false && htNEGTranType.ContainsKey(c_InputCC.StrTransType) == false)
                            {
                                throw new Exception("Transaction type is invalid in line # " + iLineNum.ToString() + ". Please correct it and try again.");
                            }
                            if (int.Parse(c_InputCC.StrTranAmt) > 0 && htPOSTranType.ContainsKey(c_InputCC.StrTransType)== false)
                            {
                                throw new Exception("Transaction Amount cannot be a positive amount for the provided tranaction type in line # " + iLineNum.ToString() + ". Please correct it and try again.");
                            }                            
                            if (int.Parse(c_InputCC.StrTranAmt) < 0 && htNEGTranType.ContainsKey(c_InputCC.StrTransType)== false)
                            {
                                throw new Exception("Transaction Amount cannot be a negative amount for the provided tranaction type in line # " + iLineNum.ToString() + ". Please correct it and try again.");
                            }
                                                        
                            allFileLines.Add(c_InputCC);
                        }
                        iLineNum++;
                    }
                    sTextFile.Dispose();
                }
            }
            catch (Exception ex)
            {
                m_errorHandler.LogError(ex.Message);
                allFileLines.Clear();
                //m_errorHandler.LogMessage("Error: occurs on loading input file.");
            }
            return allFileLines;
        }
    }
}
