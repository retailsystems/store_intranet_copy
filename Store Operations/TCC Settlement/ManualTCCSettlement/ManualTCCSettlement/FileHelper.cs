using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;
using System.Configuration;

namespace ManualTCCSettlement
{
    class FileHelper
    {
        //input master file path
        private string m_CurrentDataPath = null;
        private DateTime dtBeginDateTime;
        HTError.IHTError m_errorHandler;

        public FileHelper(HTError.IHTError RHtError)
        {
            m_errorHandler = RHtError;
        }
  
        public static string BuildExcelFilePath()
        {
            string dirPrefixInput = ConfigurationManager.AppSettings["TCCInputPrefix"].ToString();

            string today = DateTime.Today.ToString("yyyyMMdd");

            return ConfigurationManager.AppSettings["excelInputFilePath"].ToString() + dirPrefixInput + today + ".csv";
        }
       
        public static string BuildOutputFileName()
        {
            string dirPrefix = ConfigurationManager.AppSettings["TDOutputRetailReportName"].ToString();
            return ConfigurationManager.AppSettings["txtOutputFilePath"].ToString() + dirPrefix + ".txt";
        }
        
        public static string BuildOutputBalanceFileName()
        {
            string dirPrefix = ConfigurationManager.AppSettings["TDOutputBalanceReportName"].ToString();
            return ConfigurationManager.AppSettings["txtOutputFilePath"].ToString() + dirPrefix + ".txt";
            //return ConfigurationManager.AppSettings["txtOutputFilePath"].ToString() + dirPrefix + ".txt";
        }
        public static string BuildOutputStoreSumFileName()
        {
            string dirPrefix = ConfigurationManager.AppSettings["TDOutputStoreSumReportName"].ToString();
            return ConfigurationManager.AppSettings["txtOutputStoreSum4SalesAuditPath"].ToString() + dirPrefix + ".csv";
            //return ConfigurationManager.AppSettings["txtOutputFilePath"].ToString() + dirPrefix + ".txt";
        }
        public void ArchiveFile(string m_CurrentDataPath, string m_ArchivePath)
        {
            try
            {

                if (!m_CurrentDataPath.EndsWith("\\"))
                {
                    m_CurrentDataPath += "\\";
                }
                if (!m_ArchivePath.EndsWith("\\"))
                {
                    m_ArchivePath += "\\";
                }

                DirectoryInfo sourcePath = new DirectoryInfo(m_CurrentDataPath);

                DirectoryInfo destinationPath = new DirectoryInfo(m_ArchivePath);

                if (!sourcePath.Exists)
                {
                    m_errorHandler.LogWarning("Source Path: " + m_CurrentDataPath + "does not exist.");
                }

                if (!destinationPath.Exists)
                {
                    Directory.CreateDirectory(m_ArchivePath);
                }

                //Gets file in current directory

                foreach (FileInfo fiInfo in sourcePath.GetFiles())
                {
                    //Rename file name 
                    string fileDateTime = "_" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "");
                    string newName = fiInfo.Name.Insert(fiInfo.Name.Length - fiInfo.Extension.Length, fileDateTime);

                    //Copy File to Archive folder
                    File.Copy(m_CurrentDataPath + "\\" + fiInfo.Name, m_ArchivePath + "\\" + newName);

                }
            }
            catch (Exception ex)
            {
                //log error
                m_errorHandler.LogError(ex, "Error is from Archiving Files.");
                m_errorHandler.LogWarning("Error occurs once archive files from source folder: " + m_CurrentDataPath + " to destination folder: " + m_ArchivePath + ".");
            }
        }


        public void DeleteOutdatedArchivedFile(string strArchivePath, int iDeleteArchiveFileBackDays)
        {
            try
            {

                if (!strArchivePath.EndsWith("\\"))
                {
                    strArchivePath += "\\";
                }

                DirectoryInfo archivedFilePath = new DirectoryInfo(strArchivePath);

                if (!archivedFilePath.Exists)
                {
                    Directory.CreateDirectory(strArchivePath);
                }

                //Gets file in current directory

                foreach (FileInfo fiInfo in archivedFilePath.GetFiles())
                {

                    dtBeginDateTime = DateTime.Now.AddDays(-1 * iDeleteArchiveFileBackDays);

                    if ((fiInfo.LastWriteTime.CompareTo(dtBeginDateTime) < 0))
                    {
                        //delete File in Archive folder for those files older than a given period time 
                        fiInfo.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                //log error
                m_errorHandler.LogError(ex, "Error from deleting Outdated Archived File.");
                m_errorHandler.LogWarning("Error occurs once deleting outdated archive files from folder: " + strArchivePath + ".");
            }
        }
        public void ClearFileInCurrentFolder(string filename)
        {
            try
            {
                m_CurrentDataPath = ConfigurationManager.AppSettings["txtOutputFilePath"].ToString();

                if (!m_CurrentDataPath.EndsWith("\\"))
                {
                    m_CurrentDataPath += "\\";
                }
                string fullFileName = m_CurrentDataPath + filename;
                DirectoryInfo currentFilePath = new DirectoryInfo(m_CurrentDataPath);
                try
                {
                    if (currentFilePath.Exists)
                    {
                        FileInfo fiInfo = new FileInfo(fullFileName);
                        if (fiInfo != null)
                        {
                            fiInfo.Delete();
                        }
                    }
                    else
                    {
                        m_errorHandler.LogInformation("To be cleared files do not exist under the directory of " + currentFilePath.FullName);
                    }
                }
                catch
                {
                    m_errorHandler.LogInformation("Unable to find files under the directory: " + currentFilePath.FullName);
                    throw;
                }
            }
            catch (Exception ex)
            {
                //log error
                m_errorHandler.LogMessage(" Task failed to clear File In Current Output Folder ", HTError.LogLevel.Error);
                m_errorHandler.LogError(ex);
            }
        }
       
        public void WriteToFile(string strData, string filePath)
        {
            try
            {
                TextWriter stringWriter = new StringWriter();
                
                using (TextWriter streamWriter = new StreamWriter(filePath, true, Encoding.ASCII))
                {
                    streamWriter.WriteLine(strData);
                }
            }
            catch (Exception ex)
            {

                m_errorHandler.LogError(ex);
                m_errorHandler.LogMessage("Write to file task failed. ", HTError.LogLevel.Error);
            }
        }

    }
}
