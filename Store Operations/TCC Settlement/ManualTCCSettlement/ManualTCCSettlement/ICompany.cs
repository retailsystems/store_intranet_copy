using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using HTError;

namespace ManualTCCSettlement
{
    public interface ICompany
    {
        bool LoadMasterTxtFile();
        bool WriteToDetailReport();
    }
}
