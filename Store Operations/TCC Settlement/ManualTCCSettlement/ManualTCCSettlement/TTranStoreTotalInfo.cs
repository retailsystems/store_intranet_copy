using System;
using System.Collections.Generic;
using System.Text;

namespace ManualTCCSettlement
{
    class TTranStoreTotalInfo
    {
        private Int64 m_i64SumDateStoreTransAmt = 0;

        public Int64 I64SumDateStoreTransAmt
        {
            get { return m_i64SumDateStoreTransAmt; }
            set { m_i64SumDateStoreTransAmt = value; }
        }
        private int m_iStoreNumforSum = 0;

        public int IStoreNumforSum
        {
            get { return m_iStoreNumforSum; }
            set { m_iStoreNumforSum = value; }
        }
        private string m_strTransDateforSum = null;

        public string StrTransDateforSum
        {
            get { return m_strTransDateforSum; }
            set { m_strTransDateforSum = value; }
        }
        private bool m_bGoodLineTwoKeys;
        
    }
}
