using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using HTError;

namespace ManualTCCSettlement
{
    class Program
    {
        public HTError.IHTError m_errorHandler = null;
        static void Main(string[] args)
        {
            TTorrid td = new TTorrid();
            int nRunFTP = int.Parse(ConfigurationManager.AppSettings["RunFTP"].ToString());

            if (runTask(ref td) == true)
            {
                if (nRunFTP == 1)
                {
                    runCuteFTP(ref td);
                }
            }
        }

        private static bool runTask(ref TTorrid td)
        {
            bool bLoadMasterInputSucceeded = false;
            bool bWriteToDetailReportSucceeded = false;
            bool bRunTCCSuccess = false;
            try
            {
                bLoadMasterInputSucceeded = td.LoadMasterTxtFile();
                if (bLoadMasterInputSucceeded == true)
                {
                    bWriteToDetailReportSucceeded = td.WriteToDetailReport();
                    if (bWriteToDetailReportSucceeded == true)
                    {
                            bRunTCCSuccess = true;
                            td.m_errorHandler.LogNotify("Manaul TCC output task has completed successfully!");
                    }
                    else
                    {
                        td.m_errorHandler.LogError("Failed to run Manaul TCC Retail/Balance reports.");

                    }
                }
                else
                {
                    td.m_errorHandler.LogNotify("Empty Manual Input file.");
                    bRunTCCSuccess = true;

                }
            }
            catch (Exception ex)
            {
                td.m_errorHandler.LogNotify(ex.Message);
            }
            return bRunTCCSuccess;
        }
        private static bool runCuteFTP(ref TTorrid td)
        {
            bool bFTPSucceeded = false;
            try
            {
                int iProcessExitCode = 0;
                string m_strCuteFTPExePath = ConfigurationManager.AppSettings["CuteFTPExePath"].ToString();
                string m_strCuteFTPExeName = ConfigurationManager.AppSettings["CuteFTPExeName"].ToString();
                string m_strFullPathCuteFTP = m_strCuteFTPExePath + m_strCuteFTPExeName + ".exe";

                //production
                //System.Diagnostics.ProcessStartInfo proStartInfo = new System.Diagnostics.ProcessStartInfo(@"C:\TCC_Settlement_Manual\TCCManualApps\HTCuteFTP.exe");
                System.Diagnostics.ProcessStartInfo proStartInfo = new System.Diagnostics.ProcessStartInfo(m_strFullPathCuteFTP);

                proStartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                proStartInfo.UseShellExecute = false;

                using (System.Diagnostics.Process TCCProcess = System.Diagnostics.Process.Start(proStartInfo))
                {

                    TCCProcess.WaitForExit();
                    iProcessExitCode = TCCProcess.ExitCode;
                }
            }
            catch (Exception ex)
            {
                td.m_errorHandler.LogNotify("Failed to run Auto FTP.");
                td.m_errorHandler.LogNotify(ex.Message);
            }
            return bFTPSucceeded;
        }
    }
}
