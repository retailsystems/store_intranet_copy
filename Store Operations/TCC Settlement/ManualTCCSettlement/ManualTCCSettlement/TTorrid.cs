using System;
using System.Collections.Generic;
using System.Data;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using HTError;
using System.Net.Mail;

namespace ManualTCCSettlement
{
    class TTorrid : ICompany
    {
        //protected HTError.IHTError m_errorHandler;
        public HTError.IHTError m_errorHandler;
        private List<string> m_listStrProcessDays = null;
        private ArrayList m_listInputText = null;
        private Hashtable m_hashTlog = new Hashtable();

        //m_errorHandler = Program.GetLog();

        public TTorrid()
        {
            GetLog();
        }

        public bool LoadMasterTxtFile()
        {
            // Build the keys for the master file
            LoadInputTxtFile loadInputTxtFile = new LoadInputTxtFile(m_errorHandler);
            m_listInputText = loadInputTxtFile.Start();
            if (m_listInputText.Count > 0)
                return true;
            else
            {
                return false;
            }
        }

        public void GetLog()
        {
            string fileName = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.StartupPath) + "\\" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".log";
            String ProductionFilePath = ConfigurationManager.AppSettings["ProductionFilePath"].ToString();
            String DebugFilePath = ConfigurationManager.AppSettings["DebugFilePath"].ToString();
            String EmailServer = ConfigurationManager.AppSettings["EmailServer"].ToString();
            String EmailSender = ConfigurationManager.AppSettings["EmailSender"].ToString();
            String NotifyRecipients = ConfigurationManager.AppSettings["NotifyRecipients"].ToString();

            m_errorHandler = new HTError.ErrorHandler(ProductionFilePath, DebugFilePath, EmailServer, EmailSender, NotifyRecipients, HTError.LogLevel.Debug);

            m_errorHandler.ErrorEmailPriority = System.Net.Mail.MailPriority.High;
            m_errorHandler.SendMailOnError = true;
            m_errorHandler.SendMailOnWarning = true;
        }
     
        public bool WriteToDetailReport()
        {
            Hashtable htTrnTypeAMT = new Hashtable();
            Hashtable htTrnTypeCNT = new Hashtable();

            if (m_listInputText.Count == 0)
                return false;
            string strStoreNumWrite = null;
            string strTransDateWrite = null;
            string strTransNumWrite = null;
            string strLast4CreditCardWrite = null;
            //string strStoreSum = null;
            ArrayList tempTransList = new ArrayList();
            //ArrayList tempStoreList = new ArrayList();
            string finalLine = null;
            string headerLine = "";
            string trailerLine = "";
            //string headerLineToWrite = null;
            bool bFound = false;
            int m_iTotalDebitQty = 0;
            Int64 m_i64TotalDebitAmt = 0;
            int m_iTotalCreditQty = 0;
            Int64 m_i64TotalCreditAmt = 0;
            //char szDlimited = ',';
            string balaceReportLine = "";
            //string m_totalPayTransAmt = "00000000000";
            //string m_totalPayTransCount = "000000";
            //string m_netPayTransAmt = "00000000000";
            //string m_netPayTransCount = "000000";
            //string m_netTotalTransAmt = "00000000000";
            string m_netTotalTransCount = "000000";
            string m_strTotalProSalesAmt = "0000000000000";
            string m_strTotalProSalesCount = "000000000000";
            string m_strCurrentDataPath = ConfigurationManager.AppSettings["txtOutputFilePath"].ToString();
            string m_strArchivePath = ConfigurationManager.AppSettings["txtOutputArchiveFilePath"].ToString();
            int m_iDeleteArchiveFileBackDays = int.Parse(ConfigurationManager.AppSettings["txtDeleteArchiveOutputFileBackDays"].ToString());

            FileHelper FileTemp = new FileHelper(m_errorHandler);
            string m_CurrentDetailReportName = ConfigurationManager.AppSettings["TDOutputRetailReportName"].ToString() + ".txt";
            FileTemp.ClearFileInCurrentFolder(m_CurrentDetailReportName);

            string m_CurrentBalanceReportName = ConfigurationManager.AppSettings["TDOutputBalanceReportName"].ToString() + ".txt";
            FileTemp.ClearFileInCurrentFolder(m_CurrentBalanceReportName);

            //print header
            headerLine = "1" + "TORRID".PadRight(20, ' ') + DateTime.Today.ToString("yyyyMMdd") +
                         "                                     ";
            WriteToFile(headerLine, true);
            //print body
            foreach (GetTxtInputInfo oneLine in m_listInputText)
            {
                if (oneLine != null)
                {
                    bFound = false;
                    if (strStoreNumWrite == oneLine.StrStoreNum)
                    {
                        if (strTransDateWrite == oneLine.StrTranDate())
                        {
                            if (strTransNumWrite == oneLine.StrTranNum)
                            {
                                //change for split tender
                                if (strLast4CreditCardWrite == oneLine.LastFourCCNum())
                                { bFound = true; }
                            }
                        }
                    }
                }
                if (bFound == true)
                    tempTransList.Add(oneLine);
                else
                {
                    strStoreNumWrite = oneLine.StrStoreNum;
                    strTransDateWrite = oneLine.StrTranDate();
                    strTransNumWrite = oneLine.StrTranNum;
                    strLast4CreditCardWrite = oneLine.LastFourCCNum();
                    //change for split tender
                    if (tempTransList.Count == 0)
                    {
                        tempTransList.Add(oneLine);
                    }
                    else
                    {
                        bool bGoodline = false;
                        TTranSubTotalInfo rTransSubTotalInfo = new TTranSubTotalInfo();
                        bool bRet = CalculateTotalItems(tempTransList, ref finalLine, ref bGoodline, ref rTransSubTotalInfo,ref htTrnTypeAMT,ref htTrnTypeCNT);
                        if (bGoodline == true)
                        {
                            m_iTotalDebitQty += rTransSubTotalInfo.ITotalDebitQty;
                            m_i64TotalDebitAmt += rTransSubTotalInfo.I64TotalDebitAmt;
                            m_iTotalCreditQty += rTransSubTotalInfo.ITotalCreditQty;
                            m_i64TotalCreditAmt += rTransSubTotalInfo.I64TotalCreditAmt;

                        }
                        WriteToFile(finalLine, bGoodline);

                        finalLine = null;
                        tempTransList.Clear();
                        tempTransList.Add(oneLine);
                    }

                }
            }//end loop
            if (tempTransList.Count != 0)
            {
                bool bGoodline = false;
                TTranSubTotalInfo rTransSubTotalInfo = new TTranSubTotalInfo();
                bool bRet = CalculateTotalItems(tempTransList, ref finalLine, ref bGoodline, ref rTransSubTotalInfo, ref htTrnTypeAMT, ref htTrnTypeCNT);
                if (bGoodline == true)
                {
                    m_iTotalDebitQty += rTransSubTotalInfo.ITotalDebitQty;
                    m_i64TotalDebitAmt += rTransSubTotalInfo.I64TotalDebitAmt;
                    m_iTotalCreditQty += rTransSubTotalInfo.ITotalCreditQty;
                    m_i64TotalCreditAmt += rTransSubTotalInfo.I64TotalCreditAmt;
                }

                WriteToFile(finalLine, bGoodline);
            }
            //TODO sum
            string i64TotalCreditAmt = m_i64TotalCreditAmt.ToString().Replace("-", "").Trim();
            trailerLine = "9" + DateTime.Today.ToString("yyyyMMdd") + 
                          "                    " + 
                          m_i64TotalDebitAmt.ToString().PadLeft(13, '0') + 
                          m_iTotalDebitQty.ToString().PadLeft(12, '0') + 
                          i64TotalCreditAmt.PadLeft(13, '0') + 
                          m_iTotalCreditQty.ToString().PadLeft(12, '0') + 
                          m_strTotalProSalesAmt + 
                          m_strTotalProSalesCount +
                          "0000" + "                                          ";
            WriteToFile(trailerLine, true);

            // TODO wirte balance report
            int m_iGrossTransQty = m_iTotalDebitQty + m_iTotalCreditQty;

            Int64 m_i64GrossTransAmt = m_i64TotalDebitAmt - m_i64TotalCreditAmt;

            //balaceReportLine = "TORRID".PadRight(20, ' ') + 
            //                   DateTime.Today.ToString("yyyyMMdd") + 
            //                   DateTime.Today.AddDays(-1).ToString("yyyyMMdd") + 
            //                   m_i64GrossTransAmt.ToString().PadLeft(11, '0') + 
            //                   m_iGrossTransQty.ToString().PadLeft(6, '0') + 
            //                   m_i64TotalDebitAmt.ToString().PadLeft(11, '0') + 
            //                   m_iTotalDebitQty.ToString().PadLeft(6, '0') + 
            //                   i64TotalCreditAmt.PadLeft(11, '0') +
            //                   m_iTotalCreditQty.ToString().PadLeft(6, '0') + 
            //                   m_totalPayTransAmt + 
            //                   m_totalPayTransCount + 
            //                   m_netPayTransAmt + 
            //                   m_netPayTransCount + 
            //                   m_netTotalTransAmt + 
            //                   m_netTotalTransCount +
            //                   "                                                  ";
            
            //Modifed to include the all types of transactions    
            m_i64TotalDebitAmt = 0;
            m_iTotalDebitQty = 0;
            m_i64TotalCreditAmt = 0;
            m_iTotalCreditQty = 0;
            Int64 m_i64totalPayTransAmt = 0;
            int m_itotalPayTransCount = 0;
            Int64 m_i64netPayTransAmt486 = 0;            
            Int64 m_i64netPayTransAmt888 = 0;
            Int64 m_i64netPayTransAmt =0;
            int m_inetPayTransCount = 0;
            Int64 m_i64netTotalTransAmt = 0;
            int m_i64netPayTransCount = 0;

            foreach (string key in htTrnTypeAMT.Keys)
            {
                if (key == "201" || key == "202" || key == "206" || key == "452")
                {
                    m_i64TotalDebitAmt += Int64.Parse(htTrnTypeAMT[key].ToString());
                    m_iTotalDebitQty += int.Parse(htTrnTypeCNT[key].ToString());
                }
                if (key == "601" || key == "602" || key == "606")
                {
                    m_i64TotalCreditAmt += Int64.Parse(htTrnTypeAMT[key].ToString());
                    m_iTotalCreditQty += int.Parse(htTrnTypeCNT[key].ToString());
                }
                if (key == "852")
                {
                    m_i64totalPayTransAmt += Int64.Parse(htTrnTypeAMT[key].ToString());
                    m_itotalPayTransCount += int.Parse(htTrnTypeCNT[key].ToString());
                }
                if (key == "486")
                {
                    m_i64netPayTransAmt486 += Int64.Parse(htTrnTypeAMT[key].ToString());
                    m_inetPayTransCount += int.Parse(htTrnTypeCNT[key].ToString());
                }
                if (key == "888")
                {
                    m_i64netPayTransAmt888 += Int64.Parse(htTrnTypeAMT[key].ToString());
                    m_inetPayTransCount += int.Parse(htTrnTypeCNT[key].ToString());
                }            
            }

            m_i64netPayTransAmt486 = (m_i64netPayTransAmt486 < 0) ? m_i64netPayTransAmt486 * -1 : m_i64netPayTransAmt486;
            m_i64netPayTransAmt888 = (m_i64netPayTransAmt888 < 0) ? m_i64netPayTransAmt888 * -1 : m_i64netPayTransAmt888;

            m_i64netPayTransAmt = m_i64netPayTransAmt486 - m_i64netPayTransAmt888;

            m_i64netPayTransAmt = (m_i64netPayTransAmt < 0) ? m_i64netPayTransAmt * -1 : m_i64netPayTransAmt;


            m_i64netTotalTransAmt = (m_i64TotalDebitAmt + m_i64netPayTransAmt) - (m_i64TotalCreditAmt + m_i64totalPayTransAmt);
            m_i64netTotalTransAmt = (m_i64netTotalTransAmt < 0) ? m_i64netTotalTransAmt * -1 : m_i64netTotalTransAmt;

            //m_i64netPayTransCount = (m_iTotalDebitQty + m_inetPayTransCount) - (m_iTotalCreditQty + m_itotalPayTransCount);
            //m_i64netPayTransCount = (m_i64netPayTransCount < 0) ? m_i64netPayTransCount * -1 : m_i64netPayTransCount;


            balaceReportLine = "TORRID".PadRight(20, ' ') +
                               DateTime.Today.ToString("yyyyMMdd") +
                               DateTime.Today.AddDays(-1).ToString("yyyyMMdd") +
                               m_i64GrossTransAmt.ToString().PadLeft(11, '0') +
                               m_iGrossTransQty.ToString().PadLeft(6, '0') +
                               m_i64TotalDebitAmt.ToString().PadLeft(11, '0') +
                               m_iTotalDebitQty.ToString().PadLeft(6, '0') +
                               m_i64TotalCreditAmt.ToString().PadLeft(11, '0') +
                               m_iTotalCreditQty.ToString().PadLeft(6, '0') +
                               m_i64totalPayTransAmt.ToString().PadLeft(11, '0') +
                               m_itotalPayTransCount.ToString().PadLeft(6,'0') +
                               m_i64netPayTransAmt.ToString().PadLeft(11,'0') +
                               m_inetPayTransCount.ToString().PadLeft(6,'0') +
                               m_i64netTotalTransAmt.ToString().PadLeft(11, '0') +
                               m_netTotalTransCount.ToString().PadLeft(6, '0') +
                               "                                                  ";

            WriteToBalanceFile(balaceReportLine, true);
            FileTemp.ArchiveFile(m_strCurrentDataPath, m_strArchivePath);
            FileTemp.DeleteOutdatedArchivedFile(m_strArchivePath, m_iDeleteArchiveFileBackDays);
            return true;
        }

        private bool CalculateTotalItems(ArrayList tempSameTransList, ref string finalLine, ref bool bGoodLine, ref TTranSubTotalInfo rTranSubTotalInfo, ref Hashtable htTrnTypeAMT, ref Hashtable htTrnTypeCNT)
        {            
            int iTotalItemQty = 0;
            string strClassandItemAmt = null;
            finalLine = "";
            bGoodLine = true;
            int aNewTranAMT = 0;
            string newTransAMTSymboltoNeg = "606"; //606
            string newTransAMTSymboltoPos = "206"; //206
            string newFormatCardEntryType = "";

            foreach (GetTxtInputInfo oneLineList in tempSameTransList)
            {
                if (oneLineList != null)
                {
                    newFormatCardEntryType = oneLineList.StrCardEntryType;
                    //if (newFormatCardEntryType == "0")
                    //{
                    //    newFormatCardEntryType = "2";
                    //}
                        iTotalItemQty = 0;
                        
                        aNewTranAMT = int.Parse(oneLineList.StrTranAmt);
                        if (aNewTranAMT < 0)
                        {
                            string strTranAMTOutputTran = oneLineList.StrTranAmt.Replace("-", "").Trim();
                            string strTAXTran = oneLineList.StrTranTax.Replace("-", "").Trim();

                            finalLine = oneLineList.StrRecordTypeBody + 
                                       //newTransAMoneLineListTSymboltoNeg + 
                                       oneLineList.StrTransType +
                                       oneLineList.StrSameTransLineOutput() + 
                                       strTranAMTOutputTran.PadLeft(8, '0') + 
                                       strTAXTran.PadLeft(7, '0') + 
                                       oneLineList.StrSameTransLine() + 
                                       newFormatCardEntryType + 
                                       oneLineList.StrSameTransLineAfterCardEntry() + 
                                       iTotalItemQty.ToString().PadLeft(3, '0');
                            
                            rTranSubTotalInfo.ITotalCreditQty = 1;
                            rTranSubTotalInfo.I64TotalCreditAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                        }
                        else
                        {
                            finalLine = oneLineList.StrRecordTypeBody + 
                                       //newTransAMTSymboltoPos + 
                                       oneLineList.StrTransType +
                                       oneLineList.StrSameTransLineOutput() + 
                                       oneLineList.StrTranAmt.PadLeft(8, '0') + 
                                       oneLineList.StrTranTax.PadLeft(7, '0') + 
                                       oneLineList.StrSameTransLine() + 
                                       newFormatCardEntryType + 
                                       oneLineList.StrSameTransLineAfterCardEntry() + 
                                       iTotalItemQty.ToString().PadLeft(3, '0');
                            rTranSubTotalInfo.ITotalDebitQty = 1;
                            rTranSubTotalInfo.I64TotalDebitAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                        }

                        //Set the trntype variables(Amt, Cnt)
                        if (htTrnTypeAMT.ContainsKey(oneLineList.StrTransType) == false && htTrnTypeCNT.ContainsKey(oneLineList.StrTransType) == false)
                        {
                            htTrnTypeAMT.Add(oneLineList.StrTransType, int.Parse(oneLineList.StrTranAmt.ToString().Replace("-","")));
                            htTrnTypeCNT.Add(oneLineList.StrTransType, 1);
                        }
                        else
                        {
                            htTrnTypeAMT[oneLineList.StrTransType] = int.Parse(htTrnTypeAMT[oneLineList.StrTransType].ToString()) + int.Parse(oneLineList.StrTranAmt.ToString().Replace("-", ""));
                            htTrnTypeCNT[oneLineList.StrTransType] = int.Parse(htTrnTypeCNT[oneLineList.StrTransType].ToString()) + 1;
                        }

                    rTranSubTotalInfo.StoreNum = int.Parse(oneLineList.StrStoreNum);
                    rTranSubTotalInfo.I64SingleTransAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                    rTranSubTotalInfo.StrTransDate = oneLineList.StrTranDate();
                    rTranSubTotalInfo.BGoodLineTwoKeys = bGoodLine;
                }

            }
            return true;
        }
                
        public void WriteToFile(string data, bool IsGood)
        {
            string strfilePath = null;
            FileHelper FileTemp = new FileHelper(m_errorHandler);
            if (IsGood)
            {
                strfilePath = FileHelper.BuildOutputFileName();
            }
            FileTemp.WriteToFile(data, strfilePath);
        }
        public void WriteToBalanceFile(string data, bool IsGood)
        {
            string strfilePathBalance = null;
            FileHelper FileTemp = new FileHelper(m_errorHandler);
            if (IsGood)
            {
                strfilePathBalance = FileHelper.BuildOutputBalanceFileName();
            }
            FileTemp.WriteToFile(data, strfilePathBalance);
        }

    }
}
