using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;

namespace TCCSettlementOutput
{
    class GetTlogDateList
    {

        HTError.IHTError m_errorHandler;
        public GetTlogDateList(HTError.IHTError RHtError)
        {
            m_errorHandler = RHtError;
        }

        private static List<string> processdays = new List<string>();

        public static List<string> Processdays
        {
            get { return processdays; }
            set { processdays = value; }
        }

        public List<string> Start()
        {
            string fmatTraceDay;

            int traceDays = int.Parse(ConfigurationManager.AppSettings["txtTlogTraceBackDays"].ToString());

            try
            {
                for (int idays = 0; idays <= traceDays; idays++)
                {
                    if (idays == 0)
                    {
                        DateTime tDays = DateTime.Today;
                        fmatTraceDay = tDays.ToString("yyyyMMdd");
                    }
                    else
                    {
                        //get previous days
                        DateTime tDays = DateTime.Today.AddDays(-1 * idays);
                        fmatTraceDay = tDays.ToString("yyyyMMdd");
                    }

                    //construct todays date from today back to 5 days, 
                    //starting from today for some Tlog file uploading late after midnight

                    processdays.Add(fmatTraceDay);
                }
                
            }
            catch (Exception ex)
            {
                m_errorHandler.LogDebug(ex.Message);
            }
            return processdays;
        }
    }
}
