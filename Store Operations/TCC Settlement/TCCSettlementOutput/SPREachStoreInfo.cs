using System;
using System.Collections.Generic;
using System.Text;

namespace TCCSettlementOutput
{
    class SPREachStoreInfo
    {
        private string m_strTrnDateEachStore = null;

        public string StrTrnDateEachStore
        {
            get { return m_strTrnDateEachStore; }
            set { m_strTrnDateEachStore = value; }
        }
        private string m_strStoreNumEachStore = null;

        public string StrStoreNumEachStore
        {
            get { return m_strStoreNumEachStore; }
            set { m_strStoreNumEachStore = value; }
        }
        private Int64 m_iAMTEachStore = 0;

        public Int64 IAMTEachStore
        {
            get { return m_iAMTEachStore; }
            set { m_iAMTEachStore = value; }
        }
        private List<SPRCardTypeSumEachStore> listEachStoreInfo = new List<SPRCardTypeSumEachStore>();

        public List<SPRCardTypeSumEachStore> ListEachStoreInfo
        {
            get { return listEachStoreInfo; }
            set { listEachStoreInfo = value; }
        }

        
    }
}
