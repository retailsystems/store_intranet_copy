using System;
using System.Collections.Generic;
using System.Text;

namespace TCCSettlementOutput
{
    class TTranSubTotalInfo
    {
        private int m_iTotalDebitQty = 0;
        private Int64 m_i64TotalDebitAmt = 0;
        private int m_iTotalCreditQty = 0;
        private Int64 m_i64TotalCreditAmt = 0;        
        private Int64 m_i64SingleTransAmt = 0;
        private int m_storeNum = 0;
        private string m_strTransDate = null;
        private bool m_bGoodLineTwoKeys;
        //Added the below two variable for TCC payments credit - By Vnaidu
        private int m_iTotalTCCpmntCreditQty = 0;
        private Int64 m_i64TotalTCCpmntCreditAmt = 0;

        public bool BGoodLineTwoKeys
        {
            get { return m_bGoodLineTwoKeys; }
            set { m_bGoodLineTwoKeys = value; }
        }

        public string StrTransDate
        {
            get { return m_strTransDate; }
            set { m_strTransDate = value; }
        }

        public int StoreNum
        {
            get { return m_storeNum; }
            set { m_storeNum = value; }
        }

        public Int64 I64SingleTransAmt
        {
            get { return m_i64SingleTransAmt; }
            set { m_i64SingleTransAmt = value; }
        }

        public Int64 I64TotalCreditAmt
        {
            get { return m_i64TotalCreditAmt; }
            set { m_i64TotalCreditAmt = value; }
        }
        public int ITotalCreditQty
        {
            get { return m_iTotalCreditQty; }
            set { m_iTotalCreditQty = value; }
        }
        public Int64 I64TotalDebitAmt
        {
            get { return m_i64TotalDebitAmt; }
            set { m_i64TotalDebitAmt = value; }
        }
        public int ITotalDebitQty
        {
            get { return m_iTotalDebitQty; }
            set { m_iTotalDebitQty = value; }
        }
        //Adding it for TCC Payment amounts - By vnaidu
        public Int64 I64TotalTCCPmntCreditAmt
        {
            get { return m_i64TotalTCCpmntCreditAmt; }
            set { m_i64TotalTCCpmntCreditAmt = value; }
        }
        public int ITotalTCCPmntCreditQty
        {
            get { return m_iTotalTCCpmntCreditQty; }
            set { m_iTotalTCCpmntCreditQty = value; }
        }
    }
}
