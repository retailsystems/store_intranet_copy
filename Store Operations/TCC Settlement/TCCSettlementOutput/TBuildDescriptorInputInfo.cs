using System;
using System.Collections.Generic;
using System.Text;

namespace TCCSettlementOutput
{
    class TBuildDescriptorInputInfo
    {
        private string m_strClassCD;
        private string m_strDes;
        private string m_strOriginCD;
               
        public TBuildDescriptorInputInfo()
        {
            this.m_strOriginCD = "000";
        }
        
        public string StrClassCD
        {
            get { return m_strClassCD; }
            set { m_strClassCD = value; }
        }
        
        public string StrDes
        {
            get { return m_strDes; }
            set { m_strDes = value; }
        }

        public string StrOriginCD
        {
            get { return m_strOriginCD; }
            set { m_strOriginCD = value; }
        }
    }
}
