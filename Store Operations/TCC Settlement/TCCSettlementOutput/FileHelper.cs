using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;
using System.Configuration;

namespace TCCSettlementOutput
{
    class FileHelper
    {
        //input master file path
        private string m_CurrentDataPath = null;
        private DateTime dtBeginDateTime;
        HTError.IHTError m_errorHandler;

        public FileHelper(HTError.IHTError RHtError)
        {
            m_errorHandler = RHtError;
        }
        public static string BuildTxtFilePath()
        {
            string dirPrefixInput = ConfigurationManager.AppSettings["TCCInputPrefix"].ToString();

            string today = DateTime.Today.ToString("yyyyMMdd");
            
            return ConfigurationManager.AppSettings["txtInputFilePath"].ToString() + dirPrefixInput + today + ".txt";
        }
        public static string GetDescriptorTxtFilePath()
        {
            string strTCCInputDescriptorFileName = ConfigurationManager.AppSettings["TCCInputDescriptorFileName"].ToString();

            string today = DateTime.Today.ToString("yyyyMMdd");
          
            return ConfigurationManager.AppSettings["txtInputFilePath"].ToString() + strTCCInputDescriptorFileName + today + ".txt";
        }
        public static string BuildOutputDescriptorFileName()
        {
            string dirPrefix = ConfigurationManager.AppSettings["TDOutputDescriptorReportName"].ToString();
            return ConfigurationManager.AppSettings["txtOutputFilePath"].ToString() + dirPrefix + ".txt";
        }
        public static string BuildOutputFileName()
        {
            string dirPrefix = ConfigurationManager.AppSettings["TDOutputRetailReportName"].ToString();
            return ConfigurationManager.AppSettings["txtOutputFilePath"].ToString() + dirPrefix + ".txt";
        }
      
        public static string GetOutputBadLine()
        {
            string dirPrefix = ConfigurationManager.AppSettings["TDBadPrefix"].ToString();
            //string dBadFileTimeStamp = "_" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "");
            return ConfigurationManager.AppSettings["txtNoFoundTDCCInfoPath"].ToString() + dirPrefix + ".txt";
        }
        //public static string GetOutputBadLine()
        //{
        //    string dirPrefix = ConfigurationManager.AppSettings["TDBadPrefix"].ToString();
        //    string dBadFileTimeStamp = "_" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "");
        //    return ConfigurationManager.AppSettings["txtNoFoundTDCCInfoPath"].ToString() + dirPrefix + dBadFileTimeStamp + ".txt";
        //}
        public static string BuildOutputBalanceFileName()
        {
            string dirPrefix = ConfigurationManager.AppSettings["TDOutputBalanceReportName"].ToString();
            return ConfigurationManager.AppSettings["txtOutputFilePath"].ToString() + dirPrefix + ".txt";
            //return ConfigurationManager.AppSettings["txtOutputFilePath"].ToString() + dirPrefix + ".txt";
        }
        public static string BuildOutputStoreSumFileName()
        {
            string dirPrefix = ConfigurationManager.AppSettings["TDOutputStoreSumReportName"].ToString();
            return ConfigurationManager.AppSettings["txtOutputStoreSum4SalesAuditPath"].ToString() + dirPrefix + ".csv";
            //return ConfigurationManager.AppSettings["txtOutputFilePath"].ToString() + dirPrefix + ".txt";
        }
        public void ArchiveFile(string m_CurrentDataPath, string m_ArchivePath)
        {
            try
            {
                  
                if (!m_CurrentDataPath.EndsWith("\\"))
                {
                    m_CurrentDataPath += "\\";
                }
                if (!m_ArchivePath.EndsWith("\\"))
                {
                    m_ArchivePath += "\\";
                }

                DirectoryInfo sourcePath = new DirectoryInfo(m_CurrentDataPath);

                DirectoryInfo destinationPath = new DirectoryInfo(m_ArchivePath);

                if (!sourcePath.Exists)
                {
                    m_errorHandler.LogWarning("Source Path: " + m_CurrentDataPath + "does not exist.");
                }

                if (!destinationPath.Exists)
                {
                    Directory.CreateDirectory(m_ArchivePath);
                }

                //Gets file in current directory

                foreach (FileInfo fiInfo in sourcePath.GetFiles())
                {
                    //Rename file name 
                    string fileDateTime = "_" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "");
                    string newName = fiInfo.Name.Insert(fiInfo.Name.Length - fiInfo.Extension.Length, fileDateTime);

                    //Copy File to Archive folder
                    File.Copy(m_CurrentDataPath + "\\" + fiInfo.Name, m_ArchivePath + "\\" + newName);

                }
            }
            catch (Exception ex)
            {
                //log error
                m_errorHandler.LogError(ex, "Error is from Archiving Files.");
                m_errorHandler.LogNotify("Error occurs once archive files from source folder: " + m_CurrentDataPath + " to destination folder: " + m_ArchivePath +".");
            }
        }


        public void DeleteOutdatedArchivedFile(string strArchivePath,int iDeleteArchiveFileBackDays)
        {
            try
            {
                
                if (!strArchivePath.EndsWith("\\"))
                {
                    strArchivePath += "\\";
                }

                DirectoryInfo archivedFilePath = new DirectoryInfo(strArchivePath);

                if (!archivedFilePath.Exists)
                {
                    Directory.CreateDirectory(strArchivePath);
                }

                //Gets file in current directory

                foreach (FileInfo fiInfo in archivedFilePath.GetFiles())
                {
                    
                    dtBeginDateTime = DateTime.Now.AddDays(-1 * iDeleteArchiveFileBackDays);

                    if ((fiInfo.LastWriteTime.CompareTo(dtBeginDateTime) < 0))
                    {
                        //delete File in Archive folder for those files older than a given period time 
                        fiInfo.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                //log error
                m_errorHandler.LogError(ex, "Error from deleting Outdated Archived File.");
                m_errorHandler.LogNotify("Error occurs once deleting outdated archive files from folder: " + strArchivePath + ".");
            }
        }
        public void ClearFileInCurrentFolder(string filename)
        {
            try
            {
                m_CurrentDataPath = ConfigurationManager.AppSettings["txtOutputFilePath"].ToString();

                if (!m_CurrentDataPath.EndsWith("\\"))
                {
                    m_CurrentDataPath += "\\";
                }
                string fullFileName = m_CurrentDataPath + filename;
                DirectoryInfo currentFilePath = new DirectoryInfo(m_CurrentDataPath);
                try
                {
                    if (currentFilePath.Exists)
                    {
                        FileInfo fiInfo = new FileInfo(fullFileName);
                        if (fiInfo != null)
                        {
                            fiInfo.Delete();
                        }
                    }
                    else
                    {
                        m_errorHandler.LogInformation("To be cleared files do not exist under the directory of " + currentFilePath.FullName);
                    }
                }
                catch
                {
                    m_errorHandler.LogInformation("Unable to find files under the directory: " + currentFilePath.FullName);
                    throw;
                }
            }
            //catch (Exception ex)
            catch
            {
                //log error
                m_errorHandler.LogMessage(" Task failed to clear File In Current Output Folder ", HTError.LogLevel.Error);
                
            }
        }
        public void ClearFileInCurrentStoreSumFolder(string filename)
        {
            try
            {
                m_CurrentDataPath = ConfigurationManager.AppSettings["txtOutputStoreSum4SalesAuditPath"].ToString();

                if (!m_CurrentDataPath.EndsWith("\\"))
                {
                    m_CurrentDataPath += "\\";
                }
                string fullFileName = m_CurrentDataPath + filename;
                DirectoryInfo currentFilePath = new DirectoryInfo(m_CurrentDataPath);
                try
                {
                    if (currentFilePath.Exists)
                    {
                        FileInfo fiInfo = new FileInfo(fullFileName);
                        if (fiInfo != null)
                        {
                            fiInfo.Delete();
                        }
                    }
                    else
                    {
                        m_errorHandler.LogInformation("To be cleared files do not exist under the directory of " + currentFilePath.FullName);
                    }
                }
                catch
                {
                    m_errorHandler.LogInformation("Unable to find store sum file under the directory: " + currentFilePath.FullName);
                    throw;
                }

            }
            catch
            {
                //log error
                m_errorHandler.LogMessage(" Task failed to clear store sum report In Current Output Folder ", HTError.LogLevel.Error);
                
            }
        }
        public void ClearFileInCurrentNoFoundFolder(string filename)
        {
            try
            {
                m_CurrentDataPath = ConfigurationManager.AppSettings["txtNoFoundTDCCInfoPath"].ToString();

                if (!m_CurrentDataPath.EndsWith("\\"))
                {
                    m_CurrentDataPath += "\\";
                }
                string fullFileName = m_CurrentDataPath + filename;
                DirectoryInfo currentFilePath = new DirectoryInfo(m_CurrentDataPath);
                try
                {
                    if (currentFilePath.Exists)
                    {
                        FileInfo fiInfo = new FileInfo(fullFileName);
                        if (fiInfo != null)
                        {
                            fiInfo.Delete();
                        }
                    }
                    else
                    {
                        m_errorHandler.LogInformation("To be cleared files do not exist under the directory of " + currentFilePath.FullName);
                    }
                }
                catch
                {
                    m_errorHandler.LogInformation("Unable to find Not Found TCC file under the directory: " + currentFilePath.FullName);
                    throw;
                }

            }
            catch 
            {
                //log error
                m_errorHandler.LogMessage(" Task failed to clear Not Found TCC Report In Current Folder ", HTError.LogLevel.Error);

            }
        }
        public void WriteToFile(string strData, string filePath)
        {
            try
            {
                TextWriter stringWriter = new StringWriter();
                //using (TextWriter streamWriter = new StreamWriter(filePath, true))
                using (TextWriter streamWriter = new StreamWriter(filePath, true, Encoding.ASCII))
                {
                    streamWriter.WriteLine(strData);
                } 
              
            }
            catch (Exception ex)
            {
               
                m_errorHandler.LogError(ex);
                m_errorHandler.LogMessage("Write to file task failed. ", HTError.LogLevel.Error);
            }
        }

        //public List<FileInfo> ProcessDir(string sourceDir, ref List<string> listStrProcessDays)
        public List<FileInfo> ProcessDir(string sourceDir)
        {

            // List of files found in the directory 
            List<FileInfo> fi = new List<FileInfo>();
            string[] fileEntries = Directory.GetFiles(sourceDir);

            // Recurse into subdirectories of this directory.
            try
            {
                string[] subdirEntries = Directory.GetDirectories(sourceDir);
                foreach (string subdir in subdirEntries)
                {
                    //foreach (string processDay in processDays)
                    //{
                    //    if (subdir.Contains(processDay))
                    //        break;

                    //}
                    
                    DirectoryInfo difile = new DirectoryInfo(subdir);

                    try
                    {
                        if (difile.Exists)
                        {

                            FileInfo[] subdirFiles = difile.GetFiles("*.*");
                            foreach (FileInfo txtfile in subdirFiles)
                            {
                                string fullFilePath = txtfile.DirectoryName + "\\" + txtfile.Name;
                               
                                //update comment out: late coming tlog files can be added to fi list
                                
                                //string dt = txtfile.LastWriteTime.ToString("yyyyMMdd");
                                //  if (GetTlogDateList.Processdays.Contains(dt))
                                //{
                                string dt = txtfile.LastWriteTime.ToString("yyyyMMdd");
                                  if (GetTlogDateList.Processdays.Contains(dt))
                                {
                                    fi.Add(txtfile);
                                }
                                //}
                            }
                        }
                        else
                        {
                            m_errorHandler.LogInformation("The Tlog files do not exist under the directory of " + difile.FullName);

                        }
                    }
                    catch
                    {
                        m_errorHandler.LogInformation("Unable to find the subdirectory (by Tlog day folder)under the directory of:" + sourceDir);
                        throw;
                    }
                }
            }

            catch (Exception ex)
            {
                m_errorHandler.LogNotify(ex.Message);
            }
            return fi;
        }
    }
}
