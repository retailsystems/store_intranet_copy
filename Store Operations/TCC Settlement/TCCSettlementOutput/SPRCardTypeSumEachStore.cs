using System;
using System.Collections.Generic;
using System.Text;

namespace TCCSettlementOutput
{
    class SPRCardTypeSumEachStore
    {
        private string m_strCardTypeEachStore = null;

        public string StrCardTypeEachStore
        {
            get { return m_strCardTypeEachStore; }
            set { m_strCardTypeEachStore = value; }
        }
        private string m_strStoreNumEachStore = null;

        public string StrStoreNumEachStore
        {
            get { return m_strStoreNumEachStore; }
            set { m_strStoreNumEachStore = value; }
        }
        private string m_strTrnDateEachStore = null;

        public string StrTrnDateEachStore
        {
            get { return m_strTrnDateEachStore; }
            set { m_strTrnDateEachStore = value; }
        }
        private Int64 m_iAMTbySumCardT = 0;

        public Int64 IAMTbySumCardT
        {
            get { return m_iAMTbySumCardT; }
            set { m_iAMTbySumCardT = value; }
        }

        private List<string> listCardTypeInfo = new List<string>();

        public List<string> ListCardTypeInfo
        {
            get { return listCardTypeInfo; }
            set { listCardTypeInfo = value; }
        }

    }
}
