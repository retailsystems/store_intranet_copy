using System;
using System.Collections.Generic;
using System.Data;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using HTError;
using System.Net.Mail;

namespace TCCSettlementOutput
{
    class TTorrid : ICompany
    {
        //protected HTError.IHTError m_errorHandler;
        public HTError.IHTError m_errorHandler;
        private List<string> m_listStrProcessDays = null;
        private ArrayList m_listInputText = null;
        private ArrayList m_listDescriptorInputText = null;
        private Hashtable m_hashTlog = new Hashtable();

        //m_errorHandler = Program.GetLog();

        public TTorrid()
        {
            GetLog();
        }

        public bool LoadDescriptorTxtFile()
        {
            // Build the keys for the descriptor file
            TParseDescriptorTxtFile parseDecriptorTxtFile = new TParseDescriptorTxtFile(m_errorHandler);
            m_listDescriptorInputText = parseDecriptorTxtFile.Start();

            if (m_listDescriptorInputText.Count > 0)
            {
                WriteDescriptorFile();
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool LoadMasterTxtFile()
        {
            // Build the keys for the master file
            LoadInputTxtFile loadInputTxtFile = new LoadInputTxtFile(m_errorHandler);
            m_listInputText = loadInputTxtFile.Start();

            if (m_listInputText.Count > 0)
                return true;
            else
            {
                WriteEmptyBalanceFile();
                return false;
            }
        }

        public bool ListProcessDays()
        {
            //Build the List for 5 days
            GetTlogDateList getList = new GetTlogDateList(m_errorHandler);
            m_listStrProcessDays = getList.Start();
            if (m_listStrProcessDays.Count > 0)
                return true;
            else
                return false;
        }

        public bool LoadTlogFile()
        {
            //Build hashtable for TLog
            string sourceDir = ConfigurationManager.AppSettings["tlogfilePath"].ToString();
            BuildHashTable bh = new BuildHashTable(m_errorHandler);
            FileHelper FileTemp = new FileHelper(m_errorHandler);
            //bh.TLogToProcess = FileTemp.ProcessDir(sourceDir,ref List<string> listStrProcessDays);
            bh.TLogToProcess = FileTemp.ProcessDir(sourceDir);
            m_hashTlog = bh.Start();
            if (m_hashTlog.Count > 0)
                return true;
            else
                return false;
        }


        public bool SearchFullCC()
        {
            if (m_listInputText.Count > 0 && m_hashTlog != null)
            {
                return Search(m_listInputText, m_hashTlog);
            }
            else
            {
                return false;
            }
        }

        public void GetLog()
        {
            string fileName = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.StartupPath) + "\\" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".log";
            String ProductionFilePath = ConfigurationManager.AppSettings["ProductionFilePath"].ToString();
            String DebugFilePath = ConfigurationManager.AppSettings["DebugFilePath"].ToString();
            String EmailServer = ConfigurationManager.AppSettings["EmailServer"].ToString();
            String EmailSender = ConfigurationManager.AppSettings["EmailSender"].ToString();
            String NotifyRecipients = ConfigurationManager.AppSettings["NotifyRecipients"].ToString();

            m_errorHandler = new HTError.ErrorHandler(ProductionFilePath, DebugFilePath, EmailServer, EmailSender, NotifyRecipients, HTError.LogLevel.Debug);

            m_errorHandler.ErrorEmailPriority = System.Net.Mail.MailPriority.High;
            m_errorHandler.SendMailOnError = true;
            m_errorHandler.SendMailOnWarning = true;
        }
        private bool Search(ArrayList listInputText, Hashtable tLog)
        {

            foreach (GetTxtInputInfo oneLine in listInputText)
            {
                if (oneLine != null)
                {
                    string key1StoreDate = oneLine.StrComKeyStoreDate();
                    string key2TranumCC4 = oneLine.StrComKeyTranumCC4num();
                    string strKey1StoreDate = key1StoreDate == String.Empty ? String.Empty : oneLine.StrComKeyStoreDate();
                    string strkey2TranumCC4 = key2TranumCC4 == String.Empty ? String.Empty : oneLine.StrComKeyTranumCC4num();

                    Hashtable h2_dict = null;

                    if (strKey1StoreDate.Length > 0)
                    {
                        if (strkey2TranumCC4.Length > 0)
                        {
                            if (tLog.ContainsKey(strKey1StoreDate))
                            {
                                h2_dict = new Hashtable();
                                h2_dict = (Hashtable)tLog[strKey1StoreDate];
                                if (h2_dict.ContainsKey(strkey2TranumCC4))
                                {
                                    TDLogCCInfo tdLogCCInfo = new TDLogCCInfo();
                                    tdLogCCInfo = (TDLogCCInfo)h2_dict[strkey2TranumCC4];

                                    oneLine.SetFullCredit(tdLogCCInfo.FullCCNumTDCCInfo());
                                    //oneLine.SetCardEntry(tdLogCCInfo.StrCardEntry());
                                    oneLine.StrCardEntryType = tdLogCCInfo.StrCardEntry();
                                    oneLine.BKey2Tranumlast4CCFound = true;
                                    oneLine.BKey1StoreDateFound = true;

                                }
                                else
                                {
                                    oneLine.BKey2Tranumlast4CCFound = false;
                                    //m_errorHandler.SendMailOnError = true;

                                    //m_errorHandler.LogNotify("Can not find matched Credit Card Number line:" + oneLine.WholeLine +
                                    //" from Tlog Files, which has Trans number:(" +
                                    //oneLine.StrTranNum + "); Last four digit Credit Card number:(" + oneLine.LastFourCCNum() + "); store number:(" +
                                    //oneLine.StrStoreNum + "); Trans date:(" + oneLine.StrTranDate() + ").");

                                }
                            }
                            else
                            {
                                oneLine.BKey1StoreDateFound = false;
                                //m_errorHandler.SendMailOnError = true;

                                //m_errorHandler.LogNotify("Can not find matched Credit Card Number line:" + oneLine.WholeLine +
                                //" from Tlog Files, which has store number:(" + oneLine.StrStoreNum + "); Trans date:(" + oneLine.StrTranDate() +
                                //"); Trans number:(" + oneLine.StrTranNum + "); Last four digit Credit Card number:(" + oneLine.LastFourCCNum() + ").");

                            }

                        }
                    }
                }
            }
            return true;
        }
        public bool WriteToDetailReport()
        {
            if (m_listInputText.Count == 0)
                return false;
            string strStoreNumWrite = null;
            string strTransDateWrite = null;
            string strTransNumWrite = null;
            string strLast4CreditCardWrite = null;
            //string strStoreSum = null;
            ArrayList tempTransList = new ArrayList();
            //ArrayList tempStoreList = new ArrayList();
            string finalLine = null;
            string headerLine = "";
            string trailerLine = "";
            //string headerLineToWrite = null;
            bool bFound = false;
            int m_iTotalDebitQty = 0;
            Int64 m_i64TotalDebitAmt = 0;
            int m_iTotalCreditQty = 0;
            Int64 m_i64TotalCreditAmt = 0;
            //Adding the below variables for tcc pmnt count - by vnaidu
            int m_iTotalTCCpmntCreditQty = 0;
            Int64 m_i64TotalTCCpmntCreditAmt = 0;
            int m_iTotalSumOfCreditQty = 0; //combine both 602 + 852 records
            Int64 m_i64SumOfCreditAmt = 0; //combine both 602 + 852 records


            //char szDlimited = ',';
            string balaceReportLine = "";
            string m_totalPayTransAmt = "00000000000";
            string m_totalPayTransCount = "000000";
            string m_netPayTransAmt = "00000000000";
            string m_netPayTransCount = "000000";
            string m_strNetTotalTransAmt = null;
            string m_strNetTotalTransCount = null;
            string m_strTotalProSalesAmt = "0000000000000";
            string m_strTotalProSalesCount = "000000000000";
            string m_strCurrentDataPath = ConfigurationManager.AppSettings["txtOutputFilePath"].ToString();
            string m_strArchivePath = ConfigurationManager.AppSettings["txtOutputArchiveFilePath"].ToString();
            int m_iDeleteArchiveFileBackDays = int.Parse(ConfigurationManager.AppSettings["txtDeleteArchiveOutputFileBackDays"].ToString());

            FileHelper FileTemp = new FileHelper(m_errorHandler);
            string m_CurrentDetailReportName = ConfigurationManager.AppSettings["TDOutputRetailReportName"].ToString() + ".txt";
            FileTemp.ClearFileInCurrentFolder(m_CurrentDetailReportName);

            string m_CurrentBalanceReportName = ConfigurationManager.AppSettings["TDOutputBalanceReportName"].ToString() + ".txt";
            FileTemp.ClearFileInCurrentFolder(m_CurrentBalanceReportName);

            string m_CurrentNoFoundReportName = ConfigurationManager.AppSettings["TDBadPrefix"].ToString() + ".txt";
            string m_strCurrentNofoundReportPath = ConfigurationManager.AppSettings["txtNoFoundTDCCInfoPath"].ToString();
            string m_strNoFoundReportFullPathName = m_strCurrentNofoundReportPath + m_CurrentNoFoundReportName;
            if (File.Exists(m_strNoFoundReportFullPathName))
            {
                FileTemp.ClearFileInCurrentNoFoundFolder(m_CurrentNoFoundReportName);
            }
            
            string m_strArchiveNofoundReportPath = ConfigurationManager.AppSettings["txtNoFoundTDCCInfoArchivePath"].ToString();
            int m_iDeleteArchiveNofoundFileBackDays = int.Parse(ConfigurationManager.AppSettings["txtDeleteArchiveNotFoundFileBackDays"].ToString());
            //print header
            headerLine = "1" + "TORRID".PadRight(20, ' ') + DateTime.Today.ToString("yyyyMMdd") +
                         "                                     ";
            WriteToFile(headerLine, true);
            //print body
            foreach (GetTxtInputInfo oneLine in m_listInputText)
            {
                if (oneLine != null)
                {
                    bFound = false;
                    if (strStoreNumWrite == oneLine.StrStoreNum)
                    {
                        if (strTransDateWrite == oneLine.StrTranDate())
                        {
                            if (strTransNumWrite == oneLine.StrTranNum)
                            {
                                //change for split tender
                                if (strLast4CreditCardWrite == oneLine.LastFourCCNum())
                                { bFound = true; }
                            }
                        }
                    }
                }
                if (bFound == true)
                    tempTransList.Add(oneLine);
                else
                {
                    strStoreNumWrite = oneLine.StrStoreNum;
                    strTransDateWrite = oneLine.StrTranDate();
                    strTransNumWrite = oneLine.StrTranNum;
                    strLast4CreditCardWrite = oneLine.LastFourCCNum();
                    //change for split tender
                    if (tempTransList.Count == 0)
                    {
                        tempTransList.Add(oneLine);
                    }
                    else
                    {
                        bool bGoodline = false;
                        TTranSubTotalInfo rTransSubTotalInfo = new TTranSubTotalInfo();
                        bool bRet = CalculateTotalItems(tempTransList, ref finalLine, ref bGoodline, ref rTransSubTotalInfo);
                        if (bGoodline == true)
                        {
                            m_iTotalDebitQty += rTransSubTotalInfo.ITotalDebitQty;
                            m_i64TotalDebitAmt += rTransSubTotalInfo.I64TotalDebitAmt;
                            m_iTotalCreditQty += rTransSubTotalInfo.ITotalCreditQty;
                            m_i64TotalCreditAmt += rTransSubTotalInfo.I64TotalCreditAmt;
                            //Adding the TCC Pmnt count - by vnaidu
                            m_iTotalTCCpmntCreditQty += rTransSubTotalInfo.ITotalTCCPmntCreditQty;
                            m_i64TotalTCCpmntCreditAmt += rTransSubTotalInfo.I64TotalTCCPmntCreditAmt;
                        }
                        WriteToFile(finalLine, bGoodline);

                        finalLine = null;
                        tempTransList.Clear();
                        tempTransList.Add(oneLine);
                    }

                }
            }//end loop
            if (tempTransList.Count != 0)
            {
                bool bGoodline = false;
                TTranSubTotalInfo rTransSubTotalInfo = new TTranSubTotalInfo();
                bool bRet = CalculateTotalItems(tempTransList, ref finalLine, ref bGoodline, ref rTransSubTotalInfo);
                if (bGoodline == true)
                {
                    m_iTotalDebitQty += rTransSubTotalInfo.ITotalDebitQty;
                    m_i64TotalDebitAmt += rTransSubTotalInfo.I64TotalDebitAmt;
                    m_iTotalCreditQty += rTransSubTotalInfo.ITotalCreditQty;
                    m_i64TotalCreditAmt += rTransSubTotalInfo.I64TotalCreditAmt;
                    //Adding the TCC Pmnt count - by vnaidu
                    m_iTotalTCCpmntCreditQty += rTransSubTotalInfo.ITotalTCCPmntCreditQty;
                    m_i64TotalTCCpmntCreditAmt += rTransSubTotalInfo.I64TotalTCCPmntCreditAmt;
                }

                WriteToFile(finalLine, bGoodline);
            }
            //TODO sum
            string i64TotalCreditAmt = m_i64TotalCreditAmt.ToString().Replace("-", "").Trim();            
            string i64TotalTCCpmntCreditAmt = m_i64TotalTCCpmntCreditAmt.ToString().Replace("-", "").Trim(); //added by vnaidu

            //Add 852 + 602 records - by vnaidu
            m_iTotalSumOfCreditQty = m_iTotalCreditQty + m_iTotalTCCpmntCreditQty;
            m_i64SumOfCreditAmt = (m_i64TotalCreditAmt * -1) + (m_i64TotalTCCpmntCreditAmt * -1);

            
            //Comment the old trailer without TCC payments
            /*
            trailerLine = "9" + DateTime.Today.ToString("yyyyMMdd") +
                          "                    " +
                          m_i64TotalDebitAmt.ToString().PadLeft(13, '0') +
                          m_iTotalDebitQty.ToString().PadLeft(12, '0') +
                          i64TotalCreditAmt.PadLeft(13, '0') +
                          m_iTotalCreditQty.ToString().PadLeft(12, '0') +
                          m_strTotalProSalesAmt +
                          m_strTotalProSalesCount +
                          "0000" + "                                          ";
            */

            trailerLine = "9" + DateTime.Today.ToString("yyyyMMdd") +
                          "                    " +
                          m_i64TotalDebitAmt.ToString().PadLeft(13, '0') +
                          m_iTotalDebitQty.ToString().PadLeft(12, '0') +
                          m_i64SumOfCreditAmt.ToString().PadLeft(13, '0') +
                          m_iTotalSumOfCreditQty.ToString().PadLeft(12, '0') +
                          m_strTotalProSalesAmt +
                          m_strTotalProSalesCount +
                          "0000" + "                                          ";
            WriteToFile(trailerLine, true);

            // TODO wirte balance report
            int m_iGrossTransQty = m_iTotalDebitQty + m_iTotalCreditQty;
            Int64 m_i64GrossTransAmt = m_i64TotalDebitAmt - m_i64TotalCreditAmt;

            int m_iNetTransQty = m_iTotalDebitQty - m_iTotalCreditQty;
            //Calculate Net total tran amount(202 - (602 + 852)) - added 852 - by vnaidu
            Int64 m_i64NetTransAmt = m_i64TotalDebitAmt + m_i64TotalCreditAmt + m_i64TotalTCCpmntCreditAmt;

            


            if (m_i64NetTransAmt < 0)
            {
                m_strNetTotalTransAmt = m_i64NetTransAmt.ToString().Replace("-", "").Trim();
            }
            else
            {
                m_strNetTotalTransAmt = m_i64NetTransAmt.ToString();
            }
            if (m_iNetTransQty < 0)
            {
                m_strNetTotalTransCount = m_iNetTransQty.ToString().Replace("-", "").Trim();
            }
            else
            {
                m_strNetTotalTransCount = m_iNetTransQty.ToString();
            }
            //Evaluate total TCC payment amount - by vnaidu
            if(m_iTotalTCCpmntCreditQty > 0)
            {
              m_totalPayTransAmt = i64TotalTCCpmntCreditAmt.PadLeft(11,'0');
              m_totalPayTransCount = m_iTotalTCCpmntCreditQty.ToString().PadLeft(6, '0');
            }
            

            balaceReportLine = "TORRID".PadRight(20, ' ') +
                               DateTime.Today.ToString("yyyyMMdd") +
                               DateTime.Today.AddDays(-1).ToString("yyyyMMdd") +
                               m_i64GrossTransAmt.ToString().PadLeft(11, '0') +
                               m_iGrossTransQty.ToString().PadLeft(6, '0') +
                               m_i64TotalDebitAmt.ToString().PadLeft(11, '0') +
                               m_iTotalDebitQty.ToString().PadLeft(6, '0') +
                               i64TotalCreditAmt.PadLeft(11, '0') +
                               m_iTotalCreditQty.ToString().PadLeft(6, '0') +
                               m_totalPayTransAmt +
                               m_totalPayTransCount +
                               m_netPayTransAmt +
                               m_netPayTransCount +
                               m_strNetTotalTransAmt.PadLeft(11, '0') +
                               m_strNetTotalTransCount.PadLeft(6, '0')+
                               "                                                  ";
          
            WriteToBalanceFile(balaceReportLine, true);
            FileTemp.ArchiveFile(m_strCurrentDataPath, m_strArchivePath);
            FileTemp.DeleteOutdatedArchivedFile(m_strArchivePath, m_iDeleteArchiveFileBackDays);

            //send mis-matching email out
            if (File.Exists(m_strNoFoundReportFullPathName))
            {
                sendEmailofNoFoundRpt();
                FileTemp.ArchiveFile(m_strCurrentNofoundReportPath, m_strArchiveNofoundReportPath);
            }
            FileTemp.DeleteOutdatedArchivedFile(m_strArchiveNofoundReportPath, m_iDeleteArchiveNofoundFileBackDays);
            return true;
        }

        private bool CalculateTotalItems(ArrayList tempSameTransList, ref string finalLine, ref bool bGoodLine, ref TTranSubTotalInfo rTranSubTotalInfo)
        {
            int iTotalItemQty = 0;
            string strClassandItemAmt = null;
            finalLine = "";
            bGoodLine = true;
            int aNewTranAMT = 0;
            int aNewTAXTran = 0;
            string newTransAMTSymboltoNeg = "602";
            string newTransAMTSymboltoPos = "202";
            string newFormatCardEntryType = "";

            foreach (GetTxtInputInfo oneLineList in tempSameTransList)
            {
                if (oneLineList.BKey1StoreDateFound == false || oneLineList.BKey2Tranumlast4CCFound == false)
                {
                    bGoodLine = false;
                }

                if (oneLineList != null)
                {
                    newFormatCardEntryType = oneLineList.StrCardEntryType;
                    if (newFormatCardEntryType == "0")
                    {
                        newFormatCardEntryType = "2";
                    }
                    if (oneLineList.StrItemQty != "0" && oneLineList.StrItemAmt != "0")
                    {
                        iTotalItemQty = iTotalItemQty + int.Parse(oneLineList.StrItemQty.ToString());
                        aNewTranAMT = int.Parse(oneLineList.StrTranAmt);
                        if (aNewTranAMT < 0)
                        {
                            string strTranAMT = aNewTranAMT.ToString();
                            string strTranAMTOutput = strTranAMT.Replace("-", "").Trim();
                            string strTAX = oneLineList.StrTranTax.Replace("-", "").Trim();
                            string strItemAmt = oneLineList.StrItemAmt.Replace("-", "").Trim();
                            //replace new positive amt to line
                            if (oneLineList.StrItemQty == "1")
                            {
                                strClassandItemAmt = strClassandItemAmt + oneLineList.StrClassCD.PadRight(20, '0') + strItemAmt.PadLeft(7, '0');
                            }
                            else
                            {
                                if (oneLineList.StrItemQty != "0")
                                {
                                    int iItmQuantity = 0;
                                    int iUnitItemAMT = 0;
                                    int iSumItemArrayAmt = 0;
                                    iItmQuantity = int.Parse(oneLineList.StrItemQty);
                                    iUnitItemAMT = int.Parse(strItemAmt) / iItmQuantity;
                                    iSumItemArrayAmt = iUnitItemAMT * iItmQuantity;
                                    int iRestNumAMT = int.Parse(strItemAmt) - iSumItemArrayAmt;
                                    int iLastItemArrayAmt = iUnitItemAMT + iRestNumAMT;
                                    if (iItmQuantity > 0)
                                    {
                                        for (int items = 0; items < iItmQuantity - 1; items++)
                                        {
                                            strClassandItemAmt = strClassandItemAmt + oneLineList.StrClassCD.PadRight(20, '0') + iUnitItemAMT.ToString().PadLeft(7, '0');
                                        }
                                    }
                                    strClassandItemAmt = strClassandItemAmt + oneLineList.StrClassCD.PadRight(20, '0') + iLastItemArrayAmt.ToString().PadLeft(7, '0');
                                }
                                else
                                {
                                    m_errorHandler.LogNotify("Error: Item quantity can not be zero, value would return an infinity number, which has Trans Date: ("
                                        + oneLineList.StrTranDate() + "). Store Number: (" + oneLineList.StrStoreNum + "). Trans Number: (" + oneLineList.StrTranNum);
                                }
                            }
                            finalLine = oneLineList.StrRecordTypeBody +
                                        newTransAMTSymboltoNeg +
                                        oneLineList.StrSameTransLineOutput() +
                                        strTranAMTOutput.PadLeft(8, '0') +
                                        strTAX.PadLeft(7, '0') +
                                        oneLineList.StrSameTransLine() +
                                        newFormatCardEntryType +
                                        oneLineList.StrSameTransLineAfterCardEntry() +
                                        iTotalItemQty.ToString().PadLeft(3, '0') +
                                        strClassandItemAmt;

                            rTranSubTotalInfo.ITotalCreditQty = 1;
                            rTranSubTotalInfo.I64TotalCreditAmt = int.Parse(oneLineList.StrTranAmt.ToString());

                        }
                        else
                        {
                            if (oneLineList.StrItemQty == "1")
                            {
                                strClassandItemAmt = strClassandItemAmt + oneLineList.StrClassCD.PadRight(20, '0') + oneLineList.StrItemAmt.PadLeft(7, '0');
                            }
                            else
                            {
                                if (oneLineList.StrItemQty != "0")
                                {
                                    int iItmQuantity = 0;
                                    int iUnitItemAMT = 0;
                                    int iSumItemArrayAmt = 0;
                                    iItmQuantity = int.Parse(oneLineList.StrItemQty);
                                    iUnitItemAMT = int.Parse(oneLineList.StrItemAmt) / iItmQuantity;

                                    iSumItemArrayAmt = iUnitItemAMT * iItmQuantity;
                                    int iRestNumAMT = int.Parse(oneLineList.StrItemAmt) - iSumItemArrayAmt;
                                    int iLastItemArrayAmt = iUnitItemAMT + iRestNumAMT;
                                    if (iItmQuantity > 0)
                                    {
                                        for (int items = 0; items < iItmQuantity - 1; items++)
                                        {
                                            strClassandItemAmt = strClassandItemAmt + oneLineList.StrClassCD.PadRight(20, '0') + iUnitItemAMT.ToString().PadLeft(7, '0');
                                        }
                                    }
                                    strClassandItemAmt = strClassandItemAmt + oneLineList.StrClassCD.PadRight(20, '0') + iLastItemArrayAmt.ToString().PadLeft(7, '0');
                                }
                                else
                                {
                                    m_errorHandler.LogNotify("Error: Item quantity can not be zero, value would return an infinity number, which has Trans Date: ("
                                        + oneLineList.StrTranDate() + "). Store Number: (" + oneLineList.StrStoreNum + "). Trans Number: (" + oneLineList.StrTranNum);
                                }
                            }
                            finalLine = oneLineList.StrRecordTypeBody +
                                       newTransAMTSymboltoPos +
                                       oneLineList.StrSameTransLineOutput() +
                                       oneLineList.StrTranAmt.PadLeft(8, '0') +
                                       oneLineList.StrTranTax.PadLeft(7, '0') +
                                       oneLineList.StrSameTransLine() +
                                       newFormatCardEntryType +
                                       oneLineList.StrSameTransLineAfterCardEntry() +
                                       iTotalItemQty.ToString().PadLeft(3, '0') +
                                       strClassandItemAmt;
                            rTranSubTotalInfo.ITotalDebitQty = 1;
                            rTranSubTotalInfo.I64TotalDebitAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                        }
                    }
                    else
                    {
                        iTotalItemQty = 0;
                        //to split tender
                        aNewTranAMT = aNewTranAMT + int.Parse(oneLineList.StrTranAmt);
                        aNewTAXTran = aNewTAXTran + int.Parse(oneLineList.StrTranTax);
                        if (aNewTranAMT < 0)
                        {
                            string strTranAMTOutputTran = aNewTranAMT.ToString().Replace("-", "").Trim();
                            string strTAXTran = aNewTAXTran.ToString().Replace("-", "").Trim();                            

                            finalLine = oneLineList.StrRecordTypeBody +
                                       newTransAMTSymboltoNeg +
                                       oneLineList.StrSameTransLineOutput() +
                                       strTranAMTOutputTran.PadLeft(8, '0') +
                                       strTAXTran.PadLeft(7, '0') +
                                       oneLineList.StrSameTransLine() +
                                       newFormatCardEntryType +
                                       oneLineList.StrSameTransLineAfterCardEntry() +
                                       iTotalItemQty.ToString().PadLeft(3, '0');

                            rTranSubTotalInfo.ITotalCreditQty = 1;
                            //rTranSubTotalInfo.I64TotalCreditAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                            rTranSubTotalInfo.I64TotalCreditAmt = aNewTranAMT;

                        }
                        else
                        {
                            //set the newTransAMTSymboltoPos value to 852 if it is TCCPMNT - by vnaidu
                            if (oneLineList.WholeLine.Contains("TCCPMNT") == true)
                            {
                                newTransAMTSymboltoPos = "852";
                                rTranSubTotalInfo.ITotalTCCPmntCreditQty = 1;
                                //rTranSubTotalInfo.I64TotalDebitAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                                rTranSubTotalInfo.I64TotalTCCPmntCreditAmt = -1 * aNewTranAMT;
                            }
                            else
                            {
                                rTranSubTotalInfo.ITotalDebitQty = 1;
                                //rTranSubTotalInfo.I64TotalDebitAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                                rTranSubTotalInfo.I64TotalDebitAmt = aNewTranAMT;
                            }

                            finalLine = oneLineList.StrRecordTypeBody +
                                       newTransAMTSymboltoPos +
                                       oneLineList.StrSameTransLineOutput() +
                                       aNewTranAMT.ToString().PadLeft(8, '0') +
                                       aNewTAXTran.ToString().PadLeft(7, '0') +
                                       oneLineList.StrSameTransLine() +
                                       newFormatCardEntryType +
                                       oneLineList.StrSameTransLineAfterCardEntry() +
                                       iTotalItemQty.ToString().PadLeft(3, '0');
                        }
                    }
                    rTranSubTotalInfo.StoreNum = int.Parse(oneLineList.StrStoreNum);
                    //rTranSubTotalInfo.I64SingleTransAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                    //check if it TCC payment record
                    if (oneLineList.WholeLine.Contains("TCCPMNT") == true)
                    {
                        rTranSubTotalInfo.I64SingleTransAmt = -1 * aNewTranAMT;
                    }
                    else
                    {
                        rTranSubTotalInfo.I64SingleTransAmt = aNewTranAMT;
                    }
                    rTranSubTotalInfo.StrTransDate = oneLineList.StrTranDate();
                    rTranSubTotalInfo.BGoodLineTwoKeys = bGoodLine;
                }

            }
            return true;
        }
        public bool WriteToStoreSumReport()
        {
            if (m_listInputText.Count == 0)
                return false;
            string strStoreNumWriteStore = null;
            string strTransDateWriteStore = null;
            string strTransNumWriteStore = null;
            string strLast4CreditCardWriteStore = null;
            string strTransDateWriteforSum = null;
            string strStoreNumWriteStoreforSum = null;
            string strTransDateforSumWrite = null;
            ArrayList tempTransList = new ArrayList();
            ArrayList tempStoreList = new ArrayList();
            ArrayList tempStoreSumList = new ArrayList();
            ArrayList listlistStoreSumInfo = new ArrayList();
            ArrayList listStorebyDateInfo = new ArrayList();
            string finalLine = null;
            bool bFound = false;
            bool bFoundSum = false;
            bool bFoundSumStoreFinal = false;
            Int64 m_i64SumStoreTransAmt = 0;
            string m_strTransDate = null;
            int m_iStoreNum = 0;
            char szDlimited = ',';
            string headerLine = "";
            //string strSumStoreAmt = "";
            string sumStoreLine = "";
            double m_dByDateTransAmtToDeciaml = 0;
            string m_strCurrentDataPath = ConfigurationManager.AppSettings["txtOutputStoreSum4SalesAuditPath"].ToString();
            string m_strArchivePath = ConfigurationManager.AppSettings["txtOutputStoreSumArchivedFilesPath"].ToString();
            int m_iDeleteArchiveFileBackDays = int.Parse(ConfigurationManager.AppSettings["txtDeleteArchiveStoreSumFileBackDays"].ToString());

            FileHelper FileTemp = new FileHelper(m_errorHandler);
            string m_CurrentSumReportName = ConfigurationManager.AppSettings["TDOutputStoreSumReportName"].ToString() + ".csv";
            FileTemp.ClearFileInCurrentStoreSumFolder(m_CurrentSumReportName);

            //print header
            headerLine = "Torrid Credit Card Summary of Totals by Store " + szDlimited + "\n" +
                "Credit Card Transactions Processed:" + szDlimited + DateTime.Today.ToString("MM/dd/yyyy");
            WriteToStoreSumFile(headerLine, true);
            //print body
            foreach (GetTxtInputInfo oneLine in m_listInputText)
            {
                if (oneLine != null)
                {
                    bFound = false;
                    if (strStoreNumWriteStore == oneLine.StrStoreNum)
                    {
                        if (strTransDateWriteStore == oneLine.StrTranDate())
                        {
                            if (strTransNumWriteStore == oneLine.StrTranNum)
                            {
                                if (strLast4CreditCardWriteStore == oneLine.LastFourCCNum())
                                { bFound = true; }
                            }
                        }
                    }
                }
                if (bFound == true)
                    tempTransList.Add(oneLine);
                else
                {
                    strStoreNumWriteStore = oneLine.StrStoreNum;
                    strTransDateWriteStore = oneLine.StrTranDate();
                    strTransNumWriteStore = oneLine.StrTranNum;
                    strLast4CreditCardWriteStore = oneLine.LastFourCCNum();
                    if (tempTransList.Count == 0)
                    {//do nothing
                        tempTransList.Add(oneLine);
                    }
                    else
                    {
                        bool bGoodlineSum = false;
                        TTranSubTotalInfo rTransSubTotalInfo = new TTranSubTotalInfo();
                        //ArrayList listlistStoreSumInfo = new ArrayList();
                        bool bRet = CalculateTotalItemsSumStore(tempTransList, ref bGoodlineSum, ref rTransSubTotalInfo);
                        if (bGoodlineSum == true)
                        {
                            m_iStoreNum = rTransSubTotalInfo.StoreNum;
                            m_i64SumStoreTransAmt = rTransSubTotalInfo.I64SingleTransAmt;
                            m_strTransDate = rTransSubTotalInfo.StrTransDate.ToString();
                            listlistStoreSumInfo.Add(rTransSubTotalInfo);
                        }

                        //m_rReport.WriteToFile(finalLine, bGoodline);
                        finalLine = null;
                        tempTransList.Clear();
                        tempTransList.Add(oneLine);
                    }

                }
            }//end loop
            if (tempTransList.Count != 0)
            {
                bool bGoodline = false;
                TTranSubTotalInfo rTransSubTotalInfo = new TTranSubTotalInfo();
                //ArrayList listlistStoreSumInfo = new ArrayList();
                //bool bRet = CalculateTotalItems(tempTransList, ref finalLine, ref bGoodline, ref rTransSubTotalInfo);
                bool bRet = CalculateTotalItemsSumStore(tempTransList, ref bGoodline, ref rTransSubTotalInfo);
                if (bGoodline == true)
                {
                    m_iStoreNum = rTransSubTotalInfo.StoreNum;
                    m_i64SumStoreTransAmt = rTransSubTotalInfo.I64SingleTransAmt;
                    m_strTransDate = rTransSubTotalInfo.StrTransDate.ToString();
                    listlistStoreSumInfo.Add(rTransSubTotalInfo);
                }
            }
            foreach (TTranSubTotalInfo oneListLine in listlistStoreSumInfo)
            {
                if (oneListLine != null)
                {
                    bFoundSum = false;
                    if (strTransDateWriteforSum == oneListLine.StrTransDate.ToString())
                    {
                        if (strStoreNumWriteStoreforSum == oneListLine.StoreNum.ToString())
                        {
                            bFoundSum = true;
                        }
                    }
                }
                if (bFoundSum == true)
                    tempStoreList.Add(oneListLine);
                else
                {
                    strStoreNumWriteStoreforSum = oneListLine.StoreNum.ToString();
                    strTransDateWriteforSum = oneListLine.StrTransDate.ToString();
                    if (tempStoreList.Count == 0)
                    {//do nothing
                        tempStoreList.Add(oneListLine);
                    }
                    else
                    {
                        bool bGoodlineSum = false;
                        TTranStoreTotalInfo rTransStoreTotalInfo = new TTranStoreTotalInfo();
                        bool bRetSum = CalculateTranAMTByStore(tempStoreList, ref bGoodlineSum, ref rTransStoreTotalInfo);
                        if (bGoodlineSum == true)
                        {
                            m_strTransDate = rTransStoreTotalInfo.StrTransDateforSum.ToString();
                            m_iStoreNum = rTransStoreTotalInfo.IStoreNumforSum;
                            m_i64SumStoreTransAmt = rTransStoreTotalInfo.I64SumDateStoreTransAmt;
                            listStorebyDateInfo.Add(rTransStoreTotalInfo);

                        }

                        //WriteToStoreSumFile(finalLine, bGoodlineSum);

                        finalLine = null;
                        tempStoreList.Clear();
                        tempStoreList.Add(oneListLine);
                    }

                }
            }//end loop
            if (tempStoreList.Count != 0)
            {
                bool bGoodlineSum = false;
                TTranStoreTotalInfo rTransStoreTotalInfo = new TTranStoreTotalInfo();
                bool bRetSumStoredate = CalculateTranAMTByStore(tempStoreList, ref bGoodlineSum, ref rTransStoreTotalInfo);
                if (bGoodlineSum == true)
                {
                    //m_iTotalDebitQty += rTransSubTotalInfo.ITotalDebitQty;
                    m_strTransDate = rTransStoreTotalInfo.StrTransDateforSum.ToString();
                    m_iStoreNum = rTransStoreTotalInfo.IStoreNumforSum;
                    m_i64SumStoreTransAmt += rTransStoreTotalInfo.I64SumDateStoreTransAmt;
                    listStorebyDateInfo.Add(rTransStoreTotalInfo);
                }
            }
            //    //TODO print sum of store
            foreach (TTranStoreTotalInfo oneListLineSum in listStorebyDateInfo)
            {
                if (oneListLineSum != null)
                {
                    bFoundSumStoreFinal = false;
                    if (strTransDateforSumWrite == oneListLineSum.StrTransDateforSum.ToString())
                    {
                        bFoundSumStoreFinal = true;
                    }
                }
                if (bFoundSumStoreFinal == true)
                    tempStoreSumList.Add(oneListLineSum);
                else
                {
                    strTransDateforSumWrite = oneListLineSum.StrTransDateforSum.ToString();
                    if (tempStoreSumList.Count == 0)
                    {
                        tempStoreSumList.Add(oneListLineSum);
                    }
                    else
                    {
                        bool bGoodlineSum = false;
                        TTranStorebyDateSumInfo rTranStorebyDateSumInfo = new TTranStorebyDateSumInfo();
                        bool bRetSumbyDate = CalculateTranAMTByStorebyDate(tempStoreSumList, ref finalLine, ref bGoodlineSum, ref rTranStorebyDateSumInfo);
                        if (bGoodlineSum == true)
                        {
                            //to actual amt with decimal
                            //m_i64byDateTransAmt = rTranStorebyDateSumInfo.I64SumStorebyDateTransAmt;
                            m_dByDateTransAmtToDeciaml = rTranStorebyDateSumInfo.I64SumStorebyDateTransAmt * (0.01);
                        }
                        //print store sum line

                        string strFinalTranDate = szDlimited + "Transaction Date:" + szDlimited +
                                                  rTranStorebyDateSumInfo.StrTransDateFinal.ToString() + szDlimited;
                        WriteToStoreSumFile(strFinalTranDate, bGoodlineSum);
                        WriteToStoreSumFile(finalLine, bGoodlineSum);

                        sumStoreLine = szDlimited + "Grand Total for Processed Date of " + DateTime.Today.ToString("MM/dd/yyyy") + szDlimited +
                           " $ " + m_dByDateTransAmtToDeciaml.ToString() + "\n";

                        WriteToStoreSumFile(sumStoreLine, true);
                        finalLine = null;
                        tempStoreSumList.Clear();
                        tempStoreSumList.Add(oneListLineSum);
                    }
                }

            }//end loop
            if (tempStoreSumList.Count != 0)
            {
                bool bGoodlineSum = false;
                TTranStorebyDateSumInfo rTranStorebyDateSumInfo = new TTranStorebyDateSumInfo();
                bool bRetSumbyDate = CalculateTranAMTByStorebyDate(tempStoreSumList, ref finalLine, ref bGoodlineSum, ref rTranStorebyDateSumInfo);
                if (bGoodlineSum == true)
                {
                    m_dByDateTransAmtToDeciaml = rTranStorebyDateSumInfo.I64SumStorebyDateTransAmt * (0.01);

                }
                string strFinalTranDate = szDlimited + "Transaction Date:" + szDlimited +
                                                 rTranStorebyDateSumInfo.StrTransDateFinal.ToString() + szDlimited;
                WriteToStoreSumFile(strFinalTranDate, bGoodlineSum);
                WriteToStoreSumFile(finalLine, bGoodlineSum);

                sumStoreLine = szDlimited + "Grand Total for Processed Date of " + DateTime.Today.ToString("MM/dd/yyyy") + szDlimited +
                           " $ " + m_dByDateTransAmtToDeciaml.ToString() + "\n";

                WriteToStoreSumFile(sumStoreLine, true);
                finalLine = null;
                tempStoreSumList.Clear();
            }
            //send email to sales audit

            sendEmailofStoreSumRpt();
            FileTemp.ArchiveFile(m_strCurrentDataPath, m_strArchivePath);
            FileTemp.DeleteOutdatedArchivedFile(m_strArchivePath, m_iDeleteArchiveFileBackDays);
            return true;
        }

        private bool CalculateTotalItemsSumStore(ArrayList tempSameTransList, ref bool bGoodLine, ref TTranSubTotalInfo rTranSubTotalInfo)
        {
            bGoodLine = true;
            int aNewTranAMT = 0;

            foreach (GetTxtInputInfo oneLineList in tempSameTransList)
            {
                if (oneLineList.BKey1StoreDateFound == false || oneLineList.BKey2Tranumlast4CCFound == false)
                {
                    bGoodLine = false;
                }

                if (oneLineList != null)
                {

                    if (oneLineList.StrItemQty != "0" && oneLineList.StrItemAmt != "0")
                    {
                        aNewTranAMT = int.Parse(oneLineList.StrTranAmt);
                        if (aNewTranAMT < 0)
                        {
                            rTranSubTotalInfo.ITotalCreditQty = 1;
                            rTranSubTotalInfo.I64TotalCreditAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                            
                        }
                        else
                        {
                            rTranSubTotalInfo.ITotalDebitQty = 1;
                            rTranSubTotalInfo.I64TotalDebitAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                        }
                        rTranSubTotalInfo.I64SingleTransAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                    }
                    else
                    {
                        aNewTranAMT = aNewTranAMT + int.Parse(oneLineList.StrTranAmt.ToString());
                        if (aNewTranAMT < 0)
                        {
                            rTranSubTotalInfo.ITotalCreditQty = 1;
                            rTranSubTotalInfo.I64TotalCreditAmt = aNewTranAMT;
                        }
                        else 
                        {
                            //set the TCCPMNT under credit bucket - by vnaidu
                            if (oneLineList.WholeLine.Contains("TCCPMNT") == true)
                            {                                
                                rTranSubTotalInfo.ITotalTCCPmntCreditQty = 1;
                                //rTranSubTotalInfo.I64TotalDebitAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                                rTranSubTotalInfo.I64TotalTCCPmntCreditAmt = -1 * aNewTranAMT;
                            }
                            else
                            {
                                rTranSubTotalInfo.ITotalDebitQty = 1;
                                rTranSubTotalInfo.I64TotalDebitAmt = aNewTranAMT;
                            }                            
                        }
                        if (oneLineList.WholeLine.Contains("TCCPMNT") == true)
                        {
                            rTranSubTotalInfo.I64SingleTransAmt = -1 * aNewTranAMT;
                        }
                        else
                        {
                            rTranSubTotalInfo.I64SingleTransAmt = aNewTranAMT;
                        }
                    }

                    rTranSubTotalInfo.StoreNum = int.Parse(oneLineList.StrStoreNum);
                    //rTranSubTotalInfo.I64SingleTransAmt = int.Parse(oneLineList.StrTranAmt.ToString());
                    //rTranSubTotalInfo.I64SingleTransAmt = aNewTranAMT;
                    rTranSubTotalInfo.StrTransDate = oneLineList.StrTranDate();
                    rTranSubTotalInfo.BGoodLineTwoKeys = bGoodLine;
                }

            }
            return true;
        }
        private bool CalculateTranAMTByStorebyDate(ArrayList tempSameStoreTransList, ref string finalLineStoreSum, ref bool bGoodLine, ref TTranStorebyDateSumInfo rTranStorebyDateSumInfo)
        {
            Int64 m_iSameStoreDateSumAmt = 0;
            double m_dSumDateStoreTransAmt = 0;
            finalLineStoreSum = "";
            bGoodLine = true;
            string szDlimited = ",";

            foreach (TTranStoreTotalInfo oneLineListFinal in tempSameStoreTransList)
            {

                if (oneLineListFinal != null)
                {

                    m_iSameStoreDateSumAmt = m_iSameStoreDateSumAmt + oneLineListFinal.I64SumDateStoreTransAmt;
                    m_dSumDateStoreTransAmt = oneLineListFinal.I64SumDateStoreTransAmt * 0.01;

                    if (oneLineListFinal.I64SumDateStoreTransAmt < 0)
                    {
                        string m_strSameStoreDateSumAmtCredit = "(" + m_dSumDateStoreTransAmt.ToString().Replace("-", "").Trim() + ")";

                        finalLineStoreSum += szDlimited + "Store number: " + oneLineListFinal.IStoreNumforSum.ToString() + szDlimited +
                                    m_strSameStoreDateSumAmtCredit + "," + "\n";
                    }
                    else
                    {
                        finalLineStoreSum += szDlimited + "Store number: " + oneLineListFinal.IStoreNumforSum.ToString() + szDlimited +
                                        m_dSumDateStoreTransAmt.ToString() + szDlimited + "\n";
                    }
                    bGoodLine = oneLineListFinal.BGoodLineTwoKeys;
                    //rTranStorebyDateSumInfo.IByDateStoreNum = int.Parse(oneLineListFinal.ToString());
                    rTranStorebyDateSumInfo.I64SumStorebyDateTransAmt = m_iSameStoreDateSumAmt;
                    rTranStorebyDateSumInfo.StrTransDateFinal = oneLineListFinal.StrTransDateforSum;
                }

            }

            return true;
        }
        private bool CalculateTranAMTByStore(ArrayList tempSameStoreTransList, ref bool bGoodLine, ref TTranStoreTotalInfo rTransStoreTotalInfo)
        {
            Int64 m_iSameStoreSumAmtByDate = 0;

            bGoodLine = true;

            foreach (TTranSubTotalInfo oneLineList in tempSameStoreTransList)
            {

                if (oneLineList != null)
                {

                    m_iSameStoreSumAmtByDate = m_iSameStoreSumAmtByDate + oneLineList.I64SingleTransAmt;
                    rTransStoreTotalInfo.BGoodLineTwoKeys = oneLineList.BGoodLineTwoKeys;
                    rTransStoreTotalInfo.IStoreNumforSum = int.Parse(oneLineList.StoreNum.ToString());
                    rTransStoreTotalInfo.I64SumDateStoreTransAmt = m_iSameStoreSumAmtByDate;
                    rTransStoreTotalInfo.StrTransDateforSum = oneLineList.StrTransDate.ToString();
                }

            }

            return true;
        }
        public void WriteToFile(string data, bool IsGood)
        {
            string strfilePath = null;
            FileHelper FileTemp = new FileHelper(m_errorHandler);
            if (IsGood)
                strfilePath = FileHelper.BuildOutputFileName();
            else
                strfilePath = FileHelper.GetOutputBadLine();
            FileTemp.WriteToFile(data, strfilePath);
        }
        public void WriteToBalanceFile(string data, bool IsGood)
        {
            string strfilePathBalance = null;
            FileHelper FileTemp = new FileHelper(m_errorHandler);
            if (IsGood)
            {
                strfilePathBalance = FileHelper.BuildOutputBalanceFileName();
            }
            FileTemp.WriteToFile(data, strfilePathBalance);
        }
        public void WriteToStoreSumFile(string data, bool IsGood)
        {
            string strfilePathStoreSum = null;
            FileHelper FileTemp = new FileHelper(m_errorHandler);
            if (IsGood)
            {
                strfilePathStoreSum = FileHelper.BuildOutputStoreSumFileName();
            }
            FileTemp.WriteToFile(data, strfilePathStoreSum);
        }
        public void WriteToDescriptorFile(string data, bool IsGood)
        {
            string strfilePathDescriptor = null;
            FileHelper FileTemp = new FileHelper(m_errorHandler);
            if (IsGood)
            {
                strfilePathDescriptor = FileHelper.BuildOutputDescriptorFileName();
            }
            FileTemp.WriteToFile(data, strfilePathDescriptor);
        }
        public void WriteEmptyBalanceFile()
        {
            string strBalaceReportLine = "";

            FileHelper FileTemp = new FileHelper(m_errorHandler);
            string m_CurrentBalanceReportName = ConfigurationManager.AppSettings["TDOutputBalanceReportName"].ToString() + ".txt";
            string m_strCurrentDataPath = ConfigurationManager.AppSettings["txtOutputFilePath"].ToString();
            string m_strArchivePath = ConfigurationManager.AppSettings["txtOutputArchiveFilePath"].ToString();
            int m_iDeleteArchiveFileBackDays = int.Parse(ConfigurationManager.AppSettings["txtDeleteArchiveOutputFileBackDays"].ToString());
            string m_CurrentDetailReportName = ConfigurationManager.AppSettings["TDOutputRetailReportName"].ToString() + ".txt";

            FileTemp.ClearFileInCurrentFolder(m_CurrentDetailReportName);
            FileTemp.ClearFileInCurrentFolder(m_CurrentBalanceReportName);

            strBalaceReportLine = "TORRID".PadRight(20, ' ') +
                                DateTime.Today.ToString("yyyyMMdd") +
                                DateTime.Today.AddDays(-1).ToString("yyyyMMdd") +
                                "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
                                "                                                  ";
            WriteToBalanceFile(strBalaceReportLine, true);
            FileTemp.ArchiveFile(m_strCurrentDataPath, m_strArchivePath);
            FileTemp.DeleteOutdatedArchivedFile(m_strArchivePath, m_iDeleteArchiveFileBackDays);
        }
        public void WriteDescriptorFile()
        {
            string finalDesLine = "";
            FileHelper FileTemp = new FileHelper(m_errorHandler);
            string m_TDOutputDescriptorReportFileName = ConfigurationManager.AppSettings["TDOutputDescriptorReportName"].ToString() + ".txt";

            FileTemp.ClearFileInCurrentFolder(m_TDOutputDescriptorReportFileName);
            foreach (TBuildDescriptorInputInfo oneDesLine in m_listDescriptorInputText)
            {
                //Trim it to 25 if StrDes exceeds 25 characters
                if (oneDesLine.StrDes.Length > 25)
                    oneDesLine.StrDes = oneDesLine.StrDes.Substring(0, 25);

                if (oneDesLine != null)
                {
                    finalDesLine = oneDesLine.StrClassCD.PadRight(20, '0') +
                                         "          " +
                                         oneDesLine.StrDes.PadRight(25) +
                                         oneDesLine.StrOriginCD.PadLeft(3, '0') +
                                         "                      ";

                    WriteToDescriptorFile(finalDesLine, true);
                }
            }

        }
        public void sendEmailofStoreSumRpt()
        {
            String m_strEmailServer = ConfigurationManager.AppSettings["EmailServer"].ToString();
            String m_strStoreSumRptEmailSender = ConfigurationManager.AppSettings["StoreSumRptEmailSender"].ToString();
            String m_strStoreSumRptEmailRecipients = ConfigurationManager.AppSettings["StoreSumRptEmailRecipients"].ToString();
            String m_strOutputStoreSumPath = ConfigurationManager.AppSettings["txtOutputStoreSum4SalesAuditPath"].ToString();
            String m_strTDOutputStoreSumReportName = ConfigurationManager.AppSettings["TDOutputStoreSumReportName"].ToString();

            try
            {
                //create the mail message
                MailMessage mail = new MailMessage();

                //set the addresses
                mail.From = new MailAddress(m_strStoreSumRptEmailSender);
                mail.To.Add(m_strStoreSumRptEmailRecipients);

                //set the content
                mail.Subject = "Notify - Torrid Credit Card Summary Report";
                mail.Body = "Attached is the Torrid Credit Card Summary Report.";

                string strStoreSumRptFullPath = m_strOutputStoreSumPath + m_strTDOutputStoreSumReportName + ".csv";
                string sAttach = @strStoreSumRptFullPath;

                // Build an IList of mail attachments using the files named in the string.
                char[] delim = new char[] { ',' };
                foreach (string sSubstr in sAttach.Split(delim))
                {
                    mail.Attachments.Add(new Attachment(sSubstr));
                }

                //send the message
                SmtpClient smtp = new SmtpClient(m_strEmailServer);
                smtp.Send(mail);
            }
            catch (System.Net.Mail.SmtpException SmtpEx)
            {
                // If we get an error attempting to send mail, just write it to the console
                m_errorHandler.LogNotify(SmtpEx.Message);
                m_errorHandler.SendMailOnError = true;
                m_errorHandler.LogNotify("Couldn't send email to sales audit for TCC sum report using server: " + m_strEmailServer + "\" with smtp error: " + SmtpEx.ToString());
            }
        }
        public void sendEmailofNoFoundRpt()
        {
            String m_strEmailServer = ConfigurationManager.AppSettings["EmailServer"].ToString();
            String m_strStoreSumRptEmailSender = ConfigurationManager.AppSettings["StoreSumRptEmailSender"].ToString();
            String m_strNotFoundRptEmailRecipient = ConfigurationManager.AppSettings["NotFoundRptEmailRecipient"].ToString();
            String m_strNoFoundTDCCInfoPath = ConfigurationManager.AppSettings["txtNoFoundTDCCInfoPath"].ToString();
            String m_strNoFoundReportName = ConfigurationManager.AppSettings["TDBadPrefix"].ToString();
            
           try
            {
                //create the mail message
                MailMessage mail = new MailMessage();

                //set the addresses
                mail.From = new MailAddress(m_strStoreSumRptEmailSender);
                mail.To.Add(m_strNotFoundRptEmailRecipient);

                //set the content
                mail.Subject = "Notify - Torrid Mis-Matched Credit Card Report";
                mail.Body = "Attached is the Torrid Mis-Matched Credit Card Report. The system can not match Credit Card Numbers for these transcations in the TLogs.";

                string strNoFoundTransRptFullPath = m_strNoFoundTDCCInfoPath + m_strNoFoundReportName + ".txt";
                string sAttach = @strNoFoundTransRptFullPath;

                // Build an IList of mail attachments using the files named in the string.
                char[] delim = new char[] { ',' };
                foreach (string sSubstr in sAttach.Split(delim))
                {
                    mail.Attachments.Add(new Attachment(sSubstr));
                }

                //send the message
                SmtpClient smtp = new SmtpClient(m_strEmailServer);
                smtp.Send(mail);
            }
            catch (System.Net.Mail.SmtpException SmtpEx)
            {
                // If we get an error attempting to send mail, just write it to the console
                m_errorHandler.LogNotify(SmtpEx.Message);
                m_errorHandler.SendMailOnError = true;
                m_errorHandler.LogNotify("Couldn't send email out for TCC No Found Transaction Report using server: " + m_strEmailServer + "\" with smtp error: " + SmtpEx.ToString());
            }
        }
    }
}


