using System;
using System.Collections.Generic;
using System.Text;

namespace TCCSettlementOutput
{

    class GetTxtInputInfo
    {
        private string m_strStoreNum;
        private string m_strOutputTranDate;
        private string m_strTranDateKey;
        private string m_strTranNum;
        private string m_strCCNumMask;
        private string m_strTranAmt;
        private string m_strTranTax;
        private string m_strAuth;
        private string m_strClassCD;
        private string m_strItemCD;
        private string m_strItemQty;
        private string m_strItemAmt;
        private string m_strTenderType;
        private string m_strProdDate;
        private string m_strRecordTypeBody; // equal to 3
        private string m_strTransType;
        private string m_postage;           //7 zeros
        private string m_AMC;               //8 zeros
        private string m_promoCode;         //3 zeros
        private string m_DeferDate;         //default 99990101
        private string m_authBuyerSeqNum;   //5 zeros
        private string m_purcharseOrdNum;   //20 zeros
        private string m_totalPromoSalesAmt;   //13 zeros
        private string m_totalPromoSalesCount; //12 zeros
        private bool m_bKey1StoreDateFound;
        private bool m_bKey2Tranumlast4CCFound;
        private string m_strCardEntryType; //default 0
        private string wholeLine;
        private string lastFourCCNum;

        public GetTxtInputInfo()
        {
            this.m_strRecordTypeBody = "3";
            this.m_postage = "0000000";
            this.m_AMC = "00000000";
            this.m_promoCode = "001";
            this.m_DeferDate = "99990101";
            this.m_authBuyerSeqNum = "00000";
            this.m_purcharseOrdNum = "00000000000000000000";
            this.m_strCardEntryType = "2";
            this.m_totalPromoSalesAmt = "0000000000000";
            this.m_totalPromoSalesCount = "000000000000";
        }

        public string StrStoreNum
        {
            get { return m_strStoreNum; }
            set { m_strStoreNum = value; }
        }
        public string StrOutputTranDate
        {
            get { return m_strOutputTranDate; }
            set { m_strOutputTranDate = value; }
        }
        public string StrTranDate()
        {
            if (StrOutputTranDate != null)
            {
                if (StrOutputTranDate.Length > 4)
                {
                    m_strTranDateKey = StrOutputTranDate.Substring(StrOutputTranDate.Length - 6, 6);

                }
                else
                {
                    m_strTranDateKey = string.Empty;
                }
            }
            return m_strTranDateKey.ToString();
        }

        public string StrTranNum
        {
            get { return m_strTranNum; }
            set { m_strTranNum = value; }
        }
        public string StrCCNumMask
        {
            get { return m_strCCNumMask; }
            set { m_strCCNumMask = value; }
        }
        public string StrTranAmt
        {
            get { return m_strTranAmt; }
            set { m_strTranAmt = value; }
        }
        public string StrTranTax
        {
            get { return m_strTranTax; }
            set { m_strTranTax = value; }
        }
        public string StrAuth
        {
            get { return m_strAuth; }
            set { m_strAuth = value; }
        }
        public string StrClassCD
        {
            get { return m_strClassCD; }
            set { m_strClassCD = value; }
        }
        public string StrItemCD
        {
            get { return m_strItemCD; }
            set { m_strItemCD = value; }
        }
        public string StrItemQty
        {
            get { return m_strItemQty; }
            set { m_strItemQty = value; }
        }
        public string StrItemAmt
        {
            get { return m_strItemAmt; }
            set { m_strItemAmt = value; }
        }
        public string StrTenderType
        {
            get { return m_strTenderType; }
            set { m_strTenderType = value; }
        }

        public string Postage
        {
            get { return m_postage; }
            set { m_postage = value; }
        }
        public string AMC
        {
            get { return m_AMC; }
            set { m_AMC = value; }
        }
        public string PromoCode
        {
            get { return m_promoCode; }
            set { m_promoCode = value; }
        }
        public string DeferDate
        {
            get { return m_DeferDate; }
            set { m_DeferDate = value; }
        }
        public string AuthBuyerSeqNum
        {
            get { return m_authBuyerSeqNum; }
            set { m_authBuyerSeqNum = value; }
        }
        public string StrProdDate
        {
            get { return m_strProdDate; }
            set { m_strProdDate = value; }
        }

        public string StrRecordTypeBody
        {
            get { return m_strRecordTypeBody; }
            set { m_strRecordTypeBody = value; }
        }

        public string StrTransType
        {
            get { return m_strTransType; }
            set { m_strTransType = value; }
        }
        public string StrCardEntryType
        {
            get { return m_strCardEntryType; }
            set { m_strCardEntryType = value; }
        }

        public string PurcharseOrdNum
        {
            get { return m_purcharseOrdNum; }
            set { m_purcharseOrdNum = value; }
        }
        public string TotalPromoSalesAmt
        {
            get { return m_totalPromoSalesAmt; }
            set { m_totalPromoSalesAmt = value; }
        }
        public string TotalPromoSalesCount
        {
            get { return m_totalPromoSalesCount; }
            set { m_totalPromoSalesCount = value; }
        }
        public string WholeLine
        {
            get { return wholeLine; }
            set { wholeLine = value; }
        }
        public bool BKey1StoreDateFound
        {
            get { return m_bKey1StoreDateFound; }
            set { m_bKey1StoreDateFound = value; }
        }
        public bool BKey2Tranumlast4CCFound
        {
            get { return m_bKey2Tranumlast4CCFound; }
            set { m_bKey2Tranumlast4CCFound = value; }
        }

        public string LastFourCCNum()
        {

            if (StrCCNumMask != null)
            {
                if (StrCCNumMask.Length > 4)
                {
                    lastFourCCNum = StrCCNumMask.Substring(StrCCNumMask.Length - 4, 4);
                }
                else
                {
                    lastFourCCNum = string.Empty;
                }
            }
            return lastFourCCNum.ToString();
        }

        public string StrComKeyStoreDate()
        {
            StringBuilder strStoreDateKeyCom = new StringBuilder();
            strStoreDateKeyCom.Append(StrStoreNum);
            strStoreDateKeyCom.Append(StrTranDate());
            return strStoreDateKeyCom.ToString();
        }
        public string StrComKeyTranumCC4num()
        {
            StringBuilder strTranumCC4KeyCom = new StringBuilder();
            strTranumCC4KeyCom.Append(StrTranNum);
            strTranumCC4KeyCom.Append(LastFourCCNum());
			if (!wholeLine.Contains("TCCPMNT"))
				strTranumCC4KeyCom.Append(m_strAuth);
            return strTranumCC4KeyCom.ToString();
        }

        public string StrSameTransLineOutput()
        {
            StringBuilder strSameTransLineOutput = new StringBuilder();
            strSameTransLineOutput.Append(StrCCNumMask.PadLeft(7, '0'));
            strSameTransLineOutput.Append(StrStoreNum.PadLeft(5, '0'));
            return strSameTransLineOutput.ToString();
        }
        public string StrSameTransLine()
        {
            StringBuilder strSameTransLine = new StringBuilder();
            strSameTransLine.Append(Postage);
            strSameTransLine.Append(StrOutputTranDate.PadLeft(8, '0'));
            strSameTransLine.Append(AMC);
            strSameTransLine.Append(PromoCode);
            strSameTransLine.Append(StrAuth.PadLeft(6, '0'));
            strSameTransLine.Append(" ");
            return strSameTransLine.ToString();
        }
        public string StrSameTransLineAfterCardEntry()
        {
            StringBuilder StrSameTransLineAfterCardEntry = new StringBuilder();
            StrSameTransLineAfterCardEntry.Append(DeferDate);
            StrSameTransLineAfterCardEntry.Append(AuthBuyerSeqNum);
            StrSameTransLineAfterCardEntry.Append(StrTranNum.PadRight(20, '0'));
            StrSameTransLineAfterCardEntry.Append(PurcharseOrdNum);
            return StrSameTransLineAfterCardEntry.ToString();
        }
        public void SetTranAMTSymboltoPos(string newStrTranAMT)
        {

            if (this.StrTranAmt != null && newStrTranAMT != null)
            {
                this.StrTranAmt = newStrTranAMT;
            }

        }
        public void SetFullCredit(string newCreditNum)
        {
            if (this.StrCCNumMask != null && newCreditNum != null)
            {
                StrCCNumMask = StrCCNumMask.Replace(StrCCNumMask, newCreditNum);

            }
        }
    }
}

