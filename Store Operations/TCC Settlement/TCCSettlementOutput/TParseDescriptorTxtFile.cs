using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using System.Collections;


namespace TCCSettlementOutput
{
    class TParseDescriptorTxtFile
    {
        HTError.IHTError m_errorHandler;
        public TParseDescriptorTxtFile(HTError.IHTError RHtError)
        {
            m_errorHandler = RHtError;
        }
        
        public ArrayList Start()
        {
            //Open input master file
            ArrayList allDesFileLines = new ArrayList();

            string txtDesPathFile = FileHelper.GetDescriptorTxtFilePath();
            if (!File.Exists(txtDesPathFile))
            {
              m_errorHandler.LogNotify("Can not find Torrid Credit Card Descriptor input file: " + txtDesPathFile + ".Please make sure it has been run in SSIS package first.");
            }
            try
            {
                if (txtDesPathFile != null)
                {
                    System.IO.TextReader sTextFile = null;
                    sTextFile = new System.IO.StreamReader(txtDesPathFile);

                    string s_CCDesReadFile = String.Empty;
                    //Loop through eachline
                    while ((s_CCDesReadFile = sTextFile.ReadLine()) != null)
                    {
                        string MainStringDes = s_CCDesReadFile;

                        TBuildDescriptorInputInfo c_InputCCDes = new  TBuildDescriptorInputInfo();
                        //c_InputCCDes.WholeLine = s_CCReadFile;

                        string[] myARSplitDes = MainStringDes.Split(',');

                        c_InputCCDes.StrClassCD = myARSplitDes[0].ToString();
                        c_InputCCDes.StrDes = myARSplitDes[1].ToString();
                        c_InputCCDes.StrOriginCD = myARSplitDes[2].ToString();

                        allDesFileLines.Add(c_InputCCDes);
                    }
                    sTextFile.Dispose();
                }
            }
            catch (Exception ex)
            {
                m_errorHandler.LogNotify(ex.Message);

            }
            return allDesFileLines;
        }
    }
}
