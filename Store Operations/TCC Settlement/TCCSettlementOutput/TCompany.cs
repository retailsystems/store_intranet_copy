using System;
using System.Collections.Generic;
using System.Data;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using HTError;
using System.Net.Mail;

namespace TCCSettlementOutput
{
    public class TCompany : ICompany
    {
        public HTError.IHTError m_errorHandler;
        private List<string> m_listStrProcessDays = null;
        private ArrayList m_listInputText = null;
        private ArrayList m_listDescriptorInputText = null;
        private Hashtable m_hashTlog = new Hashtable();

        public TBaseTask()
        {
            GetLog();
        }
        #region Methods
        public virtual void LoadDescriptorTxtFile()
        {

        }
        public virtual bool LoadMasterTxtFile()
        {
            // Build the keys for the master file
            LoadInputTxtFile loadInputTxtFile = new LoadInputTxtFile(m_errorHandler);
            m_listInputText = loadInputTxtFile.Start();

            if (m_listInputText.Count > 0)
                return true;
            else
            {
                WriteEmptyBalanceFile();
                return false;
            }
        }
        public virtual bool ListProcessDays()
        {
            //Build the List for 5 days
            GetTlogDateList getList = new GetTlogDateList(m_errorHandler);
            m_listStrProcessDays = getList.Start();
            if (m_listStrProcessDays.Count > 0)
                return true;
            else
                return false;
        }
        public virtual bool LoadTlogFile()
        {
            //Build hashtable for TLog
            string sourceDir = ConfigurationManager.AppSettings["tlogfilePath"].ToString();
            BuildHashTable bh = new BuildHashTable(m_errorHandler);
            FileHelper FileTemp = new FileHelper(m_errorHandler);
            //bh.TLogToProcess = FileTemp.ProcessDir(sourceDir,ref List<string> listStrProcessDays);
            bh.TLogToProcess = FileTemp.ProcessDir(sourceDir);
            m_hashTlog = bh.Start();
            if (m_hashTlog.Count > 0)
                return true;
            else
                return false;
        }
        public virtual bool SearchFullCC()
        {
            if (m_listInputText.Count > 0 && m_hashTlog != null)
            {
                return Search(m_listInputText, m_hashTlog);
            }
            else
            {
                return false;
            }
        }
        public virtual bool MainWriteReport()
        {
            Console.WriteLine("run TbaseTask MainWriteReport ");
            return true;
        }

        public virtual void GetLog()
        {
            string fileName = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.StartupPath) + "\\" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".log";
            String ProductionFilePath = ConfigurationManager.AppSettings["ProductionFilePath"].ToString();
            String DebugFilePath = ConfigurationManager.AppSettings["DebugFilePath"].ToString();
            String EmailServer = ConfigurationManager.AppSettings["EmailServer"].ToString();
            String EmailSender = ConfigurationManager.AppSettings["EmailSender"].ToString();
            String NotifyRecipients = ConfigurationManager.AppSettings["NotifyRecipients"].ToString();

            m_errorHandler = new HTError.ErrorHandler(ProductionFilePath, DebugFilePath, EmailServer, EmailSender, NotifyRecipients, HTError.LogLevel.Debug);

            m_errorHandler.ErrorEmailPriority = System.Net.Mail.MailPriority.High;
            m_errorHandler.SendMailOnError = true;
            m_errorHandler.SendMailOnWarning = true;
        }
        public virtual bool Search(ArrayList listInputText, Hashtable tLog)
        {

            foreach (GetTxtInputInfo oneLine in listInputText)
            {
                if (oneLine != null)
                {
                    string key1StoreDate = oneLine.StrComKeyStoreDate();
                    string key2TranumCC4 = oneLine.StrComKeyTranumCC4num();
                    string strKey1StoreDate = key1StoreDate == String.Empty ? String.Empty : oneLine.StrComKeyStoreDate();
                    string strkey2TranumCC4 = key2TranumCC4 == String.Empty ? String.Empty : oneLine.StrComKeyTranumCC4num();

                    Hashtable h2_dict = null;

                    if (strKey1StoreDate.Length > 0)
                    {
                        if (strkey2TranumCC4.Length > 0)
                        {
                            if (tLog.ContainsKey(strKey1StoreDate))
                            {
                                h2_dict = new Hashtable();
                                h2_dict = (Hashtable)tLog[strKey1StoreDate];
                                if (h2_dict.ContainsKey(strkey2TranumCC4))
                                {
                                    TDLogCCInfo tdLogCCInfo = new TDLogCCInfo();
                                    tdLogCCInfo = (TDLogCCInfo)h2_dict[strkey2TranumCC4];

                                    oneLine.SetFullCredit(tdLogCCInfo.FullCCNumTDCCInfo());
                                    oneLine.StrCardEntryType = tdLogCCInfo.StrCardEntry();
                                    oneLine.BKey2Tranumlast4CCFound = true;
                                    oneLine.BKey1StoreDateFound = true;

                                }
                                else
                                {
                                    oneLine.BKey2Tranumlast4CCFound = false;

                                }
                            }
                            else
                            {
                                oneLine.BKey1StoreDateFound = false;

                            }

                        }
                    }
                }
            }
            return true;
        }
        #endregion Methods

    }
}
