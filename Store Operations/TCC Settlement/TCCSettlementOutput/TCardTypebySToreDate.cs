using System;
using System.Collections.Generic;
using System.Text;

namespace TCCSettlementOutput
{
    class TCardTypebySToreDate
    {
        private Int64 m_i64SumSingleCardDateStoreAmt = 0;

        public Int64 I64SumSingleCardDateStoreAmt
        {
            get { return m_i64SumSingleCardDateStoreAmt; }
            set { m_i64SumSingleCardDateStoreAmt = value; }
        }

        private int m_iStoreNumforSingleCardT = 0;

        public int IStoreNumforSingleCardT
        {
            get { return m_iStoreNumforSingleCardT; }
            set { m_iStoreNumforSingleCardT = value; }
        }

        private string m_strTransDateforSingleCardT = null;

        public string StrTransDateforSingleCardT
        {
            get { return m_strTransDateforSingleCardT; }
            set { m_strTransDateforSingleCardT = value; }
        }

        private bool m_bGoodLineTwoKeys = false;

        public bool BGoodLineTwoKeys
        {
            get { return m_bGoodLineTwoKeys; }
            set { m_bGoodLineTwoKeys = value; }
        }
        
        private string m_strCCTP = null;

        public string StrCCTP
        {
            get { return m_strCCTP; }
            set { m_strCCTP = value; }
        }
    }
}
