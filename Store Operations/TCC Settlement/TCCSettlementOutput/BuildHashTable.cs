using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;

namespace TCCSettlementOutput
{
    class BuildHashTable
    {
        HTError.IHTError m_errorHandler;
        Hashtable h1_dict = new Hashtable();
        //Hashtable h2_dict = new Hashtable();

        List<FileInfo> _tLogToProcess = null;

        public List<FileInfo> TLogToProcess
        {
            get { return _tLogToProcess; }
            set { _tLogToProcess = value; }
        }

        public BuildHashTable(HTError.IHTError RHtError)
        {
            m_errorHandler = RHtError;
        }

        public Hashtable Start()
        {
            string strTCCPMNTIdentifier = ConfigurationManager.AppSettings["TCCPMNTIdentifier"].ToString();
            //Check whether Tlog file is exist
            if (!CheckTodayData())
            {
                m_errorHandler.SendMailOnError = true;
                m_errorHandler.LogNotify("Yesterday's Tlog folder does not exist, please make sure it has been loaded to the tlog archive folder.");
                System.Environment.Exit(1);
            }

            try
            {
                if (_tLogToProcess.Count > 0)
                {
                    //Gets all files in current directory
                    foreach (FileInfo txtfile in _tLogToProcess)
                    {
                        string fullFilePath = txtfile.FullName;

                        //read log file 
                        
                        System.IO.TextReader sTLogFile = null;

                        sTLogFile = new System.IO.StreamReader(fullFilePath);
                        string s_TlogRead = String.Empty;

                        while ((s_TlogRead = sTLogFile.ReadLine()) != null)
                        {
                            GetTlogInfo b_Tlog = new GetTlogInfo(m_errorHandler);
                                                       

                            if (s_TlogRead.StartsWith("A "))
                            {

                                string[] myTlogSplit = s_TlogRead.Split(' ');

                                b_Tlog.ColOne = myTlogSplit[0] == null ? String.Empty : myTlogSplit[0].ToString();
                                b_Tlog.ColTwo = myTlogSplit[1] == null ? String.Empty : myTlogSplit[1].ToString();
                                                                
                                string strSplitTender = b_Tlog.M_StrTenderID();
                                
                                //Set the below variable for identifying the tcc payment record
                                string strTccArecInd = string.Empty;
                                if (s_TlogRead.Length >= 110)
                                    strTccArecInd = s_TlogRead.Substring(108, 2);
                                else
                                    strTccArecInd = string.Empty;

								b_Tlog.CCAuth = s_TlogRead.Substring(96, 10).Trim();

                                if (String.Compare("31", strSplitTender) == 0)
                                {
                                    try
                                    {
                                            //establish Hash Table
                                            TDLogCCInfo td_TlogCC = new TDLogCCInfo();
                                            td_TlogCC.ColOne = myTlogSplit[0] == null ? String.Empty : myTlogSplit[0].ToString();
                                            td_TlogCC.ColTwo = myTlogSplit[1] == null ? String.Empty : myTlogSplit[1].ToString();
                                            string fullValueCCNum = td_TlogCC.FullCCNumTDCCInfo();
                                            if (fullValueCCNum == "")
                                                continue;
                                            string cardEntryType = td_TlogCC.StrCardEntry();
                                            //keyH1 is storeNum and TrnDate combination
                                            string keyH1 = b_Tlog.TlogK1Combine();
                                            //keyH2 is for trnNum trnsNum and Last 4 digit of TCC number combination and Authorization Number
                                            string keyH2 = b_Tlog.TlogK2Combine();
                                            string valueCCNum = b_Tlog.M_FullCCNum();
                                            if (valueCCNum =="")
                                                continue;
                                            //construct key of Hashtable
                                            //Hashtable h1_dict = new Hashtable();
                                            Hashtable h2_dict = null;
                                            
                                            if (h1_dict.Contains(keyH1))
                                            {
                                              h2_dict = (Hashtable)h1_dict[keyH1];
                                            }
                                            else
                                            {
                                                h2_dict = new Hashtable();
                                                h1_dict.Add(keyH1, h2_dict);
                                            }

                                            //It is possible, same key1 and key2 showed up inside two tlog files as below per Tammy, so below ignore duplicate keys.
                                            // C:\SCRIPTS\UTIL\TLOGS\20080802\STR5192.PRC(2755):
                                            //A 05192020574450574450808021051313388002900080213140115856379520013071          0001052200010522600000      08          000000000000000000000000000
                                            // C:\SCRIPTS\UTIL\TLOGS\20080806\STR5192.PRC(2744):    
                                            //A 05192020574450574450808021051313388002900080513140115856379520013071          0001052200010522600000      08          000000000000000000000000000

                                            if (h2_dict.Contains(keyH2)==false)
                                            h2_dict.Add(keyH2, td_TlogCC);
                                    }
                                    catch (Exception ex)
                                    {

                                        //m_errorHandler.LogInformation("Column two:(" + b_Tlog.ColTwo + "); keyH1:(" + keyH1 + ").");
                                        //m_errorHandler.LogError(ex.Message + "There are duplicate recodes found Tlog file, which is KEY = " + m_hashTlog.Keys + ", Credit Card Numer = " + m_hashTlog.Values);
                                        m_errorHandler.LogInformation(ex.Message + "There are duplicate key found in Tlog file.");

                                        
                                        //m_errorHandler.LogNotify("Can not find matched Credit Card Number line:" + oneLine.WholeLine +
                                        //" from Tlog Files, which has store number:(" + oneLine.StrStoreNum + "); Trans date:(" + oneLine.StrTranDate() +
                                        //"); Trans number:(" + oneLine.StrTranNum + "); Last four digit Credit Card number:(" + oneLine.LastFourCCNum() + ").");

                                    }
                                  
                                }//Regular CC hash gathering end here 
                                else if (String.Compare(strTCCPMNTIdentifier, strTccArecInd) == 0) //(TCC Payment A record to build new hash)
                                {
                                    try
                                    {
                                        //establish Hash Table
                                        TDLogCCInfo td_TlogCC = new TDLogCCInfo();
                                        td_TlogCC.ColOne = myTlogSplit[0] == null ? String.Empty : myTlogSplit[0].ToString();
                                        td_TlogCC.ColTwo = myTlogSplit[1] == null ? String.Empty : myTlogSplit[1].ToString();
                                        string fullValueCCNum = td_TlogCC.FullCCNumTDCCInfo();
                                        if (fullValueCCNum == "")
                                            continue;
                                        string cardEntryType = td_TlogCC.StrCardEntry();
                                        //keyH1 is storeNum and TrnDate combination
                                        string keyH1 = b_Tlog.TlogK1Combine();
                                        //keyH2 is for trnNum trnsNum and Last 4 digit of TCC number combination
                                        string keyH2 = b_Tlog.TlogK2Combine();
                                        string valueCCNum = b_Tlog.M_FullCCNum();
                                        if (valueCCNum == "")
                                            continue;
                                        //construct key of Hashtable
                                        //Hashtable h1_dict = new Hashtable();
                                        Hashtable TCCPMNT_h2_dict = null;

                                        if (h1_dict.Contains(keyH1))
                                        {
                                            TCCPMNT_h2_dict = (Hashtable)h1_dict[keyH1];
                                        }
                                        else
                                        {
                                            TCCPMNT_h2_dict = new Hashtable();
                                            h1_dict.Add(keyH1, TCCPMNT_h2_dict);
                                        }

                                        // Sample record
                                        
                                        // A 05014010000420000421409081043260416000500071411299015856379520000029          0000200000002000            49          000000000000000000000000000 
                                        // A 05014010000420000421409081054240418000500071411299115856379520000029          0000300000000000            49          000000000000000000000000000                  

                                        if (TCCPMNT_h2_dict.Contains(keyH2) == false)
                                            TCCPMNT_h2_dict.Add(keyH2, td_TlogCC);
                                    }
                                    catch (Exception ex)
                                    {

                                        //m_errorHandler.LogInformation("Column two:(" + b_Tlog.ColTwo + "); keyH1:(" + keyH1 + ").");
                                        //m_errorHandler.LogError(ex.Message + "There are duplicate recodes found Tlog file, which is KEY = " + m_hashTlog.Keys + ", Credit Card Numer = " + m_hashTlog.Values);
                                        m_errorHandler.LogInformation(ex.Message + "There are duplicate key found in Tlog file.");


                                        //m_errorHandler.LogNotify("Can not find matched Credit Card Number line:" + oneLine.WholeLine +
                                        //" from Tlog Files, which has store number:(" + oneLine.StrStoreNum + "); Trans date:(" + oneLine.StrTranDate() +
                                        //"); Trans number:(" + oneLine.StrTranNum + "); Last four digit Credit Card number:(" + oneLine.LastFourCCNum() + ").");

                                    }

                                }
                            }
                        }
                        sTLogFile.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                m_errorHandler.LogNotify(ex.Message + " job failed during building hash table");
            }

            return h1_dict;

        }

        private bool CheckTodayData()
        {
            bool hasData = false;
           //string t_Today = DateTime.Today.ToString("yyyyMMdd");

            //need to change back to yesterday tlog folder
            string t_Today = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");

            string folderDate = String.Empty;
            foreach (FileInfo txtfile in TLogToProcess)
            {
                folderDate = txtfile.Directory.Name;
                if (String.Compare(t_Today, folderDate) == 0)
                {
                    hasData = true;
                }
            }
            return hasData;
        }
    }
}

