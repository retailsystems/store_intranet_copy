using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using HTError;


namespace TCCSettlementOutput
{
    public interface ICompany
    {
        //void GetLog();
        bool LoadDescriptorTxtFile();
        bool LoadMasterTxtFile();
        bool ListProcessDays();
        bool LoadTlogFile();
        bool SearchFullCC();
        bool WriteToDetailReport();
        bool WriteToStoreSumReport();
    }
}

