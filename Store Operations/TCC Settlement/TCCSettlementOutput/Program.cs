using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using HTError;

namespace TCCSettlementOutput
{
    class Program
    {
        public HTError.IHTError m_errorHandler = null;
        static void Main(string[] args)
        {
             TTorrid td = new TTorrid();
             int nRunFTP = int.Parse(ConfigurationManager.AppSettings["RunFTP"].ToString());

             if (runTask(ref td) == true)
             {
                 if (nRunFTP == 1)
                 {
                     runCuteFTP(ref td);
                 }
             }
        }
            
        private static bool runTask(ref TTorrid td)
        {
            bool bLoadDescriptorInputSucceeded = false;
            bool bLoadMasterInputSucceeded = false;
            bool bListProcessDaysSucceeded = false;
            bool bLoadTlogFileSucceeded = false;
            bool bSearchFullCCSucceeded = false;
            bool bWriteToDetailReportSucceeded = false;
            bool bWriteToStoreSumReportSucceeded = false;
            bool bRunTCCSuccess = false;
            try
            {
                bLoadDescriptorInputSucceeded = td.LoadDescriptorTxtFile();
                if (bLoadDescriptorInputSucceeded == true)
                {
                    td.m_errorHandler.LogInformation("TCC Descriptor File has been Loaded successfully!");
                }
                bLoadMasterInputSucceeded = td.LoadMasterTxtFile();
                if (bLoadMasterInputSucceeded == true)
                {
                    bListProcessDaysSucceeded = td.ListProcessDays();
                    if (bListProcessDaysSucceeded == true)
                    {
                        bLoadTlogFileSucceeded = td.LoadTlogFile();
                        if (bLoadTlogFileSucceeded == true)
                        {
                             bSearchFullCCSucceeded = td.SearchFullCC();
                            if (bSearchFullCCSucceeded == true)
                            {
                                bWriteToDetailReportSucceeded = td.WriteToDetailReport();
                                if (bWriteToDetailReportSucceeded == true)
                                {
                                    bWriteToStoreSumReportSucceeded = td.WriteToStoreSumReport();
                                    if (bWriteToStoreSumReportSucceeded == true)
                                    {
                                        bRunTCCSuccess = true;
                                        td.m_errorHandler.LogNotify("TCC output task has completed successfully!");
                                        
                                    }
                                    else
                                    {
                                        td.m_errorHandler.LogNotify("Failed to run TCC Store Date Summary report.");
                                        
                                    }
                                }
                                else
                                {
                                    td.m_errorHandler.LogNotify("Failed to run TCC Retail/Balance reports.");
                                  
                                }
                            }
                            else
                            {
                                td.m_errorHandler.LogNotify("Failed for searching full CC Number in TLogs.");
                                
                                
                            }
                        }
                        else
                        {
                            td.m_errorHandler.LogNotify("Failed to load Tlog files.");
                            
                        }
                    }
                    else
                    {
                        td.m_errorHandler.LogNotify("Failed for getting correct processing day of Tlogs.");
                        
                    }
                }
                else
                {
                    td.m_errorHandler.LogNotify("Empty Input file. There is no TCC transaction has been generated from Database today.");
                    bRunTCCSuccess = true;
                 
                }
            }
            catch (Exception ex)
            {
                td.m_errorHandler.LogNotify(ex.Message);
            }
            return bRunTCCSuccess;
        }
        private static bool runCuteFTP(ref TTorrid td)
        {
            bool bFTPSucceeded = false;
            try
            {
              
              int iProcessExitCode = 0;
              string m_strCuteFTPExePath = ConfigurationManager.AppSettings["CuteFTPExePath"].ToString();
              string m_strCuteFTPExeName = ConfigurationManager.AppSettings["CuteFTPExeName"].ToString();
              string m_strFullPathCuteFTP = m_strCuteFTPExePath + m_strCuteFTPExeName + ".exe";
              
              //production
              //System.Diagnostics.ProcessStartInfo proStartInfo = new System.Diagnostics.ProcessStartInfo(@"C:\scripts\TCC Settlement\TCCApps\HTCuteFTP.exe");

              //System.Diagnostics.ProcessStartInfo proStartInfo = new System.Diagnostics.ProcessStartInfo(@"C:\development\test\TestCuteFTP\TestCuteFTP\bin\Release\HTCuteFTP.exe");
              System.Diagnostics.ProcessStartInfo proStartInfo = new System.Diagnostics.ProcessStartInfo(m_strFullPathCuteFTP);
              
              proStartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
              proStartInfo.UseShellExecute = false;
              
                  using (System.Diagnostics.Process TCCProcess = System.Diagnostics.Process.Start(proStartInfo))
                  {
                     
                      TCCProcess.WaitForExit();
                      iProcessExitCode = TCCProcess.ExitCode;
                  }
                  
            }
            catch (Exception ex)
            {
                
                td.m_errorHandler.LogNotify("Failed to run Auto FTP.");
                td.m_errorHandler.LogNotify(ex.Message);
                
            }
            
            return bFTPSucceeded;
           
            
        } 
   }
}


