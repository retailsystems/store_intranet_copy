1. Once the compilation is done move the following files to the Tlog server to the appropriate folder

	a. TCCSettlementOutput.exe
	b. TCCSettlementOutput.exe.Config

	Ex: C:\scripts\TCC Settlement\TCCApps

2. Backup the old files to a new folder and move the TCCSettlementOutput.exe__2 file to backup folder as well 

3. After deploying the files, run the below command

    C:\scripts\install2e>2encryptinit.exe -m
    
    the above command will encrypt the exe and creates a file called TCCSettlementOutput.exe__2
    
