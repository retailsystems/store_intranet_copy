using System;
using System.Collections.Generic;
using System.Text;

namespace TCCSettlementOutput
{
    public class SAllStoresDailyInfo
    {
        private string m_strTrnDate;

        public string StrTrnDate
        {
            get { return m_strTrnDate; }
            set { m_strTrnDate = value; }
        }
        private Int64 m_iAllStoreDailyAMT;

        public Int64 IAllStoreDailyAMT
        {
            get { return m_iAllStoreDailyAMT; }
            set { m_iAllStoreDailyAMT = value; }
        }

        private List<TPREachStoreInfo> listAllStoreInfo = new List<TPREachStoreInfo>();

        public List<TPREachStoreInfo> ListAllStoreInfo
        {
            get { return listAllStoreInfo; }
            set { listAllStoreInfo = value; }
        }

        

    }
}
