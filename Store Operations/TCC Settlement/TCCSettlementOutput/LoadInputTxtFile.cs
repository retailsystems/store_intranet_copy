using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using System.Collections;

namespace TCCSettlementOutput
{
    class LoadInputTxtFile
    {
        HTError.IHTError m_errorHandler;
        public LoadInputTxtFile(HTError.IHTError RHtError)
        {
            m_errorHandler = RHtError;
        }
        
        public ArrayList Start()
        {
            //Open input master file
            ArrayList allFileLines = new ArrayList();

            string txtPathFile = FileHelper.BuildTxtFilePath();
            if (!File.Exists(txtPathFile))
            {
                m_errorHandler.SendMailOnError = true;
                m_errorHandler.LogNotify("Can not find Torrid Credit Card Settlement txt file: " + txtPathFile + ".Please make sure it has been run SSIS package first.");
                
                System.Environment.Exit(1);
            }
            try
            {
                if (txtPathFile != null)
                {
                    System.IO.TextReader sTextFile = null;
                    sTextFile = new System.IO.StreamReader(txtPathFile);

                    string s_CCReadFile = String.Empty;
                    //Loop through eachline
                    while ((s_CCReadFile = sTextFile.ReadLine()) != null)
                    {
                        string MainString = s_CCReadFile;

                        GetTxtInputInfo c_InputCC = new GetTxtInputInfo();
                        c_InputCC.WholeLine = s_CCReadFile;

                        string[] myARSplit = MainString.Split(',');
                       
                        c_InputCC.StrStoreNum = myARSplit[0].ToString();
                        c_InputCC.StrOutputTranDate = myARSplit[1].ToString();
                        c_InputCC.StrTranNum = myARSplit[2].ToString();
                        c_InputCC.StrClassCD = myARSplit[4].ToString();
                        c_InputCC.StrItemQty = myARSplit[5].ToString();
                        c_InputCC.StrItemAmt = myARSplit[6].ToString();
                        c_InputCC.StrTranAmt = myARSplit[7].ToString();
                        c_InputCC.StrTranTax = myARSplit[8].ToString();
                        c_InputCC.StrCCNumMask = myARSplit[9].ToString();
                        c_InputCC.StrAuth = myARSplit[10].ToString();
                        
                        allFileLines.Add(c_InputCC);
                    }
                    sTextFile.Dispose();
                }
            }
            catch (Exception ex)
            {
                m_errorHandler.LogNotify(ex.Message);
                //m_errorHandler.LogMessage("Error: occurs on loading input file.");
            }
            return allFileLines;
        }
    }
}
   

