using System;
using System.Collections.Generic;
using System.Text;

namespace TCCSettlementOutput
{
    class GetTlogInfo
    {
        private string m_colOne;
        private string m_colTwo;
        private string m_partCCNum;
        private string m_trnDate;
        private string m_trnNum;
        private string m_storeNum;
        private string m_fullCCNum;
        private string m_strCCNum;
        private string m_strTenderID;
        private string m_strCardEntryType;
		private string ccauth;

		public string CCAuth
		{
			get { return ccauth; }
			set { ccauth = value; }
		}
        public HTError.IHTError m_errorHandler;
        //private int i_strTenderID;

        //private static HTError.IHTError m_errorHandler = Program.GetLog();
        public GetTlogInfo(HTError.IHTError RHtError)
        {
            m_errorHandler = RHtError;
        }

        public string ColOne
        {
            get { return m_colOne; }
            set { m_colOne = value; }
        }
        public string ColTwo
        {
            get { return m_colTwo; }
            set { m_colTwo = value; }
        }
        public string M_strCCNum
        {
            get { return m_strCCNum; }
            set { m_strCCNum = value; }
        }
        public string M_TrnNum()
        {
            m_trnNum = m_colTwo.Substring(31, 4);
            return m_trnNum.ToString();
        }
        public string M_TrnDate()
        {
            m_trnDate = m_colTwo.Substring(19, 6);
            return m_trnDate.ToString();
        }
        public string M_StoreNum()
        {
            m_storeNum = m_colTwo.Substring(1, 4);
            return m_storeNum.ToString();
        }
        public string M_partCCNum()
        {
            m_partCCNum = m_colTwo.Substring(m_colTwo.Length - 4, 4);
            return m_partCCNum.ToString();
        }
        public string M_FullCCNum()
        {
            try
            {
                if (m_colTwo.Length >= 67)
                {
                    m_fullCCNum = m_colTwo.Substring(52, 16);
                }
                else
                {
                    m_fullCCNum = "";
                }
            }
            catch (Exception ex)
            {
                m_errorHandler.LogError(ex);
            }

            return m_fullCCNum.ToString();
        }
        public string M_StrTenderID()
        {
            m_strTenderID = m_colTwo.Substring(m_colTwo.Length - 22, 2);
            return m_strTenderID.ToString();
        }

        public string M_StrCardEntryType()
        {
            m_strCardEntryType = m_colTwo.Substring(m_colTwo.Length - 17, 1);
            return m_strCardEntryType.ToString();
        }
        public string TlogK1Combine()
        {
            StringBuilder strTlogKeyH1 = new StringBuilder();
            strTlogKeyH1.Append(M_StoreNum());
            strTlogKeyH1.Append(M_TrnDate());
            return strTlogKeyH1.ToString();
        }
        public string TlogK2Combine()
        {
            StringBuilder strTlogKeyH2 = new StringBuilder();
            strTlogKeyH2.Append(M_TrnNum());
            strTlogKeyH2.Append(M_partCCNum());
			strTlogKeyH2.Append(ccauth);
            return strTlogKeyH2.ToString();
        }
    }
}
