using System;
using System.Collections.Generic;
using System.Text;
using CuteFTPPro;
using System.Net;
using System.IO;
using System.Configuration;
using HTError;
using System.Threading;


namespace UtilCuteFTP
{
    static class Program
    {
        private static HTError.IHTError m_errorHandler = null;
        [STAThread]
        static void Main(string[] args)
        {
            
            m_errorHandler = GetLog();

            try
            {
                m_errorHandler.LogDebug("Started AUTO FTP JOB.");
                string m_filRetailSettlementReportName = ConfigurationManager.AppSettings["TorridRetailSettlementReportfileName"].ToString();
                string m_filBalanceSettlementReportName = ConfigurationManager.AppSettings["TorridBalanceSettlementReportfileName"].ToString();
                string m_filDescriptorReportName = ConfigurationManager.AppSettings["TorridOutputDescriptorReportName"].ToString();
                int m_iConnectTryTimes = int.Parse(ConfigurationManager.AppSettings["ConnectTryTimes"].ToString());
                int m_iTrySleepWaitMinutes = int.Parse(ConfigurationManager.AppSettings["TrySleepWaitMinutes"].ToString());
                string m_filLocalPath = ConfigurationManager.AppSettings["LocalFolder"].ToString();
                bool bConnected = false;
                TEConnection MySite = null;

                for (m_iConnectTryTimes = 1; m_iConnectTryTimes <= 3; m_iConnectTryTimes++)
                {
                    try
                    {
                        MySite = Activator.CreateInstance<TEConnectionClass>();
                        m_errorHandler.LogDebug("TE Instance created.");
                        
                        MySite.Host = ConfigurationManager.AppSettings["HostName"].ToString();
                        MySite.Protocol = ConfigurationManager.AppSettings["ProtocolType"].ToString();
                        MySite.Port = int.Parse(ConfigurationManager.AppSettings["PortNum"]);
                        MySite.TransferType = "BINARY";
                        MySite.DataChannel = "DEFAULT";

                        //cuteFTP login name and bypassing password
                        MySite.Login = ConfigurationManager.AppSettings["CuteFTPLoginName"].ToString();
                        try
                        {
                            //Connect to remote server
                            MySite.Connect();
                            bConnected = true;
                            m_errorHandler.LogDebug("CuteFTP Connected.");
                            break;                       
                        }
                        catch
                        {
                            m_errorHandler.LogDebug("CuteFTP Connection Failed with error:" + m_iConnectTryTimes +" Time Try.");
                        }
                        System.Threading.Thread.Sleep(m_iTrySleepWaitMinutes * 60000);
                    }
                    catch
                    {
                        m_errorHandler.LogDebug("TE Instance created Failed:" + m_iConnectTryTimes + " Time Try.");
                    }
                }//end for ()
                 
                    if (bConnected == true)
                    {
                        MySite.RemoteFolder = ConfigurationManager.AppSettings["RemoteFolderName"].ToString();
                        MySite.LocalFolder = @ConfigurationManager.AppSettings["LocalFolder"].ToString();

                        string strRetailReportPath = m_filLocalPath + m_filRetailSettlementReportName;
                        string strBalanceReportPath = m_filLocalPath + m_filBalanceSettlementReportName;
                        string strDescriptorReportPath = m_filLocalPath + m_filDescriptorReportName;

                        if (File.Exists(strDescriptorReportPath))
                        {
                            //MySite.Upload(m_filBalanceSettlementReportName, m_filBalanceSettlementReportName, 1);
                            MySite.Upload(m_filDescriptorReportName, m_filDescriptorReportName, 1);
                            m_errorHandler.LogInformation("Uploaded file " + strDescriptorReportPath + " successfully.");
                        }
                        else
                        {
                            m_errorHandler.LogNotify("The file " + strDescriptorReportPath + " doesn't exist in the TCC output folder.");
                        }

                        if (File.Exists(strBalanceReportPath))
                        {
                            MySite.Upload(m_filBalanceSettlementReportName, m_filBalanceSettlementReportName, 1);
                            m_errorHandler.LogInformation("Uploaded file " + strBalanceReportPath + " successfully.");
                        }
                        else
                        {
                            m_errorHandler.LogInformation("The file " + strBalanceReportPath + " doesn't exist in the TCC output folder.");

                        }
                        if (File.Exists(strRetailReportPath))
                        {
                            MySite.Upload(m_filRetailSettlementReportName, m_filRetailSettlementReportName, 1);
                            m_errorHandler.LogInformation("Uploaded file " + strRetailReportPath + " successfully.");
                        }
                        else
                        {
                            m_errorHandler.LogInformation("The file " + strBalanceReportPath + " doesn't exist in the TCC output folder.");
                        }


                        MySite.Disconnect();
                        MySite.Close("EXIT");
                        m_errorHandler.LogNotify("Auto CuteFTP To Alliance Data is completed successfully.");
                        m_errorHandler.LogDebug("End of AUTO FTP JOB.");
                
                    }
                    else
                    {
                        m_errorHandler.LogDebug("CuteFTP Connection Failed with error.");
                        m_errorHandler.LogError("CuteFTP Connection Failed with error.");
                    }
            }
            catch (Exception ex)
            {
                m_errorHandler.LogNotify(ex.Message);
                m_errorHandler.SendMailOnError = true;
                m_errorHandler.LogError("Auto CuteFTP To Allicance Data failed to go through.");
            }
        }

        public static HTError.IHTError GetLog()
        {
            if (m_errorHandler == null)
            {
                string fileName = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.StartupPath) + "\\" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".log";
                String ProductionFilePath = ConfigurationManager.AppSettings["ProductionFilePath"].ToString();
                String DebugFilePath = ConfigurationManager.AppSettings["DebugFilePath"].ToString();
                String EmailServer = ConfigurationManager.AppSettings["EmailServer"].ToString();
                String EmailSender = ConfigurationManager.AppSettings["EmailSender"].ToString();
                String NotifyRecipients = ConfigurationManager.AppSettings["NotifyRecipients"].ToString();

                m_errorHandler = new HTError.ErrorHandler(ProductionFilePath, DebugFilePath, EmailServer, EmailSender, NotifyRecipients, HTError.LogLevel.Debug);

                m_errorHandler.ErrorEmailPriority = System.Net.Mail.MailPriority.High;
                m_errorHandler.SendMailOnError = true;
                m_errorHandler.SendMailOnWarning = true;

            }

            return m_errorHandler;
        }
    }
}
