<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Search.aspx.vb" Inherits="DirectoryMaint.Search"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Search</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/hottopic.css" type="text/css"
			rel="stylesheet" runat="server">
	</HEAD>
	<body id="pageBody">
		<form id="Form1" method="post" runat="server">
			<iframe class="Header_Frame" id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0"
				src="http://172.16.125.2/Header/Header.aspx?mode=hottopic" frameBorder="0" width="100%"
				scrolling="no" runat="server"></iframe>
			<table width="100%">
				<tr>
					<td colSpan="2" height="1"></td>
				</tr>
				<tr>
					<td><asp:radiobuttonlist id="rblSearch" CssClass="Normal" Runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
							<asp:ListItem Value="CI">People with No Contact Info.</asp:ListItem>
							<asp:ListItem Value="SI">Subjects with No Contact Info.</asp:ListItem>
							<asp:ListItem Value="People">Subjects associated with a Specific Person</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td>
						<asp:label id="lblPerson" Runat="server" Visible="False" text="Please Select A Person To Search For:"></asp:label>
						<asp:dropdownlist id="ddlPeople" Runat="server" AutoPostBack="true" Visible="False"></asp:dropdownlist></td>
				</tr>
				<tr vAlign="top">
					<td noWrap>
						<asp:label id="lblResults" Runat="server"></asp:label></td>
				</tr>
				<tr>
					<td><br>
						<div style="OVERFLOW-Y: auto; WIDTH: 100%; HEIGHT: 410px" class="Scrollbar">
							<table width="100%">
								<tr vAlign="top">
									<td>
										<asp:datagrid id="dgCI" CssClass="DataGridSm" runat="server" Width="100%" GridLines="None" EnableViewState="True"
											AutoGenerateColumns="False">
											<ItemStyle CssClass="BC111111sm"></ItemStyle>
											<AlternatingItemStyle CssClass="BC333333sm"></AlternatingItemStyle>
											<HeaderStyle CssClass="DATAGRID_Headersm"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="NamePreferred" HeaderText="First Name"></asp:BoundColumn>
												<asp:BoundColumn DataField="NameLast" HeaderText="Last Name"></asp:BoundColumn>
												<asp:BoundColumn DataField="JobCode" HeaderText="Job Code"></asp:BoundColumn>
												<asp:BoundColumn DataField="Dept_Desc" HeaderText="Department"></asp:BoundColumn>
											</Columns>
											<PagerStyle></PagerStyle>
										</asp:datagrid></td>
								</tr>
								<tr vAlign="top">
									<td>
										<asp:datagrid id="dgSubject" CssClass="DataGridSm" runat="server" Width="100%" GridLines="None"
											EnableViewState="True" AutoGenerateColumns="False">
											<ItemStyle CssClass="BC111111sm"></ItemStyle>
											<AlternatingItemStyle CssClass="BC333333sm"></AlternatingItemStyle>
											<HeaderStyle CssClass="DATAGRID_Headersm"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="SubjectDesc" HeaderText="Subject"></asp:BoundColumn>
											</Columns>
											<PagerStyle></PagerStyle>
										</asp:datagrid></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr align="right">
					<td>
						<asp:Button CssClass="b1" Runat="server" ID="btnPrint" Text="Printer Friendly Version"></asp:Button>&nbsp;
						<asp:Button CssClass="b1" Runat="server" ID="btnReturn" Text="Return"></asp:Button></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
