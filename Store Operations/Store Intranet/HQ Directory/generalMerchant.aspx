<%@ Page Language="vb" AutoEventWireup="false" Codebehind="generalMerchant.aspx.vb" Inherits="ReferenceLists.generalMerchant"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>generalMerchant</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server">
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
			<script src="/Global/js/html5shiv.min.js"></script>
			<script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body onload="self.focus()" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%"	scrolling="no" runat="server"/>
			<div class="container home" role="main">
			<table>
				<tr>
					<td><asp:datagrid id="DataGrid1" runat="server" AutoGenerateColumns="False" CssClass="table archtbl"
							GridLines="Vertical" Height="184px" Width="399px">
							<ItemStyle CssClass="archtbltr"></ItemStyle>
							<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
							<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DeptID" HeaderText="Class" HeaderStyle-CssClass="archtblhdr">
									<HeaderStyle Width="2%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DeptDesc" HeaderText="Department" HeaderStyle-CssClass="archtblhdr">
									<HeaderStyle Width="10%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderStyle-CssClass="archtblhdr" HeaderStyle-Width="9%">
									<HeaderTemplate>
										Contact
									</HeaderTemplate>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
									<ItemTemplate>
										<a href='mailto:<%# Container.dataitem("Email") %>'>
											<asp:Label Runat="server" ID="Label1" Text='<%# Container.dataitem("contactName") %>'>
											</asp:Label></a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="contactext" HeaderText="Ext" HeaderStyle-CssClass="archtblhdr">
									<HeaderStyle Width="2%"></HeaderStyle>
								</asp:BoundColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<br>
			<table>
				<tr>
					<td><asp:datagrid id="Datagrid2" runat="server" AutoGenerateColumns="false" CssClass="table archtbl"
							GridLines="Vertical" Height="60px" Width="398px">
							<ItemStyle CssClass="archtbltr"></ItemStyle>
							<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
							<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
							<Columns>
								<asp:BoundColumn dataField="FitQuality" HeaderText="Fit Quality Control" HeaderStyle-CssClass="archtblhdr">
									<HeaderStyle Width="6%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderStyle-CssClass="archtblhdr" HeaderStyle-Width="9%">
									<HeaderTemplate>
										Name
									</HeaderTemplate>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
									<ItemTemplate>
										<a href='mailto:<%# Container.dataitem("Email") %>'>
											<asp:Label Runat="server" ID="Label2" Text='<%# Container.dataitem("Name") %>'>
											</asp:Label></a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn dataField="extension1" HeaderText="Ext" HeaderStyle-CssClass="archtblhdr">
									<HeaderStyle Width="2%"></HeaderStyle>
								</asp:BoundColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<br>
			<asp:panel id="Cordinate" Runat="server">
				<asp:datagrid id="Datagrid3" runat="server" Width="398px" Height="60px" GridLines="Vertical" CssClass="table archtbl"
					AutoGenerateColumns="false">
					<ItemStyle CssClass="archtbltr"></ItemStyle>
					<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
					<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
					<Columns>
						<asp:BoundColumn dataField="Merchandise Coordinator" HeaderText="Merchandise Coordinator" HeaderStyle-CssClass="archtblhdr">
							<HeaderStyle Width="10%"></HeaderStyle>
						</asp:BoundColumn>
						<asp:TemplateColumn HeaderStyle-CssClass="archtblhdr" HeaderStyle-Width="9%">
							<HeaderTemplate>
								Email
							</HeaderTemplate>
							<ItemTemplate>
								<a href='mailto:<%# Container.dataitem("Email") %>'>
									<asp:Label Runat="server" ID="Label3" Text='<%# Container.dataitem("Name") %>'>
									</asp:Label></a>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn dataField="extension1" HeaderText="Ext" HeaderStyle-CssClass="archtblhdr">
							<HeaderStyle Width="6%"></HeaderStyle>
						</asp:BoundColumn>
					</Columns>
				</asp:datagrid>
			</asp:panel><br>
			<table align="center">
				<tr>
					<td><input class="btn btn-danger" onclick="window.print()" type="button" value="Print" name="btnPrint">
						<input class="btn btn-danger" onclick="window.top.close()" type="button" value="Close" name="btnClose">
					</td>
				</tr>
			</table>
			<table align="left">
				<tr>
					<td><asp:label id="NewLa" Runat="server">* New items we should carry</asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="Label6" Runat="server">* Fit, Quality, Comment</asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="Label7" Runat="server">* Product Knowledge</asp:label></td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
