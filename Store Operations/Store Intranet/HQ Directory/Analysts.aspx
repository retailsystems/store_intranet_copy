<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Analysts.aspx.vb" Inherits="ReferenceLists.Analysts"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Analysts</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server">
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
			<script src="/Global/js/html5shiv.min.js"></script>
			<script src="/Global/js/respond.min.js"></script>
		<![endif]-->
  </HEAD>
	<body onload="self.focus()" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server"/>
			<div class="container home" role="main">
			<table width="100%">
				<tr width="100%">
					`
					<td width="100%"><asp:datagrid id="Datagrid1" runat="server" Width="100%" Height="184px" GridLines="Vertical" CssClass="table archtbl"
							AutoGenerateColumns="false">
							<ItemStyle CssClass="archtbltr"></ItemStyle>
							<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
							<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DeptID" HeaderText="Class" HeaderStyle-CssClass="archtblhdr">
									<HeaderStyle Width="2%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DeptDesc" HeaderText="Department" HeaderStyle-CssClass="archtblhdr">
									<HeaderStyle Width="8%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderStyle-CssClass="archtblhdr" HeaderStyle-Width="8%">
									<HeaderTemplate>
										Allocation Analysts
									</HeaderTemplate>
									<ItemStyle HorizontalAlign="Left" wrap="False"></ItemStyle>
									<ItemTemplate>
										<a href='mailto:<%# Container.dataitem("AllocAnalystsEmail") %>'>
											<asp:Label Runat="server" ID="Label1" Text='<%# Container.dataitem("AllocAnalysts") %>'>
											</asp:Label></a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="ExtAllocAnalysts" HeaderText="Ext" HeaderStyle-CssClass="archtblhdr">
									<HeaderStyle Width="2%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderStyle-CssClass="archtblhdr" HeaderStyle-Width="8%">
									<HeaderTemplate>
										P&A Analysts
									</HeaderTemplate>
									<ItemStyle HorizontalAlign="Left" wrap="False"></ItemStyle>
									<ItemTemplate>
										<a href='mailto:<%# Container.dataitem("PAnalystsEmail") %>'>
											<asp:Label Runat="server" ID="Label2" Text='<%# Container.dataitem("PAnalysts") %>'>
											</asp:Label></a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="PAnalystsext" HeaderText="Ext" HeaderStyle-CssClass="archtblhdr">
									<HeaderStyle Width="2%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderStyle-CssClass="archtblhdr" HeaderStyle-Width="8%">
									<HeaderTemplate>
										Sr. P&A Analysts
									</HeaderTemplate>
									<ItemStyle HorizontalAlign="Left" wrap="False"></ItemStyle>
									<ItemTemplate>
										<a href='mailto:<%# Container.dataitem("SrAnalystsEmail") %>'>
											<asp:Label Runat="server" ID="Label3" Text='<%# Container.dataitem("SrAnalysts") %>'>
											</asp:Label></a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="SrAnalystsext" HeaderText="Ext" HeaderStyle-CssClass="archtblhdr">
									<HeaderStyle Width="2%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderStyle-CssClass="archtblhdr" HeaderStyle-Width="8%">
									<HeaderTemplate>
										Director of P & A
									</HeaderTemplate>
									<ItemStyle HorizontalAlign="Left" wrap="False"></ItemStyle>
									<ItemTemplate>
										<a href='mailto:<%# Container.dataitem("DirPAEmail") %>'>
											<asp:Label Runat="server" ID="Label4" Text='<%# Container.dataitem("dirPA") %>'>
											</asp:Label></a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="dirPAExt" HeaderText="Ext" HeaderStyle-CssClass="archtblhdr">
									<HeaderStyle Width="2%"></HeaderStyle>
								</asp:BoundColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<asp:panel id="generalQuest" Runat="server">
<TABLE>
  <TR>
    <TD>
<asp:Label id=Quest Runat="server">General Merchandising Questions:</asp:Label></TD></TR><%
					for intCounter = 0 to ArrayListMerchant.count -1
						 Dim i As Integer
						i = InStr(ArrayListMerchant(intCounter), "test", CompareMethod.Text)
						strStart  = Mid(ArrayListMerchant(intCounter),1,i-1)
						Dim strTot As String = Mid(ArrayListMerchant(intCounter), i + 5) %>
  <TR>
    <TD><FONT size=2><%=strStart%></FONT></TD></TR>
  <TR>
    <TD><FONT 
size=1><%=strTot%></FONT></TD></TR><%	next %></TABLE><BR>
			</asp:panel><asp:panel id="FitQuest" Runat="server">
<TABLE>
  <TR>
    <TD>
<asp:Label id=FirCont Runat="server">Fit Quality Control:</asp:Label></TD></TR><%
							
							For inti = 0 To ds.Tables(0).Rows.Count -1
							   Dim x As String = ds.Tables(0).Rows(inti)("Name") + " ext. " + ds.Tables(0).Rows(inti)("extension1")
							   %>
  <TR>
    <TD><FONT size=2><%=x%></FONT></TD></TR><%Next
						%></TABLE>
			</asp:panel><br>
			<table align="right">
				<tr>
					<td><input class="btn btn-danger" onclick="window.print()" type="button" value="Print" name="btnPrint">
						<input class="btn btn-danger" onclick="window.top.close()" type="button" value="Close" name="btnClose">
					</td>
				</tr>
			</table>
			<table align="left">
				<tr>
					<td><asp:label id="NewLa" Runat="server">* Opportunities for expanding categories/general feedback about what is/isn't working within categories.</asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="Label6" Runat="server">* Significant changes in the mall (the parking lot is torn up; a new competitor moved in, etc.).</asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="Label7" Runat="server">* Allocation quantity- too little, too much.</asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="Label8" Runat="server">* Size issues.</asp:label></td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
