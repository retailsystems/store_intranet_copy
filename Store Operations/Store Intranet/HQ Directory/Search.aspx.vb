Public Class Search
    Inherits System.Web.UI.Page
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents rblSearch As System.Web.UI.WebControls.RadioButtonList
    Private myDatareader As System.Data.SqlClient.SqlDataReader
    Protected WithEvents dgCI As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblPerson As System.Web.UI.WebControls.Label
    Protected WithEvents ddlPeople As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dgSubject As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnPrint As System.Web.UI.WebControls.Button
    'Protected WithEvents ucHeader As Header
    Private RT As String 'Report Type
    Private RET As String 'Return page
    Private FillGrid As Boolean
    Protected WithEvents lblResults As System.Web.UI.WebControls.Label
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifmHeader As System.Web.UI.HtmlControls.HtmlGenericControl

    Public strPerson As String  'person searching for.
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            btnPrint.Visible = False
            RET = Request.QueryString("RET")
            ViewState("RET") = RET
        End If

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=Search")

    End Sub
    Private Sub rblSearch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rblSearch.SelectedIndexChanged
        If rblSearch.SelectedItem.Value = "People" Then
            'Subjects a specific person is associated with...
            ViewState("RT") = "People"
            dgSubject.DataSource = ""
            dgSubject.DataBind()
            dgSubject.Visible = False

            dgCI.DataSource = ""
            dgCI.DataBind()
            dgCI.Visible = False

            lblPerson.Visible = True
            ddlPeople.Visible = True
            btnPrint.Visible = False

            Dim strStoredProcedure As String
            Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
            Dim objDataReader As SqlClient.SqlDataReader
            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            objConnection.Open()
            strStoredProcedure = "spFillPeople "

            objCommand.CommandText = strStoredProcedure
            objDataReader = objCommand.ExecuteReader()

            ddlPeople.DataSource = objDataReader
            ddlPeople.DataTextField = "Name"
            ddlPeople.DataValueField = "Employeeid"
            ddlPeople.DataBind()
            objCommand.Dispose()
            objDataReader.Close()
        ElseIf rblSearch.SelectedItem.Value = "CI" Then
            'People with no contact info....
            ViewState("RT") = "CI"
            lblPerson.Visible = False
            ddlPeople.Visible = False

            dgSubject.DataSource = ""
            dgSubject.DataBind()
            dgSubject.Visible = False

            Call CountResults()
            If FillGrid = True Then
                Dim strStoredProcedure As String
                Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
                Dim objDataReader As SqlClient.SqlDataReader
                Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

                objConnection.Open()
                strStoredProcedure = "spNoContact "

                objCommand.CommandText = strStoredProcedure
                objDataReader = objCommand.ExecuteReader()

                dgCI.DataSource = objDataReader
                dgCI.DataBind()
                dgCI.Visible = True
                objCommand.Dispose()
                objDataReader.Close()
            End If  
        ElseIf rblSearch.SelectedItem.Value = "SI" Then
            'Subjects with no contact info...
            ViewState("RT") = "SI"
            lblPerson.Visible = False
            ddlPeople.Visible = False

            dgCI.DataSource = ""
            dgCI.DataBind()
            dgCI.Visible = False

            Call CountResults()
            If FillGrid = True Then
                Dim strStoredProcedure As String
                Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
                Dim objDataReader As SqlClient.SqlDataReader
                Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

                objConnection.Open()
                strStoredProcedure = "spEmptySubject "
                objCommand.CommandText = strStoredProcedure
                objDataReader = objCommand.ExecuteReader()
                dgSubject.DataSource = objDataReader
                dgSubject.DataBind()
                dgSubject.Visible = True
                objCommand.Dispose()
                objDataReader.Close()
            End If  
        End If
    End Sub
    Private Sub ddlPeople_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPeople.SelectedIndexChanged
        'Search for all subjects related to the selected person in this drop down list....
        'Dim strPerson As String
        Dim StrSql As String
        strPerson = ddlPeople.SelectedItem.Value
        ViewState("P") = strPerson

        StrSql = "Select SubjectDesc FROM Subject "
        StrSql = StrSql & "WHERE (Contactid1='" + strPerson + "' or Contactid2='" + strPerson + "' or Contactid3='" + strPerson + "' or Contactid4='" + strPerson + "' or Contactid5='" + strPerson + "' "
        StrSql = StrSql & "or Contactid6='" + strPerson + "' OR Contactid7='" + strPerson + "' or Contactid8='" + strPerson + "' or Contactid9='" + strPerson + "' or "
        StrSql = StrSql & "Contactid10='" + strPerson + "' or Contactid11='" + strPerson + "' or Contactid12='" + strPerson + "')"

        Call CountResults()
        If FillGrid = True Then
            Dim conFillSubject As System.Data.SqlClient.SqlConnection
            conFillSubject = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
            Dim cmdFillSubject As System.Data.SqlClient.SqlCommand
            cmdFillSubject = New System.Data.SqlClient.SqlCommand(StrSql, conFillSubject)
            conFillSubject.Open()
            myDatareader = cmdFillSubject.ExecuteReader

            dgSubject.DataSource = myDatareader
            dgSubject.DataBind()
            dgSubject.Visible = True
            myDatareader.Close()
            conFillSubject.Close()
        End If     
    End Sub
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        RT = Trim(ViewState("RT"))
        RET = Trim(ViewState("RET"))
        If RT = "People" Then
            strPerson = ViewState("P")
            Response.Redirect("PrintSearch.aspx?RT=" & RT & "&P=" & strPerson & "&RET=" & RET)
        Else
            Response.Redirect("PrintSearch.aspx?RT=" & RT & "&RET=" & RET)
        End If
    End Sub
    Public Function CountResults()
        'Count number of results
        RT = ViewState("RT")
        If RT = "People" Then
            Dim StrSql As String
            strPerson = ddlPeople.SelectedItem.Value
            ViewState("P") = strPerson

            StrSql = "Select SubjectDesc FROM Subject "
            StrSql = StrSql & "WHERE (Contactid1='" + strPerson + "' or Contactid2='" + strPerson + "' or Contactid3='" + strPerson + "' or Contactid4='" + strPerson + "' or Contactid5='" + strPerson + "' "
            StrSql = StrSql & "or Contactid6='" + strPerson + "' OR Contactid7='" + strPerson + "' or Contactid8='" + strPerson + "' or Contactid9='" + strPerson + "' or "
            StrSql = StrSql & "Contactid10='" + strPerson + "' or Contactid11='" + strPerson + "' or Contactid12='" + strPerson + "')"

            Dim conFillSubject As System.Data.SqlClient.SqlConnection
            conFillSubject = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
            Dim cmdFillSubject As System.Data.SqlClient.SqlCommand
            cmdFillSubject = New System.Data.SqlClient.SqlCommand(StrSql, conFillSubject)
            conFillSubject.Open()
            myDatareader = cmdFillSubject.ExecuteReader
            Dim count As Integer
            count = 0
            Do While myDatareader.Read
                count = count + 1
            Loop
            ViewState("numDGResults") = count
            If count > 0 Then
                lblResults.Text = ""
                FillGrid = True  'bind data
                btnPrint.Visible = True
            Else
                lblResults.Text = "No Results Found"
                FillGrid = False  'don't bind data
                btnPrint.Visible = False
                dgSubject.DataSource = ""
                dgSubject.DataBind()
                dgSubject.Visible = False

                dgCI.DataSource = ""
                dgCI.DataBind()
                dgCI.Visible = False
            End If
            conFillSubject.Close()
            cmdFillSubject.Dispose()
        ElseIf RT = "CI" Then
            Dim strStoredProcedure As String
            Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
            Dim cmdSearch As New SqlClient.SqlCommand(strStoredProcedure, conSearch)
            Dim dtrSearch As SqlClient.SqlDataReader
            strStoredProcedure = "spNoContact "
            conSearch.Open()
            cmdSearch.CommandText = strStoredProcedure
            dtrSearch = cmdSearch.ExecuteReader()
            Dim count As Integer
            count = 0
            Do While dtrSearch.Read
                count = count + 1
            Loop
            ViewState("numDGResults") = count
            If count > 0 Then
                lblResults.Text = ""
                FillGrid = True  'bind data
                btnPrint.Visible = True
            Else
                lblResults.Text = "No Results Found"
                FillGrid = False  'don't bind data
                btnPrint.Visible = False
                dgSubject.DataSource = ""
                dgSubject.DataBind()
                dgSubject.Visible = False

                dgCI.DataSource = ""
                dgCI.DataBind()
                dgCI.Visible = False
            End If
            conSearch.Close()
            cmdSearch.Dispose()
        ElseIf RT = "SI" Then
            Dim strStoredProcedure As String
            Dim conSearch As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
            Dim cmdSearch As New SqlClient.SqlCommand(strStoredProcedure, conSearch)
            Dim dtrSearch As SqlClient.SqlDataReader
            strStoredProcedure = "spEmptySubject "
            conSearch.Open()
            cmdSearch.CommandText = strStoredProcedure
            dtrSearch = cmdSearch.ExecuteReader()
            Dim count As Integer
            count = 0
            Do While dtrSearch.Read
                count = count + 1
            Loop
            ViewState("numDGResults") = count
            If count > 0 Then
                lblResults.Text = ""
                FillGrid = True  'bind data
                btnPrint.Visible = True
            Else
                lblResults.Text = "No Results Found"
                FillGrid = False  'don't bind data
                btnPrint.Visible = False
                dgSubject.DataSource = ""
                dgSubject.DataBind()
                dgSubject.Visible = False

                dgCI.DataSource = ""
                dgCI.DataBind()
                dgCI.Visible = False
            End If
            conSearch.Close()
            cmdSearch.Dispose()
        End If    
    End Function
    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        RET = Trim(ViewState("RET"))
        If RET = "Subj" Then
            'return to subject page
            Response.Redirect("SubjectMaint.aspx")
        ElseIf RET = "Con" Then
            'return to contact page
            Response.Redirect("ContactMaint.aspx")
        End If
    End Sub
End Class
