<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DMSearch.aspx.vb" Inherits="ReferenceLists.DMSearch"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DM Search</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server"/>
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
			<script src="/Global/js/html5shiv.min.js"></script>
			<script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server"/>
			<div class="container home" role="main">
			<table width="100%">
				<tr width="100%">
					<td noWrap width="1%">
						<asp:label id="lblSF" Visible="True" text="Search For:" Runat="server"></asp:label></td>
					<td><asp:radiobuttonlist id="rblSearchType" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
							<asp:ListItem Value="DM" Selected="True">DM</asp:ListItem>
							<asp:ListItem Value="RM">RM</asp:ListItem>
							<asp:ListItem Value="REGREC">Regional Recruiter</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr width="100%">
					<td noWrap width="1%">
						<asp:label id="lblSB" Visible="True" Runat="server" Text="Search By:"></asp:label></td>
					<td noWrap>
						<asp:radiobuttonlist id="rblDM" Runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
							<asp:ListItem Value="Num" Selected="True">District Number</asp:ListItem>
							<asp:ListItem Value="Name">District Manager Name</asp:ListItem>
						</asp:radiobuttonlist>
						<asp:radiobuttonlist id="rblRM" Runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
							<asp:ListItem Selected="True" Value="Num">Region Number</asp:ListItem>
							<asp:ListItem Value="Name">Regional Manager Name</asp:ListItem>
						</asp:radiobuttonlist>
						<asp:RadioButtonList ID="rblREC" Runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
							<asp:ListItem Selected="True" Value="AREA">Recruiter Area</asp:ListItem>
							<asp:ListItem Value="Name">Recruiter Name</asp:ListItem>
						</asp:RadioButtonList>
					</td>
				</tr>
				<tr>
					<td colSpan="2">
						<asp:label id="lblNUM" Runat="server" Text="District No."></asp:label>
						<asp:textbox id="txtNUM" Runat="server" Width="50px"></asp:textbox>
						<asp:Label Runat="server" ID="lblArea" text="Area:"></asp:Label>
						<asp:DropDownList Runat="server" ID="ddlRECAREA"></asp:DropDownList>
						<asp:label id="lblFName" text="First Name" Runat="server"></asp:label>
						<asp:textbox id="txtFName" Runat="server"></asp:textbox>&nbsp;
						<asp:label id="lblLName" text="Last Name" Runat="server"></asp:label>
						<asp:textbox id="txtLName" Runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td colSpan="2"><br>
					</td>
				</tr>
				<tr>
					<td colSpan="2">
						<asp:button CssClass="btn btn-danger" id="btnSearch" Runat="server" Text="Search"></asp:button>&nbsp;
						<asp:button CssClass="btn btn-danger" id="btnClear" Runat="server" Text="Clear"></asp:button>&nbsp;
						<asp:button CssClass="btn btn-danger" id="btnALL" Runat="server" Text="Show All DMs"></asp:button></td>
				</tr>
				<tr>
					<td colSpan="2"><br>
					</td>
				</tr>
				<tr>
					<td colSpan="2">
						<asp:label id="lblresults" runat="server" Visible="false" text="Results"></asp:label></td>
				</tr>
				<tr>
					<td colSpan="2">
						<asp:datagrid CssClass="table archtbl" id="dgResults" runat="server" Visible="false" Width="100%"
							AllowPaging="True" GridLines="None" AutoGenerateColumns="False">
							<ItemStyle CssClass="archtbltr"></ItemStyle>
							<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
							<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="district_cd" HeaderText="DM #"></asp:BoundColumn>
								<asp:BoundColumn DataField="namepreferred" HeaderText="First Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="namelast" HeaderText="Last Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="extension1" HeaderText="Ext."></asp:BoundColumn>
								<asp:BoundColumn DataField="blackberry_pin" HeaderText="Blackberry Pin"></asp:BoundColumn>
								<asp:BoundColumn DataField="office_location" HeaderText="Office"></asp:BoundColumn>
								<asp:BoundColumn DataField="office_number" HeaderText="Office Number"></asp:BoundColumn>
								<asp:BoundColumn DataField="fax_number" HeaderText="Fax Number"></asp:BoundColumn>
								<asp:BoundColumn DataField="email" HeaderText="E-Mail"></asp:BoundColumn>
							</Columns>
							<PagerStyle ForeColor="White"></PagerStyle>
						</asp:datagrid>
					</td>
				</tr>
				<tr>
					<td colspan='2'>
						<asp:datagrid CssClass="table archtbl" id="DGREG" runat="server" Visible="false" Width="100%" AutoGenerateColumns="False"
							PageSize="15" GridLines="None" AllowPaging="True">
							<ItemStyle CssClass="archtbltr"></ItemStyle>
							<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
							<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="region_cd" HeaderText="REG #"></asp:BoundColumn>
								<asp:BoundColumn DataField="namepreferred" HeaderText="First Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="namelast" HeaderText="Last Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="extension1" HeaderText="Ext."></asp:BoundColumn>
								<asp:BoundColumn DataField="blackberry_pin" HeaderText="Blackberry Pin"></asp:BoundColumn>
								<asp:BoundColumn DataField="office_number" HeaderText="Office Number"></asp:BoundColumn>
								<asp:BoundColumn DataField="fax_number" HeaderText="Fax Number"></asp:BoundColumn>
								<asp:BoundColumn DataField="email" HeaderText="E-Mail"></asp:BoundColumn>
							</Columns>
							<PagerStyle ForeColor="White"></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td colspan='2'>
						<asp:datagrid CssClass="table archtbl" id="dgRECR" runat="server" Visible="false" Width="100%" AutoGenerateColumns="False"
							PageSize="15" GridLines="None" AllowPaging="True">
							<ItemStyle CssClass="archtbltr"></ItemStyle>
							<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
							<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="recruiter_area" HeaderText="Area"></asp:BoundColumn>
								<asp:BoundColumn DataField="namepreferred" HeaderText="First Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="namelast" HeaderText="Last Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="extension1" HeaderText="Ext."></asp:BoundColumn>
								<asp:BoundColumn DataField="blackberry_pin" HeaderText="Blackberry Pin"></asp:BoundColumn>
								<asp:BoundColumn DataField="fax_number" HeaderText="Fax Number"></asp:BoundColumn>
								<asp:BoundColumn DataField="email" HeaderText="E-Mail"></asp:BoundColumn>
							</Columns>
							<PagerStyle ForeColor="White"></PagerStyle>
						</asp:datagrid>
					</td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
