<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Buyer.aspx.vb" Inherits="ReferenceLists.Buyer"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Buyer</title>
		<meta content="False" name="vs_showGrid">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server">
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
			<script src="/Global/js/html5shiv.min.js"></script>
			<script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body text="white" onload="self.focus()" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server"/>
			<div class="container home" role="main">
			<table width="100%">
				<TBODY>
					<tr width="100%">
						<td width="100%"><asp:datagrid id="DataGrid1" runat="server" ItemStyle-CssClass="archtbltr" AlternatingItemStyle-CssClass="archtblalttr"
								HeaderStyle-CssClass="archtblhdr" Width="100%" Height="184px" GridLines="Vertical" CssClass="table archtbl"
								AutoGenerateColumns="False">
								<ItemStyle CssClass="archtbltr"></ItemStyle>
								<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
								<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
								<Columns>
									<asp:BoundColumn DataField="DeptID" HeaderText="Class" HeaderStyle-CssClass="archtblhdr">
										<HeaderStyle Width="2%"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Description" HeaderText="Department" HeaderStyle-CssClass="archtblhdr">
										<HeaderStyle Width="11%"></HeaderStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderStyle-CssClass="archtblhdr" HeaderStyle-Width="7%">
										<HeaderTemplate>
											Assistant Buyer
										</HeaderTemplate>
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
										<ItemTemplate>
											<a href='mailto:<%# Container.dataitem("AsstBuyerEmail") %>'>
												<asp:Label Runat="server" ID="Label1" Text='<%# Container.dataitem("asstBuyer") %>'>
												</asp:Label></a>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="AsstBuyerExt" HeaderText="Ext" HeaderStyle-CssClass="archtblhdr">
										<HeaderStyle Width="6%"></HeaderStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderStyle-Width="7%" HeaderStyle-CssClass="archtblhdr">
										<HeaderTemplate>
											Associate Buyer
										</HeaderTemplate>
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
										<ItemTemplate>
											<a href='mailto:<%# Container.dataitem("associateBuyerEmail") %>'>
												<asp:Label Runat="server" ID="Label2" Text='<%# Container.dataitem("associateBuyer") %>'>
												</asp:Label></a>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="AssociateBuyerExt" HeaderText="Ext" HeaderStyle-CssClass="archtblhdr">
										<HeaderStyle Width="6%" Wrap="False"></HeaderStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderStyle-CssClass="archtblhdr" HeaderStyle-Width="7%">
										<HeaderTemplate>
											Buyer
										</HeaderTemplate>
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
										<ItemTemplate>
											<a href='mailto:<%# Container.dataitem("BuyerEmail") %>'>
												<asp:Label Runat="server" ID="Label3" Text='<%# Container.dataitem("Buyer") %>'>
												</asp:Label></a>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="BuyerExt" HeaderText="Ext" HeaderStyle-CssClass="archtblhdr">
										<HeaderStyle Width="6%" Wrap="False"></HeaderStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Name" HeaderStyle-CssClass="archtblhdr" HeaderStyle-Width="7%">
										<HeaderTemplate>
											DVP/DMM
										</HeaderTemplate>
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
										<ItemTemplate>
											<a href='mailto:<%# Container.dataitem("DVPEmail") %>'>
												<asp:Label Runat="server" ID="Label4" Text='<%# Container.dataitem("DVP") %>'>
												</asp:Label></a>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="DVPExt" HeaderText="Ext" HeaderStyle-CssClass="archtblhdr">
										<HeaderStyle Width="6%" Wrap="False"></HeaderStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Name" HeaderStyle-CssClass="archtblhdr" HeaderStyle-Width="7%">
										<HeaderTemplate>
											VP/GMM
										</HeaderTemplate>
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
										<ItemTemplate>
											<a href='mailto:<%# Container.dataitem("VPGMMEmail") %>'>
												<asp:Label Runat="server" ID="Label5" Text='<%# Container.dataitem("VPGMM") %>'>
												</asp:Label></a>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="VPGMMExt" HeaderText="Ext" HeaderStyle-CssClass="archtblhdr">
										<HeaderStyle Width="6%" Wrap="False"></HeaderStyle>
									</asp:BoundColumn>
								</Columns>
							</asp:datagrid></td>
					</tr>
				</TBODY>
			</table>
			<asp:panel id="generalQuest" Runat="server">
				<TABLE id="Table1">
					<TR>
						<TD>
							<asp:Label id="Quest" Runat="server">General Merchandising Questions:</asp:Label></TD>
					</TR>
					<%
					for intCounter = 0 to ArrayListMerchant.count -1
						 Dim i As Integer
						i = InStr(ArrayListMerchant(intCounter), "test", CompareMethod.Text)
						strStart  = Mid(ArrayListMerchant(intCounter),1,i-1)
						Dim strTot As String = Mid(ArrayListMerchant(intCounter), i + 5) %>
					<TR>
						<TD><FONT size="2"><%=strStart%></FONT></TD>
					</TR>
					<TR>
						<TD><FONT size="1"><%=strTot%></FONT></TD>
					</TR>
					<%	next %>
				</TABLE>
			</asp:panel><BR>
			<asp:panel id="FitQuest" Runat="server">
				<TABLE id="Table2">
					<TR>
						<TD>
							<asp:Label id="FirCont" Runat="server">Fit Quality Control:</asp:Label></TD>
					</TR>
					<%
							
							For inti = 0 To ds.Tables(0).Rows.Count -1
							   Dim x As String = ds.Tables(0).Rows(inti)("Name") + " ext. " + ds.Tables(0).Rows(inti)("extension1")
							   %>
					<TR>
						<TD><FONT size="2"><%=x%></FONT></TD>
					</TR>
					<%Next
						%>
				</TABLE>
			</asp:panel>
			<table align="right">
				<tr>
					<td><input class="btn btn-danger" onclick="window.print()" type="button" value="Print" name="btnPrint">
						<input class="btn btn-danger" onclick="window.top.close()" type="button" value="Close" name="btnClose">
					</td>
				</tr>
			</table>
			<table align="left">
				<tr>
					<td><asp:label id="NewLa" Runat="server">* New items we should carry</asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="Label6" Runat="server">* Fit, Quality, Comment</asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="Label7" Runat="server">* Product Knowledge</asp:label></td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
