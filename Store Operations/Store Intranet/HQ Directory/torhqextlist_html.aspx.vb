Imports System.Data.SqlClient
Public Class torhqextlist_html
    Inherits System.Web.UI.Page
    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm
    Dim conGetRecs As System.Data.SqlClient.SqlConnection
    Public strSQL As String
    Dim cmdFillRecs As System.Data.SqlClient.SqlCommand
    Public dt As New DataTable()
    Dim da As New SqlDataAdapter()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        strSQL = "SELECT taUltimateEmployee.NameFirst AS First, taUltimateEmployee.NameLast AS Last, "
        strSQL &= "Contact.Extension1 AS 'Ext.',  Contact.Position_DTL_Desc AS Position "
        strSQL &= "FROM Contact Contact INNER JOIN "
        strSQL &= "taUltimateEmployee taUltimateEmployee ON Contact.EmpID = taUltimateEmployee.EmployeeID "
        strSQL &= "WHERE     (taUltimateEmployee.Region = 'HQ') AND (Contact.Position_DTL_Desc LIKE "
        strSQL &= "'TORRID%') AND (Contact.Position_DTL_Desc NOT LIKE 'DISTRICT%') OR "
        strSQL &= " (Contact.Position_DTL_Desc LIKE 'TORRID%') AND (Contact.Position_DTL_Desc "
        strSQL &= "NOT LIKE 'DISTRICT%') AND (Contact.Office_Location = 'HQ') OR "
        strSQL &= "(taUltimateEmployee.Region = 'HQ') AND (Contact.Position_DTL_Desc NOT LIKE "
        strSQL &= "'DISTRICT%') AND "
        strSQL &= "(taUltimateEmployee.Dept_Desc LIKE 'TORRID%') OR "
        strSQL &= "(Contact.Position_DTL_Desc NOT LIKE 'DISTRICT%') AND (Contact.Office_Location "
        strSQL &= "= 'HQ') AND (taUltimateEmployee.Dept_Desc LIKE 'TORRID%') "
        strSQL &= "ORDER BY taUltimateEmployee.NameFirst, taUltimateEmployee.NameLast"

        conGetRecs = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        cmdFillRecs = New System.Data.SqlClient.SqlCommand(strSQL, conGetRecs)

        conGetRecs.Open()
        Page.DataBind()
        conGetRecs.Close()
        dt = New DataTable("recs")
        da = New SqlDataAdapter(strSQL, conGetRecs)
        da.Fill(dt)
        conGetRecs.Close()
    End Sub

    Enum EntireTableRows As Integer
        zeroBased = 50
        oneBased = 51
    End Enum

End Class
