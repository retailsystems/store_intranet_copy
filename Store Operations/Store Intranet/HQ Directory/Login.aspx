<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Login.aspx.vb" Inherits="DirectoryMaint.Login"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Login</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--
		<script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
		
		
		-->
		<script>
			function trapKey(e) 

			{ 
				if ( e == 13 ) {document.all.<%=btnSubmit.ClientID%>.click();}
			} 
	
		</script>
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/hottopic.css" type="text/css"
			rel="stylesheet" runat="server">
	</HEAD>
	<body id="pageBody" onkeydown="if(event.keyCode == 13){document.getElementById('btnSubmit').click();}"
		scroll="no" runat="server">
		<form id="frmTransferHome" method="post" runat="server">
			<iframe class="Header_Frame" id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0"
				src="http://172.16.125.2/Header/Header.aspx?mode=hottopic" frameBorder="0" width="100%"
				scrolling="no" runat="server"></iframe>
			<table height="100%" width="100%">
				<tr>
					<td height="1"></td>
				</tr>
				<tr>
					<td align="center">
						<table>
							<tr id="trEmpId" runat="server">
								<td class="normal">Employee Id:</td>
								<td><asp:textbox id="txtEmpId" tabIndex="1" runat="server" TextMode="Password"></asp:textbox></td>
								<td><asp:button id="btnSubmit" tabIndex="1" runat="server" Text="Submit" CssClass="b1"></asp:button>&nbsp;
									<asp:button id="btnHome" tabIndex="1" runat="server" Text="Home" CssClass="b1"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
		<script language="javascript">
		document.forms[0].txtEmpId.focus();	
		</script>
	</body>
</HTML>
