Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data.OracleClient
Imports System.Data.OleDb
Imports System.Data.SqlTypes
Imports System.Object
Imports System
Imports System.Collections
Public Structure Employees

    Public empID As Integer
    Public Name As String
    Public email As String
    Public jobCode As String
    Public extension1 As String
    Public subjectDesc As String
    Public Sub New(ByVal Name As String, ByVal email As String, ByVal jobcode As String, ByVal extension1 As String, ByVal SubjectDesc As String)
        Me.Name = Name
        Me.email = email
        Me.jobCode = jobcode
        Me.extension1 = extension1
        Me.subjectDesc = SubjectDesc
    End Sub
    Public Overrides Function tostring() As String
        Return empID & Name & email & jobCode & extension1
    End Function
End Structure
Public Class clsBuyerDAL

    Public Shared Function getBuyerDataTable() As DataTable
        Dim cnnPLSQL As OracleConnection   ' declare the connection for oracle
        Dim strConn, strOraSQL, strMsg, strConnSQL As String
        Dim OracleCmd As OracleCommand
        Dim strDeptCD As Integer
        Dim strMode As String
        Dim colArrayList As New ArrayList()
        ' sql stuff
        Dim strSQL As String
        Dim oReader As SqlDataReader
        Dim sqlCmd As SqlCommand
        Dim sqlConn As SqlConnection
        Dim strName, strJobCode As String
        Dim counter, intMode, i As Integer
        Dim strAstName, strASTEmail, strastExt As String  ' declaration for assistant buyer
        Dim strBuyName, strBuyEmail, strBuyext As String  ' declaration for Buy
        Dim strascBuyName, strASCBuyEmail, strascBuyExt As String  ' desclaration for associate buyer
        Dim strDMMName, strDMMEmail, strDMMExt As String  ' declaration for DMM
        Dim strVPGMMName, strVPGMMEmail, strVPGMMExt As String ' declaration for VP/GMM
        Dim flagAstBuy, flagBuy, flagAscBuy, flagDMM, flagVPGMM As Boolean
        flagAstBuy = False
        flagBuy = False
        flagAscBuy = False
        flagDMM = False
        flagVPGMM = False
        counter = 0
        Dim strDeptMin As String
        Dim strDeptMax As String
        Dim strDeptAddition As String

        ' query to get the Dept ID's from oracle
        strOraSQL = " Select Dept_CD, DES from dept "
        strMode = ConfigurationSettings.AppSettings("Mode")
        intMode = InStr(strMode, "=", CompareMethod.Text)
        strMode = Mid(strMode, intMode + 1, 5)
        strDeptMin = ConfigurationSettings.AppSettings("minDept")
        strDeptMax = ConfigurationSettings.AppSettings("maxDept")
        strDeptAddition = ConfigurationSettings.AppSettings("DeptAddition")

        If strDeptAddition.Length > 0 Then
            strOraSQL = strOraSQL & " where dept_cd >= " & strDeptMin & " and dept_cd <= " & strDeptMax
        Else
            strOraSQL = strOraSQL & " where (dept_cd >= " & strDeptMin & " and dept_cd <= " & strDeptMax & " or dept_cd in (" & strDeptAddition & "))"
        End If

        'If Trim(UCase(strMode)) = "HOTTO" Then
        '    strOraSQL = strOraSQL & " where dept_cd >= 1 and dept_cd <= 49"
        '    'strOraSQL = strOraSQL & " where dept_cd = 1"
        'ElseIf Trim(UCase(strMode)) = "TORRI" Then
        '    strOraSQL = strOraSQL & " where dept_cd >= 50 and dept_cd <= 98"
        '    ' strOraSQL = strOraSQL & " where dept_cd = 56"
        'End If
        strOraSQL = strOraSQL & "   Order  by dept_cd"
        Try
            cnnPLSQL = New OracleConnection
            strConn = ConfigurationSettings.AppSettings("strORAConnection")
            strConnSQL = ConfigurationSettings.AppSettings("strSQLConn")
            ' created a datatable that will contain the values both from dapartment table and Subject table
            Dim dtBuyer As New DataTable("Buyer")
            dtBuyer.Columns.Add("DeptID", GetType(String))  '0
            dtBuyer.Columns.Add("Description", GetType(String)) '1
            dtBuyer.Columns.Add("AsstBuyer", GetType(String))  '2
            dtBuyer.Columns.Add("AsstBuyerExt", GetType(String)) '3
            dtBuyer.Columns.Add("AsstBuyerEmail", GetType(String)) '4
            dtBuyer.Columns.Add("AssociateBuyer", GetType(String)) '5
            dtBuyer.Columns.Add("AssociateBuyerExt", GetType(String)) '6
            dtBuyer.Columns.Add("AssociateBuyerEmail", GetType(String)) '7
            dtBuyer.Columns.Add("Buyer", GetType(String)) '8
            dtBuyer.Columns.Add("BuyerExt", GetType(String)) '9
            dtBuyer.Columns.Add("BuyerEmail", GetType(String)) '10
            dtBuyer.Columns.Add("DVP", GetType(String))  '11
            dtBuyer.Columns.Add("DVPExt", GetType(String)) '12
            dtBuyer.Columns.Add("DVPEmail", GetType(String)) '13
            dtBuyer.Columns.Add("VPGMM", GetType(String))  '14
            dtBuyer.Columns.Add("VPGMMExt", GetType(String)) '15
            dtBuyer.Columns.Add("VPGMMEmail", GetType(String)) '16
            cnnPLSQL.ConnectionString = strConn
            ' open the connection 
            cnnPLSQL.Open()
            OracleCmd = New OracleCommand(strOraSQL, cnnPLSQL)
            OracleCmd.CommandType = CommandType.Text
            ' executing the oracle reader
            Dim oReaderOracle As OracleDataReader = OracleCmd.ExecuteReader()

            '    sqlConn = New SqlConnection(strConnSQL)
            '    sqlConn.Open()
            ' Reading each line from oracle datatbase and then putting it in the datatable and then reading the line
            ' from sql dataBase and appending it in the datatable.
            While oReaderOracle.Read()
                Dim EmpNum As Integer

                strDeptCD = oReaderOracle(0)

                colArrayList = CalcDeptNumFromSub(strDeptCD)

                Dim BuyerRow As DataRow
                Dim employees As Employees
                BuyerRow = dtBuyer.NewRow
                For i = 0 To colArrayList.Count - 1
                    employees = colArrayList.Item(i)

                    Dim strJobCode1 As String

                    strJobCode1 = employees.jobCode
                    Dim strEXt As String = employees.extension1
                    Dim strEmail As String = employees.email
                    Dim strName1 As String = employees.Name
                    Dim strSubjectDesc As String = employees.subjectDesc
                    ' Manually stored the value in the DataTable
                    If InStr(strSubjectDesc, "ASSISTANT BUYER", CompareMethod.Text) Then
                        BuyerRow(0) = strDeptCD
                        BuyerRow(1) = oReaderOracle(1)
                        strAstName = strAstName & Trim(strName1) & "/ "   ' BuyerRow(2)
                        strastExt = strastExt & Trim(strEXt) & "/ "
                        strASTEmail = strASTEmail & Trim(strEmail) & "; "
                        flagAstBuy = True

                    ElseIf InStr(strSubjectDesc, "ASSOCIATE BUYER", CompareMethod.Text) Then
                        BuyerRow(0) = strDeptCD
                        BuyerRow(1) = oReaderOracle(1)
                        strascBuyName = strascBuyName & Trim(strName1) & "/ "
                        strascBuyExt = strascBuyExt & Trim(strEXt) & "/ "
                        strASCBuyEmail = strASCBuyEmail & Trim(strEmail) & "; "
                        flagAscBuy = True
                    ElseIf InStr(strSubjectDesc, "BUYER", CompareMethod.Text) Then
                        BuyerRow(0) = strDeptCD
                        BuyerRow(1) = oReaderOracle(1)
                        strBuyName = strBuyName & Trim(strName1) & "/ "
                        strBuyext = strBuyext & Trim(strEXt) & "/ "
                        strBuyEmail = strBuyEmail & Trim(strEmail) & "; "
                        flagBuy = True
                    ElseIf InStr(strSubjectDesc, "DVP/DMM", CompareMethod.Text) Then
                        BuyerRow(0) = strDeptCD
                        BuyerRow(1) = oReaderOracle(1)
                        strDMMName = strDMMName & Trim(strName1) & "/ "
                        strDMMExt = strDMMExt & Trim(strEXt) & "/ "
                        strDMMEmail = strDMMEmail & Trim(strEmail) & "; "
                        flagDMM = True
                    ElseIf InStr(strSubjectDesc, "VP/GMM", CompareMethod.Text) Then
                        BuyerRow(0) = strDeptCD
                        BuyerRow(1) = oReaderOracle(1)
                        strDMMExt = strDMMExt & Trim(strEXt) & "/ "
                        strVPGMMName = strVPGMMName & Trim(strName1) & "/ "
                        strVPGMMExt = strVPGMMExt & Trim(strEXt) & "/ "
                        strVPGMMEmail = strVPGMMEmail & Trim(strEmail) & "; "
                        flagVPGMM = True
                    End If
                    'Select Case Trim(strJobCode1)
                    '    Case "ASTBUY"
                    '        BuyerRow(0) = strDeptCD
                    '        BuyerRow(1) = oReaderOracle(1)
                    '        strAstName = strAstName & Trim(strName1) & "/ "   ' BuyerRow(2)
                    '        strastExt = strastExt & Trim(strEXt) & "/ "
                    '        strASTEmail = strASTEmail & Trim(strEmail) & "; "
                    '        flagAstBuy = True
                    '    Case "BUY"
                    '        BuyerRow(0) = strDeptCD
                    '        BuyerRow(1) = oReaderOracle(1)
                    '        strBuyName = strBuyName & Trim(strName1) & "/ "
                    '        strBuyext = strBuyext & Trim(strEXt) & "/ "
                    '        strBuyEmail = strBuyEmail & Trim(strEmail) & "; "
                    '        flagBuy = True
                    '    Case "ASCBUY"
                    '        BuyerRow(0) = strDeptCD
                    '        BuyerRow(1) = oReaderOracle(1)

                    '        strascBuyName = strascBuyName & Trim(strName1) & "/ "

                    '        strascBuyExt = strascBuyExt & Trim(strEXt) & "/ "

                    '        strASCBuyEmail = strASCBuyEmail & Trim(strEmail) & "; "
                    '        flagAscBuy = True
                    '    Case "DMM"
                    '        BuyerRow(0) = strDeptCD
                    '        BuyerRow(1) = oReaderOracle(1)

                    '        strDMMName = strDMMName & Trim(strName1) & "/ "

                    '        strDMMExt = strDMMExt & Trim(strEXt) & "/ "

                    '        strDMMEmail = strDMMEmail & Trim(strEmail) & "; "
                    '        flagDMM = True
                    '    Case "VPGMM"
                    '        BuyerRow(0) = strDeptCD
                    '        BuyerRow(1) = oReaderOracle(1)

                    '        strDMMExt = strDMMExt & Trim(strEXt) & "/ "
                    '        strVPGMMName = strVPGMMName & Trim(strName1) & "/ "

                    '        strVPGMMExt = strVPGMMExt & Trim(strEXt) & "/ "

                    '        strVPGMMEmail = strVPGMMEmail & Trim(strEmail) & "; "
                    '        flagVPGMM = True

                    'End Select


                Next
                If strAstName <> "" Then
                    BuyerRow(2) = strAstName.Substring(0, strAstName.Length - 2)
                End If
                If strastExt <> "" Then
                    BuyerRow(3) = strastExt.Substring(0, strastExt.Length - 2)
                End If
                If strASTEmail <> "" Then
                    BuyerRow(4) = strASTEmail.Substring(0, strASTEmail.Length - 2)
                End If
                If strascBuyName <> "" Then
                    BuyerRow(5) = strascBuyName.Substring(0, strascBuyName.Length - 2)
                End If
                If strascBuyExt <> "" Then
                    BuyerRow(6) = strascBuyExt.Substring(0, strascBuyExt.Length - 2)
                End If
                If strASCBuyEmail <> "" Then
                    BuyerRow(7) = strASCBuyEmail.Substring(0, strASCBuyEmail.Length - 2)
                End If
                If strBuyName <> "" Then
                    BuyerRow(8) = strBuyName.Substring(0, strBuyName.Length - 2)
                End If
                If strBuyext <> "" Then
                    BuyerRow(9) = strBuyext.Substring(0, strBuyext.Length - 2)
                End If
                If strBuyEmail <> "" Then
                    BuyerRow(10) = strBuyEmail.Substring(0, strBuyEmail.Length - 2)
                End If
                If strDMMName <> "" Then
                    BuyerRow(11) = strDMMName.Substring(0, strDMMName.Length - 2)
                End If
                If strDMMExt <> "" Then
                    BuyerRow(12) = strDMMExt.Substring(0, strDMMExt.Length - 2)
                End If
                If strDMMEmail <> "" Then
                    BuyerRow(13) = strDMMEmail.Substring(0, strDMMEmail.Length - 2)
                End If
                If strVPGMMName <> "" Then
                    BuyerRow(14) = strVPGMMName.Substring(0, strVPGMMName.Length - 2)
                End If
                If strVPGMMExt <> "" Then
                    BuyerRow(15) = strVPGMMExt.Substring(0, strVPGMMExt.Length - 2)
                End If
                If strVPGMMEmail <> "" Then
                    BuyerRow(16) = strVPGMMEmail.Substring(0, strVPGMMEmail.Length - 2)
                End If

                If flagAstBuy = True Or flagBuy = True Or flagAscBuy = True Or flagDMM = True Or flagVPGMM = True Then
                    dtBuyer.Rows.Add(BuyerRow)
                End If
                flagAstBuy = False
                flagBuy = False
                flagAscBuy = False
                flagDMM = False
                flagVPGMM = False
                colArrayList.Clear()
                strAstName = ""
                strastExt = ""
                strASTEmail = ""
                strascBuyName = ""
                strascBuyExt = ""
                strASCBuyEmail = ""
                strBuyName = ""
                strBuyext = ""
                strBuyEmail = ""
                strDMMName = ""
                strDMMExt = ""
                strDMMEmail = ""
                strVPGMMName = ""
                strVPGMMExt = ""
                strVPGMMEmail = ""
            End While


            oReaderOracle.Close()
            cnnPLSQL.Close()

            Return dtBuyer
        Catch ex As Exception
            'sqlConn.Close()
            ' oReader.Close()

            cnnPLSQL.Close()
            strMsg = ex.Message()
        End Try

    End Function
    Public Shared Function CalcDeptNumFromSub(ByVal strDeptCD As Integer) As ArrayList
        Dim sqlCmd As SqlCommand
        Dim sqlConnF As SqlConnection
        Dim strMode As String
        Dim BuyerEmp As New ArrayList()
        Dim secondFind(5) As String
        Dim oReaderF As SqlDataReader
        Dim TotEmployee, strContractID2, strContractID3, strContractID4, strContractID5, strContractID6, strContractID7, strContractID8, strContractID9, strContractID10, strContractID11, strContractID12 As String
        Dim flag As Boolean
        flag = False
        Dim strConnF, strSQLF, strName, strEmail, strJobCode, strExtension1, strSubjectDesc As String
        Dim strNameASTBUY, strNameBUY, strASCBUY, strDMM, strVPGMM As String
        Dim strMSG As String
        Dim strkeyWord, strKey, strPartialKeyWord As String
        Dim intKeyword, intEmpID As Integer
        Dim newEmployee As New Employees()
        Try
            intEmpID = 0

            strConnF = ConfigurationSettings.AppSettings("strSQLConn")

            sqlConnF = New SqlConnection(strConnF)
            sqlConnF.Open()
            sqlCmd = New SqlCommand("sp_GetjobCode", sqlConnF)
            sqlCmd.CommandType = CommandType.StoredProcedure

            oReaderF = sqlCmd.ExecuteReader()
            While oReaderF.Read()
                strkeyWord = oReaderF(5).ToString()
                intKeyword = InStr(strkeyWord, "Dept", CompareMethod.Text)
                ' strPartialKeyWord = Mid(strkeyWord, intKeyword + 5, 2)
                If intKeyword = 0 Then
                    intKeyword = InStr(strkeyWord, "Department", CompareMethod.Text)
                    If intKeyword <> 0 Then
                        strPartialKeyWord = Mid(strkeyWord, intKeyword + 11, 2)
                    End If
                Else
                    strPartialKeyWord = Mid(strkeyWord, intKeyword + 5, 2)
                End If
                If (Trim(strPartialKeyWord)).Length < 2 Then
                    strPartialKeyWord = Mid(strkeyWord, intKeyword + 5, 3)
                End If
                If Trim(strPartialKeyWord) <> "" Then
                    If Trim(strPartialKeyWord) = strDeptCD Then
                        strJobCode = oReaderF(3)
                        If strName <> oReaderF(0) Then
                            strName = oReaderF(0)
                            strEmail = oReaderF(1)

                            strExtension1 = oReaderF(3)
                            strSubjectDesc = oReaderF(5)
                            strContractID2 = oReaderF(6)
                            strContractID3 = oReaderF(7)
                            strContractID4 = oReaderF(8)
                            strContractID5 = oReaderF(9)
                            strContractID6 = oReaderF(10)
                            strContractID7 = oReaderF(11)
                            strContractID8 = oReaderF(12)
                            strContractID9 = oReaderF(13)
                            strContractID10 = oReaderF(14)
                            strContractID11 = oReaderF(15)
                            strContractID12 = oReaderF(16)




                            BuyerEmp.Add(New Employees(strName, strEmail, strJobCode, strExtension1, strSubjectDesc))
                            If Trim(strContractID2) <> "" Then
                                secondFind = FindSecondContact(strContractID2)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExtension1 = secondFind(3)
                                strSubjectDesc = secondFind(5)
                                BuyerEmp.Add(New Employees(strName, strEmail, strJobCode, strExtension1, strSubjectDesc))
                            End If
                            If Trim(strContractID3) <> "" Then
                                secondFind = FindSecondContact(strContractID3)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExtension1 = secondFind(3)
                                strSubjectDesc = secondFind(5)
                                BuyerEmp.Add(New Employees(strName, strEmail, strJobCode, strExtension1, strSubjectDesc))
                            End If
                            If Trim(strContractID4) <> "" Then
                                secondFind = FindSecondContact(strContractID4)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExtension1 = secondFind(3)
                                strSubjectDesc = secondFind(5)
                                BuyerEmp.Add(New Employees(strName, strEmail, strJobCode, strExtension1, strSubjectDesc))
                            End If
                            If Trim(strContractID5) <> "" Then
                                secondFind = FindSecondContact(strContractID5)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExtension1 = secondFind(3)
                                strSubjectDesc = secondFind(5)
                                BuyerEmp.Add(New Employees(strName, strEmail, strJobCode, strExtension1, strSubjectDesc))
                            End If
                            If Trim(strContractID6) <> "" Then
                                secondFind = FindSecondContact(strContractID6)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExtension1 = secondFind(3)
                                strSubjectDesc = secondFind(5)
                                BuyerEmp.Add(New Employees(strName, strEmail, strJobCode, strExtension1, strSubjectDesc))
                            End If
                            If Trim(strContractID7) <> "" Then
                                secondFind = FindSecondContact(strContractID7)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExtension1 = secondFind(3)
                                strSubjectDesc = secondFind(5)
                                BuyerEmp.Add(New Employees(strName, strEmail, strJobCode, strExtension1, strSubjectDesc))
                            End If
                            If Trim(strContractID8) <> "" Then
                                secondFind = FindSecondContact(strContractID8)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExtension1 = secondFind(3)
                                strSubjectDesc = secondFind(5)
                                BuyerEmp.Add(New Employees(strName, strEmail, strJobCode, strExtension1, strSubjectDesc))
                            End If
                            If Trim(strContractID9) <> "" Then
                                secondFind = FindSecondContact(strContractID9)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExtension1 = secondFind(3)
                                strSubjectDesc = secondFind(5)
                                BuyerEmp.Add(New Employees(strName, strEmail, strJobCode, strExtension1, strSubjectDesc))
                            End If
                            If Trim(strContractID10) <> "" Then
                                secondFind = FindSecondContact(strContractID10)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExtension1 = secondFind(3)
                                strSubjectDesc = secondFind(5)
                                BuyerEmp.Add(New Employees(strName, strEmail, strJobCode, strExtension1, strSubjectDesc))
                            End If
                            If Trim(strContractID11) <> "" Then
                                secondFind = FindSecondContact(strContractID11)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExtension1 = secondFind(3)
                                strSubjectDesc = secondFind(5)
                                BuyerEmp.Add(New Employees(strName, strEmail, strJobCode, strExtension1, strSubjectDesc))
                            End If
                            If Trim(strContractID12) <> "" Then
                                secondFind = FindSecondContact(strContractID12)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExtension1 = secondFind(3)
                                strSubjectDesc = secondFind(5)
                                BuyerEmp.Add(New Employees(strName, strEmail, strJobCode, strExtension1, strSubjectDesc))
                            End If
                           
                        End If
                    End If
                End If
            End While
            oReaderF.Close()
            sqlConnF.Close()
            sqlConnF.Dispose()

            Return BuyerEmp
        Catch ex As Exception
            strMSG = ex.Message
        End Try

    End Function

    Public Shared Function generalMerchant() As ArrayList
        Dim strConn As String
        Dim sqlCmd As SqlCommand
        Dim sqlConn As SqlConnection
        Dim oReader As SqlDataReader
        Dim intEmpID As Integer
        Dim boolFirst As Boolean
        ' Dim Users As New ArrayList()
        Dim intKeyword As Integer
        Dim strPartialKeyWord, strDeptCD As String
        Dim strMerchandise As String
        Dim Users As New ArrayList()
        strConn = ConfigurationSettings.AppSettings("strSQLConn")
        sqlConn = New SqlConnection(strConn)
        sqlConn.Open()
        sqlCmd = New SqlCommand("sp_GeneralMerchandising", sqlConn)
        sqlCmd.CommandType = CommandType.StoredProcedure
        oReader = sqlCmd.ExecuteReader()

        intEmpID = 0
        While oReader.Read()
            Dim strPrefferedName, strExt, strKeyWord, total As String
            Dim strMode As String
            Dim intMode As Integer
            strMode = ConfigurationSettings.AppSettings("Mode")
            intMode = InStr(strMode, "=", CompareMethod.Text)
            strMode = Mid(strMode, intMode + 1, 5)
            If intEmpID <> oReader(0) And boolFirst = True Then
                If strDeptCD.EndsWith(",") Then
                    strDeptCD = strDeptCD.Substring(0, strDeptCD.Length - 1)
                End If
                If strMerchandise.EndsWith(",") Then
                    strMerchandise = strMerchandise.Substring(0, strMerchandise.Length - 1)
                End If
                total = Trim(strPrefferedName) & " " & "ext. " & Trim(strExt) & " " & " for depts. " & Trim(strDeptCD) & " test " & Trim(strMerchandise)
                If Trim(total) <> "ext.   for depts.  test" Then
                    Users.Add(total)
                End If
                strDeptCD = ""
                strPrefferedName = ""
                strExt = ""
                strMerchandise = ""
            End If

            If intEmpID <> oReader(0) Then

                strKeyWord = oReader(5)
                intKeyword = InStr(strKeyWord, "Dept", CompareMethod.Text)
                strPartialKeyWord = Mid(strKeyWord, intKeyword + 5, 2)
                If intKeyword = 0 Then
                    intKeyword = InStr(strKeyWord, "Department", CompareMethod.Text)
                    strPartialKeyWord = Mid(strKeyWord, intKeyword + 11, 2)
                    If strPartialKeyWord.EndsWith(",") Then
                        strPartialKeyWord = strPartialKeyWord.Substring(0, strPartialKeyWord.Length - 1)
                    End If
                Else
                    strPartialKeyWord = Mid(strKeyWord, intKeyword + 5, 2)
                    If strPartialKeyWord.EndsWith(",") Then
                        strPartialKeyWord = strPartialKeyWord.Substring(0, strPartialKeyWord.Length - 1)
                    End If
                End If
                '' write mode code here

                If strPartialKeyWord >= 1 And strPartialKeyWord <= 49 Then



                    ' end of mode code
                    intEmpID = oReader(0)
                    strPrefferedName = oReader(1)
                    strExt = oReader(4)

                    strDeptCD = strDeptCD & strPartialKeyWord + ","
                    ' reading the merchendise
                    Dim intPartialMerchant, intPartialMerchant1, IntCommaGeneral As Integer
                    Dim strpartialMerchant, strCommaGeneral As String
                    intPartialMerchant = InStr(strKeyWord, strPartialKeyWord, CompareMethod.Text)
                    strpartialMerchant = Mid(strKeyWord, intPartialMerchant + 1)
                    intPartialMerchant1 = InStr(strpartialMerchant, strPartialKeyWord, CompareMethod.Text)
                    strCommaGeneral = Mid(strpartialMerchant, intPartialMerchant1 + 1)
                    IntCommaGeneral = InStr(strCommaGeneral, ",", CompareMethod.Text)
                    strMerchandise = strMerchandise & Mid(strCommaGeneral, IntCommaGeneral + 1) + ","
                    strPartialKeyWord = ""
                    strCommaGeneral = ""
                    strpartialMerchant = ""


                    boolFirst = False

                End If
            Else

                strKeyWord = oReader(5)
                intKeyword = InStr(strKeyWord, "Dept", CompareMethod.Text)
                strPartialKeyWord = Mid(strKeyWord, intKeyword + 5, 2)
                If intKeyword = 0 Then
                    intKeyword = InStr(strKeyWord, "Department", CompareMethod.Text)
                    strPartialKeyWord = Mid(strKeyWord, intKeyword + 11, 2)
                    If strPartialKeyWord.EndsWith(",") Then
                        strPartialKeyWord = strPartialKeyWord.Substring(0, strPartialKeyWord.Length - 1)
                    End If
                Else
                    strPartialKeyWord = Mid(strKeyWord, intKeyword + 5, 2)
                    If strPartialKeyWord.EndsWith(",") Then
                        strPartialKeyWord = strPartialKeyWord.Substring(0, strPartialKeyWord.Length - 1)
                    End If
                End If
                If strPartialKeyWord >= 1 And strPartialKeyWord <= 49 Then
                    boolFirst = True
                    strPrefferedName = oReader(1)
                    strExt = oReader(4)
                    strDeptCD = strDeptCD & strPartialKeyWord + ","
                    Dim intPartialMerchant, intPartialMerchant1, IntCommaGeneral As Integer
                    Dim strpartialMerchant, strCommaGeneral As String
                    intPartialMerchant = InStr(strKeyWord, strPartialKeyWord, CompareMethod.Text)
                    strpartialMerchant = Mid(strKeyWord, intPartialMerchant + 1)
                    intPartialMerchant1 = InStr(strpartialMerchant, strPartialKeyWord, CompareMethod.Text)
                    strCommaGeneral = Mid(strpartialMerchant, intPartialMerchant1 + 1)
                    IntCommaGeneral = InStr(strCommaGeneral, ",", CompareMethod.Text)
                    strMerchandise = strMerchandise & Mid(strCommaGeneral, IntCommaGeneral + 1) + ","
                    strPartialKeyWord = ""
                    strCommaGeneral = ""
                    strpartialMerchant = ""
                End If

            End If

        End While
        Return Users

    End Function
    Public Shared Function FitQualtiy() As DataSet
        Try
            Dim connection As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("strSQLConn"))
            Dim strMode As String
            Dim intMode As Integer
            strMode = ConfigurationSettings.AppSettings("Mode")
            intMode = InStr(strMode, "=", CompareMethod.Text)
            strMode = Mid(strMode, intMode + 1, 5)
            'strMode = Mid(strMode, intMode + 1, 4)
            connection.Open()
            Dim command As SqlCommand
       
            ' input parameter
            command = New System.Data.SqlClient.SqlCommand("sp_fitQuality", connection)
            command.CommandType = System.Data.CommandType.StoredProcedure
            Dim Modes As SqlParameter = command.Parameters.Add("@Modes", SqlDbType.VarChar)
            If strMode = "torr" Then
                Modes.Value = "TORRID"
            Else
                Modes.Value = "HOT"
            End If

            Dim adapter As System.Data.SqlClient.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter(command)
            Dim dataset As New System.Data.DataSet()
            adapter.Fill(dataset)

            adapter.Dispose()
            'cleanup...
            command.Dispose()
            connection.Close()
            'return dataset...

            'cleanup...
            command.Dispose()
            connection.Close()
            Return dataset
        Catch ex As Exception

            System.Diagnostics.Debug.WriteLine(ex.Message)
        End Try

    End Function
    Public Shared Function generalMerchantTorrid() As ArrayList
        Dim strConn As String
        Dim sqlCmd As SqlCommand
        Dim sqlConn As SqlConnection
        Dim oReader As SqlDataReader
        Dim intEmpID As Integer
        Dim boolFirst As Boolean
        Dim Users As ArrayList
        Users = New ArrayList()
        Dim intKeyword As Integer
        Dim strPartialKeyWord, strDeptCD As String
        Dim strMerchandise As String
        strConn = ConfigurationSettings.AppSettings("strSQLConn")
        sqlConn = New SqlConnection(strConn)
        sqlConn.Open()
        sqlCmd = New SqlCommand("sp_GeneralMerchandising", sqlConn)
        sqlCmd.CommandType = CommandType.StoredProcedure
        oReader = sqlCmd.ExecuteReader()
        '   intEmpID = FirstEmpID()
        intEmpID = 0
        While oReader.Read()
            Dim strPrefferedName, strExt, strKeyWord, total As String
            Dim strMode As String
            Dim intMode As Integer
            strMode = ConfigurationSettings.AppSettings("Mode")
            intMode = InStr(strMode, "=", CompareMethod.Text)
            strMode = Mid(strMode, intMode + 1, 5)
            If intEmpID <> oReader(0) And boolFirst = True Then
                If strDeptCD.EndsWith(",") Then
                    strDeptCD = strDeptCD.Substring(0, strDeptCD.Length - 1)
                End If
                If strMerchandise.EndsWith(",") Then
                    strMerchandise = strMerchandise.Substring(0, strMerchandise.Length - 1)
                End If
                total = Trim(strPrefferedName) & " " & "ext. " & Trim(strExt) & " " & " for depts. " & Trim(strDeptCD) & " test " & Trim(strMerchandise)
                If Trim(total) <> "ext.   for depts.  test" Then
                    Users.Add(total)
                End If
                strDeptCD = ""
                strPrefferedName = ""
                strExt = ""
                strMerchandise = ""
            End If

            If intEmpID <> oReader(0) Then

                strKeyWord = oReader(5)
                intKeyword = InStr(strKeyWord, "Dept", CompareMethod.Text)
                strPartialKeyWord = Mid(strKeyWord, intKeyword + 5, 2)
                If intKeyword = 0 Then
                    intKeyword = InStr(strKeyWord, "Department", CompareMethod.Text)
                    strPartialKeyWord = Mid(strKeyWord, intKeyword + 11, 2)
                    If strPartialKeyWord.EndsWith(",") Then
                        strPartialKeyWord = strPartialKeyWord.Substring(0, strPartialKeyWord.Length - 1)
                    End If
                Else
                    strPartialKeyWord = Mid(strKeyWord, intKeyword + 5, 2)
                    If strPartialKeyWord.EndsWith(",") Then
                        strPartialKeyWord = strPartialKeyWord.Substring(0, strPartialKeyWord.Length - 1)
                    End If
                End If
                '' write mode code here

                If strPartialKeyWord >= 50 And strPartialKeyWord <= 98 Then



                    ' end of mode code
                    intEmpID = oReader(0)
                    strPrefferedName = oReader(1)
                    strExt = oReader(4)

                    strDeptCD = strDeptCD & strPartialKeyWord + ","
                    ' reading the merchendise
                    Dim intPartialMerchant, intPartialMerchant1, IntCommaGeneral As Integer
                    Dim strpartialMerchant, strCommaGeneral As String
                    intPartialMerchant = InStr(strKeyWord, strPartialKeyWord, CompareMethod.Text)
                    strpartialMerchant = Mid(strKeyWord, intPartialMerchant + 1)
                    intPartialMerchant1 = InStr(strpartialMerchant, strPartialKeyWord, CompareMethod.Text)
                    strCommaGeneral = Mid(strpartialMerchant, intPartialMerchant1 + 1)
                    IntCommaGeneral = InStr(strCommaGeneral, ",", CompareMethod.Text)
                    strMerchandise = strMerchandise & Mid(strCommaGeneral, IntCommaGeneral + 1) + ","
                    strPartialKeyWord = ""
                    strCommaGeneral = ""
                    strpartialMerchant = ""


                    boolFirst = False

                End If
            Else

                strKeyWord = oReader(5)
                intKeyword = InStr(strKeyWord, "Dept", CompareMethod.Text)
                strPartialKeyWord = Mid(strKeyWord, intKeyword + 5, 2)
                If intKeyword = 0 Then
                    intKeyword = InStr(strKeyWord, "Department", CompareMethod.Text)
                    strPartialKeyWord = Mid(strKeyWord, intKeyword + 11, 2)
                    If strPartialKeyWord.EndsWith(",") Then
                        strPartialKeyWord = strPartialKeyWord.Substring(0, strPartialKeyWord.Length - 1)
                    End If
                Else
                    strPartialKeyWord = Mid(strKeyWord, intKeyword + 5, 2)
                    If strPartialKeyWord.EndsWith(",") Then
                        strPartialKeyWord = strPartialKeyWord.Substring(0, strPartialKeyWord.Length - 1)
                    End If
                End If
                If strPartialKeyWord >= 50 And strPartialKeyWord <= 98 Then
                    boolFirst = True
                    strPrefferedName = oReader(1)
                    strExt = oReader(4)
                    strDeptCD = strDeptCD & strPartialKeyWord + ","
                    Dim intPartialMerchant, intPartialMerchant1, IntCommaGeneral As Integer
                    Dim strpartialMerchant, strCommaGeneral As String
                    intPartialMerchant = InStr(strKeyWord, strPartialKeyWord, CompareMethod.Text)
                    strpartialMerchant = Mid(strKeyWord, intPartialMerchant + 1)
                    intPartialMerchant1 = InStr(strpartialMerchant, strPartialKeyWord, CompareMethod.Text)
                    strCommaGeneral = Mid(strpartialMerchant, intPartialMerchant1 + 1)
                    IntCommaGeneral = InStr(strCommaGeneral, ",", CompareMethod.Text)
                    strMerchandise = strMerchandise & Mid(strCommaGeneral, IntCommaGeneral + 1) + ","
                    strPartialKeyWord = ""
                    strCommaGeneral = ""
                    strpartialMerchant = ""
                End If

            End If

        End While
        Return Users

    End Function
    Public Shared Function FindSecondContact(ByVal strContactID2 As String) As Array
        Dim strCons As String
        Dim sqlConn As SqlConnection
        Dim sqlCmd As SqlCommand
        Dim Second(5) As String
        Dim strMSG As String
        Dim oReader As SqlDataReader
        Dim strName, strEmail, strExt As String
        Dim strSQL As String
        Try
            strSQL = ""
            strSQL = "select ta.NamePreferred + ' ' + ta.NameLast as [Name], email, ta.jobCode, "
            strSQL = strSQL & " extension1, keywords, subjectDesc, ContactID2 from taUltimateEmployee ta, contact c, Subject S "
            strSQL = strSQL & " where(ta.employeeID = c.empID And location = '8001')  "
            strSQL = strSQL & " and ( S.ContactID2 = C.empID  or S.ContactID3 = C.empID or S.ContactID4 = C.empID or S.ContactID5 = C.empID or S.ContactID6 = C.empID  or "
            strSQL = strSQL & " S.ContactID7 = C.empID Or S.ContactID8 = C.empID Or S.ContactID9 = C.empID Or S.ContactID10 = C.empID Or S.ContactID11 = C.empID Or S.ContactID12 = C.empID)"
            strSQL = strSQL & " and  ta.employeeID = " & strContactID2

            strCons = ConfigurationSettings.AppSettings("strSQLConn")
            sqlConn = New SqlConnection(strCons)
            sqlConn.Open()
            sqlCmd = New SqlCommand(strSQL, sqlConn)
            sqlCmd.CommandType = CommandType.Text
            oReader = sqlCmd.ExecuteReader()
            While oReader.Read()
                Second(0) = oReader(0)
                Second(1) = oReader(1)
                Second(2) = oReader(2)
                Second(3) = oReader(3)
                Second(4) = oReader(5)
            End While
            Return Second
            oReader.Close()
            sqlConn.Close()
            sqlConn.Dispose()
        Catch ex As Exception
            strMSG = ex.Message
            System.Diagnostics.Debug.WriteLine(strMSG)
        End Try

    End Function

End Class
