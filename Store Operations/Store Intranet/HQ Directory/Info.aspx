<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Info.aspx.vb" Inherits="ReferenceLists.Info"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Helpful Numbers</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server">
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
			<script src="/Global/js/html5shiv.min.js"></script>
			<script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server" />
			<div class="container home" role="main">
			<table width="100%">
				<tr>
					<td noWrap>HR-Admin Team</td>
					<td noWrap>HQ Fax Numbers</td>
					<td noWrap></td>
				</tr>
				<tr>
					<td vAlign="top">
						<table>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Janice</font></td>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold">&nbsp;Ext. 
										2130</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										1, 2 &amp; 3A</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Gracie</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2453</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										3, 4 &amp; 6</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Shakeh</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2454</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										5, 7, Torrid, HQ &amp; DC</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Celina</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2405</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Employment 
										Verification, State Posters</font></td>
							</tr>
							<tr>
								<td noWrap><br>
								</td>
								<td noWrap></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap colSpan="3"><font style="FONT-WEIGHT: bold; FONT-SIZE: small; FONT-FAMILY: Arial">Payroll</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Marti</font></td>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2795</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										1 &amp; 4</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Robert</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2964</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										2 &amp; 7</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Rosie</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2110</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										5, 8 &amp; Torrid</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Melanie</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2201</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										6 &amp; HQ</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial"></font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2120</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										3 &amp; 3A</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Jenn</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2134</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Supervisor</font></td>
							</tr>
							<tr>
								<td noWrap><br>
								</td>
								<td noWrap></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap colSpan="3"><font style="FONT-WEIGHT: bold; FONT-SIZE: small; FONT-FAMILY: Arial">Sales 
										Audit</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Trina</font></td>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2960</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Sales 
										Audit Manager</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Jasmyn</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2418</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Banking 
										Supervisor</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Shannon</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2296</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										1</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Amanda</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2403</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										2 (not D10)</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Vanessa</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2512</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										3A &amp; D10</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Jennifer G</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2691</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										4</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Lorena</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2131</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										5 (not D27)</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Angela</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2383</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Region 
										6 &amp; D27</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Jay</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2418</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Banking</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Marilyn</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2966</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Banking</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><br>
								</td>
								<td noWrap></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap width="1%" colSpan="3"><font style="FONT-WEIGHT: bold; FONT-SIZE: small; FONT-FAMILY: Arial">Regional 
										Loss Prevention</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">Jason 
										Jones</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(888)250-9050</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Pager)</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">Jason 
										Jones </font>
								</td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(972)353-8706</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Office)</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">John 
										Hovart </font>
								</td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(888)274-8556</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Pager)</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">John 
										Hovart </font>
								</td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(813)689-9576</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Office)</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">Saundra 
										Smith </font>
								</td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(888)327-8408</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Pager)</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">Saundra 
										Smith </font>
								</td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(309)637-6877</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Office)</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><br>
								</td>
								<td noWrap></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap width="1%" colSpan="3"><font style="FONT-WEIGHT: bold; FONT-SIZE: small; FONT-FAMILY: Arial">Repair 
										&amp; Maintainance</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">R 
										&amp; M </font>
								</td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(800)240-5335</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Pager)</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">John 
										Gatturna </font>
								</td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(800)209-3320</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Pager)</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">Larry 
										Keel </font>
								</td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(305)648-0194</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Office)</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">Pam 
										Hedrick </font>
								</td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(972)691-04952</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Office)</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><br>
								</td>
								<td noWrap></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap width="1%" colSpan="3"><font style="FONT-WEIGHT: bold; FONT-SIZE: small; FONT-FAMILY: Arial">Computer&nbsp;&amp; 
										Register Services</font></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">Helpdesk</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2777</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">Helpdesk</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(800)370-1934</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">Pager</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(800)240-1788</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td colspan='3' noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">Paging 
										Instructions: Enter store # press *, then enter store # press #</font><br>
								</td>
							</tr>
							<tr>
								<td noWrap width="1%"><br>
								</td>
								<td noWrap></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap width="1%" colSpan="3"><font style="FONT-WEIGHT: bold; FONT-SIZE: small; FONT-FAMILY: Arial">To 
										Broadcast a Message:</font></td>
							</tr>
							<tr>
								<td noWrap width="1%" colSpan="3"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">1. 
										Go into your voice mail as if you're checking messages.</font></td>
							</tr>
							<tr>
								<td noWrap width="1%" colSpan="3"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">2. 
										Enter 5 to leave a message. </font>
								</td>
							</tr>
							<tr>
								<td noWrap width="1%" colSpan="3"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">3. 
										Enter 1-2 for Groups and Remote sites.</font></td>
							</tr>
							<tr>
								<td noWrap width="1%" colSpan="3"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">4. 
										Enter user id #. </font>
								</td>
							</tr>
							<tr>
								<td noWrap width="1%" colSpan="3"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;&nbsp;&nbsp; 
										100 District Managers </font>
								</td>
							</tr>
							<tr>
								<td noWrap width="1%" colSpan="3"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;&nbsp;&nbsp; 
										102 Regional Managers </font>
								</td>
							</tr>
							<tr>
								<td noWrap width="1%" colSpan="3"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">5. 
										Then follow directions for leaving a message.</font></td>
							</tr>
						</table>
					</td>
					<td vAlign="top">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial">HQ 
										Main</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(626)839-4686</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Human 
										Resources</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(626)581-9263</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Real Estate</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(626)581-1093</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Accounting</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(626)591-9226</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Warehouse</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(626)839-3236</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Merchants 1</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(626)581-1542</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Merchants 2</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(626)581-0496</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Jay Johnson</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(626)581-0894</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Torrid</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(626)771-1126</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><br>
								</td>
								<td noWrap></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-WEIGHT: bold; FONT-SIZE: small; FONT-FAMILY: Arial">HQ 
										Room Extensions</font>
								</td>
								<td noWrap></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Mailroom</font></td>
								<td noWrap width="1%"><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2234</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">HR File 
										Room</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2405</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Accounting 
										File Room</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2409</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Family 
										Room-Floor</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2600</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Family 
										Room-Platform</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2601</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Red Pod</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2602</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Green Pod</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2603</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Training 
										Room</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2604</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Red 
										Moroccan Room</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2605</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">The Crypt</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2606</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Green 
										Moroccan Room</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2607</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Operating 
										Room</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2608</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Gothic Room</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2609</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Support Pod 
										1-by HR</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2610</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">HR Meeting 
										Area</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2611</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">IT War 
										Table</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2613</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">The Octagon</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2968</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">The 
										Porthole</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext.
									</font>
								</td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">The 
										Serpentine</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										6115</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><br>
								</td>
								<td noWrap></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-WEIGHT: bold; FONT-SIZE: small; FONT-FAMILY: Arial">DC 
										Extensions</font></td>
								<td noWrap></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">DC Mechanic</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2140</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Receiving 
										Dock</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2144</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Shipping</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2142</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">CD's</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2111</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Receiving 
										Expeditor</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2241 </font>
								</td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Ticket 
										Making</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2235</font></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Ticketing 
										Supervisor</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2101</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Stationary)</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Ticketing 
										Supervisor</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2141</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Mobile 
										Phone)</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Carousel 
										Supervisor</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2102</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Stationary)</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Carousel 
										Supervisor</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2146 </font>
								</td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Mobile 
										Phone)</font></td>
							</tr>
							<tr>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial">Security</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;Ext. 
										2143</font></td>
								<td noWrap><font style="FONT-SIZE: small; FONT-FAMILY: Arial; bold: ">&nbsp;(Mobile 
										Phone)</font></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><br>
					</td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
