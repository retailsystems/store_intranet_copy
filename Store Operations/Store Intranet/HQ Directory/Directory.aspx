<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Directory.aspx.vb" Inherits="ReferenceLists.Directory"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>HQ Directory</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<script language="javascript">
			 function OpenPop(url,w, h)
			{	
				var url1 = url;
		  
				var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=no"	 
				window.open(url1,'cw',winProp);
			}
		   	  function OpenPop1(url,w, h)
			{	
				var url1 = url;
		  
				var winProp1 = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
				window.open(url1,'cw1',winProp1);
			}
			  function OpenPop2(url,w, h)
			{	
				var url2 = url;
		  
				var winProp2 = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
				window.open(url2,'cw2',winProp2);
			}
			  function OpenPop3(url,w, h)
			{	
				var url3 = url;
		  
				var winProp3 = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
				window.open(url3,'cw3',winProp3);
			}
		</script>
		<!--[if lt IE 9]>
			<script src="/Global/js/html5shiv.min.js"></script>
			<script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server" />
			<div class="container home" role="main">
			<table width="100%" border="0">
				<tr width="100%">
					<td noWrap width="1%"><asp:label id="lblSF" EnableViewState="False" Visible="True" text="Search By:" Runat="server">Search By:</asp:label></td>
					<td><asp:radiobuttonlist id="rblSearch" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
							<asp:ListItem Value="Name" Selected="True">Name</asp:ListItem>
							<asp:ListItem Value="subject">Subject</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr width="100%">
					<td noWrap width="1%"></td>
					<td><asp:label id="lblkeyword" Visible="True" text="Keyword:" Runat="server"></asp:label>
						<asp:textbox id="txtkeyword" Visible="False" Runat="server"></asp:textbox>
						<asp:label id="lblFName" Visible="True" text="First Name:" Runat="server"></asp:label>
						<asp:textbox id="txtFName" Runat="server"></asp:textbox>&nbsp;&nbsp;&nbsp;
						<asp:label id="lblLName" Visible="True" text="Last Name:" Runat="server"></asp:label>&nbsp;
						<asp:textbox id="txtLName" Runat="server"></asp:textbox>&nbsp;
						<asp:button id="btnSearch" Runat="server" CssClass="btn btn-danger" Text="Submit"></asp:button>&nbsp;
						<asp:button id="btnClear" Runat="server" CssClass="btn btn-danger" Text="Clear"></asp:button>&nbsp;
						<asp:button id="btnHome" Runat="server" CssClass="btn btn-danger" Text="Home"></asp:button>
					</td>
				</tr>
				<tr>
					<td noWrap width="1%" colSpan="2"><asp:checkbox id="chkEP" Runat="server" Text="Search Exact Phrase " TextAlign="left" ></asp:checkbox></td>
				</tr>
			</table>
			<table>
				<tr style="display:none">
					<td><asp:hyperlink id="CurrBuyer" runat="server">Buyers</asp:hyperlink></td>
				</tr>
				<tr style="display:none">
					<td><asp:hyperlink id="CurrAnalysts" runat="server">Analysts</asp:hyperlink></td>
				</tr>
				<tr style="display:none">
					<td><asp:hyperlink id="CurrGeneral" Runat="server" Width="266px">Clericals</asp:hyperlink></td>
				</tr>
				<tr>
					<td noWrap colSpan="2"><asp:label id="lblError" Visible="false" Runat="server"></asp:label></td>
				</tr>
				<tr>
					<td colSpan="2"><br>
						<asp:datagrid id="dgInfo" runat="server" EnableViewState="True" CssClass="table archtbl" Width="100%" GridLines="None" AutoGenerateColumns="False">
							<ItemStyle CssClass="archtbltr"></ItemStyle>
							<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
							<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
							<Columns>
								<asp:HyperLinkColumn DataNavigateUrlField="Email" DataNavigateUrlFormatString="mailto:{0}" DataTextField="Name"
									HeaderText="Name"></asp:HyperLinkColumn>
								<asp:BoundColumn DataField="position_dtl_desc" HeaderText="Position"></asp:BoundColumn>
								<asp:BoundColumn DataField="extension1" HeaderText="Ext."></asp:BoundColumn>
								<asp:BoundColumn DataField="office_location" HeaderText="Office"></asp:BoundColumn>
								<asp:BoundColumn DataField="office_number" HeaderText="Office Number"></asp:BoundColumn>
								<asp:BoundColumn DataField="Fax_number" HeaderText="Fax"></asp:BoundColumn>
								<asp:BoundColumn DataField="blackberry_pin" HeaderText="Blackberry" HeaderStyle-Width="65"></asp:BoundColumn>
								<asp:BoundColumn DataField="9/80" HeaderText="9/80"></asp:BoundColumn>
							</Columns>
							<PagerStyle></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td colSpan="2"><asp:datagrid id="dgKEY" runat="server" EnableViewState="True" CssClass="table archtbl" Width="100%"
							GridLines="None" AutoGenerateColumns="False">
							<ItemStyle CssClass="archtbltr"></ItemStyle>
							<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
							<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="subject" HeaderText="Subject"></asp:BoundColumn>
								<asp:BoundColumn DataField="dept" HeaderText="Department"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Name" HeaderStyle-BorderWidth="0">
									<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
									<ItemTemplate>
										<a href='mailto:<%# Container.dataitem("Email") %>'>
											<asp:Label Runat="server" ID="Label1" Text='<%# Container.dataitem("Name") %>'>
											</asp:Label></a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="position_dtl_desc" HeaderText="Position"></asp:BoundColumn>
								<asp:BoundColumn DataField="extension1" HeaderText="Ext."></asp:BoundColumn>
								<asp:BoundColumn DataField="office_location" HeaderText="Office"></asp:BoundColumn>
								<asp:BoundColumn DataField="office_number" HeaderText="Office Number"></asp:BoundColumn>
								<asp:BoundColumn DataField="Fax_number" HeaderText="Fax"></asp:BoundColumn>
								<asp:BoundColumn DataField="blackberry_pin" HeaderText="Blackberry" HeaderStyle-Width="65"></asp:BoundColumn>
								<asp:BoundColumn DataField="9/80" HeaderText="9/80"></asp:BoundColumn>
							</Columns>
							<PagerStyle></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td colSpan="2"><br>
						<asp:label id="lblSeeAlso" Visible="False" Runat="server" Text="See Also"></asp:label></td>
				</tr>
				<tr>
					<td colSpan="2"><asp:datagrid id="dgSA" runat="server" EnableViewState="True" CssClass="table archtbl" Width="100%"
							GridLines="None" AutoGenerateColumns="False">
							<ItemStyle CssClass="archtbltr"></ItemStyle>
							<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
							<HeaderStyle CssClass="archtblhdr"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="subjectdesc" HeaderText="Subject">
									<HeaderStyle Width="15%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="comments1" HeaderText="Contact Instructions">
									<HeaderStyle Width="30%"></HeaderStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle PageButtonCount="3"></PagerStyle>
						</asp:datagrid></td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
