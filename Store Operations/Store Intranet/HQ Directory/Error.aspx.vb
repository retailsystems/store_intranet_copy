Public Class _Error
    Inherits System.Web.UI.Page
    Protected WithEvents lblErrDesc As System.Web.UI.WebControls.Label
    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblErrDesc.Text = Request.QueryString("Err")
        lblHeader.Text = Request.QueryString("Title")
        If lblHeader.Text = "" Or lblHeader.Text = Nothing Then
            lblHeader.Text = "Error!"
        End If

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))


    End Sub

End Class
