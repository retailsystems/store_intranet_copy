<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MerchList.aspx.vb" Inherits="ReferenceLists.WebForm1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Merchant List</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout" bottomMargin="0" vLink="#ffffff" aLink="#ffffff" link="#ffffff" bgColor="#000000" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td><asp:RadioButtonList ID="rblMerchType" Runat="server" RepeatDirection="Horizontal" ForeColor="#ffffff" Font-Name="Arial" Font-Size="Smaller" Font-Names="Arial" Font-Bold="True">
							<asp:ListItem Value="A" Selected="True">Analyst</asp:ListItem>
							<asp:ListItem Value="B">Buyer</asp:ListItem>
						</asp:RadioButtonList></td>
				</tr>
				<tr>
					<td><asp:DropDownList Runat="server" ID="Dept"></asp:DropDownList>&nbsp;<asp:Button ID="btnDeptLookUp" Runat="server" Text="Submit" BorderStyle="Solid" BackColor="#440000" BorderColor="#990000" ForeColor="White" Font-Bold="True"></asp:Button>&nbsp;<asp:Button BorderStyle="Solid" BackColor="#440000" BorderColor="#990000" ForeColor="White" Font-Bold="True" Runat="server" ID="btnClear" Text="Clear"></asp:Button></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
