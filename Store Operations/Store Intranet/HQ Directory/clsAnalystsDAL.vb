Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data.OracleClient
Imports System.Data.OleDb
Imports System.Data.SqlTypes
Imports System.Text.RegularExpressions
Public Class clsAnalysts
    Public empID As Integer
    Public Name As String
    Public email As String
    Public jobCode As String
    Public extension1 As String
    Public SubjectDesc As String
    Public Sub New(ByVal Name As String, ByVal email As String, ByVal jobcode As String, ByVal extension1 As String, ByVal subjectDesc As String)
        Me.Name = Name
        Me.email = email
        Me.jobCode = jobcode
        Me.extension1 = extension1
        Me.SubjectDesc = subjectDesc
    End Sub
End Class
Public Class clsAnalystsDAL
    Public Function getAnalystsDataTable() As DataTable
        Dim cnnPLSQL As OracleConnection   ' declare the connection for oracle
        Dim strConn, strOraSQL, strMsg, strConnSQL As String
        Dim OracleCmd As OracleCommand
        Dim strDeptCD As Integer
        Dim strMode As String
        ' sql stuff
        Dim strSQL As String
        Dim AnalaystArr As ArrayList
        Dim oReader As SqlDataReader
        Dim sqlCmd As SqlCommand
        Dim sqlConn As SqlConnection
        Dim strName, strJobCode As String
        Dim counter, intMode, i As Integer
        Dim Allocname1, AllocName, AllocExt, AllocEmail As String ' declaratiion for Allocatiom Analaysts
        Dim PAName1, PAName, PAExt, PAEmail As String ' declaration for P & A Analaysts
        Dim srName1, srName, srExt, srEmail As String ' declaration for sr. Analaysts
        Dim dirname1, dirName, dirExt, dirEmail As String ' declaration for director of P & A
        Dim boolAlloc, boolPA, boolSR, boolDir As Boolean
        boolAlloc = False
        boolPA = False
        boolSR = False
        boolDir = False
        counter = 0
        Dim strDeptMin As String
        Dim strDeptMax As String
        Dim strDeptAddition As String

        ' query to get the Dept ID's from oracle
        strOraSQL = " Select Dept_CD, DES from dept "
        strMode = ConfigurationSettings.AppSettings("Mode")
        intMode = InStr(strMode, "=", CompareMethod.Text)
        strMode = Mid(strMode, intMode + 1, 5)
        strDeptMin = ConfigurationSettings.AppSettings("minDept")
        strDeptMax = ConfigurationSettings.AppSettings("maxDept")
        strDeptAddition = ConfigurationSettings.AppSettings("DeptAddition")

        If strDeptAddition.Length > 0 Then
            strOraSQL = strOraSQL & " where dept_cd >= " & strDeptMin & " and dept_cd <= " & strDeptMax
        Else
            strOraSQL = strOraSQL & " where (dept_cd >= " & strDeptMin & " and dept_cd <= " & strDeptMax & " or dept_cd in (" & strDeptAddition & "))"
        End If
        'If Trim(UCase(strMode)) = "HOTTO" Then
        '    strOraSQL = strOraSQL & " where dept_cd >= 1 and dept_cd <= 49"
        'ElseIf Trim(UCase(strMode)) = "TORRI" Then
        '    strOraSQL = strOraSQL & " where dept_cd >= 50 and dept_cd <= 98"
        'End If
        strOraSQL = strOraSQL & "   Order  by dept_cd"
        Try

            cnnPLSQL = New OracleConnection
            strConn = ConfigurationSettings.AppSettings("strORAConnection")
            strConnSQL = ConfigurationSettings.AppSettings("strSQLConn")
            ' created a datatable that will contain the values both from dapartment table and Subject table
            Dim dtAnalysts As New DataTable("Buyer")
            dtAnalysts.Columns.Add("DeptID", GetType(String)) '0
            dtAnalysts.Columns.Add("DeptDesc", GetType(String)) '1
            dtAnalysts.Columns.Add("AllocAnalysts", GetType(String)) '2
            dtAnalysts.Columns.Add("ExtAllocAnalysts", GetType(String)) '3
            dtAnalysts.Columns.Add("AllocAnalystsEmail", GetType(String)) '4
            dtAnalysts.Columns.Add("PAnalysts", GetType(String)) '5
            dtAnalysts.Columns.Add("PAnalystsext", GetType(String)) '6
            dtAnalysts.Columns.Add("PAnalystsEmail", GetType(String)) '7
            dtAnalysts.Columns.Add("SrAnalysts", GetType(String)) '8
            dtAnalysts.Columns.Add("SrAnalystsext", GetType(String)) '9
            dtAnalysts.Columns.Add("SrAnalystsemail", GetType(String)) '10
            dtAnalysts.Columns.Add("dirPA", GetType(String)) '11
            dtAnalysts.Columns.Add("dirPAEXT", GetType(String)) '12
            dtAnalysts.Columns.Add("dirPAEmail", GetType(String)) '13
            cnnPLSQL.ConnectionString = strConn
            ' open the connection 
            cnnPLSQL.Open()
            OracleCmd = New OracleCommand(strOraSQL, cnnPLSQL)
            OracleCmd.CommandType = CommandType.Text
            ' executing the oracle reader
            Dim oReaderOracle As OracleDataReader = OracleCmd.ExecuteReader()

            sqlConn = New SqlConnection(strConnSQL)
            sqlConn.Open()
            ' Reading each line from oracle datatbase and then putting it in the datatable and then reading the line
            ' from sql dataBase and appending it in the datatable.
            While oReaderOracle.Read()
                Dim EmpNum As Integer

                strDeptCD = oReaderOracle(0)
                AnalaystArr = New ArrayList
                AnalaystArr = CalcDeptNumForAnalysts(strDeptCD)
                ' stored procedure
                Dim Analysis As clsAnalysts


                Dim AnalystsRow As DataRow = dtAnalysts.NewRow
                For i = 0 To AnalaystArr.Count - 1
                    Analysis = AnalaystArr.Item(i)
                    ' extract the individual coumns from the arraylist

                    Dim strName1 As String = Analysis.Name
                    Dim strEmail As String = Analysis.email
                    Dim strJobCode1 As String = Analysis.jobCode
                    Dim strExt As String = Analysis.extension1
                    Dim subjectDesc As String = Analysis.SubjectDesc
                    ' Manually stored the value in the DataTable because I am reading them 
                    ' stroing the values in a variable rather then stroing them in a datatable directly. 
                    ' because If there are two names then that value is stored in the same variable.
                    ' Dim HQType As Regex = New Regex(subjectDesc)

                    If InStr(subjectDesc, "ALLOCATION ANALYST", CompareMethod.Text) Or InStr(strJobCode1, "JRANYL", CompareMethod.Text) Then
                        If Allocname1 <> strName1 Then
                            Allocname1 = strName1
                            AllocName = AllocName & Trim(strName1) + "/ "
                            AllocEmail = AllocEmail & Trim(strEmail) + " ;"
                            AllocExt = AllocExt & Trim(strExt) + " /"
                            boolAlloc = True
                        End If
                    ElseIf InStr(subjectDesc, "P & A ANALYST", CompareMethod.Text) Then
                        If PAName1 <> strName1 Then
                            PAName1 = strName1
                            PAName = PAName & Trim(strName1) + "/ "
                            PAExt = PAExt & Trim(strExt) + " /"
                            PAEmail = PAEmail & Trim(strEmail) + " ;"
                            boolPA = True
                        End If
                    ElseIf InStr(subjectDesc, "SENIOR ANALYST", CompareMethod.Text) Then
                        If srName1 <> strName1 Then
                            srName1 = strName1
                            srName = srName & Trim(strName1) + "/ "
                            srExt = srExt & Trim(strExt) + " /"
                            srEmail = srEmail & Trim(strEmail) + " ;"
                            boolSR = True
                        End If
                    ElseIf InStr(subjectDesc, "DIRECTOR OF P & A", CompareMethod.Text) Then
                        If dirname1 <> strName1 Then
                            dirname1 = strName1
                            dirName = dirName & Trim(strName1) + "/ "
                            dirExt = dirExt & Trim(strExt) + " /"
                            dirEmail = dirEmail & Trim(strEmail) + " ;"
                            boolDir = True
                        End If

                    End If


                    'Select Case Trim(strJobCode1)
                    '    Case "JRANYL"   ' code for allocation analysts
                    '        If Allocname1 <> strName1 Then
                    '            Allocname1 = strName1
                    '            AllocName = AllocName & Trim(strName1) + "/ "
                    '            AllocEmail = AllocEmail & Trim(strEmail) + " ;"
                    '            AllocExt = AllocExt & Trim(strExt) + " /"
                    '            boolAlloc = True
                    '        End If
                    '    Case "INVT"           ' P & A Analayts
                    '        If PAName1 <> strName1 Then
                    '            PAName1 = strName1
                    '            PAName = PAName & Trim(strName1) + "/ "
                    '            PAExt = PAExt & Trim(strExt) + " /"
                    '            PAEmail = PAEmail & Trim(strEmail) + " ;"
                    '            boolPA = True
                    '        End If
                    '    Case "Snrinr"  ' senior P& A Analysts
                    '        If srName1 <> strName1 Then
                    '            srName1 = strName1
                    '            srName = srName & Trim(strName1) + "/ "
                    '            srExt = srExt & Trim(strExt) + " /"
                    '            srEmail = srEmail & Trim(strEmail) + " ;"
                    '            boolSR = True
                    '        End If
                    '    Case "DIRPA"
                    '        If dirname1 <> strName1 Then
                    '            dirname1 = strName1
                    '            dirName = dirName & Trim(strName1) + "/ "
                    '            dirExt = dirExt & Trim(strExt) + " /"
                    '            dirEmail = dirEmail & Trim(strEmail) + " ;"
                    '            boolDir = True
                    '        End If
                    'End Select


                Next

                AnalystsRow(0) = strDeptCD  ' Read the dept code
                AnalystsRow(1) = oReaderOracle(1)  ' Read the Department description
                If AllocName <> "" Then
                    AnalystsRow(2) = AllocName.Substring(0, AllocName.Length - 2)
                End If
                If AllocExt <> "" Then
                    AnalystsRow(3) = AllocExt.Substring(0, AllocExt.Length - 2)
                End If
                If AllocEmail <> "" Then
                    AnalystsRow(4) = AllocEmail.Substring(0, AllocEmail.Length - 2)
                End If
                If PAName <> "" Then
                    AnalystsRow(5) = PAName.Substring(0, PAName.Length - 2)
                End If
                If PAExt <> "" Then
                    AnalystsRow(6) = PAExt.Substring(0, PAExt.Length - 2)
                End If
                If PAEmail <> "" Then
                    AnalystsRow(7) = PAEmail.Substring(0, PAEmail.Length - 2)
                End If
                If srName <> "" Then
                    AnalystsRow(8) = srName.Substring(0, srName.Length - 2)
                End If
                If srExt <> "" Then
                    AnalystsRow(9) = srExt.Substring(0, srExt.Length - 2)
                End If
                If srEmail <> "" Then
                    AnalystsRow(10) = srEmail.Substring(0, srEmail.Length - 2)
                End If
                If dirName <> "" Then
                    AnalystsRow(11) = dirName.Substring(0, dirName.Length - 2)
                End If
                If dirExt <> "" Then
                    AnalystsRow(12) = dirExt.Substring(0, dirExt.Length - 2)
                End If
                If dirEmail <> "" Then
                    AnalystsRow(13) = dirEmail.Substring(0, dirEmail.Length - 2)
                End If


                If boolAlloc = True Or boolPA = True Or boolSR = True Or boolDir = True Then
                    dtAnalysts.Rows.Add(AnalystsRow)
                End If
                AnalaystArr.Clear()
                AllocName = ""
                Allocname1 = ""
                AllocEmail = ""
                AllocExt = ""
                PAName = ""
                PAName1 = ""
                PAExt = ""
                PAEmail = ""
                srName = ""
                srName1 = ""
                srExt = ""
                srEmail = ""
                dirName = ""
                dirname1 = ""
                dirExt = ""
                dirEmail = ""
                boolAlloc = False
                boolPA = False
                boolSR = False
                boolDir = False

            End While


            oReaderOracle.Close()
            cnnPLSQL.Close()
            Return dtAnalysts
        Catch ex As Exception


            cnnPLSQL.Close()
            strMsg = ex.Message()
            System.Diagnostics.Debug.WriteLine(strMsg)
        End Try

    End Function
    ' This function will find out all the values of that particular department from 
    Public Shared Function CalcDeptNumForAnalysts(ByVal strDeptCD As Integer) As ArrayList
        Dim sqlCmd As SqlCommand
        Dim sqlConnF As SqlConnection
        Dim i As Integer
        Dim SecondReader As SqlDataReader
        Dim secondFind(5) As String
        Dim strMode As String
        Dim AnalystsArr As ArrayList
        Dim strJobCode, strName, strExt, strEmail, strTotAnalayst, strSubjectDesc, strContactID2 As String
        Dim strContactID3, strContactID4, strContactID5, strContactID6, strContactID7, strContactID8, strContactID9, strContactID10, strContactID11, strContactID12 As String
        Dim oReaderF As SqlDataReader
        Dim flag As Boolean
        flag = False
        Dim strConnF, strSQLF As String
        Dim strMSG As String
        Dim strkeyWord, strKey, strPartialKeyWord As String
        Dim intKeyword, intEmpID As Integer
        Try
            intEmpID = 0
            AnalystsArr = New ArrayList
            strConnF = ConfigurationSettings.AppSettings("strSQLConn")

            sqlConnF = New SqlConnection(strConnF)
            sqlConnF.Open()
            sqlCmd = New SqlCommand("sp_GetAnalaystsjobCode", sqlConnF)
            sqlCmd.CommandType = CommandType.StoredProcedure

            oReaderF = sqlCmd.ExecuteReader()
            While oReaderF.Read()
                strkeyWord = oReaderF(5).ToString()
                intKeyword = InStr(strkeyWord, "Dept", CompareMethod.Text)
                If intKeyword = 0 Then
                    intKeyword = InStr(strkeyWord, "Department", CompareMethod.Text)
                    If intKeyword <> 0 Then
                        strPartialKeyWord = Mid(strkeyWord, intKeyword + 11, 2)
                    End If
                Else
                    strPartialKeyWord = Mid(strkeyWord, intKeyword + 5, 2)

                End If
                If (Trim(strPartialKeyWord)).Length < 2 Then
                    strPartialKeyWord = Mid(strkeyWord, intKeyword + 5, 3)
                End If
                If strPartialKeyWord <> "" Then
                    If Trim(strPartialKeyWord) = strDeptCD Then

                        'intEmpID = oReaderF(1)
                        If strName <> oReaderF(0) Then
                            strName = oReaderF(0)
                            strJobCode = oReaderF(2)



                            strEmail = oReaderF(1)
                            strExt = oReaderF(3)
                            strSubjectDesc = oReaderF(5)
                            strContactID2 = oReaderF(6)
                            strContactID3 = oReaderF(7)
                            strContactID4 = oReaderF(8)
                            strContactID5 = oReaderF(9)
                            strContactID6 = oReaderF(10)
                            strContactID7 = oReaderF(11)
                            strContactID8 = oReaderF(12)
                            strContactID9 = oReaderF(13)
                            strContactID10 = oReaderF(14)
                            strContactID11 = oReaderF(15)
                            strContactID12 = oReaderF(16)


                            AnalystsArr.Add(New clsAnalysts(strName, strEmail, strJobCode, strExt, strSubjectDesc))
                            If Trim(strContactID2) <> "" Then
                                secondFind = FindSecondContact(strContactID2)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExt = secondFind(3)
                                strSubjectDesc = secondFind(4)
                                AnalystsArr.Add(New clsAnalysts(strName, strEmail, strJobCode, strExt, strSubjectDesc))

                            End If

                            If Trim(strContactID3) <> "" Then
                                secondFind = FindSecondContact(strContactID3)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExt = secondFind(3)
                                strSubjectDesc = secondFind(4)
                                AnalystsArr.Add(New clsAnalysts(strName, strEmail, strJobCode, strExt, strSubjectDesc))
                            End If

                            If Trim(strContactID4) <> "" Then
                                secondFind = FindSecondContact(strContactID4)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExt = secondFind(3)
                                strSubjectDesc = secondFind(4)
                                AnalystsArr.Add(New clsAnalysts(strName, strEmail, strJobCode, strExt, strSubjectDesc))
                            End If

                            If Trim(strContactID5) <> "" Then
                                secondFind = FindSecondContact(strContactID5)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExt = secondFind(3)
                                strSubjectDesc = secondFind(4)
                                AnalystsArr.Add(New clsAnalysts(strName, strEmail, strJobCode, strExt, strSubjectDesc))
                            End If

                            If Trim(strContactID6) <> "" Then
                                secondFind = FindSecondContact(strContactID6)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExt = secondFind(3)
                                strSubjectDesc = secondFind(4)
                                AnalystsArr.Add(New clsAnalysts(strName, strEmail, strJobCode, strExt, strSubjectDesc))
                            End If

                            If Trim(strContactID7) <> "" Then
                                secondFind = FindSecondContact(strContactID7)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExt = secondFind(3)
                                strSubjectDesc = secondFind(4)
                                AnalystsArr.Add(New clsAnalysts(strName, strEmail, strJobCode, strExt, strSubjectDesc))
                            End If

                            If Trim(strContactID8) <> "" Then
                                secondFind = FindSecondContact(strContactID8)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExt = secondFind(3)
                                strSubjectDesc = secondFind(4)
                                AnalystsArr.Add(New clsAnalysts(strName, strEmail, strJobCode, strExt, strSubjectDesc))
                            End If

                            If Trim(strContactID9) <> "" Then
                                secondFind = FindSecondContact(strContactID9)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExt = secondFind(3)
                                strSubjectDesc = secondFind(4)
                                AnalystsArr.Add(New clsAnalysts(strName, strEmail, strJobCode, strExt, strSubjectDesc))
                            End If

                            If Trim(strContactID10) <> "" Then
                                secondFind = FindSecondContact(strContactID10)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExt = secondFind(3)
                                strSubjectDesc = secondFind(4)
                                AnalystsArr.Add(New clsAnalysts(strName, strEmail, strJobCode, strExt, strSubjectDesc))
                            End If

                            If Trim(strContactID11) <> "" Then
                                secondFind = FindSecondContact(strContactID11)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExt = secondFind(3)
                                strSubjectDesc = secondFind(4)
                                AnalystsArr.Add(New clsAnalysts(strName, strEmail, strJobCode, strExt, strSubjectDesc))
                            End If

                            If Trim(strContactID12) <> "" Then
                                secondFind = FindSecondContact(strContactID12)
                                strName = secondFind(0)
                                strJobCode = secondFind(2)
                                strEmail = secondFind(1)
                                strExt = secondFind(3)
                                strSubjectDesc = secondFind(4)
                                AnalystsArr.Add(New clsAnalysts(strName, strEmail, strJobCode, strExt, strSubjectDesc))
                            End If


                        End If
                    End If
                End If
            End While
            oReaderF.Close()
            sqlConnF.Close()
            sqlConnF.Dispose()

            Return AnalystsArr


        Catch ex As Exception
            strMSG = ex.Message
            System.Diagnostics.Debug.WriteLine(strMSG)

        End Try

    End Function
    Public Shared Function FindSecondContact(ByVal strContactID2 As String) As Array
        Dim strCons As String
        Dim sqlConn As SqlConnection
        Dim sqlCmd As SqlCommand
        Dim Second(5) As String
        Dim strMSG As String
        Dim oReader As SqlDataReader
        Dim strName, strEmail, strExt As String
        Dim strSQL As String
        Try
            strSQL = ""
            strSQL = "select ta.NamePreferred + ' ' + ta.NameLast as [Name], email, ta.jobCode, "
            strSQL = strSQL & " extension1, keywords, subjectDesc, ContactID2 from taUltimateEmployee ta, contact c, Subject S "
            strSQL = strSQL & " where(ta.employeeID = c.empID And location = '8001')  "
            strSQL = strSQL & " and ( S.ContactID2 = C.empID  or S.ContactID3 = C.empID or S.ContactID4 = C.empID or S.ContactID5 = C.empID or S.ContactID6 = C.empID  or "
            strSQL = strSQL & " S.ContactID7 = C.empID Or S.ContactID8 = C.empID Or S.ContactID9 = C.empID Or S.ContactID10 = C.empID Or S.ContactID11 = C.empID Or S.ContactID12 = C.empID)"
            strSQL = strSQL & " and  ta.employeeID = " & strContactID2

            strCons = ConfigurationSettings.AppSettings("strSQLConn")

            sqlConn = New SqlConnection(strCons)
            sqlConn.Open()
            sqlCmd = New SqlCommand(strSQL, sqlConn)
            sqlCmd.CommandType = CommandType.Text
            oReader = sqlCmd.ExecuteReader()
            While oReader.Read()
                Second(0) = oReader(0)
                Second(1) = oReader(1)
                Second(2) = oReader(2)
                Second(3) = oReader(3)
                Second(4) = oReader(5)
            End While
            Return Second
            oReader.Close()
            sqlConn.Close()
            sqlConn.Dispose()
        Catch ex As Exception
            strMSG = ex.Message
            System.Diagnostics.Debug.WriteLine(strMSG)
        End Try

    End Function

End Class
