Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data.OracleClient
Imports System.Data.OleDb
Imports System.Data.SqlTypes
Imports System.Object
Imports System
Imports System.Collections
Public Class clsMerchant
    Public Shared Function GeneralQuestions()
        Dim cnnPLSQL As OracleConnection   ' declare the connection for oracle
        Dim strConn, strOraSQL, strMsg, strConnSQL As String
        Dim OracleCmd As OracleCommand
        Dim strDeptCD As Integer
        Dim strMode As String
        ' sql stuff
        Dim strSQL As String
        Dim oReader As SqlDataReader
        Dim sqlCmd As SqlCommand
        Dim sqlConn As SqlConnection
        Dim strName, strJobCode As String
        Dim counter, intMode, i As Integer
        counter = 0
        Dim strDeptMin As String
        Dim strDeptMax As String
        Dim strDeptAddition As String
        ' query to get the Dept ID's from oracle
        strOraSQL = " Select Dept_CD, DES from dept "
        strMode = ConfigurationSettings.AppSettings("Mode")
        intMode = InStr(strMode, "=", CompareMethod.Text)
        strMode = Mid(strMode, intMode + 1, 5)
        strDeptMin = ConfigurationSettings.AppSettings("minDept")
        strDeptMax = ConfigurationSettings.AppSettings("maxDept")
        strDeptAddition = ConfigurationSettings.AppSettings("DeptAddition")

        If strDeptAddition.Length > 0 Then
            strOraSQL = strOraSQL & " where dept_cd >= " & strDeptMin & " and dept_cd <= " & strDeptMax
        Else
            strOraSQL = strOraSQL & " where (dept_cd >= " & strDeptMin & " and dept_cd <= " & strDeptMax & " or dept_cd in (" & strDeptAddition & "))"
        End If
        'If Trim(UCase(strMode)) = "HOTTO" Then
        '    strOraSQL = strOraSQL & " where dept_cd >= 1 and dept_cd <= 49"
        '    'strOraSQL = strOraSQL & " where dept_cd = 1"
        'ElseIf Trim(UCase(strMode)) = "TORRI" Then
        '    strOraSQL = strOraSQL & " where dept_cd >= 50 and dept_cd <= 98"
        'End If
        strOraSQL = strOraSQL & " Order  by dept_cd"
        Try
            cnnPLSQL = New OracleConnection
            strConn = ConfigurationSettings.AppSettings("strORAConnection")
            strConnSQL = ConfigurationSettings.AppSettings("strSQLConn")
            ' created a datatable that will contain the values both from dapartment table and Subject table
            Dim dtReference As New DataTable("Buyer")
            dtReference.Columns.Add("DeptID", GetType(String)) '0
            dtReference.Columns.Add("DeptDesc", GetType(String)) '1
            dtReference.Columns.Add("ContactName", GetType(String)) '2
            dtReference.Columns.Add("Contactext", GetType(String)) '3
            dtReference.Columns.Add("email", GetType(String)) '4
            cnnPLSQL.ConnectionString = strConn
            ' open the connection 
            cnnPLSQL.Open()
            OracleCmd = New OracleCommand(strOraSQL, cnnPLSQL)
            OracleCmd.CommandType = CommandType.Text
            ' executing the oracle reader
            Dim oReaderOracle As OracleDataReader = OracleCmd.ExecuteReader()

            sqlConn = New SqlConnection(strConnSQL)
            sqlConn.Open()
            ' Reading each line from oracle datatbase and then putting it in the datatable and then reading the line
            ' from sql dataBase and appending it in the datatable.
            Dim secondFind(5) As String
            While oReaderOracle.Read()
                Dim EmpNum As Integer
                Dim strDeptCode, strDeptDesc, strAllocAnalysts, strExt, strEmail As String
                strDeptCD = oReaderOracle(0)

                EmpNum = getEmpID(strDeptCD)
                ' stored procedure


                sqlCmd = New SqlCommand("sp_generalQuestions", sqlConn)
                sqlCmd.CommandType = CommandType.StoredProcedure
                Dim ParamEmpID As SqlParameter = sqlCmd.Parameters.Add("@empID", SqlDbType.Char)
                ParamEmpID.Direction = ParameterDirection.Input
                ParamEmpID.Value = EmpNum

                oReader = sqlCmd.ExecuteReader()
                While (oReader.Read())  ' reading each line
                    Dim GeneralRow As DataRow = dtReference.NewRow
                    GeneralRow(0) = strDeptCD  ' Read the dept code
                    GeneralRow(1) = oReaderOracle(1)  ' Read the Department description
                    strAllocAnalysts = strAllocAnalysts & Trim(oReader(0)) & "/" ' Read the Allocation analaysts name
                    strExt = strExt & Trim(oReader(2)) & "/"  'extension
                    strEmail = strEmail & Trim(oReader(1)) & ";" 'email
                    If Trim(oReader(3)) <> "" Then
                        Dim employeeCode As Integer = oReader(3)
                        secondFind = calcOtherInfo(employeeCode)
                        strAllocAnalysts = strAllocAnalysts & Trim(secondFind(0)) & "/"
                        strExt = strExt & Trim(secondFind(2)) & "/"
                        strEmail = strEmail & Trim(secondFind(1)) & ";"
                    End If
                    If Trim(oReader(4)) <> "" Then
                        Dim employeeCode As Integer = oReader(4)
                        secondFind = calcOtherInfo(employeeCode)
                        strAllocAnalysts = strAllocAnalysts & Trim(secondFind(0)) & "/"
                        strExt = strExt & Trim(secondFind(2)) & "/"
                        strEmail = strEmail & Trim(secondFind(1)) & ";"
                    End If
                    If Trim(oReader(5)) <> "" Then
                        Dim employeeCode As Integer = oReader(5)
                        secondFind = calcOtherInfo(employeeCode)
                        strAllocAnalysts = strAllocAnalysts & Trim(secondFind(0)) & "/"
                        strExt = strExt & Trim(secondFind(2)) & "/"
                        strEmail = strEmail & Trim(secondFind(1)) & ";"
                    End If
                    If Trim(oReader(6)) <> "" Then
                        Dim employeeCode As Integer = oReader(6)
                        secondFind = calcOtherInfo(employeeCode)
                        strAllocAnalysts = strAllocAnalysts & Trim(secondFind(0)) & "/"
                        strExt = strExt & Trim(secondFind(2)) & "/"
                        strEmail = strEmail & Trim(secondFind(1)) & ";"
                    End If
                    If Trim(oReader(7)) <> "" Then
                        Dim employeeCode As Integer = oReader(7)
                        secondFind = calcOtherInfo(employeeCode)
                        strAllocAnalysts = strAllocAnalysts & Trim(secondFind(0)) & "/"
                        strExt = strExt & Trim(secondFind(2)) & "/"
                        strEmail = strEmail & Trim(secondFind(1)) & ";"
                    End If
                    If Trim(oReader(8)) <> "" Then
                        Dim employeeCode As Integer = oReader(8)
                        secondFind = calcOtherInfo(employeeCode)
                        strAllocAnalysts = strAllocAnalysts & Trim(secondFind(0)) & "/"
                        strExt = strExt & Trim(secondFind(2)) & "/"
                        strEmail = strEmail & Trim(secondFind(1)) & ";"
                    End If
                    If Trim(oReader(9)) <> "" Then
                        Dim employeeCode As Integer = oReader(9)
                        secondFind = calcOtherInfo(employeeCode)
                        strAllocAnalysts = strAllocAnalysts & Trim(secondFind(0)) & "/"
                        strExt = strExt & Trim(secondFind(2)) & "/"
                        strEmail = strEmail & Trim(secondFind(1)) & ";"
                    End If
                    If Trim(oReader(10)) <> "" Then
                        Dim employeeCode As Integer = oReader(10)
                        secondFind = calcOtherInfo(employeeCode)
                        strAllocAnalysts = strAllocAnalysts & Trim(secondFind(0)) & "/"
                        strExt = strExt & Trim(secondFind(2)) & "/"
                        strEmail = strEmail & Trim(secondFind(1)) & ";"
                    End If
                    If Trim(oReader(11)) <> "" Then
                        Dim employeeCode As Integer = oReader(11)
                        secondFind = calcOtherInfo(employeeCode)
                        strAllocAnalysts = strAllocAnalysts & Trim(secondFind(0)) & "/"
                        strExt = strExt & Trim(secondFind(2)) & "/"
                        strEmail = strEmail & Trim(secondFind(1)) & ";"
                    End If
                    If Trim(oReader(12)) <> "" Then
                        Dim employeeCode As Integer = Trim(oReader(12))
                        secondFind = calcOtherInfo(employeeCode)
                        strAllocAnalysts = strAllocAnalysts & secondFind(0) & "/"
                        strExt = strExt & Trim(secondFind(2)) & "/"
                        strEmail = strEmail & Trim(secondFind(1)) & ";"
                    End If
                    If Trim(oReader(13)) <> "" Then
                        Dim employeeCode As Integer = oReader(13)
                        secondFind = calcOtherInfo(employeeCode)
                        strAllocAnalysts = strAllocAnalysts & secondFind(0) & "/"
                        strExt = strExt & Trim(secondFind(2)) & "/"
                        strEmail = strEmail & Trim(secondFind(1)) & ";"
                    End If

                    If strAllocAnalysts <> "" Then
                        strAllocAnalysts = strAllocAnalysts.Substring(0, strAllocAnalysts.Length - 2)
                    End If

                    If strExt <> "" Then
                        strExt = strExt.Substring(0, strExt.Length - 1)
                    End If

                    If strEmail <> "" Then
                        strEmail = strEmail.Substring(0, strEmail.Length - 1)
                    End If
                    GeneralRow(2) = strAllocAnalysts
                    GeneralRow(3) = strExt
                    GeneralRow(4) = strEmail
                    dtReference.Rows.Add(GeneralRow)
                    strAllocAnalysts = ""
                    strExt = ""
                    strEmail = ""

                    Exit While
                End While

                oReader.Close()
            End While
            sqlConn.Close()  ' closing the connection
            oReader.Close()  ' closing the reader
            oReaderOracle.Close()
            cnnPLSQL.Close()
            Return dtReference
        Catch ex As Exception
            System.Diagnostics.Process.Start(ex.Message)
        End Try
   

    End Function
    Public Shared Function getEmpID(ByVal strDeptCD As Integer) As Integer
        Dim sqlCmd As SqlCommand
        Dim sqlConnF As SqlConnection
        Dim strMode As String
        Dim oReaderF As SqlDataReader
        Dim flag As Boolean
        flag = False
        Dim strConnF, strSQLF As String
        Dim strMSG As String
        Dim strkeyWord, strKey, strPartialKeyWord As String
        Dim intKeyword, intEmpID As Integer
        Try
            intEmpID = 0

            strConnF = ConfigurationSettings.AppSettings("strSQLConn")

            sqlConnF = New SqlConnection(strConnF)
            sqlConnF.Open()
            sqlCmd = New SqlCommand("sp_getEmpID", sqlConnF)
            sqlCmd.CommandType = CommandType.StoredProcedure

            oReaderF = sqlCmd.ExecuteReader()
            While oReaderF.Read()
                strkeyWord = oReaderF(1).ToString()
                intKeyword = InStr(strkeyWord, "Dept", CompareMethod.Text)
                If intKeyword = 0 Then
                    intKeyword = InStr(strkeyWord, "Department", CompareMethod.Text)
                    If intKeyword <> 0 Then
                        strPartialKeyWord = Mid(strkeyWord, intKeyword + 11, 2)
                    End If
                Else
                    strPartialKeyWord = Mid(strkeyWord, intKeyword + 5, 2)
                End If

                If strPartialKeyWord <> "" Then
                    If Trim(strPartialKeyWord) = strDeptCD Then

                        intEmpID = oReaderF(0)
                        oReaderF.Close()
                        sqlConnF.Close()
                        sqlConnF.Dispose()
                        flag = True
                        Return intEmpID


                    End If
                End If

            End While
            If flag = False Then
                Return 0
            End If
        Catch ex As Exception
            strMSG = ex.Message
        End Try

    End Function
    Public Shared Function Coordinator() As DataSet
        Dim strSQL As String
        strSQL = strSQL & " select 'Merchandise Coordinator' as [Merchandise Coordinator] , ta.NamePreferred + ' ' + ta.NameLast as [Name],extension1, subjectDESC, email  from subject S, taultimateEmployee ta, Contact c "
        strSQL = strSQL & " where(ta.employeeID = REPLICATE('0', (6 - LEN(c.EmpID))) + c.EmpID  And location = 8001)"
        strSQL = strSQL & " and subjectDesc like '%MERCHANDISE COORDINATOR%' "
        strSQL = strSQL & " and S.ContactID1 = C.empID "
        Dim conFillDG As New SqlConnection(ConfigurationSettings.AppSettings("StrSQLConn"))
        conFillDG.Open()
        Dim objDataAdapter As SqlDataAdapter
        Dim objDataset As New DataSet()
        objDataAdapter = New SqlDataAdapter(strSQL, conFillDG)
        objDataAdapter.Fill(objDataset, "Merchant")

        conFillDG.Close()
        '  objDataAdapter.Dispose()
        Return objDataset

    End Function
    Private Shared Function calcOtherInfo(ByVal EmpNum As Integer) As Array
        Dim strCons As String
        Dim sqlConn As SqlConnection
        Dim sqlCmd As SqlCommand
        Dim Second(5) As String
        Dim strMSG As String
        Dim oReader As SqlDataReader
        Dim strName, strEmail, strExt As String
        Dim strSQL As String
        Try
            strSQL = ""
            strSQL = "select ta.NamePreferred + ' ' + ta.NameLast as [Name], email, ta.jobCode, "
            strSQL = strSQL & " extension1, keywords, subjectDesc, ContactID2 from taUltimateEmployee ta, contact c, Subject S "
            strSQL = strSQL & " where(ta.employeeID = REPLICATE('0', (6 - LEN(c.EmpID))) + c.EmpID And location = '8001')  "
            strSQL = strSQL & " and ( S.ContactID2 = C.empID  or S.ContactID3 = C.empID or S.ContactID4 = C.empID or S.ContactID5 = C.empID or S.ContactID6 = C.empID  or "
            strSQL = strSQL & " S.ContactID7 = C.empID Or S.ContactID8 = C.empID Or S.ContactID9 = C.empID Or S.ContactID10 = C.empID Or S.ContactID11 = C.empID Or S.ContactID12 = C.empID)"
            strSQL = strSQL & " and  ta.employeeID = " & EmpNum

            strCons = ConfigurationSettings.AppSettings("strSQLConn")

            sqlConn = New SqlConnection(strCons)
            sqlConn.Open()
            sqlCmd = New SqlCommand(strSQL, sqlConn)
            sqlCmd.CommandType = CommandType.Text
            oReader = sqlCmd.ExecuteReader()
            While oReader.Read()
                Second(0) = oReader(0)
                Second(1) = oReader(1)
                Second(2) = oReader(3)

            End While
            Return Second
            oReader.Close()
            sqlConn.Close()
            sqlConn.Dispose()
        Catch ex As Exception
            strMSG = ex.Message
            System.Diagnostics.Debug.WriteLine(strMSG)
        End Try

    End Function



End Class
