Imports System.Data.SqlClient

Public Class hthqextlist_html
    Inherits System.Web.UI.Page
    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm
    Dim conGetRecs As System.Data.SqlClient.SqlConnection
    Public strSQL As String
    Dim cmdFillRecs As System.Data.SqlClient.SqlCommand
    Public dt As New DataTable()
    Dim da As New SqlDataAdapter()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        strSQL = "SELECT taUltimateEmployee.NamePreferred AS 'First', taUltimateEmployee.NameLast AS 'Last', "
        strSQL &= "Contact.Extension1 AS 'Ext.', Contact.Position_DTL_Desc AS 'Position' "
        strSQL &= "FROM hottopic2.dbo.Contact Contact, hottopic2.dbo.taUltimateEmployee taUltimateEmployee "
        strSQL &= "WHERE Contact.EmpID = taUltimateEmployee.EmployeeID AND ((taUltimateEmployee.Region='HQ') "
        strSQL &= "AND (Contact.Position_DTL_Desc Not Like ('TORRID%') And Contact.Position_DTL_Desc Not Like "
        strSQL &= "('DISTRICT%')) OR (Contact.Position_DTL_Desc Not Like ('TORRID%') And "
        strSQL &= "Contact.Position_DTL_Desc Not Like ('DISTRICT%')) AND (Contact.Office_Location='HQ')) "
        strSQL &= "ORDER BY taUltimateEmployee.NamePreferred, taUltimateEmployee.NameLast "

        conGetRecs = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        cmdFillRecs = New System.Data.SqlClient.SqlCommand(strSQL, conGetRecs)

        conGetRecs.Open()
        Page.DataBind()
        conGetRecs.Close()
        dt = New DataTable("recs")
        da = New SqlDataAdapter(strSQL, conGetRecs)
        da.Fill(dt)
        conGetRecs.Close()

        Page.DataBind() 'causes all data expressions on page to evaluate
    End Sub

    Enum EntireTableRows As Integer
        zeroBased = 62
        oneBased = 63
    End Enum

End Class
