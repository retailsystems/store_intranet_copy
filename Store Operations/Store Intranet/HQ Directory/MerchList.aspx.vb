Public Class WebForm1
    Inherits System.Web.UI.Page
    Protected WithEvents btnDeptLookUp As System.Web.UI.WebControls.Button
    Protected WithEvents btnClear As System.Web.UI.WebControls.Button
    Protected WithEvents Dept As System.Web.UI.WebControls.DropDownList
    Protected WithEvents rblMerchType As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents ucHeader As Header
    Public MinDept As String '(either 0001 or 0050)
    Public MaxDept As String '(either 0030 or 0065)
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        ucHeader.lblTitle = "Merchant List"
        MinDept = ConfigurationSettings.AppSettings("MinDept")
        MaxDept = ConfigurationSettings.AppSettings("MaxDept")

        If Not IsPostBack Then
            Dim conFillDept As New System.Data.OleDb.OleDbConnection(ConfigurationSettings.AppSettings("strORAConnection"))
            Dim cmdCommand As System.Data.OleDb.OleDbCommand
            Dim dtrReader As System.Data.OleDb.OleDbDataReader
            conFillDept.Open()
            cmdCommand = New System.Data.OleDb.OleDbCommand("SELECT dept_cd, dept_cd||':'||des as descrip,des FROM Dept Where Dept_cd >= " & MinDept & " AND Dept_cd <= " & MaxDept & " Order By Dept_Cd ", conFillDept)
            dtrReader = cmdCommand.ExecuteReader()
            Dept.DataSource = dtrReader
            Dept.DataTextField = "descrip"
            Dept.DataValueField = "dept_cd"
            Dept.DataBind()
            Dept.Items.Insert(0, "<Select A Department>")

            dtrReader.Close()
            conFillDept.Close()
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dept.SelectedIndex = -1
        rblMerchType.SelectedItem.Value = "Analyst"
    End Sub
End Class
