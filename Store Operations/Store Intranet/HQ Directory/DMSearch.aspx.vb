Public Class DMSearch
    Inherits System.Web.UI.Page
    Protected WithEvents btnSearch As System.Web.UI.WebControls.Button
    Protected WithEvents btnClear As System.Web.UI.WebControls.Button
    Protected WithEvents btnALLDMS As System.Web.UI.WebControls.Button
    Protected WithEvents lblFName As System.Web.UI.WebControls.Label
    Protected WithEvents txtFName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblLName As System.Web.UI.WebControls.Label
    Protected WithEvents txtLName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblresults As System.Web.UI.WebControls.Label
    Protected WithEvents dgResults As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblSF As System.Web.UI.WebControls.Label
    Protected WithEvents rblSearchType As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lblSB As System.Web.UI.WebControls.Label
    Protected WithEvents rblDM As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rblRM As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lblNUM As System.Web.UI.WebControls.Label
    Protected WithEvents txtNUM As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnALL As System.Web.UI.WebControls.Button
    Protected WithEvents dgRECR As System.Web.UI.WebControls.DataGrid
    Protected WithEvents DGREG As System.Web.UI.WebControls.DataGrid
    ' Protected WithEvents ucHeader As Header
    Private myDataTable As DataTable
    Protected WithEvents rblREC As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lblArea As System.Web.UI.WebControls.Label
    Protected WithEvents ddlRECAREA As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifmHeader As System.Web.UI.HtmlControls.HtmlGenericControl
    Public sqlString As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'ucHeader.lblTitle = "DM Search"
        If Not IsPostBack Then
            lblFName.Visible = False
            txtFName.Visible = False
            lblLName.Visible = False
            txtLName.Visible = False
            rblRM.Visible = False
            lblArea.Visible = False
            ddlRECAREA.Visible = False
        End If

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=SKU Search")
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lblFName.Visible = False
        txtFName.Visible = False
        lblLName.Visible = False
        txtLName.Visible = False
        txtFName.Text = ""
        txtLName.Text = ""
        txtNUM.Text = ""
        rblSearchType.SelectedIndex = 0
        lblresults.Visible = False
        rblRM.Visible = False
        lblArea.Visible = False
        ddlRECAREA.Visible = False
        With dgResults
            .AllowPaging = False
        End With
        dgResults.DataSource = ""
        dgResults.DataBind()
        dgResults.Visible = False

        With DGREG
            .AllowPaging = False
        End With
        DGREG.DataSource = ""
        DGREG.DataBind()
        DGREG.Visible = False

        With dgRECR
            .AllowPaging = False
        End With
        dgRECR.DataSource = ""
        dgRECR.DataBind()
        dgRECR.Visible = False
    End Sub
    Private Sub rblDM_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If rblSearchType.SelectedItem.Value = "Name" Then
            lblFName.Visible = True
            txtFName.Visible = True
            lblLName.Visible = True
            txtLName.Visible = True
            lblNUM.Visible = False
            txtNUM.Visible = False
        ElseIf rblSearchType.SelectedItem.Value = "Num" Then
            lblFName.Visible = False
            txtFName.Visible = False
            lblLName.Visible = False
            txtLName.Visible = False
            lblNUM.Visible = True
            txtNUM.Visible = True
        End If
    End Sub
    Private Sub rblRM_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rblRM.SelectedIndexChanged
        If rblSearchType.SelectedItem.Value = "Name" Then
            lblFName.Visible = True
            txtFName.Visible = True
            lblLName.Visible = True
            txtLName.Visible = True
            lblNUM.Visible = False
            txtNUM.Visible = False
        ElseIf rblSearchType.SelectedItem.Value = "Num" Then
            lblFName.Visible = False
            txtFName.Visible = False
            lblLName.Visible = False
            txtLName.Visible = False
            lblNUM.Visible = True
            txtNUM.Visible = True
        End If
    End Sub
    Private Sub rblREC_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rblREC.SelectedIndexChanged
        If rblREC.SelectedItem.Value = "Area" Then
            'show drop down list for Area's
            'sqlString = "Select Distinct Recruiter_Area from contact where recruiter_area IS NOT NULL"
            Dim conSubConnection As System.Data.SqlClient.SqlConnection
            Dim cmdSubCommand As System.Data.SqlClient.SqlCommand
            Dim dtrSubReader As System.Data.SqlClient.SqlDataReader
            conSubConnection = New System.Data.SqlClient.SqlConnection("Provider=MSDAORA.1; Data Source=genret; User ID=hottopic; Password=hottopic;")
            conSubConnection.Open()
            cmdSubCommand = New System.Data.SqlClient.SqlCommand("Select Distinct Recruiter_Area from contact where recruiter_area IS NOT NULL", conSubConnection)
            dtrSubReader = cmdSubCommand.ExecuteReader()
            ddlRECAREA.DataSource = dtrSubReader
            ddlRECAREA.DataBind()
            dtrSubReader.Close()
            conSubConnection.Close()

            lblFName.Visible = False
            txtFName.Visible = False
            lblLName.Visible = False
            txtLName.Visible = False
            lblArea.Visible = True
            ddlRECAREA.Visible = True
        ElseIf rblREC.SelectedItem.Value = "Name" Then
            lblFName.Visible = True
            txtFName.Visible = True
            lblLName.Visible = True
            txtLName.Visible = True
            lblArea.Visible = False
            ddlRECAREA.Visible = False
        End If
    End Sub
    Private Sub btnALL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnALL.Click
        If rblSearchType.SelectedItem.Value = "DM" Then
            Call ShowAllDMS()
            dgResults.Visible = True
            DGREG.Visible = False
            dgRECR.Visible = False
        ElseIf rblSearchType.SelectedItem.Value = "RM" Then
            Call ShowAllRMS()
            DGREG.Visible = True
            dgResults.Visible = False
            dgRECR.Visible = False
        ElseIf rblSearchType.SelectedItem.Value = "REGREC" Then
            Call ShowAllRECS()
            dgRECR.Visible = True
            DGREG.Visible = False
            dgResults.Visible = False
        End If
        lblresults.Visible = True
    End Sub
    Public Sub ShowAllDMS()
        Dim conFillDG As New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillDG As System.Data.SqlClient.SqlCommand
        sqlString = "Select Z.EMPID,Z.District_cd,Z.NamePreferred,Z.NameLast,extension1,blackberry_pin,office_location,office_number,fax_number,email  "
        sqlString = sqlString & "FROM CONTACT, (Select Ta.Namepreferred,Ta.namelast,D.EMPID,D.District_cd from taultimateemployee TA,DMOFFICERS D "
        sqlString = sqlString & "where TA.EMPLOYEEID= REPLICATE('0', (6 - LEN(D.EmpID))) + D.EmpID)Z where contact.empid = Z.empid order by z.district_cd "
        Dim myDataAdapter As New System.Data.SqlClient.SqlDataAdapter(sqlString, conFillDG)
        Dim myDataSet As New DataSet()
        myDataAdapter.Fill(myDataSet, "Items")
        myDataTable = myDataSet.Tables(0)
        dgResults.DataSource = myDataTable
        Dim NumPages As Integer
        NumPages = dgResults.PageCount

        'check for no results
        Dim nRows As Integer = myDataTable.Rows.Count
        If nRows = 0 Then
            'Not a valid sku.  Don't send to product locator
            With dgResults
                .AllowPaging = False
            End With
            dgResults.DataSource = ""
            dgResults.DataBind()
            dgResults.Visible = False
        Else
            'There are results so bind the data and send to product locator
            With dgResults
                .AllowPaging = True
                .PagerStyle.Mode = PagerMode.NumericPages
                .PagerStyle.PageButtonCount = 5
            End With
            dgResults.DataBind()
        End If
    End Sub
    Public Sub ShowAllRMS()
        Dim conFillDG As New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillDG As System.Data.SqlClient.SqlCommand
        sqlString = "Select Z.EMPID,Z.REGION_cd,Z.NamePreferred,Z.NameLast,extension1,blackberry_pin,office_number,fax_number,email  "
        sqlString = sqlString & "FROM CONTACT, (Select Ta.Namepreferred,Ta.namelast,D.EMPID,D.REGION_cd from taultimateemployee TA,REGOFFICERS D "
        sqlString = sqlString & "where TA.EMPLOYEEID=REPLICATE('0', (6 - LEN(D.EmpID))) + D.EmpID)Z where contact.empid = Z.empid Order by Z.Region_CD "
        Dim myDataAdapter As New System.Data.SqlClient.SqlDataAdapter(sqlString, conFillDG)
        Dim myDataSet As New DataSet()
        myDataAdapter.Fill(myDataSet, "Items")
        myDataTable = myDataSet.Tables(0)
        DGREG.DataSource = myDataTable
        Dim NumPages As Integer
        NumPages = dgResults.PageCount

        'check for no results
        Dim nRows As Integer = myDataTable.Rows.Count
        If nRows = 0 Then
            'Not a valid sku.  Don't send to product locator
            With DGREG
                .AllowPaging = False
            End With
            DGREG.DataSource = ""
            DGREG.DataBind()
            DGREG.Visible = False
        Else
            'There are results so bind the data and send to product locator
            With DGREG
                .AllowPaging = True
                .PagerStyle.Mode = PagerMode.NumericPages
                .PagerStyle.PageButtonCount = 5
            End With
            DGREG.DataBind()
        End If
    End Sub
    Public Sub ShowAllRECS()
        Dim conFillDG As New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillDG As System.Data.SqlClient.SqlCommand
        sqlString = "Select Z.EMPLOYEEID,Z.NamePreferred,Z.NameLast,extension1,blackberry_pin,fax_number,email,recruiter_area  "
        sqlString = sqlString & "FROM CONTACT, (Select TA.Employeeid,Ta.Namepreferred,Ta.namelast from taultimateemployee TA  "
        sqlString = sqlString & "where TA.JOBCODE='REGREC')Z where Replicate('0',(6 - Len(contact.empid))) + contact.empid = Z.employeeid "
        Dim myDataAdapter As New System.Data.SqlClient.SqlDataAdapter(sqlString, conFillDG)
        Dim myDataSet As New DataSet()
        myDataAdapter.Fill(myDataSet, "Items")
        myDataTable = myDataSet.Tables(0)
        dgRECR.DataSource = myDataTable
        Dim NumPages As Integer
        NumPages = dgResults.PageCount

        'check for no results
        Dim nRows As Integer = myDataTable.Rows.Count
        If nRows = 0 Then
            'Not a valid sku.  Don't send to product locator
            With dgRECR
                .AllowPaging = False
            End With
            dgRECR.DataSource = ""
            dgRECR.DataBind()
            dgRECR.Visible = False
        Else
            'There are results so bind the data and send to product locator
            With dgRECR
                .AllowPaging = True
                .PagerStyle.Mode = PagerMode.NumericPages
                .PagerStyle.PageButtonCount = 5
            End With
            dgRECR.DataBind()
        End If
    End Sub
    Private Sub dgResults_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgResults.PageIndexChanged
        dgResults.CurrentPageIndex = e.NewPageIndex
        If rblSearchType.SelectedItem.Value = "DM" Then
            Call ShowAllDMS()
        ElseIf rblSearchType.SelectedItem.Value = "RM" Then
            Call ShowAllRMS()
        ElseIf rblSearchType.SelectedItem.Value = "REGREC" Then
            Call ShowAllRECS()
        End If
        dgResults.DataBind()
        'If ChangeCriteria = 0 And VSCriteria = 0 Then
        '   dgResults.CurrentPageIndex = e.NewPageIndex
        '   Call GatherData()
        '   dgResults.DataBind()
        'ElseIf ChangeCriteria = 1 Then
        '   dgResults.CurrentPageIndex = 0
        '   Call GatherData()
        'ElseIf VSCriteria = 1 Then
        '   dgResults.CurrentPageIndex = 0
        '   Call GatherData()
        'End If
    End Sub
    Private Sub rblSearchType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rblSearchType.SelectedIndexChanged
        'Search for DM, RM, or REGIONAL RECRUITER
        lblresults.Visible = False
        If rblSearchType.SelectedItem.Value = "DM" Then
            With DGREG
                .AllowPaging = False
            End With
            DGREG.DataSource = ""
            DGREG.DataBind()
            DGREG.Visible = False
            With dgRECR
                .AllowPaging = False
            End With
            dgRECR.DataSource = ""
            dgRECR.DataBind()
            dgRECR.Visible = False
            rblDM.Visible = True
            rblRM.Visible = False
            lblFName.Visible = False
            txtFName.Visible = False
            lblLName.Visible = False
            txtLName.Visible = False
            lblNUM.Visible = True
            txtNUM.Visible = True
            lblNUM.Text = "District No."
            btnALL.Text = "Show All DMs"
        ElseIf rblSearchType.SelectedItem.Value = "RM" Then
            With dgResults
                .AllowPaging = False
            End With
            dgResults.DataSource = ""
            dgResults.DataBind()
            dgResults.Visible = False
            With dgRECR
                .AllowPaging = False
            End With
            dgRECR.DataSource = ""
            dgRECR.DataBind()
            dgRECR.Visible = False
            rblDM.Visible = False
            rblRM.Visible = True
            lblFName.Visible = False
            txtFName.Visible = False
            lblLName.Visible = False
            txtLName.Visible = False
            lblNUM.Visible = True
            txtNUM.Visible = True
            lblNUM.Text = "Region No."
            btnALL.Text = "Show All RMs"
        ElseIf rblSearchType.SelectedItem.Value = "REGREC" Then
            'only option is to search by name
            With dgResults
                .AllowPaging = False
            End With
            dgResults.DataSource = ""
            dgResults.DataBind()
            dgResults.Visible = False
            With DGREG
                .AllowPaging = False
            End With
            DGREG.DataSource = ""
            DGREG.DataBind()
            DGREG.Visible = False
            rblDM.Visible = False
            rblRM.Visible = False
            lblNUM.Visible = False
            txtNUM.Visible = False
            lblFName.Visible = False
            txtFName.Visible = False
            lblLName.Visible = False
            txtLName.Visible = False
            lblArea.Visible = True
            ddlRECAREA.Visible = True
            btnALL.Text = "Show All Regional Recruiters"
        End If
    End Sub

End Class
