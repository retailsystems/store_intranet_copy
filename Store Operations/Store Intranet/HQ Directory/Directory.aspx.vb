Option Explicit On 

Imports System.Data.SqlClient

Partial Class Directory

    Inherits System.Web.UI.Page

    'Protected WithEvents ucHeader As Header

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjDataTable As DataTable
    Private mstrSearch As String
    Dim objDAL As New clsDAL()

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        mstrSearch = Replace(Request("txtSearchStr"), "'", "''")

		'CurrBuyer.NavigateUrl = "javascript:OpenPop1('Buyer.aspx',980,560)"

		'CurrAnalysts.NavigateUrl = "javascript:OpenPop2('Analysts.aspx',810,580)"

		'CurrGeneral.NavigateUrl = "javascript:OpenPop3('generalMerchant.aspx',500,560)"

        'Put user code to initialize the page here
        'ucHeader.lblTitle = "HQ Directory"

        txtFName.Text = Trim(txtFName.Text)
        txtLName.Text = Trim(txtLName.Text)

        If Not IsPostBack Then
            txtkeyword.Visible = False
            lblkeyword.Visible = False
            ViewState("ZP") = False
            chkEP.Visible = False

            If mstrSearch > "" Then
                Call btnSearch_Click(sender, e)
            End If
        End If

        objDAL.TextboxButton(txtFName, btnSearch)
        objDAL.TextboxButton(txtkeyword, btnSearch)
        objDAL.TextboxButton(txtLName, btnSearch)

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=HQ Directory")

    End Sub

    Private Sub rblSearch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rblSearch.SelectedIndexChanged

        lblError.Text = ""
        lblError.Visible = False

        Select Case rblSearch.SelectedItem.Value
            Case "Name"
                chkEP.Visible = False
                txtkeyword.Visible = False
                lblkeyword.Visible = False
                txtFName.Visible = True
                txtLName.Visible = True
                lblFName.Visible = True
                lblLName.Visible = True
                lblSeeAlso.Visible = False

                With dgKEY
                    .AllowPaging = False
                    .DataSource = ""
                    .DataBind()
                    .Visible = False
                End With

                With dgSA
                    .AllowPaging = False
                    .DataSource = ""
                    .DataBind()
                    .Visible = False
                End With
            Case "subject"
                txtkeyword.Visible = True
                lblkeyword.Visible = True
                txtFName.Visible = False
                txtLName.Visible = False
                lblFName.Visible = False
                lblLName.Visible = False
                chkEP.Visible = True

                With dgInfo
                    .AllowPaging = False
                    .DataSource = ""
                    .DataBind()
                    .Visible = False
                End With
        End Select

    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click

        'set variables to tell submit button to set paging to zero
        ViewState("ZP") = False

        Select Case rblSearch.SelectedItem.Value
            Case "Name"
                chkEP.Visible = False
                chkEP.Checked = False
                txtFName.Visible = True
                txtLName.Visible = True
                txtFName.Text = ""
                txtLName.Text = ""
                lblFName.Visible = True
                lblLName.Visible = True
                txtkeyword.Visible = False
                lblkeyword.Visible = False
            Case "subject"
                chkEP.Visible = True
                chkEP.Checked = False
                txtkeyword.Visible = True
                txtkeyword.Text = ""
                lblkeyword.Visible = True
                txtFName.Visible = False
                txtLName.Visible = False
                lblFName.Visible = False
                lblLName.Visible = False
        End Select

        lblError.Text = ""
        lblError.Visible = False

        'rblSearch.SelectedIndex = 0
        'txtkeyword.Visible = False
        'txtkeyword.Text = ""
        'lblkeyword.Visible = False
        'txtFName.Visible = True
        'txtLName.Visible = True
        'txtFName.Text = ""
        'txtLName.Text = ""
        'lblFName.Visible = True
        'lblLName.Visible = True
        'lblError.Text = ""
        'lblError.Visible = False
        lblSeeAlso.Visible = False

        With dgInfo
            .AllowPaging = False
            .DataSource = ""
            .DataBind()
            .Visible = False
        End With

        With dgKEY
            .AllowPaging = False
            .DataSource = ""
            .DataBind()
            .Visible = False
        End With

        With dgSA
            .AllowPaging = False
            .DataSource = ""
            .DataBind()
            .Visible = False
        End With

    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim strSQL As String
        Dim PageSQL As String
        Dim conFillDG As New SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim strFirstName As String
        Dim strLastName As String
        Dim objDataAdapter As SqlDataAdapter
        Dim objDataSet As New DataSet()
        Dim intRows As Integer
        Dim intLength As Integer
        Dim strSubject As String
        Dim strWHERE As String
        Dim strKey As String
        Dim strKeyword As String
        Dim intResult As Integer
        Dim conFillDGKEY As New SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim objDataAdapterKEY As SqlDataAdapter
        Dim objDataSetKEY As New DataSet()
        Dim strTemp1 As String
        Dim strTemp2 As String
        Dim conFillDGSA As New SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim objDataAdapterSA As SqlDataAdapter
        Dim objDataSetSA As New DataSet()
        Dim intRows2, intMode As Integer
        Dim strMode As String
        strMode = ConfigurationSettings.AppSettings("Mode")
        intMode = InStr(strMode, "=", CompareMethod.Text)
        strMode = Mid(strMode, intMode + 1, 5)
        ViewState("ZP") = False

        Select Case rblSearch.SelectedItem.Value
            Case "Name"
                'Search against namepreffered, namefirst, and namelast in taultimateemplooyee table
                With dgKEY
                    .AllowPaging = False
                    .DataSource = ""
                    .DataBind()
                    .Visible = False
                End With

                With dgSA
                    .AllowPaging = False
                    .DataSource = ""
                    .DataBind()
                    .Visible = False
                End With

                lblSeeAlso.Visible = False

                strFirstName = "%" & Replace(UCase(txtFName.Text), "'", "''") & "%"
                strLastName = "%" & Replace(UCase(txtLName.Text), "'", "''") & "%"

                If txtFName.Text = "" And txtLName.Text = "" And mstrSearch = "" Then
                    'give user message that tells them to enter in info.
                    lblError.Text = "Please enter in a name to search for."
                    lblError.Visible = True
                    dgInfo.Visible = False
                    'Don't bind anything to datagrid, cuz invalid search.
                Else
                    strSQL = "SELECT DISTINCT RTRIM(Ta.namepreferred) + ' ' + RTRIM(LTRIM(Ta.namelast)) as name, ISNULL(C.Extension1,'') as Extension1, ISNULL(C.Email,'') as Email, ISNULL(C.position_dtl_desc,'') as position_dtl_desc,"
					strSQL &= " ISNULL(c.office_location,'') as office_location, ISNULL(c.office_number,'') as office_Number, ISNULL(fax_number,'') as fax_number, ISNULL(blackberry_pin,'') as blackberry_pin, ISNULL(UDFIELD13,'') as '9/80'"
					strSQL &= " FROM Contact C"
                    strSQL &= " INNER JOIN Taultimateemployee TA ON REPLICATE('0', (6 - LEN(c.EmpID))) + c.EmpID = ta.employeeid"

					If mstrSearch > "" Then
                        strSQL &= " LEFT JOIN subject B ON TA.employeeid IN (REPLICATE('0', (6 - LEN(b.contactID1))) + b.contactID1, "
                        strSQL &= "(REPLICATE('0', (6 - LEN(b.contactID2))) + b.contactID2, (REPLICATE('0', (6 - LEN(b.contactID3))) + b.contactID3, "
                        strSQL &= " (REPLICATE('0', (6 - LEN(b.contactID4))) + b.contactID4, (REPLICATE('0', (6 - LEN(b.contactID5))) + b.contactID5, "
                        strSQL &= " (REPLICATE('0', (6 - LEN(b.contactID6))) + b.contactID6, (REPLICATE('0', (6 - LEN(b.contactID7))) + b.contactID7, "
                        strSQL &= " (REPLICATE('0', (6 - LEN(b.contactID8))) + b.contactID8, (REPLICATE('0', (6 - LEN(b.contactID9))) + b.contactID9, "
                        strSQL &= " (REPLICATE('0', (6 - LEN(b.contactID10))) + b.contactID10, (REPLICATE('0', (6 - LEN(b.contactID11))) b.contactID11, "
                        strSQL &= " (REPLICATE('0', (6 - LEN(b.contactID12))) b.contactID12)"
					End If

					strSQL &= " WHERE 1=1"

					If txtFName.Text > "" Then
						strSQL &= " and (TA.namepreferred LIKE '" & strFirstName & "'"
						strSQL &= " OR Ta.NameFirst LIKE '" & strFirstName & "')"
					End If

					If txtLName.Text > "" Then
						strSQL &= " AND TA.NameLast LIKE '" & strLastName & "' "
					End If

					If txtFName.Text = "" And txtLName.Text = "" And mstrSearch > "" Then
						strSQL &= " AND (TA.namepreferred LIKE '%" & mstrSearch & "%'"
						strSQL &= " OR Ta.NameFirst LIKE '%" & mstrSearch & "%'"
						strSQL &= " OR TA.NameLast LIKE '%" & mstrSearch & "%' "
						strSQL &= " OR B.Keywords LIKE '%" & mstrSearch & "%')"
					End If

					lblError.Visible = False
					dgInfo.Visible = True
					objDataAdapter = New SqlDataAdapter(strSQL, conFillDG)
					objDataAdapter.Fill(objDataSet, "Items")
					mobjDataTable = objDataSet.Tables(0)
					dgInfo.DataSource = mobjDataTable

					intRows = mobjDataTable.Rows.Count

					'check for no results
					If intRows = 0 Then
						With dgInfo
							.AllowPaging = False
							.DataSource = ""
							.DataBind()
							.Visible = False
						End With

						lblError.Visible = True
						lblError.Text = "There were no results for your search."
					Else
						'There are results so bind the data and send to product locator
						lblError.Visible = False

						With dgInfo
							.AllowPaging = True
							.CurrentPageIndex = 0
							.PagerStyle.Mode = PagerMode.NumericPages
							.PagerStyle.PageButtonCount = 5
							.DataBind()
						End With
					End If
				End If
			Case "subject"
				dgKEY.CurrentPageIndex = 0
				dgSA.CurrentPageIndex = 0
				strSubject = Replace(txtkeyword.Text, "'", "''")

				If strSubject = "" Then			  'Begin If StrSubject is empty
					lblError.Text = "Please enter a subject for which to search."
					lblError.Visible = True
				Else
					With dgInfo
						.AllowPaging = False
						.DataSource = ""
						.DataBind()
						.Visible = False
					End With

					dgKEY.Visible = True
					lblError.Visible = False

					If Not chkEP.Checked Then
						'split up words and search separetly
						intLength = strSubject.Length				   'length of string user entered
						strWHERE = "WHERE"
						intResult = InStr(1, strSubject, " ")

						If intResult > 0 Then
							'Found a space
							Do While (intResult >= 0 And intLength > 0)
								If intResult > 0 Then
									'Then keep going through the Do Loop
									strKeyword = Left(strSubject, (intResult - 1))
									strSubject = Right(strSubject, (intLength - intResult))
									intLength = strSubject.Length
								ElseIf intResult = 0 Then
									'This will be the last time through the loop.  Set length=0 so loop will end.
									strKeyword = strSubject
									strSubject = Right(strSubject, (intLength - intResult))
									intLength = 0
								End If

								strKey = "%" & UCase(strKeyword) & "%"
								'strWHERE &= " memo1 LIKE '" & strKey & "' or memo2 LIKE '" & strKey & "' or memo3 LIKE '" & strKey & "' or "
								strWHERE &= " Keywords LIKE '" & strKey & "' or "
								intResult = InStr(1, strSubject, " ")

								If intResult = 0 And intLength = 0 Then
									'Take the "OR" off of the end of the WHERE statement
									strWHERE = Left(strWHERE, (strWHERE.Length - 3))
								End If
							Loop
						Else					'Begin If Result
							strKey = "%" & UCase(strSubject) & "%"
							'strWHERE &= " memo1 LIKE '" & strKey & "' or memo2 LIKE '" & strKey & "' or memo3 LIKE '" & strKey & "' "
							strWHERE &= " Keywords LIKE '" & strKey & "' "
						End If					'End If Result
					Else
						'search entire phrase as a whole
						strSubject = Trim(strSubject)
						strKey = "%" & UCase(strSubject) & "%"
						strWHERE = "WHERE Keywords LIKE '" & strKey & "'"
					End If

					strSQL = "SELECT dbo.PROPERCASE(RTRIM(a.namepreferred) + ' ' + RTRIM(LTRIM(a.namelast))) as Name, b.subjectDesc as subject, a.dept_desc as dept,"
					strSQL &= " ISNULL(C.Extension1,'') as extension1, ISNULL(c.Position_DTL_Desc,'') as Position_DTL_Desc, ISNULL(c.email,'') as email, ISNULL(c.office_number,'') as office_number,"
					strSQL &= " ISNULL(c.office_location,'') as office_location, ISNULL(c.fax_number,'') as fax_number, ISNULL(c.blackberry_pin,'') as blackberry_pin, ISNULL(UDFIELD13,'') as '9/80'"
					strSQL &= " FROM taUltimateEmployee A,"
					'strSQL &= " (SELECT * FROM subject WHERE memo1 LIKE '%DENTA%' or memo2 LIKE '%DENTA%' or memo3 LIKE '%DENTA%') B, "
					strSQL &= " (SELECT * FROM subject " & strWHERE & " ) B,"
					strSQL &= " (SELECT empid, extension1, email, Position_DTL_Desc, office_location, office_number, fax_number, blackberry_pin FROM contact) C"
                    strSQL &= " WHERE a.employeeid IN (REPLICATE('0', (6 - LEN(b.contactID1))) + b.contactID1, REPLICATE('0', (6 - LEN(b.contactID2))) + b.contactID2,"
                    strSQL &= "REPLICATE('0', (6 - LEN(b.contactID3))) +  b.contactID3, REPLICATE('0', (6 - LEN(b.contactID4))) + b.contactID4,"
                    strSQL &= "REPLICATE('0', (6 - LEN(b.contactID5))) +  b.contactID5, REPLICATE('0', (6 - LEN(b.contactID6))) + b.contactID6,"
                    strSQL &= "REPLICATE('0', (6 - LEN(b.contactID7))) +  b.contactID7, REPLICATE('0', (6 - LEN(b.contactID8))) + b.contactID8,"
                    strSQL &= "REPLICATE('0', (6 - LEN(b.contactID9))) +  b.contactID9, REPLICATE('0', (6 - LEN(b.contactID10))) + b.contactID10,"
                    strSQL &= "REPLICATE('0', (6 - LEN(b.contactID11))) +  b.contactID11, REPLICATE('0', (6 - LEN(b.contactID12))) + b.contactID12)"
                    strSQL &= " and a.employeeid = REPLICATE('0', (6 - LEN(c.EmpID))) + c.EmpID"
                    If Trim(UCase(strMode)) = "HOTTO" Then
                        strSQL &= " and b.subjectDesc not like '%TORRID%' "
                    Else
                        strSQL &= " and b.subjectDesc not like '%HOT%' "
                    End If
                    strSQL &= " AND (B.Flag = 0)"
                    strSQL &= " ORDER BY subject, Name"
                    objDataAdapterKEY = New SqlDataAdapter(strSQL, conFillDGKEY)
                    objDataAdapterKEY.Fill(objDataSetKEY, "Items")
                    mobjDataTable = objDataSetKEY.Tables(0)
                    dgKEY.DataSource = mobjDataTable

                    'check for no results
                    intRows = mobjDataTable.Rows.Count

                    If intRows = 0 Then    'Begin if intRows=0
                        'Not a valid sku.  Don't send to product locator
                        With dgKEY
                            .AllowPaging = False
                            .DataSource = ""
                            .DataBind()
                            .Visible = False
                        End With

                        'lblError.Visible = True
                        'lblError.Text = "There were no results for your search."
                    Else
                        'There are results so bind the data and send to product locator
                        lblError.Visible = False

                        With dgKEY
                            .AllowPaging = True
                            .PagerStyle.Mode = PagerMode.NumericPages
                            .PagerStyle.PageButtonCount = 5
                            .DataBind()
                        End With
                    End If     'End if intRows=0

                    '************************************************************************************************
                    'QUERY FOR SEE ALSO RECORDS......
                    'manipulate strWHERE to insert parenthesis
                    strWHERE &= ")"     'put ending parenthesis
                    intLength = strWHERE.Length
                    strTemp1 = Left(strWHERE, 6) & "("    'take out WHERE word in string and insert beginning parenthesis
                    strTemp2 = Right(strWHERE, (intLength - 6))
                    strWHERE = strTemp1 & strTemp2     'final string with parenthesis inserted

                    dgSA.Visible = True
                    lblSeeAlso.Visible = True
                    strSQL = " select SubjectDesc, Comments1 from subject " & strWHERE & " and flag = 1"
                    objDataAdapterSA = New SqlDataAdapter(strSQL, conFillDGSA)
                    objDataAdapterSA.Fill(objDataSetSA, "Items")
                    mobjDataTable = objDataSetSA.Tables(0)
                    dgSA.DataSource = mobjDataTable

                    'check for no results
                    intRows2 = mobjDataTable.Rows.Count

                    If intRows2 = 0 Then    'Begin if intRows2=0
                        With dgSA
                            .AllowPaging = False
                            .DataSource = ""
                            .DataBind()
                            .Visible = False
                        End With

                        lblSeeAlso.Visible = False
                    Else
                        With dgSA
                            If intRows2 > 10 Then
                                .AllowPaging = True
                            Else
                                .AllowPaging = False
                            End If

                            .PagerStyle.Mode = PagerMode.NumericPages
                            .PagerStyle.PageButtonCount = 5
                            .DataBind()
                        End With
                    End If     'End if intRows2=0

                    If intRows2 = 0 And intRows = 0 Then
                        lblError.Visible = True
                        lblError.Text = "There were no results for your search."
                    End If
				End If			 'End If StrSubject is empty
		End Select	   'End If search by subject or name

	End Sub

	Private Sub dgInfo_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgInfo.PageIndexChanged

		Dim conFillDG As New SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
		Dim strSQL As String
		Dim strFirstName As String
		Dim strLastName As String
		Dim objDataAdapter As SqlDataAdapter
		Dim objDataSet As New DataSet()

		dgInfo.CurrentPageIndex = e.NewPageIndex

		If txtFName.Text > "" Or txtLName.Text > "" Or mstrSearch > "" Then
			strFirstName = "%" & Replace(UCase(txtFName.Text), "'", "''") & "%"
			strLastName = "%" & Replace(UCase(txtLName.Text), "'", "''") & "%"

			strSQL = "SELECT DISTINCT RTRIM(Ta.namepreferred)+' '+RTRIM(LTRIM(Ta.namelast)) as name,ISNULL(C.Extension1,'') as Extension1,ISNULL(C.Email,'') as Email,ISNULL(C.position_dtl_desc,'') as position_dtl_desc, "
			strSQL &= "ISNULL(c.office_location,'') as office_location,ISNULL(c.office_number,'') as office_Number,ISNULL(fax_number,'') as fax_number,ISNULL(blackberry_pin,'') as blackberry_pin,ISNULL(UDFIELD13,'') as '9/80' "
			strSQL &= "FROM Contact C"
            strSQL &= " INNER JOIN Taultimateemployee TA ON REPLICATE('0', (6 - LEN(c.EmpID))) + c.EmpID = ta.employeeid"

			If mstrSearch > "" Then
                strSQL &= " INNER JOIN subject B ON TA.employeeid IN (REPLICATE('0', (6 - LEN(b.contactID1))) + b.contactID1, REPLICATE('0', (6 - LEN(b.contactID2))) + b.contactID2,"
                strSQL &= "REPLICATE('0', (6 - LEN(b.contactID3))) +  b.contactID3, REPLICATE('0', (6 - LEN(b.contactID4))) + b.contactID4,"
                strSQL &= "REPLICATE('0', (6 - LEN(b.contactID5))) +  b.contactID5, REPLICATE('0', (6 - LEN(b.contactID6))) + b.contactID6,"
                strSQL &= "REPLICATE('0', (6 - LEN(b.contactID7))) +  b.contactID7, REPLICATE('0', (6 - LEN(b.contactID8))) + b.contactID8,"
                strSQL &= "REPLICATE('0', (6 - LEN(b.contactID9))) +  b.contactID9, REPLICATE('0', (6 - LEN(b.contactID10))) + b.contactID10,"
                strSQL &= "REPLICATE('0', (6 - LEN(b.contactID11))) +  b.contactID11, REPLICATE('0', (6 - LEN(b.contactID12))) + b.contactID12)"

                '(b.contactID1, b.contactID2, b.contactID3, b.contactID4, b.contactID5, b.contactID6, b.contactID7, b.contactID8, b.contactID9, b.contactID10, b.contactID11, b.contactID12)"
			End If

			strSQL &= " WHERE 1=1"

			If txtFName.Text > "" Then
				strSQL &= " and (TA.namepreferred LIKE '" & strFirstName & "'"
				strSQL &= " OR Ta.NameFirst LIKE '" & strFirstName & "')"
			End If

			If txtLName.Text > "" Then
				strSQL &= " AND TA.NameLast LIKE '" & strLastName & "' "
			End If

			If txtFName.Text = "" And txtLName.Text = "" And mstrSearch > "" Then
				strSQL &= " AND (TA.namepreferred LIKE '%" & mstrSearch & "%'"
				strSQL &= " OR Ta.NameFirst LIKE '%" & mstrSearch & "%'"
				strSQL &= " OR TA.NameLast LIKE '%" & mstrSearch & "%' "
				strSQL &= " OR B.Keywords LIKE '%" & mstrSearch & "%')"
			End If

			lblError.Visible = False
			dgInfo.Visible = True
			objDataAdapter = New SqlDataAdapter(strSQL, conFillDG)
			objDataAdapter.Fill(objDataSet, "Items")
			mobjDataTable = objDataSet.Tables(0)
			dgInfo.DataSource = mobjDataTable

			'check for no results
			If mobjDataTable.Rows.Count = 0 Then
				'Not a valid sku.  Don't send to product locator
				With dgInfo
					.AllowPaging = False
					.DataSource = ""
					.DataBind()
					.Visible = False
				End With
			Else
				'There are results so bind the data and send to product locator
				With dgInfo
					.AllowPaging = True
					.PagerStyle.Mode = PagerMode.NumericPages
					.PagerStyle.PageButtonCount = 5
					.DataBind()
				End With
			End If
		Else
			dgInfo.Visible = False
			'Don't bind anything to datagrid, cuz invalid search.
		End If

		'dgInfo.DataBind()

	End Sub

	Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click

		Response.Redirect("../home.asp")

	End Sub

	Private Sub dgKey_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgKEY.PageIndexChanged

		Dim strSQL As String
		Dim strSubject As String
		Dim strWhere As String
		Dim strKey As String
		Dim strKeyword As String
		Dim strLength As Integer
		Dim intResult As Integer
		Dim conFillDGKEY As New SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
		Dim objDataAdapterKEY As SqlDataAdapter
		Dim objDataSetKEY As New DataSet()

		If ViewState("ZP") Then
			dgKEY.CurrentPageIndex = 0
			ViewState("ZP") = False
		Else
			dgKEY.CurrentPageIndex = e.NewPageIndex
		End If

		'dgKEY.CurrentPageIndex = e.NewPageIndex

		strSubject = Replace(txtkeyword.Text, "'", "''")

		If strSubject = "" Then		'Begin If StrSubject is empty
			lblError.Text = "Please enter in a subject to search for."
			lblError.Visible = True
		Else
			If Not chkEP.Checked Then
				'split up words and search separetly
				strLength = strSubject.Length			 'length of string user entered
				strWhere = "WHERE"
				intResult = InStr(1, strSubject, " ")

				If intResult > 0 Then
					'Found a space
					Do While (intResult >= 0 And strLength > 0)
						If intResult > 0 Then
							'Then keep going through the Do Loop
							strKeyword = Left(strSubject, (intResult - 1))
							strSubject = Right(strSubject, (strLength - intResult))
							strLength = strSubject.Length
						ElseIf intResult = 0 Then
							'This will be the last time through the loop.  Set length=0 so loop will end.
							strKeyword = strSubject
							strSubject = Right(strSubject, (strLength - intResult))
							strLength = 0
						End If

						strKey = "%" & UCase(strKeyword) & "%"
						'strWHERE &= " memo1 LIKE '" & strKey & "' or memo2 LIKE '" & strKey & "' or memo3 LIKE '" & strKey & "' or "
						strWhere &= " keywords LIKE '" & strKey & "' or "
						intResult = InStr(1, strSubject, " ")

						If intResult = 0 And strLength = 0 Then
							'Take the "OR" off of the end of the WHERE statement
							strWhere = Left(strWhere, (strWhere.Length - 3))
						End If
					Loop
				Else			  'Begin If Result
					strKey = "%" & UCase(strSubject) & "%"
					'strWHERE &= " memo1 LIKE '" & strKey & "' or memo2 LIKE '" & strKey & "' or memo3 LIKE '" & strKey & "' "
					strWhere &= " keywords LIKE '" & strKey & "' "
				End If			  'End If Result
			Else
				'search entire phrase as a whole
				strKey = "%" & UCase(strSubject) & "%"
				strWhere = "WHERE keywords LIKE '" & strKey & "'"
			End If		   'end chkbox if statement

			'strLength = strSubject.Length 'length of string user entered
			'strWHERE = "WHERE"
			'intResult = InStr(1, strSubject, strSpace)
			'If intResult = 0 Then  'Begin If Result
			'    strKey = "%" & UCase(strSubject) & "%"
			'    'strWHERE &= " memo1 LIKE '" & strKey & "' or memo2 LIKE '" & strKey & "' or memo3 LIKE '" & strKey & "' "
			'    strWHERE &= " keywords LIKE '" & strKey & "' "
			'ElseIf intResult > 0 Then
			'    'Found a space
			'    Do While (intResult >= 0 And strLength > 0)
			'        If intResult > 0 Then
			'            'Then keep going through the Do Loop
			'            strKeyword = Left(strSubject, (intResult - 1))
			'            strSubject = Right(strSubject, (strLength - intResult))
			'            strLength = strSubject.Length
			'        ElseIf intResult = 0 Then
			'            'This will be the last time through the loop.  Set length=0 so loop will end.
			'            strKeyword = strSubject
			'            strSubject = Right(strSubject, (strLength - intResult))
			'            strLength = 0
			'        End If

			'        strKey = "%" & UCase(Keyword) & "%"
			'        'strWHERE &= " memo1 LIKE '" & strKey & "' or memo2 LIKE '" & strKey & "' or memo3 LIKE '" & strKey & "' or "
			'        strWHERE &= " keywords LIKE '" & strKey & "' or "
			'        intResult = InStr(1, strSubject, strSpace)

			'        If intResult = 0 And strLength = 0 Then
			'            'Take the "OR" off of the end of the WHERE statement
			'            length = strWHERE.Length
			'            strWHERE = Left(strWHERE, (length - 3))
			'        End If
			'    Loop
			'End If  'End If Result

			strSQL = "SELECT RTRIM(a.namepreferred)+' '+RTRIM(LTRIM(a.namelast)) as Name, b.subjectDesc as subject, a.dept_desc as dept,"
			strSQL &= " ISNULL(C.Extension1,'') as extension1, ISNULL(c.Position_DTL_Desc,'') as Position_DTL_Desc, ISNULL(c.email,'') as email, ISNULL(c.office_number,'') as office_number,"
			strSQL &= " ISNULL(c.office_location,'') as office_location, ISNULL(c.fax_number,'') as fax_number, ISNULL(c.blackberry_pin,'') as blackberry_pin, ISNULL(UDFIELD13,'') as '9/80'"
			strSQL &= " FROM taUltimateEmployee A,"
			'strSQL &= "(SELECT * FROM subject WHERE memo1 LIKE '%DENTA%' or memo2 LIKE '%DENTA%' or memo3 LIKE '%DENTA%') B, "
			strSQL &= " (SELECT * FROM subject " & strWhere & " ) B,"
			strSQL &= " (SELECT empid, extension1,email,Position_DTL_Desc,office_location,office_number,fax_number,blackberry_pin FROM contact) C"
            strSQL &= " WHERE a.employeeid IN (REPLICATE('0', (6 - LEN(b.contactID1))) + b.contactID1, REPLICATE('0', (6 - LEN(b.contactID2))) + b.contactID2,"
            strSQL &= "REPLICATE('0', (6 - LEN(b.contactID3))) +  b.contactID3, REPLICATE('0', (6 - LEN(b.contactID4))) + b.contactID4,"
            strSQL &= "REPLICATE('0', (6 - LEN(b.contactID5))) +  b.contactID5, REPLICATE('0', (6 - LEN(b.contactID6))) + b.contactID6,"
            strSQL &= "REPLICATE('0', (6 - LEN(b.contactID7))) +  b.contactID7, REPLICATE('0', (6 - LEN(b.contactID8))) + b.contactID8,"
            strSQL &= "REPLICATE('0', (6 - LEN(b.contactID9))) +  b.contactID9, REPLICATE('0', (6 - LEN(b.contactID10))) + b.contactID10,"
            strSQL &= "REPLICATE('0', (6 - LEN(b.contactID11))) +  b.contactID11, REPLICATE('0', (6 - LEN(b.contactID12))) + b.contactID12)"
            '(b.contactID1, b.contactID2, b.contactID3, b.contactID4, b.contactID5, b.contactID6, b.contactID7, b.contactID8, b.contactID9, b.contactID10, b.contactID11, b.contactID12)"
            strSQL &= " and a.employeeid = REPLICATE('0', (6 - LEN(c.EmpID))) + c.EmpID"
			strSQL &= " ORDER BY subject, Name"
			objDataAdapterKEY = New SqlDataAdapter(strSQL, conFillDGKEY)
			objDataAdapterKEY.Fill(objDataSetKEY, "Items")
			mobjDataTable = objDataSetKEY.Tables(0)
			dgKEY.DataSource = mobjDataTable

			If mobjDataTable.Rows.Count = 0 Then		  'Begin if intRows=0
				'Not a valid sku.  Don't send to product locator
				With dgKEY
					.AllowPaging = False
					.DataSource = ""
					.DataBind()
					.Visible = False
				End With

				lblError.Visible = True
				lblError.Text = "There were no results for your search."
			Else
				'There are results so bind the data and send to product locator
				lblError.Visible = False

				With dgKEY
					.AllowPaging = True
					.PagerStyle.Mode = PagerMode.NumericPages
					.PagerStyle.PageButtonCount = 5
					.DataBind()
				End With
			End If		   'End if intRows=0
		End If	   'End If StrSubject is empty

	End Sub

	Private Sub dgSA_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgSA.PageIndexChanged

		Dim strSQL As String
		Dim strSubject As String
		Dim strKey As String
		Dim strKeyword As String
		Dim strWHERE As String
		Dim intLength As Integer
		Dim intResult As Integer
		Dim strTemp1 As String
		Dim strTemp2 As String
		Dim conFillDGSA As New SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
		Dim objDataAdapterSA As SqlDataAdapter
		Dim objDataSetSA As New DataSet()

		dgSA.CurrentPageIndex = IIf(ViewState("ZP"), 0, e.NewPageIndex)

		'dgSA.CurrentPageIndex = e.NewPageIndex
		strSubject = txtkeyword.Text
		strSubject = Replace(strSubject, "'", "''")
		intLength = strSubject.Length	   'length of string user entered
		strWHERE = "WHERE"

		If Not chkEP.Checked Then
			intResult = InStr(1, strSubject, " ")

			If intResult > 0 Then
				'Found a space
				Do While (intResult >= 0 And intLength > 0)
					If intResult > 0 Then
						'Then keep going through the Do Loop
						strKeyword = Left(strSubject, (intResult - 1))
						strSubject = Right(strSubject, (intLength - intResult))
						intLength = strSubject.Length
					ElseIf intResult = 0 Then
						'This will be the last time through the loop.  Set length=0 so loop will end.
						strKeyword = strSubject
						strSubject = Right(strSubject, (intLength - intResult))
						intLength = 0
					End If

					strKey = "%" & UCase(strKeyword) & "%"
					'strWHERE &= " memo1 LIKE '" & strKey & "' or memo2 LIKE '" & strKey & "' or memo3 LIKE '" & strKey & "' or "
					strWHERE &= " keywords LIKE '" & strKey & "' or "
					intResult = InStr(1, strSubject, " ")

					If intResult = 0 And intLength = 0 Then
						'Take the "OR" off of the end of the WHERE statement
						strWHERE = Left(strWHERE, (strWHERE.Length - 3))
					End If
				Loop
			Else		   'Begin If Result
				strKey = "%" & UCase(strSubject) & "%"
				'strWHERE &= " memo1 LIKE '" & strKey & "' or memo2 LIKE '" & strKey & "' or memo3 LIKE '" & strKey & "' "
				strWHERE &= " keywords LIKE '" & strKey & "' "
			End If		   'End If Result

			strWHERE &= ")"		   'put ending parenthesis
			intLength = strWHERE.Length
			strTemp1 = Left(strWHERE, 6) & "("		  'take out WHERE word in string and insert beginning parenthesis
			strTemp2 = Right(strWHERE, (intLength - 6))
			strWHERE = strTemp1 & strTemp2		   'final string with parenthesis inserted
		Else
			strKey = strSubject
			strWHERE &= " keywords LIKE '" & "%" & strKey & "%" & "'"
		End If

		dgSA.Visible = True
		lblSeeAlso.Visible = True
		strSQL = " select SubjectDesc, Comments1 from subject " & strWHERE & " and flag = 1"
		objDataAdapterSA = New SqlDataAdapter(strSQL, conFillDGSA)
		objDataAdapterSA.Fill(objDataSetSA, "Items")
		mobjDataTable = objDataSetSA.Tables(0)
		dgSA.DataSource = mobjDataTable

		'check for no results
		If mobjDataTable.Rows.Count = 0 Then	   'Begin if intRows2=0
			With dgSA
				.AllowPaging = False
				.DataSource = ""
				.DataBind()
				.Visible = False
			End With

			lblSeeAlso.Visible = False
		Else
			With dgSA
				.AllowPaging = True
				.PagerStyle.Mode = PagerMode.NumericPages
				.PagerStyle.PageButtonCount = 5
				.DataBind()
			End With
		End If		'End if intRows2=0

	End Sub

	Private Sub chkEP_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkEP.CheckedChanged

		'set variables to tell submit button to set paging to zero
		ViewState("ZP") = True

	End Sub

	Private Sub txtkeyword_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtkeyword.TextChanged

		'set variables to tell submit button to set paging to zero
		ViewState("ZP") = True

	End Sub




End Class
