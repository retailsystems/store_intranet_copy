Partial Class SubjectMaint
    Inherits System.Web.UI.Page
    'Protected WithEvents ucHeader As Header
    Private myDatareader As System.Data.SqlClient.SqlDataReader
    Public intSubjectid As Integer
    Public strSubjectdesc As String
    Public btFlag As Int16

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'ucHeader.lblTitle = "Subject Maintenance"
        btnDelete.Attributes.Add("onclick", "return confirm('Do you want to delete this subject?');")
        btnUpdate.Attributes.Add("onclick", "return confirm('Do you want to update this subject?');")
        lblError.Visible = False
        If Not IsPostBack Then
            Call FillSubjects()
            lblContactInstr.Visible = False
            txtContactInstr.Visible = False
            Call FillDepts()
            Call FillContact1()
            Call FillContact2()
            Call FillContact3()
            Call FillContact4()
            Call FillContact5()
            Call FillContact6()
            Call FillContact7()
            Call FillContact8()
            Call FillContact9()
            Call FillContact10()
            Call FillContact11()
            Call FillContact12()
        End If

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=Subject Maintenance")

    End Sub
    Private Sub FillSubjects()
        Dim conFillSubject As System.Data.SqlClient.SqlConnection
        conFillSubject = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillSubject As System.Data.SqlClient.SqlCommand
        cmdFillSubject = New System.Data.SqlClient.SqlCommand("Select subjectdesc,subjectid from subject order by subjectdesc", conFillSubject)
        conFillSubject.Open()
        myDatareader = cmdFillSubject.ExecuteReader
        lbSubjects.DataSource = myDatareader
        lbSubjects.DataTextField = "subjectdesc"
        lbSubjects.DataValueField = "subjectid"
        lbSubjects.DataBind()
        myDatareader.Close()
        conFillSubject.Close()
    End Sub
    Private Sub FillDepts()
        Dim conFillDept As System.Data.SqlClient.SqlConnection
        conFillDept = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillDept As System.Data.SqlClient.SqlCommand
        cmdFillDept = New System.Data.SqlClient.SqlCommand("select distinct dept,dept_desc from taultimateemployee where dept <> 'Z' order by dept_desc", conFillDept)
        conFillDept.Open()
        myDatareader = cmdFillDept.ExecuteReader
        ddlDept.DataSource = myDatareader
        ddlDept.DataTextField = "dept_desc"
        ddlDept.DataValueField = "dept"
        ddlDept.DataBind()
        ddlDept.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillDept.Close()
    End Sub
    Private Sub FillContact1()
        Dim conFillContact As System.Data.SqlClient.SqlConnection
        conFillContact = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillContact As System.Data.SqlClient.SqlCommand
        cmdFillContact = New System.Data.SqlClient.SqlCommand("SELECT employeeid,RTRIM(namepreferred) + ' ' + LTRIM(namelast) as EmpName FROM taultimateemployee where location in ('8000','8001','9000','4490') order by EmpName", conFillContact)
        conFillContact.Open()
        myDatareader = cmdFillContact.ExecuteReader
        ddlC1.DataSource = myDatareader
        ddlC1.DataTextField = "EmpName"
        ddlC1.DataValueField = "employeeid"
        ddlC1.DataBind()
        ddlC1.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillContact.Close()
    End Sub
    Private Sub FillContact2()
        Dim conFillContact As System.Data.SqlClient.SqlConnection
        conFillContact = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillContact As System.Data.SqlClient.SqlCommand
        cmdFillContact = New System.Data.SqlClient.SqlCommand("SELECT employeeid,RTRIM(namepreferred) + ' ' + LTRIM(namelast) as EmpName FROM taultimateemployee where location in ('8000','8001','9000','4490') order by EmpName", conFillContact)
        conFillContact.Open()
        myDatareader = cmdFillContact.ExecuteReader
        ddlC2.DataSource = myDatareader
        ddlC2.DataTextField = "EmpName"
        ddlC2.DataValueField = "employeeid"
        ddlC2.DataBind()
        ddlC2.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillContact.Close()
    End Sub
    Private Sub FillContact3()
        Dim conFillContact As System.Data.SqlClient.SqlConnection
        conFillContact = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillContact As System.Data.SqlClient.SqlCommand
        cmdFillContact = New System.Data.SqlClient.SqlCommand("SELECT employeeid,RTRIM(namepreferred) + ' ' + LTRIM(namelast) as EmpName FROM taultimateemployee where location in ('8000','8001','9000','4490') order by EmpName", conFillContact)
        conFillContact.Open()
        myDatareader = cmdFillContact.ExecuteReader
        ddlC3.DataSource = myDatareader
        ddlC3.DataTextField = "EmpName"
        ddlC3.DataValueField = "employeeid"
        ddlC3.DataBind()
        ddlC3.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillContact.Close()
    End Sub
    Private Sub FillContact4()
        Dim conFillContact As System.Data.SqlClient.SqlConnection
        conFillContact = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillContact As System.Data.SqlClient.SqlCommand
        cmdFillContact = New System.Data.SqlClient.SqlCommand("SELECT employeeid,RTRIM(namepreferred) + ' ' + LTRIM(namelast) as EmpName FROM taultimateemployee where location in ('8000','8001','9000','4490') order by EmpName", conFillContact)
        conFillContact.Open()
        myDatareader = cmdFillContact.ExecuteReader
        ddlC4.DataSource = myDatareader
        ddlC4.DataTextField = "EmpName"
        ddlC4.DataValueField = "employeeid"
        ddlC4.DataBind()
        ddlC4.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillContact.Close()
    End Sub
    Private Sub FillContact5()
        Dim conFillContact As System.Data.SqlClient.SqlConnection
        conFillContact = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillContact As System.Data.SqlClient.SqlCommand
        cmdFillContact = New System.Data.SqlClient.SqlCommand("SELECT employeeid,RTRIM(namepreferred) + ' ' + LTRIM(namelast) as EmpName FROM taultimateemployee where location in ('8000','8001','9000','4490') order by EmpName", conFillContact)
        conFillContact.Open()
        myDatareader = cmdFillContact.ExecuteReader
        ddlC5.DataSource = myDatareader
        ddlC5.DataTextField = "EmpName"
        ddlC5.DataValueField = "employeeid"
        ddlC5.DataBind()
        ddlC5.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillContact.Close()
    End Sub
    Private Sub FillContact6()
        Dim conFillContact As System.Data.SqlClient.SqlConnection
        conFillContact = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillContact As System.Data.SqlClient.SqlCommand
        cmdFillContact = New System.Data.SqlClient.SqlCommand("SELECT employeeid,RTRIM(namepreferred) + ' ' + LTRIM(namelast) as EmpName FROM taultimateemployee where location in ('8000','8001','9000','4490') order by EmpName", conFillContact)
        conFillContact.Open()
        myDatareader = cmdFillContact.ExecuteReader
        ddlC6.DataSource = myDatareader
        ddlC6.DataTextField = "EmpName"
        ddlC6.DataValueField = "employeeid"
        ddlC6.DataBind()
        ddlC6.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillContact.Close()
    End Sub
    Private Sub FillContact7()
        Dim conFillContact As System.Data.SqlClient.SqlConnection
        conFillContact = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillContact As System.Data.SqlClient.SqlCommand
        cmdFillContact = New System.Data.SqlClient.SqlCommand("SELECT employeeid,RTRIM(namepreferred) + ' ' + LTRIM(namelast) as EmpName FROM taultimateemployee where location in ('8000','8001','9000','4490') order by EmpName", conFillContact)
        conFillContact.Open()
        myDatareader = cmdFillContact.ExecuteReader
        ddlC7.DataSource = myDatareader
        ddlC7.DataTextField = "EmpName"
        ddlC7.DataValueField = "employeeid"
        ddlC7.DataBind()
        ddlC7.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillContact.Close()
    End Sub
    Private Sub FillContact8()
        Dim conFillContact As System.Data.SqlClient.SqlConnection
        conFillContact = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillContact As System.Data.SqlClient.SqlCommand
        cmdFillContact = New System.Data.SqlClient.SqlCommand("SELECT employeeid,RTRIM(namepreferred) + ' ' + LTRIM(namelast) as EmpName FROM taultimateemployee where location in ('8000','8001','9000','4490') order by EmpName", conFillContact)
        conFillContact.Open()
        myDatareader = cmdFillContact.ExecuteReader
        ddlC8.DataSource = myDatareader
        ddlC8.DataTextField = "EmpName"
        ddlC8.DataValueField = "employeeid"
        ddlC8.DataBind()
        ddlC8.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillContact.Close()
    End Sub
    Private Sub FillContact9()
        Dim conFillContact As System.Data.SqlClient.SqlConnection
        conFillContact = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillContact As System.Data.SqlClient.SqlCommand
        cmdFillContact = New System.Data.SqlClient.SqlCommand("SELECT employeeid,RTRIM(namepreferred) + ' ' + LTRIM(namelast) as EmpName FROM taultimateemployee where location in ('8000','8001','9000','4490') order by EmpName", conFillContact)
        conFillContact.Open()
        myDatareader = cmdFillContact.ExecuteReader
        ddlC9.DataSource = myDatareader
        ddlC9.DataTextField = "EmpName"
        ddlC9.DataValueField = "employeeid"
        ddlC9.DataBind()
        ddlC9.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillContact.Close()
    End Sub
    Private Sub FillContact10()
        Dim conFillContact As System.Data.SqlClient.SqlConnection
        conFillContact = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillContact As System.Data.SqlClient.SqlCommand
        cmdFillContact = New System.Data.SqlClient.SqlCommand("SELECT employeeid,RTRIM(namepreferred) + ' ' + LTRIM(namelast) as EmpName FROM taultimateemployee where location in ('8000','8001','9000','4490') order by EmpName", conFillContact)
        conFillContact.Open()
        myDatareader = cmdFillContact.ExecuteReader
        ddlC10.DataSource = myDatareader
        ddlC10.DataTextField = "EmpName"
        ddlC10.DataValueField = "employeeid"
        ddlC10.DataBind()
        ddlC10.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillContact.Close()
    End Sub
    Private Sub FillContact11()
        Dim conFillContact As System.Data.SqlClient.SqlConnection
        conFillContact = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillContact As System.Data.SqlClient.SqlCommand
        cmdFillContact = New System.Data.SqlClient.SqlCommand("SELECT employeeid,RTRIM(namepreferred) + ' ' + LTRIM(namelast) as EmpName FROM taultimateemployee where location in ('8000','8001','9000','4490') order by EmpName", conFillContact)
        conFillContact.Open()
        myDatareader = cmdFillContact.ExecuteReader
        ddlC11.DataSource = myDatareader
        ddlC11.DataTextField = "EmpName"
        ddlC11.DataValueField = "employeeid"
        ddlC11.DataBind()
        ddlC11.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillContact.Close()
    End Sub
    Private Sub FillContact12()
        Dim conFillContact As System.Data.SqlClient.SqlConnection
        conFillContact = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim cmdFillContact As System.Data.SqlClient.SqlCommand
        cmdFillContact = New System.Data.SqlClient.SqlCommand("SELECT employeeid,RTRIM(namepreferred) + ' ' + LTRIM(namelast) as EmpName FROM taultimateemployee where location in ('8000','8001','9000','4490') order by EmpName", conFillContact)
        conFillContact.Open()
        myDatareader = cmdFillContact.ExecuteReader
        ddlC12.DataSource = myDatareader
        ddlC12.DataTextField = "EmpName"
        ddlC12.DataValueField = "employeeid"
        ddlC12.DataBind()
        ddlC12.Items.Insert(0, "<NONE>")
        myDatareader.Close()
        conFillContact.Close()
    End Sub
    Private Sub chkDirectContact_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDirectContact.CheckedChanged
        If chkDirectContact.Checked = True Then
            'Then make contacts not visible.  Must have contact instructions
            lblC1.Visible = False
            ddlC1.Visible = False
            lblC2.Visible = False
            ddlC2.Visible = False
            lblC3.Visible = False
            ddlC3.Visible = False
            lblC4.Visible = False
            ddlC4.Visible = False
            lblC5.Visible = False
            ddlC5.Visible = False
            lblC6.Visible = False
            ddlC6.Visible = False
            lblC7.Visible = False
            ddlC7.Visible = False
            lblC8.Visible = False
            ddlC8.Visible = False
            lblC9.Visible = False
            ddlC9.Visible = False
            lblC10.Visible = False
            ddlC10.Visible = False
            lblC11.Visible = False
            ddlC11.Visible = False
            lblC12.Visible = False
            ddlC12.Visible = False
            lblDept.Visible = False
            ddlDept.Visible = False
            lblContactInstr.Visible = True
            txtContactInstr.Visible = True
        Else
            'There are direct contacts: Contacts visible, contact instructions not visible
            lblDept.Visible = True
            ddlDept.Visible = True
            lblC1.Visible = True
            ddlC1.Visible = True
            lblC2.Visible = True
            ddlC2.Visible = True
            lblC3.Visible = True
            ddlC3.Visible = True
            lblC4.Visible = True
            ddlC4.Visible = True
            lblC5.Visible = True
            ddlC5.Visible = True
            lblC6.Visible = True
            ddlC6.Visible = True
            lblC7.Visible = True
            ddlC7.Visible = True
            lblC8.Visible = True
            ddlC8.Visible = True
            lblC9.Visible = True
            ddlC9.Visible = True
            lblC10.Visible = True
            ddlC10.Visible = True
            lblC11.Visible = True
            ddlC11.Visible = True
            lblC12.Visible = True
            ddlC12.Visible = True
            lblContactInstr.Visible = False
            txtContactInstr.Visible = False

            FillDepts()
            FillContact1()
            FillContact2()
            FillContact3()
            FillContact4()
            FillContact5()
            FillContact6()
            FillContact7()
            FillContact8()
            FillContact9()
            FillContact10()
            FillContact11()
            FillContact12()
        End If
    End Sub
    Private Sub lbSubjects_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbSubjects.SelectedIndexChanged
        'User selected a subject to pull up info on.
        Dim strKeywords As String
        Dim strSQL As String
        Dim empid As String
        Dim Dept As String
        intSubjectid = lbSubjects.SelectedItem.Value
        strSQL = "Select ISNULL(Dept,'') as Dept,ISNULL(contactid1,'') as contactid1,ISNULL(contactid2,'') as contactid2, "
        strSQL = strSQL & "ISNULL(contactid3,'') as contactid3,ISNULL(contactid4,'') as contactid4,ISNULL(contactid5,'') as contactid5, "
        strSQL = strSQL & "ISNULL(contactid6,'') as contactid6,ISNULL(contactid7,'') as contactid7,ISNULL(contactid8,'') as contactid8, "
        strSQL = strSQL & "ISNULL(contactid9,'') as contactid9,ISNULL(contactid10,'') as contactid10,ISNULL(contactid11,'') as contactid11, "
        strSQL = strSQL & "ISNULL(contactid12,'') as contactid12,ISNULL(Keywords,'') as Keywords, "
        strSQL = strSQL & "ISNULL(Comments1,'') as Comments1,ISNULL(Flag,'') as Flag "
        strSQL = strSQL & "From Subject Where subjectid='" & intSubjectid & "' "


        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim objDataReader As SqlClient.SqlDataReader
        objConnection.Open()
        Dim objCommand As New SqlClient.SqlCommand(strSQL, objConnection)
        objDataReader = objCommand.ExecuteReader
        If objDataReader.Read() Then
            btFlag = objDataReader("Flag")
            If btFlag = 1 Then
                'No direct contact person, show contact instructions
                txtSubjDesc.Text = Trim(lbSubjects.SelectedItem.Text)

                If Not IsDBNull(objDataReader("keywords")) Then
                    strKeywords = objDataReader("keywords")
                Else
                    strKeywords = ""
                End If

                txtSearch.Text = Trim(strKeywords)

                lblC1.Visible = False
            ddlC1.Visible = False
            lblC2.Visible = False
            ddlC2.Visible = False
            lblC3.Visible = False
            ddlC3.Visible = False
            lblC4.Visible = False
            ddlC4.Visible = False
            lblC5.Visible = False
            ddlC5.Visible = False
            lblC6.Visible = False
            ddlC6.Visible = False
            lblC7.Visible = False
            ddlC7.Visible = False
            lblC8.Visible = False
            ddlC8.Visible = False
            lblC9.Visible = False
            ddlC9.Visible = False
            lblC10.Visible = False
            ddlC10.Visible = False
            lblC11.Visible = False
            ddlC11.Visible = False
            lblC12.Visible = False
            ddlC12.Visible = False
            lblDept.Visible = False
            ddlDept.Visible = False
            lblContactInstr.Visible = True
            txtContactInstr.Visible = True
            chkDirectContact.Checked = True

            If Not IsDBNull(objDataReader("Comments1")) Then
                txtContactInstr.Text = objDataReader("Comments1")
            Else
                txtContactInstr.Text = ""
            End If
        Else
                'Show contacts, no contact instructions
                txtSubjDesc.Text = Trim(lbSubjects.SelectedItem.Text)
                If Not IsDBNull(objDataReader("keywords")) Then
                    strKeywords = objDataReader("keywords")
                Else
                    strKeywords = ""
                End If

                txtSearch.Text = Trim(strKeywords)
               
                lblDept.Visible = True
                ddlDept.Visible = True
                lblC1.Visible = True
                ddlC1.Visible = True
                lblC2.Visible = True
                ddlC2.Visible = True
                lblC3.Visible = True
                ddlC3.Visible = True
                lblC4.Visible = True
                ddlC4.Visible = True
                lblC5.Visible = True
                ddlC5.Visible = True
                lblC6.Visible = True
                ddlC6.Visible = True
                lblC7.Visible = True
                ddlC7.Visible = True
                lblC8.Visible = True
                ddlC8.Visible = True
                lblC9.Visible = True
                ddlC9.Visible = True
                lblC10.Visible = True
                ddlC10.Visible = True
                lblC11.Visible = True
                ddlC11.Visible = True
                lblC12.Visible = True
                ddlC12.Visible = True
                lblContactInstr.Visible = False
                txtContactInstr.Visible = False
                chkDirectContact.Checked = False
                FillDepts()
                If Not IsDBNull(objDataReader("Dept")) Then
                    Dept = objDataReader("Dept")
                    ddlDept.SelectedIndex = ddlDept.Items.IndexOf(ddlDept.Items.FindByValue(Dept.ToString))
                End If
                FillContact1()
                If Not IsDBNull(objDataReader("contactid1")) Then
                    empid = objDataReader("contactid1")
                    ddlC1.SelectedIndex = ddlC1.Items.IndexOf(ddlC1.Items.FindByValue(empid.ToString))
                End If
                FillContact2()
                If Not IsDBNull(objDataReader("contactid2")) Then
                    empid = objDataReader("contactid2")
                    ddlC2.SelectedIndex = ddlC2.Items.IndexOf(ddlC2.Items.FindByValue(empid.ToString))
                End If
                FillContact3()
                If Not IsDBNull(objDataReader("contactid3")) Then
                    empid = objDataReader("contactid3")
                    ddlC3.SelectedIndex = ddlC3.Items.IndexOf(ddlC3.Items.FindByValue(empid.ToString))
                End If
                FillContact4()
                If Not IsDBNull(objDataReader("contactid4")) Then
                    empid = objDataReader("contactid4")
                    ddlC4.SelectedIndex = ddlC4.Items.IndexOf(ddlC4.Items.FindByValue(empid.ToString))
                End If
                FillContact5()
                If Not IsDBNull(objDataReader("contactid5")) Then
                    empid = objDataReader("contactid5")
                    ddlC5.SelectedIndex = ddlC5.Items.IndexOf(ddlC5.Items.FindByValue(empid.ToString))
                End If
                FillContact6()
                If Not IsDBNull(objDataReader("contactid6")) Then
                    empid = objDataReader("contactid6")
                    ddlC6.SelectedIndex = ddlC6.Items.IndexOf(ddlC6.Items.FindByValue(empid.ToString))
                End If
                FillContact7()
                If Not IsDBNull(objDataReader("contactid7")) Then
                    empid = objDataReader("contactid7")
                    ddlC7.SelectedIndex = ddlC7.Items.IndexOf(ddlC7.Items.FindByValue(empid.ToString))
                End If
                FillContact8()
                If Not IsDBNull(objDataReader("contactid8")) Then
                    empid = objDataReader("contactid8")
                    ddlC8.SelectedIndex = ddlC8.Items.IndexOf(ddlC8.Items.FindByValue(empid.ToString))
                End If
                FillContact9()
                If Not IsDBNull(objDataReader("contactid9")) Then
                    empid = objDataReader("contactid9")
                    ddlC9.SelectedIndex = ddlC9.Items.IndexOf(ddlC9.Items.FindByValue(empid.ToString))
                End If
                FillContact10()
                If Not IsDBNull(objDataReader("contactid10")) Then
                    empid = objDataReader("contactid10")
                    ddlC10.SelectedIndex = ddlC10.Items.IndexOf(ddlC10.Items.FindByValue(empid.ToString))
                End If
                FillContact11()
                If Not IsDBNull(objDataReader("contactid11")) Then
                    empid = objDataReader("contactid11")
                    ddlC11.SelectedIndex = ddlC11.Items.IndexOf(ddlC11.Items.FindByValue(empid.ToString))
                End If
                FillContact12()
                If Not IsDBNull(objDataReader("contactid12")) Then
                    empid = objDataReader("contactid12")
                    ddlC12.SelectedIndex = ddlC12.Items.IndexOf(ddlC12.Items.FindByValue(empid.ToString))
                End If
            End If
        End If
        objConnection.Close()
        objDataReader.Close()
        objCommand.Dispose()
    End Sub
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Delete the subject that is highlighted by the user
        If lbSubjects.SelectedIndex = -1 Then
            'user didn't select a subject, give error message
            lblError.Visible = True
            lblError.Text = "Please select a subject to delete."
        Else
            'user selected a subject to delete, continue
            intSubjectid = lbSubjects.SelectedItem.Value
            Dim conDelete As System.Data.SqlClient.SqlConnection
            conDelete = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
            Dim cmdDelete As System.Data.SqlClient.SqlCommand
            cmdDelete = New System.Data.SqlClient.SqlCommand("Delete FROM Subject where subjectid='" & intSubjectid & "' ", conDelete)
            conDelete.Open()
            cmdDelete.ExecuteNonQuery()
            conDelete.Close()

            Call FillSubjects()
            Call FillDepts()
            ddlC1.Visible = True
            ddlC2.Visible = True
            ddlC3.Visible = True
            ddlC4.Visible = True
            ddlC5.Visible = True
            ddlC6.Visible = True
            ddlC7.Visible = True
            ddlC8.Visible = True
            ddlC9.Visible = True
            ddlC10.Visible = True
            ddlC11.Visible = True
            ddlC12.Visible = True
            Call FillContact1()
            Call FillContact2()
            Call FillContact3()
            Call FillContact4()
            Call FillContact5()
            Call FillContact6()
            Call FillContact7()
            Call FillContact8()
            Call FillContact9()
            Call FillContact10()
            Call FillContact11()
            Call FillContact12()
            txtContactInstr.Text = ""
            txtSubjDesc.Text = ""
            txtSearch.Text = ""
            chkDirectContact.Checked = False
        End If
        
    End Sub
    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        'update a subject's information
        Dim strSQL As String
        Dim strSubjectDesc As String
        Dim strComments1 As String
        Dim intFlag As Integer
        Dim currentdate As DateTime
        Dim SearchWords As String  'Get search keywords from textbox, need to split up for Memo fields.
        Dim SearchLength As Integer 'Get length of search string keywords
        Dim strKeywords As String     'First 60 characters of search keywords
       
        If lbSubjects.SelectedIndex = -1 Then
            'user didn't select a subject, give error message.
            lblError.Visible = True
            lblError.Text = "Please select a subject to update."
        Else
            'user selected a subject, continue
            strSubjectDesc = Trim(UCase(txtSubjDesc.Text))
            strSubjectDesc = Replace(strSubjectDesc, "'", "''")
            strComments1 = Trim(txtContactInstr.Text)
            strComments1 = Replace(strComments1, "'", "''")
            currentdate = Now()
            intSubjectid = lbSubjects.SelectedItem.Value

            '********************************************************************************************
            'Get Memo Fields for update statment.
            SearchWords = Trim(UCase(txtSearch.Text))
            SearchWords = Replace(SearchWords, "'", "''")
            strKeywords = SearchWords
            '************************************************************************************************

            If chkDirectContact.Checked = True Then
                'don't need to update specific people contact info
                intFlag = 1
                strSQL = "UPDATE Subject SET subjectdesc='" & strSubjectDesc & "',Keywords='" & strKeywords & "', "
                strSQL = strSQL & "Comments1='" & strComments1 & "',Flag='" & intFlag & "',modified_date='" & currentdate & "' "
                strSQL = strSQL & " WHERE subjectid ='" & intSubjectid & "' "
                Dim conUpdate As System.Data.SqlClient.SqlConnection
                conUpdate = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
                Dim cmdUpdate As System.Data.SqlClient.SqlCommand
                cmdUpdate = New System.Data.SqlClient.SqlCommand(strSQL, conUpdate)
                conUpdate.Open()
                cmdUpdate.ExecuteNonQuery()
                conUpdate.Close()
                lblC1.Visible = False
                lblC2.Visible = False
                lblC3.Visible = False
                lblC4.Visible = False
                lblC5.Visible = False
                lblC6.Visible = False
                lblC7.Visible = False
                lblC8.Visible = False
                lblC9.Visible = False
                lblC10.Visible = False
                lblC11.Visible = False
                lblC12.Visible = False
                lblError.Visible = True
                lblError.Text = "This subject has been updated."
            ElseIf chkDirectContact.Checked = False Then
                'Need to update specific people contact info
                Dim strDept As String
                Dim strContact1 As String
                Dim strContact2 As String
                Dim strContact3 As String
                Dim strContact4 As String
                Dim strContact5 As String
                Dim strContact6 As String
                Dim strContact7 As String
                Dim strContact8 As String
                Dim strContact9 As String
                Dim strContact10 As String
                Dim strContact11 As String
                Dim strContact12 As String
                strDept = ddlDept.SelectedItem.Value
                strContact1 = ddlC1.SelectedItem.Value
                strContact2 = ddlC2.SelectedItem.Value
                strContact3 = ddlC3.SelectedItem.Value
                strContact4 = ddlC4.SelectedItem.Value
                strContact5 = ddlC5.SelectedItem.Value
                strContact6 = ddlC6.SelectedItem.Value
                strContact7 = ddlC7.SelectedItem.Value
                strContact8 = ddlC8.SelectedItem.Value
                strContact9 = ddlC9.SelectedItem.Value
                strContact10 = ddlC10.SelectedItem.Value
                strContact11 = ddlC11.SelectedItem.Value
                strContact12 = ddlC12.SelectedItem.Value
                If strDept = "<NONE>" Then
                    strDept = Nothing
                End If
                If strContact1 = "<NONE>" Then
                    strContact1 = Nothing
                End If
                If strContact2 = "<NONE>" Then
                    strContact2 = Nothing
                End If
                If strContact3 = "<NONE>" Then
                    strContact3 = Nothing
                End If
                If strContact4 = "<NONE>" Then
                    strContact4 = Nothing
                End If
                If strContact5 = "<NONE>" Then
                    strContact5 = Nothing
                End If
                If strContact6 = "<NONE>" Then
                    strContact6 = Nothing
                End If
                If strContact7 = "<NONE>" Then
                    strContact7 = Nothing
                End If
                If strContact8 = "<NONE>" Then
                    strContact8 = Nothing
                End If
                If strContact9 = "<NONE>" Then
                    strContact9 = Nothing
                End If
                If strContact10 = "<NONE>" Then
                    strContact10 = Nothing
                End If
                If strContact11 = "<NONE>" Then
                    strContact11 = Nothing
                End If
                If strContact12 = "<NONE>" Then
                    strContact12 = Nothing
                End If

                intFlag = 0
                strSQL = "Update Subject SET subjectdesc='" & strSubjectDesc & "',Dept='" & strDept & "', "
                strSQL = strSQL & "Contactid1='" & strContact1 & "',Contactid2='" & strContact2 & "',Contactid3='" & strContact3 & "', "
                strSQL = strSQL & "Contactid4='" & strContact4 & "',Contactid5='" & strContact5 & "', "
                strSQL = strSQL & "Contactid6='" & strContact6 & "',Contactid7='" & strContact7 & "', "
                strSQL = strSQL & "Contactid8='" & strContact8 & "',Contactid9='" & strContact9 & "', "
                strSQL = strSQL & "Contactid10='" & strContact10 & "',Contactid11='" & strContact11 & "',Contactid12='" & strContact12 & "', "
                strSQL = strSQL & "Keywords='" & strKeywords & "', "
                strSQL = strSQL & "Flag='" & intFlag & "',created_date='" & currentdate & "' "
                strSQL = strSQL & "WHERE subjectid ='" & intSubjectid & "' "


                Dim conUpdate As System.Data.SqlClient.SqlConnection
                conUpdate = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
                Dim cmdUpdate As System.Data.SqlClient.SqlCommand
                cmdUpdate = New System.Data.SqlClient.SqlCommand(strSQL, conUpdate)
                conUpdate.Open()
                cmdUpdate.ExecuteNonQuery()
                conUpdate.Close()
                lblError.Visible = True
                lblError.Text = "This subject has been updated."
            End If
        End If
       
    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'CLEAR CRITERIA FIELD.  SHOW BUTTONS TO INSERT OR CANCEL. HIDE BUTTONS TO ADD, UPDATE, DELETE
        btnUpdate.Visible = False
        btnAdd.Visible = False
        btnDelete.Visible = False
        btnHome.Visible = False
        btnClear.Visible = False
        btninsert.visible = True
        btnCancel.Visible = True
        lblError.Text = ""
        txtSubjDesc.Text = ""
        txtSearch.Text = ""
        txtContactInstr.Text = ""
        txtContactInstr.Visible = False
        lblContactInstr.Visible = False
        chkDirectContact.Checked = False
        lbSubjects.SelectedIndex = -1
        lblDept.Visible = True
        ddlDept.Visible = True
        ddlDept.SelectedIndex = 0
        ddlC1.Visible = True
        ddlC2.Visible = True
        ddlC3.Visible = True
        ddlC4.Visible = True
        ddlC5.Visible = True
        ddlC6.Visible = True
        ddlC7.Visible = True
        ddlC8.Visible = True
        ddlC9.Visible = True
        ddlC10.Visible = True
        ddlC11.Visible = True
        ddlC12.Visible = True
        ddlC1.SelectedIndex = 0
        ddlC2.SelectedIndex = 0
        ddlC3.SelectedIndex = 0
        ddlC4.SelectedIndex = 0
        ddlC5.SelectedIndex = 0
        ddlC6.SelectedIndex = 0
        ddlC7.SelectedIndex = 0
        ddlC8.SelectedIndex = 0
        ddlC9.SelectedIndex = 0
        ddlC10.SelectedIndex = 0
        ddlC11.SelectedIndex = 0
        ddlC12.SelectedIndex = 0


        '******************************************88
        'Dim strSQL As String
        'Dim intFlag As Integer
        'Dim currentdate As DateTime
        'Dim strKeywords As String
        'Dim strDirections As String

        'currentdate = Now()
        'strSubjectdesc = Trim(UCase(txtSubjDesc.Text))
        'strSubjectdesc = Replace(strSubjectdesc, "'", "''")
        'strKeywords = Trim(UCase(txtSearch.Text))
        'strKeywords = Replace(strKeywords, "'", "''")
        'strDirections = Trim(txtContactInstr.Text)
        'strDirections = Replace(strDirections, "'", "''")


        'If chkDirectContact.Checked = True Then
        '    'No direct contact people,just provide instructions
        '    intFlag = 1
        '    strSQL = "INSERT Subject (subjectdesc,Keywords,Comments1,Flag,created_date) "
        '    strSQL = strSQL & "Values('" & strSubjectdesc & "','" & strKeywords & "','" & strDirections & "', "
        '    strSQL = strSQL & "'" & intFlag & "','" & currentdate & "')"
        '    Dim conInsert As System.Data.SqlClient.SqlConnection
        '    conInsert = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        '    Dim cmdInsert As System.Data.SqlClient.SqlCommand
        '    cmdInsert = New System.Data.SqlClient.SqlCommand(strSQL, conInsert)
        '    conInsert.Open()
        '    cmdInsert.ExecuteNonQuery()
        '    conInsert.Close()
        '    Call FillSubjects()
        '    lblContactInstr.Visible = True
        '    txtContactInstr.Visible = True
        '    lblC1.Visible = False
        '    lblC2.Visible = False
        '    lblC3.Visible = False
        '    lblC4.Visible = False
        '    lblC5.Visible = False
        '    lblC6.Visible = False
        '    lblC7.Visible = False
        '    lblC8.Visible = False
        '    lblC9.Visible = False
        '    lblC10.Visible = False
        '    lblC11.Visible = False
        '    lblC12.Visible = False
        '    'lblDept.Visible = False
        '    'ddlDept.Visible = False
        '    lblError.Visible = True
        '    lblError.Text = "Inserted Subject."
        'Else
        '    intFlag = 0
        '    'Insert direct contact people
        '    Dim strDept As String
        '    Dim strContact1 As String
        '    Dim strContact2 As String
        '    Dim strContact3 As String
        '    Dim strContact4 As String
        '    Dim strContact5 As String
        '    Dim strContact6 As String
        '    Dim strContact7 As String
        '    Dim strContact8 As String
        '    Dim strContact9 As String
        '    Dim strContact10 As String
        '    Dim strContact11 As String
        '    Dim strContact12 As String
        '    strDept = ddlDept.SelectedItem.Value
        '    strContact1 = ddlC1.SelectedItem.Value
        '    strContact2 = ddlC2.SelectedItem.Value
        '    strContact3 = ddlC3.SelectedItem.Value
        '    strContact4 = ddlC4.SelectedItem.Value
        '    strContact5 = ddlC5.SelectedItem.Value
        '    strContact6 = ddlC6.SelectedItem.Value
        '    strContact7 = ddlC7.SelectedItem.Value
        '    strContact8 = ddlC8.SelectedItem.Value
        '    strContact9 = ddlC9.SelectedItem.Value
        '    strContact10 = ddlC10.SelectedItem.Value
        '    strContact11 = ddlC11.SelectedItem.Value
        '    strContact12 = ddlC12.SelectedItem.Value
        '    If strDept = "<NONE>" Then
        '        strDept = Nothing
        '    End If
        '    If strContact1 = "<NONE>" Then
        '        strContact1 = Nothing
        '    End If
        '    If strContact2 = "<NONE>" Then
        '        strContact2 = Nothing
        '    End If
        '    If strContact3 = "<NONE>" Then
        '        strContact3 = Nothing
        '    End If
        '    If strContact4 = "<NONE>" Then
        '        strContact4 = Nothing
        '    End If
        '    If strContact5 = "<NONE>" Then
        '        strContact5 = Nothing
        '    End If
        '    If strContact6 = "<NONE>" Then
        '        strContact6 = Nothing
        '    End If
        '    If strContact7 = "<NONE>" Then
        '        strContact7 = Nothing
        '    End If
        '    If strContact8 = "<NONE>" Then
        '        strContact8 = Nothing
        '    End If
        '    If strContact9 = "<NONE>" Then
        '        strContact9 = Nothing
        '    End If
        '    If strContact10 = "<NONE>" Then
        '        strContact10 = Nothing
        '    End If
        '    If strContact11 = "<NONE>" Then
        '        strContact11 = Nothing
        '    End If
        '    If strContact12 = "<NONE>" Then
        '        strContact12 = Nothing
        '    End If
        '    strSQL = "INSERT Subject (subjectdesc,Dept,Contactid1,Contactid2,Contactid3,Contactid4,Contactid5, "
        '    strSQL = strSQL & "Contactid6,Contactid7,Contactid8,Contactid9,Contactid10,Contactid11,Contactid12, "
        '    strSQL = strSQL & "Keywords,Flag,created_date) "
        '    strSQL = strSQL & "Values('" & strSubjectdesc & "','" & strDept & "','" & strContact1 & "', "
        '    strSQL = strSQL & "'" & strContact2 & "','" & strContact3 & "','" & strContact4 & "','" & strContact5 & "', "
        '    strSQL = strSQL & "'" & strContact6 & "','" & strContact7 & "','" & strContact8 & "','" & strContact9 & "', "
        '    strSQL = strSQL & "'" & strContact10 & "','" & strContact11 & "','" & strContact12 & "',  "
        '    strSQL = strSQL & "'" & strKeywords & "','" & intFlag & "','" & currentdate & "')"
        '    Dim conInsert As System.Data.SqlClient.SqlConnection
        '    conInsert = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        '    Dim cmdInsert As System.Data.SqlClient.SqlCommand
        '    cmdInsert = New System.Data.SqlClient.SqlCommand(strSQL, conInsert)
        '    conInsert.Open()
        '    cmdInsert.ExecuteNonQuery()
        '    conInsert.Close()
        '    lblError.Visible = True
        '    lblError.Text = "Inserted Subject."
        'End If
    End Sub
    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        Response.Redirect("../home.asp")
    End Sub
    Private Sub btnInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInsert.Click
        Dim strSQL As String
        Dim intFlag As Integer
        Dim currentdate As DateTime
        Dim strKeywords As String
        Dim strDirections As String

        currentdate = Now()
        strSubjectdesc = Trim(UCase(txtSubjDesc.Text))
        strSubjectdesc = Replace(strSubjectdesc, "'", "''")
        strKeywords = Trim(UCase(txtSearch.Text))
        strKeywords = Replace(strKeywords, "'", "''")
        strDirections = Trim(txtContactInstr.Text)
        strDirections = Replace(strDirections, "'", "''")

        If strSubjectdesc = "" Then
            'Dont' allow insert of blank subject
            lblError.Visible = True
            lblError.Text = "Can not insert a blank subject!"
            btnCancel.Visible = True
            btnInsert.Visible = True
            btnClear.Visible = False
            btnAdd.Visible = False
            btnUpdate.Visible = False
            btnDelete.Visible = False
            btnHome.Visible = False
        Else
            'continue
            If chkDirectContact.Checked = True Then
                'No direct contact people,just provide instructions
                intFlag = 1
                strSQL = "INSERT Subject (subjectdesc,Keywords,Comments1,Flag,created_date) "
                strSQL = strSQL & "Values('" & strSubjectdesc & "','" & strKeywords & "','" & strDirections & "', "
                strSQL = strSQL & "'" & intFlag & "','" & currentdate & "')"
                Dim conInsert As System.Data.SqlClient.SqlConnection
                conInsert = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
                Dim cmdInsert As System.Data.SqlClient.SqlCommand
                cmdInsert = New System.Data.SqlClient.SqlCommand(strSQL, conInsert)
                conInsert.Open()
                cmdInsert.ExecuteNonQuery()
                conInsert.Close()
                Call FillSubjects()
                lblContactInstr.Visible = True
                txtContactInstr.Visible = True
                lblC1.Visible = False
                lblC2.Visible = False
                lblC3.Visible = False
                lblC4.Visible = False
                lblC5.Visible = False
                lblC6.Visible = False
                lblC7.Visible = False
                lblC8.Visible = False
                lblC9.Visible = False
                lblC10.Visible = False
                lblC11.Visible = False
                lblC12.Visible = False
                'lblDept.Visible = False
                'ddlDept.Visible = False
                lblError.Visible = True
                lblError.Text = "Inserted Subject."
                Call FillSubjects()
            Else
                intFlag = 0
                'Insert direct contact people
                Dim strDept As String
                Dim strContact1 As String
                Dim strContact2 As String
                Dim strContact3 As String
                Dim strContact4 As String
                Dim strContact5 As String
                Dim strContact6 As String
                Dim strContact7 As String
                Dim strContact8 As String
                Dim strContact9 As String
                Dim strContact10 As String
                Dim strContact11 As String
                Dim strContact12 As String
                strDept = ddlDept.SelectedItem.Value
                strContact1 = ddlC1.SelectedItem.Value
                strContact2 = ddlC2.SelectedItem.Value
                strContact3 = ddlC3.SelectedItem.Value
                strContact4 = ddlC4.SelectedItem.Value
                strContact5 = ddlC5.SelectedItem.Value
                strContact6 = ddlC6.SelectedItem.Value
                strContact7 = ddlC7.SelectedItem.Value
                strContact8 = ddlC8.SelectedItem.Value
                strContact9 = ddlC9.SelectedItem.Value
                strContact10 = ddlC10.SelectedItem.Value
                strContact11 = ddlC11.SelectedItem.Value
                strContact12 = ddlC12.SelectedItem.Value
                If strDept = "<NONE>" Then
                    strDept = Nothing
                End If
                If strContact1 = "<NONE>" Then
                    strContact1 = Nothing
                End If
                If strContact2 = "<NONE>" Then
                    strContact2 = Nothing
                End If
                If strContact3 = "<NONE>" Then
                    strContact3 = Nothing
                End If
                If strContact4 = "<NONE>" Then
                    strContact4 = Nothing
                End If
                If strContact5 = "<NONE>" Then
                    strContact5 = Nothing
                End If
                If strContact6 = "<NONE>" Then
                    strContact6 = Nothing
                End If
                If strContact7 = "<NONE>" Then
                    strContact7 = Nothing
                End If
                If strContact8 = "<NONE>" Then
                    strContact8 = Nothing
                End If
                If strContact9 = "<NONE>" Then
                    strContact9 = Nothing
                End If
                If strContact10 = "<NONE>" Then
                    strContact10 = Nothing
                End If
                If strContact11 = "<NONE>" Then
                    strContact11 = Nothing
                End If
                If strContact12 = "<NONE>" Then
                    strContact12 = Nothing
                End If
                strSQL = "INSERT Subject (subjectdesc,Dept,Contactid1,Contactid2,Contactid3,Contactid4,Contactid5, "
                strSQL = strSQL & "Contactid6,Contactid7,Contactid8,Contactid9,Contactid10,Contactid11,Contactid12, "
                strSQL = strSQL & "Keywords,Flag,created_date) "
                strSQL = strSQL & "Values('" & strSubjectdesc & "','" & strDept & "','" & strContact1 & "', "
                strSQL = strSQL & "'" & strContact2 & "','" & strContact3 & "','" & strContact4 & "','" & strContact5 & "', "
                strSQL = strSQL & "'" & strContact6 & "','" & strContact7 & "','" & strContact8 & "','" & strContact9 & "', "
                strSQL = strSQL & "'" & strContact10 & "','" & strContact11 & "','" & strContact12 & "',  "
                strSQL = strSQL & "'" & strKeywords & "','" & intFlag & "','" & currentdate & "')"
                Dim conInsert As System.Data.SqlClient.SqlConnection
                conInsert = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
                Dim cmdInsert As System.Data.SqlClient.SqlCommand
                cmdInsert = New System.Data.SqlClient.SqlCommand(strSQL, conInsert)
                conInsert.Open()
                cmdInsert.ExecuteNonQuery()
                conInsert.Close()
                lblError.Visible = True
                lblError.Text = "Inserted Subject."
                Call FillSubjects()
            End If
            btnCancel.Visible = False
            btnInsert.Visible = False
            btnClear.Visible = True
            btnAdd.Visible = True
            btnUpdate.Visible = True
            btnDelete.Visible = True
            btnHome.Visible = True
        End If
        'If chkDirectContact.Checked = True Then
        '    'No direct contact people,just provide instructions
        '    intFlag = 1
        '    strSQL = "INSERT Subject (subjectdesc,Keywords,Comments1,Flag,created_date) "
        '    strSQL = strSQL & "Values('" & strSubjectdesc & "','" & strKeywords & "','" & strDirections & "', "
        '    strSQL = strSQL & "'" & intFlag & "','" & currentdate & "')"
        '    Dim conInsert As System.Data.SqlClient.SqlConnection
        '    conInsert = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        '    Dim cmdInsert As System.Data.SqlClient.SqlCommand
        '    cmdInsert = New System.Data.SqlClient.SqlCommand(strSQL, conInsert)
        '    conInsert.Open()
        '    cmdInsert.ExecuteNonQuery()
        '    conInsert.Close()
        '    Call FillSubjects()
        '    lblContactInstr.Visible = True
        '    txtContactInstr.Visible = True
        '    lblC1.Visible = False
        '    lblC2.Visible = False
        '    lblC3.Visible = False
        '    lblC4.Visible = False
        '    lblC5.Visible = False
        '    lblC6.Visible = False
        '    lblC7.Visible = False
        '    lblC8.Visible = False
        '    lblC9.Visible = False
        '    lblC10.Visible = False
        '    lblC11.Visible = False
        '    lblC12.Visible = False
        '    'lblDept.Visible = False
        '    'ddlDept.Visible = False
        '    lblError.Visible = True
        '    lblError.Text = "Inserted Subject."
        '    Call FillSubjects()
        'Else
        '    intFlag = 0
        '    'Insert direct contact people
        '    Dim strDept As String
        '    Dim strContact1 As String
        '    Dim strContact2 As String
        '    Dim strContact3 As String
        '    Dim strContact4 As String
        '    Dim strContact5 As String
        '    Dim strContact6 As String
        '    Dim strContact7 As String
        '    Dim strContact8 As String
        '    Dim strContact9 As String
        '    Dim strContact10 As String
        '    Dim strContact11 As String
        '    Dim strContact12 As String
        '    strDept = ddlDept.SelectedItem.Value
        '    strContact1 = ddlC1.SelectedItem.Value
        '    strContact2 = ddlC2.SelectedItem.Value
        '    strContact3 = ddlC3.SelectedItem.Value
        '    strContact4 = ddlC4.SelectedItem.Value
        '    strContact5 = ddlC5.SelectedItem.Value
        '    strContact6 = ddlC6.SelectedItem.Value
        '    strContact7 = ddlC7.SelectedItem.Value
        '    strContact8 = ddlC8.SelectedItem.Value
        '    strContact9 = ddlC9.SelectedItem.Value
        '    strContact10 = ddlC10.SelectedItem.Value
        '    strContact11 = ddlC11.SelectedItem.Value
        '    strContact12 = ddlC12.SelectedItem.Value
        '    If strDept = "<NONE>" Then
        '        strDept = Nothing
        '    End If
        '    If strContact1 = "<NONE>" Then
        '        strContact1 = Nothing
        '    End If
        '    If strContact2 = "<NONE>" Then
        '        strContact2 = Nothing
        '    End If
        '    If strContact3 = "<NONE>" Then
        '        strContact3 = Nothing
        '    End If
        '    If strContact4 = "<NONE>" Then
        '        strContact4 = Nothing
        '    End If
        '    If strContact5 = "<NONE>" Then
        '        strContact5 = Nothing
        '    End If
        '    If strContact6 = "<NONE>" Then
        '        strContact6 = Nothing
        '    End If
        '    If strContact7 = "<NONE>" Then
        '        strContact7 = Nothing
        '    End If
        '    If strContact8 = "<NONE>" Then
        '        strContact8 = Nothing
        '    End If
        '    If strContact9 = "<NONE>" Then
        '        strContact9 = Nothing
        '    End If
        '    If strContact10 = "<NONE>" Then
        '        strContact10 = Nothing
        '    End If
        '    If strContact11 = "<NONE>" Then
        '        strContact11 = Nothing
        '    End If
        '    If strContact12 = "<NONE>" Then
        '        strContact12 = Nothing
        '    End If
        '    strSQL = "INSERT Subject (subjectdesc,Dept,Contactid1,Contactid2,Contactid3,Contactid4,Contactid5, "
        '    strSQL = strSQL & "Contactid6,Contactid7,Contactid8,Contactid9,Contactid10,Contactid11,Contactid12, "
        '    strSQL = strSQL & "Keywords,Flag,created_date) "
        '    strSQL = strSQL & "Values('" & strSubjectdesc & "','" & strDept & "','" & strContact1 & "', "
        '    strSQL = strSQL & "'" & strContact2 & "','" & strContact3 & "','" & strContact4 & "','" & strContact5 & "', "
        '    strSQL = strSQL & "'" & strContact6 & "','" & strContact7 & "','" & strContact8 & "','" & strContact9 & "', "
        '    strSQL = strSQL & "'" & strContact10 & "','" & strContact11 & "','" & strContact12 & "',  "
        '    strSQL = strSQL & "'" & strKeywords & "','" & intFlag & "','" & currentdate & "')"
        '    Dim conInsert As System.Data.SqlClient.SqlConnection
        '    conInsert = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        '    Dim cmdInsert As System.Data.SqlClient.SqlCommand
        '    cmdInsert = New System.Data.SqlClient.SqlCommand(strSQL, conInsert)
        '    conInsert.Open()
        '    cmdInsert.ExecuteNonQuery()
        '    conInsert.Close()
        '    lblError.Visible = True
        '    lblError.Text = "Inserted Subject."
        '    Call FillSubjects()
        'End If
        'btnCancel.Visible = False
        'btnInsert.Visible = False
        'btnAdd.Visible = True
        'btnUpdate.Visible = True
        'btnDelete.Visible = True
        'btnHome.Visible = True
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        btnCancel.Visible = False
        btnInsert.Visible = False
        btnClear.Visible = True
        btnAdd.Visible = True
        btnUpdate.Visible = True
        btnDelete.Visible = True
        btnHome.Visible = True
        ddlDept.Visible = True
        ddlDept.SelectedIndex = 0
        ddlC1.Visible = True
        ddlC2.Visible = True
        ddlC3.Visible = True
        ddlC4.Visible = True
        ddlC5.Visible = True
        ddlC6.Visible = True
        ddlC7.Visible = True
        ddlC8.Visible = True
        ddlC9.Visible = True
        ddlC10.Visible = True
        ddlC11.Visible = True
        ddlC12.Visible = True
        ddlC1.SelectedIndex = 0
        ddlC2.SelectedIndex = 0
        ddlC3.SelectedIndex = 0
        ddlC4.SelectedIndex = 0
        ddlC5.SelectedIndex = 0
        ddlC6.SelectedIndex = 0
        ddlC7.SelectedIndex = 0
        ddlC8.SelectedIndex = 0
        ddlC9.SelectedIndex = 0
        ddlC10.SelectedIndex = 0
        ddlC11.SelectedIndex = 0
        ddlC12.SelectedIndex = 0
        txtContactInstr.Visible = False
        lblContactInstr.Visible = False
        chkDirectContact.Checked = False
        lblDept.Visible = True
        lblError.Text = ""
        txtSubjDesc.Text = ""
        txtSearch.Text = ""
        txtContactInstr.Text = ""
    End Sub
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lbSubjects.SelectedIndex = -1
        lblError.Text = ""
        txtSubjDesc.Text = ""
        txtSearch.Text = ""
        txtContactInstr.Text = ""
        txtContactInstr.Visible = False
        lblContactInstr.Visible = False
        chkDirectContact.Checked = False
        btnCancel.Visible = False
        btnInsert.Visible = False
        btnAdd.Visible = True
        btnUpdate.Visible = True
        btnDelete.Visible = True
        btnHome.Visible = True
        ddlDept.Visible = True
        ddlDept.SelectedIndex = 0
        ddlC1.Visible = True
        ddlC2.Visible = True
        ddlC3.Visible = True
        ddlC4.Visible = True
        ddlC5.Visible = True
        ddlC6.Visible = True
        ddlC7.Visible = True
        ddlC8.Visible = True
        ddlC9.Visible = True
        ddlC10.Visible = True
        ddlC11.Visible = True
        ddlC12.Visible = True
        ddlC1.SelectedIndex = 0
        ddlC2.SelectedIndex = 0
        ddlC3.SelectedIndex = 0
        ddlC4.SelectedIndex = 0
        ddlC5.SelectedIndex = 0
        ddlC6.SelectedIndex = 0
        ddlC7.SelectedIndex = 0
        ddlC8.SelectedIndex = 0
        ddlC9.SelectedIndex = 0
        ddlC10.SelectedIndex = 0
        ddlC11.SelectedIndex = 0
        ddlC12.SelectedIndex = 0
    End Sub
End Class
