Public Class ContactMaint
    Inherits System.Web.UI.Page
    Protected WithEvents lblEmp As System.Web.UI.WebControls.Label
    Protected WithEvents lbEmployees As System.Web.UI.WebControls.ListBox
    'Protected WithEvents ucHeader As Header
    Protected WithEvents lblExt As System.Web.UI.WebControls.Label
    Protected WithEvents txtExt As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEmail As System.Web.UI.WebControls.Label
    Protected WithEvents txtEmail As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblOffLoc As System.Web.UI.WebControls.Label
    Protected WithEvents txtOffLoc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblOffNum As System.Web.UI.WebControls.Label
    Protected WithEvents txtOffNum As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblFax As System.Web.UI.WebControls.Label
    Protected WithEvents txtFax As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblBlackberry As System.Web.UI.WebControls.Label
    Protected WithEvents txtBlackberry As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtJob As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblJob As System.Web.UI.WebControls.Label
    Protected WithEvents lblrecruiter As System.Web.UI.WebControls.Label
    Protected WithEvents txtRecruiter As System.Web.UI.WebControls.TextBox
    Protected WithEvents Delete As System.Web.UI.WebControls.Button
    Protected WithEvents btnUpdate As System.Web.UI.WebControls.Button
    Private myDatareader As System.Data.SqlClient.SqlDataReader
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents btnHome As System.Web.UI.WebControls.Button
    Protected WithEvents btnClear As System.Web.UI.WebControls.Button
    Public strSQL As String
    Protected WithEvents hlSearch As System.Web.UI.WebControls.HyperLink
    Public empid As String
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifmHeader As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'ucHeader.lblTitle = "Contact Maintenance"
        lblError.Visible = False
        'pageBody.Attributes.Add("onload", "")
        Delete.Attributes.Add("onclick", "return confirm('Do you want to delete this person\'s contact information?');")
        btnUpdate.Attributes.Add("onclick", "return confirm('Do you want to update this person\'s contact information?');")
        If Not IsPostBack Then
            Dim conFillEmployee As System.Data.SqlClient.SqlConnection
            conFillEmployee = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
            Dim cmdFillEmployee As System.Data.SqlClient.SqlCommand
            cmdFillEmployee = New System.Data.SqlClient.SqlCommand("Select RTRIM(Namepreferred) + ' ' + LTRIM(NameLast) as Name,Employeeid From Taultimateemployee where location IN ('8000','8001','9000','4490') Order By NamePreferred", conFillEmployee)
            'cmdFillEmployee = New System.Data.SqlClient.SqlCommand("Select Distinct RTRIM(Namepreferred) + ' ' + LTRIM(NameLast) as Name,Employeeid From Taultimateemployee where location IN ('8000','8001','9000') ", conFillEmployee)
            conFillEmployee.Open()
            myDatareader = cmdFillEmployee.ExecuteReader
            lbEmployees.DataSource = myDatareader
            lbEmployees.DataTextField = "Name"
            lbEmployees.DataValueField = "Employeeid"
            lbEmployees.DataBind()
            myDatareader.Close()
            conFillEmployee.Close()
        End If

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=Contact Maintenance")

    End Sub
    Private Sub GetContactInfo()
        empid = lbEmployees.SelectedItem.Value

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
        Dim objDataReader As SqlClient.SqlDataReader
        objConnection.Open()
        Dim objCommand As New SqlClient.SqlCommand("Select Extension1,Email,Office_Location,Office_Number,position_dtl_Desc,fax_number,blackberry_pin,recruiter_area FROM CONTACT WHERE empid= '" + empid + "' ", objConnection)
        objDataReader = objCommand.ExecuteReader
        If objDataReader.Read() Then
            If Not IsDBNull(objDataReader("Extension1")) Then
                txtExt.Text = Trim(objDataReader("Extension1"))
            Else
                txtExt.Text = ""
            End If

            If Not IsDBNull(objDataReader("Email")) Then
                txtEmail.Text = Trim(objDataReader("Email"))
            Else
                txtEmail.Text = ""
            End If

            If Not IsDBNull(objDataReader("Office_Location")) Then
                txtOffLoc.Text = Trim(objDataReader("Office_Location"))
            Else
                txtOffLoc.Text = ""
            End If

            If Not IsDBNull(objDataReader("Office_Number")) Then
                txtOffNum.Text = Trim(objDataReader("Office_Number"))
            Else
                txtOffNum.Text = ""
            End If

            If Not IsDBNull(objDataReader("Fax_Number")) Then
                txtFax.Text = Trim(objDataReader("Fax_Number"))
            Else
                txtFax.Text = ""
            End If

            If Not IsDBNull(objDataReader("Blackberry_pin")) Then
                txtBlackberry.Text = Trim(objDataReader("Blackberry_pin"))
            Else
                txtBlackberry.Text = ""
            End If

            If Not IsDBNull(objDataReader("position_dtl_desc")) Then
                txtJob.Text = Trim(objDataReader("position_dtl_desc"))
            Else
                txtJob.Text = ""
            End If

            If Not IsDBNull(objDataReader("recruiter_area")) Then
                lblrecruiter.Visible = True
                txtRecruiter.Visible = True
                txtRecruiter.Text = Trim(objDataReader("recruiter_area"))
            ElseIf (IsDBNull(objDataReader("recruiter_area"))) Then
                lblrecruiter.Visible = False
                txtRecruiter.Visible = False
                txtRecruiter.Text = ""
            Else
                lblrecruiter.Visible = False
                txtRecruiter.Visible = False
                txtRecruiter.Text = ""
            End If
        Else
            txtExt.Text = ""
            txtEmail.Text = ""
            txtOffLoc.Text = ""
            txtOffNum.Text = ""
            txtFax.Text = ""
            lblrecruiter.Visible = True
            txtRecruiter.Visible = True
            txtRecruiter.Text = ""
            txtBlackberry.Text = ""
            txtJob.Text = ""
        End If
        objConnection.Close()
        objDataReader.Close()
        objCommand.Dispose()
    End Sub
    Private Sub lbEmployees_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbEmployees.SelectedIndexChanged
        'grab employee id user selected, pull up contact info if it exists
        Call GetContactInfo()
    End Sub
    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        'Update or Insert Contact information for employee
        Dim blnContinue As Boolean 'If False, b/c showing error message to user, don't insert or update
        Dim strExt As String
        Dim strEmail As String
        Dim strOffLoc As String
        Dim strOffNum As String
        Dim strFax As String
        Dim strBlackberry As String
        Dim strJob As String
        Dim strRecArea As String
        Dim CurrentDate As Date
        Dim NumCheck As Boolean
        CurrentDate = Now()

        If lbEmployees.SelectedIndex = -1 Then
            'user didn't select an employee, show an error message.
            lblError.Visible = True
            lblError.Text = "Please select an employee to update their information."
        Else
            'user has selected an employee, continue
            strExt = Trim(txtExt.Text)
            strExt = Replace(strExt, "'", "''")
            strEmail = Trim(txtEmail.Text)
            strEmail = Replace(strEmail, "'", "''")
            strOffLoc = Trim(txtOffLoc.Text)
            strOffLoc = Replace(strOffLoc, "'", "''")
            strOffNum = Trim(txtOffNum.Text)
            strOffNum = Replace(strOffNum, "'", "''")
            strFax = Trim(txtFax.Text)
            strFax = Replace(strFax, "'", "''")
            strBlackberry = Trim(txtBlackberry.Text)
            strBlackberry = Replace(strBlackberry, "'", "''")
            strJob = Trim(txtJob.Text)
            strJob = Replace(strJob, "'", "''")
            strRecArea = Trim(txtRecruiter.Text)
            strRecArea = Replace(strRecArea, "'", "''")
            empid = lbEmployees.SelectedItem.Value
            blnContinue = True
            If strExt <> "" Then
                NumCheck = IsNumeric(strExt)
                If NumCheck = True Then
                    lblError.Visible = False
                Else
                    blnContinue = False
                    lblError.Visible = True
                    lblError.Text = "The extension entered is invalid.  It should be numeric."
                End If
            ElseIf strBlackberry <> "" Then
                NumCheck = IsNumeric(strBlackberry)
                If NumCheck = True Then
                    lblError.Visible = False
                Else
                    blnContinue = False
                    lblError.Visible = True
                    lblError.Text = "The blackberry pin entered is invalid.  It should be numeric."
                End If
            ElseIf (strExt = "" And strEmail = "" And strOffLoc = "" And strOffNum = "" And strFax = "" And strBlackberry = "" And strJob = "" And strRecArea = "") Then
                'check to make sure not all fields are blank!!!!
                blnContinue = False
                lblError.Visible = True
                lblError.Text = "Please fill in the information to be updated or inserted!"
            End If

            If blnContinue = False Then
                'Don't Insert or Update.  Show error message to user.
            ElseIf blnContinue = True Then
                'continue with insert/update
                Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
                Dim objDataReader As SqlClient.SqlDataReader
                objConnection.Open()
                Dim objCommand As New SqlClient.SqlCommand("Select Extension1,Email,Office_Location,Office_Number,position_dtl_Desc,fax_number,blackberry_pin,recruiter_area FROM CONTACT WHERE empid= '" + empid + "' ", objConnection)
                objDataReader = objCommand.ExecuteReader
                If objDataReader.Read() Then
                    'Update contact info
                    strSQL = "UPDATE Contact SET Extension1='" & strExt & "',Email='" & strEmail & "',Office_Location='" & strOffLoc & "',Office_Number='" & strOffNum & "', "
                    strSQL = strSQL & "Fax_Number='" & strFax & "',Blackberry_Pin='" & strBlackberry & "',Position_DTL_DESC='" & strJob & "',Recruiter_Area='" & strRecArea & "', "
                    strSQL = strSQL & "modified_date='" & CurrentDate & "' WHERE empid='" + empid + "' "
                    Dim conUpdate As System.Data.SqlClient.SqlConnection
                    conUpdate = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
                    Dim cmdUpdate As System.Data.SqlClient.SqlCommand
                    cmdUpdate = New System.Data.SqlClient.SqlCommand(strSQL, conUpdate)
                    conUpdate.Open()
                    cmdUpdate.ExecuteNonQuery()
                    conUpdate.Close()
                    lblError.Text = "The person's information has been updated."
                    lblError.Visible = True
                Else
                    'Insert new contact information
                    strSQL = "INSERT Contact (Empid,Extension1,Email,Office_Location,Office_Number,Fax_Number,Blackberry_Pin,Position_DTL_DESC,Recruiter_Area,Created_Date) "
                    strSQL = strSQL & "VALUES ('" & empid & "','" & strExt & "','" & strEmail & "','" & strOffLoc & "','" & strOffNum & "', "
                    strSQL = strSQL & "'" & strFax & "','" & strBlackberry & "','" & strJob & "','" & strRecArea & "','" & CurrentDate & "')"

                    Dim conInsert As System.Data.SqlClient.SqlConnection
                    conInsert = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
                    Dim cmdInsert As System.Data.SqlClient.SqlCommand
                    cmdInsert = New System.Data.SqlClient.SqlCommand(strSQL, conInsert)
                    conInsert.Open()
                    cmdInsert.ExecuteNonQuery()
                    conInsert.Close()
                    lblError.Text = "The person's information has been inserted."
                    lblError.Visible = True
                End If
                objConnection.Close()
                objDataReader.Close()
                objCommand.Dispose()
            End If  'End blnContinue STMT

        End If
    End Sub
    Private Sub Delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Delete.Click
        'Delete Contact info for employee
        If lbEmployees.SelectedIndex = -1 Then
            'user didn't select employee, give error message
            lblError.Text = "Please select a person in order to delete their information."
            lblError.Visible = True
        Else
            'continue, user selected an employee
            empid = lbEmployees.SelectedItem.Value
            Dim conDelete As System.Data.SqlClient.SqlConnection
            conDelete = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
            Dim cmdDelete As System.Data.SqlClient.SqlCommand
            cmdDelete = New System.Data.SqlClient.SqlCommand("Delete FROM Contact where empid='" + empid + "' ", conDelete)
            conDelete.Open()
            cmdDelete.ExecuteNonQuery()
            conDelete.Close()

            Call GetContactInfo()
            lblError.Text = "The person's information has been deleted."
            lblError.Visible = True
        End If
        
    End Sub
    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        Response.Redirect("../home.asp")
    End Sub
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lblError.Visible = False
        lbEmployees.SelectedIndex = -1
        txtExt.Text = ""
        txtJob.Text = ""
        txtBlackberry.Text = ""
        txtFax.Text = ""
        txtRecruiter.Text = ""
        txtOffLoc.Text = ""
        txtOffNum.Text = ""
        txtEmail.Text = ""
    End Sub
End Class
