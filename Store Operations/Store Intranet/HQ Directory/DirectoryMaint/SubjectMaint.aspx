<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SubjectMaint.aspx.vb" Inherits="DirectoryMaint.SubjectMaint" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Subject Maintenance</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/hottopic.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
  </HEAD>
	<body id="pageBody">
		<form id="Form1" method="post" runat="server">
			<iframe class="Header_Frame" id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="http://172.16.125.2/Header/Header.aspx?mode=hottopic" frameBorder="0" width="100%" scrolling="no" runat="server">
			</iframe>
			<table width="100%">
				<tr>
					<td width="50%">
						<asp:label id="lblSubj" Text="Subjects:" Runat="server" Visible="True" EnableViewState="False">Subjects:</asp:label></td>
					<td width="1%">&nbsp;</td>
					<td width="49%">&nbsp;</td>
				</tr>
				<tr>
					<td rowspan="16" vAlign="top">
						<asp:listbox id="lbSubjects" Runat="server" SelectionMode="Single" Height="400px" AutoPostBack="True" Width="100%"></asp:listbox></td>
					<td noWrap>
						<asp:label id="lblSubjDesc" Runat="server" text="Subject Description:">Subject Description:</asp:label></td>
					<td>
						<asp:textbox id="txtSubjDesc" Runat="server" MaxLength="40" Width="100%"></asp:textbox></td>
				</tr>
				<tr vAlign="top">
					<td noWrap width="1%">
						<asp:label id="lblSearch" Runat="server" text="Search Phrases:">Search Phrases:</asp:label></td>
					<td>
						<asp:textbox id="txtSearch" Runat="server" MaxLength="180" Width="100%" TextMode="MultiLine"></asp:textbox></td>
				</tr>
				<tr>
					<td noWrap width="1%">
						<asp:label id="lblchkDirectContact" Text="No Direct Contact:" Runat="server"></asp:label></td>
					<td>
						<asp:checkbox id="chkDirectContact" Runat="server" AutoPostBack="True"></asp:checkbox></td>
				</tr>
				<tr>
					<td noWrap width="1%"><asp:label id="lblContactInstr" Runat="server" text="Contact Instructions:">Contact Instructions:</asp:label></td>
					<td>
						<asp:textbox id="txtContactInstr" Runat="server" MaxLength="60" Width="100%"></asp:textbox></td>
				</tr>
				<tr>
					<td noWrap width="1%">
						<asp:label id="lblDept" Runat="server" text="Department:"></asp:label></td>
					<td>
						<asp:dropdownlist id="ddlDept" Runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap width="1%">
						<asp:label id="lblC1" Runat="server" EnableViewState="False" text="Contact 1:">Contact 1:</asp:label></td>
					<td>
						<asp:dropdownlist id="ddlC1" runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap width="1%">
						<asp:label id="lblC2" Runat="server" EnableViewState="False" text="Contact 2:">Contact 2:</asp:label></td>
					<td>
						<asp:dropdownlist id="ddlC2" runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap width="1%">
						<asp:label id="lblC3" Runat="server" EnableViewState="False" text="Contact 3:">Contact 3:</asp:label></td>
					<td>
						<asp:dropdownlist id="ddlC3" runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap width="1%">
						<asp:label id="lblC4" Runat="server" EnableViewState="False" text="Contact 4:">Contact 4:</asp:label></td>
					<td>
						<asp:dropdownlist id="ddlC4" runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap width="1%">
						<asp:label id="lblC5" Runat="server" EnableViewState="False" text="Contact 5:">Contact 5:</asp:label></td>
					<td><asp:dropdownlist id="ddlC5" runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap width="1%">
						<asp:label id="lblC6" Runat="server" EnableViewState="False" text="Contact 6:">Contact 6:</asp:label></td>
					<td><asp:dropdownlist id="ddlC6" runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap width="1%">
						<asp:label id="lblC7" Runat="server" EnableViewState="False" text="Contact 7:">Contact 7:</asp:label></td>
					<td><asp:dropdownlist id="ddlC7" runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap width="1%">
						<asp:label id="lblC8" Runat="server" EnableViewState="False" text="Contact 8:">Contact 8:</asp:label></td>
					<td><asp:dropdownlist id="ddlC8" runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap width="1%">
						<asp:label id="lblC9" Runat="server" EnableViewState="False" text="Contact 9:">Contact 9:</asp:label></td>
					<td><asp:dropdownlist id="ddlC9" runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap width="1%">
						<asp:label id="lblC10" Runat="server" EnableViewState="False" text="Contact 10:">Contact 10:</asp:label></td>
					<td><asp:dropdownlist id="ddlC10" runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap>
						<asp:label id="lblC11" Runat="server" EnableViewState="False" text="Contact 11:">Contact 11:</asp:label></td>
					<td>
						<asp:dropdownlist id="ddlC11" runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td>
						<asp:HyperLink id="hlSearch" Runat="server" NavigateUrl="Search.aspx?RET=Subj">Go To Search Page</asp:HyperLink></td>
					<TD noWrap width="1%">
						<asp:label id="lblC12" Runat="server" EnableViewState="False" text="Contact 12:">Contact 12:</asp:label></TD>
					<td>
						<asp:dropdownlist id="ddlC12" runat="server" Width="20em"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap colSpan="3" align="right">
						<asp:button id="btnAdd" CssClass="b1" Text="Add New Subject" Runat="server"></asp:button>&nbsp;&nbsp;
						<asp:button id="btnUpdate" CssClass="b1" Text="Update Subject" Runat="server"></asp:button>&nbsp;&nbsp;
						<asp:button id="btnDelete" CssClass="b1" Text="Delete Subject" Runat="server"></asp:button>&nbsp;&nbsp;
						<asp:button id="btnHome" CssClass="b1" Text="Home" Runat="server"></asp:button>&nbsp;&nbsp;
						<asp:button id="btnClear" CssClass="b1" Text="Clear" Runat="server"></asp:button>
						<asp:button id="btnInsert" CssClass="b1" Text="Insert Subject" Visible="false" Runat="server"></asp:button>&nbsp;&nbsp;
						<asp:button id="btnCancel" CssClass="b1" Text="Cancel" Visible="false" Runat="server"></asp:button></td>
				</tr>
				<tr>
					<td colspan="3" nowrap align="middle">
						<asp:Label ID="lblError" Runat="server" CssClass="Header_Title" Visible="false"></asp:Label></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
