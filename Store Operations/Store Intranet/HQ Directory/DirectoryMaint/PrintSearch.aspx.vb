Partial Class PrintSearch
    Inherits System.Web.UI.Page
    Private myDatareader As System.Data.SqlClient.SqlDataReader
    Private RT As String 'Report Type
    Private RET As String 'return string
    Public strPerson As String  'Person searching for.

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim TDate As Date
        TDate = Now().ToShortDateString
        lblDate.Text = TDate
        RT = Trim(Request.QueryString("RT"))
        RET = Trim(Request.QueryString("RET"))
        If RT = "People" Then
            'Subjects a specific person is associated with...
            lblReport.Text = "Subjects with this person."
            strPerson = Trim(Request.QueryString("P"))
            dgSubject.DataSource = ""
            dgSubject.DataBind()

            dgCI.DataSource = ""
            dgCI.DataBind()
            dgCI.Visible = False

            Dim StrSql As String
            StrSql = "Select SubjectDesc FROM Subject "
            StrSql = StrSql & "WHERE (Contactid1='" + strPerson + "' or Contactid2='" + strPerson + "' or Contactid3='" + strPerson + "' or Contactid4='" + strPerson + "' or Contactid5='" + strPerson + "' "
            StrSql = StrSql & "or Contactid6='" + strPerson + "' OR Contactid7='" + strPerson + "' or Contactid8='" + strPerson + "' or Contactid9='" + strPerson + "' or "
            StrSql = StrSql & "Contactid10='" + strPerson + "' or Contactid11='" + strPerson + "' or Contactid12='" + strPerson + "')"

            Dim conFillSubject As System.Data.SqlClient.SqlConnection
            conFillSubject = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
            Dim cmdFillSubject As System.Data.SqlClient.SqlCommand
            cmdFillSubject = New System.Data.SqlClient.SqlCommand(StrSql, conFillSubject)
            conFillSubject.Open()
            myDatareader = cmdFillSubject.ExecuteReader
            dgSubject.DataSource = myDatareader
            dgSubject.DataBind()
            dgSubject.Visible = True
            myDatareader.Close()
            conFillSubject.Close()
        ElseIf RT = "CI" Then
            'People with no contact info....
            lblReport.Text = "People with no contact information."
            dgSubject.DataSource = ""
            dgSubject.DataBind()
            dgSubject.Visible = False

            Dim strStoredProcedure As String
            Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
            Dim objDataReader As SqlClient.SqlDataReader
            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            objConnection.Open()
            strStoredProcedure = "spNoContact "

            objCommand.CommandText = strStoredProcedure
            objDataReader = objCommand.ExecuteReader()

            dgCI.DataSource = objDataReader
            dgCI.DataBind()
            dgCI.Visible = True
            objCommand.Dispose()
            objDataReader.Close()
        ElseIf RT = "SI" Then
            'Subjects with no contact info...
            lblReport.Text = "Subjects with no contact information."
            dgCI.DataSource = ""
            dgCI.DataBind()
            dgCI.Visible = False

            Dim strStoredProcedure As String
            Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
            Dim objDataReader As SqlClient.SqlDataReader
            Dim objCommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)

            objConnection.Open()
            strStoredProcedure = "spEmptySubject "

            objCommand.CommandText = strStoredProcedure
            objDataReader = objCommand.ExecuteReader()

            dgSubject.DataSource = objDataReader
            dgSubject.DataBind()
            dgSubject.Visible = True
            objCommand.Dispose()
            objDataReader.Close()
        End If
    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Response.Redirect("Search.aspx?RET=" & RET)
    End Sub
End Class
