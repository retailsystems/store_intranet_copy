<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ContactMaint.aspx.vb" Inherits="ReferenceLists.ContactMaint"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Contact Maintenance</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body id="pageBody" bottomMargin="0" vLink="#ffffff" aLink="#ffffff" link="#ffffff" bgColor="#000000" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td colSpan="2" height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr width="100%">
					<td><asp:label id="lblEmp" Text="Employees:" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" Visible="True" ForeColor="#ffffff" EnableViewState="False">Employees:</asp:label>
					<td></td>
				</tr>
				<tr width="100%">
					<td><asp:listbox id="lbEmployees" Runat="server" SelectionMode="Single" Height="400px" AutoPostBack="True"></asp:listbox></td>
					<td vAlign="top">
						<table>
							<tr vAlign="top">
								<td><asp:label id="lblExt" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" text="Extension:" EnableViewState="False">Extension:</asp:label></td>
								<td><asp:textbox id="txtExt" Runat="server" MaxLength="5"></asp:textbox></td>
								<td><asp:label id="lblEmail" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" text="Email:" EnableViewState="False">Email:</asp:label></td>
								<td><asp:textbox id="txtEmail" Runat="server" Width="190px" MaxLength="35"></asp:textbox></td>
							</tr>
							<tr>
								<td><asp:label id="lblOffLoc" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" text="Office Location:" EnableViewState="False">Office Location:</asp:label></td>
								<td><asp:textbox id="txtOffLoc" Runat="server" MaxLength="15"></asp:textbox></td>
								<td><asp:label id="lblOffNum" Text="Office Number:" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False">Office Number:</asp:label></td>
								<td><asp:textbox id="txtOffNum" runat="server" MaxLength="15"></asp:textbox></td>
							</tr>
							<tr>
								<td><asp:label id="lblFax" Text="Fax:" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False">Fax:</asp:label></td>
								<td><asp:textbox id="txtFax" Runat="server" MaxLength="15"></asp:textbox></td>
								<td><asp:label id="lblBlackberry" Text="Blackberry:" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False">Blackberry:</asp:label></td>
								<td><asp:textbox id="txtBlackberry" Runat="server" MaxLength="10"></asp:textbox></td>
							</tr>
							<tr>
								<td><asp:label id="lblJob" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" text="Position:" EnableViewState="False">Position:</asp:label></td>
								<td><asp:textbox id="txtJob" Runat="server" Width="215px" MaxLength="50"></asp:textbox></td>
							</tr>
							<tr>
								<td><asp:label id="lblrecruiter" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" Visible="false" ForeColor="#ffffff" text="Recruiter Area:"></asp:label></td>
								<td><asp:textbox id="txtRecruiter" Runat="server" Visible="false" MaxLength="10"></asp:textbox></td>
							</tr>
							<tr>
								<td colSpan="3"><asp:button id="btnUpdate" Text="Update Information" Runat="server" Font-Bold="True" ForeColor="White" BorderColor="#990000" BackColor="#440000" BorderStyle="Solid"></asp:button><asp:button id="Delete" Text="Delete Information" Runat="server" Font-Bold="True" ForeColor="White" BorderColor="#990000" BackColor="#440000" BorderStyle="Solid"></asp:button></td>
							</tr>
							<tr>
								<td colspan="4"><asp:label ID="lblError" Visible="False" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="RED"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
