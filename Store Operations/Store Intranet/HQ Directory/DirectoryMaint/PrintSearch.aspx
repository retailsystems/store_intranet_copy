<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PrintSearch.aspx.vb" Inherits="DirectoryMaint.PrintSearch"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Print Search Results</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--
		<script language="JavaScript" src="Javascript/TrapKeyPress.js"></script>
		<script language="JavaScript" src="Javascript/AllowOneSubmit.js"></script>
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		-->
	</HEAD>
	<body scroll="yes">
		<form id="frmPrintReport" method="post" runat="server"><!-- onsubmit="return checkSubmit();" -->
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="left"><asp:Label Runat="server" ID="lblReport" Font-Size="X-Small" Font-Names="arial" Font-Bold="true" ForeColor="Black" BackColor="White"></asp:Label></td>
					<td align="right"><asp:Label Runat="server" ID="lblDate" Font-Size="X-Small" Font-Names="arial" Font-Bold="true" ForeColor="Black" BackColor="White"></asp:Label></td>
				</tr>
				<tr>
					<td nowrap colspan='2'>
						<asp:DataGrid id="dgCI" Runat="server" Width="100%" AutoGenerateColumns="False">
							<ItemStyle Font-Size="XX-Small"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Font-Names="arial" ForeColor="Black" BackColor="White"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="NamePreferred" HeaderText="First Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="NameLast" HeaderText="Last Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="JobCode" HeaderText="Job Code"></asp:BoundColumn>
								<asp:BoundColumn DataField="Dept_Desc" HeaderText="Department"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
				<tr>
					<td nowrap colspan='2'>
						<asp:DataGrid ID="dgSubject" Runat="server" Width="100%" AutoGenerateColumns="False">
							<ItemStyle Font-Size="XX-Small"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Font-Names="arial" ForeColor="Black" BackColor="White"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="SubjectDesc" HeaderText="Subject"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
				<tr>
					<td align="right" nowrap colspan='2'><INPUT type="button" value="Print" onclick="window.print()">&nbsp;<asp:button ID="btnReturn" Runat="server" text="Return"></asp:button></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
