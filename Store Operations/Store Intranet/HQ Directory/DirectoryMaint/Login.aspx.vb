Partial Class Login
    Inherits System.Web.UI.Page
    Protected WithEvents lblEmpid As System.Web.UI.WebControls.Label
    'Protected WithEvents ucHeader As Header
    Public EmpId As String
    Public strPage As String 'maint page to direct to
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pageBody.Attributes.Add("onload", "")
        ' ucHeader.lblTitle = "Login"
        txtEmpId.Attributes.Add("onkeyup", "if ( event.keyCode == 13 ) {document.all." & btnSubmit.ClientID & ".click();}")  '"trapKey(event.keyCode);")

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=Login")

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        strPage = Request.QueryString("Maint")
        'strPage = "Contact"
        EmpId = Trim(txtEmpid.Text)
        EmpId = Replace(EmpId, "'", "''")
        If EmpId = Nothing Or EmpId = "" Then
            'Tell user to input an employeeid
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter in an employee id.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
        Else
            EmpId = txtEmpid.Text.PadLeft(5, "0"c)  'Insert zero's if user didn't enter in 5 digits
            EmpId = Replace(EmpId, "'", "''")
            'check employee id to allow access to Directory Maint pages.
            Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConn"))
            Dim objDataReader As SqlClient.SqlDataReader
            objConnection.Open()
            Dim objCommand As New SqlClient.SqlCommand("Select Employeeid FROM ShipperNumberElig WHERE employeeid= '" + EmpId + "' and StoreMaint=2 ", objConnection)
            objDataReader = objCommand.ExecuteReader
            If objDataReader.Read() Then
                'Then result returned, valid employee
                If strPage = "Contact" Then
                    'Forward User to Contact Maint
                    Response.Redirect("ContactMaint.aspx")
                ElseIf strPage = "Subject" Then
                    'Forward User to Subject Maint
                    Response.Redirect("SubjectMaint.aspx")
                End If
            Else
                'not valid employee
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Invalid Employee ID.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            End If
            objConnection.Close()
            objDataReader.Close()
            objCommand.Dispose()
        End If
    End Sub
    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        Response.Redirect("../home.asp")
    End Sub

End Class
