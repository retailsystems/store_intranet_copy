<%@ Page Language="vb" AutoEventWireup="false" Codebehind="hthqextlist_html.aspx.vb" Inherits="DirectoryMaint.hthqextlist_html"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Hot Topic HQ Extension List</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="inc/font.css" type="text/css" rel="STYLESHEET">
		<LINK href="inc/table.css" type="text/css" rel="STYLESHEET">
		<LINK href="inc/body.css" type="text/css" rel="STYLESHEET">
		<style type="text/css">P.breakhere {PAGE-BREAK-BEFORE: always }
		</style>
	</HEAD>
	<body class="bgWhite">
		<form id="Form1" method="post" runat="server">
			<% dim intTotalRows as Integer = dt.Rows.count 
			   dim intRemainingRows As Integer = intTotalRows 
			   dim intAccumRows As Integer = 0 %>
			<!--x-->
			<% Dim intRowCounter As Integer = 0 
			   Dim intColCounter as Integer = 0%>
			<table align="center">
				<tr>
					<td><!--ABOVE IS ABSOLUTE OUTER-->
						<!--BELOW IS LOOP DECIDING WHETHER A NEW PAGE IS IN ORDER (ANY REMANING ROWS)-->
						<% Do While intRemainingRows > 0 %>
						<!--ABOVE IS LOOP DECIDING WHETHER A NEW PAGE IS IN ORDER; BELOW BEGINS "ONE PAGE" OF CODE-->
						<table class="pageOuterFrame" align="center">
							<tr>
								<td>
									<!--below starts the entire first "data" table of grid info-->
									<!--Start to determine whether rows exist, then build accordingly-->
									<%
						        Select intRemainingRows   ' Evaluate intRemainingRows
					         	   Case 1 To EntireTableRows.oneBased   ' only enough rows to fill a partial table (side), or one exactly %>
									<!--Below starts header row code -->
									<table cellpadding="1" cellspacing="0" border="1" bordercolor="black">
										<tr bgcolor="black" bordercolor="black">
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">First</font>
											</td>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">Last</font>
											</td>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">Ext.</font>
											</td>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">Position</font>
											</td>
										</tr>
										<!--Above is last line of header row code-->
										<%'do while rows exists
						    		 Do While intRemainingRows > 0 %>
										<tr class="std">
											<% For intColCounter = 0 to dt.Columns.Count - 1 %>
											<td class="std" NOWRAP><%= dt.Rows(intRowCounter+intAccumRows).Item(intColCounter)%></td>
											<% Next %>
										</tr>
										<%intRemainingRows = intRemainingRows - 1
											  intAccumRows = intAccumRows + 1 
											  Loop 
											  intRemainingRows = 0 'prob don't even need this 
											  intRowCounter = 0 'reset 
								              intColCounter = 0 'reset %>
									</table>
									<%Case Is > EntireTableRows.oneBased   ' more than enough rows to fill an entire table (side)%>
									<!--Below starts header row code -->
									<table cellpadding="1" cellspacing="0" border="1" bordercolor="black">
										<tr>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">First</font>
											</td>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">Last</font>
											</td>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">Ext.</font>
											</td>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">Position</font>
											</td>
										</tr>
										<!--Above is last line of header row code-->
										<% 'for loop, exactly for known rowcount of table this page 
							                    For intRowCounter=0 To EntireTableRows.zeroBased %>
										<tr class="std">
											<% For intColCounter = 0 to dt.Columns.Count - 1 %>
											<td class="std" NOWRAP><%= dt.Rows(intRowCounter+intAccumRows).Item(intColCounter)%></td>
											<% Next %>
										</tr>
										<% Next 
							'deduct known row amount for maxxing this table from intRemainingRows 
							intRemainingRows = intRemainingRows - EntireTableRows.oneBased
							intAccumRows = intAccumRows + EntireTableRows.oneBased
							intRowCounter = 0 'reset
							intColCounter = 0 'reset							
						   Case Else '
                        End Select %>
									</table>
									<!--Above is last line of 1st "Data" table-grid--></td>
								<td><!--CENTER CELL CONTENTS HERE-->
								</td>
								<td>
									<!--below starts the entire second "data" table of grid info-->
									<!--Start to determine whether rows exist, then build accordingly-->
									<%Select intRemainingRows   ' Evaluate intRemainingRows
						                  Case 1 To EntireTableRows.oneBased   ' only enough rows to fill a partial table (side), or one exactly%>
									<table cellpadding="1" cellspacing="0" border="1" bordercolor="black">
										<!--Below starts header row code -->
										<tr>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">First</font>
											</td>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">Last</font>
											</td>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">Ext.</font>
											</td>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">Position</font>
											</td>
										</tr>
										<!--Above is last line of header row code-->
										<% 'do while rows exist; still only enough to do one side							 
							             	 Do While intRemainingRows <> 0 %>
										<tr class="std">
											<% For intColCounter = 0 to dt.Columns.Count - 1 %>
											<td class="std" NOWRAP><%= dt.Rows(intRowCounter+intAccumRows).Item(intColCounter)%></td>
											<% Next %>
										</tr>
										<%intRemainingRows = intRemainingRows - 1
											  intAccumRows = intAccumRows + 1
											  Loop 
											  intRemainingRows = 0 'prob dont even need this. %>
									</table>
									<%Case Is > EntireTableRows.oneBased   ' more than enough rows to fill an entire table (side)%>
									<!--Below starts header row code -->
									<table cellpadding="1" cellspacing="0" border="1" bordercolor="black">
										<tr>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">First</font>
											</td>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">Last</font>
											</td>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">Ext.</font>
											</td>
											<td class="hdrCell" align="middle" bgcolor="black"><font class="white8">Position</font>
											</td>
										</tr>
										<!--Above is last line of header row code-->
										<%'for loop, exactly for known rowcount of table this page 							     
							                 For intRowCounter=0 To EntireTableRows.zeroBased %>
										<tr class="std">
											<% For intColCounter = 0 to dt.Columns.Count - 1 %>
											<td class="std" NOWRAP><%= dt.Rows(intRowCounter+intAccumRows).Item(intColCounter)%></td>
											<% Next %>
										</tr>
										<% Next 
							                   'deduct known row amount for maxxing this table from intRemainingRows 
							                   intRemainingRows = intRemainingRows - EntireTableRows.oneBased
						                       intAccumRows = intAccumRows + EntireTableRows.oneBased
						                       intRowCounter = 0
						                       intColCounter = 0 %>
									</table>
									<% Case Else 'nada
                                       End Select %>
									<!--above is last line of 2nd "data" table grid--></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<% if intRemainingRows > 0 Then %>
			<p class="breakhere"></p>
			<% End If %>
			</TD></TR></TABLE>
			<% Loop %>
		</form>
		<!--ABSOLUTE OUTER--> </TD></TR></TABLE>
	</body>
</HTML>