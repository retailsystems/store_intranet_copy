
Partial Class generalMerchant
    Inherits System.Web.UI.Page
    Protected WithEvents NameJ As System.Web.UI.WebControls.Label
    Protected WithEvents ext As System.Web.UI.WebControls.Label
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ArrayListMerchant As ArrayList
    Public intCounter As Integer = 0
    Public strStart, strTot As String
    Public dsCorr As New DataSet()
    Public inti As Integer
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim strMode As String
        Dim intMode As Integer
        Dim ds As DataSet
        ' generalQuest.Visible = False

        strMode = ConfigurationSettings.AppSettings("Mode")
        intMode = InStr(strMode, "=", CompareMethod.Text)
        strMode = Mid(strMode, intMode + 1, 5)
        If Trim(UCase(strMode)) = "TORRI" Then
            ArrayListMerchant = clsBuyerDAL.generalMerchantTorrid()
            Cordinate.Visible = False
        ElseIf Trim(UCase(strMode)) = "HOTTO" Then
            ArrayListMerchant = clsBuyerDAL.generalMerchant()

            Cordinate.Visible = True
         
        End If
        Dim dt As New DataTable()
        dt = clsMerchant.GeneralQuestions

        DataGrid1.DataSource = dt
        DataGrid1.DataBind()
        ds = clsBuyerDAL.FitQualtiy
        Datagrid2.DataSource = ds
        ' ds.Tables(0).Rows()
        Datagrid2.DataBind()
        ' MERCHANDISE COORDINATOR
        dsCorr = clsMerchant.Coordinator
        Datagrid3.DataSource = dsCorr
        Datagrid3.DataBind()

        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=Merchandise Clerical Dept.")
    End Sub

End Class
