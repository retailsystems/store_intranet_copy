Partial Class Buyer
    Inherits System.Web.UI.Page
    Protected WithEvents BuyerTitle As System.Web.UI.WebControls.Label

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ArrayListMerchant As ArrayList
    Public intCounter As Integer = 0
    Public strStart, strTot As String
    Public ds As New DataSet()
    Public inti As Integer
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim strMode As String
        Dim intMode As Integer

        '  ArrayListMerchant = New ArrayList()

        strMode = ConfigurationSettings.AppSettings("Mode")
        intMode = InStr(strMode, "=", CompareMethod.Text)
        strMode = Mid(strMode, intMode + 1, 5)
        If Trim(UCase(strMode)) = "TORRI" Then
            DataGrid1.Columns(10).Visible = False
            DataGrid1.Columns(11).Visible = False
            generalQuest.Visible = False
            FitQuest.Visible = False
            ArrayListMerchant = clsBuyerDAL.generalMerchantTorrid()
        ElseIf Trim(UCase(strMode)) = "HOTTO" Then
            ArrayListMerchant = clsBuyerDAL.generalMerchant()
            generalQuest.Visible = False
            FitQuest.Visible = False

        End If

        Dim dt As New DataTable()
        dt = clsBuyerDAL.getBuyerDataTable
        'dt.Rows(0)("AsstBuyerEmail")
        DataGrid1.DataSource = dt
        DataGrid1.DataBind()
        ds = clsBuyerDAL.FitQualtiy
        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=Buyers")
    End Sub

End Class
