<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SubjectMaint.aspx.vb" Inherits="ReferenceLists.SubjectMaint"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Subject Maintenance</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body id="pageBody" bottomMargin="0" vLink="#ffffff" aLink="#ffffff" link="#ffffff" bgColor="#000000" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td colSpan="2" height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr width="100%">
					<td><asp:label id="lblSubj" Text="Subjects:" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" Visible="True" ForeColor="#ffffff" EnableViewState="False">Subjects:</asp:label>
					<td></td>
				</tr>
				<tr width="100%">
					<td vAlign="top"><asp:listbox id="lbSubjects" Runat="server" SelectionMode="Single" Height="450px" AutoPostBack="True"></asp:listbox></td>
					<td vAlign="top">
						<table width="100%">
							<tr vAlign="top">
								<td noWrap width="1%"><asp:label id="lblSubjDesc" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" text="Subject Description:">Subject Description:</asp:label></td>
								<td><asp:textbox id="txtSubjDesc" Runat="server" MaxLength="40" Width="95%" ForeColor="Black" Font-Size="X-Small" Font-Names="Arial"></asp:textbox></td>
							</tr>
							<tr vAlign="top">
								<td noWrap width="1%"><asp:label id="lblSearch" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" text="Search Phrases:">Search Phrases:</asp:label></td>
								<td><asp:textbox id="txtSearch" Runat="server" MaxLength="180" Width="95%" TextMode="MultiLine" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:textbox></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblchkDirectContact" Text="No Direct Contact:" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff"></asp:label></td>
								<td><asp:checkbox id="chkDirectContact" Runat="server" AutoPostBack="True"></asp:checkbox></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblContactInstr" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" text="Contact Instructions:">Contact Instructions:</asp:label></td>
								<td><asp:textbox id="txtContactInstr" Runat="server" MaxLength="60" Width="95%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:textbox></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblDept" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" text="Department:"></asp:label></td>
								<td><asp:dropdownlist id="ddlDept" Runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblC1" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False" text="Contact 1:">Contact 1:</asp:label></td>
								<td><asp:dropdownlist id="ddlC1" runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblC2" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False" text="Contact 2:">Contact 2:</asp:label></td>
								<td><asp:dropdownlist id="ddlC2" runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblC3" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False" text="Contact 3:">Contact 3:</asp:label></td>
								<td><asp:dropdownlist id="ddlC3" runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblC4" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False" text="Contact 4:">Contact 4:</asp:label></td>
								<td><asp:dropdownlist id="ddlC4" runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblC5" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False" text="Contact 5:">Contact 5:</asp:label></td>
								<td><asp:dropdownlist id="ddlC5" runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblC6" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False" text="Contact 6:">Contact 6:</asp:label></td>
								<td><asp:dropdownlist id="ddlC6" runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblC7" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False" text="Contact 7:">Contact 7:</asp:label></td>
								<td><asp:dropdownlist id="ddlC7" runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblC8" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False" text="Contact 8:">Contact 8:</asp:label></td>
								<td><asp:dropdownlist id="ddlC8" runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblC9" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False" text="Contact 9:">Contact 9:</asp:label></td>
								<td><asp:dropdownlist id="ddlC9" runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblC10" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False" text="Contact 10:">Contact 10:</asp:label></td>
								<td><asp:dropdownlist id="ddlC10" runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap><asp:label id="lblC11" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False" text="Contact 11:">Contact 11:</asp:label></td>
								<td><asp:dropdownlist id="ddlC11" runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap width="1%"><asp:label id="lblC12" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="#ffffff" EnableViewState="False" text="Contact 12:">Contact 12:</asp:label></td>
								<td><asp:dropdownlist id="ddlC12" runat="server" Width="50%" ForeColor="Black" Font-Size="Smaller" Font-Names="Arial"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap colSpan="2"><asp:button id="btnAdd" Text="Add New Subject" Runat="server" Font-Bold="True" ForeColor="White" BorderColor="#990000" BackColor="#440000" BorderStyle="Solid"></asp:button>
									&nbsp;&nbsp;<asp:button id="btnUpdate" Text="Update Subject" Runat="server" Font-Bold="True" ForeColor="White" BorderColor="#990000" BackColor="#440000" BorderStyle="Solid"></asp:button>
									&nbsp;&nbsp;<asp:button id="btnDelete" Text="Delete Subject" Runat="server" Font-Bold="True" ForeColor="White" BorderColor="#990000" BackColor="#440000" BorderStyle="Solid"></asp:button></td>
							</tr>
							<tr>
								<td colspan="2" nowrap><asp:Label ID="lblError" Runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" Font-Name="Arial" ForeColor="RED" Visible="false"></asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
