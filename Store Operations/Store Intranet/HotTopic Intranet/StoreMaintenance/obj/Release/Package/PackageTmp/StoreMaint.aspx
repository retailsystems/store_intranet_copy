<%@ Page Language="vb"  AutoEventWireup="false" Codebehind="StoreMaint.aspx.vb" Inherits="StoreMaint.WebForm1" enableViewState="True"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Store Lookup</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
		<script language="JavaScript">
			<!--
			javascript:window.history.forward(1);
			//-->
		</script>
	</HEAD>
	<body id="pageBody" bgProperties="fixed" runat="server">
		<form id="StoreMaint" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server" />
			<div class="container home" role="main">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" align="left">
				<TBODY>
					<TR>
						<TD style="WIDTH: 196px" align="right"><asp:label id="lblCoName2" runat="server">Company Name :</asp:label></TD>
						<TD><asp:dropdownlist id="cboCoName" tabIndex="20" runat="server" AutoPostBack="True">
								<asp:ListItem Value="Hot Topic">Hot Topic</asp:ListItem>
								<asp:ListItem Value="Torrid">Torrid</asp:ListItem>
                                <asp:ListItem Value="Blackheart">BoxLunch</asp:ListItem>
								<asp:ListItem Value="LoveSick">LoveSick</asp:ListItem>
							</asp:dropdownlist>&nbsp;
							<asp:label id="lblStaging" runat="server">Staging :</asp:label><asp:checkbox id="cbStaging" tabIndex="1" runat="server"></asp:checkbox></TD>
						<TD align="right"><asp:label id="lblSrchOpenDate" runat="server">Open Date</asp:label><asp:label id="lblP1Format" runat="server">&nbsp;(Format 01/23/75) :</asp:label></TD>
						<TD><asp:textbox id="txtFDate" tabIndex="9" runat="server" Width="60px" ToolTip="Beginning Date."
								MaxLength="10"></asp:textbox>&nbsp;
							<asp:label id="lblTo2" runat="server">To  </asp:label>&nbsp;
							<asp:textbox id="txtTDate" tabIndex="10" runat="server" Width="60px" ToolTip="Ending Date." MaxLength="10"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 196px" align="right"><asp:label id="lblStoNum" runat="server">Store Number :</asp:label></TD>
						<TD><asp:textbox id="txtSrchStoNum" tabIndex="5" runat="server" Width="60px" MaxLength="8"></asp:textbox></TD>
						<TD align="right"><asp:label id="lblSrchRegion" runat="server">Region :</asp:label></TD>
						<TD><asp:dropdownlist id="cboSrchRegion" tabIndex="11" runat="server" AutoPostBack="True" Width="60px"></asp:dropdownlist></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 196px" align="right"><asp:label id="lblStoName" runat="server">Store Name :</asp:label></TD>
						<TD><asp:textbox id="txtSrchStoName" tabIndex="6" runat="server" Width="150px" MaxLength="50"></asp:textbox></TD>
						<TD align="right"><asp:label id="lblSrchDist" runat="server">District :</asp:label></TD>
						<TD><asp:dropdownlist id="cboSrchDist" tabIndex="12" runat="server" AutoPostBack="True" Width="60px"></asp:dropdownlist></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 196px" align="right"><asp:label id="lbSrchState" runat="server">State  :</asp:label></TD>
						<TD><asp:dropdownlist id="cboSrchState" tabIndex="7" runat="server" AutoPostBack="True" Width="60px"></asp:dropdownlist></TD>
						<TD align="right"><asp:label id="lblMgr" runat="server">Manager :</asp:label></TD>
						<TD><asp:dropdownlist id="cboMgr" tabIndex="13" runat="server" AutoPostBack="True" Width="150px"></asp:dropdownlist></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 196px" align="right"><asp:label id="lblCity1" runat="server">City  :</asp:label></TD>
						<TD><asp:dropdownlist id="cboCity" tabIndex="8" runat="server" AutoPostBack="True" Width="150px"></asp:dropdownlist></TD>
						<TD align="right"><asp:label id="Label2" runat="server">Sq. Feet :</asp:label></TD>
						<TD><asp:textbox id="txtSize" tabIndex="14" runat="server" Width="46px" MaxLength="6"></asp:textbox>&nbsp;&nbsp;&nbsp;
							<asp:label id="lblTo" runat="server">To  </asp:label>&nbsp;&nbsp;&nbsp;
							<asp:textbox id="txtSize2" tabIndex="15" runat="server" Width="46px" MaxLength="6"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 196px" align="right">&nbsp;</TD>
						<TD>&nbsp;&nbsp;</TD>
						<TD></TD>
						<TD></TD>
						<TD></TD>
					</TR>
					<TR>
						<td style="WIDTH: 196px; HEIGHT: 48px" align="right"><SPAN tabIndex="0">&nbsp;<asp:label id="lblReports" runat="server" Font-Italic="True" EnableViewState="False"> Current Store List :</asp:label></SPAN></td>
						<TD align="left" style="HEIGHT: 48px">&nbsp;&nbsp;
							<asp:hyperlink id="btnExportExcelHT" runat="server" EnableViewState="False" ForeColor="Red"
								BorderWidth="2px" Height="21px" Font-Bold="True" BorderStyle="None" navigateurl="Export_HT_StoreList.aspx"
								target="_blank">Hot Topic List</asp:hyperlink></TD>
						<TD tabIndex="0" style="HEIGHT: 48px">&nbsp;&nbsp;</TD>
						<TD tabIndex="0" align="center" colSpan="2" style="HEIGHT: 48px"><asp:button id="btnSearch" tabIndex="1" runat="server" ToolTip="Click to retrieve data" CssClass="btn btn-danger"
								Text="Search"></asp:button>&nbsp;&nbsp;&nbsp;
							<asp:button id="btnAddStore" tabIndex="2" runat="server" ToolTip="Click to add a new store"
								EnableViewState="False" CssClass="btn btn-danger" Text="Add Store"></asp:button>&nbsp;&nbsp;&nbsp;
							<asp:button id="btnClear" tabIndex="3" runat="server" CssClass="btn btn-danger" Text="Clear"></asp:button>&nbsp;&nbsp;&nbsp;
							<asp:button id="btnReturn" tabIndex="4" runat="server" CssClass="btn btn-danger" Text="Home"></asp:button></TD>
					</TR>
					<tr>
						<td style="WIDTH: 196px">&nbsp;</td>
						<TD align="left">&nbsp;&nbsp;
							<asp:hyperlink id="btnExportExcelTD" tabIndex="3" runat="server" EnableViewState="False" ForeColor="Red"
								Height="21px" Font-Bold="True" BorderStyle="None" navigateurl="Export_TD_StoreList.aspx" target="_blank">Torrid 
							List</asp:hyperlink>
							<br />
							<br />
							</TD>
						<TD></TD>
						<td colSpan="2">&nbsp;</td>
					</tr>
					<tr>
						<td style="WIDTH: 196px">&nbsp;</td>
						<TD align="left">&nbsp;&nbsp;
							<asp:hyperlink id="btnExportExcelBL" tabIndex="4" runat="server" 
                                EnableViewState="False" ForeColor="Red"
								Height="21px" Font-Bold="True" BorderStyle="None" navigateurl="Export_BL_StoreList.aspx" target="_blank">BoxLunch 
							List</asp:hyperlink>
                            <br />
							<br />
                        </TD>
						<TD>&nbsp;</TD>
						<td colSpan="2">&nbsp;</td>
					</tr>
					<tr>
						<td style="WIDTH: 196px">&nbsp;</td>
						<TD align="left">&nbsp;&nbsp;&nbsp;<asp:hyperlink id="btnExportExcelLS" 
                                tabIndex="4" runat="server" 
                                EnableViewState="False" ForeColor="Red"
								Height="21px" Font-Bold="True" BorderStyle="None" navigateurl="Export_LS_StoreList.aspx" target="_blank">Lovesick 
							List</asp:hyperlink></TD>
						<TD>&nbsp;</TD>
						<td colSpan="2">&nbsp;</td>
					</tr>
					<tr>
						<td style="WIDTH: 196px"><asp:label id="lblNoRecords" Runat="server" Visible="False"></asp:label></td>
					</tr>
					<tr>
						<td colSpan="4"><asp:datagrid id="dgSearch" runat="server" Width="100%" CssClass="table archtbl" AllowPaging="True"
								PageSize="5" AutoGenerateColumns="False" GridLines="None">
								<ItemStyle CssClass="archtbltr"></ItemStyle>
								<AlternatingItemStyle CssClass="archtblalttr"></AlternatingItemStyle>
								<Columns>
									<asp:TemplateColumn HeaderText=" ">
										<ItemTemplate>
											<a href='StoreDetail.aspx?pStoreID=<%#Container.DataItem("Store Number")%>&pCoNameChoice=<%# pCoNameChoice%>'>
												<%# container.DataItem("Store Name") %>
											</a>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="Address1" HeaderText="Address"></asp:BoundColumn>
									<asp:BoundColumn DataField="Address2"></asp:BoundColumn>
									<asp:BoundColumn DataField="Address3"></asp:BoundColumn>
									<asp:BoundColumn DataField="City" HeaderText="City"></asp:BoundColumn>
									<asp:BoundColumn DataField="State" HeaderText="State"></asp:BoundColumn>
									<asp:BoundColumn DataField="Phone1" HeaderText="Phone&nbsp;Number"></asp:BoundColumn>
									<asp:BoundColumn DataField="Manager" HeaderText="Manager"></asp:BoundColumn>
								</Columns>
								<PagerStyle NextPageText="" PrevPageText="" PageButtonCount="5" Mode="NumericPages"></PagerStyle>
							</asp:datagrid></td>
					</tr>
				</TBODY>
			</TABLE>
			</div>
			<script language="javascript">
				document.StoreMaint.txtSrchStoNum.focus();
			</script>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
