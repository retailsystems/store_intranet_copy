Public MustInherit Class Header

    Inherits System.Web.UI.UserControl

    Protected WithEvents lblEmpName As System.Web.UI.WebControls.Label
    Protected WithEvents lbLogoff As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbHome As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbNewTrans As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbCheckIn As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdHome As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdNewTrans As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdCheckIn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbExit As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdSearch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbSearch As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbMaint As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdMaint As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbProfileList As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdProfileList As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbICSearch As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdICSearch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblStore As System.Web.UI.WebControls.Label
    Protected WithEvents tdExit As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblHeaderTitle As System.Web.UI.WebControls.Label
    Protected WithEvents frSessionAlive As System.Web.UI.HtmlControls.HtmlGenericControl
    'Protected WithEvents lbDCMaint As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents tdDCMaint As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbProfileAdmin As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdProfileAdmin As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbAdvSearch As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdAdvSearch As System.Web.UI.HtmlControls.HtmlTableCell

    Private strCurrentPage As PageName
    Private objUserInfo As New UserInfo()  
    Protected WithEvents lbDCMaint As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tdDCMaint As System.Web.UI.HtmlControls.HtmlTableCell
    Private strCurrentMode As HeaderGroup

    Public Enum HeaderGroup
        IC = 1
        Transfer = 2
        Login = 3
    End Enum

    Public Enum PageName
        Home = 1
        NewTransfer = 2
        CheckIn = 3
        Search = 4
        Maint = 5
        IC_Profiles = 6
        IC_Search = 7
        'DCMaint = 8
        ProfileAdmin = 9
    End Enum
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()

        tdMaint.Visible = False

        lblStore.Text = objUserInfo.StoreNumber
        lbHome.Style.Add("text-decoration", "none")
        lbNewTrans.Style.Add("text-decoration", "none")
        lbCheckIn.Style.Add("text-decoration", "none")
        lbExit.Style.Add("text-decoration", "none")
        lbSearch.Style.Add("text-decoration", "none")
        lbMaint.Style.Add("text-decoration", "none")
        lbICSearch.Style.Add("text-decoration", "none")
        lbProfileList.Style.Add("text-decoration", "none")
        'lbDCMaint.Style.Add("text-decoration", "none")
        lbProfileAdmin.Style.Add("text-decoration", "none")

        tdSearch.Visible = False
        tdMaint.Visible = False
        tdICSearch.Visible = False
        tdProfileList.Visible = False
        tdHome.Visible = False
        'tdDCMaint.Visible = False
        tdProfileAdmin.Visible = False

        DisplayItems()

        Select Case strCurrentPage

            Case 1
                tdHome.BgColor = "#990000"
            Case 2
                tdNewTrans.BgColor = "#990000"
            Case 3
                tdCheckIn.BgColor = "#990000"
            Case 4
                tdSearch.BgColor = "#990000"
            Case 5
                tdMaint.BgColor = "#990000"
            Case 6
                tdProfileList.BgColor = "#990000"
            Case 7
                tdICSearch.BgColor = "#990000"
                'Case 8
                'tdDCMaint.BgColor = "#990000"
            Case 8
                tdProfileAdmin.BgColor = "#990000"

        End Select

    End Sub


    Public Sub DisableButtons()

        lbExit.Enabled = False
        lbHome.Enabled = False
        lbNewTrans.Enabled = False
        lbCheckIn.Enabled = False
        lbSearch.Enabled = False
        lbMaint.Enabled = False
        lbICSearch.Enabled = False
        lbProfileList.Enabled = False
        'lbDCMaint.Enabled = False
        lbProfileAdmin.Enabled = False
        lbLogoff.Enabled = False

    End Sub

    Private Sub DisplayItems()

        Select Case strCurrentMode

            Case HeaderGroup.IC

                If objUserInfo.EmpId Is Nothing Then
                    lbLogoff.Text = "login"
                End If

                lblEmpName.Text = objUserInfo.EmpFullName
                tdProfileList.Visible = True
                tdICSearch.Visible = True
                'tdDCMaint.Visible = True
                tdNewTrans.Visible = False
                tdCheckIn.Visible = False
                tdProfileAdmin.Visible = True


            Case HeaderGroup.Transfer

                tdHome.Visible = True
                tdSearch.Visible = True

                If objUserInfo.EmpId Is Nothing Then
                    lbLogoff.Text = "login"
                End If

                lblEmpName.Text = objUserInfo.EmpFullName
                tdNewTrans.Visible = True
                tdCheckIn.Visible = True


            Case Else

                lbLogoff.Text = "login"

        End Select

    End Sub

    Private Sub lbLogoff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbLogoff.Click

        If lbLogoff.Text = "login" Then

            If strCurrentMode = HeaderGroup.Transfer Then
                Response.Redirect("Login.aspx?Mode=1")
            Else
                Response.Redirect("Login.aspx?Mode=0")
            End If

        Else
            Dim objTransferLock As New TransferLock()

            objTransferLock.UnlockTransfer("", "", Session.SessionID)

            objUserInfo.LogOff()

            Session.Abandon()
            objUserInfo.ClearEmployeeSession()
            lbLogoff.Text = "login"

            If strCurrentMode = HeaderGroup.IC Then
                Response.Redirect("IC_ProfileList.aspx")
            Else
                Response.Redirect("default.aspx")
            End If

        End If

    End Sub

    Property lblTitle()
        Get
            Return lblHeaderTitle.Text
        End Get
        Set(ByVal Value)
            lblHeaderTitle.Text = Value
        End Set
    End Property

    Public Sub CurrentPage(ByVal Value As PageName)

        strCurrentPage = Value

    End Sub

    Public Sub CurrentMode(ByVal Value As HeaderGroup)

        strCurrentMode = Value

    End Sub

    Private Sub lbHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbHome.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("default.aspx")

    End Sub

    Private Sub lbNewTrans_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbNewTrans.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("CreateNewTrans.aspx")

    End Sub

    Private Sub lbCheckIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbCheckIn.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("SelectTransfer.aspx")

    End Sub

    Private Sub lbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbSearch.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("TransferSearch.aspx")

    End Sub

    Private Sub lbMaint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbMaint.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("StoreSearch.aspx")

    End Sub

    Private Sub lbExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbExit.Click

        Dim objTransferLock As New TransferLock()

        objUserInfo.GetEmployeeInfo()

        If objUserInfo.EmpOnline Then
            Response.Redirect("EmployeeLogOff.aspx?Mode=" & strCurrentMode)
            'objUserInfo.LogOff()
        Else
            frSessionAlive.Attributes("src") = ""

            objTransferLock.UnlockTransfer("", "", Session.SessionID)
            Session.Abandon()

            Response.Write("<script languague=javascript>window.close()</script>")
        End If


    End Sub

    Private Sub lbProfileList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbProfileList.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("IC_ProfileList.aspx")

    End Sub

    Private Sub lbICSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbICSearch.Click

        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("IC_Search.aspx")

    End Sub
    Private Sub lbProfileAdmin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbProfileAdmin.Click
        Dim objTransferLock As New TransferLock()

        objTransferLock.UnlockTransfer("", "", Session.SessionID)
        Response.Redirect("IC_ProfileAdmin.aspx")

    End Sub

    'Private Sub lbDCMaint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbDCMaint.Click
    '    Dim objTransferLock As New TransferLock()

    '    objTransferLock.UnlockTransfer("", "", Session.SessionID)
    '    Response.Redirect("IC_DCMaint.aspx")
    'End Sub
End Class
