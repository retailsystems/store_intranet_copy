<%@ Control Language="vb" AutoEventWireup="false" Codebehind="Header.ascx.vb" Inherits="SSTransfer.Header" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table cellSpacing="0" cellPadding="4" width="100%" bgColor="#330000" border="0" id="Table1">
	<tr>
		<td width="1%" vAlign="center"><IMG src="images\hdr_main3.gif"></td>
		<td align="right" height="100%">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0" id="Table2">
				<tr>
					<td width="1%" valign="bottom">
						<asp:label id="lblStore" Font-Size="XX-Small" Font-Names="Arial" ForeColor="Red" runat="server"></asp:label>
					</td>
					<td valign="center" align="middle">
						<asp:label id="lblHeaderTitle" runat="server" ForeColor="Red" Font-Names="Arial Black" Font-Size="Large"></asp:label>
						<iframe runat="server" id='frSessionAlive' frameborder="no" scrolling="no" width="1" height="1" src="SessionAlive.aspx">
						</iframe>
					</td>
					<td width="1%" vAlign="top" align="right">
						<table borderColor="#990000" cellSpacing="0" cellPadding="0" width="180" border="1" id="Table3">
							<tr>
								<td nowrap align="middle" bgColor="#440000">&nbsp;<asp:label id="lblEmpName" Font-Bold="True" Font-Size="X-Small" Font-Names="Arial" ForeColor="Red" runat="server"></asp:label>&nbsp;</td>
							</tr>
							<tr>
								<td align="middle" bgColor="#440000"><asp:linkbutton id="lbLogoff" Font-Size="X-Small" Font-Names="Arial" ForeColor="Red" runat="server" BorderStyle="None">log off</asp:linkbutton></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table borderColor="#990000" cellSpacing="0" cellPadding="2" width="100%" border="1" id="Table4">
	<tr>
		<td id="tdHome" noWrap align="middle" width="100" bgColor="#440000" runat="server"><asp:linkbutton id="lbHome" Font-Bold="true" Font-Size="x-small" ForeColor="white" runat="server" Font-Name="arial">Info</asp:linkbutton></td>
		<td id="tdNewTrans" noWrap align="middle" width="100" bgColor="#440000" runat="server"><asp:linkbutton id="lbNewTrans" Font-Bold="true" Font-Size="x-small" ForeColor="white" runat="server" Font-Name="arial">New Transfer</asp:linkbutton></td>
		<td id="tdCheckIn" noWrap align="middle" width="100" bgColor="#440000" runat="server"><asp:linkbutton id="lbCheckIn" Font-Bold="true" Font-Size="x-small" ForeColor="white" runat="server" Font-Name="arial">Check In</asp:linkbutton></td>
		<td id="tdSearch" noWrap align="middle" width="100" bgColor="#440000" runat="server"><asp:linkbutton id="lbSearch" Font-Bold="true" Font-Size="x-small" ForeColor="white" runat="server" Font-Name="arial">Search</asp:linkbutton></td>
		<td id="tdMaint" noWrap align="middle" width="100" bgColor="#440000" runat="server"><asp:linkbutton id="lbMaint" Font-Bold="true" Font-Size="x-small" ForeColor="white" runat="server" Font-Name="arial">Maint</asp:linkbutton></td>
		<td id="tdProfileList" noWrap align="middle" width="100" bgColor="#440000" runat="server"><asp:linkbutton id="lbProfileList" Font-Bold="true" Font-Size="x-small" ForeColor="white" runat="server" Font-Name="arial">Profiles</asp:linkbutton></td>
		<td id="tdProfileAdmin" nowrap align="middle" width="100" bgcolor="#440000" runat="server"><asp:LinkButton ID="lbProfileAdmin" Font-Bold="true" Font-Size="x-small" ForeColor="white" runat="server" Font-Name="arial">Profile Admin</asp:LinkButton>
		</td>
		<td id="tdICSearch" noWrap align="middle" width="100" bgColor="#440000" runat="server"><asp:linkbutton id="lbICSearch" Font-Bold="true" Font-Size="x-small" ForeColor="white" runat="server" Font-Name="arial">Search</asp:linkbutton></td>
		
		<td id="tdExit" noWrap align="middle" width="100" bgColor="#440000" runat="server"><asp:linkbutton id="lbExit" Font-Bold="true" Font-Size="x-small" ForeColor="white" runat="server" Font-Name="arial">Home\Exit</asp:linkbutton></td>
		<td align="right" bgcolor="#440000">&nbsp;</td>
	</tr>
</table>
