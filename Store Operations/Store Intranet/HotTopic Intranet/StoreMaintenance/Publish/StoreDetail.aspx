<%@ Page Language="vb" AutoEventWireup="false" Codebehind="StoreDetail.aspx.vb" Inherits="StoreMaint.StoreDetail" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
		<title>Store Detail</title>
		<meta content="True" name="vs_showGrid">
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="http://172.16.2.13/StyleSheets/HotTopic.css" type="text/css" rel="stylesheet" runat="server">
		test
		</LINK>
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript">
			<!--
			javascript:window.history.forward(1);
			//-->
		</script>
</head>
	<body id="pageBody" scroll="yes" runat="server">
		<form id="StoreDetail" method="post" runat="server">
			<iframe class="Header_Frame" id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0"
				src="http://172.16.1.9/Header/Header.aspx?mode=hottopic" frameBorder="0" width="100%"
				scrolling="no" runat="server"></iframe>
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD>
						<TABLE id="tblDisplay" cellSpacing="0" cellPadding="0" width="100%" align="left">
							<TR>
								<TD align="right"><asp:label id="lblCoName" runat="server" EnableViewState="False">Co. Name :</asp:label></TD>
								<TD>
									<P><asp:dropdownlist id="cboCoName" tabIndex="1" runat="server" Width="170px">
											<ASP:LISTITEM Value="Hot Topic">Hot Topic</ASP:LISTITEM>
											<ASP:LISTITEM Value="Torrid">Torrid</ASP:LISTITEM>
											<ASP:LISTITEM Value="Blackheart">BoxLunch</ASP:LISTITEM>
											<ASP:LISTITEM Value="LoveSick">LoveSick</ASP:LISTITEM>
										</asp:dropdownlist></P>
								</TD>
								<TD align="right"><asp:label id="lbtStoSize" runat="server" EnableViewState="False"> Sq. Feet :</asp:label></TD>
								<TD><asp:textbox id="txtStoDim" tabIndex="14" runat="server" Width="60px" MaxLength="4"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblStoreNum" runat="server" EnableViewState="False">Store Number :</asp:label></TD>
								<TD><asp:textbox id="txtStoNum" tabIndex="1" runat="server" Width="60px" MaxLength="6"></asp:textbox>&nbsp;
									<asp:label id="lblStaging" runat="server">Staging :</asp:label><asp:checkbox id="cbStaging" tabIndex="1" runat="server"></asp:checkbox></TD>
								<TD align="right"><asp:label id="lblRegion" runat="server" EnableViewState="False">Region :</asp:label></TD>
								<TD><asp:dropdownlist id="cboRegion" tabIndex="15" runat="server" Width="60px"></asp:dropdownlist><asp:textbox id="txtRegion" tabIndex="15" runat="server" Width="60px" MaxLength="3"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblStoreName" runat="server" EnableViewState="False">Store Name :</asp:label></TD>
								<TD><asp:textbox id="txtStoName" tabIndex="2" runat="server" Width="170px" MaxLength="50"></asp:textbox></TD>
								<TD align="right"><asp:label id="lblDist" runat="server" EnableViewState="False">District :</asp:label></TD>
								<TD><asp:dropdownlist id="cboDistrict" tabIndex="16" runat="server" Width="60px"></asp:dropdownlist><asp:textbox id="txtDistrict" tabIndex="16" runat="server" Width="60px" MaxLength="4"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblStoAddr" runat="server" EnableViewState="False">Address Line 1 :</asp:label></TD>
								<TD><asp:textbox id="txtAddr" tabIndex="3" runat="server" Width="170px" MaxLength="105"></asp:textbox></TD>
								<TD align="right"><asp:label id="lblWan" runat="server" EnableViewState="False">WAN :</asp:label></TD>
								<TD><asp:checkbox id="cboWan" tabIndex="17" runat="server"></asp:checkbox></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="Label1" runat="server" EnableViewState="False">Address Line 2 :</asp:label></TD>
								<TD><asp:textbox id="txtAddr2" tabIndex="4" runat="server" Width="170px" MaxLength="35"></asp:textbox></TD>
								<TD align="right"><asp:label id="lblSubnet" runat="server">Subnet :</asp:label></TD>
								<TD><asp:textbox id="txtSubnet1" tabIndex="17" runat="server" Width="30px" MaxLength="3"></asp:textbox>.
									<asp:textbox id="txtSubnet2" tabIndex="18" runat="server" Width="30px" MaxLength="3"></asp:textbox>.
									<asp:textbox id="txtSubnet3" tabIndex="19" runat="server" Width="30px" MaxLength="3"></asp:textbox>.
									<asp:textbox id="txtSubnet4" tabIndex="20" runat="server" Width="30px" MaxLength="3"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="Label2" runat="server" EnableViewState="False">Address Line 3 :</asp:label></TD>
								<TD><asp:textbox id="txtAddr3" tabIndex="5" runat="server" Width="170px" MaxLength="35"></asp:textbox></TD>
								<TD align="right"><asp:label id="labManager" runat="server" EnableViewState="False"> Manager :</asp:label></TD>
								<TD><asp:textbox id="txtStoMgr" tabIndex="21" runat="server" Width="170px" MaxLength="30"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 27px" align="right"><asp:label id="lblCity" runat="server" EnableViewState="False">City :</asp:label></TD>
								<TD style="HEIGHT: 27px"><asp:textbox id="txtCity" tabIndex="6" runat="server" Width="170px" MaxLength="35"></asp:textbox></TD>
								<TD style="HEIGHT: 27px" align="right"><asp:label id="lblAsstMgr1" runat="server" EnableViewState="False"> ASM :</asp:label></TD>
								<TD style="HEIGHT: 27px"><asp:textbox id="txtAsstMgr1" tabIndex="22" runat="server" Width="170px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblState" runat="server" EnableViewState="False">State :</asp:label></TD>
								<TD><asp:dropdownlist id="cboState" tabIndex="7" runat="server" Width="60px"></asp:dropdownlist>&nbsp;
									<asp:label id="lblZip" runat="server" EnableViewState="False">Zip :</asp:label>&nbsp;&nbsp;
									<asp:textbox id="txtZip" tabIndex="7" runat="server" Width="60px" MaxLength="7"></asp:textbox></TD>
								<TD align="right"><asp:label id="lblAsstMgr2" runat="server" EnableViewState="False"> ASM :</asp:label></TD>
								<TD><asp:textbox id="txtAsstMgr2" tabIndex="23" runat="server" Width="170px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblCountry" runat="server">Country :</asp:label></TD>
								<TD><asp:textbox id="txtCountry" tabIndex="8" runat="server" Width="60px" MaxLength="2" ToolTip="(US)-United States, (PR)-Puerto Rico"></asp:textbox>&nbsp;
									<asp:label id="lblSimon" runat="server" EnableViewState="False">Simon :</asp:label><asp:textbox id="txtSimon" tabIndex="8" runat="server" Width="26px" MaxLength="1" ToolTip="(S) - Simon Mall, (M) - Macerich/Wescor Mall, (W) - Westfield Mall, (G) - General Growth Property Mall, (C) - CBL Mall, (P) - Premier Mall, etc."></asp:textbox></TD>
								<TD align="right"><asp:label id="lblAsstMgr3" runat="server" EnableViewState="False"> ASM :</asp:label></TD>
								<TD>
									<P><asp:textbox id="txtAsstMgr3" tabIndex="24" runat="server" Width="170px" MaxLength="30"></asp:textbox></P>
								</TD>
							</TR>
							<TR>
								<TD align="right">&nbsp;<asp:label id="lblPhone1" runat="server" EnableViewState="False" Height="10px">Phone 1:</asp:label>
									<asp:label id="lblP1Format" runat="server" EnableViewState="False">(xxx)xxx-xxxx</asp:label>&nbsp;</TD>
								<TD><asp:textbox id="txtPhone1" tabIndex="9" runat="server" Width="170px" MaxLength="13"></asp:textbox></TD>
								<TD align="right"><asp:label id="lblAsstMgr4" runat="server" EnableViewState="False"> ASM :</asp:label></TD>
								<TD>
									<P><asp:textbox id="txtAsstMgr4" tabIndex="25" runat="server" Width="170px" MaxLength="30"></asp:textbox></P>
								</TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblPhone2" runat="server" EnableViewState="False">Phone 2:</asp:label>&nbsp;<asp:label id="lblP2Format" runat="server" EnableViewState="False">(xxx)xxx-xxxx</asp:label>&nbsp;</TD>
								<TD><asp:textbox id="txtPhone2" tabIndex="10" runat="server" Width="170px" MaxLength="13"></asp:textbox></TD>
								<TD align="right"><asp:label id="lblAsstMgr5" runat="server" EnableViewState="False"> ASM :</asp:label></TD>
								<TD>
									<P><asp:textbox id="txtAsstMgr5" tabIndex="26" runat="server" Width="170px" MaxLength="30"></asp:textbox></P>
								</TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblEMail" runat="server" EnableViewState="False">E-Mail :</asp:label></TD>
								<TD><asp:textbox id="txtEmail" tabIndex="11" runat="server" Width="170px" MaxLength="40"></asp:textbox></TD>
								<TD align="right"><asp:label id="lblShipperNumber" runat="server" EnableViewState="False">FedEx/UPS Meter Number :</asp:label></TD>
								<TD><asp:textbox id="txtShipperNumber" tabIndex="28" runat="server" EnableViewState="False" Width="170px"
										MaxLength="7"></asp:textbox></TD>
							</TR>
							<!-- SRI 4/14/2004 Adding Servicing DC drop down-->
							<tr>
								<TD align="right"><asp:label id="lblCD" runat="server" EnableViewState="False">CD :</asp:label></TD>
								<TD><asp:checkbox id="chkCD" tabIndex="12" runat="server"></asp:checkbox></TD>
								<TD align="right"><asp:label id="lblMyUPSID" runat="server">FedEx/UPS Account Number(1-UPS) :</asp:label></TD>
								<td><asp:textbox id="txtMyUPSID" tabIndex="29" runat="server" Width="170px" MaxLength="30"></asp:textbox></td>
							</tr>
							<tr>
								<TD align="right"><asp:label id="lblServiceDC" runat="server" EnableViewState="False">Servicing DC :</asp:label></TD>
								<TD><asp:dropdownlist id="lstServiceDC" tabIndex="13" Runat="server">
										<ASP:LISTITEM Value="">--select one--</ASP:LISTITEM>
										<ASP:LISTITEM Value="CADC">CADC</ASP:LISTITEM>
										<ASP:LISTITEM Value="TNDC">TNDC</ASP:LISTITEM>
										<ASP:LISTITEM Value="BOTH">BOTH(Swing Store)</ASP:LISTITEM>
									</asp:dropdownlist></TD>
								<td align="right"><asp:label id="lblOpenDate" runat="server" EnableViewState="False">Open Date [Format: 1/23/75] :</asp:label></td>
								<td><asp:textbox id="txtOpenDate" tabIndex="30" runat="server" Width="170px" MaxLength="10"></asp:textbox></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;
									<asp:label id="lblReqOpenDate" runat="server" Visible="False"></asp:label></td>
								<td>&nbsp;</td>
							</tr>
							<TR>
								<TD align="right">&nbsp;</TD>
								<TD align="left"><asp:hyperlink id="btnStageConfig" runat="server" EnableViewState="False" Height="21px" BorderStyle="None"
										Font-Bold="True" BorderWidth="2px" ForeColor="Red" CssClass="red" NavigateUrl="StageConfig.aspx" Visible="False">Config Staging Store</asp:hyperlink></TD>
								<TD align="right" colSpan="2"><SPAN><asp:button id="btnModify" tabIndex="31" runat="server" CssClass="B1" Text="Update"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:button id="btnAddStore" tabIndex="32" runat="server" Visible="False" CssClass="B1" Text="Add"></asp:button><asp:button id="btnRemodel" tabIndex="33" runat="server" Visible="False" CssClass="B1" Text="Remodel"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:button id="btnClear" tabIndex="34" runat="server" Visible="False" CssClass="B1" Text="Clear"></asp:button><asp:button id="btnDelete" tabIndex="35" runat="server" CssClass="B1" Text="Delete"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:button id="btnReturn" tabIndex="36" runat="server" CssClass="B1" Text="Return"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
									</SPAN>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
		<hr>
	</body>
</html>
