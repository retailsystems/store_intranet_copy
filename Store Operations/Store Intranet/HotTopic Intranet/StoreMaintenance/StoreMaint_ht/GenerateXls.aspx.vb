Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Export.Xls
Imports DataDynamics.ActiveReports.Document.Margins


Public Class ActiveExcel
    Inherits System.Web.UI.Page

    Private m_objDataSet As New DataSet()
    Private m_BoxId As String

    Dim rpt As New ActiveExcelReport()
    ' Dim xls As New Export()

    Dim xls As ExcelExport = New ExcelExport()



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        StreamXLS()

    End Sub

    Private Sub StreamXLS()


        rpt.PageSettings.Margins.Left = 0.5
        rpt.PageSettings.Margins.Right = 0.5
        rpt.PageSettings.Margins.Bottom = 0.5
        rpt.PageSettings.Margins.Top = 0.5

        Try
            rpt.Run()
        Catch eRunReport As Exception
            ' Failure running report, just report the error to the user:
            Response.Clear()
            Response.Write("<h1>Error running report:</h1>")
            Response.Write(eRunReport.ToString())
            Exit Sub
        End Try

        ' Buffer this pages output until the XLS is complete.
        'Response.Buffer = True

        ' Clear any part of this page that might have already been buffered for output.
        Response.ClearContent()
        ' Clear any headers that might have already been buffered (such as the content type for an HTML page)
        Response.ClearHeaders()
        ' Tell the browser and the "network" that this resulting data of this page should be be cached since this could be a dynamic report that changes upon each request.
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        ' Tell the browser this is a Excel document so it will use an appropriate viewer.
        Response.ContentType = "application/xls"

        ' Create the Excel export object


        ' New XlsExport()

        ' Create a new memory stream that will hold the pdf output
        Dim memStream As System.IO.MemoryStream = New System.IO.MemoryStream()
        ' Export the report to Excel:
        xls.Export(rpt.Document, memStream)

        Response.AddHeader("Content-Length", memStream.Length.ToString())
        Response.AddHeader("Content-Disposition", "inline; filename=storeinfo.xls")
        Response.BinaryWrite(memStream.ToArray())
        Response.End()


    End Sub

End Class
