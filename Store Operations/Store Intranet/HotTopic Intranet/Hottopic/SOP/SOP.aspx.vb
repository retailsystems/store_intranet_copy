Partial Class SOP
    Inherits System.Web.UI.Page
    'Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim objDAL As New clsDAL()
    Dim objDataTable As New DataTable()
    Dim strImageSuffix As String
    Dim m_blnFilter As Boolean = False
    Dim m_blnClearSection As Boolean = False

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblError.InnerText = ""
        strImageSuffix = ConfigurationSettings.AppSettings("Image_Suffix")
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=SOP Manual")
        If Not IsPostBack Then
            Call mPageLoad()
            lblReturnState.Value = "True"
            Exit Sub
        End If
        If lblReturn.Value = "True" Then
            lblReturn.Value = ""
            If lblReturnState.Value = "True" Then
                Response.Redirect("../Home.asp")
            Else
                Call mPageLoad()
                cmdFilter.Value = "Show Latest"
                lblReturnState.Value = "True"
                Exit Sub
            End If
        End If

        lblReturn.Value = ""
        lblReturnState.Value = ""
        If cmdFilter.Value = "Show Latest" Then
            m_blnFilter = False
        Else
            m_blnFilter = True
        End If
        'If lblFilter.Value = "True" And lblPlus.Value = "" Then
        '    m_blnClearSection = True
        'End If
        If lblFilter.Value = "True" Then
            If m_blnFilter Then
                m_blnFilter = False
                cmdFilter.Value = "Show Latest"

            Else
                m_blnFilter = True
                cmdFilter.Value = "Show All"
                If lblPlus.Value = "" Then
                    m_blnClearSection = True
                End If
            End If
        ElseIf lblFilter.Value = "False" And m_blnFilter = True Then
            lblImageId.Value = "dltSection__ctl0_ibnSection"
            lblImageName.Value = "dltSection:_ctl0:ibnSection"
        End If
        If lblFilter.Value = "True" And lblPlus.Value = "True" Then

            lblFilter.Value = "False"
            Call mPageLoad()
            lblPlus.Value = ""
        End If

        'If (lblFilter.Value = "True" Or cmdFilter.Value = "Show All") And Not lblFilter.Value = "Reload" Then
        '    m_blnFilter = True
        '    'lblFilter.Value = "False"
        '    If lblPlus.Value = "True" Then
        '        Call mPageLoad()
        '    End If
        '    ' ElseIf cmdFilter.Value = "Show All" Then
        '    '    lblFilter.Value = "Reload"
        'ElseIf cmdFilter.Value = "Show All" And lblFilter.Value = "Reload" Then
        '    Call mPageLoad()
        '    lblFilter.Value = "False"
        '    cmdFilter.Value = "Show Latest"
        'End If
        'If (m_blnFilter Or lblFilter.Value = "Reload") And cmdFilter.Value = "Show All" Then
        '    cmdFilter.Value = "Show Latest"
        'ElseIf m_blnFilter And cmdFilter.Value = "Show Latest" Then
        '    cmdFilter.Value = "Show All"
        'End If

        'Use custom Stylesheet & Header


    End Sub

    Private Sub mPageLoad(Optional ByVal SectionID As Long = 0)
        Try
            If m_blnFilter = False And lblPlus.Value = "True" Then
                lblReturnState.Value = "True"
            End If
            dltSection.DataSource = objDAL.SelectSectionForms(SectionID, "Main", m_blnFilter, lblPlus.Value)
            dltSection.DataBind()
            If dltSection.DataSource.rows.count = 0 Then
                Throw New Exception("There are no recent files.")
            End If
        Catch ex As Exception
            lblError.InnerText = ex.Message
        End Try
    End Sub

    Public Sub ibnSection_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim blnLast As Boolean
        Dim blnPlus As Boolean
        Dim lngSectionID As Long
        Dim objDataList As DataList
        Dim objDataListItem As DataListItem
        Dim objImage As WebControls.Image
        Dim objImageButton As ImageButton
        'If m_blnFilter Then
        '    cmdFilter.Value = "Show All"
        'Else
        '    cmdFilter.Value = "Show Latest"
        'End If
        If TypeName(sender) = "ImageButton" Then
            objImageButton = sender

            With objImageButton

                blnPlus = (InStr(.ImageUrl, "Plus", CompareMethod.Text) > 0)
                If Not blnPlus And lblFilter.Value = "True" Then
                    blnPlus = True
                End If
                If lblFilter.Value = "True" Then
                    lblFilter.Value = "False"
                End If
                'If blnPlus Then
                '    lblPlus.Value = "True"
                'Else
                '    lblPlus.Value = ""
                'End If
                'If Not blnPlus And (lblFilter.Value = "Reload" Or lblFilter.Value = "True") Then
                '    blnPlus = True
                '    lblFilter.Value = "False"
                'End If

                If blnPlus Then
                    lngSectionID = Val(.CommandArgument)
                Else
                    lblSecNo.Value = "0"
                    lblImageName.Value = ""
                    lblImageId.Value = ""
                    'lblFilter.Value = "False"
                End If
                If Not blnPlus Then
                    lblPlus.Value = "True"
                End If
                If m_blnClearSection Then
                    lblImageName.Value = ""
                    lblImageId.Value = ""
                    lngSectionID = 0
                    lblPlus.Value = "True"
                End If
                Call mPageLoad(lngSectionID)
                lblPlus.Value = ""
                lblSecNo.Value = lngSectionID
            End With

            If blnPlus Then
                For Each objDataListItem In dltSection.Items
                    objImageButton = objDataListItem.FindControl("ibnSection")

                    With objImageButton
                        If .CommandArgument = lngSectionID Then
                            .ImageUrl = "../Images/Minus" & strImageSuffix & ".jpg"
                            blnLast = True
                        ElseIf blnLast Then
                            blnLast = False

                            Exit For
                        End If
                    End With
                Next

                If blnLast Then
                    objDataList = objDataListItem.FindControl("dltForm")
                    If Not objDataList.Items.Count = 0 Then
                        objDataListItem = objDataList.Items(objDataList.Items.Count - 1)
                        objImage = objDataListItem.FindControl("imgLine")
                        objImage.ImageUrl = "../Images/Line2e" & strImageSuffix & ".jpg"
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub dltSection_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dltSection.ItemDataBound

        Dim objDataList As DataList
        Dim objDataListItem As DataListItem
        Dim objImage As WebControls.Image
        Dim objImageButton As ImageButton
        If Not lblFilter.Value = "True" Then
            objImageButton = CType(e.Item, DataListItem).FindControl("ibnSection")
            objImageButton.ImageUrl = "../Images/Plus" & strImageSuffix & ".jpg"
        End If
        objDataList = CType(e.Item, DataListItem).FindControl("dltForm")

        For Each objDataListItem In objDataList.Items
            objImage = objDataListItem.FindControl("imgLine")
            objImage.ImageUrl = "../Images/Line2" & strImageSuffix & ".jpg"

            'objImage = objDataListItem.FindControl("imgForm")
            'objImage.ImageUrl = "../Images/Line3_" & objImage.AccessKey & strImageSuffix & ".jpg"
        Next

    End Sub

    Private Sub cmdSubmit_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSubmit.ServerClick
        Response.Redirect("Search.aspx?txtQuery=" & txtQuery.Value)
    End Sub

    'Private Sub cmdFilter_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFilter.ServerClick
    '    Dim int As Integer
    '    Dim objDataListItem As DataListItem
    '    If lblSecNo.Value = "" Then
    '        int = 0
    '    Else
    '        int = CInt(lblSecNo.Value)
    '    End If
    '    If cmdFilter.Value = "Show Latest" Then
    '        cmdFilter.Value = "Show All"
    '        dltSection.DataSource = objDAL.SelectSectionForms(int, "Main", True)
    '    Else
    '        cmdFilter.Value = "Show Latest"
    '        dltSection.DataSource = objDAL.SelectSectionForms(int, "Main", False)
    '    End If
    '    dltSection.DataBind()
    'End Sub
End Class