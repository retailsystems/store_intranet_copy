Imports Cisso
Imports ADODB
Partial Class Search
    Inherits System.Web.UI.Page
    'Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim strQuery As String = String.Empty
    Dim strVirtual As String = String.Empty
    Dim ds As DataSet 'Object
    Dim lcErrorMsg As String = String.Empty
    Dim lcVirtual As String = String.Empty
    Dim lcQuery As String = String.Empty
    Dim intRecordCount As Int32
    Dim intResTop As Int32
    Dim intResBot As Int32
    Dim intLoop As Int32
    Dim intLoop1 As Int32
    Dim intLoop3 As Int32
    Dim strTitle As String = String.Empty
    Dim strPath As String = String.Empty
    Dim intAbsPage As Int32
    Dim blnMoreThan10Results As Boolean
    Dim rstSOPDetail As New DataSet   'ADODB.Recordset()
    Dim strSQL As String = String.Empty
    Dim m_objDal As New clsDAL
    Dim objCommand As New Data.SqlClient.SqlCommand
    Dim objAdapter As New Data.SqlClient.SqlDataAdapter

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Response.Buffer = True
            Response.Expires = 0

            ''rstSOPDetail = Server.CreateObject("ADODB.Recordset")
            lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
            ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=SOP Manual")

            If Request.QueryString("txtQuery") <> "" Then
                strQuery = Request.QueryString("txtQuery")
                If Request.QueryString("AP") = "" Then
                    intAbsPage = 1
                Else
                    intAbsPage = Request.QueryString("AP")
                End If
            End If
            'If Request.Form("txtQuery") <> "" Then
            '    strQuery = Request("txtQuery")
            '    intAbsPage = 1
            'Else
            '    If Request.QueryString("txtQuery") > "" Then
            '        strQuery = Request.QueryString("txtQuery")
            '        intAbsPage = Request.QueryString("AP")
            '    Else
            '        strQuery = Request("txtSearchStr") & ""
            '        intAbsPage = 1
            '    End If
            'End If

            If strQuery <> "" Then
                lcErrorMsg = ""
                ds = RunQuery(strQuery, strVirtual)
            End If

            If ds Is Nothing Then 'If TypeName(rs) <> "Recordset" Then
                'Call gShowHeader("SOP Manual")
                'Response.Write("<iframe class='Header_Frame' id='ifmHeader' marginWidth='0' hspace='0' marginHeight='0' src='http://172.16.125.2/Header/Header.aspx?mode=hottopic' frameBorder='0' width='100%' scrolling='no' runat='server'></iframe>")
                lcErrorMsg = "<center><font class='red'><b>Your search returned no matching documents.</b><br></font>"
                lcErrorMsg = lcErrorMsg & "<font class='red'><b>Please try another search.</b></font><br><br></center>"
            End If

                If Len(lcErrorMsg) > 0 Then
                    'Response.Write("<p><b><font class='red'>" & lcErrorMsg & "</font></b>")
                    frmForms.InnerHtml += "<p><b><font class='red'>" & lcErrorMsg & "</font></b>"
                End If


            If lcErrorMsg = "" Then
                intRecordCount = ds.Tables(0).Rows.Count 'rs.recordcount
                '  get the top number of the current results page (i.e. 1 of 10) 1 is the number we are getting here
                intResTop = CInt((intAbsPage - 1) & "1")

                If (intRecordCount - intResTop) < 10 Then
                    intResBot = intRecordCount
                Else
                    intResBot = intResTop + 9
                End If

                frmForms.InnerHtml += "<font class='normal'>" & intResTop & " to " & intResBot & " of " & intRecordCount & " results for: <i><b>" & strQuery & "</b></i></font>"

                Call InquiryButton()

                frmForms.InnerHtml += "<table border='0' cellspacing='1' cellpadding='2'>"

                Dim int As Integer
                Dim int2 As Integer
                Dim int3 As Integer
                'If ds.Tables(0).Rows.Count < intResBot Then
                '    int3 = ds.Tables(0).Rows.Count - 1
                'Else
                '    int3 = intResBot - 1
                'End If
                'Dim row As DataRow
                For int = intResTop - 1 To intResBot - 1 'Do Until rs.eof
                    For int2 = 0 To ds.Tables(0).Columns.Count - 1
                        If ds.Tables(0).Rows(int)(int2).Equals(DBNull.Value) Then
                            ds.Tables(0).Rows(int)(int2) = ""
                        End If
                    Next
                    If ds.Tables(0).Rows(int)("DocTitle") <> "" Then
                        strTitle = ds.Tables(0).Rows(int)("DocTitle")
                    Else
                        strTitle = ds.Tables(0).Rows(int)("filename")
                    End If
                    strPath = ds.Tables(0).Rows(int)("Path")

                    If Len(strPath) = 0 Then strPath = "Path Not Found"

                    strSQL = "SELECT dbo.PROPERCASE(SOP_Title) AS Title "
                    strSQL = strSQL & "FROM SOP_Dtl "
                    strSQL = strSQL & "WHERE     ({ fn UCASE(SOP_PDF_Path) } = 'SOPDOCS/" & Replace(UCase(ds.Tables(0).Rows(int)("FileName")), "'", "''") & "') "
                    m_objDal.Connection.Open()
                    objCommand = m_objDal.Connection.CreateCommand
                    objCommand.CommandText = strSQL
                    objAdapter = New Data.SqlClient.SqlDataAdapter(objCommand)
                    'rstSOPDetail.Open(strSQL, m_objDal.Connection)
                    objAdapter.Fill(rstSOPDetail)
                    If rstSOPDetail.Tables(0).Rows.Count > 0 Then 'If rstSOPDetail.RecordCount > 0 Then
                        strTitle = rstSOPDetail.Tables(0).Rows(0)("Title")
                    Else
                        strTitle = "No Title"
                    End If
                    rstSOPDetail.Clear()
                    'rstSOPDetail.Close()

                    '  set up the numbering of each hyperlinked item within the page
                    If intAbsPage = 1 Or blnMoreThan10Results Then
                        intLoop1 = intLoop1 + 1
                    Else
                        intLoop1 = ((intAbsPage - 1) * 10) + 1
                        blnMoreThan10Results = True
                    End If

                    frmForms.InnerHtml += "<tr>"
                    frmForms.InnerHtml += "<td valign='top'><b><font class='normal'>" & intLoop1 & ".</font></b> </td>"
                    frmForms.InnerHtml += "<td valign='top'>"
                    frmForms.InnerHtml += "<font class='normal'>" & Fix(ds.Tables(0).Rows(int)("rank") / 10) & "%</font>&nbsp;<a href='../SOPDocs/" & ds.Tables(0).Rows(int)("FileName") & "' class='redUL'><b>" & strTitle & "</a>&nbsp;&nbsp;</b></td>"
                    frmForms.InnerHtml += "</tr>"
                    frmForms.InnerHtml += "<tr>"
                    frmForms.InnerHtml += "<td>&nbsp;</td>"
                    frmForms.InnerHtml += "<td>"
                    frmForms.InnerHtml += "<font class='normal'><b>Summary:</b>" & ds.Tables(0).Rows(int)("characterization") & "</font>"

                    If ds.Tables(0).Rows(int)("dockeywords") <> "" Then
                        frmForms.InnerHtml += "<font class='normal'><b>Keywords:</b>" & ds.Tables(0).Rows(int)("dockeywords") & "</font>"
                    End If

                    frmForms.InnerHtml += "<i><font class='normal'>Updated: " & ds.Tables(0).Rows(int)("write") & "</font></i>"
                    frmForms.InnerHtml += "</td>"
                    frmForms.InnerHtml += "</tr>"
                    'rs.MoveNext()

                    If intAbsPage = 1 And intLoop1 = 10 Then
                        Exit For
                    End If
                    'int += 1
                Next

                'rs.Close()

                frmForms.InnerHtml += "</table>"
            End If

            Call WriteNav()
            Call ClosingTags()
            'frmForms.InnerHtml +=  "</body>")
            'frmForms.InnerHtml +=  "</html>")


            'Lindsey's Version.  This one doesn't include the 'Return to Inquiry' button in this routine.  
        Catch ex As Exception
            frmForms.InnerHtml += "<p><b><font class='red'>" & ex.Message & "</font></b>"
        End Try
    End Sub
    Private Function RunQuery(ByVal lcQuery, ByVal lcVirtual)

        Dim oQ As New Cisso.CissoQuery
        Dim oU As New Cisso.CissoUtil
        'Dim rs As Object

        'Call gShowHeader("SOP Manual")
        'Response.Write("<iframe class='Header_Frame' id='ifmHeader' marginWidth='0' hspace='0' marginHeight='0' src='http://172.16.125.2/Header/Header.aspx?mode=hottopic' frameBorder='0' width='100%' scrolling='no' runat='server'></iframe>")
        If TypeName(lcVirtual) <> "String" Then
            lcVirtual = "/"
        End If

        If TypeName(lcQuery) <> "String" Then
            RunQuery = Nothing
            Exit Function
        End If

        ''oQ = Server.CreateObject("ixsso.Query")
        'oQ.Catalog = "Web"
        oQ.Catalog = "SOPHottopic"
        oQ.Query = lcQuery
        ' Allows you to specify which page properties are returned in the recordset
        oQ.Columns = "Path,Vpath,DocTitle,Filename,Write,Characterization,DocKeyWords,Rank"
        ' Other properties: Contents, hitcount, Path, DocAuthor
        oQ.SortBy = "Rank [d]"
        oQ.MaxRecords = 100

        'oU = Server.CreateObject("ixsso.util")

        'oU.AddScopeToQuery(oQ, "\\Argentdevbox\hottopic intranet\hottopic\SOPDocs", "shallow")

        On Error Resume Next
        Dim ds As New DataSet
        Dim adapter As New OleDb.OleDbDataAdapter
        'Dim rs As New ADODB.RecordsetClass()
        'rs = oQ.CreateRecordset("nonsequential")
        adapter.Fill(ds, oQ.CreateRecordset("nonsequential"), "Table")

        'rs.PageSize = 10

        'rs.AbsolutePage = intAbsPage

        If Err.Number <> 0 Then
            If Err.Number = 3021 Then
                Response.Write("<center><br><font class='red'><b>No matches.  Please try again.</b></font><br><br></center>")
            ElseIf Err.Number = -2147215867 Then
                frmForms.InnerHtml += "<center><p><b><font class='red'>&quot;" & lcQuery & "&quot; is an ignored word. </font></b></center>"
                'Response.Write("<center><br><font color='red'>" & Err.Description & "</font><br><br></center>")
            Else
                Response.Write("<center><br><font class='red'>" & Err.Description & "</font><br><br></center>")
            End If
            'Call ClosingTags()
            'Response.End()

        Else
            If ds.Tables.Count = 0 Then 'If TypeName(rs) <> "Recordset" Then
                RunQuery = Nothing
                Exit Function
            End If

            If ds.Tables(0).Rows.Count < 1 Then 'If rs.RecordCount < 1 Then
                RunQuery = Nothing
            Else
                ' Return result RecordSet
                RunQuery = ds 'rs
            End If
        End If

    End Function
    Private Sub WriteNav()

        If intRecordCount > 10 Then
            frmForms.InnerHtml += "<br><table border='0' cellspacing='0' cellpadding='0' width='100%'>"
            frmForms.InnerHtml += "<tr><td>"
            If intRecordCount = intResBot And intResBot > 10 Then
                frmForms.InnerHtml += "<a href='search.aspx?txtQuery=" & Server.UrlEncode(strQuery) & "&AP=" & (intAbsPage - 1) & "'><< <font>Previous Page</font></a>"
            Else
                If intAbsPage <> 1 Then
                    'frmForms.InnerHtml += "<font><< Previous Page</font>"
                    frmForms.InnerHtml += "<a href='search.aspx?txtQuery=" & Server.UrlEncode(strQuery) & "&AP=" & (intAbsPage - 1) & "'><< <font>Previous Page</font></a>"
                End If
            End If
            'If intResBot = 10 Then
            '    frmForms.InnerHtml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
            'End If
            If intRecordCount > intResBot Then
                frmForms.InnerHtml += "&nbsp;&nbsp;&nbsp;" '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                frmForms.InnerHtml += "<a href='search.aspx?txtQuery=" & Server.UrlEncode(strQuery) & "&AP=" & (intAbsPage + 1) & "'><font>&nbsp;Next Page >></font></a>"
            Else
                If intRecordCount = intResBot And intResBot > 10 Then
                    'Do nothing
                Else
                    frmForms.InnerHtml += "<font>Next Page >></font>"
                End If
            End If
            frmForms.InnerHtml += "</td>"
            'Response.Write "<td align='right'>"
            'Response.Write "&nbsp;&nbsp;<input class='B1' type='button' name='cmdReturn' value='Return to Inquiry' onClick='window.document.location=" & chr(34) & "SOP.asp" & chr(34) & "'"
            'Response.Write "</td></tr>"
            frmForms.InnerHtml += "</tr>"
        End If
    End Sub

    'Lindsey Sub Routine to get the "Return To Inquiry" Button
    Private Sub InquiryButton()
        'If intRecordCount > 10 Then
        frmForms.InnerHtml += "<table border='0' cellspacing='0' cellpadding='0' width='100%'>"
        frmForms.InnerHtml += "<tr><td>"
        frmForms.InnerHtml += "</td>"
        frmForms.InnerHtml += "<td align='right'>"
		frmForms.InnerHtml += "&nbsp;&nbsp;<input class='btn btn-danger' type='button' name='cmdReturn' value='Return to Inquiry' onClick='window.document.location=" & Chr(34) & "SOP.aspx" & Chr(34) & "'"
        frmForms.InnerHtml += "</td></tr>"
        'End If
    End Sub
    Private Sub ClosingTags()

        frmForms.InnerHtml += "<table border='0' cellspacing='0' cellpadding='0' width='100%'>"
        frmForms.InnerHtml += "<tr><td><hr></td></tR>"
		frmForms.InnerHtml += "<td align='right'><input class='btn btn-danger' type='button' name='cmdReturn' value='Return' onClick='history.back();'>&nbsp;&nbsp;<input class='btn btn-danger' type='button' value='Home' onClick='document.location=" & Chr(34) & "../Home.asp" & Chr(34) & ";' id='button' name='button'></td></tr>"
        frmForms.InnerHtml += "</table>"

    End Sub

End Class
