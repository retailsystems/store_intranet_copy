Option Explicit On 

Imports System.Data.SqlClient
Imports SOP
Public Class clsDAL

    Private blnScript As Boolean
    Private objSqlCommand As SqlCommand
    Private mstrDatabase As String = "Main"
    Public ReadOnly Property Connection() As SqlConnection
        Get
            Return cnnConnection()
        End Get
    End Property
    Private Function cnnConnection() As SqlConnection

        cnnConnection = New SqlConnection(ConfigurationSettings.AppSettings(ConfigurationSettings.AppSettings("Database_" & mstrDatabase)))
        'cnnConnection = New SqlConnection(ConfigurationSettings.AppSettings("ArgentDevBox_HotTopic"))
    End Function

    Private Function Execute() As String

        Dim intRowsAffected As Integer

        Try
            With objSqlCommand
                .Connection = cnnConnection()
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
            End With
        Catch
            Return "Error #" & Err.Number & ": " & Err.Description
        End Try

    End Function

    Public Function ExecuteSQL(ByVal vstrSQL As String, Optional ByVal vstrDatabase As String = "Main") As String

        objSqlCommand = New SqlCommand()
        mstrDatabase = vstrDatabase

        With objSqlCommand
            .CommandText = vstrSQL
            .CommandType = CommandType.Text
        End With

        Return Execute()

    End Function

    Private Function GetDataTable() As DataTable

        Dim objSqlDataAdapter As SqlDataAdapter
        Dim objDataTable As New DataTable()

        objSqlCommand.Connection = cnnConnection()

        objSqlDataAdapter = New SqlDataAdapter(objSqlCommand)
        objSqlDataAdapter.Fill(objDataTable)

        Return objDataTable

    End Function

    Public Function SelectSQL(ByVal vstrSQL As String, Optional ByVal vstrDatabase As String = "Main") As DataTable

        objSqlCommand = New SqlCommand()
        mstrDatabase = vstrDatabase

        With objSqlCommand
            .CommandText = vstrSQL
            .CommandType = CommandType.Text
        End With

        Return GetDataTable()

    End Function

    Public Function SelectSectionForms(ByVal vlngSectionID As Long, Optional ByVal vstrDatabase As String = "Main", Optional ByVal vblnFilter As Boolean = False, Optional ByVal vstrPlus As String = "") As DataTable

        Dim objDataSet As New DataSet()
        Dim objSqlDataAdapter As SqlDataAdapter
        Dim objSqlDataAdapter2 As SqlDataAdapter
        Dim strSQL As String
        Try
            mstrDatabase = vstrDatabase
            If vblnFilter Then
                strSQL = "SELECT DISTINCT SH.* FROM "
                strSQL += "SOP_Header SH RIGHT JOIN SOP_Dtl SD "
                strSQL += "ON SD.SOP_No BETWEEN SH.SOP_Sec_FROM AND SH.SOP_Sec_To "
                strSQL += "WHERE ((SD.Created_Date > '" & Now.AddDays(-30) & "') OR "
                strSQL = strSQL & "(SD.Modified_Date > '" & Now.AddDays(-30) & "')) "
                If vstrPlus <> "True" Then
                    strSQL &= " AND SH.SOP_Sec_No = " & vlngSectionID
                End If
            Else
                strSQL = "SELECT * "
                strSQL += "FROM SOP_Header "
                strSQL += "ORDER BY SOP_Sec_No"
            End If
            objSqlDataAdapter = New SqlDataAdapter(strSQL, cnnConnection)
            objSqlDataAdapter.Fill(objDataSet, "Header")
            'If objDataSet.Tables("Header").Rows.Count = 0 Then
            '    Throw New Exception("There are no recent files.")
            'End If
            strSQL = "SELECT SD.*, SH.SOP_Sec_No "
            strSQL += "FROM SOP_Dtl SD INNER JOIN "
            strSQL += "SOP_Header SH ON SD.SOP_No BETWEEN SH.SOP_Sec_From AND SH.SOP_Sec_To "
            strSQL += "WHERE SH.SOP_Sec_No = " & vlngSectionID & " "
            If vblnFilter Then
                strSQL = strSQL & "AND ((SD.Created_Date > '" & Now.AddDays(-30) & "') OR "
                strSQL = strSQL & "(SD.Modified_Date > '" & Now.AddDays(-30) & "')) "
                'Else
                '    strSQL += "WHERE SH.SOP_Sec_No = " & vlngSectionID & " "
            End If
            strSQL += "ORDER BY SD.SOP_No"
            objSqlDataAdapter2 = New SqlDataAdapter(strSQL, cnnConnection)
            objSqlDataAdapter2.Fill(objDataSet, "Detail")
            'If objDataSet.Tables("Detail").Rows.Count > 0 Then
            objDataSet.Relations.Add("HeaderDetail", objDataSet.Tables("Header").Columns("SOP_Sec_No"), objDataSet.Tables("Detail").Columns("SOP_Sec_No"))
            'End If
            Return objDataSet.Tables("Header")
     
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    'Javascript section

    Private Sub ButtonScript(ByVal objPage As Page)

        Dim objControl As Control
        Dim objHtmlForm As HtmlForm

        If Not blnScript Then
            For Each objControl In objPage.Controls
                If TypeName(objControl) = "HtmlForm" Then
                    objHtmlForm = objControl
                    objHtmlForm.Attributes.Add("onkeydown", "CatchKeyPress(window.event.keyCode, window.event.srcElement);")
                End If
            Next

            With objPage.Response
                .Write("<script language='javascript'> " & vbCrLf)
                .Write("function CatchKeyPress(KeyCode, Sender) { " & vbCrLf)
                .Write("var btnToBeClicked = null; " & vbCrLf)
                .Write("RemoveEnterAndEscEvents(); " & vbCrLf)
                .Write("if(KeyCode == '13') { " & vbCrLf)
                .Write("var ButtonName = Sender.getAttribute('TargetButton'); " & vbCrLf)
                .Write("btnToBeClicked = document.getElementById(ButtonName); " & vbCrLf)
                .Write("if(btnToBeClicked) { " & vbCrLf)
                .Write("btnToBeClicked.click(); " & vbCrLf)
                .Write("} " & vbCrLf)

                .Write("else {" & vbCrLf)
                .Write("Sender = document.getElementById('" & objHtmlForm.ID & "'); " & vbCrLf)
                .Write("ButtonName = Sender.getAttribute('TargetButton'); " & vbCrLf)
                .Write("btnToBeClicked = document.getElementById(ButtonName); " & vbCrLf)
                .Write("if(btnToBeClicked) { " & vbCrLf)
                .Write("btnToBeClicked.click(); " & vbCrLf)
                .Write("} " & vbCrLf)
                .Write("} " & vbCrLf)

                .Write("} " & vbCrLf)
                .Write("} " & vbCrLf)

                .Write("function RemoveEnterAndEscEvents() { " & vbCrLf)
                .Write("if (event.keyCode == 13 || event.keyCode == 27) { " & vbCrLf)
                .Write("event.cancelBubble = true; event.returnValue = false; " & vbCrLf)
                .Write("} " & vbCrLf)
                .Write("} " & vbCrLf)
                .Write("</script> " & vbCrLf)
            End With

            blnScript = True
        End If

    End Sub

    Public Sub FocusScript(ByVal objControl As Control)

        objControl.Page.RegisterStartupScript("focus", "<SCRIPT language='javascript'>document.getElementById('" & objControl.ID & "').focus() </SCRIPT>")

    End Sub

    Public Sub TextboxButton(ByVal objTextBox As TextBox, ByVal objButton As Control)

        objTextBox.Attributes.Add("TargetButton", objButton.ID)

        Call ButtonScript(objTextBox.Page)

    End Sub

    Public Sub ClearCookies(ByRef Request As HttpRequest, ByRef Response As HttpResponse)

        Dim intCookie As Integer
        Dim objCookie As Object
        Dim strName As String

        For intCookie = Request.Cookies.Count - 1 To 0 Step -1
            objCookie = Request.Cookies(intCookie)

            Select Case TypeName(objCookie)
                Case "HttpCookie"
                    strName = objCookie.Name
                Case "String"
                    strName = objCookie
                Case Else
                    strName = ""
            End Select

            Select Case strName
                Case "ASP.NET_SessionId", "StoreNo", ""
                    'Do nothing
                Case Else
                    Response.Cookies(strName).Expires = "1/1/1984"
                    Request.Cookies.Remove(strName)
            End Select
        Next

    End Sub

    Public Function GetUser(ByVal Request As HttpRequest, ByVal response As HttpResponse) As String

        Dim strName As String
        Dim strUserName As String

        If IsNothing(Request.Cookies("User")) Then
            response.Cookies.Add(New HttpCookie("User"))
        End If

        strUserName = Request.Cookies("User")("UserName")
        strName = Request.Cookies("User")("Name")

        Return IIf(strName = "" Or Not IsNumeric(strUserName), strUserName, strName)

    End Function

End Class
