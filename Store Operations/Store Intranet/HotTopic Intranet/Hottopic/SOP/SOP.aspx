<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SOP.aspx.vb" Inherits="SOP.SOP"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>SOP</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<script language="javascript">
			function filter(id, name) {
			//debugger 
				document.getElementById("lblImageId").value=id;
				document.getElementById("lblImageName").value=name;
				__doPostBack(name, id)
			}
			function poster() {
			//debugger 
				//if(document.getElementById("cmdFilter").value=="Show Latest") 
					document.getElementById("lblFilter").value="True";
				//else
					//document.getElementById("lblFilter").value="Reload"
				if(document.getElementById("lblImageName").value=="") {
					document.getElementById("lblPlus").value="True"
					__doPostBack("","")
					}
				else {
					document.getElementById("lblPlus").value=""
					__doPostBack(document.getElementById("lblImageName").value, document.getElementById("lblImageId").value)
				}
			}
			function key() {
				if(window.event.keyCode==13)
					document.getElementById("cmdSubmit").click();
			}
		</script>
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body>
		<form id="frmForms" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server" />
			<div class="container home" role="main">
			<table width="100%">
				<tr>
					<td>Table of Contents</td>
					<td align="right"><font>Search</font>&nbsp;&nbsp; <input id="txtQuery" type="text" onkeypress="key()" runat="server">&nbsp;&nbsp;
						<input class="btn btn-danger" id="cmdSubmit" type="button" value="Submit" runat="server">&nbsp;&nbsp;
						<input class="btn btn-danger" id="cmdFilter" onclick="poster()" type="button" value="Show Latest" runat="server"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<p>At Hot Topic, Inc. we believe that our greatest success is achieved when we work together to deliver results. 
				We refer to all employees as Associates. Some associates are hourly and some are Salaried, whether you work at Headquarters, 
				in the Distribution Center or Stores as a Store Manager, Assistant Manager, Keyholder or Sales employee, we all fall under the umbrella of being Associates.</p>
			<p><b>Company Polices & Procedures</b><br />You have access to the company�s general Standard Operating Procedures (SOPs) below. 
				These SOPs contain important information on Hot Topic, Inc�s. general policies and procedures and the privileges and obligations of an associate. 
				You are expected to read, understand, and adhere to company policies. The company may interpret, modify, rescind, delete, or add to any policies, benefits, 
				or practices at any time it deems appropriate.  Material changes will be communicated to you in any reasonable manner, including e-mail and by posting to the company intranet. 
				You are expected to read, understand, and adhere to these changes.</p>
			<table width="100%">
				<tr>
					<td><asp:datalist id="dltSection" runat="server" ShowFooter="False" ShowHeader="False" Width="100%">
							<ItemTemplate>
								<TABLE width="100%">
									<TR>
										<TD>
											<asp:ImageButton id="ibnSection" runat="server" OnClick="ibnSection_Click" onmousedown="filter(this.id, this.name)" CommandArgument='<%# Container.DataItem("SOP_Sec_No") %>' ImageUrl="../Images/Plus.jpg">
											</asp:ImageButton>Section
											<%# Container.DataItem("SOP_Sec_No") %>
											-
											<%# Container.DataItem("SOP_Sec_Name") %>
										</TD>
									</TR>
									<TR>
										<TD>
											<asp:DataList id="dltForm" runat="server" CellPadding=0 CellSpacing=0 Width="100%" ShowHeader="False" ShowFooter="False" datasource='<%# Container.DataItem.Row.GetChildRows("HeaderDetail") %>'>
												<ItemTemplate>
													<asp:Image id="imgLine" runat="server" ImageUrl="../Images/Line2.jpg"></asp:Image>
													<a href='../<%# Container.DataItem("SOP_PDF_Path") %>' >
														<%# Container.DataItem("SOP_No") %>
														-
														<%# Container.DataItem("SOP_Title") %>
													</a>
													<asp:Label id="lblNew" runat="server">
														<%# IIF(DATEDIFF("d", Container.DataItem("Modified_Date"), NOW) < 30, "&nbsp;&nbsp;&nbsp;<font class='red'>[revised]</font>", "") %>
													</asp:Label>
												</ItemTemplate>
											</asp:DataList>
										</TD>
									</TR>
								</TABLE>
							</ItemTemplate>
						</asp:datalist></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td>
						<font class="red" runat="server" id="lblError"></font>
					</td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="right"><asp:hyperlink id="hypReturn" runat="server" cssclass="btn btn-danger" NavigateUrl="javascript:document.getElementById('lblReturn').value='True';document.forms[0].submit();">Return</asp:hyperlink>&nbsp;&nbsp;
						<asp:hyperlink id="hypHome" runat="server" cssclass="btn btn-danger" navigateurl="../Home.asp">Home</asp:hyperlink></td>
				</tr>
			</table>
			<input id="lblFilter" type="hidden" value="False" runat="server"> <input id="lblImageId" type="hidden" runat="server">
			<input id="lblImageName" type="hidden" runat="server"> <input id="lblSecNo" type="hidden" runat="server">
			<input id="lblPlus" type="hidden" runat="server"> <input id="lblReturn" type="hidden" runat="server">
			<input id="lblReturnState" type="hidden" runat="server">
			</div>
		</form>
		<script language="javascript">
			document.getElementById("txtQuery").focus();
		</script>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
