<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SessionEnd.aspx.vb" Inherits="SSTransfer.SessionEnd"%>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Transfer Home</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body scroll="no" bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmTransferHome" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td height="1%"><uc1:Header id="ucHeader" runat="server"></uc1:Header></td>
				</tr>
				<tr>
					<td valign="center" align="middle">
						<asp:Label id="lblError" runat="server" Font-Names="Arial" Font-Size="Medium" ForeColor="Red">Your session has expired. Please log in to create a new session.</asp:Label><br>
					</td>
				</tr>
				<tr>
					<td height="1%"><uc1:Footer id="ucFooter" runat="server"></uc1:Footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
