Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document


Public Class ActiveExcelReport
    Inherits ActiveReport

    Private m_objDataSet As New DataSet()
    Private m_RowCount As Int32

    Dim connHeader As System.Data.SqlClient.SqlConnection


    Public Sub New()
        MyBase.New()
        InitializeReport()
    End Sub


#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As PageHeader = Nothing
    Private WithEvents Detail As Detail = Nothing
    Private WithEvents PageFooter As PageFooter = Nothing
	Public ds As DataDynamics.ActiveReports.DataSources.SqlDBDataSource = Nothing
	Private Picture1 As Picture = Nothing
	Private Label1 As Label = Nothing
	Private Label5 As Label = Nothing
	Private Label6 As Label = Nothing
	Private Label7 As Label = Nothing
	Private Label8 As Label = Nothing
	Private Label9 As Label = Nothing
	Private Label10 As Label = Nothing
	Private Label11 As Label = Nothing
	Private Label12 As Label = Nothing
	Private Label13 As Label = Nothing
	Private Label14 As Label = Nothing
	Private Label15 As Label = Nothing
	Private Label16 As Label = Nothing
	Private Label17 As Label = Nothing
	Private Label18 As Label = Nothing
	Private Label19 As Label = Nothing
	Private Label20 As Label = Nothing
	Private txtStoNum As TextBox = Nothing
	Private txtZip As TextBox = Nothing
	Private txtSt As TextBox = Nothing
	Private txtCity As TextBox = Nothing
	Private txtSimon As TextBox = Nothing
	Private txtTag As TextBox = Nothing
	Private txtPhone As TextBox = Nothing
	Private txtStoName As TextBox = Nothing
	Private txtRegion As TextBox = Nothing
	Private txtDistrict As TextBox = Nothing
	Private txtAddress As TextBox = Nothing
	Private txtMgr As TextBox = Nothing
	Private txtAssMgr As TextBox = Nothing
	Private txtDim As TextBox = Nothing
	Private txtOpenDate As TextBox = Nothing
	Private Line1 As Line = Nothing
	Private Line2 As Line = Nothing
	Private Line3 As Line = Nothing
	Private Line4 As Line = Nothing
	Private Line5 As Line = Nothing
	Private Line6 As Line = Nothing
	Private Line7 As Line = Nothing
	Private Line8 As Line = Nothing
	Private Line9 As Line = Nothing
	Private Line10 As Line = Nothing
	Private Line11 As Line = Nothing
	Private Line12 As Line = Nothing
	Private Line13 As Line = Nothing
	Private Line14 As Line = Nothing
	Private Label2 As Label = Nothing
	Private Label3 As Label = Nothing
	Private Label4 As Label = Nothing
	Public Sub InitializeReport()
		Me.LoadLayout(Me.GetType, "StoreMaint.ActiveExcelReport.rpx")
		Me.ds = CType(Me.DataSource,DataDynamics.ActiveReports.DataSources.SqlDBDataSource)
		Me.PageHeader = CType(Me.Sections("PageHeader"),DataDynamics.ActiveReports.PageHeader)
		Me.Detail = CType(Me.Sections("Detail"),DataDynamics.ActiveReports.Detail)
		Me.PageFooter = CType(Me.Sections("PageFooter"),DataDynamics.ActiveReports.PageFooter)
		Me.Picture1 = CType(Me.PageHeader.Controls(0),DataDynamics.ActiveReports.Picture)
		Me.Label1 = CType(Me.PageHeader.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label5 = CType(Me.PageHeader.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Label6 = CType(Me.Detail.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label7 = CType(Me.Detail.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label8 = CType(Me.Detail.Controls(2),DataDynamics.ActiveReports.Label)
		Me.Label9 = CType(Me.Detail.Controls(3),DataDynamics.ActiveReports.Label)
		Me.Label10 = CType(Me.Detail.Controls(4),DataDynamics.ActiveReports.Label)
		Me.Label11 = CType(Me.Detail.Controls(5),DataDynamics.ActiveReports.Label)
		Me.Label12 = CType(Me.Detail.Controls(6),DataDynamics.ActiveReports.Label)
		Me.Label13 = CType(Me.Detail.Controls(7),DataDynamics.ActiveReports.Label)
		Me.Label14 = CType(Me.Detail.Controls(8),DataDynamics.ActiveReports.Label)
		Me.Label15 = CType(Me.Detail.Controls(9),DataDynamics.ActiveReports.Label)
		Me.Label16 = CType(Me.Detail.Controls(10),DataDynamics.ActiveReports.Label)
		Me.Label17 = CType(Me.Detail.Controls(11),DataDynamics.ActiveReports.Label)
		Me.Label18 = CType(Me.Detail.Controls(12),DataDynamics.ActiveReports.Label)
		Me.Label19 = CType(Me.Detail.Controls(13),DataDynamics.ActiveReports.Label)
		Me.Label20 = CType(Me.Detail.Controls(14),DataDynamics.ActiveReports.Label)
		Me.txtStoNum = CType(Me.Detail.Controls(15),DataDynamics.ActiveReports.TextBox)
		Me.txtZip = CType(Me.Detail.Controls(16),DataDynamics.ActiveReports.TextBox)
		Me.txtSt = CType(Me.Detail.Controls(17),DataDynamics.ActiveReports.TextBox)
		Me.txtCity = CType(Me.Detail.Controls(18),DataDynamics.ActiveReports.TextBox)
		Me.txtSimon = CType(Me.Detail.Controls(19),DataDynamics.ActiveReports.TextBox)
		Me.txtTag = CType(Me.Detail.Controls(20),DataDynamics.ActiveReports.TextBox)
		Me.txtPhone = CType(Me.Detail.Controls(21),DataDynamics.ActiveReports.TextBox)
		Me.txtStoName = CType(Me.Detail.Controls(22),DataDynamics.ActiveReports.TextBox)
		Me.txtRegion = CType(Me.Detail.Controls(23),DataDynamics.ActiveReports.TextBox)
		Me.txtDistrict = CType(Me.Detail.Controls(24),DataDynamics.ActiveReports.TextBox)
		Me.txtAddress = CType(Me.Detail.Controls(25),DataDynamics.ActiveReports.TextBox)
		Me.txtMgr = CType(Me.Detail.Controls(26),DataDynamics.ActiveReports.TextBox)
		Me.txtAssMgr = CType(Me.Detail.Controls(27),DataDynamics.ActiveReports.TextBox)
		Me.txtDim = CType(Me.Detail.Controls(28),DataDynamics.ActiveReports.TextBox)
		Me.txtOpenDate = CType(Me.Detail.Controls(29),DataDynamics.ActiveReports.TextBox)
		Me.Line1 = CType(Me.Detail.Controls(30),DataDynamics.ActiveReports.Line)
		Me.Line2 = CType(Me.Detail.Controls(31),DataDynamics.ActiveReports.Line)
		Me.Line3 = CType(Me.Detail.Controls(32),DataDynamics.ActiveReports.Line)
		Me.Line4 = CType(Me.Detail.Controls(33),DataDynamics.ActiveReports.Line)
		Me.Line5 = CType(Me.Detail.Controls(34),DataDynamics.ActiveReports.Line)
		Me.Line6 = CType(Me.Detail.Controls(35),DataDynamics.ActiveReports.Line)
		Me.Line7 = CType(Me.Detail.Controls(36),DataDynamics.ActiveReports.Line)
		Me.Line8 = CType(Me.Detail.Controls(37),DataDynamics.ActiveReports.Line)
		Me.Line9 = CType(Me.Detail.Controls(38),DataDynamics.ActiveReports.Line)
		Me.Line10 = CType(Me.Detail.Controls(39),DataDynamics.ActiveReports.Line)
		Me.Line11 = CType(Me.Detail.Controls(40),DataDynamics.ActiveReports.Line)
		Me.Line12 = CType(Me.Detail.Controls(41),DataDynamics.ActiveReports.Line)
		Me.Line13 = CType(Me.Detail.Controls(42),DataDynamics.ActiveReports.Line)
		Me.Line14 = CType(Me.Detail.Controls(43),DataDynamics.ActiveReports.Line)
		Me.Label2 = CType(Me.PageFooter.Controls(0),DataDynamics.ActiveReports.Label)
		Me.Label3 = CType(Me.PageFooter.Controls(1),DataDynamics.ActiveReports.Label)
		Me.Label4 = CType(Me.PageFooter.Controls(2),DataDynamics.ActiveReports.Label)
	End Sub

#End Region

    Private Sub ActiveExcelReport_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        CreateDataSet()

    End Sub

    Private Sub CreateDataSet()


        Dim strSQL As String
        Dim strFilter As String
        Dim strReportType As String
        Dim strFileName As String
        Dim objSQLConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))

        Dim objSQLCommand As New SqlCommand()
        Dim objSQLDataAdapter As New SqlDataAdapter()

        Dim objRelation As DataRelation
        Dim strStoredProcedure As String

        strSQL = "SELECT StoreNum, Simon, Tags, Phone1, "
        strSQL = strSQL & " StoreName, Region, District, Manager,  "
        strSQL = strSQL & " AsstMgr1, AsstMgr2, AsstMgr3, AsstMgr4,  "
        strSQL = strSQL & " AsstMgr5, Address1, Address2, Address3, City, State, Zip,  "
        strSQL = strSQL & " Dimension, OpenDate from STORE "
        strSQL = strSQL & " ORDER BY StoreNum "


        objSQLCommand.Connection = objSQLConnection
        objSQLConnection.Open()
        objSQLCommand.CommandText = strSQL
        objSQLDataAdapter.SelectCommand = objSQLCommand
        objSQLDataAdapter.FillSchema(m_objDataSet, SchemaType.Mapped)
        objSQLDataAdapter.Fill(m_objDataSet, "StoreInfo")
        objSQLConnection.Close()

    End Sub


    Private Sub ActiveExcelReport_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles MyBase.FetchData
        Try
            If m_objDataSet.Tables("BoxItems").Rows.Count > m_RowCount Then
                Me.Fields("STORENUM").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("STORENUM")
                Me.Fields("SIMON").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("SIMON")
                Me.Fields("TAGS").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("TAGS")
                Me.Fields("PHONE1").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("PHONE1")
                Me.Fields("STORENAME").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("STORENAME")
                Me.Fields("REGION").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("REGION")
                Me.Fields("DISTRICT").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("DISTRICT")
                Me.Fields("MANAGER").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("MANAGER")
                Me.Fields("ASSTMGR1").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("ASSTMGR1")
                Me.Fields("ASSTMGR2").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("ASSTMGR2")
                Me.Fields("ASSTMGR3").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("ASSTMGR3")
                Me.Fields("ASSTMGR4").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("ASSTMGR4")
                Me.Fields("ASSTMGR5").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("ASSTMGR5")
                Me.Fields("ADDRESS1").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("ADDRESS1")
                Me.Fields("ADDRESS2").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("ADDRESS2")
                Me.Fields("ADDRESS3").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("ADDRESS3")
                Me.Fields("ADDRESS4").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("ADDRESS4")
                Me.Fields("ADDRESS5").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("ADDRESS5")
                Me.Fields("CITY").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("CITY")
                Me.Fields("STATE").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("STATE")
                Me.Fields("ZIP").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("ZIP")
                Me.Fields("DIMENSION").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("DIMENSION")
                Me.Fields("OPENDATE").Value = m_objDataSet.Tables("StoreInfo").Rows(m_RowCount)("OPENDATE")
                m_RowCount = m_RowCount + 1

                eArgs.EOF = False
            Else
                eArgs.EOF = True
            End If

        Catch
            eArgs.EOF = True
        End Try

    End Sub

End Class
