Public Class SetStore
    Inherits System.Web.UI.Page

    Protected WithEvents ucHeader As Header
    Protected WithEvents ddlSetStore As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSetStore As System.Web.UI.WebControls.Button

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private m_objUserInfo As New UserInfo()

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Get employee info
        m_objUserInfo.GetEmployeeInfo()

        'Set header porperties
        ucHeader.CurrentMode(Header.HeaderGroup.Transfer)
        ucHeader.lblTitle = "Assign Store Number"

        'Disable header buttons if store number is not set
        If m_objUserInfo.StoreNumber = -1 Then
            ucHeader.DisableButtons()
        End If

        If Not Page.IsPostBack Then
            FillStoreDDL(ddlSetStore, m_objUserInfo.StoreNumber) 'Fill store dropdown list
        End If

    End Sub

    'Filss dropdown list
    Private Sub FillStoreDDL(ByRef StoreDDL As System.Web.UI.WebControls.DropDownList, ByVal intSelected As Int32)

        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        Dim objDataReader As SqlClient.SqlDataReader
        Dim objItem As New ListItem()

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("asFillTStore", objConnection)

        objDataReader = objCommand.ExecuteReader()

        StoreDDL.DataSource = objDataReader
        StoreDDL.DataBind()

        'Add store
        objItem.Text = "Select A Store"
        objItem.Value = 0

        StoreDDL.Items.Insert(0, objItem)
        StoreDDL.SelectedIndex = StoreDDL.Items.IndexOf(StoreDDL.Items.FindByValue(intSelected.ToString))

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()

    End Sub

    'Set store cookie
    Private Sub btnSetStore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetStore.Click

        Response.Cookies("StoreNo").Value = ddlSetStore.SelectedItem.Value
        Response.Cookies("StoreNo").Expires = #1/1/2100#
        Response.Redirect("SetStore.aspx")

    End Sub

End Class
