<%@ Page CodeBehind="security.aspx.vb" Language="vb" AutoEventWireup="false" Inherits="StoreMaint.security" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body>
		<%
Public Function CheckLoginCookie(strForm)

	Dim strSQL
	Dim rstRecordset
	Dim intTimeout
	
	If Request.Cookies("User")("Id") <> "" then
		strSQL = "Select Description from List_Dtl where Table_Code = 'CookieTimeout' and Table_Key='" & strForm & "'"
		Set rstRecordset = Server.CreateObject("ADODB.Recordset")
		Set rstRecordset = cnnConnection.Execute(strSQL)
		If not rstRecordset.EOF then
			intTimeout = rstRecordset.Fields("Description")
			Response.Cookies("User").Expires = DateAdd("m",intTimeout,Now())
			rstRecordset.Close
			Set rstRecordset = Nothing
		End If
	Else
		'Response.Write "<html>"
		'Response.Write "<head><title>Login Expired</title></head>"
		'Response.Write "<body bgcolor='black' background='Images\htbackground.jpg' bgproperties='fixed' >"
		'Response.Write "<font color='Red'>You are no longer logged into the system.  Please Login again.</font><br>"
		'Response.Write "<a href='Login.asp'><font color='Red'>Back to Login Page</font></a>"
		'Response.Write "</body>"
		'Response.Write "</html>"
		'Response.End
		Call Response.Redirect("loginerror.asp")
	End If
	

End Function

Public Function CheckSecurityCookie(strForm)

	Dim strSQL
	Dim rstRecordset
	Dim intTimeout
	
	Select Case strForm
	Case "SOP"
		If Request.Cookies("User")("SOPAdmin") <> "Y" then
			'Response.Write "<html>"
			'Response.Write "<head><title>Access Denied</title></head>"
			'Response.Write "<body bgcolor='black' background='Images\htbackground.jpg' bgproperties='fixed' >"
			'Response.Write "<font color='Red'>You do not have access to this page.</font><br>"
			'Response.Write "<a href='Login.asp'><font color='Red'>Back to Home Page</font></a>"
			'Response.Write "</body>"
			'Response.Write "</html>"
			'Response.End
			Call Response.Redirect("loginerror.asp")
		End If
		
	Case "EFORM"
		If strForm = "EFORM" then
			If Request.Cookies("User")("EFORMSAdmin") <> "Y" then
				'Response.Write "<html>"
				'Response.Write "<head><title>Access Denied</title></head>"
				'Response.Write "<body bgcolor='black' background='Images\htbackground.jpg' bgproperties='fixed' >"
				'Response.Write "<font color='Red'>You do not have access to this page.</font><br>"
				'Response.Write "<a href='Login.asp'><font color='Red'>Back to Home Page</font></a>"
				'Response.Write "</body>"
				'Response.Write "</html>"
				'Response.End
				Call Response.Redirect("loginerror.asp")
			End If
		End If
	
	Case "SOR"
		If Request.Cookies("User")("SOR") <> "Y" then 
			Call Response.Redirect("loginerror.asp")
		End If
		
	Case "SO"
		If Request.Cookies("User")("SO") <> "Y" then 
			Call Response.Redirect("loginerror.asp")
		End If
		
	Case "MAINT"
		If Request.Cookies("User")("MAINT") <> "Y" then 
			Call Response.Redirect("loginerror.asp")
		End If
	
	End Select

End Function
%>
	</body>
</html>
