
Imports System.Data.OleDb


Public Class StoreSearch

    Inherits System.Web.UI.Page
    Protected WithEvents lblSrchOpenDate As System.Web.UI.WebControls.Label
    Protected WithEvents txtSrchOpenDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblStoNum As System.Web.UI.WebControls.Label
    Protected WithEvents txtSrchStoNum As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSrchRegion As System.Web.UI.WebControls.Label
    Protected WithEvents cboSrchRegion As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblStoName As System.Web.UI.WebControls.Label
    Protected WithEvents txtSrchStoName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSrchDist As System.Web.UI.WebControls.Label
    Protected WithEvents cboSrchDist As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbSrchState As System.Web.UI.WebControls.Label
    Protected WithEvents cboSrchState As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblSrchTags As System.Web.UI.WebControls.Label
    Protected WithEvents txtSrchTag As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSrchEmail As System.Web.UI.WebControls.Label
    Protected WithEvents txtSrchEmail As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSrchSimon As System.Web.UI.WebControls.Label
    Protected WithEvents txtSrchSimon As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEmpty As System.Web.UI.WebControls.Label
    Protected WithEvents lblNoResults As System.Web.UI.WebControls.Label
    Protected WithEvents btnSearch As System.Web.UI.WebControls.Button
    Protected WithEvents btnClear As System.Web.UI.WebControls.Button
    Protected WithEvents btnAdd As System.Web.UI.WebControls.Button


    'Flags to identify which search fields usrs have filled
    Dim SA_Num As Integer
    Dim SA_Name As Integer
    Dim SA_State As Integer
    Dim SA_Email As Integer
    Dim SA_OpenDate As Integer
    Dim SA_Region As Integer
    Dim SA_Dist As Integer
    Dim SA_Tags As Integer
    Dim SA_Simon As Integer
    Dim SA_CoName As Integer
    Dim SA_ShipNumb As Integer

    'Error Flags
    Dim ErrNum As Integer
    Dim ErrName As Integer
    Dim ErrState As Integer
    Dim ErrEmail As Integer
    Dim ErrOpenDate As Integer
    Dim ErrRegion As Integer
    Dim ErrDist As Integer
    Dim ErrTags As Integer
    Dim ErrSimon As Integer
    Dim ErrCoName As Integer
    Dim ErrShipNumb As Integer


    'connStrings 
    Dim connHeader As System.Data.SqlClient.SqlConnection
    Protected WithEvents lblCoName As System.Web.UI.WebControls.Label
    Protected WithEvents lblShipNumb As System.Web.UI.WebControls.Label
    Protected WithEvents txtShipNumb As System.Web.UI.WebControls.TextBox
    Protected WithEvents cboCoName As System.Web.UI.WebControls.DropDownList
    Dim connOra As System.Data.OleDb.OleDbConnection


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblEmpty.Visible = False
        lblNoResults.Visible = False

        If Not IsPostBack Then

            'populate pull downs
            build_DDL_CoName()
            build_DDL_State()
            build_DDL_Region()
            build_DDL_District()

        End If

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click

        cboSrchState.SelectedIndex = 0
        cboSrchRegion.SelectedIndex = 0
        cboSrchDist.SelectedIndex = 0
        cboCoName.SelectedIndex = 0

        txtSrchStoNum.Text = Nothing
        txtSrchStoName.Text = Nothing
        txtSrchEmail.Text = Nothing
        txtSrchOpenDate.Text = Nothing
        txtSrchTag.Text = Nothing
        txtSrchSimon.Text = Nothing
        txtShipNumb.Text = Nothing

    End Sub


    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Function ParseQuotes(ByVal ParseText As String) As String
        Dim Offset As Integer, X As Integer

        ParseText = Trim$(ParseText) 'optional but it is a good place to do it
        X = InStr(ParseText, "'")
        If X > 0 Then
            Offset = 1
            Do While X > 0
                ParseText = Left$(ParseText, X) & Mid$(ParseText, X)

                'copies the single quote twice
                Offset = X + 2     'get past both quotes for search to resume
                X = InStr(Offset, ParseText, "'")
            Loop
        End If

        ParseQuotes = ParseText

    End Function

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub actSearch(ByVal StoNum, ByVal StoName, ByVal StoState, ByVal StoEmail, ByVal StoOpenDate, _
    ByVal StoRegion, ByVal StoDist, ByVal StoTag, ByVal StoSimon, ByVal StoCoName, ByVal StoShipNumb)

        Dim CommandString As String = "SELECT  StoreNum AS 'Store Number', StoreName AS 'Store Name', Email, City, State, Region, District, Tags, Simon, OpenDate, CompanyName, ShipperNumber FROM STORE WHERE 'x' = 'x'"

        If SA_Num = 1 Then 'QA'd
            CommandString = CommandString & " AND StoreNum = " & StoNum
        End If

        If SA_Name = 1 Then 'QA'd
            CommandString = CommandString & " AND StoreName LIKE '" & StoName & "'"
        End If

        If SA_Email = 1 Then
            CommandString = CommandString & " AND Email LIKE '" & StoEmail & "'"
        End If

        If SA_State = 1 Then
            CommandString = CommandString & " AND State LIKE '" & StoState & "'"
        End If

        If SA_OpenDate = 1 Then
            CommandString = CommandString & " AND OpenDate = " & StoOpenDate
        End If

        If SA_Region = 1 Then 'QA'd
            CommandString = CommandString & " AND Region LIKE '" & StoRegion & "'"
        End If

        If SA_Dist = 1 Then 'QA'd
            CommandString = CommandString & " AND District = " & StoDist
        End If

        If SA_Tags = 1 Then
            CommandString = CommandString & " AND Tags = " & StoTag
        End If

        If SA_Simon = 1 Then
            CommandString = CommandString & " AND Simon = " & StoSimon
        End If

        If SA_CoName = 1 Then
            CommandString = CommandString & " AND CompanyName = '" & StoCoName & "'"
        End If

        If SA_ShipNumb = 1 Then
            CommandString = CommandString & " AND ShipperNumber LIKE '" & StoShipNumb & "'"
        End If

        'Bounce to the StoreResults page
        Response.Redirect("StoreResults.aspx?SQL=" & System.Web.HttpUtility.UrlEncode(CommandString))

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim StoNum As Integer
        Dim StoName As String
        Dim StoState As String
        Dim StoEmail As String
        Dim StoOpenDate As String
        Dim StoRegion As String
        Dim StoDist As String
        Dim StoTag As String
        Dim StoSimon As String
        Dim StoCoName As String
        Dim StoShipNumb As String

        Dim GoSearch As Boolean 'Counter 
        Dim Item As Integer 'Identifies which Vars user enters

        Dim ErrFlag As Integer

        'Hide msgs
        lblNoResults.Visible = False

        'Flag
        GoSearch = False

        '-----------------------------------------------------------------------------
        ' Input Validation happens here
        '
        '-----------------------------------------------------------------------------

        'Check if all fields are used
        If txtSrchStoNum.Text = Nothing And txtSrchStoName.Text = Nothing And txtSrchEmail.Text = Nothing And _
        txtSrchOpenDate.Text = Nothing And txtSrchSimon.Text = Nothing And _
        txtShipNumb.Text = Nothing And txtSrchTag.Text = Nothing And _
        cboSrchRegion.SelectedItem.Value = Nothing And _
        cboCoName.SelectedItem.Value = Nothing And _
        cboSrchState.SelectedItem.Value = Nothing And cboSrchDist.SelectedItem.Value = Nothing Then
            lblEmpty.Text = "No Search Data entered. Please fill in at least one search field."
            lblEmpty.Visible = True
            GoSearch = 0
        End If


        'StoreNum (1) 
        If txtSrchStoNum.Text <> Nothing Then
            If IsNumeric(txtSrchStoNum.Text) Then
                StoNum = ParseQuotes(txtSrchStoNum.Text)
                SA_Num = 1
                GoSearch = True
            Else
                ErrNum = 1
            End If
        End If


        'StoreName (2) 
        If txtSrchStoName.Text <> Nothing Then
            If Not IsNumeric(txtSrchStoName.Text) Then
                StoName = ParseQuotes(txtSrchStoName.Text)
                StoName = "%" & StoName & "%"
                SA_Name = 1
                GoSearch = True
            Else
                ErrName = 1
            End If
        End If


        'Email (3)
        If txtSrchEmail.Text <> Nothing Then
            If Not IsNumeric(txtSrchEmail.Text) Then
                StoEmail = ParseQuotes(txtSrchEmail.Text)
                StoEmail = "%" & StoEmail & "%"
                SA_Email = 1
                GoSearch = True
            Else
                ErrEmail = 1
            End If
        End If


        'OpenDate (4)
        If txtSrchOpenDate.Text <> Nothing Then
            If IsDate(txtSrchOpenDate) Then
                StoOpenDate = ParseQuotes(txtSrchOpenDate.Text)
                StoOpenDate = "%" & StoOpenDate & "%"
                SA_OpenDate = 1
                GoSearch = True
            Else
                ErrOpenDate = 1
            End If

        End If


        'Simon (5) ''
        If txtSrchSimon.Text <> Nothing Then
            If Not IsNumeric(txtSrchSimon.Text) Then
                StoSimon = ParseQuotes(txtSrchSimon.Text)
                StoSimon = "%" & StoSimon & "%"
                SA_Simon = 1
                GoSearch = True
            Else
                ErrSimon = 1
            End If
        End If

        'Tag(6) 
        If txtSrchTag.Text <> Nothing Then
            If Not IsNumeric(txtSrchTag.Text) Then
                StoTag = ParseQuotes(txtSrchTag.Text)
                StoTag = "%" & StoTag & "%"
                SA_Tags = 1
                GoSearch = True
            Else
                ErrTags = 1
            End If
        End If

        'State(7) - combo box so no checking required
        If cboSrchState.SelectedItem.Value <> Nothing Then
            StoState = ParseQuotes(cboSrchState.SelectedItem.Value)
            'StoState = "%" & StoState & "%"
            SA_State = 1
            GoSearch = True
        End If


        'District(8) combo box so no checking required 
        If cboSrchDist.SelectedItem.Value <> Nothing Then
            StoDist = ParseQuotes(cboSrchDist.SelectedItem.Value)
            'StoDist = "%" & StoDist & "%"
            SA_Dist = 1
            GoSearch = True
        End If


        'Region (9) combo box so no checking required
        If cboSrchRegion.SelectedItem.Value <> Nothing Then
            StoRegion = ParseQuotes(cboSrchRegion.SelectedItem.Value)
            'StoRegion = "%" & StoRegion & "%"
            SA_Region = 1
            GoSearch = True
        End If

        'Company Name(10) - combo box so no checking required
        If cboCoName.SelectedItem.Value <> Nothing Then
            StoCoName = ParseQuotes(cboCoName.SelectedItem.Value)
            'StoState = "%" & StoState & "%"
            SA_CoName = 1
            GoSearch = True
        End If

        'Shipper Number(11) 
        If txtShipNumb.Text <> Nothing Then
            If Not IsNumeric(txtShipNumb.Text) Then
                StoShipNumb = ParseQuotes(txtShipNumb.Text)
                StoShipNumb = "%" & StoShipNumb & "%"
                SA_ShipNumb = 1
                GoSearch = True
            Else
                ErrShipNumb = 1
            End If
        End If

        'If all fields are not empty
        If GoSearch = True Then
            actSearch(StoNum, StoName, StoState, StoEmail, StoOpenDate, StoRegion, StoDist, StoTag, StoSimon, StoCoName, StoShipNumb)
            'actSearchVars(StoNum, StoName, StoState, StoEmail, StoOpenDate, StoRegion, StoDist, StoTag, StoSimon)
        End If

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnHeaderOpen()

        'connection
        connHeader = New System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreSQLConn"))
        connHeader.Open()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Public Sub ConnHeaderClose()

        connHeader.Close()

    End Sub
    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_CoName()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(CompanyName) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "CompanyName")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboCoName.DataSource = dataTable.DefaultView
        cboCoName.DataTextField = "CompanyName"
        cboCoName.DataValueField = "CompanyName"
        cboCoName.DataBind()
        cboCoName.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub

    Private Sub build_DDL_State()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(State) FROM STATE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "State")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboSrchState.DataSource = dataTable.DefaultView
        cboSrchState.DataTextField = "State"
        cboSrchState.DataValueField = "State"
        cboSrchState.DataBind()
        cboSrchState.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_Region()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(Region) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboSrchRegion.DataSource = dataTable.DefaultView
        cboSrchRegion.DataTextField = "Region"
        cboSrchRegion.DataBind()
        cboSrchRegion.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_District()

        ConnHeaderOpen()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(District) FROM STORE "

        'data adapter
        Dim dataAdapt_State As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(CommandString, connHeader)

        'dataset 
        Dim dataset As DataSet = New DataSet()

        'Fill the reader 
        dataAdapt_State.Fill(dataset, "Store")

        'Get the one table from the dataset
        Dim dataTable As DataTable = dataset.Tables(0)

        cboSrchDist.DataSource = dataTable.DefaultView
        cboSrchDist.DataTextField = "District"
        cboSrchDist.DataBind()
        cboSrchDist.Items.Insert(0, "")

        ConnHeaderClose()

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Response.Redirect("Login.aspx")

    End Sub

End Class
