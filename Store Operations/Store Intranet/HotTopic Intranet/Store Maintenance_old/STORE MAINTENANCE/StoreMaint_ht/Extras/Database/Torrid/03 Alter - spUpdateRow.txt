ALTER  PROCEDURE spUpdateRow 
(
@StoNum smallINT,
@StoName VARCHAR(50), 
@Addr Varchar(105) = NULL, 
@Addr2 Varchar(35) = NULL, 
@Addr3 Varchar(35) = NULL, 
@City Varchar(35) = NULL, 
@State char(2) = NULL, 
@Zip Varchar(5) = NULL, 
@Phone1 Varchar(15) = NULL, 
@Phone2 Varchar(15)= NULL, 
@Email Varchar(40)= NULL, 
@OpenDate datetime = NULL, 
@Dimension smallInt = NULL, 
@Region varchar(3) = NULL, 
@District varchar(4)= NULL, 
@Manager varchar(50)= NULL, 
@AsstMgr1 varchar(50)= NULL, 
@AsstMgr2 varchar(50)= NULL,
@AsstMgr3 varchar(50)= NULL, 
@AsstMgr4 varchar(50)= NULL, 
@AsstMgr5 varchar(50)= NULL, 
@Tags varchar(5)= NULL, 
@Simon varchar(1) = NULL,
@ShipNumb varchar(10),
@CoName varchar(25),
@Modified_ID smallInt=NULL,
@Deleted bit=NULL,
@CD bit = NULL,
@ServiceDC varchar(10) = NULL,
@StagingStore Bit,
@MyUPSID int,
@Wan Bit,
@Subnet Varchar(15),
@Country char(2)
) 
/*-------------------------------------------------------------------------------------------------------------------
REVISION HISTORY:

--04/14/05  SRI BAJJURI New parameter "@ServiceDC " added
--05/09/05 Matthew Dinh New Parameter "@StagingStore" added
--05/11/05 Matthew Dinh New Parameter "@MyUPSID" added
--06/16/05 Matthew Dinh New Parameter "@wan, @Subnet, and @Country" added
--------------------------------------------------------------------------------------------------------------------*/
AS
UPDATE STORE SET 
StoreName = @StoName, 
Address = @Addr + ', ' + @Addr2,
Address1 = @Addr, 
Address2 = @Addr2, 
Address3 = @Addr3, 
City = @City, 
State = @State, 
Zip = @Zip,
Phone1 = @Phone1, 
Phone2 = @Phone2, 
Email = @Email, 
OpenDate = @OpenDate,
Dimension = @Dimension, 
Region = @Region, 
District = @District,
Manager = @Manager,
AsstMgr1 = @AsstMgr1, 
AsstMgr2 = @AsstMgr2,
AsstMgr3 = @AsstMgr3,
AsstMgr4 = @AsstMgr4, 
AsstMgr5 = @AsstMgr5,
Tags = @Tags, 
Simon = @Simon,
ShipperNumber = @ShipNumb,
CompanyName = @CoName,
Modified_ID = @Modified_ID,
Modified_Date = convert(varchar, GETDATE(), 101),
Deleted = @Deleted,
CD = @CD,
Service_DC = @ServiceDC,
StagingStore = @StagingStore,
MYUPS_Id = @MyUPSID,
Wan = @Wan,
Subnet = @subnet,
Country = @Country
WHERE 
StoreNum = @StoNum
GO