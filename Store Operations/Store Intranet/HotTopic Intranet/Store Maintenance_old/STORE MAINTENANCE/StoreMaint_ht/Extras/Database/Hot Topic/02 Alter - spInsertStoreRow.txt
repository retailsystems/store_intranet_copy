ALTER  PROCEDURE spInsertStoreRow
(
@StoNum SMALLINT,
@StoName VARCHAR(50), 
@Addr Varchar(105) = NULL, 
@Addr2 Varchar(35) = NULL, 
@Addr3 Varchar(35) = NULL, 
@City Varchar(35) = NULL, 
@State char(2) = NULL, 
@Zip Varchar(5) = NULL, 
@Phone1 Varchar(15) = NULL, 
@Phone2 Varchar(15)= NULL, 
@Email Varchar(40)= NULL, 
@OpenDate datetime = NULL, 
@Dimension smallInt = NULL, 
@Region varchar(3) = NULL, 
@District varchar(4)= NULL, 
@Manager varchar(50)= NULL, 
@AsstMgr1 varchar(50)= NULL, 
@AsstMgr2 varchar(50)= NULL,
@AsstMgr3 varchar(50)= NULL, 
@AsstMgr4 varchar(50)= NULL, 
@AsstMgr5 varchar(50)= NULL, 
@Tags varchar(5)= NULL, 
@Simon varchar(1) = NULL,
@ShipNumb varchar(10)=NULL,
@CoName varchar(25),
@Created_ID smallint,
@CD bit,
@ServiceDC varchar(10) = NULL,
@StagingStore Bit,
@MyUPSID Int,
@Wan Bit,
@Subnet varchar(15),
@Country char(2)
)
/*-------------------------------------------------------------------------------------------------------------------
REVISION HISTORY:

--04/14/05  SRI BAJJURI New parameter "@ServiceDC " added
--05/09/05 Matthew Dinh New Parameter "@StagingStore" added
--05/14/05 Matthew Dinh New Parameter "@MyUPSID" added
--06/15/05 Matthew Dinh New Parameter "@Wan" and "@Subnet" added
--------------------------------------------------------------------------------------------------------------------*/
AS
INSERT INTO STORE(StoreNum, StoreName, Address, Address1, Address2, Address3, 
City, State, Zip, Phone1, Phone2, Email, OpenDate, Dimension, 
Region, District, Manager, AsstMgr1, AsstMgr2, AsstMgr3,
AsstMgr4, AsstMgr5, Tags, Simon, ShipperNumber, CompanyName, Created_ID, 
Created_Date,CD,Service_DC, StagingStore, MYUPS_Id,
Wan, Subnet, Country, InternetStore)  
VALUES (@StoNum, @StoName, @Addr + ', ' + @Addr2, @Addr, @Addr2,@Addr3, @City, @State, @Zip, 
@Phone1, @Phone2, @Email, @OpenDate, @Dimension, 
@Region, @District, @Manager, @AsstMgr1, @AsstMgr2, 
@AsstMgr3, @AsstMgr4, @AsstMgr5, @Tags, @Simon, 
@ShipNumb, @CoName, @Created_ID, convert(varchar, GETDATE(), 101),
@CD,@ServiceDC, @StagingStore, @MyUPSID, @Wan, @Subnet, @Country, 0)
GO