<%@ Page Language="vb" AutoEventWireup="false" Codebehind="StageConfig.aspx.vb" Inherits="StoreMaint.StageConfig"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>StageConfig</title>
		<meta content="True" name="vs_showGrid">
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/HotTopic.css" type="text/css"
			rel="stylesheet" runat="server">
		</LINK>
		<script language="JavaScript" src="Javascript/DisableClientBack.js"></script>
		<script language="JavaScript">
			<!--
			javascript:window.history.forward(1);
			//-->
		</script>
	</HEAD>
	<body id="pageBody" scroll="yes" runat="server">
		<form id="StageConfig" method="post" runat="server">
			<iframe class="Header_Frame" id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0"
				src="http://172.16.1.9/Header/Header.aspx?mode=hottopic" frameBorder="0" width="100%"
				scrolling="no" runat="server"></iframe>
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD>
						<TABLE id="tblDisplay" cellSpacing="0" cellPadding="0" width="100%" align="left">
							<TR>
								<TD align="center" colSpan="4">This form is to be used to Modify the default 
									Staging Store Configuration.</TD>
							</TR>
							<TR>
								<TD align="right">&nbsp;</TD>
								<TD>&nbsp;</TD>
								<TD align="right">&nbsp;</TD>
								<TD>&nbsp;</TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblCoName" runat="server" EnableViewState="False">Company :</asp:label></TD>
								<TD><asp:dropdownlist id="cboCoName" tabIndex="20" runat="server" AutoPostBack="True">
										<asp:ListItem Value="1">Hot Topic</asp:ListItem>
										<asp:ListItem Value="2">Torrid</asp:ListItem>
									</asp:dropdownlist></TD>
								<TD align="right"></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblDefaultName" runat="server" EnableViewState="False">Default Name :</asp:label></TD>
								<TD><asp:textbox id="txtDefaultName" tabIndex="3" runat="server" MaxLength="105" Width="170px"></asp:textbox></TD>
								<TD align="right">&nbsp;</TD>
								<TD>&nbsp;</TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblStoAddr" runat="server" EnableViewState="False">Address Line 1 :</asp:label></TD>
								<TD><asp:textbox id="txtAddr" tabIndex="3" runat="server" MaxLength="105" Width="170px"></asp:textbox></TD>
								<TD align="right"><asp:label id="lblStaging" runat="server">Staging :</asp:label></TD>
								<TD><asp:checkbox id="cbStaging" tabIndex="1" runat="server" Enabled="False"></asp:checkbox></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblStoAddr2" runat="server" EnableViewState="False">Address Line 2 :</asp:label></TD>
								<TD><asp:textbox id="txtAddr2" tabIndex="4" runat="server" MaxLength="35" Width="170px"></asp:textbox></TD>
								<TD align="right"><asp:label id="lblCD" runat="server" EnableViewState="False">CD :</asp:label></TD>
								<TD><asp:checkbox id="chkCD" tabIndex="12" runat="server"></asp:checkbox></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblCity" runat="server" EnableViewState="False">City :</asp:label></TD>
								<TD><asp:textbox id="txtCity" tabIndex="6" runat="server" MaxLength="35" Width="170px"></asp:textbox></TD>
								<TD align="right"><asp:label id="lblWan" runat="server" EnableViewState="False">WAN :</asp:label></TD>
								<TD><asp:checkbox id="cboWan" tabIndex="17" runat="server"></asp:checkbox></TD>
							<TR>
								<TD align="right"><asp:label id="lblState" runat="server" EnableViewState="False">State :</asp:label></TD>
								<TD><asp:dropdownlist id="cboState" tabIndex="7" runat="server" Width="60px"></asp:dropdownlist></TD>
								<TD align="right"></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD align="right" style="HEIGHT: 24px"><asp:label id="lblZip" runat="server" EnableViewState="False">Zip :</asp:label></TD>
								<TD style="HEIGHT: 24px"><asp:textbox id="txtZip" tabIndex="7" runat="server" MaxLength="5" Width="60px"></asp:textbox></TD>
								<TD align="right" style="HEIGHT: 24px"><asp:label id="lblServiceDC" runat="server" EnableViewState="False">Servicing DC :</asp:label></TD>
								<TD style="HEIGHT: 24px"><asp:dropdownlist id="lstServiceDC" tabIndex="13" Runat="server">
										<ASP:LISTITEM Value="">--select one--</ASP:LISTITEM>
										<ASP:LISTITEM Value="CADC">CADC</ASP:LISTITEM>
										<ASP:LISTITEM Value="TNDC">TNDC</ASP:LISTITEM>
										<ASP:LISTITEM Value="BOTH">BOTH(Swing Store)</ASP:LISTITEM>
									</asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblCountry" runat="server">Country :</asp:label></TD>
								<TD><asp:textbox id="txtCountry" tabIndex="8" runat="server" MaxLength="2" Width="60px" ToolTip="(US)-United States, (PR)-Puerto Rico"></asp:textbox>&nbsp;</TD>
								<TD align="right"></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD align="right">&nbsp;<asp:label id="lblPhone1" runat="server" EnableViewState="False" Height="10px">Phone 1:</asp:label>
									<asp:label id="lblP1Format" runat="server" EnableViewState="False">(xxx)xxx-xxxx</asp:label>&nbsp;</TD>
								<TD><asp:textbox id="txtPhone1" tabIndex="9" runat="server" MaxLength="13" Width="170px"></asp:textbox></TD>
								<TD align="right"><asp:label id="lblShipperNumber" runat="server" EnableViewState="False">UPS Shipper Number :</asp:label></TD>
								<TD><asp:textbox id="txtShipperNumber" tabIndex="28" runat="server" EnableViewState="False" MaxLength="7"
										Width="170px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblPhone2" runat="server" EnableViewState="False">Phone 2:</asp:label>&nbsp;<asp:label id="lblP2Format" runat="server" EnableViewState="False">(xxx)xxx-xxxx</asp:label>&nbsp;</TD>
								<TD><asp:textbox id="txtPhone2" tabIndex="10" runat="server" MaxLength="13" Width="170px"></asp:textbox></TD>
								<TD align="right"><asp:label id="lblMyUPSID" runat="server">MyUPS ID :</asp:label></TD>
								<TD><asp:textbox id="txtMyUPSID" tabIndex="29" runat="server" MaxLength="4" Width="170px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="right"></TD>
								<TD></TD>
								<TD align="right"></TD>
								<TD></TD>
							</TR>
							<!-- SRI 4/14/2004 Adding Servicing DC drop down-->
							<tr>
								<TD align="right"></TD>
								<TD></TD>
								<TD align="right"></TD>
								<td></td>
							</tr>
							<tr>
								<TD align="right">&nbsp;</TD>
								<TD>&nbsp;</TD>
								<td align="right">&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<TR>
								<TD align="right"></TD>
								<TD></TD>
								<TD align="right" colSpan="2"><SPAN><asp:button id="btnModify" tabIndex="31" runat="server" Text="Update" CssClass="B1"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:button id="btnReturn" tabIndex="36" runat="server" Text="Return" CssClass="B1"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
									</SPAN>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
		<hr>
	</body>
</HTML>
