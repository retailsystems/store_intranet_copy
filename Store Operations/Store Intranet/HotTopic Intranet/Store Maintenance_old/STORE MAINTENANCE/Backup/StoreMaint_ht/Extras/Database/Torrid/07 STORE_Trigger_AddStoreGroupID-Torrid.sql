CREATE TRIGGER STORE_Trigger_AddStoreGroupID
ON  dbo.STORE 
FOR INSERT
AS
/**************************************************************************
**	File: STORE_Trigger_AddStoreGroupID-Torrid.sql
**	Name: STORE_Trigger_AddStoreGroupID
**	Desc:  This trigger will add new GroupID to STS for new Store
**
**************************************************************************/
DECLARE @Country VARCHAR(2)
DECLARE @StoreNumber INT
SELECT @Country = (SELECT country FROM Inserted)
SELECT @StoreNumber = (SELECT storenum FROM Inserted)
IF (@Country = 'US')
BEGIN
	INSERT INTO SSTransferDB_Torrid.dbo.Store_Group(StoreGroup_ID, StoreNum)
	VALUES(1, @StoreNumber)
END
IF (@Country = 'PR')
BEGIN
	INSERT INTO SSTransferDB_Torrid.dbo.Store_Group(StoreGroup_ID, StoreNum)
	VALUES(2, @StoreNumber)
END