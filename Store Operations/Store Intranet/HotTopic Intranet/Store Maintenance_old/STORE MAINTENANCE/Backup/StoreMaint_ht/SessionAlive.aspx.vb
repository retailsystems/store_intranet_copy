Public Class SessionAlive
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim objTransferLock As New TransferLock()
        Dim m_objUserInfo As New UserInfo()

        'Get employee info
        m_objUserInfo.GetEmployeeInfo()

        'If employee is logged on to the system then their locks are kept alive
        If m_objUserInfo.EmpOnline Then
            m_objUserInfo.KeepLogonAlive()
            objTransferLock.KeepLocksAlive(m_objUserInfo.EmpId)
        End If

        'Refresh every x - 3 minutes where x is the length of the session before it times out
        Response.AddHeader("refresh", Convert.ToString((Convert.ToInt32(Session.Timeout.ToString())) * 60) - 180)

    End Sub

End Class
