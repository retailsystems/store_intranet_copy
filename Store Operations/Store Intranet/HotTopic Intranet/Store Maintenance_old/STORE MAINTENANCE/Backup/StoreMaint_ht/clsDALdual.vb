Option Explicit On 

'Imports System.Data.OracleClient
Imports System.Data.SqlClient
Imports System.IO
Imports System.Globalization


Public Class clsDALdual

    'Private objOracleCommand As OracleCommand
    Private objSqlCommand As SqlCommand
    Private strDatabase As String

    'Private Function cnnOracleConnection() As OracleConnection
    '    Dim objAppSettingsReader As New AppSettingsReader()
    '    cnnOracleConnection = New OracleConnection(ConfigurationSettings.AppSettings(strDatabase))
    'End Function

    Private Function cnnSqlConnection() As SqlConnection
        Dim objAppSettingsReader As New AppSettingsReader()
        cnnSqlConnection = New SqlConnection(ConfigurationSettings.AppSettings(strDatabase))
    End Function

    'Private Function ExecuteOracle() As Single
    '    Dim intRecordsAffected As Single
    '    strDatabase = "Oracle"
    '    With objOracleCommand
    '        .Connection = cnnOracleConnection()
    '        .Connection.Open()
    '        intRecordsAffected = .ExecuteNonQuery()
    '        .Connection.Close()
    '    End With
    '    Return intRecordsAffected
    'End Function

    Private Function ExecuteSQL() As Single
        Dim intRecordsAffected As Single
        With objSqlCommand
            .Connection = cnnSqlConnection()
            .Connection.Open()
            intRecordsAffected = .ExecuteNonQuery()
            .Connection.Close()
        End With
        Return intRecordsAffected
    End Function

    'Public Function ExecuteOracleSQL(ByVal vstrSQL As String) As Single
    '    objOracleCommand = New OracleCommand()
    '    With objOracleCommand
    '        .CommandText = vstrSQL
    '        .CommandType = CommandType.Text
    '    End With
    '    Return ExecuteOracle()
    'End Function

    Public Function ExecuteSQL(ByVal vstrSQL As String, ByVal vstrDatabase As String) As Single
        strDatabase = vstrDatabase
        objSqlCommand = New SqlCommand()
        With objSqlCommand
            .CommandText = vstrSQL
            .CommandType = CommandType.Text
        End With
        Return ExecuteSQL()
    End Function

    'Private Function GetOracleDataTable() As DataTable
    '    Dim objSQL As OracleDataAdapter
    '    Dim objDataTable As New DataTable()
    '    objOracleCommand.Connection = cnnOracleConnection()
    '    objSQL = New OracleDataAdapter(objOracleCommand)
    '    objSQL.Fill(objDataTable)
    '    Return objDataTable
    'End Function

    Private Function GetSqlDataTable() As DataTable
        Dim objSQL As SqlDataAdapter
        Dim objDataTable As New DataTable()
        objSqlCommand.Connection = cnnSqlConnection()
        objSQL = New SqlDataAdapter(objSqlCommand)
        objSQL.Fill(objDataTable)
        Return objDataTable
    End Function

    'Public Function SelectOracleSQL(ByVal vstrSQL As String) As DataTable
    '    strDatabase = "Oracle"

    '    objOracleCommand = New OracleCommand()
    '    With objOracleCommand
    '        .CommandText = vstrSQL
    '        .CommandType = CommandType.Text
    '    End With
    '    Return GetOracleDataTable()
    'End Function

    Public Function SelectSQL(ByVal vstrSQL As String, ByVal vstrDatabase As String) As DataTable
        strDatabase = vstrDatabase
        objSqlCommand = New SqlCommand()
        With objSqlCommand
            .CommandText = vstrSQL
            .CommandType = CommandType.Text
        End With
        Return GetSqlDataTable()
    End Function

    Public Function SelectSQL_SP(ByVal vstrDatabase As String, ByVal strSPName As String) As DataTable
        strDatabase = vstrDatabase

        objSqlCommand = New SqlCommand()

        With objSqlCommand

            .CommandText = strSPName

            .CommandType = CommandType.StoredProcedure

        End With

        Return GetSqlDataTable()

    End Function


    Public Function GetUser(ByVal Request As HttpRequest, ByVal response As HttpResponse) As String
        Dim strName As String
        Dim strUserName As String
        If IsNothing(Request.Cookies("User")) Then
            response.Cookies.Add(New HttpCookie("User"))
        End If
        strUserName = Request.Cookies("User")("UserName")
        strName = Request.Cookies("User")("Name")
        Return IIf(strName = "" Or Not IsNumeric(strUserName), strUserName, strName)
    End Function

    'Javascript section
    Public Sub AlertScript(ByVal objPage As Page, ByVal strMessage As String)
        objPage.RegisterStartupScript("alert", "<SCRIPT language='javascript'>alert(""" & strMessage & """);</SCRIPT>")
    End Sub

    ' Moves the Cursor on the Store Number Text Field
    Public Sub FocusScript(ByVal objControl As Control)
        objControl.Page.RegisterStartupScript("focus", "<SCRIPT language='javascript'>document.getElementById('" & objControl.ID & "').focus() </SCRIPT>")
    End Sub

    'makes the specified button and textbox the default, so pressing Enter on keyboard, is like clicking the button specified   
    Dim blnScript As Boolean

    Public Sub TextboxButton(ByVal objTextBox As TextBox, ByVal objButton As Control)

        objTextBox.Attributes.Add("TargetButton", objButton.ID)

        Call ButtonScript(objTextBox.Page)

    End Sub

    Private Sub ButtonScript(ByVal objPage As Page)

        Dim objControl As Control

        Dim objHtmlForm As HtmlForm

        If Not blnScript Then

            For Each objControl In objPage.Controls

                If TypeName(objControl) = "HtmlForm" Then

                    objHtmlForm = objControl

                    objHtmlForm.Attributes.Add("onkeydown", "CatchKeyPress(window.event.keyCode, window.event.srcElement);")

                End If

            Next

            With objPage.Response

                .Write("<script language='javascript'> " & vbCrLf)

                .Write("function CatchKeyPress(KeyCode, Sender) { " & vbCrLf)

                .Write("var btnToBeClicked = null; " & vbCrLf)

                .Write("RemoveEnterAndEscEvents(); " & vbCrLf)

                .Write("if(KeyCode == '13') { " & vbCrLf)

                .Write("var ButtonName = Sender.getAttribute('TargetButton'); " & vbCrLf)

                .Write("btnToBeClicked = document.getElementById(ButtonName); " & vbCrLf)

                .Write("if(btnToBeClicked) { " & vbCrLf)

                .Write("btnToBeClicked.click(); " & vbCrLf)

                .Write("} " & vbCrLf)

                .Write("else {" & vbCrLf)

                .Write("Sender = document.getElementById('Form1'); " & vbCrLf)

                .Write("ButtonName = Sender.getAttribute('TargetButton'); " & vbCrLf)

                .Write("btnToBeClicked = document.getElementById(ButtonName); " & vbCrLf)

                .Write("if(btnToBeClicked) { " & vbCrLf)

                .Write("btnToBeClicked.click(); " & vbCrLf)

                .Write("} " & vbCrLf)

                .Write("} " & vbCrLf)

                .Write("} " & vbCrLf)

                .Write("} " & vbCrLf)

                .Write("function RemoveEnterAndEscEvents() { " & vbCrLf)

                .Write("if (event.keyCode == 13 || event.keyCode == 27) { " & vbCrLf)

                .Write("event.cancelBubble = true; event.returnValue = false; " & vbCrLf)

                .Write("} " & vbCrLf)

                .Write("} " & vbCrLf)

                .Write("</script> " & vbCrLf)

            End With

            blnScript = True

        End If

    End Sub

    Public Sub DefaultButton(ByVal objButton As Control)

        Dim objControl As Control

        Dim objHtmlForm As HtmlForm

        For Each objControl In objButton.Page.Controls

            If TypeName(objControl) = "HtmlForm" Then

                objHtmlForm = objControl

                objHtmlForm.Attributes.Add("TargetButton", objButton.ID)

            End If

        Next

        Call ButtonScript(objButton.Page)

    End Sub


End Class