<%@ Page Language="vb" AutoEventWireup="false" Codebehind="GetCurrentHT.aspx.vb" Inherits="StoreMaint.GetCurrentHT"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>GetCurrentHT</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/hottopic.css" type="text/css" rel="stylesheet" runat="server">
		</LINK>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<iframe class="Header_Frame" id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="http://172.16.2.13/Header/Header.aspx?mode=hottopic" frameBorder="0" width="100%" scrolling="no" runat="server">
			</iframe>
			<!--Below is table for controls before grid section -->
			<!--Below is table for grid section wControls --><br>
			<table class="datagrid" width="100%" align="middle">
				<tr align="middle">
					<td>Current Store List - Hot Topic
					</td>
				</tr>
				<tr>
					<!--bgcolor below has to be there, or table turns burgandy-->
					<td align="middle" bgColor="black">
						<div style="OVERFLOW-Y: scroll; SCROLLBAR-HIGHLIGHT-COLOR: #990000; WIDTH: 100%; SCROLLBAR-ARROW-COLOR: #ffffff; SCROLLBAR-BASE-COLOR: #440000; HEIGHT: 300px" align="center"><asp:datagrid id="dgHTStoreList" runat="server" Width="100%" CssClass="datagridsm" showfooter="True" AutoGenerateColumns="False" EnableViewState="True" AllowSorting="True" gridlines="None">
								<AlternatingItemStyle CssClass="BC333333sm"></AlternatingItemStyle>
								<ItemStyle CssClass="BC111111sm"></ItemStyle>
								<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
								<Columns>
									<asp:BoundColumn DataField="Store" HeaderText="Store #" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Phone" HeaderText="Phone" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="StoreName" HeaderText="Store Name" HeaderStyle-Wrap="False" HeaderStyle-HorizontalAlign="Left">
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Reg" HeaderText="Reg" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="District" HeaderText="Dst" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Manager" HeaderText="Manager" HeaderStyle-Wrap="False" HeaderStyle-HorizontalAlign="Left">
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="AsstMgrs" HeaderText="AsstMgrs" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="AM2_blank" HeaderText="" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="AM3_blank" HeaderText="" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="AM4_blank" HeaderText="" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="AM5_blank" HeaderText="" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Address" HeaderText="Address" HeaderStyle-Wrap="False" HeaderStyle-HorizontalAlign="Left">
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Add2_blank" HeaderText="Space #" HeaderStyle-Wrap="False" HeaderStyle-HorizontalAlign="Left">
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="City" HeaderText="City" HeaderStyle-Wrap="False" HeaderStyle-HorizontalAlign="Left">
										<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="ST" HeaderText="ST" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Zip" HeaderText="Zip" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Sq_Ft" HeaderText="Sq.Ft." HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Date_Open" HeaderText="Date_Open" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Simon" HeaderText="Simon" HeaderStyle-Wrap="False">
										<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
							</asp:datagrid></div>
					</td>
				</tr>
			</table>
			<br>
			<table width="100%">
				<tr>
					<td>
						<hr>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td colSpan="3"><asp:label id="lblError" Width="329px" Visible="False" Runat="server" text=""></asp:label></td>
							</tr>
							<tr>
								<td width="50" align="left">&nbsp;&nbsp;&nbsp;&nbsp;<asp:button id="btnExport" runat="server" Text="Export to Excel" CssClass="b1"></asp:button></td>
								<td align="middle"><asp:button id="btnReturn" runat="server" Text="Return" CssClass="b1"></asp:button>&nbsp;&nbsp;<asp:button id="btnHome" runat="server" Text="Home" CssClass="B1"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
		<script language="javascript">
		<!--
			document.forms[0].btnExport.focus();	
		//-->
		</script>
	</body>
</HTML>
