Imports System.Data
Imports System.Data.OleDb
Imports System.Web.Security

Public Class Login
    Inherits System.Web.UI.Page
    Protected WithEvents txtEmployeeId As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents trEmpId As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents ucHeader As Header
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl

    Private strCurrentPage As String
    Private objUserInfo As New UserInfo()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()

        pageBody.Attributes.Add("onload", "")

        ucHeader.lblTitle = "Employee Login"

        SetButtons()

        'strCurrentPage = Request("CP")

        If txtEmployeeId.Text.Length > 0 Then
            Login()
        End If

    End Sub

    Private Sub SetButtons()

        Dim strTemp As String

        strTemp = Request("ReturnURL")

        strTemp = UCase(strTemp)

        If Len(strTemp) > 0 Then

            If strTemp.IndexOf("IC_PROFILELIST.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_SEARCH.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_DCMAINT.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_PROFILEADMIN.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_COMMENT.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_EDITTRANSFER.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_NAMEPROFILE.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_PRINTDISCREPANCY.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_PRINTSEARCHRESULTS.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_PRINTSEARCHRESULTSTT.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_RESHIP.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_RESULTS.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_RESULTSTT.ASPX") <> -1 Or _
                strTemp.IndexOf("IC_VOIDCONFIRM.ASPX") <> -1 Then

                ucHeader.CurrentMode(Header.HeaderGroup.IC)
            Else
                ucHeader.CurrentMode(Header.HeaderGroup.Transfer)

            End If



        Else
            If Request("Mode") = 1 Then
                ucHeader.CurrentMode(Header.HeaderGroup.Transfer)
            Else
                ucHeader.CurrentMode(Header.HeaderGroup.IC)
            End If
        End If

    End Sub

    Private Sub Login()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objSQLDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        strStoredProcedure = "spGetEmployeeInfo '" & txtEmployeeId.Text.PadLeft(5, "0"c) & "' "

        Dim objACommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        objSQLDataReader = objACommand.ExecuteReader()

        'Dim strSQL As String
        'Dim objORACommand As New OleDbCommand()
        'Dim objORAConnection As New OleDbConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        'Dim objORADataReader As OleDbDataReader
        'Dim objSQLDataReader As SqlClient.SqlDataReader

        'objORAConnection.Open()

        'strSQL = "SELECT U.EMPLOYEEID, U.NAMEFIRST, U.NAMELAST, U.JOBCODE "
        'strSQL = strSQL & "FROM TAULTIMATEEMPLOYEE U "
        'strSQL = strSQL & "WHERE U.EMPLOYEEID = LPAD('" & txtEmployeeId.Text & "',5,0) "

        'objORACommand.Connection = objORAConnection
        'objORACommand.CommandText = strSQL
        'objORADataReader = objORACommand.ExecuteReader()

        If objSQLDataReader.Read Then

            If IsDBNull(objSQLDataReader("Emp_Id")) Then

                objUserInfo.SetEmployeeInfo(txtEmployeeId.Text, objSQLDataReader("NAMEFIRST"), objSQLDataReader("NAMELAST"), objSQLDataReader("JOBCODE"), objSQLDataReader("DEPT"))
                objUserInfo.LogOn()
                objConnection.Close()
                FormsAuthentication.RedirectFromLoginPage(txtEmployeeId.Text, False)

            Else
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("You cannot have more than one session open per employee ID. Login has failed.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
                'lblError.Text = "You cannot have more than one session open per employee ID. Login has failed."

            End If

            'Response.Redirect(strCurrentPage)
        Else
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Employee number not found. Please try again.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            'lblError.Text = "Employee number not found. Please try again."
        End If

        objSQLDataReader.Close()
        objACommand.Dispose()
        objConnection.Close()

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Login()

    End Sub

End Class
