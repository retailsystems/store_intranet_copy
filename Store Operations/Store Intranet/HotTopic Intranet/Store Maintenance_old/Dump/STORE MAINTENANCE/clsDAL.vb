'***********************************
'   Project: Store Maintenance
'   Author: 
'   File Name: clsDAL.vb
'   Description:
'       Connects and calls Stored Procedures
'   Last Modified:
'       6-21-2005 : Matthew Dinh : Updated the delete section
'***********************************
Option Explicit On 

Imports System.Data.SqlClient

Public Class clsDAL

    Private blnScript As Boolean
    Private objSqlCommand As SqlCommand
    Private mstrDatabase As String
    Private strExecute As String

    Private Function cnnConnection() As SqlConnection

        cnnConnection = New SqlConnection(ConfigurationSettings.AppSettings(mstrDatabase))

    End Function

    Private Function Execute() As String

        Dim intRowsAffected As Integer

        'Try
            With objSqlCommand
                .Connection = cnnConnection()
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
            End With
        ' Catch
        'Return "Error #" & Err.Number & ": " & Err.Description
        'End Try

    End Function


    Public Function ExecuteSQL(ByVal vstrSQL As String, Optional ByVal vstrDatabase As String = "strStoreSQLConn1") As String

        objSqlCommand = New SqlCommand
        mstrDatabase = vstrDatabase

        With objSqlCommand
            .CommandText = vstrSQL
            .CommandType = CommandType.Text
        End With

        Return Execute()

    End Function

    Private Function GetDataTable() As DataTable

        Dim objSqlDataAdapter As SqlDataAdapter
        Dim objDataTable As New DataTable

        objSqlCommand.Connection = cnnConnection()

        objSqlDataAdapter = New SqlDataAdapter(objSqlCommand)
        objSqlDataAdapter.Fill(objDataTable)

        Return objDataTable

    End Function

    Public Function SelectSQL(ByVal vstrSQL As String, Optional ByVal vstrDatabase As String = "strStoreSQLConn1") As DataTable

        objSqlCommand = New SqlCommand
        mstrDatabase = vstrDatabase

        With objSqlCommand
            .CommandText = vstrSQL
            .CommandType = CommandType.Text
        End With

        Return GetDataTable()

    End Function

    Public Function CountDeletedStore(ByVal gStoNum As Long, ByRef strCount As Long, ByVal vstrDatabase As String) As String

        objSqlCommand = New SqlCommand
        mstrDatabase = vstrDatabase

        objSqlCommand.CommandText = "spCountDeletedStore"
        objSqlCommand.CommandType = CommandType.StoredProcedure

        With objSqlCommand.Parameters
            .Add("@StoNum", gStoNum)
            .Add(New SqlParameter("@Count", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, strCount))
        End With

        strExecute = Execute()

        With objSqlCommand
            strCount = .Parameters("@Count").Value
        End With

        Return strExecute

    End Function

    Public Function CountStore(ByVal gStoNum As Long, ByRef strCount As Long, ByVal vstrDatabase As String) As String

        objSqlCommand = New SqlCommand
        mstrDatabase = vstrDatabase

        objSqlCommand.CommandText = "spCountStore"
        objSqlCommand.CommandType = CommandType.StoredProcedure

        With objSqlCommand.Parameters
            .Add("@StoNum", gStoNum)
            .Add(New SqlParameter("@Count", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, strCount))
        End With

        strExecute = Execute()

        With objSqlCommand
            strCount = .Parameters("@Count").Value
        End With

        Return strExecute

    End Function

    Public Function CountShipperNumber(ByVal gStoNum As Long, ByVal gShipperNumber As String, ByRef strCount As Long, ByVal vstrDatabase As String) As String

        objSqlCommand = New SqlCommand
        mstrDatabase = vstrDatabase

        objSqlCommand.CommandText = "spCountShipperNumber"
        objSqlCommand.CommandType = CommandType.StoredProcedure

        With objSqlCommand.Parameters
            .Add("@StoNum", gStoNum)
            .Add("@ShipperNumber", gShipperNumber)
            .Add(New SqlParameter("@Count", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, strCount))
        End With

        strExecute = Execute()

        With objSqlCommand
            strCount = .Parameters("@Count").Value
        End With

        Return strExecute

    End Function

    Public Function DeleteStore(ByVal StoNumF As Long, ByVal gEmpID As Long, ByVal vstrDatabase As String) As String

        objSqlCommand = New SqlCommand
        mstrDatabase = vstrDatabase

        objSqlCommand.CommandText = "spDeleteStore"
        objSqlCommand.CommandType = CommandType.StoredProcedure

        With objSqlCommand.Parameters
            .Add("@StoreNum", StoNumF)
            .Add("@Modified_ID", gEmpID)
        End With

        Return Execute()

    End Function

    Public Function UpdateDefaultStageData(ByVal eSSName As String, _
                                ByVal eStagingStore As Boolean, _
                                ByVal eAddress1 As String, _
                                ByVal eAddress2 As String, _
                                ByVal eCity As String, _
                                ByVal eState As String, _
                                ByVal eZip As String, _
                                ByVal eCountry As String, _
                                ByVal ePhone1 As String, _
                                ByVal ePhone2 As String, _
                                ByVal eShipperNumber As String, _
                                ByVal eMyUPSID As Integer, _
                                ByVal eServiceDC As String, _
                                ByVal eWan As Boolean, _
                                ByVal eCD As Boolean, _
                                ByVal vstrDatabase As String) As String

        objSqlCommand = New SqlCommand
        mstrDatabase = vstrDatabase

        objSqlCommand.CommandText = "spUpdateDefaultStageData"
        objSqlCommand.CommandType = CommandType.StoredProcedure

        With objSqlCommand.Parameters
            .Add("@SSName", eSSName)
            .Add("@StagingStore", eStagingStore)
            .Add("@Address1", eAddress1)
            .Add("@Address2", eAddress2)
            .Add("@City", eCity)
            .Add("@State", eState)
            .Add("@Zip", eZip)
            .Add("@Country", eCountry)
            .Add("@Phone1", ePhone1)
            .Add("@Phone2", ePhone2)
            .Add("@ShipperNumber", eShipperNumber)
            .Add("@MyUPSID", eMyUPSID)
            .Add("@ServiceDC", eServiceDC)
            .Add("@Wan", eWan)
            .Add("@CD", eCD)

        End With

        Return Execute()

    End Function

    Public Function FindStoreData(ByVal StoreID As Integer, ByRef SIDOut As Integer, ByRef SNameOut As String, ByRef SAddrOut As String, ByRef SAddrOut2 As String, ByRef SAddrOut3 As String, ByRef SCityOut As String, ByRef SStateOut As String, ByRef SZipOut As String, ByRef SPhone1Out As String, ByRef SPhone2Out As String, ByRef SEmailOut As String, ByRef SOpenOut As Date, ByRef SDimOut As Integer, ByRef SRegionOut As String, ByRef SDistrictOut As String, ByRef SMgrOut As String, ByRef SAM1Out As String, ByRef SAM2Out As String, ByRef SAM3Out As String, ByRef SAM4Out As String, ByRef SAM5Out As String, ByRef STagsOut As String, ByRef SSimonOut As String, ByRef SShipNumbOut As String, ByRef SCoNameOut As String, ByRef SCDOut As Boolean, ByRef SServiceDCOut As String, ByRef SStagingStoreOut As Boolean, ByRef SMyUPSIDOut As Integer, ByRef SWan As Boolean, ByRef SSubnet As String, ByRef SCountry As String, ByVal vstrDatabase As String) As String

        objSqlCommand = New SqlCommand
        mstrDatabase = vstrDatabase

        objSqlCommand.CommandText = "spFindStoreData"
        objSqlCommand.CommandType = CommandType.StoredProcedure

        With objSqlCommand.Parameters
            .Add("@StoreID", StoreID)
            .Add(New SqlParameter("@SIDOut", SqlDbType.SmallInt, 0, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SIDOut))
            .Add(New SqlParameter("@SNameOut", SqlDbType.VarChar, 50, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SNameOut))
            .Add(New SqlParameter("@SAddrOut", SqlDbType.VarChar, 105, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SAddrOut))
            .Add(New SqlParameter("@SAddrOut2", SqlDbType.VarChar, 35, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SAddrOut2))
            .Add(New SqlParameter("@SAddrOut3", SqlDbType.VarChar, 35, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SAddrOut3))
            .Add(New SqlParameter("@SCityOut", SqlDbType.VarChar, 35, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SCityOut))
            .Add(New SqlParameter("@SStateOut", SqlDbType.Char, 2, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SStateOut))
            .Add(New SqlParameter("@SZipOut", SqlDbType.VarChar, 5, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SZipOut))
            .Add(New SqlParameter("@SPhone1Out", SqlDbType.VarChar, 15, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SPhone1Out))
            .Add(New SqlParameter("@SPhone2Out", SqlDbType.VarChar, 15, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SPhone2Out))
            .Add(New SqlParameter("@SEmailOut", SqlDbType.VarChar, 40, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SEmailOut))
            .Add(New SqlParameter("@SOpenOut", SqlDbType.DateTime, 0, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SOpenOut))
            .Add(New SqlParameter("@SDimOut", SqlDbType.SmallInt, 0, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SDimOut))
            .Add(New SqlParameter("@SRegionOut", SqlDbType.VarChar, 3, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SRegionOut))
            .Add(New SqlParameter("@SDistrictOut", SqlDbType.VarChar, 4, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SDistrictOut))
            .Add(New SqlParameter("@SMgrOut", SqlDbType.VarChar, 50, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SMgrOut))
            .Add(New SqlParameter("@SAM1Out", SqlDbType.VarChar, 50, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SAM1Out))
            .Add(New SqlParameter("@SAM2Out", SqlDbType.VarChar, 50, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SAM2Out))
            .Add(New SqlParameter("@SAM3Out", SqlDbType.VarChar, 50, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SAM3Out))
            .Add(New SqlParameter("@SAM4Out", SqlDbType.VarChar, 50, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SAM4Out))
            .Add(New SqlParameter("@SAM5Out", SqlDbType.VarChar, 50, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SAM5Out))
            .Add(New SqlParameter("@STagsOut", SqlDbType.VarChar, 5, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, STagsOut))
            .Add(New SqlParameter("@SSimonOut", SqlDbType.VarChar, 1, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SSimonOut))
            .Add(New SqlParameter("@SShipNumbOut", SqlDbType.VarChar, 10, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SShipNumbOut))
            .Add(New SqlParameter("@SCoNameOut", SqlDbType.VarChar, 25, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SCoNameOut))
            .Add(New SqlParameter("@SCDOut", SqlDbType.Bit, 0, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SCDOut))
            .Add(New SqlParameter("@SServiceDCOut", SqlDbType.VarChar, 10, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SServiceDCOut))
            .Add(New SqlParameter("@SMyUPSIDOut", SqlDbType.Int, 4, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SMyUPSIDOut))
            .Add(New SqlParameter("@SStagingStoreOut", SqlDbType.Bit, 0, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SStagingStoreOut))
            .Add(New SqlParameter("@SWan", SqlDbType.Bit, 0, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SWan))
            .Add(New SqlParameter("@SSubnet", SqlDbType.VarChar, 15, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SSubnet))
            .Add(New SqlParameter("@SCountry", SqlDbType.Char, 2, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, SCountry))

        End With

        strExecute = Execute()

        With objSqlCommand
            SIDOut = Val(.Parameters("@SIDOut").Value & "")
            SNameOut = .Parameters("@SNameOut").Value & ""
            SAddrOut = .Parameters("@SAddrOut").Value & ""
            SAddrOut2 = .Parameters("@SAddrOut2").Value & ""
            SAddrOut3 = .Parameters("@SAddrOut3").Value & ""
            SCityOut = .Parameters("@SCityOut").Value & ""
            SStateOut = .Parameters("@SStateOut").Value & ""
            SZipOut = .Parameters("@SZipOut").Value & ""
            SPhone1Out = .Parameters("@SPhone1Out").Value & ""
            SPhone2Out = .Parameters("@SPhone2Out").Value & ""
            SEmailOut = .Parameters("@SEmailOut").Value & ""

            If IsDate(.Parameters("@SOpenOut").Value) Then
                SOpenOut = .Parameters("@SOpenOut").Value
            End If

            SDimOut = Val(.Parameters("@SDimOut").Value & "")
            SRegionOut = .Parameters("@SRegionOut").Value & ""
            SDistrictOut = .Parameters("@SDistrictOut").Value & ""
            SMgrOut = .Parameters("@SMgrOut").Value & ""
            SAM1Out = .Parameters("@SAM1Out").Value & ""
            SAM2Out = .Parameters("@SAM2Out").Value & ""
            SAM3Out = .Parameters("@SAM3Out").Value & ""
            SAM4Out = .Parameters("@SAM4Out").Value & ""
            SAM5Out = .Parameters("@SAM5Out").Value & ""
            STagsOut = .Parameters("@STagsOut").Value & ""
            SSimonOut = .Parameters("@SSimonOut").Value & ""
            SShipNumbOut = .Parameters("@SShipNumbOut").Value & ""
            SCoNameOut = .Parameters("@SCoNameOut").Value & ""

            If IsDBNull(.Parameters("@SCDOut").Value) Then
                SCDOut = False
            Else
                SCDOut = .Parameters("@SCDOut").Value
            End If
            SServiceDCOut = .Parameters("@SServiceDCOut").Value & ""
            SStagingStoreOut = .Parameters("@SStagingStoreOut").Value & ""
            SMyUPSIDOut = .Parameters("@SMyUPSIDOut").Value & ""
            SWan = .Parameters("@SWan").Value & ""
            SSubnet = .Parameters("@SSubnet").Value & ""
            SCountry = .Parameters("@SCountry").Value & ""

        End With

        Return strExecute

    End Function

    Public Function GetEligEmp(ByVal EmpID As String, ByRef Count As Long, ByVal vstrDatabase As String) As String

        objSqlCommand = New SqlCommand
        mstrDatabase = vstrDatabase

        objSqlCommand.CommandText = "spGetEligEmp"
        objSqlCommand.CommandType = CommandType.StoredProcedure

        With objSqlCommand.Parameters
            .Add("@EmpID", EmpID)
            .Add(New SqlParameter("@Count", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, Count))
        End With

        strExecute = Execute()

        Count = objSqlCommand.Parameters("@Count").Value

        Return strExecute

    End Function

    Public Function GetShipperNumber(ByVal StoreNum As Integer, ByRef ShipperNumber As String, ByVal vstrDatabase As String) As String

        objSqlCommand = New SqlCommand
        mstrDatabase = vstrDatabase

        objSqlCommand.CommandText = "spGetShipperNumber"
        objSqlCommand.CommandType = CommandType.StoredProcedure

        With objSqlCommand.Parameters
            .Add("@StoreNum", StoreNum)
            .Add(New SqlParameter("@ShipperNumber", SqlDbType.VarChar, 10, ParameterDirection.Output, False, 0, 0, "", DataRowVersion.Current, ShipperNumber))
        End With

        strExecute = Execute()

        ShipperNumber = objSqlCommand.Parameters("@ShipperNumber").Value & ""

        Return strExecute

    End Function

    Public Function InsertStoreRow(ByVal gStoNum As Integer, _
                                    ByVal gStoName As String, _
                                    ByVal gAddr As String, _
                                    ByVal gAddr2 As String, _
                                    ByVal gAddr3 As String, _
                                    ByVal gCity As String, _
                                    ByVal gState As String, _
                                    ByVal gZip As String, _
                                    ByVal gPhone1 As String, _
                                    ByVal gPhone2 As String, _
                                    ByVal gEmail As String, _
                                    ByVal gOpen As Date, _
                                    ByVal gDim As Integer, _
                                    ByVal gRegion As String, _
                                    ByVal gDist As String, _
                                    ByVal gStoMgr As String, _
                                    ByVal gAM1 As String, _
                                    ByVal gAM2 As String, _
                                    ByVal gAM3 As String, _
                                    ByVal gAM4 As String, _
                                    ByVal gAM5 As String, _
                                    ByVal gTags As String, _
                                    ByVal gSimon As String, _
                                    ByVal gCoName As String, _
                                    ByVal gShipNumb As String, _
                                    ByVal gEmpID As Long, _
                                    ByVal gCD As Boolean, _
                                    ByVal gServiceDC As String, _
                                    ByVal gStagingStore As Boolean, _
                                    ByVal gMyUPSID As Integer, _
                                    ByVal gCountry As String, _
                                    ByVal gWan As Boolean, _
                                    ByVal gSubnet As String, _
                                    ByVal vstrDatabase As String) As String

        objSqlCommand = New SqlCommand
        mstrDatabase = vstrDatabase

        objSqlCommand.CommandText = "spInsertStoreRow"
        objSqlCommand.CommandType = CommandType.StoredProcedure

        With objSqlCommand.Parameters
            .Add("@StoNum", gStoNum)
            .Add("@StoName", gStoName)
            .Add("@Addr", gAddr)
            .Add("@Addr2", gAddr2)
            .Add("@Addr3", gAddr3)
            .Add("@City", gCity)
            .Add("@State", gState)
            .Add("@Zip", gZip)
            .Add("@Phone1", gPhone1)
            .Add("@Phone2", gPhone2)
            .Add("@Email", gEmail)
            .Add("@OpenDate", gOpen)
            .Add("@Dimension", gDim)
            .Add("@Region", gRegion)
            .Add("@District", gDist)
            .Add("@Manager", gStoMgr)
            .Add("@AsstMgr1", gAM1)
            .Add("@AsstMgr2", gAM2)
            .Add("@AsstMgr3", gAM3)
            .Add("@AsstMgr4", gAM4)
            .Add("@AsstMgr5", gAM5)
            .Add("@Tags", gTags)
            .Add("@Simon", gSimon)
            .Add("@ShipNumb", gShipNumb)
            .Add("@CoName", gCoName)
            .Add("@Created_ID", gEmpID.ToString())
            .Add("@CD", gCD)
            .Add("@ServiceDC", StringToNull(gServiceDC))
            .Add("@StagingStore", gStagingStore)
            .Add("@MyUPSID", gMyUPSID)
            .Add("@Wan", gWan)
            .Add("@Subnet", gSubnet)
            .Add("@Country", gCountry)

        End With

        Return Execute()

    End Function

    Public Function UpdateRow(ByVal eStoNum As Integer, _
                                ByVal eStoName As String, _
                                ByVal eAddr As String, _
                                ByVal eAddr2 As String, _
                                ByVal eAddr3 As String, _
                                ByVal eCity As String, _
                                ByVal eState As String, _
                                ByVal eZip As String, _
                                ByVal ePhone1 As String, _
                                ByVal ePhone2 As String, _
                                ByVal eEmail As String, _
                                ByVal eOpen As Date, _
                                ByVal eDim As Integer, _
                                ByVal eRegion As String, _
                                ByVal eDist As String, _
                                ByVal eStoMgr As String, _
                                ByVal eAM1 As String, _
                                ByVal eAM2 As String, _
                                ByVal eAM3 As String, _
                                ByVal eAM4 As String, _
                                ByVal eAM5 As String, _
                                ByVal eTags As String, _
                                ByVal eSimon As String, _
                                ByVal eCoName As String, _
                                ByVal eShipNumb As String, _
                                ByVal eCD As Boolean, _
                                ByVal eServiceDC As String, _
                                ByVal gEmpID As Integer, _
                                ByVal eStagingStore As Boolean, _
                                ByVal eMyUPSID As Integer, _
                                ByVal eWan As Boolean, _
                                ByVal eSubnet As String, _
                                ByVal eCountry As String, _
                                ByVal vstrDatabase As String) As String

        objSqlCommand = New SqlCommand
        mstrDatabase = vstrDatabase

        objSqlCommand.CommandText = "spUpdateRow"
        objSqlCommand.CommandType = CommandType.StoredProcedure

        With objSqlCommand.Parameters
            .Add("@StoNum", eStoNum)
            .Add("@StoName", eStoName)
            .Add("@Addr", eAddr)
            .Add("@Addr2", eAddr2)
            .Add("@Addr3", eAddr3)
            .Add("@City", eCity)
            .Add("@State", eState)
            .Add("@Zip", eZip)
            .Add("@Phone1", ePhone1)
            .Add("@Phone2", ePhone2)
            .Add("@Email", eEmail)
            .Add("@OpenDate", eOpen)
            .Add("@Dimension", eDim)
            .Add("@Region", eRegion)
            .Add("@District", eDist)
            .Add("@Manager", eStoMgr)
            .Add("@AsstMgr1", eAM1)
            .Add("@AsstMgr2", eAM2)
            .Add("@AsstMgr3", eAM3)
            .Add("@AsstMgr4", eAM4)
            .Add("@AsstMgr5", eAM5)
            .Add("@Tags", eTags)
            .Add("@Simon", eSimon)
            .Add("@CoName", eCoName)
            .Add("@ShipNumb", eShipNumb)
            .Add("@CD", eCD)
            .Add("@ServiceDC", StringToNull(eServiceDC))
            .Add("@Modified_ID", gEmpID.ToString())
            .Add("@StagingStore", eStagingStore)
            .Add("@MyUPSID", eMyUPSID)
            .Add("@Wan", eWan)
            .Add("@Subnet", eSubnet)
            .Add("@Country", eCountry)
            .Add("@Deleted", 0)

        End With

        Return Execute()

    End Function
    '4/14/05 SRI
    Private Shared Function StringToNull(ByVal Value As String) As Object
        If Value.Trim = "" Then
            Return Convert.DBNull
        Else
            Return Value
        End If
    End Function
    Private Function Output(ByRef Value As Object, ByVal ParameterName As String, ByVal vSqlDbType As SqlDbType, Optional ByVal Size As Integer = 0) As SqlParameter

        Dim objSQLParameter As New SqlParameter

        With objSQLParameter
            .Direction = ParameterDirection.Output
            .ParameterName = ParameterName
            .SqlDbType = vSqlDbType
            .Size = Size
            .Value = Value
        End With

    End Function

    'Javascript section

    Private Sub ButtonScript(ByVal objPage As Page)

        Dim objControl As Control
        Dim objHtmlForm As HtmlForm
        Dim objHtmlGenericControl As HtmlGenericControl
        Dim strID As String

        If Not blnScript Then
            For Each objControl In objPage.Controls
                Select Case TypeName(objControl)
                    Case "HtmlForm"
                        objHtmlForm = objControl
                        objHtmlForm.Attributes.Add("onkeydown", "CatchKeyPress(window.event.keyCode, window.event.srcElement);")
                        strID = objHtmlForm.ID
                    Case "HtmlGenericControl"
                        If objControl.ID = "pageBody" Then
                            objHtmlGenericControl = objControl
                            objHtmlGenericControl.Attributes.Add("onkeydown", "CatchKeyPress(window.event.keyCode, window.event.srcElement);")
                            strID = objHtmlGenericControl.ID
                        End If
                End Select
            Next

            With objPage.Response
                .Write("<script language='javascript'> " & vbCrLf)
                .Write("function CatchKeyPress(KeyCode, Sender) { " & vbCrLf)
                .Write("var btnToBeClicked = null; " & vbCrLf)
                .Write("RemoveEnterAndEscEvents(); " & vbCrLf)
                .Write("if(KeyCode == '13') { " & vbCrLf)
                .Write("var ButtonName = Sender.getAttribute('TargetButton'); " & vbCrLf)
                .Write("btnToBeClicked = document.getElementById(ButtonName); " & vbCrLf)
                .Write("if(btnToBeClicked) { " & vbCrLf)
                .Write("btnToBeClicked.click(); " & vbCrLf)
                .Write("} " & vbCrLf)

                .Write("else {" & vbCrLf)
                .Write("Sender = document.getElementById('" & strID & "'); " & vbCrLf)
                .Write("ButtonName = Sender.getAttribute('TargetButton'); " & vbCrLf)
                .Write("btnToBeClicked = document.getElementById(ButtonName); " & vbCrLf)
                .Write("if(btnToBeClicked) { " & vbCrLf)
                .Write("btnToBeClicked.click(); " & vbCrLf)
                .Write("} " & vbCrLf)
                .Write("} " & vbCrLf)

                .Write("} " & vbCrLf)
                .Write("} " & vbCrLf)

                .Write("function RemoveEnterAndEscEvents() { " & vbCrLf)
                .Write("if (event.keyCode == 13 || event.keyCode == 27) { " & vbCrLf)
                .Write("event.cancelBubble = true; event.returnValue = false; " & vbCrLf)
                .Write("} " & vbCrLf)
                .Write("} " & vbCrLf)
                .Write("</script> " & vbCrLf)
            End With

            blnScript = True
        End If

    End Sub

    Public Sub FocusScript(ByVal objControl As Control)

        objControl.Page.RegisterStartupScript("focus", "<SCRIPT language='javascript'>document.getElementById('" & objControl.ID & "').focus() </SCRIPT>")

    End Sub

    Public Sub TextboxButton(ByVal objTextBox As TextBox, ByVal objButton As Control)

        objTextBox.Attributes.Add("TargetButton", objButton.ID)

        Call ButtonScript(objTextBox.Page)

    End Sub

    Public Sub ClearCookies(ByRef Request As HttpRequest, ByRef Response As HttpResponse)

        Dim intCookie As Integer
        Dim objCookie As Object
        Dim strName As String

        For intCookie = Request.Cookies.Count - 1 To 0 Step -1
            objCookie = Request.Cookies(intCookie)

            Select Case TypeName(objCookie)
                Case "HttpCookie"
                    strName = objCookie.Name
                Case "String"
                    strName = objCookie
                Case Else
                    strName = ""
            End Select

            Select Case strName
                Case "ASP.NET_SessionId", "StoreNo", ""
                    'Do nothing
                Case Else
                    Response.Cookies(strName).Expires = "1/1/1984"
                    Request.Cookies.Remove(strName)
            End Select
        Next

    End Sub

    Public Function GetUser(ByVal Request As HttpRequest, ByVal response As HttpResponse) As String

        Dim strName As String
        Dim strUserName As String

        If IsNothing(Request.Cookies("User")) Then
            response.Cookies.Add(New HttpCookie("User"))
        End If

        strUserName = Request.Cookies("User")("UserName")
        strName = Request.Cookies("User")("Name")

        Return IIf(strName = "" Or Not IsNumeric(strUserName), strUserName, strName)

    End Function

End Class
