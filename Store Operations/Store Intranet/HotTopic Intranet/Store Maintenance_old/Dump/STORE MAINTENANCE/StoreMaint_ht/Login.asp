<!-- #include file="dbConn.asp" -->
<!-- #include file="globalconst.asp" -->
<!-- #include file="gfunctions.asp" -->
<!-- #include file="gheader.asp" -->
<%
Response.Buffer = True
Response.Expires = 0

Dim strAction
Dim strHome

strAction = Request.QueryString("Action")
strHome = Request.QueryString("Home")

Private Sub DisplayPage()
	
	If strAction = "S" then 
		Call ValidateLogin()
	End If
	Call gShowHeader("Login")
	Call ShowLogin()
	
End Sub

Private Sub ShowLogin()
	
	If strAction = "S" then
		Response.Write "<table border='0' cellspacing='0' cellpadding='0' width='750'>"
		Response.Write "<tr><td><font color='white'>Login attempt failed.  Please make sure the LoginID and Password are correct.</font></td></tr>"
		Response.Write "<tr><td>&nbsp;</td></tr>"
		Response.Write "</table>"
	End If
	Response.Write "<table border='0' cellspacing='0' cellpadding='0' width='750'>"
	Response.Write "<tr><td>&nbsp;</td></tr>"
	Response.Write "<tr><td width='30%'>&nbsp;</td><td width='15%'><font size='2' face='" & gcMainFontFace & "' color='white'><b>Login ID:</b></font></td><td width='24%'><input type='text' name='txtLoginID' value=''></td><td>&nbsp;</td></tr>"
	Response.Write "<tr><td>&nbsp;</td><td><font size='2' face='" & gcMainFontFace & "' color='white'><b>Password:</b></font></td><td><input type='password' name='txtPassword' value=''></td><td>&nbsp;</td></tr>"
	Response.Write "<tr><td>&nbsp;</td></tr>"
	Response.Write "<tr><td><hr color='" & gcPageLineColor & "'></td><td><hr color='" & gcPageLineColor & "'></td><td><hr color='" & gcPageLineColor & "'></td><td><hr color='" & gcPageLineColor & "'></td></tr>"
	Response.Write "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td align='right'><input class='B1' type='Submit' name='cmdSubmit' value='Submit' onClick='submitLogin();'>&nbsp;&nbsp;<input class='B1' type='button' name='Home' value='Home' onClick=""document.location='Home.asp';""></td></tr>"
	Response.Write "</table>"
	
End Sub

Private Sub ValidateLogin()
	
	Dim strLoginID
	Dim strPassword
	Dim strSQL
	Dim rstLogin
	
	strLoginID = Request.Form("txtLoginID")
	strPassword = Request.Form("txtPassword")
	
	strSQL = "Select * from User_Mst where User_Login = '" & strLoginID & "' and Password = '" & strPassword & "'"
	Set rstLogin = Server.CreateObject("ADODB.Recordset")
	Set rstLogin = cnnConnection.Execute(strSQL)

	If not rstLogin.EOF then
		Response.Cookies("User")("UserName") = Request.Form("txtLoginID")
		Response.Cookies("User")("ID") = rstLogin("User_ID")
		Response.Cookies("User")("Store") = Trim(rstLogin("Store") & "")
		Response.Cookies("User")("Home") = strHome
		
		'  get the securities that match with the users Job_Class
		strSQL = "Select * from Security where Job_Class = '" & Trim(rstLogin.Fields("Job_Class")) & "'"
		Set rstLogin = cnnConnection.Execute(strSQL)
		
		While not rstLogin.EOF
			Response.Cookies("User")("" & Trim(rstLogin.Fields("Function")) & "") = Trim(rstLogin.Fields("Access"))
			rstLogin.moveNext
		Wend
		'get the securities that match with the user_ID
		strSQL = "Select * from Security where User_ID = '" & Request.Cookies("User")("ID") & "'"
		Set rstLogin = cnnConnection.Execute(strSQL)
		While not rstLogin.EOF
			Response.Cookies("User")("" & Trim(rstLogin.Fields("Function")) & "") = Trim(rstLogin.Fields("Access"))
			rstLogin.moveNext
		Wend
		'Response.Write Request.Cookies("User")("EFORMSAdmin")
		Set rstLogin = Nothing
		Response.Redirect "Home.asp"
	End If

End Sub


%>


<html>
<head>
	<title>Login</title>
	
<script language='Javascript'>

function submitLogin(){
	
	if(validate()){
		document.frmLogin.method="post"
		document.frmLogin.action="Login.asp?Action=S&Home=<%=strHome%>"
		document.frmLogin.submit();
	}

}

function validate(){
	
	if(document.frmLogin.txtLoginID.value==""){
		alert("Please enter a Login ID.");
		return false;
	}else{
		if(document.frmLogin.txtPassword.value==""){
			alert("Please enter a Password.");
			return false;
		}else{
			return true;
		}
	}
}

function setFocus(strAction){
	if(strAction==""){
		document.frmLogin.txtLoginID.focus();
	}
}

</script>

<%Call gGetStyles()%>

</head>

<body bgcolor='<%=gcBGColor%>' bgproperties='fixed' onLoad="setFocus('<%=Request.QueryString("Action")%>')">
<form action='Login.asp' method='POST' name='frmLogin'>
	<%Call DisplayPage%>
</form>
</body>
</html>
