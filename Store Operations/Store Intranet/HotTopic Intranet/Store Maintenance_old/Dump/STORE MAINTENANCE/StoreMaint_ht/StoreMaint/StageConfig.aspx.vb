'***********************************
'   Project: Store Maintenance
'   Author: 
'   File Name: StageConfig.aspx.vb
'   Description:
'       Codebehind for the StageConfig.aspx
'   Last Modified:
'       6-21-2005 : Matthew Dinh : Created the FormValidation Function to validate form.
'       
'***********************************
Option Explicit On 
'Imports System.Data.SqlClient

Partial Class StageConfig
    Inherits System.Web.UI.Page







    Protected WithEvents btnClear As System.Web.UI.WebControls.Button
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub


#End Region

    Dim objDAL As New clsDAL
    Dim objStoreDetail As StoreDetail
    Dim objDataTable As New DataTable
    Dim pCoNameChoice As Integer

    'parameters
    Dim pStageConfigName As String
    Dim pAddress1 As String
    Dim pAddress2 As String
    Dim pCity As String
    Dim pState As String
    Dim pZip As String
    Dim pCountry As String
    Dim pPhone1 As String
    Dim pPhone2 As String

    Dim pStaging As Boolean
    Dim pWan As Boolean
    Dim pCD As Boolean

    Dim pServiceDC As String

    Dim pShipperNumber As String
    Dim pMyUPSID As Integer


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pageBody.Attributes.Add("onload", "")

        Dim strUser As String = objDAL.GetUser(Request, Response)
        'Dim temp_date As String
        'Dim cn As SqlConnection
        'Dim cm As SqlCommand
        'Dim dr As SqlDataReader
        Dim strSQL As String
        'cboCoName.Enabled = False

        If Trim(strUser) = "" Then
            Session("pEmployee") = 0
        End If

        If Session("pEmployee") <= 0 Then
            'Response.Redirect("Login_Store.aspx")
            Response.Redirect("/home/home.aspx")
        End If

        'pCoNameChoice = Request.QueryString("pCoNameChoice")
        pCoNameChoice = Session("sCoNameChoice")

        If Not IsPostBack Then

            Select Case pCoNameChoice
                Case 1, 0
                    cboCoName.SelectedValue = 1
                Case 2
                    cboCoName.SelectedValue = 2
                Case Else
                    cboCoName.SelectedValue = 1
            End Select

            If pCoNameChoice = 2 Then
                lblCD.Visible = False
                chkCD.Visible = False
            End If
            Call GetDefaultStage()
        End If



        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=" & strUser & "&title=Default Staging Store Configuration")

    End Sub

    Public Function strConnHeaderOpen() As String

        'connection
        Select Case pCoNameChoice
            Case 1, 0
                Return "strStoreSQLConn1"
            Case 2
                Return "strStoreSQLConn2"
        End Select

    End Function

    Public Sub GetDefaultStage()

        'commands
        Dim CommandStateString As String = "SELECT DISTINCT(State) FROM STATE where state > '' "

        'Get the one table from the dataset
        Dim dataTable As DataTable = objDAL.SelectSQL(CommandStateString, strConnHeaderOpen)

        cboState.DataSource = dataTable.DefaultView
        cboState.DataTextField = "State"
        cboState.DataValueField = "State"
        cboState.DataBind()
        cboState.Items.Insert(0, "")

        Dim CommandStagingString As String = "SELECT * FROM STAGING_STORE WHERE StageID = 1"

        'Get the one table from the dataset
        Dim dtStagingData As DataTable = objDAL.SelectSQL(CommandStagingString, strConnHeaderOpen)

        txtDefaultName.Text = dtStagingData.Rows(0).Item("SSName")
        txtAddr.Text = dtStagingData.Rows(0).Item("Address1")
        txtAddr2.Text = dtStagingData.Rows(0).Item("Address2")
        txtCity.Text = dtStagingData.Rows(0).Item("City")
        cboState.SelectedValue = dtStagingData.Rows(0).Item("State").ToString()
        txtZip.Text = dtStagingData.Rows(0).Item("zip")
        txtCountry.Text = dtStagingData.Rows(0).Item("Country")
        txtPhone1.Text = dtStagingData.Rows(0).Item("Phone1")
        txtPhone2.Text = dtStagingData.Rows(0).Item("Phone2")


        If dtStagingData.Rows(0).Item("StagingStore") Then
            cbStaging.Checked = True
        Else
            cbStaging.Checked = False
        End If

        If dtStagingData.Rows(0).Item("CD") Then
            chkCD.Checked = True
        Else
            chkCD.Checked = False
        End If

        If dtStagingData.Rows(0).Item("wan") Then
            cboWan.Checked = True
        Else
            cboWan.Checked = False
        End If

        lstServiceDC.SelectedValue = dtStagingData.Rows(0).Item("Service_DC").ToString()
        txtShipperNumber.Text = dtStagingData.Rows(0).Item("ShipperNumber")
        txtMyUPSID.Text = dtStagingData.Rows(0).Item("MyUPS_ID")

    End Sub

    Function UpdateDefaultStage() As Boolean

        Try
            Call objDAL.UpdateDefaultStageData(txtDefaultName.Text, _
                                           cbStaging.Checked, _
                                           txtAddr.Text, _
                                           txtAddr2.Text, _
                                           txtCity.Text, _
                                           cboState.SelectedItem.Text, _
                                           txtZip.Text, _
                                           txtCountry.Text, _
                                           txtPhone1.Text, _
                                           txtPhone2.Text, _
                                           txtShipperNumber.Text, _
                                           txtMyUPSID.Text, _
                                           lstServiceDC.SelectedItem.Value, _
                                           cboWan.Checked, _
                                           chkCD.Checked, _
                                           strConnHeaderOpen)
        Catch ex As Exception
            'Return False
            Throw ex
        End Try
        Return True

    End Function

    Function FormValidation() As Boolean
        Dim strOnLoad As String

        If Trim(txtDefaultName.Text) = "" Then
            strOnLoad = Server.UrlEncode("Default Name or Type is required.  Please check your input.") & "','Error'"
            Call objDAL.FocusScript(txtDefaultName)
        ElseIf Trim(txtAddr.Text) = "" Then
            strOnLoad = Server.UrlEncode("Address 1 is required.  Please check your input.") & "','Error'"
            Call objDAL.FocusScript(txtAddr)
        ElseIf Trim(cboCoName.SelectedItem.Text) = "" Then
            strOnLoad = Server.UrlEncode("Company Name is required. Please check your input.") & "','Error'"
            Call objDAL.FocusScript(cboCoName)
        ElseIf Trim(txtCity.Text) = "" Then
            strOnLoad = Server.UrlEncode("City is required. Please check your input.") & "','Error'"
            Call objDAL.FocusScript(txtCity)
        ElseIf Trim(cboState.SelectedValue) = "" Then
            strOnLoad = Server.UrlEncode("State is required. Please select a state.") & "','Error'"
            Call objDAL.FocusScript(cboState)
        ElseIf Not IsNumeric(txtZip.Text) Or txtZip.Text.Length < 5 Then
            strOnLoad = Server.UrlEncode("Please enter a valid Zip code.") & "','Error'"
            Call objDAL.FocusScript(txtZip)
        ElseIf Trim(txtCountry.Text) = "" Or Not System.Text.RegularExpressions.Regex.IsMatch(txtCountry.Text, _
           "^([a-zA-z\s]{2,})$") Then
            strOnLoad = Server.UrlEncode("Please enter a valid Country.") & "','Error'"
            Call objDAL.FocusScript(txtCountry)
        ElseIf Not Trim(txtPhone1.Text) = "" And Not System.Text.RegularExpressions.Regex.IsMatch(txtPhone1.Text, _
   "^*\(\w+([-+.]\w+)*\)\w+([-.]\w+)*\-\w+([-.]\w+)*$") Then
            strOnLoad = Server.UrlEncode("Please enter a valid Phone Number.") & "','Error'"
            Call objDAL.FocusScript(txtPhone1)
        ElseIf Not Trim(txtPhone2.Text) = "" And Not System.Text.RegularExpressions.Regex.IsMatch(txtPhone2.Text, _
"^*\(\w+([-+.]\w+)*\)\w+([-.]\w+)*\-\w+([-.]\w+)*$") Then
            strOnLoad = Server.UrlEncode("Please enter a valid Phone Number.") & "','Error'"
            Call objDAL.FocusScript(txtPhone2)
        ElseIf Trim(txtShipperNumber.Text) = "" Then
            strOnLoad = Server.UrlEncode("Shipper Number is required. Please check your input.") & "','Error'"
            Call objDAL.FocusScript(cboCoName)
        ElseIf Trim(txtMyUPSID.Text) = "" Then
            strOnLoad = Server.UrlEncode("MyUPS ID is required. Please check your input.") & "','Error'"
            Call objDAL.FocusScript(txtMyUPSID)
        ElseIf Not Trim(txtMyUPSID.Text) = "" And Not IsNumeric(txtMyUPSID.Text) Then
            strOnLoad = Server.UrlEncode("MyUPS ID must be an numeric value. Please check your input.") & "','Error'"
            Call objDAL.FocusScript(txtMyUPSID)
            'ElseIf Not IsNumeric(txtZip.Text) Then
            'strOnLoad = Server.UrlEncode("Zip Code must be numeric.") & "','Error'"
            'Call objDAL.FocusScript(txtZip)
        Else
            Return True
        End If

        pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & strOnLoad & ",'center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
    End Function

    Private Sub btnModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModify.Click

        'check if all inputs are valid 
        If FormValidation() Then
            If UpdateDefaultStage() Then
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("This record has been successfully updated!") & "&Title=" & Server.UrlEncode("Success!") & "','Success','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            Else
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Error in Updating!") & "&Title=" & Server.UrlEncode("Error!") & "','Success','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            End If

        End If

    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click

        'If btnModify.Text = "Add" Then
        Response.Redirect("StoreDetail.aspx?pCoNameChoice=" & pCoNameChoice & "")
        'Else
        '    Response.Write("<script languague=javascript>window.close()</script>")
        'End If

    End Sub

    Private Sub cboCoName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCoName.SelectedIndexChanged
        Session("sCoNameChoice") = cboCoName.SelectedValue
        Response.Redirect("StageConfig.aspx")
        'Call GetDefaultStage()

    End Sub
End Class
