Public Class NewTransferForm
    Inherits System.Web.UI.Page
    Protected WithEvents btnScanItem As System.Web.UI.WebControls.Button
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents layoutHeader As Header
    Protected WithEvents ucTransferBox As TransferBox
    Protected WithEvents ucScanItem As ScanItem
    Protected WithEvents btnEdit As System.Web.UI.WebControls.Button
    Protected WithEvents btnUpdate As System.Web.UI.WebControls.Button
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents btnVoid As System.Web.UI.WebControls.Button
    Protected WithEvents ucTransferHeader As TransferHeader
    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnCancelTransfer As System.Web.UI.WebControls.Button

    Private objUserInfo As New UserInfo()
    Private objTransferLock As New TransferLock()

    Private m_TType As Int32

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objUserInfo.GetEmployeeInfo()

        pageBody.Attributes.Add("onload", "")
        btnCancelTransfer.Attributes.Add("onclick", "return confirm('Are you sure you want to cancel the current transfer?')")

        layoutHeader.CurrentMode(Header.HeaderGroup.Transfer)
        layoutHeader.CurrentPage(Header.PageName.NewTransfer)
        layoutHeader.DisableButtons()

        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyReq
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyShipped_Edit
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyReceived_Edit
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyReceived

        m_TType = Request.QueryString("TType")

        ucScanItem.SetForm = "SENDING"
        ucScanItem.SetTType = m_TType
        ucScanItem.CustomerInfoVisible = True
        ucTransferHeader.TType = m_TType

        If m_TType = 2 Then
            layoutHeader.lblTitle = "Return to DC/HQ"
        Else
            layoutHeader.lblTitle = "Store to Store"
        End If

        If Not Page.IsPostBack Then

            If Len(Request("BID")) > 0 Then
                ucTransferHeader.BoxId = Request("BID")
                ucTransferBox.Box_Id = Request("BID")
                ucTransferHeader.SStore = objUserInfo.StoreNumber
                ucTransferHeader.EditRStore = True
                ucTransferHeader.LoadData()

                'Redirect if status has changed
                If ucTransferHeader.BoxStatus <> 3 And ucTransferHeader.BoxStatus <> 6 Then
                    Response.Redirect("ViewTransfer.aspx?BID=" & Request("BID") & "&RET=default.aspx")
                End If

                If Not objTransferLock.LockTransfer(Request("BID"), objUserInfo.EmpId, Session.SessionID) Then
                    Response.Redirect("ViewTransfer.aspx?BID=" & Request("BID") & "&RET=default.aspx")
                End If

            Else
                ucTransferHeader.CreateNewHeader(objUserInfo.StoreNumber, 1, objUserInfo.EmpId, objUserInfo.EmpFullName)
                ucTransferBox.Box_Id = ucTransferHeader.BoxId
                ucTransferHeader.EditRStore = True
                ucTransferHeader.LoadData()
            End If

            'ucTransferHeader.EditRStore = True
            ' ucTransferHeader.LoadData()

        End If

        ucTransferBox.LoadData()
        'If Len(Request("BID")) > 0 Then

        ' ucTransferHeader.BoxId = Request("BID")
        '   ucTransferHeader.LoadData()

        'Redirect if status has changed
        '  If ucTransferHeader.BoxStatus <> 3 And ucTransferHeader.BoxStatus <> 6 Then
        '         Response.Redirect("ViewTransfer.aspx?BID=" & Request("BID"))
        '     End If

        '        If Not objTransferLock.LockTransfer(Request("BID"), objUserInfo.EmpId, Session.SessionID) Then
        '           Response.Redirect("ViewTransfer.aspx?BID=" & Request("BID"))
        '       End If

        '  End If

        If ucTransferHeader.BoxStatus = 6 And _
        (objUserInfo.IsStoreManager Or objUserInfo.IsFullTimeAssistantManager _
         Or objUserInfo.IsPartTimeAssistantManager Or objUserInfo.IsDistrictManager) _
         And Len(Request("BID")) > 0 Then

            btnVoid.Visible = True
        End If

        If ViewState("Edit") = True Then

            ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyShipped
            ucTransferBox.ShowColumn = TransferBox.BoxColumnName.QtyShipped_Edit

            ucScanItem.EnableScan = False

            btnSave.Visible = False
            btnSubmit.Visible = False
            btnEdit.Visible = False
            btnCancelTransfer.Visible = False

            btnUpdate.Visible = True
            btnCancel.Visible = True

        End If

    End Sub

    Private Sub btnVoid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoid.Click

        ucTransferHeader.EmpId = objUserInfo.EmpId
        ucTransferHeader.EmpFullName = objUserInfo.EmpFullName
        ucTransferHeader.BoxStatus = 7
        ucTransferHeader.Save()
        Response.Redirect("default.aspx")

    End Sub

    Sub ucScanItem_OnReceivedSKU(ByVal SKU_Num As String, ByVal Xfer_Transfer_Cd As Byte, ByVal Xfer_Transfer_Desc As String, ByVal Xfer_Cust_Id As Int32) Handles ucScanItem.OnReceivedSKU

        If Xfer_Transfer_Cd <> 0 Then

            If Xfer_Transfer_Cd = 3 And Xfer_Cust_Id = 0 Then
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Customer Name\Phone Required.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            Else
                ucTransferBox.Box_Id = ucTransferHeader.BoxId
                ucTransferBox.AddItemToBox(Xfer_Transfer_Cd, SKU_Num, Xfer_Cust_Id, 0, Xfer_Transfer_Desc)
                ucTransferBox.RebindData()
            End If

        Else
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please select a Transfer Type.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
        End If
        'Response.Redirect("NewTransferForm.aspx?TType=" & Request.QueryString("TType") & "&BID=" & ucTransferHeader.BoxId & "&RStore=" & ucTransferHeader.RStore)

    End Sub

    Sub ucTransferBox_SKU(ByVal SKUNumber As String, ByVal ErrNumber As Byte, ByVal ErrDesc As String) Handles ucTransferBox.InValidSKU

        pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(ErrDesc) & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If IsPageVaild() Then
            ucTransferHeader.BoxStatus = 6 'Saved Transfer
            ucTransferHeader.SStore = objUserInfo.StoreNumber
            ucTransferHeader.EmpId = objUserInfo.EmpId
            ucTransferHeader.EmpFullName = objUserInfo.EmpFullName
            ucTransferHeader.Save()
            ucTransferBox.Box_Id = ucTransferHeader.BoxId
            ucTransferBox.Save()
            ucTransferBox.InsertICStatus()
            ucTransferBox.ClearDataset()
            'objTransferLock.UnlockTransfer(ucTransferHeader.BoxId)
            Response.Redirect("CreateNewTrans.aspx")
        End If

    End Sub

    Private Function IsPageVaild() As Boolean

        Dim booValid As Boolean = True
        Dim strErrorMsg As String

        If ucTransferHeader.RStore = 0 Then
            booValid = False
            strErrorMsg = "Receiving Store Required."
        End If

        If (Not ucTransferHeader.IsMerchandiseSeleted And ucTransferBox.ItemCount > 0) Then
            booValid = False
            strErrorMsg = strErrorMsg & "<br>Merchandise checkbox not checked."
            If Not ValidAmt() Then
                booValid = False
                strErrorMsg = strErrorMsg & "<br>" & "Total Amount can not exceed $" & ConfigurationSettings.AppSettings("mnyMaxTransferAmt")
            End If
        End If

        If (ucTransferHeader.IsMerchandiseSeleted And ucTransferBox.ItemCount <= 0) Then
            booValid = False
            strErrorMsg = strErrorMsg & "<br>Merchandise not included."
        End If

        If ucTransferHeader.IsBoxIncludesEmpty And ucTransferBox.ItemCount <= 0 Then
            booValid = False
            strErrorMsg = strErrorMsg & "<br>Box Includes is required."
        End If

        If Not booValid Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode(strErrorMsg) & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
        End If

        Return booValid

    End Function

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If IsPageVaild() Then
            ucTransferHeader.BoxStatus = 6 'Packing
            ucTransferHeader.SStore = objUserInfo.StoreNumber
            ucTransferHeader.ShipmentDate = Now
            ucTransferHeader.EmpId = objUserInfo.EmpId
            ucTransferHeader.EmpFullName = objUserInfo.EmpFullName
            ucTransferHeader.Save()
            ucTransferBox.Box_Id = ucTransferHeader.BoxId
            ucTransferBox.Save()
            ucTransferBox.InsertICStatus()
            ucTransferBox.ClearDataset()
            'objTransferLock.UnlockTransfer(ucTransferHeader.BoxId)
            Response.Redirect("SelectShipping.aspx?TType=" & m_TType & "&UPSOn=1&Err=0&BID=" & ucTransferHeader.BoxId)
            'Response.Redirect("TransferProcessing.aspx?BID=" & ucTransferHeader.BoxId)
        End If

    End Sub

    Private Function ValidAmt() As Boolean

        If ucTransferBox.GetTotalAmt > ConfigurationSettings.AppSettings("mnyMaxTransferAmt") Then
            Return False
        Else
            Return True
        End If

    End Function

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyShipped
        ucTransferBox.ShowColumn = TransferBox.BoxColumnName.QtyShipped_Edit

        ucScanItem.EnableScan = False

        btnSave.Visible = False
        btnSubmit.Visible = False
        btnEdit.Visible = False
        btnCancelTransfer.Visible = False

        btnUpdate.Visible = True
        btnCancel.Visible = True

        ViewState("Edit") = True
        ucTransferBox.RebindData()

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        ucScanItem.EnableScan = True

        btnSave.Visible = True
        btnSubmit.Visible = True
        btnEdit.Visible = True
        btnCancelTransfer.Visible = True

        btnUpdate.Visible = False
        btnCancel.Visible = False

        ViewState("Edit") = False
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyShipped_Edit
        ucTransferBox.ShowColumn = TransferBox.BoxColumnName.QtyShipped

    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        ucTransferBox.UpdateEdit()

        ucScanItem.EnableScan = True

        btnSave.Visible = True
        btnSubmit.Visible = True
        btnEdit.Visible = True
        btnCancelTransfer.Visible = True

        btnUpdate.Visible = False
        btnCancel.Visible = False

        ViewState("Edit") = False
        ucTransferBox.HideColumn = TransferBox.BoxColumnName.QtyShipped_Edit
        ucTransferBox.ShowColumn = TransferBox.BoxColumnName.QtyShipped

    End Sub


    Private Sub btnCancelTransfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelTransfer.Click
        Response.Redirect("default.aspx")
    End Sub

End Class
