Option Explicit On 
Imports System.IO
Imports System

Partial Class Export_HT_StoreList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim dgICReport As New DataGrid()
        Dim objDataSet As New DataSet()
        Dim objHtmlTextWriter As HtmlTextWriter
        Dim objStringWriter As New StringWriter()

        ''''''''''''''''''''''''
        'Build  sql statement
        Dim strSQL
        strSQL = "SELECT STORE.StoreNum AS 'Store #', STORE.Phone1 AS 'Phone', STORE.StoreName AS 'Store Name', "
        strSQL &= "STORE.Region AS 'Reg', STORE.District AS 'Dst ', STORE.Manager, STORE.AsstMgr1 AS 'AsstMgr 1', "
        strSQL &= "STORE.AsstMgr2 AS 'AsstMgr 2', STORE.AsstMgr3 AS 'AsstMgr 3', STORE.AsstMgr4 AS 'AsstMgr 4', STORE.AsstMgr5 AS 'AsstMgr 5', "
        strSQL &= "STORE.Address1 AS 'Address', STORE.Address2 AS 'Space #', STORE.City AS 'City', STORE.State AS 'ST', "
        strSQL &= "STORE.Zip AS 'Zip', STORE.Dimension AS 'Sq. Ft.', CONVERT(varchar,STORE.OpenDate,101) AS 'Date Open', "
        strSQL &= " STORE.Simon, STORE.Service_DC AS 'SERVICING DC' "
        strSQL &= "FROM hottopic2.dbo.STORE STORE "
        strSQL &= "WHERE (STORE.Deleted Is Null) AND (STORE.StoreNum<>0) OR (STORE.Deleted<>1) AND (STORE.StoreNum<>0)"
        strSQL &= "ORDER BY STORE.StoreNum ASC"

        ''''''''''
        Dim objDAL As New clsDAL()
        Dim objDataTable As New DataTable()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        With Response
            ' Set the content type to Excel.
            .ContentType = "application/vnd.ms-excel"

            ' Remove the charset from the Content-Type header.
            .Charset = ""

            .Write("<table border='0'>")
            .Write("<td><tr><br></td></tr>")
            .Write("<tr align=left><td><img src=../images/hdr_main2.gif></td></tr>" & "<td></td>" & "<td></td>" & "<td></td>" & "<td></td>" & "<td colspan=8><font size=16><b>Current Store List</b></font></td>")
            .Write("<tr><td><br></td></tr>")
            .Write("<tr align=left><td colspan=12><b>Headquarters Phone Number for Vendors, Customers and Other External People: (626) 839-4681   Fax (626) 839-4686</b></td></tr>")
            .Write("</table>")
            .Write("<table border='1'><tr><td>" & Session("stores") & "</td></tr>")
            .Write("</table>")
        End With

        objDataSet.Tables.Add(objDAL.SelectSQL(strSQL, "strHotTopicConnection"))

        objHtmlTextWriter = New HtmlTextWriter(objStringWriter)

        With dgICReport
            .DataSource = objDataSet
            .DataBind()

            With .HeaderStyle.Font
                .Bold = True
                .Size = FontUnit.Point(10)
                .Name = "Arial"
            End With

            ' Get the HTML for the control.
            .RenderControl(objHtmlTextWriter)
        End With

        ' Write the HTML back to the browser.
        Response.Write(objStringWriter.ToString())

        'End the response.
        Response.End()

    End Sub

End Class
