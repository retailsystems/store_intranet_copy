CREATE PROCEDURE spUpdateDefaultStageData 
(
@SSName Varchar(20) = '',
@StagingStore Bit,
@Address1 Varchar(35) = '',
@Address2 Varchar(35) = '',
@City Varchar(35) = '',
@State char(2), 
@Zip Varchar(5) = '', 
@Country Char(2) = '',
@Phone1 Varchar(15) = '', 
@Phone2 Varchar(15) = '',
@ShipperNumber Varchar(10) = '',
@MyUPSID Int = '',
@ServiceDC varchar(10) = '',
@Wan Bit,
@CD Bit
) 
/*-------------------------------------------------------------------------------------------------------------------
REVISION HISTORY:

--07/06/05 Matthew Dinh Created Procedure
--------------------------------------------------------------------------------------------------------------------*/
AS
UPDATE STAGING_STORE SET 
SSName = @SSName,
StagingStore = @StagingStore,
Address1 = @Address1,
Address2 = @Address2,
City = @City,
State = @State, 
Zip = @Zip, 
Country = @Country,
Phone1 = @Phone1, 
Phone2 = @Phone2,
ShipperNumber = @ShipperNumber,
MyUPS_Id = @MyUPSID,
Service_DC = @ServiceDC,
Wan = @Wan,
CD = @CD
WHERE 
StageID = '1'
GO
