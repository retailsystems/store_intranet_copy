<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Login_Store.aspx.vb" Inherits="StoreMaint.Login_Store" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Login</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="http://172.16.125.2/StyleSheets/HotTopic.css" type="text/css"
			rel="stylesheet" runat="server">
		<script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
	</HEAD>
	<body id="pageBody" runat="server" bottomMargin="0" leftMargin="0" topMargin="0" scroll="no"
		rightMargin="0">
		<form id="frmTransferHome" method="post" runat="server">
			<iframe class="Header_Frame" id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0"
				src="http://172.16.125.2/Header/Header.aspx?mode=hottopic" frameBorder="0" width="100%"
				scrolling="no" runat="server"></iframe>
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td height="1"></td>
				</tr>
				<tr>
					<td align="center">
						<table cellSpacing="2" cellPadding="3">
							<tr id="trEmpId" runat="server">
								<td class="normal">Employee Id:</td>
								<td><asp:textbox id="txtEmployeeId" tabIndex="1" runat="server" TextMode="Password"></asp:textbox></td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" colspan="2">
									<asp:button id="btnHome" tabIndex="1" runat="server" CssClass="B1" Text="Home"></asp:button>&nbsp;&nbsp;
									<asp:button id="btnSubmit" tabIndex="1" runat="server" CssClass="B1" Text="Submit"></asp:button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
		<script language="javascript">
		document.forms[0].txtEmployeeId.focus();	
		</script>
	</body>
</HTML>
