'Imports System.Data
'Imports System.Data.OleDb
'Imports System.Web.Security

Partial Class Login_Store

    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public pEmp As String
    Private strCurrentPage As String
    ' Private objUserInfo As New UserInfo()
    Dim objDAL As New clsDAL()

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '   objUserInfo.GetEmployeeInfo()

        pageBody.Attributes.Add("onload", "")

        'strCurrentPage = Request("CP")

        If Not IsPostBack Then
            pEmp = Request("pEmp") & ""

            If pEmp > "" Then
                txtEmployeeId.Text = pEmp
                Call Login()
            End If
        End If

        Call objDAL.TextboxButton(txtEmployeeId, btnSubmit)

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=Store Login")

    End Sub

    Private Sub Login()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        Dim objSQLDataReader As SqlClient.SqlDataReader

        objConnection.Open()
        '6 digit change by Lan 11/30/2010
        strStoredProcedure = "spGetEmployeeInfo '" & Replace(txtEmployeeId.Text.PadLeft(6, "0"c), "'", "''") & "' "

        Dim objACommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
        objSQLDataReader = objACommand.ExecuteReader()

        'Dim strSQL As String
        'Dim objORACommand As New OleDbCommand()
        'Dim objORAConnection As New OleDbConnection(ConfigurationSettings.AppSettings("strSQLConnection"))
        'Dim objORADataReader As OleDbDataReader
        'Dim objSQLDataReader As SqlClient.SqlDataReader

        'objORAConnection.Open()

        'strSQL = "SELECT U.EMPLOYEEID, U.NAMEFIRST, U.NAMELAST, U.JOBCODE "
        'strSQL = strSQL & "FROM TAULTIMATEEMPLOYEE U "
        'strSQL = strSQL & "WHERE U.EMPLOYEEID = LPAD('" & txtEmployeeId.Text & "',5,0) "

        'objORACommand.Connection = objORAConnection
        'objORACommand.CommandText = strSQL
        'objORADataReader = objORACommand.ExecuteReader()

        If objSQLDataReader.Read Then
            pEmp = txtEmployeeId.Text.PadLeft(6, "0"c) '6 digit change by Lan 11/30/2010
            'Response.Redirect("StoreMaint.aspx?pEmp=" & pEmp & "")
            Session("pEmployee") = pEmp
            Response.Redirect("StoreMaint.aspx")
        Else
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Employee number " & Replace(txtEmployeeId.Text, "'", "") & " not found. Please try again.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
        End If

        objSQLDataReader.Close()
        objACommand.Dispose()
        objConnection.Close()

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Login()

    End Sub

    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        'Response.Redirect("..\home.asp")
        Response.Redirect("/home/home.aspx")
    End Sub
End Class
