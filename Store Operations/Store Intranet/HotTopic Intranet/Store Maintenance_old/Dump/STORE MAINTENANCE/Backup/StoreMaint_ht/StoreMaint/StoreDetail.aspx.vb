'***********************************
'   Project: Store Maintenance
'   Author: 
'   File Name: StoreDetail.aspx.vb
'   Description:
'       Codebehind for the StoreDetail.aspx
'   Last Modified:
'       6-21-2005 : Matthew Dinh : Created the FormValidation Function to validate form.
'       
'***********************************
Option Explicit On 

Partial Class StoreDetail

    Inherits System.Web.UI.Page

    Protected WithEvents lblShipNumbb As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCoName As System.Web.UI.WebControls.Label

    ' Protected WithEvents txtShipperNumberb As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCoName As System.Web.UI.WebControls.TextBox





#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    'Data passed in by POST
    Dim pStoreID As Long            'Store Number page is working with
    Dim pEmp As Long                'Employee Number variable used to get employee using system
    Dim pCoNameChoice As Integer    'Company Name chossen from  StoreMaint Page
    Dim pDefaultName As String      'Default Name of the Staging pulled from database
    Dim pExist As Boolean           'Check if the Store Exists
    Dim pRemodel As Boolean         'Set to True if the Store is being remodeld (Session)
    'Dim pReadOnly As Integer
    Dim region As Integer           'Session to store the region number
    Dim district As Integer         'Session to store the district Number
    Dim demension As Integer        'Session to store the demension number of the store

    'Abort Flag
    Dim blnAbort As Boolean         'Abort current process
    Dim gErrStoNum As Boolean '1    'Store the Store number with error
    Dim gCount As Integer           'Value query from the checking duplicate store
    Dim gErrorShipperNumber As Boolean  'Boolean to set after checking for used Shipper number

    'Public sourcepage As WebForm1
    'Private objUserInfo As New UserInfo()

    Dim objDAL As New clsDAL()
    Dim objDataTable As New DataTable()

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        pageBody.Attributes.Add("onload", "")

        Dim strUser As String
        'Dim temp_date As String

        strUser = objDAL.GetUser(Request, Response)
        If Trim(strUser) = "" Then
            Session("pEmployee") = 0
        End If

        pEmp = Session("pEmployee")
        'pEmp = Nothing
        'pReadOnly = Nothing

        'Commented Out so users can get access to page with out modifying data.
        'If pEmp <= 0 Then
        '    Response.Redirect("Login_Store.aspx")
        'End If

        'pReadOnly = Request.QueryString("pReadOnly")
        pStoreID = Val(Request.QueryString("pStoreID"))
        pCoNameChoice = Request.QueryString("pCoNameChoice")

        Session("sCoNameChoice") = pCoNameChoice 'Session generated to to be able keep company choice

        'When Store Region and District are set, can still be used if user modifies or remodels the store
        If Session("StoreRegion") <> "" Then
            region = Session("StoreRegion")
        End If

        If Session("StoreDistrict") <> "" Then
            district = Session("StoreDistrict")
        End If

        demension = Session("StoreDemension")

        ' If the Remodeling button is pressed, session is created, the form is edited and becomes available to be remodeled.
        If Session("Remodeling") Then
            pRemodel = True
        End If

        If Not IsPostBack Then
            If pCoNameChoice = 2 Then
                lblCD.Visible = False
                chkCD.Visible = False
            End If

            If pEmp > 0 Then
                'If no store id Is Passed Add Store, or else modify store
                If pStoreID = 0 Then
                    'new store will be added
                    Call make_new() 'Button will be changed to Add
                    Call SetDefaultStage() 'Query StagingStore Table to get default store.
                    Call objDAL.FocusScript(txtStoNum) 'Moves Curser to the txtStoNum Field
                Else
                    'existing store will be modified
                    btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this store?')")
                    Call make_modify() 'Button will be changed to Modify
                End If
            Else
                Call make_read_only() 'store access, Sets the data to be read-only
            End If
        End If

        Call ResetErrs() 'Resets all boolean error flags

        'Call GetEligEmp()
        'Call ExtractFields()
        'Call build_DDL_DisplayState_All()

        If Not IsPostBack Then
            'Display an old record
            If pStoreID > 0 Then
                Call actSearchOne(pStoreID)

                txtStoNum.Enabled = False
                cboCoName.Enabled = False
                txtRegion.Visible = False
                txtRegion.Enabled = False
                txtDistrict.Enabled = False
                txtDistrict.Visible = False
                txtMyUPSID.Enabled = False
            Else
                txtStoNum.Enabled = True
                cboRegion.Enabled = False
                cboDistrict.Enabled = False
                cboRegion.Visible = False
                cboDistrict.Visible = False
                txtRegion.Enabled = True
                txtDistrict.Enabled = True
            End If
        End If

        lblOpenDate.Text = "Open Date [Format: " & Format(Now, "M/d/yy") & "] :"

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=" & strUser & "&title=Store Detail")

    End Sub

    '-----------------------------------------------------------------------------
    ' Choice between two store used for the database connection
    '-----------------------------------------------------------------------------
    Public Function strConnHeaderOpen() As String

        'connection
        Select Case pCoNameChoice
            Case 1, 0
                Return "strStoreSQLConn1"
            Case 2
                Return "strStoreSQLConn2"
        End Select

    End Function

    '-----------------------------------------------------------------------------
    'Selects and sets the State Drop down list for all the states
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_DisplayState_All()

        'commands
        Dim CommandString As String = "SELECT DISTINCT(State) FROM STATE where state > '' "

        'Get the one table from the dataset
        Dim dataTable As DataTable = objDAL.SelectSQL(CommandString, strConnHeaderOpen)

        cboState.DataSource = dataTable.DefaultView
        cboState.DataTextField = "State"
        cboState.DataValueField = "State"
        cboState.DataBind()
        cboState.Items.Insert(0, "")

    End Sub

    '-----------------------------------------------------------------------------
    ' SetDefaultStage Function will query database to get default data for Store
    '
    '-----------------------------------------------------------------------------
    Public Sub SetDefaultStage()

        Dim CommandStagingString As String = "SELECT * FROM STAGING_STORE WHERE StageID = 1"

        'Get the one table from the dataset
        Dim dtStagingData As DataTable = objDAL.SelectSQL(CommandStagingString, strConnHeaderOpen)

        pDefaultName = dtStagingData.Rows(0).Item("SSName")
        Session("DefaultName") = pDefaultName
        txtAddr.Text = dtStagingData.Rows(0).Item("Address1")
        txtAddr2.Text = dtStagingData.Rows(0).Item("Address2")
        txtCity.Text = dtStagingData.Rows(0).Item("City")
        cboState.SelectedValue = dtStagingData.Rows(0).Item("State").ToString()
        txtZip.Text = dtStagingData.Rows(0).Item("zip")
        txtCountry.Text = dtStagingData.Rows(0).Item("Country")
        txtPhone1.Text = dtStagingData.Rows(0).Item("Phone1")
        txtPhone2.Text = dtStagingData.Rows(0).Item("Phone2")

        If dtStagingData.Rows(0).Item("StagingStore") Then
            cbStaging.Checked = True
        Else
            cbStaging.Checked = False
        End If

        If dtStagingData.Rows(0).Item("CD") Then
            chkCD.Checked = True
        Else
            chkCD.Checked = False
        End If

        If dtStagingData.Rows(0).Item("wan") Then
            cboWan.Checked = True
        Else
            cboWan.Checked = False
        End If

        lstServiceDC.SelectedValue = dtStagingData.Rows(0).Item("Service_DC").ToString()
        txtShipperNumber.Text = dtStagingData.Rows(0).Item("ShipperNumber")
        txtMyUPSID.Text = dtStagingData.Rows(0).Item("MyUPS_ID")

        txtShipperNumber.ReadOnly = True
        cboWan.Enabled = False
        cbStaging.Enabled = False
        'txtShipperNumber.ReadOnly = True
        txtMyUPSID.Enabled = False
        'lblReqOpenDate.Visible = True
        'txtStoName.Enabled = False
    End Sub

    'When the text out of the Store Number field this function gets called to set Store name and shipper number of right company
    Private Sub txtStoNum_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtStoNum.TextChanged
        Dim dblMyUPSID As Double
        Dim myUPSID As Long
        Dim UPSID() As String

        If cbStaging.Checked = True Then
            txtStoName.Text = txtStoNum.Text & " " & Session("DefaultName")
        End If

        'If FormValidation() Then 'validates the form before performing
        '    Select Case pCoNameChoice
        '        Case 1, 0 'Hot Topic
        '            dblMyUPSID = (txtStoNum.Text / 100) + 1
        '        Case 2 'Torrid
        '            dblMyUPSID = ((txtStoNum.Text / 100) - 50) + 1
        '    End Select

        '    UPSID = Split(CStr(dblMyUPSID), ".")
        '    myUPSID = CLng(UPSID(0))

        '    txtMyUPSID.Text = myUPSID
        'End If

    End Sub
    '-----------------------------------------------------------------------------
    ' Only certain employees will have access to UPS shipment information
    '
    '-----------------------------------------------------------------------------

    'Private Sub GetEligEmp() 'helen

    '    Dim lngCount As Long
    '    Dim ShipperNumber As String

    '    Call objDAL.GetEligEmp(pEmp, lngCount, strConnHeaderOpen)

    '    gCount = lngCount

    '    Call objDAL.GetShipperNumber(pStoreID, ShipperNumber, strConnHeaderOpen)

    '    If gCount = 0 Then
    '        lblShipperNumber.Visible = True
    '        lblShipperNumber.Enabled = True
    '        txtShipperNumber.Visible = True
    '        txtShipperNumber.Enabled = True
    '        If Not IsDBNull(ShipperNumber) Then
    '            'gShipNumb = ShipperNumber
    '            txtShipperNumber.Text = ShipperNumber
    '        End If
    '    End If

    'End Sub

    '-----------------------------------------------------------------------------
    ' Selects all available Region to be listed
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_DisplayRegion()
        'commands
        Dim CommandString As String = "SELECT DISTINCT(Region) FROM STORE "

        'Get the one table from the dataset
        Dim dataTable As DataTable = objDAL.SelectSQL(CommandString, strConnHeaderOpen)

        cboRegion.DataSource = dataTable.DefaultView
        cboRegion.DataTextField = "Region"
        cboRegion.DataBind()
    End Sub

    '-----------------------------------------------------------------------------
    'Selects all districts to be listed
    '
    '-----------------------------------------------------------------------------

    Private Sub build_DDL_DisplayDistrict()
        'commands
        Dim CommandString As String = "SELECT DISTINCT(District) FROM STORE where district > '' "

        'Get the one table from the dataset
        Dim dataTable As DataTable = objDAL.SelectSQL(CommandString, strConnHeaderOpen)

        cboDistrict.DataSource = dataTable.DefaultView
        cboDistrict.DataTextField = "District"
        cboDistrict.DataBind()
    End Sub

    '-----------------------------------------------------------------------------
    '' Changes the Buttons to Add and Clear  This function pretty much resetes the
    ' to be modified
    '-----------------------------------------------------------------------------
    Private Sub make_new()

        btnModify.Text = "Add"      'Modify button text will be set to Add
        btnDelete.Text = "Clear"    'Delete button will be set to Clear

        btnStageConfig.Visible = True 'Make the Stage Conifguration Link visible only on unedited form

        If pCoNameChoice = 1 Or pCoNameChoice = 0 Then
            cboCoName.SelectedIndex = 0
        Else
            cboCoName.SelectedIndex = 1
        End If

        cboCoName.Enabled = False

        'If cboRegion.SelectedIndex = -1 Then
        Call build_DDL_DisplayRegion()
        'End If

        'If cboState.SelectedIndex = -1 Then
        Call build_DDL_DisplayState_All()
        'End If

        'If cboDistrict.SelectedIndex = -1 Then
        Call build_DDL_DisplayDistrict()
        'End If

    End Sub

    '-----------------------------------------------------------------------------
    ' Change this page to a modifieable page
    ' 
    '-----------------------------------------------------------------------------
    Private Sub make_modify()

        btnDelete.Visible = True
        btnModify.Text = "Modify"
        btnDelete.Text = "Delete"

    End Sub

    '-----------------------------------------------------------------------------
    ' Change the Page to Read Only for users who who aer not logged in. Only to view
    ' 
    '-----------------------------------------------------------------------------
    Private Sub make_read_only()

        btnModify.Visible = False
        btnModify.Enabled = False
        btnDelete.Visible = False
        btnDelete.Enabled = False
        chkCD.Enabled = False
        btnRemodel.Visible = False
        btnRemodel.Enabled = False
        btnAddStore.Visible = False
        btnAddStore.Enabled = False

        txtStoNum.Enabled = False
        txtStoName.Enabled = False
        txtAddr.Enabled = False
        txtAddr2.Enabled = False
        txtAddr3.Enabled = False
        cboState.Enabled = False
        txtZip.Enabled = False
        txtPhone1.Enabled = False
        txtPhone2.Enabled = False
        txtEmail.Enabled = False
        txtSimon.Enabled = False
        txtOpenDate.Enabled = False
        txtStoDim.Enabled = False
        cboRegion.Enabled = False
        cboDistrict.Enabled = False
        txtStoMgr.Enabled = False
        txtAsstMgr1.Enabled = False
        txtAsstMgr2.Enabled = False
        txtAsstMgr3.Enabled = False
        txtAsstMgr4.Enabled = False
        txtAsstMgr5.Enabled = False
        cboCoName.Enabled = False
        txtShipperNumber.Enabled = False
        txtCity.Enabled = False
        lstServiceDC.Enabled = False
        cbStaging.Enabled = False
        txtCountry.Enabled = False
        cboWan.Enabled = False
        txtSubnet1.Enabled = False
        txtSubnet2.Enabled = False
        txtSubnet3.Enabled = False
        txtSubnet4.Enabled = False
        txtMyUPSID.Enabled = False
        btnStageConfig.Visible = False

        txtStoNum.Font.Bold = True
        txtStoName.Font.Bold = True
        txtAddr.Font.Bold = True
        txtAddr2.Font.Bold = True
        txtAddr3.Font.Bold = True
        cboState.Font.Bold = True
        txtZip.Font.Bold = True
        txtPhone1.Font.Bold = True
        txtPhone2.Font.Bold = True
        txtEmail.Font.Bold = True
        txtSimon.Font.Bold = True
        txtOpenDate.Font.Bold = True
        txtStoDim.Font.Bold = True
        cboRegion.Font.Bold = True
        cboDistrict.Font.Bold = True
        txtStoMgr.Font.Bold = True
        txtAsstMgr1.Font.Bold = True
        txtAsstMgr2.Font.Bold = True
        txtAsstMgr3.Font.Bold = True
        txtAsstMgr4.Font.Bold = True
        txtAsstMgr5.Font.Bold = True
        cboCoName.Font.Bold = True
        txtShipperNumber.Font.Bold = True
        txtCity.Font.Bold = True
        lstServiceDC.Font.Bold = True
        txtCountry.Font.Bold = True
        txtSubnet1.Font.Bold = True
        txtSubnet2.Font.Bold = True
        txtSubnet3.Font.Bold = True
        txtSubnet4.Font.Bold = True
        txtMyUPSID.Font.Bold = True

    End Sub

    '-----------------------------------------------------------------------------
    'Searches and assigns the values to the text fields for specific stores
    'This will be the modified view
    '
    '-----------------------------------------------------------------------------
    Private Sub actSearchOne(ByVal pStoreID As Long)
        'parameters
        Dim pStoNum As Integer
        Dim pStoName As String
        Dim pAddr As String
        Dim pAddr2 As String
        Dim pAddr3 As String
        Dim pCity As String
        Dim pState As String
        Dim pZip As String
        Dim pPhone1 As String
        Dim pPhone2 As String
        Dim pEmail As String
        Dim pOpen As Date
        Dim pDim As Integer
        Dim pReg As String
        Dim pDist As String
        Dim pGM As String
        Dim pAM1 As String
        Dim pAM2 As String
        Dim pAM3 As String
        Dim pAM4 As String
        Dim pAM5 As String
        Dim pTags As String
        Dim pSimon As String
        Dim pCoName As String
        Dim pShipNumb As String
        Dim pCD As Boolean
        Dim CurDate As Date = Now        ' Current date and time.
        Dim openDate As Date
        Dim pServiceDC As String
        Dim pStaging As Boolean
        Dim pMyUPSID As Integer
        Dim pWan As Boolean
        Dim pSubnet As String
        Dim pCountry As String

        Call objDAL.FindStoreData(pStoreID, pStoNum, pStoName, pAddr, pAddr2, pAddr3, pCity, pState, pZip, pPhone1, pPhone2, pEmail, pOpen, pDim, pReg, pDist, pGM, pAM1, pAM2, pAM3, pAM4, pAM5, pTags, pSimon, pShipNumb, pCoName, pCD, pServiceDC, pStaging, pMyUPSID, pWan, pSubnet, pCountry, strConnHeaderOpen)

        'Build the State Drop Down in Display Area
        Call build_DDL_DisplayState_All()
        Call build_DDL_DisplayRegion()
        Call build_DDL_DisplayDistrict()

        Session("StoreRegion") = pReg
        Session("StoreDistrict") = pDist
        Session("StoreDemension") = pDim

        If IsDBNull(pStaging) Then
            cbStaging.Checked = False
        Else
            cbStaging.Checked = pStaging
        End If

        If pStaging = False And pEmp <> 0 Then
            btnRemodel.Visible = True
        End If

        If IsDBNull(pCD) Then
            chkCD.Checked = False
        Else
            chkCD.Checked = pCD
        End If

        '#
        If IsDBNull(pStoNum) Then
            txtStoNum.Text = ""
        Else
            txtStoNum.Text = pStoNum
        End If

        'Name(2)
        If IsDBNull(pStoName) Then
            txtStoName.Text = ""
        Else
            txtStoName.Text = pStoName
        End If

        'Addr (3)
        If IsDBNull(pAddr) Then
            txtAddr.Text = ""
        Else
            txtAddr.Text = pAddr
        End If

        If IsDBNull(pAddr2) Then
            txtAddr2.Text = ""
        Else
            txtAddr2.Text = pAddr2
        End If

        If IsDBNull(pAddr3) Then
            txtAddr3.Text = ""
        Else
            txtAddr3.Text = pAddr3
        End If

        'City (4)
        If IsDBNull(pCity) Then
            txtCity.Text = ""
        Else
            txtCity.Text = pCity
        End If

        'State (5)
        If IsDBNull(pState) Then
            cboState.SelectedIndex = -1
        Else
            cboState.SelectedIndex = cboState.Items.IndexOf(cboState.Items.FindByValue(pState))
        End If

        'Zip (6) 
        If IsDBNull(pZip) Then
            txtZip.Text = ""
        Else
            txtZip.Text = pZip
        End If

        'Phone1 (7) 
        If IsDBNull(pPhone1) Then
            txtPhone1.Text = ""
        Else
            txtPhone1.Text = pPhone1
        End If

        'Phone2 (8) 
        If IsDBNull(pPhone2) Then
            txtPhone2.Text = ""
        Else
            txtPhone2.Text = pPhone2
        End If

        'Email (9) 
        If IsDBNull(pEmail) Then
            txtEmail.Text = ""
        Else
            txtEmail.Text = pEmail
        End If

        If IsDBNull(pSimon) Then
            txtSimon.Text = ""
        Else
            txtSimon.Text = pSimon
        End If

        'Open (10) 
        If IsDate(pOpen) Then
            txtOpenDate.Text = CDate(Left(pOpen, 12))
            openDate = CDate(Left(pOpen, 12))
        Else
            txtOpenDate.Text = ""
        End If

        'CurDate = CurDate.AddDays(+1)

        'If openDate >= CurDate Then
        If openDate >= DateAdd("D", 9, Now) Then
            txtAddr.Enabled = False
            txtAddr2.Enabled = False
            txtAddr3.Enabled = False
            txtCity.Enabled = False
            cboState.Enabled = False
            txtZip.Enabled = False
            txtCountry.Enabled = False
            lstServiceDC.Enabled = False
            txtShipperNumber.ReadOnly = True
        End If

        If openDate >= DateAdd("D", 9, Now) Then
            cbStaging.Enabled = False
            txtStoName.Enabled = False
        End If

        'Dim (11)
        If IsDBNull(pDim) Then
            txtStoDim.Text = ""
        Else
            txtStoDim.Text = pDim
        End If

        'Reg (12) 
        If IsDBNull(pReg) Then
            cboRegion.SelectedIndex = -1
        Else
            cboRegion.SelectedIndex = cboRegion.Items.IndexOf(cboRegion.Items.FindByValue(pReg))
        End If

        'District (13) 
        If IsDBNull(pDist) Then
            cboDistrict.SelectedIndex = -1
        Else
            cboDistrict.SelectedIndex = cboDistrict.Items.IndexOf(cboDistrict.Items.FindByValue(pDist))
        End If

        'GM (14) 
        If IsDBNull(pGM) Then
            txtStoMgr.Text = ""
        Else
            txtStoMgr.Text = pGM
        End If

        'AM1 (15) 
        If IsDBNull(pAM1) Then
            txtAsstMgr1.Text = ""
        Else
            txtAsstMgr1.Text = pAM1
        End If

        'AM2 (16)
        If IsDBNull(pAM2) Then
            txtAsstMgr2.Text = ""
        Else
            txtAsstMgr2.Text = pAM2
        End If

        'AM3 (17) 
        If IsDBNull(pAM3) Then
            txtAsstMgr3.Text = ""
        Else
            txtAsstMgr3.Text = pAM3
        End If

        'AM4 (18) 
        If IsDBNull(pAM4) Then
            txtAsstMgr4.Text = ""
        Else
            txtAsstMgr4.Text = pAM4
        End If

        'AM5 (19) 
        If IsDBNull(pAM5) Then
            txtAsstMgr5.Text = ""
        Else
            txtAsstMgr5.Text = pAM5
        End If

        'Company Name (22) 
        If IsDBNull(pCoName) Then
            cboCoName.SelectedIndex = -1
        Else
            cboCoName.SelectedIndex = cboCoName.Items.IndexOf(cboCoName.Items.FindByValue(pCoName))
        End If

        'Shipper Number (23) 
        If IsDBNull(pShipNumb) Then
            txtShipperNumber.Text = ""
        Else
            txtShipperNumber.Text = pShipNumb
        End If

        If IsDBNull(pMyUPSID) Then
            txtMyUPSID.Text = ""
        Else
            txtMyUPSID.Text = pMyUPSID
        End If

        If pWan = 0 Then
            cboWan.Checked = False
        Else
            cboWan.Checked = True
        End If

        If pSubnet = "" Then
            txtSubnet1.Text = ""
            txtSubnet2.Text = ""
            txtSubnet3.Text = ""
            txtSubnet4.Text = ""
        Else
            Dim aSubnet() As String
            aSubnet = Split(pSubnet, ".")
            txtSubnet1.Text = aSubnet(0)
            txtSubnet2.Text = aSubnet(1)
            txtSubnet3.Text = aSubnet(2)
            txtSubnet4.Text = aSubnet(3)
        End If

        If IsDBNull(pCountry) Then
            txtCountry.Text = ""
        Else
            txtCountry.Text = pCountry
        End If
        '4/14/05 SRI BAJJURI - Service DC MOD
        pServiceDC = IIf(IsDBNull(pServiceDC), "", pServiceDC)
        Dim li As ListItem
        lstServiceDC.SelectedIndex = -1
        For Each li In lstServiceDC.Items
            If li.Value.ToUpper = pServiceDC.ToUpper Then
                li.Selected = True
                Exit For
            End If
        Next

    End Sub 'end actSearchOne   

    Private Sub ResetErrs()

        blnAbort = False
        gCount = 0
        gErrStoNum = False
        gErrorShipperNumber = False

    End Sub

    '-----------------------------------------------------------------------------
    'Prelim Delete the record
    '
    '
    '-----------------------------------------------------------------------------
    Private Sub btnAddStore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddStore.Click
        btnAddStore.Attributes.Add("onclick", "return confirm('Are you sure you want to Add this new store?')")
        If FormValidation() Then
            Call actInsertVars()
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        'txtAddr2.Text = ""
        txtAddr3.Text = ""
        txtSimon.Text = ""
        txtPhone1.Text = ""
        txtPhone2.Text = ""
        txtEmail.Text = ""
        txtOpenDate.Text = ""
        'txtStoDim.Text = ""
        'cboRegion.SelectedIndex = -1
        cboRegion.SelectedIndex = cboRegion.Items.IndexOf(cboRegion.Items.FindByValue(cboRegion.SelectedIndex))
        'txtRegion.Text = ""
        cboDistrict.SelectedIndex = cboDistrict.Items.IndexOf(cboDistrict.Items.FindByValue(cboDistrict.SelectedIndex))
        'txtDistrict.Text = ""
        txtStoMgr.Text = ""
        txtAsstMgr1.Text = ""
        txtAsstMgr2.Text = ""
        txtAsstMgr3.Text = ""
        txtAsstMgr4.Text = ""
        txtAsstMgr5.Text = ""
        'txtShipperNumber.Text = ""
        'txtCountry.Text = ""
        txtSubnet1.Text = ""
        txtSubnet2.Text = ""
        txtSubnet3.Text = ""
        txtSubnet4.Text = ""
    End Sub


    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        'Dim Store As String
        btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this store?')")

        If pStoreID = 0 Then 'new record 
            'Call ClearCells()
            Response.Redirect("StoreDetail.aspx?pCoNameChoice=" & pCoNameChoice & "")
        Else
            'StoNum = Trim(txtStoNum.Text)
            'StoNumF = Val(txtStoNum.Text)

            Call ActDelete(Val(txtStoNum.Text))

            Response.Redirect("StoreMaint.aspx?pCoNameChoice=" & pCoNameChoice & "")
        End If

    End Sub

    '-----------------------------------------------------------------------------
    'Final Delete the record
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub ActDelete(ByVal StoNumF As Long)

        Call objDAL.DeleteStore(StoNumF, pEmp, strConnHeaderOpen)

        'pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("STORE " & StoNumF & " has been successfully deleted. ") & "&Title=Successful Update" & "','Success','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")

    End Sub

    '-----------------------------------------------------------------------------
    'Passes Store Number in and checks if the record already exists
    '
    '
    '-----------------------------------------------------------------------------

    Private Sub CheckDup(ByVal gStoNum As Long) 'helen

        Dim strCount As String

        Call objDAL.CountStore(gStoNum, strCount, strConnHeaderOpen)

        gCount = Val(strCount)

        If gCount <> 0 Then
            blnAbort = True
            gErrStoNum = True
            'Call CheckDeleted()
        End If

    End Sub

    Private Sub CheckDupShipperNumber(ByVal gStoNum As Long, ByVal gShipperNumber As String) 'Matt

        Dim strCount As String

        Call objDAL.CountShipperNumber(gStoNum, gShipperNumber, strCount, strConnHeaderOpen)

        gCount = Val(strCount)

        If gCount <> 0 Then
            blnAbort = True
            gErrorShipperNumber = True
        End If

    End Sub

    Private Sub CheckDeleted()

        Dim strCount As String

        Call objDAL.CountDeletedStore(Val(txtStoNum.Text), strCount, strConnHeaderOpen)

        gCount = Val(strCount)

        If gCount <> 0 Then
            pExist = True
            Call UpdateDeletedRecord()
        End If

    End Sub

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Function actInsertVars() As Boolean

        Dim strOnLoad As String
        Dim strSubnet As String

        If txtSubnet1.Text = "" And txtSubnet2.Text = "" And txtSubnet3.Text = "" And txtSubnet4.Text = "" Then
            strSubnet = "0.0.0.0"
        Else
            strSubnet = txtSubnet1.Text & "." & txtSubnet2.Text & "." & txtSubnet3.Text & "." & txtSubnet4.Text
        End If

        ' If the Shipper Number is Blank give it a value of 0
        'If txtShipperNumber.Text = "" Then
        '    txtShipperNumber.Text = 0
        'End If

        ' If the My UPS ID is Blank give it a value of 0
        If txtMyUPSID.Text = "" Then
            txtMyUPSID.Text = 0
        End If

        'Inputs
        Try

            Call objDAL.InsertStoreRow(Val(txtStoNum.Text), _
                                        txtStoName.Text, _
                                        txtAddr.Text, _
                                        txtAddr2.Text, _
                                        txtAddr3.Text, _
                                        txtCity.Text, _
                                        cboState.SelectedItem.Text, _
                                        txtZip.Text, _
                                        txtPhone1.Text, _
                                        txtPhone2.Text, _
                                        txtEmail.Text, _
                                        CDate(txtOpenDate.Text), _
                                        Val(txtStoDim.Text), _
                                        txtRegion.Text, _
                                        txtDistrict.Text, _
                                        txtStoMgr.Text, _
                                        txtAsstMgr1.Text, _
                                        txtAsstMgr2.Text, _
                                        txtAsstMgr3.Text, _
                                        txtAsstMgr4.Text, _
                                        txtAsstMgr5.Text, _
                                        "", _
                                        txtSimon.Text, _
                                        cboCoName.SelectedItem.Text, _
                                        txtShipperNumber.Text, _
                                        pEmp, _
                                        chkCD.Checked, _
                                        lstServiceDC.SelectedItem.Value, _
                                        cbStaging.Checked, _
                                        txtMyUPSID.Text, _
                                        txtCountry.Text, _
                                        cboWan.Checked, _
                                        strSubnet, _
                                        strConnHeaderOpen)

            Return True

            'strOnLoad = Server.UrlEncode("This record has been successfully added!") & "&Title=Successful Update" & "','Error'"

            'Response.Redirect("StoreMaint.aspx?pCoNameChoice=" & pCoNameChoice & "")
        Catch ex As Exception
            pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Data not loaded, please check your input. Please check with administator.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
        End Try


    End Function

    '-----------------------------------------------------------------------------
    '
    '
    '
    '-----------------------------------------------------------------------------

    Private Function UpdateTable() As Boolean

        Dim strOnLoad As String
        Dim strSubnet As String
        Dim dblMyUPSID As Double
        Dim myUPSID As Long
        Dim UPSID() As String

        If txtSubnet1.Text = "" And txtSubnet2.Text = "" And txtSubnet3.Text = "" And txtSubnet4.Text = "" Then
            strSubnet = "0.0.0.0"
        Else
            strSubnet = txtSubnet1.Text & "." & txtSubnet2.Text & "." & txtSubnet3.Text & "." & txtSubnet4.Text
        End If

        '' Sets the MyUPSID to the right ID if the Store is not a staging store.
        'If Not cbStaging.Checked = True Then
        '    Select Case pCoNameChoice
        '        Case 1, 0 'Hot Topic
        '            dblMyUPSID = (txtStoNum.Text / 100) + 1
        '        Case 2 'Torrid
        '            dblMyUPSID = ((txtStoNum.Text / 100) - 50) + 1
        '    End Select

        '    UPSID = Split(CStr(dblMyUPSID), ".")
        '    myUPSID = CLng(UPSID(0))

        '    txtMyUPSID.Text = myUPSID
        'End If

        Try
            Call objDAL.UpdateRow(Val(txtStoNum.Text), _
                                           txtStoName.Text, _
                                           txtAddr.Text, _
                                           txtAddr2.Text, _
                                           txtAddr3.Text, _
                                           txtCity.Text, _
                                           cboState.SelectedItem.Text, _
                                           txtZip.Text, _
                                           txtPhone1.Text, _
                                           txtPhone2.Text, _
                                           txtEmail.Text, _
                                           CDate(txtOpenDate.Text), _
                                           Val(txtStoDim.Text), _
                                           cboRegion.SelectedItem.Text, _
                                           cboDistrict.SelectedItem.Text, _
                                           txtStoMgr.Text, _
                                           txtAsstMgr1.Text, _
                                           txtAsstMgr2.Text, _
                                           txtAsstMgr3.Text, _
                                           txtAsstMgr4.Text, _
                                           txtAsstMgr5.Text, _
                                           "", _
                                           txtSimon.Text, _
                                           cboCoName.SelectedItem.Text, _
                                           txtShipperNumber.Text, _
                                           chkCD.Checked, _
                                           lstServiceDC.SelectedItem.Value, _
                                           pEmp, _
                                           cbStaging.Checked, _
                                           txtMyUPSID.Text, _
                                           cboWan.Checked, _
                                           strSubnet, _
                                           txtCountry.Text, _
                                           strConnHeaderOpen)

            Return True
        Catch ex As Exception
            Throw ex
        End Try


        'strOnLoad = Server.UrlEncode("This record has been successfully updated!") & "&Title=Successful Update" & "','Success'"

        'Response.Redirect("StoreMaint.aspx?pCoNameChoice=" & pCoNameChoice & "")

    End Function

    Private Sub UpdateDeletedRecord()

        Dim strOnLoad As String
        Dim strSubnet As String

        If txtSubnet1.Text = "" Or txtSubnet2.Text = "" Or txtSubnet3.Text = "" Or txtSubnet4.Text = "" Then
            strSubnet = "0.0.0.0"
        Else
            strSubnet = txtSubnet1.Text & "." & txtSubnet2.Text & "." & txtSubnet3.Text & "." & txtSubnet4.Text
        End If

        If FormValidation() Then
            Call objDAL.UpdateRow(Val(txtStoNum.Text), _
                                    txtStoName.Text, _
                                    txtAddr.Text, _
                                    txtAddr2.Text, _
                                    txtAddr3.Text, _
                                    txtCity.Text, _
                                    cboState.SelectedItem.Text, _
                                    txtZip.Text, _
                                    txtPhone1.Text, _
                                    txtPhone2.Text, _
                                    txtEmail.Text, _
                                    CDate(txtOpenDate.Text), _
                                    Val(txtStoDim.Text), _
                                    cboRegion.SelectedItem.Text, _
                                    cboDistrict.SelectedItem.Text, _
                                    txtStoMgr.Text, _
                                    txtAsstMgr1.Text, _
                                    txtAsstMgr2.Text, _
                                    txtAsstMgr3.Text, _
                                    txtAsstMgr4.Text, _
                                    txtAsstMgr5.Text, _
                                    "", _
                                    txtSimon.Text, _
                                    cboCoName.SelectedItem.Text, _
                                    txtShipperNumber.Text, _
                                    chkCD.Checked, _
                                    lstServiceDC.SelectedItem.Value, _
                                    pEmp, _
                                    cbStaging.Checked, _
                                    txtMyUPSID.Text, _
                                    cboWan.Checked, _
                                    strSubnet, _
                                    txtCountry.Text, _
                                    strConnHeaderOpen)

            strOnLoad = Server.UrlEncode("This record has been successfully created!") & "&Title=Successful Update" & "','Success'"
        End If

        pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & strOnLoad & ",'center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")

    End Sub

    '-----------------------------------------------------------------------------
    ' This is either the Add or Save Record button - forked based on flag passed in
    '
    '
    '-----------------------------------------------------------------------------
    Private Sub btnRemodel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemodel.Click
        Call make_new()
        Call SetDefaultStage()
        btnRemodel.Visible = False
        btnDelete.Visible = False
        btnClear.Visible = True
        btnModify.Visible = False
        btnAddStore.Visible = True
        cboRegion.Visible = False
        txtRegion.Visible = True
        cboDistrict.Visible = False
        txtDistrict.Visible = True
        Session("Remodeling") = True
        cbStaging.Enabled = False
        txtStoNum.Enabled = True
        txtShipperNumber.ReadOnly = True

        'txtStoNum.Text = "19" & txtStoNum.Text

        lblReqOpenDate.Visible = True
        txtStoName.Enabled = True
        'txtAddr2.Text = ""
        txtAddr3.Text = ""
        txtSimon.Text = ""
        txtPhone1.Text = ""
        txtPhone2.Text = ""
        txtEmail.Text = ""
        txtOpenDate.Text = ""
        'txtStoDim.Text = ""
        'cboRegion.SelectedIndex = -1
        txtRegion.Text = region
        'txtRegion.Text = ""
        txtStoDim.Text = demension
        txtDistrict.Text = district
        txtStoMgr.Text = ""
        txtAsstMgr1.Text = ""
        txtAsstMgr2.Text = ""
        txtAsstMgr3.Text = ""
        txtAsstMgr4.Text = ""
        txtAsstMgr5.Text = ""
        txtSubnet1.Text = ""
        txtSubnet2.Text = ""
        txtSubnet3.Text = ""
        txtSubnet4.Text = ""
    End Sub

    Private Sub btnModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModify.Click

        If pStoreID = 0 Then
            'check if all inputs are valid 
            If FormValidation() Then
                If actInsertVars() Then
                    Call make_read_only()
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("This record has been successfully added!") & "&Title=" & Server.UrlEncode("Success!") & "','Success','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                    'Response.Redirect("StoreDetail.aspx?pStoreID=" & txtStoNum.Text & "&pCoNameChoice=" & pCoNameChoice & "")
                End If
            End If
        Else 'If this is an "Update Record" 
            If FormValidation() Then
                If UpdateTable() Then
                    pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("This record has been successfully updated!") & "&Title=" & Server.UrlEncode("Success!") & "','Success','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                End If
            End If
        End If

    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click

        'If btnModify.Text = "Add" Then
        Response.Redirect("StoreMaint.aspx?pCoNameChoice=" & pCoNameChoice & "")
        'Else
        '    Response.Write("<script languague=javascript>window.close()</script>")
        'End If

    End Sub

    'Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    Call ClearCells()

    'End Sub

    'Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    Response.Redirect("StoreMaint.aspx?pEmp=" & pEmp & "")

    'End Sub

    Private Sub txtCity_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCity.TextChanged

    End Sub

    Private Sub lstServiceDC_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstServiceDC.SelectedIndexChanged

    End Sub

    Function FormValidation() As Boolean
        Dim strOnLoad As String
        Dim chkDate As Boolean
        Dim strDate As String
        Dim aDate() As String

        If pEmp <= 0 Then
            Response.Redirect("Login_Store.aspx")
        End If

        Try
            If Not Trim(txtOpenDate.Text) = "" Then
                If IsDate(txtOpenDate.Text) Then
                    'If (CDate(txtOpenDate.Text) < CDate("1/1/1900")) Then
                    aDate = Split(Trim(txtOpenDate.Text), "/")
                    If aDate(0) = "" Then
                        chkDate = False
                    ElseIf aDate(1) = "" Then
                        chkDate = False
                    ElseIf aDate(2) = "" Then
                        chkDate = False
                    Else
                        chkDate = True
                    End If
                    'End If
                Else
                    chkDate = False
                End If
            End If
        Catch ex As Exception
            pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Data not loaded, please check your input.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
        End Try

        If pStoreID = 0 Or pRemodel = True Then
            If Trim(txtStoNum.Text) = "" Or Not IsNumeric(txtStoNum.Text) Then 'blank store number
                strOnLoad = Server.UrlEncode("Store Number is required.  Please check your input.") & "','Error'"
                Call objDAL.FocusScript(txtStoNum)
            Else
                Call CheckDup(txtStoNum.Text)
            End If
        End If

        If Not Trim(txtShipperNumber.Text) = "" And cbStaging.Checked = False Then
            Call CheckDupShipperNumber(txtStoNum.Text, txtShipperNumber.Text)
        End If

        If Trim(txtStoNum.Text) = "" Or Not IsNumeric(txtStoNum.Text) Then 'blank store number
            strOnLoad = Server.UrlEncode("Store Number is required.  Please check your input.") & "','Error'"
            Call objDAL.FocusScript(txtStoNum)
        ElseIf (pCoNameChoice = 2 And Int(txtStoNum.Text) < 5000) And pRemodel <> True Then
            strOnLoad = Server.UrlEncode("Torrid store numbers must be greater than 5000.") & "','Error'"
            Call objDAL.FocusScript(txtStoNum)
        ElseIf (pCoNameChoice = 1 And Int(txtStoNum.Text) > 4999) And pRemodel <> True Then
            strOnLoad = Server.UrlEncode("Hot Topic store numbers must be less than 5000.") & "','Error'"
            Call objDAL.FocusScript(txtStoNum)
        ElseIf gErrStoNum Then  'store exists
            strOnLoad = Server.UrlEncode("This Store Number already exists.") & "','Error'"
            Call objDAL.FocusScript(txtStoNum)
        ElseIf Trim(txtStoName.Text) = "" Then
            strOnLoad = Server.UrlEncode("Store Name is required.  Please check your input.") & "','Error'"
            Call objDAL.FocusScript(txtStoName)
        ElseIf Trim(cboCoName.SelectedItem.Text) = "" Then
            strOnLoad = Server.UrlEncode("Company Name is required.  Please check your input.") & "','Error'"
            Call objDAL.FocusScript(cboCoName)
        ElseIf Trim(txtCity.Text) = "" Then
            strOnLoad = Server.UrlEncode("City is required. Please check your input.") & "','Error'"
            Call objDAL.FocusScript(txtCity)
        ElseIf Trim(cboState.SelectedValue) = "" Then
            strOnLoad = Server.UrlEncode("State is required. Please select a state.") & "','Error'"
            Call objDAL.FocusScript(cboState)
        ElseIf Not IsNumeric(txtZip.Text) Or txtZip.Text.Length < 5 Then
            strOnLoad = Server.UrlEncode("Please enter a valid Zip code.") & "','Error'"
            Call objDAL.FocusScript(txtZip)
        ElseIf Trim(txtCountry.Text) = "" Or Not System.Text.RegularExpressions.Regex.IsMatch(txtCountry.Text, _
           "^([a-zA-z\s]{2,})$") Then
            strOnLoad = Server.UrlEncode("Please enter a valid Country.") & "','Error'"
            Call objDAL.FocusScript(txtCountry)
        ElseIf Not Trim(txtPhone1.Text) = "" And Not System.Text.RegularExpressions.Regex.IsMatch(txtPhone1.Text, _
   "^*\(\w+([-+.]\w+)*\)\w+([-.]\w+)*\-\w+([-.]\w+)*$") Then
            strOnLoad = Server.UrlEncode("Please enter a valid Phone Number.") & "','Error'"
            Call objDAL.FocusScript(txtPhone1)
        ElseIf Not Trim(txtPhone2.Text) = "" And Not System.Text.RegularExpressions.Regex.IsMatch(txtPhone2.Text, _
"^*\(\w+([-+.]\w+)*\)\w+([-.]\w+)*\-\w+([-.]\w+)*$") Then
            strOnLoad = Server.UrlEncode("Please enter a valid Phone Number.") & "','Error'"
            Call objDAL.FocusScript(txtPhone2)
        ElseIf Not Trim(txtEmail.Text) = "" And Not System.Text.RegularExpressions.Regex.IsMatch(txtEmail.Text, _
   "^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$") Then
            strOnLoad = Server.UrlEncode("Please enter a valid eMail address.") & "','Error'"
            Call objDAL.FocusScript(txtEmail)
        ElseIf Not Trim(txtStoDim.Text) = "" And Not IsNumeric(txtStoDim.Text) Then
            strOnLoad = Server.UrlEncode("Dimension fields (Sq. Feet) must be numeric.") & "','Error'"
            Call objDAL.FocusScript(txtStoDim)
        ElseIf Not Trim(txtStoDim.Text) = "" And Not Val(txtStoDim.Text) >= 0 Then
            strOnLoad = Server.UrlEncode("Please enter a non-negative Sq. Feet number.") & "','Error'"
            Call objDAL.FocusScript(txtStoDim)
        ElseIf Not Trim(txtSubnet1.Text) = "" And Not IsNumeric(txtSubnet1.Text) Then
            strOnLoad = Server.UrlEncode("Subnet value must be numeric.") & "','Error'"
            Call objDAL.FocusScript(txtSubnet1)
        ElseIf Not Trim(txtSubnet2.Text) = "" And Not IsNumeric(txtSubnet2.Text) Then
            strOnLoad = Server.UrlEncode("Subnet value must be numeric.") & "','Error'"
            Call objDAL.FocusScript(txtSubnet2)
        ElseIf Not Trim(txtSubnet3.Text) = "" And Not IsNumeric(txtSubnet3.Text) Then
            strOnLoad = Server.UrlEncode("Subnet value must be numeric.") & "','Error'"
            Call objDAL.FocusScript(txtSubnet3)
        ElseIf Not Trim(txtSubnet4.Text) = "" And Not IsNumeric(txtSubnet4.Text) Then
            strOnLoad = Server.UrlEncode("Subnet value must be numeric.") & "','Error'"
            Call objDAL.FocusScript(txtSubnet4)
        ElseIf gErrorShipperNumber Then  'Shipper Number Exist for Non Staging Store
            strOnLoad = Server.UrlEncode("This Shipper Number already exists. Non Staging stores may not have duplicate Shipper Numbers.") & "','Error'"
            Call objDAL.FocusScript(txtShipperNumber)
        ElseIf Trim(txtShipperNumber.Text) = "" Then
            strOnLoad = Server.UrlEncode("Shipper Number is required. Please check your input.") & "','Error'"
            Call objDAL.FocusScript(cboCoName)
        ElseIf Trim(txtMyUPSID.Text) = "" Then
            strOnLoad = Server.UrlEncode("MyUPS ID is required. Please check your input.") & "','Error'"
            Call objDAL.FocusScript(txtMyUPSID)
        ElseIf Not IsDate(txtOpenDate.Text) Then
            strOnLoad = Server.UrlEncode("Open Date field must be a date") & "','Error'"
            Call objDAL.FocusScript(txtOpenDate)
        ElseIf Not IsDate(txtOpenDate.Text) Or chkDate = False Then
            strOnLoad = Server.UrlEncode("Please enter a valid opening date.") & "','Error'"
            Call objDAL.FocusScript(txtOpenDate)
        ElseIf CDate(txtOpenDate.Text) < CDate("1/1/1900") Then
            strOnLoad = Server.UrlEncode("Please enter a valid opening date.") & "','Error'"
            Call objDAL.FocusScript(txtOpenDate)
            'ElseIf Not IsNumeric(txtZip.Text) Then
            'strOnLoad = Server.UrlEncode("Zip Code must be numeric.") & "','Error'"
            'Call objDAL.FocusScript(txtZip)
        ElseIf (CDate(txtOpenDate.Text).Date > CDate(DateAdd("M", 3, Now)).Date) Or (CDate(txtOpenDate.Text).Year > CDate(Now).Year) Then
            strOnLoad = Server.UrlEncode("This store can not be added at this time. Stores may only be added within 3 Months of Store opening.") & "','Error'"
            Call objDAL.FocusScript(txtOpenDate)
        ElseIf (cbStaging.Checked = False) And (CDate(txtOpenDate.Text) > CDate(DateAdd("D", 9, Now))) Then
            strOnLoad = Server.UrlEncode("A Store must be a Staging Store if Open date is further than 9 Days from current date.") & "','Error'"
            Call objDAL.FocusScript(txtOpenDate)
        Else
            Return True
        End If

        pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & strOnLoad & ",'center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
    End Function
End Class
