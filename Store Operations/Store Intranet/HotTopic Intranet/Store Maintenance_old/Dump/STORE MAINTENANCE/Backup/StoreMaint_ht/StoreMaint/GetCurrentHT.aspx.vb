Public Class GetCurrentHT
    Inherits System.Web.UI.Page

    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents btnExport As System.Web.UI.WebControls.Button
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Protected WithEvents btnHome As System.Web.UI.WebControls.Button
    Protected WithEvents dgHTStoreList As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ifmHeader As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim objDAL As New clsDALdual()
    Dim objDataTable As New DataTable()


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=&title=Hot Topic Current Store List")

        populateGrid()
    End Sub


    Public Sub populateGrid()
       
        objDataTable = objDAL.SelectSQL_SP("strHotTopicConnection", "spGetCurrentStoreListHT")
        dgHTStoreList.DataSource = objDataTable
        dgHTStoreList.DataBind()
        dgHTStoreList.Visible = True
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim strSQL As String

        ''''''''''''''''''''''''
        'Build  sql statement
        strSQL = "SELECT STORE.StoreNum AS 'Store #', STORE.Phone1 AS 'Phone', STORE.StoreName AS 'Store Name', "
        strSQL &= "STORE.Region AS 'Reg', STORE.District AS 'Dst ', STORE.Manager, STORE.AsstMgr1 AS 'AsstMgr 1', "
        strSQL &= "STORE.AsstMgr2 AS 'AsstMgr 2', STORE.AsstMgr3 AS 'AsstMgr 3', STORE.AsstMgr4 AS 'AsstMgr 4', STORE.AsstMgr5 AS 'AsstMgr 5', "
        strSQL &= "STORE.Address1 AS 'Address', STORE.Address2 AS 'Space #', STORE.City AS 'City', STORE.State AS 'ST', "
        strSQL &= "STORE.Zip AS 'Zip', STORE.Dimension AS 'Sq. Ft.', CONVERT(varchar,STORE.OpenDate,101) AS 'Date Open', "
        strSQL &= " STORE.Simon "
        strSQL &= "FROM hottopic2.dbo.STORE STORE "
        strSQL &= "WHERE (STORE.Deleted Is Null) AND (STORE.StoreNum<>0) OR (STORE.Deleted<>1) AND (STORE.StoreNum<>0)"
        strSQL &= "ORDER BY STORE.StoreNum ASC"

        ''''''''''
        'Now pull back datatable, and put in session variable; for next page

        Session("dsExport") = objDAL.SelectSQL(strSQL, "strHotTopicConnection")
        Response.Redirect("Export_HT_StoreList.aspx")
    End Sub


    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Response.Redirect("storemaint.aspx")
    End Sub

    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        Response.Redirect(ConfigurationSettings.AppSettings("hottopic_home") + "home/home.aspx")
    End Sub
End Class
