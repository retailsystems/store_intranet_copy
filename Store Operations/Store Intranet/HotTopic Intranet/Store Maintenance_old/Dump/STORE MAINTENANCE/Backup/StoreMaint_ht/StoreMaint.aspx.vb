'***********************************
'   Project: Store Maintenance
'   Author: 
'   File Name: StoreMaint.aspx.vb
'   Description:
'       Codebehind for the StoreMaint.aspx
'   Last Modified:
'       6-21-2005 : Matthew Dinh : Modified the Search Query to exclude Staging Store.
'       
'***********************************
Option Explicit On 

Public Class WebForm1

    Inherits System.Web.UI.Page

    Protected WithEvents btnClear As System.Web.UI.WebControls.Button
    Protected WithEvents btnReturn As System.Web.UI.WebControls.Button
    Protected WithEvents btnSearch As System.Web.UI.WebControls.Button
    Protected WithEvents btnAddStore As System.Web.UI.WebControls.Button

    Protected WithEvents cboActivities As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboCity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboCoName As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboExcelExport As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboMgr As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboSrchDist As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboSrchRegion As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboSrchState As System.Web.UI.WebControls.DropDownList

    Protected WithEvents dgSearch As System.Web.UI.WebControls.DataGrid

    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblCity1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblCoName2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblMgr As System.Web.UI.WebControls.Label
    Protected WithEvents lblP1Format As System.Web.UI.WebControls.Label
    Protected WithEvents lblSrchDist As System.Web.UI.WebControls.Label
    Protected WithEvents lblSrchOpenDate As System.Web.UI.WebControls.Label
    Protected WithEvents lblSrchRegion As System.Web.UI.WebControls.Label
    Protected WithEvents lblStoName As System.Web.UI.WebControls.Label
    Protected WithEvents lblStoNum As System.Web.UI.WebControls.Label
    Protected WithEvents lblTo As System.Web.UI.WebControls.Label
    Protected WithEvents lblTo2 As System.Web.UI.WebControls.Label
    Protected WithEvents lbSrchState As System.Web.UI.WebControls.Label
    Protected WithEvents lblNoRecords As System.Web.UI.WebControls.Label
    Protected WithEvents lblReports As System.Web.UI.WebControls.Label
    Protected WithEvents lblStaging As System.Web.UI.WebControls.Label

    Protected WithEvents pageBody As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lnkStyles As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifmHeader As System.Web.UI.HtmlControls.HtmlGenericControl

    Protected WithEvents txtFDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSize As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSize2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSrchStoName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSrchStoNum As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTDate As System.Web.UI.WebControls.TextBox

    Protected WithEvents btnExportExcelHT As System.Web.UI.WebControls.HyperLink
    Protected WithEvents btnExportExcelTD As System.Web.UI.WebControls.HyperLink

    Protected WithEvents cbStaging As System.Web.UI.WebControls.CheckBox

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mstrSearch As String
    Public pCoNameChoice As Integer
    Public pEmp As Integer

    Dim objDAL As New clsDAL()
    Dim objDataTable As New DataTable()

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strTitle As String
        Dim strUser As String
        lblNoRecords.Visible = False
        pageBody.Attributes.Add("onload", "")

        strUser = objDAL.GetUser(Request, Response)
        mstrSearch = Request("txtSearchStr")
        pCoNameChoice = Request.QueryString("pCoNameChoice")
        'pEmp = Request.QueryString("pEmp")
        'pEmp = Session("pEmployee")
        dgSearch.Visible = False

        'Test to check of User actually logged into Store Maintenance
        If Trim(strUser) = "" Then
            Session("pEmployee") = 0
        End If

        pEmp = Session("pEmployee")

        If Not IsPostBack Then
			If InStr(ConfigurationSettings.AppSettings("Mode"), "Torrid", CompareMethod.Text) > 0 Then
				cboCoName.SelectedIndex = 1
			End If

            'populate pull downs
            build_DDL_State()
            build_DDL_Region()
            build_DDL_District()
            build_DDL_City()
            build_DDL_Mgr()

            If mstrSearch > "" Then
                Call GatherData()
                dgSearch.Visible = True
            End If
        End If

        If pEmp > 0 Then
            lblReports.Visible = True
            btnExportExcelHT.Visible = True
            btnExportExcelTD.Visible = True
            btnAddStore.Visible = True
            strTitle = "Store Maintenance"
        Else
            lblReports.Visible = False
            btnExportExcelHT.Visible = False
            btnExportExcelTD.Visible = False
            btnAddStore.Visible = False
            lblStaging.Visible = False
            cbStaging.Visible = False
            strTitle = "Store Lookup"
        End If

        lblP1Format.Text = "&nbsp;(Format " & Format(Now, "MM/dd/yy") & ") :"

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=&user=" & strUser & "&title=" & strTitle)

    End Sub

    ' This function will return 1 for Hot Topic store or 2 for Torrid tore
    Private Sub CoNameChoice()

        ' Hot Topic Store Is 0 and Torrid Store is 1
        If cboCoName.SelectedIndex = 0 Then
            pCoNameChoice = 1
        ElseIf cboCoName.SelectedIndex = 1 Then
            pCoNameChoice = 2
        ElseIf cboCoName.SelectedIndex = 2 Then
            pCoNameChoice = 3
        End If

    End Sub

    'Searches and assigns the values to the text fields
    Private Sub dgSearch_PageIndexChanged(ByVal Source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgSearch.PageIndexChanged

        dgSearch.CurrentPageIndex = e.NewPageIndex

        Call GatherData()

        dgSearch.DataBind()

    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        dgSearch.CurrentPageIndex = 0   'Fixes current index problem.

        'Hide Display Area
        dgSearch.Visible = False

        GatherData()

    End Sub

    Private Sub GatherData()

        Dim dteNewstrThruDate As Date
        Dim blnGoSearch As Boolean 'Counter 
        Dim strCommandString As String
        Dim blnSAOpenDate As Boolean
        Dim strFromDate As String
        Dim strThruDate As String
        Dim chkDate As Boolean
        Dim strDate As String
        Dim aDate() As String

        blnGoSearch = True

        If Not Trim(txtFDate.Text) = "" Then
            If IsDate(txtFDate.Text) Then
                If (CDate(txtFDate.Text) < CDate("1/1/1900")) Then
                    aDate = Split(Trim(txtFDate.Text), "/")
                    If aDate(0) = "" Then
                        chkDate = False
                    ElseIf aDate(1) = "" Then
                        chkDate = False
                    ElseIf aDate(2) = "" Then
                        chkDate = False
                    Else
                        chkDate = True
                    End If
                End If
            Else
                chkDate = False
            End If
            If chkDate = False Then
                pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter a Valid Date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                blnGoSearch = False
            End If
        End If

        If Not Trim(txtTDate.Text) = "" Then
            If IsDate(txtTDate.Text) Then
                If (CDate(txtTDate.Text) < CDate("1/1/1900")) Then
                    aDate = Split(Trim(txtTDate.Text), "/")
                    If aDate(0) = "" Then
                        chkDate = False
                    ElseIf aDate(1) = "" Then
                        chkDate = False
                    ElseIf aDate(2) = "" Then
                        chkDate = False
                    Else
                        chkDate = True
                    End If
                End If
            Else
                chkDate = False
            End If

            If chkDate = False Then
                pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter a Valid Date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                blnGoSearch = False
            End If
        End If

        'Check if Search
        If mstrSearch > "" And txtSrchStoNum.Text = "" And txtSrchStoName.Text = "" And _
                        (txtFDate.Text = "" Or txtTDate.Text = "") And _
                        (txtSize.Text = "" Or txtSize2.Text = "") And _
                        cboSrchRegion.SelectedItem.Value = "" And _
                        cboCity.SelectedItem.Value = "" And _
                        cboMgr.SelectedItem.Value = "" And _
                        cboSrchState.SelectedItem.Value = "" And cboSrchDist.SelectedItem.Value = "" Then
            blnGoSearch = False
        End If

        'StoreNum (1) 
        If txtSrchStoNum.Text > "" Then
            If Not IsNumeric(txtSrchStoNum.Text) Then
                pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Store Number must be numeric.  Please check your input.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                txtSrchStoNum.Text = ""
                blnGoSearch = False
            End If
        End If

        If txtSize.Text > "" And txtSize2.Text > "" Then
            If Not IsNumeric(Trim(txtSize.Text)) Or Not IsNumeric(Trim(txtSize2.Text)) Then
                pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Sq. Ft. value must be numeric.  Please check your input.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                blnGoSearch = False
            ElseIf Val(txtSize.Text) > Val(txtSize2.Text) Then
                pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Sq. Ft. From value must be less than To value.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                blnGoSearch = False
            End If
        ElseIf txtSize.Text = "" And txtSize2.Text > "" Then
            pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter beginning range for sq. feet.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            blnGoSearch = False
        ElseIf txtSize.Text > "" And txtSize2.Text = "" Then
            pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter ending range for sq. feet.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            blnGoSearch = False
        End If

        If txtFDate.Text = "" And txtTDate.Text > "" Then
            pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter From Date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            blnGoSearch = False
        ElseIf txtFDate.Text > "" And txtTDate.Text = "" Then
            pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter To Date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
            blnGoSearch = False
        End If

        If txtFDate.Text > "" And txtTDate.Text > "" And blnGoSearch Then
            'DATE VALIDATION
            blnSAOpenDate = True
            blnGoSearch = True

            strFromDate = txtFDate.Text    'assigning info to strFromDate
            strThruDate = txtTDate.Text    'assigning info to strThruDate

            If Not IsDate(strFromDate) Then
                pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter valid date in From date box.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                blnGoSearch = False
                blnSAOpenDate = False
            End If

            If Not IsDate(strThruDate) Then
                pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Please enter valid date in To date box.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                blnGoSearch = False
                blnSAOpenDate = False
            End If

            If (IsDate(strFromDate) And IsDate(strThruDate)) And (blnGoSearch And blnSAOpenDate) Then
                If CDate(txtFDate.Text) > CDate(txtTDate.Text) Then
                    'Not allowing searching with future dates.
                    pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("From date cannot be later than Through date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                    blnGoSearch = False
                    blnSAOpenDate = False
                ElseIf cbStaging.Checked = False And Date.Compare(strThruDate, Now.AddDays(3).ToShortDateString) > 0 Then
                    'Not allowing searching with future dates.
                    pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("Existing stores cannot be searched with dates 4 days past the current date.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                    blnGoSearch = False
                    blnSAOpenDate = False
                ElseIf Date.Compare(strFromDate, strThruDate) > 0 Then
                    'switching date around so they are in correct order.
                    dteNewstrThruDate = strFromDate
                    strFromDate = strThruDate
                    strThruDate = dteNewstrThruDate
                    'lblError.Text = ""
                End If
            Else  'one of the dates is not a valid date
                pageBody.Attributes.Add("onload", "javascript:window.showModalDialog('Error.aspx?Err=" & Server.UrlEncode("The date information you entered is invalid.") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=150px;');")
                blnGoSearch = False
                blnSAOpenDate = False
            End If

            'ElseIf (lblError.Text = "") Then 'everything passed date validation.  Call Search procedure
            'lblError.Text = ""
        End If

        'If all fields are not empty
        If blnGoSearch Or mstrSearch > "" Then
            'commands
            strCommandString = "SELECT StoreNum AS 'Store Number', StoreName AS 'Store Name', Email, City, State, Region, District, Tags, Simon, OpenDate, Manager, Dimension, Address1, Address2, Address3, Phone1"
            strCommandString &= " FROM STORE"
            strCommandString &= " WHERE (Deleted = 0"
            strCommandString &= " or Deleted is NULL)"

            'If the Company name choice is a Staging store do not use this line
            If cbStaging.Checked Then
                strCommandString &= " AND (StagingStore = 1)"
                'strCommandString &= " OR StoreName LIKE '%staging%')"
            Else
                strCommandString &= " AND (OpenDate <= GETDATE()+7)"
                strCommandString &= " AND (StagingStore = 0)"
            End If

            If blnGoSearch Then
                If IsNumeric(txtSrchStoNum.Text) Then 'QA'd
                    strCommandString &= " AND StoreNum = " & Val(txtSrchStoNum.Text)
                End If

                If txtSrchStoName.Text > "" Then 'QA'd
                    strCommandString &= " AND StoreName LIKE '%" & ParseQuotes(txtSrchStoName.Text) & "%'"
                End If

                If cboSrchState.SelectedItem.Value > "" Then
                    strCommandString &= " AND State LIKE '" & ParseQuotes(cboSrchState.SelectedItem.Value) & "'"
                End If

                If blnSAOpenDate Then
                    strCommandString &= " AND OpenDate BETWEEN '" & strFromDate & "' AND '" & strThruDate & "'"
                End If

                If cboSrchRegion.SelectedItem.Value > "" Then 'QA'd
                    strCommandString &= " AND Region LIKE '" & ParseQuotes(cboSrchRegion.SelectedItem.Value) & "'"
                End If

                If Val(cboSrchDist.SelectedItem.Value) > 0 Then 'QA'd
                    strCommandString &= " AND District = " & Val(cboSrchDist.SelectedItem.Value)
                End If

                If cboCity.SelectedItem.Value > "" Then
                    strCommandString &= " AND City = '" & ParseQuotes(cboCity.SelectedItem.Value) & "'"
                End If

                If cboCoName.SelectedItem.Value > "" Then
                    strCommandString &= " AND CompanyName = '" & ParseQuotes(cboCoName.SelectedItem.Value) & "'"
                End If

                If cboMgr.SelectedItem.Value > "" Then
                    strCommandString &= " AND Manager = '" & ParseQuotes(cboMgr.SelectedItem.Value) & "'"
                End If

                If Val(txtSize.Text) > 0 Or Val(txtSize2.Text) > 0 Then
                    strCommandString &= " AND Dimension BETWEEN " & Val(txtSize.Text) & " AND " & Val(txtSize2.Text)
                End If
            Else
                strCommandString &= " AND (StoreName LIKE '%" & ParseQuotes(mstrSearch) & "%'"
                strCommandString &= " OR State = '" & ParseQuotes(mstrSearch) & "'"
                strCommandString &= " OR Region = '" & ParseQuotes(mstrSearch) & "'"
                strCommandString &= " OR City LIKE '%" & ParseQuotes(mstrSearch) & "%'"
                strCommandString &= " OR Manager LIKE '%" & ParseQuotes(mstrSearch) & "%'"

                If Val(mstrSearch) Then 'QA'd
                    strCommandString &= " OR StoreNum = " & Val(mstrSearch)
                    strCommandString &= " OR District = " & Val(mstrSearch)
                    strCommandString &= " OR Dimension BETWEEN " & Val(mstrSearch) & " AND " & Val(mstrSearch)
                End If

                If IsDate(mstrSearch) Then
                    strCommandString &= " OR OpenDate BETWEEN '" & mstrSearch & "' AND '" & mstrSearch & "'"
                End If

                strCommandString &= ")"
            End If

            dgSearch.DataSource = objDAL.SelectSQL(strCommandString, strConnHeaderOpen)
            If dgSearch.DataSource.rows.count = 0 Then
                lblNoRecords.Text = "0 records returned"
                lblNoRecords.Visible = True
            End If
            dgSearch.DataBind()
            dgSearch.Visible = True
        End If

    End Sub

    Private Function strConnHeaderOpen() As String

        Call CoNameChoice()

        Select Case cboCoName.SelectedItem.Text
            Case "Hot Topic"
                'connection
                Return "strStoreSQLConn1"
            Case "Torrid"
                Return "strStoreSQLConn2"

        End Select

    End Function

    ' Populates the State Drop Down List
    Private Sub build_DDL_State()

        Dim objListItem As ListItem
        Dim strCommandString As String
        Dim strState As String

        If cboSrchState.SelectedIndex > 0 Then
            strState = cboSrchState.SelectedItem.Text
        End If

        'commands
        strCommandString = "SELECT DISTINCT State FROM STORE"
        strCommandString &= " WHERE (DELETED = 0 or DELETED IS NULL)"
        strCommandString &= " AND State > ''"

        If cboCity.SelectedIndex > 0 Then
            strCommandString &= " AND City = '" & cboCity.SelectedItem.Text & "'"
        End If

        If cboSrchDist.SelectedIndex > 0 Then
            strCommandString &= " AND District = '" & cboSrchDist.SelectedItem.Text & "'"
        ElseIf cboSrchRegion.SelectedIndex > 0 Then
            strCommandString &= " AND Region = '" & cboSrchRegion.SelectedItem.Text & "'"
        End If

        If cboMgr.SelectedIndex > 0 Then
            strCommandString &= " AND Manager = '" & cboMgr.SelectedItem.Text & "'"
        End If

        strCommandString &= " order by State ASC "  'added sort states by Lan 11/30/2010
        'Get the one table from the objDataSet
        objDataTable = objDAL.SelectSQL(strCommandString, strConnHeaderOpen)

        cboSrchState.DataSource = objDataTable.DefaultView
        cboSrchState.DataTextField = "State"
        cboSrchState.DataValueField = "State"
        cboSrchState.DataBind()
        cboSrchState.Items.Insert(0, "")

        objListItem = cboSrchState.Items.FindByText(strState)

        If Not IsNothing(objListItem) Then
            objListItem.Selected = True
        ElseIf cboSrchState.Items.Count = 2 Then
            cboSrchState.SelectedIndex = 1
        End If

    End Sub

    ' Populates the City Drop Down List
    Private Sub build_DDL_City()

        Dim objListItem As ListItem
        Dim strCommandString As String
        Dim strCity As String

        If cboCity.SelectedIndex > 0 Then
            strCity = cboCity.SelectedItem.Text
        End If

        'commands
        strCommandString = "SELECT DISTINCT (City) FROM STORE"
        strCommandString &= " WHERE (DELETED = 0 or DELETED IS NULL)"
        strCommandString &= " AND City > ''"

        If cboSrchDist.SelectedIndex > 0 Then
            strCommandString &= " AND District = '" & cboSrchDist.SelectedItem.Text & "'"
        ElseIf cboSrchRegion.SelectedIndex > 0 Then
            strCommandString &= " AND Region = '" & cboSrchRegion.SelectedItem.Text & "'"
        End If

        If cboMgr.SelectedIndex > 0 Then
            strCommandString &= " AND Manager = '" & cboMgr.SelectedItem.Text & "'"
        End If

        'Get the one table from the objDataSet
        objDataTable = objDAL.SelectSQL(strCommandString, strConnHeaderOpen)

        cboCity.DataSource = objDataTable.DefaultView
        cboCity.DataTextField = "City"
        cboCity.DataValueField = "City"
        cboCity.DataBind()
        cboCity.Items.Insert(0, "")

        objListItem = cboCity.Items.FindByText(strCity)

        If Not IsNothing(objListItem) Then
            objListItem.Selected = True
        ElseIf cboCity.Items.Count = 2 Then
            cboCity.SelectedIndex = 1
        End If

    End Sub

    Private Sub build_DDL_City_by_State()

        Dim objListItem As ListItem
        Dim strCommandString As String
        Dim strCity As String

        If cboCity.SelectedIndex > 0 Then
            strCity = cboCity.SelectedItem.Text
        End If

        'commands
        strCommandString = "SELECT DISTINCT(City) FROM STORE WHERE State = '" & cboSrchState.SelectedItem.Text & "'"

        If cboSrchDist.SelectedIndex > 0 Then
            strCommandString &= " AND District = '" & cboSrchDist.SelectedItem.Text & "'"
        ElseIf cboSrchRegion.SelectedIndex > 0 Then
            strCommandString &= " AND Region = '" & cboSrchRegion.SelectedItem.Text & "'"
        End If

        If cboMgr.SelectedIndex > 0 Then
            strCommandString &= " AND Manager = '" & cboMgr.SelectedItem.Text & "'"
        End If

        'Get the one table from the objDataSet
        objDataTable = objDAL.SelectSQL(strCommandString, strConnHeaderOpen)

        cboCity.DataSource = objDataTable.DefaultView
        cboCity.DataTextField = "City"
        cboCity.DataValueField = "City"
        cboCity.DataBind()
        cboCity.Items.Insert(0, "")

        objListItem = cboCity.Items.FindByText(strCity)

        If Not IsNothing(objListItem) Then
            objListItem.Selected = True
        ElseIf cboCity.Items.Count = 2 Then
            cboCity.SelectedIndex = 1
        End If

    End Sub

    Private Sub build_DDL_Region()

        Dim objListItem As ListItem
        Dim strCommandString As String
        Dim strRegion As String

        If cboSrchRegion.SelectedIndex > 0 Then
            strRegion = cboSrchRegion.SelectedItem.Text
        End If

        'commands
        strCommandString = "SELECT DISTINCT Region FROM STORE"
        strCommandString &= " WHERE (DELETED = 0 or DELETED IS NULL)"
        strCommandString &= " AND Region > ''"

        If cboCity.SelectedIndex > 0 Then
            strCommandString &= " AND City = '" & cboCity.SelectedItem.Text & "'"
        ElseIf cboSrchState.SelectedIndex > 0 Then
            strCommandString &= " AND State = '" & cboSrchState.SelectedItem.Text & "'"
        End If

        If cboSrchDist.SelectedIndex > 0 Then
            strCommandString &= " AND District = '" & cboSrchDist.SelectedItem.Text & "'"
        End If

        If cboMgr.SelectedIndex > 0 Then
            strCommandString &= " AND Manager = '" & cboMgr.SelectedItem.Text & "'"
        End If

        'Get the one table from the objDataSet
        objDataTable = objDAL.SelectSQL(strCommandString, strConnHeaderOpen)

        cboSrchRegion.DataSource = objDataTable.DefaultView
        cboSrchRegion.DataTextField = "Region"
        cboSrchRegion.DataBind()
        cboSrchRegion.Items.Insert(0, "")

        objListItem = cboSrchRegion.Items.FindByText(strRegion)

        If Not IsNothing(objListItem) Then
            objListItem.Selected = True
        ElseIf cboSrchRegion.Items.Count = 2 Then
            cboSrchRegion.SelectedIndex = 1
        End If

    End Sub

    Private Sub build_DDL_District()

        Dim objListItem As ListItem
        Dim strCommandString As String
        Dim strDistrict As String

        If cboSrchDist.SelectedIndex > 0 Then
            strDistrict = cboSrchDist.SelectedItem.Text
        End If

        'commands
        strCommandString = "SELECT DISTINCT District FROM STORE"
        strCommandString &= " WHERE (DELETED = 0 or DELETED IS NULL)"
        strCommandString &= " AND District > ''"

        If cboCity.SelectedIndex > 0 Then
            strCommandString &= " AND City = '" & cboCity.SelectedItem.Text & "'"
        ElseIf cboSrchState.SelectedIndex > 0 Then
            strCommandString &= " AND State = '" & cboSrchState.SelectedItem.Text & "'"
        End If

        If cboMgr.SelectedIndex > 0 Then
            strCommandString &= " AND Manager = '" & cboMgr.SelectedItem.Text & "'"
        End If

        'Get the one table from the objDataSet
        objDataTable = objDAL.SelectSQL(strCommandString, strConnHeaderOpen)

        cboSrchDist.DataSource = objDataTable.DefaultView
        cboSrchDist.DataTextField = "District"
        cboSrchDist.DataBind()
        cboSrchDist.Items.Insert(0, "")

        objListItem = cboSrchDist.Items.FindByText(strDistrict)

        If Not IsNothing(objListItem) Then
            objListItem.Selected = True
        ElseIf cboSrchDist.Items.Count = 2 Then
            cboSrchDist.SelectedIndex = 1
        End If

    End Sub

    ' 
    Private Sub build_DDL_District_by_Region()

        Dim objListItem As ListItem
        Dim strCommandString As String
        Dim strDistrict As String

        'commands
        If cboSrchDist.SelectedIndex > 0 Then
            strDistrict = cboSrchDist.SelectedItem.Text
        End If

        strCommandString = "SELECT DISTINCT(District) FROM STORE WHERE Region = '" & cboSrchRegion.SelectedItem.Text & "'"

        If cboCity.SelectedIndex > 0 Then
            strCommandString &= " AND City = '" & cboCity.SelectedItem.Text & "'"
        ElseIf cboSrchState.SelectedIndex > 0 Then
            strCommandString &= " AND State = '" & cboSrchState.SelectedItem.Text & "'"
        End If

        If cboMgr.SelectedIndex > 0 Then
            strCommandString &= " AND Manager = '" & cboMgr.SelectedItem.Text & "'"
        End If

        'Get the one table from the objDataSet
        objDataTable = objDAL.SelectSQL(strCommandString, strConnHeaderOpen)

        cboSrchDist.DataSource = objDataTable.DefaultView
        cboSrchDist.DataTextField = "District"
        cboSrchDist.DataBind()
        cboSrchDist.Items.Insert(0, "")

        objListItem = cboSrchDist.Items.FindByText(strDistrict)

        If Not IsNothing(objListItem) Then
            objListItem.Selected = True
        ElseIf cboSrchDist.Items.Count = 2 Then
            cboSrchDist.SelectedIndex = 1
        End If

    End Sub

    Private Sub build_DDL_Mgr()

        Dim objListItem As ListItem
        Dim strCommandString As String
        Dim strManager As String

        If cboMgr.SelectedIndex > 0 Then
            strManager = cboMgr.SelectedItem.Text
        End If

        'commands
        strCommandString = "SELECT DISTINCT Manager FROM STORE"
        strCommandString &= " WHERE (DELETED = 0 or DELETED IS NULL)"
        strCommandString &= " AND Manager > ''"

        If cboCity.SelectedIndex > 0 Then
            strCommandString &= " AND City = '" & cboCity.SelectedItem.Text & "'"
        ElseIf cboSrchState.SelectedIndex > 0 Then
            strCommandString &= " AND State = '" & cboSrchState.SelectedItem.Text & "'"
        End If

        If cboSrchDist.SelectedIndex > 0 Then
            strCommandString &= " AND District = '" & cboSrchDist.SelectedItem.Text & "'"
        ElseIf cboSrchRegion.SelectedIndex > 0 Then
            strCommandString &= " AND Region = '" & cboSrchRegion.SelectedItem.Text & "'"
        End If

        'Get the one table from the objDataSet
        objDataTable = objDAL.SelectSQL(strCommandString, strConnHeaderOpen)

        cboMgr.DataSource = objDataTable.DefaultView
        cboMgr.DataTextField = "Manager"
        cboMgr.DataValueField = "Manager"
        cboMgr.DataBind()
        cboMgr.Items.Insert(0, "")

        objListItem = cboSrchState.Items.FindByText(strManager)

        If Not IsNothing(objListItem) Then
            objListItem.Selected = True
        ElseIf cboMgr.Items.Count = 2 Then
            cboMgr.SelectedIndex = 1
        End If

    End Sub

    Function ParseQuotes(ByVal ParseText As String) As String

        Return Replace(Trim(ParseText), "'", "''")

    End Function

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click

        dgSearch.Visible = False

        'Num (1)
        txtSrchStoNum.Text = ""
        'Name(2)
        txtSrchStoName.Text = ""
        'Addr (3)
        'City (4)
        cboCity.SelectedIndex = 0
        'Region 
        cboSrchRegion.SelectedIndex = 0
        'District 
        cboSrchDist.SelectedIndex = 0
        'Phone1 (7) 
        txtFDate.Text = ""
        txtTDate.Text = ""
        'Dim (11)
        'State
        cboSrchState.SelectedIndex = 0
        'Reg (12) 
        cboSrchRegion.SelectedIndex = 0
        'District (13) 
        cboSrchDist.SelectedIndex = 0
        'GM (14) 
        cboMgr.SelectedIndex = 0
        'Size range
        txtSize.Text = ""
        txtSize2.Text = ""

		'build_DDL_State()
		'build_DDL_City()
		'build_DDL_Region()
		'build_DDL_District()
		'build_DDL_Mgr()

    End Sub

    Private Sub cboSrchState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSrchState.SelectedIndexChanged

        If cboSrchState.SelectedIndex = 0 Then
            Call build_DDL_City()
        Else
            Call build_DDL_City_by_State()
        End If

        Call build_DDL_Region()

        If cboSrchRegion.SelectedIndex > 0 Then
            Call build_DDL_District_by_Region()
        Else
            Call build_DDL_District()
        End If

        Call build_DDL_Mgr()

    End Sub

    ' Will send user back to the home page.
    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        'Session("pEmployee") = 0
        'Response.Redirect("..\home.asp")
        Response.Redirect("/home/home.aspx")

    End Sub

    Private Sub cboSrchRegion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSrchRegion.SelectedIndexChanged

        If cboSrchRegion.SelectedIndex = 0 Then
            Call build_DDL_District()
        Else
            Call build_DDL_District_by_Region()
        End If

        Call build_DDL_State()

        If cboSrchState.SelectedIndex > 0 Then
            Call build_DDL_City_by_State()
        Else
            Call build_DDL_City()
        End If

        Call build_DDL_Mgr()

    End Sub

    ' The user will be forwarded to  StoreDetail.aspx with form to add a store
    Private Sub btnAddStore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddStore.Click

        ' Checks If Employee is Logged in
        If pEmp <= 0 Then
            Response.Redirect("Login_Store.aspx")
        End If

        CoNameChoice()
        Response.Redirect("StoreDetail.aspx?pCoNameChoice=" & pCoNameChoice & "")

    End Sub

    ' With this method even occurs the form will be reset for the new input for the proper store.
    Private Sub cboCoName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCoName.SelectedIndexChanged

        txtSrchStoNum.Text = ""
        txtSrchStoName.Text = ""
        cbStaging.Checked = False
        cboCity.SelectedIndex = 0
        cboSrchRegion.SelectedIndex = 0
        cboSrchDist.SelectedIndex = 0
        txtFDate.Text = ""
        txtTDate.Text = ""
        txtSize.Text = ""
        txtSize2.Text = ""

        'populate pull downs
        build_DDL_State()
        build_DDL_Region()
        build_DDL_District()
        build_DDL_City()
        build_DDL_Mgr()

    End Sub

    Private Sub cboCity_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCity.SelectedIndexChanged

        Call build_DDL_State()
        Call build_DDL_Region()

        If cboSrchDist.SelectedIndex > 0 Then
            Call build_DDL_District_by_Region()
        Else
            Call build_DDL_District()
        End If

        Call build_DDL_Mgr()

    End Sub

    Private Sub cboSrchDist_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSrchDist.SelectedIndexChanged

        Call build_DDL_State()

        If cboSrchState.SelectedIndex > 0 Then
            Call build_DDL_City_by_State()
        Else
            Call build_DDL_City()
        End If

        Call build_DDL_Region()
        Call build_DDL_Mgr()

    End Sub

    Private Sub cboMgr_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMgr.SelectedIndexChanged

        Call build_DDL_State()

        If cboSrchState.SelectedIndex > 0 Then
            Call build_DDL_City_by_State()
        Else
            Call build_DDL_City()
        End If

        Call build_DDL_Region()

        If cboSrchDist.SelectedIndex > 0 Then
            Call build_DDL_District_by_Region()
        Else
            Call build_DDL_District()
        End If

    End Sub

    Private Sub dgSearch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgSearch.SelectedIndexChanged

    End Sub
End Class
