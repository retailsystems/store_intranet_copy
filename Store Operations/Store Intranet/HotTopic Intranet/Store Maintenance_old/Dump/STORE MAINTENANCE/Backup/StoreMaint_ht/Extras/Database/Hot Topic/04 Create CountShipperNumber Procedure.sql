CREATE PROCEDURE spCountShipperNumber
(
@StoNum Int,
@ShipperNumber Varchar(10),
@Count Int Output
)
AS 
SELECT @Count = Count(StoreNum) FROM STORE
WHERE ShipperNumber = @ShipperNumber
AND StoreNum <> @StoNum
GO
