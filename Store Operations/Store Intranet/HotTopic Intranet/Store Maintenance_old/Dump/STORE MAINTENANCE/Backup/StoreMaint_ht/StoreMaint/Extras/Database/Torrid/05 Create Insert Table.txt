CREATE TABLE STAGING_STORE(
	StageID INTEGER PRIMARY KEY,
	SSName VARCHAR(20),
	StagingStore BIT,
	Address1 VARCHAR(35),
	Address2 VARCHAR(35),
	City VARCHAR(35),
	State CHAR(2),
	Zip VARCHAR(5),
	Country CHAR(2),
	Phone1 VARCHAR(15),
	Phone2 VARCHAR(15),
	ShipperNumber VARCHAR(10),
	MyUPS_ID INT,
	Service_DC VARCHAR(10),
	WAN BIT,
	CD BIT);

INSERT INTO STAGING_STORE VALUES (1, 'Staging', 1, '18305 E. San Jose Ave.','', 'City of Industry', 'CA', 91748, 'US', '(626)839-4681','', '90118E', 5, 'CADC', 1, 0) 
	