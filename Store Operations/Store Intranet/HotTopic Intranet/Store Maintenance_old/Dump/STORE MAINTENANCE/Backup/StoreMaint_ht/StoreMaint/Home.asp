<!-- #include file="globalconst.asp" -->
<!-- #include file="gfunctions.asp" -->
<!-- #include file="gheader.asp" -->
<%

Response.Buffer = True
Response.Expires = -100

Dim mstrHome
Dim mstrMode

mstrHome = Request.Cookies("User")("Home")
mstrMode = Request.QueryString("Mode")

Private Sub DisplayPage()
	
	If mstrMode = "Logoff" Then 
		Call gClearAllCookies()
		Call Response.Redirect("Home.asp")
	End If
	
	Select Case mstrHome
	
	Case "A"
		If Request.Cookies("User")("ADMINHome")="Y" then
			Call gShowHeader("Administration Menu")
			Call ShowOptions_2()
		Else
			Call Response.Redirect("loginerror.asp")
		End If
		
	Case "S"
		If Request.Cookies("User")("SUPPLYORDERHome")="Y" then
			Call gShowHeader("Supply Order Menu")
			Call ShowOptions_3()
		Else
			Call Response.Redirect("loginerror.asp")
		End If
		
	Case Else
		Call gShowHeader("Main Menu")
		Call ShowOptions()
	
	End Select
	
	
	
End Sub

Private Sub ShowOptions()
	
	Response.Write "<table border='0' cellspacing='0' cellpadding='0' width='750'>"
	Response.Write "<tr><td>&nbsp;</td></tr>"

	Response.Write "<tr><td align='left'><a href='Forms.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Forms</b></font></a></td></tr>"
    'Response.Write "<tr><td align='left'><a href='PhoneLists.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Phone Lists</b></font></a></td></tr>"
    Response.Write "<tr><td align='left'><a href='PLU_TD/PLUSearch.aspx'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>SKU Search</b></font></a></td></tr>"
	Response.Write "<tr><td align='left'><a href='1inventory_new.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Product Locator</b></font></a></td></tr>"
    Response.Write "<tr><td align='left'><a href='empLogin.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Reports</b></font></a></td></tr>"
    Response.Write "<tr><td align='left'><a href='SOP.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>SOP Manual</b></font></a></td></tr>"
'    Response.Write "<tr><td align='left'><a href='#' onclick='javascript:window.open(""http://172.16.1.23/torrid/SStransfer/default.aspx"",""SSTransfer"",""fullscreen"");'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Store To Store Transfer</b></font></a></td></tr>"    
    Response.Write "<tr><td align='left'><a href='http://172.16.4.103/StoreMaint/StoreMaint.aspx'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Store Lookup</b></font></a></td></tr>"
'   Response.Write "<tr><td align='left'><a href='#' onclick='javascript:window.open(""http://172.16.4.103/SStransfer/StoreMaint.aspx"",""SSTransfer"");'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Store Lookup</b></font></a></td></tr>"    
'Response.Write"<tr><td align='left'><a href='Storelist.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Store List</b></font></a></td></tr>"
    Response.Write "<tr><td align='center'>&nbsp;</font></td></tr>"
	'Response.Write "<tr><td align='left'><a href='Login.asp?Home=S'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>DM</b></font></a></td></tr>"
	'Response.Write "<tr><td align='left'><a href='Login.asp?Home=S'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Regional</b></font></a></td></tr>"
	Response.Write "<tr><td align='left'><a href='Login.asp?Home=S'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Supply Order Form   (Store Management)</b></font></a></td></tr>"
    Response.Write "<tr><td align='right'><a href='Login.asp?Home=A'><font face='" & gcMainFontFace & "' color='white' size='-2'><b>Admin Use Only</b></font></a></td></tr>"	
	'Response.Write "<tr><td align='right'><a href='mailto:helpdesk@hottopic.com'><font face='" & gcMainFontFace & "' color='white' size='-2'><b>Contact Helpdesk</b></font></a></td></tr>"	
	Response.Write "<tr><td>&nbsp;</td></tr>"
	Response.Write "<tr><td><hr color='" & gcPageLineColor & "'></td></tr>"
	Response.Write "</table>"
	
End Sub


Private Sub ShowOptions_2()
	
	Response.Write "<table border='0' cellspacing='0' cellpadding='0' width='750'>"
	Response.Write "<tr><td>&nbsp;</td></tr>"
	Response.Write "<tr><td align='left'><a href='SOP.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>SOP Manual</b></font></a></td></tr>"
	Response.Write "<tr><td align='left'><a href='Forms.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Forms</b></font></a></td></tr>"
	Response.Write "<tr><td align='left'><a href='1inventory_new.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Product Locator</b></font></a></td></tr>"
	
	Response.Write "<tr><td align='left'>&nbsp;</font></td></tr>"
	Response.Write "<tr><td align='left'><a href='SOCats.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Supply Order Form</b></font></a></td></tr>"
	If Request.Cookies("User")("SOR")="Y" then
		Response.Write "<tr><td align='left'><a href='SOSearch.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Supply Order Search</b></font></a></td></tr>"
	End If
	If Request.Cookies("User")("SOR")="Y" then
		Response.Write "<tr><td align='left'><a href='SOReview.asp?CP=0&Sort=1&SortType=Asc'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Supply Order Review</b></font></a></td></tr>"
	End If
	
	Response.Write "<tr><td align='center'>&nbsp;</font></td></tr>"
	If Request.Cookies("User")("MAINTENANCE")="Y" then
		Response.Write "<tr><td align='left'><a href='UserMaint.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>User Maintenance</b></font></a></td></tr>"
		Response.Write "<tr><td align='left'><a href='PartMaint.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Part Maintenance</b></font></a></td></tr>"
		Response.Write "<tr><td align='left'><a href='ListDtlMaint.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>List Detail Maintenance</b></font></a></td></tr>"
	End If

	Response.Write "<tr><td align='center'>&nbsp;</font></td></tr>"
	If Request.Cookies("User")("StoreMaint")="Y" then
		Response.Write "<tr><td align='left'><a href='http://172.16.4.103/StoreMaint/Login_Store.aspx'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Store Maintenance</b></font></a></td></tr>"
	End If
	
	Response.Write "<tr><td>&nbsp;</td></tr>"
	Response.Write "<tr><td><hr color='" & gcPageLineColor & "'></td></tr>"
	
	Response.Write "</table>"
	
End Sub

Private Sub ShowOptions_3()
	
	Response.Write "<table border='0' cellspacing='0' cellpadding='0' width='750'>"

	Response.Write "<tr><td align='left'>&nbsp;</font></td></tr>"
	'need link for reports page for store management, dm's, and rm's.
	'Response.Write "<tr><td align='left'><a href='empLogin.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Reports</b></font></a></td></tr>"
	Response.Write "<tr><td align='left'><a href='SOCats.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Supply Order Form</b></font></a></td></tr>"
	'Response.Write "<tr><td align='left'><a href='SOSearch.asp'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Supply Order Search</b></font></a></td></tr>"
	If Request.Cookies("User")("SOSummary")="Y" then
		Response.Write "<tr><td align='left'><a href='SOSummary.asp?CP=0&Sort=1&SortType=Asc'><font face='" & gcMainFontFace & "' color='white' size='-1'><b>Supply Order Summary</b></font></a></td></tr>"
	End If

	Response.Write "<tr><td>&nbsp;</td></tr>"
	Response.Write "<tr><td><hr color='" & gcPageLineColor & "'></td></tr>"
	
	Response.Write "</table>"
	

End Sub

%>

<html>
<head>
	<title>Home</title>
	

<%Call gGetStyles()%>
</head>

<body bgcolor='<%=gcBGColor%>'  bgproperties='fixed' >
<form action='' method='POST' name='frmHome'>
	<%Call DisplayPage%>
</form>
</body>
</html>
