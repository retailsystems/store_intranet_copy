<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="StoreSearch.aspx.vb" Inherits="StoreMaint.StoreSearch" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WebForm1</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript">
			<!--
			javascript:window.history.forward(1);
			//-->
		</script>
	</HEAD>
	<body bgColor="black" leftMargin="0" topMargin="0">
		<div class="container home" role="main">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" id="Main" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD height="1"></TD>
				</TR>
				<TR valign="top">
					<TD>
						<TABLE id="Table1" style="BORDER-TOP-WIDTH: thin; BORDER-LEFT-WIDTH: thin; BORDER-LEFT-COLOR: #cc3366; BORDER-BOTTOM-WIDTH: thin; BORDER-BOTTOM-COLOR: #cc3366; BORDER-TOP-COLOR: #cc3366; HEIGHT: 161px; BORDER-RIGHT-WIDTH: thin; BORDER-RIGHT-COLOR: #cc3366" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
							<TR>
								<TD style="WIDTH: 131px" align="right">
									<asp:label id="lblStoNum" runat="server" Font-Size="Smaller" Font-Names="Arial" ForeColor="White" BackColor="Transparent" Font-Bold="True">Store Number:</asp:label></TD>
								<TD style="WIDTH: 245px">
									<asp:textbox id="txtSrchStoNum" runat="server" Width="77px" MaxLength="5" tabIndex="1"></asp:textbox></TD>
								<TD style="WIDTH: 206px" align="right"><SPAN>
										<asp:label id="lblSrchOpenDate" runat="server" Font-Size="Smaller" Font-Names="Arial" ForeColor="White" BackColor="Transparent" Font-Bold="True">Open Date:</asp:label></SPAN></TD>
								<TD style="WIDTH: 282px">
									<asp:textbox id="txtSrchOpenDate" runat="server" Width="194px" tabIndex="5"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 131px" align="right">
									<asp:label id="lblStoName" runat="server" Font-Size="Smaller" Font-Names="Arial" ForeColor="White" BackColor="Transparent" Font-Bold="True">Store Name:</asp:label></TD>
								<TD style="WIDTH: 245px">
									<asp:textbox id="txtSrchStoName" runat="server" Width="194px" tabIndex="2"></asp:textbox></TD>
								<TD style="WIDTH: 206px" align="right"><SPAN>
										<asp:label id="lblSrchRegion" runat="server" Font-Size="Smaller" Font-Names="Arial" ForeColor="White" BackColor="Transparent" Font-Bold="True">Region :</asp:label></SPAN></TD>
								<TD style="WIDTH: 282px">
									<asp:dropdownlist id="cboSrchRegion" runat="server" Width="73px" tabIndex="6"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 131px; HEIGHT: 19px" align="right">
									<asp:label id="lbSrchState" runat="server" Font-Size="Smaller" Font-Names="Arial" ForeColor="White" BackColor="Transparent" Font-Bold="True">State:</asp:label></TD>
								<TD style="WIDTH: 245px; HEIGHT: 19px">
									<asp:dropdownlist id="cboSrchState" runat="server" Width="73px" tabIndex="3"></asp:dropdownlist></TD>
								<TD style="WIDTH: 206px; HEIGHT: 19px" align="right">
									<asp:label id="lblSrchDist" runat="server" Font-Size="Smaller" Font-Names="Arial" ForeColor="White" BackColor="Transparent" Font-Bold="True">District :</asp:label></TD>
								<TD style="WIDTH: 282px; HEIGHT: 19px">
									<asp:dropdownlist id="cboSrchDist" runat="server" Width="73px" tabIndex="7"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 131px; HEIGHT: 13px" align="right">
									<asp:label id="lblSrchEmail" runat="server" Font-Size="Smaller" Font-Names="Arial" ForeColor="White" BackColor="Transparent" Font-Bold="True">Email:</asp:label></TD>
								<TD style="WIDTH: 245px; HEIGHT: 13px">
									<asp:textbox id="txtSrchEmail" runat="server" Width="239px" tabIndex="4"></asp:textbox></TD>
								<TD style="WIDTH: 206px; HEIGHT: 13px" align="right">
									<asp:label id="lblSrchTags" runat="server" Font-Size="Smaller" Font-Names="Arial" ForeColor="White" BackColor="Transparent" Font-Bold="True">Tag :</asp:label></TD>
								<TD style="WIDTH: 282px; HEIGHT: 13px">
									<asp:textbox id="txtSrchTag" runat="server" Width="19px" tabIndex="8"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:label id="lblSrchSimon" runat="server" Font-Size="Smaller" Font-Names="Arial" ForeColor="White" BackColor="Transparent" Font-Bold="True">Simon :</asp:label>
									<asp:textbox id="txtSrchSimon" runat="server" Width="19px" tabIndex="9"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 131px; HEIGHT: 19px" align="right"></TD>
								<TD style="WIDTH: 245px; HEIGHT: 19px">
									<asp:label id="lblEmpty" runat="server" Font-Size="Smaller" Font-Names="Arial" ForeColor="Red" BackColor="Transparent" Font-Bold="True">Generic</asp:label></TD>
								<TD style="WIDTH: 206px; HEIGHT: 19px" align="right">&nbsp;</TD>
								<TD style="WIDTH: 282px; HEIGHT: 19px"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 131px; HEIGHT: 19px" align="right"></TD>
								<TD style="WIDTH: 245px; HEIGHT: 19px">
									<asp:label id="lblNoResults" runat="server" Font-Size="Smaller" Font-Names="Arial" ForeColor="Red" BackColor="Transparent" Font-Bold="True">No Record(s) Found.</asp:label>
									<asp:button id="btnSearch" runat="server" ForeColor="White" BackColor="#440000" Font-Bold="True" Width="93px" BorderColor="#990000" Text="Search"></asp:button></TD>
								<TD style="WIDTH: 206px; HEIGHT: 19px" align="left">
									<asp:button id="btnClear" runat="server" ForeColor="White" BackColor="#440000" Font-Bold="True" Width="95px" BorderColor="#990000" Text="Clear Fields"></asp:button></TD>
								<TD>
									<asp:button id="btnAdd" runat="server" ForeColor="White" BackColor="#440000" Font-Bold="True" Width="95px" BorderColor="#990000" Text="Add Store"></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
		</div>
	</body>
</HTML>
