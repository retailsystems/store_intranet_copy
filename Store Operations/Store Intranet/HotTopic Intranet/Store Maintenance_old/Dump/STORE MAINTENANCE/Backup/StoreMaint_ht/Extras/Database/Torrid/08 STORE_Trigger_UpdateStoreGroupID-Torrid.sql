CREATE TRIGGER STORE_Trigger_UpdateStoreGroupID
ON  dbo.STORE 
FOR UPDATE
AS
/**************************************************************************
**	File: STORE_Trigger_UpdateStoreGroupID.sql
**	Name: STORE_Trigger_UpdateStoreGroupID
**	Desc:  This trigger will Update the Group ID depending on the Country
**
**************************************************************************/
DECLARE @OLDCountry VARCHAR(2)
DECLARE @NEWCountry VARCHAR(2)
DECLARE @StoreNumber INT
SELECT @OLDCountry = (SELECT country FROM Deleted)
SELECT @NEWCountry = (SELECT country FROM Inserted)
SELECT @StoreNumber = (SELECT storenum FROM Inserted)
IF (@OLDCountry = 'US'AND @NEWCountry = 'PR')
BEGIN
	UPDATE SSTransferDB_Torrid.dbo.Store_Group
	SET StoreGroup_ID = 2
	WHERE StoreNum = @StoreNumber
	AND StoreGroup_ID = 1
END
IF (@OLDCountry = 'PR'AND @NEWCountry = 'US')
BEGIN
	UPDATE SSTransferDB_Torrid.dbo.Store_Group
	SET StoreGroup_ID = 1
	WHERE StoreNum = @StoreNumber
	AND StoreGroup_ID = 2
END