<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Login.aspx.vb" Inherits="SSTransfer.Login"%>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Layout/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Layout/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Login</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="Javascript/TrapKeyPress.js" language="JavaScript"></script>
		<script src="Javascript/DisableClientBack.js" language="JavaScript"></script>
		<script src="Javascript/AllowOneSubmit.js" language="JavaScript"></script>
	</HEAD>
	<body id="pageBody" runat="server" bottomMargin="0" vLink="white" aLink="white" link="white" leftMargin="0" topMargin="0" scroll="no" rightMargin="0">
		<form id="frmTransferHome" method="post" runat="server" onsubmit="return checkSubmit();">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" bgColor="#000000" border="0">
				<tr>
					<td height="1"><uc1:header id="ucHeader" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td align="middle">
						<table cellSpacing="2" cellPadding="3" border="0">
							<tr id="trEmpId" runat="server">
								<td><font face="Arial" color="white" size="2"><b>Employee Id:</b></font></td>
								<td><asp:textbox id="txtEmployeeId" tabIndex="1" runat="server" TextMode="Password"></asp:textbox></td>
								<td><asp:button id="btnSubmit" tabIndex="1" runat="server" BackColor="#440000" BorderColor="#990000" ForeColor="White" Text="Submit" Font-Bold="True"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1"><uc1:footer id="ucFooter" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
		<script language="javascript">
		document.forms[0].txtEmployeeId.focus();	
		</script>
	</body>
</HTML>
