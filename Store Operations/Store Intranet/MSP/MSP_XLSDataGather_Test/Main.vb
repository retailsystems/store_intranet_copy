Imports System.Data
Imports System.Data.OleDb
Imports System.IO

Public Class Main
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnTest As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnTest = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnTest
        '
        Me.btnTest.Location = New System.Drawing.Point(8, 8)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(272, 23)
        Me.btnTest.TabIndex = 0
        Me.btnTest.Text = "Run Test"
        '
        'Main
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 37)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.btnTest})
        Me.Name = "Main"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTest.Click

        Dim objAppSettingsReader As New Configuration.AppSettingsReader()
        Dim strXLSFileConn As String = objAppSettingsReader.GetValue("strMSPXLSConn", GetType(System.String))
        Dim strXLSDir As String = objAppSettingsReader.GetValue("strXLSDir", GetType(System.String))
        Dim strCurrentXLSFileConn As String

        Dim objConnection As New SqlClient.SqlConnection(objAppSettingsReader.GetValue("strMSPStoreConn", GetType(System.String)))
        Dim objDataReader As SqlClient.SqlDataReader

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("asFillStore", objConnection)

        objDataReader = objCommand.ExecuteReader()

        Do While objDataReader.Read()

            strCurrentXLSFileConn = Replace(strXLSFileConn, "%ownerID%", objDataReader("StoreNum").ToString.PadLeft(4, "0"))

            If File.Exists(strXLSDir & "Store " & objDataReader("StoreNum").ToString.PadLeft(4, "0") & ".xls") Then
                If ImportXLSData(strCurrentXLSFileConn, objDataReader("StoreNum")) Then
                    UpdateImportLog(objDataReader("StoreNum"))
                End If
            End If

        Loop

        objDataReader.Close()
        objCommand.Dispose()
        objConnection.Close()

    End Sub

    Private Sub UpdateImportLog(ByVal StoreNum As Integer)

        Dim objAppSettingsReader As New Configuration.AppSettingsReader()
        Dim objConnection As New SqlClient.SqlConnection(objAppSettingsReader.GetValue("strMSPConn", GetType(System.String)))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("proc_InsertMSPDataImport", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parStoreNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@Store_Num", SqlDbType.SmallInt)
        Dim parLastImportDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@Last_Import_Date", SqlDbType.DateTime)

        parStoreNum.Value = StoreNum
        parLastImportDate.Value = Now()

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()

    End Sub

    Private Function ImportXLSData(ByVal strDBConn As String, ByVal StoreNum As Integer) As Boolean

        Try

            Dim objAppSettingsReader As New Configuration.AppSettingsReader()
            Dim objConnection As New SqlClient.SqlConnection(objAppSettingsReader.GetValue("strMSPConn", GetType(System.String)))

            Dim CurrentRow As Integer = 0

            Dim objXLSConn As New OleDbConnection(strDBConn)

            objConnection.Open()
            objXLSConn.Open()

            Dim objCmdSelect As New OleDbCommand("SELECT * FROM [A5:Z70]", objXLSConn)
            Dim objAdapter As New OleDbDataAdapter()

            objAdapter.SelectCommand = objCmdSelect

            Dim objDataset As New DataSet()

            objAdapter.Fill(objDataset, "MSP")

            Do While objDataset.Tables(0).Rows.Count > CurrentRow

                If IsDate(objDataset.Tables(0).Rows(CurrentRow).Item(0)) Then

                    Dim objCommand As New SqlClient.SqlCommand("proc_InsertMSPDetail", objConnection)
                    objCommand.CommandType = CommandType.StoredProcedure

                    Dim parStoreNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt)
                    Dim parMSPDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@MSPDate", SqlDbType.DateTime)
                    Dim parDLYSales As SqlClient.SqlParameter = objCommand.Parameters.Add("@DLYSales", SqlDbType.Money)
                    Dim parDPlanSales As SqlClient.SqlParameter = objCommand.Parameters.Add("@DPlanSales", SqlDbType.Money)
                    Dim parDActualSales As SqlClient.SqlParameter = objCommand.Parameters.Add("@DActualSales", SqlDbType.Money)
                    Dim parDActvPlanPlusMinus As SqlClient.SqlParameter = objCommand.Parameters.Add("@DActvPlanPlusMinus", SqlDbType.Money)
                    Dim parDActvPlanPercent As SqlClient.SqlParameter = objCommand.Parameters.Add("@DActvPlanPercent", SqlDbType.Float)
                    Dim parDActvLYPlusMinus As SqlClient.SqlParameter = objCommand.Parameters.Add("@DActvLYPlusMinus", SqlDbType.Money)
                    Dim parDActvLYPercent As SqlClient.SqlParameter = objCommand.Parameters.Add("@DActvLYPercent", SqlDbType.Float)
                    Dim parWTDActualSalesAmt As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDActualSalesAmt", SqlDbType.Money)
                    Dim parWTDPlanSalesAmt As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDPlanSalesAmt", SqlDbType.Money)
                    Dim parWTDActvPlanPlusMinus As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDActvPlanPlusMinus", SqlDbType.Money)
                    Dim parWTDActvPlanPercent As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDActvPlanPercent", SqlDbType.Float)
                    Dim parWTDLYSalesAmt As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDLYSalesAmt", SqlDbType.Money)
                    Dim parWTDActvLYPlusMinus As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDActvLYPlusMinus", SqlDbType.Money)
                    Dim parWTDActvLYPercent As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDActvLYPercent", SqlDbType.Float)
                    Dim parCust As SqlClient.SqlParameter = objCommand.Parameters.Add("@Cust", SqlDbType.Float)
                    Dim parItems As SqlClient.SqlParameter = objCommand.Parameters.Add("@Items", SqlDbType.Float)
                    Dim parUPT As SqlClient.SqlParameter = objCommand.Parameters.Add("@UPT", SqlDbType.Money)
                    Dim parAverage As SqlClient.SqlParameter = objCommand.Parameters.Add("@Average", SqlDbType.Money)
                    Dim parOS As SqlClient.SqlParameter = objCommand.Parameters.Add("@OS", SqlDbType.Money)
                    Dim parModified_Date As SqlClient.SqlParameter = objCommand.Parameters.Add("@Modified_Date", SqlDbType.DateTime)
                    Dim parCreated_Date As SqlClient.SqlParameter = objCommand.Parameters.Add("@Created_Date", SqlDbType.DateTime)

                    parStoreNum.Value = StoreNum
                    parMSPDate.Value = objDataset.Tables(0).Rows(CurrentRow).Item(0)
                    parDLYSales.Value = objDataset.Tables(0).Rows(CurrentRow).Item(1)
                    parDPlanSales.Value = objDataset.Tables(0).Rows(CurrentRow).Item(2)
                    parDActualSales.Value = objDataset.Tables(0).Rows(CurrentRow).Item(3)
                    parDActvPlanPlusMinus.Value = objDataset.Tables(0).Rows(CurrentRow).Item(4)
                    parDActvPlanPercent.Value = objDataset.Tables(0).Rows(CurrentRow).Item(6)
                    parDActvLYPlusMinus.Value = objDataset.Tables(0).Rows(CurrentRow).Item(7)
                    parDActvLYPercent.Value = objDataset.Tables(0).Rows(CurrentRow).Item(9)
                    parWTDActualSalesAmt.Value = objDataset.Tables(0).Rows(CurrentRow).Item(11)
                    parWTDPlanSalesAmt.Value = objDataset.Tables(0).Rows(CurrentRow).Item(12)
                    parWTDActvPlanPlusMinus.Value = objDataset.Tables(0).Rows(CurrentRow).Item(13)
                    parWTDActvPlanPercent.Value = objDataset.Tables(0).Rows(CurrentRow).Item(15)
                    parWTDLYSalesAmt.Value = objDataset.Tables(0).Rows(CurrentRow).Item(16)
                    parWTDActvLYPlusMinus.Value = objDataset.Tables(0).Rows(CurrentRow).Item(17)
                    parWTDActvLYPercent.Value = objDataset.Tables(0).Rows(CurrentRow).Item(19)
                    parCust.Value = objDataset.Tables(0).Rows(CurrentRow).Item(21)
                    parItems.Value = objDataset.Tables(0).Rows(CurrentRow).Item(22)
                    parUPT.Value = objDataset.Tables(0).Rows(CurrentRow).Item(23)
                    parAverage.Value = objDataset.Tables(0).Rows(CurrentRow).Item(24)
                    parOS.Value = objDataset.Tables(0).Rows(CurrentRow).Item(25)
                    parModified_Date.Value = Now()
                    parCreated_Date.Value = Now()

                    objCommand.ExecuteNonQuery()

                    objCommand.Dispose()


                End If

                CurrentRow += 1

            Loop

            objXLSConn.Close()
            objConnection.Close()

            Return True

        Catch
            Return False
        End Try

    End Function

End Class
