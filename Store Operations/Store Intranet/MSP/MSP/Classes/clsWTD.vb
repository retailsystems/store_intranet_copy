Public Class clsWTD

    Private strDBConn As String
    Private strDBConn_NoPool As String
    Private dblTotalDLYSales As Double
    Private dblTotalDPlanSales As Double
    Private dblTotalDActualSales As Double
    Private dblTotalCust As Double
    Private dblTotalItem As Double
    Private dblTotalUPT As Double
    Private dblTotalAverage As Double
    Private dblTotalOS As Double
    Private booConectionError As Boolean
    Private booSPCallError As Boolean

    Public Sub New(ByVal DBConn As String, ByVal DBConn_Alt As String)
        strDBConn = DBConn
        strDBConn_NoPool = DBConn_Alt
    End Sub

    ReadOnly Property ConectionError()
        Get
            Return booConectionError
        End Get
    End Property

    ReadOnly Property SPCallError()
        Get
            Return booSPCallError
        End Get
    End Property


    ReadOnly Property TotalDLYSales()
        Get
            Return dblTotalDLYSales
        End Get
    End Property
    ReadOnly Property TotalDPlanSales()
        Get
            Return dblTotalDPlanSales
        End Get
    End Property
    ReadOnly Property TotalDActualSales()
        Get
            Return dblTotalDActualSales
        End Get
    End Property
    ReadOnly Property TotalCust()
        Get
            Return dblTotalCust
        End Get
    End Property
    ReadOnly Property TotalItem()
        Get
            Return dblTotalItem
        End Get
    End Property
    ReadOnly Property TotalUPT()
        Get
            Return dblTotalUPT
        End Get
    End Property
    ReadOnly Property TotalAverage()
        Get
            Return dblTotalAverage
        End Get
    End Property
    ReadOnly Property TotalOS()
        Get
            Return dblTotalOS
        End Get
    End Property

    Public Function GetWTD(ByVal CurrentDate As Date, ByVal StoreNum As Integer) As DataTable

        Dim objConnection As New SqlClient.SqlConnection(strDBConn)
        Dim objDataTable As New DataTable("MSP")
        Dim objDataAdapter As New SqlClient.SqlDataAdapter

        Try
            objConnection.Open()
        Catch
            Try
                If objConnection.State <> ConnectionState.Closed Then
                    objConnection.Close()
                End If

                objConnection.ConnectionString = strDBConn_NoPool
                objConnection.Open()

            Catch

                If objConnection.State <> ConnectionState.Closed Then
                    objConnection.Close()
                End If

                booConectionError = True

            End Try
        End Try

        Try

            Dim objCommand As New SqlClient.SqlCommand("proc_GetWTDbyDate", objConnection)
            objCommand.CommandType = CommandType.StoredProcedure

            objCommand.Parameters.Add("@CurrentDate", SqlDbType.DateTime).Value = CurrentDate
            objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt).Value = StoreNum

            objDataAdapter.SelectCommand = objCommand

            objDataAdapter.Fill(objDataTable)

            booConectionError = False
            booSPCallError = False

            objDataAdapter.Dispose()
            objCommand.Dispose()
            objConnection.Close()

            Return objDataTable

        Catch

            If objConnection.State <> ConnectionState.Closed Then
                objConnection.Close()
            End If

            booSPCallError = True

        End Try

    End Function

    Public Function CalculateWTDTotals(ByVal CurrentDate As Date, ByVal StoreNum As Integer) As Boolean

        Try

            Dim objDataTable As New DataTable("WTD")
            Dim I As Integer

            objDataTable = GetWTD(CurrentDate, StoreNum)

            For I = 0 To objDataTable.Rows.Count - 1

                If IsDBNull(objDataTable.Rows(I).Item("DLYSales")) Then
                    dblTotalDLYSales += 0
                Else
                    dblTotalDLYSales += objDataTable.Rows(I).Item("DLYSales")
                End If

                If IsDBNull(objDataTable.Rows(I).Item("DPlanSales")) Then
                    dblTotalDPlanSales += 0
                Else
                    dblTotalDPlanSales += objDataTable.Rows(I).Item("DPlanSales")
                End If

                If IsDBNull(objDataTable.Rows(I).Item("DActualSales")) Then
                    dblTotalDActualSales += 0
                Else
                    dblTotalDActualSales += objDataTable.Rows(I).Item("DActualSales")
                End If

                If IsDBNull(objDataTable.Rows(I).Item("Cust")) Then
                    dblTotalCust += 0
                Else
                    dblTotalCust += objDataTable.Rows(I).Item("Cust")
                End If

                If IsDBNull(objDataTable.Rows(I).Item("Items")) Then
                    dblTotalItem += 0
                Else
                    dblTotalItem += objDataTable.Rows(I).Item("Items")
                End If

                If IsDBNull(objDataTable.Rows(I).Item("OS")) Then
                    dblTotalOS += 0
                Else
                    dblTotalOS += objDataTable.Rows(I).Item("OS")
                End If

                ' dblTotalDActualSales += CType(objDataTable.Rows(I).Item("DActualSales").ToString, Double)

            Next

            If dblTotalCust > 0 Then
                dblTotalUPT = dblTotalItem / dblTotalCust
            Else
                dblTotalUPT = 0
            End If

            If dblTotalCust > 0 Then
                dblTotalAverage = dblTotalDActualSales / dblTotalCust
            Else
                dblTotalAverage = 0
            End If

            Return True
        Catch

            Return False
        End Try

    End Function

    Public Function InsertOS(ByVal MSPDetailID As Integer, ByVal CurrentDate As Date, ByVal StoreNum As Integer, ByVal OS As Double) As Boolean

        Dim objConnection As New SqlClient.SqlConnection(strDBConn)

        Try

            objConnection.Open()

        Catch
            Try
                If objConnection.State <> ConnectionState.Closed Then
                    objConnection.Close()
                End If

                objConnection.ConnectionString = strDBConn_NoPool
                objConnection.Open()

            Catch ex As Exception

                If objConnection.State <> ConnectionState.Closed Then
                    objConnection.Close()
                End If

                booConectionError = True
                LogError("clsWTD.InsertOS(Store " + StoreNum.ToString + "):" + ex.Message)

            End Try
        End Try

        Try

            Dim objCommand As New SqlClient.SqlCommand("proc_UpdateOS", objConnection)
            objCommand.CommandType = CommandType.StoredProcedure

            objCommand.Parameters.Add("@MSP_Dtl_Id", SqlDbType.Int).Value = MSPDetailID
            objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt).Value = StoreNum
            objCommand.Parameters.Add("@MSPDate", SqlDbType.DateTime).Value = CType(CurrentDate, Date)
            objCommand.Parameters.Add("@OS", SqlDbType.Money).Value = OS
            objCommand.Parameters.Add("@Modified_Date", SqlDbType.DateTime).Value = Now()

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            booConectionError = False
            booSPCallError = False

            Return True

        Catch ex As Exception

            If objConnection.State <> ConnectionState.Closed Then
                objConnection.Close()
            End If

            booSPCallError = True
            LogError("clsWTD.InsertOS(Store " + StoreNum.ToString + "):" + ex.Message)

            Return False

        End Try

    End Function

    Public Function InsertDaily(ByVal MSPDetailID As Integer, ByVal CurrentDate As Date, ByVal StoreNum As Integer, ByVal ActualSales As Double, ByVal Cust As Double, ByVal Items As Double, ByVal OS As Double) As Boolean

        Dim objConnection As New SqlClient.SqlConnection(strDBConn)

        Try
            objConnection.Open()
        Catch
            Try
                If objConnection.State <> ConnectionState.Closed Then
                    objConnection.Close()
                End If

                objConnection.ConnectionString = strDBConn_NoPool
                objConnection.Open()

            Catch ex As Exception

                If objConnection.State <> ConnectionState.Closed Then
                    objConnection.Close()
                End If

                booConectionError = True
                LogError("clsWTD.InsertDaily(Store " + StoreNum.ToString + "):" + ex.Message)

            End Try
        End Try

        Try

            Dim objCommand As New SqlClient.SqlCommand("proc_UpdateMSPDaily", objConnection)
            objCommand.CommandType = CommandType.StoredProcedure

            objCommand.Parameters.Add("@MSP_Dtl_Id", SqlDbType.Int).Value = MSPDetailID
            objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt).Value = StoreNum
            objCommand.Parameters.Add("@MSPDate", SqlDbType.DateTime).Value = CType(CurrentDate, Date)
            objCommand.Parameters.Add("@DActualSales", SqlDbType.Money).Value = ActualSales
            objCommand.Parameters.Add("@Cust", SqlDbType.Float).Value = Cust
            objCommand.Parameters.Add("@Items", SqlDbType.Float).Value = Items
            objCommand.Parameters.Add("@OS", SqlDbType.Money).Value = OS
            objCommand.Parameters.Add("@Modified_Date", SqlDbType.DateTime).Value = Now()

            objCommand.ExecuteNonQuery()

            objCommand.Dispose()
            objConnection.Close()

            booConectionError = False
            booSPCallError = False

            Return True

        Catch ex As Exception
            If objConnection.State <> ConnectionState.Closed Then
                objConnection.Close()
            End If

            booSPCallError = True
            LogError("clsWTD.InsertDaily(Store " + StoreNum.ToString + "):" + ex.Message)

            Return False
        End Try

    End Function

    Private Sub LogError(ByVal message As String)
        Dim sw As System.IO.StreamWriter
        Try
            If ConfigurationSettings.AppSettings("Logging").ToLower = "true" Then
                sw = New System.IO.StreamWriter(ConfigurationSettings.AppSettings("LogPath"), True)
                sw.WriteLine(DateTime.Now.ToString + " " + message)
                sw.Close()
            End If
        Catch
        End Try
    End Sub

End Class
