Public Class clsUser

    Private strUserName As String
    Private strUser As String
    Private intUserStore As Integer
    Private strUserId As String
    Private booValidUser As Boolean
    Private booValidStore As Boolean
    Private strJobCode As String

    ReadOnly Property UserName()
        Get
            Return strUserName
        End Get
    End Property

    ReadOnly Property JobCode()
        Get
            Return strJobCode
        End Get
    End Property

    ReadOnly Property IsValidUser()
        Get
            Return booValidUser
        End Get
    End Property

    ReadOnly Property IsValidStore()
        Get
            Return booValidStore
        End Get
    End Property

    ReadOnly Property User()
        Get
            Return strUser
        End Get
    End Property

    ReadOnly Property UserStore()
        Get
            Return intUserStore
        End Get
    End Property

    ReadOnly Property UserStorePadded()
        Get
            Return intUserStore.ToString.PadLeft(4, "0"c)
        End Get
    End Property

    ReadOnly Property UserId()
        Get
            Return strUserId
        End Get
    End Property

    Public Sub SetUser(ByVal EmpId As String)

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        Dim objSQLDataReader As SqlClient.SqlDataReader

        Try
            objConnection.Open()
        Catch
            Try
                If objConnection.State <> ConnectionState.Closed Then
                    objConnection.Close()
                End If

                objConnection.ConnectionString = ConfigurationSettings.AppSettings("strMainDB_NoPool")
                objConnection.Open()
            Catch
                If objConnection.State <> ConnectionState.Closed Then
                    objConnection.Close()
                End If
            End Try
        End Try

        Try
            strStoredProcedure = "spGetEmployeeInfo '" & EmpId.PadLeft(5, "0"c) & "' "

            Dim objACommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
            objSQLDataReader = objACommand.ExecuteReader()

            If objSQLDataReader.Read Then

                If IsDBNull(objSQLDataReader("EmployeeID")) Then
                    booValidUser = False
                Else
                    strUserName = Trim(objSQLDataReader("NAMEFIRST").ToString) & " " & Trim(objSQLDataReader("NAMELAST").ToString)
                    strUser = Trim(objSQLDataReader("NAMEFIRST").ToString)
                    strUserId = EmpId.ToString
                    strJobCode = Trim(objSQLDataReader("JobCode").ToString)

                    booValidUser = True
                End If
            Else
                booValidUser = False
            End If

            objSQLDataReader.Close()
            objACommand.Dispose()
            objConnection.Close()
        Catch

            If objConnection.State <> ConnectionState.Closed Then
                objConnection.Close()
            End If

        End Try

    End Sub


    Public Sub ClearUserInfo()

        strUserName = ""
        strUser = ""
        strUserId = ""
        intUserStore = -1
        strJobCode = ""

        System.Web.HttpContext.Current.Response.Cookies("User")("UserName") = Nothing
        System.Web.HttpContext.Current.Response.Cookies("User")("Name") = Nothing
        System.Web.HttpContext.Current.Response.Cookies("User")("ID") = Nothing
        System.Web.HttpContext.Current.Response.Cookies("StoreNo").Value = Nothing
        System.Web.HttpContext.Current.Response.Cookies("User")("JobCode") = Nothing

    End Sub

    Public Sub GetStoreInfo()

        intUserStore = GetStoreFromIP()

        If intUserStore = -1 Then
            Try
                intUserStore = System.Web.HttpContext.Current.Request.Cookies("StoreNo").Value

                If intUserStore = 0 Then
                    booValidStore = False
                Else
                    booValidStore = True
                End If
            Catch
                booValidStore = False
            End Try
        Else
            booValidStore = True
        End If

    End Sub

    Public Sub GetUserInfo()

        Try
            strUserName = System.Web.HttpContext.Current.Request.Cookies("User")("UserName")
            strUser = System.Web.HttpContext.Current.Request.Cookies("User")("Name")
            strUserId = System.Web.HttpContext.Current.Request.Cookies("User")("ID").ToString
            strJobCode = System.Web.HttpContext.Current.Request.Cookies("User")("JobCode")

            If strUserId.Trim.Length > 0 Then
                booValidUser = True
            Else
                booValidUser = False
            End If
        Catch
            booValidUser = False
        End Try

    End Sub

    Public Sub New()

        strJobCode = "None"
        strUserName = "Anonymous"
        strUser = "Anonymous"
        intUserStore = 0
        strUserId = "00000"
        booValidUser = False
        booValidStore = False

    End Sub


    'Function generates subnet from storeip then uses subnet to look up store number
    Private Function GetStoreFromIP() As Integer

        Dim StoreIP() As String
        Dim StoreSubnetMask() As String
        Dim strStoreSubnet As String
        Dim strTemp As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strStoreDB"))

        StoreIP = Split(System.Web.HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), ".")
        StoreSubnetMask = Split(ConfigurationSettings.AppSettings("strStoreSubnetMask"), ".")

        strStoreSubnet = StoreIP(0) And StoreSubnetMask(0)
        strStoreSubnet = strStoreSubnet & "."
        strStoreSubnet = strStoreSubnet & (StoreIP(1) And StoreSubnetMask(1))
        strStoreSubnet = strStoreSubnet & "."
        strStoreSubnet = strStoreSubnet & (StoreIP(2) And StoreSubnetMask(2))
        strStoreSubnet = strStoreSubnet & "."
        strStoreSubnet = strStoreSubnet & (StoreIP(3) And StoreSubnetMask(3))

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("spGetStoreFromSubnet", objConnection)

        objCommand.CommandType = CommandType.StoredProcedure

        Dim parStoreSubnet As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreSubnet", SqlDbType.VarChar, 15)
        Dim parStoreNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt)

        parStoreSubnet.Direction = ParameterDirection.Input
        parStoreNum.Direction = ParameterDirection.Output

        parStoreSubnet.Value = strStoreSubnet

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()

        Return parStoreNum.Value

    End Function

End Class
