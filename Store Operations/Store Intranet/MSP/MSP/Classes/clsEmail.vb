Imports System.Web.Mail

Public Class clsEmail

    Private strSmtpServer As String
    Private strSender As String

    Public Sub New(ByVal SmtpServer As String, ByVal Sender As String)

        strSmtpServer = SmtpServer
        strSender = Sender

    End Sub

    Public Function SendEmail(ByVal strBody As String, ByVal strRecevier As String, ByVal Subject As String) As Boolean

        Dim objEmail As New MailMessage()
        Dim booReturn As Boolean = True

        Try
            objEmail.To = strRecevier
            objEmail.From = strSender

            objEmail.BodyFormat = MailFormat.Text
            objEmail.Priority = MailPriority.High
            objEmail.Subject = Subject

            objEmail.Body = strBody

            SmtpMail.SmtpServer = strSmtpServer

            SmtpMail.Send(objEmail)

        Catch

            booReturn = False

        End Try

        Return booReturn

    End Function

End Class
