Imports System.Diagnostics

Partial Class WebForm1
    Inherits System.Web.UI.Page

    Private gStoreNum As Integer
    Private objUser As New clsUser()
  
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim UserName As String
        Dim StoreNum As String

        objUser.GetUserInfo()

        UserName = objUser.User

        objUser.GetStoreInfo()
        If objUser.IsValidStore Then
            StoreNum = objUser.UserStorePadded
            gStoreNum = objUser.UserStore
        Else
            Response.Redirect("GetStoreInfo.aspx")
        End If

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=Store " & StoreNum & "&user=" & UserName & "&title=Monthly Sales Plan&Version=" & ConfigurationSettings.AppSettings("strVersion"))

        pageBody.Attributes.Add("onload", "")

        If Not Page.IsPostBack Then
            If grdDaily_Bind() Then
                grdWTD_Bind()
            End If
        End If

    End Sub

    Private Function grdDaily_Bind() As Boolean

        Dim objWTD As New clsWTD(ConfigurationSettings.AppSettings("strMainDB"), ConfigurationSettings.AppSettings("strMainDB_NoPool"))

        grdDaily.DataSource = objWTD.GetWTD(Now.AddHours(-4).Date, gStoreNum)
        grdDaily.DataBind()

        If objWTD.ConectionError Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Title=" & Server.UrlEncode("Warning") & "&Err=" & Server.UrlEncode("The database connection could not be resolved. Please close the application and try again later.") & "','Warning','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            Return False
        ElseIf objWTD.SPCallError Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Title=" & Server.UrlEncode("Warning") & "&Err=" & Server.UrlEncode("Stored Procedure Error.") & "','Warning','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            Return False
        Else
            Return True
        End If

    End Function

    Private Function grdWTD_Bind() As Boolean

        Dim objWTD As New clsWTD(ConfigurationSettings.AppSettings("strMainDB"), ConfigurationSettings.AppSettings("strMainDB_NoPool"))

        grdWTD.DataSource = objWTD.GetWTD(Now.AddHours(-4).Date, gStoreNum)
        grdWTD.DataBind()

        If objWTD.ConectionError Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Title=" & Server.UrlEncode("Warning") & "&Err=" & Server.UrlEncode("The database connection could not be resolved. Please close the application and try again later.") & "','Warning','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            Return False
        ElseIf objWTD.SPCallError Then
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Title=" & Server.UrlEncode("Warning") & "&Err=" & Server.UrlEncode("Stored Procedure Error.") & "','Warning','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            Return False
        Else
            Return True
        End If

    End Function

    'Private Sub grdWTD_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdWTD.ItemCreated

    '    If e.Item.DataItem Is Nothing Then Exit Sub

    '    Dim dteMSPDate As Date = DataBinder.Eval(e.Item.DataItem, "MSPDate")

    '    If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

    '        'Hightlight Current Day
    '        If dteMSPDate = Now.Date Then
    '            e.Item.CssClass = "SelectedRow"
    '        Else
    '            If e.Item.ItemType = ListItemType.AlternatingItem Then
    '                e.Item.CssClass = "BC333333"
    '            Else
    '                e.Item.CssClass = "BC111111"
    '            End If
    '        End If

    '    End If

    'End Sub




    'Private Sub grdDaily_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDaily.ItemCreated

    '    If e.Item.DataItem Is Nothing Then Exit Sub

    '    Dim dteMSPDate As Date = DataBinder.Eval(e.Item.DataItem, "MSPDate")

    '    If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

    '        If dteMSPDate = Now.Date Then
    '            e.Item.CssClass = "SelectedRow"
    '        Else
    '            If e.Item.ItemType = ListItemType.AlternatingItem Then
    '                e.Item.CssClass = "BC333333"
    '            Else
    '                e.Item.CssClass = "BC111111"
    '                e.Item.BackColor = System.Drawing.Color.FromName("#111111")
    '            End If
    '        End If

    '    End If

    'End Sub

    Public Function IsTextBoxVisible(ByVal MSPDate As Date, ByVal Mode As Integer) As Boolean

        Select Case Mode
            Case 1
                If MSPDate <= Now.AddHours(-4).Date Then
                    Return True
                Else
                    Return False
                End If
            Case Else
                If MSPDate = Now.AddHours(-4).Date Then
                    Return True
                Else
                    Return False
                End If
        End Select

    End Function

    Public Function IsLabelVisible(ByVal MSPDate As Date, ByVal Mode As Integer) As Boolean

        Select Case Mode
            Case 1
                If MSPDate <= Now.AddHours(-4).Date Then
                    Return False
                Else
                    Return True
                End If
            Case Else
                If MSPDate = Now.AddHours(-4).Date Then
                    Return False
                Else
                    Return True
                End If
        End Select


    End Function

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim i As Integer
        Dim _item As DataGridItem
        Dim objWTD As New clsWTD(ConfigurationSettings.AppSettings("strMainDB"), ConfigurationSettings.AppSettings("strMainDB_NoPool"))
        Dim txtActualSales As TextBox
        Dim dteMSPDate As Date
        Dim txtCust As TextBox
        Dim txtItems As TextBox
        Dim txtOS As TextBox
        Dim booValidPage As Boolean = True
        Dim booLogError As Boolean = False
        Dim strErrorMsg As String

        For i = 0 To grdDaily.Items.Count - 1

            _item = grdDaily.Items(i)

            dteMSPDate = _item.Cells(1).Text

            If dteMSPDate <= Now.AddHours(-4).Date() Then
                txtActualSales = _item.FindControl("txtActualSales")
                txtCust = _item.FindControl("txtCust")
                txtItems = _item.FindControl("txtItems")
                txtOS = _item.FindControl("txtOS")

                If Not IsLineValid(_item) Then
                    strErrorMsg = "The fields highlighted in red are required and/or require valid data.."
                    booValidPage = False
                Else
                    If txtActualSales.Visible And txtCust.Visible And txtItems.Visible And txtOS.Visible Then

                        If Not objWTD.InsertDaily(_item.Cells(0).Text, _item.Cells(1).Text, gStoreNum, IIf(txtActualSales.Text.Trim.Length = 0, 0, txtActualSales.Text), IIf(txtCust.Text.Trim.Length = 0, 0, txtCust.Text), IIf(txtItems.Text.Trim.Length = 0, 0, txtItems.Text), IIf(txtOS.Text.Trim.Length = 0, 0, txtOS.Text)) Then

                            strErrorMsg = "An unexpected error has occurred in saving today's numbers."
                            booValidPage = False
                            booLogError = True

                        End If

                    Else

                        If Not objWTD.InsertOS(_item.Cells(0).Text, _item.Cells(1).Text, gStoreNum, IIf(txtOS.Text.Trim.Length = 0, 0, txtOS.Text)) Then

                            strErrorMsg = "An unexpected error has occured in saving O/S values."
                            booValidPage = False
                            booLogError = True

                        End If

                    End If

                End If

                'Exit For
            End If

        Next

        If booLogError Then

            strErrorMsg = "An unexpected error has occurred.."
            booValidPage = False

        End If

        If booValidPage Then
            Response.Redirect("Summary.aspx")
        Else
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Title=" & Server.UrlEncode("Warning") & "&Err=" & Server.UrlEncode(strErrorMsg) & "','Warning','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
        End If

    End Sub

    Private Function IsLineValid(ByVal _Item As DataGridItem) As Boolean

        Dim booValid As Boolean
        Dim txtCust As TextBox = _Item.FindControl("txtCust")
        Dim txtItems As TextBox = _Item.FindControl("txtItems")
        Dim txtOS As TextBox = _Item.FindControl("txtOS")
        Dim txtActualSales As TextBox = _Item.FindControl("txtActualSales")

        booValid = True

        If txtActualSales.Visible Then
            If Len(txtActualSales.Text) > 0 Then
                If Not IsNumeric(txtActualSales.Text) Then
                    txtActualSales.BackColor = System.Drawing.Color.Red
                    booValid = False
                Else
                    txtActualSales.BackColor = System.Drawing.Color.White
                End If
            Else
                txtActualSales.BackColor = System.Drawing.Color.Red
                booValid = False
            End If
        End If

        If txtCust.Visible Then
            If Len(txtCust.Text) > 0 Then
                If Not IsNumeric(txtCust.Text) Then
                    txtCust.BackColor = System.Drawing.Color.Red
                    booValid = False
                Else
                    txtCust.BackColor = System.Drawing.Color.White
                End If
            Else
                txtCust.BackColor = System.Drawing.Color.Red
                booValid = False
            End If
        End If

        If txtItems.Visible Then
            If Len(txtItems.Text) > 0 Then
                If Not IsNumeric(txtItems.Text) Then
                    txtItems.BackColor = System.Drawing.Color.Red
                    booValid = False
                Else
                    txtItems.BackColor = System.Drawing.Color.White
                End If
            Else
                txtItems.BackColor = System.Drawing.Color.Red
                booValid = False
            End If
        End If

        If txtOS.Visible Then
            If Len(txtOS.Text) > 0 Then
                If Not IsNumeric(txtOS.Text) Then
                    txtOS.BackColor = System.Drawing.Color.Red
                    booValid = False
                Else
                    txtOS.BackColor = System.Drawing.Color.White
                End If
            Else
                txtOS.BackColor = System.Drawing.Color.Red
                booValid = False
            End If
        End If

        If booValid Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function IsPageValid(ByVal _Item As DataGridItem) As Boolean

        Dim booValid As Boolean
        Dim strError As String
        Dim txtCust As TextBox = _Item.FindControl("txtCust")
        Dim txtItems As TextBox = _Item.FindControl("txtItems")
        Dim txtOS As TextBox = _Item.FindControl("txtOS")
        Dim txtActualSales As TextBox = _Item.FindControl("txtActualSales")

        booValid = True

        If Len(txtActualSales.Text) > 0 Then
            If Not IsNumeric(txtActualSales.Text) Then
                booValid = False
                strError = "[Actual Sales] must be a numeric value.."
            End If
        Else
            booValid = False
            strError = "[Actual Sales] is required.."
        End If

        If Len(txtCust.Text) > 0 Then
            If Not IsNumeric(txtCust.Text) Then
                booValid = False
                strError = strError & "[Cust] must be a numeric value.."
            End If
        Else
            booValid = False
            strError = strError & "[Cust] is required.."
        End If

        If Len(txtItems.Text) > 0 Then
            If Not IsNumeric(txtItems.Text) Then
                booValid = False
                strError = strError & "[Items] must be a numeric value.."
            End If
        Else
            booValid = False
            strError = strError & "[Items] is required.."
        End If

        If Len(txtOS.Text) > 0 Then
            If Not IsNumeric(txtOS.Text) Then
                booValid = False
                strError = strError & "[OS] must be a numeric value.."
            End If
        Else
            booValid = False
            strError = strError & "[OS] is required.."
        End If

        If booValid Then
            Return True
        Else
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Title=" & Server.UrlEncode("Warning") & "&Err=" & Server.UrlEncode(strError) & "','Warning','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            Return False
        End If

    End Function


    Private Sub grdDaily_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDaily.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Footer

                Dim objWTD As New clsWTD(ConfigurationSettings.AppSettings("strMainDB"), ConfigurationSettings.AppSettings("strMainDB_NoPool"))

                objWTD.CalculateWTDTotals(Now.AddHours(-4).Date, gStoreNum)

                e.Item.Cells(2).Text = String.Format("{0:#,##0}", objWTD.TotalDLYSales)
                e.Item.Cells(2).Attributes.Add("align", "right")
                e.Item.Cells(3).Text = String.Format("{0:#,##0}", objWTD.TotalDPlanSales)
                e.Item.Cells(3).Attributes.Add("align", "right")
                e.Item.Cells(4).Text = String.Format("{0:#,##0}", objWTD.TotalDActualSales)
                e.Item.Cells(4).Attributes.Add("align", "right")
                e.Item.Cells(9).Text = String.Format("{0:#,##0}", objWTD.TotalCust)
                e.Item.Cells(9).Attributes.Add("align", "right")
                e.Item.Cells(10).Text = String.Format("{0:#,##0}", objWTD.TotalItem)
                e.Item.Cells(10).Attributes.Add("align", "right")
                e.Item.Cells(11).Text = String.Format("{0:#,##0.00}", objWTD.TotalUPT)
                e.Item.Cells(11).Attributes.Add("align", "right")
                e.Item.Cells(12).Text = String.Format("{0:#,##0.00}", objWTD.TotalAverage)
                e.Item.Cells(12).Attributes.Add("align", "right")
                e.Item.Cells(13).Text = String.Format("{0:#,##0.00}", objWTD.TotalOS)
                e.Item.Cells(13).Attributes.Add("align", "right")

        End Select
    End Sub

    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        Response.Redirect(ConfigurationSettings.AppSettings("HomeURL"))
    End Sub

End Class
