<%@ Page Language="vb" AutoEventWireup="false" Codebehind="default.aspx.vb" Inherits="MSP.WebForm1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MSP - Daily Sales Entry</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body id="pagebody" runat="server">
		<form id="Form1" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server" />
			<div class="container home" role="main">
			<table width="100%">
				<tr>
					<td align="center"><asp:label id="lblTitle" Font-Size="large" Runat="server">Store Sales Plan</asp:label></td>
				</tr>
				<tr>
					<td align="center"><asp:label id="lblDaily" Runat="server">Daily</asp:label><br>
						<asp:datagrid id="grdDaily" runat="server" ShowFooter="True" Width="98%" AutoGenerateColumns="false" CssClass="table archtbl">
							<ALTERNATINGITEMSTYLE cssclass="archtblalttr"></ALTERNATINGITEMSTYLE>
							<ItemStyle cssclass="archtbltr"></ItemStyle>
							<HEADERSTYLE cssclass="archtblhdr"></HEADERSTYLE>
							<FooterStyle></FooterStyle>
							<COLUMNS>
								<ASP:BOUNDCOLUMN Readonly="True" HeaderText="MSP Detail ID" FooterStyle-Wrap="false" ItemStyle-Wrap="False"
									DataField="MSP_Dtl_Id" Visible="False"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN Readonly="True" HeaderText="Date" FooterStyle-Wrap="false" ItemStyle-Wrap="False"
									DataField="MSPDate" DataFormatString="{0:MM-dd-yyyy}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN FooterStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="LY Sales" DataField="DLYSales" DataFormatString="{0:#,##0}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN FooterStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="Plan Sales" DataField="DPlanSales" DataFormatString="{0:#,##0}"></ASP:BOUNDCOLUMN>
								<asp:TemplateColumn FooterStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
									ItemStyle-Wrap="False" HeaderText="Actual Sales">
									<ItemTemplate>
										<asp:Label Visible='<%# IsLabelVisible(DataBinder.Eval(Container.DataItem, "MSPDate"),0) %>' id="lblActualSales" Runat="server" text='<%# System.String.Format("{0:#,##0}", DataBinder.Eval(Container.DataItem, "DActualSales")) %>'>
										</asp:Label>
										<asp:TextBox style="text-align:right" MaxLength="12" Visible='<%# IsTextBoxVisible(DataBinder.Eval(Container.DataItem, "MSPDate"),0) %>' width="100%" id="txtActualSales" runat="server" text='<%# System.String.Format("{0:#,##0}", DataBinder.Eval(Container.DataItem, "DActualSales")) %>'>
										</asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<ASP:BOUNDCOLUMN FooterStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="Act v. Plan + or -" DataField="DActvPlanPlusMinus"
									DataFormatString="{0:#,##0}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN FooterStyle-Wrap="false" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="Act v. Plan %" DataField="DActvPlanPercent"
									DataFormatString="{0:##0.0%}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN FooterStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="Act v. LY + or -" DataField="DActvLYPlusMinus"
									DataFormatString="{0:#,##0}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN FooterStyle-Wrap="false" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="Act v. LY %" DataField="DActvLYPercent" DataFormatString="{0:##0.0%}"></ASP:BOUNDCOLUMN>
								<asp:TemplateColumn FooterStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
									ItemStyle-Wrap="False" HeaderText="Cust">
									<ItemTemplate>
										<asp:Label Visible='<%# IsLabelVisible(DataBinder.Eval(Container.DataItem, "MSPDate"),0) %>' id="lblCust" Runat="server" text='<%# System.String.Format("{0:#,##0}", DataBinder.Eval(Container.DataItem, "Cust")) %>'>
										</asp:Label>
										<asp:TextBox style="text-align:right" MaxLength="12" Visible='<%# IsTextBoxVisible(DataBinder.Eval(Container.DataItem, "MSPDate"),0) %>' width="100%" id="txtCust" runat="server" text='<%# System.String.Format("{0:#,##0}", DataBinder.Eval(Container.DataItem, "Cust")) %>'>
										</asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn FooterStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
									ItemStyle-Wrap="False" HeaderText="Items">
									<ItemTemplate>
										<asp:Label Visible='<%# IsLabelVisible(DataBinder.Eval(Container.DataItem, "MSPDate"),0) %>' id="lblItems" Runat="server" text='<%# System.String.Format("{0:#,##0}", DataBinder.Eval(Container.DataItem, "Items")) %>'>
										</asp:Label>
										<asp:TextBox style="text-align:right" Maxlength="12" Visible='<%# IsTextBoxVisible(DataBinder.Eval(Container.DataItem, "MSPDate"),0) %>' width="100%" id="txtItems" runat="server" text='<%# System.String.Format("{0:#,##0}", DataBinder.Eval(Container.DataItem, "Items")) %>'>
										</asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<ASP:BOUNDCOLUMN FooterStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="UPT" DataField="UPT" DataFormatString="{0:#,##0.00}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN FooterStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="AVG$" DataField="Average" DataFormatString="{0:#,##0.00}"></ASP:BOUNDCOLUMN>
								<asp:TemplateColumn FooterStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right" ItemStyle-Wrap="false"
									ItemStyle-HorizontalAlign="Right" HeaderText="O/S">
									<ItemTemplate>
										<asp:Label Visible='<%# IsLabelVisible(DataBinder.Eval(Container.DataItem, "MSPDate"),1) %>' id="lblOS" Runat="server" text='<%# System.String.Format("{0:#,##0.00}", DataBinder.Eval(Container.DataItem, "OS")) %>'>
										</asp:Label>
										<asp:TextBox style="text-align:right" MaxLength="12" Visible='<%# IsTextBoxVisible(DataBinder.Eval(Container.DataItem, "MSPDate"),1) %>' width="100%" id="txtOS" runat="server" text='<%# System.String.Format("{0:#,##0.00}", DataBinder.Eval(Container.DataItem, "OS")) %>'>
										</asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
							</COLUMNS>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="center"><asp:label id="lblSubTitle" Runat="server">Week to Date</asp:label><br>
						<asp:datagrid id="grdWTD" runat="server" Width="98%" AutoGenerateColumns="false" CssClass="table archtbl">
							<ALTERNATINGITEMSTYLE cssclass="archtblalttr"></ALTERNATINGITEMSTYLE>
							<ItemStyle cssclass="archtbltr"></ItemStyle>
							<HEADERSTYLE cssclass="archtblhdr"></HEADERSTYLE>
							<COLUMNS>
								<ASP:BOUNDCOLUMN Readonly="True" HeaderText="Date" FooterStyle-Wrap="false" ItemStyle-Wrap="False"
									DataField="MSPDate" DataFormatString="{0:MM-dd-yyyy}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-Wrap="false"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="Actual Sales $" DataField="WTDActualSalesAmt"
									DataFormatString="{0:#,##0}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-Wrap="false"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="Plan Sales $" DataField="WTDPlanSalesAmt"
									DataFormatString="{0:#,##0}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="False"
									Readonly="True" HeaderText="Act v. Plan + or -" DataField="WTDActvPlanPlusMinus" DataFormatString="{0:#,##0}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" FooterStyle-Wrap="false"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="Act v. Plan %" DataField="WTDActvPlanPercent"
									DataFormatString="{0:##0.0%}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-Wrap="false"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="LY Sales $" DataField="WTDLYSalesAmt" DataFormatString="{0:#,##0}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-Wrap="false"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="Act v. LY + or -" DataField="WTDActvLYPlusMinus"
									DataFormatString="{0:#,##0}"></ASP:BOUNDCOLUMN>
								<ASP:BOUNDCOLUMN HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" FooterStyle-Wrap="false"
									ItemStyle-Wrap="False" Readonly="True" HeaderText="Act v. LY %" DataField="WTDActvLYPercent"
									DataFormatString="{0:##0.0%}"></ASP:BOUNDCOLUMN>
							</COLUMNS>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td>
						<hr>
					</td>
				</tr>
				<tr>
					<td align="right"><asp:button id="btnSubmit" Runat="server" CssClass="btn btn-danger" Text="Submit"></asp:button>&nbsp;<asp:button id="btnHome" Runat="server" CssClass="btn btn-danger" Text="Home"></asp:button></td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
