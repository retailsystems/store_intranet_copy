Partial Class EmailConformation
    Inherits System.Web.UI.Page

    Private gStoreNum As Integer
    Private objUser As New clsUser()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim UserName As String
        Dim StoreNum As String

        objUser.GetUserInfo()

        UserName = objUser.User

        objUser.GetStoreInfo()
        If objUser.IsValidStore Then
            StoreNum = objUser.UserStorePadded
            gStoreNum = objUser.UserStore
        Else
            Response.Redirect("GetStoreInfo.aspx")
        End If

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=Store " & StoreNum & "&user=" & UserName & "&title=Monthly Sales Plan&Version=" & ConfigurationSettings.AppSettings("strVersion"))

    End Sub

    Private Sub btnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Response.Redirect("default.aspx")
    End Sub

    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        Response.Redirect(ConfigurationSettings.AppSettings("HomeURL"))
    End Sub

End Class
