'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.5485
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On



'''<summary>
'''GetStoreInfo class.
'''</summary>
'''<remarks>
'''Auto-generated class.
'''</remarks>
Partial Public Class GetStoreInfo

	'''<summary>
	'''lnkStyles control.
	'''</summary>
	'''<remarks>
	'''Auto-generated field.
	'''To modify move field declaration from designer file to code-behind file.
	'''</remarks>
	Protected WithEvents lnkStyles As Global.System.Web.UI.HtmlControls.HtmlGenericControl

	'''<summary>
	'''ifmHeader control.
	'''</summary>
	'''<remarks>
	'''Auto-generated field.
	'''To modify move field declaration from designer file to code-behind file.
	'''</remarks>
	Protected WithEvents ifmHeader As Global.System.Web.UI.HtmlControls.HtmlGenericControl

	'''<summary>
	'''FormReportQuery control.
	'''</summary>
	'''<remarks>
	'''Auto-generated field.
	'''To modify move field declaration from designer file to code-behind file.
	'''</remarks>
	Protected WithEvents FormReportQuery As Global.System.Web.UI.HtmlControls.HtmlForm

	'''<summary>
	'''btnReturn control.
	'''</summary>
	'''<remarks>
	'''Auto-generated field.
	'''To modify move field declaration from designer file to code-behind file.
	'''</remarks>
	Protected WithEvents btnReturn As Global.System.Web.UI.WebControls.Button

	'''<summary>
	'''btnHome control.
	'''</summary>
	'''<remarks>
	'''Auto-generated field.
	'''To modify move field declaration from designer file to code-behind file.
	'''</remarks>
	Protected WithEvents btnHome As Global.System.Web.UI.WebControls.Button
End Class
