Imports System.Data
Imports System.Data.OleDb
Imports System.IO

Public Class frmMain
    Inherits System.Windows.Forms.Form

    Private objEventLog As New EventLog()
    Private strMSPConn As String
    Private strMSPStoreConn As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents cboStore As System.Windows.Forms.ComboBox
    Friend WithEvents tbWaitTime As System.Windows.Forms.TrackBar
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.cboStore = New System.Windows.Forms.ComboBox()
        Me.tbWaitTime = New System.Windows.Forms.TrackBar()
        CType(Me.tbWaitTime, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnImport
        '
        Me.btnImport.Location = New System.Drawing.Point(136, 8)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(168, 23)
        Me.btnImport.TabIndex = 0
        Me.btnImport.Text = "Run Import"
        '
        'cboStore
        '
        Me.cboStore.Items.AddRange(New Object() {"Hottopic", "Torrid"})
        Me.cboStore.Location = New System.Drawing.Point(8, 8)
        Me.cboStore.Name = "cboStore"
        Me.cboStore.Size = New System.Drawing.Size(121, 21)
        Me.cboStore.TabIndex = 1
        '
        'tbWaitTime
        '
        Me.tbWaitTime.LargeChange = 100
        Me.tbWaitTime.Location = New System.Drawing.Point(8, 32)
        Me.tbWaitTime.Maximum = 2000
        Me.tbWaitTime.Minimum = 10
        Me.tbWaitTime.Name = "tbWaitTime"
        Me.tbWaitTime.Size = New System.Drawing.Size(296, 42)
        Me.tbWaitTime.SmallChange = 10
        Me.tbWaitTime.TabIndex = 2
        Me.tbWaitTime.TickFrequency = 100
        Me.tbWaitTime.TickStyle = System.Windows.Forms.TickStyle.TopLeft
        Me.tbWaitTime.Value = 100
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(312, 69)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.tbWaitTime, Me.cboStore, Me.btnImport})
        Me.Name = "frmMain"
        Me.Text = "Main"
        CType(Me.tbWaitTime, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strCommandLineArgs As String
        Dim objAppSettingsReader As New Configuration.AppSettingsReader()

        If Not objEventLog.SourceExists("MSP_XLSDataImport") Then
            EventLog.CreateEventSource("MSP_XLSDataImport", "Application")
        End If

        strCommandLineArgs = System.Environment.CommandLine

        Dim cmdLine() As String = System.Environment.GetCommandLineArgs()

        If cmdLine.Length > 1 Then

            tbWaitTime.Value = Replace(cmdLine(2), "-", "")

            If cmdLine(1) = "-HT" Then
                strMSPConn = objAppSettingsReader.GetValue("strMSPConn_HT", GetType(System.String))
                strMSPStoreConn = objAppSettingsReader.GetValue("strMSPStoreConn_HT", GetType(System.String))
                ImportXLSData()
                Application.Exit()
            End If

            If cmdLine(1) = "-TD" Then
                strMSPConn = objAppSettingsReader.GetValue("strMSPConn_TD", GetType(System.String))
                strMSPStoreConn = objAppSettingsReader.GetValue("strMSPStoreConn_TD", GetType(System.String))
                ImportXLSData()
                Application.Exit()
            End If
        End If

    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click

        btnImport.Enabled = False
        btnImport.Text = "IMPORTING..."

        Dim objAppSettingsReader As New Configuration.AppSettingsReader()

        If cboStore.SelectedIndex = 0 Then
            strMSPConn = objAppSettingsReader.GetValue("strMSPConn_HT", GetType(System.String))
            strMSPStoreConn = objAppSettingsReader.GetValue("strMSPStoreConn_HT", GetType(System.String))
            ImportXLSData()
        ElseIf cboStore.SelectedIndex = 1 Then
            strMSPConn = objAppSettingsReader.GetValue("strMSPConn_TD", GetType(System.String))
            strMSPStoreConn = objAppSettingsReader.GetValue("strMSPStoreConn_TD", GetType(System.String))
            ImportXLSData()
        End If

        btnImport.Text = "Run Import"
        btnImport.Enabled = True

    End Sub

    Private Function ImportXLSData() As Boolean

        Dim objConnection As New SqlClient.SqlConnection(strMSPStoreConn)

        Try

            Dim objAppSettingsReader As New Configuration.AppSettingsReader()
            Dim strXLSFileConn As String = objAppSettingsReader.GetValue("strMSPXLSConn", GetType(System.String))
            Dim strXLSDir As String = objAppSettingsReader.GetValue("strXLSDir", GetType(System.String))
            Dim strCurrentXLSFileConn As String

            Dim objDataReader As SqlClient.SqlDataReader

            objConnection.Open()

            Dim objCommand As New SqlClient.SqlCommand("asFillStore", objConnection)

            objDataReader = objCommand.ExecuteReader()

            Do While objDataReader.Read()

                System.Windows.Forms.Application.DoEvents()

                strCurrentXLSFileConn = Replace(strXLSFileConn, "%ownerID%", objDataReader("StoreNum").ToString.PadLeft(4, "0"))

                If File.Exists(strXLSDir & "Store " & objDataReader("StoreNum").ToString.PadLeft(4, "0") & ".xls") Then
                    If ImportXLSFile(strCurrentXLSFileConn, objDataReader("StoreNum")) Then
                        UpdateImportLog(objDataReader("StoreNum"))
                    Else
                        objEventLog.Source = "MSP_XLSDataImport"
                        objEventLog.WriteEntry("Data Import for Store #" & objDataReader("StoreNum").ToString.PadLeft(4, "0") & " failed.", EventLogEntryType.Error)
                    End If
                Else
                    objEventLog.Source = "MSP_XLSDataImport"
                    objEventLog.WriteEntry("File for Store #" & objDataReader("StoreNum").ToString.PadLeft(4, "0") & " does not exist.", EventLogEntryType.Warning)
                End If

            Loop

            objDataReader.Close()
            objCommand.Dispose()
            objConnection.Close()

            Return True
        Catch
            objEventLog.Source = "MSP_XLSDataImport"
            objEventLog.WriteEntry("MSP_XLSDataImport Failed", EventLogEntryType.Error)

            If objConnection.State <> ConnectionState.Closed Then
                objConnection.Close()
            End If

            Return False
        End Try

    End Function

    Private Sub UpdateImportLog(ByVal StoreNum As Integer)

        Dim objConnection As New SqlClient.SqlConnection(strMSPConn)

        objConnection.Open()

        Dim objCommand As New SqlClient.SqlCommand("proc_InsertMSPDataImport", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim parStoreNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@Store_Num", SqlDbType.SmallInt)
        Dim parLastImportDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@Last_Import_Date", SqlDbType.DateTime)

        parStoreNum.Value = StoreNum
        parLastImportDate.Value = Now()

        objCommand.ExecuteNonQuery()

        objCommand.Dispose()
        objConnection.Close()

    End Sub

    Private Function ImportXLSFile(ByVal strDBConn As String, ByVal StoreNum As Integer) As Boolean

        Dim objConnection As New SqlClient.SqlConnection(strMSPConn)
        Dim objXLSConn As New OleDbConnection(strDBConn)


        Try

            Dim objAppSettingsReader As New Configuration.AppSettingsReader()

            Dim CurrentRow As Integer = 0

            objConnection.Open()
            objXLSConn.Open()

            Dim objCmdSelect As New OleDbCommand("SELECT * FROM [A7:Z70]", objXLSConn)
            Dim objAdapter As New OleDbDataAdapter()

            objAdapter.SelectCommand = objCmdSelect

            Dim objDataset As New DataSet()

            objAdapter.Fill(objDataset, "MSP")

            Do While objDataset.Tables(0).Rows.Count > CurrentRow

                System.Windows.Forms.Application.DoEvents()

                If IsDate(objDataset.Tables(0).Rows(CurrentRow).Item(0)) Then

                    Dim objCommand As New SqlClient.SqlCommand("proc_InsertMSPDetail", objConnection)
                    objCommand.CommandType = CommandType.StoredProcedure

                    Dim parStoreNum As SqlClient.SqlParameter = objCommand.Parameters.Add("@StoreNum", SqlDbType.SmallInt)
                    Dim parMSPDate As SqlClient.SqlParameter = objCommand.Parameters.Add("@MSPDate", SqlDbType.DateTime)
                    Dim parDLYSales As SqlClient.SqlParameter = objCommand.Parameters.Add("@DLYSales", SqlDbType.Money)
                    Dim parDPlanSales As SqlClient.SqlParameter = objCommand.Parameters.Add("@DPlanSales", SqlDbType.Money)
                    Dim parDActualSales As SqlClient.SqlParameter = objCommand.Parameters.Add("@DActualSales", SqlDbType.Money)
                    Dim parDActvPlanPlusMinus As SqlClient.SqlParameter = objCommand.Parameters.Add("@DActvPlanPlusMinus", SqlDbType.Money)
                    Dim parDActvPlanPercent As SqlClient.SqlParameter = objCommand.Parameters.Add("@DActvPlanPercent", SqlDbType.Float)
                    Dim parDActvLYPlusMinus As SqlClient.SqlParameter = objCommand.Parameters.Add("@DActvLYPlusMinus", SqlDbType.Money)
                    Dim parDActvLYPercent As SqlClient.SqlParameter = objCommand.Parameters.Add("@DActvLYPercent", SqlDbType.Float)
                    Dim parWTDActualSalesAmt As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDActualSalesAmt", SqlDbType.Money)
                    Dim parWTDPlanSalesAmt As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDPlanSalesAmt", SqlDbType.Money)
                    Dim parWTDActvPlanPlusMinus As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDActvPlanPlusMinus", SqlDbType.Money)
                    Dim parWTDActvPlanPercent As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDActvPlanPercent", SqlDbType.Float)
                    Dim parWTDLYSalesAmt As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDLYSalesAmt", SqlDbType.Money)
                    Dim parWTDActvLYPlusMinus As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDActvLYPlusMinus", SqlDbType.Money)
                    Dim parWTDActvLYPercent As SqlClient.SqlParameter = objCommand.Parameters.Add("@WTDActvLYPercent", SqlDbType.Float)
                    Dim parCust As SqlClient.SqlParameter = objCommand.Parameters.Add("@Cust", SqlDbType.Float)
                    Dim parItems As SqlClient.SqlParameter = objCommand.Parameters.Add("@Items", SqlDbType.Float)
                    Dim parUPT As SqlClient.SqlParameter = objCommand.Parameters.Add("@UPT", SqlDbType.Money)
                    Dim parAverage As SqlClient.SqlParameter = objCommand.Parameters.Add("@Average", SqlDbType.Money)
                    Dim parOS As SqlClient.SqlParameter = objCommand.Parameters.Add("@OS", SqlDbType.Money)
                    Dim parModified_Date As SqlClient.SqlParameter = objCommand.Parameters.Add("@Modified_Date", SqlDbType.DateTime)
                    Dim parCreated_Date As SqlClient.SqlParameter = objCommand.Parameters.Add("@Created_Date", SqlDbType.DateTime)

                    parStoreNum.Value = StoreNum
                    parMSPDate.Value = objDataset.Tables(0).Rows(CurrentRow).Item(0)
                    parDLYSales.Value = objDataset.Tables(0).Rows(CurrentRow).Item(1)
                    parDPlanSales.Value = objDataset.Tables(0).Rows(CurrentRow).Item(2)
                    parDActualSales.Value = objDataset.Tables(0).Rows(CurrentRow).Item(3)
                    parDActvPlanPlusMinus.Value = objDataset.Tables(0).Rows(CurrentRow).Item(4)
                    parDActvPlanPercent.Value = objDataset.Tables(0).Rows(CurrentRow).Item(6)
                    parDActvLYPlusMinus.Value = objDataset.Tables(0).Rows(CurrentRow).Item(7)
                    parDActvLYPercent.Value = objDataset.Tables(0).Rows(CurrentRow).Item(9)
                    parWTDActualSalesAmt.Value = objDataset.Tables(0).Rows(CurrentRow).Item(11)
                    parWTDPlanSalesAmt.Value = objDataset.Tables(0).Rows(CurrentRow).Item(12)
                    parWTDActvPlanPlusMinus.Value = objDataset.Tables(0).Rows(CurrentRow).Item(13)
                    parWTDActvPlanPercent.Value = objDataset.Tables(0).Rows(CurrentRow).Item(15)
                    parWTDLYSalesAmt.Value = objDataset.Tables(0).Rows(CurrentRow).Item(16)
                    parWTDActvLYPlusMinus.Value = objDataset.Tables(0).Rows(CurrentRow).Item(17)
                    parWTDActvLYPercent.Value = objDataset.Tables(0).Rows(CurrentRow).Item(19)
                    parCust.Value = objDataset.Tables(0).Rows(CurrentRow).Item(21)
                    parItems.Value = objDataset.Tables(0).Rows(CurrentRow).Item(22)
                    parUPT.Value = objDataset.Tables(0).Rows(CurrentRow).Item(23)
                    parAverage.Value = objDataset.Tables(0).Rows(CurrentRow).Item(24)
                    parOS.Value = objDataset.Tables(0).Rows(CurrentRow).Item(25)
                    parModified_Date.Value = Now()
                    parCreated_Date.Value = Now()

                    Try
                        objCommand.ExecuteNonQuery()
                    Catch
                        objEventLog.Source = "MSP_XLSDataImport"
                        objEventLog.WriteEntry(parMSPDate.Value & " Data Import for Store #" & StoreNum.ToString.PadLeft(4, "0") & " failed to import.", EventLogEntryType.Warning)
                    End Try

                    objCommand.Dispose()


                End If

                CurrentRow += 1
                System.Threading.Thread.Sleep(tbWaitTime.Value)

            Loop

            objXLSConn.Close()
            objConnection.Close()

            Return True

        Catch

            If objConnection.State <> ConnectionState.Closed Then
                objConnection.Close()
            End If

            If objXLSConn.State <> ConnectionState.Closed Then
                objXLSConn.Close()
            End If

            Return False

        End Try

    End Function

End Class
