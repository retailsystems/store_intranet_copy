<%@ Page Language="vb" AutoEventWireup="false" Codebehind="GetStoreInfo.aspx.vb" Inherits="MSP.GetStoreInfo"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Select Store</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body>
		<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0"
			src="" frameBorder="0" width="100%" scrolling="no" runat="server">
		</iframe>
		<div class="container home" role="main">
		<form id="FormReportQuery" method="post" runat="server">
			<div class="alert alert-warning" role="alert">
			<table cellSpacing="2" cellPadding="1" width="750" border="0">
				<tr>
					<td><font face="arial" size="2"> You have not set your store number. <A href="/Home/SetStore.aspx">
								<font face="arial" size="2">Click here to set your store.</font></A><BR>
							You will not be able to&nbsp;use MSP&nbsp;until your store number is set.</font>
					</td>
				</tr>
			</table>
			</div>
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td>
						<hr color="#990000">
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;&nbsp;<asp:button id="btnReturn" runat="server" CssClass="btn btn-danger" Text="Return"></asp:button>&nbsp;&nbsp;<asp:button id="btnHome" CssClass="btn btn-danger" runat="server" Text="Home"></asp:button></td>
				</tr>
			</table>
		</form>
		</div>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
