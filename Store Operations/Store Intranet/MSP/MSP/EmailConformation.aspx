<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EmailConformation.aspx.vb" Inherits="MSP.EmailConformation"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MSP - Daily Sales Entry</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0"
				src="" frameBorder="0" width="100%" scrolling="no" runat="server">
			</iframe>
			<div class="container home" role="main">
			<table width="100%">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="center"><asp:label id="lblTitle" Font-Size="Medium" Runat="server">Your email has been sent.</asp:label></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td>
						<hr>
					</td>
				</tr>
				<tr>
					<td align="right"><asp:button id="btnReturn" Runat="server" CssClass="btn btn-danger" Text="Return"></asp:button>&nbsp;<asp:button id="btnHome" Runat="server" CssClass="btn btn-danger" Text="Home"></asp:button></td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
