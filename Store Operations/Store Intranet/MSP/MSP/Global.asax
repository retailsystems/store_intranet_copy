<%@ Application Codebehind="Global.asax.vb" Inherits="MSP.Global" %>

<script runat="server">
sub Application_Error
	'***********************************************************************
	'**		Auth: Sri Bajjuri
	'**		Date: 2/2/2004
	'***********************************************************************
	' Fires when an error occurs
	'Get the last un-handled exception
	Application("Exp") = Server.GetLastError
end sub
</script>
