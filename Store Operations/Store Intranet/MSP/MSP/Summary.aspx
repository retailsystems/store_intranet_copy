<%@ Page Language="vb" validateRequest="false" AutoEventWireup="false" Codebehind="Summary.aspx.vb" Inherits="MSP.Summary"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MSP - Daily Sales Entry</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
		<link href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
		<link href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
		<!--[if lt IE 9]>
		  <script src="/Global/js/html5shiv.min.js"></script>
		  <script src="/Global/js/respond.min.js"></script>
		<![endif]-->
	</HEAD>
	<body id="pageBody" runat="server">
		<form id="Form1" method="post" runat="server">
			<iframe id="ifmHeader" marginWidth="0" hspace="0" marginHeight="0" src="" frameBorder="0" width="100%" scrolling="no" runat="server" />
			<div class="container home" role="main">
			<table width="100%">
				<tr>
					<td colspan="2" align="center"><asp:label id="lblTitle" Runat="server" Font-Size="large">Store Sales Plan</asp:label></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><asp:label id="lblSubTitle" Runat="server" Font-Size="medium">Email Summary</asp:label></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr style="background-color:#EEEEEE">
					<td width="50%" align="center">
						<table>
							<tr>
								<td colSpan="9"><asp:label id="lblDate" Runat="server"></asp:label></td>
							</tr>
							<tr>
								<td colSpan="4"><asp:label id="lblDailyTitle" Runat="server"><u>DAILY</u></asp:label></td>
								<td>&nbsp;</td>
								<td colSpan="4"><asp:label id="lblWTDTitle" Runat="server"><u>WTD</u></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblDTY" Runat="server">TY:</asp:label></td>
								<td colSpan="2" align="right"><asp:label id="lblDTYValue" Runat="server"></asp:label></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td><asp:label id="lblWTY" Runat="server">TY:</asp:label></td>
								<td colSpan="2" align="right"><asp:label id="lblWTYValue" Runat="server"></asp:label></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><asp:label id="lblDLY" Runat="server">LY:</asp:label></td>
								<td colSpan="2" align="right"><asp:label id="lblDLYValue" Runat="server"></asp:label></td>
								<td align="right"><asp:label id="lblDAvLYValue" Runat="server"></asp:label></td>
								<td>&nbsp;</td>
								<td><asp:label id="lblWLY" Runat="server">LY:</asp:label></td>
								<td align="right" colSpan="2"><asp:label id="lblWLYValue" Runat="server"></asp:label></td>
								<td align="right"><asp:label id="lblWAvLYValue" Runat="server"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblDPlan" Runat="server">PLAN:</asp:label></td>
								<td align="right" colSpan="2"><asp:label id="lblDPlanValue" Runat="server"></asp:label></td>
								<td align="right"><asp:label id="lblDAvPValue" Runat="server"></asp:label></td>
								<td>&nbsp;</td>
								<td><asp:label id="lblWPlan" Runat="server">PLAN:</asp:label></td>
								<td align="right" colSpan="2"><asp:label id="lblWPlanValue" Runat="server"></asp:label></td>
								<td align="right"><asp:label id="lblWAvPValue" Runat="server"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblDADT" Runat="server">ADT:</asp:label></td>
								<td align="right" colSpan="2"><asp:label id="lblDADTValue" Runat="server"></asp:label></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td><asp:label id="lblWADT" Runat="server">ADT:</asp:label></td>
								<td align="right" colSpan="2"><asp:label id="lblWADTValue" Runat="server"></asp:label></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><asp:label id="lblDUPT" Runat="server">UPT:</asp:label></td>
								<td align="right" colSpan="2"><asp:label id="lblDUPTValue" Runat="server"></asp:label></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td><asp:label id="lblWUPT" Runat="server">UPT:</asp:label></td>
								<td align="right" colSpan="2"><asp:label id="lblWUPTValue" Runat="server"></asp:label></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><asp:label id="lblDOS" Runat="server">CASH O/S:</asp:label></td>
								<td align="right" colSpan="2"><asp:label id="lblDOSValue" Runat="server"></asp:label></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td><asp:label id="lblWOS" Runat="server">CASH O/S:</asp:label></td>
								<td align="right" colSpan="2"><asp:label id="lblWOSValue" Runat="server"></asp:label></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
					<td width="50%">
						<table width="100%">
							<tr>
								<td width="1%" nowrap><asp:label id="lblEmail" Runat="server">Email To:</asp:label></td>
								<td><asp:textbox id="txtEmail" runat="server" MaxLength="50" width="100%"></asp:textbox></td>
							</tr>
							<tr>
								<td colspan="2"><asp:TextBox Runat="server" ID="txtEmailBody" Width="100%" Rows="10" TextMode="MultiLine"></asp:TextBox></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td>
						<hr>
					</td>
				</tr>
				<tr>
					<td align="right"><asp:button id="btnCancel" Runat="server" Text="Cancel" CssClass="btn btn-danger"></asp:button>&nbsp;<asp:button id="btnEmail" Runat="server" Text="Email" CssClass="btn btn-danger"></asp:button></td>
				</tr>
			</table>
			</div>
		</form>
		<script src="/Global/js/jquery.min.js"></script>
		<script src="/Global/js/bootstrap.min.js"></script>
	</body>
</HTML>
