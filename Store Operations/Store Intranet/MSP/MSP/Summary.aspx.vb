Partial Class Summary
    Inherits System.Web.UI.Page

    Private gStoreNum As Integer
    Private objUser As New clsUser()


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim UserName As String
        Dim StoreNum As String

        objUser.GetUserInfo()

        UserName = objUser.User

        objUser.GetStoreInfo()
        If objUser.IsValidStore Then
            StoreNum = objUser.UserStorePadded
            gStoreNum = objUser.UserStore
        Else
            Response.Redirect("GetStoreInfo.aspx")
        End If

        pageBody.Attributes.Add("onload", "")

        'Use custom Stylesheet & Header
        lnkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings("Stylesheet"))
        ifmHeader.Attributes.Add("src", ConfigurationSettings.AppSettings("Mode") & "&location=Store " & StoreNum & "&user=" & UserName & "&title=Monthly Sales Plan&Version=" & ConfigurationSettings.AppSettings("strVersion"))

        If Not Page.IsPostBack Then
            BindData()
            GetDMEmail()
        End If

    End Sub

    Private Sub GetDMEmail()

        Dim strStoredProcedure As String
        Dim objConnection As New SqlClient.SqlConnection(ConfigurationSettings.AppSettings("strMainDB"))
        Dim objSQLDataReader As SqlClient.SqlDataReader

        Try
            objConnection.Open()
        Catch
            Try
                If objConnection.State <> ConnectionState.Closed Then
                    objConnection.Close()
                End If

                objConnection.ConnectionString = ConfigurationSettings.AppSettings("strMainDB_NoPool")
                objConnection.Open()

            Catch

                If objConnection.State <> ConnectionState.Closed Then
                    objConnection.Close()
                End If
            End Try
        End Try

        Try
            strStoredProcedure = "proc_GetDMEmailAddress " & gStoreNum & " "

            Dim objACommand As New SqlClient.SqlCommand(strStoredProcedure, objConnection)
            objSQLDataReader = objACommand.ExecuteReader()

            If objSQLDataReader.Read Then

                txtEmail.Text = objSQLDataReader("EmailAddress").ToString

            End If

            objSQLDataReader.Close()
            objACommand.Dispose()
            objConnection.Close()
        Catch
            If objConnection.State <> ConnectionState.Closed Then
                objConnection.Close()
            End If

            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Title=" & Server.UrlEncode("Warning") & "&Err=" & Server.UrlEncode("The database connection could not be resolved. Please close the application and try again.") & "','Warning','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")

        End Try
    End Sub

    Private Sub BindData()

        Dim objWTD As New clsWTD(ConfigurationSettings.AppSettings("strMainDB"), ConfigurationSettings.AppSettings("strMainDB_NoPool"))
        Dim objDataTable As New DataTable("MSP")
        Dim I As Integer

        objDataTable = objWTD.GetWTD(Now.AddHours(-4).Date, gStoreNum)

        lblDate.Text = "Date: " & Now.AddHours(-4).Date

        For I = 0 To objDataTable.Rows.Count - 1

            If objDataTable.Rows(I).Item("MSPDate") = Now.AddHours(-4).Date Then

                lblDTYValue.Text = String.Format("{0:$#,##0}", objDataTable.Rows(I).Item("DActualSales"))
                lblWTYValue.Text = String.Format("{0:$#,##0}", objDataTable.Rows(I).Item("WTDActualSalesAmt"))
                lblDLYValue.Text = String.Format("{0:$#,##0}", objDataTable.Rows(I).Item("DLYSales"))
                lblWLYValue.Text = String.Format("{0:$#,##0}", objDataTable.Rows(I).Item("WTDLYSalesAmt"))
                lblDPlanValue.Text = String.Format("{0:$#,##0}", objDataTable.Rows(I).Item("DPlanSales"))
                lblWPlanValue.Text = String.Format("{0:$#,##0}", objDataTable.Rows(I).Item("WTDPlanSalesAmt"))
                lblDADTValue.Text = String.Format("{0:$#,##0.00}", objDataTable.Rows(I).Item("Average"))
                lblDOSValue.Text = String.Format("{0:$#,##0.00}", objDataTable.Rows(I).Item("OS"))
                lblDUPTValue.Text = String.Format("{0:#,##0.00}", objDataTable.Rows(I).Item("UPT"))
                lblDAvPValue.Text = String.Format("{0:0.0%}", objDataTable.Rows(I).Item("DActvPlanPercent"))
                lblWAvPValue.Text = String.Format("{0:0.0%}", objDataTable.Rows(I).Item("WTDActvPlanPercent"))
                lblDAvLYValue.Text = String.Format("{0:0.0%}", objDataTable.Rows(I).Item("DActvLYPercent"))
                lblWAvLYValue.Text = String.Format("{0:0.0%}", objDataTable.Rows(I).Item("WTDActvLYPercent"))

            End If

        Next

        objWTD.CalculateWTDTotals(Now.AddHours(-4).Date, gStoreNum)

        lblWOSValue.Text = String.Format("{0:$#,##0.00}", objWTD.TotalOS)
        lblWADTValue.Text = String.Format("{0:$#,##0.00}", objWTD.TotalAverage)
        lblWUPTValue.Text = String.Format("{0:#,##0.00}", objWTD.TotalUPT)

    End Sub



    Private Sub btnEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmail.Click

        Dim objEmail As New clsEmail(ConfigurationSettings.AppSettings("strSmtpServer"), ConfigurationSettings.AppSettings("strSender"))
        Dim strBody As String

        strBody = "Store #" & gStoreNum.ToString.PadLeft(4, "0") & " Daily Sales Numbers" & vbCrLf & vbCrLf
        strBody = strBody & lblDate.Text & vbCrLf
        strBody = strBody & "DAILY" & vbCrLf
        strBody = strBody & "--------------------------------------------" & vbCrLf
        strBody = strBody & "TY:" & vbTab & vbTab & lblDTYValue.Text & vbCrLf
        strBody = strBody & "LY:" & vbTab & vbTab & lblDLYValue.Text & vbTab & lblDAvLYValue.Text & vbCrLf
        strBody = strBody & "PLAN:" & vbTab & vbTab & lblDPlanValue.Text & vbTab & lblDAvPValue.Text & vbCrLf
        strBody = strBody & "ADT:" & vbTab & vbTab & lblDADTValue.Text & vbCrLf
        strBody = strBody & "UPT:" & vbTab & vbTab & lblDUPTValue.Text & vbCrLf
        strBody = strBody & "CASH 0/S:" & vbTab & lblDOSValue.Text & vbCrLf & vbCrLf
        strBody = strBody & "WTD" & vbCrLf
        strBody = strBody & "--------------------------------------------" & vbCrLf
        strBody = strBody & "TY:" & vbTab & vbTab & lblWTYValue.Text & vbCrLf
        strBody = strBody & "LY:" & vbTab & vbTab & lblWLYValue.Text & vbTab & lblWAvLYValue.Text & vbCrLf
        strBody = strBody & "PLAN:" & vbTab & vbTab & lblWPlanValue.Text & vbTab & lblWAvPValue.Text & vbCrLf
        strBody = strBody & "ADT:" & vbTab & vbTab & lblWADTValue.Text & vbCrLf
        strBody = strBody & "UPT:" & vbTab & vbTab & lblWUPTValue.Text & vbCrLf
        strBody = strBody & "CASH 0/S:" & vbTab & lblWOSValue.Text & vbCrLf & vbCrLf

        strBody = strBody & vbCrLf & vbCrLf & Server.HtmlEncode(txtEmailBody.Text)

        If IsPageValid() Then
            If objEmail.SendEmail(strBody, txtEmail.Text, "Store #" & gStoreNum.ToString.PadLeft(4, "0") & " Daily Sales Numbers") Then
                Response.Redirect("EmailConformation.aspx")
            Else
                pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Title=" & Server.UrlEncode("Error") & "&Err=" & Server.UrlEncode("Email failed to Send..") & "','Error','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            End If
        End If

    End Sub

    Private Function IsPageValid() As Boolean

        Dim booValid As Boolean
        Dim strError As String

        booValid = True

        If Len(Trim(txtEmail.Text)) = 0 Then
            booValid = False
            strError = "[Email] is required.."
        End If

        If booValid Then
            Return True
        Else
            pageBody.Attributes.Add("onload", "javasvript:window.showModalDialog('Error.aspx?Title=" & Server.UrlEncode("Warning") & "&Err=" & Server.UrlEncode(strError) & "','Warning','center=1;scroll=0;status=no;dialogWidth=350px;dialogHeight=180px;');")
            Return False
        End If

    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("default.aspx")
    End Sub

End Class
