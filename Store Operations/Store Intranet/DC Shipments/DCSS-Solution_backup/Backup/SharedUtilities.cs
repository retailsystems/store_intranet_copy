﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace PWSClient
{
    class Util
    {
        public static void Print(string name, object o1, object o2)
        {
            Console.Write(offset);
            Console.WriteLine(name + ": {0} {1}", o1, o2);
        }

        public static void Print(string name, object o)
        {
            Console.Write(offset);
            if (o is string && string.IsNullOrEmpty((string)o))
                Console.WriteLine(name + ": - nil");
            else
                Console.WriteLine(name + ": {0}", o);
        }

        public static void Print(string name)
        {
            Console.Write(offset);
            Console.WriteLine(name);
        }

        private static string offset = string.Empty;
        public static void Push()
        {
            offset += "\t";
        }
        public static void Pop()
        {
            if (offset.Length > 0)
                offset = offset.Remove(offset.Length - 1, 1);
        }
    }
    public class ShipmentInfo
    {
        public string ScanType { get; set; }
        public string PIN { get; set; }        
        public string ScanDate { get; set; }
        public string ScanTime { get; set; }        
        public string Description { get; set; }
        public string Comment { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDesc { get; set; }
        public string ErrorAddlInfo { get; set; }

        //public ShipmentInfo(string scantype, string pin, string scandate, string scantime, string description)
        //{
        //    ScanType = scantype;
        //    PIN = pin;
        //    ScanDate = scandate;
        //    ScanTime = scantime;
        //    Description = description;
        //}
        //Other properties, methods, events...
        public void GetShipmentInfo()
        {
            Console.WriteLine("ScanType:: " + ScanType);
            Console.WriteLine("PIN:: " + PIN);
            Console.WriteLine("ScanDate:: " + ScanDate);
            Console.WriteLine("ScanTime:: " + ScanTime);
            Console.WriteLine("Description:: " + Description);
            Console.WriteLine("Comment:: " + Comment);
            Console.WriteLine("ErrorCode:: " + ErrorCode);
            Console.WriteLine("ErrorDesc:: " + ErrorDesc);
            Console.WriteLine("ErrorAddlInfo:: " + ErrorAddlInfo);
        }
    }
}