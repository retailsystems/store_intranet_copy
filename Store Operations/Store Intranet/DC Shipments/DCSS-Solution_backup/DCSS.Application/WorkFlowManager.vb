Imports HotTopic.DCSS.Services
Imports System.Data.SqlClient
Imports System.Data.OracleClient

Namespace HotTopic.DCSS.Application
    Public Class WorkFlowManager
        Public Shared Function GetShipmentDates() As DataSet
            Try
                Return SQLDataManager.GetInstance().GetDataSet("DCSP_GetShipDates")
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListShipmentsByDate(ByVal deliveryDate As Date, ByVal storeNum As Int16) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(1) {}
                arParms(0) = New SqlParameter("@scheduledDeliveryDate", SqlDbType.DateTime)
                arParms(0).Value = deliveryDate
                arParms(1) = New SqlParameter("@storeNum", SqlDbType.SmallInt)
                arParms(1).Value = storeNum
                Return SQLDataManager.GetInstance().GetDataSet("DCSP_GetShipmentsByDate", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'this function is for admin module use only
        Public Shared Function ListStoreShipmentsByDate(ByVal deliveryDate As Date, ByVal storeNum As Int16) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(1) {}
                arParms(0) = New SqlParameter("@scheduledDeliveryDate", SqlDbType.DateTime)
                arParms(0).Value = deliveryDate
                arParms(1) = New SqlParameter("@storeNum", SqlDbType.SmallInt)
                arParms(1).Value = storeNum
                Return SQLDataManager.GetInstance().GetDataSet("DCSP_GetStoreShipmentsByDate", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListPendingShipments(ByVal numDaysOld As Int16, ByVal whse As Int16) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(1) {}
                arParms(0) = New SqlParameter("@numDaysOld", SqlDbType.SmallInt)
                arParms(0).Value = numDaysOld
                arParms(1) = New SqlParameter("@whse", SqlDbType.SmallInt)
                arParms(1).Value = whse
                Return SQLDataManager.GetInstance().GetDataSet("DCSP_ListPendingShipments", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Shared Sub SqlQueryBuilder(ByRef sqlFilter As String, ByVal colName As String, ByVal colVal As String, ByVal dataType As SqlDbType, Optional ByVal CompareMode As String = "=")
            If Len(sqlFilter) > 0 Then
                sqlFilter &= " AND "
            Else
                sqlFilter &= " WHERE "
            End If
            Select Case dataType
                Case SqlDbType.Int, SqlDbType.BigInt, SqlDbType.Binary, SqlDbType.Float, SqlDbType.Money, SqlDbType.Decimal, SqlDbType.SmallInt, SqlDbType.SmallMoney, SqlDbType.Bit
                    sqlFilter &= colName & CompareMode & colVal
                Case SqlDbType.DateTime, SqlDbType.SmallDateTime
                    sqlFilter &= colName & CompareMode & "'" & colVal & "'"
                Case SqlDbType.VarChar, SqlDbType.NChar, SqlDbType.NVarChar, SqlDbType.Char, SqlDbType.Text, SqlDbType.NText
                    sqlFilter &= colName & CompareMode & "'" & UtilityManager.InsertApos(colVal) & "'"
                Case Else
                    sqlFilter &= colName & CompareMode & UtilityManager.InsertApos(colVal) & ""
            End Select
        End Sub
        Private Shared Sub SqlQueryBuilder(ByRef sqlFilter As String, ByVal colName As String, ByVal dateFrom As String, ByVal dateTo As String)
            If Len(sqlFilter) > 0 Then
                sqlFilter &= " AND "
            Else
                sqlFilter &= " WHERE "
            End If
            sqlFilter &= colName & " BETWEEN '" & dateFrom & "' AND '" & dateTo & "'"
        End Sub
        Public Shared Function ListTravelMatrix(ByVal searchParams As StoreTravelMatrix) As DataSet
            Try
                Dim strSql As String = ""
                Dim sqlFilter As String = ""
                strSql = "SELECT t.*, t.ServiceProvider, CASE t.ServiceProvider WHEN 1 THEN 'UPS' WHEN 4 THEN 'FedEx' WHEN 5 THEN 'Purolator' END AS ShippingCarrier, " & _
                        " s.StoreName, CASE Verified_Flag " & _
                        " WHEN 1 THEN 'YES' WHEN 0 THEN 'NO' END AS Status " & _
                        " ,CASE MasterWhseFlag WHEN 1 THEN 'YES' WHEN 0 THEN 'NO' END AS MasterWhse " & _
                        " FROM dcss_upstravelTime t INNER JOIN DCSS_AllHtTorridStores s " & _
                        " ON t.Store_Num = s.StoreNum "
                If searchParams.StoreNum > 0 Then
                    SqlQueryBuilder(sqlFilter, "t.Store_Num", searchParams.StoreNum, SqlDbType.VarChar, "=")
                End If
                If searchParams.Whse > 0 Then
                    SqlQueryBuilder(sqlFilter, "t.WHSE", searchParams.Whse, SqlDbType.Int, "=")
                End If
                If searchParams.Carrier > 0 Then
                    SqlQueryBuilder(sqlFilter, "t.ServiceProvider", searchParams.Carrier, SqlDbType.Int, "=")
                End If
                If Not IsNothing(searchParams.VerifiedFlag) AndAlso searchParams.VerifiedFlag >= 0 Then
                    SqlQueryBuilder(sqlFilter, "t.Verified_Flag", searchParams.VerifiedFlag, SqlDbType.Bit, "=")
                End If
                strSql &= sqlFilter
                Return SQLDataManager.GetInstance().GetDataSet(strSql)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function SearchShipments(ByVal searchParams As SearchParams) As DataSet
            Try
                Dim strSql As String = ""
                Dim sqlFilter As String = ""
                strSql = "SELECT  Carrier_DeliveredDate,shippedlpn,Tracking_statusDesc,UpsTracking,StoreNum,ServiceType,ShippedDateTime" & _
                        " ,PackageType,ups.StatusCD,sl.Description as Status,Comments,ReceiverName,WHSE" & _
                        " ,CASE WHEN ServiceProvider = 1 THEN 'http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&loc=en_MX&AgreeToTermsAndConditions=yes&tracknum=' + UpsTracking " & _
                        " WHEN ServiceProvider = 5 THEN 'https://www.purolator.com/purolator/ship-track/tracking-details.page?pin=' + UpsTracking " & _
                        " ELSE 'http://www.fedex.com/Tracking?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers=' + UpsTracking " & _
                        " END AS TrackingURL " & _
                        " FROM DCSS_AllHtTorridShipments ups LEFT OUTER JOIN DCSS_StatusLookUp sl" & _
                        " ON ups.StatusCD = sl.StatusCD "
                With searchParams
                    If IsDate(.PostDateFrom) AndAlso IsDate(.PostDateTo) Then
                        SqlQueryBuilder(sqlFilter, "ShippedDateTime", .PostDateFrom, .PostDateTo)
                    ElseIf IsDate(.PostDateFrom) Then
                        SqlQueryBuilder(sqlFilter, "ShippedDateTime", .PostDateFrom & " 00:00:00", .PostDateFrom & " 23:59:59")
                    ElseIf IsDate(.PostDateTo) Then
                        SqlQueryBuilder(sqlFilter, "ShippedDateTime", .PostDateTo & " 00:00:00", .PostDateTo & " 23:59:59")
                    End If
                    If IsNumeric(.StatusCD) Then
                        SqlQueryBuilder(sqlFilter, "ups.StatusCD", .StatusCD, SqlDbType.Int)
                    End If
                    If IsNumeric(.StoreNum) AndAlso .StoreNum > 0 Then
                        SqlQueryBuilder(sqlFilter, "StoreNum", .StoreNum, SqlDbType.Int)
                    End If
                    If .ServiceType <> "" Then
                        SqlQueryBuilder(sqlFilter, "ServiceType", .ServiceType, SqlDbType.VarChar)
                    End If
                    '12/02/04 Sri Bajjuri enabled Tracking # partial search.
                    If .TrackingNum <> "" Then
                        SqlQueryBuilder(sqlFilter, "UpsTracking", .TrackingNum, SqlDbType.VarChar, " LIKE ")
                    End If
                    If .PackageType <> "" Then
                        SqlQueryBuilder(sqlFilter, "PackageType", .PackageType, SqlDbType.VarChar)
                    End If
                    If .ManifestNbr <> "" Then
                        SqlQueryBuilder(sqlFilter, "ManifestNbr", .ManifestNbr, SqlDbType.VarChar)
                    End If
                    If Not IsNothing(.WHSE) AndAlso .WHSE > 0 Then
                        SqlQueryBuilder(sqlFilter, "WHSE", .WHSE, SqlDbType.Int, "=")
                    End If
                End With
                If sqlFilter = "" Then
                    Throw New DCSSAppException("Please fill in at least one search parameter")
                Else
                    strSql &= sqlFilter
                End If

                Return SQLDataManager.GetInstance().GetDataSet(strSql)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function SearchShipmentItems(ByVal store As UInt16, ByVal item As String, ByVal itemdesc As String, ByVal fromSHPDate As String, ByVal toSHPDate As String, ByVal trackingnbr As String) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(5) {}
                arParms(0) = New SqlParameter("@Item", SqlDbType.VarChar, 12)
                arParms(0).Value = item
                arParms(1) = New SqlParameter("@StoreNum", SqlDbType.SmallInt, 4)
                arParms(1).Value = store
                arParms(2) = New SqlParameter("@ItemDesc", SqlDbType.VarChar, 100)
                arParms(2).Value = itemdesc
                arParms(3) = New SqlParameter("@FromSHPDate", SqlDbType.VarChar, 20)
                arParms(3).Value = fromSHPDate
                arParms(4) = New SqlParameter("@ToSHPDate", SqlDbType.VarChar, 20)
                arParms(4).Value = toSHPDate
                arParms(5) = New SqlParameter("@TrackingNum", SqlDbType.VarChar, 30)
                arParms(5).Value = trackingnbr

                Return SQLDataManager.GetInstance().GetDataSet("DCSP_GetShipmentItems", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListLateShipments(ByVal searchParams As SearchParams) As DataSet
            Try
                Dim strSql As String = ""
                Dim sqlFilter As String = ""
                strSql = "SELECT  UpsTracking,StoreNum,ServiceType,ShippedDateTime" & _
                        " ,OriginalScheduledDeliveryDate, DeliveredDate, IgnoreLateShipmentFlag " & _
                        " ,PackageType,StatusCD,Status,Comments,ReceiverName,WHSE" & _
                        " ,CASE WHEN ServiceProvider = 1 THEN 'http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&loc=en_MX&AgreeToTermsAndConditions=yes&tracknum=' + UpsTracking " & _
                        " WHEN ServiceProvider = 5 THEN 'https://www.purolator.com/purolator/ship-track/tracking-details.page?pin=' + UpsTracking " & _
                        " Else 'http://www.fedex.com/Tracking?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers=' + UpsTracking " & _
                        " End AS TrackingURL " & _
                        " FROM DCSS_AllHtTorridShipments "
                sqlFilter = " WHERE DATEDIFF(day,OriginalScheduledDeliveryDate,DeliveredDate ) > 0 " & _
                            " AND StatusCD = " & ShipmentStatus.Received
                With searchParams
                    If IsDate(.PostDateFrom) AndAlso IsDate(.PostDateTo) Then
                        SqlQueryBuilder(sqlFilter, "ShippedDateTime", .PostDateFrom, .PostDateTo)
                    ElseIf IsDate(.PostDateFrom) Then
                        SqlQueryBuilder(sqlFilter, "ShippedDateTime", .PostDateFrom & " 00:00:00", .PostDateFrom & " 23:59:59")
                    ElseIf IsDate(.PostDateTo) Then
                        SqlQueryBuilder(sqlFilter, "ShippedDateTime", .PostDateTo & " 00:00:00", .PostDateTo & " 23:59:59")
                    End If
                    If Not .ShowAllLateShipments Then
                        SqlQueryBuilder(sqlFilter, "IgnoreLateShipmentFlag", "1", SqlDbType.Bit, "<>")
                    End If
                    If Not IsNothing(.WHSE) AndAlso .WHSE > 0 Then
                        SqlQueryBuilder(sqlFilter, "WHSE", .WHSE, SqlDbType.Int, "=")
                    End If
                End With
                strSql &= sqlFilter
                'Throw New DCSSAppException(strSql)
                Return SQLDataManager.GetInstance().GetDataSet(strSql)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Author:SRI BAJJURI - 3/27/06 - TravelTime Exception report
        Public Shared Function ListTravelTimeExceptions(ByVal searchParams As SearchParams) As DataSet
            Try
                Dim strSql As String = ""
                Dim sqlFilter As String = ""
                strSql = "SELECT  UpsTracking,StoreNum,ServiceType,ShippedDateTime" & _
                        " ,OriginalScheduledDeliveryDate, ScheduledDeliveryDate, DeliveredDate " & _
                        " ,PackageType,StatusCD,Status,Comments,ReceiverName,WHSE" & _
                        " ,CASE WHEN ServiceProvider = 1 THEN 'http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&loc=en_MX&AgreeToTermsAndConditions=yes&tracknum=' + UpsTracking " & _
                        "  WHEN ServiceProvider = 5 THEN 'https://www.purolator.com/purolator/ship-track/tracking-details.page?pin=' + UpsTracking " & _
                        " Else 'http://www.fedex.com/Tracking?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers=' + UpsTracking " & _
                        " End AS TrackingURL " & _
                        " FROM DCSS_AllHtTorridShipments "
                sqlFilter = " WHERE DATEDIFF(day,OriginalScheduledDeliveryDate,ScheduledDeliveryDate ) <> 0 "

                With searchParams
                    If IsDate(.PostDateFrom) AndAlso IsDate(.PostDateTo) Then
                        SqlQueryBuilder(sqlFilter, "ShippedDateTime", .PostDateFrom, .PostDateTo)
                    ElseIf IsDate(.PostDateFrom) Then
                        SqlQueryBuilder(sqlFilter, "ShippedDateTime", .PostDateFrom & " 00:00:00", .PostDateFrom & " 23:59:59")
                    ElseIf IsDate(.PostDateTo) Then
                        SqlQueryBuilder(sqlFilter, "ShippedDateTime", .PostDateTo & " 00:00:00", .PostDateTo & " 23:59:59")
                    End If
                    If IsNumeric(.StoreNum) AndAlso .StoreNum > 0 Then
                        SqlQueryBuilder(sqlFilter, "StoreNum", .StoreNum, SqlDbType.Int)
                    End If
                    If Not IsNothing(.WHSE) AndAlso .WHSE > 0 Then
                        SqlQueryBuilder(sqlFilter, "WHSE", .WHSE, SqlDbType.Int, "=")
                    End If
                End With
                strSql &= sqlFilter
                'Throw New DCSSAppException(strSql)
                Return SQLDataManager.GetInstance().GetDataSet(strSql)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListServiceTypes() As DataSet
            Try
                'returns all hottopic and torrid stores
                Return SQLDataManager.GetInstance().GetDataSet("DCSP_ListServiceTypes")
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListAllStores() As DataSet
            Try
                'returns all hottopic and torrid stores
                Return SQLDataManager.GetInstance().GetDataSet("DCSP_ListAllStores")
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListRegionStores(ByVal regionId As String) As DataSet
            Try
                'returns all stores belongs to a region
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@regionId", SqlDbType.VarChar, 5)
                arParms(0).Value = regionId
                Return SQLDataManager.GetInstance().GetDataSet("DCSP_ListRegionStores", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListDistrictStores(ByVal districtId As String) As DataSet
            Try
                'returns all stores belongs to a district
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@districtId", SqlDbType.VarChar, 5)
                arParms(0).Value = districtId
                Return SQLDataManager.GetInstance().GetDataSet("DCSP_ListDistrictStores", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetStoreMonthlyShipments(ByVal mm As Int16, ByVal yy As Int16, ByVal storeNum As Int16) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                arParms(0) = New SqlParameter("@mm", SqlDbType.SmallInt)
                arParms(0).Value = mm
                arParms(1) = New SqlParameter("@yy", SqlDbType.SmallInt)
                arParms(1).Value = yy
                arParms(2) = New SqlParameter("@storeNum", SqlDbType.SmallInt)
                arParms(2).Value = storeNum
                Return SQLDataManager.GetInstance().GetDataSet("DCSP_GetStoreMonthlyShipments", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListMonthlyShipmentsByStore(ByVal mm As Int16, ByVal yy As Int16, ByVal storeNum As Int16) As DataSet
            'this function is for admin module use
            Try
                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                arParms(0) = New SqlParameter("@mm", SqlDbType.SmallInt)
                arParms(0).Value = mm
                arParms(1) = New SqlParameter("@yy", SqlDbType.SmallInt)
                arParms(1).Value = yy
                arParms(2) = New SqlParameter("@storeNum", SqlDbType.SmallInt)
                arParms(2).Value = storeNum
                Return SQLDataManager.GetInstance().GetDataSet("DCSP_ListMonthlyShipmentsByStore", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetShipmentCount(ByVal beginDate As String, ByVal endDate As String) As Integer
            Try
                Dim ds As DataSet
                Dim cnt As Integer = -1
                Dim arParms() As SqlParameter = New SqlParameter(1) {}
                arParms(0) = New SqlParameter("@beginDate", SqlDbType.VarChar, 20)
                arParms(0).Value = beginDate
                arParms(1) = New SqlParameter("@endDate", SqlDbType.VarChar, 20)
                arParms(1).Value = endDate
                ds = SQLDataManager.GetInstance().GetDataSet("DCSP_GetShipmentCount", arParms)
                If ds.Tables(0).Rows.Count > 0 Then
                    cnt = ds.Tables(0).Rows(0).Item("cnt")
                End If
                If cnt = -1 Then
                    Throw New DCSSAppException("Unable to fetch shipment count, try again later")
                End If
                Return cnt
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetCoOrdinatorEmail(ByVal StoreNum As Int16) As String
            Dim CoOrdinatorEmail As String = "helpdesk@hottopic.com"
            Try
                Dim arParms() As SqlParameter = New SqlParameter(1) {}
                arParms(0) = New SqlParameter("@StoreNum", SqlDbType.Int)
                arParms(0).Value = StoreNum
                arParms(1) = New SqlParameter("@CoOrdinatorEmail", SqlDbType.VarChar, 255)
                arParms(1).Direction = ParameterDirection.Output
                SQLDataManager.GetInstance().Execute("DCSP_GetCoOrdinatorEmail", arParms)
                CoOrdinatorEmail = arParms(1).Value
            Catch ex As Exception
                'do nothing
            End Try
            Return CoOrdinatorEmail
        End Function
        Public Shared Sub ExportData(ByVal beginDate As String, ByVal endDate As String, ByVal cnt As Integer, ByVal adminCnt As Integer, ByVal UserID As String, ByVal UserName As String)
            Try
                Dim arParms() As SqlParameter = New SqlParameter(4) {}
                arParms(0) = New SqlParameter("@beginDate", SqlDbType.VarChar, 20)
                arParms(0).Value = beginDate
                arParms(1) = New SqlParameter("@endDate", SqlDbType.VarChar, 20)
                arParms(1).Value = endDate
                arParms(2) = New SqlParameter("@runDateFrom", SqlDbType.DateTime)
                arParms(2).Value = StringToDate(beginDate)
                arParms(3) = New SqlParameter("@runDateTo", SqlDbType.DateTime)
                arParms(3).Value = StringToDate(endDate)
                arParms(4) = New SqlParameter("@UserID", SqlDbType.VarChar, 20)
                arParms(4).Value = UserID
                SQLDataManager.GetInstance().Execute("DCSP_ImportShipmentData", arParms)
                'MOD #1/19/04# send e-mail to admin upon sucess
                Dim emailMgr As EmailManager = New EmailManager(HotTopic.DCSS.Settings.AppSetting.SmtpServer)
                Dim mailBody As String
                mailBody = HotTopic.DCSS.Settings.AppSetting.SuccessEmailBody
                mailBody = mailBody.Replace("%beginDate%", beginDate)
                mailBody = mailBody.Replace("%endDate%", endDate)
                mailBody = mailBody.Replace("%cnt%", cnt)
                mailBody = mailBody.Replace("%adminCnt%", adminCnt)
                mailBody = mailBody.Replace("%userName%", UserName)
                With emailMgr
                    .AddRecipient(HotTopic.DCSS.Settings.AppSetting.DcssMailReceivers)
                    .Sender = HotTopic.DCSS.Settings.AppSetting.SmtpSender
                    .Subject = "DC shipment export job alert"
                    .Format = Web.Mail.MailFormat.Html
                    .Priority = Web.Mail.MailPriority.Normal
                    .Send(mailBody)
                End With
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw New DCSSAppException(ex.Message)
            End Try
        End Sub

        Public Shared Sub RunDataFix(ByVal UserID As String)
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@UserID", SqlDbType.VarChar, 20)
                arParms(0).Value = UserID
                SQLDataManager.GetInstance().Execute("DCSP_RunDataFix", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub CancelExport(ByVal beginDate As String, ByVal endDate As String, ByVal cnt As Integer, ByVal adminCnt As Integer, ByVal UserID As String, ByVal UserName As String)
            Try

                Dim arParms() As SqlParameter = New SqlParameter(3) {}
                arParms(0) = New SqlParameter("@beginDate", SqlDbType.DateTime)
                arParms(0).Value = StringToDate(beginDate)
                arParms(1) = New SqlParameter("@endDate", SqlDbType.DateTime)
                arParms(1).Value = StringToDate(endDate)
                arParms(2) = New SqlParameter("@statusFl", SqlDbType.Int)
                arParms(2).Value = JobStatus.Cancelled
                arParms(3) = New SqlParameter("@UserID", SqlDbType.VarChar, 20)
                arParms(3).Value = UserID
                SQLDataManager.GetInstance().Execute("DCSP_UpdateJobHistory", arParms)

            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
            'SEND Cancellation email
            Try
                Dim emailMgr As EmailManager = New EmailManager(HotTopic.DCSS.Settings.AppSetting.SmtpServer)
                Dim mailBody As String


                mailBody = HotTopic.DCSS.Settings.AppSetting.CancellationEmailBody
                mailBody = mailBody.Replace("%beginDate%", beginDate)
                mailBody = mailBody.Replace("%endDate%", endDate)
                mailBody = mailBody.Replace("%cnt%", cnt)
                mailBody = mailBody.Replace("%adminCnt%", adminCnt)
                mailBody = mailBody.Replace("%userName%", UserName)
                With emailMgr
                    .AddRecipient(HotTopic.DCSS.Settings.AppSetting.DcssMailReceivers)
                    .Sender = HotTopic.DCSS.Settings.AppSetting.SmtpSender
                    .Subject = "DC shipment export job cancellation alert"
                    .Format = Web.Mail.MailFormat.Html
                    .Priority = Web.Mail.MailPriority.High
                    .Send(mailBody)
                End With
            Catch ex As Exception
                Throw New DCSSAppException(ex.Message)
            End Try
        End Sub
        Public Shared Sub UpdateShipmentStatus(ByVal upsTracking As String, ByVal statusCd As ShipmentStatus, ByVal userID As String)
            Try
                Dim arParms() As SqlParameter
                If statusCd = ShipmentStatus.Received Then
                    arParms = New SqlParameter(3) {}
                    arParms(0) = New SqlParameter("@UpsTracking", SqlDbType.VarChar, 30)
                    arParms(0).Value = upsTracking
                    arParms(1) = New SqlParameter("@StatusCD", SqlDbType.SmallInt)
                    arParms(1).Value = statusCd
                    arParms(2) = New SqlParameter("@UserID", SqlDbType.VarChar)
                    arParms(2).Value = userID
                    arParms(3) = New SqlParameter("@DeliveredDate", SqlDbType.DateTime)
                    arParms(3).Value = Now
                Else
                    arParms = New SqlParameter(2) {}
                    arParms(0) = New SqlParameter("@UpsTracking", SqlDbType.VarChar, 30)
                    arParms(0).Value = upsTracking
                    arParms(1) = New SqlParameter("@StatusCD", SqlDbType.SmallInt)
                    arParms(1).Value = statusCd
                    arParms(2) = New SqlParameter("@UserID", SqlDbType.VarChar)
                    arParms(2).Value = userID
                End If

                SQLDataManager.GetInstance().Execute("DCSP_UpdateShipmentStatus", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub UpdateShipmentInfo(ByVal upsTracking As String, ByVal statusCd As Int16, ByVal comments As String, ByVal userID As String)
            Try
                Dim arParms() As SqlParameter = New SqlParameter(3) {}
                arParms(0) = New SqlParameter("@UpsTracking", SqlDbType.VarChar, 30)
                arParms(0).Value = upsTracking
                arParms(1) = New SqlParameter("@StatusCD", SqlDbType.SmallInt)
                arParms(1).Value = statusCd
                arParms(2) = New SqlParameter("@Comments", SqlDbType.NVarChar, 2000)
                arParms(2).Value = comments
                arParms(3) = New SqlParameter("@UserID", SqlDbType.VarChar)
                arParms(3).Value = userID
                SQLDataManager.GetInstance().Execute("DCSP_UpdateShipmentInfo", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub UpdateLateShipmentInfo(ByVal upsTracking As String, ByVal IgnoreLateShipment As Int16, ByVal comments As String, ByVal userID As String)
            Try
                Dim arParms() As SqlParameter = New SqlParameter(3) {}
                arParms(0) = New SqlParameter("@UpsTracking", SqlDbType.VarChar, 30)
                arParms(0).Value = upsTracking
                arParms(1) = New SqlParameter("@IgnoreLateShipmentFlag", SqlDbType.Bit)
                arParms(1).Value = IgnoreLateShipment
                arParms(2) = New SqlParameter("@Comments", SqlDbType.NVarChar, 2000)
                arParms(2).Value = comments
                arParms(3) = New SqlParameter("@UserID", SqlDbType.VarChar)
                arParms(3).Value = userID
                SQLDataManager.GetInstance().Execute("DCSP_UpdateLateShipmentInfo", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub UpdateTravelMatrix(ByVal travelMatrix As StoreTravelMatrix, ByVal UserID As String)
            Try
                Dim arParms() As SqlParameter = New SqlParameter(10) {}
                arParms(0) = New SqlParameter("@TravelMatrixID", SqlDbType.SmallInt)
                arParms(0).Value = travelMatrix.TravelMatrixID
                arParms(1) = New SqlParameter("@next_travel", SqlDbType.SmallInt)
                arParms(1).Value = travelMatrix.NextTravel
                arParms(2) = New SqlParameter("@nextam_travel", SqlDbType.SmallInt)

                If travelMatrix.NextAmTravel > 0 Then
                    arParms(2).Value = travelMatrix.NextAmTravel
                Else
                    arParms(2).Value = System.DBNull.Value
                End If

                arParms(3) = New SqlParameter("@nextsaver_travel", SqlDbType.SmallInt)
                If travelMatrix.NextSaverTravel > 0 Then
                    arParms(3).Value = travelMatrix.NextSaverTravel
                Else
                    arParms(3).Value = System.DBNull.Value
                End If

                arParms(4) = New SqlParameter("@day2_travel", SqlDbType.SmallInt)
                arParms(4).Value = travelMatrix.Day2Travel

                arParms(5) = New SqlParameter("@day2am_travel", SqlDbType.SmallInt)
                If travelMatrix.Day2AmTravel > 0 Then
                    arParms(5).Value = travelMatrix.Day2AmTravel
                Else
                    arParms(5).Value = System.DBNull.Value
                End If

                arParms(6) = New SqlParameter("@day3_travel", SqlDbType.SmallInt)
                arParms(6).Value = travelMatrix.Day3Travel
                arParms(7) = New SqlParameter("@ground_travel", SqlDbType.SmallInt)
                arParms(7).Value = travelMatrix.GroundTravel
                arParms(8) = New SqlParameter("@verified_flag", SqlDbType.Bit)
                arParms(8).Value = travelMatrix.VerifiedFlag
                arParms(9) = New SqlParameter("@UserID", SqlDbType.VarChar, 20)
                arParms(9).Value = UserID
                arParms(10) = New SqlParameter("@MasterWhseFlag", SqlDbType.Bit)
                arParms(10).Value = travelMatrix.MasterWhseFlag
                SQLDataManager.GetInstance().Execute("DCSP_UpdateTravelMatrixByStore", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub AddManifestInfo(ByVal manifestData As ManifestData, ByVal userID As String)
            Try
                Dim arParms() As SqlParameter = New SqlParameter(4) {}
                arParms(0) = New SqlParameter("@ManifestNumber", SqlDbType.VarChar, 50)
                arParms(0).Value = manifestData.ManifestNum
                arParms(1) = New SqlParameter("@ServiceProvider", SqlDbType.Int)
                arParms(1).Value = manifestData.ServiceProvider
                arParms(2) = New SqlParameter("@whse", SqlDbType.Int)
                arParms(2).Value = manifestData.WearhouseNum
                arParms(3) = New SqlParameter("@CloseDate", SqlDbType.SmallDateTime)
                arParms(3).Value = manifestData.CloseDate
                arParms(4) = New SqlParameter("@UserID", SqlDbType.VarChar, 50)
                arParms(4).Value = userID
                SQLDataManager.GetInstance().Execute("DCSP_AddManifestDate", arParms)
            Catch ex As SqlException
                If ex.Message.IndexOf("duplicate key") > 0 Then
                    Throw New DCSSAppException("Duplicate Manifest number")
                Else
                    Throw New DCSSAppException(ex.Message)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Public Shared Sub UpdateManifestInfoByID(ByVal manifestData As ManifestData, ByVal userID As String)
        '    Try
        '        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        '        arParms(0) = New SqlParameter("@ManifestID", SqlDbType.Int)
        '        arParms(0).Value = manifestData.ManifestID
        '        arParms(1) = New SqlParameter("@CloseDate", SqlDbType.SmallDateTime)
        '        arParms(1).Value = manifestData.CloseDate
        '        arParms(2) = New SqlParameter("@UserID", SqlDbType.VarChar, 50)
        '        arParms(2).Value = userID
        '        SQLDataManager.GetInstance().Execute("DCSP_UpdateManifestDate", arParms)
        '    Catch ex As SqlException
        '        Throw New DCSSAppException(ex.Message)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        Public Shared Sub CreateShipmentFileByManifest(ByVal strConn As String, ByVal manifestNbr As String, ByVal userID As String)
            Try
                Dim arParms() As OracleParameter = New OracleParameter(0) {}
                arParms(0) = New OracleParameter("p_MANIF_NBR", OracleType.VarChar)
                arParms(0).Value = manifestNbr
                Dim wmsCn As OracleDataManager = New OracleDataManager(strConn)
                wmsCn.ExecuteNonQuery("HT_DCSS.DCSP_FILE_PROCESS_BY_MANIFEST", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As OracleException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub UpdateManifestInfoByID(ByVal strConn As String, ByVal manifestData As ManifestData, ByVal userID As String)
            Try
                Dim arParms() As OracleParameter = New OracleParameter(11) {}
                arParms(0) = New OracleParameter("p_MANIF_NBR", OracleType.VarChar)
                arParms(0).Value = manifestData.ManifestNum
                arParms(1) = New OracleParameter("p_WHSE", OracleType.VarChar)
                arParms(1).Value = manifestData.WearhouseNum
                arParms(2) = New OracleParameter("p_SERVICE_PROVIDER", OracleType.Int16)
                arParms(2).Value = manifestData.ServiceProvider
                arParms(3) = New OracleParameter("p_CLOSE_DATE", OracleType.DateTime)
                arParms(3).Value = manifestData.CloseDate
                arParms(4) = New OracleParameter("p_SHIP_DATE", OracleType.DateTime)
                arParms(4).Value = manifestData.ShipDate
                arParms(5) = New OracleParameter("p_STAT_CD", OracleType.Int16)
                arParms(5).Value = manifestData.StatusCD
                arParms(6) = New OracleParameter("p_USER_ID", OracleType.VarChar)
                arParms(6).Value = userID
                arParms(7) = New OracleParameter("p_LOCAL_PATH", OracleType.VarChar)
                arParms(7).Value = manifestData.LocalPath
                arParms(8) = New OracleParameter("p_FTP_HOST", OracleType.VarChar)
                arParms(8).Value = manifestData.FtpHost
                arParms(9) = New OracleParameter("p_FTP_REMOTE_PATH", OracleType.VarChar)
                arParms(9).Value = manifestData.FtpFolder
                arParms(10) = New OracleParameter("p_FTP_USER", OracleType.VarChar)
                arParms(10).Value = manifestData.FtpUser
                arParms(11) = New OracleParameter("p_FTP_PWD", OracleType.VarChar)
                arParms(11).Value = manifestData.FtpPwd

                Dim wmsCn As OracleDataManager = New OracleDataManager(strConn)
                wmsCn.ExecuteNonQuery("HT_DCSS.DCSP_UpdateManifest", arParms)
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As OracleException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function SubmitPurolatorOrderForReprocess(ByVal strConn As String, ByVal strCartonNbr As String, ByVal strUserID As String) As String
            Dim strError As String = String.Empty
            Try
                Dim arParms() As OracleParameter = New OracleParameter(2) {}
                arParms(0) = New OracleParameter("I_carton_number", OracleType.VarChar, 200)
                arParms(0).Value = strCartonNbr
                arParms(0).Direction = ParameterDirection.Input
                arParms(1) = New OracleParameter("I_user", OracleType.VarChar, 200)
                arParms(1).Value = strUserID
                arParms(1).Direction = ParameterDirection.Input
                arParms(2) = New OracleParameter("O_error_message", OracleType.VarChar, 200)
                arParms(2).Direction = ParameterDirection.Output
                arParms(2).Value = String.Empty

                Dim wmsCn As OracleDataManager = New OracleDataManager(strConn)
                wmsCn.ExecuteNonQuery("HT_PUROLATOR_CARTON_FEED.HT_RETRIGGER_CARTON_STAGE_LOAD", arParms)

                If arParms(2).Value.ToString().Length > 0 Then
                    strError = arParms(2).Value.ToString()
                End If
                Return strError
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As OracleException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListManifest() As DataSet
            'DCSP_ListManifest
            Try
                Dim ds As DataSet
                Dim arParms() As SqlParameter = New SqlParameter() {}
                ds = SQLDataManager.GetInstance().GetDataSet("DCSP_ListManifest", arParms)
                Return ds
            Catch ex As SqlException
                'Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListManifest(ByVal strConn As String) As DataSet
            'DCSP_ListManifest
            Try
                Dim ds As DataSet
                Dim arParms() As OracleParameter = New OracleParameter(0) {}
                arParms(0) = New OracleParameter("p_cur_manifest", OracleType.Cursor)
                arParms(0).Direction = ParameterDirection.Output
                Dim wmsCn As OracleDataManager = New OracleDataManager(strConn)
                ds = wmsCn.ExecuteDataSet("HT_DCSS.DCSP_ListManifest", arParms)
                Return ds
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub SendMail(ByVal SmtpServer As String, ByVal SmtpSender As String, ByVal mailTo As String, ByVal mailSubject As String, ByVal mailBody As String, Optional ByVal ccEmail As String = "")
            Try
                Dim emailMgr As EmailManager = New EmailManager(SmtpServer)
                With emailMgr
                    .Sender = SmtpSender
                    .AddRecipient(mailTo)
                    .Subject = mailSubject
                    If ccEmail.Trim.Length > 0 Then
                        .AddCC(ccEmail)
                    End If

                    'make sure to send only if there's a recipient
                    If Not emailMgr.Recipient Is Nothing Then
                        If emailMgr.Recipient <> "" Then
                            emailMgr.Send(mailBody)
                        End If
                    End If
                End With

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        ' Convert a date string of type "mm/dd/yyyy hhmm" to "mm/dd/yyyy hh:mm:ss"
        Private Shared Function StringToDate(ByVal dtStr As String) As DateTime
            Dim dt As DateTime
            dt = Left(dtStr, 13) & ":" & Right(dtStr, 2) & ":00"
            Return dt
        End Function

    End Class
    Public Enum JobStatus
        Success = 1
        Cancelled = 2
        Failed = 3
    End Enum
    Public Enum ShipmentStatus
        Shipped = 0
        InTransit = 1
        Delivered = 2
        Received = 3
        PickUp = 4
        ManifestPickUp = 5
        Exception = 6
    End Enum
    Public Enum UserRole
        DM = 4
        Store = 5
        RM = 6
    End Enum
    <Serializable()> _
   Public Structure SearchParams
        Public PackageType As String
        Public PostDateFrom As String
        Public PostDateTo As String
        Public ServiceType As String
        Public StoreNum As String
        Public StatusCD As String
        Public TrackingNum As String
        Public ShowAllLateShipments As Boolean
        '12/02/04 Sri Bajjuri added Manifest# to search params
        Public ManifestNbr As String
        '1/13/04 TNDC Enhancements
        Public WHSE As Int16
    End Structure

    Public Structure ManifestData
        'Public ManifestID As Integer
        Public ServiceProvider As Int16 '1-UPS,2-LYN,3-OTHER
        Public ManifestNum As String
        Public WearhouseNum As Int16
        Public CloseDate As Date
        Public ShipDate As Date
        Public StatusCD As Int16 '0 - new, 1- Ready, 3-Exported
        Public LocalPath As String
        Public FtpHost As String
        Public FtpFolder As String
        Public FtpUser As String
        Public FtpPwd As String

    End Structure
    Public Structure StoreTravelMatrix
        Public TravelMatrixID As Int16
        Public StoreNum As Int16
        Public NextTravel As Int16
        Public NextAmTravel As Int16
        Public NextSaverTravel As Int16
        Public Day2Travel As Int16
        Public Day2AmTravel As Int16
        Public Day3Travel As Int16
        Public GroundTravel As Int16
        Public VerifiedFlag As Int16
        Public Whse As Int16
        Public Carrier As Int16
        Public MasterWhseFlag As Boolean
    End Structure
    <Serializable()> _
    Public Structure WareHouse
        Public WhseID As Int16
        Public WhseName As String
        Public DbConnection As String
        Public Company As String
        Public ActiveFlag As Boolean
        Public AdminEmail As String
        Public CoOrdinatorEmail As String
    End Structure
End Namespace

