﻿<%@ Page Language="vb" ValidateRequest="false" AutoEventWireup="false" CodeBehind="CartonSearch.aspx.vb" Inherits="DCSS.CartonSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
    <LINK href="<%=Application("StyleSheet")%>" type="text/css" rel="stylesheet">
    <link id="Link1" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" runat="server" />
    <link id="Link2" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" runat="server" />
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->
    <style type="text/css">
        .cellvalueleft
        {}
    </style>
</head>
<body>
<!--#include file="include/calender.js"-->
<iframe id="CalFrame" style="DISPLAY: none; Z-INDEX: 100; WIDTH: 148px; POSITION: absolute; HEIGHT: 194px" marginWidth="0" marginHeight="0" src="include/calendar/calendar.htm" frameBorder="0" noResize scrolling="no"></iframe>
    <form id="Form1"  method="post" runat="server">
    
    <div class="panel panel-primary">
    <!-- Default panel contents -->
            <div class="panel-heading">
                Carton Search
        </div>
         <div class="panel-body">
    <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
      
       <tr>
               <table border="0">
               <tr>
					<td colspan="2" align="middle">
                       <asp:Label ID="lblError" CssClass="ErrStyle" Runat="server" Visible="False" ForeColor="Red"></asp:Label>
					</td>
				</tr>
               <tr>
               <td class="cellvaluecaption" align="left">Item or SKU: </td>
               <td align="left"><asp:TextBox ID="txtSku" runat="server" Width="170px"></asp:TextBox>
               </td>
               </tr>
              <tr> 
               <td class="cellvaluecaption" align="left">Item Description: </td>
               <td  align="left"><asp:TextBox ID="txtItemDesc" runat="server" Width="324px"></asp:TextBox></td>
               </tr>
               <tr>
               <td class="cellvaluecaption" align="left">Shipment Date From: </td>
               <td width="330"  class="cellvaluecaption"><asp:TextBox ID="txtPostDateFrom" 
                       Runat="server" CssClass="cellvalueleft" Width="110px"></asp:TextBox>
								
               <a onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar1,document.Form1.txtPostDateFrom,null,0,330)">

	           <img id="calendar1" src="include/calendar/calendar.gif" width=34px align="absBottom" border="0" name="calendar1"></a>
					To: <asp:TextBox ID="txtPostDateTo" Runat="server" CssClass="cellvalueleft" Width="110px"></asp:TextBox>
		       <a onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar2,document.Form1.txtPostDateTo,null,0,330)">
               <img id="calendar2" src="include/calendar/calendar.gif" width=34px align="absBottom" border="0" name="calendar2"></a>
								
                                        </td>              

               </tr>
               <tr> 
               <td class="cellvaluecaption" align="left">TrackingNumber </td>
               <td  align="left"><asp:TextBox ID="txttrackingnum" runat="server" Width="324px"></asp:TextBox></td>
               </tr>
               <tr>
               <td></td>
               <td align ="center"><asp:Button ID="btnSearch" Text="Search" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
               <asp:Button ID="btnClear" Text="Clear" runat="server" /></td><td></td>
               </tr>

       </table>
       </tr>
       </div>
       <tr>
         <div class="panel-heading">
                Carton Search Results
        </div>
      </tr>
      <div class="panel-body">
       <tr>
      <asp:Panel ID="pnlDgShipmentItem" runat="server">      
      <asp:Label ID="lblRecCount" runat="server"></asp:Label>
             <asp:GridView ID="dgShipmentItems" 
                    runat="server" Width="100%"
                 EnableModelValidation="True" cssclass="table archtbl" font-size="11px" gridlines="None" AllowPaging="True"
                 OnPageIndexChanging="OnPageIndexChanging" PageSize="10">    
										<headerstyle cssclass="archtblhdr"></headerstyle>
                </asp:GridView>
                </asp:Panel>
       </tr>
       </div>
       
    
    </table>
    </div>
    </form>
     <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
</body>
</html>
