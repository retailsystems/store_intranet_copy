Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Partial Class ListShipments
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.DCSS.Application.Session



    Private _ds As DataSet
    Private _storeNum As Int16 = 0
    Protected kioskBoxCnt As Int16 = 0
    Private strTrackingNum As String = String.Empty
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            lblError.Text = ""
            lblError.Visible = False
            _sessionUser = New Session(HttpContext.Current)
            If Not Request("store") Is Nothing AndAlso IsNumeric(Request("store")) Then
                _storeNum = Request("store")
            End If
            lblDeliveryDate.Text = Request.QueryString("dt")

            If Not IsDate(lblDeliveryDate.Text) Then
                Throw New DCSSAppException("Invalid Delivery Date")
            End If
            'lblEmailTo.Text = Application("ShipCoOrdinatorEmail")
            lblEmailTo.Text = _sessionUser.GetCoOrdinatorEmail(_storeNum)
        Catch ex As StoreNotFoundException
            Response.Redirect("StoreNotFound.aspx")
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        If Not IsPostBack Then

            BindData()
        End If
    End Sub
    Private Sub BindData()
        Try
            _ds = _sessionUser.ListShipmentsByDate(lblDeliveryDate.Text, _storeNum)
            dgShipment.DataSource = _ds
            'Dim txtBoxTrcNums As New TextBox
            If _ds.Tables(0).Rows.Count > 0 Then

                Dim dr As DataRow
                For Each dr In _ds.Tables(0).Rows
                    strTrackingNum += dr.ItemArray(0) + vbCrLf
                Next
            End If
            txtTrackNum.Text = strTrackingNum
            dgShipment.DataBind()


        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub ShowError(ByVal msg As String)
        If msg.Trim.Length > 0 Then
            lblError.Text = msg
            lblError.Visible = True
        End If
    End Sub

    Private Sub dgShipment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgShipment.ItemDataBound
        'e.Item.Cells(0).Controls.Clear()
    End Sub
    Public Function ShowCheckBox(ByVal statusID As Integer, ByVal UpsTracking As String) As String
        Dim cbStr As String = "<input type=checkbox id='cbDgShipment' name='cbDgShipment' value='" & UpsTracking & "'>"
        Select Case statusID
            Case ShipmentStatus.Received
                cbStr = ""
        End Select
        Return cbStr
    End Function
    Public Function ShowKioskIcon(ByVal KioskFlag As Boolean, ByRef kioskBoxCnt As Int16) As String
        Dim iconStr As String = ""
        Try
            If KioskFlag Then
                kioskBoxCnt += 1
                iconStr = "<img src='images/Kiosk.png' width=17 height=17>"
            End If
        Catch ex As Exception

        End Try
        Return iconStr
    End Function
    Public Function ShowStatus(ByVal statusID As Integer, ByVal Status As String) As String
        Dim cbStr As String = Status
        Select Case statusID
            Case ShipmentStatus.Delivered
                cbStr = "<font color=red><b> &Dagger;</b></font>" & cbStr
        End Select
        Return cbStr
    End Function
    Public Function BuildTrackStr(ByVal TrackingNum As String, ByVal serviceProvider As Int16) As String
        Select Case serviceProvider
            Case 1
                '<a href="http://wwwapps.ups.com/etracking/tracking.cgi?tracknums_displayed=1&amp;TypeOfInquiryNumber=T&amp;HTMLVersion=4.0&amp;InquiryNumber1=<%#Container.DataItem("UpsTracking") %>" target="_blank"><%#Container.DataItem("UpsTracking") %></a>
                Return "<a class=XXSmall href=""http://wwwapps.ups.com/etracking/tracking.cgi?tracknums_displayed=1&TypeOfInquiryNumber=T&HTMLVersion=4.0&InquiryNumber1=" & TrackingNum & """ target=""_blank"">" & TrackingNum & "</a>"
            Case 4
                Return "<a class=XXSmall href=""http://www.fedex.com/Tracking?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers=" & TrackingNum & """ target=""_blank"">" & TrackingNum & "</a>"
            Case Else
                Return TrackingNum
        End Select
    End Function
    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim cbCheckedStr As String = ""
        Dim arrCbChecked() As String
        Dim i As Int16 = 0
        If Not Request("cbDgShipment") Is Nothing Then
            cbCheckedStr = Request("cbDgShipment")
        End If
        If cbCheckedStr.Length > 0 Then
            arrCbChecked = Split(cbCheckedStr, ",")
            For i = 0 To arrCbChecked.GetUpperBound(0)
                Try
                    _sessionUser.UpdateShipmentStatus(arrCbChecked(i), ShipmentStatus.Received)
                    'Response.Write(arrCbChecked(i) & Request("lstStatus") & "<br>")
                Catch ex As DCSSAppException
                    ShowError(ex.Message)
                    Exit Sub
                Catch ex As Exception
                    Throw ex
                End Try
            Next
            BindData()
        End If
    End Sub

    Private Sub btnSendMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendMail.Click
        Try
            Dim mailSubject, ccEmail As String
            '2/05/04 - Send copy of email to store
            ccEmail = "s" & _storeNum.ToString.PadLeft(4, "0") & "@hottopic.com"
            mailSubject = "[" & Session("SessionUser").FirstName & " " & Session("SessionUser").LastName & " @Store-" & _sessionUser.storeNum & "]-" & txtEmailSubject.Text
            _sessionUser.SendMail(lblEmailTo.Text, mailSubject, txtEmailBody.Text, ccEmail)
            ShowError("Email has been sent to shipping Co-Ordinator")
        Catch ex As DCSSAppException
            ShowError(ex.Message)
            Exit Sub
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
End Class
