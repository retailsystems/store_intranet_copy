<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Home.aspx.vb" Inherits="DCSS.Home" %>

<!DOCTYPE html>
<html>
<head>
    <style>
		UL { MARGIN-TOP: 1px; FONT-WEIGHT: bolder; FONT-SIZE: 7pt; MARGIN-BOTTOM: 1px; MARGIN-LEFT: 10px; COLOR: #ffffff; FONT-FAMILY: Arial }
		LI { FONT-WEIGHT: 500; FONT-SIZE: 7pt; MARGIN-LEFT: 8px; COLOR: #00639a; FONT-FAMILY: Arial; LIST-STYLE-TYPE: square }
	</style>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <LINK href="<%=Application("StyleSheet")%>" type="text/css" rel="stylesheet">
    <link id="Link1" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link id="Link2" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->

    <script language='javascript'>
			 function OpenPop(url,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,'cw',winProp);
		   }
		   	 function OpenPop1(url,cw,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,cw,winProp);
		   }
		 function ChangeBgColor(obj,act){
				if(act == "in"){
					obj.style.border='#4ebfde';
					
				}else{
					obj.style.background='#246178';					
				}
		   }
		function PrintMe(){
			window.print();
		}				
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
        <input type="hidden" runat="server" id="hdnRefeshFlag" value="0">
        <asp:Label ID="lblError" Visible="False" runat="server" CssClass="errStyle"></asp:Label></td>
        <h4>
            Expected Shipments</h4>
        <div class="input-group calendar_droplist">
            <asp:Panel CssClass="input-group-addon" ID="pnlSelectStore" runat="server" Visible="False">
                Store #:</asp:Panel>
            <asp:DropDownList ID="lstStore" CssClass="form-control" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </div>
        <div class="callwraper">
            <asp:Calendar ID="shpmtCalendar" CssClass="CalStyle" CellPadding="2" CellSpacing="2"
                TitleStyle-CssClass="CalTitleStyle" DayHeaderStyle-CssClass="CalDayHeaderStyle"
                DayStyle-CssClass="CalDayStyle" SelectionMode="None" DayStyle-VerticalAlign="Middle"
                runat="server" Width="100%" NextPrevFormat="ShortMonth" NextPrevStyle-CssClass="CalTitleStyle">
            </asp:Calendar>
        </div>
        <br />
        <div class="Calhints" style="border-left: solid 10px #4682b4">
            Pending Shipments</div>
        <div class="Calhints" style="border-left: solid 10px #636363">
            All Shipments received</div>
        <div class="Calhints">
            <font color="red">*</font>&nbsp;STS order(s)</div>
        <div class="Calhints" 
            style="border-left: solid 10px #4682d8; border-left-color: #669900;">
            <a href="javascript:OpenPop('Cartonsearch.aspx',650,600)"  style='text-decoration: underline; color: #000000'>Carton Search</a>
            </div>        
        <div class="clearfix">
        </div>
        <br />
        <label style="font: normal 11px arial">
            <sup>*</sup>Shipment delivery dates may vary. Please check for missing transfers
            two days prior and after the expected delivery date.</label>
    </form>
    <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
    <script>
    // removing drop downlist if you are not logged in as a DM -->Bassem
    $(document).ready(function(){
		    var listLen = $("#lstStore option").length;
		    if(listLen <= 0){
		    $("#lstStore").remove();
		    }		    
		});
    </script>
    <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
</body>
</html>
