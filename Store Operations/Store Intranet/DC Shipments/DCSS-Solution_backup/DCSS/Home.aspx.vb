Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Partial Class Home
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.DCSS.Application.Session
    Private _ds As DataSet
    Private _dv As DataView
    Private _employeeId As String
    Private _storeNum As Int16
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
     
        Try
            InitializeForm()
            If Not IsPostBack Then

                If Not Request("EmpId") Is Nothing AndAlso IsNumeric(Request("EmpId")) Then
                    _employeeId = Request("EmpId")
                Else
                    'check for DM or RM login
                    Try
                        If Request.Cookies("User").HasKeys Then
                            _employeeId = Request.Cookies("User")("UserName").ToString
                        End If
                    Catch ex As NullReferenceException

                    End Try
                End If
                '_employeeId = "00772"
                If _employeeId = "" Then
                    Session.Abandon()
                    Response.Redirect("/Home/EmpLogin.aspx?Target=DCSS")
                End If
                'need to pass httpcontext at every page load
                '_sessionUser = New Session()

                _sessionUser = New Session(HttpContext.Current, _employeeId, ConfigurationSettings.AppSettings("ProjectName"))
                'TO DO Put RM/DM get store logic here.
                If _sessionUser.SessionUser.CurrentUserRoleId = UserRole.DM Or _sessionUser.SessionUser.CurrentUserRoleId = UserRole.RM Then
                    'get Rd/DM store list
                    _ds = _sessionUser.ListStores()
                    If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then
                        pnlSelectStore.Visible = True
                        lstStore.DataSource = _ds.Tables(0)
                        lstStore.DataTextField = "StoreName"
                        lstStore.DataValueField = "StoreNum"
                        lstStore.DataBind()
                    Else
                        Throw New DCSSAppException("Your current privileges doesn't allow to access this application, Please contact helpdesk for assistance.")
                    End If
                Else
                    _sessionUser.AuthenticateStoreUser()
                End If
                BindData(Now.Month, Now.Year)
            Else
                _sessionUser = New Session(HttpContext.Current)
                'refresh calendar if pop-up window is closed
                If hdnRefeshFlag.Value = "1" Then
                    hdnRefeshFlag.Value = "0"
                    If shpmtCalendar.VisibleDate.Year > 1 Then
                        BindData(shpmtCalendar.VisibleDate.Month, shpmtCalendar.VisibleDate.Year)
                    Else
                        BindData(Now.Month, Now.Year)
                    End If
                End If
            End If
        Catch ex As StoreNotFoundException
            Response.Redirect("StoreNotFound.aspx")
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub InitializeForm()
        lblError.Text = ""
        lblError.Visible = False
        shpmtCalendar.Visible = True
    End Sub
    Private Sub ShowError(ByVal msg As String)
        shpmtCalendar.Visible = False
        lblError.Text = msg
        lblError.Visible = True
    End Sub
    Private Sub BindData(ByVal mm As Integer, ByVal yy As Integer)

        If _sessionUser.SessionUser.CurrentUserRoleId = UserRole.DM Or _sessionUser.SessionUser.CurrentUserRoleId = UserRole.RM Then
            _storeNum = lstStore.SelectedItem.Value
        Else
            _storeNum = _sessionUser.SessionUser.StoreNum
        End If
        _ds = _sessionUser.GetStoreMonthlyShipments(mm, yy, _storeNum)
    End Sub

    Private Sub shpmtCalendar_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles shpmtCalendar.DayRender
        Dim cntStr As String = ""
        Dim snsInd As String = ""
        Dim dr As DataRow
        Dim i, shipTot, receivedTot, snsTot As Int16
        If e.Day.IsOtherMonth Then
            e.Cell.Controls.Clear()
            e.Cell.Height = UI.WebControls.Unit.Pixel(0)
        Else
            If _ds.Tables(0).Rows.Count > 0 Then
                _dv = _ds.Tables(0).DefaultView
                _dv.RowFilter = "scheduledDeliveryDate='" & e.Day.Date & "'"
                If _dv.Count > 0 Then
                    'MOD 1/6/04 display received shipments cnt , toltal cnt on calendar date
                    shipTot = 0
                    receivedTot = 0
                    For i = 0 To _dv.Count - 1
                        dr = _dv.Item(i).Row
                        If dr("StatusCd") = ShipmentStatus.Received Then
                            receivedTot = dr("cnt")
                        End If
                        shipTot += dr("cnt")
                        snsTot += dr("SnSCnt")
                    Next
                    If shipTot = receivedTot Then
                        If Not IsNothing(Application("Mode")) AndAlso Application("Mode").toUpper = "TORRID" Then
                            e.Cell.CssClass = ("shpRec")
                        Else
                            e.Cell.CssClass = ("shpRec")
                        End If
                    Else
                        e.Cell.CssClass = ("shpPending")
                    End If
                    If snsTot > 0 Then
                        snsInd = "<font color='red'><sup>*</sup></font>"
                    End If
                    cntStr = "<br><a href=""javascript:OpenPop('ListShipments.aspx?dt=" & e.Day.Date & "&store=" & _storeNum & "',550,590)"">" & shipTot & "&nbsp;Shipment(s)</a>" & snsInd
                    cntStr &= "<br>" & shipTot - receivedTot & "&nbsp;pending"
                    e.Cell.Controls.Add(New LiteralControl(cntStr))
                End If
                _dv.RowFilter = ""
            End If
        End If
    End Sub

    Private Sub shpmtCalendar_VisibleMonthChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MonthChangedEventArgs) Handles shpmtCalendar.VisibleMonthChanged
        BindData(e.NewDate.Month, e.NewDate.Year)
    End Sub

    

    Private Sub lstStore_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstStore.SelectedIndexChanged
        If shpmtCalendar.VisibleDate.Year > 1 Then
            BindData(shpmtCalendar.VisibleDate.Month, shpmtCalendar.VisibleDate.Year)
        Else
            BindData(Now.Month, Now.Year)
        End If
    End Sub
End Class
