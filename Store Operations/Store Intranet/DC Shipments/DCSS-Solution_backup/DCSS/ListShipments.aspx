<%@ Page Language="vb" ValidateRequest="false" AutoEventWireup="false" CodeBehind="ListShipments.aspx.vb"
    Inherits="DCSS.ListShipments" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<%=Application("StyleSheet")%>" type="text/css" rel="stylesheet">
    <link id="Link1" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet"
        runat="server" />
    <link id="Link2" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet"
        runat="server" />
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->
    <script language="javascript">
        var plusImg = 'images/Plus.gif';
        var minusImg = 'images/Minus.gif';
        function showtree(lngSection) {
            //alert(lngSection);
            if (eval('S' + lngSection).style.display == 'none') {
                eval('S' + lngSection).style.display = 'block';
                eval('document.Form1.I' + lngSection).src = minusImg;
            } else {
                eval('S' + lngSection).style.display = 'none';
                eval('document.Form1.I' + lngSection).src = plusImg;
            }
        }
			 function OpenPop(url,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,'cw',winProp);
		   }
		   	 function OpenPop1(url,cw,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,cw,winProp);
		   }
        function ShowMailDiv() {
            document.all.divEmail.style.display = "inline";
        }
        function HideMailDiv() {
            document.all.divEmail.style.display = "none";
        }
        function ReloadOpener() {
            //alert(window.opener.document.location);
            window.opener.document.Form1.hdnRefeshFlag.value = "1";
            window.opener.document.Form1.submit();
        }
        function CopyToClipboard() {
            //document.getElementById("Div2").style.visibility = 'invisible'; 
            window.clipboardData.clearData();

            //var txtToCopy = "Hot Topic, CCC";
            var textboxVal = document.getElementById("txtTrackNum").value; //txtText id of a textbox  
            //alert(textboxVal);  

            if (window.clipboardData) {
                window.clipboardData.setData("Text", textboxVal);

            }
            else {
                alert(" Please use Internet Explorer for web browser.");
            }
        }
    </script>
</head>
<body onload="self.focus()" onunload="ReloadOpener()">
    <form id="Form1" method="post" runat="server">
    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading">
            Expected Shipments</div>
        <div class="panel-body">
            <p>
                Scheduled Delivery Date:&nbsp;
                <asp:label id="lblDeliveryDate" cssclass="contentTextStrong" runat="server"></asp:label>
            </p>
        </div>
        <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td class="cellvaluecaption" align="right">
                    <a onclick="javascript:ShowMailDiv()" href="#">
                        <img src="images/email.gif" border="0"></a> <a onclick="javascript:ShowMailDiv()"
                            href="#">Email</a> &nbsp;&nbsp;<a onclick="window.print()" href="#"><img src="images/print.gif"
                                border="0"></a> <a onclick="window.print()" href="#">Print</a> &nbsp;&nbsp;<a onclick="self.close()"
                                    href="#">Close</a>&nbsp;
                </td>
            </tr>
            <tr height="5">
                <td>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:label id="lblError" visible="False" cssclass="ErrStyle" runat="server"></asp:label>
                </td>
            </tr>
            <tr height="5">
                <td>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <div id="divEmail" style="display: none">
                        <table class="TableBorder" cellspacing="0" cellpadding="2" width="500" align="center"
                            border="0">
                            <tr height="15">
                                <td class="RedBg">
                                    <font class="cellvaluecaption">&nbsp;Send email to shipping co-ordinator</font>
                                </td>
                                <td class="RedBg" align="right">
                                    <a class="cellvaluecaption" href="javascript:HideMailDiv()">
                                        <img alt="close" src="images/delete.gif" border="0"></a>&nbsp;
                                </td>
                            </tr>
                            <tr height="5">
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" colspan="2">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="10">
                                            </td>
                                            <td class="cellvaluecaption">
                                                Email To:
                                            </td>
                                            <td class="reqStyle" align="center" width="10">
                                            </td>
                                            <td class="cellvaluewhite">
                                                <asp:label id="lblEmailTo" runat="server" enableviewstate="False"></asp:label>
                                            </td>
                                        </tr>
                                        <tr height="5">
                                            <td colspan="4">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10">
                                            </td>
                                            <td class="cellvaluecaption">
                                                Subject:
                                            </td>
                                            <td class="reqStyle" align="center" width="10">
                                                *
                                            </td>
                                            <td>
                                                <asp:textbox id="txtEmailSubject" cssclass="cellvalueleft" runat="server" enableviewstate="False"
                                                    width="400px"></asp:textbox>
                                                <asp:requiredfieldvalidator id="Requiredfieldvalidator1" cssclass="errStyle" runat="server"
                                                    enableviewstate="False" errormessage="Email subject is required" display="Dynamic"
                                                    controltovalidate="txtEmailSubject"></asp:requiredfieldvalidator>
                                            </td>
                                        </tr>
                                        <tr height="5">
                                            <td colspan="4">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10">
                                            </td>
                                            <td class="cellvaluecaption" valign="top">
                                                Comments:
                                            </td>
                                            <td class="reqStyle" valign="top" align="center" width="10">
                                                *
                                            </td>
                                            <td>
                                                <asp:textbox id="txtEmailBody" cssclass="cellvalueleft" runat="server" enableviewstate="False"
                                                    width="400px" rows="5" textmode="MultiLine"></asp:textbox>
                                                <asp:requiredfieldvalidator id="Requiredfieldvalidator2" cssclass="errStyle" runat="server"
                                                    enableviewstate="False" errormessage="Email body is required" display="Dynamic"
                                                    controltovalidate="txtEmailBody"></asp:requiredfieldvalidator>
                                            </td>
                                        </tr>
                                        <tr height="5">
                                            <td colspan="4">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <asp:button id="btnSendMail" cssclass="btnSmall" runat="server" enableviewstate="False"
                                                    text="Send"></asp:button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr height="5">
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr height="5">
                <td>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <!-- data grid-->
                    <asp:datagrid id="dgShipment" cssclass="table" runat="server" width="650px" pagerstyle-cssclass="pagerStyle"
                        pagerstyle-mode="NumericPages" allowsorting="True" autogeneratecolumns="false"
                        alternatingitemstyle-cssclass=" " itemstyle-cssclass="BC111111sm" headerstyle-cssclass="DATAGRID_HeaderSm"
                        cellpadding="1" gridlines="None" itemstyle-verticalalign="Top">
                            <Columns>
                           
                                <asp:TemplateColumn HeaderStyle-Width="30px">
                                    <ItemTemplate>
                                        <%#ShowCheckBox(Container.DataItem("StatusCD"),Container.DataItem("UpsTracking"))%>
                                        <%#ShowKioskIcon(Container.DataItem("KioskFlag"),kioskBoxCnt)%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Tracking #" HeaderStyle-Width="140px">
                                    <ItemTemplate>
                                        <!--BuildTrackStr(Container.DataItem("UpsTracking"),Container.DataItem("ServiceProvider"))-->
                                        <%#Container.DataItem("TrackingStr")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                    <div class="Calhints" style="border-left-color: rgb(102, 153, 0); border-left-width: 10px; border-left-style: solid;">
                                   
            <a style="color: rgb(0, 0, 0); text-decoration: underline;" href='javascript:OpenPop(&quot;<%# "CartonSearch.aspx?Id=" + Container.DataItem("UpsTracking")%>&quot;,850,600)'>Carton Search</a>
            </div>
                                
                                  <%-- <asp:HyperLink ID="cartonSearch" runat="server" Text="Carton Search" NavigateUrl='<%# "~/CartonSearch.aspx?Id=" + Container.DataItem("UpsTracking")%>' />--%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="ShippedDateTime" HeaderText="Shipped Date" HeaderStyle-Width="80px"
                                    DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ServiceType" HeaderText="Service Type" HeaderStyle-Width="80px">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="WhseName" HeaderText="Shipped From" HeaderStyle-Width="95px">
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderStyle-Width="90">
                                    <ItemTemplate>
                                        <%#ShowStatus(Container.DataItem("StatusCD"),Container.DataItem("Status"))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:datagrid>
                </td>
            </tr>
            <tr height="5">
                <td>
                </td>
            </tr>
            <tr>
                <td class="cellvaluewhite" align="center">
                    <table cellspacing="0" cellpadding="0" width="500">
                        <tbody>
                            <tr>
                                <td class="cellvaluewhite">
                                    Receive
                                    <input class="cellvalueleft" disabled type="checkbox" checked>
                                    items &nbsp;<asp:button id="btnUpdate" cssclass="btnSmall" runat="server" text="Receive"
                                        causesvalidation="False"></asp:button>
                                </td>
                            </tr>
                            <tr>
                                <td height="5">
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    &nbsp;&nbsp;<sup><font color="red">*</font></sup>HTConnect order(s)<br>
                                    &nbsp;&nbsp;<sup><font color="red">&Dagger;</font></sup>FedEX shows shipment delivered,
                                    Please confirm receipt
                                </td>
                            </tr>
                            <tr>
                                <td height="8">
                                </td>
                            </tr>
                            <tr>
                                <td height="8">
                                </td>
                            </tr>
                            <tr>
                                <td height="8">
                                </td>
                            </tr>
                            <tr>
                                <td height="8">
                                </td>
                            </tr>
                            <tr>
                                <td height="8">
                                </td>
                            </tr>
                            <tr>
                                <td class="cellvaluecaption" align="left">
                                    Tracking All Pending Shipments
                                </td>
                            </tr>
                            <tr>
                                <td height="5">
                                </td>
                            </tr>
                            <tr>
                                <td class="cellvaluecaption" align="left">
                                    <img src="images/moveright.gif" border="0" height="5" width="5">
                                    Click <a onclick="javascript:CopyToClipboard()" href="#"><font color="#6495ed">here</font></a>
                                    to copy Tracking Numbers.
                                </td>
                            </tr>
                            <tr>
                                <td class="cellvaluecaption" align="left">
                                    <img src="images/moveright.gif" border="0" height="5" width="5">
                                    Access the FedEx site by clicking <a href="http://www.fedex.com/us/" target="_blank">
                                        <font color="#6495ed">here</font></a>.
                                </td>
                                <tr>
                                    <td class="cellvaluecaption" align="left">
                                        <img src="images/moveright.gif" border="0" height="5" width="5">
                                        Place your cursor in the FedEx Tracking text box, right click then select paste.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="cellvaluecaption" align="left">
                                        <img src="images/moveright.gif" border="0" height="5" width="5">
                                        Click "Track".
                                    </td>
                                </tr>
                                <%IF kioskBoxCnt > 0 %>
                                <tr>
                                    <td height="5">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        &nbsp;&nbsp;&nbsp;<img height="22" src="images/Kiosk.png" width="22">
                                        - Kiosk shipments
                                    </td>
                                </tr>
                                <%End If%>
                        </tbody>
                    </table>
                    <div id="Div2" style="display: none">
                        <table>
                            <tr>
                                <td>
                                    <asp:textbox id="txtTrackNum" cssclass="cellvalueleft" runat="server" enableviewstate="False"
                                        width="400px" rows="10" textmode="MultiLine"></asp:textbox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
    <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
</body>
</html>
