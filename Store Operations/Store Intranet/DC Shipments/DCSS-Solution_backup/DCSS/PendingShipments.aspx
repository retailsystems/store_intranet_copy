<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PendingShipments.aspx.vb" Inherits="DCSS.PendingShipments"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<!-- #include file="include/header.inc" -->
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td align="middle">
						<asp:Label ID="lblError" CssClass="errStyle" Runat="server" Visible="False"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="middle" class="pagecaption">
						Pending Shipments
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
				<tr>
					<td align="middle">						
						<!-- data grid-->
						<asp:DataGrid Runat="server" ID="dgShipment" Width="500px" BorderColor="#990000" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false"  AllowSorting="True"  PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
							<Columns>
								<asp:TemplateColumn>
									<ItemTemplate>									
										<%#ShowCheckBox(Container.DataItem("StatusCD"),Container.DataItem("UpsTracking"))%>									
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Tracking #" HeaderStyle-Width="130px">
									<ItemTemplate>
										<a href="http://wwwapps.ups.com/etracking/tracking.cgi?tracknums_displayed=1&amp;TypeOfInquiryNumber=T&amp;HTMLVersion=4.0&amp;InquiryNumber1=<%#Container.DataItem("UpsTracking") %>" target="_blank"><%#Container.DataItem("UpsTracking") %></a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="ShippedDateTime" HeaderText="Shipped Date" HeaderStyle-Width="70px" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>								
								<asp:BoundColumn DataField="ServiceType" HeaderText="Service Type" HeaderStyle-Width="100px"></asp:BoundColumn>
								<asp:BoundColumn DataField="PackageType" HeaderText="Package Type" HeaderStyle-Width="100px"></asp:BoundColumn>
								<asp:BoundColumn DataField="Status" HeaderText="Status" HeaderStyle-Width="100px"></asp:BoundColumn>								
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
					<tr height=5px>
					<td></td>
				</tr>
				<tr>
					<td class=cellvaluewhite>
						Set the <input type="checkbox" class="cellvalueleft" checked disabled> items status to: <select class="cellvalueleft" id="lstStatus" name="lstStatus" >
										<option value=3 selected>Received</option>										
									</select>
									&nbsp;<asp:Button Runat=server CssClass=btnSmall ID="btnUpdate" Text="Update"></asp:Button>
					</td>
				</tr>
			</table>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
