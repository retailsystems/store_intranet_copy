	/***Vars and functions for menu***/
	
	//vars for current header and menu
	var activeHeader = null;
	var activeMenu = null;
	
	//Sets the current header and menu
	function setMenu(menuHeaderID,menuID)
	{
		var top = 0;
		var left = 0;
		var currentEle;
		
		if(document.all)
		{
			if(activeHeader != null && activeMenu != null)
			{
				if(activeMenu.style.visibility != 'hidden')
				{
					menuHide();
					showSelect();
				}
			}
		
			//alert(menuID);
			//alert(menuHeaderID);
			activeHeader = eval("document.all('" + menuHeaderID + "');");
			activeMenu = eval("document.all('" + menuID + "');");
			menuChange(activeHeader);
			currentEle = activeHeader;
				
			//Find the top and left of header and its parent elements
			while(currentEle.tagName.toLowerCase() != 'body')
			{
				top += currentEle.offsetTop;
				left += currentEle.offsetLeft;
				currentEle = currentEle.offsetParent;
			}
			
			//Add the width of the header, and width of extra image.
			top += (activeHeader.offsetHeight);
						
			//alert(left);
			activeMenu.style.left = left;
			activeMenu.style.top = top;
			
			hideSelect();
			menuShow();
			
			event.cancelBubble = true;
		}
	}
	
	//Show the current menu
	function menuShow()
	{
		if(document.all)
		{
			//activeHeader.className = 'over';
			activeMenu.style.visibility = 'visible';
		}
	}
	
	//Hide the current menu
	function menuHide()
	{
		if(document.all)
		{
			//activeHeader.className = 'norm';
			activeMenu.style.visibility = 'hidden';
		}
	}
	
	//Hide the current menu and reset vars
	//if the moved to element is not contained
	//within the menu.
	function hideMenu()
	{
		if(document.all)
		{
			if(activeHeader != null && activeMenu != null)
			{
				//Check if the "moved to" element is not 
				//contained within activeMenu.
				if(!activeMenu.contains(event.toElement)) 
				{
					activeMenu.style.visibility = 'hidden';
					//activeHeader.className = 'norm';
					menuChange(activeHeader);
					//alert("error")
					activeHeader = null;
					activeMenu = null;
					showSelect();
				}
			}
		}
	}
	
	//show dropdown when menu is hidden
	function showSelect()
	{
		var obj;
		
		for(var i = 0; i < document.all.tags("select").length; i++)
		{
			obj = document.all.tags("select")[i];
			//alert(obj.id);
			if(!obj || !obj.offsetParent)
				continue;
			obj.style.visibility = 'visible';
		}
	}
	
	//hide dropdown so menu can cover it when menu is visible
	function hideSelect()
	{
		var obj;
		var currentEle;
		var top = 0;
		var left = 0;
		var menuHeight;
		var timeout;
		
		for(var i = 0; i < document.all.tags("select").length; i++)
		{
			obj = document.all.tags("select")[i];
			currentEle = obj;
		
			while(currentEle.tagName.toLowerCase() != 'body')
			{
				top += currentEle.offsetTop;
				left += currentEle.offsetLeft;
				currentEle = currentEle.offsetParent;
			}
			if(activeMenu != null)
			{
				menuHeight = (activeMenu.offsetTop + activeMenu.offsetHeight);
				
				if(top < menuHeight)
				{			
					if((left < (activeMenu.offsetLeft + activeMenu.offsetWidth)) && (left + obj.offsetWidth > activeMenu.offsetLeft)) 
						obj.style.visibility = 'hidden';
				}
			}
			top = 0;
			left = 0;
		}
	}
/***************************************************************************************************************/
	//Hightlight the menu option
	function menuChange(srcEle)
	{				
		/*
		if(srcEle.className.toLowerCase() == 'menuregular')
		{
			srcEle.className = 'menuHighlight';
		}
		else
		{
			srcEle.className = 'menuRegular';
		}
		*/
	}
/***************************************************************************************************************/