<%@ Control Language="vb" AutoEventWireup="false" Codebehind="AdminCount.ascx.vb" Inherits="DcssAdmin.AdminCount" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table border=0 cellpadding=0 cellspacing=0 width="100%">
	<tr>
		<td>
			<td class="cellvaluecaption" width="200px" align=right>Admin &nbsp;#<asp:Label ID="lblStationID" Runat=server CssClass="cellvaluecaption" ></asp:Label>:</td>
			<td class="reqStyle" width="16px" align=middle>*</td>
			<td width="334px">
				<asp:TextBox Runat=server ID="txtAdminCount" CssClass=cellvalueleft Width=50px MaxLength=5></asp:TextBox>
				<asp:RequiredFieldValidator id=RequiredFieldValidator1  ErrorMessage="Shipment label count is required." runat="server" CssClass="ErrStyle" Display="Dynamic" ControlToValidate="txtAdminCount"></asp:RequiredFieldValidator>
				
				<asp:CompareValidator id=CompareValidator2  CssClass=errStyle ControlToValidate="txtAdminCount" Display="Dynamic" runat="server" ErrorMessage="Invalid Count" Type="Integer" Operator="GreaterThanEqual" ValueToCompare="0"></asp:CompareValidator>
			</td>
		</td>
	</tr>
	<tr height="5">
			<td colSpan="3"></td>
	</tr>
</table>
