Imports System.Web
Imports System.Web.SessionState
Imports HotTopic.DCSS.Settings
Public Class [Global]
    Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        ' Get all application settings
        Dim settings As New AppSetting()
        With settings
            .ConnectionString = ConfigurationSettings.AppSettings("ConnectionString")
            .SecurityConnectionString = ConfigurationSettings.AppSettings("SecurityConnectionString")
            .CacheDependencyFile = ConfigurationSettings.AppSettings("CacheDependency")
            .SmtpServer = ConfigurationSettings.AppSettings("SmtpServer")
            .SmtpSender = ConfigurationSettings.AppSettings("SmtpSender")
            .WebmasterEmail = ConfigurationSettings.AppSettings("WebmasterEmail")
            .ProjectName = ConfigurationSettings.AppSettings("ProjectName")
            '.ErrorLog = ConfigurationSettings.AppSettings("ErrorLog")
            .DcssMailReceivers = ConfigurationSettings.AppSettings("DcssMailReceivers")
            .CancellationEmailBody = ConfigurationSettings.AppSettings("CancellationEmailBody")
            .SuccessEmailBody = ConfigurationSettings.AppSettings("SuccessEmailBody")
            .NumDaysOld = ConfigurationSettings.AppSettings("NumDaysOld")
            .HQDCConnectionString = ConfigurationSettings.AppSettings("HQDCConnectionString")
            .TNDCConnectionString = ConfigurationSettings.AppSettings("TNDCConnectionString")
            .LocalPath = ConfigurationSettings.AppSettings("LocalPath")
            .FtpHost = ConfigurationSettings.AppSettings("FtpHost")
            .FtpFolder = ConfigurationSettings.AppSettings("FtpFolder")
            .FtpUser = ConfigurationSettings.AppSettings("FtpUser")
            .FtpPwd = ConfigurationSettings.AppSettings("FtpPwd")
            .AdminRoles = ConfigurationSettings.AppSettings("AdminRoles")

        End With
        Application("AppSetting") = settings
        Application("DebugMode") = ConfigurationSettings.AppSettings("DebugMode")
        If Not ConfigurationSettings.AppSettings("DcAdminCnt") Is Nothing Then
            Application("DcAdminCnt") = ConfigurationSettings.AppSettings("DcAdminCnt")
        Else
            Application("DcAdminCnt") = 1
        End If
        Application("WMSConnectionString") = ConfigurationSettings.AppSettings("WMSConnectionString")
        Application("LocalPath") = ConfigurationSettings.AppSettings("LocalPath")
        Application("FtpHost") = ConfigurationSettings.AppSettings("FtpHost")
        Application("FtpFolder") = ConfigurationSettings.AppSettings("FtpFolder")
        Application("FtpUser") = ConfigurationSettings.AppSettings("FtpUser")
        Application("FtpPwd") = ConfigurationSettings.AppSettings("FtpPwd")
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
        Application("Exp") = Server.GetLastError
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class
