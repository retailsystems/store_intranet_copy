Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Partial Class Login
    Inherits System.Web.UI.Page

    Private _sessionUser As HotTopic.DCSS.Application.Session

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        lblExMsg.Text = ""
        Page.RegisterStartupScript("focus", "<script language='javascript'>" & vbCrLf & _
             vbTab & "Form1." & txtEmployeeId.ClientID & ".focus();" & vbCrLf & _
             "</script>")

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            _sessionUser = New Session(HttpContext.Current, txtEmployeeId.Text, txtPwd.Text, ConfigurationSettings.AppSettings("ProjectName"))
            '01/11/05 - TNDC Enhancements - check if logged in user is ADMIN
            Dim adminRoles As String = HotTopic.DCSS.Settings.AppSetting.AdminRoles.Trim
            Session("isDcssAdmin") = False
            If adminRoles <> "" Then
                Dim roleArr() As String = adminRoles.Split("|")
                Dim i As Int16
                For i = 0 To UBound(roleArr)
                    If _sessionUser.SessionUser.CurrentUserRoleId = roleArr(i) Then
                        Session("isDcssAdmin") = True
                        Exit For
                    End If
                Next
            End If

            If _sessionUser.SessionUser.CurrentDefaultPage <> "" Then
                Response.Redirect(_sessionUser.SessionUser.CurrentDefaultPage)
            Else
                Response.Redirect("Home.aspx")
            End If

        Catch ex As DCSSAppException
            lblExMsg.Text = ex.Message
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Sub

End Class
