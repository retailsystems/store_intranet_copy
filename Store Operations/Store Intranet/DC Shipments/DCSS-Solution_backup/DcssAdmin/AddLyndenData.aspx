<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AddLyndenData.aspx.vb" Inherits="DcssAdmin.AddLyndenData"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AddLyndenData</title> 
		<!-- #include file="include/header.inc" -->
		<script language="JavaScript">

function addTableRow (e) { 
	e = window.event;
	var keyCode = e.keyCode;	
	//enter key code = 13 submit form if enter key pressed
	//alert(keyCode);
	if (keyCode == 9){
		var tableID = "tblLynden";
		var table = 
			document.all ? document.all[tableID] : document.getElementById(tableID);
		
		var row = table.insertRow(table.rows.length);
		var rowID = table.rows.length + 1;
		var input = document.createElement('INPUT');
		var htmlTxt = "";	
			var cell = row.insertCell(0);		
			htmlTxt = "<input type=text name=txtLpn" + rowID + "  class='cellvalueleft' style='width:100px'>";
			cell.innerHTML = htmlTxt;
			var cell1 = row.insertCell(1);
			htmlTxt = "<input type=text name=txtPkg" + rowID + "  onKeyDown='return addTableRow(event)' class='cellvalueleft' style='width:100px'>";
			cell1.innerHTML = htmlTxt;
			//eval("document.Form1.txtLpn" + rowID+".focus()");
			var obj = eval("document.Form1.txtLpn" + rowID);	
			obj.focus();	
			return false;	
	}else if(keyCode == 13){
		//enter key pressed
		document.Form1.submit();
		//return true;
	}else{
		return true;
	}
	
}


		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<input type="hidden" id="hdnRowCnt" name="hdnRowCnt" value="1">
			<table width="500">
				<tr>
					<td align="middle">
						<asp:Label ID="lblError" CssClass="errStyle" Runat="server" Visible="False"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="middle" class="pagecaption">
						Lynden Shipments
					</td>
				</tr>
				<tr><td height=10px></td></tr>
				<tr>
					<td align=center class="cellvaluecaption">
						Store #: <input type=text class=cellvalueleft name="txtStoreNum" style="width:100px">
						&nbsp;&nbsp;Invoice #: <input type=text class=cellvalueleft name="txtInvoiceNum" style="width:100px">
						&nbsp;&nbsp;Shipped On: <input type=text class=cellvalueleft name="txtInvoiceNum" style="width:100px">
					</td>
				</tr>
				<tr>
					<td align=center height=10px>
						
					</td>
				</tr>
				<tr>
				<td align=center bgcolor=#b22222>
					<table id="tblLynden" border="0" cellpadding=1 cellspacing=1 width=100%  >
						<tr>
							<td width="150px" class="cellvaluecaption" bgcolor=#b22222>
								Lpn #
							</td>
							<td width="150px" class="cellvaluecaption" bgcolor=#b22222>
								Service Type
							</td>
							<td width="150px" class="cellvaluecaption" bgcolor=#b22222>
								Package type
							</td>
						</tr>
						<tr>
							<td bgcolor=#000000>
								&nbsp;<input type="text" id="txtLpn1" name="txtLpn1" class="cellvalueleft" style="width:100px">
							</td>
							<td bgcolor=#000000>
								&nbsp;<select name="lstServiceType" class="cellvalueleft">
									<option value="3" selected >3 Day</option>
									<option value="2"  >2 Day</option>
									<option value="1"  >Next Day</option>
								</select>
							</td>
							<td bgcolor=#000000>
								&nbsp;<input type="text" id="txtPkg1" name="txtPkg1" class="cellvalueleft" style="width:100px" onKeyDown="return addTableRow()">
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
