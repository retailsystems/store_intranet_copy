Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services

Partial Class TravelMatrix
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.DCSS.Application.Session
    Private _ds As DataSet
    Private _storeDs As DataSet
    Protected menuStr As String
    Private _travelMatrix As StoreTravelMatrix
    Private _searchParams As StoreTravelMatrix
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            lblError.Text = ""
            lblError.Visible = False
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr
            If Not IsPostBack Then
                _sessionUser.FillWhseList(lstWhse, "", "---all---")
                _sessionUser.FillCarrierList(lstCarrier, "", "---all---")

                BindData()
            End If
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub ShowError(ByVal msg As String)
        If msg.Trim.Length > 0 Then
            lblError.Text = msg
            lblError.Visible = True
        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        dgTravelMatrix.CurrentPageIndex = 0
        dgTravelMatrix.EditItemIndex = -1
        BindData()
    End Sub
    Private Sub PrepareSearchParams()
        With _searchParams
            If IsNumeric(lstCarrier.SelectedItem.Value) AndAlso lstCarrier.SelectedItem.Value > 0 Then
                .Carrier = lstCarrier.SelectedItem.Value
            End If
            If IsNumeric(lstWhse.SelectedItem.Value) AndAlso lstWhse.SelectedItem.Value > 0 Then
                .Whse = lstWhse.SelectedItem.Value
            End If
            If txtStoreNum.Text.Trim.Length > 0 AndAlso IsNumeric(txtStoreNum.Text.Trim) AndAlso txtStoreNum.Text.Trim > 0 Then
                .StoreNum = txtStoreNum.Text.Trim
            End If
            If lstVerify.SelectedIndex > 0 Then
                .VerifiedFlag = lstVerify.SelectedItem.Value
            Else
                .VerifiedFlag = -1
            End If
        End With
    End Sub
    Private Sub BindData()
        Try
            PrepareSearchParams()
            _ds = _sessionUser.ListTravelMatrix(_searchParams)
            If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then
                dgTravelMatrix.DataSource = _ds.Tables(0)
                _ds.Tables(0).DefaultView.Sort = SortBy.Text & " " & SortDirection.Text
                If dgTravelMatrix.CurrentPageIndex > 0 AndAlso _ds.Tables(0).Rows.Count <= (dgTravelMatrix.CurrentPageIndex * dgTravelMatrix.PageSize) Then
                    dgTravelMatrix.CurrentPageIndex = dgTravelMatrix.CurrentPageIndex - 1
                End If
                dgTravelMatrix.DataBind()
                lblRecCount.Text = _ds.Tables(0).Rows.Count & " Record(s) found."
                pnlTravelMatrix.Visible = True
            Else
                pnlTravelMatrix.Visible = False

                ShowError("No records found")
            End If
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub

    Private Sub dgTravelMatrix_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgTravelMatrix.SortCommand
        If SortBy.Text = e.SortExpression Then
            Select Case SortDirection.Text
                Case "asc"
                    SortDirection.Text = "desc"
                Case "desc"
                    SortDirection.Text = "asc"
            End Select
        Else
            SortDirection.Text = "asc"
        End If
        SortBy.Text = e.SortExpression
        dgTravelMatrix.EditItemIndex = -1
        BindData()
    End Sub

    Private Sub dgTravelMatrix_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTravelMatrix.EditCommand
        Dim txtNextAm, txtNextSaver, txtDay2Am As TextBox

        dgTravelMatrix.EditItemIndex = e.Item.ItemIndex
        BindData()

        If IsNumeric(lstCarrier.SelectedItem.Value) AndAlso lstCarrier.SelectedItem.Value = 4 Then
            txtNextAm = dgTravelMatrix.Items(e.Item.ItemIndex).FindControl("txtNextAm")
            txtNextSaver = dgTravelMatrix.Items(e.Item.ItemIndex).FindControl("txtNextSaver")
            txtDay2Am = dgTravelMatrix.Items(e.Item.ItemIndex).FindControl("txtDay2Am")
            txtNextAm.Visible = False
            txtNextSaver.Visible = False
            txtDay2Am.Visible = False
        End If
    End Sub

    Private Sub dgTravelMatrix_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTravelMatrix.UpdateCommand
        Try
            Dim storeNum, travelMatrixID As Integer
            Dim txtNextTravel, txtNextAm, txtNextSaver, txtDay2, txtDay2Am, txtDay3, txtGround As TextBox
            Dim lstVerified As DropDownList
            Dim masterWhseFlag As Boolean = False
            Dim ServiceProvider As Label

            txtNextTravel = e.Item.FindControl("txtNextTravel")
            txtNextAm = e.Item.FindControl("txtNextAm")
            txtNextSaver = e.Item.FindControl("txtNextSaver")
            txtDay2 = e.Item.FindControl("txtDay2")
            txtDay2Am = e.Item.FindControl("txtDay2Am")
            txtDay3 = e.Item.FindControl("txtDay3")
            txtGround = e.Item.FindControl("txtGround")
            lstVerified = e.Item.FindControl("lstVerified")
            travelMatrixID = dgTravelMatrix.DataKeys(e.Item.ItemIndex)
            masterWhseFlag = IIf(Request("lstMasterWhse") = "1", True, False)
            ServiceProvider = e.Item.FindControl("lblCarrier")

            With _travelMatrix
                .TravelMatrixID = travelMatrixID
                .NextTravel = VelidateTravelData(txtNextTravel.Text.Trim, "Next Travel")
                .Day2Travel = VelidateTravelData(txtDay2.Text.Trim, "Day2 Travel")
                .Day3Travel = VelidateTravelData(txtDay3.Text.Trim, "Day3 Travel")
                .GroundTravel = VelidateTravelData(txtGround.Text.Trim, "Ground Travel")
                Dim carrier As Integer
                carrier = CInt(ServiceProvider.Text)
                If carrier = 4 Then
                    .NextAmTravel = -1
                    .NextSaverTravel = -1
                    .Day2AmTravel = -1
                Else
                    .NextAmTravel = VelidateTravelData(txtNextAm.Text.Trim, "NextAm Travel")
                    .NextSaverTravel = VelidateTravelData(txtNextSaver.Text.Trim, "NextSaver Travel")
                    .Day2AmTravel = VelidateTravelData(txtDay2Am.Text.Trim, "Day2Am Travel")
                End If

                .VerifiedFlag = lstVerified.SelectedItem.Value
                .MasterWhseFlag = masterWhseFlag
            End With
            _sessionUser.UpdateTravelMatrix(_travelMatrix)
            ' ShowError(storeNum.ToString.PadLeft(4, "0") & " travel matrix updated successfully.")
            dgTravelMatrix.EditItemIndex = -1
            BindData()
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            ShowError(ex.Message)
        End Try
    End Sub
    Private Function VelidateTravelData(ByVal numDays As String, ByVal fldName As String) As Int16
        If IsNumeric(numDays.Trim) AndAlso numDays.Trim > 0 Then
            Return numDays.Trim
        Else
            Throw New DCSSAppException("Invalid data entered in " & fldName.ToUpper & ", valid data range is 1-9 days.")
        End If
    End Function
    Private Sub dgTravelMatrix_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgTravelMatrix.PageIndexChanged
        dgTravelMatrix.CurrentPageIndex = e.NewPageIndex
        dgTravelMatrix.EditItemIndex = -1
        BindData()
    End Sub

    Private Sub dgTravelMatrix_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTravelMatrix.CancelCommand
        dgTravelMatrix.EditItemIndex = -1
        'ShowError("MasterWhse: " & Request("lstMasterWhse"))
        BindData()
    End Sub

    Private Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Response.Redirect("TravelMatrix.aspx")
    End Sub
End Class

