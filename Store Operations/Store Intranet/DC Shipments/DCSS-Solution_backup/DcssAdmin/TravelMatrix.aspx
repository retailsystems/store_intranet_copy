<%@ Page Language="vb" AutoEventWireup="false" Codebehind="TravelMatrix.aspx.vb" Inherits="DcssAdmin.TravelMatrix"%>
<script language=vb runat=server>
	Public Function IsSelected(byval val1 as boolean,byval val2 as boolean) as string
		Dim returnVal as string = ""
		IF val1 = val2 then
			returnVal = " Selected "
		end if
		return returnVal		
	ENd Function
</script>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<!-- #include file="include/header.inc" -->
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:label id="SortBy" Text="Store_Num" Runat="server" Visible="False"></asp:label><asp:label id="SortDirection" Text="asc" Runat="server" Visible="False"></asp:label>
			<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td class="pagecaption" align="middle">Travel Matrix
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="middle"><asp:label id="lblError" Runat="server" Visible="False" CssClass="errStyle"></asp:label></td>
				</tr>
				<tr>
					<td align="middle">
						<table style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid" cellSpacing="2" cellPadding="2" width="770" border="0">
							<tr>
								<td align="middle" colSpan="4"><asp:validationsummary id="validationSummary1" style="TEXT-ALIGN: center" Runat="server" CssClass="errStyle" Enabled="true" ShowSummary="true" ShowMessageBox="false" EnableClientScript="true" DisplayMode="BulletList"></asp:validationsummary></td>
							</tr>
							<tr>
								<td class="cellvaluecaption" align="right" width="60">Carrier:</td>								
								<td width="100">
									<asp:DropDownList id="lstCarrier" Runat="server" CssClass="cellvalueleft" ></asp:DropDownList> 
								</td>	
								<td class="cellvaluecaption" align="right" width="60">Warehouse:</td>								
								<td width="100">
									<asp:DropDownList id="lstWhse" Runat="server" CssClass="cellvalueleft" ></asp:DropDownList> 
								</td>	
								<td class="cellvaluecaption" align="right" width="60">Store #:</td>
								<td width="70"><asp:textbox id="txtStoreNum" Runat="server" CssClass="cellvalueleft" MaxLength="4" Width="60px"></asp:textbox>
									<asp:CompareValidator id="CompareValidator1" runat="server" ErrorMessage="Invalid Store #" Display="None" ControlToValidate="txtStoreNum" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator></td>
								<td class="cellvaluecaption" align="right" width="60">Status:</td>
								<td width="130"><asp:dropdownlist id="lstVerify" Runat="server" CssClass="cellvalueleft">
										<asp:ListItem Value="" Selected="True">--select all--</asp:ListItem>
										<asp:ListItem Value="0">Verification Pending</asp:ListItem>
										<asp:ListItem Value="1">Verified</asp:ListItem>
									</asp:dropdownlist></td>
								<td width="150">&nbsp;<asp:button id="btnSearch" Text="Search" Runat="server" CausesValidation="True" cssclass="btnSmall"></asp:button>
									<asp:button id="btnClear" Text="Clear" Runat="server" CausesValidation="False" cssclass="btnSmall"></asp:button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
				<asp:panel id="pnlTravelMatrix" Runat="server" Visible="False">
					<TR height="15">
						<TD class="contentTextStrong">&nbsp;&nbsp;
							<asp:Label id="lblRecCount" Runat="server" CssClass="contentTextStrong"></asp:Label></TD>
					</TR>
					<TR>
						<TD align="middle"><!-- data grid-->
							<asp:DataGrid id="dgTravelMatrix" Runat="server" CssClass="GridStyle" Width="770px" DataKeyField="TravelMatrixID" BorderColor="#990000" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false" AllowSorting="True" AllowPaging="True" PageSize="20" PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
								<Columns>
									<asp:TemplateColumn SortExpression="Store_Num" HeaderText="STORE #" HeaderStyle-Width="50px">
										<ItemTemplate>
											<%#Container.DataItem("Store_Num").toString.PadLeft(4, "0") %>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="StoreName" ReadOnly="True" SortExpression="StoreName" HeaderText="STORE NAME" HeaderStyle-Width="150px"></asp:BoundColumn>
									<asp:BoundColumn DataField="WHSE" ReadOnly="True" SortExpression="WHSE" HeaderText="WHSE" HeaderStyle-Width="40px"></asp:BoundColumn>
									<asp:TemplateColumn HeaderStyle-Width="40px" ItemStyle-HorizontalAlign="Center"  SortExpression="MasterWHSE" HeaderText="Master WHSE?">
										<ItemTemplate>
											<%#Container.DataItem("MasterWHSE")%>
										</ItemTemplate>
										<EditItemTemplate>
											<select name="lstMasterWhse" Class="cellvalueleft">
												<option value="1" <%#IsSelected(Container.DataItem("MasterWHSEFlag"),true)%> >YES</option>
												<option value="0"  <%#IsSelected(Container.DataItem("MasterWHSEFlag"),false)%>>NO</option>
											</select>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
										<HeaderTemplate>
											NEXT TRAVEL
										</HeaderTemplate>
										<ItemTemplate>
											<%#Container.DataItem("NEXT_TRAVEL")%>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox ID="txtNextTravel" Runat=server Width=20px MaxLength=1 text='<%#Container.DataItem("NEXT_TRAVEL")%>' CssClass=cellvalueleft >
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
										<HeaderTemplate>
											NEXTAM TRAVEL
										</HeaderTemplate>
										<ItemTemplate>
											<%#Container.DataItem("NEXTAM_TRAVEL")%>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox ID="txtNextAm" Runat=server Width=20px MaxLength=1 text='<%#Container.DataItem("NEXTAM_TRAVEL")%>' CssClass=cellvalueleft >
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
										<HeaderTemplate>
											NEXT SAVER
										</HeaderTemplate>
										<ItemTemplate>
											<%#Container.DataItem("nextsaver_travel")%>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox ID="txtNextSaver" Runat=server Width=20px MaxLength=1 text='<%#Container.DataItem("nextsaver_travel")%>' CssClass=cellvalueleft >
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
										<HeaderTemplate>
											DAY2 TRAVEL
										</HeaderTemplate>
										<ItemTemplate>
											<%#Container.DataItem("day2_travel")%>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox ID="txtDay2" Runat=server Width=20px MaxLength=1 text='<%#Container.DataItem("day2_travel")%>' CssClass=cellvalueleft >
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
										<HeaderTemplate>
											DAY2AM TRAVEL
										</HeaderTemplate>
										<ItemTemplate>
											<%#Container.DataItem("day2am_travel")%>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox ID="txtDay2Am" Runat=server Width=20px MaxLength=1 text='<%#Container.DataItem("day2am_travel")%>' CssClass=cellvalueleft >
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
										<HeaderTemplate>
											DAY3 TRAVEL
										</HeaderTemplate>
										<ItemTemplate>
											<%#Container.DataItem("day3_travel")%>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox ID="txtDay3" Runat=server Width=20px MaxLength=1 text='<%#Container.DataItem("day3_travel")%>' CssClass=cellvalueleft >
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
										<HeaderTemplate>
											GROUND TRAVEL
										</HeaderTemplate>
										<ItemTemplate>
											<%#Container.DataItem("ground_travel")%>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox ID="txtGround" Runat=server Width=20px MaxLength=1 text='<%#Container.DataItem("ground_travel")%>' CssClass=cellvalueleft >
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									
									
									<asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
										<HeaderTemplate>
											CARRIER
										</HeaderTemplate>
										<ItemTemplate>
											<%#Container.DataItem("ShippingCarrier")%>
										</ItemTemplate>
									</asp:TemplateColumn>
									
									<asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center" Visible="False">
										<HeaderTemplate>
											CARRIERINT
										</HeaderTemplate>
										<ItemTemplate>
											<%#Container.DataItem("ServiceProvider")%>
											</ItemTemplate>
											<EditItemTemplate>
											<asp:Label ID="lblCarrier" Runat=server Width=20px text='<%#Container.DataItem("ServiceProvider")%>' CssClass=cellvalueleft >
											</asp:Label>
										</EditItemTemplate>
										
									</asp:TemplateColumn>
									
									<asp:TemplateColumn HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Center" HeaderText="VERIFIED ?" SortExpression="Status">
										<ItemTemplate>
											<%#Container.DataItem("Status")%>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:DropDownList ID="lstVerified" Runat="server" CssClass="cellvalueleft">
												<asp:ListItem Selected="True" Value="1">YES</asp:ListItem>
												<asp:ListItem Value="0">NO</asp:ListItem>
											</asp:DropDownList>
										</EditItemTemplate>
									</asp:TemplateColumn>
									
									<asp:EditCommandColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" EditText="Edit" UpdateText="Update" CancelText="Cancel"></asp:EditCommandColumn>
								</Columns>
							</asp:DataGrid></TD>
					<TR height="5">
						<TD></TD>
					</TR>
				</asp:panel></table>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
