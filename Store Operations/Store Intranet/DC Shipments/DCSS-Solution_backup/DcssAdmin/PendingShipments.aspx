<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PendingShipments.aspx.vb" Inherits="DcssAdmin.PendingShipments"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<!-- #include file="include/header.inc" -->	
	<body>
		<script language=javascript>
			//-------------------------------------------------------------
			// Select all the checkboxes 
			//-------------------------------------------------------------
			function SelectAllCheckboxes(spanChk){    
				
				var theBox=spanChk
				var xState=spanChk.checked; 								
				elm=theBox.form.elements;					
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].id!=theBox.id)
					{
					//elm[i].click();
					if(elm[i].checked!=xState)
					elm[i].click();
					//elm[i].checked=xState;
					}
				
			}
			function UpdateCheckedItemStr(){
				var checkedStr = "";
				elm=document.Form1.elements;					
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].id.indexOf("cbDgShipment") > 0 )
					{					
					if(elm[i].checked){
						if (checkedStr.length>0) checkedStr += ",";
						checkedStr += elm[i].value ;						
					}				
					}	
				document.Form1.hdnCheckedItemStr.value = checkedStr;					
				if (!document.Form1.hdnCheckedItemStr.value.length > 0) {
					//alert(document.Form1.hdnCheckedItemStr.value);
					return false;
				}else{
					return true;
				}
			}
		</script>
		<form id="Form1" method="post" runat="server">
		<input type=hidden name="hdnCheckedItemStr" id="hdnCheckedItemStr" value="" runat=server>
		
		<asp:Label ID="SortDirection" Visible="False" Runat="server"></asp:Label>
			<table width="500px">
				<tr>
					<td align="middle">
						<asp:Label ID="lblError" CssClass="errStyle" Runat="server" Visible="False"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="middle" class="pagecaption">
						Pending Shipments
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
				<tr>
					<td class="contentText" align=center>
						(List of  packages shipped but not yet flagged as received after <%=ConfigurationSettings.AppSettings("NumDaysOld")%> days.)
					</td>
				</tr>
				<tr>
					<td align=center class="cellvaluecaption">
						Warehouse:<asp:DropDownList id="lstWhse" Runat="server" CssClass="cellvalueleft" AutoPostBack=True ></asp:DropDownList> 
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
				<asp:Panel ID="pnlDgShipment" Runat=server Visible=False>
				<tr height="5">
					<td class="contentTextStrong">					
					<asp:Label Runat=server ID="lblRecCount" CssClass="contentTextStrong"></asp:Label>&nbsp;Shipment(s) found.
					&nbsp;<asp:LinkButton Runat=server ID="exportToExcel" text="Export to Excel"></asp:LinkButton>
					&nbsp;&nbsp;Sort By :: <asp:Label ID="SortBy" CssClass="cellvaluecaption" Visible="True" Runat="server"></asp:Label>
					&nbsp;<asp:LinkButton Runat=server ID="btnResetSort" CssClass=contentTextStrong Visible=False text="reset"></asp:LinkButton>
					</td>
				</tr>
				<tr>
					<td align="middle">						
						<!-- data grid-->
						<asp:DataGrid Runat="server" ID="dgShipment" DataKeyField="UpsTracking" Width="750px" BorderColor="#990000" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false"  AllowSorting="True" AllowPaging=True PageSize=20  PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
							<Columns>
								<asp:TemplateColumn HeaderStyle-Width=20px>
								<HeaderTemplate>
									<asp:CheckBox id="chkAll" 
										onclick="javascript:SelectAllCheckboxes(this);" runat="server" 
										AutoPostBack="false" ToolTip="Select/Deselect All" />
								</HeaderTemplate>
								<ItemTemplate> 
									<input type=checkbox id="cbDgShipment"  name="cbDgShipment"
										runat="server" value='<%#container.dataitem("UpsTracking")%>'
										 />
								</ItemTemplate> 
								</asp:TemplateColumn> 								
								<asp:TemplateColumn HeaderText="Tracking #"  HeaderStyle-Width="130px">
									<ItemTemplate>
										<a href="<%#Container.DataItem("TrackingURL") %>" target="_blank"><%#Container.DataItem("UpsTracking") %></a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="StoreNum" HeaderText="Store #"  HeaderStyle-Width="70px">
									<ItemTemplate><%#Container.DataItem("StoreNum").toString.PadLeft(4, "0")%></ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  SortExpression="ShippedDateTime" HeaderText="Shipped Date"  HeaderStyle-Width="70px">
									<ItemTemplate><%#Container.DataItem("ShippedDateTime").ToShortDateString%></ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="ServiceType" HeaderText="Service Type"  HeaderStyle-Width="100px">
									<ItemTemplate><%#Container.DataItem("ServiceType")%></ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="WHSE" HeaderText="WHSE"  HeaderStyle-Width="40px">
									<ItemTemplate><%#Container.DataItem("WHSE")%></ItemTemplate>
								</asp:TemplateColumn>								
					            <asp:TemplateColumn SortExpression="TrackingStatusDesc" HeaderText="Tracking Status Desc"  HeaderStyle-Width="100px">
									<ItemTemplate><%#Container.DataItem("Tracking_StatusDesc")%></ItemTemplate>
								</asp:TemplateColumn>	
								
								<asp:TemplateColumn HeaderText="Comments" HeaderStyle-Width=180px>
									<ItemTemplate>
										<%#Container.DataItem("Comments")%>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox Runat=server EnableViewState=False ID="txtComment" TextMode=MultiLine Rows=5 Width=99% MaxLength=2000 Text='<%#container.dataitem("Comments")%>' CssClass=cellvalueleft></asp:TextBox>
									</EditItemTemplate>									
								</asp:TemplateColumn>
								<asp:EditCommandColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" EditText="Edit" UpdateText="Update" CancelText="Cancel"></asp:EditCommandColumn>
								
							</Columns>
						</asp:DataGrid>
					</td>
				<tr height=5px>
					<td></td>
				</tr>
				<tr>
					<td class=cellvaluewhite >
						
										Receive <input type="checkbox" class="cellvalueleft" checked disabled> items 
									&nbsp;<asp:Button Runat=server CssClass=btnSmall ID="btnUpdate" Text="Receive" CausesValidation=False></asp:Button>
									
								
					</td>
				</tr>
			</asp:Panel>
			</table>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
