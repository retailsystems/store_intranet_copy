<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Login.aspx.vb" Inherits="DcssAdmin.Login"%>
<HTML>
	<HEAD>
		<title>DC to Store Shipments</title>
		<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico">
		<LINK href="include/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body text="white" vLink="white" aLink="white" link="white" bgColor="black" face="arial">
		<table id="Table1" cellSpacing="0" cellPadding="0" width="750" border="0">
			<tr>
				<td>
					<%if InStr(Request.Url.ToString, "Torrid", CompareMethod.Text) > 0 then%>
					<A href="http://www.torrid.com"><img src='Images/TorridBlk.jpg' border='0' alt='Torrid'></A>
					<%else%>
					<A HREF='http://www.hottopic.com'><img src='Images/hdr_main2.gif' border='0' alt='hottopic'></A>
					<%end if%>
				</td>
				<td></td>
				<td width='1200' align='right' valign='top'>
			<tr>
				<td colspan='2' align='left'><font face='Arial'><font size='+1' color='white'></font></font></td>
				<td colspan='2' align='right'><font face='Arial'><font size='+1' color='red'>DC to Store 
							Shipments v1.0 </font></font>
				</td>
			</tr>
			<tr height="3">
				<td colspan='2' height="3"><hr color='#990000'>
				</td>
				<td colspan='2' height="3"><hr color='#990000'>
				</td>
			</tr>
			<tr>
				<td colspan="4">
				</td>
			</tr>
		</table>
		<!--end tree-->
		<table border='0' cellspacing='0' cellpadding='0' width='750' ID="Table3">
			<tr height="250">
				<td colspan="4" valign="top" align="middle">
					<!--end header-->
					<form id="Form1" method="post" runat="server">
						<br>
						<table cellSpacing="0" cellPadding="0" width="400" border="0" align="center">
							<tr>
								<td class="pageCaption" align="middle" colSpan="3">Login</td>
							</tr>
							<tr height="15">
								<td colSpan="3"></td>
							</tr>
							<tr>
								<td colSpan="3" align="middle">
									<asp:Label id="lblExMsg" runat="server" CssClass="ErrStyle"></asp:Label></td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">Employee ID:</td>
								<td class="reqStyle" width="10">*</td>
								<td class="cellvalueleft">
									<asp:TextBox ID="txtEmployeeId" CssClass="cellvalueleft" Runat="server"></asp:TextBox>
									<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" CssClass="errStyle" ErrorMessage="Employee ID is required." Display="Dynamic" ControlToValidate="txtEmployeeId"></asp:RequiredFieldValidator>
									<asp:CompareValidator id="CompareValidator1" runat="server" CssClass="errStyle" ControlToValidate="txtEmployeeId" Display="Dynamic" ErrorMessage="Invalid EmployeeID." Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
								</td>
							</tr>
							<tr height="15">
								<td colSpan="3"></td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">Password:</td>
								<td class="reqStyle" width="10">*</td>
								<td class="cellvalueleft">
									<asp:TextBox ID="txtPwd" Runat="server" CssClass="cellvalueleft" TextMode="Password"></asp:TextBox>
									<asp:RequiredFieldValidator id="Requiredfieldvalidator1" runat="server" CssClass="errStyle" ErrorMessage="Password is required." Display="Dynamic" ControlToValidate="txtPwd"></asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
					<!--footer-->
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<hr color="#990000">
				</td>
			</tr>
			<tr>
				<td colspan="4" align="right"><asp:button id="btnSubmit" CssClass="B1" Runat="server" Text="Submit"></asp:button>&nbsp;&nbsp;<input class="B1" id="Button1" onclick="document.location='Login.aspx';" type="button" value="Home" name="Home"></td>
			</tr>
		</table>
		</FORM>
	</body>
</HTML>
