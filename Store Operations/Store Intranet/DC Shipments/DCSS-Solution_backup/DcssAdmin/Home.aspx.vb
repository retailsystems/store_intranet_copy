Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Imports System.Data.Common
Partial Class Home
    Inherits System.Web.UI.Page

    Private _sessionUser As HotTopic.DCSS.Application.Session
    Private _ds As DataSet
    Private _beginDate, _endDate As String
    Private _dv As DataRowView()
    'Protected WithEvents lblShipDtFrom As System.Web.UI.WebControls.Label
    Public adminCtrl As AdminCount
    Private _dt1, _dt2 As DateTime
    Private _culture As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("")
    Private _DcAdminCnt As Integer

    Protected menuStr As String
    Private _manifestData As ManifestData

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
       
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
       
        Try
            lblError.Text = ""
            lblError.Visible = False
            lblError.CssClass = "ErrStyle"
            'need to pass httpcontext at every page load
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr

        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        If Not IsPostBack Then
            Try
                Dim dtFrom, dtTo As String
                _sessionUser.FillWhseList(lstWhse)
                ShowManifestList()
                ''ShowNewManifestPanel(True)
                ShowResubmitPanel()
                btnHideRetail.Attributes.Add("onclick", "return ConfirmHideRetail()")
            Catch ex As DCSSAppException
                lblError.Text = ex.Message
                lblError.Visible = True
            Catch ex As Exception
                Throw ex
            End Try
        End If
    End Sub
    Private Sub HideResubmitPanel()
        pnlResubmit.Visible = False
    End Sub
    Private Sub ShowResubmitPanel()
        'show re-submit panel to admins
        pnlResubmit.Visible = False
        If IsNothing(Session("BTNDATAFIX")) Then
            Session("BTNDATAFIX") = False
            _dv = _sessionUser.ListActionsByModuleId()
            Dim drv As DataRowView
            For Each drv In _dv
                If Not IsDBNull(drv("ControlName")) AndAlso drv("ControlName").ToString.ToUpper = "BTNDATAFIX" Then
                    Session("BTNDATAFIX") = True
                    Exit For
                End If
            Next
        End If
        If Session("BTNDATAFIX") Then
            pnlResubmit.Visible = True
        Else
            pnlResubmit.Visible = False
        End If
    End Sub
    Private Sub ShowManifestList()
        Try
            If IsNumeric(lstWhse.SelectedItem.Value) AndAlso lstWhse.SelectedItem.Value > 0 Then
                _ds = _sessionUser.ListManifest(lstWhse.SelectedItem.Value)
                dgManifest.DataSource = _ds
                If dgManifest.CurrentPageIndex > 0 AndAlso _ds.Tables(0).Rows.Count <= (dgManifest.CurrentPageIndex * dgManifest.PageSize) Then
                    dgManifest.CurrentPageIndex = dgManifest.CurrentPageIndex - 1
                End If
                dgManifest.DataBind()
                dgManifest.Visible = True
            Else
                dgManifest.Visible = False
            End If
        Catch ex As DCSSAppException
            lblError.Text = ex.Message
            lblError.Visible = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgManifest_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgManifest.EditCommand
        dgManifest.EditItemIndex = e.Item.ItemIndex
        ShowManifestList()
        HideResubmitPanel()
        'ShowNewManifestPanel(False)
    End Sub
   
    Private Sub dgManifest_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgManifest.UpdateCommand
        Try
            Dim statCd As Int16 = 0
            Dim manifNbr As String
            Dim txtCloseDate As TextBox
            Dim hdnStatCd, hdnServiceProvider, hdnWhse, hdnCloseDate, hdnManifNbr As HtmlInputHidden
            Dim cbManifProcess As CheckBox
            txtCloseDate = e.Item.FindControl("txtCloseDate")
            hdnStatCd = e.Item.FindControl("hdnStatCd")
            hdnServiceProvider = e.Item.FindControl("hdnServiceProvider")
            hdnWhse = e.Item.FindControl("hdnWhse")
            hdnCloseDate = e.Item.FindControl("hdnCloseDate")
            hdnManifNbr = e.Item.FindControl("hdnManifNbr")
            If IsItemChecked(hdnManifNbr.Value) Then
                statCd = 1
            Else
                statCd = hdnStatCd.Value
            End If
            'cbManifProcess = e.Item.FindControl("cbManifProcess")
            'If Not cbManifProcess Is Nothing AndAlso cbManifProcess.Checked Then
            '    statCd = 1
            'Else
            '    statCd = hdnStatCd.Value
            'End If
            manifNbr = dgManifest.DataKeys(e.Item.ItemIndex)
            If Not IsDate(txtCloseDate.Text) Then
                lblError.Text = "Invalid close date"
                lblError.Visible = True
            Else
                With _manifestData
                    .ManifestNum = manifNbr
                    .ShipDate = txtCloseDate.Text
                    .StatusCD = statCd
                    .WearhouseNum = hdnWhse.Value
                    .ServiceProvider = hdnServiceProvider.Value
                    .CloseDate = IIf(IsDate(hdnCloseDate.Value), hdnCloseDate.Value, txtCloseDate.Text)
                End With
                FillManifestFtpInfo(_manifestData)
                _sessionUser.UpdateManifestInfoByID(_manifestData)
                dgManifest.EditItemIndex = -1
                ShowManifestList()
                ShowResubmitPanel()
            End If
        Catch ex As DCSSAppException
            lblError.Text = ex.Message
            lblError.Visible = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub FillManifestFtpInfo(ByRef manifestData As ManifestData)
        With manifestData
            .LocalPath = Application("LocalPath")
            .FtpHost = Application("FtpHost")
            .FtpFolder = Application("FtpFolder")
            .FtpUser = Application("FtpUser")
            .FtpPwd = Application("FtpPwd")
        End With
    End Sub
    Private Sub dgManifest_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgManifest.CancelCommand
        dgManifest.EditItemIndex = -1
        ShowManifestList()
        ShowResubmitPanel()
    End Sub

    Private Sub dgManifest_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgManifest.PageIndexChanged
        dgManifest.EditItemIndex = -1
        dgManifest.CurrentPageIndex = e.NewPageIndex
        ShowManifestList()
        'ShowNewManifestPanel(True)
    End Sub

    Public Function ShowCB(ByVal statCd As Int16)
        Dim cbStr As String
        Select Case statCd
            Case 0
                cbStr = "TRUE"
            Case Else
                cbStr = "FALSE"
        End Select
        Return cbStr
    End Function
    Private Sub ShowMessage(ByVal msg As String)
        lblError.Text = msg
        lblError.Visible = True
    End Sub

    Private Sub btnResubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResubmit.Click
        Try
            _sessionUser.CreateShipmentFileByManifest(txtManifNum.Text, lstWhse.SelectedItem.Value)
            ShowMessage("Manifest/Load is submitted for re-process")
        Catch ex As DCSSAppException
            ShowMessage(ex.Message)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnReady_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReady.Click
        Try
            Dim li As DataGridItem
            Dim hdnStatCd, hdnServiceProvider, hdnWhse, hdnCloseDate, hdnShipDate, hdnManifNbr As HtmlInputHidden
            For Each li In dgManifest.Items
                If li.ItemType <> ListItemType.Header And li.ItemType <> ListItemType.Footer Then
                    hdnManifNbr = li.FindControl("hdnManifNbr")
                    hdnStatCd = li.FindControl("hdnStatCd")
                    hdnServiceProvider = li.FindControl("hdnServiceProvider")
                    hdnWhse = li.FindControl("hdnWhse")
                    hdnCloseDate = li.FindControl("hdnCloseDate")
                    hdnShipDate = li.FindControl("hdnShipDate")
                    hdnShipDate.Value = IIf(IsDate(hdnShipDate.Value), hdnShipDate.Value, Now.ToShortDateString)
                    If IsItemChecked(hdnManifNbr.Value) Then
                        With _manifestData
                            .ManifestNum = hdnManifNbr.Value
                            .ShipDate = hdnShipDate.Value
                            .StatusCD = 1
                            .WearhouseNum = hdnWhse.Value
                            .ServiceProvider = hdnServiceProvider.Value
                            .CloseDate = IIf(IsDate(hdnCloseDate.Value), hdnCloseDate.Value, hdnShipDate.Value)
                        End With
                        FillManifestFtpInfo(_manifestData)
                        _sessionUser.UpdateManifestInfoByID(_manifestData)
                    End If
                End If
            Next
        Catch ex As DCSSAppException
            ShowMessage(ex.Message)
        Catch ex As Exception
            Throw ex
        Finally
            dgManifest.EditItemIndex = -1
            ShowManifestList()
            ShowResubmitPanel()
            hdnCheckedItemStr.Value = ""
        End Try
    End Sub
    Private Function IsItemChecked(ByVal manifNbr As String) As Boolean
        Dim ckFlag As Boolean = False
        Dim cbCheckedStr As String = ""
        Dim arrCbChecked() As String
        Dim i As Int16 = 0
        cbCheckedStr = hdnCheckedItemStr.Value
        If cbCheckedStr.Length > 0 Then
            arrCbChecked = Split(cbCheckedStr, ",")
            For i = 0 To arrCbChecked.GetUpperBound(0)
                If manifNbr = arrCbChecked(i) Then
                    ckFlag = True
                    Exit For
                End If
            Next
        End If
        Return ckFlag
    End Function

    Private Sub lstWhse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstWhse.SelectedIndexChanged
        hdnCheckedItemStr.Value = ""
        txtManifNum.Text = ""
        dgManifest.EditItemIndex = -1
        dgManifest.CurrentPageIndex = 0
        ShowManifestList()
    End Sub

    Private Sub btnHideRetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHideRetail.Click
        Try
            Dim li As DataGridItem
            Dim hdnStatCd, hdnServiceProvider, hdnWhse, hdnCloseDate, hdnShipDate, hdnManifNbr As HtmlInputHidden
            For Each li In dgManifest.Items
                If li.ItemType <> ListItemType.Header And li.ItemType <> ListItemType.Footer Then
                    hdnManifNbr = li.FindControl("hdnManifNbr")
                    hdnStatCd = li.FindControl("hdnStatCd")
                    hdnServiceProvider = li.FindControl("hdnServiceProvider")
                    hdnWhse = li.FindControl("hdnWhse")
                    hdnCloseDate = li.FindControl("hdnCloseDate")
                    hdnShipDate = li.FindControl("hdnShipDate")
                    hdnShipDate.Value = IIf(IsDate(hdnShipDate.Value), hdnShipDate.Value, Now.ToShortDateString)
                    If IsItemChecked(hdnManifNbr.Value) AndAlso hdnServiceProvider.Value <> "1" Then
                        With _manifestData
                            .ManifestNum = hdnManifNbr.Value
                            .ShipDate = hdnShipDate.Value
                            .StatusCD = 0
                            .WearhouseNum = hdnWhse.Value
                            .ServiceProvider = 3 '1-UPS, 2-LYN, 3-RETA
                            .CloseDate = IIf(IsDate(hdnCloseDate.Value), hdnCloseDate.Value, hdnShipDate.Value)
                        End With
                        FillManifestFtpInfo(_manifestData)
                        _sessionUser.UpdateManifestInfoByID(_manifestData)
                    End If
                End If
            Next
        Catch ex As DCSSAppException
            ShowMessage(ex.Message)
        Catch ex As Exception
            Throw ex
        Finally
            dgManifest.EditItemIndex = -1
            ShowManifestList()
            ShowResubmitPanel()
            hdnCheckedItemStr.Value = ""
        End Try
    End Sub
End Class
