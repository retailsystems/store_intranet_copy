Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Partial Class PendingShipments
    Inherits System.Web.UI.Page
    Protected WithEvents lblDeliveryDate As System.Web.UI.WebControls.Label
    Private _sessionUser As HotTopic.DCSS.Application.Session

    Private _ds As DataSet
    Protected menuStr As String
    Protected _whse As Int16 = 0
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            lblError.Text = ""
            lblError.Visible = False
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr
            If Not IsPostBack Then
                _sessionUser.FillWhseList(lstWhse, "", "---all---")
                btnUpdate.Attributes.Add("onclick", "return UpdateCheckedItemStr()")
                BindData()
            End If
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
       
    End Sub
    Private Sub BindData(Optional ByVal resetFlag As Boolean = False)
        Try

            If Not IsPostBack OrElse resetFlag = True Then
                'SortBy.Text = "ShippedDateTime"
                'SortDirection.Text = "asc"
                'SortBy.Text = "ShippedDateTime  ASC"

                hdnCheckedItemStr.Value = ""
                dgShipment.EditItemIndex = -1
                dgShipment.CurrentPageIndex = 0
            End If
            If IsNumeric(lstWhse.SelectedItem.Value) AndAlso lstWhse.SelectedItem.Value > 0 Then
                _whse = lstWhse.SelectedItem.Value
            End If
            _ds = _sessionUser.ListPendingShipments(_whse)
            If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then
                dgShipment.DataSource = _ds.Tables(0)
                '_ds.Tables(0).DefaultView.Sort = SortBy.Text '& " " & SortDirection.Text
                _ds.Tables(0).DefaultView.Sort = IIf(SortBy.Text.Trim.Length > 0, SortBy.Text, "ShippedDateTime  ASC")

                If dgShipment.CurrentPageIndex > 0 AndAlso _ds.Tables(0).Rows.Count <= (dgShipment.CurrentPageIndex * dgShipment.PageSize) Then
                    dgShipment.CurrentPageIndex = dgShipment.CurrentPageIndex - 1
                End If
                'dgShipment.DataSource = _ds
                dgShipment.DataBind()
                lblRecCount.Text = _ds.Tables(0).Rows.Count
                pnlDgShipment.Visible = True
            Else
                pnlDgShipment.Visible = False
                ShowError("No records found")
            End If
        Catch ex As DCSSAppException
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ShowError(ByVal msg As String)
        If msg.Trim.Length > 0 Then
            lblError.Text = msg
            lblError.Visible = True
        End If
    End Sub

    Private Sub dgShipment_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgShipment.PageIndexChanged
        dgShipment.CurrentPageIndex = e.NewPageIndex
        dgShipment.EditItemIndex = -1
        BindData()

    End Sub

    Private Sub dgShipment_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgShipment.SortCommand
        'If SortBy.Text = e.SortExpression Then
        '    Select Case SortDirection.Text
        '        Case "asc"
        '            SortDirection.Text = "desc"
        '        Case "desc"
        '            SortDirection.Text = "asc"
        '    End Select
        'Else
        '    SortDirection.Text = "asc"
        'End If
        'SortBy.Text = e.SortExpression
        SortBy.Text = UtilityManager.BuildSortExpression(SortBy.Text, e.SortExpression)
        dgShipment.EditItemIndex = -1
        btnResetSort.Visible = True
        BindData()
    End Sub

    Private Sub dgShipment_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShipment.EditCommand
        dgShipment.EditItemIndex = e.Item.ItemIndex
        BindData()
        Dim tb As TextBox = dgShipment.Items(e.Item.ItemIndex).Cells(6).FindControl("txtComment")
        Page.RegisterStartupScript("focus", "<script language='javascript'>" & vbCrLf & _
            vbTab & "Form1." & tb.ClientID & ".focus();" & vbCrLf & _
            "</script>")
    End Sub

    Private Sub dgShipment_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShipment.CancelCommand
        dgShipment.EditItemIndex = -1
        BindData()
    End Sub

    Private Sub dgShipment_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShipment.UpdateCommand
        Try
            Dim statusCd As Int16 = 0
            Dim comments, upsTrack As String
            Dim cb As HtmlControls.HtmlInputCheckBox = e.Item.Cells(0).FindControl("cbDgShipment")
            Dim tb As TextBox = e.Item.Cells(6).FindControl("txtComment")
            upsTrack = cb.Value
            If cb.Checked Then
                statusCd = 3
            End If
            comments = tb.Text
            _sessionUser.UpdateShipmentInfo(upsTrack, statusCd, comments)
            dgShipment.EditItemIndex = -1
            BindData()
        Catch ex As DCSSAppException
            ShowError(ex.Message)
            Exit Sub
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim cbCheckedStr As String = ""
        Dim arrCbChecked() As String
        Dim i As Int16 = 0
        If Not Request("hdnCheckedItemStr") Is Nothing Then
            cbCheckedStr = Request("hdnCheckedItemStr")
        End If
        If cbCheckedStr.Length > 0 Then
            arrCbChecked = Split(cbCheckedStr, ",")
            For i = 0 To arrCbChecked.GetUpperBound(0)
                Try
                    _sessionUser.UpdateShipmentInfo(arrCbChecked(i), ShipmentStatus.Received, "")

                Catch ex As DCSSAppException
                    ShowError(ex.Message)
                    Exit Sub
                Catch ex As Exception
                    Throw ex
                End Try
            Next
            dgShipment.EditItemIndex = -1
            BindData()
        End If
    End Sub

    Private Sub exportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles exportToExcel.Click

        If IsNumeric(lstWhse.SelectedItem.Value) AndAlso lstWhse.SelectedItem.Value > 0 Then
            _whse = lstWhse.SelectedItem.Value
        End If
        Server.Transfer("ExportToExcel.aspx?qry=pending&whse=" & _whse)
    End Sub

    Private Sub lstWhse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstWhse.SelectedIndexChanged

        BindData(True)
    End Sub

    Private Sub btnResetSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetSort.Click
        SortBy.Text = ""
        dgShipment.EditItemIndex = -1
        btnResetSort.Visible = False
        BindData()
    End Sub
End Class
