<%@ Page Language="vb" AutoEventWireup="false" EnableViewStateMac="False" Codebehind="ExportToExcel.aspx.vb" Inherits="DcssAdmin.ExportToExcel"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>DC Shipments</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<style>
		.cellValueLeft
		{
			FONT-SIZE: 8pt;
			COLOR: #000000;
			FONT-FAMILY: Arial;
			TEXT-ALIGN: left
		}

		.cellValueCaption
		{
			font-size: 8pt;
			font-family: Arial;	
			color:#000000;
			font-weight:bold;
		}
		.pageCaption
		{
			font-size: 12pt;
			font-family: Arial;
			text-align: center;
			color:#000000;
			font-weight:bold;
		}
		</style>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<%SELECT CASE qry
			CASE "search" 
		%>
			<table border="1" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="8" align="center" class="pageCaption" bgcolor="#d3d3d3">Search 
						Shipments</td>
				</tr>
				<%IF searchFilter <> "" THEN%>
				<tr>
					<td colspan="8" align="center" class="cellValueCaption" bgcolor="#e0ffff"><%=searchFilter%></td>
				</tr>
				<%END IF%>
				<tr class="cellValueCaption">
					<th width="130">
						Tracking #</th>
					<th width="60">
						Store #</th>
					<th width="80">
						Shipped Date</th>
					<th width="100">
						Service Type</th>
					<th width="100">
						WHSE</th>
					<th width="100">
						Status</th>
					<th width="150">
						Received By</th>
					<th width="250">
						Comments</th>
				</tr>
				<%For Each dr in _ds.Tables(0).Rows	%>
				<tr class="cellValueLeft">
					<td width="130"><%=dr("UpsTracking")%></td>
					<td width="60"><%=dr("StoreNum").toString.PadLeft(4, "0") %></td>
					<td width="80"><%=dr("ShippedDateTime").ToShortDateString%></td>
					<td width="100"><%=dr("ServiceType")%></td>
					<td width="100"><%=dr("WHSE")%></td>
					<td width="100"><%=dr("Status")%></td>
					<td width="150"><%=dr("ReceiverName")%></td>
					<td width="250"><%=dr("Comments")%></td>
				</tr>
				<%Next%>
			</table>
			<%
			CASE "late"
		%>
			<table border="1" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="8" align="center" class="pageCaption" bgcolor="#d3d3d3">Late Shipments</td>
				</tr>
				<%IF searchFilter <> "" THEN%>
				<tr>
					<td colspan="8" align="center" class="cellValueCaption" bgcolor="#e0ffff"><%=searchFilter%></td>
				</tr>
				<%END IF%>
				<tr class="cellValueCaption">
					<th width="130">
						Tracking #</th>
					<th width="60">
						Store #</th>
					<th width="100">
						Shipped Date</th>
					<th width="100">
						Scheduled Delivery Date</th>
					<th width="100">
						Received On</th>
					<th width="100">
						Service Type</th>
					<th width="100">
						WHSE</th>
					<th width="250">
						Comments</th>
				</tr>
				<%For Each dr in _ds.Tables(0).Rows	%>
				<tr class="cellValueLeft">
					<td width="130"><%=dr("UpsTracking")%></td>
					<td width="60"><%=dr("StoreNum").toString.PadLeft(4, "0") %></td>
					<td width="100"><%=dr("ShippedDateTime").ToShortDateString%></td>
					<td width="100"><%=dr("OriginalScheduledDeliveryDate").ToShortDateString%></td>
					<td width="100"><%=dr("DeliveredDate").ToShortDateString%></td>
					<td width="100"><%=dr("ServiceType")%></td>
					<td width="100"><%=dr("WHSE")%></td>
					<td width="250"><%=dr("Comments")%></td>
				</tr>
				<%Next%>
			</table>
			<%
			CASE "pending"
		%>
			<table border="1" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="6" align="center" class="pageCaption" bgcolor="#d3d3d3">Pending 
						Shipments</td>
				</tr>
				<%IF searchFilter <> "" THEN%>
				<tr>
					<td colspan="6" align="center" class="cellValueCaption" bgcolor="#e0ffff"><%=searchFilter%></td>
				</tr>
				<%END IF%>
				<tr class="cellValueCaption">
					<th width="130">
						Tracking #</th>
					<th width="60">
						Store #</th>
					<th width="100">
						Shipped Date</th>
					<th width="100">
						Service Type</th>
					<th width="100">
						WHSE</th>
                    <th width="100">
						TrackingStatusDesc</th>
					<th width="250">
						Comments</th>
				</tr>
				<%For Each dr in _ds.Tables(0).Rows	%>
				<tr class="cellValueLeft">
					<td width="130"><%=dr("UpsTracking")%></td>
					<td width="60"><%=dr("StoreNum").toString.PadLeft(4, "0") %></td>
					<td width="100"><%=dr("ShippedDateTime").ToShortDateString%></td>
					<td width="100"><%=dr("ServiceType")%></td>
					<td width="100"><%=dr("WHSE")%></td>
                    <td width="100"><%=dr("Tracking_StatusDesc")%></td>
					<td width="250"><%=dr("Comments")%></td>
				</tr>
				<%Next%>
			</table>
			<%			
			CASE "travelEx"
			%>
			<table border="1" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="8" align="center" class="pageCaption" bgcolor="#d3d3d3">UPS Traveltime Exceptions</td>
				</tr>
				<%IF searchFilter <> "" THEN%>
				<tr>
					<td colspan="8" align="center" class="cellValueCaption" bgcolor="#e0ffff"><%=searchFilter%></td>
				</tr>
				<%END IF%>
				<tr class="cellValueCaption">
					<th width="120">
						Tracking #</th>
					<th width="40">
						Store #</th>
					<th width="70">
						Shipped Date</th>
					<th width="120">
						Original Scheduled DeliveryDt</th>
					<th width="120">
						UPS Scheduled DeliveryDt</th>
					<th width="80">
						Service Type</th>
					<th width="40">
						WHSE</th>
					<th width="80">
						Status</th>
				</tr>
				<%For Each dr in _ds.Tables(0).Rows	%>
				<tr class="cellValueLeft">
					<td width="120"><%=dr("UpsTracking")%></td>
					<td width="40"><%=dr("StoreNum").toString.PadLeft(4, "0") %></td>
					<td width="70"><%=dr("ShippedDateTime").ToShortDateString%></td>
					<td width="120"><%=dr("OriginalScheduledDeliveryDate").ToShortDateString%></td>
					<td width="120"><%=dr("ScheduledDeliveryDate").ToShortDateString%></td>
					<td width="80"><%=dr("ServiceType")%></td>
					<td width="40"><%=dr("WHSE")%></td>
					<td width="80"><%=dr("Status")%></td>
				</tr>
				<%Next%>
			</table>
			<%
		END SELECT
		%>
		</form>
	</body>
</html>
