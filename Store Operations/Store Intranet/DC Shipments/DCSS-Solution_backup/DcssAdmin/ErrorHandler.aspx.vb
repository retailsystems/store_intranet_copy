Partial Class ErrorHandler
    Inherits System.Web.UI.Page
    Protected sessionExpired As Boolean = False
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            If Session.Count <= 0 Then
                If Not ConfigurationSettings.AppSettings("SessionCheckOn") Is Nothing AndAlso ConfigurationSettings.AppSettings("SessionCheckOn").ToUpper = "TRUE" Then
                    sessionExpired = True
                    If Not ConfigurationSettings.AppSettings("SessionExpiryRedirectUrl") Is Nothing Then
                        SessionExpiryRedirectUrl.Value = ConfigurationSettings.AppSettings("SessionExpiryRedirectUrl")
                    End If
                End If
            End If
            If Not IsPostBack And Not sessionExpired Then
                If Not ConfigurationSettings.AppSettings("DebugMode") Is Nothing AndAlso ConfigurationSettings.AppSettings("DebugMode") = "yes" Then
                    txtDetails.Text = Application("Exp").ToString
                    txtDetails.Visible = True
                Else
                    'Log exceptions
                    txtDetails.Visible = False
                    Dim IsValidSettings As Boolean = True
                    Dim exceptionSettings As HotTopic.ExceptionManagement.ConfigSettings
                    With exceptionSettings
                        If Not ConfigurationSettings.AppSettings("ProjectName") Is Nothing AndAlso ConfigurationSettings.AppSettings("ProjectName").Length > 0 Then
                            .ProjectName = ConfigurationSettings.AppSettings("ProjectName")
                        Else
                            .ProjectName = ""
                        End If
                        If Not ConfigurationSettings.AppSettings("ErrorLog") Is Nothing AndAlso ConfigurationSettings.AppSettings("ErrorLog").Length > 0 Then
                            .LogFileName = ConfigurationSettings.AppSettings("ErrorLog")
                        Else
                            Return
                        End If
                        If Not ConfigurationSettings.AppSettings("SendExceptionMail") Is Nothing AndAlso ConfigurationSettings.AppSettings("SendExceptionMail").ToUpper = "TRUE" Then
                            .SendMail = True
                        Else
                            .SendMail = False
                        End If
                        If Not ConfigurationSettings.AppSettings("SmtpServer") Is Nothing AndAlso ConfigurationSettings.AppSettings("SmtpServer").Length > 0 Then
                            .SmtpServer = ConfigurationSettings.AppSettings("SmtpServer")
                        Else
                            .SendMail = False
                        End If
                        If Not ConfigurationSettings.AppSettings("SmtpSender") Is Nothing AndAlso ConfigurationSettings.AppSettings("SmtpSender").Length > 0 Then
                            .SmtpSender = ConfigurationSettings.AppSettings("SmtpSender")
                        Else
                            .SendMail = False
                        End If
                        If Not ConfigurationSettings.AppSettings("WebmasterEmail") Is Nothing AndAlso ConfigurationSettings.AppSettings("WebmasterEmail").Length > 0 Then
                            .WebMasterEmail = ConfigurationSettings.AppSettings("WebmasterEmail")
                        Else
                            .SendMail = False
                        End If

                    End With

                    HotTopic.ExceptionManagement.ExceptionPublisher.Publish(Application("Exp"), exceptionSettings)

                End If
            End If
        Catch ex As Exception
            txtDetails.Text = ex.ToString
            txtDetails.Visible = True
        End Try

    End Sub

End Class
