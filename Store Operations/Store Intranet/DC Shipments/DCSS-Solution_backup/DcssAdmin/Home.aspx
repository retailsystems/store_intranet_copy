<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Home.aspx.vb" Inherits="DcssAdmin.Home"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<script runat="server" language="vb">
	Public Function ShowCheckBox(ByVal statCd As Int16, ByVal manifNbr As String)
		Dim cbStr As String
        Select Case statCd
            Case 0
                'cbStr = "TRUE"
                cbStr = "<input type=checkbox id=""cbDgManifest""  name=""cbDgManifest""" & _
                        " value='" & manifNbr & "' onclick=""UpdateCheckedItemStr()"" />"
            Case Else
                cbStr = ""
        End Select
        Return cbStr
	End Function
	</script>
	<!-- #include file="include/header.inc" -->
	<script language="javascript">
			function ConfirmHideRetail(){
				//return confirm("Are you sure to hide selected load(s) from view?");
				//return ValidateCheckedItemStr;
				UpdateCheckedItemStr();							
				if (!document.Form1.hdnCheckedItemStr.value.length > 0) {	
					alert('Please select load(s) to hide!'); 				
					return false;
				}else{
					if (confirm('Are you sure to hide selected load(s) from view?')){
						return true;
					}else return false;
				}
			}
			//-------------------------------------------------------------
			// Select all the checkboxes 
			//-------------------------------------------------------------
			function SelectAllCheckboxes(spanChk){    
				
				var theBox=spanChk
				var xState=spanChk.checked; 								
				elm=theBox.form.elements;					
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].id!=theBox.id && elm[i].id.indexOf("cbDgManifest") > -1)
					{
					//elm[i].click();
					if(elm[i].checked!=xState)
					//elm[i].click();
					elm[i].checked=xState;
					}
				
				UpdateCheckedItemStr();
			}
			//-------------------------------------------------------------
			// Update hidden variable with checked items 
			//-------------------------------------------------------------
			function UpdateCheckedItemStr(){
				var checkedStr = "";
				
				var ckAllState;
				elm=document.Form1.elements;					
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].id.indexOf("cbDgManifest") > -1 )
					{										
					if(elm[i].checked){						
						if (checkedStr.length>0) checkedStr += ",";
						checkedStr += elm[i].value ;						
					}				
					}	
				document.Form1.hdnCheckedItemStr.value = checkedStr;	
				 SyncChkAll();
			}
			//---------------------------------------------------
			// sync the chkAll chekbox status		
			//---------------------------------------------------
			function SyncChkAll(){	
				var cbTotalCnt = 0;
				var cbCheckedCnt = 0;
				elm=document.Form1.elements;
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].id.indexOf("cbDgManifest") > -1 )
				{
					cbTotalCnt += 1;					
					if(elm[i].checked){
						cbCheckedCnt += 1;											
					}				
				}				
				if(cbTotalCnt == cbCheckedCnt && cbTotalCnt > 0){
					ckAllState = true;
				}else{
					ckAllState = false;
				}
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].id.indexOf("chkAll") > -1 )
					{
						elm[i].checked = ckAllState;
					}
			}
			//-------------------------------------------------------------
			// Validate hidden variable 
			//-------------------------------------------------------------
			function ValidateCheckedItemStr(){	
				UpdateCheckedItemStr();							
				if (!document.Form1.hdnCheckedItemStr.value.length > 0) {	
					alert('Please select shipment(s) to delete'); 				
					return false;
				}else{
					if (confirm('Are you sure to drop selected shipments?')){
						return true;
					}else return false;
				}
			}
	
			//-------------------------------------------------------------
			// Initialize shipment check boxes  
			//-------------------------------------------------------------
			function InitializecbDgManifest(){
				var checkedStr = document.Form1.hdnCheckedItemStr.value;				
				if (checkedStr.length > 0){
				var separator = ',';
				var stringArray = checkedStr.split(separator);				
				elm=document.Form1.elements;					
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].id.indexOf("cbDgManifest") > -1 )
					{	
						for (var j=0; j < stringArray.length; j++){
							if (elm[i].value == stringArray[j]){
								elm[i].checked = true;
								break;
							}
						}								
					}	
				}
				SyncChkAll()
			}
			window.onload = InitializecbDgManifest;
			
	</script>
	<body>
		<form id="Form1" method="post" runat="server">
			<input type="hidden" name="hdnCheckedItemStr" id="hdnCheckedItemStr" runat="server">
			<table cellSpacing="0" cellPadding="0" width="550" align="center" border="0">
				<TR height="10">
					<TD></TD>
				</TR>
				<tr>
					<td align="center"><asp:label id="lblError" Visible="False" CssClass="errStyle" Runat="server"></asp:label></td>
				</tr>
				<tr>
					<td align="center" class="cellvaluecaption">
						Warehouse:
						<asp:DropDownList id="lstWhse" Runat="server" CssClass="cellvalueleft" AutoPostBack="True"></asp:DropDownList>
						<asp:RequiredFieldValidator Runat="server" ID="reqValidator1" CssClass="reqStyle" EnableClientScript="true"
							ControlToValidate="lstWhse" Display="Dynamic" ErrorMessage="Please select a Warehouse." InitialValue=""></asp:RequiredFieldValidator>
					</td>
				</tr>
				<TR height="15">
					<TD></TD>
				</TR>
				<asp:panel id="pnlResubmit" Visible="True" Runat="server">
  <TR>
						<TD align="center">
							<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
								cellSpacing="0" cellPadding="2" width="325" align="center" border="0">
								<TR height="15">
									<TD class="cellvaluecaption" bgColor="#990000" colSpan="2">&nbsp;Re-Run 
										Manifest/Load export process
									</TD>
								</TR>
								<TR height="5">
									<TD colSpan="2"></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="center" colSpan="2">
										<TABLE borderColor="#990000" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR height="10">
												<TD colSpan="3"></TD>
											</TR>
											<TR>
												<TD class="cellvaluecaption" align="right">Manifest / Load #:</TD>
												<TD class="reqStyle">*</TD>
												<TD>
													<asp:TextBox id="txtManifNum" Runat="server" CssClass="cellvalueleft" Width="105px"></asp:TextBox>
													<asp:Button id="btnResubmit" Runat="server" CssClass="btnSmall" Text="Submit"></asp:Button>
													<asp:RequiredFieldValidator id="Requiredfieldvalidator1" Runat="server" CssClass="ErrStyle" ErrorMessage="Manifest / Load # is required"
														Display="Dynamic" ControlToValidate="txtManifNum"></asp:RequiredFieldValidator></TD>
											</TR>
											<TR height="10">
												<TD colSpan="3"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR height="5">
									<TD colSpan="2"></TD>
								</TR>
							</TABLE>
						</TD>
  <TR height="5">
						<TD colSpan="2"></TD>
					</TR></TR>
				</asp:panel>
				<tr>
					<td align="center">
						<table width="700">
							<tr>
								<td align="center">
									<asp:DataGrid ID="dgManifest" DataKeyField="MANIF_NBR" Runat="server" Width="700px" BorderColor="#990000"
										CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle"
										AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false" AllowPaging="True"
										PageSize="10" AllowSorting="False" PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
										<Columns>
											<asp:BoundColumn DataField="STAT_CD" ReadOnly="True" Visible="False" HeaderText="Manifest/Load #"
												HeaderStyle-Width="150px"></asp:BoundColumn>
											<asp:TemplateColumn HeaderStyle-Width="20px" ItemStyle-Width="20px">
												<HeaderTemplate>
												</HeaderTemplate>
												<ItemTemplate>
													<input type=hidden runat=server id=hdnStatCd name=hdnStatCd value='<%#Container.DataItem("STAT_CD")%>'>
													<input type=hidden runat=server id="hdnServiceProvider" name=hdnServiceProvider value='<%#Container.DataItem("SERVICE_PROVIDER")%>'>
													<input type=hidden runat=server id="hdnWhse" name=hdnWhse value='<%#Container.DataItem("WHSE")%>'>
													<input type=hidden runat=server id="hdnCloseDate" name=hdnCloseDate value='<%#Container.DataItem("Close_Date")%>'>
													<input type=hidden runat=server id="hdnManifNbr" name=hdnManifNbr value='<%#Container.DataItem("MANIF_NBR")%>'>
													<input type=hidden runat=server id="hdnShipDate" name=hdnShipDate value='<%#FormatDateTime(Container.DataItem("Ship_Date"),2)%>'>
													<%#ShowCheckBox(Container.DataItem("STAT_CD"),Container.dataitem("MANIF_NBR"))%>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="MANIF_NBR" ReadOnly="True" HeaderText="Manifest/Load #" HeaderStyle-Width="150px"></asp:BoundColumn>
											<asp:BoundColumn DataField="ServiceProviderName" ReadOnly="True" HeaderText="Ship Via" HeaderStyle-Width="100px"></asp:BoundColumn>
											<asp:BoundColumn DataField="Close_Date" ReadOnly="True" HeaderText="Close Date" HeaderStyle-Width="100px"
												DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
											<asp:TemplateColumn HeaderStyle-Width="120px">
												<HeaderTemplate>
													Ship Date
												</HeaderTemplate>
												<ItemTemplate>
													<%#FormatDateTime(Container.DataItem("Ship_Date"),2)%>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox ID="txtCloseDate" Runat=server Width=100px text='<%#FormatDateTime(Container.DataItem("Ship_Date"),2)%>' CssClass=cellvalueleft >
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="Status_Desc" ReadOnly="True" HeaderText="Status" HeaderStyle-Width="50px"></asp:BoundColumn>
											<asp:BoundColumn DataField="CARTON_PROCESS_CNT" ReadOnly="True" HeaderText="# Exported" HeaderStyle-Width="60px"></asp:BoundColumn>
											<asp:EditCommandColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" EditText="Edit" UpdateText="Update"
												CancelText="Cancel"></asp:EditCommandColumn>
										</Columns>
									</asp:DataGrid></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
				<tr>
					<td class="cellvaluewhite">
						Mark <input type="checkbox" class="cellvalueleft" checked disabled> items as 
						&nbsp;<asp:Button Runat="server" CssClass="btnSmall" ID="btnReady" Text="Ready to Process" CausesValidation="False"></asp:Button>
						&nbsp;<asp:Button Runat="server" CssClass="btnSmall" ID="btnHideRetail" Text="Hide retail route load"
							CausesValidation="False"></asp:Button>
					</td>
				</tr>
			</table>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
