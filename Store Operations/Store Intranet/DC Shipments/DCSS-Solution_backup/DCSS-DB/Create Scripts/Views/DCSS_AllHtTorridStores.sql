IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'DCSS_AllHtTorridStores')
	BEGIN
		PRINT 'Dropping View DCSS_AllHtTorridStores'
		DROP  View DCSS_AllHtTorridStores
	END
GO

/******************************************************************************
**		File: 
**		Name: DCSS_AllHtTorridStores
**		Desc: 
**
**		This template can be customized:
**              
**
**		Auth: Sri Bajjuri
**		Date: 8/4/2004
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    01/14/05		Sri Bajjuri			Column "InternetStore" added to view
**	  04/27/05		Sri Bajjuri			Column "Service_DC" added to view
*******************************************************************************/

PRINT 'Creating View DCSS_AllHtTorridStores'
GO
CREATE View DCSS_AllHtTorridStores
as
SELECT StoreNum, StoreName,District,Region, Deleted,StagingStore,InternetStore,Service_DC  FROM STORE
UNION
SELECT StoreNum, StoreName,District,Region, Deleted,StagingStore,InternetStore,Service_DC FROM Torrid.DBO.STORE


GO


GRANT SELECT ON DCSS_AllHtTorridStores TO AppUser

GO
