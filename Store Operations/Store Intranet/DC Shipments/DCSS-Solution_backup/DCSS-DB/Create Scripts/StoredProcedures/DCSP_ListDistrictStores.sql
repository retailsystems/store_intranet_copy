IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ListDistrictStores')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ListDistrictStores'
		DROP  Procedure  DCSP_ListDistrictStores
	END

GO

PRINT 'Creating Procedure DCSP_ListDistrictStores'
GO
CREATE Procedure DCSP_ListDistrictStores
	/* Param List */
	@DistrictID varchar(5)
AS

/******************************************************************************
**		File: DCSP_ListDistrictStores.sql
**		Name: DCSP_ListDistrictStores
**		Desc: Returns list of stores belong to a district
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 2/20/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT StoreNum, StoreName FROM Store
	WHERE (Deleted is Null OR Deleted =0) AND StagingStore <> 1 AND District = @DistrictID
	ORDER BY StoreNum

GO

GRANT EXEC ON DCSP_ListDistrictStores TO AppUser

GO
