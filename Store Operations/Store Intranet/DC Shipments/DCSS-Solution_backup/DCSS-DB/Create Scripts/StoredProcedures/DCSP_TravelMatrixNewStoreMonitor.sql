IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_TravelMatrixNewStoreMonitor')
	BEGIN
		PRINT 'Dropping Procedure DCSP_TravelMatrixNewStoreMonitor'
		DROP  Procedure  DCSP_TravelMatrixNewStoreMonitor
	END

GO

PRINT 'Creating Procedure DCSP_TravelMatrixNewStoreMonitor'
GO
CREATE Procedure DCSP_TravelMatrixNewStoreMonitor
	/* Param List */
AS

/******************************************************************************
**		File: 
**		Name: DCSP_TravelMatrixNewStoreMonitor
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by: Scheduled SQL JOB  
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 08/04/2004
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    4/27/2005		Sri Bajjuri			Adding TNDC travel matrix & MasterWhseFlag code 
*******************************************************************************/
DECLARE @sqlStr varchar(1000)
DECLARE @mailRecipients varchar(500), @msg varchar(500)
--SET @mailRecipients = 'sbajjuri@hottopic.com;GWashington@hottopic.com;BSickles@hottopic.com'
SET @mailRecipients = 'sbajjuri@hottopic.com'
IF EXISTS(
	SELECT s.StoreNum FROM DCSS_AllHtTorridStores s 
		LEFT OUTER JOIN dcss_upstravelTime upst 
		ON s.StoreNum = upst.Store_Num AND upst.WHSE = 999
		WHERE upst.Store_Num IS NULL 
		AND (s.Deleted is Null OR s.Deleted =0) 
		AND s.StagingStore <> 1 AND s.Region <> 90
	UNION
		SELECT s.StoreNum FROM DCSS_AllHtTorridStores s 
		LEFT OUTER JOIN dcss_upstravelTime upst 
		ON s.StoreNum = upst.Store_Num AND upst.WHSE = 997
		WHERE upst.Store_Num IS NULL 
		AND (s.Deleted is Null OR s.Deleted =0) 
		AND s.StagingStore <> 1 AND s.Region <> 90
	)
	
BEGIN
	-- Send notfication E-mail
	SET @sqlStr = 'SELECT s.StoreNum,s.StoreName FROM HotTopic2.DBO.DCSS_AllHtTorridStores s ' +
					' LEFT OUTER JOIN HotTopic2.DBO.dcss_upstravelTime upst ' +
					' ON s.StoreNum = upst.Store_Num  AND upst.WHSE = 999' +
					' WHERE upst.Store_Num IS NULL ' +
					' AND (s.Deleted is Null OR s.Deleted =0) ' +
					' AND s.StagingStore <> 1 AND s.Region <> 90 ' +
					' UNION ' +
					' SELECT s.StoreNum,s.StoreName FROM HotTopic2.DBO.DCSS_AllHtTorridStores s ' +
					' LEFT OUTER JOIN HotTopic2.DBO.dcss_upstravelTime upst ' +
					' ON s.StoreNum = upst.Store_Num  AND upst.WHSE = 997' +
					' WHERE upst.Store_Num IS NULL ' +
					' AND (s.Deleted is Null OR s.Deleted =0) ' +
					' AND s.StagingStore <> 1 AND s.Region <> 90 '
					
	EXEC master.dbo.xp_sendmail @recipients = @mailRecipients, 
	@query = @sqlStr,
	@subject = 'DCSS new store travel time update alert ',
	@message = 'Attched document has list of new stores; Please verify/update the UPS Travel matrix for them',
	@attach_results = 'TRUE', @width = 500
	IF @@error <> 0
		BEGIN			
			RAISERROR('Failed to deliver  exception email',16,3)
			RETURN -1
		END

	--Load new store travel time with defaults for CADC

	INSERT INTO dcss_upstravelTime
		(store_num,WHSE, next_travel, nextam_travel, nextsaver_travel,ground_travel, day2_travel,day2am_travel, day3_travel)
	SELECT s.StoreNum,999,1,1,1,3,2,2,3 FROM DCSS_AllHtTorridStores s 
		LEFT OUTER JOIN dcss_upstravelTime upst 
		ON s.StoreNum = upst.Store_Num AND upst.WHSE = 999
		WHERE upst.Store_Num IS NULL 
		AND (s.Deleted is Null OR s.Deleted =0) 
		AND s.StagingStore <> 1 AND s.Region <> 90
		
	--Load new store travel time with defaults for TNDC
	INSERT INTO dcss_upstravelTime
		(store_num,WHSE, next_travel, nextam_travel, nextsaver_travel,ground_travel, day2_travel,day2am_travel, day3_travel)
	SELECT s.StoreNum,997,1,1,1,3,2,2,3 FROM DCSS_AllHtTorridStores s 
		LEFT OUTER JOIN dcss_upstravelTime upst 
		ON s.StoreNum = upst.Store_Num AND upst.WHSE = 997
		WHERE upst.Store_Num IS NULL 
		AND (s.Deleted is Null OR s.Deleted =0) 
		AND s.StagingStore <> 1 AND s.Region <> 90
	/**************************************************************************/	
	/*******************Begin Update Masterwhse flag***************************/
	/**************************************************************************/
	UPDATE dcss_upstravelTime SET MasterWhseFlag = 0 
		FROM  dcss_upstravelTime upst JOIN DCSS_AllHtTorridStores s
		ON s.StoreNum = upst.Store_Num AND upst.WHSE = 999
		WHERE s.Service_DC = 'TNDC' AND upst.MasterWhseFlag = 1
	UPDATE dcss_upstravelTime SET MasterWhseFlag = 0 
		FROM  dcss_upstravelTime upst JOIN DCSS_AllHtTorridStores s
		ON s.StoreNum = upst.Store_Num AND upst.WHSE = 997
		WHERE s.Service_DC = 'CADC' AND upst.MasterWhseFlag = 1
	UPDATE dcss_upstravelTime SET MasterWhseFlag = 1 
		FROM  dcss_upstravelTime upst JOIN DCSS_AllHtTorridStores s
		ON s.StoreNum = upst.Store_Num AND upst.WHSE = 999
		WHERE s.Service_DC = 'CADC' AND upst.MasterWhseFlag = 0
	UPDATE dcss_upstravelTime SET MasterWhseFlag = 1 
		FROM  dcss_upstravelTime upst JOIN DCSS_AllHtTorridStores s
		ON s.StoreNum = upst.Store_Num AND upst.WHSE = 997
		WHERE s.Service_DC = 'TNDC' AND upst.MasterWhseFlag = 0
	/**************************************************************************/	
	/*******************  End Update Masterwhse flag***************************/
	/**************************************************************************/	
		
		
		
	IF @@error <> 0
		BEGIN			
			RAISERROR('Failed to insert new store record in dcss_upstravelTime',16,3)
			RETURN -1
		END
END
GO


GRANT EXEC ON DCSP_TravelMatrixNewStoreMonitor TO AppUser

GO
