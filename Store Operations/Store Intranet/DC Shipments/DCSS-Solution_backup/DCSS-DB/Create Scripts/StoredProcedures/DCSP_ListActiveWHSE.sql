IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ListActiveWHSE')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ListActiveWHSE'
		DROP  Procedure  DCSP_ListActiveWHSE
	END

GO

PRINT 'Creating Procedure DCSP_ListActiveWHSE'
GO
CREATE Procedure DCSP_ListActiveWHSE
	/* Param List */
AS

/******************************************************************************
**		File: DCSP_ListActiveWHSE.sql
**		Name: DCSP_ListActiveWHSE
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 01/17/05
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
SELECT * FROM DCSS_WhseConfig WHERE isActive = 1


GO

GRANT EXEC ON DCSP_ListActiveWHSE TO AppUser

GO
