IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DSCP_AddShipment')
	BEGIN
		PRINT 'Dropping Procedure DSCP_AddShipment'
		DROP  Procedure  DSCP_AddShipment
	END

GO

PRINT 'Creating Procedure DSCP_AddShipment'
GO
CREATE Procedure DSCP_AddShipment
/* Param List */
	@StoreNum int
	,@ShippedLpn varchar(20)
	,@UpsTracking varchar(30)
	,@PackageType varchar(10)	
	,@PackageWeight varchar(5)
	,@ServiceType varchar(21)
	,@ShippedDateTime datetime	
	,@WHSE smallint = 999
	,@ManifestNbr varchar(20) = NULL
	,@EndOfFile bit = NULL
AS

/******************************************************************************
**		File: DSCP_AddShipment.sql
**		Name: DSCP_AddShipment
**		Desc: Add/Edit Shipment info
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:  WMS(Biztalk)
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 11/15/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    5/11/04		Sri Bajjuri			Added new column "ServiceProvider" shipment table
**											to handle lynden shipments
**    6/15/04		Sri Bajjuri			Chop the time part in @ScheduledDeliveryDate
**	  8/05/04		Sri bajjuri			Get the ship date from DCSS_ManifestCloseDate table	
**	  10/26/04		Sri Bajjuri			Associcate KIOSK shipments with their home store
										Kiosk store #s are between 3000 to 4000
**	  12/21/04		Sri Bajjuri			In put parameter @ShippedDateTime will contain 
										actual Ship date (fetched from HT_DCSS_Manifest_Close_Date table)										
**	  12/21/04		Sri Bajjuri			new input parameter @EndOfFile added
**	  07/06/05		Sri Bajjuri			Inserting shipment count email_queue, get the count by manifest & whse#.
*******************************************************************************/
DECLARE  @ServiceTypeStr varchar(21), @ScheduledDeliveryDate DATETIME, @ServiceProvider int
DECLARE @mailRecipients varchar(500), @msg varchar(500), @cartonCnt int
SET @mailRecipients = 'sbajjuri@hottopic.com;'
DECLARE @KioskFlag bit 
SELECT @ServiceTypeStr = ServiceType, @ServiceProvider = ServiceProvider FROM DCSS_ShipVia where ShipVia = @ServiceType

SET @KioskFlag = NULL
IF @StoreNum BETWEEN 3000 AND 4000
	BEGIN -- #KIOSK
		SET @KioskFlag = 1
		SET @StoreNum = SUBSTRING(convert(varchar,@StoreNum),2,3)
	END -- #KIOSK
/*	
IF @ManifestNbr IS NOT NULL
BEGIN
	IF EXISTS(SELECT Manifestnumber FROM DCSS_ManifestCloseDate WHERE Manifestnumber = @ManifestNbr AND WHSE = @WHSE)
	
		SELECT @ShippedDateTime = CloseDate FROM DCSS_ManifestCloseDate WHERE Manifestnumber = @ManifestNbr AND WHSE = @WHSE
		
END
*/
SELECT  @ScheduledDeliveryDate = DBO.DCSFN_WMSGetDeliveryDate(@StoreNum,@ShippedDateTime,@ServiceType,@WHSE)

SELECT @ScheduledDeliveryDate = convert(varchar,DATEPART(mm, @ScheduledDeliveryDate)) + '/' + convert(varchar,DATEPART(dd, @ScheduledDeliveryDate)) + '/' + convert(varchar,DATEPART(yyyy, @ScheduledDeliveryDate))

IF @ServiceProvider IS NULL
	SET @ServiceProvider = 1 --Default it to UPS
	
IF @StoreNum < 5000 --Hot Topic 
	BEGIN -- #1
		
		IF EXISTS(SELECT UpsTracking FROM DCSS_UpsShipments WHERE UpsTracking = @UpsTracking)
			UPDATE DCSS_UpsShipments
				SET
				StoreNum = @StoreNum 
				,ShippedLpn = @ShippedLpn				
				,PackageType = @PackageType 				
				,PackageWeight = @PackageWeight
				,ServiceType = @ServiceTypeStr 
				,ShippedDateTime = @ShippedDateTime 
				,OriginalShippedDateTime = @ShippedDateTime 
				,ScheduledDeliveryDate = @ScheduledDeliveryDate
				,OriginalScheduledDeliveryDate = @ScheduledDeliveryDate
				,ServiceProvider = @ServiceProvider
				,UpdateBy = 'system'
				,UpdateDate = getDate()
				,WHSE = @WHSE
				,ManifestNbr = @ManifestNbr
				,KioskFlag = @KioskFlag
				WHERE  UpsTracking = @UpsTracking
		ELSE
			INSERT INTO DCSS_UpsShipments
				(
				StoreNum
				,UpsTracking
				,ShippedLpn				
				,PackageType				
				,PackageWeight
				,ServiceType
				,ShippedDateTime	
				,OriginalShippedDateTime

				,ScheduledDeliveryDate
				,OriginalScheduledDeliveryDate
				,ServiceProvider
				,UpdateBy
				,UpdateDate 	
				,WHSE
				,ManifestNbr
				,KioskFlag		
				)
				VALUES
				(
				@StoreNum
				,@UpsTracking
				,@ShippedLpn				
				,@PackageType				
				,@PackageWeight
				,@ServiceTypeStr
				,@ShippedDateTime	
				,@ShippedDateTime 
				,@ScheduledDeliveryDate
				,@ScheduledDeliveryDate
				,@ServiceProvider
				,'system'
				,getDate()
				,@WHSE
				,@ManifestNbr
				,@KioskFlag
				)
	END -- #1
ELSE
	-- Torrid
	BEGIN -- #2
		IF EXISTS(SELECT UpsTracking FROM Torrid.DBO.DCSS_UpsShipments WHERE UpsTracking = @UpsTracking)
			UPDATE Torrid.DBO.DCSS_UpsShipments
				SET
				StoreNum = @StoreNum 
				,ShippedLpn = @ShippedLpn				
				,PackageType = @PackageType 				
				,PackageWeight = @PackageWeight
				,ServiceType = @ServiceTypeStr 
				,ShippedDateTime = @ShippedDateTime 
				,OriginalShippedDateTime = @ShippedDateTime 
				,ScheduledDeliveryDate = @ScheduledDeliveryDate
				,OriginalScheduledDeliveryDate = @ScheduledDeliveryDate
				,ServiceProvider = @ServiceProvider
				,UpdateBy = 'system'
				,UpdateDate = getDate()
				,WHSE = @WHSE
				,ManifestNbr = @ManifestNbr
				,KioskFlag = @KioskFlag
				WHERE  UpsTracking = @UpsTracking
		ELSE
			INSERT INTO Torrid.DBO.DCSS_UpsShipments
				(
				StoreNum
				,UpsTracking
				,ShippedLpn				
				,PackageType				
				,PackageWeight
				,ServiceType
				,ShippedDateTime
				,OriginalShippedDateTime	
				,ScheduledDeliveryDate
				,OriginalScheduledDeliveryDate
				,ServiceProvider
				,UpdateBy
				,UpdateDate 		
				,WHSE
				,ManifestNbr	
				,KioskFlag 
				)
				VALUES
				(
				@StoreNum
				,@UpsTracking
				,@ShippedLpn				
				,@PackageType				
				,@PackageWeight
				,@ServiceTypeStr
				,@ShippedDateTime	
				,@ShippedDateTime	
				,@ScheduledDeliveryDate
				,@ScheduledDeliveryDate
				,@ServiceProvider
				,'system'
				,getDate()
				,@WHSE
				,@ManifestNbr
				,@KioskFlag
				)
	END -- #2
	
	IF @@error <> 0
			BEGIN			
				RAISERROR('Failed to add/edit shipment data',16,3)
			END
	IF (IsNULL(@EndOfFile,0) = 1 AND @ManifestNbr IS NOT NULL) -- SEND COMPLETION EMAIL TO DC STAFF 
		BEGIN
			set @cartonCnt = 0			
			select @cartonCnt = count(*) from DCSS_AllHtTorridShipments 
				WHERE manifestnbr = @ManifestNbr and WHSE = @WHSE
			INSERT INTO DCSS_Email_Queue (ManifestNbr,NumRecords)
				VALUES(@ManifestNbr ,@cartonCnt)
			/*
			set @msg = convert(varchar,@cartonCnt) + ' Carton(s) data exported to DCSS for Manifest/Load # ' + @ManifestNbr 
			EXEC master.dbo.xp_sendmail @recipients = @mailRecipients			
			,@subject = 'DCSS Alert'
			,@message = @msg
			,@set_user = 'ntdomain\dev_intranet'
			IF @@ERROR <> 0
				BEGIN
				INSERT INTO DCSS_ERROR_LOG(MsgSource,MsgDesc)
					VALUES('DSCP_AddShipment','Failed to deliver email :' + @msg)
					RETURN(0)
				END
			*/
		END	
	
GO

GRANT EXEC ON DSCP_AddShipment TO AppUser

GO
