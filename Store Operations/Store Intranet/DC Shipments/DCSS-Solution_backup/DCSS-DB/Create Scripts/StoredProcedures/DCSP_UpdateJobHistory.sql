IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_UpdateJobHistory')
	BEGIN
		PRINT 'Dropping Procedure DCSP_UpdateJobHistory'
		DROP  Procedure  DCSP_UpdateJobHistory
	END

GO

PRINT 'Creating Procedure DCSP_UpdateJobHistory'
GO
CREATE Procedure DCSP_UpdateJobHistory
	/* Param List */
	@beginDate datetime
	,@endDate datetime
	,@statusFl int
	,@UserID varchar(20)
	
	
AS

/******************************************************************************
**		File: DCSP_UpdateJobHistory.sql
**		Name: DCSP_UpdateJobHistory
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 10/30/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
IF EXISTS(SELECT RunID FROM DCSS_ImportJobHistory 
		WHERE RunDateFrom = @beginDate)
	UPDATE DCSS_ImportJobHistory SET
		StatusFl = @statusFl
		,RunDateTo = @endDate
		,RunBy = @UserID
		,RunDate = getDate()
	WHERE RunDateFrom = @beginDate	
	
ELSE

INSERT INTO DCSS_ImportJobHistory
	(
	RunDateFrom
	,RunDateTo
	,StatusFl
	,RunBy
	,RunDate
	)
	VALUES
	(
	@beginDate
	,@endDate 
	,@statusFl 
	,@UserID 	
	,getDate()
	)
	
IF @@error <> 0
	RAISERROR('Unable to fetch shipment count',16,3)
	

GO

GRANT EXEC ON DCSP_UpdateJobHistory TO AppUser

GO
