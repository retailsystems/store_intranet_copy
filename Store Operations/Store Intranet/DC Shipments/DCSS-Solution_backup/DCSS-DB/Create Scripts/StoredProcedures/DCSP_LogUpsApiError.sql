IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_LogUpsApiError')
	BEGIN
		PRINT 'Dropping Procedure DCSP_LogUpsApiError'
		DROP  Procedure  DCSP_LogUpsApiError
	END

GO

PRINT 'Creating Procedure DCSP_LogUpsApiError'
GO
CREATE Procedure DCSP_LogUpsApiError
	/* Param List */
	@UpsTracking varchar(30)
	,@ErrorCode int
	,@ErrorMsg varchar(2000)
AS

/******************************************************************************
**		File: DCSP_LogUpsApiError.sql
**		Name: DCSP_LogUpsApiError
**		Desc: Logs UPS API errors
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 05/19/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
INSERT INTO DCSS_UpsApiErrorLog
	(
	UpsTracking
	,ErrorCode
	,ErrorMsg
	)
	VALUES
	(
	@UpsTracking
	,@ErrorCode
	,@ErrorMsg
	)


GO

GRANT EXEC ON DCSP_LogUpsApiError TO AppUser

GO
