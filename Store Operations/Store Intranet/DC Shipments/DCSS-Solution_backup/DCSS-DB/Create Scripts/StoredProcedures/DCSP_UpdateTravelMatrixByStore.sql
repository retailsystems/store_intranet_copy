IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_UpdateTravelMatrixByStore')
	BEGIN
		PRINT 'Dropping Procedure DCSP_UpdateTravelMatrixByStore'
		DROP  Procedure  DCSP_UpdateTravelMatrixByStore
	END

GO

PRINT 'Creating Procedure DCSP_UpdateTravelMatrixByStore'
GO
CREATE Procedure DCSP_UpdateTravelMatrixByStore
	/* Param List */
	@TravelMatrixID smallint
	,@next_travel smallint
	,@nextam_travel smallint
	,@nextsaver_travel	smallint
	,@day2_travel smallint
	,@day2am_travel smallint
	,@day3_travel smallint
	,@ground_travel smallint
	,@verified_flag bit
	,@UserId varchar(20)
	,@MasterWHSEFlag bit
	
AS

/******************************************************************************
**		File: DCSP_UpdateTravelMatrixByStore.sql
**		Name: DCSP_UpdateTravelMatrixByStore
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 08/05/2004
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    01/15/04		Sri Bajjuri		Update by TravelMatrixID in stead of by Store_Num
*******************************************************************************/
DECLARE @whseFlag bit
SET @whseFlag = 0
UPDATE dcss_upstravelTime
	SET
		next_travel = @next_travel 
		,nextam_travel = @nextam_travel 
		,nextsaver_travel = @nextsaver_travel	
		,day2_travel = @day2_travel 
		,day2am_travel = @day2am_travel 
		,day3_travel = @day3_travel 
		,ground_travel = @ground_travel 
		,verified_flag = @verified_flag 
		,UpdateBy = @UserId 
		,UpdateDate = getDate()
		,MasterWHSEFlag = @MasterWHSEFlag
	WHERE TravelMatrixID = @TravelMatrixID 
	
	IF (@MasterWHSEFlag = 1)
			SET @whseFlag = 0		
	ELSE
		BEGIN
			SET @whseFlag = 1
		END
		
		UPDATE dcss_upstravelTime 
			SET MasterWHSEFlag = @whseFlag
			WHERE Store_Num in (select store_num from dcss_upstravelTime WHERE TravelMatrixID = @TravelMatrixID )
			AND TravelMatrixID <> @TravelMatrixID 
	
	
	
IF @@error <> 0
	RAISERROR('Failed to update travel time',16,3)
GO

GRANT EXEC ON DCSP_UpdateTravelMatrixByStore TO AppUser

GO
