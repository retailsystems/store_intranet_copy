IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_GetRunID')
	BEGIN
		PRINT 'Dropping Procedure DCSP_GetRunID'
		DROP  Procedure  DCSP_GetRunID
	END

GO

PRINT 'Creating Procedure DCSP_GetRunID'
GO
CREATE Procedure DCSP_GetRunID
	/* Param List */
	@runDateFrom datetime
	,@runDateTo datetime
	,@statusFl int
	,@UserID varchar(20)
	,@runID int OUTPUT
AS

/******************************************************************************
**		File: DCSP_GetRunID.sql
**		Name: DCSP_GetRunID
**		Desc: This procedure returns RunID # for the given daterange, for new 
**			daterange creates a new RunID.
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
SELECT @runID = RunID FROM DCSS_ImportJobHistory 
	WHERE RunDateFrom = @runDateFrom	

IF @runID IS NULL
	BEGIN
		INSERT INTO DCSS_ImportJobHistory
		(
		RunDateFrom
		,RunDateTo
		,StatusFl
		,RunBy
		,RunDate
		)
		VALUES
		(
		@runDateFrom
		,@runDateTo 
		,@statusFl 
		,@UserID 	
		,getDate()
		)
		
	SELECT @runID = @@IDENTITY
	END
GO

GRANT EXEC ON DCSP_GetRunID TO AppUser

GO
