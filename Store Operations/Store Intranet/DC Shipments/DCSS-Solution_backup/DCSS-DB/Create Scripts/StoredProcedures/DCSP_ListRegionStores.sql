IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ListRegionStores')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ListRegionStores'
		DROP  Procedure  DCSP_ListRegionStores
	END

GO

PRINT 'Creating Procedure DCSP_ListRegionStores'
GO
CREATE Procedure DCSP_ListRegionStores
	/* Param List */
	@RegionID varchar(5)
	
AS

/******************************************************************************
**		File: DCSP_ListRegionStores.sql
**		Name: DCSP_ListRegionStores
**		Desc: Returns list of stores belong to a region.
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 2/20/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT StoreNum, StoreName FROM Store
	WHERE (Deleted is Null OR Deleted =0) AND StagingStore <> 1 AND Region = @RegionID
	ORDER BY StoreNum


GO

GRANT EXEC ON DCSP_ListRegionStores TO AppUser

GO
