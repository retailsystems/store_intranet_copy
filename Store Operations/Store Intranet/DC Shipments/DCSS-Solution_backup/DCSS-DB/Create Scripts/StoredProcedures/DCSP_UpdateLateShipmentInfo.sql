IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_UpdateLateShipmentInfo')
	BEGIN
		PRINT 'Dropping Procedure DCSP_UpdateLateShipmentInfo'
		DROP  Procedure  DCSP_UpdateLateShipmentInfo
	END

GO

PRINT 'Creating Procedure DCSP_UpdateLateShipmentInfo'
GO
CREATE Procedure DCSP_UpdateLateShipmentInfo
	/* Param List */
	@UpsTracking varchar(30)	
	,@IgnoreLateShipmentFlag bit
	,@Comments nvarchar(2000)
	,@UserID varchar(20)
AS

/******************************************************************************
**		File: DCSP_UpdateLateShipmentInfo.sql
**		Name: DCSP_UpdateLateShipmentInfo
**		Desc: Updates Comments & IgnoreLateShipmentFlag of a shipment
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 4/05/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

--CHECK IF Exists in HotTopic Database
IF EXISTS(SELECT UpsTracking FROM DCSS_UpsShipments WHERE UpsTracking = @UpsTracking ) 
	--HotTopic 
	BEGIN
		IF len(@Comments) > 0
			UPDATE DCSS_UpsShipments
			SET Comments = @Comments 			
			WHERE UpsTracking = @UpsTracking
		UPDATE DCSS_UpsShipments
			SET IgnoreLateShipmentFlag = @IgnoreLateShipmentFlag
			,UpdateBy = @UserID
			,UpdateDate = getDate()
			WHERE UpsTracking = @UpsTracking		
		
	END
ELSE --Torrid	
	BEGIN			
		IF len(@Comments) > 0
			UPDATE Torrid_Dev.DBO.DCSS_UpsShipments
			SET Comments = @Comments 			
			WHERE UpsTracking = @UpsTracking
		UPDATE Torrid_Dev.DBO.DCSS_UpsShipments
			SET IgnoreLateShipmentFlag = @IgnoreLateShipmentFlag
			,UpdateBy = @UserID
			,UpdateDate = getDate()
			WHERE UpsTracking = @UpsTracking
	END


GO

GRANT EXEC ON DCSP_UpdateLateShipmentInfo TO AppUser

GO
