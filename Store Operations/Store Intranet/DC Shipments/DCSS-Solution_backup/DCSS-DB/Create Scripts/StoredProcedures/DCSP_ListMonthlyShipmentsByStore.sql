IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ListMonthlyShipmentsByStore')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ListMonthlyShipmentsByStore'
		DROP  Procedure  DCSP_ListMonthlyShipmentsByStore
	END

GO

PRINT 'Creating Procedure DCSP_ListMonthlyShipmentsByStore'
GO
CREATE Procedure DCSP_ListMonthlyShipmentsByStore
	/* Param List */
	@mm smallint
	,@yy smallint
	,@storeNum smallint
AS

/******************************************************************************
**		File: DCSP_ListMonthlyShipmentsByStore.sql
**		Name: DCSP_ListMonthlyShipmentsByStore
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

IF @storeNum < 5000 -- Hot Topic store
	EXEC DCSP_GetStoreMonthlyShipments @mm 	,@yy ,@storeNum 
ELSE -- Torrid store
	EXEC Torrid_Dev.DBO.DCSP_GetStoreMonthlyShipments @mm,@yy ,@storeNum 


GO

GRANT EXEC ON DCSP_ListMonthlyShipmentsByStore TO AppUser

GO
