IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ListServiceTypes')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ListServiceTypes'
		DROP  Procedure  DCSP_ListServiceTypes
	END

GO

PRINT 'Creating Procedure DCSP_ListServiceTypes'
GO
CREATE Procedure DCSP_ListServiceTypes
	/* Param List */
AS

/******************************************************************************
**		File: DCSP_ListServiceTypes.sql
**		Name: DCSP_ListServiceTypes
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 4/1/2004
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT DISTINCT ServiceType FROM DCSS_ShipVia ORDER BY ServiceType


GO

GRANT EXEC ON DCSP_ListServiceTypes TO AppUser

GO
