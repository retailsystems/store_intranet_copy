IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ListPendingShipments')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ListPendingShipments'
		DROP  Procedure  DCSP_ListPendingShipments
	END

GO

PRINT 'Creating Procedure DCSP_ListPendingShipments'
GO
CREATE Procedure DCSP_ListPendingShipments
	/* Param List */
	@numDaysOld smallint
	,@whse smallint
AS

/******************************************************************************
**		File: 
**		Name: DCSP_ListPendingShipments
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**     1/30/04		Sri Bajjuri			Date difference calculation changed to  column 
**										"ScheduledDeliveryDate" from "ShippedDateTime"
**		1/14/05		Sri Bajjuri			Filter resultset by whse #(if passed)
*******************************************************************************/
IF @whse > 0 
	SELECT  UpsTracking,dcss.StoreNum,ServiceType,ShippedDateTime,PackageType,StatusCD
		,Status,Comments,whse
		FROM DCSS_AllHtTorridShipments dcss
		JOIN DCSS_AllHtTorridStores s ON dcss.StoreNum = s.StoreNum
		WHERE datediff(day,ScheduledDeliveryDate,getdate()) > @numDaysOld
		AND StatusCD <> 3
		AND isNull(s.internetstore,0) = 0
		AND isNull(s.stagingstore,0) = 0
		AND dcss.WHSE = @whse
ELSE
	SELECT  UpsTracking,dcss.StoreNum,ServiceType,ShippedDateTime,PackageType,StatusCD
		,Status,Comments,whse
		FROM DCSS_AllHtTorridShipments dcss
		JOIN DCSS_AllHtTorridStores s ON dcss.StoreNum = s.StoreNum
		WHERE datediff(day,ScheduledDeliveryDate,getdate()) > @numDaysOld
		AND StatusCD <> 3
		AND isNull(s.internetstore,0) = 0
		AND isNull(s.stagingstore,0) = 0

/*
SELECT  UpsTracking,StoreNum,ServiceType,ShippedDateTime,PackageType,ups.StatusCD
	,sl.Description as Status,Comments
	FROM DCSS_UPSShipments ups
	INNER JOIN DCSS_StatusLookUp sl ON ups.StatusCD = sl.StatusCD
	WHERE datediff(day,ScheduledDeliveryDate,getdate()) > @numDaysOld
	AND ups.StatusCD not in(3)	
	AND StoreNum in(select storeNum from store where (internetstore is null or internetstore = 0)
			 AND (stagingstore is null or stagingstore = 0))
UNION

SELECT  UpsTracking,StoreNum,ServiceType,ShippedDateTime,PackageType,ups.StatusCD
	,sl.Description as Status,Comments
	FROM Torrid.dbo.DCSS_UPSShipments ups
	INNER JOIN Torrid.dbo.DCSS_StatusLookUp sl ON ups.StatusCD = sl.StatusCD
	WHERE datediff(day,ScheduledDeliveryDate,getdate()) > @numDaysOld
	AND ups.StatusCD not in(3)	
	AND StoreNum in(select storeNum from torrid.dbo.store where (internetstore is null or internetstore = 0)
			 AND (stagingstore is null or stagingstore = 0))	
	Order BY ShippedDateTime,StoreNum
*/

GO

GRANT EXEC ON DCSP_ListPendingShipments TO AppUser

GO
