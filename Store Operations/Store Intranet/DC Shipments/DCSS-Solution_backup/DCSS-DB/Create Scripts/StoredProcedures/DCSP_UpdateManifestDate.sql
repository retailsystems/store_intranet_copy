IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_UpdateManifestDate')
	BEGIN
		PRINT 'Dropping Procedure DCSP_UpdateManifestDate'
		DROP  Procedure  DCSP_UpdateManifestDate
	END

GO

PRINT 'Creating Procedure DCSP_UpdateManifestDate'
GO
CREATE Procedure DCSP_UpdateManifestDate
	/* Param List */
	@ManifestID int
	,@CloseDate smalldatetime
	,@userID varchar(50)
AS

/******************************************************************************
**		File: DCSP_UpdateManifestDate.sql
**		Name: DCSP_UpdateManifestDate
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 7/29/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

UPDATE DCSS_ManifestCloseDate
	SET CloseDate =  @CloseDate
	,updateby = @userID
	,updatedate = getdate()
	WHERE ManifestID = @ManifestID
	
IF @@error <> 0
	BEGIN			
		RAISERROR('Failed to update manifest info',16,3)		
	END

GO

GRANT EXEC ON DCSP_UpdateManifestDate TO AppUser

GO
