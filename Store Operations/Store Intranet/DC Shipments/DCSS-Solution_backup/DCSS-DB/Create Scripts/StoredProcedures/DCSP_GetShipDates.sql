IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_GetShipDates')
	BEGIN
		PRINT 'Dropping Procedure DCSP_GetShipDates'
		DROP  Procedure  DCSP_GetShipDates
	END

GO

PRINT 'Creating Procedure DCSP_GetShipDates'
GO
CREATE Procedure DCSP_GetShipDates
	/* Param List */
	
AS

/******************************************************************************
**		File: DCSP_GetShipDates.sql
**		Name: DCSP_GetShipDates
**		Desc: Returns new run date range.
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    11/19/02		Sri					Always return current Sys Date as end date
*******************************************************************************/

DECLARE @beginDate datetime	,@endDate datetime
DECLARE @maxRowStatus int , @maxRowBeginDt datetime, @maxRowEndDt datetime
-- Always return current Sys Date as end date MOD 11/19/03
SET @endDate = getDate()

IF EXISTS(SELECT RunID FROM DCSS_ImportJobHistory WHERE StatusFl in(2,3))
	SELECT @maxRowStatus = StatusFl
		, @maxRowBeginDt = RunDateFrom
		, @maxRowEndDt = RunDateTo
	FROM DCSS_ImportJobHistory
	WHERE  StatusFl in(2,3)
ELSE
	SELECT @maxRowStatus = StatusFl
		, @maxRowBeginDt = RunDateFrom
		, @maxRowEndDt = RunDateTo
	FROM DCSS_ImportJobHistory
	WHERE RunDateTo = (SELECT MAX(RunDateTo) FROM DCSS_ImportJobHistory WHERE  StatusFl = 1)

IF @maxRowStatus IS NOT NULL
	-- Status 1-Success, 2- Cancelled, 3-Failed
	BEGIN
		IF @maxRowStatus = 1 
			BEGIN
				--SET @endDate = DATEADD(day, 1, @maxRowEndDt)
				SET @beginDate = DATEADD(minute, 1, @maxRowEndDt)				
				/*
				IF DATEPART(weekday,@endDate) = 7 -- if Saturday bump it to Monday
					SELECT @endDate = DATEADD(day,2,@endDate)
				IF DATEPART(weekday,@endDate) = 1 -- if Sunday bump it to Monday
					SELECT @endDate = DATEADD(day,1,@endDate)	
				*/			
			END
		ELSE
			BEGIN
				SET @endDate =  @maxRowEndDt
				SET @beginDate = @maxRowBeginDt				
			END
		

	END

SELECT @beginDate as BeginDate, @endDate as EndDate


IF @@error <> 0
	RAISERROR('Unable to fetch shipment date range',16,3)
	
GO

GRANT EXEC ON DCSP_GetShipDates TO AppUser

GO
