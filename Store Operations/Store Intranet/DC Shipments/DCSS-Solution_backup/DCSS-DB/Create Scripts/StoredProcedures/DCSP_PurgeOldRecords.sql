IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_PurgeOldRecords')
	BEGIN
		PRINT 'Dropping Procedure DCSP_PurgeOldRecords'
		DROP  Procedure  DCSP_PurgeOldRecords
	END

GO

PRINT 'Creating Procedure DCSP_PurgeOldRecords'
GO
CREATE Procedure DCSP_PurgeOldRecords
	/* Param List */
	--@purge_cutoff_date datetime = NULL
AS

/******************************************************************************
**		File: DCSP_PurgeOldRecords.sql
**		Name: DCSP_PurgeOldRecords
**		Desc: Purges shipment records older than older than 7 months
**				
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 07/06/2005
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
DECLARE @purge_cutoff_date datetime
SET @purge_cutoff_date = DATEADD(month,-7,convert(varchar,getdate(),101))

DELETE FROM Hottopic2.dbo.DCSS_UpsShipments WHERE ShippedDateTime < @purge_cutoff_date

DELETE FROM Torrid.dbo.DCSS_UpsShipments WHERE ShippedDateTime < @purge_cutoff_date

GO

GRANT EXEC ON DCSP_PurgeOldRecords TO AppUser

GO
