IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ListUpsTrackingDs')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ListUpsTrackingDs'
		DROP  Procedure  DCSP_ListUpsTrackingDs
	END

GO

PRINT 'Creating Procedure DCSP_ListUpsTrackingDs'
GO
CREATE Procedure DCSP_ListUpsTrackingDs
	/* Param List */
AS

/******************************************************************************
**		File: DCSP_ListUpsTrackingDs.sql
**		Name: DCSP_ListUpsTrackingDs
**		Desc: Used by UpsXMLAPI batch process
**
**		This template can be customized:
**              
**		Return values: 
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 5/18/2004
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT StoreNum, UpsTracking, ScheduledDeliveryDate,StatusCD
	FROM DCSS_AllHtTorridShipments 	
	WHERE StatusCD not in(2,3) AND ServiceProvider = 1	
	AND OriginalScheduledDeliveryDate >= getdate()
	order by shippeddatetime


--AND StoreNum in (1)
GO

GRANT EXEC ON DCSP_ListUpsTrackingDs TO AppUser

GO
