Imports System.Data.OracleClient
Namespace HotTopic.DCSS.Services
    Public Enum EnumRSTypes As Byte
        DataSet = 1
        DataReader = 2
    End Enum
    Public Class OracleDataManager
        Public strConn As String
        Private _cn As OracleConnection
        Private cnnSQL As OracleConnection
        Sub New()
            GetConnectionString()
        End Sub
        Sub New(ByVal connectionString As String)
            strConn = connectionString
        End Sub
        Private Sub GetConnectionString()
            strConn = "" ' set the connection string value here
        End Sub

        Public Function ExecuteReader(ByVal strSQL As String) As OracleDataReader
            Return OracleHelper.ExecuteReader(strConn, CommandType.Text, strSQL)
        End Function
        Public Function ExecuteReader(ByVal spName As String, ByVal ParamArray commandParameters() As OracleParameter) As OracleDataReader
            Return OracleHelper.ExecuteReader(strConn, CommandType.StoredProcedure, spName, commandParameters)
        End Function
        Public Function ExecuteDataSet(ByVal strSQL As String) As DataSet
            Return OracleHelper.ExecuteDataset(strConn, CommandType.Text, strSQL)
        End Function
        Public Function ExecuteDataSet(ByVal spName As String, ByVal ParamArray commandParameters() As OracleParameter) As DataSet
            Return OracleHelper.ExecuteDataset(strConn, CommandType.StoredProcedure, spName, commandParameters)
        End Function

        Public Function ExecuteNonQuery(ByVal strSQL As String) As Integer
            Return OracleHelper.ExecuteNonQuery(strConn, CommandType.Text, strSQL)
        End Function
        Public Function ExecuteNonQuery(ByVal spName As String, ByVal ParamArray commandParameters() As OracleParameter) As Integer
            Return OracleHelper.ExecuteNonQuery(strConn, CommandType.StoredProcedure, spName, commandParameters)
        End Function
        Public Function ExecuteScalar(ByVal spName As String, ByVal ParamArray commandParameters() As OracleParameter) As Integer
            Return OracleHelper.ExecuteScalar(strConn, CommandType.StoredProcedure, spName, commandParameters)
        End Function
    End Class


End Namespace