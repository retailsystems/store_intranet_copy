' Static Model

Imports System.Data.SqlClient

Namespace HotTopic.DCSS.Services

    Public Class UtilityManager

        Public Shared Function InsertApos(ByVal InStr As String) As String
            Dim outStr As String = ""
            outStr = Replace(InStr, "'", "''")
            Return outStr
        End Function

        Public Shared Function NullToInteger(ByVal Value As Object) As Int32
            If Convert.IsDBNull(Value) Then
                Return 0
            Else
                Return Convert.ToInt32(Value)
            End If
        End Function
        Public Shared Function NullToDouble(ByVal Value As Object) As Double
            If Convert.IsDBNull(Value) Then
                Return 0
            Else
                Return Convert.ToDouble(Value)
            End If
        End Function
        Public Shared Function NullToString(ByVal Value As Object) As String
            If Convert.IsDBNull(Value) Then
                Return ""
            Else
                Return Convert.ToString(Value)
            End If
        End Function

        Public Shared Function NullToDate(ByVal Value As Object) As Date
            If Convert.IsDBNull(Value) Then
                Return #12:00:00 AM#
            Else
                Return Value
            End If
        End Function

        Public Shared Function DateToNull(ByVal Value As Date) As Object
            If Value = #12:00:00 AM# Then
                Return Convert.DBNull
            Else
                Return Value
            End If
        End Function

        Public Shared Function IntegerToNull(ByVal Value As Int32) As Object
            If Value = 0 Then
                Return Convert.DBNull
            Else
                Return Value
            End If
        End Function
        Public Shared Function StringToNull(ByVal Value As String) As Object
            If Value.Trim = "" Then
                Return Convert.DBNull
            Else
                Return Value
            End If
        End Function
        '4/19/2005 SRI
        Public Shared Function BuildSortExpression(ByVal sortStr As String, ByVal sortColumn As String) As String
            Dim arrSort As String()
            Dim i As Int16
            Dim sortExpression As String = ""
            Dim blnExists As Boolean = False
            sortStr = sortStr.Trim
            If sortStr.Length > 0 Then
                arrSort = sortStr.Split(",")
                For i = 0 To UBound(arrSort)
                    sortExpression &= IIf(sortExpression.Length > 0, ",", "")
                    If arrSort(i).StartsWith(sortColumn & " ") Then
                        If arrSort(i).EndsWith("ASC") Then
                            sortExpression &= sortColumn & " DESC"
                        Else
                            sortExpression &= sortColumn & " ASC"
                        End If
                        blnExists = True
                    Else
                        sortExpression &= arrSort(i)
                    End If
                Next
            End If
            If Not blnExists Then
                sortExpression &= IIf(sortExpression.Length > 0, ",", "") & sortColumn & " ASC"
            End If
            Return sortExpression
        End Function


    End Class ' END CLASS DEFINITION UtilityManager

End Namespace ' HotTopic.ISS.Services

