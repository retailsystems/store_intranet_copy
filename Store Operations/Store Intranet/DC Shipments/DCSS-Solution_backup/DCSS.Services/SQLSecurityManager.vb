Namespace HotTopic.DCSS.Services
    Public Class SQLSecurityManager
        Inherits SQLDataManager

        Private Shared _sqlDataMgr As SQLDataManager

        Public Shared Shadows Function GetInstance() As SQLDataManager
            If _sqlDataMgr Is Nothing Then
                _sqlDataMgr = New SQLSecurityManager(HotTopic.DCSS.Settings.AppSetting.SecurityConnectionString)
            End If
            Return _sqlDataMgr
        End Function

        Private Sub New(ByVal connString As String)
            MyBase.New(connString)
        End Sub


    End Class

End Namespace
