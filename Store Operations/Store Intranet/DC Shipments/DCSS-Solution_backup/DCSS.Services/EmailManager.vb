' Static Model
Imports System.Web.Mail
Imports System.Text

Namespace HotTopic.DCSS.Services

    Public Class EmailManager

        Private _mail As MailMessage
        Private _smtpServer As String

        Public Sub New(ByVal smtpServer As String)
            If (smtpServer.Length = 0) Then
                'throw exception
                Throw New Exception("No SMTP server.")
            Else
                _smtpServer = smtpServer
            End If
            _mail = New MailMessage()
            _mail.BodyFormat = MailFormat.Text
            _mail.Priority = MailPriority.Normal
        End Sub

        Public Sub Send(ByVal mailBody As String)
            _mail.Body = mailBody
            SmtpMail.SmtpServer = _smtpServer
            If (_mail.From Is Nothing) Or (_mail.To Is Nothing) Then
                'throw exception
                Throw New Exception("No email sender and/or recipient.")
            Else
                SmtpMail.Send(_mail)
            End If
        End Sub

        Public Sub AddCC(ByVal ccEmail As String)
            If Not (_mail.Cc Is Nothing) Then
                Dim sb As StringBuilder
                sb = New StringBuilder(_mail.Cc)
                sb.Append(";")
                sb.Append(ccEmail)
                ccEmail = sb.ToString()
            End If
            _mail.Cc = ccEmail
        End Sub

        Public Sub AddRecipient(ByVal toEmail As String)
            Dim sb As StringBuilder
            If (_mail.To Is Nothing) Then
                _mail.To = ""
                sb = New StringBuilder()
            Else
                sb = New StringBuilder(_mail.To)
                sb.Append("; ")
            End If
            sb.Append(toEmail)
            toEmail = sb.ToString()
            _mail.To = toEmail
        End Sub

        Public Sub AddAttachment(ByVal filePath As String)
            Try
                _mail.Attachments.Add(New MailAttachment(filePath))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Property Recipient() As String
            Get
                Return _mail.To
            End Get

            Set(ByVal Value As String)
                _mail.To = Value
            End Set
        End Property

        Public Property Subject() As String
            Get
                Return _mail.Subject
            End Get

            Set(ByVal Value As String)
                _mail.Subject = Value
            End Set
        End Property

        Public Property Sender() As String
            Get
                Return _mail.From
            End Get

            Set(ByVal Value As String)
                _mail.From = Value
            End Set
        End Property

        Public Property Format() As MailFormat
            Get
                Return _mail.BodyFormat
            End Get

            Set(ByVal Value As MailFormat)
                _mail.BodyFormat = Value
            End Set
        End Property

        Public Property Priority() As MailPriority
            Get
                Return _mail.Priority
            End Get

            Set(ByVal Value As MailPriority)
                _mail.Priority = Value
            End Set
        End Property

    End Class ' END CLASS DEFINITION EmailManager

End Namespace ' HotTopic.ISS.Services

