Namespace HotTopic.DCSS.Settings

    Public Class AppSetting

        Private Shared _connString As String
        Private Shared _secConnString As String
        Private Shared _cacheDependency As String
        Private Shared _smtpServer As String
        Private Shared _smtpSender As String
        Private Shared _webmasterEmail As String
        Private Shared _strStoreSubnetMask As String
        Private Shared _projectName As String
        'Private Shared _errorLog As String
        Private Shared _dcssMailReceivers As String
        Private Shared _cancellationEmailBody As String
        Private Shared _successEmailBody As String
        Private Shared _numDaysOld As Integer
        Private Shared _isRoleBasedSecurityOn As Boolean = True
        Private Shared _dmJobCodes As String
        Private Shared _rdJobCodes As String
        Private Shared _upsXmlRequest As String
        Private Shared _upsXmlPostURL As String

        Private Shared _FedExXmlRequest As String
        Private Shared _FedExXmlPostURL As String

        Private Shared _httpPostFailLimit As Int16
        '01/11/05 TNDC enhancements
        Private Shared _HQDCConnectionString As String
        Private Shared _TNDCConnectionString As String
        Private Shared _localPath As String
        Private Shared _ftpHost As String
        Private Shared _ftpFolder As String
        Private Shared _ftpUser As String
        Private Shared _ftpPwd As String
        Private Shared _adminRoles As String
        Private Shared _processLog As String


        Public Shared Property ConnectionString() As String
            Get
                Return _connString
            End Get
            Set(ByVal Value As String)
                _connString = Value
            End Set
        End Property
        Public Shared Property SecurityConnectionString() As String
            Get
                Return _secConnString
            End Get
            Set(ByVal Value As String)
                _secConnString = Value
            End Set
        End Property
        Public Shared Property CacheDependencyFile() As String
            Get
                Return _cacheDependency
            End Get
            Set(ByVal Value As String)
                _cacheDependency = Value
            End Set
        End Property

        Public Shared Property SmtpServer() As String
            Get
                Return _smtpServer
            End Get
            Set(ByVal Value As String)
                _smtpServer = Value
            End Set
        End Property

        Public Shared Property SmtpSender() As String
            Get
                Return _smtpSender
            End Get
            Set(ByVal Value As String)
                _smtpSender = Value
            End Set
        End Property

        Public Shared Property StoreSubnetMask() As String
            Get
                Return _strStoreSubnetMask
            End Get
            Set(ByVal Value As String)
                _strStoreSubnetMask = Value
            End Set
        End Property
        Public Shared Property ProjectName() As String
            Get
                Return _projectName
            End Get
            Set(ByVal Value As String)
                _projectName = Value
            End Set
        End Property
        'Public Shared Property ErrorLog() As String
        '    Get
        '        Return _errorLog
        '    End Get
        '    Set(ByVal Value As String)
        '        _errorLog = Value
        '    End Set
        'End Property
        Public Shared Property WebmasterEmail() As String
            Get
                Return _webmasterEmail
            End Get
            Set(ByVal Value As String)
                _webmasterEmail = Value
            End Set
        End Property


        Public Shared Property DcssMailReceivers() As String
            Get
                Return _dcssMailReceivers
            End Get
            Set(ByVal Value As String)
                _dcssMailReceivers = Value
            End Set
        End Property

        Public Shared Property CancellationEmailBody() As String
            Get
                Return _cancellationEmailBody
            End Get
            Set(ByVal Value As String)
                _cancellationEmailBody = Value
            End Set
        End Property
        Public Shared Property SuccessEmailBody() As String
            Get
                Return _successEmailBody
            End Get
            Set(ByVal Value As String)
                _successEmailBody = Value
            End Set
        End Property
        Public Shared Property NumDaysOld() As Integer
            Get
                Return _numDaysOld
            End Get
            Set(ByVal Value As Integer)
                _numDaysOld = Value
            End Set
        End Property
        Public Shared Property IsRoleBasedSecurityOn() As Boolean
            Get
                Return _isRoleBasedSecurityOn
            End Get
            Set(ByVal Value As Boolean)
                _isRoleBasedSecurityOn = Value
            End Set
        End Property
        Public Shared Property DmJobCodes() As String
            Get
                Return _dmJobCodes
            End Get
            Set(ByVal Value As String)
                _dmJobCodes = Value
            End Set
        End Property
        Public Shared Property RdJobCodes() As String
            Get
                Return _rdJobCodes
            End Get
            Set(ByVal Value As String)
                _rdJobCodes = Value
            End Set
        End Property
        Public Shared Property UpsXmlRequest() As String
            Get
                Return _upsXmlRequest
            End Get
            Set(ByVal Value As String)
                _upsXmlRequest = Value
            End Set
        End Property

        Public Shared Property UpsXmlPostURL() As String
            Get
                Return _upsXmlPostURL
            End Get
            Set(ByVal Value As String)
                _upsXmlPostURL = Value
            End Set
        End Property

        Public Shared Property FedExXmlRequest() As String
            Get
                Return _FedExXmlRequest
            End Get
            Set(ByVal Value As String)
                _FedExXmlRequest = Value
            End Set
        End Property

        Public Shared Property FedExXmlPostURL() As String
            Get
                Return _FedExXmlPostURL
            End Get
            Set(ByVal Value As String)
                _FedExXmlPostURL = Value
            End Set
        End Property
        Public Shared Property HttpPostFailLimit() As Int16
            Get
                Return _httpPostFailLimit
            End Get
            Set(ByVal Value As Int16)
                _httpPostFailLimit = Value
            End Set
        End Property
        '01/11/05 TNDC enhancements
        Public Shared Property HQDCConnectionString() As String
            Get
                Return _HQDCConnectionString
            End Get
            Set(ByVal Value As String)
                _HQDCConnectionString = Value
            End Set
        End Property
        Public Shared Property TNDCConnectionString() As String
            Get
                Return _TNDCConnectionString
            End Get
            Set(ByVal Value As String)
                _TNDCConnectionString = Value
            End Set
        End Property
        Public Shared Property LocalPath() As String
            Get
                Return _localPath
            End Get
            Set(ByVal Value As String)
                _localPath = Value
            End Set
        End Property
        Public Shared Property FtpHost() As String
            Get
                Return _ftpHost
            End Get
            Set(ByVal Value As String)
                _ftpHost = Value
            End Set
        End Property
        Public Shared Property FtpFolder() As String
            Get
                Return _ftpFolder
            End Get
            Set(ByVal Value As String)
                _ftpFolder = Value
            End Set
        End Property
        Public Shared Property FtpUser() As String
            Get
                Return _ftpUser
            End Get
            Set(ByVal Value As String)
                _ftpUser = Value
            End Set
        End Property
        Public Shared Property FtpPwd() As String
            Get
                Return _ftpPwd
            End Get
            Set(ByVal Value As String)
                _ftpPwd = Value
            End Set
        End Property
        Public Shared Property AdminRoles() As String
            Get
                Return _adminRoles
            End Get
            Set(ByVal Value As String)
                _adminRoles = Value
            End Set
        End Property

        Public Shared Property ProcessLog() As String
            Get
                Return _processLog
            End Get
            Set(ByVal Value As String)
                _processLog = Value
            End Set
        End Property
    End Class

End Namespace

