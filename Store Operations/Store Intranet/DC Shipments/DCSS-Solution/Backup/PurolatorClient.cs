﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using TrackingClient = PWSClient.TrackingClient;

namespace PWSClient
{
    class WMSOrderTracker
    {
        static void Main(string[] args)
        {
            string strTrackingNumber = string.Empty;            
            strTrackingNumber = "ANG000000059";
            ShipmentInfo shipmentinfo = new ShipmentInfo();
            shipmentinfo.PIN = strTrackingNumber;

            TrackingClient.PWSClient trackingClient = new TrackingClient.PWSClient();
            //trackingClient.CallGetDeliveryDetails(strTrackingNumber);            
            trackingClient.CallTrackPackagesByPin(ref shipmentinfo);
            //trackingClient.CallTrackPackagesByReference();
            shipmentinfo.GetShipmentInfo();
            Console.ReadLine();
        }
    }
}
