Imports HotTopic.DCSS.Services
Imports HotTopic.DCSS.Settings
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Xml
Imports PWSClient
Imports TrackingClient = PWSClient.TrackingClient


Namespace HotTopic.DCSS.UpsAPI
    Public Enum ErrorCode
        XmlParseError = 1
        HTTPPostFail = 2
        DBUpdate = 3
        Others = 99
    End Enum
    Public Enum ShipmentStatus
        Shipped = 0
        InTransit = 1
        Delivered = 2
        Received = 3
        PickUp = 4
        ManifestPickUp = 5
        Exception = 6
        NoInfoAvailable = 7
    End Enum
    Public Class ApiManager
        Public debugStr As String
        Public shpMnt As Shipment
        Private _XmlReq As String
        Private _XmlRes As String
        'Private _FedExXmlReq As String
        'Private _FedExXmlRes As String
        Public HttpPostFailCnt As Int32 = 0


        Public TotCnt As Int32
        Public SuccessCnt As Int32
        Public ProcessBeginDt As Date
        Private PrcoessCnt As Int32

        'Private _trackDs As DataSet
        ' Returns all non-delivered Shipments from DB
        Public Function GetPendingShipments(ByVal brand, ByVal provider) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(1) {}
                arParms(0) = New SqlParameter("@Brand", SqlDbType.VarChar, 2)
                arParms(0).Value = brand
                arParms(1) = New SqlParameter("@Provider", SqlDbType.VarChar, 25)
                arParms(1).Value = provider

                'Return SQLDataManager.GetInstance().GetDataSet("DCSP_ListUpsTrackingDs", arParms)
                Return SQLDataManager.GetInstance().GetDataSet("DCSP_ListUpsTrackingDs_ByBrandByProvider", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub UpdateShipmentInfo()
            Try
                Dim arParms() As SqlParameter = New SqlParameter(8) {}
                arParms(0) = New SqlParameter("@UpsTracking", SqlDbType.VarChar, 30)
                arParms(0).Value = shpMnt.TrackingNum
                arParms(1) = New SqlParameter("@StatusCD", SqlDbType.SmallInt)
                arParms(1).Value = shpMnt.Status
                arParms(2) = New SqlParameter("@Comments", SqlDbType.NVarChar, 2000)
                arParms(2).Value = ""
                arParms(3) = New SqlParameter("@UserID", SqlDbType.VarChar)
                arParms(3).Value = "System"
                arParms(4) = New SqlParameter("@ScheduledDeliveryDate", SqlDbType.DateTime)
                arParms(4).Value = UtilityManager.DateToNull(shpMnt.ScheduledDeliveryDate)
                arParms(5) = New SqlParameter("@DeliveredDate", SqlDbType.DateTime)
                arParms(5).Value = UtilityManager.DateToNull(shpMnt.DeliveredDate)
                arParms(6) = New SqlParameter("@UpdateFlag", SqlDbType.Bit)
                arParms(6).Value = 1
                arParms(7) = New SqlParameter("@UpsErrorCode", SqlDbType.Int)
                arParms(7).Value = UtilityManager.IntegerToNull(shpMnt.UpsErrorCode)
                arParms(8) = New SqlParameter("@UpsErrorDesc", SqlDbType.VarChar, 500)
                arParms(8).Value = UtilityManager.StringToNull(shpMnt.UpsErrorDesc)

                SQLDataManager.GetInstance().Execute("DCSP_UpdateShipmentInfo", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub UpdateFedExShipmentInfo()
            Try
                Dim arParms() As SqlParameter = New SqlParameter(4) {}
                arParms(0) = New SqlParameter("@UpsTracking", SqlDbType.VarChar, 30)
                arParms(0).Value = shpMnt.TrackingNum
                arParms(1) = New SqlParameter("@StatusCD", SqlDbType.VarChar, 50)
                arParms(1).Value = shpMnt.FedExStatus
                arParms(2) = New SqlParameter("@StatusDesc", SqlDbType.VarChar, 255)
                arParms(2).Value = shpMnt.FedExStatusDesc
                arParms(3) = New SqlParameter("@Carrier_DeliveredDate", SqlDbType.DateTime)
                If shpMnt.Carrier_Delivereddate Is Nothing Then
                    shpMnt.Carrier_Delivereddate = "#12:00:00 AM#"
                End If
                arParms(3).Value = UtilityManager.DateToNull(shpMnt.Carrier_Delivereddate)
                arParms(4) = New SqlParameter("@Carrier_PickupFlag", SqlDbType.VarChar, 1)
                arParms(4).Value = shpMnt.Carrier_PickupFlag


                SQLDataManager.GetInstance().Execute("DCSP_UpdateFedExShipmentStatus", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub LogException(ByVal ErrorCode As ErrorCode, ByVal e As Exception)
            Try

                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                arParms(0) = New SqlParameter("@UpsTracking", SqlDbType.VarChar, 30)
                arParms(0).Value = shpMnt.TrackingNum
                arParms(1) = New SqlParameter("@ErrorCode", SqlDbType.SmallInt)
                arParms(1).Value = ErrorCode
                arParms(2) = New SqlParameter("@ErrorMsg", SqlDbType.NVarChar, 2000)
                arParms(2).Value = Left(e.Message, 2000)
                SQLDataManager.GetInstance().Execute("DCSP_LogUpsApiError", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub LogExceptionTest(ByVal ErrorCode As ErrorCode, ByVal e As String)
            Try

                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                arParms(0) = New SqlParameter("@UpsTracking", SqlDbType.VarChar, 30)
                arParms(0).Value = shpMnt.TrackingNum
                arParms(1) = New SqlParameter("@ErrorCode", SqlDbType.SmallInt)
                arParms(1).Value = ErrorCode
                arParms(2) = New SqlParameter("@ErrorMsg", SqlDbType.NVarChar, 2000)
                arParms(2).Value = e
                SQLDataManager.GetInstance().Execute("DCSP_LogUpsApiError", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        '6/8/05 Update SHIP_VIA 
        Public Function GetUpsShipVia() As String
            Dim objXMLTextReader As XmlTextReader
            Dim objXPathDocument As XPath.XPathDocument
            Dim objXPathNavigator As XPath.XPathNavigator
            Dim objXPathNodeIterator As XPath.XPathNodeIterator
            Dim _statusCode As String = ""
            Dim _tempDateStr As String = ""
            Dim _schdDeliveryDate As Date
            Dim _DeliveredDate As String = ""
            Dim _responseStatusCode As String = "0"
            Dim _errDesc As String = ""
            Dim _errCode As String = ""
            Dim _upsErrCode As Int32 = 0
            Dim _upsShipViaCd As String = ""
            Dim _upsShipViaDesc As String = ""
            Dim sql, res As String
            Dim svcErrorMsg As String = ""
            Try
                _XmlReq = ""
                _XmlRes = ""
                _XmlReq = AppSetting.UpsXmlRequest
                _XmlReq = _XmlReq.Replace("%UpsTrackingNbr%", shpMnt.TrackingNum)

                '_XmlRes = SendRequest("UPS")

                Dim upsSvc As New UPSProcessSvc.UPSProcessServiceSoapClient("UPSProcessServiceSoap")
                _XmlRes = upsSvc.TrackShipment(AppSetting.UpsXmlPostURL, _XmlReq, HttpPostFailCnt, svcErrorMsg)
                upsSvc.Close()

                If svcErrorMsg.Trim().Length > 0 Then
                    Throw New UpsXmlException(svcErrorMsg)
                End If

                PrcoessCnt += 1
                objXMLTextReader = New XmlTextReader(_XmlRes, XmlNodeType.Document, Nothing)

                objXPathDocument = New XPath.XPathDocument(objXMLTextReader)
                objXPathNavigator = objXPathDocument.CreateNavigator()
                'MOD 7/22/04 Checking for  response status 1-success, 0-failure.
                _responseStatusCode = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Response/ResponseStatusCode/text()"))

                If _responseStatusCode = "1" Then
                    _upsShipViaCd = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Service/Code/text()"))
                    _upsShipViaDesc = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Service/Description/text()"))
                    _statusCode = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Package/Activity/Status/StatusType/Code/text()"))
                    _tempDateStr = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/ScheduledDeliveryDate/text()"))
                    If (_tempDateStr.Trim.Length = 8) Then
                        _schdDeliveryDate = GetDateFromUpsDate(_tempDateStr)
                    End If
                    _tempDateStr = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Package/RescheduledDeliveryDate/text()"))
                    If (_tempDateStr.Trim.Length = 8) Then
                        _schdDeliveryDate = GetDateFromUpsDate(_tempDateStr)
                    End If
                    If Not IsDBNull(UtilityManager.DateToNull(_schdDeliveryDate)) AndAlso (_schdDeliveryDate <> shpMnt.ScheduledDeliveryDate) Then
                        shpMnt.ScheduledDeliveryDate = _schdDeliveryDate
                        shpMnt.UpdateFlag = True
                    End If

                    Select Case UCase(Trim(_statusCode))
                        Case "D"
                            shpMnt.Status = ShipmentStatus.Delivered
                            _DeliveredDate = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Package/Activity/Date/text()"))
                            If (_DeliveredDate.Trim.Length = 8) Then
                                shpMnt.DeliveredDate = GetDateFromUpsDate(_DeliveredDate, GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Package/Activity/Time/text()")))
                            End If
                            shpMnt.UpdateFlag = True
                        Case "X"
                            If shpMnt.Status <> ShipmentStatus.Exception Then
                                shpMnt.Status = ShipmentStatus.Exception
                                shpMnt.UpdateFlag = True
                            End If

                    End Select

                    sql = "UPDATE Temp_DcShipments1 SET "
                    sql &= " UPS_SHIP_VIA_CD = '" & _upsShipViaCd & "'" & _
                            ",UPS_SHIP_VIA_DESC ='" & _upsShipViaDesc & "'"
                    If Not IsDBNull(UtilityManager.DateToNull(_schdDeliveryDate)) Then
                        sql &= ",ScheduledDeliveryDate = '" & _schdDeliveryDate & "'"
                    End If
                    If Not IsDBNull(UtilityManager.DateToNull(shpMnt.DeliveredDate)) Then
                        sql &= ",DeliveredDate = '" & shpMnt.DeliveredDate & "'"
                    End If
                    sql &= " WHERE INV_TRKG = '" & shpMnt.TrackingNum & "'"
                    SQLDataManager.GetInstance().ExecuteNonQuery(sql)
                    'If Not IsDBNull(UtilityManager.DateToNull(_schdDeliveryDate)) Then
                    '    sql = "UPDATE Temp_DcShipments SET "
                    '    sql &= " UPS_SHIP_VIA_CD = '" & _upsShipViaCd & "'" & _
                    '            ",UPS_SHIP_VIA_DESC ='" & _upsShipViaDesc & "'"
                    '    sql &= ",ScheduledDeliveryDate = '" & _schdDeliveryDate & "'"
                    '    sql &= " WHERE INV_TRKG = '" & shpMnt.TrackingNum & "'"
                    '    SQLDataManager.GetInstance().ExecuteNonQuery(sql)
                    'End If
                    'If Not IsDBNull(UtilityManager.DateToNull(shpMnt.DeliveredDate)) Then
                    '    sql = "UPDATE Temp_DcShipments SET "
                    '    sql &= "DeliveredDate = '" & shpMnt.DeliveredDate & "'"
                    '    sql &= " WHERE INV_TRKG = '" & shpMnt.TrackingNum & "'"
                    '    SQLDataManager.GetInstance().ExecuteNonQuery(sql)
                    'End If
                    'res = " - " & _upsShipViaCd & " " & _upsShipViaDesc
                Else
                    _errCode = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Response/Error/ErrorCode/text()"))
                    _errDesc = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Response/Error/ErrorDescription/text()"))
                    res = _errCode & _errDesc
                End If

            Catch ex As UpsXmlException
                If ex.Message = "Missing UPS XML Response" Then
                    LogException(ErrorCode.HTTPPostFail, ex)
                Else
                    LogException(ErrorCode.XmlParseError, ex)
                End If
            Catch ex As SqlException
                LogException(ErrorCode.DBUpdate, ex)
            Catch ex As Exception
                Throw ex
            End Try
            Return res
        End Function

        ' Returns Shipment current status
        Public Function GetTrackingDetails()

            Dim objXMLTextReader As XmlTextReader
            Dim objXPathDocument As XPath.XPathDocument
            Dim objXPathNavigator As XPath.XPathNavigator
            Dim objXPathNodeIterator As XPath.XPathNodeIterator
            Dim _statusCode As String = ""
            Dim _tempDateStr As String = ""
            Dim _tempTimeStr As String = ""
            Dim _schdDeliveryDate As Date
            Dim _DeliveredDate As String = ""
            Dim _responseStatusCode As String = "0"
            Dim _errDesc As String = ""
            Dim _errCode As String = ""
            Dim _upsErrCode As Int32 = 0
            Dim svcErrorMsg As String = ""

            Try
                _XmlReq = ""
                _XmlRes = ""
                _XmlReq = AppSetting.UpsXmlRequest
                _XmlReq = _XmlReq.Replace("%UpsTrackingNbr%", shpMnt.TrackingNum)
                '_XmlRes = SendRequest("UPS")

                Dim upsSvc As New UPSProcessSvc.UPSProcessServiceSoapClient("UPSProcessServiceSoap")
                _XmlRes = upsSvc.TrackShipment(AppSetting.UpsXmlPostURL, _XmlReq, HttpPostFailCnt, svcErrorMsg)
                upsSvc.Close()

                If svcErrorMsg.Trim().Length > 0 Then
                    Throw New UpsXmlException(svcErrorMsg)
                End If


                PrcoessCnt += 1
                objXMLTextReader = New XmlTextReader(_XmlRes, XmlNodeType.Document, Nothing)

                objXPathDocument = New XPath.XPathDocument(objXMLTextReader)
                objXPathNavigator = objXPathDocument.CreateNavigator()

                'start - added below code to update the delivery status
                shpMnt.UpsErrorCode = 0
                shpMnt.UpsErrorDesc = ""

                shpMnt.FedExStatus = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Package/Activity/Status/StatusType/Code/text()"))
                shpMnt.FedExStatusDesc = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Package/Activity/Status/StatusType/Description/text()"))

                shpMnt.Carrier_PickupFlag = "N"
                If (GetNodeValue(objXPathNavigator.Select("//TrackResponse/Response/ResponseStatusCode/text()")) = 1) Then
                    _tempDateStr = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/ScheduledDeliveryDate/text()"))
                    If (_tempDateStr.Trim.Length = 8) Then
                        shpMnt.Carrier_PickupFlag = "Y"
                    End If
                    _tempDateStr = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Package/RescheduledDeliveryDate/text()"))
                    If (_tempDateStr.Trim.Length = 8) Then
                        shpMnt.Carrier_PickupFlag = "Y"
                    End If

                End If

                If (shpMnt.FedExStatus = "D" And GetNodeValue(objXPathNavigator.Select("//TrackResponse/Response/ResponseStatusCode/text()")) = 1) Then
                    _tempDateStr = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Package/Activity/Date/text()"))
                    '_tempTimeStr = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Package/Activity/Time/text()"))
                    If (_tempDateStr.Trim.Length = 8) Then
                        shpMnt.Carrier_PickupFlag = "Y"
                        shpMnt.Carrier_Delivereddate = GetDateFromUpsDate(_tempDateStr)
                    End If
                End If


                UpdateFedExShipmentInfo()
                'end
                'MOD 7/22/04 Checking for  response status 1-success, 0-failure.
                _responseStatusCode = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Response/ResponseStatusCode/text()"))

                If _responseStatusCode = "1" Then
                    shpMnt.UpsErrorCode = 0
                    shpMnt.UpsErrorDesc = ""
                    _statusCode = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Package/Activity/Status/StatusType/Code/text()"))
                    _tempDateStr = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/ScheduledDeliveryDate/text()"))
                    If (_tempDateStr.Trim.Length = 8) Then
                        _schdDeliveryDate = GetDateFromUpsDate(_tempDateStr)
                    End If
                    _tempDateStr = GetNodeValue(objXPathNavigator.Select("//TrackResponse/Shipment/Package/RescheduledDeliveryDate/text()"))
                    If (_tempDateStr.Trim.Length = 8) Then
                        _schdDeliveryDate = GetDateFromUpsDate(_tempDateStr)
                    End If
                    If Not IsDBNull(UtilityManager.DateToNull(_schdDeliveryDate)) AndAlso (_schdDeliveryDate <> shpMnt.ScheduledDeliveryDate) Then
                        shpMnt.ScheduledDeliveryDate = _schdDeliveryDate
                        shpMnt.UpdateFlag = True
                    End If
                End If

                'UpdateShipmentInfo_Debug()

                If shpMnt.UpdateFlag = True Then
                    UpdateShipmentInfo()
                End If
                SuccessCnt += 1
            Catch ex As UpsXmlException
                If ex.Message = "Missing UPS XML Response" Then
                    LogException(ErrorCode.HTTPPostFail, ex)
                Else
                    LogException(ErrorCode.XmlParseError, ex)
                End If
            Catch ex As SqlException
                LogException(ErrorCode.DBUpdate, ex)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub UpdateShipmentInfo_Debug()
            Dim sw As New StreamWriter(".\UPS_Extract.csv", True)

            Dim strDelimeter As String = ","

            sw.WriteLine(shpMnt.TrackingNum & strDelimeter & shpMnt.Status & strDelimeter & shpMnt.ScheduledDeliveryDate & strDelimeter & shpMnt.DeliveredDate & strDelimeter & shpMnt.FedExStatus & strDelimeter & shpMnt.FedExStatusDesc)
            sw.Close()

        End Sub


        Private Function GetDateFromUpsDate(ByVal dateStr As String, Optional ByVal timeStr As String = "") As DateTime
            Dim tempDateTimeStr As String = ""
            tempDateTimeStr = dateStr.Substring(4, 2) & "/" & dateStr.Substring(6, 2) & "/" & dateStr.Substring(0, 4)
            If timeStr.Trim.Length = 6 Then
                tempDateTimeStr &= " " & timeStr.Substring(0, 2) & ":" & timeStr.Substring(2, 2) & ":" & timeStr.Substring(4, 2)
            Else
                tempDateTimeStr &= " 00:00:00"
            End If
            Return tempDateTimeStr
        End Function

        Private Function GetDateFromFedExDate(ByVal dateStr As String, ByVal timeStr As String) As DateTime
            Dim tempDateTimeStr As String = ""
            tempDateTimeStr = dateStr.Substring(5, 2) & "/" & dateStr.Substring(8, 2) & "/" & dateStr.Substring(0, 4)
            If timeStr.Trim.Length = 8 Then
                tempDateTimeStr &= " " & timeStr
            Else
                tempDateTimeStr &= " 00:00:00"
            End If
            Return tempDateTimeStr
        End Function

        Private Function GetNodeValue(ByVal nav As XPath.XPathNodeIterator) As String
            Dim nodeVal As String = ""
            If (nav.MoveNext) Then
                nodeVal = nav.Current.Value
            End If
            Return nodeVal
        End Function

        Private Function SendRequest(ByVal ServiceProvider As String) As String
            Dim blnSuccess As Boolean
            Dim objHttpWebRequest As HttpWebRequest
            Dim objHttpWebResponse As HttpWebResponse
            Dim objStreamReader As StreamReader
            Dim objStreamWriter As StreamWriter


            Dim strActivityDate As String
            Dim strActivityTime As String
            Dim strXMLResponse As String = ""

            Try
                ' ServicePointManager.SecurityProtocol = DirectCast(3072, System.Net.SecurityProtocolType)
                If ServiceProvider = "UPS" Then
                    objHttpWebRequest = CType(WebRequest.Create(AppSetting.UpsXmlPostURL), HttpWebRequest)
                ElseIf ServiceProvider = "FedEx" Then
                    objHttpWebRequest = CType(WebRequest.Create(AppSetting.FedExXmlPostURL), HttpWebRequest)
                End If

                'Configure HttpWebRequest object
                With objHttpWebRequest
                    .ContentType = "application/xml" ' "application/x-www-form-urlencoded"
                    .Method = "POST"
                    .ContentLength = Len(_XmlReq)
                    '.Timeout = 100000 
                    .KeepAlive = False
                    '.Connection = " "
                End With

                objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())

                objStreamWriter.Write(_XmlReq)
                objStreamWriter.Flush()

                'Get Response from UPS
                objHttpWebResponse = CType(objHttpWebRequest.GetResponse(), HttpWebResponse)

                objStreamWriter.Close()
                'Parse through response
                If objHttpWebResponse.StatusCode <> HttpStatusCode.OK Then
                    HttpPostFailCnt += 1
                    Throw New UpsXmlException("Missing UPS/FedEx XML Response")
                Else
                    objStreamReader = New StreamReader(objHttpWebResponse.GetResponseStream(), Encoding.UTF8)
                    strXMLResponse = objStreamReader.ReadToEnd()
                    'SuccessCnt += 1
                End If
            Catch ex As Exception
                Console.WriteLine(ex.ToString())
                Throw New UpsXmlException(ex.Message)
            End Try
            Return strXMLResponse

        End Function

        Public Sub SendMail(ByVal mailType As Int16, Optional ByVal failureReason As String = "")
            Try
                Dim emailMgr As EmailManager = New EmailManager(HotTopic.DCSS.Settings.AppSetting.SmtpServer)
                Dim mailBody, msg As String
                'mail type 1-sucess 2-exception
                Select Case mailType
                    Case 1
                        mailBody = HotTopic.DCSS.Settings.AppSetting.SuccessEmailBody
                    Case 2
                        mailBody = HotTopic.DCSS.Settings.AppSetting.CancellationEmailBody
                End Select

                mailBody = mailBody.Replace("%beginDate%", ProcessBeginDt)
                mailBody = mailBody.Replace("%endDate%", Now)
                mailBody = mailBody.Replace("%totCnt%", TotCnt)
                mailBody = mailBody.Replace("%processCnt%", PrcoessCnt)
                mailBody = mailBody.Replace("%failCnt%", PrcoessCnt - SuccessCnt)
                mailBody = mailBody.Replace("%exception%", failureReason)
                '3/30/06 - Update process log.
                msg = "PROCESS: Batch" & vbNewLine
                msg &= "Begin date: " & ProcessBeginDt & vbNewLine
                msg &= "End date: " & Now & vbNewLine
                msg &= "Total # of records found: " & TotCnt & vbNewLine
                msg &= "Records processed: " & PrcoessCnt & vbNewLine
                msg &= "Records failed: " & PrcoessCnt - SuccessCnt & vbNewLine
                msg &= "Failure reason: " & IIf(failureReason.Trim = "", "N/A", failureReason.Trim) & vbNewLine
                LogMessage(HotTopic.DCSS.Settings.AppSetting.ProcessLog, msg)

                With emailMgr
                    .AddRecipient(HotTopic.DCSS.Settings.AppSetting.DcssMailReceivers)
                    .Sender = HotTopic.DCSS.Settings.AppSetting.SmtpSender
                    .Subject = "DCSS Xml API process alert"
                    .Format = Web.Mail.MailFormat.Html
                    .Priority = Web.Mail.MailPriority.Normal
                    .Send(mailBody)
                End With
            Catch ex As Exception

            End Try
        End Sub

        Public Shared Sub LogMessage(ByVal logFile As String, ByVal msg As String)
            Try
                Dim objStreamWriter As StreamWriter
                'Open the file.
                objStreamWriter = New StreamWriter(logFile, True)
                objStreamWriter.WriteLine("Log Entry Time: " & Now)
                objStreamWriter.WriteLine(msg)
                'Close the file.
                objStreamWriter.Close()
            Catch ex As Exception
                'Throw ex
            End Try
        End Sub

        Public Function GetFedExTrackingDetails()
            Dim objXMLTextReader As XmlTextReader
            Dim objXPathDocument As XPath.XPathDocument
            Dim objXPathNavigator As XPath.XPathNavigator
            Dim objXPathNodeIterator As XPath.XPathNodeIterator

            'Dim _responseStatusCode As String = "0"
            Dim _StatusCode As String = ""
            Dim _StatusDesc As String = ""
            Dim _ErrorCode As String = ""
            Dim _tempDateStr As String = ""
            Dim _tempTimeStr As String = ""
            Dim _DeliveredDate As String = ""
            Dim _schdDeliveryDate As String = ""

            Try
                _XmlReq = ""
                _XmlRes = ""
                _XmlReq = AppSetting.FedExXmlRequest
                _XmlReq = _XmlReq.Replace("%FedExTrackingNbr%", shpMnt.TrackingNum)
                _XmlReq = _XmlReq.Replace("%MeterNumber%", shpMnt.MeterNumber)
                _XmlRes = SendRequest("FedEx")

                PrcoessCnt += 1
                objXMLTextReader = New XmlTextReader(_XmlRes, XmlNodeType.Document, Nothing)
                objXPathDocument = New XPath.XPathDocument(objXMLTextReader)
                objXPathNavigator = objXPathDocument.CreateNavigator()

                Try
                    shpMnt.UpsErrorCode = 0
                    shpMnt.UpsErrorDesc = ""

                    _StatusCode = GetNodeValue(objXPathNavigator.Select("//Package/StatusCode/text()"))
                    _StatusDesc = GetNodeValue(objXPathNavigator.Select("//Package/StatusDescription/text()"))

                    shpMnt.FedExStatus = _StatusCode
                    shpMnt.FedExStatusDesc = _StatusDesc

                    UpdateFedExShipmentInfo()

                    If _StatusCode = "DL" Then
                        SuccessCnt += 1
                    Else
                        _tempDateStr = GetNodeValue(objXPathNavigator.Select("//Package/EstimatedDeliveryDate/text()"))
                        _tempTimeStr = GetNodeValue(objXPathNavigator.Select("//Package/EstimatedDeliveryDate/text()"))

                        If (_tempDateStr.Trim.Length = 10) Then
                            _schdDeliveryDate = GetDateFromFedExDate(_tempDateStr, _tempTimeStr)
                        End If
                        If Not IsDBNull(UtilityManager.DateToNull(CDate(_schdDeliveryDate))) AndAlso (_schdDeliveryDate <> CStr(shpMnt.ScheduledDeliveryDate)) Then
                            shpMnt.ScheduledDeliveryDate = CDate(_schdDeliveryDate)
                            shpMnt.UpdateFlag = True
                        End If

                        If shpMnt.UpdateFlag Then
                            UpdateShipmentInfo()
                        End If

                        SuccessCnt += 1
                    End If
                Catch ex As Exception

                End Try

            Catch ex As FedExXmlException
                If ex.Message = "Missing FedEx XML Response" Then
                    LogException(ErrorCode.HTTPPostFail, ex)
                Else
                    LogException(ErrorCode.XmlParseError, ex)
                End If
            Catch ex As SqlException
                LogException(ErrorCode.DBUpdate, ex)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPurolatorTrackingDetails()
            Dim _tempDateStr As String = ""
            Dim _tempTimeStr As String = ""
            Dim _DeliveredDate As String = ""
            Dim _schdDeliveryDate As String = ""
            Dim shipmentinfo As New PurolatorSvc.ShipmentInfo

            'Dim shipmentinfo As New PWSClient.ShipmentInfo
            'Dim TC As New TrackingClient.PWSClient
            Dim TC As New PurolatorSvc.Service

            TC.Url = System.Configuration.ConfigurationSettings.AppSettings("PurolatorWSURL").ToString()

            Try
                shipmentinfo.PIN = shpMnt.TrackingNum
                PrcoessCnt += 1

                'start - added below code to update the delivery status
                shpMnt.UpsErrorCode = 0
                shpMnt.UpsErrorDesc = ""

                TC.CallTrackPackagesByPin(shipmentinfo)

                shpMnt.FedExStatus = shipmentinfo.ScanType
                shpMnt.FedExStatusDesc = shipmentinfo.Description

                shpMnt.Carrier_PickupFlag = "N"
                If (shipmentinfo.ScanType = Nothing) Then
                    shipmentinfo.ScanType = "Nothing"
                End If
                If (shipmentinfo.ScanType.Trim().Length > 0 AndAlso shipmentinfo.ScanType <> "Nothing") Then
                    SuccessCnt += 1
                    shpMnt.Carrier_PickupFlag = "Y"
                End If

                If shipmentinfo.ScanType.ToLower().Trim() = "delivery" Then
                    _tempDateStr = shipmentinfo.ScanDate
                    _tempTimeStr = shipmentinfo.ScanTime
                    If (_tempDateStr.Trim.Length = 10) Then
                        shpMnt.Carrier_Delivereddate = GetDateFromFedExDate(_tempDateStr, _tempTimeStr)
                    End If
                End If


                'Just to update tracking statuscd, statusdesc
                UpdateFedExShipmentInfo()

                If shipmentinfo.ScanType.ToLower().Trim() = "delivery" Then

                    _tempDateStr = shipmentinfo.ScanDate
                    _tempTimeStr = shipmentinfo.ScanTime

                    If (_tempDateStr.Trim.Length = 10) Then
                        _schdDeliveryDate = GetDateFromFedExDate(_tempDateStr, _tempTimeStr)
                    End If
                    If Not IsDBNull(UtilityManager.DateToNull(CDate(_schdDeliveryDate))) AndAlso (_schdDeliveryDate <> CStr(shpMnt.ScheduledDeliveryDate)) Then
                        shpMnt.ScheduledDeliveryDate = CDate(_schdDeliveryDate)
                        shpMnt.UpdateFlag = True
                    End If

                    If shpMnt.UpdateFlag Then
                        UpdateShipmentInfo()
                    End If
                End If

            Catch ex As SqlException
                LogException(ErrorCode.DBUpdate, ex)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class ' END CLASS DEFINITION ApiManager

End Namespace