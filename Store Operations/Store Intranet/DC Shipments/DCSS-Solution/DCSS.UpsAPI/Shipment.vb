Namespace HotTopic.DCSS.UpsApi

    Public Structure Shipment

        Public StoreNum As Int16

        Public TrackingNum As String

        Public Status As ShipmentStatus

        Public ShippedDate As Date

        Public ScheduledDeliveryDate As Date

        Public DeliveredDate As Date

        Public UpdateFlag As Boolean

        Public UpsErrorCode As Int32

        Public UpsErrorDesc As String

        Public MeterNumber As String

        Public FedExStatus As String

        Public FedExStatusDesc As String

        Public Carrier_PickupFlag As String

        Public Carrier_Delivereddate As String

    End Structure ' END STRUCTURE DEFINITION Shipment
   
End Namespace ' HotTopic.DCSS.UpsApi


