﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

'
'This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
'
Namespace PurolatorSvc
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1099.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Web.Services.WebServiceBindingAttribute(Name:="ServiceSoap", [Namespace]:="http://HotTopic.Purolator/")>  _
    Partial Public Class Service
        Inherits System.Web.Services.Protocols.SoapHttpClientProtocol
        
        Private CallTrackPackagesByPinOperationCompleted As System.Threading.SendOrPostCallback
        
        Private useDefaultCredentialsSetExplicitly As Boolean
        
        '''<remarks/>
        Public Sub New()
            MyBase.New
            Me.Url = Global.My.MySettings.Default.DCSS_UpsAPI_PurolatorSvc_Service
            If (Me.IsLocalFileSystemWebService(Me.Url) = true) Then
                Me.UseDefaultCredentials = true
                Me.useDefaultCredentialsSetExplicitly = false
            Else
                Me.useDefaultCredentialsSetExplicitly = true
            End If
        End Sub
        
        Public Shadows Property Url() As String
            Get
                Return MyBase.Url
            End Get
            Set
                If (((Me.IsLocalFileSystemWebService(MyBase.Url) = true)  _
                            AndAlso (Me.useDefaultCredentialsSetExplicitly = false))  _
                            AndAlso (Me.IsLocalFileSystemWebService(value) = false)) Then
                    MyBase.UseDefaultCredentials = false
                End If
                MyBase.Url = value
            End Set
        End Property
        
        Public Shadows Property UseDefaultCredentials() As Boolean
            Get
                Return MyBase.UseDefaultCredentials
            End Get
            Set
                MyBase.UseDefaultCredentials = value
                Me.useDefaultCredentialsSetExplicitly = true
            End Set
        End Property
        
        '''<remarks/>
        Public Event CallTrackPackagesByPinCompleted As CallTrackPackagesByPinCompletedEventHandler
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://HotTopic.Purolator/CallTrackPackagesByPin", RequestNamespace:="http://HotTopic.Purolator/", ResponseNamespace:="http://HotTopic.Purolator/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Sub CallTrackPackagesByPin(ByRef shipinfo As ShipmentInfo)
            Dim results() As Object = Me.Invoke("CallTrackPackagesByPin", New Object() {shipinfo})
            shipinfo = CType(results(0),ShipmentInfo)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub CallTrackPackagesByPinAsync(ByVal shipinfo As ShipmentInfo)
            Me.CallTrackPackagesByPinAsync(shipinfo, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub CallTrackPackagesByPinAsync(ByVal shipinfo As ShipmentInfo, ByVal userState As Object)
            If (Me.CallTrackPackagesByPinOperationCompleted Is Nothing) Then
                Me.CallTrackPackagesByPinOperationCompleted = AddressOf Me.OnCallTrackPackagesByPinOperationCompleted
            End If
            Me.InvokeAsync("CallTrackPackagesByPin", New Object() {shipinfo}, Me.CallTrackPackagesByPinOperationCompleted, userState)
        End Sub
        
        Private Sub OnCallTrackPackagesByPinOperationCompleted(ByVal arg As Object)
            If (Not (Me.CallTrackPackagesByPinCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent CallTrackPackagesByPinCompleted(Me, New CallTrackPackagesByPinCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        Public Shadows Sub CancelAsync(ByVal userState As Object)
            MyBase.CancelAsync(userState)
        End Sub
        
        Private Function IsLocalFileSystemWebService(ByVal url As String) As Boolean
            If ((url Is Nothing)  _
                        OrElse (url Is String.Empty)) Then
                Return false
            End If
            Dim wsUri As System.Uri = New System.Uri(url)
            If ((wsUri.Port >= 1024)  _
                        AndAlso (String.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) = 0)) Then
                Return true
            End If
            Return false
        End Function
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3163.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://HotTopic.Purolator/")>  _
    Partial Public Class ShipmentInfo
        
        Private scanTypeField As String
        
        Private pINField As String
        
        Private scanDateField As String
        
        Private scanTimeField As String
        
        Private descriptionField As String
        
        Private commentField As String
        
        Private errorCodeField As String
        
        Private errorDescField As String
        
        Private errorAddlInfoField As String
        
        '''<remarks/>
        Public Property ScanType() As String
            Get
                Return Me.scanTypeField
            End Get
            Set
                Me.scanTypeField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property PIN() As String
            Get
                Return Me.pINField
            End Get
            Set
                Me.pINField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property ScanDate() As String
            Get
                Return Me.scanDateField
            End Get
            Set
                Me.scanDateField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property ScanTime() As String
            Get
                Return Me.scanTimeField
            End Get
            Set
                Me.scanTimeField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property Description() As String
            Get
                Return Me.descriptionField
            End Get
            Set
                Me.descriptionField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property Comment() As String
            Get
                Return Me.commentField
            End Get
            Set
                Me.commentField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property ErrorCode() As String
            Get
                Return Me.errorCodeField
            End Get
            Set
                Me.errorCodeField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property ErrorDesc() As String
            Get
                Return Me.errorDescField
            End Get
            Set
                Me.errorDescField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property ErrorAddlInfo() As String
            Get
                Return Me.errorAddlInfoField
            End Get
            Set
                Me.errorAddlInfoField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1099.0")>  _
    Public Delegate Sub CallTrackPackagesByPinCompletedEventHandler(ByVal sender As Object, ByVal e As CallTrackPackagesByPinCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1099.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class CallTrackPackagesByPinCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property shipinfo() As ShipmentInfo
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),ShipmentInfo)
            End Get
        End Property
    End Class
End Namespace
