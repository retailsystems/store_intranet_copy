IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'DCSS_AllHtTorridShipments')
	BEGIN
		PRINT 'Dropping View DCSS_AllHtTorridShipments'
		DROP  View DCSS_AllHtTorridShipments
	END
GO

/******************************************************************************
**		File: DCSS_AllHtTorridShipments.sql
**		Name: DCSS_AllHtTorridShipments
**		Desc: View contains Both HT & Torrid shipments
**
**		This template can be customized:
**              
**
**		Auth: Sri Bajjuri
**		Date: 4/1/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------		------------------------------------------
**    4/16/2004		Sri Bajjuri			Added status description
*******************************************************************************/

PRINT 'Creating View DCSS_AllHtTorridShipments'
GO

CREATE VIEW [dbo].[DCSS_AllHtTorridShipments]
AS
SELECT ups.StoreNum,ShippedLpn,UpsTracking,PackageType,ServiceType,PackageWeight
	,OriginalShippedDateTime,ShippedDateTime,ScheduledDeliveryDate,OriginalScheduledDeliveryDate
	,ups.StatusCD,DeliveredDate,ReceivedBy,ups.UpdateBy,ups.UpdateDate,RunID,Comments
	,IgnoreLateShipmentFlag,ServiceProvider,WHSE,ManifestNbr,KioskFlag,Tracking_StatusCD,Tracking_StatusDesc
	,LTRIM(RTRIM(isNull(RTRIM(emp.FirstName),'') + ' ' + isNull(RTRIM(emp.LastName),''))) as ReceiverName
	,sl.Description as Status, '1234567' as MeterNumber
	FROM dbo.DCSS_UpsShipments ups
	INNER JOIN DCSS_StatusLookUp sl ON ups.StatusCD = sl.StatusCD
	LEFT OUTER JOIN HtSecurity.dbo.UserProfile emp ON ups.ReceivedBy = emp.EmployeeID
UNION
SELECT ups.StoreNum,ShippedLpn,UpsTracking,PackageType,ServiceType,PackageWeight
	,OriginalShippedDateTime,ShippedDateTime,ScheduledDeliveryDate,OriginalScheduledDeliveryDate
	,ups.StatusCD,DeliveredDate,ReceivedBy,ups.UpdateBy,ups.UpdateDate,RunID,Comments
	,IgnoreLateShipmentFlag,ServiceProvider,WHSE,ManifestNbr,KioskFlag,Tracking_StatusCD,Tracking_StatusDesc
	, LTRIM(RTRIM(isNull(RTRIM(emp.FirstName),'') + ' ' + isNull(RTRIM(emp.LastName),''))) as ReceiverName
	,sl.Description as Status, '1234567' as MeterNumber
	FROM Torrid.dbo.DCSS_UpsShipments ups
	INNER JOIN DCSS_StatusLookUp sl ON ups.StatusCD = sl.StatusCD
	LEFT OUTER JOIN HtSecurity.dbo.UserProfile emp ON ups.ReceivedBy = emp.EmployeeID


GO


GRANT SELECT ON DCSS_AllHtTorridShipments TO AppUser

GO

--GRANT SELECT ON HtSecurity_Dev.dbo.UserProfile TO AppUser

--GO