IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_SendMail')
	BEGIN
		PRINT 'Dropping Procedure DCSP_SendMail'
		DROP  Procedure  DCSP_SendMail
	END

GO

PRINT 'Creating Procedure DCSP_SendMail'
GO
CREATE Procedure DCSP_SendMail
	/* Param List */
AS

/******************************************************************************
**		File: 
**		Name: DCSP_SendMail
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

EXEC master.dbo.xp_sendmail @recipients = 'sbajjuri@hottopic.com', 
   @message = 'The master database is full.',   
   @subject = 'Master Database Status'



GO

GRANT EXEC ON DCSP_SendMail TO AppUser

GO
