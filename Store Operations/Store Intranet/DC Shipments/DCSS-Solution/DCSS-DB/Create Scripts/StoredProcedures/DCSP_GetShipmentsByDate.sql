IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_GetShipmentsByDate')
	BEGIN
		PRINT 'Dropping Procedure DCSP_GetShipmentsByDate'
		DROP  Procedure  DCSP_GetShipmentsByDate
	END

GO

PRINT 'Creating Procedure DCSP_GetShipmentsByDate'
GO
CREATE Procedure DCSP_GetShipmentsByDate
	/* Param List */
	@scheduledDeliveryDate datetime
	,@storeNum smallint
AS

/******************************************************************************
**		File: DCSP_GetShipmentsByDate.sql
**		Name: DCSP_GetShipmentsByDate
**		Desc: Returns Shipment details (tracking#,service type,shipdate) for a given day/store.
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------		-------------------------------------------
**     10/26/2004	Sri Bajjuri			Add Kiosk flag to select query
**		01/18/2005	Sri Bajjuri			Add WhseName to select query
**		05/17/2010	Sri Bajjuri			Including SnS order tree node to TrackingStr
*******************************************************************************/
/*
SELECT UpsTracking,ServiceType,ShippedDateTime,PackageType
	,StatusCD,Status,ReceiverName,ServiceProvider
	,IsNull(KioskFlag,0) AS KioskFlag
	,IsNull(WhseName,ups.Whse) AS WhseName
	FROM DCSS_AllHtTorridShipments	ups
	LEFT OUTER JOIN DCSS_WhseConfig wc ON ups.WHSE = wc.WhseID 
	WHERE scheduledDeliveryDate = @scheduledDeliveryDate
	AND storeNum = @storeNum AND StatusCD <> 6
	Order BY StatusCD,ShippedDateTime,UpsTracking
*/
	
SELECT UpsTracking,ServiceType,ShippedDateTime,PackageType
	,ups.StatusCD,sl.Description as Status,ServiceProvider
	,IsNull(KioskFlag,0) AS KioskFlag
	,IsNull(WhseName,ups.Whse) AS WhseName
	,[dbo].[DCSFN_BuildTrackHtmlStr] (UpsTracking,ServiceProvider) as TrackingStr
	FROM DCSS_UPSShipments ups
	INNER JOIN DCSS_StatusLookUp sl ON ups.StatusCD = sl.StatusCD
	LEFT OUTER JOIN Hottopic2.DBO.DCSS_WhseConfig wc ON ups.WHSE = wc.WhseID 
	WHERE scheduledDeliveryDate = @scheduledDeliveryDate
	AND storeNum = @storeNum AND ups.StatusCD <> 6
	Order BY ups.StatusCD,ShippedDateTime,UpsTracking
GO

GRANT EXEC ON DCSP_GetShipmentsByDate TO AppUser

GO
