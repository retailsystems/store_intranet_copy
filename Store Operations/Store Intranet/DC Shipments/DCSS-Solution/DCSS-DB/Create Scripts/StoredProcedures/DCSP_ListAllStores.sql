IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ListAllStores')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ListAllStores'
		DROP  Procedure  DCSP_ListAllStores
	END

GO

PRINT 'Creating Procedure DCSP_ListAllStores'
GO
CREATE Procedure DCSP_ListAllStores
	/* Param List */
AS

/******************************************************************************
**		File: DCSP_ListAllStores.sql
**		Name: DCSP_ListAllStores
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 2/4/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT StoreNum, StoreName FROM Store
	WHERE (Deleted is Null OR Deleted =0) AND StagingStore <> 1 AND isNull(Region,0) <> 90
UNION
SELECT StoreNum, StoreName FROM Torrid_Dev.DBO.Store
	WHERE (Deleted is Null OR Deleted =0) AND StagingStore <> 1 AND isNull(Region,0) <> 90
ORDER BY StoreNum


GO

GRANT EXEC ON DCSP_ListAllStores TO AppUser

GO
