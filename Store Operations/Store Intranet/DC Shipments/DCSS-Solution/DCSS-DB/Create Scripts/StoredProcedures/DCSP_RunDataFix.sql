IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_RunDataFix')
	BEGIN
		PRINT 'Dropping Procedure DCSP_RunDataFix'
		DROP  Procedure  DCSP_RunDataFix
	END

GO

PRINT 'Creating Procedure DCSP_RunDataFix'
GO
CREATE Procedure DCSP_RunDataFix
	/* Param List */
	@UserID varchar(20)
AS

/******************************************************************************
**		File: DCSP_RunDataFix.sql
**		Name: DCSP_RunDataFix
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 11/10/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
DECLARE  @sqlStr varchar(1000), @UpsTracking varchar(30)
DECLARE @lpn varchar(20),@udf2 varchar(35), @storeNum int, @charInd int, @storeCheckOk bit
DECLARE @mailRecipients varchar(100), @msg varchar(500)
DECLARE @runID int, @statusFl int
SET @mailRecipients = 'sbajjuri@hottopic.com'
--Set RunID to 0 for DataFix Run
SET @runID = 0


--TRUNCATE Table DCSS_UPSShipMents_History
DELETE FROM DCSS_UPSShipMents_History WHERE runID  = @runID

/* This a test sql query
INSERT INTO DCSS_UPSShipMents_History (RunID,OriginalStoreNum,ShippedLpn,UpsTracking
			,PackageType,ServiceType,PackageWeight,OriginalShippedDateTime
			,ShippedDateTime,UDF1,UDF2,UDF3,UDF4)
			SELECT @runID,StoreNum,ShippedLpn,UpsTracking,PackageType,ServiceType
			,PackageWeight,OriginalShippedDateTime,ShippedDateTime,UDF1,UDF2,UDF3,UDF4
			FROM DCSS_UpsDataFix 
*/	
--Build a sql string to query DOVTREE
	
	
	
	SET @sqlStr =   'INSERT INTO DCSS_UPSShipMents_History (RunID,OriginalStoreNum,ShippedLpn,UpsTracking	,PackageType,ServiceType,PackageWeight,OriginalShippedDateTime,ShippedDateTime)' +	
			' SELECT ' + convert(varchar,@runID) + ',dov.STORE_NUM,dov.Shipped_Lpn, dov.Ups_Tracking,dov.Package_Type, dov.Service_Type, dov.Package_Weight, ' +
			'  dov.Shipped_Date_Time, dov.Post_Date_Time  FROM OPENQUERY( dovtree,' +
			' '' ' + 'SELECT STORE_NUM,Shipped_Lpn,Ups_Tracking,Package_Type,Service_Type, Package_Weight, Shipped_Date_Time,Post_Date_Time ' + 
			' FROM intranet_exception_upload   '' ) AS dov '
	
	
	EXEC(@sqlStr)
	
	
	UPDATE DCSS_UPSShipMents_History SET StoreNum = OriginalStoreNum	
		WHERE  OriginalStoreNum Between '0001' AND '9999' AND runID  = @runID
	
	
	CREATE TABLE #temp1
	(StoreNum int)
	
	DECLARE UpsCur CURSOR FOR SELECT ISNULL(ShippedLpn,''), UpsTracking
		FROM DCSS_UPSShipMents_History WHERE StoreNum is null AND runID  = @runID
	OPEN UpsCur
	FETCH NEXT FROM UpsCur INTO @lpn,@UpsTracking
		
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @storeNum = null
			SET @charInd = 0
			SET @storeCheckOk = 0
			/*
			IF (@storeCheckOk = 0)
			BEGIN
			
			--Parse UDF2
				SELECT @charInd = CHARINDEX('#',@UDF2)
			IF (@charInd > 0)
				BEGIN				
					IF isNumeric(ltrim(rtrim(right(@UDF2,len(@UDF2) - @charInd)))) = 1 
						BEGIN
							SELECT @storeNum = ltrim(rtrim(right(@UDF2,len(@UDF2) - @charInd)))
							SET @storeCheckOk = 1
						END
				END
			END
			*/
			IF (@storeCheckOk = 0)
			BEGIN
				IF IsNumeric(@lpn) = 1
				BEGIN
					Delete from #temp1
					-- Try to get it from dovtree table cartonpush
					SET @sqlStr = 'INSERT INTO #temp1 SELECT top 1 Push_Store FROM OPENQUERY( dovtree,' +
								' '' ' + 'SELECT Push_Store FROM CARTONPUSH WHERE Push_Lpn = ''''' + @lpn + ''''' '')'  
						
					EXEC(@sqlStr)
					SELECT @storeNum = StoreNum From #temp1 
					IF (@storeNum is NOT NULL)
						SET @storeCheckOk = 1
				END	
			END
			IF (@storeCheckOk = 1)
				BEGIN
					UPDATE DCSS_UPSShipMents_History SET StoreNum = @storeNum WHERE UpsTracking = @UpsTracking
				END
			FETCH NEXT FROM UpsCur INTO @lpn,@UpsTracking
		END
	CLOSE UpsCur
	DEALLOCATE UpsCur
	DROP TABLE #temp1
	--Exceptions
	-- 1.Missing StoreNum, 2-Duplicate upstracking #, 3-Travel time error
	UPDATE DCSS_UPSShipMents_History
		SET ExceptionTypeID = 1
		WHERE StoreNum is NULL AND RunID = @runID
	UPDATE DCSS_UPSShipMents_History
		SET ExceptionTypeID = 2
		WHERE UpsTracking in 
			(SELECT UpsTracking FROM DCSS_UPSShipMents_History
				WHERE RunID = @runID Group BY UpsTracking HAVING count(*) > 1)
	
	IF @@error <> 0
		BEGIN
			--RAISERROR('Failed to export data',16,3)
			--Return -1
			SET @msg= 'Failed to export data'
			GOTO Error_Para
		END
		
	BEGIN TRAN
	--Populate hottopic shipments
	INSERT INTO DCSS_UPSShipMents (StoreNum,ShippedLpn,UpsTracking,PackageType,ServiceType,PackageWeight,OriginalShippedDateTime,ShippedDateTime,RunID,UpdateBy,UpdateDate)
		SELECT UPH.StoreNum,UPH.ShippedLpn,UPH.UpsTracking,UPH.PackageType,UPH.ServiceType
		,UPH.PackageWeight,UPH.OriginalShippedDateTime,UPH.ShippedDateTime,UPH.RunID, @userID, getDate()
		FROM DCSS_UPSShipMents_History UPH INNER JOIN Store ON UPH.StoreNum = Store.StoreNum
		LEFT OUTER JOIN DCSS_UPSShipMents UPS ON UPH.UpsTracking = UPS.UpsTracking 
		--AND UPH.RunID = UPS.RunID		
		WHERE UPS.UpsTracking IS NULL AND ExceptionTypeID = 0 AND UPH.RunID = @runID 
	--Populate torrid shipments
	INSERT INTO Torrid_Dev.dbo.DCSS_UPSShipMents (StoreNum,ShippedLpn,UpsTracking,PackageType,ServiceType,PackageWeight,OriginalShippedDateTime,ShippedDateTime,RunID,UpdateBy,UpdateDate)
		SELECT UPH.StoreNum,UPH.ShippedLpn,UPH.UpsTracking,UPH.PackageType,UPH.ServiceType
		,UPH.PackageWeight,UPH.OriginalShippedDateTime,UPH.ShippedDateTime,UPH.RunID, @userID, getDate()
		FROM DCSS_UPSShipMents_History UPH INNER JOIN Torrid_Dev.dbo.Store Store ON UPH.StoreNum = Store.StoreNum
		LEFT OUTER JOIN Torrid_Dev.dbo.DCSS_UPSShipMents UPS ON UPH.UpsTracking = UPS.UpsTracking 
		--AND UPH.RunID = UPS.RunID		
		WHERE UPS.UpsTracking IS NULL AND ExceptionTypeID = 0 AND UPH.RunID = @runID 		
	
	IF @@error <> 0
		BEGIN
			ROLLBACK TRAN
			--RAISERROR('Failed to export data',16,3)
			SET @msg= 'Failed to export data'
			GOTO Error_Para
		END
	ELSE
		COMMIT TRAN	
	
	IF EXISTS(SELECT RunID FROM DCSS_UPSShipMents_History WHERE RunID = @runID )
	BEGIN
		--Update scheduled delivery date		
		EXEC DCSP_UpdateDeliveryTime
		EXEC Torrid_Dev.dbo.DCSP_UpdateDeliveryTime
		IF @@error <> 0
			BEGIN
				set @msg = 'Failed to update scheduled delivery date for shipments'
				GOTO Error_Para			
			END	
	END
		
	--  3-Travel time exception
	UPDATE DCSS_UPSShipMents_History
		SET ExceptionTypeID = 3
		FROM DCSS_UPSShipMents_History UPH, DCSS_UPSShipMents UPS
		WHERE UPH.UpsTracking = UPS.UpsTracking AND UPH.RunID = @runID AND ExceptionTypeID = 0
		AND UPS.ScheduledDeliveryDate IS NULL
		
	UPDATE DCSS_UPSShipMents_History
		SET ExceptionTypeID = 3
		FROM DCSS_UPSShipMents_History UPH, Torrid_Dev.dbo.DCSS_UPSShipMents UPS
		WHERE UPH.UpsTracking = UPS.UpsTracking AND UPH.RunID = @runID AND ExceptionTypeID = 0
		AND UPS.ScheduledDeliveryDate IS NULL
	
		
	DELETE FROM DCSS_UPSShipMents_History WHERE ExceptionTypeID = 0 AND RunID = @runID	
	
	
	--Send exceptions E-MAIL
	IF exists(select UpsTracking from DCSS_UPSShipMents_History WHERE ExceptionTypeID > 0 AND RunID = @runID	)
	BEGIN
		SET @sqlStr = 'SELECT RunID,UpsTracking, EXP.Description as Error_Msg '+
			' FROM HotTopic2_Dev.dbo.DCSS_UPSShipMents_History UPH ' +
			' INNER JOIN HotTopic2_Dev.dbo.DCSS_ExceptionTypes EXP ON UPH.ExceptionTypeID = EXP.ExceptionTypeID ' +
			' WHERE UPH.ExceptionTypeID > 0 AND RunID =' + convert(varchar,@runID)
		EXEC master.dbo.xp_sendmail @recipients = @mailRecipients, 
		@query = @sqlStr,
		@subject = 'DC Shipments exceptions ',
		@message = 'Document attached',
		@attach_results = 'TRUE', @width = 800
		IF @@error <> 0
			BEGIN			
				RAISERROR('Failed to deliver  exception email',16,3)
			END
	END
	
	RETURN 0
	
Error_Para:
BEGIN
	IF (@runID IS NOT NULL)
	BEGIN
		IF (@runID = 0)
			SET @msg = @msg + ' Job Run #: Data Fix' 
		ELSE
			SET @msg = @msg + ' Job Run #:' + convert(varchar,@runID)
	END
	EXEC master.dbo.xp_sendmail @recipients = @mailRecipients, 
			@subject = 'DC Shipments Alert',
			@message = @msg
	
	RAISERROR(@msg,16,3)	
	Return -1	
END


GO

GRANT EXEC ON DCSP_RunDataFix TO AppUser

GO
