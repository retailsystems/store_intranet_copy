IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_GetStoreMonthlyShipments')
	BEGIN
		PRINT 'Dropping Procedure DCSP_GetStoreMonthlyShipments'
		DROP  Procedure  DCSP_GetStoreMonthlyShipments
	END

GO

PRINT 'Creating Procedure DCSP_GetStoreMonthlyShipments'
GO
CREATE Procedure DCSP_GetStoreMonthlyShipments
	/* Param List */
	@mm smallint
	,@yy smallint
	,@storeNum smallint
AS

/******************************************************************************
**		File: DCSP_GetStoreMonthlyShipments.sql
**		Name: DCSP_GetStoreMonthlyShipments
**		Desc: Returns daywise count of shipments made to a store for a given month
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    11/18/2004	Sri Bajjuri			Hide the shipments from store view if the status 
**											set to exception
*******************************************************************************/
/*
SELECT COUNT(*) AS cnt,scheduledDeliveryDate,StatusCd FROM DCSS_UPSShipments 
	WHERE DATEPART(m,scheduledDeliveryDate) = @mm 
	AND DATEPART(yy,scheduledDeliveryDate) = @yy
	AND storeNum = @storeNum
	AND StatusCd <> 6
	GROUP BY scheduledDeliveryDate,StatusCd
	ORDER BY scheduledDeliveryDate,StatusCd
*/
	
SELECT COUNT(distinct A.UpsTracking) AS cnt,COUNT(distinct B.TrackingNum) AS SnSCnt
	,scheduledDeliveryDate,StatusCd 
	FROM DCSS_UPSShipments A
	LEFT OUTER JOIN dbo.DCSS_SnSOrders B ON A.UpsTracking = B.TrackingNum
	WHERE DATEPART(m,A.scheduledDeliveryDate) = @mm 
	AND DATEPART(yy,A.scheduledDeliveryDate) = @yy
	AND A.storeNum = @storeNum
	AND A.StatusCd <> 6
	GROUP BY A.scheduledDeliveryDate,A.StatusCd
	ORDER BY A.scheduledDeliveryDate,A.StatusCd
	
GO

GRANT EXEC ON DCSP_GetStoreMonthlyShipments TO AppUser

GO
