IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_AddManifestDate')
	BEGIN
		PRINT 'Dropping Procedure DCSP_AddManifestDate'
		DROP  Procedure  DCSP_AddManifestDate
	END

GO

PRINT 'Creating Procedure DCSP_AddManifestDate'
GO
CREATE Procedure DCSP_AddManifestDate
	/* Param List */
	@ManifestNumber varchar(50)
	,@ServiceProvider int
	,@whse int
	,@CloseDate smalldatetime
	,@userID varchar(50)
	
AS

/******************************************************************************
**		File: DCSP_AddManifestDate.sql
**		Name: DCSP_AddManifestDate
**		Desc: Saves Manfiest data in DCSS_ManifestCloseDate
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 7/29/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

INSERT INTO DCSS_ManifestCloseDate
	(
	ManifestNumber
	,ServiceProvider
	,whse
	,CloseDate
	,updateby
	,updatedate
	)
VALUES
	(
	@ManifestNumber
	,@ServiceProvider
	,@whse
	,@CloseDate
	,@userID 
	,getdate()
	)
IF @@error <> 0
	BEGIN			
		RAISERROR('Failed to save manifest info',16,3)		
	END
GO

GRANT EXEC ON DCSP_AddManifestDate TO AppUser

GO
