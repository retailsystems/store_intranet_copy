IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_SendShipmentEmail')
	BEGIN
		PRINT 'Dropping Procedure DCSP_SendShipmentEmail'
		DROP  Procedure  DCSP_SendShipmentEmail
	END

GO

PRINT 'Creating Procedure DCSP_SendShipmentEmail'
GO
CREATE Procedure DCSP_SendShipmentEmail
	/* Param List */
AS

/******************************************************************************
**		File: DCSP_SendShipmentEmail.sql
**		Name: DCSP_SendShipmentEmail
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 12/23/2004
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
DECLARE @ManifestNbr varchar(30), @NumRecords int, @EmailQueueID int
DECLARE @msg nvarchar(500),@mailRecipients varchar(500)

SET @mailRecipients = 'sbajjuri@hottopic.com;'

DECLARE ManifestCur CURSOR FOR SELECT EmailQueueID,ManifestNbr, NumRecords
		FROM DCSS_Email_Queue WHERE isNull(EmailSentFlag,0) = 0
OPEN ManifestCur
FETCH NEXT FROM ManifestCur INTO @EmailQueueID, @ManifestNbr, @NumRecords
	
WHILE @@FETCH_STATUS = 0
	BEGIN
		set @msg = convert(varchar,@NumRecords) + ' Carton(s) data exported to DCSS for Manifest/Load # ' + @ManifestNbr 
		EXEC master.dbo.xp_sendmail @recipients = @mailRecipients			
		,@subject = 'DCSS Alert'
		,@message = @msg
		,@set_user = 'ntdomain\dev_intranet'
		UPDATE DCSS_Email_Queue SET 
			EmailSentFlag = 1
			,EmailSentDate = getDate()
		WHERE EmailQueueID = @EmailQueueID
		/*
		IF @@ERROR <> 0
			BEGIN
			INSERT INTO DCSS_ERROR_LOG(MsgSource,MsgDesc)
				VALUES('DSCP_AddShipment','Failed to deliver email :' + @msg)
				RETURN(0)
			END
		*/
		FETCH NEXT FROM ManifestCur INTO @EmailQueueID, @ManifestNbr, @NumRecords
	END
CLOSE ManifestCur
DEALLOCATE ManifestCur

GO

GRANT EXEC ON DCSP_SendShipmentEmail TO AppUser

GO
