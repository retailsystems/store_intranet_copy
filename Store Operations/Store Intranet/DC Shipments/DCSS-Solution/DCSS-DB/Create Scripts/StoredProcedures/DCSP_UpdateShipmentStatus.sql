IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_UpdateShipmentStatus')
	BEGIN
		PRINT 'Dropping Procedure DCSP_UpdateShipmentStatus'
		DROP  Procedure  DCSP_UpdateShipmentStatus
	END

GO

PRINT 'Creating Procedure DCSP_UpdateShipmentStatus'
GO
CREATE Procedure DCSP_UpdateShipmentStatus
	/* Param List */
	@UpsTracking varchar(30)
	,@StatusCD smallint
	,@UserID varchar(20)
	,@ScheduledDeliveryDate datetime  = NULL
	,@DeliveredDate datetime  = NULL
	,@UpsErrorCode int = NULL
	,@UpsErrorDesc varchar(500) = NULL
AS

/******************************************************************************
**		File: 
**		Name: DCSP_UpdateShipmentStatus
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    3/30/04		Sri					New param deliveredDate added 
*******************************************************************************/
IF @ScheduledDeliveryDate IS Null 
		SELECT @ScheduledDeliveryDate = ScheduledDeliveryDate
		FROM DCSS_UpsShipments WHERE UpsTracking = @UpsTracking
		
IF @statusCd = 3  --received	
	BEGIN		
	IF @DeliveredDate IS Null 
		SET @DeliveredDate = getDate()
		
	UPDATE DCSS_UpsShipments
		SET StatusCd = @statusCd 
		,ReceivedBy = @UserID
		,DeliveredDate = @DeliveredDate
		,UpdateBy = @UserID
		,UpdateDate = getDate()
		WHERE UpsTracking = @UpsTracking	
	END
ELSE
	UPDATE DCSS_UpsShipments
		SET StatusCd = @statusCd 
		,ReceivedBy = NULL
		,ScheduledDeliveryDate = @ScheduledDeliveryDate
		,DeliveredDate = @DeliveredDate
		,UpdateBy = @UserID
		,UpdateDate = getDate()
		--,UpsErrorCode = @UpsErrorCode
		--,UpsErrorDesc =@UpsErrorDesc
		WHERE UpsTracking = @UpsTracking

IF @@error <> 0
	RAISERROR('Failed to update shipment status, Try again later',16,3)
GO

GRANT EXEC ON DCSP_UpdateShipmentStatus TO AppUser

GO
