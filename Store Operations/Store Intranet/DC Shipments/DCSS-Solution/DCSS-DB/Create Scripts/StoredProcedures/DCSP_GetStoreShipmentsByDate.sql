IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_GetStoreShipmentsByDate')
	BEGIN
		PRINT 'Dropping Procedure DCSP_GetStoreShipmentsByDate'
		DROP  Procedure  DCSP_GetStoreShipmentsByDate
	END

GO

PRINT 'Creating Procedure DCSP_GetStoreShipmentsByDate'
GO
CREATE Procedure DCSP_GetStoreShipmentsByDate
	/* Param List */
	@scheduledDeliveryDate datetime
	,@storeNum smallint
AS

/******************************************************************************
**		File: DCSP_GetStoreShipmentsByDate.sql
**		Name: DCSP_GetStoreShipmentsByDate
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------		-------------------------------------------
**      4/16/2004	Sri Bajjuri			Fetching the data from view
**     10/26/2004	Sri Bajjuri			Add Kiosk flag to select query
**      11/18/2004 	Sri Bajjuri	Hide shipments with status "Exception"
**		01/17/2005	Sri Bajjuri		WhseName added to resultset
*******************************************************************************/
/*
SELECT UpsTracking,ServiceType,ShippedDateTime,PackageType
	,StatusCD,Status,ReceiverName,ServiceProvider
	,IsNull(KioskFlag,0) AS KioskFlag
	,IsNull(WhseName,ups.Whse) AS WhseName
	FROM DCSS_AllHtTorridShipments	ups
	LEFT OUTER JOIN DCSS_WhseConfig wc ON ups.WHSE = wc.WhseID 
	WHERE scheduledDeliveryDate = @scheduledDeliveryDate
	AND storeNum = @storeNum AND StatusCD <> 6
	Order BY StatusCD,ShippedDateTime,UpsTracking
*/		

SELECT UpsTracking,ServiceType,ShippedDateTime,PackageType
	,StatusCD,Status,ReceiverName,ServiceProvider
	,IsNull(KioskFlag,0) AS KioskFlag
	,IsNull(WhseName,ups.Whse) AS WhseName
	,[dbo].[DCSFN_BuildTrackHtmlStr] (UpsTracking,ServiceProvider) as TrackingStr
	FROM DCSS_AllHtTorridShipments	ups
	LEFT OUTER JOIN DCSS_WhseConfig wc ON ups.WHSE = wc.WhseID 
	WHERE scheduledDeliveryDate = @scheduledDeliveryDate
	AND storeNum = @storeNum AND StatusCD <> 6
	Order BY StatusCD,ShippedDateTime,UpsTracking
	
GO

GRANT EXEC ON DCSP_GetStoreShipmentsByDate TO AppUser

GO
