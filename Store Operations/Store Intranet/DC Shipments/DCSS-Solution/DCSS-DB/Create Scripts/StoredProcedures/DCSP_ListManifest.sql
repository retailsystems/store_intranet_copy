IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ListManifest')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ListManifest'
		DROP  Procedure  DCSP_ListManifest
	END

GO

PRINT 'Creating Procedure DCSP_ListManifest'
GO
CREATE Procedure DCSP_ListManifest
	/* Param List */
AS

/******************************************************************************
**		File: 
**		Name: DCSP_ListManifest
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT TOP 30 ManifestID, ManifestNumber, WHSE, CloseDate
	,  Case ServiceProvider 
		WHEN 1 then 'UPS' 
		WHEN 2 THEN 'LYNDEN' 
		ELSE 'Others' end as ServiceProviderName
	FROM DCSS_ManifestCloseDate Order By CloseDate DESC,ManifestID DESC

GO

GRANT EXEC ON DCSP_ListManifest TO AppUser

GO
