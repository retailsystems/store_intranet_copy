 IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_LoadSNSOrders')
	BEGIN
		PRINT 'Dropping Procedure DCSP_LoadSNSOrders'
		DROP  Procedure  DCSP_LoadSNSOrders
	END

GO

PRINT 'Creating Procedure DCSP_LoadSNSOrders'
GO
CREATE Procedure DCSP_LoadSNSOrders
	/* Param List */
AS

/******************************************************************************
**		File: DCSP_LoadSNSOrders.sql
**		Name: DCSP_LoadSNSOrders
**		Desc: Used by DCSS_SnSETL.dtsx
**
**		This template can be customized:
**              
**		Return values: 
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 5/12/2010
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

--update existing data

--insert new records
-- torrid
INSERT INTO Torrid.dbo.DCSS_SnSOrders (TrackingNum,EcomOrderNum,StoreNum,CartonNum)
SELECT DISTINCT S.TrackingNum,S.EcomOrderNum,S.StoreNum,S.CartonNum 
	FROM [dbo].[DCSS_BASE_SnSOrders] S 
	LEFT OUTER JOIN dbo.DCSS_SnSOrders T ON S.TrackingNum = T.TrackingNum AND S.EcomOrderNum = T.EcomOrderNum 
	WHERE T.EcomOrderNum IS NULL AND S.StoreNum between 5000 AND 6000

-- Hottopic
INSERT INTO HotTopic2.dbo.DCSS_SnSOrders (TrackingNum,EcomOrderNum,StoreNum,CartonNum)
SELECT DISTINCT S.TrackingNum,S.EcomOrderNum,S.StoreNum,S.CartonNum 
	FROM [dbo].[DCSS_BASE_SnSOrders] S 
	LEFT OUTER JOIN dbo.DCSS_SnSOrders T ON S.TrackingNum = T.TrackingNum AND S.EcomOrderNum = T.EcomOrderNum
	WHERE T.EcomOrderNum IS NULL AND NOT (S.StoreNum between 5000 AND 6000)

GO

GRANT EXEC ON DCSP_LoadSNSOrders TO AppUser

GO
