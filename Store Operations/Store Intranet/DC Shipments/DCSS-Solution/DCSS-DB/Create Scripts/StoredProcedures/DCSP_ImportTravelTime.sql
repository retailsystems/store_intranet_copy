IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ImportTravelTime')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ImportTravelTime'
		DROP  Procedure  DCSP_ImportTravelTime
	END

GO

PRINT 'Creating Procedure DCSP_ImportTravelTime'
GO
CREATE Procedure DCSP_ImportTravelTime
	/* Param List */
AS

/******************************************************************************
**		File: DCSP_ImportTravelTime.sql
**		Name: DCSP_ImportTravelTime
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   Scheduled job to import ups travel time from dovtree
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 3/12/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    4/19/04		Sri Bajjuri			Fetch & Populate Travel time column data only
*******************************************************************************/
--BEGIN TRAN
TRUNCATE TABLE DCSS_UPSTravelTime

INSERT INTO DCSS_UPSTravelTime 
SELECT * FROM OPENQUERY(dovtree, 
	'SELECT 
	STORE_NUM	
	,SERVICE_TYPE
	,GROUND_NEXTDAY_FLAG	
	,NEXT_TRAVEL
	,NEXTAM_TRAVEL
	,NEXTSAVER_TRAVEL
	,GROUND_TRAVEL
	,DAY2_TRAVEL
	,DAY2AM_TRAVEL
	,DAY3_TRAVEL
	FROM UPS_address WHERE STORE_NUM IS NOT NULL')

IF @@error <> 0
	BEGIN
		--ROLLBACK TRAN
		RAISERROR('Failed to import ups travel time data',16,3)		
	END
--ELSE
	--COMMIT TRAN	

GO

GRANT EXEC ON DCSP_ImportTravelTime TO AppUser

GO
