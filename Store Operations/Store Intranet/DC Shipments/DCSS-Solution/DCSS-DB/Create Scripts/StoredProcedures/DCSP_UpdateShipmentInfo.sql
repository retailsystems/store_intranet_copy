IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_UpdateShipmentInfo')
	BEGIN
		PRINT 'Dropping Procedure DCSP_UpdateShipmentInfo'
		DROP  Procedure  DCSP_UpdateShipmentInfo
	END

GO

PRINT 'Creating Procedure DCSP_UpdateShipmentInfo'
GO
CREATE Procedure DCSP_UpdateShipmentInfo
	/* Param List */
	@UpsTracking varchar(30)	
	,@StatusCD smallint
	,@Comments nvarchar(2000)
	,@UserID varchar(20)
	,@ScheduledDeliveryDate datetime = NULL
	,@DeliveredDate datetime = NULL
	,@UpdateFlag bit = 0
	,@UpsErrorCode int = NULL
	,@UpsErrorDesc varchar(500) = NULL
	
	
AS

/******************************************************************************
**		File: DCSP_UpdateShipmentInfo.sql
**		Name: DCSP_UpdateShipmentInfo
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by: PendingShipments.aspx (admin module)  
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 1/8/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
--CHECK IF Exists in HotTopic Database
IF EXISTS(SELECT UpsTracking FROM DCSS_UpsShipments WHERE UpsTracking = @UpsTracking ) 
	--HotTopic 
	BEGIN
		IF len(@Comments) > 0
			UPDATE DCSS_UpsShipments
			SET Comments = @Comments 
			,UpdateBy = @UserID
			,UpdateDate = getDate()
			WHERE UpsTracking = @UpsTracking
		IF (@StatusCD > 0 OR @UpdateFlag = 1)
			EXEC DCSP_UpdateShipmentStatus @UpsTracking,@StatusCD,@UserID,@ScheduledDeliveryDate,@DeliveredDate, @UpsErrorCode ,@UpsErrorDesc
	END
ELSE --Torrid	
	BEGIN
		IF len(@Comments) > 0
			UPDATE Torrid.DBO.DCSS_UpsShipments
			SET Comments = @Comments 
			,UpdateBy = @UserID
			,UpdateDate = getDate()
			WHERE UpsTracking = @UpsTracking
		IF (@StatusCD > 0 OR @UpdateFlag = 1)
			EXEC Torrid.DBO.DCSP_UpdateShipmentStatus @UpsTracking,@StatusCD,@UserID,@ScheduledDeliveryDate,@DeliveredDate, @UpsErrorCode ,@UpsErrorDesc
	END



GO

GRANT EXEC ON DCSP_UpdateShipmentInfo TO AppUser

GO
