IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_UpdateDeliveryTime')
	BEGIN
		PRINT 'Dropping Procedure DCSP_UpdateDeliveryTime'
		DROP  Procedure  DCSP_UpdateDeliveryTime
	END

GO

PRINT 'Creating Procedure DCSP_UpdateDeliveryTime'
GO
CREATE Procedure DCSP_UpdateDeliveryTime
	/* Param List */	
AS

/******************************************************************************
**		File: DCSP_UpdateDeliveryTime.sql
**		Name: DCSP_UpdateDeliveryTime
**		Desc: Sets the scheduled delivery time based on "Travel Time" matrix table.
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

CREATE TABLE #tempUpsAddress
	(
	Store_Num int
	,NEXT_TRAVEL int
	,NEXTAM_TRAVEL int
	,NEXTSAVER_TRAVEL int
	,DAY2_TRAVEL int
	,DAY2AM_TRAVEL int
	,DAY3_TRAVEL int
	,GROUND_TRAVEL int 	 
	)

INSERT INTO #tempUpsAddress select 
Store_Num
,isNull(NEXT_TRAVEL,1)
,isNull(NEXTAM_TRAVEL,1) 
,isNull(NEXTSAVER_TRAVEL,1)
,isNull(DAY2_TRAVEL,2)
,isNull(DAY2AM_TRAVEL,2)
,isNull(DAY3_TRAVEL,3)
,isNull(GROUND_TRAVEL,5)
from openquery(dovtree, 'SELECT Store_Num,NEXT_TRAVEL,NEXTAM_TRAVEL,
NEXTSAVER_TRAVEL,DAY2_TRAVEL,DAY2AM_TRAVEL,DAY3_TRAVEL,GROUND_TRAVEL
 FROM UPS_address')


UPDATE DCSS_UpsShipments SET ScheduledDeliveryDate = 
	dbo.DCSFN_GetDeliveryDate (ShippedDateTime , NEXT_TRAVEL) FROM 
	DCSS_UpsShipments  JOIN #tempUpsAddress 
	ON StoreNum = Store_Num AND ServiceType = 'Next Day Air'
	AND ScheduledDeliveryDate IS NULL
	
UPDATE DCSS_UpsShipments SET ScheduledDeliveryDate = 
	dbo.DCSFN_GetDeliveryDate (ShippedDateTime , NEXTAM_TRAVEL) FROM 
	DCSS_UpsShipments  JOIN #tempUpsAddress 
	ON StoreNum = Store_Num AND ServiceType = 'Next Day Air Early AM'  AND ScheduledDeliveryDate IS NULL
	
UPDATE DCSS_UpsShipments SET ScheduledDeliveryDate = 
	dbo.DCSFN_GetDeliveryDate (ShippedDateTime , NEXTSAVER_TRAVEL) FROM 
	DCSS_UpsShipments  JOIN #tempUpsAddress 
	ON StoreNum = Store_Num AND ServiceType = 'Next Day Air Saver'  AND ScheduledDeliveryDate IS NULL
	
UPDATE DCSS_UpsShipments SET ScheduledDeliveryDate = 
	dbo.DCSFN_GetDeliveryDate (ShippedDateTime , DAY2_TRAVEL)  FROM 
	DCSS_UpsShipments  JOIN #tempUpsAddress 
	ON StoreNum = Store_Num AND ServiceType = '2ND Day Air'  AND ScheduledDeliveryDate IS NULL
	
UPDATE DCSS_UpsShipments SET ScheduledDeliveryDate = 
	dbo.DCSFN_GetDeliveryDate (ShippedDateTime , DAY2AM_TRAVEL) FROM 
	DCSS_UpsShipments  JOIN #tempUpsAddress 
	ON StoreNum = Store_Num AND ServiceType = '2ND Day Air AM' AND ScheduledDeliveryDate IS NULL
	
UPDATE DCSS_UpsShipments SET ScheduledDeliveryDate = 
	dbo.DCSFN_GetDeliveryDate (ShippedDateTime , DAY3_TRAVEL)  FROM 
	DCSS_UpsShipments  JOIN #tempUpsAddress 
	ON StoreNum = Store_Num AND ServiceType = '3 Day Select' AND ScheduledDeliveryDate IS NULL

UPDATE DCSS_UpsShipments SET ScheduledDeliveryDate = 
	dbo.DCSFN_GetDeliveryDate (ShippedDateTime , GROUND_TRAVEL) FROM 
	DCSS_UpsShipments  JOIN #tempUpsAddress 
	ON StoreNum = Store_Num AND ServiceType = 'Ground' AND ScheduledDeliveryDate IS NULL
	
UPDATE DCSS_UpsShipments SET ScheduledDeliveryDate = 
	dbo.DCSFN_GetDeliveryDate (ShippedDateTime , GROUND_TRAVEL) FROM 
	DCSS_UpsShipments  JOIN #tempUpsAddress 
	ON StoreNum = Store_Num AND ServiceType = 'Standard' AND ScheduledDeliveryDate IS NULL
	
DROP TABLE #tempUpsAddress
GO

GRANT EXEC ON DCSP_UpdateDeliveryTime TO AppUser

GO
