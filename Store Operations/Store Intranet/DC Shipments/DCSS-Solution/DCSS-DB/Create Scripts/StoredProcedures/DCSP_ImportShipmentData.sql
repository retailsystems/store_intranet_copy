IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ImportShipmentData')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ImportShipmentData'
		DROP  Procedure  DCSP_ImportShipmentData
	END

GO

PRINT 'Creating Procedure DCSP_ImportShipmentData'
GO
CREATE Procedure DCSP_ImportShipmentData
	/* Param List */
	@beginDate varchar(20)
	,@endDate varchar(20)
	,@runDateFrom datetime
	,@runDateTo datetime
	,@UserID varchar(20)
AS

/******************************************************************************
**		File: DCSP_ImportShipmentData.sql
**		Name: DCSP_ImportShipmentData
**		Desc: Imports shipment data to intranet db from dovetree for a given date range
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:	   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth:	Sri Bajjuri 
**		Date:	11/27/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------		-------------------------------------------
**    01/28/04		Sri Bajjuri			Get the max shipment datetime for this run and update 
**										update the run date to max shipment datetime 
**    3/30/04		Sri					Store_Num length check filter added, max 4 digits allowed
*******************************************************************************/

DECLARE @sqlStr varchar(1000), @UpsTracking varchar(30)
DECLARE @lpn varchar(20),@udf2 varchar(35),@udf3 varchar(35), @storeNum int, @charInd int, @storeCheckOk bit
DECLARE @mailRecipients varchar(100), @msg varchar(500)
DECLARE @runID int, @statusFl int, @maxShipDtChar varchar(20), @maxShipDt datetime
SET @mailRecipients = 'sbajjuri@hottopic.com'
--Get RunID
EXEC DCSP_GetRunID @runDateFrom ,@runDateTo ,0,@UserID, @runID OUTPUT

IF @runID IS NULL
	BEGIN
	--RAISERROR('Failed to initiate export process',16,3)
	SET @msg= 'Failed to initiate export process'
	GOTO Error_Para
	--Return -1
	END
--TRUNCATE Table DCSS_UPSShipMents_History
DELETE FROM DCSS_UPSShipMents_History WHERE runID  = @runID

CREATE TABLE #temp1
	(StoreNum int)
	
--Build a sql string to query DOVTREE
	--Transfer HotTopic store i.e storenum between 0001 and 4999
	SET @sqlStr =   'INSERT INTO DCSS_UPSShipMents_History (RunID,OriginalStoreNum,ShippedLpn,UpsTracking	,PackageType,ServiceType,PackageWeight,OriginalShippedDateTime' +	
			',UDF1,UDF2,UDF3,UDF4) SELECT ' + convert(varchar,@runID) + ',dov.STORE_NUM,dov.Shipped_Lpn, dov.Ups_Tracking,dov.Package_Type, dov.Service_Type, dov.Package_Weight, ' +
			'  dov.Shipped_Date_Time, dov.UDF1,dov.UDF2, dov.UDF3,dov.UDF4  FROM OPENQUERY( dovtree,' +
			' '' ' + 'SELECT STORE_NUM,Shipped_Lpn,Ups_Tracking,Package_Type,Service_Type, Package_Weight, Shipped_Date_Time ' + 
			' ,UDF1,UDF2,UDf3,UDF4' +			
			' FROM ups_batch_export   where void_indicator <> ''''Y''''   AND Shipped_Date_Time between  ' +
			' ''''' + @beginDate + ''''' AND ''''' +  @endDate  + '''''' +
			' AND LENGTH(STORE_NUM) < 5 '' ) AS dov '

	
	EXEC(@sqlStr)
	-- set @end date as shippeddate.
	UPDATE DCSS_UPSShipMents_History SET ShippedDateTime = substring(@endDate,1,10)  WHERE runID  = @runID
	/* MOD #01/28/04# */
	SELECT @maxShipDtChar = MAX(OriginalShippedDateTime) FROM
			DCSS_UPSShipMents_History WHERE ShippedDateTime = substring(@endDate,1,10)
	SELECT @maxShipDt = substring(@maxShipDtChar,1,11) + substring(@maxShipDtChar,12,2)+':' + substring(@maxShipDtChar,14,2)
	/* END MOD #01/28/04# */
	UPDATE DCSS_UPSShipMents_History SET StoreNum = OriginalStoreNum	
		WHERE  OriginalStoreNum Between '0001' AND '9999' AND runID  = @runID
		
	DECLARE UpsCur CURSOR FOR SELECT ISNULL(ShippedLpn,''), ISNULL(UDF2 ,''),ISNULL(UDF3 ,''), UpsTracking
		FROM DCSS_UPSShipMents_History WHERE StoreNum is null AND runID  = @runID
	OPEN UpsCur
	FETCH NEXT FROM UpsCur INTO @lpn, @UDF2,@UDF3,@UpsTracking
		
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @storeNum = null
			SET @charInd = 0
			SET @storeCheckOk = 0
			IF (@storeCheckOk = 0)
			BEGIN
			--Parse UDF2
				SELECT @charInd = CHARINDEX('#',@UDF2)
			IF (@charInd > 0)
				BEGIN				
					IF isNumeric(ltrim(rtrim(right(@UDF2,len(@UDF2) - @charInd)))) = 1 
						BEGIN
							SELECT @storeNum = ltrim(rtrim(right(@UDF2,len(@UDF2) - @charInd)))
							SET @storeCheckOk = 1
						END
				END
			END
			IF (@storeCheckOk = 0)
			BEGIN
			--Parse UDF3
				SELECT @charInd = CHARINDEX('#',@UDF3)
			IF (@charInd > 0)
				BEGIN				
					IF isNumeric(ltrim(rtrim(right(@UDF3,len(@UDF3) - @charInd)))) = 1 
						BEGIN
							SELECT @storeNum = ltrim(rtrim(right(@UDF3,len(@UDF3) - @charInd)))
							SET @storeCheckOk = 1
						END
				END
			END
			IF (@storeCheckOk = 0)
			BEGIN
				IF IsNumeric(@lpn) = 1
				BEGIN
					Delete from #temp1
					-- Try to get it from dovtree table cartonpush
					SET @sqlStr = 'INSERT INTO #temp1 SELECT top 1 Push_Store FROM OPENQUERY( dovtree,' +
								' '' ' + 'SELECT Push_Store FROM CARTONPUSH WHERE Push_Lpn = ''''' + @lpn + ''''' '')'  
						
					EXEC(@sqlStr)
					SELECT @storeNum = StoreNum From #temp1 
					IF (@storeNum is NOT NULL)
						SET @storeCheckOk = 1
				END	
			END
			IF (@storeCheckOk = 1)
				BEGIN
					UPDATE DCSS_UPSShipMents_History SET StoreNum = @storeNum WHERE UpsTracking = @UpsTracking
				END
			FETCH NEXT FROM UpsCur INTO @lpn, @UDF2,@UDF3,@UpsTracking
		END
	CLOSE UpsCur
	DEALLOCATE UpsCur
	DROP TABLE #temp1
	--Exceptions
	-- 1.Missing StoreNum, 2-Duplicate upstracking #, 3-Travel time error
	UPDATE DCSS_UPSShipMents_History
		SET ExceptionTypeID = 1
		WHERE StoreNum is NULL AND RunID = @runID
	UPDATE DCSS_UPSShipMents_History
		SET ExceptionTypeID = 2
		WHERE UpsTracking in 
			(SELECT UpsTracking FROM DCSS_UPSShipMents_History
				WHERE RunID = @runID Group BY UpsTracking HAVING count(*) > 1)
	
	IF @@error <> 0
		BEGIN
			--RAISERROR('Failed to export data',16,3)
			--Return -1
			SET @msg= 'Failed to export data'
			GOTO Error_Para
		END
		
	BEGIN TRAN
	--Populate hottopic shipments
	INSERT INTO DCSS_UPSShipMents (StoreNum,ShippedLpn,UpsTracking	,PackageType,ServiceType,PackageWeight,OriginalShippedDateTime,ShippedDateTime,RunID,UpdateBy,UpdateDate)
		SELECT UPH.StoreNum,UPH.ShippedLpn,UPH.UpsTracking,UPH.PackageType,UPH.ServiceType
		,UPH.PackageWeight,UPH.OriginalShippedDateTime,UPH.ShippedDateTime,UPH.RunID, @userID, getDate()
		FROM DCSS_UPSShipMents_History UPH INNER JOIN Store ON UPH.StoreNum = Store.StoreNum
		LEFT OUTER JOIN DCSS_UPSShipMents UPS ON UPH.UpsTracking = UPS.UpsTracking 
		--AND UPH.RunID = UPS.RunID		
		WHERE UPS.UpsTracking IS NULL AND ExceptionTypeID = 0 AND UPH.RunID = @runID 
	--Populate torrid shipments
	INSERT INTO Torrid_Dev.dbo.DCSS_UPSShipMents (StoreNum,ShippedLpn,UpsTracking,PackageType,ServiceType,PackageWeight,OriginalShippedDateTime,ShippedDateTime,RunID,UpdateBy,UpdateDate)
		SELECT UPH.StoreNum,UPH.ShippedLpn,UPH.UpsTracking,UPH.PackageType,UPH.ServiceType
		,UPH.PackageWeight,UPH.OriginalShippedDateTime,UPH.ShippedDateTime,UPH.RunID, @userID, getDate()
		FROM DCSS_UPSShipMents_History UPH INNER JOIN Torrid_Dev.dbo.Store Store ON UPH.StoreNum = Store.StoreNum
		LEFT OUTER JOIN Torrid_Dev.dbo.DCSS_UPSShipMents UPS ON UPH.UpsTracking = UPS.UpsTracking 
		--AND UPH.RunID = UPS.RunID		
		WHERE UPS.UpsTracking IS NULL AND ExceptionTypeID = 0 AND UPH.RunID = @runID 		
	
	IF @@error <> 0
		BEGIN
			ROLLBACK TRAN
			--RAISERROR('Failed to export data',16,3)
			SET @msg= 'Failed to export data'
			GOTO Error_Para
		END
	ELSE
		COMMIT TRAN	
	IF EXISTS(SELECT RunID FROM DCSS_UPSShipMents_History WHERE RunID = @runID )
	BEGIN
		--Update scheduled delivery date	
		EXEC DCSP_UpdateDeliveryTime
		EXEC Torrid_Dev.dbo.DCSP_UpdateDeliveryTime
		IF @@error <> 0
			BEGIN
				set @msg = 'Failed to update scheduled delivery date for shipments'
				GOTO Error_Para			
			END
	END
	
	--UPDATE JobStatus
	--@maxShipmentDateTime
	--UPDATE DCSS_ImportJobHistory SET RunDateTo = @runDateTo, StatusFl = 1 WHERE RunID = @runID
	UPDATE DCSS_ImportJobHistory SET RunDateTo = @maxShipDt, StatusFl = 1 WHERE RunID = @runID
		
	--  3-Travel time exception
	UPDATE DCSS_UPSShipMents_History
		SET ExceptionTypeID = 3
		FROM DCSS_UPSShipMents_History UPH, DCSS_UPSShipMents UPS
		WHERE UPH.UpsTracking = UPS.UpsTracking AND UPH.RunID = @runID AND ExceptionTypeID = 0
		AND UPS.ScheduledDeliveryDate IS NULL
		
	UPDATE DCSS_UPSShipMents_History
		SET ExceptionTypeID = 3
		FROM DCSS_UPSShipMents_History UPH, Torrid_Dev.dbo.DCSS_UPSShipMents UPS
		WHERE UPH.UpsTracking = UPS.UpsTracking AND UPH.RunID = @runID AND ExceptionTypeID = 0
		AND UPS.ScheduledDeliveryDate IS NULL
	
		
	DELETE FROM DCSS_UPSShipMents_History WHERE ExceptionTypeID = 0 AND RunID = @runID	
	
	
	--Send exceptions E-MAIL
	IF exists(select UpsTracking from DCSS_UPSShipMents_History WHERE ExceptionTypeID > 0 AND RunID = @runID	)
	BEGIN
		SET @sqlStr = 'SELECT RunID,UpsTracking, EXP.Description as Error_Msg '+
			' FROM HotTopic2_Dev.dbo.DCSS_UPSShipMents_History UPH ' +
			' INNER JOIN HotTopic2_Dev.dbo.DCSS_ExceptionTypes EXP ON UPH.ExceptionTypeID = EXP.ExceptionTypeID ' +
			' WHERE UPH.ExceptionTypeID > 0 AND RunID =' + convert(varchar,@runID)
		EXEC master.dbo.xp_sendmail @recipients = @mailRecipients, 
		@query = @sqlStr,
		@subject = 'DC Shipments exceptions ',
		@message = 'Document attached',
		@attach_results = 'TRUE', @width = 800
		IF @@error <> 0
			BEGIN			
				RAISERROR('Failed to deliver  exception email',16,3)
			END
	END
	
	RETURN 0
	
Error_Para:
BEGIN
	IF (@runID IS NOT NULL)
		-- Set the job status to cancelled
		UPDATE DCSS_ImportJobHistory SET  RunDateTo = @runDateTo, StatusFl = 3 WHERE RunID = @runID	
	SET @msg = @msg + ' Job Run #:' + convert(varchar,@runID)
	
	EXEC master.dbo.xp_sendmail @recipients = @mailRecipients, 
			@subject = 'DC Shipments Alert',
			@message = @msg
	
	RAISERROR(@msg,16,3)	
	Return -1	
END
GO

GRANT EXEC ON DCSP_ImportShipmentData TO AppUser

GO
