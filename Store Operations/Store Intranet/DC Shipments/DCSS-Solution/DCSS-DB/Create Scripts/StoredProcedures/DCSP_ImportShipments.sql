IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ImportShipments')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ImportShipments'
		DROP  Procedure  DCSP_ImportShipments
	END

GO

PRINT 'Creating Procedure DCSP_ImportShipments'
GO
CREATE Procedure DCSP_ImportShipments
	@companyID int -- 1-HotTopic, 2-Torrid
	/* Param List */
AS

/******************************************************************************
**		File: DCSP_ImportShipments.sql
**		Name: DCSP_ImportShipments
**		Desc: .
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     		----------							-----------
**		
**		Auth: Sri Bajjuri
**		Date: 10/22/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------			-------------------------------------------
**   
*******************************************************************************/
SET NOCOUNT ON
SET QUOTED_IDENTIFIER OFF


DECLARE @beginDate datetime, @endDate datetime, @sqlStr varchar(1000)
DECLARE @storeMin varchar(4), @storeMax varchar(4)

BEGIN --#1
	SET @beginDate = DATEADD(day, -2, getDate()) 
	SET @endDate =  getDate() 
	/*********************/
	IF (@companyID = 1) -- HotTopic
		BEGIN
			SET @storeMin = '0001'
			SET @storeMax = '4999'
		END
	IF (@companyID = 2) -- Torrid
		BEGIN
			SET @storeMin = '5000'
			SET @storeMax = '6999'
		END
	/********************/
	--Build a sql string to query DOVTREE
	--Transfer HotTopic store i.e storenum between 0001 and 4999
	SET @sqlStr =   'INSERT INTO DCSS_UPSShipMents (StoreNum,ShippedLpn,UpsTracking	,PackageType,ServiceType,PackageWeight,ShippedDateTime) ' +	
			'SELECT dov.STORE_NUM,dov.Shipped_Lpn, dov.Ups_Tracking,dov.Package_Type, dov.Service_Type, dov.Package_Weight, ' +
			'  cast(substring(dov.Shipped_Date_Time,1,10) as datetime)  FROM OPENQUERY( dovtree,' +
			' '' ' + 'SELECT STORE_NUM,Shipped_Lpn,Ups_Tracking,Package_Type,Service_Type, Package_Weight, Shipped_Date_Time ' + 			
			' FROM ups_batch_export   where void_indicator <> ''''Y'''' AND  STORE_NUM IS NOT NULL  AND substr(Shipped_Date_Time,1,10) between ' +
			' ''''' + convert(varchar(10), @beginDate ,101) + ''''' AND ''''' + convert(varchar(10), @endDate ,101) + '''''' +
			' AND Store_Num between ''''' + @storeMin + ''''' AND ''''' + @storeMax + ''''' ' +
			' '' ) AS dov LEFT OUTER JOIN DCSS_UPSShipMents dcss ON dov.Ups_Tracking = dcss.UpsTracking ' +
			' WHERE dcss.UpsTracking IS NULL ORDER BY Shipped_Date_Time '

	--PRINT @sqlStr
	
	EXEC(@sqlStr)
	
	/************************** Torrid Only ********************************************************/
	--Delete single digit store data (string comparison copying data with store num 8,9 etc
	IF (@companyID = 2)
		DELETE FROM DCSS_UPSShipMents where storeNum < @storeMin
	/*************************************** ********************************************************/
END --#1
GO

GRANT EXEC ON DCSP_ImportShipments TO AppUser

GO
