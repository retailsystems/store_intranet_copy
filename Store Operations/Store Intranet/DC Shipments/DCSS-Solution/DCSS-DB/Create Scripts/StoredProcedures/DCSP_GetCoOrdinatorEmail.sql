IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_GetCoOrdinatorEmail')
	BEGIN
		PRINT 'Dropping Procedure DCSP_GetCoOrdinatorEmail'
		DROP  Procedure  DCSP_GetCoOrdinatorEmail
	END

GO

PRINT 'Creating Procedure DCSP_GetCoOrdinatorEmail'
GO
CREATE Procedure DCSP_GetCoOrdinatorEmail
	/* Param List */
	@StoreNum int,
	@CoOrdinatorEmail varchar(255) OUTPUT
AS

/******************************************************************************
**		File: DCSP_GetCoOrdinatorEmail.sql
**		Name: DCSP_GetCoOrdinatorEmail
**		Desc: Returns Shipping co-ordinator email.
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 01/27/2005
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**		
*******************************************************************************/
SELECT @CoOrdinatorEmail = CoOrdinatorEmail
	FROM Hottopic2.dbo.DCSS_WhseConfig w
	JOIN Hottopic2.dbo.DCSS_UpsTravelTime s
	ON w.whseid = s.whse AND s.MasterWhseFlag = 1
	WHERE s.Store_Num = @StoreNum

GO

GRANT EXEC ON DCSP_GetCoOrdinatorEmail TO AppUser

GO
