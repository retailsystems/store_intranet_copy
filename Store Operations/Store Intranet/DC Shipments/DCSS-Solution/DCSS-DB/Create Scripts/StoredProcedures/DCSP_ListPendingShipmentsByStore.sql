IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_ListPendingShipmentsByStore')
	BEGIN
		PRINT 'Dropping Procedure DCSP_ListPendingShipmentsByStore'
		DROP  Procedure  DCSP_ListPendingShipmentsByStore
	END

GO

PRINT 'Creating Procedure DCSP_ListPendingShipmentsByStore'
GO
CREATE Procedure DCSP_ListPendingShipmentsByStore
	/* Param List */
	@storeNum smallint
AS

/******************************************************************************
**		File: 
**		Name: DCSP_ListPendingShipmentsByStore
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT UpsTracking,ServiceType,ShippedDateTime,PackageType,ups.StatusCD,sl.Description as Status
	FROM DCSS_UPSShipments ups
	INNER JOIN DCSS_StatusLookUp sl ON ups.StatusCD = sl.StatusCD
	WHERE datediff(day,ShippedDateTime,getdate()) > 7 
	AND ups.StatusCD not in(3)
	AND storeNum = @storeNum
	Order BY ShippedDateTime



GO

GRANT EXEC ON DCSP_ListPendingShipmentsByStore TO AppUser

GO
