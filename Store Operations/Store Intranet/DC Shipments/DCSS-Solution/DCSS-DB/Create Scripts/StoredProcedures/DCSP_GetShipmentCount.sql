IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'DCSP_GetShipmentCount')
	BEGIN
		PRINT 'Dropping Procedure DCSP_GetShipmentCount'
		DROP  Procedure  DCSP_GetShipmentCount
	END

GO

PRINT 'Creating Procedure DCSP_GetShipmentCount'
GO
CREATE Procedure DCSP_GetShipmentCount
	@beginDate varchar(20)
	,@endDate varchar(20)
	
AS

/******************************************************************************
**		File: DCSP_GetShipmentCount.sql
**		Name: DCSP_GetShipmentCount
**		Desc: Returns no.of shipments made for a given daterange.
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

DECLARE  @sqlStr varchar(400)

SET @sqlStr =   'SELECT cnt FROM OPENQUERY( dovtree,' +
			' '' ' + 'SELECT Count(Ups_Tracking) as cnt  FROM ups_batch_export ' +
			'  where void_indicator <> ''''Y''''   AND Shipped_Date_Time between  ' +
			' ''''' + @beginDate + ''''' AND ''''' +  @endDate  + '''''' +
			' '' )' 
EXEC(@sqlStr)


IF @@error <> 0
	RAISERROR('Unable to fetch shipment count',16,3)
	


GO

GRANT EXEC ON DCSP_GetShipmentCount TO AppUser

GO
