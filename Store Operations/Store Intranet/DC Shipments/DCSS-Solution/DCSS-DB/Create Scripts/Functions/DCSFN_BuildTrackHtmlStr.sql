if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DCSFN_BuildTrackHtmlStr]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[DCSFN_BuildTrackHtmlStr]
GO

CREATE FUNCTION [dbo].[DCSFN_BuildTrackHtmlStr]
	(
	@trackingNum varchar(30), @ServiceProvider smallint
	)
RETURNS varchar(5000)
AS
/******************************************************************************
**		File: DCSFN_BuildTrackHtmlStr.sql
**		Name: DCSFN_BuildTrackHtmlStr
**		Desc:  This function returns tracking html string
**
**		This template can be customized:
**              
**		Return values: Table
** 
**		Called by:   
**              
**		Parameters:
**		Input									Output
**     ----------								-----------
**		
**	
**		Auth: Sri Bajjuri
**		Date: 04/28/2010
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    
*******************************************************************************/
	BEGIN
		DECLARE @htmlStr varchar(5000), @trackStr varchar(5000), @ecomOrder varchar(30)
		DECLARE @recCnt smallint
		DECLARE ecomCur CURSOR FOR
			select EcomOrderNum from dbo.DCSS_SnSOrders 
			where TrackingNum = @trackingNum;
			
		IF (@ServiceProvider = 1)			
			SET @trackStr = '<a style="font-size:xx-small;" href="http://wwwapps.ups.com/etracking/tracking.cgi?tracknums_displayed=1&TypeOfInquiryNumber=T&HTMLVersion=4.0&InquiryNumber1=' +  @trackingNum + '" target=_blank>' +  @trackingNum + '</a>';
		ELSE IF	(@ServiceProvider = 4)	
			SET @trackStr = '<a style="font-size:xx-small;" href="http://www.fedex.com/Tracking?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers=' +  @trackingNum + '" target=_blank>' +  @trackingNum + '</a>';
		ELSE
			SET @trackStr = @trackingNum;
		select @recCnt= count(*) from dbo.DCSS_SnSOrders where TrackingNum = @trackingNum;
		IF 	@recCnt = 0 		
			SET @htmlStr = '<IMG src="images/spacer.gif" width="5px" height="1px">' + @trackStr
		ELSE
			-- build tree
			BEGIN
				SET @htmlStr = '<table cellpadding=0 cellspacing=0 border=0><TR><TD align=left><IMG onclick=javascript:showtree(''' + @trackingNum + ''') src="images/Plus.gif" border=0 name=I' + @trackingNum + '></TD><TD style="font-size:x-small;font-family:Arial;">';
				SET @htmlStr = @htmlStr + @trackStr	 +'<font color=red><sup>*</sup></font></TD></TR><TR><TD colSpan=2><TABLE id=S' + @trackingNum + ' style="DISPLAY: none" cellSpacing=0 cellPadding=0 width=100% border=0>';
				
				OPEN ecomCur;
				FETCH NEXT FROM ecomCur into @ecomOrder;
				WHILE @@FETCH_STATUS = 0
				BEGIN
					SET @htmlStr = @htmlStr + '<TR><TD style="font-size:xx-small;font-family:Arial;height:15px"><IMG src="images/spacer.gif" width="25px" height="1px">- ' + @ecomOrder + '</TD></tr>';
					FETCH NEXT FROM ecomCur into @ecomOrder;
				END;
				CLOSE ecomCur;
				DEALLOCATE ecomCur;
				SET @htmlStr = @htmlStr +'</table></td></tr></table>';
			END;
		
	RETURN @htmlStr
	END
GO

GRANT EXEC ON DCSFN_GetDeliveryDate TO AppUser

GO