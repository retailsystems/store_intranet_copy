if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DCSFN_GetDeliveryDate]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[DCSFN_GetDeliveryDate]
GO

CREATE FUNCTION DCSFN_GetDeliveryDate
	(
	@shippedDate datetime, @travelTime int
	)
RETURNS datetime
AS
/******************************************************************************
**		File: DCSFN_GetDeliveryDate.sql
**		Name: DCSFN_GetDeliveryDate
**		Desc:  This function returns delivery date
**
**		This template can be customized:
**              
**		Return values: Table
** 
**		Called by:   
**              
**		Parameters:
**		Input									Output
**     ----------								-----------
**		
**	
**		Auth: Sri Bajjuri
**		Date: 11/03/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    
*******************************************************************************/
	BEGIN
		DECLARE @deliveryDate datetime
		DECLARE @maxCnt smallint, @cnt smallint
		SET @maxCnt = @travelTime + 1
		SET @cnt = 1
		SET @deliveryDate = @shippedDate
		WHILE (@cnt < @maxCnt)
		BEGIN
			SELECT @deliveryDate = DATEADD(day,1,@deliveryDate)
			IF DATEPART(weekday,@deliveryDate) = 7 -- if Saturday bump it to Monday
				SELECT @deliveryDate = DATEADD(day,2,@deliveryDate)
			IF DATEPART(weekday,@deliveryDate) = 1 -- if Sunday bump it to Monday
				SELECT @deliveryDate = DATEADD(day,1,@deliveryDate)

			SET @cnt = @cnt + 1
		END
		
	RETURN @deliveryDate
	END

GO

GRANT EXEC ON DCSFN_GetDeliveryDate TO AppUser

GO
