' Static Model
Imports System.Data.SqlClient

Namespace HotTopic.DCSS.Services

    ' Manages data services
    ' Manages data services
    Public Class SQLDataManager

        Private Shared _sqlDataMgr As SQLDataManager
        Protected _dAx As HotTopic.AIMS.DataAccess.SQLDataAccessor
        Protected _sqlTrans As Hashtable

        'Singleton class, access instance via this method
        Public Shared Function GetInstance() As SQLDataManager
            If _sqlDataMgr Is Nothing Then

                _sqlDataMgr = New SQLDataManager(HotTopic.DCSS.Settings.AppSetting.ConnectionString)

            End If
            Return _sqlDataMgr
        End Function

        Public Function GetDataSet(ByVal spName As String, ByVal ParamArray cmdParams() As SqlParameter) As DataSet
            Try
                Return _dAx.ExecuteDataset(CommandType.StoredProcedure, spName, cmdParams)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetDataSet(ByVal sqlText As String) As DataSet
            Try
                Return _dAx.ExecuteDataset(CommandType.Text, sqlText)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDataReader(ByVal spName As String, ByVal ParamArray cmdParams() As SqlParameter) As SqlDataReader
            Try
                Return _dAx.ExecuteReader(CommandType.StoredProcedure, spName, cmdParams)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub Execute(ByVal spName As String, ByVal ParamArray cmdParams() As SqlParameter)
            Try
                _dAx.ExecuteNonQuery(CommandType.StoredProcedure, spName, cmdParams)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub ExecuteNonQuery(ByVal sql As String)
            Try
                _dAx.ExecuteNonQuery(CommandType.Text, sql)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub ExecuteTransaction(ByVal spName As String, ByVal tranName As String, ByVal ParamArray cmdParams() As SqlParameter)
            Try
                Dim tran As SqlTransaction = DirectCast(_sqlTrans(tranName), SqlTransaction)
                _dAx.ExecuteNonQuery(tran, CommandType.StoredProcedure, spName, cmdParams)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub BeginTransaction(ByVal tranName As String, Optional ByVal isoLevel As IsolationLevel = IsolationLevel.ReadCommitted)
            Try
                Dim tran As SqlTransaction = _dAx.BeginTransaction(tranName, isoLevel)
                If _sqlTrans.ContainsKey(tranName) Then
                    _sqlTrans(tranName) = tran
                Else
                    _sqlTrans.Add(tranName, tran)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub CommitTransaction(ByVal tranName As String)
            Try
                Dim tran As SqlTransaction = DirectCast(_sqlTrans(tranName), SqlTransaction)
                _dAx.CommitTransaction(tran)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub RollbackTransaction(ByVal tranName As String)
            Try
                Dim tran As SqlTransaction = DirectCast(_sqlTrans(tranName), SqlTransaction)
                _dAx.RollbackTransaction(tran)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub New(ByVal connString As String)
            _dAx = New HotTopic.AIMS.DataAccess.SQLDataAccessor(connString)
            _sqlTrans = New Hashtable()
        End Sub
    End Class ' END CLASS DEFINITION SQLDataManager


End Namespace ' HotTopic.ISS.Services

