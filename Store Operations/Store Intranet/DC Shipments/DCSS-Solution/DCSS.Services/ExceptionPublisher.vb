Imports System.IO
Imports System.Web.Mail
Imports System.Collections.Specialized
Imports System.Security
Imports HotTopic.DCSS.Services
Namespace HotTopic.DCSS.Services

    Public Class ExceptionPublisher
        Implements Microsoft.ApplicationBlocks.ExceptionManagement.IExceptionPublisher

        Private m_LogName As String = "C:\ErrorLog.txt"
        Private m_OpMail As String = ""
        Private Shared EXCEPTIONMANAGER_NAME As String = "HotTopic"
        Public Shared Sub Publish(ByVal exception As Exception)
            Dim ePub As ExceptionPublisher = New ExceptionPublisher()
            ePub.Publish(exception, Nothing, Nothing)
        End Sub 'Publish

        ' Provide implementation of the IPublishException interface
        ' This contains the single Publish method.
        Sub Publish(ByVal e As Exception, ByVal AdditionalInfo As NameValueCollection, ByVal ConfigSettings As NameValueCollection) Implements Microsoft.ApplicationBlocks.ExceptionManagement.IExceptionPublisher.Publish
            ' Load Config values if they are provided.

            If Not HotTopic.DCSS.Settings.AppSetting.ErrorLog Is Nothing AndAlso HotTopic.DCSS.Settings.AppSetting.ErrorLog.Length > 0 Then
                m_LogName = HotTopic.DCSS.Settings.AppSetting.ErrorLog
            End If
            If Not HotTopic.DCSS.Settings.AppSetting.WebmasterEmail Is Nothing AndAlso HotTopic.DCSS.Settings.AppSetting.WebmasterEmail.Length > 0 Then
                m_OpMail = HotTopic.DCSS.Settings.AppSetting.WebmasterEmail
            End If
            If Not HotTopic.DCSS.Settings.AppSetting.ProjectName Is Nothing AndAlso HotTopic.DCSS.Settings.AppSetting.ProjectName.Length > 0 Then
                EXCEPTIONMANAGER_NAME = HotTopic.DCSS.Settings.AppSetting.ProjectName
            End If
            ' Create StringBuilder to maintain publishing information.
            Dim strInfo As System.Text.StringBuilder = New System.Text.StringBuilder()

            '********************************
            If AdditionalInfo Is Nothing Then AdditionalInfo = New NameValueCollection()

            Try
                AdditionalInfo.Add(EXCEPTIONMANAGER_NAME + ".MachineName", Environment.MachineName)
            Catch ex As SecurityException
                AdditionalInfo.Add(EXCEPTIONMANAGER_NAME + ".MachineName", "RES_EXCEPTIONMANAGEMENT_PERMISSION_DENIED")
            Catch
                AdditionalInfo.Add(EXCEPTIONMANAGER_NAME + ".MachineName", "RES_EXCEPTIONMANAGEMENT_INFOACCESS_EXCEPTION")
            End Try

            Try
                AdditionalInfo.Add(EXCEPTIONMANAGER_NAME + ".TimeStamp", DateTime.Now.ToString())
            Catch ex As SecurityException
                AdditionalInfo.Add(EXCEPTIONMANAGER_NAME + ".TimeStamp", "RES_EXCEPTIONMANAGEMENT_PERMISSION_DENIED")
            Catch
                AdditionalInfo.Add(EXCEPTIONMANAGER_NAME + ".TimeStamp", "RES_EXCEPTIONMANAGEMENT_INFOACCESS_EXCEPTION")
            End Try

            Try
                AdditionalInfo.Add(EXCEPTIONMANAGER_NAME + ".FullName", Reflection.Assembly.GetExecutingAssembly().FullName)

            Catch ex As SecurityException
                AdditionalInfo.Add(EXCEPTIONMANAGER_NAME + ".FullName", "RES_EXCEPTIONMANAGEMENT_PERMISSION_DENIED")
            Catch
                AdditionalInfo.Add(EXCEPTIONMANAGER_NAME + ".FullName", "RES_EXCEPTIONMANAGEMENT_INFOACCESS_EXCEPTION")
            End Try

            Try
                AdditionalInfo.Add(EXCEPTIONMANAGER_NAME + ".AppDomainName", AppDomain.CurrentDomain.FriendlyName)
            Catch ex As SecurityException
                AdditionalInfo.Add(EXCEPTIONMANAGER_NAME + ".AppDomainName", "RES_EXCEPTIONMANAGEMENT_PERMISSION_DENIED")
            Catch
                AdditionalInfo.Add(EXCEPTIONMANAGER_NAME + ".AppDomainName", "RES_EXCEPTIONMANAGEMENT_INFOACCESS_EXCEPTION")
            End Try


            '********************************

            'Record the contents of the AdditionalInfo collection.
            If Not AdditionalInfo Is Nothing Then
                ' Record General information.
                strInfo.AppendFormat("{0}{0}General Information", Environment.NewLine)
                strInfo.AppendFormat("{0}Project: " & EXCEPTIONMANAGER_NAME, Environment.NewLine)
                strInfo.AppendFormat("{0}Additonal Info:", Environment.NewLine)
                Dim i As String
                For Each i In AdditionalInfo
                    strInfo.AppendFormat("{0}{1}: {2}", Environment.NewLine, i, AdditionalInfo.Get(i))
                Next

                ' Append the exception text
                strInfo.AppendFormat("{0}{0}Exception Information{0}{1}", Environment.NewLine, e.ToString())

                ' Write the entry to the log file.
                Dim fs As FileStream = File.Open(m_LogName, FileMode.Append, FileAccess.Write)
                Dim sw As New StreamWriter(fs)

                sw.Write(strInfo.ToString())

                sw.Close()
                fs.Close()

                ' send notification email if operatorMail attribute was provided
                If m_OpMail.Length > 0 Then
                    Dim subject As String = "Exception Notification"
                    Dim body As String = strInfo.ToString()

                    Notify(HotTopic.DCSS.Settings.AppSetting.SmtpSender, m_OpMail, subject, body)
                End If

            End If

            ' End If

        End Sub
        Private Sub Notify(ByVal sender As String, ByVal mailTo As String, ByVal subject As String, ByVal body As String)
            Try
                Dim emailMgr As EmailManager = New EmailManager(HotTopic.DCSS.Settings.AppSetting.SmtpServer)
                With emailMgr
                    '.Format = Web.Mail.MailFormat.Html
                    .Sender = sender
                    .Recipient = mailTo
                    .Subject = subject
                    .Send(body)
                End With
            Catch ex As Exception

            End Try
        End Sub

    End Class
End Namespace