
Namespace HotTopic.DCSS.Services

    Public Class DCSSAppException
        Inherits ApplicationException

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

    End Class
    Public Class StoreNotFoundException
        Inherits ApplicationException

        Public Sub New()
            MyBase.New("Store no. not found")
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

    End Class
    Public Class SessionExpiredException
        Inherits ApplicationException

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

    End Class
    Public Class UpsXmlException
        Inherits ApplicationException


        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

    End Class

    Public Class FedExXmlException
        Inherits ApplicationException


        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

    End Class


End Namespace
