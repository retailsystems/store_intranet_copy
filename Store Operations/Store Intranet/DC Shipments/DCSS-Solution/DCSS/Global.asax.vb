Imports System.Web
Imports System.Web.SessionState
Imports HotTopic.DCSS.Settings
Public Class [Global]
    Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        ' Get all application settings
        Dim settings As New AppSetting()
        With settings
            .ConnectionString = ConfigurationSettings.AppSettings("ConnectionString")
            .SecurityConnectionString = ConfigurationSettings.AppSettings("SecurityConnectionString")
            .CacheDependencyFile = ConfigurationSettings.AppSettings("CacheDependency")
            .SmtpServer = ConfigurationSettings.AppSettings("SmtpServer")
            .SmtpSender = ConfigurationSettings.AppSettings("SmtpSender")
            .WebmasterEmail = ConfigurationSettings.AppSettings("WebmasterEmail")
            .ProjectName = ConfigurationSettings.AppSettings("ProjectName")
            '.ErrorLog = ConfigurationSettings.AppSettings("ErrorLog")
            .StoreSubnetMask = ConfigurationSettings.AppSettings("StoreSubnetMask")
            If Not IsNothing(ConfigurationSettings.AppSettings("RoleBasedSecurity")) AndAlso ConfigurationSettings.AppSettings("RoleBasedSecurity").ToUpper = "ON" Then
                .IsRoleBasedSecurityOn = True
            Else
                .IsRoleBasedSecurityOn = False
            End If
            .DmJobCodes = ConfigurationSettings.AppSettings("DmJobCodes")
            .RdJobCodes = ConfigurationSettings.AppSettings("RdJobCodes")
        End With
        Application("AppSetting") = settings
        Application("DebugMode") = ConfigurationSettings.AppSettings("DebugMode")
        Application("ShipCoOrdinatorEmail") = ConfigurationSettings.AppSettings("ShipCoOrdinatorEmail")

        Application("Mode") = ConfigurationSettings.AppSettings("Mode")
        Application("StyleSheet") = ConfigurationSettings.AppSettings("StyleSheet")
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
        Application("Exp") = Server.GetLastError
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class
