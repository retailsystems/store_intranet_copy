<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Home.aspx.vb" Inherits="DCSS.Home" %>

<!DOCTYPE html>
<html>
<head>
    <style>
        UL
        {
            margin-top: 1px;
            font-weight: bolder;
            font-size: 7pt;
            margin-bottom: 1px;
            margin-left: 10px;
            color: #ffffff;
            font-family: Arial;
        }
        LI
        {
            font-weight: 500;
            font-size: 7pt;
            margin-left: 8px;
            color: #00639a;
            font-family: Arial;
            list-style-type: square;
        }
    </style>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<%=Application("StyleSheet")%>" type="text/css" rel="stylesheet">
    <link id="Link1" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link id="Link2" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->
    <script language='javascript'>
        function OpenPop(url, w, h) {
            var url1 = url;

            var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h + ",width=" + w + ",scrollbars=yes"
            window.open(url1, 'cw', winProp);
        }
        function OpenPop1(url, cw, w, h) {
            var url1 = url;

            var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h + ",width=" + w + ",scrollbars=yes"
            window.open(url1, cw, winProp);
        }
        function ChangeBgColor(obj, act) {
            if (act == "in") {
                obj.style.border = '#4ebfde';

            } else {
                obj.style.background = '#246178';
            }
        }
        function PrintMe() {
            window.print();
        }
        function OpenCartonsearch() {
            debugger;
            var ddl = document.getElementById("lstStore");

        }		
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <input type="hidden" runat="server" id="hdnRefeshFlag" value="0">
    <asp:label id="lblError" visible="False" runat="server" cssclass="errStyle"></asp:label>
    </td>
    <h4>
        Expected Shipments</h4>
    <div class="input-group calendar_droplist">
        <asp:panel cssclass="input-group-addon" id="pnlSelectStore" runat="server" visible="False">
                Store #:</asp:panel>
        <asp:dropdownlist id="lstStore" cssclass="form-control" runat="server" autopostback="True">
            </asp:dropdownlist>
    </div>
    <div class="callwraper">
        <asp:calendar id="shpmtCalendar" cssclass="CalStyle" cellpadding="2" cellspacing="2"
            titlestyle-cssclass="CalTitleStyle" dayheaderstyle-cssclass="CalDayHeaderStyle"
            daystyle-cssclass="CalDayStyle" selectionmode="None" daystyle-verticalalign="Middle"
            runat="server" width="100%" nextprevformat="ShortMonth" nextprevstyle-cssclass="CalTitleStyle">
            </asp:calendar>
    </div>
    <br />
    <div class="Calhints" style="border-left: solid 10px #4682b4">
        Pending Shipments</div>
    <div class="Calhints" style="border-left: solid 10px #636363">
        All Shipments received</div>
    <div class="Calhints">
        <font color="red">*</font>&nbsp;STS order(s)</div>
    <div class="Calhints" style="border-left: solid 10px #4682d8; border-left-color: #669900;">
       <!-- <%--<a href="javascript:OpenPop('Cartonsearch.aspx',650,600)" onclick="OpenCartonsearch()"
            style='text-decoration: underline; color: #000000'>Carton Search</a>--%> -->
            <asp:HyperLink id="CartonSearchhyperlink" 
                  NavigateUrl="javascript:OpenPop('Cartonsearch.aspx',650,600)"
                  Text="Carton Search"
                  runat="server"/> 
            
    </div>
    <div class="clearfix">
    </div>
    <br />
    <label style="font: normal 11px arial">
        <sup>*</sup>Shipment delivery dates may vary. Please check for missing transfers
        two days prior and after the expected delivery date.</label>
    </form>
    <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
    <script>
        // removing drop downlist if you are not logged in as a DM -->Bassem
        $(document).ready(function () {
            var listLen = $("#lstStore option").length;
            if (listLen <= 0) {
                $("#lstStore").remove();
            }
        });
    </script>
    <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
</body>
</html>
