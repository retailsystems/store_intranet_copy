Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Public Class PendingShipments
    Inherits System.Web.UI.Page

    Protected WithEvents lblDeliveryDate As System.Web.UI.WebControls.Label
    Protected WithEvents dgShipment As System.Web.UI.WebControls.DataGrid
    Private _sessionUser As HotTopic.DCSS.Application.Session
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents btnUpdate As System.Web.UI.WebControls.Button
    Private _ds As DataSet
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

   
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            lblError.Text = ""
            lblError.Visible = False
            _sessionUser = New Session()
        Catch ex As StoreNotFoundException
            Response.Redirect("StoreNotFound.aspx")
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        If Not IsPostBack Then
            BindData()
        End If
    End Sub
    Private Sub BindData()
        _ds = _sessionUser.ListPendingShipments()
        dgShipment.DataSource = _ds
        dgShipment.DataBind()
    End Sub
    Private Sub ShowError(ByVal msg As String)
        If msg.Trim.Length > 0 Then
            lblError.Text = msg
            lblError.Visible = True
        End If
    End Sub

    Private Sub dgShipment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgShipment.ItemDataBound
        'e.Item.Cells(0).Controls.Clear()
    End Sub
    Public Function ShowCheckBox(ByVal statusID As Integer, ByVal UpsTracking As String) As String
        Dim cbStr As String = "<input type=checkbox id='cbDgShipment' name='cbDgShipment' value='" & UpsTracking & "'>"
        Select Case statusID
            Case ShipmentStatus.Received
                cbStr = ""
        End Select
        Return cbStr
    End Function

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim cbCheckedStr As String = ""
        Dim arrCbChecked() As String
        Dim i As Int16 = 0
        If Not Request("cbDgShipment") Is Nothing Then
            cbCheckedStr = Request("cbDgShipment")
        End If
        If cbCheckedStr.Length > 0 Then
            arrCbChecked = Split(cbCheckedStr, ",")
            For i = 0 To arrCbChecked.GetUpperBound(0)
                Try
                    _sessionUser.UpdateShipmentStatus(arrCbChecked(i), Request("lstStatus"))
                    'Response.Write(arrCbChecked(i) & Request("lstStatus") & "<br>")
                Catch ex As DCSSAppException
                    ShowError(ex.Message)
                    Exit Sub
                Catch ex As Exception
                    Throw ex
                End Try
            Next
            BindData()
        End If
    End Sub
End Class
