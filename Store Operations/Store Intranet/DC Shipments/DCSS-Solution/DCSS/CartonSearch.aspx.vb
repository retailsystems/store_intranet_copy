﻿Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Imports System.Globalization

Public Class CartonSearch
    Inherits System.Web.UI.Page

    Private _sessionUser As HotTopic.DCSS.Application.Session
    Private _ds As DataSet
    Private _storeNum As Int16 = 0




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'need to pass httpcontext at every page load
            lblError.Text = ""
            lblError.Visible = False
            _sessionUser = New Session(HttpContext.Current)
           
            ' i added this
            'lblDeliveryDate.Text = Request.QueryString("dt")

            'If Not IsDate(lblDeliveryDate.Text) Then
            'Throw New DCSSAppException("Invalid Delivery Date")
            'End If
            'lblEmailTo.Text = Application("ShipCoOrdinatorEmail")
            'lblEmailTo.Text = _sessionUser.GetCoOrdinatorEmail(_storeNum)
        Catch ex As StoreNotFoundException
            Response.Redirect("StoreNotFound.aspx")
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        If Not IsPostBack Then
            Dim dt As Date = Date.Today
            txtPostDateFrom.Text = dt.AddDays(-7).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)
            txtPostDateTo.Text = dt.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)
            'BindData()
            Dim trackingNum As String
            If Not Request.QueryString("ID") Is Nothing Then

                trackingNum = Request.QueryString("ID")
                If Not String.IsNullOrEmpty(trackingNum) Then
                    txtPostDateFrom.Text = dt.AddDays(-30).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)
                    txttrackingnum.Text = trackingNum
                    BindData()
                End If
            End If

        End If
    End Sub
    Private Sub BindData()
        Try
            _sessionUser = New Session(HttpContext.Current)
            If Not Request("store") Is Nothing AndAlso IsNumeric(Request("store")) Then
                _sessionUser.storeNum = Request("store")
            End If
            _ds = _sessionUser.SearchShipmentItems(_sessionUser.storeNum, txtSku.Text, txtItemDesc.Text, txtPostDateFrom.Text, txtPostDateTo.Text, txttrackingnum.Text)
            If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then
                dgShipmentItems.DataSource = _ds.Tables(0)
                '_ds.Tables(0).DefaultView.Sort = SortBy.Text & " " & SortDirection.Text
                '_ds.Tables(0).DefaultView.Sort = IIf(SortBy.Text.Trim.Length > 0, SortBy.Text, "ShippedDateTime  ASC")
                'If dgShipmentItems.CurrentPageIndex > 0 AndAlso _ds.Tables(0).Rows.Count <= (dgShipmentItems.CurrentPageIndex * dgShipmentItems.PageSize) Then
                '    dgShipmentItems.CurrentPageIndex = dgShipmentItems.CurrentPageIndex - 1
                'End If
                dgShipmentItems.DataBind()
                lblRecCount.Text = _ds.Tables(0).Rows.Count & " Record(s) found."
                pnlDgShipmentItem.Visible = True
            Else
                pnlDgShipmentItem.Visible = False
                ShowError("No records found")
            End If
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If ValidateFields() = True Then
            Exit Sub
        End If
        BindData()
        'PrepareSearchParams()
       
        'Dim table As New DataTable()
        'table.Columns.Add("CartonNumber")
        'table.Columns.Add("TrackingNumber")
        'table.Columns.Add("SKU")
        'table.Columns.Add("Item Desc")
        'table.Columns.Add("Quantity")
        'table.Columns.Add("Shipped Date")
        'table.Columns.Add("Shipped From")
        'Dim dr As DataRow = table.NewRow()
        'dr(0) = "10000000000081364562"
        'dr(1) = "1Z758W850313579562"
        'dr(2) = "10950822"
        'dr(3) = "4WHT TULLE PETTICOAT"
        'dr(4) = 15
        'dr(5) = "03/02/2018"
        'dr(6) = "DC-Industry,CA"
        'table.Rows.Add(dr)

        'dgShipmentItems.DataSource = table
        'dgShipmentItems.DataBind()

    End Sub


    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        lblError.Text = String.Empty
        txtItemDesc.Text = String.Empty
        txtPostDateFrom.Text = String.Empty
        txtPostDateTo.Text = String.Empty
        txtSku.Text = String.Empty
        lblRecCount.Text = String.Empty
        txttrackingnum.Text = String.Empty
        dgShipmentItems.DataSource = Nothing
        dgShipmentItems.DataBind()
    End Sub
    Private Sub ShowError(ByVal msg As String)
        If msg.Trim.Length > 0 Then
            lblError.Text = msg
            lblError.Visible = True
        End If
    End Sub
    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        dgShipmentItems.PageIndex = e.NewPageIndex
        BindData()
    End Sub

    Private Function ValidateFields() As Boolean
        Dim strMsg As String = String.Empty
        Dim formats() As String = {"MM/dd/yy", "MM/dd/yyyy", "M/d/yy", "M/dd/yyyy", "MM/d/yyyy", "M/d/yyyy"}

        Dim thisDt, thisdt1 As DateTime

        If DateTime.TryParseExact(txtPostDateFrom.Text, formats,
                                  Globalization.CultureInfo.InvariantCulture,
                                  DateTimeStyles.None, thisDt) = False Or DateTime.TryParseExact(txtPostDateTo.Text, formats,
                                  Globalization.CultureInfo.InvariantCulture,
                                  DateTimeStyles.None, thisdt1) = False Then
            strMsg = "Please provide the valid post from and to dates in MM/DD/YYYY format"
            ShowError(strMsg)
            Return True
        End If
        If txtPostDateFrom.Text.Trim().Length = 0 Or txtPostDateTo.Text.Trim().Length = 0 Then
            strMsg = "Please provide the valid post from and to dates"
        ElseIf DateDiff("d", txtPostDateFrom.Text, txtPostDateTo.Text, ) > 30 Or DateDiff("d", txtPostDateFrom.Text, txtPostDateTo.Text, ) < 0 Then
            strMsg = "Please provide the valid post from and to dates. it should be within 30 days range"
        End If

        If (strMsg.Length > 0) Then
            ShowError(strMsg)
            Return True
        Else
            Return False
        End If

    End Function

   

End Class