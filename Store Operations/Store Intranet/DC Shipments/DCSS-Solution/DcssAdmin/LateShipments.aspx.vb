Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Partial Class LateShipments
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.DCSS.Application.Session
    Private _ds As DataSet
    Private _storeDs As DataSet
    Protected menuStr As String
    'Protected WithEvents validationSummary1 As System.Web.UI.WebControls.ValidationSummary
    'Protected WithEvents dateValidator1 As System.Web.UI.WebControls.CompareValidator
    Private _searchParams As SearchParams
    Private _searchFilter As String = ""

    'Protected WithEvents reqValidator1 As System.Web.UI.WebControls.RequiredFieldValidator

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            lblError.Text = ""
            lblError.Visible = False
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr
            If Not IsPostBack Then
                btnDrop.Attributes.Add("onclick", "return ValidateCheckedItemStr()")
                _sessionUser.FillWhseList(lstWhse, "", "---all---")

            End If
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub ShowError(ByVal msg As String)
        If msg.Trim.Length > 0 Then
            lblError.Text = msg
            lblError.Visible = True
        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            hdnCheckedItemStr.Value = ""
            dgShipment.CurrentPageIndex = 0
            BindData()
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub PrepareSearchParams()
        With _searchParams
            .PostDateFrom = txtPostDateFrom.Text
            .PostDateTo = txtPostDateTo.Text
            If IsNumeric(lstWhse.SelectedItem.Value) AndAlso lstWhse.SelectedItem.Value > 0 Then
                .WHSE = lstWhse.SelectedItem.Value
            End If
            If cbShowAll.Checked Then
                .ShowAllLateShipments = True
            Else
                .ShowAllLateShipments = False
            End If
        End With
    End Sub
    Private Sub BindData()
        Try
            PrepareSearchParams()
            _ds = _sessionUser.ListLateShipments(_searchParams)
            If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then
                dgShipment.DataSource = _ds.Tables(0)
                '_ds.Tables(0).DefaultView.Sort = SortBy.Text & " " & SortDirection.Text
                _ds.Tables(0).DefaultView.Sort = IIf(SortBy.Text.Trim.Length > 0, SortBy.Text, "StoreNum  ASC")
                If dgShipment.CurrentPageIndex > 0 AndAlso _ds.Tables(0).Rows.Count <= (dgShipment.CurrentPageIndex * dgShipment.PageSize) Then
                    dgShipment.CurrentPageIndex = dgShipment.CurrentPageIndex - 1
                End If
                dgShipment.DataBind()
                lblRecCount.Text = _ds.Tables(0).Rows.Count & " Record(s) found."
                pnlDgShipment.Visible = True
            Else
                pnlDgShipment.Visible = False
                ShowError("No records found")
            End If
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub

    Private Sub dgShipment_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgShipment.PageIndexChanged
        dgShipment.CurrentPageIndex = e.NewPageIndex
        dgShipment.EditItemIndex = -1
        BindData()
    End Sub

    Private Sub dgShipment_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgShipment.SortCommand
        'If SortBy.Text = e.SortExpression Then
        '    Select Case SortDirection.Text
        '        Case "asc"
        '            SortDirection.Text = "desc"
        '        Case "desc"
        '            SortDirection.Text = "asc"
        '    End Select
        'Else
        '    SortDirection.Text = "asc"
        'End If
        'SortBy.Text = e.SortExpression       
        SortBy.Text = UtilityManager.BuildSortExpression(SortBy.Text, e.SortExpression)
        dgShipment.EditItemIndex = -1
        btnResetSort.Visible = True
        BindData()
    End Sub
    Private Sub dgShipment_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShipment.EditCommand
        Dim s1, s2 As String
        s1 = Request("hdnCheckedItemStr")
        s2 = hdnCheckedItemStr.Value

        dgShipment.EditItemIndex = e.Item.ItemIndex
        BindData()
        Dim tb As TextBox = dgShipment.Items(e.Item.ItemIndex).Cells(6).FindControl("txtComment")
        Page.RegisterStartupScript("focus", "<script language='javascript'>" & vbCrLf & _
            vbTab & "Form1." & tb.ClientID & ".focus();" & vbCrLf & _
            "</script>")
    End Sub

    Private Sub dgShipment_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShipment.CancelCommand
        dgShipment.EditItemIndex = -1
        BindData()
    End Sub

    Private Sub dgShipment_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShipment.UpdateCommand
        Try
            Dim IgnoreLateShipmentFlag As Int16 = 0
            Dim comments As String
            Dim lblUpsTrack As WebControls.Label
            Dim cb As HtmlControls.HtmlInputCheckBox
            cb = e.Item.Cells(0).FindControl("cbDgShipment")
            If cb.Value = "" OrElse cb.Checked Then
                IgnoreLateShipmentFlag = 1
            End If

            Dim tb As TextBox = e.Item.Cells(6).FindControl("txtComment")
            lblUpsTrack = e.Item.Cells(1).FindControl("lblUpsTracking")

            comments = tb.Text
            _sessionUser.UpdateLateShipmentInfo(lblUpsTrack.Text, IgnoreLateShipmentFlag, comments)
            UpdateCheckedItemStr(lblUpsTrack.Text)
            dgShipment.EditItemIndex = -1
            BindData()
        Catch ex As DCSSAppException
            ShowError(ex.Message)
            Exit Sub
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnDrop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDrop.Click
        Dim cbCheckedStr As String = ""
        Dim arrCbChecked() As String
        Dim i As Int16 = 0
        cbCheckedStr = hdnCheckedItemStr.Value
        If cbCheckedStr.Length > 0 Then
            arrCbChecked = Split(cbCheckedStr, ",")
            For i = 0 To arrCbChecked.GetUpperBound(0)
                Try
                    _sessionUser.UpdateLateShipmentInfo(arrCbChecked(i), 1, "")

                Catch ex As DCSSAppException
                    ShowError(ex.Message)
                    Exit Sub
                Catch ex As Exception
                    Throw ex
                End Try
            Next
            hdnCheckedItemStr.Value = ""
            dgShipment.EditItemIndex = -1
            BindData()
        End If
    End Sub

    Private Sub dgShipment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgShipment.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            If e.Item.Cells(0).Text = "True" Then
                e.Item.Cells(1).Controls.Clear()
            End If
        End If
    End Sub
    Private Sub UpdateCheckedItemStr(ByVal upsTrack As String)
        hdnCheckedItemStr.Value = Replace(hdnCheckedItemStr.Value, upsTrack, "")
        hdnCheckedItemStr.Value = Replace(hdnCheckedItemStr.Value, ",,", ",")
        hdnCheckedItemStr.Value = hdnCheckedItemStr.Value.Trim(",")
    End Sub
    Private Sub exportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles exportToExcel.Click
        PrepareSearchParams()
        PrepareSearchFilter()
        Session("SearchParams") = _searchParams
        Session("SearchFilter") = _searchFilter
        Server.Transfer("ExportToExcel.aspx?qry=late")
    End Sub
    Private Sub PrepareSearchFilter()
        _searchFilter = ""
        Dim delimiter As String = " , "
        With _searchParams
            If .WHSE > 0 Then
                _searchFilter &= "WHSE=" & .WHSE & delimiter
            End If
            If IsDate(.PostDateFrom) And IsDate(.PostDateTo) Then
                _searchFilter &= "Date Range=" & .PostDateFrom & "-" & .PostDateTo & delimiter
            ElseIf IsDate(.PostDateFrom) Then
                _searchFilter &= "Date Range=" & .PostDateFrom & delimiter
            ElseIf IsDate(.PostDateTo) Then
                _searchFilter &= "Date Range=" & .PostDateTo & delimiter
            End If

            If .ShowAllLateShipments Then
                _searchFilter &= "Include Dropped Shipments = True" & delimiter
            Else
                _searchFilter &= "Include Dropped Shipments = False" & delimiter
            End If
        End With
        _searchFilter = _searchFilter.Trim
        _searchFilter = _searchFilter.Trim(delimiter.Trim)

    End Sub

    
    Private Sub lstWhse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstWhse.SelectedIndexChanged
        hdnCheckedItemStr.Value = ""
        pnlDgShipment.Visible = False
    End Sub
    Private Sub btnResetSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetSort.Click
        SortBy.Text = ""
        dgShipment.EditItemIndex = -1
        btnResetSort.Visible = False
        BindData()
    End Sub
End Class
