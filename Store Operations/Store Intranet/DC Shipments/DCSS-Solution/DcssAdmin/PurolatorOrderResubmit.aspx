﻿<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PurolatorOrderResubmit.aspx.vb" Inherits="DcssAdmin.PurolatorOrderResubmit"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<!-- #include file="include/header.inc" -->	
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="500px">
            <tr><td>&nbsp;</td></tr>
            <tr><td class="cellvaluecaption">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select Warehouse::</td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td class="cellvaluecaption">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:RadioButton runat="server"  Text ="DC-Industry, CA" ID="rdCADC" Checked="true" 
                    AutoPostBack="True" CausesValidation="True"/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:RadioButton runat="server"  Text ="DC-LaVergne, TN" ID="rdTNDC" 
                    AutoPostBack="True" CausesValidation="True"/>
            </td></tr>
            <tr><td>&nbsp;</td></tr>

            <tr>
					<td align="middle">
						<asp:Label ID="lblError" CssClass="errStyle" Runat="server" Visible="False"></asp:Label>
					</td>
				</tr>				
				<tr>
					<td class="cellvaluecaption" align="middle">
						
									Enter Carton number::
                                    <asp:TextBox ID="txtOrderNbr" CssClass="cellvalueleft" Runat="server" CausesValidation="True"></asp:TextBox>
                                     &nbsp;<asp:Button Runat=server CssClass=btnSmall ID="btnSubmit" Text="Submit" CausesValidation=False></asp:Button>
								
					</td>
				</tr>
            </table>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
