<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LateShipments.aspx.vb" Inherits="DcssAdmin.LateShipments"%>
<script runat=server language="vb">
	Public Function ShowCheckBox(byval UpsTracking as string , byval IgnoreLateShipment as boolean)
		Dim cbStr as string = ""
		IF IgnoreLateShipment THEN
			cbStr = ""
		ELSE
			cbStr = "<input type=checkbox id=""cbDgShipment""  name=""cbDgShipment""" & _
					"  value='" & UpsTracking & "'" & _
					" onclick=""UpdateCheckedItemStr()"" />"
		End If
		Return cbStr
	End Function
</script>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<!-- #include file="include/header.inc" -->
	<body>
		<!--#include file="include/calender.js"-->
		<script language=javascript>
			//-------------------------------------------------------------
			// Select all the checkboxes 
			//-------------------------------------------------------------
			function SelectAllCheckboxes(spanChk){    
				
				var theBox=spanChk
				var xState=spanChk.checked; 								
				elm=theBox.form.elements;					
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].id!=theBox.id && elm[i].id.indexOf("cbDgShipment") > -1)
					{
					//elm[i].click();
					if(elm[i].checked!=xState)
					//elm[i].click();
					elm[i].checked=xState;
					}
				
				UpdateCheckedItemStr();
			}
			//-------------------------------------------------------------
			// Update hidden variable with checked items 
			//-------------------------------------------------------------
			function UpdateCheckedItemStr(){
				var checkedStr = "";
				
				var ckAllState;
				elm=document.Form1.elements;					
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].id.indexOf("cbDgShipment") > -1 )
					{										
					if(elm[i].checked){						
						if (checkedStr.length>0) checkedStr += ",";
						checkedStr += elm[i].value ;						
					}				
					}	
				document.Form1.hdnCheckedItemStr.value = checkedStr;	
				 SyncChkAll();
			}
			//---------------------------------------------------
			// sync the chkAll chekbox status		
			//---------------------------------------------------
			function SyncChkAll(){	
				var cbTotalCnt = 0;
				var cbCheckedCnt = 0;
				elm=document.Form1.elements;
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].id.indexOf("cbDgShipment") > -1 )
				{
					cbTotalCnt += 1;					
					if(elm[i].checked){
						cbCheckedCnt += 1;											
					}				
				}				
				if(cbTotalCnt == cbCheckedCnt && cbTotalCnt > 0){
					ckAllState = true;
				}else{
					ckAllState = false;
				}
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].id.indexOf("chkAll") > -1 )
					{
						elm[i].checked = ckAllState;
					}
			}
			//-------------------------------------------------------------
			// Validate hidden variable 
			//-------------------------------------------------------------
			function ValidateCheckedItemStr(){	
				UpdateCheckedItemStr();							
				if (!document.Form1.hdnCheckedItemStr.value.length > 0) {	
					alert('Please select shipment(s) to delete'); 				
					return false;
				}else{
					if (confirm('Are you sure to drop selected shipments?')){
						return true;
					}else return false;
				}
			}
	
			//-------------------------------------------------------------
			// Initialize shipment check boxes  
			//-------------------------------------------------------------
			function InitializeCbDgShipment(){
				var checkedStr = document.Form1.hdnCheckedItemStr.value;				
				if (checkedStr.length > 0){
				var separator = ',';
				var stringArray = checkedStr.split(separator);				
				elm=document.Form1.elements;					
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].id.indexOf("cbDgShipment") > -1 )
					{	
						for (var j=0; j < stringArray.length; j++){
							if (elm[i].value == stringArray[j]){
								elm[i].checked = true;
								break;
							}
						}								
					}	
				}
				SyncChkAll()
			}
			window.onload = InitializeCbDgShipment;
		</script>
		<iframe id="CalFrame" style="DISPLAY: none; Z-INDEX: 100; WIDTH: 148px; POSITION: absolute; HEIGHT: 194px" marginWidth="0" marginHeight="0" src="include/calendar/calendar.htm" frameBorder="0" noResize scrolling="no"></iframe>
		<form id="Form1" method="post" runat="server">
			<input type=hidden name="hdnCheckedItemStr" id="hdnCheckedItemStr" runat=server value="">
			
			<asp:Label ID="SortDirection" Visible="False" Runat="server" Text="asc"></asp:Label>
			<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="middle" class="pagecaption">
						Late Shipments 
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="middle">
						<asp:Label ID="lblError" CssClass="errStyle" Runat="server" Visible="False"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="middle">
						<table width="710" border="0" cellpadding="2" cellspacing="2" style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid">
							<tr>
								<td colspan="5" align="middle">
									<asp:ValidationSummary ID="validationSummary1" CssClass="errStyle" style=" TEXT-ALIGN: center"  DisplayMode="BulletList" EnableClientScript="true" ShowMessageBox="false" ShowSummary="true" Enabled="true" Runat="server"></asp:ValidationSummary>
								</td>
							</tr>
							<tr>
								<td width="200" Class="cellvaluecaption">&nbsp;Warehouse:
									<asp:DropDownList id="lstWhse" Runat="server" CssClass="cellvalueleft" AutoPostBack=True ></asp:DropDownList> 
									
								</td>
								<td width="90" class="cellvaluecaption" align="right">Post Date From:</td>
								<td width="230"  class="cellvaluecaption"><asp:TextBox ID="txtPostDateFrom" Runat="server" CssClass="cellvalueleft" Width="65px"></asp:TextBox>
									<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar1,document.Form1.txtPostDateFrom,null,0,330)">
										<IMG id="calendar1" src="include/calendar/calendar.gif" width=34px align="absBottom" border="0" name="calendar1"></A>
										To: <asp:TextBox ID="txtPostDateTo" Runat="server" CssClass="cellvalueleft" Width="65px"></asp:TextBox>
										<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar2,document.Form1.txtPostDateTo,null,0,330)"><IMG id="calendar2" src="include/calendar/calendar.gif" width=34px align="absBottom" border="0" name="calendar2"></A>
									<asp:CompareValidator ID="dateValidator1" ControlToValidate="txtPostDateFrom" Type="Date" Display="None" EnableClientScript="true" Operator="DataTypeCheck" ErrorMessage="Invalid Post Date." CssClass="reqStyle" Enabled="true" Runat="server" ></asp:CompareValidator>
									<asp:CompareValidator ID="dateValidator2" ControlToValidate="txtPostDateTo" Type="Date" Display="None" EnableClientScript="true" Operator="DataTypeCheck" ErrorMessage="Invalid Post Date." CssClass="reqStyle" Enabled="true" Runat="server"></asp:CompareValidator>
									<asp:CompareValidator ID="Comparevalidator1" ControlToValidate="txtPostDateTo" ControlToCompare="txtPostDateFrom" Type="Date" Display="None" EnableClientScript="true" Operator="GreaterThan" ErrorMessage="'Post Date To' must be greater than 'Post Date From'." CssClass="reqStyle" Enabled="true" Runat="server"></asp:CompareValidator>
								</td>	
								<td  colspan=2>
									<asp:CheckBox ID="cbShowAll" CssClass=cellvaluewhite Runat=server Text="Include dropped shipments"></asp:CheckBox>
								</td>
								
							</tr>						
						
							<tr>
								<td colspan="5" align="middle">
									<asp:Button ID="btnSearch" Runat="server" cssclass="btnSmall" Text="Submit" CausesValidation="True"></asp:Button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
				<asp:Panel ID="pnlDgShipment" Runat=server Visible=False>
				<tr height="15">
					<td class="contentTextStrong" >&nbsp;&nbsp;<asp:Label Runat="server" ID="lblRecCount" CssClass="contentTextStrong"></asp:Label>
						&nbsp;<asp:LinkButton Runat=server ID="exportToExcel" text="Export to Excel"></asp:LinkButton>
						&nbsp;&nbsp;Sort By :: <asp:Label ID="SortBy" CssClass="cellvaluecaption" Visible="True" Runat="server"></asp:Label>
					&nbsp;<asp:LinkButton Runat=server ID="btnResetSort" CssClass=contentTextStrong Visible=False text="reset"></asp:LinkButton>
					</td>
				</tr>
				<tr>
					<td align="middle">
						<!-- data grid-->
						<!-- data grid-->
						<asp:DataGrid Runat="server" ID="dgShipment" DataKeyField="UpsTracking" Width="770px" BorderColor="#990000" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false"  AllowSorting="True" AllowPaging=True PageSize=20  PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
							<Columns>
								<asp:BoundColumn DataField="IgnoreLateShipmentFlag" SortExpression="IgnoreLateShipmentFlag" HeaderText="Dropped Item"  Visible="False" HeaderStyle-Width="10px" ReadOnly=True></asp:BoundColumn>
								<asp:TemplateColumn HeaderStyle-Width=20px>
								<HeaderTemplate>
									<asp:CheckBox id="chkAll" 
										onclick="javascript:SelectAllCheckboxes(this);" runat="server" 
										AutoPostBack="false" ToolTip="Select/Deselect All" />
								</HeaderTemplate>
								<ItemTemplate> 
									<input type=checkbox id="cbDgShipment"  name="cbDgShipment"
										runat="server" value='<%#container.dataitem("UpsTracking")%>'
										onclick="UpdateCheckedItemStr()" />
								</ItemTemplate> 
								</asp:TemplateColumn> 	
								<asp:TemplateColumn HeaderText="Tracking #"  HeaderStyle-Width="120px">
									<ItemTemplate>
										<a href="<%#Container.DataItem("TrackingURL")%>" target="_blank">
												<asp:Label Runat=server ID="lblUpsTracking" text='<%#Container.DataItem("UpsTracking")%>'></asp:Label>
										</a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="StoreNum" HeaderText="Store #" HeaderStyle-Width="40px">
									<ItemTemplate>
										<%#Container.DataItem("StoreNum").toString.PadLeft(4, "0") %>
									</ItemTemplate>
								</asp:TemplateColumn>	
								<asp:BoundColumn DataField="ShippedDateTime" SortExpression="ShippedDateTime" HeaderText="Shipped Date" DataFormatString="{0:MM/dd/yyyy}"  HeaderStyle-Width="60px" ReadOnly=True></asp:BoundColumn>
								<asp:BoundColumn DataField="OriginalScheduledDeliveryDate" SortExpression="OriginalScheduledDeliveryDate" HeaderText="Expected Delivery Date"   DataFormatString="{0:MM/dd/yyyy}"  HeaderStyle-Width="60px"  ReadOnly=True></asp:BoundColumn>
								<asp:BoundColumn DataField="DeliveredDate" SortExpression="DeliveredDate" HeaderText="Received On"   DataFormatString="{0:MM/dd/yyyy}"  HeaderStyle-Width="60px"  ReadOnly=True></asp:BoundColumn>
								<asp:BoundColumn DataField="ServiceType" SortExpression="ServiceType" HeaderText="Service Type"  HeaderStyle-Width="80px" ReadOnly=True></asp:BoundColumn>
								<asp:BoundColumn DataField="PackageType" SortExpression="PackageType" HeaderText="Package Type"  HeaderStyle-Width="80px" ReadOnly=True Visible=False></asp:BoundColumn>
								<asp:BoundColumn DataField="WHSE" SortExpression="PackageType" HeaderText="WHSE"  HeaderStyle-Width="40px" ReadOnly=True></asp:BoundColumn> 
								<asp:TemplateColumn HeaderText="Comments" HeaderStyle-Width=180px>
									<ItemTemplate>
										<%#Container.DataItem("Comments")%>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox Runat=server EnableViewState=False ID="txtComment" TextMode=MultiLine Rows=5 Width=99% MaxLength=2000 Text='<%#container.dataitem("Comments")%>' CssClass=cellvalueleft></asp:TextBox>
									</EditItemTemplate>									
								</asp:TemplateColumn>
								<asp:EditCommandColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" EditText="Edit" UpdateText="Update" CancelText="Cancel"></asp:EditCommandColumn>
								
							</Columns>
						</asp:DataGrid>
					</td>
				<tr height=5px>
					<td></td>
				</tr>
				<tr>
					<td class=cellvaluewhite >
						
										Drop <input type="checkbox" class="cellvalueleft" checked disabled> items from the list 
									&nbsp;<asp:Button Runat=server CssClass=btnSmall ID="btnDrop" Text="Drop Shipments" CausesValidation=False></asp:Button>
									
								
					</td>
				</tr>
				</asp:Panel>
			</table>
		
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
