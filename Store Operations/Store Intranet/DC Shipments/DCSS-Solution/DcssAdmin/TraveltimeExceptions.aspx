<%@ Page Language="vb" AutoEventWireup="false" Codebehind="TraveltimeExceptions.aspx.vb" Inherits="DcssAdmin.TraveltimeExceptions"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<!-- #include file="include/header.inc" -->
	<body>
		<!--#include file="include/calender.js"-->
		<iframe id="CalFrame" style="DISPLAY: none; Z-INDEX: 100; WIDTH: 148px; POSITION: absolute; HEIGHT: 194px" marginWidth="0" marginHeight="0" src="include/calendar/calendar.htm" frameBorder="0" noResize scrolling="no"></iframe>
		<form id="Form1" method="post" runat="server">
			<asp:Label ID="SortDirection" Visible="False" Runat="server" Text="asc"></asp:Label>
			<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="middle" class="pagecaption">
						UPS Traveltime Exceptions
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="middle">
						<asp:Label ID="lblError" CssClass="errStyle" Runat="server" Visible="False"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="middle">
						<table width="770" border="0" cellpadding="2" cellspacing="2" style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid">
							<tr>
								<td colspan="4" align="middle">
									<asp:ValidationSummary ID="validationSummary1" CssClass="errStyle" style=" TEXT-ALIGN: center" DisplayMode="BulletList" EnableClientScript="true" ShowMessageBox="false" ShowSummary="true" Enabled="true" Runat="server"></asp:ValidationSummary>
								</td>
							</tr>
							<tr>
								<td width="200" Class="cellvaluecaption">&nbsp;Warehouse:
									<asp:DropDownList id="lstWhse" Runat="server" CssClass="cellvalueleft" AutoPostBack="True"></asp:DropDownList>
								</td>
								<td width="90" class="cellvaluecaption" align="right">Post Date From:</td>
								<td width="230" class="cellvaluecaption"><asp:TextBox ID="txtPostDateFrom" Runat="server" CssClass="cellvalueleft" Width="65px"></asp:TextBox>
									<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar1,document.Form1.txtPostDateFrom,null,0,330)">
										<IMG id="calendar1" src="include/calendar/calendar.gif" width="34" align="absBottom" border="0" name="calendar1"></A>
									To:
									<asp:TextBox ID="txtPostDateTo" Runat="server" CssClass="cellvalueleft" Width="65px"></asp:TextBox>
									<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar2,document.Form1.txtPostDateTo,null,0,330)">
										<IMG id="calendar2" src="include/calendar/calendar.gif" width="34" align="absBottom" border="0" name="calendar2"></A>
									<asp:CompareValidator ID="dateValidator1" ControlToValidate="txtPostDateFrom" Type="Date" Display="None" EnableClientScript="true" Operator="DataTypeCheck" ErrorMessage="Invalid Post Date." CssClass="reqStyle" Enabled="true" Runat="server"></asp:CompareValidator>
									<asp:CompareValidator ID="dateValidator2" ControlToValidate="txtPostDateTo" Type="Date" Display="None" EnableClientScript="true" Operator="DataTypeCheck" ErrorMessage="Invalid Post Date." CssClass="reqStyle" Enabled="true" Runat="server"></asp:CompareValidator>
									<asp:CompareValidator ID="Comparevalidator1" ControlToValidate="txtPostDateTo" ControlToCompare="txtPostDateFrom" Type="Date" Display="None" EnableClientScript="true" Operator="GreaterThan" ErrorMessage="'Post Date To' must be greater than 'Post Date From'." CssClass="reqStyle" Enabled="true" Runat="server"></asp:CompareValidator>
								</td>
								<td  class="cellvaluecaption" align="right">Store #:
									<asp:DropDownList ID="lstStore" Runat="server" CssClass="cellvalueleft" Width="170px"></asp:DropDownList></td>
							</tr>
							<tr>
								<td colspan="4" align="middle">
									<asp:Button ID="btnSearch" Runat="server" cssclass="btnSmall" Text="Submit" CausesValidation="True"></asp:Button>
								</td>
							</tr>
					</table>
				</td></tr>
				<asp:Panel ID="pnlDgShipment" Runat=server Visible=False>
				
				<tr height="15">
					<td class="contentTextStrong" >&nbsp;&nbsp;<asp:Label Runat="server" ID="lblRecCount" CssClass="contentTextStrong"></asp:Label>
						
						<asp:LinkButton Runat=server ID="exportToExcel" text="Export to Excel"></asp:LinkButton>
						&nbsp;&nbsp;Sort By :: <asp:Label ID="SortBy" CssClass="cellvaluecaption" Visible="True" Runat="server"></asp:Label>
					&nbsp;<asp:LinkButton Runat=server ID="btnResetSort" CssClass=contentTextStrong Visible=False text="reset"></asp:LinkButton>
					</td>
				</tr>
				<tr>
					<td align="middle">
						<!-- data grid-->
						<asp:DataGrid Runat="server" ID="dgShipment" DataKeyField="UpsTracking" Width="770px" BorderColor="#990000" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false" AllowSorting="True" AllowPaging="True" PageSize="20" PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
							<Columns>
								<asp:TemplateColumn HeaderText="Tracking #" HeaderStyle-Width="120px">
									<ItemTemplate>
										<a href="<%#Container.DataItem("TrackingURL")%>" target="_blank"><%#Container.DataItem("UpsTracking") %></a>
										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="StoreNum" HeaderText="Store #" HeaderStyle-Width="40px">
									<ItemTemplate>
										<%#Container.DataItem("StoreNum").toString.PadLeft(4, "0") %>
									</ItemTemplate>
								</asp:TemplateColumn>								
								<asp:BoundColumn DataField="ShippedDateTime" DataFormatString="{0:MM/dd/yyyy}" SortExpression="ShippedDateTime" HeaderText="Shipped Date" HeaderStyle-Width="70px"></asp:BoundColumn>
								<asp:BoundColumn DataField="OriginalScheduledDeliveryDate" DataFormatString="{0:MM/dd/yyyy}" SortExpression="OriginalScheduledDeliveryDate" HeaderText="Orig. Sched. DeliveryDt" HeaderStyle-Width="120px"></asp:BoundColumn>
								<asp:BoundColumn DataField="ScheduledDeliveryDate" DataFormatString="{0:MM/dd/yyyy}" SortExpression="ScheduledDeliveryDate" HeaderText="UPS Sched. DeliveryDt" HeaderStyle-Width="120px"></asp:BoundColumn>
								<asp:BoundColumn DataField="ServiceType" SortExpression="ServiceType" HeaderText="Service Type" HeaderStyle-Width="80px"></asp:BoundColumn>
								<asp:BoundColumn DataField="WHSE" SortExpression="WHSE" HeaderText="WHSE" HeaderStyle-Width="40px"></asp:BoundColumn>
								<asp:BoundColumn DataField="Status" SortExpression="StatusCD" HeaderText="Status" HeaderStyle-Width="80px"></asp:BoundColumn>
								
							</Columns>
						</asp:DataGrid>
					</td>
					</tr>
				<tr height="5">
					<td></td>
				</tr>
			
				</asp:Panel>
						</table>
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
			</table>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
