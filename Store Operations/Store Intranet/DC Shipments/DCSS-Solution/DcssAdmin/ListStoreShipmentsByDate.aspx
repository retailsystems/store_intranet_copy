<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ListStoreShipmentsByDate.aspx.vb" Inherits="DcssAdmin.ListStoreShipmentsByDate"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<!-- #include file="include/Popupheader.inc" -->
	<script language="javascript">
		var plusImg = 'images/Plus.gif';
		var minusImg = 'images/Minus.gif';
		function showtree(lngSection){
			//alert(lngSection);
			if (eval('S' + lngSection).style.display == 'none'){
				eval('S' + lngSection).style.display = 'block';
				eval('document.Form1.I' + lngSection).src = minusImg;
			}else{
				eval('S' + lngSection).style.display = 'none';
				eval('document.Form1.I' + lngSection).src =  plusImg;
			}
		}
		function ReloadOpener(){
			//alert(window.opener.document.location);
			window.opener.document.Form1.hdnRefeshFlag.value = "1";
			window.opener.document.Form1.submit();			
		}
	</script>
	<body onload="self.focus()" onunload="ReloadOpener()">
		<form id="Form1" method="post" runat="server">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
				<tr>
					<td align="right" class="cellvaluecaption">
						&nbsp;&nbsp;<a href="#" onclick="window.print()"><img src="images/print.gif" border="0"></a>
						<a href="#" onclick="window.print()">Print</a> &nbsp;&nbsp;<a href="#" onclick="self.close()">Close</a>&nbsp;
					</td>
				</tr>
				<tr>
					<td align="center" class="pageCaption">Expected Shipments</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="center">
						<asp:Label ID="lblError" Runat="server" CssClass="ErrStyle" Visible="False"></asp:Label>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="center">
						<font class="cellvaluecaption">Store #:</font> &nbsp;
						<asp:Label Runat="server" ID="lblStoreNum" CssClass="contentTextStrong"></asp:Label>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="center">
						<font class="cellvaluecaption">Scheduled Delivery Date:</font> &nbsp;
						<asp:Label Runat="server" ID="lblDeliveryDate" CssClass="contentTextStrong"></asp:Label>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="center">
						<!-- data grid-->
						<asp:DataGrid Runat="server" ID="dgShipment" Width="600px" BorderColor="#990000" CellPadding="1"
							HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle"
							AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false" AllowSorting="True"
							PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle" >
							<Columns>
								<asp:TemplateColumn HeaderStyle-Width="45px">
									<ItemTemplate>
										<%#ShowCheckBox(Container.DataItem("StatusCD"),Container.DataItem("UpsTracking"))%>
										<%#ShowKioskIcon(Container.DataItem("KioskFlag"),kioskBoxCnt)%>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Tracking #" HeaderStyle-Width="120px">
									<ItemTemplate>
										<!--BuildTrackStr(Container.DataItem("UpsTracking"),Container.DataItem("ServiceProvider"))-->
										<%#Container.DataItem("TrackingStr")%>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="ShippedDateTime" HeaderText="Shipped Date" HeaderStyle-Width="70px" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
								<asp:BoundColumn DataField="ServiceType" HeaderText="Service Type" HeaderStyle-Width="100px"></asp:BoundColumn>
								<asp:BoundColumn DataField="WhseName" HeaderText="Shpped From" HeaderStyle-Width="90px"></asp:BoundColumn>
								<asp:BoundColumn DataField="Status" HeaderText="Status" HeaderStyle-Width="70px"></asp:BoundColumn>
								<asp:BoundColumn DataField="ReceiverName" HeaderText="Received By" HeaderStyle-Width="105px"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
			</table>
			<tr height="5px">
				<td></td>
			</tr>
			<tr>
				<td class="cellvaluewhite" align="center">
					<table width="500" cellpadding="0" cellspacing="0">
						<tr>
							<td class="cellvaluewhite">
								<asp:Button Runat="server" CssClass="btnSmall" ID="btnReceive" Text="Receive Shipment(s)" CausesValidation="False"></asp:Button>
								&nbsp;&nbsp;<asp:Button Runat="server" CssClass="btnSmall" ID="btnUnReceive" Text="UnReceive Shipment(s)"
									CausesValidation="False"></asp:Button>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<%IF kioskBoxCnt > 0 %>
			<tr>
				<td height="5"></td>
			</tr>
			<tr>
				<td class="contentTextSmall" align="left">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/Kiosk.png" width="22" height="22">
					- Kiosk shipments
				</td>
			</tr>
			<%End If%>
			<tr>
				<td height="5"></td>
			</tr>
			<tr>
				<td class="contentTextSmall" align="left">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<sup><font color="red">*</font></sup>&nbsp;HTConnect order(s)
				</td>
			</tr>
		</form>
		<!-- #include file="include/PopupFooter.inc" -->
	</body>
</HTML>
