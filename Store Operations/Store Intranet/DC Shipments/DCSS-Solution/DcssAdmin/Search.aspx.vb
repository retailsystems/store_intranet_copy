Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Partial Class Search
    Inherits System.Web.UI.Page
    'Protected WithEvents txtPackageType As System.Web.UI.WebControls.TextBox
    Private _sessionUser As HotTopic.DCSS.Application.Session
    Private _ds As DataSet
    Private _storeDs As DataSet
    Protected menuStr As String
    Private _searchParams As SearchParams
    Private _searchFilter As String = ""
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            lblError.Text = ""
            lblError.Visible = False
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr
            If Not IsPostBack Then
                _sessionUser.FillWhseList(lstWhse, "", "---all---")
                _storeDs = _sessionUser.ListAllStores
                If _storeDs.Tables(0).Rows.Count > 0 Then
                    lstStore.DataTextField = "StoreName"
                    lstStore.DataValueField = "StoreNum"
                    lstStore.DataSource = _storeDs
                    lstStore.DataBind()
                End If
                lstStore.Items.Insert(0, New ListItem("--all--", 0))
                _ds = _sessionUser.ListServiceTypes
                If _ds.Tables(0).Rows.Count > 0 Then
                    If _ds.Tables(0).Rows.Count > 0 Then
                        lstServiceType.DataTextField = "ServiceType"
                        lstServiceType.DataValueField = "ServiceType"
                        lstServiceType.DataSource = _ds
                        lstServiceType.DataBind()
                    End If
                End If
                lstServiceType.Items.Insert(0, New ListItem("--all--", ""))
            End If
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub ShowError(ByVal msg As String)
        If msg.Trim.Length > 0 Then
            lblError.Text = msg
            lblError.Visible = True
        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            dgShipment.CurrentPageIndex = 0
            BindData()
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub PrepareSearchParams()
        With _searchParams
            '.PackageType = txtPackageType.Text.Trim
            .PostDateFrom = txtPostDateFrom.Text
            .PostDateTo = txtPostDateTo.Text
            .ServiceType = lstServiceType.SelectedItem.Value
            .StatusCD = lstStatusCD.SelectedItem.Value
            .StoreNum = lstStore.SelectedItem.Value
            .TrackingNum = txtTrackingNum.Text.Trim
            '12/02/04 Sri Bajjuri added Manifest# to search params
            .ManifestNbr = txtManifestNbr.Text
            '01/14/05 TNDC enahancements
            If IsNumeric(lstWhse.SelectedItem.Value) AndAlso lstWhse.SelectedItem.Value > 0 Then
                .WHSE = lstWhse.SelectedItem.Value
            End If
        End With
    End Sub
    Private Sub BindData()
        Try
            PrepareSearchParams()
            
            _ds = _sessionUser.SearchShipments(_searchParams)
            If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then
                dgShipment.DataSource = _ds.Tables(0)
                '_ds.Tables(0).DefaultView.Sort = SortBy.Text & " " & SortDirection.Text
                _ds.Tables(0).DefaultView.Sort = IIf(SortBy.Text.Trim.Length > 0, SortBy.Text, "ShippedDateTime  ASC")
                If dgShipment.CurrentPageIndex > 0 AndAlso _ds.Tables(0).Rows.Count <= (dgShipment.CurrentPageIndex * dgShipment.PageSize) Then
                    dgShipment.CurrentPageIndex = dgShipment.CurrentPageIndex - 1
                End If
                dgShipment.DataBind()
                lblRecCount.Text = _ds.Tables(0).Rows.Count & " Record(s) found."
                pnlDgShipment.Visible = True
            Else
                pnlDgShipment.Visible = False
                ShowError("No records found")
            End If
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub

    Private Sub dgShipment_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgShipment.PageIndexChanged
        dgShipment.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub

    Private Sub dgShipment_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgShipment.SortCommand
        'If SortBy.Text = e.SortExpression Then
        '    Select Case SortDirection.Text
        '        Case "asc"
        '            SortDirection.Text = "desc"
        '        Case "desc"
        '            SortDirection.Text = "asc"
        '    End Select
        'Else
        '    SortDirection.Text = "asc"
        'End If
        'SortBy.Text = e.SortExpression

        SortBy.Text = UtilityManager.BuildSortExpression(SortBy.Text, e.SortExpression)
        btnResetSort.Visible = True
        BindData()
    End Sub

    Private Sub exportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles exportToExcel.Click
        PrepareSearchParams()
        PrepareSearchFilter()
        Session("SearchParams") = _searchParams
        Session("SearchFilter") = _searchFilter
        Server.Transfer("ExportToExcel.aspx?qry=search")
    End Sub
    Private Sub PrepareSearchFilter()
        _searchFilter = ""
        Dim delimiter As String = " , "
        With _searchParams
            If IsDate(.PostDateFrom) And IsDate(.PostDateTo) Then
                _searchFilter &= "Date Range=" & .PostDateFrom & "-" & .PostDateTo & delimiter
            ElseIf IsDate(.PostDateFrom) Then
                _searchFilter &= "Date Range=" & .PostDateFrom & delimiter
            ElseIf IsDate(.PostDateTo) Then
                _searchFilter &= "Date Range=" & .PostDateTo & delimiter
            End If
            If .ServiceType <> "" Then
                _searchFilter &= "Service Type=" & lstServiceType.SelectedItem.Text & delimiter
            End If
            If .StoreNum > 0 Then
                _searchFilter &= "Store # =" & .StoreNum.ToString.PadLeft(4, "0") & delimiter
            End If
            If .StatusCD <> "" Then
                _searchFilter &= "Status=" & lstStatusCD.SelectedItem.Text & delimiter
            End If
            If .TrackingNum <> "" Then
                _searchFilter &= "Tracking #=" & .TrackingNum & delimiter
            End If
            If .PackageType <> "" Then
                _searchFilter &= "Package Type=" & .PackageType & delimiter
            End If
            If .WHSE > 0 Then
                _searchFilter &= "WHSE=" & .WHSE & delimiter
            End If
        End With
        _searchFilter = _searchFilter.Trim
        _searchFilter = _searchFilter.Trim(delimiter.Trim)

    End Sub
    Private Sub btnResetSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetSort.Click
        SortBy.Text = ""
        'dgShipment.EditItemIndex = -1
        btnResetSort.Visible = False
        BindData()
    End Sub
End Class
