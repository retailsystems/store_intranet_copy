<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Search.aspx.vb" Inherits="DcssAdmin.Search"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<!-- #include file="include/header.inc" -->
	<body>
		<!--#include file="include/calender.js"-->
		<iframe id="CalFrame" style="DISPLAY: none; Z-INDEX: 100; WIDTH: 148px; POSITION: absolute; HEIGHT: 194px" marginWidth="0" marginHeight="0" src="include/calendar/calendar.htm" frameBorder="0" noResize scrolling="no"></iframe>
		<form id="Form1" method="post" runat="server">
			<asp:Label ID="SortBy1" Visible="False" Runat="server" Text="ShippedDateTime"></asp:Label>
			<asp:Label ID="SortDirection" Visible="False" Runat="server" Text="asc"></asp:Label>
			<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="middle" class="pagecaption">
						Search Shipments 
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="middle">
						<asp:Label ID="lblError" CssClass="errStyle" Runat="server" Visible="False"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="middle">
						<table width="750" border="0" cellpadding="2" cellspacing="2" style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid">
							<tr>
								<td colspan="4" align="middle">
									<asp:ValidationSummary ID="validationSummary1" CssClass="errStyle" style=" TEXT-ALIGN: center"  DisplayMode="BulletList" EnableClientScript="true" ShowMessageBox="false" ShowSummary="true" Enabled="true" Runat="server"></asp:ValidationSummary>
								</td>
							</tr>
							<tr>
								<td width="130" class="cellvaluecaption" align="right">Service Type:</td>
								<td width="160"><asp:DropDownList ID="lstServiceType" Width="130px" Runat="server" CssClass="cellvalueleft"></asp:DropDownList></td>
								<td width="130" class="cellvaluecaption" align="right">Post Date From:</td>
								<td width="330"  class="cellvaluecaption"><asp:TextBox ID="txtPostDateFrom" Runat="server" CssClass="cellvalueleft" Width="65px"></asp:TextBox>
									<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar1,document.Form1.txtPostDateFrom,null,0,330)">
										<IMG id="calendar1" src="include/calendar/calendar.gif" width=34px align="absBottom" border="0" name="calendar1"></A>
										To: <asp:TextBox ID="txtPostDateTo" Runat="server" CssClass="cellvalueleft" Width="65px"></asp:TextBox>
										<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar2,document.Form1.txtPostDateTo,null,0,330)"><IMG id="calendar2" src="include/calendar/calendar.gif" width=34px align="absBottom" border="0" name="calendar2"></A>
									<asp:CompareValidator ID="dateValidator1" ControlToValidate="txtPostDateFrom" Type="Date" Display="None" EnableClientScript="true" Operator="DataTypeCheck" ErrorMessage="Invalid Post Date." CssClass="reqStyle" Enabled="true" Runat="server" ></asp:CompareValidator>
									<asp:CompareValidator ID="dateValidator2" ControlToValidate="txtPostDateTo" Type="Date" Display="None" EnableClientScript="true" Operator="DataTypeCheck" ErrorMessage="Invalid Post Date." CssClass="reqStyle" Enabled="true" Runat="server"></asp:CompareValidator>
									<asp:CompareValidator ID="Comparevalidator1" ControlToValidate="txtPostDateTo" ControlToCompare="txtPostDateFrom" Type="Date" Display="None" EnableClientScript="true" Operator="GreaterThan" ErrorMessage="'Post Date To' must be greater than 'Post Date From'." CssClass="reqStyle" Enabled="true" Runat="server"></asp:CompareValidator>
								</td>								
							</tr>
							<tr>
								<td width="130" class="cellvaluecaption" align="right">Status:</td>
								<td width="160">
									<asp:DropDownList ID="lstStatusCD" Width="130px" Runat="server" CssClass="cellvalueleft">
										<asp:ListItem Selected="True" Value="">--all--</asp:ListItem>
										<asp:ListItem Value="0">Shipped</asp:ListItem>
										<asp:ListItem Value="2">UPS Delivered</asp:ListItem>
										<asp:ListItem Value="3">Received</asp:ListItem>
									</asp:DropDownList>
								</td>
								<td width="130" class="cellvaluecaption" align="right">Store #:</td>
								<td width="330"><asp:DropDownList ID="lstStore" Runat="server" CssClass="cellvalueleft"></asp:DropDownList></td>
							</tr>
							<tr>
								<td width="130" class="cellvaluecaption" align="right">Warehouse:</td>
								<td width="160">
									<asp:DropDownList id="lstWhse" Runat="server" CssClass="cellvalueleft"></asp:DropDownList> 
								</td>
								<td  colspan=2 class="cellvaluecaption" align=right >
								Manifest/Load #:
								<asp:TextBox ID="txtManifestNbr" Runat="server" CssClass="cellvalueleft" Width="125px"></asp:TextBox>
								Tracking #:
								<asp:TextBox ID="txtTrackingNum" Runat="server" CssClass="cellvalueleft" Width="130px"></asp:TextBox></td>
							</tr>
							<tr>
								<td colspan="4" align="middle">
									<asp:Button ID="btnSearch" Runat="server" cssclass="btnSmall" Text="Search" CausesValidation="True"></asp:Button>
									&nbsp;
									<input type=button name="btnClear" id="btnClear" value="Clear" class="btnSmall" onclick="location.href='Search.aspx'">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
				<asp:Panel ID="pnlDgShipment" Runat=server Visible=False>
				<tr height="15">
					<td class="contentTextStrong" >&nbsp;&nbsp;<asp:Label Runat="server" ID="lblRecCount" CssClass="contentTextStrong"></asp:Label>
						
						<asp:LinkButton Runat=server ID="exportToExcel" text="Export to Excel"></asp:LinkButton>
						&nbsp;&nbsp;Sort By :: <asp:Label ID="SortBy" CssClass="cellvaluecaption" Visible="True" Runat="server"></asp:Label>
					&nbsp;<asp:LinkButton Runat=server ID="btnResetSort" CssClass=contentTextStrong Visible=False text="reset"></asp:LinkButton>
					</td>
				</tr>
				<tr>
					<td align="middle">
						<!-- data grid-->
						<asp:DataGrid Runat="server" ID="dgShipment" DataKeyField="UpsTracking" Width="770px" BorderColor="#990000" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false" AllowSorting="True" AllowPaging="True" PageSize="20" PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
							<Columns>
								<asp:TemplateColumn HeaderText="Tracking #" HeaderStyle-Width="120px">
									<ItemTemplate>
										<a href="<%#Container.DataItem("TrackingURL")%>" target="_blank"><%#Container.DataItem("UpsTracking") %></a>
										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="StoreNum" HeaderText="Store #" HeaderStyle-Width="40px">
									<ItemTemplate>
										<%#Container.DataItem("StoreNum").toString.PadLeft(4, "0") %>
									</ItemTemplate>
								</asp:TemplateColumn>								                                
                                <asp:BoundColumn DataField="ShippedLPN" SortExpression="ShippedLPN" HeaderText="ShippedLPN" HeaderStyle-Width="40px"></asp:BoundColumn>
								<asp:BoundColumn DataField="ShippedDateTime" DataFormatString="{0:MM/dd/yyyy}" SortExpression="ShippedDateTime" HeaderText="Shipped Date" HeaderStyle-Width="70px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Carrier_DeliveredDate" DataFormatString="{0:MM/dd/yyyy}" SortExpression="CarrierDeliveredDate" HeaderText="Carrier Delivered Date" HeaderStyle-Width="70px"></asp:BoundColumn>
								<asp:BoundColumn DataField="ServiceType" SortExpression="ServiceType" HeaderText="Service Type" HeaderStyle-Width="80px"></asp:BoundColumn>
								<asp:BoundColumn DataField="WHSE" SortExpression="WHSE" HeaderText="WHSE" HeaderStyle-Width="40px"></asp:BoundColumn>
								<asp:BoundColumn DataField="Status" SortExpression="StatusCD" HeaderText="Status" HeaderStyle-Width="80px"></asp:BoundColumn>
								<asp:BoundColumn DataField="ReceiverName" SortExpression="ReceiverName" HeaderText="Received By" HeaderStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Tracking_statusDesc" SortExpression="Tracking_statusDesc" HeaderText="Tracking StatusDesc" HeaderStyle-Width="120px"></asp:BoundColumn>
								<asp:BoundColumn DataField="Comments"  HeaderText="Comments" HeaderStyle-Width="180px"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				<tr height="5">
					<td></td>
				</tr>
				</asp:Panel>
			</table>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
