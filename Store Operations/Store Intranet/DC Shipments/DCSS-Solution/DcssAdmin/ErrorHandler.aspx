<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ErrorHandler.aspx.vb" Inherits="DcssAdmin.ErrorHandler"%>
<HTML>
	<HEAD>
		<title>HotTopic</title>
		<style>
			.errStyle { FONT-WEIGHT: bold; FONT-SIZE: xx-small; COLOR: #ff0000; FONT-FAMILY: Arial; TEXT-ALIGN: left }
			</style>
	</HEAD>
	<body text="white" vLink="white" aLink="white" link="white" bgColor="black" face="arial">
		<form id="Form1" method="post" runat="server">
			<input type="hidden" id="SessionExpiryRedirectUrl" name="SessionExpiryRedirectUrl" runat="server">
			<%IF sessionExpired THEN%>
			<script language="javascript"> 
					var redirUrl = Form1.SessionExpiryRedirectUrl.value;					
					document.bgColor = "#000000";				
					if (redirUrl != ""){
						var obj = window.opener;
						
						if(obj){
						window.close();
						window.opener.location.href=redirUrl; 
						}else{
							alert('Active session expired, Please re-submit your login information!!');
							window.location.href=redirUrl;
						}
					}	
			</script>
			<table cellSpacing="0" cellPadding="0" width="500" align="center">
				<tr>
					<td colSpan="2"><br>
						<br>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="errStyle">
						<P>Active session was expired due to prolonged inactivity,&nbsp;
						You may need to re-submit your login information.
						</P>
						
					</td>
				</tr>
				
			</table>
			<%ELSE%>
			<table cellSpacing="0" cellPadding="0" width="500" align="center">
				<tr>
					<td colSpan="2"><br>
						<br>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="errStyle">
						<P>Unexpected error occured while processing your request,&nbsp;you 
							may&nbsp;contact your system administrator for further assistance.
						</P>
						<P>
							Click <a href="javascript:history.go(-1)">here</a> to go back.
						</P>
					</td>
				</tr>
				<tr>
					<td colspan="2"><br>
						&nbsp;<asp:TextBox Runat="server" TextMode="MultiLine" CssClass="cellvalueleft" Width="500" Rows="7" id="txtDetails" Visible="False"></asp:TextBox>
					</td>
				</tr>
			</table>
			<%END IF%>
		</form>
	</body>
</HTML>
