Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Partial Class ListStoreShipmentsByDate
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.DCSS.Application.Session
    Private _ds As DataSet
    Protected kioskBoxCnt As Int16 = 0
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            lblError.Text = ""
            lblError.Visible = False
            _sessionUser = New Session(HttpContext.Current)
            If Not IsPostBack Then
                lblStoreNum.Text = Request.QueryString("storeNum").ToString.PadLeft(4, "0")
                lblDeliveryDate.Text = Request.QueryString("dt")
                If Not IsDate(lblDeliveryDate.Text) Then
                    Throw New DCSSAppException("Invalid Delivery Date")
                End If
                BindData()
            End If
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub BindData()
        Try
            _ds = _sessionUser.ListStoreShipmentsByDate(lblDeliveryDate.Text, lblStoreNum.Text)
            dgShipment.DataSource = _ds
            dgShipment.DataBind()
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub ShowError(ByVal msg As String)
        If msg.Trim.Length > 0 Then
            lblError.Text = msg
            lblError.Visible = True
        End If
    End Sub
    Public Function ShowKioskIcon(ByVal KioskFlag As Boolean, ByRef kioskBoxCnt As Int16) As String
        Dim iconStr As String = ""
        Try
            If KioskFlag Then
                kioskBoxCnt += 1
                iconStr = "<img src='images/Kiosk.png' width=17 height=17>"
            End If
        Catch ex As Exception

        End Try
        Return iconStr
    End Function
    Public Function ShowCheckBox(ByVal statusID As Integer, ByVal UpsTracking As String) As String
        Dim cbStr As String = ""

        Select Case statusID
            Case ShipmentStatus.Received
                cbStr = "<input type=checkbox id='cbDgShipment'class='RedBg' name='cbDgShipment' value='" & UpsTracking & "'>"
            Case Else
                cbStr = "<input type=checkbox id='cbDgShipment' name='cbDgShipment' value='" & UpsTracking & "'>"
        End Select
        Return cbStr
    End Function
    Public Function BuildTrackStr(ByVal TrackingNum As String, ByVal serviceProvider As Int16) As String
        Select Case serviceProvider
            Case 1
                '<a href="http://wwwapps.ups.com/etracking/tracking.cgi?tracknums_displayed=1&amp;TypeOfInquiryNumber=T&amp;HTMLVersion=4.0&amp;InquiryNumber1=<%#Container.DataItem("UpsTracking") %>" target="_blank"><%#Container.DataItem("UpsTracking") %></a>
                Return "<a href=""http://wwwapps.ups.com/etracking/tracking.cgi?tracknums_displayed=1&TypeOfInquiryNumber=T&HTMLVersion=4.0&InquiryNumber1=" & TrackingNum & """ target=""_blank"">" & TrackingNum & "</a>"
            Case 4
                Return "<a href=""http://www.fedex.com/Tracking?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers=" & TrackingNum & """ target=""_blank"">" & TrackingNum & "</a>"
            Case 5
                Return "<a href=""https://www.purolator.com/purolator/ship-track/tracking-details.page?pin=" & TrackingNum & """ target=""_blank"">" & TrackingNum & "</a>"

            Case Else
                Return TrackingNum
        End Select
    End Function
    Private Sub UpdateStatus(ByVal shipmentStaus As ShipmentStatus)
        Dim cbCheckedStr As String = ""
        Dim arrCbChecked() As String
        Dim i As Int16 = 0
        If Not Request("cbDgShipment") Is Nothing Then
            cbCheckedStr = Request("cbDgShipment")
        End If
        If cbCheckedStr.Length > 0 Then
            arrCbChecked = Split(cbCheckedStr, ",")
            For i = 0 To arrCbChecked.GetUpperBound(0)
                Try
                    _sessionUser.UpdateShipmentInfo(arrCbChecked(i), shipmentStaus, "")
                    'Response.Write(arrCbChecked(i) & Request("lstStatus") & "<br>")
                Catch ex As DCSSAppException
                    ShowError(ex.Message)
                    Exit Sub
                Catch ex As Exception
                    Throw ex
                End Try
            Next
            BindData()
        End If
    End Sub

    Private Sub btnReceive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReceive.Click
        UpdateStatus(ShipmentStatus.Received)
    End Sub

    Private Sub btnUnReceive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnReceive.Click
        UpdateStatus(ShipmentStatus.Shipped)
    End Sub
End Class
