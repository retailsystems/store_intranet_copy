Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Partial Class StoreView
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.DCSS.Application.Session
    Protected WithEvents btnGo As System.Web.UI.WebControls.Button
    Protected menuStr As String
    Private _storeDs As DataSet
    Private _ds As DataSet
    Private _dv As DataView
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            lblError.Text = ""
            lblError.Visible = False
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr
            If Not IsPostBack Then
                _storeDs = _sessionUser.ListAllStores
                If _storeDs.Tables(0).Rows.Count > 0 Then
                    lstStore.DataTextField = "StoreName"
                    lstStore.DataValueField = "StoreNum"
                    lstStore.DataSource = _storeDs
                    lstStore.DataBind()
                End If
                lstStore.Items.Insert(0, New ListItem("--Select a store--", 0))
            Else
                'refresh calendar if pop-up window is closed
                If hdnRefeshFlag.Value = "1" Then
                    hdnRefeshFlag.Value = "0"
                    If shpmtCalendar.VisibleDate.Year > 1 Then
                        BindData(shpmtCalendar.VisibleDate.Month, shpmtCalendar.VisibleDate.Year)
                    Else
                        BindData(Now.Month, Now.Year)
                    End If
                End If
            End If
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub ShowError(ByVal msg As String)
        If msg.Trim.Length > 0 Then
            lblError.Text = msg
            lblError.Visible = True
        End If
    End Sub
    Private Sub BindData(ByVal mm As Integer, ByVal yy As Integer)
        _ds = _sessionUser.ListMonthlyShipmentsByStore(mm, yy, lstStore.SelectedItem.Value)
        shpmtCalendar.DataBind()
    End Sub
    Private Sub lstStore_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstStore.SelectedIndexChanged
        If lstStore.SelectedIndex > 0 Then
            'load store calendar
            shpmtCalendar.Visible = True
            shpmtCalendar.VisibleDate = Now
            BindData(Now.Month, Now.Year)

        Else
            shpmtCalendar.Visible = False
        End If
    End Sub
    Private Sub shpmtCalendar_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles shpmtCalendar.DayRender
        Dim cntStr As String = ""
        Dim snsInd As String = ""
        Dim storeNum As String = lstStore.SelectedItem.Value
        Dim dr As DataRow
        Dim i, shipTot, receivedTot, snsTot As Int16
        If e.Day.IsOtherMonth Then
            e.Cell.Controls.Clear()
            e.Cell.Height = UI.WebControls.Unit.Pixel(0)
        Else
            If _ds.Tables(0).Rows.Count > 0 Then
                _dv = _ds.Tables(0).DefaultView
                _dv.RowFilter = "scheduledDeliveryDate='" & e.Day.Date & "'"
                If _dv.Count > 0 Then
                    'MOD 1/6/04 display received shipments cnt , toltal cnt on calendar date
                    shipTot = 0
                    receivedTot = 0
                    snsTot = 0
                    For i = 0 To _dv.Count - 1
                        dr = _dv.Item(i).Row
                        If dr("StatusCd") = ShipmentStatus.Received Then
                            receivedTot = dr("cnt")
                        End If
                        shipTot += dr("cnt")
                        snsTot += dr("SnSCnt")
                    Next
                    If shipTot = receivedTot Then
                        e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#636363")
                    Else
                        e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#4682b4")
                    End If
                    If snsTot > 0 Then
                        snsInd = "&nbsp;<font style='color:red;font-size:8pt;font-weight:bold'><sup>*</sup></font>"
                    End If
                    cntStr = "<br>&nbsp;&nbsp;<a href=""javascript:OpenPop('ListStoreShipmentsBydate.aspx?dt=" & e.Day.Date & "&storeNum=" & storeNum & "',650,590)""><font class='contentTextSmallWhite'><b>" & shipTot & "&nbsp;Shipment(s)</b></font></a>" & snsInd
                    cntStr &= "<br>&nbsp;&nbsp;<font class='contentTextSmallWhite'>" & shipTot - receivedTot & "&nbsp;pending"
                    e.Cell.Controls.Add(New LiteralControl(cntStr))
                End If
                _dv.RowFilter = ""
            End If
        End If
    End Sub

    Private Sub shpmtCalendar_VisibleMonthChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MonthChangedEventArgs) Handles shpmtCalendar.VisibleMonthChanged
        BindData(e.NewDate.Month, e.NewDate.Year)
    End Sub
End Class
