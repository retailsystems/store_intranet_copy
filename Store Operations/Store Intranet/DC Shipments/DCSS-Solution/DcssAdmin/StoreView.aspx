<%@ Page Language="vb" AutoEventWireup="false" Codebehind="StoreView.aspx.vb" Inherits="DcssAdmin.StoreView"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<!-- #include file="include/header.inc" -->
	<body>
		<form id="Form1" method="post" runat="server">
		<input type="hidden" runat="server" id="hdnRefeshFlag" value="0" NAME="hdnRefeshFlag">
			<table width="695px">
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="middle"><asp:label id="lblError" Visible="False" Runat="server" CssClass="errStyle"></asp:label></td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="middle" class="cellvaluecaption">
						
						
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								
								<td  align="right" class="cellvaluecaption" >
									Store #:
						<asp:DropDownList Runat="server" CssClass="cellvalueleft" id="lstStore" AutoPostBack="True"></asp:DropDownList>&nbsp;
								</td>
								<td align="right" width="295px">
									<table width="160" cellSpacing="2" cellPadding="2" style="	BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid">
										<tr>
											<td vAlign="middle" width="15" bgColor="#4682b4"><img src="images/spacer.gif" width="15" height="1"></td>
											<td width="120" class="contentTextSmall">&nbsp;Pending Shipments</td>
										</tr>
										<tr>
											<td width="15" vAlign="middle" bgColor="#636363"><img src="images/spacer.gif" width="15" height="1"></td>
											<td width="120" class="contentTextSmall">&nbsp;All Shipments received</td>
										</tr>
										<tr>
											<td width="15" vAlign="middle"><font style='color:red;font-size:8pt;font-weight:bold'>*</font></td>
											<td width="120" class="contentTextSmall">&nbsp;HTConnect order(s)</td>
										</tr>
									</table>
								</td>
							</TR>
						</table>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="middle"><asp:calendar id="shpmtCalendar" TitleStyle-Height="20" TitleStyle-BackColor="#990000" TitleStyle-Font-Name="Arial" TitleStyle-Font-Size="10px" DayHeaderStyle-Font-Name="Arial" DayHeaderStyle-Font-Size="10px" DayHeaderStyle-BorderWidth="1" TitleStyle-Font-Bold="True" SelectionMode="None" DayHeaderStyle-Height="20" ForeColor="#ffffff" DayStyle-HorizontalAlign="Left" DayStyle-VerticalAlign="Top" DayStyle-BorderColor="#990000" BorderColor="#990000" DayStyle-Font-Size="9px" DayStyle-Height="30px" Runat="server" Width="695" DayStyle-BorderWidth="1" NextPrevFormat="ShortMonth" Visible="False"></asp:calendar></td>
				</tr>
			</table>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
