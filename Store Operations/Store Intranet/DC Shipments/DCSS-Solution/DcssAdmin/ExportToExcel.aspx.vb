Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Partial Class ExportToExcel
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.DCSS.Application.Session
    Protected _ds As New DataSet()
    Protected dr As DataRow
    Private _searchParams As SearchParams
    Private _fileName As String = "DC_Shipments.xls"
    Protected qry As String
    Protected searchFilter As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            _sessionUser = New Session(HttpContext.Current)
            If Not IsNothing(Session("SearchParams")) Then
                _searchParams = Session("SearchParams")
                Session("SearchParams") = Nothing
            End If
            If Not IsNothing(Session("SearchFilter")) Then
                searchFilter = Session("SearchFilter")
                Session("SearchFilter") = Nothing
            End If
            qry = Request("qry")
            Select Case qry
                Case "search"
                    _ds = _sessionUser.SearchShipments(_searchParams)
                    _fileName = "DCSS_SearchResults.xls"
                Case "late"
                    _ds = _sessionUser.ListLateShipments(_searchParams)
                    _fileName = "DCSS_LateShipments.xls"
                Case "pending"
                    _ds = _sessionUser.ListPendingShipments(Request("whse"))
                    _fileName = "DCSS_PendingShipments.xls"
                Case "travelEx"
                    _ds = _sessionUser.ListTravelTimeExceptions(_searchParams)
                    _fileName = "DCSS_TraveltimeExceptions.xls"
            End Select

            Response.ContentType = "Application/x-msexcel"
            Response.AppendHeader("content-disposition", _
                        "attachment; filename=" + _fileName)
            'Response.Write(_ds.GetXml.ToString)
        Catch ex As DCSSAppException
            Response.Write(ex.Message)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    

End Class
