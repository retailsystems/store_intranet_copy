﻿Imports HotTopic.DCSS.Application
Imports HotTopic.DCSS.Services
Partial Class PurolatorOrderResubmit
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.DCSS.Application.Session

    Private _ds As DataSet
    Protected menuStr As String
    Protected _whse As Int16 = 0
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            lblError.Text = ""
            lblError.Visible = False
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr
            If Not IsPostBack Then
                '_sessionUser.FillWhseList(lstWhse, "", "---all---")
                'btnUpdate.Attributes.Add("onclick", "return UpdateCheckedItemStr()")
                'BindData()
            End If
        Catch ex As DCSSAppException
            'show msg
            ShowError(ex.Message)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub ShowError(ByVal msg As String)
        If msg.Trim.Length > 0 Then
            lblError.Text = msg
            lblError.Visible = True
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click

        If rdCADC.Checked = True Then
            _whse = 999
        Else
            _whse = 997
        End If


        If txtOrderNbr.Text.Length = 0 Then
            lblError.Text = "Enter valid order number"
            lblError.Visible = True
        Else
            lblError.Text = String.Empty
            lblError.Visible = False
            Dim strErrorMsg As String = String.Empty
            strErrorMsg = _sessionUser.SubmitPurolatorOrderForReprocess(txtOrderNbr.Text.ToString().Trim(), _whse)
            If strErrorMsg.Trim().Length > 0 Then
                lblError.Text = strErrorMsg
                lblError.Visible = True
            Else
                lblError.Text = "Successfully submitted the order for reprocess"
                lblError.Visible = True
            End If
        End If
    End Sub

    Protected Sub rdCADC_CheckedChanged(sender As Object, e As EventArgs) Handles rdCADC.CheckedChanged
        rdTNDC.Checked = False
    End Sub

    Protected Sub rdTNDC_CheckedChanged(sender As Object, e As EventArgs) Handles rdTNDC.CheckedChanged
        rdCADC.Checked = False
    End Sub
End Class