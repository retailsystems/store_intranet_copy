Imports HotTopic.DCSS.Settings
Imports HotTopic.DCSS.UpsAPI
Imports System.IO
Imports System.Threading

Public Class App
    ' Entry point
    Public Shared Sub Main(ByVal args() As String)
        Dim UpsAppForm As UpsBatch = New UpsBatch()
        UpsAppForm.BeginUpsProcess()
        UpsAppForm.Close()
        UpsAppForm.Dispose()

    End Sub 'Main
End Class

Public Class UpsBatch
    Inherits System.Windows.Forms.Form
    Public settings As New AppSetting
    Private _shipDs As DataSet
    Private _apiMgr As New ApiManager
    Private _dr As DataRow
    Private _shipment As Shipment
    Private _debugMode As Boolean = False
    Private _debugStr As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        InitializeAppSettings()
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtResults As System.Windows.Forms.TextBox
    Friend WithEvents btnSubmit As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnSubmit = New System.Windows.Forms.Button
        Me.txtResults = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'btnSubmit
        '
        Me.btnSubmit.Location = New System.Drawing.Point(144, 16)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.TabIndex = 0
        Me.btnSubmit.Text = "Start!"
        '
        'txtResults
        '
        Me.txtResults.Location = New System.Drawing.Point(24, 128)
        Me.txtResults.Multiline = True
        Me.txtResults.Name = "txtResults"
        Me.txtResults.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtResults.Size = New System.Drawing.Size(416, 144)
        Me.txtResults.TabIndex = 1
        Me.txtResults.Text = ""
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(24, 96)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(128, 23)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Results:"
        '
        'UpsBatch
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(472, 390)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label1, Me.txtResults, Me.btnSubmit})
        Me.Name = "UpsBatch"
        Me.Text = "Hot Topic-UpsTools"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'txtResults.Text = HotTopic.DCSS.UpsApi.ApiManager.GetShipments
        BeginUpsProcess()
    End Sub

    Private Sub InitializeAppSettings()
        With settings
            .ConnectionString = Configuration.ConfigurationSettings.AppSettings("ConnectionString")
            .SmtpServer = Configuration.ConfigurationSettings.AppSettings("SmtpServer")
            .SmtpSender = Configuration.ConfigurationSettings.AppSettings("SmtpSender")
            .UpsXmlPostURL = Configuration.ConfigurationSettings.AppSettings("UpsXmlPostURL")
            .FedExXmlPostURL = Configuration.ConfigurationSettings.AppSettings("FedExXmlPostURL")
            .HttpPostFailLimit = Configuration.ConfigurationSettings.AppSettings("HttpPostFailLimit")
            .DcssMailReceivers = Configuration.ConfigurationSettings.AppSettings("DcssMailReceivers")
            .CancellationEmailBody = Configuration.ConfigurationSettings.AppSettings("CancellationEmailBody")
            .SuccessEmailBody = Configuration.ConfigurationSettings.AppSettings("SuccessEmailBody")
            .ProcessLog = Configuration.ConfigurationSettings.AppSettings("ProcessLog")
            If Configuration.ConfigurationSettings.AppSettings("DebugMode") = "TRUE" Then
                _debugMode = True
            End If
            .UpsXmlRequest = GetUpsXmlStr()
            .FedExXmlRequest = GetFedExXmlStr()
        End With
    End Sub

    Private Function GetUpsXmlStr() As String
        Dim xmlReq As String
        Dim objReader As New StreamReader("UpsXmlRequest.txt")
        xmlReq = objReader.ReadToEnd
        Return xmlReq
    End Function

    Private Function GetFedExXmlStr() As String
        Dim xmlReq As String
        Dim objReader As New StreamReader("FedExXmlRequest.txt")
        xmlReq = objReader.ReadToEnd
        Return xmlReq
    End Function

    Public Sub BeginUpsProcess()
        ' get shipment dataset       
        Dim res As String
        Dim sleeptTimer As Int16 = 0

        Dim arguments As String() = Environment.GetCommandLineArgs()

        Dim brand As String = arguments(1)
        Dim provider As String = arguments(2)

        Try
            If (Not IsNothing(Configuration.ConfigurationSettings.AppSettings("SleepTimer"))) AndAlso IsNumeric(Configuration.ConfigurationSettings.AppSettings("SleepTimer")) Then
                sleeptTimer = Configuration.ConfigurationSettings.AppSettings("SleepTimer")
            End If

            Console.WriteLine("Begin Process:" & Now)
            Dim i = 0
            _shipDs = _apiMgr.GetPendingShipments(brand, provider)
            _apiMgr.ProcessBeginDt = Now
            txtResults.Text = ""
            If _shipDs.Tables.Count > 0 Then
                _apiMgr.TotCnt = _shipDs.Tables(0).Rows.Count
                Console.WriteLine(_apiMgr.TotCnt & " records found")
                For Each _dr In _shipDs.Tables(0).Rows

                    _debugStr = ""
                    _shipment = New Shipment
                    With _shipment
                        .TrackingNum = _dr("UpsTracking")
                        .StoreNum = _dr("StoreNum")
                        .ScheduledDeliveryDate = _dr("ScheduledDeliveryDate")
                        .Status = _dr("StatusCD")
                        .MeterNumber = _dr("MeterNumber")
                        .UpdateFlag = False
                    End With
                    _apiMgr.shpMnt = _shipment

                    'Console.WriteLine("ServiceProveder = " & _dr("ServiceProvider"))

                    If (_dr("ServiceProvider") = 1) Then
                        ' UPS Shipments
                        'Console.WriteLine("Processing UPS Shipments")
                        _apiMgr.GetTrackingDetails()
                    ElseIf (_dr("ServiceProvider") = 5) Then
                        'Purolator Shipments
                        _apiMgr.GetPurolatorTrackingDetails()
                    Else
                        ' FedEx Shipments
                        'Console.WriteLine("Processing FedEx Shipments")
                        'Commenting the below line to take care of UPS migration
                        '_apiMgr.GetFedExTrackingDetails()
                    End If

                    'res = _apiMgr.GetUpsShipVia()
                    _debugStr &= _apiMgr.shpMnt.TrackingNum & vbTab
                    _debugStr &= _apiMgr.shpMnt.Status & vbTab
                    _debugStr &= _apiMgr.shpMnt.ScheduledDeliveryDate & vbTab
                    _debugStr &= _apiMgr.shpMnt.DeliveredDate
                    ''txtResults.Text &= _debugStr & vbNewLine
                    If _debugMode Then
                        Console.WriteLine(_debugStr & res)
                    End If
                    'Me.Refresh()
                    'Thread.CurrentThread.Sleep(1000)
                    If sleeptTimer > 0 Then
                        Thread.CurrentThread.Sleep(sleeptTimer)
                    End If
                    i += 1
                    If i Mod 100 = 0 Then
                        Console.WriteLine(i & " records processed, time elapsed: " & DateDiff(DateInterval.Second, _apiMgr.ProcessBeginDt, Now) & " sec.")
                        'Exit For
                    End If
                    If _apiMgr.HttpPostFailCnt = settings.HttpPostFailLimit Then
                        _apiMgr.SendMail(2, "Reached HTTPPOST failuerlimit, remote listener is down")
                        Environment.Exit(-1)
                        Exit For
                    End If

                Next
                _apiMgr.SendMail(1)
            End If

        Catch ex As Exception
            'txtResults.Text = ex.Message
            _apiMgr.SendMail(2, ex.Message)
            Console.WriteLine(ex.Message)
            Environment.Exit(-1)
        Finally
            Console.WriteLine("End Process:" & Now & ", Total time elapsed:" & DateDiff(DateInterval.Second, _apiMgr.ProcessBeginDt, Now) & " sec.")
        End Try
    End Sub
End Class
