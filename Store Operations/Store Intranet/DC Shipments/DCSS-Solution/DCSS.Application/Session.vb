' Static Model

Imports System.web
Imports HotTopic.DCSS.Security
Imports HotTopic.DCSS.Services

Namespace HotTopic.DCSS.Application

    Public Class Session

        Public SessionUser As New HotTopic.DCSS.Security.User()
        Public storeNum As String
        Private _context As HttpContext
        Private _cacheMgr As CacheManager
        Public MenuStr As String
        Private _appUrl As String

        ' Use this constructor to instantiate in all pages after successful login
        Public Sub New(ByVal context As HttpContext)
            Try
                _context = context
                If _context.Session("SessionUser") Is Nothing Then
                    'Throw New DCSSAppException("Unauthenticated user.")
                    Throw New SessionExpiredException("Unauthenticated user.")
                End If
                'set app url
                SetAppUrl()
                SessionUser = _context.Session("SessionUser")
                If Not IsNothing(_context.Session("StoreNum")) Then
                    storeNum = _context.Session("StoreNum")
                End If
                _cacheMgr = New CacheManager(_context)
                If IsNothing(HotTopic.DCSS.Settings.AppSetting.IsRoleBasedSecurityOn) OrElse HotTopic.DCSS.Settings.AppSetting.IsRoleBasedSecurityOn Then
                    Dim moduleId As Int16 = _cacheMgr.GetModuleId(HotTopic.DCSS.Settings.AppSetting.CacheDependencyFile, SessionUser.CurrentUserRoleId, SessionUser.CurrentProjectId, GetModuleName(_context.Request.PhysicalPath))
                    If moduleId = 0 Then

                        Throw New DCSSAppException("No authorization to access this page.")
                    Else
                        SessionUser.CurrentModuleId = moduleId
                        MenuStr = _cacheMgr.GetMenu(HotTopic.DCSS.Settings.AppSetting.CacheDependencyFile, SessionUser.CurrentUserRoleId, SessionUser.CurrentModuleId)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ' Use this constructor to authenticate user by employeeId
        Public Sub New(ByVal context As HttpContext, ByVal employeeId As String, ByVal projectName As String)
            _context = context
            Try
                'attempt login
                'padding zero to employee id
                If employeeId.Length < 5 Then
                    employeeId = employeeId.PadLeft(5, "0")
                End If
                SessionUser = SecurityManager.Login(employeeId)
                If HotTopic.DCSS.Settings.AppSetting.IsRoleBasedSecurityOn Then
                    Initialize(projectName)
                Else
                    'Set User Role
                    SessionUser.CurrentUserRoleId = GetUserRoleID(SessionUser.JobCode)
                End If
                _context.Session("SessionUser") = SessionUser
            Catch ex As Security.NoUserFoundException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Public Sub New()
        '    '10/31/2003 - No login required, check for valid store num
        '    _context = System.Web.HttpContext.Current
        '    Try
        '        If Not _context.Session("StoreNum") Is Nothing AndAlso _context.Session("StoreNum").ToString.Length > 0 Then
        '            storeNum = _context.Session("StoreNum")
        '        Else
        '            storeNum = GetStoreNum()
        '            _context.Session("StoreNum") = storeNum
        '        End If
        '    Catch ex As StoreNotFoundException
        '        Throw ex
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        ' Use this constructor to authenticate user by employeeId, password
        Public Sub New(ByVal context As HttpContext, ByVal userID As String, ByVal password As String, ByVal projectName As String)
            _context = context
            Try
                'attempt login
                'padding zero to employee id
                If userID.Length < 5 Then
                    userID = userID.PadLeft(5, "0")
                End If
                SessionUser = SecurityManager.Login(userID, password)
                Initialize(projectName)
                _context.Session("SessionUser") = SessionUser
            Catch ex As Security.PasswordException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Security.NoUserFoundException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        ' initializes project level user settings
        Private Sub Initialize(ByVal projectName As String)
            Try
                _context.Session("SessionUser") = SessionUser
                'set app url
                SetAppUrl()
                Dim obj As Object = SessionUser.UserProjects(projectName.ToLower)
                If obj Is Nothing Then
                    'no access to project
                    Throw New DCSSAppException("No authorization to access this application.")
                Else
                    Dim userProj As UserProject = DirectCast(obj, UserProject)
                    SessionUser.CurrentProjectId = userProj.ProjectId
                    SessionUser.CurrentUserRoleId = userProj.UserRoleId
                    SessionUser.CurrentDefaultPage = userProj.DefaultPage
                    _cacheMgr = New CacheManager(_context)
                End If
            Catch ex As NullReferenceException
                Throw New DCSSAppException("No authorization to access this application.")
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        'returns web page name
        Private Function GetModuleName(ByVal physicalPath As String) As String
            Dim fi As System.IO.FileInfo = New System.IO.FileInfo(physicalPath)
            Return fi.Name.Substring(0, fi.Name.IndexOf("."))
        End Function
        '
        Private Sub SetAppUrl()
            Try
                'set app url
                _appUrl = Replace(_context.Request.Url.ToString, _context.Request.RawUrl.ToString, "") & _context.Request.ApplicationPath.ToString
            Catch ex As Exception
                'do nothing
            End Try
        End Sub
        ' Checks if logged in user belongs to a store
        Public Function AuthenticateStoreUser() As Boolean
            Try
                'call function GetStoreNum()
                GetStoreNum()
                If SessionUser.StoreNum.ToString.PadLeft(4, "0") = storeNum Then
                    Return True
                Else
                    Throw New DCSSAppException("EmployeeID " & SessionUser.EmployeeId & " doesn't belong to Store #" & storeNum)
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ' Returns the storenum
        Private Function GetStoreNum() As String
            Try
                'Dim storeNum As String
                If Not _context.Session("StoreNum") Is Nothing AndAlso _context.Session("StoreNum").ToString.Length > 0 Then
                    storeNum = _context.Session("StoreNum")
                Else
                    storeNum = SecurityManager.GetStoreFromIP(_context.Request.ServerVariables("REMOTE_ADDR"))
                    If storeNum = "-1" Then
                        'Try to get store num from cookie
                        Try
                            If _context.Request.Cookies("StoreNo").Value <> "" Then
                                storeNum = _context.Request.Cookies("StoreNo").Value
                            End If
                        Catch ex As NullReferenceException
                            storeNum = "-1"
                        End Try
                    End If

                    If storeNum = "-1" Then
                        storeNum = ""
                        Throw New StoreNotFoundException("StoreNum Not Found.")
                        'To Do: remove this hardcoded value
                        'storeNum = "0001"
                    Else
                        storeNum = storeNum.PadLeft(4, "0")
                        _context.Session("StoreNum") = storeNum
                    End If
                End If
                Return storeNum
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Lists store shipments by date
        Public Function ListShipmentsByDate(ByVal deliveryDate As Date, ByVal storeNum As Int16) As DataSet
            Return WorkFlowManager.ListShipmentsByDate(deliveryDate, storeNum)
        End Function
        'This function is for Admin module use only, Lists store shipments by date
        Public Function ListStoreShipmentsByDate(ByVal deliveryDate As Date, ByVal storeNum As Int16) As DataSet
            Return WorkFlowManager.ListStoreShipmentsByDate(deliveryDate, storeNum)
        End Function
        'Lists all pending shipments
        Public Function ListPendingShipments(ByVal whse As Int16) As DataSet
            Return WorkFlowManager.ListPendingShipments(HotTopic.DCSS.Settings.AppSetting.NumDaysOld, whse)
        End Function
        'search shipments
        Public Function SearchShipments(ByVal searchParams As SearchParams) As DataSet
            Return WorkFlowManager.SearchShipments(searchParams)
        End Function
        'search shipment items
        Public Function SearchShipmentItems(ByVal store As UInt16, ByVal item As String, ByVal itemdesc As String, ByVal fromSHPDate As String, ByVal toSHPDate As String, ByVal trackingNum As String) As DataSet
            Return WorkFlowManager.SearchShipmentItems(store, item, itemdesc, fromSHPDate, toSHPDate, trackingNum)
        End Function
        'list travel matrix
        Public Function ListTravelMatrix(ByVal searchParams As StoreTravelMatrix) As DataSet
            Return WorkFlowManager.ListTravelMatrix(searchParams)
        End Function
        Public Sub UpdateTravelMatrix(ByVal travelMatrix As StoreTravelMatrix)
            WorkFlowManager.UpdateTravelMatrix(travelMatrix, SessionUser.EmployeeId)
        End Sub
        'traveltime exceptions
        Public Function ListTravelTimeExceptions(ByVal searchParams As SearchParams) As DataSet
            Return WorkFlowManager.ListTravelTimeExceptions(searchParams)
        End Function
        'late shipments
        Public Function ListLateShipments(ByVal searchParams As SearchParams) As DataSet
            Return WorkFlowManager.ListLateShipments(searchParams)
        End Function
        'Lists day wise no.of shipments made to a store in a calendar month
        Public Function GetStoreMonthlyShipments(ByVal mm As Int16, ByVal yy As Int16, ByVal storeNum As Int16) As DataSet
            Return WorkFlowManager.GetStoreMonthlyShipments(mm, yy, storeNum)
        End Function
        'this function is for admin module use and lists day wise no.of shipments made to a store in a calendar month
        Public Function ListMonthlyShipmentsByStore(ByVal mm As Int16, ByVal yy As Int16, ByVal storeNum As Int16) As DataSet
            Return WorkFlowManager.ListMonthlyShipmentsByStore(mm, yy, storeNum)
        End Function
        'returns shipment begin,end dates for next run
        Public Function GetShipMentDates() As DataSet
            Return WorkFlowManager.GetShipmentDates
        End Function
        'returns no.of shipments made from dovtree tables
        Public Function GetShipmentCount(ByVal beginDate As String, ByVal endDate As String) As Integer
            Return WorkFlowManager.GetShipmentCount(beginDate, endDate)
        End Function
        'Resubmit Purolator Order number
        Public Function SubmitPurolatorOrderForReprocess(ByVal strCartonNbr As String, ByVal _whse As Short) As String
            'the change is implemented only for TNDC
            Dim whse As WareHouse = GetWhseInfoByID(_whse)
            Return WorkFlowManager.SubmitPurolatorOrderForReprocess(whse.DbConnection, strCartonNbr, SessionUser.EmployeeId)
        End Function
        'cancels data export and sends cancellation email to application owners.
        Public Sub CancelExport(ByVal beginDate As String, ByVal endDate As String, ByVal cnt As Integer, ByVal adminCnt As Integer)
            WorkFlowManager.CancelExport(beginDate, endDate, cnt, adminCnt, SessionUser.EmployeeId, SessionUser.FirstName & " " & SessionUser.LastName)
        End Sub
        ' exports shipment data to intranet db from dovtree
        Public Sub ExportData(ByVal beginDate As String, ByVal endDate As String, ByVal cnt As Integer, ByVal adminCnt As Integer)
            WorkFlowManager.ExportData(beginDate, endDate, cnt, adminCnt, SessionUser.EmployeeId, SessionUser.FirstName & " " & SessionUser.LastName)
        End Sub
        '
        Public Sub RunDataFix()
            WorkFlowManager.RunDataFix(SessionUser.EmployeeId)
        End Sub
        Public Function ListActionsByModuleId() As DataRowView()
            Return _cacheMgr.ListActions(HotTopic.DCSS.Settings.AppSetting.CacheDependencyFile, SessionUser.CurrentUserRoleId, SessionUser.CurrentModuleId)
        End Function
        'updates shipment status
        Public Function UpdateShipmentStatus(ByVal upsTracking As String, ByVal statusCd As Int16)
            WorkFlowManager.UpdateShipmentStatus(upsTracking, statusCd, SessionUser.EmployeeId)
        End Function
        'updates shipment info
        Public Function UpdateShipmentInfo(ByVal upsTracking As String, ByVal statusCd As Int16, ByVal comments As String)
            WorkFlowManager.UpdateShipmentInfo(upsTracking, statusCd, comments, SessionUser.EmployeeId)
        End Function
        'updates late shipment info
        Public Function UpdateLateShipmentInfo(ByVal upsTracking As String, ByVal IgnoreLateShipment As Int16, ByVal comments As String)
            WorkFlowManager.UpdateLateShipmentInfo(upsTracking, IgnoreLateShipment, comments, SessionUser.EmployeeId)
        End Function
        Public Function AddManifestInfo(ByVal manifestData As ManifestData)
            WorkFlowManager.AddManifestInfo(manifestData, SessionUser.EmployeeId)
        End Function
        Public Function UpdateManifestInfoByID(ByVal manifestData As ManifestData)
            Dim whse As WareHouse = GetWhseInfoByID(manifestData.WearhouseNum)
            'WorkFlowManager.UpdateManifestInfoByID(_context.Application("WMSConnectionString"), manifestData, SessionUser.EmployeeId)
            WorkFlowManager.UpdateManifestInfoByID(whse.DbConnection, manifestData, SessionUser.EmployeeId)
        End Function
        Public Function CreateShipmentFileByManifest(ByVal manifestNbr As String, ByVal whseID As Int16)
            Dim whse As WareHouse = GetWhseInfoByID(whseID)
            'WorkFlowManager.CreateShipmentFileByManifest(_context.Application("WMSConnectionString"), manifestNbr, SessionUser.EmployeeId)
            WorkFlowManager.CreateShipmentFileByManifest(whse.DbConnection, manifestNbr, SessionUser.EmployeeId)
        End Function
        Public Function GetCoOrdinatorEmail(ByVal storeNum As Int16) As String
            'If IsNothing(_context.Session("ShipCoOrdinatorEmail")) Then
            '    _context.Session("ShipCoOrdinatorEmail") = WorkFlowManager.GetCoOrdinatorEmail(storeNum)
            'End If
            'Return _context.Session("ShipCoOrdinatorEmail")
            Return WorkFlowManager.GetCoOrdinatorEmail(storeNum)
        End Function
        Public Function ListManifest(ByVal whseID As Int16) As DataSet
            Try
                Dim whse As WareHouse = GetWhseInfoByID(whseID)
                'Return WorkFlowManager.ListManifest(_context.Application("WMSConnectionString"))
                Return WorkFlowManager.ListManifest(whse.DbConnection)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ListAllStores() As DataSet
            Try
                Return WorkFlowManager.ListAllStores()
            Catch ex As Exception
                Throw New DCSSAppException(ex.Message)
            End Try
        End Function
        Public Function ListServiceTypes() As DataSet
            Try
                Return WorkFlowManager.ListServiceTypes()
            Catch ex As Exception
                Throw New DCSSAppException(ex.Message)
            End Try
        End Function
        Public Function ListStores() As DataSet
            Try
                Dim storeDs = New DataSet()
                Select Case SessionUser.CurrentUserRoleId
                    Case UserRole.RM
                        storeDs = WorkFlowManager.ListRegionStores(SessionUser.RegionId)
                    Case UserRole.DM
                        storeDs = WorkFlowManager.ListDistrictStores(SessionUser.DistrictId)
                End Select
                Return storeDs
            Catch ex As Exception
                Throw New DCSSAppException(ex.Message)
            End Try
        End Function
        Public Sub SendMail(ByVal mailTo As String, ByVal mailSubject As String, ByVal mailBody As String, Optional ByVal ccEmail As String = "")
            Try
                WorkFlowManager.SendMail(HotTopic.DCSS.Settings.AppSetting.SmtpServer, HotTopic.DCSS.Settings.AppSetting.SmtpSender, mailTo, mailSubject, mailBody, ccEmail)
            Catch ex As Exception
                Throw New DCSSAppException(ex.Message)
            End Try
        End Sub
        Private Function GetUserRoleID(ByVal jobCode As String) As Int16
            Dim RdArrStr, DmArrStr As String
            Dim userRoleID, i As Int16
            ' UserRoleids 4-DM, 5 -Store , 6 - RD
            ' intialize userRoleID as store
            userRoleID = UserRole.Store ' default
            jobCode = jobCode.ToUpper

            RdArrStr = HotTopic.DCSS.Settings.AppSetting.RdJobCodes
            DmArrStr = HotTopic.DCSS.Settings.AppSetting.DmJobCodes
            'check if RD
            If Not RdArrStr Is Nothing And RdArrStr <> "" Then
                Dim RdArr() As String = RdArrStr.Split(",")
                If RdArr.Length > 0 Then
                    For i = 0 To RdArr.Length - 1
                        If jobCode = RdArr(i).ToUpper Then
                            userRoleID = UserRole.RM
                            Return userRoleID
                        End If
                    Next
                End If
            End If
            'check if DM
            If Not DmArrStr Is Nothing And DmArrStr <> "" Then
                Dim DmArr() As String = DmArrStr.Split(",")
                If DmArr.Length > 0 Then
                    For i = 0 To DmArr.Length - 1
                        If jobCode = DmArr(i).ToUpper Then
                            userRoleID = UserRole.DM
                            Return userRoleID
                        End If
                    Next
                End If
            End If
            Return userRoleID
        End Function

        '1/10/2005 TNDC Enhancements.
        Public Sub FillWhseList(ByRef lstWhse As UI.WebControls.DropDownList, Optional ByVal dfltValue As String = "", Optional ByVal dfltText As String = "--select one--")
            Dim ds As DataSet
            Dim dr As DataRow
            Dim dv As DataView
            Try
                'ds = MyConfig.WhseTagHelper.GetActiveWareHouseList()
                ds = _cacheMgr.ListActiveWHSE(HotTopic.DCSS.Settings.AppSetting.CacheDependencyFile)
                If Not _context.Session("isDcssAdmin") Then
                    ds.Tables(0).DefaultView.RowFilter = "Company='" & SessionUser.Company & "'"
                    If ds.Tables(0).DefaultView.Count <= 0 Then
                        ds.Tables(0).DefaultView.RowFilter = ""
                        ds.Tables(0).DefaultView.RowFilter = "WhseID=999"
                    End If
                Else
                    ds.Tables(0).DefaultView.RowFilter = ""
                End If
                dv = ds.Tables(0).DefaultView
                lstWhse.DataSource = dv
                lstWhse.DataTextField = "WhseName"
                lstWhse.DataValueField = "WhseID"
                lstWhse.DataBind()
                If lstWhse.Items.Count > 1 Then
                    lstWhse.Items.Insert(0, New UI.WebControls.ListItem(dfltText, dfltValue))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub FillCarrierList(ByRef lstCarrier As UI.WebControls.DropDownList, Optional ByVal dfltValue As String = "", Optional ByVal dfltText As String = "--select one--")
            Dim ds As DataSet
            Dim dr As DataRow
            Dim dv As DataView

            Try
                ds = _cacheMgr.ListActiveCarrier(HotTopic.DCSS.Settings.AppSetting.CacheDependencyFile)
                dv = ds.Tables(0).DefaultView
                lstCarrier.DataSource = dv
                lstCarrier.DataTextField = "CarrierName"
                lstCarrier.DataValueField = "CarrierID"
                lstCarrier.DataBind()
                If lstCarrier.Items.Count > 1 Then
                    lstCarrier.Items.Insert(0, New UI.WebControls.ListItem(dfltText, dfltValue))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetWhseInfoByID(ByVal whseID As Int16) As WareHouse
            Dim ds As DataSet
            Dim drv As DataRowView
            Dim dv As DataView
            Dim objWhse As WareHouse
            Dim whseVar As String
            Try
                whseVar = "WHSE_" & whseID
                If IsNothing(_context.Application(whseVar)) Then
                    'ds = MyConfig.WhseTagHelper.GetActiveWareHouseList()
                    ds = _cacheMgr.ListActiveWHSE(HotTopic.DCSS.Settings.AppSetting.CacheDependencyFile)
                    ds.Tables(0).DefaultView.RowFilter = "WhseID='" & whseID & "'"
                    dv = ds.Tables(0).DefaultView
                    If dv.Count > 0 Then
                        drv = dv(0)
                        'objWhse = New MyConfig.WareHouse(drv("WhseID"), drv("WhseName"), drv("DbConnectionString"), drv("IsActive"), drv("Company"))
                        With objWhse
                            .WhseID = drv("WhseID")
                            .WhseName = drv("WhseName")
                            .DbConnection = drv("DbConnectionString")
                            .Company = drv("Company")
                            .AdminEmail = drv("AdminEmail")
                            .CoOrdinatorEmail = drv("CoOrdinatorEmail")
                            .ActiveFlag = drv("isActive")
                        End With
                    End If
                    _context.Application(whseVar) = objWhse
                Else
                    objWhse = _context.Application(whseVar)
                End If
                Return objWhse
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class ' END CLASS DEFINITION Session

End Namespace ' HotTopic.ISS.Application

