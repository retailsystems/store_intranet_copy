' Static Model
Imports System.Web
Imports System.Web.Caching

Imports HotTopic.DCSS.Services
Imports HotTopic.DCSS.Security
Imports System.Data.SqlClient
Namespace HotTopic.DCSS.Application



    ' Responsible for getting and caching lookup data
    Public Class CacheManager

        Private _context As HttpContext

        Friend Sub New(ByVal context As HttpContext)
            _context = context
        End Sub


        Public Function ListActions(ByVal dependency As String, ByVal userRoleId As Int16, ByVal moduleId As Int16) As DataRowView()
            Dim controlType As Int16 = 0 'non-menu = 0?
            Dim dv As DataView = GetControlView(dependency, moduleId)
            dv.Sort = "UserRoleId, ControlType" 'for given role, give all Menu types
            Return dv.FindRows(New Object() {userRoleId, controlType})
        End Function

        Public Function GetModuleId(ByVal dependency As String, ByVal userRoleId As Int16, ByVal projectId As Int16, ByVal moduleName As String) As Int16
            Dim dt As DataTable
            Dim cacheName As String = projectId.ToString() + "Modules"
            If IsNothing(_context.Cache(cacheName)) Then
                dt = SecurityManager.ListModulesByProjectId(projectId)
                _context.Cache.Insert(cacheName, dt, _
                    New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)


            Else
                dt = _context.Cache(cacheName)
            End If
            Dim dv As DataView = New DataView(dt)
            dv.Sort = "ModuleName, UserRoleId"
            Dim rowIndex As Int32 = dv.Find(New Object() {moduleName, userRoleId})
            If rowIndex = -1 Then
                Return 0 'no module or no access
            Else
                Return Convert.ToInt16(dv(rowIndex)("ModuleId"))
            End If
        End Function

        Private Function GetControlView(ByVal dependency As String, ByVal moduleId As Int16) As DataView
            Dim dt As DataTable
            Dim cacheName As String = moduleId.ToString() + "Controls"
            If IsNothing(_context.Cache(cacheName)) Then
                dt = SecurityManager.ListControlsByModuleId(moduleId)
                _context.Cache.Insert(cacheName, dt, _
                    New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
            Else
                dt = _context.Cache(cacheName)
            End If
            Dim dv As DataView = New DataView(dt)
            Return dv
        End Function
        Public Function GetMenu(ByVal dependency As String, ByVal userRoleId As Int16, ByVal moduleId As Int16) As String
            Dim controlType As Int16 = 1 'menu = 1?
            Dim dv As DataView = GetControlView(dependency, moduleId)
            dv.Sort = "UserRoleId, ControlType" 'for given role, give all Menu types
            'Dim foundRows As DataRowView() = dv.FindRows(New Object() {userRoleId, controlType})

            '***********************************************************************
            '                            MENU 
            ' Cache menuString per UserRole as reportdistro doesn't have page specific menu.
            '***********************************************************************
            Dim cacheName As String
            cacheName = "CachedMenu" & userRoleId
            If IsNothing(_context.Cache(cacheName)) Then
                'If IsNothing(_context.Session("MenuStr")) Then
                dv.RowFilter = "UserRoleId=" & userRoleId & " and ControlType=1 and ParentControlId =0"
                Dim menuHeader, menuTree As String
                If dv.Count > 0 Then
                    menuHeader = "<table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#246178' ><tr> "


                    'TODO: loop thru found rows and build menu
                    'build headerMenu
                    Dim dvChild As DataView
                    Dim dr, drChild As DataRow
                    Dim drv, drvChild As DataRowView
                    dvChild = dv
                    'For Each drv In dv
                    '    dr = drv.Row
                    '    menuHeader += "<td align='center' bgcolor='#246178' onClick=""window.location='" & dr("ControlUrl") & "'"" class='white79' width='150px' id='MOD_" & dr("ControlId") & "' onMouseOver=""ChangeBgColor(this,'in')"" onMouseOut=""ChangeBgColor(this,'out')"" >" & _
                    '            "<a href='" & dr("ControlUrl") & "'   onFocus='this.blur()' style='text-decoration: none; color: #FFFFFF'><b>" & dr("ControlName") & "</b></a></td><td bgcolor='#000000' width=1></td>"

                    'Next
                    For Each drv In dv
                        dr = drv.Row
                        dvChild.RowFilter = "UserRoleId=" & userRoleId & " and ControlType=1 and ParentControlId =" & dr("ControlID")
                        'hide the root node if no child nodes found
                        If dvChild.Count > 0 Then
                            menuHeader += "<td align='center' bgcolor='#246178' class='menuRegular' width='130' id='MOD_" & dr("ControlId") & "' onmouseover=""ChangeBgColor(this,'in');setMenu('MOD_" & dr("ControlId") & "', 'MENU_" & dr("ControlId") & "')""   onMouseOut=""ChangeBgColor(this,'out')"">" & _
                                       "<a href='#'   onFocus='this.blur()' style='text-decoration: none; color: #FFFFFF'><b>" & dr("ControlName") & "</b></a></td><td bgcolor='#000000' width=1></td>" & _
                                       "<td bgcolor='#000000' width=1></td>"
                            'build treeNode
                            menuTree += "<table border='0' cellpadding=0  cellspacing=0  bgcolor=#548597 class='menu' id='MENU_" & dr("ControlId") & "' width='150'  onmouseout='hideMenu()' >" & _
                                        "<tr><td width=5></td><td><table border='0' cellpadding=0  cellspacing=0 width=100% >"
                            For Each drvChild In dvChild
                                drChild = drvChild.Row
                                menuTree += "<tr><td nowrap height=15 class='dl' id='td1' onmouseover=""this.style.background='#000000'"" onmouseout=""this.style.background='#548597'"">" & _
                                            "&nbsp;&nbsp;<a href='" & drChild("ControlUrl") & "'   class='dl' title=''>" & drChild("ControlName") & "</a></td></tr>" & _
                                            "<tr><td><img src='../" & "Images/wspacer.gif' border='0' width='100%' height='1px'></td></tr>"

                            Next
                            menuTree += "</table></td><td width=5></td></tr></table>"
                        Else
                            menuHeader += "<td align='center' bgcolor='#246178' onClick=""window.location='" & dr("ControlUrl") & "'"" class='menuRegular' width='150px' id='MOD_" & dr("ControlId") & "' onMouseOver=""ChangeBgColor(this,'in');hideMenu()"" onMouseOut=""ChangeBgColor(this,'out')"" >" & _
                                    "<a href='" & dr("ControlUrl") & "'   onFocus='this.blur()' style='text-decoration: none; color: #FFFFFF'><b>" & dr("ControlName") & "</b></a></td><td bgcolor='#000000' width=1></td>"
                        End If
                    Next
                    menuHeader += "<td onMouseOver=""hideMenu()"" align=right valign=bottom>&nbsp;<a href=""javascript:PrintMe()""><img src=""images/print.gif"" alt=""Print"" border=0></a>&nbsp;&nbsp;</td></tr></table>"
                End If
                _context.Cache.Insert(cacheName, menuHeader & menuTree, _
                   New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
                '_context.Session("MenuStr") = menuHeader & menuTree
            End If
            Return _context.Cache(cacheName)
            'Return menuHeader & menuTree
            'Return _context.Session("MenuStr")
            '***********************************************************************
            '                            MENU 
            '***********************************************************************
        End Function
        Public Function GetFiscalMonthDateRange(ByVal dependency As String, ByRef fm As FiscalMonth)
            Try
                Dim cacheName As String = fm.MonthNum & "_" & fm.Year
                If IsNothing(_context.Cache(cacheName)) Then
                    Dim arParms() As SqlParameter = New SqlParameter(3) {}

                    arParms(0) = New SqlParameter("@MonthNum", SqlDbType.Int)
                    arParms(0).Value = fm.MonthNum
                    arParms(1) = New SqlParameter("@FiscalYear", SqlDbType.Int)
                    arParms(1).Value = fm.Year
                    arParms(2) = New SqlParameter("@BeginDate", SqlDbType.DateTime)
                    arParms(2).Direction = ParameterDirection.Output
                    arParms(3) = New SqlParameter("@EndDate", SqlDbType.DateTime)
                    arParms(3).Direction = ParameterDirection.Output
                    SQLDataManager.GetInstance().Execute("RSP_GetFiscalMonthBeginEndDates", arParms)

                    With fm
                        .BeginDate = arParms(2).Value
                        .EndDate = arParms(3).Value
                    End With
                    _context.Cache.Insert(cacheName, fm, _
                    New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
                Else
                    fm = _context.Cache(cacheName)
                End If
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ListActiveWHSE(ByVal dependency As String) As DataSet
            Dim ds As DataSet
            Dim cacheName As String = "WHSE"
            Try
                If IsNothing(_context.Cache(cacheName)) Then
                    Dim arParms() As SqlParameter = New SqlParameter() {}
                    ds = SQLDataManager.GetInstance().GetDataSet("DCSP_ListActiveWHSE", arParms)
                    _context.Cache.Insert(cacheName, ds, _
                        New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
                Else
                    ds = _context.Cache(cacheName)
                End If

                Return ds
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ListActiveCarrier(ByVal dependency As String) As DataSet
            Dim ds As DataSet
            Dim cacheName As String = "Carrier"
            Try
                If IsNothing(_context.Cache(cacheName)) Then
                    Dim arParms() As SqlParameter = New SqlParameter() {}
                    ds = SQLDataManager.GetInstance().GetDataSet("DCSP_ListActiveCarrier", arParms)
                    _context.Cache.Insert(cacheName, ds, _
                        New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
                Else
                    ds = _context.Cache(cacheName)
                End If

                Return ds
            Catch ex As SqlException
                Throw New DCSSAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        '1/10/2005 TNDC Enhancements.
        'Public Function ListAllWhse(ByVal dependency As String) As DataSet
        '    Dim cacheName As String = "WHSE"
        '    Dim ds As New DataSet()
        '    Dim dt As DataTable = New DataTable("WHSE")
        '    Dim dr As DataRow
        '    Try
        '        If IsNothing(_context.Cache(cacheName)) Then
        '            'Add City Of Industry DC
        '            dr = dt.NewRow
        '            dr(0) = "999"
        '            dr(1) = "999-Industry,CA"
        '            dt.Rows.Add(dr)

        '            'Add TNDC
        '            dr = dt.NewRow
        '            dr(0) = "997"
        '            dr(1) = "997-LaVergne,TN"
        '            dt.Rows.Add(dr)

        '            ds.Tables.Add(dt)
        '            _context.Cache.Insert(cacheName, ds, _
        '           New CacheDependency(dependency), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration)
        '        Else
        '            ds = _context.Cache(cacheName)
        '        End If

        '        Return ds
        '    Catch ex As SqlException
        '        Throw New DCSSAppException(ex.Message)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

    End Class ' END CLASS DEFINITION CacheManager
    <Serializable()> _
    Public Structure FiscalMonth
        Public MonthNum As Int16
        Public Year As Int16
        Public BeginDate As Date
        Public EndDate As Date
    End Structure
End Namespace ' HotTopic.RD.Application

