Imports System
Imports System.Web
Imports System.Xml
Imports System.Configuration
Imports System.Enum

Namespace HotTopic.DCSS.Application.MyConfig


    Public Class MyConfigSectionHandler
        Implements IConfigurationSectionHandler

        Friend Function Create(ByVal parent As Object, _
                               ByVal configContext As Object, _
                               ByVal section As XmlNode) As Object _
            Implements IConfigurationSectionHandler.Create
            Dim nodWhse As XmlNode
            Dim whseList As New ArrayList()
            For Each nodWhse In section
                Dim WhseID As Int16 = 0
                Dim WhseName As String = ""
                Dim DbConnectionString As String = ""
                Dim wmsLocalFileDir As String = ""
                Dim ActiveFlag As Boolean = 0
                Dim Company As String = ""
                ConfigHelper.GetElementIntegerValue(nodWhse, "ID", WhseID)
                ConfigHelper.GetElementStringValue(nodWhse, "Name", WhseName)
                ConfigHelper.GetElementStringValue(nodWhse, "DbConnection", DbConnectionString)
                ConfigHelper.GetElementBooleanValue(nodWhse, "IsActive", ActiveFlag)
                ConfigHelper.GetElementStringValue(nodWhse, "Company", Company)
                whseList.Add(New WareHouse(WhseID, WhseName, DbConnectionString, ActiveFlag, Company))
            Next
            Return whseList
        End Function
    End Class

    Public Class WareHouse
        Private _whseID As Int16
        Private _whseName As String
        Private _dbConnection As String
        Private _activeFlag As Boolean
        Private _company As String

        Public Sub New(ByVal whseID As Int16, _
                       ByVal whseName As String, _
                        ByVal dbConnection As String, _
                        ByVal activeFlag As Boolean, _
                        ByVal company As String)
            _whseID = whseID
            _whseName = whseName
            _dbConnection = dbConnection

            _activeFlag = activeFlag
            _company = company
        End Sub

        Public ReadOnly Property WhseID() As Int16
            Get
                Return _whseID
            End Get

        End Property

        Public ReadOnly Property WhseName() As String
            Get
                Return _whseName
            End Get
        End Property
        Public ReadOnly Property DbConnection() As String
            Get
                Return _dbConnection
            End Get
        End Property
        Public ReadOnly Property Company() As String
            Get
                Return _company
            End Get
        End Property
        Public ReadOnly Property ActiveFlag() As Boolean
            Get
                Return _activeFlag
            End Get
        End Property

    End Class

    Friend Class ConfigHelper
        Public Shared Function GetEnumValue(ByVal node As XmlNode, _
                                            ByVal attribute As String, _
                                            ByVal enumType As Type, _
                                            ByRef val As Integer) As XmlNode
            Dim a As XmlNode = node.Attributes.RemoveNamedItem(attribute)
            If (a Is Nothing) Then
                Throw New ConfigurationException("Attribute required: " + attribute)
            End If

            If IsDefined(enumType, a.Value) Then
                val = CType(Parse(enumType, a.Value), Integer)
            Else
                Throw New ConfigurationException("Invalid Level: '" + a.Value + "'", a)
            End If

            Return a
        End Function

        Public Shared Sub GetStringValue(ByVal node As XmlNode, _
                                         ByVal attribute As String, _
                                         ByRef val As String)
            Dim a As XmlNode = node.Attributes.RemoveNamedItem(attribute)
            If (a Is Nothing) Then
                Throw New ConfigurationException("Attribute required: " + attribute)
            Else
                val = a.Value
            End If
        End Sub
        Public Shared Sub GetElementStringValue(ByVal node As XmlNode, _
                                        ByVal elm As String, _
                                        ByRef val As String)
            Dim a As XmlNode = node.SelectSingleNode(elm)

            If (a Is Nothing) Then
                Throw New ConfigurationException("Element required: " + elm)
            Else
                val = a.InnerText
            End If
        End Sub
        Public Shared Sub GetElementIntegerValue(ByVal node As XmlNode, _
                                        ByVal elm As String, _
                                        ByRef val As Int32)
            Dim a As XmlNode = node.SelectSingleNode(elm)

            If (a Is Nothing) Then
                Throw New ConfigurationException("Element required: " + elm)
            Else
                If IsNumeric(a.InnerText) Then
                    val = a.InnerText
                Else
                    Throw New ConfigurationException("Invalid integer data in Element: " + elm)
                End If
            End If
        End Sub
        Public Shared Sub GetElementBooleanValue(ByVal node As XmlNode, _
                                       ByVal elm As String, _
                                       ByRef val As Boolean)
            Dim a As XmlNode = node.SelectSingleNode(elm)

            If (a Is Nothing) Then
                Throw New ConfigurationException("Element required: " + elm)
            Else
                If a.InnerText = 0 OrElse a.InnerText = 1 Then
                    val = a.InnerText
                Else
                    Throw New ConfigurationException("Invalid boolean data in Element: " + elm)
                End If
            End If
        End Sub
    End Class
    Public Class WhseTagHelper
        Public Shared Function GetActiveWareHouseList() As DataSet
            Dim whseList As New ArrayList()
            Dim ds As New DataSet()
            Dim whse As WareHouse
            Dim dt As DataTable = New DataTable("WHSE")
            Dim dr As DataRow
            If Not IsNothing(System.Web.HttpContext.Current.Application("WhseList")) Then
                ds = System.Web.HttpContext.Current.Application("WhseList")
            Else
                dt.Columns.Add("WhseID")
                dt.Columns.Add("WhseName")
                dt.Columns.Add("DbConnectionString")
                'dt.Columns.Add("WmsLocalFileDir")
                dt.Columns.Add("IsActive")
                dt.Columns.Add("Company")
                Dim _context As System.Web.HttpContext = System.Web.HttpContext.Current
                whseList = System.Web.HttpContext.Current.GetConfig("system.web/WhseList")
                For Each whse In whseList
                    If whse.ActiveFlag Then
                        dr = dt.NewRow
                        With whse
                            dr(0) = .WhseID
                            dr(1) = .WhseName
                            dr(2) = .DbConnection
                            'dr(3) = .WmsLocalFileDir
                            dr(3) = .ActiveFlag
                            dr(4) = .Company
                        End With
                        dt.Rows.Add(dr)
                    End If
                Next
                ds.Tables.Add(dt)
                System.Web.HttpContext.Current.Application("WhseList") = ds
            End If
            Return ds
        End Function
    End Class
End Namespace
