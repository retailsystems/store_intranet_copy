﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace DCSS.PurolatorAPI.WS
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://HotTopic.Purolator/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Service : System.Web.Services.WebService
    {

        [WebMethod]
        public void CallTrackPackagesByPin(ref PWSClient.ShipmentInfo shipinfo)
        {
            TrackingClient.PWSClient client = new TrackingClient.PWSClient();
            client.CallTrackPackagesByPin(ref shipinfo);
        }
    }
}