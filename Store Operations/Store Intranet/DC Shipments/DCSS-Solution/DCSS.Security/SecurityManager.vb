' Static Model
Imports HotTopic.DCSS.Services
Imports System.Data.SqlClient
Namespace HotTopic.DCSS.Security

    Public Class SecurityManager

        Public Overloads Shared Function Login(ByVal employeeId As String, _
             ByVal password As String) As User
            Dim authUser As New User()
            Dim dr As SqlDataReader
            Try

                ' Set report parameters (1 input) 
                Dim arParms() As SqlParameter = New SqlParameter(2) {}

                arParms(0) = New SqlParameter("@employeeId", SqlDbType.VarChar, 10)
                arParms(0).Value = employeeId
                arParms(1) = New SqlParameter("@password", SqlDbType.VarChar, 15)
                arParms(1).Value = password
                arParms(2) = New SqlParameter("@retValue", SqlDbType.Int)
                arParms(2).Direction = ParameterDirection.Output

                dr = SQLSecurityManager.GetInstance().GetDataReader("ssp_LoginUser", arParms)
                If arParms(2).Value = 0 Then
                    While dr.Read
                        With authUser
                            .UserId = dr.GetInt32(0)
                            .EmployeeId = employeeId
                            .FirstName = dr.GetString(5)
                            .LastName = dr.GetString(3)
                            .StoreNum = UtilityManager.NullToInteger(dr(7))
                            .JobCode = dr.GetString(6)
                            .DistrictId = UtilityManager.NullToString(dr(8))
                            .RegionId = UtilityManager.NullToString(dr(9))
                            .IsNotIndividual = dr.GetBoolean(10)
                            .Company = UtilityManager.NullToString(dr(15))
                        End With
                    End While

                    Return authUser
                ElseIf arParms(2).Value = -1 Then 'wrong password
                    Throw New PasswordException("Invalid password.")
                ElseIf arParms(2).Value = -2 Then 'wrong empid
                    Throw New NoUserFoundException("Invalid Employee Id.")
                End If

            Catch ex As Exception
                Throw ex 'exception shld contain reason for login failure
            Finally
                If Not dr Is Nothing Then
                    dr.Close()
                End If
            End Try
        End Function
        Public Overloads Shared Function Login(ByVal employeeId As String) As User
            Dim authUser As New User()
            Dim dr As SqlDataReader
            Try

                ' Set report parameters (1 input) 
                Dim arParms() As SqlParameter = New SqlParameter(1) {}

                arParms(0) = New SqlParameter("@employeeId", SqlDbType.VarChar, 10)
                arParms(0).Value = employeeId
                arParms(1) = New SqlParameter("@retValue", SqlDbType.Int)
                arParms(1).Direction = ParameterDirection.Output

                dr = SQLSecurityManager.GetInstance().GetDataReader("ssp_LoginByEmpID", arParms)
                If arParms(1).Value = 0 Then
                    While dr.Read
                        With authUser
                            .UserId = dr.GetInt32(0)
                            .EmployeeId = employeeId
                            .FirstName = dr.GetString(5)
                            .LastName = dr.GetString(3)

                            .StoreNum = UtilityManager.NullToInteger(dr(7))
                            .JobCode = UtilityManager.NullToString(dr(6)).Trim.ToUpper
                            .DistrictId = UtilityManager.NullToString(dr(8))
                            .RegionId = UtilityManager.NullToString(dr(9))
                            .IsNotIndividual = dr.GetBoolean(10)
                        End With
                    End While

                    Return authUser
                ElseIf arParms(1).Value = -2 Then 'wrong empid
                    Throw New NoUserFoundException("Invalid Employee Id.")
                End If

            Catch ex As Exception
                Throw ex 'exception shld contain reason for login failure
            Finally
                If Not dr Is Nothing Then
                    dr.Close()
                End If
            End Try
        End Function
        'Public Overloads Shared Function Login(ByVal employeeId As String) As User
        '    Dim authUser As New User()
        '    Dim dr As SqlDataReader
        '    Try
        '        'TODO: fix login routine with new scurity model.

        '        ' Set report parameters (1 input) 
        '        Dim arParms() As SqlParameter = New SqlParameter(0) {}

        '        arParms(0) = New SqlParameter("@EmployeeId", SqlDbType.VarChar, 30)
        '        arParms(0).Value = employeeId

        '        dr = SQLDataManager.GetInstance().GetDataReader("RSP_GetUserInfo", arParms)

        '        While dr.Read
        '            With authUser
        '                .UserId = dr.GetString(0)
        '                .EmployeeId = employeeId
        '                .FirstName = dr.GetString(1)
        '                .LastName = dr.GetString(2)

        '                'If IsNumeric(Trim(dr.GetString(3))) Then
        '                '    .StoreNum = Convert.ToInt32(Trim(dr.GetString(3)))
        '                'Else
        '                '    .StoreNum = 0
        '                'End If
        '                .StoreNum = UtilityManager.RemoveLeadingZero(Trim(dr.GetString(3)))
        '                '.UserRole = dr.GetString(4)
        '                '.OwnerId = dr.GetString(5)
        '                '.OwnerScope = dr.GetInt32(6)
        '            End With
        '        End While

        '        Return authUser
        '    Catch ex As Exception
        '        Throw ex 'exception shld contain reason for login failure
        '    Finally
        '        dr.Close()
        '    End Try
        'End Function

        Public Shared Function ListProjectsByUserId(ByVal userId As Int32, Optional ByVal solutionId As Int16 = 0) As DataTable
            Try
                Dim ds As DataSet
                Dim dt As DataTable
                Dim arParms() As SqlParameter

                If solutionId = 0 Then
                    arParms = New SqlParameter(0) {}
                    arParms(0) = New SqlParameter("@userId", SqlDbType.Int)
                    arParms(0).Value = userId
                Else
                    arParms = New SqlParameter(1) {}
                    arParms(0) = New SqlParameter("@userId", SqlDbType.Int)
                    arParms(0).Value = userId
                    arParms(1) = New SqlParameter("@solutionId", SqlDbType.Int)
                    arParms(1).Value = solutionId

                End If

                Try
                    ds = SQLSecurityManager.GetInstance().GetDataSet("ssp_ListProjectsByUserId", arParms)
                    dt = ds.Tables(0)
                    Return dt
                Catch ex As Exception
                    Throw ex
                Finally
                    If Not ds Is Nothing Then
                        ds.Dispose()
                    End If
                End Try
            Catch ex As Exception
                Throw ex 'exception shld contain reason for login failure
            End Try

        End Function


        Public Shared Function ListModulesByProjectId(ByVal projectId As Int16) As DataTable
            Try
                Dim ds As DataSet
                Dim dt As DataTable
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@projectId", SqlDbType.SmallInt)
                arParms(0).Value = projectId

                Try
                    ds = SQLSecurityManager.GetInstance().GetDataSet("ssp_ListModulesByProjectId", arParms)
                    dt = ds.Tables(0)
                    Return dt
                Catch ex As Exception
                    Throw ex
                Finally
                    If Not ds Is Nothing Then
                        ds.Dispose()
                    End If
                End Try
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function ListControlsByModuleId(ByVal moduleId As Int16) As DataTable
            Try
                Dim ds As DataSet

                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@moduleId", SqlDbType.SmallInt)
                arParms(0).Value = moduleId

                Try
                    ds = SQLSecurityManager.GetInstance().GetDataSet("ssp_ListControlsByModuleId", arParms)
                    Return ds.Tables(0)
                Catch ex As Exception
                    Throw ex
                Finally
                    If Not ds Is Nothing Then
                        ds.Dispose()
                    End If
                End Try
            Catch ex As Exception
                Throw ex 'exception shld contain reason for login failure
            End Try

        End Function
        Public Shared Function ListUsersByRoleId(ByVal UserRoleID As Int16, ByVal ProjectID As Int16) As DataTable
            Try
                Dim ds As DataSet

                Dim arParms() As SqlParameter = New SqlParameter(1) {}
                arParms(0) = New SqlParameter("@UserRoleID", SqlDbType.SmallInt)
                arParms(0).Value = UserRoleID
                arParms(1) = New SqlParameter("@ProjectID", SqlDbType.SmallInt)
                arParms(1).Value = ProjectID

                Try
                    ds = SQLSecurityManager.GetInstance().GetDataSet("ssp_GetAllUsersByRole", arParms)
                    Return ds.Tables(0)
                Catch ex As Exception
                    Throw ex
                Finally
                    If Not ds Is Nothing Then
                        ds.Dispose()
                    End If
                End Try
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Shared Function GetStoreFromIP(ByVal ClientIP As String) As String
            Dim storeNum As String
            Dim StoreIP() As String
            Dim StoreSubnetMask() As String
            Dim strStoreSubnet As String
            StoreIP = Split(ClientIP, ".")
            StoreSubnetMask = Split(HotTopic.DCSS.Settings.AppSetting.StoreSubnetMask, ".")
            strStoreSubnet = StoreIP(0) And StoreSubnetMask(0)
            strStoreSubnet = strStoreSubnet & "."
            strStoreSubnet = strStoreSubnet & (StoreIP(1) And StoreSubnetMask(1))
            strStoreSubnet = strStoreSubnet & "."
            strStoreSubnet = strStoreSubnet & (StoreIP(2) And StoreSubnetMask(2))
            strStoreSubnet = strStoreSubnet & "."
            strStoreSubnet = strStoreSubnet & (StoreIP(3) And StoreSubnetMask(3))

            Dim arParms() As SqlParameter = New SqlParameter(1) {}

            arParms(0) = New SqlParameter("@StoreSubnet", SqlDbType.VarChar, 15)
            arParms(0).Value = strStoreSubnet
            arParms(1) = New SqlParameter("@StoreNum", SqlDbType.Int)
            arParms(1).Direction = ParameterDirection.Output
            SQLDataManager.GetInstance().Execute("spphGetStoreFromSubnet", arParms)
            storeNum = UtilityManager.NullToString(arParms(1).Value)
            Return storeNum
        End Function
    End Class ' END CLASS DEFINITION SecurityManager

    Public Class PasswordException
        Inherits ApplicationException

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        Public Sub New(ByVal message As String, ByVal inner As Exception)
            MyBase.New(message, inner)
        End Sub 'New

    End Class
    Public Class NoUserFoundException
        Inherits ApplicationException

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        Public Sub New(ByVal message As String, ByVal inner As Exception)
            MyBase.New(message, inner)
        End Sub 'New

    End Class
End Namespace ' HotTopic.RD.Security

