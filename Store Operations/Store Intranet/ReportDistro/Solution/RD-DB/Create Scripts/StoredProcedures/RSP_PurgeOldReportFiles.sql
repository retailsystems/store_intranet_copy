IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_PurgeOldReportFiles')
	BEGIN
		PRINT 'Dropping Procedure RSP_PurgeOldReportFiles'
		DROP  Procedure  RSP_PurgeOldReportFiles
	END

GO

PRINT 'Creating Procedure RSP_PurgeOldReportFiles'
GO
CREATE Procedure RSP_PurgeOldReportFiles
	/* Param List */
	@purge_cutoff_date datetime
AS

/******************************************************************************
**		File: RSP_PurgeOldReportFiles.sql
**		Name: RSP_PurgeOldReportFiles
**		Desc: Purges ReportFiles & Reportsubmissions older than passed in cut off date.
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 11/17/2004
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    12/07/2004	Sri Bajjuri			Load old data in to archive tables
*******************************************************************************/
Print ('*******************************************************************************')
Print ('Purging ReportDistro data older than ' + convert(varchar,@purge_cutoff_date)) 
Print ('*******************************************************************************')
Print ('Purging data from ReportFileAccess table')

INSERT INTO Archive_ReportFileAccess
	SELECT RFA.* FROM ReportFileAccess RFA
	INNER JOIN ReportFile RF ON RF.ReportFileID = RFA.ReportFileID
	INNER JOIN  reportSubmission RS	ON RS.ReportSubmissionID = RF.ReportSubmissionID
	LEFT OUTER JOIN Archive_ReportFileAccess ARFA ON RFA.ReportFileID = ARFA.ReportFileID AND RFA.EmployeeID =  ARFA.EmployeeID
	WHERE RS.FiscalweekEndingDate <= @purge_cutoff_date
	AND ARFA.ReportFileID IS NULL
	

DELETE ReportFileAccess 
FROM ReportFileAccess RFA
	INNER JOIN ReportFile RF ON RF.ReportFileID = RFA.ReportFileID
	INNER JOIN  reportSubmission RS	ON RS.ReportSubmissionID = RF.ReportSubmissionID
	WHERE RS.FiscalweekEndingDate <= @purge_cutoff_date


Print ('Purging data from ReportFile table')

INSERT INTO Archive_ReportFile
	SELECT RF.* FROM ReportFile RF 
	INNER JOIN  reportSubmission RS	ON RS.ReportSubmissionID = RF.ReportSubmissionID
	LEFT OUTER JOIN Archive_ReportFile ARF ON RF.ReportFileId = ARF.ReportFileId
	WHERE RS.FiscalweekEndingDate <= @purge_cutoff_date
	AND ARF.ReportFileId IS NULL
	
DELETE ReportFile
FROM ReportFile RF INNER JOIN  reportSubmission RS
	ON RS.ReportSubmissionID = RF.ReportSubmissionID
	WHERE RS.FiscalweekEndingDate <= @purge_cutoff_date

Print ('Purging data from ReportSubmission approval table')

INSERT INTO Archive_ReportSubmissionApproval
SELECT RSA.* FROM ReportSubmissionApproval RSA 
	INNER JOIN  reportSubmission RS	ON RS.ReportSubmissionID = RSA.ReportSubmissionID
	LEFT OUTER JOIN Archive_ReportSubmissionApproval ARSA 
	ON RSA.ReportSubmissionID = ARSA.ReportSubmissionID AND RSA.EmployeeId = ARSA.EmployeeId
	WHERE RS.FiscalweekEndingDate <= @purge_cutoff_date
	AND ARSA.ReportSubmissionID IS NULL
	
DELETE ReportSubmissionApproval 	
FROM ReportSubmissionApproval RSA INNER JOIN  reportSubmission RS
	ON RS.ReportSubmissionID = RSA.ReportSubmissionID
	WHERE RS.FiscalweekEndingDate <= @purge_cutoff_date

Print ('Purging data from ReportSubmission table')

INSERT INTO Archive_reportSubmission
	SELECT RS.* FROM reportSubmission RS
	LEFT OUTER JOIN Archive_reportSubmission ARS ON RS.ReportSubmissionID = ARS.ReportSubmissionID
	where RS.FiscalweekEndingDate <= @purge_cutoff_date
	AND ARS.ReportSubmissionID IS NULL
	
DELETE FROM reportSubmission where FiscalweekEndingDate <= @purge_cutoff_date



GO

GRANT EXEC ON RSP_PurgeOldReportFiles TO RdsApp

GO
