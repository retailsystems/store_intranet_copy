IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_AddReportFileStoreOwners')
	BEGIN
		PRINT 'Dropping Procedure RSP_AddReportFileStoreOwners'
		DROP  Procedure  RSP_AddReportFileStoreOwners
	END

GO

PRINT 'Creating Procedure RSP_AddReportFileStoreOwners'
GO
CREATE Procedure RSP_AddReportFileStoreOwners
	@reportFileId	int
	,@reportID		int
	,@reportDate	datetime
	,@contentScope	smallint
	,@userId		varchar(20)
	,@regionId		varchar(5) = null
	,@districtId	varchar(5) = null
	,@storeNum		int = null
AS

/******************************************************************************
**		File: RSP_AddReportFileStoreOwners.sql
**		Name: RSP_AddReportFileStoreOwners
**		Desc: Adds all stores who owns report
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Jeric Saez
**		Date: 6/23/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

	SET NOCOUNT ON
/*
	DECLARE	@reportDate datetime
			,@reportID int
			,@contentScope varchar(50)
	
	SELECT	@reportID = R.ReportID
			,@reportDate = R.ReportDate
			,@contentScope = L.LookupText
	FROM	ReportFile RF INNER JOIN 
			ReportSubmission RS ON RF.ReportSubmissionId = RS.ReportSubmissionId INNER JOIN
			Report R ON RS.ReportID = R.ReportID INNER JOIN
			Lookup L ON (R.ContentScopeType = L.LookUpValue AND L.LookupType = 'ContentScopeType')
	WHERE	RF.ReportFileID = @ReportFileID
*/	

	DELETE	ReportFileStoreOwner
	WHERE	ReportFileId = @reportFileId

	IF (@contentScope = 1)--'Company'
	BEGIN
		INSERT	ReportFileStoreOwner (
				ReportFileID,
				StoreNum,
				InsertBy,
				InsertDate)
		SELECT	@reportFileId
				,StoreNum
				,@userId
				,GETDATE()
		FROM	StoreDistrict SD INNER JOIN
				Company C ON SD.CompanyId = C.CompanyId INNER JOIN
				ReportCompanyOwner RC ON C.CompanyId = RC.CompanyId AND RC.ReportId = @reportId
		WHERE	SD.CDStoreFlag = RC.CDStoresFlag
		AND		SD.EffectivityStartDate <= @reportDate
		AND		(SD.EffectivityEndDate IS NULL OR SD.EffectivityEndDate >= @reportDate)
		
	END
	
	IF (@contentScope = 2)--'Region'
	BEGIN
		INSERT	ReportFileStoreOwner (
				ReportFileID,
				StoreNum,
				InsertBy,
				InsertDate)
		SELECT	@reportFileId
				,StoreNum
				,@userId
				,GETDATE()
		FROM	StoreDistrict SD INNER JOIN
				Company C ON SD.CompanyId = C.CompanyId INNER JOIN
				ReportCompanyOwner RC ON C.CompanyId = RC.CompanyId AND RC.ReportId = @reportId
		WHERE	SD.RegionId = @regionId
		AND		SD.CDStoreFlag = RC.CDStoresFlag
		AND		SD.EffectivityStartDate <= @reportDate
		AND		(SD.EffectivityEndDate IS NULL OR SD.EffectivityEndDate >= @reportDate)
	END

	IF (@contentScope = 3)--'District'
	BEGIN
		INSERT	ReportFileStoreOwner (
				ReportFileID,
				StoreNum,
				InsertBy,
				InsertDate)
		SELECT	@reportFileId
				,StoreNum
				,@userId
				,GETDATE()
		FROM	StoreDistrict SD INNER JOIN
				Company C ON SD.CompanyId = C.CompanyId INNER JOIN
				ReportCompanyOwner RC ON C.CompanyId = RC.CompanyId AND RC.ReportId = @reportId
		WHERE	SD.DistrictId = @districtId
		AND		SD.CDStoreFlag = RC.CDStoresFlag
		AND		SD.EffectivityStartDate <= @reportDate
		AND		(SD.EffectivityEndDate IS NULL OR SD.EffectivityEndDate >= @reportDate)
	END

	IF (@contentScope = 4)--'Store'
	BEGIN
		INSERT	ReportFileStoreOwner (
				ReportFileID,
				StoreNum,
				InsertBy,
				InsertDate)
		VALUES	(@reportFileId
				,@storeNum
				,@userId
				,GETDATE())
	END
GO

GRANT EXEC ON RSP_AddReportFileStoreOwners TO RDSApp

GO
