IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetReportSubmissionById')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetReportSubmissionById'
		DROP  Procedure  RSP_GetReportSubmissionById
	END

GO

PRINT 'Creating Procedure RSP_GetReportSubmissionById'
GO
CREATE Procedure RSP_GetReportSubmissionById
	@reportSubmissionId int
	/* Param List */
AS

/******************************************************************************
**		File: RSP_GetReportSubmissionById.sql
**		Name: RSP_GetReportSubmissionById
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
select ReportSubmissionId,rs.ReportId,ReportTitle,FiscalYear,FiscalWeek,ReportDate,rs.StagingPath,
		rs.LocationPath,EffectivityStartDate,EffectivityEndDate,Comment,Status
		,FiscalWeekEndingDate,MdReportType,MdEffectiveDate,MdPcNum
		from ReportSubmission rs join report rpt on rs.reportid = rpt.reportID		
		 where ReportSubmissionId = @ReportSubmissionId


GO

GRANT EXEC ON RSP_GetReportSubmissionById TO RdsApp

GO
