SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_UpdateStoreDistrict]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_UpdateStoreDistrict]
GO

CREATE PROCEDURE dbo.RSP_UpdateStoreDistrict
	@StoreNum int,	
	@RegionID int,
	@DistrictID int,
	@CompanyName varchar(30),
	@deleted bit

/******************************************************************************
**		File: RSP_UpdateStoreDistrict.sql
**		Name: RSP_UpdateStoreDistrict
**		Desc:  This storedprocedure  Add/Update StoreDistrict  information 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:  trigger STORE_TriggerReportStoreDistrict from HotTopic2_dev, Torrid database.
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportID
**	
**		Auth: Sri Bajjuri
**		Date: 6/11/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
AS
	SET NOCOUNT ON
	declare @errCode int, @CompanyID int, @curRegionID int, @curDistrictID int
	
	select @errCode  = 0
	select @CompanyID = CompanyID from  Company where CompanyName =@CompanyName
	if @deleted = 1
		begin
		update StoreDistrict
					set EffectivityEndDate = getDate()					
					where  StoreNum = @StoreNum and EffectivityEndDate is null
		if @@error <> 0
					select @errCode  = -1
		end
	else
	BEGIN
	if exists(select StoreNum from StoreDistrict where StoreNum = @StoreNum)
		begin
			
			select @curDistrictID = 0
			select @curRegionID = 0
			select @curDistrictID = DistrictID, @curRegionID = RegionID from StoreDistrict where  StoreNum = @StoreNum and EffectivityEndDate is null
			if (@curDistrictID <>  @DistrictId or @curRegionID <> @regionID)
				begin
				-- set the expirydate to old record
				update StoreDistrict
					set EffectivityEndDate = getDate()
					where  StoreNum = @StoreNum and EffectivityEndDate is null
				-- add new record	
				insert into StoreDistrict (CompanyID,RegionID,DistrictId,StoreNum,EffectivityStartDate)
				values(@CompanyID,@regionID,@DistrictId,@StoreNum,getDate())
					if @@error <> 0
					select @errCode  = -1
				end
		end
	else
		begin
			
			insert into StoreDistrict (CompanyID,RegionID,DistrictId,StoreNum,EffectivityStartDate)
				values(@CompanyID,@regionID,@DistrictId,@StoreNum,getDate())
				
				if @@error <> 0
					select @errCode  = -1
				
		end
	END
	return @errCode
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

