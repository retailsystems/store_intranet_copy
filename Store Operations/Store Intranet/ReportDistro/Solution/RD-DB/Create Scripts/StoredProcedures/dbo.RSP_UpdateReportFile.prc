SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_UpdateReportFile]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_UpdateReportFile]
GO


CREATE PROCEDURE dbo.RSP_UpdateReportFile
		@ReportFileID int OUTPUT,
		@ReportSubmissionID int ,
		@ArchivedFlag bit ,		
		@FileName varchar(50),
		@OwnerId varchar(5),
		@ContentScopeType smallint,
		@UserId varchar(20)	
			

/******************************************************************************
**		File: RSP_UpdateReportFile.sql
**		Name: RSP_UpdateReportFile
**		Desc:   This storedprocedure  Add/Update ReportFile information and returns
				 reportfileid	upon success.
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportSubmissionID
**	
**		Auth: Sri Bajjuri
**		Date: 6/10/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/


AS

 SET NOCOUNT ON 
 
if @ReportFileID > 0
	--Update Existing Report
	BEGIN
	update ReportFile
		set 
				ReportSubmissionID = @ReportSubmissionID,				
				FileName = @FileName,
				ArchivedFlag = @ArchivedFlag,					
				OwnerId = @OwnerId,
				ContentScopeType = @ContentScopeType,
				UpdateBy =@UserId,
				UpdateDate= getdate()
				where ReportFileID = @reportFileID
		
				
				if @@error != 0 
					begin						
						RAISERROR ('Failed to Update Report File: RSP_UpdateReportFile.', 16, 3)
						RETURN -1
					end
		
		
		
	END
else
	BEGIN
	
		INSERT INTO ReportFile 
				(				
				ReportSubmissionID,				
				FileName,				
				ArchivedFlag,							
				OwnerId,
				ContentScopeType,
				InsertBy,
				InsertDate
				)
		values(
				@ReportSubmissionID,			
				@FileName ,
				@ArchivedFlag ,	
				@OwnerId,						
				@ContentScopeType,
				@UserId,
				getdate()
			)
		
		if @@error = 0 
			begin
				select @ReportFileID = @@identity
			end
		else
			begin
						RAISERROR ('Failed to Add Report File: RSP_UpdateReportFile.', 16, 3)
						RETURN -1
			end
		
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

