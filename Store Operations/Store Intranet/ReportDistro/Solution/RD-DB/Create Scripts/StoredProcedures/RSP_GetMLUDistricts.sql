IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetMLUDistricts')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetMLUDistricts'
		DROP  Procedure  RSP_GetMLUDistricts
	END

GO

PRINT 'Creating Procedure RSP_GetMLUDistricts'
GO
CREATE Procedure RSP_GetMLUDistricts
	/* Param List */
	@EmployeeID varchar(20)
AS

/******************************************************************************
**		File: RSP_GetMLUDistricts.sql
**		Name: RSP_GetMLUDistricts
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
CREATE TABLE #MyTempRegion
			(RegionID varchar(2) not null)			

DECLARE @MarketID int
SELECT @MarketID = MarketID FROM  HtSecurity_Dev.dbo.MarketOfficers WHERE EmployeeID = @EmployeeID
INSERT #MyTempRegion EXECUTE   HtSecurity_Dev.dbo.ssp_GetRegionsByMarketID  @MarketID

SELECT Distinct Sd.RegionID,DistrictID FROM StoreDistrict Sd
	INNER JOIN #MyTempRegion R ON Sd.RegionID = R.RegionID
	WHERE (Sd.EffectivityEndDate > getDate() OR  Sd.EffectivityEndDate is NUll)
	ORDER BY Sd.RegionID,DistrictID

DROP TABLE 	#MyTempRegion	

GO

GRANT EXEC ON RSP_GetMLUDistricts TO RdsApp

GO
