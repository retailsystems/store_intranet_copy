IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetFilesBySubmissionID')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetFilesBySubmissionID'
		DROP  Procedure  RSP_GetFilesBySubmissionID
	END

GO

PRINT 'Creating Procedure RSP_GetFilesBySubmissionID'
GO
CREATE Procedure RSP_GetFilesBySubmissionID
	@ReportSubmissionID int
AS

/******************************************************************************
**		File: RSP_GetFilesBySubmissionID.sql
**		Name: RSP_GetFilesBySubmissionID
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 7/8/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT  rf.ReportFileID, rf.FileName,(rs.LocationPath +  convert(varchar,rs.FiscalYear) + '\' + convert(varchar,rs.FiscalWeek) + '\' + convert(varchar,rs.ReportId) + '\' + rf.FileName) as FilePath
	FROM reportFile rf INNER JOIN ReportSubmission rs ON rf.ReportSubmissionID = rs.ReportSubmissionId AND rs.ReportSubmissionID=@ReportSubmissionID
	


GO

GRANT EXEC ON RSP_GetFilesBySubmissionID TO RdsApp

GO
