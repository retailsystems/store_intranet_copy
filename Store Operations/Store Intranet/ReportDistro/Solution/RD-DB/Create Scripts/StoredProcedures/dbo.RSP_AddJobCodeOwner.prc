SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_AddJobCodeOwner]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_AddJobCodeOwner]
GO

CREATE PROCEDURE dbo.RSP_AddJobCodeOwner
	@ReportID int,
	@companyID int,
	@jobcode varchar(30),
	@UserId varchar(30),
	@oldJobcode varchar(30) = ''

/******************************************************************************
**		File: RSP_AddJobCodeOwner.sql
**		Name: RSP_AddJobCodeOwner
**		Desc:  This storedprocedure  Adds ReportJobCode Owner information 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportID
**	
**		Auth: Sri Bajjuri
**		Date: 6/10/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
AS
	SET NOCOUNT ON
	
	if  exists(select ReportID from ReportJobCodeOwner where ReportID = @reportID and CompanyID = @companyID and JobCode = @oldJobCode)
	begin
		if not exists(select ReportID from ReportJobCodeOwner where ReportID = @reportID and CompanyID = @companyID and JobCode = @jobCode)
			begin
				UPDATE ReportJobCodeOwner 
					SET Jobcode = @jobCode,
						UpdateBy = @UserId,
						UpdateDate = getdate()
					WHERE ReportID = @reportID and CompanyID = @companyID and JobCode = @oldJobCode
					
			end
		else
			begin
				RAISERROR ('Duplicate Report Owner : RSP_AddJobCodeOwner.', 16, 3)
				RETURN -1
			end	
	end
	else
	begin
		if not exists(select ReportID from ReportJobCodeOwner where ReportID = @reportID and CompanyID = @companyID and JobCode = @jobCode)
			begin
				insert into ReportJobCodeOwner (ReportID,CompanyID,Jobcode,InsertBY,InsertDate)
					values(@reportID,@companyID,@jobCode,@UserId,getDate())
								
			end
		else
			begin
				RAISERROR ('Duplicate  Report Owner : RSP_AddJobCodeOwner.', 16, 3)
				RETURN -1
			end
	end	
	if @@error != 0 
	begin						
		RAISERROR ('Failed to Add/Update Report Owners : RSP_AddJobCodeOwner.', 16, 3)
		RETURN -1
	end	
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

