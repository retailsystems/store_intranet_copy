SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_UpdateSubmissionApproval]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_UpdateSubmissionApproval]
GO

CREATE PROCEDURE dbo.RSP_UpdateSubmissionApproval
	@ReportSubmissionID int,
	@ReportApproverID int,
	@status int,	
	@submitBy varchar(30),
	@Comment ntext

/******************************************************************************
**		File: RSP_UpdateSubmissionApproval.sql
**		Name: RSP_UpdateSubmissionApproval
**		Desc:  This storedprocedure  Add/Update ReportSubmissionApproval information 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportID
**	
**		Auth: Sri Bajjuri
**		Date: 6/11/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
AS
	SET NOCOUNT ON
	declare @errCode int
	select @errCode  = 0
	if exists(select ReportSubmissionID from ReportSubmissionApproval where ReportSubmissionID = @ReportSubmissionID and ReportApproverID = @ReportApproverID)
		begin
			update ReportSubmissionApproval
				set Status = @Status,
					Comment = @Comment, 
					UpdateBy = @SubmitBy,
					UpdateDate = getDate()
					where ReportSubmissionID = @ReportSubmissionID and ReportApproverID = @ReportApproverID
					if @@error <> 0
					select @errCode  = -1
		end
	else
		begin
			insert into ReportSubmissionApproval 
				(
				ReportSubmissionID,
				ReportApproverID,
				Status,
				Comment,
				InsertBy,
				InsertDate
				)
				values
				(
				@ReportSubmissionID,
				@ReportApproverID,
				@Status,
				@Comment,
				@SubmitBy,
				getDate()
				)				
				
				if @@error <> 0
					select @errCode  = -1
				
		end
		
	select @errCode
GO

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXEC ON RSP_UpdateSubmissionApproval TO RdsApp

GO
