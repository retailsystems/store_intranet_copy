IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_ListStores')
	BEGIN
		PRINT 'Dropping Procedure RSP_ListStores'
		DROP  Procedure  RSP_ListStores
	END

GO

PRINT 'Creating Procedure RSP_ListStores'
GO
CREATE Procedure RSP_ListStores
	@CompanyFilter int = 0
	/* Param List */
AS

/******************************************************************************
**		File: 
**		Name: RSP_ListStores
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 12/18/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
IF @CompanyFilter > 0 
	SELECT StoreNum, Region, District, CompanyName,StoreName
	FROM RV_StoreInfo WHERE (Deleted IS NULL OR Deleted=0) 
	AND CompanyID = @CompanyFilter
	ORDER BY StoreNum
ELSE
	SELECT StoreNum, Region, District, CompanyName,StoreName
	FROM RV_StoreInfo WHERE (Deleted IS NULL OR Deleted=0) ORDER BY StoreNum

GO

GRANT EXEC ON RSP_ListStores TO RdsApp

GO
