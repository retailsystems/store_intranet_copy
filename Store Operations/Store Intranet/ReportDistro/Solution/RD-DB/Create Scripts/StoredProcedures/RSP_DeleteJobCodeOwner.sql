IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_DeleteJobCodeOwner')
	BEGIN
		PRINT 'Dropping Procedure RSP_DeleteJobCodeOwner'
		DROP  Procedure  RSP_DeleteJobCodeOwner
	END

GO

PRINT 'Creating Procedure RSP_DeleteJobCodeOwner'
GO
CREATE Procedure RSP_DeleteJobCodeOwner
	@ReportID int,
	@companyID int,
	@jobcode varchar(30)
	/* Param List */
AS

/******************************************************************************
**		File: RSP_DeleteJobCodeOwner.sql
**		Name: RSP_DeleteJobCodeOwner
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 6/25/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    7/17/2003		Sri Bajjui			Calling RSP_ResetReportCompleteFlag to reset report status
*******************************************************************************/

	DELETE FROM ReportJobCodeOwner where ReportID = @reportID and CompanyID = @companyID and JobCode = @jobCode
	
	if @@error != 0 
	begin						
		RAISERROR ('Failed to Delete Report Owners : RSP_AddJobCodeOwner.', 16, 3)
		RETURN -1
	end	
	else
	begin
		-- reset the report complete flag
		EXEC RSP_ResetReportCompleteFlag @reportID
	end

GO

GRANT EXEC ON RSP_DeleteJobCodeOwner TO RdsApp

GO
