IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_DeleteSubCompanyOwnersByReport')
	BEGIN
		PRINT 'Dropping Procedure RSP_DeleteSubCompanyOwnersByReport'
		DROP  Procedure  RSP_DeleteSubCompanyOwnersByReport
	END

GO

PRINT 'Creating Procedure RSP_DeleteSubCompanyOwnersByReport'
GO
CREATE Procedure RSP_DeleteSubCompanyOwnersByReport
	@ReportID int
AS

/******************************************************************************
**		File: 
**		Name: RSP_DeleteSubCompanyOwnersByReport
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

DELETE FROM ReportSubCompanyOwner WHERE ReportID = @ReportID
IF @@ERROR != 0 
		BEGIN
			RAISERROR ('Failed to Delete ReportSubCompanyOwners', 16, 3)
			RETURN -1
		END
	RETURN 0

GO

GRANT EXEC ON RSP_DeleteSubCompanyOwnersByReport TO RdsApp

GO
