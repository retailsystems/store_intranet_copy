SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_AddApprovers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_AddApprovers]
GO

CREATE PROCEDURE dbo.RSP_AddApprovers
	@ReportID int,
	@approverStr varchar(2000),
	@submitBy varchar(30)

/******************************************************************************
**		File: RSP_AddApprovers.sql
**		Name: RSP_AddApprovers
**		Desc:  This storedprocedure  Adds ReportJobCode Owner information 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input								Output
**     ----------							-----------
**		@ReportID,@approverStr,@submitBy
**	
**		Auth: Sri Bajjuri
**		Date: 6/10/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
AS
	SET NOCOUNT ON
	declare @errCode int, @EmployeeID varchar(20), @RouteOrder int, @ReportApproverID int, @Deleted bit
	select @errCode  = 0
	
	BEGIN TRAN AddApprovers
	
	-- create cursor
	Declare ApproverCur cursor
		for select EmployeeID,RouteOrder from dbo.RFN_ApproverstrToTable (@approverStr)
	open ApproverCur
	fetch next from ApproverCur into @EmployeeID, @RouteOrder
	while @@FETCH_STATUS = 0
		begin
			
			if not exists(select EmployeeID from ReportApprover where EmployeeID= @EmployeeID and ReportID = @ReportID)
				begin
				insert into ReportApprover (ReportID,EmployeeID,RouteOrder,InsertBY,InsertDate)
						values(@ReportID,@EmployeeID,@RouteOrder,@SubmitBy,getDate())
				if @@ERROR <> 0
					BEGIN
						ROLLBACK TRAN AddApprovers
						RAISERROR('RSP_AddApprovers: Error inserting to Approver', 16, 1)
						RETURN -1
					END
				end
			else
				begin
					update ReportApprover set						
					    RouteOrder = @RouteOrder,
					    UpdateBy = @SubmitBy,
					    UpdateDate = getDate() 
					    where EmployeeID= @EmployeeID and ReportID = @ReportID
					if @@ERROR <> 0
						BEGIN
							ROLLBACK TRAN AddApprovers
							RAISERROR('RSP_AddApprovers: Error updating to Approver', 16, 1)
							RETURN -1
						END
				end		
		
		fetch next from ApproverCur into @EmployeeID, @RouteOrder
		end
	close ApproverCur
	deallocate ApproverCur
	
	Delete from ReportApprover
					    where ReportID = @ReportID and EmployeeID not in(
							select EmployeeID from dbo.RFN_ApproverstrToTable (@approverStr))
				if @@ERROR <> 0
						BEGIN
							ROLLBACK TRAN AddApprovers
							RAISERROR('RSP_AddApprovers: Error updating to Approver', 16, 1)
							RETURN -1
						END
	COMMIT TRAN AddApprovers
	
	return 0
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

