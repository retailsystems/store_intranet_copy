SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_GetCurrentReports]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_GetCurrentReports]
GO

CREATE PROCEDURE dbo.RSP_GetCurrentReports
	@StoreNum int,
	@grpBy int, -- 1 grp by Title , 2- grp by Fiscal week
	@employeeId varchar(30)
AS
SET NOCOUNT ON 

/******************************************************************************
**		File: RSP_GetCurrentReports.sql
**		Name: RSP_GetCurrentReports
**		Desc:  This storedprocedure  Adds ReportJobCode Owner information 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input								Output
**     ----------							-----------
**		@StoreNum
**	
**		Auth: Sri Bajjuri
**		Date: 6/12/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
/*
	Select rf.* from RV_CurrentReportFile rf inner join ReportFileStoreOwner rfo
		on rf.ReportFileID = rfo.ReportFileID and rfo.StoreNum = @storeNum 
		
*/
if @StoreNum > 0 
begin
	if @grpBy = 2 
		Select rf.ReportFileID, rf.FileName,r.ReportID, r.ReportTitle, rs.locationPath,rs.FiscalWeek ,rs.FiscalYear, rs.Comment,convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId
				from ReportFile rf join ReportSubmission rs on rs.ReportSubmissionId = rf.ReportSubmissionID
				join Report r on rs.ReportID = r.ReportID
				where rf.OwnerId = @StoreNum and rf.contentScopeType = 4 and ArchivedFlag=0				
				order by rs.FiscalWeek,r.ReportTitle,r.sortOrder
	else
		Select rf.ReportFileID, rf.FileName,r.ReportID, r.ReportTitle, rs.locationPath,rs.FiscalWeek ,rs.FiscalYear, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId
				from ReportFile rf join ReportSubmission rs on rs.ReportSubmissionId = rf.ReportSubmissionID
				join Report r on rs.ReportID = r.ReportID	
				where rf.OwnerId = @StoreNum and rf.contentScopeType = 4  and ArchivedFlag=0														
				order by r.ReportTitle,r.sortOrder,rs.FiscalWeek
			
end
else
begin
-- dm view
if @grpBy = 2 
		Select  rf.ReportFileID,rf.OwnerID,rf.contentScopeType, rf.FileName,r.ReportID, r.ReportTitle, rs.locationPath ,rs.FiscalWeek ,rs.FiscalYear, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId,r.sortOrder
				from ReportFile rf join ReportSubmission rs on rs.ReportSubmissionId = rf.ReportSubmissionID
				join Report r on rs.ReportID = r.ReportID
				join dbo.RFN_GetStoresByUser(@employeeId) fn on fn.contentScopeType = rf.contentScopeType and fn.OwnerID = rf.OwnerID				 									
				where ArchivedFlag=0 and(fn.EffectivityEndDate is null OR fn.EffectivityEndDate >= rs.FiscalWeekEndingDate )				 					
				order by  rs.FiscalWeek,rf.contentScopeType,rf.OwnerID,r.Sortorder
	else
		Select  rf.ReportFileID,rf.OwnerID,rf.contentScopeType, rf.FileName,r.ReportID, r.ReportTitle, rs.locationPath ,rs.FiscalWeek ,rs.FiscalYear, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId,r.sortOrder
				from ReportFile rf join ReportSubmission rs on rs.ReportSubmissionId = rf.ReportSubmissionID
				join Report r on rs.ReportID = r.ReportID
				join dbo.RFN_GetStoresByUser(@employeeId) fn on fn.contentScopeType = rf.contentScopeType and fn.OwnerID = rf.OwnerID 
				where ArchivedFlag=0 and (fn.EffectivityEndDate is null OR fn.EffectivityEndDate >= rs.FiscalWeekEndingDate )				 					
				order by r.ReportTitle,r.sortOrder,rf.contentScopeType,rf.OwnerID
end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

