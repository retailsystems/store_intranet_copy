IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_ListReports')
	BEGIN
		PRINT 'Dropping Procedure RSP_ListReports'
		DROP  Procedure  RSP_ListReports
	END

GO

PRINT 'Creating Procedure RSP_ListReports'
GO
CREATE Procedure RSP_ListReports
	/* Param List */
AS

/******************************************************************************
**		File: RSP_ListReports.sql
**		Name: RSP_ListReports
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 6/25/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

Select reportId,ReportTitle,stagingPath,contentScopeType,
	dbo.RFN_GetLookUpText(contentScopeType,'ContentScope') as scope, 
	defaultFileName,DefaultFileIdentifier,
	dbo.RFN_GetLookUpText(DefaultFileIdentifier,'FileIdentifier') as FileIdentifier,
	SortOrder,SetupCompleteFlag
	From Report order by SortOrder


GO

GRANT EXEC ON RSP_ListReports TO RdsApp

GO
