IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetFiscalMonthBeginEndDates')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetFiscalMonthBeginEndDates'
		DROP  Procedure  RSP_GetFiscalMonthBeginEndDates
	END

GO

PRINT 'Creating Procedure RSP_GetFiscalMonthBeginEndDates'
GO
CREATE Procedure RSP_GetFiscalMonthBeginEndDates
	@MonthNum int
	,@FiscalYear int
	,@BeginDate datetime OUTPUT
	,@EndDate datetime OUTPUT
	
AS

/******************************************************************************
**		File: 
**		Name: RSP_GetFiscalMonthBeginEndDates
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 7/25/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT @BeginDate = MIN(BEG_DT) FROM RV_FiscalCalender WHERE PER_NUM = @MonthNum AND YR = @FiscalYear

SELECT @EndDate = MAX(END_DT) FROM RV_FiscalCalender WHERE PER_NUM = @MonthNum AND YR = @FiscalYear

IF (@BeginDate IS NULL ) OR (@EndDate IS NULL )
	BEGIN
			RAISERROR ('InValid Fiscal Calendar Month', 16, 3)
			RETURN -1
	END

IF @@ERROR != 0 
		BEGIN
			RAISERROR ('Failed to Fetch Fiscal Month Date Range', 16, 3)
			RETURN -1
		END
	RETURN 0



GO

GRANT EXEC ON RSP_GetFiscalMonthBeginEndDates TO RdsApp

GO
