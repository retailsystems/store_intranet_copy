IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetAllDistricts')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetAllDistricts'
		DROP  Procedure  RSP_GetAllDistricts
	END

GO

PRINT 'Creating Procedure RSP_GetAllDistricts'
GO
CREATE Procedure RSP_GetAllDistricts
	/* Param List */
	@CompanyID int = 0
AS

/******************************************************************************
**		File: RSP_GetAllDistricts.sql
**		Name: RSP_GetAllDistricts
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 10/07/2004
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
IF (@CompanyID = 1 OR @CompanyID = 2)
SELECT Distinct RegionID,DistrictID FROM StoreDistrict 
	WHERE (EffectivityEndDate > getDate() OR EffectivityEndDate is NUll)
	AND RegionID > 0 AND DistrictID > 0
	AND CompanyID = @CompanyID
	ORDER BY RegionID,DistrictID
ELSE IF (@CompanyID = 3)
	SELECT Distinct RegionID,DistrictID FROM StoreDistrict 
	WHERE (EffectivityEndDate > getDate() OR EffectivityEndDate is NUll)
	AND RegionID > 0 AND DistrictID > 0
	AND CdStoreFlag = 1
	ORDER BY RegionID,DistrictID
ELSE
	SELECT Distinct RegionID,DistrictID FROM StoreDistrict 
	WHERE (EffectivityEndDate > getDate() OR EffectivityEndDate is NUll)
	AND RegionID > 0 AND DistrictID > 0	
	ORDER BY RegionID,DistrictID
GO

GRANT EXEC ON RSP_GetAllDistricts TO RdsApp

GO
