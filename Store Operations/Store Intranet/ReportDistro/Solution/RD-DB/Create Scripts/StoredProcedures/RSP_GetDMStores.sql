IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetDMStores')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetDMStores'
		DROP  Procedure  RSP_GetDMStores
	END

GO

PRINT 'Creating Procedure RSP_GetDMStores'
GO
CREATE Procedure RSP_GetDMStores
	@EmployeeID varchar(20)
	,@UserRoleID int
AS

/******************************************************************************
**		File: RSP_GetDMStores.sql
**		Name: RSP_GetDMStores
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 8/8/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT Distinct RegionID,DistrictID,StoreNum FROM StoreDistrict Sd
	INNER JOIN RFN_GetStoresByUser (@EmployeeID,@UserRoleID) Fn
	ON Sd.StoreNum = fn.OwnerID AND ContentScopeType =4
	WHERE (Fn.EffectivityEndDate > getDate() OR  Fn.EffectivityEndDate is NUll)
		 AND (Sd.EffectivityEndDate > getDate() OR  Sd.EffectivityEndDate is NUll)
	ORDER BY RegionID,DistrictID,StoreNum


GO

GRANT EXEC ON RSP_GetDMStores TO RdsApp

GO
