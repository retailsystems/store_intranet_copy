SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_UpdateCompanyOwner]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_UpdateCompanyOwner]
GO

CREATE PROCEDURE dbo.RSP_UpdateCompanyOwner
	@ReportID int,
	@companyID int,
	@CdStoresFlag int,
	@submitBy varchar(30)

/******************************************************************************
**		File: RSP_UpdateCompanyOwner.sql
**		Name: RSP_UpdateCompanyOwner
**		Desc:  This storedprocedure  Add/Update ReportCompany Owner information 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportID
**	
**		Auth: Sri Bajjuri
**		Date: 6/11/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
AS
	SET NOCOUNT ON
	declare @errCode int
	select @errCode  = 0
	if exists(select ReportID from ReportCompanyOwner where ReportID = @reportID and CompanyID = @companyID)
		begin
			update ReportCompanyOwner
				set CdStoresFlag = @CdStoresFlag
					where ReportID = @reportID and CompanyID = @companyID
					if @@error <> 0
					select @errCode  = -1
		end
	else
		begin
			insert into ReportCompanyOwner (ReportID,CompanyID,CdStoresFlag)
				values(@reportID,@companyID,@CdStoresFlag)
				
				if @@error <> 0
					select @errCode  = -1
				
		end
	
	select @errCode
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXEC ON RSP_UpdateCompanyOwner TO RdsApp

GO
