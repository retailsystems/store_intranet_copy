SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_AddReportFileOwner]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_AddReportFileOwner]
GO

CREATE PROCEDURE dbo.RSP_AddReportFileOwner	
	@ReportFileID int,
	@SubmitBy varchar(30)

/******************************************************************************
**		File: RSP_AddReportFileOwner.sql
**		Name: RSP_AddReportFileOwner
**		Desc:  This storedprocedure  Adds ReportFileOwner information 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportID
**	
**		Auth: Sri Bajjuri
**		Date: 6/10/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
AS
	SET NOCOUNT ON
	declare @errCode int
	select @errCode  = 0
	declare @rptEffectivityStartDate datetime, @rptEffectivityEndDate datetime
	declare @reportID int, @regionID int, @districtID int, @fileIdentifier varchar(20)
	
	select @ReportID=rf.ReportID,@rptEffectivityStartDate=rf.EffectivityStartDate,
			@rptEffectivityEndDate=rf.EffectivityEndDate, @fileIdentifier = DefaultFileIdentifier
			from ReportFile rf 	inner join Report r on rf.ReportID = r.ReportID 
			where rf.ReportFileID = @ReportFileID
		
	delete from ReportFileStoreOwner where ReportFileID = @reportFileID
	if @fileIdentifier = 'RegionID'
		insert into ReportFileStoreOwner
					(
					ReportFileID,
					StoreNum,
					InsertBy,
					InsertDate
					)
					select @reportFileID,StoreNum,@SubmitBy,getDate()
						from StoreDistrict where RegionID =@regionID
							and companyID in(select CompanyID from ReportCompanyOwner
											where ReportID =@reportID)
							and effectivityStartDate <= @rptEffectivityStartDate
							and (effectivityEndDate is null or effectivityEndDate >= @rptEffectivityEndDate)
											
	if @fileIdentifier = 'DistrictID'
		insert into ReportFileStoreOwner
					(
					ReportFileID,
					StoreNum,
					InsertBy,
					InsertDate
					)
					select @reportFileID,StoreNum,@SubmitBy,getDate()
						from StoreDistrict where DistrictID =@DistrictID
							and companyID in(select CompanyID from ReportCompanyOwner
											where ReportID =@reportID)
							and effectivityStartDate <= @rptEffectivityStartDate
							and (effectivityEndDate is null or effectivityEndDate >= @rptEffectivityEndDate)
											
		if @@error <> 0
			select @errCode = -1
	
	return @errCode
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

