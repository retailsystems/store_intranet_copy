IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_ListCurrentSubmissions')
	BEGIN
		PRINT 'Dropping Procedure RSP_ListCurrentSubmissions'
		DROP  Procedure  RSP_ListCurrentSubmissions
	END

GO

PRINT 'Creating Procedure RSP_ListCurrentSubmissions'
GO
CREATE Procedure RSP_ListCurrentSubmissions
	@EmployeeId varchar(30)
	,@UserId varchar(30)
	,@UserRoleId int
	/* Param List */
AS

/******************************************************************************
**		File: ListCurrentSubmissions.sql
**		Name: ListCurrentSubmissions
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    9/22/03		Sri Bajjuri		Approvers sql query modified to pull approver uploaded files.
*******************************************************************************/

--ToDo: data permissions based on user role.
IF @UserRoleId = 3 -- HQ users
	SELECT ReportSubmissionID,ReportID,ReportTitle,FiscalWeekEndingDate,
		ReportDate,LookUpText,Status,Comment
		FROM  RV_CurrentSubmissions rv 
		INNER JOIN LookUp ON lookUpValue = status and LookUpType ='ReportStatus'
		where Status <> 4  AND rv.insertBy = @userID
		Order by 1 desc
ELSE IF @UserRoleId = 2 -- approvers
	SELECT ReportSubmissionID,rv.ReportID,ReportTitle,FiscalWeekEndingDate,
		ReportDate,LookUpText,Status,Comment
		FROM  RV_CurrentSubmissions rv 
		INNER JOIN LookUp ON lookUpValue = status and LookUpType ='ReportStatus'
		INNER JOIN ReportApprover ra ON rv.ReportID = ra.ReportID AND ra.EmployeeID = @EmployeeID
		where Status <> 4 
	UNION
	SELECT ReportSubmissionID,ReportID,ReportTitle,FiscalWeekEndingDate,
			ReportDate,LookUpText,Status,Comment
			FROM  RV_CurrentSubmissions rv 
			INNER JOIN LookUp ON lookUpValue = status and LookUpType ='ReportStatus'
			where Status <> 4  AND rv.insertBy = @userID
			Order by 1 desc
ELSE
	SELECT ReportSubmissionID,ReportID,ReportTitle,FiscalWeekEndingDate,
		ReportDate,LookUpText,Status,Comment
		FROM  RV_CurrentSubmissions rv 
		INNER JOIN LookUp ON lookUpValue = status and LookUpType ='ReportStatus'
		where Status <> 4 
		Order by 1 desc
		
		
		



GO

GRANT EXEC ON RSP_ListCurrentSubmissions TO RdsApp

GO
