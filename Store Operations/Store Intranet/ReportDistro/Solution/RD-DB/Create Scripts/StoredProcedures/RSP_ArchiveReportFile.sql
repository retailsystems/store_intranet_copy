IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_ArchiveReportFile')
	BEGIN
		PRINT 'Dropping Procedure RSP_ArchiveReportFile'
		DROP  Procedure  RSP_ArchiveReportFile
	END

GO

PRINT 'Creating Procedure RSP_ArchiveReportFile'
GO
CREATE Procedure RSP_ArchiveReportFile
	@ReportFileID int, @SubmitBy varchar(30)
AS

/******************************************************************************
**		File: RSP_ArchiveReportFile.sql
**		Name: RSP_ArchiveReportFile
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportFileID
**		Auth: Sri Bajjuri
**		Date: 6/13/02
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

	-- fix me later to set new locationpath.
	Update ReportFile set ArchivedFlag = 1, UpdateBy = @SubmitBy, UpdateDate = getDate()
		where ReportFileID = @ReportFileID
	if @@ERROR <> 0
		return -1
	else
		return 0


GO

GRANT EXEC ON RSP_ArchiveReportFile TO RdsApp

GO
