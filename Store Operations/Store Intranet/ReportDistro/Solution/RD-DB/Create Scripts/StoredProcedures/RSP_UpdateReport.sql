SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_UpdateReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_UpdateReport]
GO


CREATE PROCEDURE dbo.RSP_UpdateReport	
		@ReportID int ,
		@ReportTitle varchar(30),
		@ReportDesc varchar(255),
		@InputType int,
		@StagingPath varchar(255),
		@LocationPath varchar(255),
		@ExpirationType int,
		@SortOrder int,
		@DefaultFile varchar(50),
		@UserId varchar(20),			
		@Scope int = null,				
		@FileIdentifier varchar(20)= null,
		@FileFormat varchar(20)= null,		
		@ArchiveNth int = null,
		@MaxFileSize int = null,		
		@ApproverEmail varchar(30) = null,
		@ApprovalType int = null,
		@ReadOnlyFlag int = null,
		@newReportID int output,
		@AllowMultiFlag int = null

		
			

/******************************************************************************
**		File: RSP_UpdateReport.sql
**		Name: RSP_UpdateReport
**		Desc:  This storedprocedure  Add/Update Report information and returns reportid
					upon success.
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportID
**	
**		Auth: Sri Bajjuri
**		Date: 6/10/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

AS

 SET NOCOUNT ON 
 
 --declare @errCode int, @errMsg varchar(255)
 
if @reportID > 0
	--Update Existing Report
	BEGIN
	IF 	@InputType = 1 -- 1 System, 2 Upload
		BEGIN
		if exists(select ReportID from Report where ReportID <> @ReportID and StagingPath = @StagingPath and DefaultFileName =@DefaultFile )
			BEGIN
			RAISERROR ('Specified Staging path and default FileName are being used by another report', 16, 3)
			RETURN -1
			END
		END
		
	update Report
		set 
				ReportTitle = @ReportTitle,
				ReportDesc = @ReportDesc,
				InputType = @InputType,
				StagingPath = @StagingPath,
				LocationPath = @LocationPath,
				ContentScopeType = @Scope,
				DefaultFileName = @DefaultFile,
				DefaultFileIdentifier = @FileIdentifier,
				FileFormat = @FileFormat,
				ExpirationType = @ExpirationType,
				ArchiveNth = @ArchiveNth,
				MaxFileSize = @MaxFileSize,
				SortOrder = @SortOrder,
				ApproverGroupEmail = @ApproverEmail,
				ApprovalType = @ApprovalType,
				ReadOnlyFlag = @ReadOnlyFlag,
				AllowMultiFlag = @AllowMultiFlag,
				UpdateBy =@UserId,
				UpdateDate= getdate()
				where ReportID = @reportID
				if @@error != 0 
					begin						
						RAISERROR ('Failed to Update Report : rsp_UpdateReport.', 16, 3)
						RETURN -1
					end	
				else
					select @newReportID = @reportID
		/*end
		else
			begin				
				RAISERROR ('Duplicate Report Title: rsp_UpdateReport.', 16, 3)
				RETURN -1
			end
		*/
	END
else
	begin
		-- check for duplicate title
		/*
		if not exists(select ReportID from Report where  ReportTitle like @ReportTitle)
		begin
		*/
		IF 	@InputType = 1 -- 1 System, 2 Upload
		BEGIN
		if exists(select ReportID from Report where  StagingPath = @StagingPath and DefaultFileName =@DefaultFile )
			BEGIN
			RAISERROR ('Specified Staging path and default FileName are being used by another report', 16, 3)
			RETURN -1
			END
		END
		
		INSERT INTO Report 
				(				
				ReportTitle,
				ReportDesc,
				InputType,
				StagingPath,
				LocationPath,
				ContentScopeType,
				DefaultFileName,
				DefaultFileIdentifier,
				FileFormat,
				ExpirationType,
				ArchiveNth,
				MaxFileSize,
				SortOrder,
				ApproverGroupEmail,
				ApprovalType,
				ReadOnlyFlag,
				InsertBy,
				InsertDate,
				AllowMultiFlag
				)
		values(
			@ReportTitle,
			@ReportDesc ,
			@InputType ,
			@StagingPath ,
			@LocationPath ,
			@Scope ,
			@DefaultFile,
			@FileIdentifier ,
			@FileFormat,
			@ExpirationType ,
			@ArchiveNth ,
			@MaxFileSize ,
			@SortOrder ,
			@ApproverEmail ,
			@ApprovalType ,
			@ReadOnlyFlag ,
			@UserId,
			getdate(),
			@AllowMultiFlag
			)
			if @@error = 0 
				begin
					select @newReportID = @@identity
					
				end
			else
				begin
					RAISERROR ('Failed to Add Report : rsp_UpdateReport.', 16, 3)
					RETURN -1
				end
		/*end
		else
			begin
				RAISERROR ('Duplicate Report Title: rsp_UpdateReport.', 16, 3)
				RETURN -1
			end
		*/
	end
	
	EXEC RSP_ResetReportCompleteFlag @newReportID
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXEC ON RSP_UpdateReport TO RdsApp

GO
