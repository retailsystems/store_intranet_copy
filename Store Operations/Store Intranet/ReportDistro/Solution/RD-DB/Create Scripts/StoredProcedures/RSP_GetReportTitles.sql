IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetReportTitles')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetReportTitles'
		DROP  Procedure  RSP_GetReportTitles
	END

GO

PRINT 'Creating Procedure RSP_GetReportTitles'
GO
CREATE Procedure RSP_GetReportTitles
	@inputType smallint = NULL
AS

/******************************************************************************
**		File: RSP_GetReportTitles.sql
**		Name: RSP_GetReportTitles
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 6/23/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**		7/7/2003	Jeric Saez			Added optional input type param
**		7/17/2003	Sri Bajjuri			Added SetupCompleteFlag filter
*******************************************************************************/

	IF (@inputType IS NULL)
		SELECT	ReportID
				,ReportTitle
				,StagingPath as Path 
		FROM	Report 
		WHERE	SetupCompleteFlag = 1
		ORDER BY SortOrder
	ELSE
		SELECT	ReportID
				,ReportTitle
				,StagingPath as Path 
		FROM	Report 
		WHERE	SetupCompleteFlag = 1 AND InputType = @inputType
		ORDER BY SortOrder
	



GO

GRANT EXEC ON RSP_GetReportTitles TO RdsApp

GO
