SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_GetReportByID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_GetReportByID]
GO

CREATE PROCEDURE dbo.RSP_GetReportByID
	@ReportID int
	,@reportOnly bit = 0

/******************************************************************************
**		File: RSP_GetReportByID.sql
**		Name: RSP_GetReportByID
**		Desc: Retrieve Report Details by ID
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportID
**	
**		Auth: Sri Bajjuri
**		Date: 6/10/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**		6/19/2003	Jeric Saez			Added other related tables
*******************************************************************************/
AS
	SET NOCOUNT ON
	
	/* Report table */
	SELECT * From Report where ReportID = @ReportID
	
	IF @reportOnly = 0
	BEGIN
		/* Company table */
		SELECT	RCO.ReportId
				,RCO.CompanyId
				,RCO.CDStoresFlag
				,C.CompanyName
		FROM	ReportCompanyOwner RCO INNER JOIN
				Company C ON (RCO.CompanyId = C.CompanyId AND
				RCO.ReportId = @ReportId)
				
		/* ReportApprover table */
		SELECT	ReportId
				,RA.EmployeeId
				,ltrim(rtrim(NameFirst)) AS FirstName --TODO: get from user table
				,ltrim(rtrim(NameLast)) AS LastName --TODO: get from user table
				,RouteOrder
		FROM	ReportApprover RA INNER JOIN HotTopic2_Dev.dbo.taUltimateEmployee TA
		ON RA.EmployeeId = TA.EmployeeId
		WHERE	ReportId = @ReportID
		ORDER BY RouteOrder
		
		/* ReportJobCodeOwner */
		SELECT	RCO.ReportId
				,RCO.CompanyId
				,JC.JobCode
				,CO.CompanyName
		FROM	ReportCompanyOwner RCO INNER JOIN
				ReportJobCodeOwner JC ON (RCO.ReportId = JC.ReportId AND
				RCO.CompanyId = JC.CompanyId AND
				RCO.ReportId = @ReportId)
				INNER JOIN Company CO ON RCO.CompanyID = CO.CompanyID
	
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT EXEC ON RSP_GetReportByID TO RdsApp

GO
