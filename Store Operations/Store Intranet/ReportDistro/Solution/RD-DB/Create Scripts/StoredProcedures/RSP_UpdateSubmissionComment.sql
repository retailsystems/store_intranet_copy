IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_UpdateSubmissionComment')
	BEGIN
		PRINT 'Dropping Procedure RSP_UpdateSubmissionComment'
		DROP  Procedure  RSP_UpdateSubmissionComment
	END

GO

PRINT 'Creating Procedure RSP_UpdateSubmissionComment'
GO
CREATE Procedure RSP_UpdateSubmissionComment
	@ReportSubmissionID int 
	,@UserId varchar(20)	
	,@Comment varchar(2000)
	
AS

/******************************************************************************
**		File: RSP_UpdateSubmissionComment.sql
**		Name: RSP_UpdateSubmissionComment
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 8/19/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
if @ReportSubmissionID > 0	
	--Update Existing Report
	BEGIN
	update ReportSubmission
		set 
			Comment = @Comment				
			,UpdateBy =@UserId
			,UpdateDate= getdate()
			where ReportSubmissionID = @ReportSubmissionID
	
				if @@error != 0 
				begin						
					RAISERROR ('Failed to Update Report Submission: RSP_UpdateSubmissionComment.', 16, 3)
					RETURN -1
				end	
		
	END



GO

GRANT EXEC ON RSP_UpdateSubmissionComment TO RdsApp

GO
