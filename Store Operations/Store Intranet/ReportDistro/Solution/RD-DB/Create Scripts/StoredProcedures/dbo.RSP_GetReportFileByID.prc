SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_GetReportFileByID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_GetReportFileByID]
GO

CREATE PROCEDURE dbo.RSP_GetReportFileByID
	@ReportFileID int

/******************************************************************************
**		File: RSP_GetReportFileByID.sql
**		Name: RSP_GetReportFileByID
**		Desc: Retrieve ReportFile Details by ID
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@ReportID
**	
**		Auth: Sri Bajjuri
**		Date: 6/10/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
AS
	SET NOCOUNT ON
	
	SELECT rf.*, r.ReportTitle From ReportFile rf inner join Report r 
		on rf.ReportID=r.ReportID  where ReportFileID = @ReportFileID
	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

