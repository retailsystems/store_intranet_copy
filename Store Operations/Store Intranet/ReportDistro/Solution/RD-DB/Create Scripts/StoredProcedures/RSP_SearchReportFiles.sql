IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_SearchReportFiles')
	BEGIN
		PRINT 'Dropping Procedure RSP_SearchReportFiles'
		DROP  Procedure  RSP_SearchReportFiles
	END

GO

PRINT 'Creating Procedure RSP_SearchReportFiles'
GO


CREATE Procedure RSP_SearchReportFiles
	@EmployeeID varchar(30)
	,@JobCode as varchar(30)
	,@ReportID int	
	,@SearchDate datetime = null
	,@ArchivedFlag bit = null
	,@SubmissionID int = null
AS

/******************************************************************************
**		File: RSP_SearchReportFiles.sql
**		Name: RSP_SearchReportFiles
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**     5/25/04		Sri Bajjuri			Market Level users search function included.
*******************************************************************************/
DECLARE	@FWEDate	smalldatetime
IF @JobCode = '12'
	BEGIN
	CREATE TABLE #MyTempRegion
		(RegionID varchar(2) not null)
	CREATE TABLE #MyTempScope 
		(	OwnerID varchar(5),
			ContentScopeType int,
			EffectivityEndDate datetime,
			CDStoreFlag bit,
			CompanyId Int
		)
	declare @regionId varchar(5), @districtId varchar(3),@companyId int
	select @regionId=RegionID,@districtId=districtId, @companyId = CompanyID from RV_AppUsers where employeeID = @employeeId
	insert into #MyTempScope  values(@companyId, 1 ,'12/12/2039',1,@companyId)

	DECLARE @MarketID int
	SELECT @MarketID = MarketID FROM  HtSecurity.dbo.MarketOfficers WHERE EmployeeID = @EmployeeID
	INSERT #MyTempRegion EXECUTE   HtSecurity.dbo.ssp_GetRegionsByMarketID  @MarketID
	INSERT #MyTempScope SELECT RegionID,2,'12/12/2039',1,@companyId FROM #MyTempRegion
	insert into #MyTempScope  select distinct DistrictId,3,EffectivityEndDate,1,@companyId 
		from StoreDistrict where RegionId in (select distinct RegionID from #MyTempRegion)
	IF (@SearchDate IS NOT NULL)
		BEGIN
		SELECT	@FWEDate = END_DT
		FROM	RV_FiscalCalender
		WHERE	@SearchDate BETWEEN BEG_DT AND END_DT
		SELECT  rf.ReportFileID
				,CASE rf.contentScopeType
				WHEN 1 THEN
					CASE rf.OwnerID
						WHEN 1 THEN 'Hot Topic'
						WHEN 2 THEN 'Torrid'
					END
				ELSE
					(lookUpText + ' ' +rf.OwnerID)
				END  as Owner
				,r.ReportID, r.ReportTitle, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId, rf.FileName
						, case  when rfa.AccessedDate is not null then 1 else 0 end as isAccessed
						FROM ReportFile rf JOIN ReportSubmission rs ON rs.ReportSubmissionId = rf.ReportSubmissionID 
						JOIN Report r ON rs.ReportID = r.ReportID AND r.ReportID = 	@ReportID		
						JOIN LookUp ON lookUpValue = rf.contentScopeType AND LookUpType ='ContentScope'
						join #MyTempScope fn on fn.contentScopeType = rf.contentScopeType and fn.OwnerID = rf.OwnerID				 									
						Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
						WHERE ArchivedFlag=1  AND rs.FiscalWeekEndingDate= @FWEDate
						AND dbo.RFN_JobCodeOwnerCheck(r.ReportID,convert(int,@JobCode)) = 1
						order by rf.ReportFileID,rs.ReportSubmissionId desc
		END
	ELSE		
		SELECT  rf.ReportFileID
				,CASE rf.contentScopeType
				WHEN 1 THEN
					CASE rf.OwnerID
						WHEN 1 THEN 'Hot Topic'
						WHEN 2 THEN 'Torrid'
					END
				ELSE
					(lookUpText + ' ' +rf.OwnerID)
				END  as Owner
				,r.ReportID, r.ReportTitle, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId, rf.FileName
						, case  when rfa.AccessedDate is not null then 1 else 0 end as isAccessed
						FROM ReportFile rf JOIN ReportSubmission rs ON rs.ReportSubmissionId = rf.ReportSubmissionID 
						JOIN Report r ON rs.ReportID = r.ReportID AND r.ReportID = 	@ReportID		
						JOIN LookUp ON lookUpValue = rf.contentScopeType AND LookUpType ='ContentScope'
						join #MyTempScope fn on fn.contentScopeType = rf.contentScopeType and fn.OwnerID = rf.OwnerID				 									
						Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
						WHERE ArchivedFlag=1
						AND dbo.RFN_JobCodeOwnerCheck(r.ReportID,convert(int,@JobCode)) = 1
						order by rf.ReportFileID,rs.ReportSubmissionId desc
	DROP TABLE 	#MyTempRegion
	DROP TABLE 	#MyTempScope
	END
ELSE
BEGIN
	IF (@SubmissionID IS Not NULL)
	BEGIN
		IF (@ArchivedFlag IS Not NULL)	
			SELECT  rf.ReportFileID
				,CASE rf.contentScopeType
				WHEN 1 THEN
					CASE rf.OwnerID
						WHEN 1 THEN 'Hot Topic'
						WHEN 2 THEN 'Torrid'
					END
				ELSE
					(lookUpText + ' ' +rf.OwnerID)
				END  as Owner
				,r.ReportID, r.ReportTitle, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId, rf.FileName
						, case  when rfa.AccessedDate is not null then 1 else 0 end as isAccessed
						FROM ReportFile rf JOIN ReportSubmission rs ON rs.ReportSubmissionId = rf.ReportSubmissionID AND rs.ReportSubmissionId = @SubmissionID
						JOIN Report r ON rs.ReportID = r.ReportID AND r.ReportID = 	@ReportID		
						JOIN LookUp ON lookUpValue = rf.contentScopeType AND LookUpType ='ContentScope'
						JOIN dbo.RFN_GetStoresByUser(@employeeId,@JobCode) fn on fn.contentScopeType = rf.contentScopeType AND fn.OwnerID = rf.OwnerID				 									
						Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
						WHERE ArchivedFlag=@ArchivedFlag 
						AND dbo.RFN_JobCodeOwnerCheck(r.ReportID,@JobCode) = 1
						order by rf.ReportFileID,rs.ReportSubmissionId desc
		ELSE
			SELECT  rf.ReportFileID
				,CASE rf.contentScopeType
				WHEN 1 THEN
					CASE rf.OwnerID
						WHEN 1 THEN 'Hot Topic'
						WHEN 2 THEN 'Torrid'
					END
				ELSE
					(lookUpText + ' ' +rf.OwnerID)
				END  as Owner
				,r.ReportID, r.ReportTitle, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId, rf.FileName
						, case  when rfa.AccessedDate is not null then 1 else 0 end as isAccessed
						FROM ReportFile rf JOIN ReportSubmission rs ON rs.ReportSubmissionId = rf.ReportSubmissionID AND rs.ReportSubmissionId = @SubmissionID
						JOIN Report r ON rs.ReportID = r.ReportID AND r.ReportID = 	@ReportID		
						JOIN LookUp ON lookUpValue = rf.contentScopeType AND LookUpType ='ContentScope'
						JOIN dbo.RFN_GetStoresByUser(@employeeId,@JobCode) fn on fn.contentScopeType = rf.contentScopeType AND fn.OwnerID = rf.OwnerID				 									
						Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
						WHERE dbo.RFN_JobCodeOwnerCheck(r.ReportID,@JobCode) = 1
						order by rf.ReportFileID,rs.ReportSubmissionId desc
	END
	ELSE IF (@SearchDate IS Not NULL)
	BEGIN
		SELECT	@FWEDate = END_DT
		FROM	RV_FiscalCalender
		WHERE	@SearchDate BETWEEN BEG_DT AND END_DT
		IF (@ArchivedFlag IS Not NULL)	
			SELECT  rf.ReportFileID
				,CASE rf.contentScopeType
				WHEN 1 THEN
					CASE rf.OwnerID
						WHEN 1 THEN 'Hot Topic'
						WHEN 2 THEN 'Torrid'
					END
				ELSE
					(lookUpText + ' ' +rf.OwnerID)
				END  as Owner
				,r.ReportID, r.ReportTitle, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId, rf.FileName
						, case  when rfa.AccessedDate is not null then 1 else 0 end as isAccessed

						FROM ReportFile rf JOIN ReportSubmission rs ON rs.ReportSubmissionId = rf.ReportSubmissionID
						JOIN Report r ON rs.ReportID = r.ReportID AND r.ReportID = 	@ReportID		
						JOIN LookUp ON lookUpValue = rf.contentScopeType AND LookUpType ='ContentScope'
						JOIN dbo.RFN_GetStoresByUser(@employeeId,@JobCode) fn on fn.contentScopeType = rf.contentScopeType AND fn.OwnerID = rf.OwnerID				 									
						Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
						WHERE ArchivedFlag=@ArchivedFlag AND rs.FiscalWeekEndingDate= @FWEDate
						AND dbo.RFN_JobCodeOwnerCheck(r.ReportID,@JobCode) = 1
						order by rf.ReportFileID,rs.ReportSubmissionId desc
		ELSE
			SELECT  rf.ReportFileID
				,CASE rf.contentScopeType
				WHEN 1 THEN
					CASE rf.OwnerID
						WHEN 1 THEN 'Hot Topic'
						WHEN 2 THEN 'Torrid'
					END
				ELSE
					(lookUpText + ' ' +rf.OwnerID)
				END  as Owner
				,r.ReportID, r.ReportTitle, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId, rf.FileName
						, case  when rfa.AccessedDate is not null then 1 else 0 end as isAccessed
						FROM ReportFile rf JOIN ReportSubmission rs ON rs.ReportSubmissionId = rf.ReportSubmissionID
						JOIN Report r ON rs.ReportID = r.ReportID AND r.ReportID = 	@ReportID		
						JOIN LookUp ON lookUpValue = rf.contentScopeType AND LookUpType ='ContentScope'
						JOIN dbo.RFN_GetStoresByUser(@employeeId,@JobCode) fn on fn.contentScopeType = rf.contentScopeType AND fn.OwnerID = rf.OwnerID				 									
						Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
						WHERE rs.FiscalWeekEndingDate= @FWEDate
						AND dbo.RFN_JobCodeOwnerCheck(r.ReportID,@JobCode) = 1
						order by rf.ReportFileID,rs.ReportSubmissionId desc
								 					
	END
	ELSE
	BEGIN
		IF (@ArchivedFlag IS Not NULL)	
			SELECT  rf.ReportFileID
				,CASE rf.contentScopeType
				WHEN 1 THEN
					CASE rf.OwnerID
						WHEN 1 THEN 'Hot Topic'
						WHEN 2 THEN 'Torrid'
					END
				ELSE
					(lookUpText + ' ' +rf.OwnerID)
				END  as Owner
				,r.ReportID, r.ReportTitle, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId, rf.FileName
						, case  when rfa.AccessedDate is not null then 1 else 0 end as isAccessed
						FROM ReportFile rf join ReportSubmission rs ON rs.ReportSubmissionId = rf.ReportSubmissionID
						JOIN Report r ON rs.ReportID = r.ReportID	 AND r.ReportID = 	@ReportID	
						JOIN LookUp ON lookUpValue = rf.contentScopeType AND LookUpType ='ContentScope'		
						JOIN dbo.RFN_GetStoresByUser(@employeeId,@JobCode) fn on fn.contentScopeType = rf.contentScopeType AND fn.OwnerID = rf.OwnerID				 									
						Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
						WHERE ArchivedFlag=@ArchivedFlag 
						AND dbo.RFN_JobCodeOwnerCheck(r.ReportID,@JobCode) = 1
						order by rf.ReportFileID,rs.ReportSubmissionId desc
		ELSE
			SELECT  rf.ReportFileID
				,CASE rf.contentScopeType
				WHEN 1 THEN
					CASE rf.OwnerID
						WHEN 1 THEN 'Hot Topic'
						WHEN 2 THEN 'Torrid'
					END
				ELSE
					(lookUpText + ' ' +rf.OwnerID)
				END  as Owner
				, r.ReportTitle, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId, rf.FileName
						, case  when rfa.AccessedDate is not null then 1 else 0 end as isAccessed
						FROM ReportFile rf join ReportSubmission rs ON rs.ReportSubmissionId = rf.ReportSubmissionID
						JOIN Report r ON rs.ReportID = r.ReportID	 AND r.ReportID = 	@ReportID	
						JOIN LookUp ON lookUpValue = rf.contentScopeType AND LookUpType ='ContentScope'		
						JOIN dbo.RFN_GetStoresByUser(@employeeId,@JobCode) fn on fn.contentScopeType = rf.contentScopeType AND fn.OwnerID = rf.OwnerID				 									
						Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
						WHERE  dbo.RFN_JobCodeOwnerCheck(r.ReportID,@JobCode) = 1
						order by rf.ReportFileID,rs.ReportSubmissionId desc	
	END
END
if @@error <> 0 
			begin
				RAISERROR ('Failed to process request.:RSP_SearchReportFiles', 16, 3)
				RETURN -1
			end



GO


GRANT EXEC ON RSP_SearchReportFiles TO RdsApp

GO
