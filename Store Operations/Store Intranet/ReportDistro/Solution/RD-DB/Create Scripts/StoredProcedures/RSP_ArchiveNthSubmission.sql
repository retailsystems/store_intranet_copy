IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_ArchiveNthSubmission')
	BEGIN
		PRINT 'Dropping Procedure RSP_ArchiveNthSubmission'
		DROP  Procedure  RSP_ArchiveNthSubmission
	END

GO

PRINT 'Creating Procedure RSP_ArchiveNthSubmission'
GO
CREATE Procedure RSP_ArchiveNthSubmission
	@ReportID int,@ArchiveNth int
AS

/******************************************************************************
**		File: RSP_ArchiveNthSubmission.sql
**		Name: RSP_ArchiveNthSubmission
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**     7/24/03		Sri Bajjuri		Archive all the submissions below the Nth Submission.
**		2/23/05		Sri Bajjuri		Bug fix - add distinct key word in in sub query to get count
*******************************************************************************/
DECLARE @ReportSubmissionID int 
DECLARE @FWEDate datetime 
IF @ArchiveNth > 0 
BEGIN
	SELECT @FWEDate=a.FiscalWeekEndingDate from reportSubmission a
		WHERE @ArchiveNth = (SELECT COUNT(DISTINCT FiscalWeekEndingDate) 
					FROM  reportSubmission b
					WHERE a.FiscalWeekEndingDate < b.FiscalWeekEndingDate
					AND b.reportID = @ReportID  
					AND b.Status = 2
					)
		AND a.ReportID = @ReportID AND a.status = 2

	IF @FWEDate IS NOT NULL
	BEGIN
		Update ReportFile 
		SET ArchivedFlag=1 
		,UpdateDate = getDate()
		--WHERE ReportSubmissionID = @ReportSubmissionID
		WHERE ArchivedFlag <> 1 
			AND ReportSubmissionID in
				(Select ReportSubmissionID FROM ReportSubmission
					WHERE ReportID = @ReportID AND FiscalWeekEndingDate <= @FWEDate AND Status=2)
		
		UPDATE ReportSubmission 
			SET Status = 4 
			--WHERE ReportSubmissionID = @ReportSubmissionID
			WHERE Status = 2 AND  ReportID = @ReportID AND FiscalWeekEndingDate <= @FWEDate
	END
END
if @@ERROR <> 0
BEGIN							
	RAISERROR('RSP_ArchiveNthSubmission: Error Archiving ReportSubmission ', 16, 1)
	RETURN -1
END
RETURN 0

GO

GRANT EXEC ON RSP_ArchiveNthSubmission TO RdsApp

GO
