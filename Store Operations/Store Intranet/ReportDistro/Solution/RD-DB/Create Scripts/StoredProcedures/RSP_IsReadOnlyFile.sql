IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_IsReadOnlyFile')
	BEGIN
		PRINT 'Dropping Procedure RSP_IsReadOnlyFile'
		DROP  Procedure  RSP_IsReadOnlyFile
	END

GO

PRINT 'Creating Procedure RSP_IsReadOnlyFile'
GO
CREATE Procedure RSP_IsReadOnlyFile
		@reportFileID  integer	
	,@ReadOnly  bit output
AS

/******************************************************************************
**		File: 
**		Name: RSP_IsReadOnlyFile
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SELECT @ReadOnly = ISNULL(ReadOnlyFlag,1) FROM
	Report R JOIN REPORTSUBMISSION RS ON R.ReportID = RS.ReportID
	JOIN ReportFile RF ON RF.ReportSubmissionID = RS.ReportSubmissionID AND RF.ReportFileID = @reportFileID

IF @@Error != 0
	BEGIN
		SET @ReadOnly = 1
	END

GO

GRANT EXEC ON RSP_IsReadOnlyFile TO RdsApp

GO
