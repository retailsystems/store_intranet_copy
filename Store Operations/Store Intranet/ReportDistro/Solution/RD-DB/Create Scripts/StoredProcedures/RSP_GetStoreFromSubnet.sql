IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetStoreFromSubnet')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetStoreFromSubnet'
		DROP  Procedure  RSP_GetStoreFromSubnet
	END

GO

PRINT 'Creating Procedure RSP_GetStoreFromSubnet'
GO
CREATE Procedure RSP_GetStoreFromSubnet
	/* Param List */
	@StoreSubnet varchar(15),
	@StoreNum smallint OUTPUT

AS

/******************************************************************************
**		File: 
**		Name: RSP_GetStoreFromSubnet
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 01/22/2004
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
 DECLARE @tmpReturn smallint
  SELECT @tmpReturn = StoreNum
	FROM  RV_StoreInfo
	WHERE  Subnet = @StoreSubnet
	
  IF(@tmpReturn IS NULL)
    SELECT @StoreNum = -1
  ELSE
    SELECT @StoreNum = @tmpReturn
Return




GO

GRANT EXEC ON RSP_GetStoreFromSubnet TO RdsApp

GO
