IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetFilePathById')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetFilePathById'
		DROP  Procedure  RSP_GetFilePathById
	END

GO

PRINT 'Creating Procedure RSP_GetFilePathById'
GO
CREATE Procedure RSP_GetFilePathById
	@reportFileID  integer
	,@EmployeeId  varchar(30)
	,@filePath  varchar(100) output
	/* Param List */
AS

/******************************************************************************
**		File: RSP_GetFilePathById.sql
**		Name: RSP_GetFilePathById
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 6/27/2003
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
IF NOT EXISTS(Select ReportFileID from ReportFileAccess where EmployeeID=@EmployeeId and ReportFileID =@reportFileID )
	INSERT INTO ReportFileAccess (ReportFileID,EmployeeID,AccessedDate) values (@reportFileID,@EmployeeId,getDate())
	
SELECT @filePath = (rs.LocationPath +  convert(varchar,rs.FiscalYear) + '\' + convert(varchar,rs.FiscalWeek) + '\' + convert(varchar,rs.ReportId) + '\' + rf.FileName) 
	FROM reportFile rf INNER JOIN ReportSubmission rs ON rf.ReportSubmissionID = rs.ReportSubmissionId
	WHERE reportFileId = @reportFileID


GO

GRANT EXEC ON RSP_GetFilePathById TO RdsApp

GO
