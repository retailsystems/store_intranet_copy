IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'RSP_GetLookUpData')
	BEGIN
		PRINT 'Dropping Procedure RSP_GetLookUpData'
		DROP  Procedure  RSP_GetLookUpData
	END

GO

PRINT 'Creating Procedure RSP_GetLookUpData'
GO
CREATE Procedure RSP_GetLookUpData
	/* Param List */
AS

/******************************************************************************
**		File: RSP_GetLookUpData.sql
**		Name: RSP_GetLookUpData
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri
**		Date: 6/17/03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/


select LookUpType,LookUpText,LookUpValue from LookUp order by 1

GO

GRANT EXEC ON RSP_GetLookUpData TO RdsApp

GO
