SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RSP_GetCurrentReports]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[RSP_GetCurrentReports]
GO

CREATE PROCEDURE dbo.RSP_GetCurrentReports
	@StoreNum int
	,@grpBy int -- 1 grp by Title , 2- grp by Fiscal week
	,@employeeId varchar(30)
	,@JobCode varchar(30)
	
AS
SET NOCOUNT ON 

/******************************************************************************
**		File: RSP_GetCurrentReports.sql
**		Name: RSP_GetCurrentReports
**		Desc:  This storedprocedure  Adds ReportJobCode Owner information 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input								Output
**     ----------							-----------
**		@StoreNum
**	
**		Auth: Sri Bajjuri
**		Date: 6/12/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    9/25/2003		Sri Bajjuri			"Distinct" added in select stmt.
*******************************************************************************/


begin
	IF @JobCode = '12'
		BEGIN
		CREATE TABLE #MyTempRegion
			(RegionID varchar(2) not null)
		CREATE TABLE #MyTempScope 
			(	OwnerID varchar(5),
				ContentScopeType int,
				EffectivityEndDate datetime,
				CDStoreFlag bit,
				CompanyId Int
			)
		declare @regionId varchar(5), @districtId varchar(3),@companyId int
		select @regionId=RegionID,@districtId=districtId, @companyId = CompanyID from RV_AppUsers where employeeID = @employeeId
		insert into #MyTempScope  values(@companyId, 1 ,'12/12/2039',1,@companyId)

		DECLARE @MarketID int
		SELECT @MarketID = MarketID FROM  HtSecurity_Dev.dbo.MarketOfficers WHERE EmployeeID = @EmployeeID
		INSERT #MyTempRegion EXECUTE   HtSecurity_Dev.dbo.ssp_GetRegionsByMarketID  @MarketID
		INSERT #MyTempScope SELECT RegionID,2,'12/12/2039',1,@companyId FROM #MyTempRegion
		insert into #MyTempScope  select distinct DistrictId,3,EffectivityEndDate,1,@companyId 
			from StoreDistrict where RegionId in (select distinct RegionID from #MyTempRegion)
		--select * from #MyTempScope
		if @grpBy = 2 -- order by fiscal week
			Select distinct rf.ReportFileID,rf.OwnerID,rf.contentScopeType, rf.FileName,r.ReportID, r.ReportTitle, rs.locationPath ,rs.FiscalWeek ,rs.FiscalYear, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId,r.sortOrder, case  when rfa.AccessedDate is not null then 1 else 0 end as isAccessed
					from ReportFile rf join ReportSubmission rs on rs.ReportSubmissionId = rf.ReportSubmissionID
					join Report r on rs.ReportID = r.ReportID
					--Join ReportJobCodeOwner rjo ON r.ReportID = rjo.ReportID and JobCode = @JobCode
					Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
					join #MyTempScope fn on fn.contentScopeType = rf.contentScopeType and fn.OwnerID = rf.OwnerID				 									
					JOIN ReportCompanyOwner rco ON rco.ReportID = r.ReportID AND rco.CompanyID = fn.CompanyID 
					where rs.Status=2 AND ArchivedFlag=0 and(fn.EffectivityEndDate is null OR fn.EffectivityEndDate >= rs.FiscalWeekEndingDate )				 					
					and EffectivityStartDate <= getDate()
					AND dbo.RFN_JobCodeOwnerCheck(r.ReportID,@JobCode) = 1
					order by  rs.FiscalWeek,rf.contentScopeType,rf.OwnerID,r.Sortorder
		ELSE
			Select distinct rf.ReportFileID,rf.OwnerID,rf.contentScopeType, rf.FileName,r.ReportID, r.ReportTitle, rs.locationPath ,rs.FiscalWeek ,rs.FiscalYear, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId,r.sortOrder, case  when rfa.AccessedDate is not null then 1 else 0 end as isAccessed
					from ReportFile rf join ReportSubmission rs on rs.ReportSubmissionId = rf.ReportSubmissionID
					join Report r on rs.ReportID = r.ReportID
					--Join ReportJobCodeOwner rjo ON r.ReportID = rjo.ReportID and JobCode = @JobCode
					Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
					join #MyTempScope fn on fn.contentScopeType = rf.contentScopeType and fn.OwnerID = rf.OwnerID				 									
					JOIN ReportCompanyOwner rco ON rco.ReportID = r.ReportID AND rco.CompanyID = fn.CompanyID 
					where rs.Status=2 AND ArchivedFlag=0 and(fn.EffectivityEndDate is null OR fn.EffectivityEndDate >= rs.FiscalWeekEndingDate )				 					
					and EffectivityStartDate <= getDate()
					AND dbo.RFN_JobCodeOwnerCheck(r.ReportID,@JobCode) = 1
					order by r.sortOrder,r.ReportTitle,rf.contentScopeType,rf.OwnerID
		
		DROP TABLE #MyTempRegion
		DROP TABLE #MyTempScope
		END
	ELSE
	BEGIN
	-- dm view
	if @grpBy = 2 
			Select distinct rf.ReportFileID,rf.OwnerID,rf.contentScopeType, rf.FileName,r.ReportID, r.ReportTitle, rs.locationPath ,rs.FiscalWeek ,rs.FiscalYear, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId,r.sortOrder, case  when rfa.AccessedDate is not null then 1 else 0 end as isAccessed
					from ReportFile rf join ReportSubmission rs on rs.ReportSubmissionId = rf.ReportSubmissionID
					join Report r on rs.ReportID = r.ReportID
					--Join ReportJobCodeOwner rjo ON r.ReportID = rjo.ReportID and JobCode = @JobCode
					Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
					join dbo.RFN_GetStoresByUser(@employeeId,@JobCode) fn on fn.contentScopeType = rf.contentScopeType and fn.OwnerID = rf.OwnerID				 									
					JOIN ReportCompanyOwner rco ON rco.ReportID = r.ReportID AND rco.CompanyID = fn.CompanyID AND dbo.RFN_CdStoreCheck(fn.CdStoreFlag, rco.CdStoresFlag) = 1
					where rs.Status=2 AND ArchivedFlag=0 and(fn.EffectivityEndDate is null OR fn.EffectivityEndDate >= rs.FiscalWeekEndingDate )				 					
					and EffectivityStartDate <= getDate()
					AND dbo.RFN_JobCodeOwnerCheck(r.ReportID,@JobCode) = 1
					order by  rs.FiscalWeek,rf.contentScopeType,rf.OwnerID,r.Sortorder
		else
			Select distinct rf.ReportFileID,rf.OwnerID,rf.contentScopeType, rf.FileName,r.ReportID, r.ReportTitle, rs.locationPath ,rs.FiscalWeek ,rs.FiscalYear, rs.Comment, convert(varchar,rs.FiscalWeekEndingDate,101) as ReportDate,rs.ReportSubmissionId,r.sortOrder, case  when rfa.AccessedDate is not null then 1 else 0 end as isAccessed
					from ReportFile rf join ReportSubmission rs on rs.ReportSubmissionId = rf.ReportSubmissionID
					join Report r on rs.ReportID = r.ReportID
					--Join ReportJobCodeOwner rjo ON r.ReportID = rjo.ReportID and JobCode = @JobCode
					Left Outer Join ReportFileAccess rfa ON rf.ReportFileID = rfa.ReportFileID AND rfa.EmployeeID = @employeeId
					join dbo.RFN_GetStoresByUser(@employeeId,@JobCode) fn on fn.contentScopeType = rf.contentScopeType and fn.OwnerID = rf.OwnerID 
					JOIN ReportCompanyOwner rco ON rco.ReportID = r.ReportID AND rco.CompanyID = fn.CompanyID AND dbo.RFN_CdStoreCheck(fn.CdStoreFlag, rco.CdStoresFlag) = 1
					where rs.Status=2 AND  ArchivedFlag=0 and (fn.EffectivityEndDate is null OR fn.EffectivityEndDate >= rs.FiscalWeekEndingDate )				 					
					and EffectivityStartDate <= getDate()
					AND dbo.RFN_JobCodeOwnerCheck(r.ReportID,@JobCode) = 1
					order by r.sortOrder,r.ReportTitle,rf.contentScopeType,rf.OwnerID
	END
end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXEC ON RSP_GetCurrentReports TO RdsApp

GO
