--RFN_JobCodeOwnerCheck
if exists (select * from dbo.sysobjects where id = object_id(N'RFN_JobCodeOwnerCheck') and xtype in (N'FN', N'IF', N'TF'))
drop function RFN_JobCodeOwnerCheck
GO

CREATE FUNCTION RFN_JobCodeOwnerCheck
	(
	 @ReportID int, @JobCode int
	)
RETURNS bit
AS
/******************************************************************************
**		File: RFN_JobCodeOwnerCheck.sql
**		Name: RFN_JobCodeOwnerCheck
**		Desc:  This function returns file approval pending status 
**
**		This template can be customized:
**              
**		Return values: bit
** 
**		Called by:   RSP_GetCurrentReports, RSP_SearchReportFiles
**              
**		Parameters:
**		Input									Output
**     ----------								-----------
**		
**	
**		Auth: Sri Bajjuri
**		Date: 7/23/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    
*******************************************************************************/
	BEGIN
		declare @CheckOk bit
		SET @CheckOk = 0
		IF EXISTS(SELECT ReportID FROM ReportJobCodeOwner WHERE ReportID = @ReportID)
			-- Report has restricted access
			BEGIN
			IF EXISTS(SELECT ReportID FROM ReportJobCodeOwner WHERE ReportID = @ReportID AND JobCode = @JobCode)
				SET @CheckOk = 1
			END			
		ELSE
			-- Report is visible to all
			SET @CheckOk = 1
		
			
		RETURN @CheckOk
	END

GO

GRANT EXEC ON RFN_JobCodeOwnerCheck TO RdsApp

GO
 