if exists (select * from dbo.sysobjects where id = object_id(N'RFN_GetStoresByUser') and xtype in (N'FN', N'IF', N'TF'))
drop function RFN_GetStoresByUser
GO

CREATE Function RFN_GetStoresByUser ( @employeeId varchar(30),@JobCode varchar(30)) 
--************  @JobCode is userRoleID ********************
returns @UserTable table 
	(
	OwnerID varchar(5),
	ContentScopeType int,
	EffectivityEndDate datetime,
	CDStoreFlag bit,
	CompanyId Int
	)
AS
begin
/******************************************************************************
**		File: RFN_GetStoresByUser.sql
**		Name: RFN_GetStoresByUser
**		
**
**		This template can be customized:
**              
**		Return values: Table
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@array
**	
**		Auth: Sri Bajjuri
**		Date: 6/27/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    		7/18/03 	Sri Bajjuri	Included companyid
*******************************************************************************/
	BEGIN
	declare @regionId varchar(5), @districtId varchar(3),@companyId int
	select @regionId=RegionID,@districtId=districtId, @companyId = CompanyID from RV_AppUsers where employeeID = @employeeId
	insert into @UserTable  values(@companyId, 1 ,'12/12/2039',1,@companyId)
	insert into @UserTable  values(@regionId, 2 ,'12/12/2039',1,@companyId)
	-- using UserRole inplace of JobCode -- role id  6 RM, 4 DM,  5 - Store
	if @JobCode = '6'
		begin		
		
		insert into @UserTable  select distinct DistrictId,3,EffectivityEndDate,1,@companyId from StoreDistrict where RegionId =  @regionId 
		insert into @UserTable  select distinct storeNum,4,EffectivityEndDate,CDStoreFlag,@companyId from StoreDistrict where RegionId =  @regionId 
--EffectivityEndDate
		end
	else if @JobCode = '4'
		begin		
		insert into @UserTable  values(@districtId, 3,'12/12/2039',1,@companyId)
		insert into @UserTable  select distinct storeNum,4,EffectivityEndDate,CDStoreFlag,@companyId from StoreDistrict where DistrictId =  @districtId 
		end
	else 
		begin
		insert into @UserTable  values(@districtId, 3,'12/12/2039',1,@companyId)
		insert into @UserTable  select distinct storeNum,4,EffectivityEndDate,CDStoreFlag,@companyId from StoreDistrict
			 where storeNum in(select storeNum from RV_AppUsers where employeeID = @employeeId)
		update @UserTable set CDStoreFlag = (select top 1 CDStoreFlag from @UserTable where ContentScopeType = 4)
		end
		
		if exists(select CDStoreFlag from @UserTable  where ContentScopeType=4 and CDStoreFlag=1)
			update @UserTable set CDStoreFlag =1 where ContentScopeType <> 4
		else
			update @UserTable set CDStoreFlag =null where ContentScopeType <> 4
			
		
	END
	return
end


GO

GRANT SELECT ON RFN_GetStoresByUser TO RdsApp

GO
 