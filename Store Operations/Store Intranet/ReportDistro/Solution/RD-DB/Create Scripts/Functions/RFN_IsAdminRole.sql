if exists (select * from dbo.sysobjects where id = object_id(N'RFN_IsAdminRole') and xtype in (N'FN', N'IF', N'TF'))
drop function RFN_IsAdminRole
GO

CREATE FUNCTION RFN_IsAdminRole
	(
	@userRoleId int
	)
RETURNS bit
AS
/******************************************************************************
**		File: RFN_IsAdminRole.sql
**		Name: RFN_IsAdminRole
**		Desc:  This function returns file approval pending status 
**
**		This template can be customized:
**              
**		Return values: Table
** 
**		Called by:   
**              
**		Parameters:
**		Input									Output
**     ----------								-----------
**		
**	
**		Auth: Sri Bajjuri
**		Date: 7/8/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    
*******************************************************************************/
	BEGIN
		declare @isAdmin bit
		set @isAdmin = 0
		if @userRoleId = 1
			set @isAdmin = 1
	RETURN @isAdmin
	END


GO

GRANT EXEC ON RFN_IsAdminRole TO RdsApp

GO
