if exists (select * from dbo.sysobjects where id = object_id(N'RFN_ApproverstrToTable') and xtype in (N'FN', N'IF', N'TF'))
drop function RFN_ApproverstrToTable
GO

CREATE Function RFN_ApproverstrToTable ( @array nvarchar(2000)) 
returns @UserTable table 
	(
	RouteOrder int IDENTITY (1, 1)  NOT NULL,
	EmployeeID varchar(20))
AS
begin
/******************************************************************************
**		File: RFN_ApproverstrToTable.sql
**		Name: RFN_ApproverstrToTable
**		Desc:  This function converted '/' seperated approver string to table with sortorder
**
**		This template can be customized:
**              
**		Return values: Table
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**		@array
**	
**		Auth: Sri Bajjuri
**		Date: 6/12/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    
*******************************************************************************/
	set @array = ltrim(rtrim(@array)) 
	if @array <> ''
	begin
	declare @separator char(1)
	
	set @separator = '/'

	declare @separator_position int 
	declare @array_value varchar(2000) 	
	
	set @array = ltrim(rtrim(@array)) + '/'
	
	while patindex('%/%' , @array) <> 0 
	begin
	
	  select @separator_position =  patindex('%/%' , @array)
	  select @array_value = left(@array, @separator_position - 1)
	
		Insert into @UserTable(EmployeeID)
		Values (@array_value )

	  select @array = stuff(@array, 1, @separator_position, '')
	end
	end

	return
end

GO

GRANT SELECT ON RFN_ApproverstrToTable TO RdsApp

GO
