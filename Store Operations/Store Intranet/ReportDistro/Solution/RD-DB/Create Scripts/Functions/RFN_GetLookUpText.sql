if exists (select * from dbo.sysobjects where id = object_id(N'RFN_GetLookUpText') and xtype in (N'FN', N'IF', N'TF'))
drop function RFN_GetLookUpText
GO


CREATE FUNCTION RFN_GetLookUpText
	(
	@lookUpval int, @lookUpType varchar(30)
	)
RETURNS varchar(30)
AS
/******************************************************************************
**		File: RFN_GetLookUpText.sql
**		Name: RFN_GetLookUpText
**		Desc:  This function returns lookUpText 
**
**		This template can be customized:
**              
**		Return values: Table
** 
**		Called by:   
**              
**		Parameters:
**		Input									Output
**     ----------								-----------
**		
**	
**		Auth: Sri Bajjuri
**		Date: 6/26/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			-------------------------------------------
**    
*******************************************************************************/
	BEGIN
		declare @lookUptext varchar(30)
		select @lookUptext=LookUpText from LookUp where lookUpValue = @lookUpVal and lookUpType= @lookUpType
	RETURN @lookUptext
	END

GO

GRANT EXEC ON RFN_GetLookUpText TO RdsApp

GO
