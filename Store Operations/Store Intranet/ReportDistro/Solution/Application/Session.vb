' Static Model
Imports System.Web
Imports System.Data.SqlClient

Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Imports HotTopic.RD.ReportFiles
Imports HotTopic.RD.ReportWorkflow
Imports HotTopic.RD.Reports
Imports HotTopic.RD.Security
Imports HotTopic.RD.Services
Namespace HotTopic.RD.Application

    '*******************************************************************************
    '**		Change History
    '*******************************************************************************
    '**		Date:		Author:				Description:
    '**		--------	--------			---------------------------------------
    '**    6/18/03      Sri Bajjuri         New Function AddApprovers added to class.
    '*******************************************************************************

    ' Constructor would require a UserId
    Public Class Session

        ' Struct representing user account
        Public SessionUser As New HotTopic.RD.Security.User()
        Public MenuStr As String
        Private _context As HttpContext
        Private _cacheMgr As CacheManager
        Private _uiMgr As UIManager
        Private _appUrl As String

        Public Sub New(ByVal context As HttpContext)
            Try
                _context = context
                If _context.Session("SessionUser") Is Nothing Then
                    Throw New RDAppException("Unauthenticated user.")
                End If
                'set app url
                SetAppUrl()
                SessionUser = _context.Session("SessionUser")
                _cacheMgr = New CacheManager(_context)

                Dim moduleId As Int16 = _cacheMgr.GetModuleId(HotTopic.RD.Settings.AppSetting.CacheDependencyFile, SessionUser.CurrentUserRoleId, SessionUser.CurrentProjectId, GetModuleName(_context.Request.PhysicalPath))
                If moduleId = 0 Then
                    Throw New RDAppException("No authorization to access this page.")
                Else
                    SessionUser.CurrentModuleId = moduleId
                    MenuStr = _cacheMgr.GetMenu(HotTopic.RD.Settings.AppSetting.CacheDependencyFile, SessionUser.CurrentUserRoleId, SessionUser.CurrentModuleId)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub New(ByVal context As HttpContext, ByVal employeeId As String, ByVal projectName As String)
            _context = context
            Try
                'attempt login
                'padding zero to employee id 

                'If employeeId.Length < 5 Then
                '    employeeId = employeeId.PadLeft(5, "0")
                'End If

                'Modified for 6 digit emp change on Nov,2010 by Lan
                If employeeId.Length < 6 Then
                    employeeId = employeeId.PadLeft(6, "0")
                End If
                SessionUser = SecurityManager.Login(employeeId)
                Initialize(projectName)
                _context.Session("SessionUser") = SessionUser
            Catch ex As Security.NoUserFoundException
                Throw New RDAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub New(ByVal context As HttpContext, ByVal userID As String, ByVal password As String, ByVal projectName As String)
            _context = context
            Try
                'attempt login
                'padding zero to employee id

                'If userID.Length < 5 Then
                '    userID = userID.PadLeft(5, "0")
                'End If

                'Modified for 6 digit emp change on Nov,2010
                If userID.Length < 6 Then
                    userID = userID.PadLeft(6, "0")
                End If
                SessionUser = SecurityManager.Login(userID, password)
                Initialize(projectName)
                _context.Session("SessionUser") = SessionUser
            Catch ex As Security.PasswordException
                Throw New RDAppException(ex.Message)
            Catch ex As Security.NoUserFoundException
                Throw New RDAppException(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetCacheManager() As CacheManager
            Return _cacheMgr
        End Function

        Public Function GetReport(ByVal reportId As Int32) As Report
            Return ReportManager.GetReport(reportId)
        End Function

        Public Function GetReportOnly(ByVal reportId As Int32) As Report
            Dim rpt As Report = New Report(reportId)
            EntityManager.GetReportById(rpt)
            Return rpt
        End Function

        Public Sub SaveReport(ByVal rpt As Report)
            Try
                ReportManager.Save(rpt, SessionUser.UserId)
            Catch ex As SqlException
                If ex.Message.IndexOf("'IX_Report'") > -1 Then
                    Throw New RDAppException("Report Title is under use by another Report.")
                Else
                    Throw New RDAppException(Replace(ex.Message, ": rsp_UpdateReport", ""))
                End If

            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        'SRI 8/19/03 Update Submission Comment
        Public Sub UpdateReportSubmissionComment(ByVal rptSubmission As Submission)
            Try
                ReportManager.UpdateReportSubmissionComment(rptSubmission, SessionUser.UserId)
            Catch ex As SqlException
                If ex.Message.IndexOf("RSP_UpdateSubmissionComment") > -1 Then
                    Throw New RDAppException("Unexpected error while processing your request. Please try again or report to system admin if problems persist.")
                Else
                    Throw ex
                End If

            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        Public Sub SubmitReport(ByVal rptSubmission As Submission)
            Try
                If rptSubmission.InputType = FileInputType.System Then
                    'check staging for files
                    Dim searchPattern As String = rptSubmission.DefaultFileName + "*" '+ "." + rptSubmission.FileFormat
                    If Not FileSystemManager.Exists(rptSubmission.StagingPath, searchPattern) Then
                        Throw New RDAppException("No report file(s) found in the staging folder.")
                    End If
                End If

                ReportManager.Submit(rptSubmission, SessionUser.UserId)
              
                If rptSubmission.ApprovalType = ReportApprovalType.None Then
                    WorkflowManager.Approved(rptSubmission, SessionUser.UserId)
                Else
                    '*********** send email to approver ******************
                    WorkflowManager.Notify(rptSubmission, HotTopic.RD.Settings.AppSetting.SmtpServer, HotTopic.RD.Settings.AppSetting.SmtpSender, _appUrl, HotTopic.RD.Settings.AppSetting.ApproverEmailBody)
                End If
            Catch ex As SqlException
                If ex.Message.IndexOf("Duplicate report for fiscal week.") > -1 Then
                    Throw New RDAppException("A report for the same title already exists for this fiscal week.")
                ElseIf ex.Message.IndexOf("rsp_AddReportSubmission") > -1 Then
                    Throw New RDAppException("Unexpected error while processing your request. Please try again or report to system admin if problems persist.")
                Else
                    Throw ex
                End If

            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        Public Sub GetMDReportDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByRef storeDs As DataSet)
            Try
                ReportManager.Submit(rptSubmission, SessionUser.UserId)
                ReportManager.GetMDReportDataSet(rptSubmission, mdDs, storeDs)
            Catch ex As SqlException
                Throw New RDAppException(ex.Message)
            Catch ex As Exception
                Dim msg As String = ex.Message
                If msg.IndexOf("MDReport:") > -1 Then
                    msg = msg.Replace("MDReport:", "")
                    DeleteReportSubmissionById(rptSubmission.ReportSubmissionId)
                    Throw New RDAppException(msg)
                Else
                    Throw ex
                End If
            End Try
        End Sub
        Public Sub GetMDMasterReportDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByVal companyId As Int16)
            Try
                ReportManager.GetMDMasterDataSet(rptSubmission, mdDs, companyId)
            Catch ex As SqlException
                Throw New RDAppException(ex.Message)
            Catch ex As Exception
                Dim msg As String = ex.Message
                If msg.IndexOf("MDReport:") > -1 Then
                    msg = msg.Replace("MDReport:", "")
                    Throw New RDAppException(msg)
                Else
                    Throw ex
                End If
            End Try
        End Sub
        Public Sub GetMDWHReportDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByVal companyId As Int16, ByVal warehouse As String)
            Try
                ReportManager.GetMDWHDataSet(rptSubmission, mdDs, companyId, warehouse)
            Catch ex As SqlException
                Throw New RDAppException(ex.Message)
            Catch ex As Exception
                Dim msg As String = ex.Message
                If msg.IndexOf("MDReport:") > -1 Then
                    msg = msg.Replace("MDReport:", "")
                    Throw New RDAppException(msg)
                Else
                    Throw ex
                End If
            End Try
        End Sub
		Public Sub GetPromoReportDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByRef storeDs As DataSet)
			Try
				ReportManager.Submit(rptSubmission, SessionUser.UserId)
				ReportManager.GetPromoReportDataSet(rptSubmission, mdDs, storeDs)
			Catch ex As SqlException
				Throw New RDAppException(ex.Message)
			Catch ex As Exception
				Dim msg As String = ex.Message
				If msg.IndexOf("PromoReport:") > -1 Then
					msg = msg.Replace("PromoReport:", "")
					DeleteReportSubmissionById(rptSubmission.ReportSubmissionId)
					Throw New RDAppException(msg)
				Else
					Throw ex
				End If
			End Try
		End Sub
		Public Sub DeleteReportSubmissionById(ByVal rptSubmissionId As Integer)
			ReportManager.DeleteReportSubmissionById(rptSubmissionId)
		End Sub
        Public Sub SaveApprovers(ByVal reportID As Integer, ByVal approvers As ArrayList)
            Try
                ReportManager.SaveApprovers(reportID, approvers, SessionUser.UserId)
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        Public Sub SaveSubCompanyOwners(ByVal reportID As Integer, ByVal SubCompanyOwners As ArrayList)
            Try
                ReportManager.SaveSubCompanyOwners(reportID, SubCompanyOwners, SessionUser.UserId)
            Catch ex As SqlException
                Throw New RDAppException(ex.Message)
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub

        Public Sub SaveJobCodeOwners(ByVal reportId As Integer, ByVal companyId As Integer, ByVal jobCode As String, Optional ByVal oldJobCode As String = "")
            Try
                ReportManager.SaveJobCodeOwners(reportId, companyId, jobCode, SessionUser.UserId, oldJobCode)
            Catch ex As SqlException
                Throw New RDAppException(Replace(ex.Message, " : RSP_AddJobCodeOwner", ""))
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub

        Public Sub DeleteJobCodeOwners(ByVal reportId As Integer, ByVal companyId As Integer, ByVal jobCode As String)
            Try
                ReportManager.DeleteJobCodeOwner(reportId, companyId, jobCode)
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        Public Sub DeleteSubCompanyOwners(ByVal reportId As Integer)
            Try
                ReportManager.DeleteSubCompanyOwners(reportId)
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        Public Function GetFilePathByID(ByVal ReportFileID As Integer) As String
            Try
                Return FileManager.GetFilePathByID(ReportFileID, SessionUser.EmployeeId)
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Function
        Public Function IsReadOnlyFile(ByVal ReportFileID As Integer) As Boolean
            Try
                Return FileManager.IsReadOnlyFile(ReportFileID)
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Function
        Public Function ListReports() As DataSet
            Return ReportManager.List()

        End Function

        Public Sub ApproveReport(ByVal rptApproval As Approval, ByVal reportId As Integer)
            Try
                Dim submissionId As Integer
                WorkflowManager.ApproveReport(rptApproval, SessionUser.EmployeeId, SessionUser.CurrentUserRoleId)
                submissionId = rptApproval.ReportSubmissionId
                Dim rptSubmission As Submission = New Submission(reportId, submissionId, True)
                'SRI: 8/29/03 Fix to Copy files if status is archived
                If rptSubmission.Status = ReportStatus.Approved Or rptSubmission.Status = ReportStatus.Archived Then
                    WorkflowManager.Approved(rptSubmission, SessionUser.UserId)
                Else
                    'SRI: 8/14/2003 Notify Administrators if Report is declined.
                    If rptApproval.Status = ReportStatus.Declined Then
                        '*********** send email to Administrators ******************

                        Dim dt As DataTable = _cacheMgr.ListUsersByRoleId(HotTopic.RD.Settings.AppSetting.CacheDependencyFile, SessionUser.CurrentProjectId)

                        If dt.Rows.Count > 0 Then
                            WorkflowManager.NotifyAdmin(rptSubmission, HotTopic.RD.Settings.AppSetting.SmtpServer, HotTopic.RD.Settings.AppSetting.SmtpSender, _appUrl, HotTopic.RD.Settings.AppSetting.DeclinedEmailBody, dt)
                        End If
                    End If
                    'SRI: 8/14/2003 Notify approvers if Report is Cancelled.
                    '#MOD# SRI 1/14/04 send email if approval process is enabled
                    If rptApproval.Status = ReportStatus.Cancelled AndAlso rptSubmission.ApprovalType <> ReportApprovalType.None Then
                        '*********** send cancellation email to approvers ******************
                        WorkflowManager.Notify(rptSubmission, HotTopic.RD.Settings.AppSetting.SmtpServer, HotTopic.RD.Settings.AppSetting.SmtpSender, _appUrl, HotTopic.RD.Settings.AppSetting.CancelledEmailBody)
                    End If
                    If rptSubmission.ApprovalType = ReportApprovalType.All And rptApproval.Status = ReportStatus.Approved Then
                        '*********** send email to next approver ******************
                        WorkflowManager.Notify(rptSubmission, HotTopic.RD.Settings.AppSetting.SmtpServer, HotTopic.RD.Settings.AppSetting.SmtpSender, _appUrl, HotTopic.RD.Settings.AppSetting.ApproverEmailBody, SessionUser.EmployeeId)
                    End If
                End If
            Catch ex As SqlException
                If ex.Message.IndexOf("RSP_UpdateSubmissionApproval") > -1 Then
                    Throw New RDAppException("Error updating to SubmissionApproval Status")
                End If
                If ex.Message.IndexOf("RSP_ResetSubmissionStatus") > -1 Then
                    Throw New RDAppException("Error updating ReportSubmission Status")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub



        Public Function ListSubmissions() As DataSet
            Return WorkflowManager.List(SessionUser.EmployeeId, SessionUser.UserId, SessionUser.CurrentUserRoleId)
        End Function

        Public Function ListPendingApprovals() As DataSet
            Return WorkflowManager.ListPendingApprovals(SessionUser.EmployeeId, SessionUser.CurrentUserRoleId)
        End Function
        'Public Function ListSubmissions() As SubmissionDataSet

        'End Function

        'Public Function ListFiles() As FileDataSet

        'End Function
        Public Function ListFiles(ByVal storeNum As Integer, ByVal groupBy As Integer) As DataSet
            Return FileManager.List(storeNum, groupBy, SessionUser.EmployeeId, SessionUser.CurrentUserRoleId)
        End Function

        Public Function ListFilesCalendarView(ByVal fm As FiscalMonth) As DataSet
            'SRI 7/24/2003 this function is for testing
            Return FileManager.ListCalendarView(SessionUser.EmployeeId, SessionUser.CurrentUserRoleId, fm.BeginDate, fm.EndDate)
        End Function

        Public Function ListFiles(ByVal rptSubmission As Submission) As DataSet
            Try
                Return FileManager.List(rptSubmission)
            Catch ex As IO.DirectoryNotFoundException
                Throw New RDAppException("Invalid File/Folder Path")
            Catch ex As SqlException
                Throw New RDAppException("Error occured while processing")
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ListApprovals(ByVal ReportSubmissionID As Long) As FileDataSet

        End Function

        Public Function ListApproverNotes(ByVal ReportSubmissionID As Long) As DataSet
            Try
                Return WorkflowManager.ListApproverNotes(ReportSubmissionID)
            Catch ex As SqlException
                Throw New RDAppException("Error occured while processing")
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ListAllApprovers() As DataSet
            Return ReportManager.GetAllApprovers()
        End Function
        Public Function ListDmStores() As DataSet
            Try
                Return ReportManager.ListDmStores(SessionUser.EmployeeId, SessionUser.CurrentUserRoleId)
            Catch ex As SqlException
                Throw New RDAppException("Failed to Fetch District Stores")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ListAllActiveStoresByCompany(ByVal CompanyID As Int16) As DataSet
            Try
                Return ReportManager.ListAllActiveStores(CompanyID)
            Catch ex As SqlException
                Throw New RDAppException("Failed to Fetch District Stores")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ListMLUDistricts() As DataSet
            Try
                Return ReportManager.ListMLUDistricts(SessionUser.EmployeeId)
            Catch ex As SqlException
                Throw New RDAppException("Failed to Fetch Store Districts")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListAllSubCompanyOwners(ByVal ReportId As Integer) As DataSet
            Try
                Dim ds As DataSet = ReportManager.ListAllSubCompanyOwners(ReportId)
                If ds.Tables(0).Rows.Count = 0 Then
                    Throw New RDAppException("No SubCompanyOwners Found.")
                End If
                Return ds
            Catch ex As SqlException
                Throw New RDAppException("Failed to Fetch SubCompanyOwners")
            End Try
        End Function

        Public Function GetAllReportTitles(Optional ByVal inputType As FileInputType = 0) As DataSet
            Return ReportManager.GetAllReportTitles(inputType)
        End Function

        Public Function SearchFiles(ByVal ReportID As Integer, Optional ByVal FiscalWeekDate As String = "", Optional ByVal ArchivedFlag As Int16 = -1, Optional ByVal RptSubmissionID As Integer = 0) As DataSet
            Return FileManager.Search(SessionUser.EmployeeId, SessionUser.CurrentUserRoleId, ReportID, FiscalWeekDate, ArchivedFlag, RptSubmissionID)
        End Function
        Public Function PathExists(ByVal path As String) As Boolean
            Try

                Dim exists As Boolean = FileSystemManager.Exists(path)
                If exists Then
                    'now try to write
                    exists = FileSystemManager.CanWrite(path)
                End If

                Return exists

            Catch ex As System.IO.DirectoryNotFoundException
                Return False
            Catch ex As System.ArgumentException
                Throw New RDAppException("The UNC path should be of the form \\server\share.")
            Catch ex As System.Security.SecurityException
                Throw New RDAppException("No permission to access path/folder.")
            Catch ex As System.UnauthorizedAccessException
                Throw New RDAppException("No permission to access path/folder.")
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function FilesExist(ByVal path As String, ByVal searchPattern As String) As Boolean
            Try
                Return FileSystemManager.Exists(path, searchPattern)
            Catch ex As System.IO.DirectoryNotFoundException
                Return False
            Catch ex As System.ArgumentException
                Throw New RDAppException("The UNC path should be of the form \\server\share.")
            Catch ex As System.Security.SecurityException
                Throw New RDAppException("No permission to access path/folder.")
            End Try
        End Function

        'Private Function GetProjectName(ByVal rawUrl As String) As String
        '    Return rawUrl.Substring(1, rawUrl.IndexOf("/", 1) - 1).ToLower()
        'End Function

        Private Function GetModuleName(ByVal physicalPath As String) As String
            Dim fi As System.IO.FileInfo = New System.IO.FileInfo(physicalPath)
            Return fi.Name.Substring(0, fi.Name.IndexOf("."))
        End Function
        Public Function GetFiscalMonthDateRange(ByRef fm As FiscalMonth)
            Try
                _cacheMgr.GetFiscalMonthDateRange(HotTopic.RD.Settings.AppSetting.CacheDependencyFile, fm)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub SetAppUrl()
            Try
                'set app url
                _appUrl = Replace(_context.Request.Url.ToString, _context.Request.RawUrl.ToString, "") & _context.Request.ApplicationPath.ToString
            Catch ex As Exception
                'do nothing
            End Try
        End Sub

        Private Sub Initialize(ByVal projectName As String)
            Try
                _context.Session("SessionUser") = SessionUser
                'set app url
                SetAppUrl()
                Dim obj As Object = SessionUser.UserProjects(projectName.ToLower)
                If obj Is Nothing Then
                    'no access to project
                    Throw New RDAppException("No authorization to access this application.")
                Else
                    Dim userProj As UserProject = DirectCast(obj, UserProject)
                    SessionUser.CurrentProjectId = userProj.ProjectId
                    SessionUser.CurrentUserRoleId = userProj.UserRoleId
                    SessionUser.CurrentDefaultPage = userProj.DefaultPage
                    _cacheMgr = New CacheManager(_context)
                End If
            Catch ex As NullReferenceException
                Throw New RDAppException("No authorization to access this application.")
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        ' Checks if logged in user belongs to a store
        Public Function AuthenticateStoreUser() As Boolean
            Try
                Dim storeNum As String
                'call function GetStoreNum()
                storeNum = GetStoreNum()
                If SessionUser.StoreNum.ToString.PadLeft(4, "0") = storeNum Then
                    Return True
                Else
                    Throw New RDAppException("EmployeeID " & SessionUser.EmployeeId & " doesn't belong to Store #" & storeNum)
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ' Returns the storenum
        Private Function GetStoreNum() As String
            Try
                Dim storeNum As String
                If Not _context.Session("StoreNum") Is Nothing AndAlso _context.Session("StoreNum").ToString.Length > 0 Then
                    storeNum = _context.Session("StoreNum")
                Else
                    storeNum = SecurityManager.GetStoreFromIP(_context.Request.ServerVariables("REMOTE_ADDR"))
                    If storeNum = "-1" Then
                        'Try to get store num from cookie
                        Try
                            If _context.Request.Cookies("StoreNo").Value <> "" Then
                                storeNum = _context.Request.Cookies("StoreNo").Value
                            End If
                        Catch ex As NullReferenceException
                            storeNum = "-1"
                        End Try
                    End If

                    If storeNum = "-1" Then
                        storeNum = ""
                        Throw New RDAppException("You have not set your store number. <a href='../SetStore.asp'  class='cellvaluecaption'>Click here to set your store.</a><br> You will not be able to view your reports until your store number is set.")

                    Else
                        storeNum = storeNum.PadLeft(4, "0")
                        '_context.Session("StoreNum") = storeNum
                    End If
                End If
                Return storeNum
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class ' END CLASS DEFINITION Session

End Namespace ' HotTopic.RD.Application

