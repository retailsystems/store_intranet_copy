' Static Model
Imports System.Web.UI

Namespace HotTopic.RD.Application
    '*******************************************************************************
    '**		Change History
    '*******************************************************************************
    '**		Date:		Author:				Description:
    '**		--------	--------			---------------------------------------
    '**    6/18/03      Sri Bajjuri         New method SetDataGridProperties added 
    '**                                     to UIManager class.
    '*******************************************************************************

    Public Class UIManager
        Public Shared Sub SetDataGridProperties(ByVal gridname As WebControls.DataGrid, ByVal gridWidth As Integer, ByVal pageSize As Integer)

            gridname.PageSize = pageSize
            gridname.AutoGenerateColumns = False
            gridname.CellPadding = 1
            gridname.CellSpacing = 0
            gridname.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#440000") 'F7962B'  System.Drawing.ColorTranslator.FromHtml("#ffffff")
            gridname.HeaderStyle.ForeColor = System.Drawing.Color.White
            gridname.HeaderStyle.VerticalAlign = WebControls.VerticalAlign.Middle
            gridname.HeaderStyle.HorizontalAlign = WebControls.HorizontalAlign.Left
            gridname.HeaderStyle.Font.Bold = True
            gridname.ItemStyle.VerticalAlign = WebControls.VerticalAlign.Middle
            gridname.ItemStyle.HorizontalAlign = WebControls.HorizontalAlign.Left
            gridname.AlternatingItemStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#808080")
            gridname.AlternatingItemStyle.BorderStyle = Web.UI.WebControls.BorderStyle.Solid
            gridname.AlternatingItemStyle.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(2)
            gridname.FooterStyle.VerticalAlign = WebControls.VerticalAlign.Bottom
            gridname.FooterStyle.HorizontalAlign = WebControls.HorizontalAlign.Right
            gridname.GridLines = WebControls.GridLines.Both
            gridname.BorderStyle = WebControls.BorderStyle.Solid
            gridname.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(2)
            gridname.BorderColor = System.Drawing.ColorTranslator.FromHtml("#990000")
            gridname.ItemStyle.BackColor = System.Drawing.Color.Black
            'gridname.HeaderStyle.Height = System.Web.UI.WebControls.Unit.Pixel(23)
            'gridname.ItemStyle.Height = System.Web.UI.WebControls.Unit.Pixel(21)
            gridname.Width = System.Web.UI.WebControls.Unit.Pixel(gridWidth)
            gridname.Font.Size = System.Web.UI.WebControls.FontUnit.XXSmall
            gridname.Font.Name = "arial"
            gridname.ShowFooter = False
            gridname.ShowHeader = True
            gridname.AllowPaging = True
            gridname.AllowSorting = True
            gridname.AllowCustomPaging = False
            gridname.PagerStyle.Mode = WebControls.PagerMode.NumericPages
        End Sub

    End Class ' END CLASS DEFINITION UIManager

End Namespace ' HotTopic.RD.Application

