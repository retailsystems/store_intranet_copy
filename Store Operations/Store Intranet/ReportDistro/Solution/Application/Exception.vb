
Namespace HotTopic.RD.Application

    Public Class RDAppException
        Inherits ApplicationException

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

    End Class
   
    Public Class LogException

        Public Sub New()

        End Sub
        Public Shared Function LogError(ByVal logName As String, ByVal srcName As String, ByVal exp As Exception)
            Try
                Dim winLog As AIMS.Logging.WinLogger = New AIMS.Logging.WinLogger(logName, srcName)
                winLog.WriteException(exp)
                'winLog.WriteItem(exp.ToString)
            Catch ex As Exception
                'do nothing
            End Try
        End Function
    End Class

End Namespace
