<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ArchivedReports.aspx.vb"
    Inherits="ReportDistro.ArchivedReports" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>ArchivedReports</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
    <meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <!--
    <link href="<%=Application("StyleSheet")%>" type="text/css" rel="stylesheet">
    -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link id="Link1" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link id="Link2" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
     <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
     <script type="text/Javascript">

         $(document).ready(function () { setSessionTimeOut(); });
         function setSessionTimeOut() {
             console.log("Session Time out called archived reports");
             parent.clearInterval(parent.timer);
             var timeOutInterval = $('#hdnSessionTimeOut').val();
             if (timeOutInterval) {
                 parent.timer = parent.setInterval(function () {
                     alert('Active Session Expired!');
                     window.top.location.href = parent.location.origin;
                 }, (parseInt(timeOutInterval) * 60 * 1000));
             }
         }
        </script>
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->
</head>
<body bgcolor="black" text="white" vlink="white" alink="white" link="white" face="arial">
    <form id="Form1" method="post" runat="server">
    <asp:HiddenField runat="server" ID="hdnSessionTimeOut" />
        <asp:Label runat="server" ID="lblErrMsg" Visible="False" CssClass="errStyle"></asp:Label>
        <table width="100%">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="cellvaluecaption">
                                <div class="input-group" style="width: 60%">
                                    <span class="input-group-addon">Report Title</span>
                                    <asp:DropDownList ID="lstTitle" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                    </div>
                                    <br />
                                    <div class="input-group" style="width: 60%">
                                    <span class="input-group-addon">Fiscal Week Date</span>
                                    <asp:TextBox ID="txtFiscalDate" CssClass="form-control" runat="server"></asp:TextBox>                                                                        
                                </div>
                                <div>
                                <br />
                                    <asp:Button ID="btnSearch" CssClass="btn btn-danger" runat="server" Text="Search">
                                    </asp:Button>&nbsp;&nbsp;<asp:Button ID="btnClear" CausesValidation="False"
                                        CssClass="btn btn-danger" runat="server" Text="Clear"></asp:Button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="label" ErrorMessage="Please select a report title"
                                    runat="server" Display="Dynamic" ControlToValidate="lstTitle"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" CssClass="label" ErrorMessage="Invalid Date"
                                    runat="server" Display="Dynamic" ControlToValidate="txtFiscalDate" Type="Date"
                                    Operator="DataTypeCheck"></asp:CompareValidator>
                                    <br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="5">
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DataGrid ID="dgReport" Width="100%" runat="server" GridLines="Vertical" CellPadding="1" CssClass="table archtbl" HeaderStyle-BackColor="#EEEEEE" AutoGenerateColumns="false" AllowPaging="True"
                        PageSize="10" AllowSorting="True" PagerStyle-Mode="NumericPages" PagerStyle-CssClass="text-center" PagerStyle-Wrap="true">
                        <Columns>
                            <asp:TemplateColumn HeaderText="FiscalWeek" SortExpression="ReportDate">
                                <ItemTemplate>
                                    <a href="ViewReportFile.aspx?ReportFileId=<%# Container.DataItem("ReportFileID")%>"
                                        target="_blank">
                                        <%# Container.DataItem("ReportDate")%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Owner" HeaderStyle-Width="75px" SortExpression="Owner"
                                HeaderText="Owner"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Comment" HeaderStyle-Width="500px" SortExpression="Comment"
                                HeaderText="Comment"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Label ID="SortBy" Visible="False" runat="server"></asp:Label>
                    <asp:Label ID="SortDirection" Visible="False" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </form>
   
</body>
</html>
