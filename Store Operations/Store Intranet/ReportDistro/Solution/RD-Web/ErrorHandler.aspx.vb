Imports HotTopic.RD.Application
Public Class ErrorHandler
    Inherits System.Web.UI.Page
    Protected WithEvents txtDetails As System.Web.UI.WebControls.TextBox

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            If Not IsPostBack Then
                'Response.Write(ConfigurationSettings.AppSettings("ProjectName"))
                txtDetails.Text = Application("Exp").ToString
                'log the error to event log.
                HotTopic.RD.Application.LogException.LogError(ConfigurationSettings.AppSettings("EventLog"), ConfigurationSettings.AppSettings("ProjectName"), Application("Exp"))
            End If
        Catch ex As Exception

        End Try
    End Sub

End Class
