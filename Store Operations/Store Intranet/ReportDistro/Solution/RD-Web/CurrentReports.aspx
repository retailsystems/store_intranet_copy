<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CurrentReports.aspx.vb" Inherits="ReportDistro.CurrentReports" %>

<!DOCTYPE html>
<html>
<head>
    <title>Current Reports</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <%
        If Request("role") = "dm" Then
            Response.Redirect("currentReportsDm.aspx?role=dm")
        End If
    %>
    <link href='<%=Application("StyleSheet")%>' type="text/css" rel="stylesheet">
    <meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
    <meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link id="Link1" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link id="Link2" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
    <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->
    <%
        Response.Write("<SCRIPT language='Javascript'>")
        Response.Write("var plusImg = '../images/Plus" & _treeImgSfx & ".jpg';")
        Response.Write("var minusImg = '../images/Minus" & _treeImgSfx & ".jpg';")
        Response.Write("</SCRIPT>")
    %>

    <script type="text/javascript">

        $(document).ready(function () { setSessionTimeOut(); });
        function setSessionTimeOut() {
            console.log("Session Time out called Current reports");
            parent.clearInterval(parent.timer);
            var timeOutInterval = $('#hdnSessionTimeOut').val();
            if (timeOutInterval) {
                parent.timer = parent.setInterval(function () {
                    alert('Active Session Expired!');
                    window.top.location.href = parent.location.origin;
                }, (parseInt(timeOutInterval) * 60 * 1000));
            }
        }

function showtree(lngSection){
	if (eval('S' + lngSection).style.display == 'none'){
		eval('S' + lngSection).style.display = 'block';
		eval('document.Form1.I' + lngSection).src = minusImg;
	}else{
		eval('S' + lngSection).style.display = 'none';
		eval('document.Form1.I' + lngSection).src = plusImg;
	}
}
function ShowReport(){
	var id = document.Form1.grpBy[document.Form1.grpBy.selectedIndex].value;
	if(id == "1"){
		document.all.grp1.style.display = 'inline';
		document.all.grp2.style.display = 'none';	
	}
	if(id == "2"){
		document.all.grp1.style.display = 'none';
		document.all.grp2.style.display = 'inline';	
	}
}
function ShowGrid(flg){
	if (flg == 1){
	document.all.gridDiv.style.display = 'inline';	
	}else{
	document.all.gridDiv.style.display = 'none';	
	}
}
    </script>

    <script language="javascript">
       
			 function OpenPop(url,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,addressbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,'cw',winProp);
		   }
		   	 function OpenPop1(url,cw,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,addressbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,cw,winProp);
		   }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:HiddenField runat="server" ID="hdnSessionTimeOut" />
        <div>
            <div class="fl" style="width: 655px">
                <asp:Label ID="lblErrMsg" CssClass="errStyle" Visible="False" runat="server"></asp:Label>
                <div class="clearfix">
                </div>
                <div class="input-group" style="width: 60%">
                    <span class="input-group-addon"><b>
                        <%=RptCaption%>
                    </b>Reports By</span>
                    <asp:DropDownList CssClass="form-control" ID="lstGrpBy" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="1" Selected="True">Report Title</asp:ListItem>
                        <asp:ListItem Value="2">Fiscal Week</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="clearfix">
                </div>
                <br />
                <%=_treeStr%>
            </div>
        </div>
        <div class="fr">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <!--
                <div class="panel-heading">
                    Daily Reports</div>
                <%
                    Dim _salesPlanUrl As String = "#"
                    If Not IsNothing(Application("DailySalesPlanDir")) Then
                        _salesPlanUrl = Application("DailySalesPlanDir").Trim
                        If Not _salesPlanUrl.EndsWith("\") Then
                            _salesPlanUrl &= "\"
                        End If
                        _salesPlanUrl = _salesPlanUrl.Replace("\", "\\")
                        _salesPlanUrl &= "Store " & Session("SessionUser").StoreNum.ToString.PadLeft(4, "0") & ".xls"
                        '_salesPlanUrl = "ViewReportFile.aspx?FilePath=" & _salesPlanUrl
                    End If
                %>
                <a class="list-group-item" href="javascript:OpenPop('<%=_salesPlanUrl%>',700,700)">Sales
                    Plan</a>
                <asp:HyperLink ID="linkWTD" CssClass="list-group-item" Visible="False" runat="server"
                    Text="WTD By Department"></asp:HyperLink>
                <%If Not IsNothing(Application("ShowApReport")) AndAlso Application("ShowApReport").ToUpper = "TRUE" Then%>
                <a class="list-group-item" href="javascript:OpenPop1('WTDReportMain.aspx?RptType=3','wtd',790,590)">
                    Pulse Report</a>
                <%End If%>
                <asp:HyperLink ID="lnkKiosk" runat="server" Visible="False" CssClass="list-group-item"
                    Text="Ship to Store"></asp:HyperLink>
                <%If Not IsNothing(Application("ShowShkReport")) AndAlso Application("ShowShkReport").ToUpper = "TRUE" Then%>
                <asp:HyperLink ID="shkReferUrl" runat="server" Visible="True" CssClass="list-group-item"
                    Text="Shk Referral Report"></asp:HyperLink>
                <%End If%>
                -->
            </div>
        </div>
        <div class="clearfix">
        </div>
    </form>
   
</body>
</html>
