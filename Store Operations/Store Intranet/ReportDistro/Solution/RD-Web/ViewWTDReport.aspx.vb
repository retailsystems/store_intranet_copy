Imports System.Data
Imports System.Data.OleDb

Partial Class ViewWTDReport
    Inherits System.Web.UI.Page

    Private strConn As String
    Private cmd As OleDbDataAdapter
    Private dsHeader As New DataSet()
    Private dsSummary As New DataSet()
    Private dsData As New DataSet()

    Private _filePath As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            lblError.Text = ""

            If IsNumeric(Request("DistrictID")) Then
                strConn = ConfigurationSettings.AppSettings("DmWTDConnStr")
                strConn = strConn.Replace("%ownerID%", Request("DistrictID"))
            End If
            If IsNumeric(Request("StoreID")) Then
                strConn = ConfigurationSettings.AppSettings("StoreWTDConnStr")
                strConn = strConn.Replace("%ownerID%", Request("StoreID").PadLeft(4, "0"))
            End If
            If Not IsPostBack Then
                IntializeForm()
            End If
        Catch ex As Exception
            ParseException(ex)
        End Try
    End Sub
    Private Sub IntializeForm()
        SortBy.Text = "Dept"
        SortDirection.Text = "asc"
        BindSummary()
        BindData()

    End Sub

    Private Sub BindSummary()
        Try
            cmd = New OleDbDataAdapter("Select * From Header" & lblWeekPrefix.Text, strConn)
            cmd.Fill(dsHeader)

            If dsHeader.Tables(0).Rows.Count > 0 Then
                lblDistrict.Text = dsHeader.Tables(0).Rows(0).Item(0)
                'MOD 9/19/03 Hiding time part from Date field
                Select Case btnSwap.Text
                    Case "Last Week"
                        Dim rptDate As Date
                        If IsDate(dsHeader.Tables(0).Rows(1).Item(1)) Then
                            rptDate = dsHeader.Tables(0).Rows(1).Item(1)
                            rptDate = rptDate.ToShortDateString
                            lblRptDate.Text = rptDate
                        End If
                        lblRptDateCaption.Text = "Results as of:"
                    Case Else
                        lblRptDateCaption.Text = "Results for"
                        lblRptDate.Text = "Last Week"
                End Select

                'lblRptDate.Text = dsHeader.Tables(0).Rows(1).Item(1)
            End If
            cmd = New OleDbDataAdapter("Select * From Summary" & lblWeekPrefix.Text, strConn)
            cmd.Fill(dsSummary)
            dgSummary.DataSource = dsSummary.Tables(0)
            dgSummary.DataBind()
        Catch ex As OleDbException
            ParseException(ex)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindData()
        Try
            cmd = New OleDbDataAdapter("Select * From Data" & lblWeekPrefix.Text, strConn)
            cmd.Fill(dsData)
            dgData.DataSource = dsData.Tables(0)
            dsData.Tables(0).DefaultView.Sort = SortBy.Text & " " & SortDirection.Text
            dgData.DataBind()
        Catch ex As OleDbException
            ParseException(ex)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub dgData_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgData.SortCommand
        If SortBy.Text = e.SortExpression Then
            Select Case SortDirection.Text
                Case "asc"
                    SortDirection.Text = "desc"
                Case "desc"
                    SortDirection.Text = "asc"
            End Select
        Else
            SortDirection.Text = "asc"
        End If
        SortBy.Text = e.SortExpression

        BindData()
    End Sub

    Private Sub dgSummary_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSummary.ItemCreated
        If e.Item.ItemIndex = 0 Then
            e.Item.Cells(2).Text = ""
        End If
    End Sub

    Private Sub btnSwap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSwap.Click
        Select Case btnSwap.Text
            Case "Last Week"
                btnSwap.Text = "This Week"
                lblWeekPrefix.Text = "Lw"
            Case Else
                btnSwap.Text = "Last Week"
                lblWeekPrefix.Text = ""
        End Select
        IntializeForm()
    End Sub
    Private Sub ParseException(ByVal ex As Exception)
        If ex.Message.IndexOf("already opened exclusively by another user") > 0 Then
            lblError.Text = "Report is already opened exclusively by another user"
        ElseIf ex.Message.IndexOf("Make sure the object exists and that you spell its name and the path name correctly") > 0 Then
            lblError.Text = "Report is currently not available"
        ElseIf ex.Message.IndexOf("missing district identifier") > 0 Then
            lblError.Text = "Invalid/missing district identifier"
        Else
            Throw ex
        End If
    End Sub
End Class
