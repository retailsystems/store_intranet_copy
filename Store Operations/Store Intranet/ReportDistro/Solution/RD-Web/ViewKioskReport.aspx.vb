Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Imports System.IO
Imports System.Data

Public Class ViewKioskReport
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dlArticleList As System.Web.UI.WebControls.DataList

    Protected WithEvents lblStoreInfo As System.Web.UI.WebControls.Label
    Private _sessionUser As HotTopic.RD.Application.Session
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Protected dtFiles As DataTable


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        If Not Request.QueryString("StoreNo") Is Nothing Then
            Dim strMyStore As String
            strMyStore = Request.QueryString("StoreNo")
            dtFiles = New DataTable
            Dim dirInfo As New DirectoryInfo(Server.MapPath(Application("KioskAbsDir")))
            Dim dc1 As DataColumn
            Dim dc2 As DataColumn
            Dim dc3 As DataColumn

            dc1 = New DataColumn("FileName", System.Type.GetType("System.String"))
            dc2 = New DataColumn("TextName", System.Type.GetType("System.String"))
            dc3 = New DataColumn("Created", System.Type.GetType("System.String"))

            dtFiles.Columns.Add(dc1)
            dtFiles.Columns.Add(dc2)
            dtFiles.Columns.Add(dc3)
            Dim strMyPath As String

            If (strMyStore = "all") Then
                Me.lblStoreInfo.Text = "Kiosk Sales Report for All Stores"
                strMyPath = "*.xls"
            Else
                Me.lblStoreInfo.Text = "Kiosk Sales Report for Store: " & strMyStore
                strMyPath = "*_" & strMyStore & "_*.xls"
            End If

            Dim i As Integer
            Dim fi As FileInfo()
            fi = dirInfo.GetFiles(strMyPath)
            Array.Sort(fi, New CompareFileInfoEntries)
           
            Dim upper As Integer
            If fi.Length >= 9 Then
                upper = 9
            Else
                upper = fi.Length - 1
            End If

            For i = 0 To upper
                Dim dro As DataRow
                dro = dtFiles.NewRow()
                dro("FileName") = Application("KioskAbsDir") & fi(i).Name
                Dim reportDate As String
                reportDate = (fi(i).Name.Substring(fi(i).Name.Length - 12, 8))
                reportDate = reportDate.Substring(4, 2) & "/" & reportDate.Substring(6, 2) & "/" & reportDate.Substring(0, 4)
                dro("TextName") = "Kiosk Sales on " & reportDate
                dro("Created") = fi(i).CreationTime.ToShortDateString
                dtFiles.Rows.Add(dro)

            Next

            dlArticleList.DataSource = dtFiles
            ' dtFiles.DefaultView.Sort = "Created DESC"


            dlArticleList.DataBind()
            dtFiles.Clear()
            dtFiles = Nothing
            dirInfo = Nothing
        End If

    End Sub

End Class

Public Class CompareFileInfoEntries
    Implements IComparer

    Public Overridable Overloads Function Compare(ByVal file1 As _
           Object, ByVal file2 As Object) _
           As Integer Implements IComparer.Compare
        'Convert file1 and file2 to FileInfo entries
        Dim f1 As FileInfo = CType(file1, FileInfo)
        Dim f2 As FileInfo = CType(file2, FileInfo)
        Dim f1Date As String
        Dim f2Date As String
        f1Date = (f1.Name.Substring(f1.Name.Length - 12, 8))
        f1Date = f1Date.Substring(4, 2) & "/" & f1Date.Substring(6, 2) & "/" & f1Date.Substring(0, 4)
        f2Date = (f2.Name.Substring(f2.Name.Length - 12, 8))
        f2Date = f2Date.Substring(4, 2) & "/" & f2Date.Substring(6, 2) & "/" & f2Date.Substring(0, 4)

        Return DateTime.Compare(DateTime.Parse(f2Date), DateTime.Parse(f1Date))

         
    End Function
End Class
