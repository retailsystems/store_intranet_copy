Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Public Class ManageReportOwners
    Inherits System.Web.UI.Page
    Private ds As DataSet
    Private dt As DataTable
    Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lstCompany As System.Web.UI.WebControls.DropDownList
    Protected WithEvents JobCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents pnlNew As System.Web.UI.WebControls.Panel
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents dg As System.Web.UI.WebControls.DataGrid
    Private dr As DataRow
    Private _sessionUser As HotTopic.RD.Application.Session
    Private _reportID As Integer
    Private jobCodeArr As ArrayList = New ArrayList()
    Private _companyArr As ArrayList = New ArrayList()
    Protected WithEvents btnDone As System.Web.UI.WebControls.Button
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'ToDo: update and delete functionality are incomplete
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current, 1)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        _reportID = CInt("0" + Request("ReportID"))

        If Not IsPostBack Then



            BindData()


        End If
    End Sub
    Private Sub BindData()

        Dim rpt As New Report()

        rpt = _sessionUser.GetReport(_reportID)

        jobCodeArr = rpt.JobCodeOwners
        dg.DataSource = GetDataSetFromArrayList(jobCodeArr)
        UIManager.SetDataGridProperties(dg, 520, 10)
        dg.AllowPaging = False
        dg.DataBind()
        'populate company list
        _companyArr = rpt.CompanyOwners
        Dim i As Integer = 0
        Dim company As CompanyOwner
        For i = 0 To _companyArr.Count - 1
            company = _companyArr(i)
            lstCompany.Items.Add(New ListItem(company.CompanyName, company.CompanyId))

        Next
    End Sub
    Private Function GetDataSetFromArrayList(ByVal arrayList As ArrayList) As DataSet
        ds = New DataSet()
        dt = New DataTable()
        dt.TableName = "Table1"
        dt.Columns.Add("CompanyID")
        dt.Columns.Add("CompanyName")
        dt.Columns.Add("JobCode")
        Dim i As Integer

        For i = 0 To arrayList.Count - 1
            Dim _jcOwner As JobCodeOwner = New JobCodeOwner()
            _jcOwner = arrayList(i)
            With _jcOwner
                dr = dt.NewRow
                dr.Item(0) = .CompanyId
                dr.Item(1) = .CompanyName
                dr.Item(2) = .JobCode
                dt.Rows.Add(dr)
            End With
        Next
        ds.Tables.Add(dt)
        Return ds
    End Function

    Private Sub dg_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dg.EditCommand
        dg.EditItemIndex = e.Item.ItemIndex
        pnlNew.Visible = False
        BindData()
        'Dim myDeleteButton As TableCell
        'myDeleteButton = e.Item.Cells(4)

        'myDeleteButton.Enabled = False
        'myDeleteButton.Visible = False
        'myDeleteButton.Text = ""

    End Sub

    Private Sub dg_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dg.CancelCommand
        dg.EditItemIndex = -1
        pnlNew.Visible = False
        BindData()
    End Sub

    Private Sub dg_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dg.DeleteCommand
        Try
            Dim companyId As Integer
            Dim _companyID As TextBox
            Dim _jobCode As Label
            _companyID = e.Item.FindControl("txtCompanyId")
            companyId = CInt("0" + _companyID.Text)
            _jobCode = e.Item.FindControl("lblJobCode")
            _sessionUser.DeleteJobCodeOwners(_reportID, companyId, _jobCode.Text)
            dg.EditItemIndex = -1
            pnlNew.Visible = False
            BindData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dg_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dg.UpdateCommand
        Try
            Dim companyId As Integer
            Dim _jobCode, _oldCode, _companyID As TextBox
            _companyID = e.Item.FindControl("txtCompanyId")
            companyId = CInt("0" + _companyID.Text)
            _jobCode = e.Item.FindControl("txtJobCode")
            _oldCode = e.Item.FindControl("txtOldCode")
            If Trim(_oldCode.Text) <> Trim(_jobCode.Text) Then
                _sessionUser.SaveJobCodeOwners(_reportID, companyId, _jobCode.Text, _oldCode.Text)
            End If
            dg.EditItemIndex = -1
            pnlNew.Visible = False
            BindData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        InitializeForm()
        pnlNew.Visible = True
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlNew.Visible = False
        BindData()
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            _sessionUser.SaveJobCodeOwners(_reportID, lstCompany.SelectedItem.Value, JobCode.Text)
            pnlNew.Visible = False
            BindData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dg_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dg.ItemCreated
        Dim myDeleteButton As TableCell
        myDeleteButton = e.Item.Cells(4)
        myDeleteButton.Attributes.Add("onclick", "return confirm('Are you Sure you want to delete this JobCode?');")
    End Sub
    Private Sub InitializeForm()
        lstCompany.SelectedItem.Selected = False
        lstCompany.SelectedIndex = 0
        JobCode.Text = ""
    End Sub

    Private Sub btnDone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDone.Click
        Response.Redirect("ViewReport.aspx?ReportId=" & _reportID)
    End Sub
End Class
