Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Public Class ManageApprovers
    Inherits System.Web.UI.Page
    Protected WithEvents lstUsers As System.Web.UI.WebControls.ListBox
    Protected WithEvents moveUp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents moveRight As System.Web.UI.WebControls.ImageButton
    Protected WithEvents moveLeft As System.Web.UI.WebControls.ImageButton
    Protected WithEvents moveDown As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lstApprovers As System.Web.UI.WebControls.ListBox

    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Private dsApprovers As DataSet
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Private approverStr As String
    Private _reportID As Integer
    Private _sessionUser As HotTopic.RD.Application.Session
    Private _rpt As Report
    Private _approverArr As ArrayList
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        _reportID = CInt("0" + Request("ReportID"))
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current, 1)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        If Not IsPostBack Then
            'lstUsers = _sessionUser.ListAllApprover
            _rpt = _sessionUser.GetReport(_reportID)
            _approverArr = _rpt.Approvers
            If _approverArr.Count > 0 Then
                Dim i As Integer = 0
                Dim approver As Approver
                For i = 0 To _approverArr.Count - 1
                    approver = _approverArr(i)
                    lstApprovers.Items.Add(New ListItem(approver.FirstName & " " & approver.LastName, approver.EmployeeId))
                Next
            End If
            dsApprovers = _sessionUser.ListAllApprovers
            lstUsers.DataValueField = "EmployeeID"
            lstUsers.DataTextField = "FullName"
            lstUsers.DataSource = dsApprovers
            lstUsers.DataBind()
        End If
        ' Response.Write(Session("SessionUser"))
    End Sub
    Private Sub moveDown_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles moveDown.Click
        Dim ind As Integer

        If lstApprovers.SelectedIndex > -1 And lstApprovers.SelectedIndex < lstApprovers.Items.Count - 1 Then
            ind = lstApprovers.SelectedIndex
            Dim li, li1 As ListItem
            li = lstApprovers.SelectedItem
            li1 = lstApprovers.Items(ind + 1)
            lstApprovers.Items.RemoveAt(ind)
            lstApprovers.Items.Insert(ind, li1)
            lstApprovers.Items.RemoveAt(ind + 1)

            lstApprovers.Items.Insert(ind + 1, li)
            lstApprovers.SelectedIndex = -1
            lstApprovers.SelectedIndex = ind + 1
        End If
    End Sub

    Private Sub moveLeft_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles moveLeft.Click
        If lstApprovers.SelectedIndex > -1 Then
            lstUsers.Items.Add(lstApprovers.SelectedItem)
            lstApprovers.Items.Remove(lstUsers.SelectedItem)
            lstUsers.SelectedIndex = -1
        End If
    End Sub

    Private Sub moveRight_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles moveRight.Click
        Dim li As ListItem
        Dim i As Integer
        Dim indArr As ArrayList
        indArr = New ArrayList()
        For Each li In lstUsers.Items
            If li.Selected Then
                If lstApprovers.Items.IndexOf(li) = -1 Then
                    lstApprovers.Items.Add(li)
                    'lstUsers.Items.Remove(li)
                    indArr.Add(li)
                End If
            End If
        Next
        If indArr.Count > 0 Then
            For i = 0 To indArr.Count - 1
                lstUsers.Items.Remove(indArr(i))
            Next
        End If
        lstApprovers.SelectedIndex = -1
        'If lstUsers.SelectedIndex > -1 Then
        '    lstApprovers.Items.Add(lstUsers.SelectedItem)
        '    lstUsers.Items.Remove(lstUsers.SelectedItem)
        '    lstApprovers.SelectedIndex = -1
        'End If

    End Sub

    Private Sub moveUp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles moveUp.Click
        Dim ind As Integer

        If lstApprovers.SelectedIndex > 0 Then
            ind = lstApprovers.SelectedIndex
            Dim li, li1 As ListItem
            li = lstApprovers.SelectedItem
            li1 = lstApprovers.Items(ind - 1)
            lstApprovers.Items.RemoveAt(ind - 1)
            lstApprovers.Items.Insert(ind - 1, li)
            lstApprovers.Items.RemoveAt(ind)
            lstApprovers.Items.Insert(ind, li1)

            lstApprovers.SelectedIndex = -1
            lstApprovers.SelectedIndex = ind - 1
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            approverStr = ""
            Dim approverArr As ArrayList
            Dim approver As HotTopic.RD.Entity.Approver
            approverArr = New ArrayList()
            Dim li As ListItem
            For Each li In lstApprovers.Items
                approver = New HotTopic.RD.Entity.Approver(li.Value)

                approverArr.Add(approver)
            Next
            'For Each li In lstApprovers.Items
            '    If Len(approverStr) > 0 Then
            '        approverStr = approverStr & "/"
            '    End If
            '    approverStr = approverStr & li.Value
            'Next
            'Response.Write(approverStr)
            'Response.End()
            _sessionUser.SaveApprovers(_reportID, approverArr)
            If _sessionUser.ErrCode = 0 Then
                If Request("mode") = "new" Then
                    Response.Redirect("ManageReportOwners.aspx?mode=new&ReportID=" & _reportID)
                Else
                    Response.Redirect("ViewReport.aspx?ReportID=" & _reportID)
                End If
            Else
                lblError.Text = _sessionUser.ErrMsg
            End If
        Catch ex As Exception
            lblError.Text = "Following Error Occured while processing your request :" & ex.Message
        End Try
    End Sub
End Class
