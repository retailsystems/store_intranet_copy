<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CurrentReportsDM.aspx.vb"
    Inherits="ReportDistro.CurrentReportsDM" %>

<!DOCTYPE html>
<html>
<head>
    <title>CurrentReportsDM</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link href="<%=Application("StyleSheet")%>" type="text/css" rel="stylesheet">
    <meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
    <meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link id="Link1" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link id="Link2" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->
    <%
        Response.Write("<SCRIPT language='Javascript'>")
        Response.Write("var plusImg = '../images/Plus" & _treeImgSfx & ".jpg';")
        Response.Write("var minusImg = '../images/Minus" & _treeImgSfx & ".jpg';")
        Response.Write("</SCRIPT>")
    %>

    <script language="Javascript">
function showtree(lngSection){
	//alert(lngSection);
	if (eval('S' + lngSection).style.display == 'none'){
		eval('S' + lngSection).style.display = 'block';
		eval('document.Form1.I' + lngSection).src = minusImg;
	}else{
		eval('S' + lngSection).style.display = 'none';
		eval('document.Form1.I' + lngSection).src =  plusImg;
	}
}
function ShowReport(){
	var id = document.Form1.grpBy[document.Form1.grpBy.selectedIndex].value;
	if(id == "1"){
		document.all.grp1.style.display = 'inline';
		document.all.grp2.style.display = 'none';	
	}
	if(id == "2"){
		document.all.grp1.style.display = 'none';
		document.all.grp2.style.display = 'inline';	
	}
}
function ShowGrid(flg){
	if (flg == 1){
	document.all.gridDiv.style.display = 'inline';	
	}else{
	document.all.gridDiv.style.display = 'none';	
	}
}
    </script>

    <script language='javascript'>
			 function OpenPop(url,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'toolbar=no,status=no,menubar=yes,resizable=yes,directories=no,location=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,'cw',winProp);
		   }
		   	 function OpenPop1(url,cw,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'toolbar=no,status=no,menubar=yes,resizable=yes,directories=no,location=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,cw,winProp);
		   }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div class="fl" style="width: 502px; position: relative">
            <!-- new Content here -->
            <asp:Label runat="server" ID="lblErrMsg" Visible="False" CssClass="errStyle"></asp:Label>
            <div class="clearfix">
            </div>
            <div class="input-group" style="width: 60%">
                <span class="input-group-addon"><b>
                    <%=RptCaption%>
                </b>Reports By</span>
                <asp:DropDownList CssClass="form-control" ID="lstGrpBy" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="1" Selected="True">Report Title</asp:ListItem>
                    <asp:ListItem Value="2">Fiscal Week</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="clearfix">
            </div>
            <br />
            <%=_treeStr%>
        </div>
        <div class="fr" style="position: absolute; right: -170px; top: 77px">
            <div class="panel panel-default">
            <!--
                <div class="panel-heading">
                    Daily Reports</div>
                <a href="javascript:OpenPop1('WTDReportMain.aspx?RptType=2','wtd',750,700)" class="list-group-item">
                    Sales Plan</a> <a href="javascript:OpenPop1('WTDReportMain.aspx','wtd',750,550)"
                        class="list-group-item">WTD By Department</a>
                <%If Not IsNothing(Application("ShowApReport")) AndAlso Application("ShowApReport").ToUpper = "TRUE" Then%>
                <a href="javascript:OpenPop1('WTDReportMain.aspx?RptType=3','wtd',790,590)" class="list-group-item">
                    Pulse Report</a>
                <%End If%>
                <%If Not IsNothing(Application("ShowShkReport")) AndAlso Application("ShowShkReport").ToUpper = "TRUE" Then%>
                <asp:HyperLink ID="shkReferUrl" runat="server" Visible="True" CssClass="list-group-item"
                    Text="Shk Referral Report"></asp:HyperLink>
                <asp:HyperLink ID="shkEmpReferUrl" runat="server" Visible="True" CssClass="list-group-item"
                    Text="Shk Emp Referral Rpt"></asp:HyperLink>
                <%End If%>
                <%If Not IsNothing(Application("ShowHTReport")) AndAlso Application("ShowHTReport").ToUpper = "TRUE" Then%>
                <asp:HyperLink ID="lnkHTPlus" runat="server" Visible="False" CssClass="list-group-item"
                    Text="HT+1 Capture WTD"></asp:HyperLink>
                <%End If%>
            -->
            </div>
        </div>
        <div class="clearfix">
        </div>
    </form>
    <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
</body>
</html>
