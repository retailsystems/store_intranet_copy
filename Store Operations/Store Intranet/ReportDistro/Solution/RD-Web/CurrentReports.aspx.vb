Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Partial Class CurrentReports
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.RD.Application.Session
    Protected _treeStr As String
    Private _grpBy As Integer = 1
    Protected rptCaption As String
    'Added by Davendar 7/9/09
    'Added by Davendar 12/22/2009
    'Protected WithEvents lnkHTPlus As System.Web.UI.WebControls.HyperLink
    '6/1/04 tree image variables
    Protected _treeImgSfx As String = ""

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblErrMsg.Text = ""
        lblErrMsg.Visible = False
        Try
            'See if the user has access to Kiosk reports
            
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
            SetKioskPermission(_sessionUser.SessionUser.StoreNum, _sessionUser.SessionUser.CurrentUserRoleId)
            'Added by Davendar 7/9/09
            SetShkReferPermission(_sessionUser.SessionUser.StoreNum, _sessionUser.SessionUser.CurrentUserRoleId, _sessionUser.SessionUser.JobCode)
            'Added by Davendar 11/19/2009
            'SetHTCapturePermission(_sessionUser.SessionUser.StoreNum, _sessionUser.SessionUser.CurrentUserRoleId, _sessionUser.SessionUser.JobCode)

            Select Case _sessionUser.SessionUser.CurrentUserRoleId
                Case UserRole.DM
                    rptCaption = "District " & _sessionUser.SessionUser.DistrictId
                Case UserRole.Store
                    rptCaption = "Store " & _sessionUser.SessionUser.StoreNum.ToString.PadLeft(4, "0")
                    linkWTD.NavigateUrl = "javascript:OpenPop('ViewWTDReport.aspx?StoreId=" & _sessionUser.SessionUser.StoreNum.ToString.PadLeft(4, "0") & "',600,550)"
                    linkWTD.Visible = True
            End Select
            hdnSessionTimeOut.Value = Session.Timeout
            '6/1/04 initialize the _treeImgSfx
            If Not IsNothing(Application("Mode")) AndAlso Application("Mode").toUpper = "TORRID" Then
                _treeImgSfx = "_TD"
            End If
            If Not IsPostBack Then
                If Request("GrpBy") <> "" Then
                    _grpBy = Request("GrpBy")
                    lstGrpBy.SelectedIndex = -1
                    lstGrpBy.Items.FindByValue(_grpBy).Selected = True
                Else
                    _grpBy = 1
                End If
                BuildTree()
            End If

        Catch ex As RDAppException
            If ex.Message = "Unauthenticated user." Then
                Dim jsStr As String = ""
                jsStr += "<script>"
                jsStr += "alert('Active Session Expired!');"
                jsStr += "window.top.location.reload();"
                jsStr += "</script>"
                Response.Write(jsStr)
                Response.End()
            End If

            lblErrMsg.Text = ex.Message
            lblErrMsg.Visible = True
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
      
    End Sub
    Private Sub BuildTree()
        _treeStr = ""
        Dim ds As DataSet = _sessionUser.ListFiles(_sessionUser.SessionUser.StoreNum, _grpBy)
        Dim nodeName, nodeID As String
        Dim cnt As Integer = 0
        Dim dr As DataRow
        Select Case _grpBy
            Case 1
                ' group by title
                For Each dr In ds.Tables(0).Rows

                    If nodeID <> dr("reportID") Then
                        If cnt > 0 Then
                            _treeStr = _treeStr & "</div>"
                        End If
                        nodeName = dr("ReportTitle")
                        nodeID = dr("reportID")
                        BuildNodeHtml(nodeID, nodeName)
                    End If
                    BuildLeafHtml(dr)
                    cnt = cnt + 1
                Next
            Case 2
                'group by fiscal week
                For Each dr In ds.Tables(0).Rows

                    If nodeID <> dr("fiscalWeek") Then
                        If cnt > 0 Then
                            _treeStr = _treeStr & "</div>"
                        End If
                        nodeName = "Week " & dr("ReportDate")
                        nodeID = dr("fiscalWeek")
                        BuildNodeHtml(nodeID, nodeName)
                    End If
                    BuildLeafHtmlByWeek(dr)
                    cnt = cnt + 1
                Next

        End Select
       
    End Sub
    Private Sub BuildNodeHtml(ByVal nodeID As String, ByVal nodeName As String)
        _treeStr = _treeStr & "<div><span class='pls'><IMG onclick=javascript:showtree(" & nodeID & ") height=11 src='../images/Plus" & _treeImgSfx & ".jpg' width=17 border=0 name=I" & nodeID & "></span>" & _
                    "" & nodeName & "</div>" & _
                    "<div id=S" & nodeID & " style='DISPLAY: none'>"
         
    End Sub
    Private Sub BuildLeafHtml(ByVal dr As DataRow)
        Dim desc As String = ""
        Dim newFile As String = ""
        If dr("isAccessed") = 0 Then
            newFile = "&nbsp;<img src='images/new.gif' border=0>&nbsp;"
        Else
            newFile = "</a>"
        End If
        _treeStr = _treeStr & "<span class='pls'><IMG src='../images/Line2" & _treeImgSfx & ".jpg' border=0>" & _
                                "<IMG src='../images/Line3_1" & _treeImgSfx & ".jpg' border=0></span>" & _
                                "<div>" & newFile & "<a href='ViewReportFile.aspx?reportFileId=" & dr("ReportFileId") & "' onclick='setSessionTimeOut();' target='_blank'><FONT>week " & dr("ReportDate") & "</FONT></a>"
        If Len(dr("Comment")) > 65 Then
            _treeStr = _treeStr & "&nbsp;-&nbsp;" & Left(dr("Comment"), 65) & " ...<a href=""javascript:OpenPop('More.aspx?ReportId=" & dr("reportId") & "&submissionID=" & dr("ReportSubmissionId") & "',550,350)"" ><FONT color=#0099cc>more</font></a></div>"
        Else
            _treeStr = _treeStr & "&nbsp;-&nbsp;" & dr("Comment") & "</div>"
        End If
        _treeStr = _treeStr & ""

       
    End Sub
    Private Sub BuildLeafHtmlByWeek(ByVal dr As DataRow)
        Dim desc As String = ""
        Dim newFile As String = ""
        If dr("isAccessed") = 0 Then
            newFile = "&nbsp;<img src='images/new.gif' border=0>&nbsp;"
        Else
            newFile = ""
        End If
        _treeStr = _treeStr & "<span class='pls'><IMG src='../images/Line2" & _treeImgSfx & ".jpg' border=0>" & _
                                "<IMG src='../images/Line3_1" & _treeImgSfx & ".jpg' border=0></span>" & _
                                "<div>" & newFile & "<a href='ViewReportFile.aspx?reportFileId=" & dr("ReportFileId") & "' onclick='setSessionTimeOut();' target='_blank'><FONT>" & dr("ReportTitle") & "</FONT></a>"
        If Len(dr("Comment")) > 65 Then
            _treeStr = _treeStr & "&nbsp;-&nbsp;" & Left(dr("Comment"), 65) & " ...<a href=""javascript:OpenPop('More.aspx?ReportId=" & dr("reportId") & "&submissionID=" & dr("ReportSubmissionId") & "',550,350)"" ><FONT color=#0099cc>more</font></a>"
        Else
            _treeStr = _treeStr & "&nbsp;-&nbsp;" & dr("Comment") & ""
        End If
        _treeStr = _treeStr & "</div>"


    End Sub

    Private Sub lstGrpBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstGrpBy.SelectedIndexChanged
        If lstGrpBy.SelectedItem.Value = 3 Then
            Server.Transfer("CalendarView.aspx")
        Else
            _grpBy = lstGrpBy.SelectedItem.Value
            BuildTree()
        End If
        
    End Sub


    Private Sub SetKioskPermission(ByVal StoreNum As Short, ByVal RoleId As Short)
        Dim isStore As Boolean
        isStore = False

        If RoleId = UserRole.Store Then


            Dim strStores As String()
            If Not Application("KioskList") Is Nothing Then
                strStores = Application("KioskList")
            End If
            Dim i As Integer
            For i = 0 To strStores.Length - 1
                If StoreNum = Integer.Parse(strStores(i)) Then
                    isStore = True
                    Exit For
                End If
            Next

        End If

        lnkKiosk.NavigateUrl = "javascript:OpenPop('WTDReportMain.aspx?StoreId=" & _sessionUser.SessionUser.StoreNum.ToString.PadLeft(4, "0") & "&RptType=4',600,550)"

        If (RoleId = UserRole.Store And isStore = True) Or RoleId = UserRole.Admin Then
            lnkKiosk.Visible = True
            'ElseIf RoleId <> UserRole.MLU Then
            '    lnkKiosk.Visible = True
        Else
            lnkKiosk.Visible = False
        End If


    End Sub

    'Added by Davendar 7/9/09
    Private Sub SetShkReferPermission(ByVal StoreNum As Short, ByVal RoleId As Short, ByVal JobCode As String)
        Dim _shkReferUrl As String = "#"
        If Not IsNothing(Application("ShkReferRptDir")) Then
            _shkReferUrl = Application("ShkReferRptDir").Trim
            If Not _shkReferUrl.EndsWith("/") Then
                _shkReferUrl &= "/"
            End If
        End If

        If (RoleId = UserRole.Store And JobCode.Trim.ToString = "SM") Then

            _shkReferUrl &= "District/District_" & _sessionUser.SessionUser.DistrictId.ToString & "_Referral_Report.xls"

        Else
            _shkReferUrl &= "Store/Store_" & Session("SessionUser").StoreNum.ToString & "_Referral_Report.xls"

        End If

        '_shkReferUrl &= "&role=" & RoleId & "&JC=" & JobCode
        shkReferUrl.NavigateUrl = "javascript:OpenPop('" & _shkReferUrl & "',600,550)"

    End Sub



    'Removed by Davendar Rai on 04/16/2010
    ' Added by Davendar Rai on 11/19/2009
    'Modified by Davendar Rai on 12/22/2009 - changed Url link to xls to link page with historical report links based on District
    'Private Sub SetHTCapturePermission(ByVal StoreNum As Short, ByVal RoleId As Short, ByVal JobCode As String)

    '    lnkHTPlus.NavigateUrl = "javascript:OpenPop('WTDReportMain.aspx?DistrictId=" & _sessionUser.SessionUser.DistrictId.ToString.PadLeft(3, "0") & "&RptType=5',600,550)"

    '    If (RoleId = UserRole.Store) Then
    '        lnkHTPlus.Visible = False

    '    Else
    '        lnkHTPlus.Visible = False

    '    End If

    'End Sub

End Class
