Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Partial Class ReportMain
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.RD.Application.Session

    Private _employeeId As String
    Protected blnLoginFail As Boolean = False
    Protected FormAction As String
    Protected Caption As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
           

            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            lblErrMsg.Text = ""
            lblErrMsg.Visible = False
            If Request("EmpId") <> "" Then
                _employeeId = Request("EmpId")
            Else
                Try
                    '# TODO - enabled for production
                    If Request.Cookies("User").HasKeys Then
                        _employeeId = Request.Cookies("User")("UserName").ToString
                    End If


                Catch ex As NullReferenceException

                End Try
            End If
            If _employeeId = "" Then
                _employeeId = 0
            End If
            _sessionUser = New Session(HttpContext.Current, _employeeId, ConfigurationSettings.AppSettings("ProjectName"))
            'If _sessionUser.SessionUser.JobCode = "DM" Then
            '    Session("role") = "DM"
            'End If
            Select Case _sessionUser.SessionUser.CurrentUserRoleId
                Case UserRole.DM
                    FormAction = "CurrentReportsDM.aspx"
                    Caption = "District " & _sessionUser.SessionUser.DistrictId
                Case UserRole.Store
                    ''check the store cookie for a match                  
                    'Try
                    '    If RemoveLeadingZero(Request.Cookies("StoreNo").Value) <> _sessionUser.SessionUser.StoreNum Then
                    '        Session.Abandon()
                    '        Throw New RDAppException("The EmployeeID " & _sessionUser.SessionUser.EmployeeId & " doesn't belong to Store " & Request.Cookies("StoreNo").Value)
                    '    End If
                    'Catch ex As NullReferenceException
                    '    Session.Abandon()
                    '    Throw New RDAppException("You have not set your store number. <a href='../SetStore.asp'  class='cellvaluecaption'>Click here to set your store.</a><br> You will not be able to view your reports until your store number is set.")
                    'End Try
                    ' MOD #1/22/2004# AUTH:Sri -  storeNum check logic moved to session.vb
                    _sessionUser.AuthenticateStoreUser()
                    FormAction = "CurrentReports.aspx"
                    Caption = "Store " & _sessionUser.SessionUser.StoreNum.ToString.PadLeft(4, "0")
                Case UserRole.RM
                    FormAction = "CurrentReportsDM.aspx"
                    Caption = "Region " & _sessionUser.SessionUser.RegionId
                Case UserRole.MLU
                    FormAction = "CurrentReportsDM.aspx"
                    Caption = "Market: " & _sessionUser.SessionUser.Market
                Case Else
                    Throw New RDAppException("No authorization to access this application.")
            End Select

        Catch ex As RDAppException
            blnLoginFail = True
            lblErrMsg.Text = ex.Message
            lblErrMsg.Visible = True
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Function RemoveLeadingZero(ByVal inStr As String) As Integer
        inStr = Trim(inStr)
        If inStr = "" Then
            Return 0
        Else

            Dim chArr As String
            Dim i As Integer = 0
            For i = 0 To inStr.Length - 1
                If inStr.Chars(i) = "0" Then
                    chArr += "0"
                End If
            Next
            'inStr.TrimStart(chArr)
            inStr.Replace(chArr, "")
            inStr = Trim(inStr)
            If IsNumeric(inStr) Then
                Return CInt(inStr)
            Else
                Return 0
            End If
        End If
    End Function

End Class
