Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Partial Class More
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.RD.Application.Session
    Private _rptSubmission As Submission
    Private _submissionId As Integer = 0
    Private _reportId As Integer = 0
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            lblErrMsg.Text = ""
            lblErrMsg.Visible = False
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
        Catch ex As RDAppException
            lblErrMsg.Text = ex.Message
            lblErrMsg.Visible = True
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try

        If Not IsPostBack Then
            If IsNumeric(Request("reportID")) Then
                _reportId = Request("reportID")
            End If
            If IsNumeric(Request("submissionId")) Then
                _submissionId = Request("submissionId")
            End If

            _rptSubmission = New Submission(_reportId, _submissionId)
            With _rptSubmission
                lblTitle.Text = .Title
                lblFiscalWeek.Text = .ReportDate
                lblComment.Text = .Comment
            End With
            '_sessionUser.

        End If
    End Sub

End Class
