Imports HotTopic.RD.Application
Imports System.IO
Imports sautinsoftlocal
Imports Microsoft.Office.Interop.Excel

'Imports HotTopic.RD.Entity
Partial Class ViewReportFile
    Inherits System.Web.UI.Page
    Private _reportFileId As Integer = 0
    Private _fileName As String
    Private _sessionUser As HotTopic.RD.Application.Session
    Private _blnReadOnly As Boolean = True
    Private _forceDownLoadFlag As Boolean = False

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        
        If IsNumeric(Request("reportFileId")) Then
            Try
                'need to pass httpcontext at every page load
                'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
                _sessionUser = New Session(HttpContext.Current)
            Catch ex As Exception
                If ex.Message = "Unauthenticated user." Then
                    'Response.Redirect("../SetStore.asp")

                    Dim jsStr As String = ""
                    jsStr += "<script>"
                    jsStr += "alert('Active Session Expired!');"
                    jsStr += "window.top.location.href=parent.location.protocol + '//' + parent.location.hostname + (parent.location.port ? ':' + parent.location.port: '');"
                    jsStr += "</script>"
                    Response.Write(jsStr)
                    Response.End()
                    'loginRedirect.Visible = True
                    Exit Sub
                End If
                'Throw ex 'ASP.NET error page should handle
            End Try
        End If
        If Not IsPostBack Then
            If IsNumeric(Request("reportFileId")) Then
                _reportFileId = Request("ReportFileID")
                _fileName = _sessionUser.GetFilePathByID(_reportFileId)


            Else
                _fileName = Request("FilePath")

                _fileName = _fileName.Replace("/", "\")
            End If
            If Not IsNothing(Request("ForceDownload")) AndAlso Request("ForceDownload").ToUpper = "TRUE" Then
                _forceDownLoadFlag = True
            End If
            'SRI:7/29/03 IF Report ReadonlyFlag is OFF open the file in EDIT Mode.
            If _fileName <> "" Then
                If _reportFileId > 0 Then
                    _blnReadOnly = _sessionUser.IsReadOnlyFile(_reportFileId)
                End If
                If Not _blnReadOnly Then
                    _fileName = _fileName.Replace("\", "\\")

                    'Response.Redirect(_fileName)
                    Response.Write("<script>window.location.href='" & _fileName & "';</script>")
                    Response.End()
                End If
                Dim BrowserName As String = Request.Browser.Browser
                If Path.GetExtension(_fileName) = ".xls" And BrowserName.ToLower().Contains("safari") Then
                    If File.Exists(_fileName) Then
                        Dim tempPath As String = Server.MapPath("~/tempPDFs/" & Path.GetFileNameWithoutExtension(_fileName) & ".pdf")
                        If System.IO.File.Exists(tempPath) Then
                            System.IO.File.Delete(tempPath)
                        End If
                        Dim x As New SautinSoft.ExcelToPdf
                        x.Serial = "10044127362"
                        x.PageStyle.PageSize.A4()


                        x.OutputFormat = SautinSoft.ExcelToPdf.eOutputFormat.Pdf

                        x.ConvertFile(_fileName, tempPath)
                        ' ConvertToPDF(_fileName, tempPath)
                        _fileName = tempPath
                        DownloadFile(_fileName, deleteFile:=True)
                        'DownloadFile(_fileName)

                    Else
                        Response.Write("<script>alert('Selected Report File is currently not available1!');self.close();</script>")
                        Response.End()
                    End If
                Else
                    DownloadFile(_fileName)
                End If





            End If
        End If

    End Sub
    Private Sub DownloadFile(ByVal filePath As String, Optional ByVal ForceDownLoad As Boolean = False, Optional ByVal deleteFile As Boolean = False)

        Try


            If File.Exists(filePath) Then
                Dim name = Path.GetFileName(filePath)
                Dim ext = Path.GetExtension(filePath)
                Dim type As String = ""

                If Not IsDBNull(ext) Then
                    ext = LCase(ext)
                End If

                Select Case ext
                    Case ".htm", ".html"
                        type = "text/HTML"
                    Case ".txt"
                        type = "text/plain"
                    Case ".doc", ".rtf"
                        type = "Application/msword"
                    Case ".csv", ".xls"
                        type = "Application/x-msexcel"
                    Case ".xlsb"
                        type = "application/vnd.ms-excel.sheet.binary.macroEnabled.12"
                    Case ".pdf", ".PDF"
                        type = "Application/pdf"
                    Case Else
                        type = "text/plain"
                End Select

                Response.Clear()
                If type <> "" Then
                    Response.ContentType = type
                End If
                If _forceDownLoadFlag = True Then
                    Dim fileName As String = filePath.Substring(filePath.LastIndexOf("\") + 1)
                    ' Response.AppendHeader("Content-Type", "application/octet-stream")

                    Response.AppendHeader("content-disposition", _
                       "attachment; filename=" + fileName)
                End If


                ''for pdf
                'Using oFileStream As FileStream = File.Open(filePath, FileMode.Open)
                '    Dim FileSize As Long
                '    FileSize = oFileStream.Length
                '    Dim Buffer(CInt(FileSize)) As Byte
                '    oFileStream.Read(Buffer, 0, CInt(FileSize))
                '    oFileStream.Close()

                '    Response.ContentType = type '"application/pdf"
                '    Response.OutputStream.Write(Buffer, 0, FileSize)
                '    System.IO.File.Delete(filePath)
                '    Response.Flush()
                '    Response.Close()
                '    Response.End()

                'End Using
                ''end for pdf


                ' Else

                'Response.WriteFile(filePath)
                'Response.End()
                Response.Buffer = True
                Response.Charset = ""

                Dim bData As Byte()
                Dim stream As FileStream = System.IO.File.OpenRead(filePath)

                Dim br As BinaryReader = New BinaryReader(stream)
                bData = br.ReadBytes(br.BaseStream.Length)
                stream.Close()
                If deleteFile Then
                    System.IO.File.Delete(filePath)
                End If

                Using MyMemoryStream As New MemoryStream(bData, 0, bData.Length)

                    MyMemoryStream.Write(bData, 0, bData.Length)

                    MyMemoryStream.WriteTo(Response.OutputStream)

                    Response.Flush()
                    Response.End()

                End Using


            Else
                Response.Write("<script>alert('Selected Report File is currently not available2!');self.close();</script>")
                Response.End()
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

End Class
