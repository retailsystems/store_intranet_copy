Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Partial Class CalendarView
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.RD.Application.Session

    Private _employeeId As String
    Protected Caption As String
    Private ds As DataSet
    Private dv As DataView
    Protected rptCaption As String
    Private _redirUrl As String
    Private _fiscalMonth As HotTopic.RD.Application.FiscalMonth
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            lblErrMsg.Text = ""
            lblErrMsg.Visible = False
           
         
            _sessionUser = New Session(HttpContext.Current)
            Select Case _sessionUser.SessionUser.CurrentUserRoleId
                Case UserRole.DM
                    rptCaption = "District " & _sessionUser.SessionUser.DistrictId
                    _redirUrl = "CurrentReportsDM.aspx"
                Case UserRole.Store
                    rptCaption = "Store " & CStr(_sessionUser.SessionUser.StoreNum).PadLeft(4, "0")
                    _redirUrl = "CurrentReports.aspx"
                Case UserRole.RM
                    rptCaption = "Region " & _sessionUser.SessionUser.RegionId
                    _redirUrl = "CurrentReportsDM.aspx"
            End Select
           
            If Not IsPostBack Then
                With _fiscalMonth
                    If Now.Month = 1 Then
                        .MonthNum = 12
                        .Year = Now.Year - 1
                    Else
                        .MonthNum = Now.Month - 1
                        .Year = Now.Year
                    End If

                End With
                _sessionUser.GetFiscalMonthDateRange(_fiscalMonth)
                BindData()
            End If

        Catch ex As RDAppException
            If ex.Message = "Unauthenticated user." Then
                Dim jsStr As String = ""
                jsStr += "<script>"
                jsStr += "alert('Active Session Expired!');"
                jsStr += "window.top.location.reload();"
                jsStr += "</script>"
                Response.Write(jsStr)
                Response.End()
            End If
            lblErrMsg.Text = ex.Message
            lblErrMsg.Visible = True
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub BindData()
        ds = New DataSet()
        ds = _sessionUser.ListFilesCalendarView(_fiscalMonth)

    End Sub
    Private Sub rptCalendar_VisibleMonthChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MonthChangedEventArgs) Handles rptCalendar.VisibleMonthChanged
        With _fiscalMonth
            If e.NewDate.Month = 1 Then
                .MonthNum = 12
                .Year = e.NewDate.Year - 1
            Else
                .MonthNum = e.NewDate.Month - 1
                .Year = e.NewDate.Year
            End If
        End With
        Try
            _sessionUser.GetFiscalMonthDateRange(_fiscalMonth)
            BindData()
        Catch ex As RDAppException
            lblErrMsg.Text = ex.Message
            lblErrMsg.Visible = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub rptCalendar_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles rptCalendar.DayRender
        Dim reportStr As String
        Dim i As Integer
        If e.Day.Date < _fiscalMonth.BeginDate Or e.Day.Date > _fiscalMonth.EndDate Then
            e.Cell.Visible = False
        Else

            If ds.Tables.Count > 0 Then
                dv = ds.Tables(0).DefaultView
                If dv.Count > 0 Then
                    'dv.RowFilter = "FiscalWeekEndingDate='" & e.Day.Date & "'"
                    'SRI 7/31/2003 Filter by ReportDate
                    dv.RowFilter = "ReportDate='" & e.Day.Date & "'"
                    If dv.Count > 0 Then
                        'e.Cell.Controls.Add(New LiteralControl("<ul><li><a href=''>Test Title1</a></li><li><a href=''>Test Title2</a></li><li><a href=''>Test Title sasasaasasas</a></li></ul>"))
                        reportStr = "<ul>"
                        For i = 0 To dv.Count - 1
                            reportStr += "<li>"
                            reportStr += "<a href=""javascript:OpenPop('ViewReportFiles.aspx?sid=" & dv.Item(i).Item("ReportSubmissionID") & "&rid=" & dv.Item(i).Item("ReportID") & "',500,590)"">" & dv.Item(i).Item("ReportTitle") & "</a>"
                            If dv.Item(i).Item("Comment") <> "" Then
                                reportStr += "<br>&nbsp;<font color=white>(<a href=""javascript:OpenPop('ViewReportFiles.aspx?mode=comment&sid=" & dv.Item(i).Item("ReportSubmissionID") & "&rid=" & dv.Item(i).Item("ReportID") & "',500,450)"">" & Left(dv.Item(i).Item("Comment"), 10) & "...)</font></a>"
                            End If
                            reportStr += "</li>"

                        Next
                        reportStr += "</ul>"
                        e.Cell.Controls.Add(New LiteralControl(reportStr))
                    End If
                    dv.RowFilter = ""
                End If
            End If
        End If
    End Sub



    Private Sub lstGrpBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstGrpBy.SelectedIndexChanged
        If lstGrpBy.SelectedItem.Value <> 3 Then
            Server.Transfer(_redirUrl & "?GrpBy=" & lstGrpBy.SelectedItem.Value)
        End If
    End Sub
End Class
