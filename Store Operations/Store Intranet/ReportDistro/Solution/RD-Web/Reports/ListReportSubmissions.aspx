<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ListReportSubmissions.aspx.vb" Inherits="ReportDistro.ListReportSubmissions"%>
<HTML>
	<body>
		<!--#include file="../include/header.inc"-->
		<form id="Form1" method="post" runat="server">
			<br>
			<table border="0" cellpadding="0" cellspacing="0" width="520" align="center">
				<tr>
					<td align="middle" class="pageCaption"><u>Current Submissions</u></td>
				</tr>
				<tr height="10">
					<td></td>
				</tr>
				<tr>
					<td>
						<asp:DataGrid ID="dgReport" Width="650" Runat="server" BorderColor ="#990000" 
							CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" 
							CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" 
							AlternatingItemStyle-CssClass="GridAlternateStyle" 
							AutoGenerateColumns="false" AllowPaging=True AllowSorting=True PagerStyle-Mode=NumericPages PagerStyle-CssClass="pagerStyle"  >
							<Columns>
							<asp:BoundColumn DataField="ReportSubmissionID" SortExpression="ReportSubmissionID" HeaderText="ReportSubmissionID" Visible=False></asp:BoundColumn>
							<asp:BoundColumn DataField="ReportTitle" SortExpression="ReportTitle" HeaderText="Title"></asp:BoundColumn>
							<asp:BoundColumn DataField="FiscalWeekEndingDate" HeaderStyle-Width="70px"   DataFormatString="{0:MM/dd/yyyy}" SortExpression="FiscalWeekEndingDate" HeaderText="Fiscal Week"></asp:BoundColumn>
							<asp:BoundColumn DataField="ReportDate" HeaderStyle-Width="70px" DataFormatString="{0:MM/dd/yyyy}" SortExpression="ReportDate"	HeaderText="Report Date"></asp:BoundColumn>						
							<asp:BoundColumn DataField="Comment" HeaderStyle-Width="250px" SortExpression="Comment" HeaderText="Comment"></asp:BoundColumn>
							<asp:BoundColumn DataField="LookUpText" HeaderStyle-Width="80px" SortExpression="LookUpText" HeaderText="LookUpText"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
						<asp:Label ID="SortBy" Visible="False" Runat="server"></asp:Label>
						<asp:Label ID="SortDirection" Visible="False" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
			</table>
			<!-- end here-->
			<!--footer-->
			<!--#include file="../include/footer.inc"--></form>
	</body>
</HTML>
