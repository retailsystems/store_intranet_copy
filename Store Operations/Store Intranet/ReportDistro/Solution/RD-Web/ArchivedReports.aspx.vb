Imports HotTopic.RD.Application
Partial Class ArchivedReports
    Inherits System.Web.UI.Page

    Private _sessionUser As HotTopic.RD.Application.Session
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            _sessionUser = New Session(HttpContext.Current)
            If Not IsPostBack Then
                InitializeForm()
            End If
            hdnSessionTimeOut.Value = Session.Timeout
        Catch ex As RDAppException
            If ex.Message = "Unauthenticated user." Then
                Dim jsStr As String = ""
                jsStr += "<script>"
                jsStr += "alert('Active Session Expired!');"
                jsStr += "window.top.location.reload();"
                jsStr += "</script>"
                Response.Write(jsStr)
                Response.End()
            End If

            lblErrMsg.Text = ex.Message
            lblErrMsg.Visible = True
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub
    Private Sub InitializeForm()
        SortBy.Text = "ReportDate"
        SortDirection.Text = "desc"
        lstTitle.Items.Clear()
        Dim ds As DataSet = _sessionUser.GetAllReportTitles()
        lstTitle.DataValueField = "ReportID"
        lstTitle.DataTextField = "ReportTitle"
        lstTitle.DataSource = ds
        lstTitle.DataBind()
        lstTitle.Items.Insert(0, New ListItem("---", ""))
        lstTitle.DataSource = _sessionUser.GetAllReportTitles()
        txtFiscalDate.Text = ""
        dgReport.Controls.Clear()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        dgReport.CurrentPageIndex = 0
        SortBy.Text = "ReportDate"
        SortDirection.Text = "desc"
        BindData()

    End Sub
    Private Sub BindData()
        Dim ds As DataSet
        ds = _sessionUser.SearchFiles(lstTitle.SelectedItem.Value, txtFiscalDate.Text, 1)
        dgReport.DataSource = ds
        dgReport.DataBind()
    End Sub

    Private Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        InitializeForm()
    End Sub

    Private Sub dgReport_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgReport.PageIndexChanged
        dgReport.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub

    Private Sub dgReport_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgReport.SortCommand
        If SortBy.Text = e.SortExpression Then
            Select Case SortDirection.Text
                Case "asc"
                    SortDirection.Text = "desc"
                Case "desc"
                    SortDirection.Text = "asc"
            End Select
        Else
            SortDirection.Text = "asc"
        End If
        SortBy.Text = e.SortExpression

        BindData()
    End Sub
End Class
