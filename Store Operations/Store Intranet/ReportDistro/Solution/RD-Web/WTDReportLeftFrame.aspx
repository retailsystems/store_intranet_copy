<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WTDReportLeftFrame.aspx.vb" Inherits="ReportDistro.WTDReportLeftFrame"%>
<HTML>
	<HEAD>
		<title>CurrentReports</title>
		<LINK href="<%=Application("StyleSheet")%>" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<%
			Response.Write("<SCRIPT language='Javascript'>" )
			Response.Write("var plusImg = '../images/Plus" & _treeImgSfx & ".jpg';")
			Response.Write("var minusImg = '../images/Minus" & _treeImgSfx & ".jpg';")
			Response.Write("</SCRIPT>")
		%>		
		<SCRIPT language="Javascript">
function showtree(lngSection){
	//alert(lngSection);
	if (eval('S' + lngSection).style.display == 'none'){
		eval('S' + lngSection).style.display = 'block';
		eval('document.Form1.I' + lngSection).src = minusImg ;
	}else{
		eval('S' + lngSection).style.display = 'none';
		eval('document.Form1.I' + lngSection).src = plusImg;
	}
}
function ShowReport(){
	var id = document.Form1.grpBy[document.Form1.grpBy.selectedIndex].value;
	if(id == "1"){
		document.all.grp1.style.display = 'inline';
		document.all.grp2.style.display = 'none';	
	}
	if(id == "2"){
		document.all.grp1.style.display = 'none';
		document.all.grp2.style.display = 'inline';	
	}
}
function ShowGrid(flg){
	if (flg == 1){
	document.all.gridDiv.style.display = 'inline';	
	}else{
	document.all.gridDiv.style.display = 'none';	
	}
}
		</SCRIPT>
		<script language='javascript'>
			 function OpenPop(url,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,'cw',winProp);
		   }
		   	 function OpenPop1(url,cw,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,cw,winProp);
		   }
		   	 function OpenPop2(url,cw,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=yes,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,cw,winProp,true);				 
		   }
		   function PrintReport(){
			var obj = window.parent.frames['main'];
			var url1 = obj.location.href ;
			var ind1;
					
			if (url1.indexOf("ApReports") > 0){
				ind1 = url1.indexOf("ApReports") + 8;
			}else{
				ind1 = parseInt(url1.lastIndexOf("/"));		
			}	
			url1 = "ViewReportFile.aspx?ForceDownload=TRUE&FilePath=" + Form1.hdnAbsFileDir.value + url1.substr(ind1+1);
						
			//OpenPop(url1,10,10);
			//obj.location.href = "wait.aspx";
			doTargetURL(url1,'_blank');		
			/*
			if (obj){
				obj.focus();
				obj.print();
			}	
			*/			
		   }
		   function doTargetURL(url, dest) {
				if (document.all!=null) {
				
				var elTarget = document.all.targetURL
				// Setup the anchor
				elTarget.href = url
				elTarget.target = dest
				// Click the anchor
				elTarget.click();
				window.parent.frames['main'].location.href = "Blank.aspx";
				}  
				else // Default to new window 
				window.open(url)  
			}
			
		</script>
	</HEAD>
	<body bgcolor="black" text="white" vlink="white" alink="white" link="white" face="arial">
		<form id="Form1" method="post" runat="server">
			<asp:Label Runat="server" ID="lblErrMsg" Visible="False" CssClass="errStyle"></asp:Label>
			<input type=hidden name="hdnAbsFileDir" id="hdnAbsFileDir" runat=server value="">
			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" ID="Table6">
				<tr>
					<td width=10px>&nbsp;</td>
				</tr>
				<tr>
					<td width=10px>&nbsp;</td>
					<td class=errStyle>
					<!--11/09/2004 AP Report renamed as Pulse Report -->
					<%IF Request("RptType") = "3" THEN%>
						Pulse Report
					<%ELSE%>
						<%=rptCaption%>
					<%END IF%>
					</td>
				</tr>
				<tr>
					<td width=10px>&nbsp;</td>
					<td>
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" ID="Table6">				
							<%=_treeStr%>
						</TABLE>
					</td>
				</tr>
				<%IF Request("RptType") = "2" OR Request("RptType") = "3" THEN%>
				<tr>
					<td width=10px height=5px></td>
				</tr>
				<tr>
					<td width=10px>&nbsp;</td>
					<td>
						<a href="javascript:PrintReport()" >Print View</a>
						<A STYLE="display: none" ID=targetURL></A>
					</td>
				</tr>
				<%END IF%>
			</TABLE>	
			 
		</form>
	</body>
<%
	If Not IsNothing(Request("RptType")) AndAlso Request("RptType") = "3" THEN
		response.write("<script>window.top.frames[1].location='" & StartUpUrl & "';</script>")
	ELSE
		response.write("<script>window.top.frames[1].location='" & StartUpUrl & "';showtree('" & StartUpnode & "');</script>")
	END IF
	
%>

</script>
</HTML>
