<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ReportMain.aspx.vb" Inherits="ReportDistro.ReportMain" %>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='<%=Application("StyleSheet")%>' type="text/css" rel="stylesheet">
    <link id="Link3" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link id="Link4" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
    <!--[if lt IE 9]>
	  <script src="/Global/js/html5shiv.min.js"></script>
	  <script src="/Global/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
    <% Response.CacheControl = "no-cache"%>
    <% Response.AddHeader("pragma", "no-cache")%>
    <% Response.Expires = -1%>
    <%
        Response.Write("<SCRIPT language='Javascript'>")
        Response.Write(" var mode = '" & Application("Mode") & "';")
        Response.Write("</SCRIPT>")
    %>

    <script language="Javascript">

        $(document).ready(function () { alert("ReportMain");});

function showtree(lngSection){
	if (eval('S' + lngSection).style.display == 'none'){
		eval('S' + lngSection).style.display = 'block';
		eval('document.Form1.I' + lngSection).src = 'Images/Minus.jpg';
	}else{
		eval('S' + lngSection).style.display = 'none';
		eval('document.Form1.I' + lngSection).src = 'Images/Plus.jpg';
	}
}
function ShowReport(){
	var id = document.Form1.grpBy[document.Form1.grpBy.selectedIndex].value;
	if(id == "1"){
		document.all.grp1.style.display = 'inline';
		document.all.grp2.style.display = 'none';	
	}
	if(id == "2"){
		document.all.grp1.style.display = 'none';
		document.all.grp2.style.display = 'inline';	
	}
}
function ShowGrid(flg){
	if (flg == 1){
	document.all.gridDiv.style.display = 'inline';	
	}else{
	document.all.gridDiv.style.display = 'none';	
	}
}
    </script>

    <script language='javascript'>
			 function OpenPop(url,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,'cw',winProp);
		   }
		   	 function OpenPop1(url,cw,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,cw,winProp);
		   }
    </script>

    <script language="javascript" type="text/javascript" src="Include/domtab.js"></script>

    <form id="Form1" method="post" runat="server">
        <asp:Label runat="server" ID="lblErrMsg" Visible="False" CssClass="alert-danger myalert"></asp:Label>
        <%If Not blnLoginFail Then%>
        <!--
	These are the tabs. Make sure that each of them has the correct id,
		target and corresponding number in the domTab() call.
	 -->
	 <h4>Reports</h4>
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="#target1" onclick="domTab(1,mode)" id="link1">Current Reports</a></li>
            <li><a href="#target2" onclick="domTab(2,mode)" id="link2">Archived Reports</a></li>
        </ul>
        <br />
        <div id="contentblock1">
            <a name="target1"></a>
            
            <!-- current report  tree view control-->
            <iframe width="100%" height="610" name="IF1" id="IF1" src='wait.aspx?role=<%=request("role")%>&amp;FormAction=<%=FormAction%>'
                frameborder="0" scrolling="auto"></iframe>
        </div>
        <div id="contentblock2" style="display: none">
            <a name="target2"></a>
            <iframe height="610" width="100%" name="IF2" id="IF2" src="ArchivedReports.aspx"
                frameborder="0" scrolling="auto"></iframe>
        </div>
        <%End If%>
    </form>
    <%If Not blnLoginFail Then%>

    <script>
	function intialize(){
	domTab(1,mode);
	
	//set tab div heights based on resolution
	document.all.contentblock1.style.height = window.screen.height - 335;
	document.all.contentblock2.style.height = window.screen.height - 335;
	
	}
	window.onload=intialize;
    </script>

    <%End If%>
    <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
</body>
</html>
