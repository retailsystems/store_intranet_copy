<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ReportApprovers.ascx.vb" Inherits="ReportDistro.ReportApprovers" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table align="center" width="300" border="0" cellpadding="2" cellspacing="0" style="BORDER-RIGHT: crimson 1px solid; BORDER-TOP: crimson 1px solid; BORDER-LEFT: crimson 1px solid; BORDER-BOTTOM: crimson 1px solid">
	<tr height="15">
		<td bgcolor="crimson" class="cellvaluecaption">&nbsp;Approvers</td>
		<td bgcolor="crimson" align="right" valign="center">
			<input type="button" name="btnEdit" value="Update" id="btnEdit" class="btnSmall" onclick="location.href='ManageApprovers.aspx?reportID=<%=reportId%>'">
		</td>
	</tr>
	<tr height="5">
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2">
			<table cellspacing="0" cellpadding="0" bordercolor="#990000" border="0">
				<tr>
					<td width="5"></td>
					<td>
						<asp:DataGrid ID="dgApprovers" Runat="server"  CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false">
							<Columns>
							<asp:BoundColumn DataField="Firstname" HeaderText="First Name"></asp:BoundColumn>
							<asp:BoundColumn DataField="Lastname" HeaderText="Last Name"></asp:BoundColumn>
							<asp:BoundColumn DataField="EmployeeID" HeaderText="Employee Id"></asp:BoundColumn>
							<asp:BoundColumn DataField ="RouteOrder" HeaderText="Route Order"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr height="5">
		<td colspan="2"></td>
	</tr>
</table>
