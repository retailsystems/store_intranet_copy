Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Public MustInherit Class ReportJobCodeOwners
    Inherits System.Web.UI.UserControl
    Protected WithEvents dgCodeOwners As System.Web.UI.WebControls.DataGrid
    Public report As Report
    Private _codeOwnersArr As ArrayList
    Protected reportId As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        With report
            _codeOwnersArr = .JobCodeOwners
            reportId = .ReportId
        End With
        BindData()
    End Sub
    Private Sub BindData()
        dgCodeOwners.DataSource = _codeOwnersArr
        dgCodeOwners.DataBind()
    End Sub
End Class