<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ViewWTDReport.aspx.vb" Inherits="ReportDistro.ViewWTDReport"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WTD By Department</title>
		<script language="vb" runat="server">

Public  Function NullToString(ByVal Value As Object) As String
            If IsDBNull(Value) Then
                Return ""
            Else
                Return CStr(Value)
            End If
End Function
Public  Function NullToDouble(ByVal Value As Object) As Double
            If IsDBNull(Value) Then
                Return 0
            Else
                Return CDbl(Value)
            End If
End Function
Public Function GetPct(byval inStr as string,byval noDecDigits as integer) as string
	try
		dim pct as string
		
		IF isNumeric(inStr) then
			pct = FormatNumber(Double.parse(inStr)*100,noDecDigits) & "%"
		else
			pct = inStr
		END IF	
		
		Return pct
	catch ex as exception
		
	end try
End Function

		</script>
		<LINK href="<%=Application("StyleSheet")%>" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body text="white" vLink="white" aLink="white" link="white" bgColor="black" onload="self.focus()" face="arial">
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td align="middle">
						<table width="100%">
							<tr>
								<td align="right" colspan="3">
									<input type="button" name="btnPrint" onclick="window.print()" value="Print" class="btnSmall">
									&nbsp;<input type="button" name="btnClose" onclick="window.top.close()" value="Close" class="btnSmall">
								</td>
							</tr>
							<tr>
								<td align="middle" colSpan="3"><asp:label id="lblError" CssClass="ErrStyle" Runat="server"></asp:label></td>
							</tr>
							<tr>
								<td width="20%"></td>
								<td align="middle">
									<table>
										<tr>
											<td colSpan="2"><asp:label id="lblDistrict" CssClass="cellvaluecaption" Runat="server"></asp:label></td>
										</tr>
										<tr>
											<td class="cellvaluewhite">
												<asp:Label ID="lblRptDateCaption" Runat="server" text="Results as of:"></asp:Label>
											</td>
											<td><asp:label id="lblRptDate" CssClass="errStyle" Runat="server"></asp:label></td>
										</tr>
									</table>
								</td>
								<td align="right" width="20%"><asp:button id="btnSwap" CssClass="B1" Runat="server" Text="Last Week"></asp:button></td>
							</tr>
						</table>
						<asp:datagrid id="dgSummary" Runat="server" AutoGenerateColumns="False" BorderWidth="0" ShowHeader="False" ItemStyle-CssClass="cellvaluecaption">
							<Columns>
								<asp:BoundColumn DataField="F1" ItemStyle-Width="120"></asp:BoundColumn>
								<asp:BoundColumn DataField="F2" DataFormatString="{0:c}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="80"></asp:BoundColumn>
								<asp:TemplateColumn ItemStyle-HorizontalAlign="Right" ItemStyle-Width="60">
									<ItemTemplate>
										<%#GetPct(NullToString(Container.DataItem("F3")),0)%>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid><br>
						<asp:datagrid id="dgData"  Runat="server" AutoGenerateColumns="false" GridLines=Vertical CellPadding="1" HeaderStyle-CssClass="DATAGRID_HeaderSm" CssClass="DatagridSm" ItemStyle-CssClass="BC111111sm" AlternatingItemStyle-CssClass="BC333333sm"  AllowPaging="False" AllowSorting="True">
							<Columns>
								<asp:BoundColumn DataField="Dept" HeaderText="Dept" SortExpression="Dept" ItemStyle-Width="60"></asp:BoundColumn>
								<asp:BoundColumn DataField="Description" HeaderText="Description" ItemStyle-Width="150" SortExpression="Description"></asp:BoundColumn>
								<asp:BoundColumn DataField="Dollars Sold" HeaderText="Dollars Sold" DataFormatString="{0:c}" SortExpression="Dollars Sold" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="% Of Sales" ItemStyle-HorizontalAlign="Right" SortExpression="% Of Sales">
									<ItemTemplate>
										<%#GetPct(NullToString(Container.DataItem("% of Sales")),1)%>
										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="Units Sold" HeaderText="Units Sold" SortExpression="Units Sold" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
							</Columns>
						</asp:datagrid><asp:label id="SortBy" Runat="server" Visible="False"></asp:label><asp:label id="SortDirection" Runat="server" Visible="False"></asp:label><asp:label id="lblWeekPrefix" Runat="server" Visible="False"></asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
