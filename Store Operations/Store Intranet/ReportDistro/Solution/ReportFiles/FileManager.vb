' Static Model
Imports HotTopic.RD.Entity
Imports HotTopic.RD.ReportFiles
Imports HotTopic.RD.Services
Imports System.Data.SqlClient

Namespace HotTopic.RD.ReportFiles



    Public Class FileManager

        Public Sub View()

        End Sub

        Public Shared Function Search(ByVal EmployeeId As String, ByVal jobCode As String, ByVal ReportID As Integer, Optional ByVal FiscalWeekDate As String = "", Optional ByVal ArchivedFlag As Int16 = -1, Optional ByVal RptSubmissionID As Integer = 0) As DataSet
            ' Set report parameters (3 input) 

            Dim SearchDate As Date
            If IsDate(FiscalWeekDate) Then
                SearchDate = CDate(FiscalWeekDate)
            End If
            Dim arParms() As SqlParameter = New SqlParameter(5) {}

            arParms(0) = New SqlParameter("@EmployeeId", SqlDbType.VarChar, 30)
            arParms(0).Value = EmployeeId
            arParms(1) = New SqlParameter("@jobCode", SqlDbType.VarChar, 30)
            arParms(1).Value = jobCode
            arParms(2) = New SqlParameter("@ReportID", SqlDbType.Int)
            arParms(2).Value = ReportID
            arParms(3) = New SqlParameter("@SearchDate", SqlDbType.DateTime)
            arParms(3).Value = UtilityManager.DateToNull(SearchDate)
            arParms(4) = New SqlParameter("@ArchivedFlag", SqlDbType.Bit)
            If (ArchivedFlag = 0 Or ArchivedFlag = 1) Then
                arParms(4).Value = ArchivedFlag
            End If
            arParms(5) = New SqlParameter("@SubmissionID", SqlDbType.Int)
            If RptSubmissionID > 0 Then
                arParms(5).Value = RptSubmissionID
            End If
            Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_SearchReportFiles", arParms)
            Return ds

        End Function

        ' Lists reports for given store user
        'Public Function List() As FileDataSet

        'End Function
        'ToDo use FileDataSet
        Public Shared Function List(ByVal storeNum As Integer, ByVal groupBy As Integer, ByVal employeeId As String, ByVal JobCode As String) As DataSet

            ' Set report parameters (3 input) 
            Dim arParms() As SqlParameter = New SqlParameter(3) {}

            arParms(0) = New SqlParameter("@StoreNum", SqlDbType.Int)
            arParms(0).Value = storeNum
            arParms(1) = New SqlParameter("@grpBy", SqlDbType.Int)
            arParms(1).Value = groupBy
            arParms(2) = New SqlParameter("@employeeId", SqlDbType.VarChar, 30)
            arParms(2).Value = employeeId
            arParms(3) = New SqlParameter("@JobCode", SqlDbType.VarChar, 30)
            arParms(3).Value = JobCode
            Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_GetCurrentReports", arParms)
            Return ds
        End Function

        Public Shared Function ListCalendarView(ByVal employeeId As String, ByVal JobCode As String, ByVal BeginDate As Date, ByVal EndDate As Date) As DataSet
            'SRI 7/24/2003 this function is for testing
            ' Set report parameters (3 input) 
            Dim arParms() As SqlParameter = New SqlParameter(3) {}


            arParms(0) = New SqlParameter("@employeeId", SqlDbType.VarChar, 30)
            arParms(0).Value = employeeId
            arParms(1) = New SqlParameter("@JobCode", SqlDbType.VarChar, 30)
            arParms(1).Value = JobCode
            arParms(2) = New SqlParameter("@BeginDate", SqlDbType.DateTime)
            arParms(2).Value = BeginDate
            arParms(3) = New SqlParameter("@EndDate", SqlDbType.DateTime)
            arParms(3).Value = EndDate
            Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_LoadCalendarView", arParms)
            Return ds
        End Function
        Public Shared Function List(ByVal rptSubmission As Submission) As DataSet
            Dim lstFiles As ArrayList
            Dim searchPattern As String = rptSubmission.DefaultFileName + "*"
            Dim locPath As String = rptSubmission.LocationPath
            If Not locPath.EndsWith("\") Then
                locPath += "\"
            End If
            locPath += rptSubmission.FiscalYear.ToString() _
                + "\" + rptSubmission.FiscalWeek.ToString() _
                + "\" + rptSubmission.ReportId.ToString()
            Dim staginPath As String = rptSubmission.StagingPath
            If rptSubmission.InputType = FileInputType.Upload Then
                'staginPath += "\" + rptSubmission.FiscalWeekEndingDate.ToString("MMddyyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo) + "\"
                searchPattern = rptSubmission.ReportSubmissionId & "_*.*"
            End If
            If Not staginPath.EndsWith("\") Then
                staginPath += "\"
            End If
            '******** new dataset **************
            Dim ds As DataSet = New DataSet()
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow
            dt.TableName = "Table1"
            dt.Columns.Add("FileName")
            dt.Columns.Add("FilePath")

            Dim i As Integer
            '*******************************
            Select Case rptSubmission.Status
                Case ReportStatus.Pending
                    'get file list from staging folder.
                    If rptSubmission.InputType = FileInputType.System And rptSubmission.CompanySubOwners.Count > 0 Then
                        Dim cSubOwner As SubCompanyOwner
                        Dim ownerID As String
                        For Each cSubOwner In rptSubmission.CompanySubOwners
                            ownerID = cSubOwner.OwnerID
                            If rptSubmission.ContentScope = ContentScopeType.Store Then
                                ownerID = ownerID.Trim.PadLeft(4, "0")
                            End If
                            searchPattern = rptSubmission.DefaultFileName & "*" & ownerID & "*"
                            lstFiles = FileSystemManager.ListFileNames(staginPath, searchPattern)
                            For i = 0 To lstFiles.Count - 1
                                dr = dt.NewRow
                                dr.Item(0) = lstFiles(i)
                                dr.Item(1) = staginPath & lstFiles(i)
                                dt.Rows.Add(dr)
                            Next
                        Next
                    Else
                        lstFiles = FileSystemManager.ListFileNames(staginPath, searchPattern)
                        For i = 0 To lstFiles.Count - 1
                            dr = dt.NewRow
                            dr.Item(0) = lstFiles(i)
                            dr.Item(1) = staginPath & lstFiles(i)
                            dt.Rows.Add(dr)
                        Next
                    End If

                Case Else 'ReportStatus.Approved
                    'SRI 8/29/03 show files for all status not only approved
                    'get file list from location path
                    'SRI 7/30/2003 Get Files From DB
                    Dim arParms() As SqlParameter = New SqlParameter(0) {}

                    arParms(0) = New SqlParameter("@ReportSubmissionID", SqlDbType.Int)
                    arParms(0).Value = rptSubmission.ReportSubmissionId
                    Dim ds1 As DataSet
                    Dim dr1 As DataRow
                    ds1 = SQLDataManager.GetInstance().GetDataSet("RSP_GetFilesBySubmissionID", arParms)
                    For Each dr1 In ds1.Tables(0).Rows
                        dr = dt.NewRow
                        dr.Item(0) = dr1("FileName")
                        dr.Item(1) = dr1("FilePath")
                        dt.Rows.Add(dr)
                    Next
                    'lstFiles = FileSystemManager.ListFileNames(locPath, searchPattern)
                    'For i = 0 To lstFiles.Count - 1
                    '    dr = dt.NewRow
                    '    dr.Item(0) = lstFiles(i)
                    '    dr.Item(1) = locPath & "\" & lstFiles(i)
                    '    dt.Rows.Add(dr)
                    'Next
            End Select
            ds.Tables.Add(dt)
            Return ds
        End Function

        Public Shared Function GetFilePathByID(ByVal ReportFileID As Integer, ByVal EmployeeId As String) As String
            Try
                'RSP_GetFilePathById
                ' Set report parameters (1 input, 1 output) 
                Dim arParms() As SqlParameter = New SqlParameter(2) {}

                arParms(0) = New SqlParameter("@reportFileID", SqlDbType.Int)
                arParms(0).Value = ReportFileID
                arParms(1) = New SqlParameter("@EmployeeId", SqlDbType.VarChar, 30)
                arParms(1).Value = EmployeeId
                arParms(2) = New SqlParameter("@filePath", SqlDbType.VarChar, 200)
                arParms(2).Direction = ParameterDirection.Output
                SQLDataManager.GetInstance().Execute("RSP_GetFilePathById", arParms)
                Return arParms(2).Value
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Shared Function IsReadOnlyFile(ByVal ReportFileID As Integer) As Boolean
            Try
                'RSP_GetFilePathById
                ' Set report parameters (1 input, 1 output) 
                Dim arParms() As SqlParameter = New SqlParameter(1) {}

                arParms(0) = New SqlParameter("@reportFileID", SqlDbType.Int)
                arParms(0).Value = ReportFileID
                arParms(1) = New SqlParameter("@ReadOnly", SqlDbType.Bit)
                arParms(1).Direction = ParameterDirection.Output
                SQLDataManager.GetInstance().Execute("RSP_IsReadOnlyFile", arParms)
                Return arParms(1).Value
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ' Moves file from staging to final location
        Public Sub Approve()

        End Sub

        ' For uploaded files, individual records need to be saved in the ReportFile table
        Public Shared Sub Save(ByVal rptFile As File, ByVal userId As String)
            'save ReportFile record
            ' Set report parameters (7 input) 
            Dim arParms() As SqlParameter = New SqlParameter(6) {}

            With rptFile
                arParms(0) = New SqlParameter("@ReportFileId", SqlDbType.Int)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = .ReportFileId
                arParms(1) = New SqlParameter("@ReportSubmissionId", SqlDbType.Int)
                arParms(1).Value = .ReportSubmissionId
                arParms(2) = New SqlParameter("@ArchivedFlag", SqlDbType.Bit)
                arParms(2).Value = .ArchivedFlag
                arParms(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 50)
                arParms(3).Value = .FileName
                arParms(4) = New SqlParameter("@OwnerId", SqlDbType.VarChar, 5)
                arParms(4).Value = .OwnerId
                arParms(5) = New SqlParameter("@ContentScopeType", SqlDbType.SmallInt)
                arParms(5).Value = .ContentScope
                arParms(6) = New SqlParameter("@UserId", SqlDbType.VarChar, 20)
                arParms(6).Value = userId

            End With
            SQLDataManager.GetInstance().Execute("RSP_UpdateReportFile", arParms)
            rptFile.ReportFileId = Convert.ToInt32(arParms(0).Value)
        End Sub


    End Class ' END CLASS DEFINITION FileManager

End Namespace ' HotTopic.RD.ReportFiles

