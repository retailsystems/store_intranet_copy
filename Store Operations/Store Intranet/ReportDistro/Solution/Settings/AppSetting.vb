Namespace HotTopic.RD.Settings

    Public Class AppSetting

        Private Shared _connString As String
        Private Shared _secConnString As String
        Private Shared _cacheDependency As String
        Private Shared _smtpServer As String
        Private Shared _smtpSender As String
        Private Shared _approverEmailBody As String
        Private Shared _declinedEmailBody As String
        Private Shared _cancelledEmailBody As String
        Private Shared _oracleConnectionString As String
        Private Shared _strStoreSubnetMask As String
        Public Shared Property ConnectionString() As String
            Get
                Return _connString
            End Get
            Set(ByVal Value As String)
                _connString = Value
            End Set
        End Property

        Public Shared Property SecurityConnectionString() As String
            Get
                Return _secConnString
            End Get
            Set(ByVal Value As String)
                _secConnString = Value
            End Set
        End Property
        Public Shared Property CacheDependencyFile() As String
            Get
                Return _cacheDependency
            End Get
            Set(ByVal Value As String)
                _cacheDependency = Value
            End Set
        End Property

        Public Shared Property SmtpServer() As String
            Get
                Return _smtpServer
            End Get
            Set(ByVal Value As String)
                _smtpServer = Value
            End Set
        End Property

        Public Shared Property SmtpSender() As String
            Get
                Return _smtpSender
            End Get
            Set(ByVal Value As String)
                _smtpSender = Value
            End Set
        End Property
        Public Shared Property ApproverEmailBody() As String
            Get
                Return _approverEmailBody
            End Get
            Set(ByVal Value As String)
                _approverEmailBody = Value
            End Set
        End Property
        Public Shared Property DeclinedEmailBody() As String
            Get
                Return _declinedEmailBody
            End Get
            Set(ByVal Value As String)
                _declinedEmailBody = Value
            End Set
        End Property
        Public Shared Property CancelledEmailBody() As String
            Get
                Return _cancelledEmailBody
            End Get
            Set(ByVal Value As String)
                _cancelledEmailBody = Value
            End Set
        End Property
        Public Shared Property OracleConnectionString() As String
            Get
                Return _oracleConnectionString
            End Get
            Set(ByVal Value As String)
                _oracleConnectionString = Value
            End Set
        End Property
        Public Shared Property StoreSubnetMask() As String
            Get
                Return _strStoreSubnetMask
            End Get
            Set(ByVal Value As String)
                _strStoreSubnetMask = Value
            End Set
        End Property
    End Class

End Namespace

