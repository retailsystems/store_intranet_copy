<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Login.aspx.vb" Inherits="ReportDistroAdmin.Login"%>
<HTML>
	<HEAD>
		<title>Report Distribution System</title>
		<script language='javascript' type='text/javascript' src='include/menu.js'></script>
		<script language='javascript'>
			 function OpenPop(url,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,'cw',winProp);
		   }
		   	 function OpenPop1(url,cw,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,cw,winProp);
		   }
		</script>
		<LINK href="include/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bgcolor="black" text="white" vlink="white" alink="white" link="white" face="arial">
		<table border='0' cellspacing='0' cellpadding='0' width='750' ID="Table1">
			<tr>
				<td>
					<%if InStr(Request.Url.ToString, "Torrid", CompareMethod.Text) > 0 then%>
					<A HREF='http://www.torrid.com'><img src='Images/TorridBlk.jpg' border='0' alt='Torrid'></A>
					<%else%>
					<A HREF='http://www.hottopic.com'><img src='Images/hdr_main2.gif' border='0' alt='hottopic'></A>
					<%end if%>
				</td>
				<td></td>
				<td width='1200' align='right' valign='top'>
			<tr>
			</tr>
			<tr>
			</tr>
			<tr>
			</tr>
			<tr>
			</tr>
			<tr>
				<td colspan='2' align='left'><font face='Arial'><font size='+1' color='white'></font></font></td>
				<td colspan='2' align='right'><font face='Arial'><font size='+1' color='red'>Report Distro 
							v1.0</font></font></td>
			</tr>
			<tr height="3">
				<td colspan='2' height="3"><hr color='#990000'>
				</td>
				<td colspan='2' height="3"><hr color='#990000'>
				</td>
			</tr>
			<tr>
				<td colspan="4">
				</td>
			</tr>
		</table>
		<!--end tree-->
		<table border='0' cellspacing='0' cellpadding='0' width='750' ID="Table3">
			<tr height="300">
				<td colspan="4" valign="top" align="middle">
					<!--end header-->
					<form id="Form1" method="post" runat="server">
						<br>
						<table cellSpacing="0" cellPadding="0" width="400" border="0" align="center">
							<tr>
								<td class="pageCaption" align="middle" colSpan="3">Login</td>
							</tr>
							<tr height="15">
								<td colSpan="3"></td>
							</tr>
							<tr>
								<td colSpan="3" align="middle">
									<asp:Label id="lblExMsg" runat="server" CssClass="ErrStyle"></asp:Label></td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">Employee ID:</td>
								<td class="reqStyle" width="10">*</td>
								<td class="cellvalueleft">
									<asp:TextBox ID="txtEmployeeId" CssClass="cellvalueleft" Runat="server" ></asp:TextBox>
									<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" CssClass="errStyle" ErrorMessage="Employee ID is required." Display="Dynamic" ControlToValidate="txtEmployeeId"></asp:RequiredFieldValidator>
									<asp:CompareValidator id="CompareValidator1" runat="server" CssClass="errStyle" ControlToValidate="txtEmployeeId" Display="Dynamic" ErrorMessage="Invalid EmployeeID." Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
								</td>
							</tr>
							<tr height="15">
								<td colSpan="3"></td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">Password:</td>
								<td class="reqStyle" width="10">*</td>
								<td class="cellvalueleft">
									<asp:TextBox ID="txtPwd" Runat="server" CssClass="cellvalueleft" TextMode="Password"></asp:TextBox>
									<asp:RequiredFieldValidator id="Requiredfieldvalidator1" runat="server" CssClass="errStyle" ErrorMessage="Password is required." Display="Dynamic" ControlToValidate="txtPwd"></asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
					<!--footer-->
				</td>
			</tr>
			<tr>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td align="right"><asp:button id="btnSubmit" CssClass="B1" Runat="server" Text="Submit"></asp:button>&nbsp;&nbsp;<input class="B1" id="Button1" onclick="document.location='../';" type="button" value="Home" name="Home"></td>
			</tr>
		</table>
		</FORM>
	</body>
</HTML>
