<%@ Control Language="vb" AutoEventWireup="false" Codebehind="MDMasterReport.ascx.vb" Inherits="ReportDistroAdmin.MDMasterReport" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server" language="vb">
Public Function NullToString(ByVal Value As Object) As String
    If Convert.IsDBNull(Value) Then
        Return ""
    Else
        Return Convert.ToString(Value)
    End If
End Function
Public Function GetShortDate(ByVal Value As Object) As String
    If Convert.IsDBNull(Value) Then
        Return ""
    Else
		IF isDate(Value)then
			Return Convert.ToDateTime(Value).ToShortDateString()
		Else
			Return ""
		End If        
    End If
End Function
</script>
<style> BODY { margin: 5px; }
	TH { font-weight: bold; font-size: 11px; font-family: 'Arial Narrow' ,Arial, Verdana; }
	TD { font-size: 10px; font-family: 'Arial Narrow' ,Arial, Verdana; }
	.PageTitle { font-weight: bold; font-size: 15pt; font-family: Arial, Verdana; }
	.Caption { font-weight: bold; font-size: 12pt; font-family: Arial, Verdana; }
	.StrongText { font-weight: bold; font-size: 13px; font-family: 'Arial Narrow' , Arial, Verdana; }
</style>
<table width="660" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="middle">
			<table width="100%">
				<tr>
					<td>
					</td>
					<td align="middle" class="PageTitle"><b><%=rptHeader.CompanyName%></b></td>
					<td class="StrongText">
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="middle" class="PageTitle">Master List for
						<%=rptHeader.RptCaption%>
						Report</td>
					<td class="StrongText"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="middle" height="5px">
		</td>
	</tr>
	<tr>
		<td align="middle" width="100%">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<th width="30" align="left">
						DEPT</th>
					<th width="50" align="left">
						CLASS</th>
					<th width="50" align="left">
						VENDOR NUMBER</th>
					<th width="50" align="left">
						ITEM NUMBER</th>
					<th width="150" align="left">
						ITEM DESCRIPTION</th>
					<th width="50" align="left">
						SLIP NUMBER</th>
					<th width="60" align="left">
						EFFECTIVE DATE</th>
					<th width="40" align="left">
						SIZE</th>
					<th width="30" align="middle">
						P.C CODE</th>
					<th width="40" align="middle">
						CURR. RETAIL</th>
					<th width="40" align="middle">
						NEW RETAIL</th>
					<th width="70" align="right">
						NEW SKU</th>
				</tr>
				<tr>
					<td colspan="12"><hr style="COLOR: black;HEIGHT: 2px">
					</td>
				</tr>
				<%
				IF DV.Count > 0 THEN
					Dim i as integer = 0
					Dim deptCd as integer = 0
					Dim classCd as integer = 0
					For  i = 0  to DV.Count - 1
						dr = DV.Item(i).Row
						IF deptCd <> dr("DEPT_CD") THEN
							deptCd = dr("DEPT_CD")
							classCd = 0
						%>
				<tr>
					<td colspan="12"><b><%=deptCd.toString.PadLeft(4,"0")%></b></td>
				</tr>
				<%
						END IF
						IF classCd <> dr("CLASS_CD") THEN							
							classCd = dr("CLASS_CD")
						%>
				<tr>
					<td></td>
					<td colspan="11"><b><%=classCd.toString.PadLeft(4,"0")%>&nbsp;&nbsp;<%=dr("CLASS_DESC")%></b></td>
				</tr>
				<%
						END IF
				%>
				<tr>
					<td></td>
					<td></td>
					<td><%=dr("VE_CD")%></td>
					<td><%=dr("ITM_CD")%></td>
					<td><%=dr("ITEM_DESC")%></td>
					<td><%=dr("PC_NUM")%></td>
					<td><%=GetShortDate(dr("BEG_DT"))%></td>
					<td><%=NullToString(dr("SIZE_CD"))%></td>
					<td><%=dr("PC_TP")%></td>
					<td align="right"><%=FormatNumber(dr("PREV_PERM_RET"),2)%></td>
					<td align="right"><%=FormatNumber(dr("RET_PRC"),2)%></td>
					<td align="right"><%=dr("SKU_NUM")%></td>
				</tr>
				<%		
					Next
				END IF
				%>
			</table>
		</td>
	</tr>
</table>
