Imports System.IO
Public Class ViewReportFile
    Inherits System.Web.UI.Page
    Private _reportFileId As Integer = 0
    Private _fileName As String
    Private _sessionUser As HotTopic.RD.Application.Session
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        
        If Not IsPostBack Then
            'If IsNumeric(Request("reportFileId")) Then
            '    _reportFileId = Request("ReportFileID")
            'End If
            '_fileName = _sessionUser.GetFilePathByID(_reportFileId)
            _fileName = Request("FilePath")
            DownloadFile(_fileName)
        End If
    End Sub
    Private Sub DownloadFile(ByVal filePath As String)

        Try


            If File.Exists(filePath) Then
                Dim name = Path.GetFileName(filePath)
                Dim ext = Path.GetExtension(filePath)
                Dim type As String = ""

                If Not IsDBNull(ext) Then
                    ext = LCase(ext)
                End If

                Select Case ext
                    Case ".htm", ".html"
                        type = "text/HTML"
                    Case ".txt"
                        type = "text/plain"
                    Case ".doc", ".rtf"
                        type = "Application/msword"
                    Case ".csv", ".xls"
                        type = "Application/x-msexcel"
                    Case ".pdf", ".PDF"
                        type = "Application/pdf"
                    Case Else
                        type = "text/plain"
                End Select


                If type <> "" Then
                    Response.ContentType = type
                End If

                Response.WriteFile(filePath)
                Response.End()
            Else
                Response.Write("<script>alert('Selected Report File is currently not available!');self.close();</script>")
                Response.End()
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub


End Class
