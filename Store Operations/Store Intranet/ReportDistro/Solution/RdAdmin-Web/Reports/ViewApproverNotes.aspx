<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ViewApproverNotes.aspx.vb" Inherits="ReportDistroAdmin.ViewApproverNotes"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Report Distribution System</title>
		<LINK href="../include/Styles.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script>
		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" bgcolor="#000000" onload="self.focus()">
		<form id="Form1" method="post" runat="server">
			<table width="490" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center">
						<table>
							<tr>
								<td colspan="3" align="middle" class="pageCaption">Approver Notes</td>
							</tr>
							<tr>
								<td colspan="3" height="20">
									<asp:Label ID="lblExMsg" Runat="server" CssClass="errStyle"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">Report Title</td>
								<td width="10" class="reqStyle"></td>
								<td class="cellvaluecaption">
									<asp:Label id="lblTitle" Runat="server" CssClass="cellvaluecaption"></asp:Label>
								</td>
							</tr>
							<tr height="10">
								<td colspan="3"></td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">Fiscal Week</td>
								<td width="10" class="reqStyle"></td>
								<td class="cellvaluecaption"><asp:Label ID="lblfiscalWeek" runat="server"></asp:Label>
								</td>
							</tr>
							<tr height="10">
								<td colspan="3"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<asp:DataGrid ID="dgReport" Width="460" Runat="server" BorderColor="#990000" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false" PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
							<Columns>
								<asp:BoundColumn DataField="fullName" HeaderText="Posted By" HeaderStyle-Width="100" ItemStyle-VerticalAlign="Top"></asp:BoundColumn>
								<asp:BoundColumn DataField="insertDate" HeaderText="Posted On" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="80" ItemStyle-VerticalAlign="Top"></asp:BoundColumn>
								<asp:BoundColumn DataField="Comment" HeaderText="Notes" HeaderStyle-Width="280" ItemStyle-VerticalAlign="Top"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
				<tr height="10">
					<td></td>
				</tr>
				<tr>
					<td align="middle">
						<input type="button" id="btnClose" name="btnClose" class="B1" value="Close" onclick="self.close()">
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
