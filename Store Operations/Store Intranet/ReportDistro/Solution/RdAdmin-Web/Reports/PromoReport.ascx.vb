Public Partial Class PromoReport
	Inherits System.Web.UI.UserControl

	Public ReportDs As New DataSet()
	Public DV As DataView
	Public rptHeader As ReportHeader
	Public rptItem As ReportItem
	Protected dr As DataRow
	Protected counter As Integer
	Structure ReportHeader
		Public IssueDate As String
		Public EffectiveDate As String
		Public CancelDate As String
		Public StoreNum As String
		Public StoreName As String
		Public RptCaption As String
		Public ReportType As String
		Public SplInstruction As String
		Public CompanyName As String
	End Structure
	Structure ReportItem
		Public Dept As String
		Public VendorCd As String
		Public VendorName As String
		Public ItemCd As String
		Public ItemDesc As String
		Public Size As String
		Public SKU As String
		Public CurrentPrice As Double
		Public NewPrice As Double
		Public Promo As String
		Public Qty As Int32
		Public Count As Integer
	End Structure
#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		'Put user code to initialize the page here

	End Sub
	Public Sub BindData()
	End Sub
	Public Sub IntializeItem(ByRef rptItem As ReportItem)
		With rptItem
			.Dept = ""
			.VendorCd = ""
			.VendorName = ""
			.ItemCd = ""
			.ItemDesc = ""
			.Size = ""
			.CurrentPrice = 0
			.NewPrice = 0
			.Promo = ""
			.Qty = 0
		End With
	End Sub
	Public Function PrepareItemStr(ByVal rptItem As ReportItem) As String

        Dim itemStr As String = ""
        Dim scene7Url As String = ConfigurationSettings.AppSettings("scene7ImgURL")
        itemStr = "<td valign='top' style='border-right-color: black; border-right-width: 1px; border-right-style: solid;'>" & _
        "<div style='page-break-inside: avoid;'>" & _
        "<table width='250' align='left' cellspacing='0' cellpadding='0' valign='top'>" & _
        "<tbody>" & _
        "<tr>" & _
        "<td width='50' valign='top'><STRONG>" & rptItem.Dept & ":<br>" & rptItem.ItemCd & "</STRONG></td>" & _
        "<td height='30' valign='top' colspan='5'>" & rptItem.ItemDesc & "</td>" & _
        "</tr>" & _
        "<tr>" & _
        "<td width='40' valign='top' rowspan='3'>" & _
        "<img onerror='imgError(this);' alt='IMG' src=' " & scene7Url + rptItem.ItemCd & "_hi?wid=45' " & _
        " onmouseover='swapImage(this," & scene7Url + rptItem.ItemCd & "_hi?wid=400"")'; onmouseout='swapImage(this," & scene7Url + rptItem.ItemCd & "_hi?wid=45"")'; align='texttop'>" & _
        "</td>" & _
        "<td width='250' valign='top' rowspan='3' colspan='2'>" & _
        "<table width='100%' align='left' border='0' cellspacing='0' cellpadding='0' valign='top' rowspan='3'>" & _
        "<tbody>" & _
        "<tr>" & _
        "<td width='190' align='left' valign='top'>" & rptItem.SKU & " NONE: " & rptItem.Size & "</td>" & _
        "</tr>" & _
        "</tbody>" & _
        "</table>" & _
        "</td>" & _
        "<td width='80' align='right' valign='top'>" & rptItem.Promo & "</td>" & _
        "</tr>" & _
        "</tbody>" & _
        "</table>" & _
        "</div>" & _
        "</td>"

		counter = counter + 1
		If (counter Mod 3 = 0) Then
			itemStr &= "</tr><tr>"
		End If
		Return itemStr
	End Function
	Public Function FormatSize(ByVal sizeStr As String) As String
		If Trim(sizeStr) <> "" Then
			Return "[" & sizeStr & "]"
		Else
			Return ""
		End If
	End Function
	Public Function NullToString(ByVal Value As Object) As String
		If Convert.IsDBNull(Value) Then
			Return ""
		Else
			Return Convert.ToString(Value)
		End If
	End Function
End Class