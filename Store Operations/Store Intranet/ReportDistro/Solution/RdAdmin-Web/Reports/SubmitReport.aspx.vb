Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Public Class SubmitReport1
    Inherits System.Web.UI.Page

    Private _sessionUser As HotTopic.RD.Application.Session

    Protected menuStr As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr

        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try

        If Not IsPostBack Then
            InitializeForm()
        End If
    End Sub

    Private Sub InitializeForm()
        Dim ds As DataSet = _sessionUser.GetAllReportTitles(FileInputType.System)

        lstRptTitle.DataValueField = "ReportID"
        lstRptTitle.DataTextField = "ReportTitle"
        lstRptTitle.DataSource = ds
        lstRptTitle.DataBind()
        lstRptTitle.Items.Insert(0, New ListItem("---", ""))
        Comparevalidator3.ValueToCompare = Date.Today.ToShortDateString()
    End Sub

    Private Sub lstRptTitle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstRptTitle.SelectedIndexChanged
        txtStagingPath.Text = ""
        hidLocationPath.Value = ""
        If lstRptTitle.SelectedIndex > 0 Then
            Dim rpt As Report = _sessionUser.GetReportOnly(Convert.ToInt32(lstRptTitle.SelectedItem.Value))

            txtStagingPath.Text = rpt.StagingPath
            hidLocationPath.Value = rpt.LocationPath
            If rpt.ApprovalType = ReportApprovalType.None Then
                hidStatus.Value = ReportStatus.Approved
            Else
                hidStatus.Value = ReportStatus.Pending
            End If
            If rpt.ExpirationType = ExpType.ExpDate Then
                divExp.Style("display") = "inline"
            ElseIf rpt.ExpirationType = ExpType.NthReport Then
                divExp.Style("display") = "none"
            End If
            Requiredfieldvalidator4.Enabled = (rpt.ExpirationType = ExpType.ExpDate)
            Comparevalidator3.Enabled = (rpt.ExpirationType = ExpType.ExpDate)
            CompareValidator4.Enabled = (rpt.ExpirationType = ExpType.ExpDate)
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim rptSubmission As Submission = New Submission(Convert.ToInt32(lstRptTitle.SelectedItem.Value), True)
        Try
            'validate staging path
            If Not _sessionUser.PathExists(txtStagingPath.Text) Then
                lblExMsg.Text = "Staging folder is inaccessible."
                Return
            End If
            'pre-validate if files are there
            If rptSubmission.CompanySubOwners.Count = 0 Then
                If Not _sessionUser.FilesExist(txtStagingPath.Text, rptSubmission.DefaultFileName & "*") Then
                    lblExMsg.Text = "Report file(s) do not exist in the staging folder."
                    Return
                End If
            Else
                Dim subCoOwner As SubCompanyOwner
                For Each subCoOwner In rptSubmission.CompanySubOwners
                    If Not _sessionUser.FilesExist(txtStagingPath.Text, rptSubmission.DefaultFileName & "*" & subCoOwner.OwnerID & "*") Then
                        lblExMsg.Text = "Report file(s) do not exist for all defined owners in the staging folder."
                        Return
                    End If
                Next
            End If

            With rptSubmission
                .ReportSubmissionId = 0
                .StagingPath = txtStagingPath.Text
                .LocationPath = hidLocationPath.Value
                .ReportDate = txtReportDate.Text
                .EffectivityStartDate = txtEffectiveDt.Text & " " & lstEffectiveTime.SelectedItem.Text
                If txtExpiryDt.Text.Trim.Length > 0 Then
                    .EffectivityEndDate = txtExpiryDt.Text & " " & lstExpiryTime.SelectedItem.Text
                End If
                .Comment = txtComment.Text
                .Status = hidStatus.Value
            End With
            _sessionUser.SubmitReport(rptSubmission)
            'go to lists
            Response.Redirect("ListReportSubmissions.aspx")
        Catch ex As RDAppException
            lblExMsg.Text = ex.Message
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class