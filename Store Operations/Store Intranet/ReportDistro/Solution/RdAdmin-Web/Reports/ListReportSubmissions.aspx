<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ListReportSubmissions.aspx.vb" Inherits="ReportDistroAdmin.ListReportSubmissions" %>
<HTML>
	<body>
		<!--#include file="../include/header.inc"-->
		<!--#include file="../include/clipAlert.inc"-->
		<script>
			function SubmitForm(){
				Form1.hdnReBindFlag.value = "1";
				Form1.submit();
			}
			function ShowAlert(msg){
				ShowClipAlert(msg);
				setTimeout("SubmitForm();",4500);
				
			}
			

		</script>
		<form id="Form1" method="post" runat="server">
			<input type=hidden id="hdnReBindFlag" name="hdnReBindFlag" value="0" runat=server>
			<br>
			<table border="0" cellpadding="0" cellspacing="0" width="520" align="center">
				<tr>
					<td align="middle" class="pageCaption">My Reports</td>
				</tr>
				<tr height="10">
					<td></td>
				</tr>
				<tr>
					<td>
						<asp:DataGrid ID="dgReport" DataKeyField="ReportSubmissionID" Width="750" Runat="server" BorderColor="#990000" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false" AllowPaging="True" AllowSorting="True" PageSize=10 PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
							<Columns>
								<asp:BoundColumn DataField="ReportSubmissionID" SortExpression="ReportSubmissionID" HeaderText="ReportSubmissionID" Visible="False"></asp:BoundColumn>								
								<asp:TemplateColumn HeaderText="Title"  SortExpression="ReportTitle" HeaderStyle-Width="150" ItemStyle-Width="150px" >
										<ItemTemplate>
											<a href="javascript:OpenPop('ViewReportFiles.aspx?sid=<%#container.dataitem("ReportSubmissionID")%>&rid=<%#container.dataitem("ReportId")%>',500,580)"><%#container.dataitem("ReportTitle")%></a>
										</ItemTemplate>
								</asp:TemplateColumn>		
								<asp:BoundColumn DataField="ReportDate" HeaderStyle-Width="60px"   DataFormatString="{0:MM/dd/yyyy}" SortExpression="ReportDate" HeaderText="Report Date"></asp:BoundColumn>						
								<asp:BoundColumn DataField="FiscalWeekEndingDate" HeaderStyle-Width="60px"   DataFormatString="{0:MM/dd/yyyy}" SortExpression="FiscalWeekEndingDate" HeaderText="Fiscal Week"></asp:BoundColumn>								
								<asp:BoundColumn DataField="Comment" HeaderStyle-Width="240px"   SortExpression="Comment" HeaderText="Comment"></asp:BoundColumn>
								
								<asp:TemplateColumn HeaderStyle-Width="50px" SortExpression="LookUpText"   HeaderText="Status">
									<ItemTemplate >										
										<%#ShowStatus(container.dataitem("Status"),container.dataitem("LookUpText"))%>
										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderStyle-Width="190px">
									<ItemTemplate>
										<a href="javascript:OpenPop('EditSubmissionComment.aspx?sid=<%#container.dataitem("ReportSubmissionID")%>&rid=<%#container.dataitem("ReportID")%>',500,550)">EditComment</a>
										&nbsp;
										<a href="javascript:OpenPop('ViewApproverNotes.aspx?sid=<%#container.dataitem("ReportSubmissionID")%>&rid=<%#container.dataitem("ReportID")%>',500,550)">ApproverNotes</a>
										&nbsp;
										<!-- SRI show link irrespective of status-->
										<a href="javascript:OpenPop('CancelSubmission.aspx?sid=<%#Container.Dataitem("ReportSubmissionID")%>&rid=<%#Container.Dataitem("ReportID")%>',500,500)"><%#ShowLink(Container.Dataitem("status"))%></a>
										<!--a href="javascript:OpenPop('CancelSubmission.aspx?sid=<%#Container.Dataitem("ReportSubmissionID")%>&rid=<%#Container.Dataitem("ReportID")%>',500,500)">Cancel</a-->
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:DataGrid>
						<asp:Label ID="SortBy" Visible="False" Runat="server"></asp:Label>
						<asp:Label ID="SortDirection" Visible="False" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
			</table>
			<!-- end here-->
			<!--footer-->
			<!--#include file="../include/footer.inc"--></form>
	</body>
</HTML>
