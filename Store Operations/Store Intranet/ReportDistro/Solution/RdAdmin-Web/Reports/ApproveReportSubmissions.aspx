<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ApproveReportSubmissions.aspx.vb" Inherits="ReportDistroAdmin.ApproveReportSubmissions" %>
<HTML>
	<body>
		<!--#include file="../include/header.inc"-->
		<!--#include file="../include/clipAlert.inc"-->
		<script>
			function SubmitForm(){
				Form1.hdnReBindFlag.value = "1";
				Form1.submit();
			}
			function ShowAlert(msg){
				ShowClipAlert(msg);
				setTimeout("SubmitForm();",4500);
				
			}
			

		</script>
		<form id="Form1" method="post" runat="server">
			<input type=hidden id="hdnReBindFlag" name="hdnReBindFlag" value="0" runat=server>
			<br>
			<table border="0" cellpadding="0" cellspacing="0" width="520" align="center">
				<tr>
					<td align="middle" class="pageCaption">Approve Reports</td>
				</tr>
				<tr height="10">
					<td></td>
				</tr>
				<tr>
					<td>
						<asp:DataGrid ID="dgReport" Width="730" Runat="server" BorderColor="#990000" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false" AllowPaging="True" PageSize=10 AllowSorting="True" PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
							<Columns>
								<asp:BoundColumn DataField="ReportSubmissionID" SortExpression="ReportSubmissionID" HeaderText="ReportSubmissionID" Visible="False"></asp:BoundColumn>
								<asp:BoundColumn DataField="ReportSubmissionID" SortExpression="ReportID" HeaderText="ReportID" Visible="False"></asp:BoundColumn>
								<asp:BoundColumn DataField="ReportTitle" SortExpression="ReportTitle" HeaderText="Title" HeaderStyle-Width="170"></asp:BoundColumn>
								<asp:BoundColumn DataField="ReportDate" HeaderStyle-Width="70px" DataFormatString="{0:MM/dd/yyyy}" SortExpression="ReportDate" HeaderText="Report Date"></asp:BoundColumn>
								<asp:BoundColumn DataField="FiscalWeekEndingDate" HeaderStyle-Width="70px" DataFormatString="{0:MM/dd/yyyy}" SortExpression="FiscalWeekEndingDate" HeaderText="Fiscal Week"></asp:BoundColumn>								
								<asp:BoundColumn DataField="Comment" HeaderStyle-Width="250px" SortExpression="Comment" HeaderText="Comment"></asp:BoundColumn>
								<asp:TemplateColumn ItemStyle-Width=170>
									<ItemTemplate>
										&nbsp;<a href="javascript:OpenPop('ViewReportFiles.aspx?sid=<%#container.dataitem("ReportSubmissionID")%>&rid=<%#container.dataitem("ReportID")%>',500,580)">View</a>
										&nbsp;|&nbsp;<a href="javascript:OpenPop('ViewApproverNotes.aspx?sid=<%#container.dataitem("ReportSubmissionID")%>&rid=<%#container.dataitem("ReportID")%>',500,550)">Approver Notes</a>
										&nbsp;|&nbsp;<a href="javascript:OpenPop('Approve.aspx?sid=<%#container.dataitem("ReportSubmissionID")%>&rid=<%#container.dataitem("ReportID")%>',500,500)">Approve</a>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:DataGrid>
						<asp:Label ID="SortBy" Visible="False" Runat="server"></asp:Label>
						<asp:Label ID="SortDirection" Visible="False" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
			</table>
			<!-- end here-->
			<!--footer-->
			<!--#include file="../include/footer.inc"--></form>
			
	</body>
</HTML>
<script>
function Test(){
ShowClipAlert('testing');
}
//window.onload = Test;

</script>