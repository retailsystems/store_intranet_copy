<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SubmitPromoReport.aspx.vb" Inherits="ReportDistroAdmin.SubmitPromoReport" %>
<%@ Register tagprefix="uc" tagname="PromoReport" src="PromoReport.ascx" %>

<HTML>
	<body>
		<!--#include file="../include/header.inc"-->
		<!--#include file="../include/calender.js"--><iframe id="CalFrame" style="DISPLAY: none; Z-INDEX: 100; WIDTH: 148px; POSITION: absolute; HEIGHT: 194px" marginWidth="0" marginHeight="0" src="../include/calendar/calendar.htm" frameBorder="0" noResize scrolling="no"></iframe>
		<form id="Form1" method="post" runat="server">
			<!--#include file="../include/wait.inc"-->
			<INPUT id="hidLocationPath" type="hidden" runat="server" NAME="hidLocationPath">
			<INPUT id="hidStatus" type="hidden" name="hidStatus" value="1" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" width="600" border="0">
				<tr>
					<td class="pageCaption" align="center" colSpan="3">Submit Promo Report</td>
				</tr>
				<tr height="8">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td colSpan="3" align="center"><asp:label id="lblExMsg" runat="server" CssClass="ErrStyle"></asp:label></td>
				</tr>
				<tr height="8">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="145">Report Title</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft" width="445"><asp:dropdownlist id="lstRptTitle" CssClass="cellvalueleft" Runat="server" Width="275px" AutoPostBack="True"></asp:dropdownlist>&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" CssClass="ErrStyle" ErrorMessage="Select a report title" Display="Dynamic" ControlToValidate="lstRptTitle"></asp:requiredfieldvalidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
<!--
				<tr>
					<td class="cellvaluecaption" width="145">Report Type</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft" width="445">
						<asp:dropdownlist id="lstRptType" CssClass="cellvalueleft" Runat="server" Width="275px" AutoPostBack="True"></asp:dropdownlist>
						&nbsp;<asp:requiredfieldvalidator id="Requiredfieldvalidator6" runat="server" CssClass="ErrStyle" ErrorMessage="Select a report type" Display="Dynamic" ControlToValidate="lstRptType"></asp:requiredfieldvalidator>
					</td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="145">Report Scope</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft">
						<asp:RadioButtonList Runat="server" ID="rdoMdReportScope" AutoPostBack="True" CssClass="cellvaluewhite" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="1" Selected="True">Store</asp:ListItem>
							<asp:ListItem Value="2">Master</asp:ListItem>
							<asp:ListItem Value="3">Warehouse</asp:ListItem>
						</asp:RadioButtonList>
					</td>
				</tr>
				<asp:Panel ID="pnlWarehouse" Visible="False" Runat="server">
					<tr height="5">
						<td colSpan="3"></td>
					</tr>
					<tr>
						<td class="cellvaluecaption" width="145">Warehouse</td>
						<td class="reqStyle" width="10">*</td>
						<td class="cellvalueleft">
							<asp:RadioButtonList Runat="server" ID="rdoMDWarehouse" AutoPostBack="True" CssClass="cellvaluewhite" RepeatDirection="Horizontal" RepeatLayout="Flow">
								<asp:ListItem Value="9999" Selected="True">CA</asp:ListItem>
								<asp:ListItem Value="9997">TN</asp:ListItem>
							</asp:RadioButtonList>
						</td>
					</tr>
				</asp:Panel>
-->
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="145">Report By</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft">
						<asp:RadioButtonList Runat="server" ID="rdoReportBy" AutoPostBack="True" CssClass="cellvaluewhite" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="1" Selected="True">Promo Date</asp:ListItem>

						</asp:RadioButtonList>
					</td>
				</tr>
				<asp:Panel ID="pnlEffectiveDt" Visible="True" Runat="server">
					<TR height="5">
						<TD colSpan="3"></TD>
					</TR>
					<TR>
						<TD class="cellvaluecaption" width="145">Promo Date</TD>
						<TD class="reqStyle" width="10">*</TD>
						<TD class="cellvalueleft">
							<asp:textbox id="txtMDEffectiveDate" CssClass="cellvalueleft" Width="100px" Runat="server"></asp:textbox>&nbsp;<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar4,document.Form1.txtMDEffectiveDate,null,0,330)"><IMG id="calendar4" src="../include/calendar/calendar.gif" align="absBottom" border="0" name="calendar4"></A>
							<asp:requiredfieldvalidator id="Requiredfieldvalidator5" runat="server" CssClass="ErrStyle" ControlToValidate="txtMDEffectiveDate" Display="Dynamic" ErrorMessage="Effective date is required."></asp:requiredfieldvalidator>
							<asp:comparevalidator id="Comparevalidator6" runat="server" CssClass="ErrStyle" ControlToValidate="txtMDEffectiveDate" Display="Dynamic" ErrorMessage="Invalid Date." Type="Date" Operator="DataTypeCheck"></asp:comparevalidator></TD>
					</TR>
				</asp:Panel>
				<asp:Panel ID="pnlPcNum" Visible="false" Runat="server">
					<TR height="5">
						<TD colSpan="3"></TD>
					</TR>
					<TR>
						<TD class="cellvaluecaption" width="145">Price Change Number
						</TD>
						<TD class="reqStyle" width="10">*</TD>
						<TD class="cellvalueleft">
							<asp:textbox id="txtPrcNum" CssClass="cellvalueleft" Width="100px" Runat="server"></asp:textbox>
							<asp:requiredfieldvalidator id="Requiredfieldvalidator7" runat="server" CssClass="ErrStyle" ControlToValidate="txtPrcNum" Display="Dynamic" ErrorMessage="PC Num is required."></asp:requiredfieldvalidator>
							<asp:CompareValidator id="CompareValidator7" runat="server" CssClass="ErrStyle" ControlToValidate="txtPrcNum" Display="Dynamic" ErrorMessage="Invalid PC Num." Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator></TD>
					</TR>
				</asp:Panel>
				<asp:Panel ID="pnlRptMetaData" Runat="server" Visible="True"> <!--Hide this panel for master/ware house report-->
					<TR height="5">
						<TD colSpan="3"></TD>
					</TR>
					<TR>
						<TD class="cellvaluecaption" width="145" height="23">Report Date</TD>
						<TD class="reqStyle" width="10" height="23">*</TD>
						<TD class="cellvalueleft" height="23">
							<asp:textbox id="txtReportDate" CssClass="cellvalueleft" Width="100px" Runat="server"></asp:textbox>&nbsp;<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar1,document.Form1.txtReportDate,null,0,330)"><IMG id="calendar1" src="../include/calendar/calendar.gif" align="absBottom" border="0" name="calendar1"></A>
							<asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" CssClass="ErrStyle" ControlToValidate="txtReportDate" Display="Dynamic" ErrorMessage="Report date is required."></asp:requiredfieldvalidator>
							<asp:comparevalidator id="CompareValidator1" runat="server" CssClass="ErrStyle" ControlToValidate="txtReportDate" Display="Dynamic" ErrorMessage="Invalid Date." Type="Date" Operator="DataTypeCheck"></asp:comparevalidator></TD>
					</TR>
					<TR height="5">
						<TD colSpan="3"></TD>
					</TR>
					<TR>
						<TD class="cellvaluecaption" width="145">Store Visibilty Date</TD>
						<TD class="reqStyle" width="10">*</TD>
						<TD class="cellvalueleft">
							<asp:textbox id="txtEffectiveDt" CssClass="cellvalueleft" Width="100px" Runat="server"></asp:textbox>&nbsp;<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar2,document.Form1.txtEffectiveDt,null,0,330)"><IMG id="calendar2" src="../include/calendar/calendar.gif" align="absBottom" border="0" name="calendar2"></A>
							<asp:DropDownList id="lstEffectiveTime" CssClass="cellvalueleft" Runat="server">
								<asp:ListItem Selected="True">12:00 AM</asp:ListItem>
								<asp:ListItem>01:00 AM</asp:ListItem>
								<asp:ListItem>02:00 AM</asp:ListItem>
								<asp:ListItem>03:00 AM</asp:ListItem>
								<asp:ListItem>04:00 AM</asp:ListItem>
								<asp:ListItem>05:00 AM</asp:ListItem>
								<asp:ListItem>06:00 AM</asp:ListItem>
								<asp:ListItem>07:00 AM</asp:ListItem>
								<asp:ListItem>08:00 AM</asp:ListItem>
								<asp:ListItem>09:00 AM</asp:ListItem>
								<asp:ListItem>10:00 AM</asp:ListItem>
								<asp:ListItem>11:00 AM</asp:ListItem>
								<asp:ListItem>12:00 PM</asp:ListItem>
								<asp:ListItem>01:00 PM</asp:ListItem>
								<asp:ListItem>02:00 PM</asp:ListItem>
								<asp:ListItem>03:00 PM</asp:ListItem>
								<asp:ListItem>04:00 PM</asp:ListItem>
								<asp:ListItem>05:00 PM</asp:ListItem>
								<asp:ListItem>06:00 PM</asp:ListItem>
								<asp:ListItem>07:00 PM</asp:ListItem>
								<asp:ListItem>08:00 PM</asp:ListItem>
								<asp:ListItem>09:00 PM</asp:ListItem>
								<asp:ListItem>10:00 PM</asp:ListItem>
								<asp:ListItem>11:00 PM</asp:ListItem>
							</asp:DropDownList>
							<asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" CssClass="ErrStyle" ControlToValidate="txtEffectiveDt" Display="Dynamic" ErrorMessage="Effective date is required."></asp:requiredfieldvalidator>
							<asp:comparevalidator id="Comparevalidator2" runat="server" CssClass="ErrStyle" ControlToValidate="txtEffectiveDt" Display="Dynamic" ErrorMessage="Invalid date format." Type="Date" Operator="DataTypeCheck"></asp:comparevalidator>
							<asp:CompareValidator id="CompareValidator5" runat="server" CssClass="ErrStyle" ControlToValidate="txtEffectiveDt" Display="Dynamic" ErrorMessage="Effectivity date should not be less than Report Date." Type="Date" Operator="GreaterThanEqual" ControlToCompare="txtReportDate"></asp:CompareValidator></TD>
					</TR>
					<TR height="5">
						<TD colSpan="3"></TD>
					</TR>
<!--
					<TR>
						<TD align="left" colSpan="3">
							<DIV id="divExp" style="DISPLAY: none" align="left" runat="server">
								<TABLE cellSpacing="0" cellPadding="0" align="left" border="0">
									<TR>
										<TD class="cellvaluecaption" width="145">Expires On</TD>
										<TD class="reqStyle" align="left" width="10">*</TD>
										<TD class="cellvalueleft" align="left" width="445">
											<asp:textbox id="txtExpiryDt" CssClass="cellvalueleft" Width="100px" Runat="server"></asp:textbox>&nbsp;<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar3,document.Form1.txtExpiryDt,null,0,330)"><IMG id="calendar3" src="../include/calendar/calendar.gif" align="absBottom" border="0" name="calendar3"></A>
											<asp:DropDownList id="lstExpiryTime" CssClass="cellvalueleft" Runat="server">
												<asp:ListItem Selected="True">12:00 AM</asp:ListItem>
												<asp:ListItem>01:00 AM</asp:ListItem>
												<asp:ListItem>02:00 AM</asp:ListItem>
												<asp:ListItem>03:00 AM</asp:ListItem>
												<asp:ListItem>04:00 AM</asp:ListItem>
												<asp:ListItem>05:00 AM</asp:ListItem>
												<asp:ListItem>06:00 AM</asp:ListItem>
												<asp:ListItem>07:00 AM</asp:ListItem>
												<asp:ListItem>08:00 AM</asp:ListItem>
												<asp:ListItem>09:00 AM</asp:ListItem>
												<asp:ListItem>10:00 AM</asp:ListItem>
												<asp:ListItem>11:00 AM</asp:ListItem>
												<asp:ListItem>12:00 PM</asp:ListItem>
												<asp:ListItem>01:00 PM</asp:ListItem>
												<asp:ListItem>02:00 PM</asp:ListItem>
												<asp:ListItem>03:00 PM</asp:ListItem>
												<asp:ListItem>04:00 PM</asp:ListItem>
												<asp:ListItem>05:00 PM</asp:ListItem>
												<asp:ListItem>06:00 PM</asp:ListItem>
												<asp:ListItem>07:00 PM</asp:ListItem>
												<asp:ListItem>08:00 PM</asp:ListItem>
												<asp:ListItem>09:00 PM</asp:ListItem>
												<asp:ListItem>10:00 PM</asp:ListItem>
												<asp:ListItem>11:00 PM</asp:ListItem>
											</asp:DropDownList>
											<asp:requiredfieldvalidator id="Requiredfieldvalidator4" runat="server" CssClass="ErrStyle" ControlToValidate="txtExpiryDt" Display="Dynamic" ErrorMessage="Expiry date is required." Enabled="False"></asp:requiredfieldvalidator>
											<asp:comparevalidator id="Comparevalidator3" runat="server" CssClass="ErrStyle" ControlToValidate="txtExpiryDt" Display="Dynamic" ErrorMessage="Invalid date format or past date disallowed." Type="Date" Operator="GreaterThanEqual" Enabled="False"></asp:comparevalidator>&nbsp;
											<asp:comparevalidator id="CompareValidator4" runat="server" CssClass="ErrStyle" ControlToValidate="txtExpiryDt" Display="Dynamic" ErrorMessage="Expiry date should be greater than Effective date." Type="Date" Operator="GreaterThan" ControlToCompare="txtEffectiveDt" Enabled="False"></asp:comparevalidator></TD>
									</TR>
								</TABLE>
							</DIV>
						</TD>
					</TR>
-->
					<TR height="5">
						<TD colSpan="3"></TD>
					</TR>
					<TR>
						<TD class="cellvaluecaption" vAlign="top" width="145">
							<asp:CheckBox id="cbSplInstruction" CssClass="cellvalueleft" Runat="server"></asp:CheckBox>Special 
							Instructions</TD>
						<TD width="10"></TD>
						<TD class="cellvalueleft">
							<asp:textbox id="txtSplInstruction" CssClass="cellvalueleft" Runat="server" width="275px" MaxLength="300" Rows="4" TextMode="MultiLine"></asp:textbox></TD>
					</TR>
					<TR height="5">
						<TD colSpan="3"></TD>
					</TR>
					<TR>
						<TD class="cellvaluecaption" vAlign="top" width="145">Comments:</TD>
						<TD width="10"></TD>
						<TD class="cellvalueleft">
							<asp:textbox id="txtComment" CssClass="cellvalueleft" Runat="server" width="275px" MaxLength="300" Rows="4" TextMode="MultiLine"></asp:textbox></TD>
					</TR>
				</asp:Panel>
			</table>
			</TD>  
			<!-- end here-->
			<!--footer-->
			<tr>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td align="right"><div id="w1" style="DISPLAY:inline" onclick="showWait()"><asp:button id="btnSubmit" CssClass="B1" Runat="server" Text="Submit"></asp:button></div>
					&nbsp;&nbsp; 
					<!--#include file="../include/homeLink.inc"--></td>
			</tr>
			</TABLE>
		</form>
	</body>
</HTML>