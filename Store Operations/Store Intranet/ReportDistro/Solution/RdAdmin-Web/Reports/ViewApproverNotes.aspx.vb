Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Public Class ViewApproverNotes
    Inherits System.Web.UI.Page

    Private _sessionUser As HotTopic.RD.Application.Session
    Private _reportId As Integer
    Private _submissionId As Integer
    Private _submission As Submission
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
            _reportId = Request("rid")
            _submissionId = Request("sid")
            _submission = New Submission(_reportId, _submissionId)
            If Not IsPostBack Then
                InitializeForm()
            End If
        Catch ex As RDAppException
            lblExMsg.Text = ex.Message
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
       
       
    End Sub
    Private Sub InitializeForm()
        With _submission
            lblTitle.Text = .Title
            lblfiscalWeek.Text = .FiscalWeekEndingDate
        End With
        BindData()
    End Sub
    Private Sub BindData()
        Try
            Dim ds As DataSet
            ds = _sessionUser.ListApproverNotes(_submissionId)
            dgReport.DataSource = ds
            dgReport.DataBind()
        Catch ex As RDAppException
            lblExMsg.Text = ex.Message
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
