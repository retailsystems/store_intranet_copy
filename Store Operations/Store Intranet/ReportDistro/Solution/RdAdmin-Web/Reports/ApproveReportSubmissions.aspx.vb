
Imports HotTopic.RD.Application
Partial Class ApproveReportSubmissions
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.RD.Application.Session
    Protected menuStr As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        If Not IsPostBack Then
            hdnReBindFlag.Value = 0
            SortBy.Text = "ReportDate"
            SortDirection.Text = "desc"
            BindData()
        Else
            If hdnReBindFlag.Value = "1" Then
                BindData()
               
                hdnReBindFlag.Value = 0
            End If
        End If

    End Sub
    Private Sub BindData()
        Dim ds As DataSet
        ds = _sessionUser.ListPendingApprovals
        dgReport.DataSource = ds.Tables(0)
        ds.Tables(0).DefaultView.Sort = SortBy.Text & " " & SortDirection.Text
        If dgReport.CurrentPageIndex > 0 And ds.Tables(0).Rows.Count <= (dgReport.CurrentPageIndex * dgReport.PageSize) Then
            dgReport.CurrentPageIndex = dgReport.CurrentPageIndex - 1
        End If
        dgReport.DataBind()
        
    End Sub

    Private Sub dgReport_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgReport.PageIndexChanged
        dgReport.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub

    Private Sub dgReport_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgReport.SortCommand
        If SortBy.Text = e.SortExpression Then
            Select Case SortDirection.Text
                Case "asc"
                    SortDirection.Text = "desc"
                Case "desc"
                    SortDirection.Text = "asc"
            End Select
        Else
            SortDirection.Text = "asc"
        End If
        SortBy.Text = e.SortExpression

        BindData()
    End Sub
End Class
