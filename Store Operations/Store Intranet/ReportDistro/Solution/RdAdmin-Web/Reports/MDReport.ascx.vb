Imports System.Configuration
Imports System.Web

Partial Class MDReport
	Inherits System.Web.UI.UserControl

	Public deptCd As Integer = 0
	Public ReportDs As New DataSet()
	Public DV As DataView
	Public rptHeader As ReportHeader
	Public rptItem As ReportItem
	Protected dr As DataRow
	Public index As Integer = 0
	Public prevItem As String = ""
	Public breakLine As Boolean = False
	Public addCell As Integer = 0
	Public rowCount As Integer
	Public breakPage As Boolean = False
	Public firstPage As Boolean = True
	Structure ReportHeader
		Public IssueDate As String
		Public EffectiveDate As String
		Public CancelDate As String
		Public StoreNum As String
		Public StoreName As String
		Public RptCaption As String
		Public ReportType As String
		Public SplInstruction As String
		Public CompanyName As String
	End Structure
	Structure ReportItem
		Public VendorCd As String
		Public VendorName As String
		Public ItemCd As String
		Public ItemDesc As String
		Public Size As String
		Public CurrentPrice As Double
		Public NewPrice As Double
		Public Qty As Int32
		Public Color As String
		Public ImgLink As String
		Public Count As Integer
		Public SkuT() As String
		Public ItemPos As Integer
	End Structure
#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		'Put user code to initialize the page here

	End Sub
	Public Sub BindData()
	End Sub
	Public Sub IntializeItem(ByRef rptItem As ReportItem)
		With rptItem
			.VendorCd = ""
			.VendorName = ""
			.ItemCd = ""
			.ItemDesc = ""
			.Size = ""
			.Color = ""
			.CurrentPrice = 0
			.NewPrice = 0
			.Qty = 0
			.ImgLink = "http://htintranet/hottopic/RptAdmin/images/wspacer.gif"
			.Count = 0
			' .SkuT(0) = Nothing
		End With
	End Sub

	Public Function PrepareItemStrFinal(ByVal rptItem As ReportItem) As String

		Dim itemStr As String = ""
		'8/22/13 Michelle An - separate Item line and Sku line     
		Dim skuStr As String = ""

		'get the sku count first
		Try
			If rptItem.Size.Contains("|") Then
				'clean up the extra "|"
				If rptItem.Size.EndsWith("|") Then
					rptItem.Size = rptItem.Size.Remove(rptItem.Size.Length - 1, 1)
				End If
				Dim multiSku() As String
				multiSku = rptItem.Size.Split("|")
				rptItem.SkuT = multiSku
				rptItem.Count = rptItem.SkuT.Length
			Else
				Dim singleSku(0) As String
				singleSku(0) = rptItem.Color & ": " & rptItem.Size
				rptItem.SkuT = singleSku
				rptItem.Count = 1
			End If
			Dim beginTR As String = ""
			Dim endTR As String = ""
			Dim pageHeader As String = ""

			Dim border As String = ""
			Dim breakStyle As String = ""
			Dim counter As Integer
			'determine the position of the item in the layout

			Dim pos As Integer
			pos = GetPos(index)

			Select Case pos
				Case 1
					'            beginTR = "<!--" & index & "row" & rowCount & "-1st item-->"
				Case 3
					'           beginTR = "<!--" & index & "row" & rowCount & "-3rd item-->"
				Case 2
					'          beginTR = "<!--" & index & "row" & rowCount & "-2nd item-->"
			End Select

			If pos = 1 Then
				If rowCount = 1 And firstPage = False Then
					pageHeader = PrintHeader()
				End If
				beginTR += "<tr>"
			End If
			If firstPage = True Then
				counter = 7
			Else
				counter = 9
			End If


			If rowCount >= counter And pos = 3 Then
				breakPage = True
				rowCount = 0
				firstPage = False
			End If

			' if tallys up to be 9 rows and position is on the right, do a page break afterwards
			If (breakPage = True) Then
				breakStyle = "<div style='page-break-inside:avoid; page-break-after:always;'>"
			Else
				breakStyle = "<div style='page-break-inside:avoid;'>"
			End If

			itemStr = pageHeader + beginTR + "<td valign='top' "
			itemStr += "style='BORDER-RIGHT: black solid 1px;'>"
			itemStr += breakStyle + "<table width='250' align = 'left' valign='top' cellspacing=0 cellpadding=0 ><tr><td width='50' valign='top'><STRONG>" & _
					   deptCd.ToString.PadLeft(4, "0") & ":<br>" & _
					   rptItem.ItemCd & "</STRONG></td>" & _
			   "<td valign='top' colspan='5' height='30'>" & rptItem.ItemDesc & "</td></tr>"
			'parse img url
            If rptItem.ItemCd <> "" Then
                If rptHeader.CompanyName.ToUpper = "TORRID" Then
                    rptItem.ImgLink = ConfigurationSettings.AppSettings("TDImgURL").Replace("xxxxxx", rptItem.ItemCd)
                Else
                    rptItem.ImgLink = ConfigurationSettings.AppSettings("ImgURL").Replace("xxxxxx", rptItem.ItemCd)
                End If

            End If
			'Sku line

            Dim strMouseOverImgLink As String = rptItem.ImgLink.Replace(rptItem.ImgLink.Substring(rptItem.ImgLink.IndexOf("?"), rptItem.ImgLink.Length - rptItem.ImgLink.IndexOf("?")), "?wid=400")

            itemStr += "<tr>" & _
                "<td  width='40' valign='top' rowspan='3'><img src='" & rptItem.ImgLink & "' ALT='IMG' onmouseover='swapImage(this,""" & strMouseOverImgLink & """)'; onmouseout='swapImage(this,""" & rptItem.ImgLink & """)'; onerror='imgError(this);' align='texttop'></td>" & _
                "<td colspan='2' width='250' valign='top' rowspan='3'>"



			Dim i As Integer
			Dim s As Integer
			Dim j As Integer


			j = 0

			skuStr += "<table align = 'left' width='100%' valign='top' cellspacing=0 cellpadding=0 border=0 rowspan='3'>"
			For i = 0 To rptItem.Count - 1
				skuStr += "<tr>"
				skuStr += "<td valign='top' align='left' width='190'>" & rptItem.SkuT(i) & "</td>"
				skuStr += "</tr>"
			Next
			skuStr += "</table>"

			skuStr += "</td>"
			itemStr += skuStr & "<td align='right' width='40' valign='top'>" & FormatNumber(rptItem.CurrentPrice, 2) & "</td>" & _
					"<td align='right' width='40' valign='top'>" & FormatNumber(rptItem.NewPrice, 2) & "</td>" & _
					"<td align='right' width='30' valign='top'>" & rptItem.Qty & "&nbsp;</td></tr>" & _
					"<tr><td>STR CNT</td><td align='right' width='40' height='30' valign='bottom'>______</td><td>&nbsp;</td></tr>" & _
					"<tr><td>MGR INIT</td><td align='right' width='40' height='30' valign='bottom'>______</td><td>&nbsp;</td></tr></table></div></td>"


			If rptItem.ItemCd <> prevItem Then
				prevItem = rptItem.ItemCd
				'    If addCell = 2 Then
				'        itemStr += "<!--add cell--><td style='BORDER-RIGHT: black solid 1px;'><div style='page-break-inside:avoid;'>&nbsp;</div></td><td style='BORDER-RIGHT: black solid 1px;'>&nbsp;</td>"

				'    ElseIf addCell = 1 Then
				'        itemStr += "<!--add cell--><td style='BORDER-RIGHT: black solid 1px;'><div style='page-break-inside:avoid;'>&nbsp;</div></td>"

				'    End If
				'    addCell = 0
				If index <= 1 Then
					index = index + 1
				Else
					itemStr += "</tr>"
					index = 0
					rowCount = rowCount + 1
				End If

			End If
			'reset breakLine every time - it only needs to be recognized once
			breakLine = False
			breakPage = False
		Catch Ex As Exception
			Throw Ex
		End Try
		Return itemStr
	End Function

	Public Function CheckPos(ByVal ind As Integer) As Integer
		Dim line As Integer = 0
		Select Case ind
			Case 0
				'first cell, add two empty cell and end line
				'line = "<!--1st item end class-->"
				'line+= "<td valign='top' colspan=1 style='BORDER-RIGHT: black solid 1px;'>&nbsp;</td>"
				'line += "<td valign='top' colspan=1 style='BORDER-RIGHT: black solid 1px;'>&nbsp;</td></tr>"
				index = ind + 2
				line = 2

			Case 1
				'second cell, add 1 empty cell and end line
				'line = "<!--2nd item end class-->"
				'line+="<td valign='top' colspan=1 style='BORDER-RIGHT: black solid 1px;'>&nbsp;</td></tr>"
				index = ind + 1
				line = 1

			Case 2
				'third cell, no empty cell and no need to end line
				' line = "<!--3rd item end class-->"
			Case Else
				'line = "<!--wrong index-->"

		End Select
		Return line

	End Function
	Public Function PrintHeader() As String
		Dim header As String
		header = "<tr>" & _
	 "<td style='BORDER-RIGHT: black solid 1px;'>" & _
	"<table align='left' width='100%' cellpadding='1' cellspacing='0'><tr>" & _
	"<th width='45' align='left'>" & _
	"DEPT:<br>STYLE</th>" & _
	"<th align='left' width='110'>CHILD SKU</th>" & _
	"<th width='35' align='right'>CURR. PRICE</th>" & _
	 "<th width='35' align='right'>NEW PRICE</th>" & _
 "<th align='right'>OH QTY</th></tr></table></td>" & _
 "<td style='BORDER-RIGHT: black solid 1px;'>" & _
	"<table align='left' width='100%' cellpadding='1' cellspacing='0'><tr>" & _
	"<th width='45' align='left'>" & _
	"DEPT:<br>STYLE</th>" & _
	"<th align='left' width='110'>CHILD SKU</th>" & _
	"<th width='35' align='right'>CURR. PRICE</th>" & _
	 "<th width='35' align='right'>NEW PRICE</th>" & _
 "<th align='right'>OH QTY</th></tr></table></td>" & _
 "<td style='BORDER-RIGHT: black solid 1px;'>" & _
	"<table align='left' width='100%' cellpadding='1' cellspacing='0'><tr>" & _
	"<th width='45' align='left'>" & _
	"DEPT:<br>STYLE</th>" & _
	"<th align='left' width='110'>CHILD SKU</th>" & _
	"<th width='35' align='right'>CURR. PRICE</th>" & _
	 "<th width='35' align='right'>NEW PRICE</th>" & _
 "<th align='right'>OH QTY</th></tr></table></td>" & _
 "</tr><tr><td colspan='16'><hr style='COLOR: black;HEIGHT: 2px'></td></tr>"

		Return header
	End Function

	Public Function GetPos(ByVal ind As Integer) As Integer
		Dim pos As Integer
		If (index Mod 3) = 0 Then
			pos = 1
		ElseIf ((index + 1) Mod 3 = 0) Then
			pos = 3
		Else
			pos = 2
		End If
		Return pos
	End Function
	Public Function CheckURL(ByVal URL As String) As String
		Try
			Dim request As System.Net.WebRequest = System.Net.WebRequest.Create(URL)
			Dim response As System.Net.WebResponse = request.GetResponse()
		Catch ex As Exception
			Return "http://htintranet/hottopic/RptAdmin/images/wspacer.gif"
		End Try
		Return URL
	End Function

	Public Function FormatSize(ByVal sizeStr As String) As String
		If Trim(sizeStr) <> "" Then
			Return "[" & sizeStr & "]"
		Else
			Return ""
		End If
	End Function
	Public Function NullToString(ByVal Value As Object) As String
		If Convert.IsDBNull(Value) Then
			Return ""
		Else
			Return Convert.ToString(Value)
		End If
	End Function

	Public Function PrepareItemStr(ByVal rptItem As ReportItem) As String

		Dim itemStr As String = ""
		'8/22/13 Michelle An - separate Item line and Sku line     
		Dim skuStr As String = ""
		Dim SkuPerCol As Integer
		'get the sku count first
		Try
			If rptItem.Size.Contains("|") Then
				'clean up the extra "|"
				If rptItem.Size.EndsWith("|") Then
					rptItem.Size = rptItem.Size.Remove(rptItem.Size.Length - 1, 1)
				End If
				Dim multiSku() As String
				multiSku = rptItem.Size.Split("|")
				rptItem.SkuT = multiSku
				rptItem.Count = rptItem.SkuT.Length
			Else
				Dim singleSku(0) As String
				singleSku(0) = rptItem.Color & ": " & rptItem.Size
				rptItem.SkuT = singleSku
				rptItem.Count = 1
			End If

			itemStr = "<tr><td width='30'></td>" & _
			   "<td  width='50' valign='top'><STRONG>" & rptItem.ItemCd & "</STRONG></td>" & _
			   "<td   valign='top' colspan='2'>" & rptItem.ItemDesc & "</td>" & _
			   "<td align='right' width='40' valign='top'>" & FormatNumber(rptItem.CurrentPrice, 2) & "</td>" & _
			   "<td align='right' width='40' valign='top'>" & FormatNumber(rptItem.NewPrice, 2) & "</td>" & _
			   "<td align='right' width='30' valign='top'>" & rptItem.Qty & "</td>" & _
			   "<td align='right' width='40' valign='top'>______</td>" & _
			   "<td align='right' width='40' valign='top'>______</td>" & _
			   "</tr>"
			'parse img url
			If rptItem.ItemCd <> "" Then
				rptItem.ImgLink = ConfigurationSettings.AppSettings("ImgURL").Replace("xxxxxx", rptItem.ItemCd)
				' rptItem.ImgLink = CheckURL(rptItem.ImgLink)
			End If
			'Sku line



			itemStr += "<tr><td width='30'></td>" & _
					  "<td  width='50' valign='top' rowspan='1'><img src='" & rptItem.ImgLink & "' ALT='IMG' onerror='imgError(this);' ></STRONG></td>" & _
					  "<td colspan='2' width='350' valign='top'>"

			'Sku Table - depends on how many skus in that item split into 3 
			skuStr = "<table align = 'left' width='100%' valign='top' cellspacing=0 cellpadding=0 border=0><tr><td valign='top'>"

			Dim i As Integer
			Dim s As Integer
			Dim NoOfTable As Integer
			'Dim upperLimit As Integer
			If (rptItem.Count > 10) Then
				SkuPerCol = Integer.Parse(Math.Ceiling(rptItem.Count / 3))
				NoOfTable = 2
			ElseIf (rptItem.Count < 10 And rptItem.Count > 5) Then
				SkuPerCol = 5
				NoOfTable = 1
			Else
				SkuPerCol = rptItem.Count
				NoOfTable = 0
			End If

			For s = 0 To NoOfTable
				skuStr += "&nbsp;<table align = 'left' valign='top' cellspacing=0 cellpadding=0 border=0>"
				If s = 0 Then
					For i = 0 To SkuPerCol - 1
						skuStr += "<tr><td valign='top' >" & rptItem.SkuT(i) & "</td></tr>"
					Next
				ElseIf s = 1 Then
					For i = SkuPerCol To SkuPerCol * 2 - 1
						If (rptItem.SkuT.Length > SkuPerCol * 2 - 1) Then
							skuStr += "<tr><td valign='top' >" & rptItem.SkuT(i) & "</td></tr>"
						End If
					Next
				Else
					For i = SkuPerCol * 2 To SkuPerCol * 3 - 1
						If (rptItem.SkuT.Length > SkuPerCol * 3 - 1) Then
							skuStr += "<tr><td valign='top'>" & rptItem.SkuT(i) & "</td></tr>"
						End If
					Next
				End If

				skuStr += "</table>"
				'upperLimit = rptItem.Count - (SkuPerCol * (s + 1))
				i = 0
			Next

			skuStr += "</td></tr></table>"
			itemStr += skuStr & "</td><td align='right' width='40' valign='top'>&nbsp;</td>" & _
					"<td align='right' width='40' valign='top'>&nbsp;</td>" & _
					"<td align='right' width='30' valign='top'>&nbsp;</td><td></td><td></td></tr>"
			'"<td align='right' width='40' valign='top'>______</td>" & _
			'"<td align='right' width='40' valign='top'>______</td></tr>"
		Catch Ex As Exception
			Throw Ex
		End Try
		Return itemStr
	End Function

End Class