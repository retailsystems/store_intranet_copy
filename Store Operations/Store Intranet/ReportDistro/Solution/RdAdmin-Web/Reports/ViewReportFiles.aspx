<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ViewReportFiles.aspx.vb" Inherits="ReportDistroAdmin.ViewReportFiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Report Distribution System</title>
		<LINK href="../include/Styles.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script>
		
		</script>
		<LINK href="../include/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout" bgcolor="#000000" vlink="white" alink="white" link="white" onload="self.focus()">
		<form id="Form1" method="post" runat="server">
			<table width="490" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center">
						<table>
							<tr>
								<td colspan="3" align="middle" class="pageCaption">Report Files</td>
							</tr>
							<tr>
								<td colspan="3" height="10">
									<asp:Label ID="lblExMsg" Runat="server" CssClass="errStyle"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">Report Title</td>
								<td width="10" class="reqStyle"></td>
								<td class="cellvaluecaption">
									<asp:Label id="lblTitle" Runat="server" CssClass="cellvaluecaption"></asp:Label>
								</td>
							</tr>
							<tr height="5">
								<td colspan="3"></td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">Fiscal Week</td>
								<td width="10" class="reqStyle"></td>
								<td class="cellvaluecaption"><asp:Label ID="lblfiscalWeek" runat="server"></asp:Label>
								</td>
							</tr>
							<tr height="5">
								<td colspan="3"></td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">No. Of Files</td>
								<td width="10" class="reqStyle"></td>
								<td class="cellvaluecaption"><asp:Label ID="lblCnt" runat="server"></asp:Label>
								</td>
							</tr>
							<tr height="5">
								<td colspan="3"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<asp:DataGrid ID="dgReport" CellPadding="2" AllowPaging="True" PageSize="20" Width="200" Runat="server" BorderColor="#990000" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false" PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
							<Columns>
								<asp:HyperLinkColumn ItemStyle-CssClass="dl" DataNavigateUrlField="FilePath" DataTextField="FileName" DataNavigateUrlFormatString="ViewReportFile.aspx?FilePath={0}" Target="_blank" HeaderText="Report File"></asp:HyperLinkColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
				<tr height="10">
					<td></td>
				</tr>
				<tr>
					<td align="middle">
						<input type="button" id="btnClose" name="btnClose" class="B1" value="Close" onclick="self.close()">
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
