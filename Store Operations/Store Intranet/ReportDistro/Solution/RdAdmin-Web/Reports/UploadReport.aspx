<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UploadReport.aspx.vb" Inherits="ReportDistroAdmin.UploadReport"%>
<HTML>
	<body>
		<!--#include file="../include/header.inc"-->
		<!--#include file="../include/calender.js"--><iframe id="CalFrame" style="DISPLAY: none; Z-INDEX: 100; WIDTH: 148px; POSITION: absolute; HEIGHT: 194px" marginWidth="0" marginHeight="0" src="../include/calendar/calendar.htm" frameBorder="0" noResize scrolling="no"></iframe>
		<form id="Form1" method="post" encType="multipart/form-data" runat="server">
			<INPUT id="hidLocationPath" type="hidden" name="hidLocationPath" runat="server">
			<INPUT id="hidStagingPath" type="hidden" name="hidStagingPath" runat="server"> <INPUT id="hidStatus" type="hidden" name="Hidden1" runat="server">
			<br>
			<table id="Table1" cellSpacing="0" cellPadding="0" width="550" border="0">
				<tr>
					<td class="pageCaption" align="middle" colSpan="3">Upload Report</td>
				</tr>
				<tr height="15">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td colSpan="3"><asp:label id="lblExMsg" runat="server" CssClass="ErrStyle"></asp:label></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="95">Report Title</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft" width="445"><asp:dropdownlist id="lstRptTitle" AutoPostBack="True" CssClass="cellvalueleft" Width="275px" Runat="server"></asp:dropdownlist>&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" CssClass="ErrStyle" ErrorMessage="Select a report title" Display="Dynamic" ControlToValidate="lstRptTitle"></asp:requiredfieldvalidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="95">Fiscal Week</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft"><asp:textbox id="txtReportDate" CssClass="cellvalueleft" Width="100px" Runat="server"></asp:textbox>&nbsp;<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar1,document.Form1.txtReportDate,null,0,330)"><IMG id="calendar1" src="../include/calendar/calendar.gif" align="absBottom" border="0" name="calendar1"></A>&nbsp;
						<asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" CssClass="ErrStyle" ErrorMessage="Report date is required." Display="Dynamic" ControlToValidate="txtReportDate"></asp:requiredfieldvalidator><asp:comparevalidator id="CompareValidator1" runat="server" CssClass="ErrStyle" ErrorMessage="Invalid Date." Display="Dynamic" ControlToValidate="txtReportDate" Operator="DataTypeCheck" Type="Date"></asp:comparevalidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="95">Report File</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft"><input id="rptFile" type="file" size="27" name="rptFile" runat="server">
						<asp:requiredfieldvalidator id="Requiredfieldvalidator5" runat="server" CssClass="ErrStyle" ErrorMessage="Report File is required." Display="Dynamic" ControlToValidate="rptFile"></asp:requiredfieldvalidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="95">Effectivity Date</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft"><asp:textbox id="txtEffectiveDt" CssClass="cellvalueleft" Width="100px" Runat="server"></asp:textbox>&nbsp;<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar2,document.Form1.txtEffectiveDt,null,0,330)"><IMG id="calendar2" src="../include/calendar/calendar.gif" align="absBottom" border="0" name="calendar2"></A>
						<asp:DropDownList ID="lstEffectiveTime" Runat="server" CssClass="cellvalueleft">
							<asp:ListItem Selected="True">12:00 AM</asp:ListItem>
							<asp:ListItem>01:00 AM</asp:ListItem>
							<asp:ListItem>02:00 AM</asp:ListItem>
							<asp:ListItem>03:00 AM</asp:ListItem>
							<asp:ListItem>04:00 AM</asp:ListItem>
							<asp:ListItem>05:00 AM</asp:ListItem>
							<asp:ListItem>06:00 AM</asp:ListItem>
							<asp:ListItem>07:00 AM</asp:ListItem>
							<asp:ListItem>08:00 AM</asp:ListItem>
							<asp:ListItem>09:00 AM</asp:ListItem>
							<asp:ListItem>10:00 AM</asp:ListItem>
							<asp:ListItem>11:00 AM</asp:ListItem>
							<asp:ListItem>12:00 PM</asp:ListItem>
							<asp:ListItem>01:00 PM</asp:ListItem>
							<asp:ListItem>02:00 PM</asp:ListItem>
							<asp:ListItem>03:00 PM</asp:ListItem>
							<asp:ListItem>04:00 PM</asp:ListItem>
							<asp:ListItem>05:00 PM</asp:ListItem>
							<asp:ListItem>06:00 PM</asp:ListItem>
							<asp:ListItem>07:00 PM</asp:ListItem>
							<asp:ListItem>08:00 PM</asp:ListItem>
							<asp:ListItem>09:00 PM</asp:ListItem>
							<asp:ListItem>10:00 PM</asp:ListItem>
							<asp:ListItem>11:00 PM</asp:ListItem>
						</asp:DropDownList>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" CssClass="ErrStyle" ErrorMessage="Effective date is required." Display="Dynamic" ControlToValidate="txtEffectiveDt"></asp:requiredfieldvalidator><asp:comparevalidator id="Comparevalidator2" runat="server" CssClass="ErrStyle" ErrorMessage="Invalid date format." Display="Dynamic" ControlToValidate="txtEffectiveDt" Operator="DataTypeCheck" Type="Date"></asp:comparevalidator>
						<asp:CompareValidator id="CompareValidator5" runat="server" CssClass="ErrStyle" ControlToValidate="txtEffectiveDt" Display="Dynamic" ErrorMessage="Effectivity date should not be less than Fiscal Week Date." Type="Date" Operator="GreaterThanEqual" ControlToCompare="txtReportDate"></asp:CompareValidator></td>
						</td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td align="left" colSpan="3">
						<DIV id="divExp" style="DISPLAY: none" runat="server">
							<TABLE cellSpacing="0" cellPadding="0" align="left" border="0">
								<TR>
									<td class="cellvaluecaption" width="95">Expires On</td>
									<td class="reqStyle" align="left" width="10">*</td>
									<td class="cellvalueleft" align="left"><asp:textbox id="txtExpiryDt" CssClass="cellvalueleft" Width="100px" Runat="server"></asp:textbox>&nbsp;<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar3,document.Form1.txtExpiryDt,null,0,330)"><IMG id="calendar3" src="../include/calendar/calendar.gif" align="absBottom" border="0" name="calendar3"></A>
										<asp:DropDownList ID="lstExpiryTime" Runat="server" CssClass="cellvalueleft">
											<asp:ListItem Selected="True">12:00 AM</asp:ListItem>
											<asp:ListItem>01:00 AM</asp:ListItem>
											<asp:ListItem>02:00 AM</asp:ListItem>
											<asp:ListItem>03:00 AM</asp:ListItem>
											<asp:ListItem>04:00 AM</asp:ListItem>
											<asp:ListItem>05:00 AM</asp:ListItem>
											<asp:ListItem>06:00 AM</asp:ListItem>
											<asp:ListItem>07:00 AM</asp:ListItem>
											<asp:ListItem>08:00 AM</asp:ListItem>
											<asp:ListItem>09:00 AM</asp:ListItem>
											<asp:ListItem>10:00 AM</asp:ListItem>
											<asp:ListItem>11:00 AM</asp:ListItem>
											<asp:ListItem>12:00 PM</asp:ListItem>
											<asp:ListItem>01:00 PM</asp:ListItem>
											<asp:ListItem>02:00 PM</asp:ListItem>
											<asp:ListItem>03:00 PM</asp:ListItem>
											<asp:ListItem>04:00 PM</asp:ListItem>
											<asp:ListItem>05:00 PM</asp:ListItem>
											<asp:ListItem>06:00 PM</asp:ListItem>
											<asp:ListItem>07:00 PM</asp:ListItem>
											<asp:ListItem>08:00 PM</asp:ListItem>
											<asp:ListItem>09:00 PM</asp:ListItem>
											<asp:ListItem>10:00 PM</asp:ListItem>
											<asp:ListItem>11:00 PM</asp:ListItem>
										</asp:DropDownList>
										<asp:requiredfieldvalidator id="Requiredfieldvalidator4" runat="server" CssClass="ErrStyle" ErrorMessage="Expiry date is required." Display="Dynamic" ControlToValidate="txtExpiryDt" Enabled="False"></asp:requiredfieldvalidator><asp:comparevalidator id="Comparevalidator3" runat="server" CssClass="ErrStyle" ErrorMessage="Invalid date format or past date disallowed." Display="Dynamic" ControlToValidate="txtExpiryDt" Operator="GreaterThanEqual" Type="Date" Enabled="False"></asp:comparevalidator>&nbsp;
										<asp:comparevalidator id="CompareValidator4" runat="server" CssClass="ErrStyle" ErrorMessage="Expiry date should be greater than Effective date." Display="Dynamic" ControlToValidate="txtExpiryDt" Operator="GreaterThan" Type="Date" Enabled="False" ControlToCompare="txtEffectiveDt"></asp:comparevalidator></td>
								</TR>
							</TABLE>
						</DIV>
					</td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" vAlign="top" width="95">Comments</td>
					<td width="10"></td>
					<td class="cellvalueleft" width="445"><asp:textbox id="txtComment" CssClass="cellvalueleft" Runat="server" TextMode="MultiLine" Rows="4" Columns="40"></asp:textbox></td>
				</tr>
			</table>
			</TD>
			<tr>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td align="right"><asp:button id="btnSubmit" CssClass="B1" Runat="server" Text="Submit"></asp:button>&nbsp;&nbsp; 
					<!--#include file="../include/homeLink.inc"--></td>
			</tr>
			</TABLE>
		</form>
	</body>
</HTML>
