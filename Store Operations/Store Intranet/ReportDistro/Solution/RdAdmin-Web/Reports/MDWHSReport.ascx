<%@ Control Language="vb" AutoEventWireup="false" Codebehind="MDWHSReport.ascx.vb" Inherits="ReportDistroAdmin.MDWHSReport" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script language="vb" runat="server">
Public Function NullToString(ByVal Value As Object) As String
    If Convert.IsDBNull(Value) Then
        Return ""
    Else
        Return Convert.ToString(Value)
    End If
End Function
</script>
<style> BODY { margin: 5px; }
	TH { font-weight: bold; font-size: 11px; font-family: 'Arial Narrow' ,Arial, Verdana; }
	TD { font-size: 11px; font-family: 'Arial Narrow' ,Arial, Verdana; }
	.PageTitle { font-weight: bold; font-size: 15pt; font-family: Arial, Verdana; }
	.Caption { font-weight: bold; font-size: 12pt; font-family: Arial, Verdana; }
	.StrongText { font-weight: bold; font-size: 13px; font-family: 'Arial Narrow' , Arial, Verdana; }
	.CellValueStrong { font-weight: bold; font-size: 11px; font-family: 'Arial Narrow' , Arial, Verdana; }
</style>
<%IF loadCtrl THEN
	IF DV.Count > 0 THEN
		Dim i as integer = 0
		Dim deptCd as integer = 0
		Dim classCd as integer = 0
		Dim locTot as integer = 0
		Dim grandTot as integer = 0
		Dim locCode as string = ""
		Dim locType as string = ""
		Dim footerFlag as boolean = false
		Dim l
		For  i = 0  to DV.Count - 1
			dr = DV.Item(i).Row
			IF locType <> dr("loc_tp") THEN				
				if i > 0 then
				'show footer
				%>
<tr>
	<td colspan="7"></td>
	<td class="CellValueStrong" colspan="2" align="right"><%=locCode%></td>
	<td class="CellValueStrong" align="right">Total:</td>
	<td class="CellValueStrong" style="BORDER-TOP: #000000 1px solid" align="right"><%=locTot%></td>
	<td colspan="2"></td>
</tr>
<tr>
	<td colspan="7"></td>
	<td class="CellValueStrong" colspan="2" align="right"><%=locType%></td>
	<td class="CellValueStrong" align="right">Total:</td>
	<td class="CellValueStrong" style="BORDER-TOP: #000000 1px solid" align="right"><%=grandTot%></td>
	<td colspan="2"></td>
</tr>
</TABLE></TD></TR></TABLE>
<P id="Paragraph" style="PAGE-BREAK-BEFORE: always; TEXT-ALIGN: left"><br>
</P>
<%
				end if
				locType = dr("loc_tp")
				locCode = ""
				locTot = 0
				grandTot = 0
				classCd = 0
				deptCd = 0
					%>
<table cellSpacing="0" cellPadding="0" width="670" border="0">
	<tr>
		<td align="middle">
			<table width="100%">
				<tr>
					<td>Issue Date:
						<%=rptHeader.IssueDate%>
					</td>
					<td class="PageTitle" align="middle"><b><%=rptHeader.CompanyName%></b></td>
					<td class="StrongText">STORE:&nbsp;<%=rptHeader.StoreNum%>&nbsp;<%=rptHeader.StoreName%><!--9999 9999 Warehouse-->
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="StrongText" align="middle">Price Change Notification</td>
					<td class="StrongText">Location Type:&nbsp;<%=locType%></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="middle">
						<table style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid" cellPadding="0" width="400" cellspacin="0">
							<tr>
								<td>EFFECTIVE DATE:<%=rptHeader.EffectiveDate%></td>
								<td>CANCEL DATE:<%=rptHeader.CancelDate%></td>
							</tr>
						</table>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="Caption" align="middle"><%=rptHeader.rptCaption%><br>
		</td>
	</tr>
	<tr>
		<td align="middle" height="5"></td>
	</tr>
	<tr>
		<td align="middle">
			<table width="80%" border="0">
				<tr>
					<td class="StrongText"><%=rptHeader.SplInstruction%></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="middle" height="5"></td>
	</tr>
	<tr>
		<td align="middle" width="100%">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<th align="left" width="30">
						DEPT</th>
					<th align="left" width="40">
						VENDOR #</th>
					<th align="left" width="130">
						VENDOR NAME</th>
					<th align="left" width="50">
						ITEM</th>
					<th align="left" width="130">
						DESCRIPTION</th>
					<th align="left" width="25">
						SIZE</th>
					<th align="left" width="25">
						PC TYPE</th>
					<th align="left" width="60">
						NEW SKU</th>
					<th align="middle" width="40">
						CURR. PRICE</th>
					<th align="middle" width="40">
						NEW PRICE</th>
					<th align="middle" width="30">
						OH QTY</th>
					<th align="middle" width="35">
						STORE COUNT</th>
					<th align="middle" width="35">
						MGR INIT</th></tr>
				<tr>
					<td colSpan="13">
						<hr style="COLOR: black; HEIGHT: 2px">
					</td>
				</tr>
				<tr>
					<td class="StrongText" colSpan="13">Location Type: &nbsp;<%=locType%>
					</td>
				</tr>
				<% END IF 'end if loc type
						IF locCode <> dr("LOC_CD") THEN
							IF locCode <> "" THEN
							%>
				<tr>
					<td colSpan="7"></td>
					<td align="right" colSpan="2"><b><%=locCode%></b></td>
					<td align="right"><b>Total:</b></td>
					<td style="BORDER-TOP: #000000 1px solid" align="right"><b><%=locTot%></b></td>
					<td colSpan="2"></td>
				</tr>
				<%								
							END IF	
							locTot = 0
							locCode = dr("LOC_CD")
							classCd = 0
							deptCd = 0
				%>
				<tr>
					<td class="CellValueStrong" colSpan="13">Location Code: &nbsp;<%=locCode%>
					</td>
				</tr>
				<%						
						
						END IF
						IF deptCd <> dr("DEPT_CD") THEN
							deptCd = dr("DEPT_CD")
							classCd = 0
						%>
				<tr>
					<td colSpan="13"><b><%=deptCd.toString.PadLeft(4,"0")%></b></td>
				</tr>
				<%
						END IF
						IF classCd <> dr("CLASS_CD") THEN							
							classCd = dr("CLASS_CD")
						%>
				<tr>
					<td colSpan="13"><b>CLASS:&nbsp;<%=classCd.toString.PadLeft(4,"0")%>&nbsp;<%=dr("CLASS_DESC")%></b></td>
				</tr>
				<%
						END IF
				%>
				<tr>
					<td></td>
					<td><%=dr("VE_CD")%></td>
					<td><%=dr("VE_NAME")%></td>
					<td><%=dr("ITM_CD")%></td>
					<td><%=dr("ITEM_DESC")%></td>
					<td><%=NullToString(dr("SIZE_CD"))%></td>
					<td><%=dr("PC_TP")%></td>
					<td><b><%=dr("SKU_NUM")%></b></td>
					<td align="right"><%=FormatNumber(dr("PREV_PERM_RET"),2)%></td>
					<td align="right"><%=FormatNumber(dr("RET_PRC"),2)%></td>
					<td align="right"><%=dr("OH_QTY")%></td>
					<td align="right">______</td>
					<td align="right">______</td>
				</tr>
				<%	
					locTot += dr("OH_QTY")
					grandTot += dr("OH_QTY")	
		
		Next
		'IF i = DV.Count - 1 THEN
								%>
				<tr>
					<td colSpan="7"></td>
					<td class="CellValueStrong" align="right" colSpan="2"><%=locCode%></td>
					<td class="CellValueStrong" align="right">Total:</td>
					<td class="CellValueStrong" style="BORDER-TOP: #000000 1px solid" align="right"><%=locTot%></td>
					<td colSpan="2"></td>
				</tr>
				<tr>
					<td colSpan="7"></td>
					<td class="CellValueStrong" align="right" colSpan="2"><%=locType%></td>
					<td class="CellValueStrong" align="right">Total:</td>
					<td class="CellValueStrong" style="BORDER-TOP: #000000 1px solid" align="right"><%=grandTot%></td>
					<td colSpan="2"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%				
				grandTot = 0
		'END IF
	END IF
END IF
%>
