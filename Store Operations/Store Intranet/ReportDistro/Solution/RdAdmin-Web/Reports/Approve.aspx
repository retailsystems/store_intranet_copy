<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Approve.aspx.vb" Inherits="ReportDistroAdmin.Approve"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Report Distribution System</title>
		<LINK href="../include/Styles.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script>
		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" bgcolor="#000000" onload="self.focus()">
		<form id="Form1" method="post" runat="server">
			<!--#include file="../include/wait.inc"-->
			<table width="340" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="3" align="middle" class="pageCaption">Approve Report</td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:Label ID="lblExMsg" Runat="server" CssClass="errStyle"></asp:Label>
					</td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Report Title</td>
					<td width="10" class="reqStyle"></td>
					<td class="cellvaluecaption">
						<asp:Label id="lblTitle" Runat="server" CssClass="cellvaluecaption"></asp:Label>
					</td>
				</tr>
				<tr height="10">
					<td colspan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Fiscal Week</td>
					<td width="10" class="reqStyle"></td>
					<td class="cellvaluecaption"><asp:Label ID="lblfiscalWeek" runat="server"></asp:Label>
					</td>
				</tr>
				<tr height="10">
					<td colspan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Status</td>
					<td width="10" class="reqStyle">*</td>
					<td class="cellvaluecaption">
						<asp:RadioButtonList ID="rdoStatus" Runat="server" CssClass="cellvaluewhite" RepeatDirection="Horizontal">
							<asp:ListItem Selected="True" Value="2">Approve</asp:ListItem>
							<asp:ListItem Value="3">Decline</asp:ListItem>
						</asp:RadioButtonList>
					</td>
				</tr>
				<tr height="10">
					<td colspan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110" valign="top">Comments</td>
					<td width="10"></td>
					<td class="cellvalueleft">
						<asp:TextBox Runat="server" ID="txtComment" Columns="25" Rows="4" TextMode="MultiLine" CssClass="cellvalueleft"></asp:TextBox>
					</td>
				</tr>
				<tr height="10">
					<td colspan="3"></td>
				</tr>
				<tr>
					<td colspan="3" align="middle">
						<div id="w1" style="display:inline" onclick="showWait()"><asp:Button ID="btnSubmit" cssclass='B1' Runat="server" Text="Submit"></asp:Button></div>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
