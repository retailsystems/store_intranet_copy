Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Partial Class Approve
    Inherits System.Web.UI.Page

    Private _sessionUser As HotTopic.RD.Application.Session
    Private _reportId As Integer
    Private _submissionId As Integer
    Private _submission As Submission

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        Try
            _reportId = Request("rid")
            _submissionId = Request("sid")
            _submission = New Submission(_reportId, _submissionId)
        Catch ex As Exception
            Throw ex
        End Try
        If Not IsPostBack Then
            InitializeForm()
        End If
    End Sub
    Private Sub InitializeForm()
        With _submission
            lblTitle.Text = .Title
            lblfiscalWeek.Text = .FiscalWeekEndingDate
        End With
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim approval As Approval = New Approval(_submissionId, _sessionUser.SessionUser.EmployeeId)
            With approval
                .Comment = txtComment.Text
                .Status = rdoStatus.SelectedItem.Value
            End With
            _sessionUser.ApproveReport(approval, _reportId)
            Dim jScript, alertMsg As String
            alertMsg = "Report Submission Id:" & _submissionId & " Status Changed"
            jScript += "<script language='javascript'>"
            jScript += "window.opener.focus();window.opener.ShowAlert('" & alertMsg & "');"
            jScript += "self.close();"
            jScript += "</script>"
            Response.Write(jScript)
        Catch ex As RDAppException
            lblExMsg.Text = ex.Message
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
