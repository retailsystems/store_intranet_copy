Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Imports System.IO

Partial Class SubmitMDReport
    Inherits System.Web.UI.Page

    Private _sessionUser As HotTopic.RD.Application.Session
    Protected menuStr As String
	Protected WithEvents mdReport As MDReport
	'Protected WithEvents whsReport As MDWHSReport

    Private _mdDs As DataSet
    Private _storeDs As DataSet
    Private _dr As DataRow
    Private _dv As DataView
    Private _dirStaging As DirectoryInfo
    Private _stagingPath, _fileFullPath As String
    Private _splInstruction As String = ""
    Private _rptSubmissionID, _fileCnt As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr
            lblExMsg.Text = ""
            'set 20 min timeout
            Server.ScriptTimeout = 1200000

        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try

        If Not IsPostBack Then
            InitializeForm()
        End If
    End Sub
    Private Sub InitializeForm()
        Dim ds As DataSet = _sessionUser.GetAllReportTitles(FileInputType.MDReport)
        lstRptTitle.DataValueField = "ReportID"
        lstRptTitle.DataTextField = "ReportTitle"
        lstRptTitle.DataSource = ds
        lstRptTitle.DataBind()
        lstRptTitle.Items.Insert(0, New ListItem("---", ""))
        Comparevalidator3.ValueToCompare = Date.Today.ToShortDateString()
        'fill MdReportTypes
        Dim cacheMgr As CacheManager = _sessionUser.GetCacheManager()
        Dim mdReportds As DataSet
        mdReportds = cacheMgr.ListMDReportTypes(ConfigurationSettings.AppSettings("CacheDependency"))
        lstRptType.DataValueField = "MdReportTypeID"
        lstRptType.DataTextField = "MdReportName"
        lstRptType.DataSource = mdReportds
        lstRptType.DataBind()
        lstRptType.Items.Insert(0, New ListItem("---", ""))
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Select Case rdoMdReportScope.SelectedItem.Value
            Case 1
                CreateStoreReports()
            Case 2
                CreateMasterReports()
            Case 3
                CreateWHReports()
        End Select
    End Sub
    Private Sub CreateStoreReports()
        Dim rptSubmission As Submission = New Submission(Convert.ToInt32(lstRptTitle.SelectedItem.Value), True)
        Try
            'validate staging path
            If Not _sessionUser.PathExists(rptSubmission.StagingPath) Then
                lblExMsg.Text = "Staging folder is inaccessible."
                Return
            End If
            If cbSplInstruction.Checked Then
                If txtSplInstruction.Text.Trim.Length > 0 Then
                    _splInstruction = txtSplInstruction.Text.Trim
                Else
                    txtSplInstruction.Text = ""
                    Throw New RDAppException("Spceial instructions can not be blank")
                End If
            End If
            'create MD Report files
            With rptSubmission
                .ReportSubmissionId = 0
                .StagingPath = rptSubmission.StagingPath
                .LocationPath = rptSubmission.LocationPath
                .ReportDate = txtReportDate.Text
                .Comment = lstRptType.SelectedItem.Text
                If rdoReportBy.SelectedItem.Value = 1 Then
                    .MDEffectiveDate = txtMDEffectiveDate.Text
                    ' MOD #2/11/04# add MD effective date in report comments
                    .Comment &= " effective on " & txtMDEffectiveDate.Text & ". "
                    '# disabled. MOD 02/08/2013 RMS change by LJ
                    'ElseIf rdoReportBy.SelectedItem.Value = 2 Then
                    '    .MDPcNum = txtPrcNum.Text
                End If
                .Comment &= txtComment.Text.Trim
                .MDReportType = lstRptType.SelectedItem.Value
                .EffectivityStartDate = txtEffectiveDt.Text & " " & lstEffectiveTime.SelectedItem.Text
                If txtExpiryDt.Text.Trim.Length > 0 Then
                    .EffectivityEndDate = txtExpiryDt.Text & " " & lstExpiryTime.SelectedItem.Text
                End If
                .Status = hidStatus.Value
            End With
            'Dim effectiveDate As String = rptSubmission.MDEffectiveDate.Day & "-" & MonthName(rptSubmission.MDEffectiveDate.Month, True).ToLower & "-" & rptSubmission.MDEffectiveDate.Year
            'lblExMsg.Text = effectiveDate
            'Return
            _sessionUser.GetMDReportDataSet(rptSubmission, _mdDs, _storeDs)

            _dv = _mdDs.Tables(0).DefaultView
            _rptSubmissionID = rptSubmission.ReportSubmissionId
            If _storeDs.Tables(0).Rows.Count > 0 Then
                'create folder
                _stagingPath = rptSubmission.StagingPath
                If Not _stagingPath.EndsWith("\") Then
                    _stagingPath &= "\"
                End If
                _stagingPath = _stagingPath & rptSubmission.ReportSubmissionId & "\"
                rptSubmission.StagingPath = _stagingPath
                _dirStaging = New DirectoryInfo(_stagingPath)
                If Not _dirStaging.Exists Then
                    _dirStaging.Create()
                End If
                ' _dirStaging.
                For Each _dr In _storeDs.Tables(0).Rows
                    '_mdDs.Tables(0).Rows
                    ' _dv.RowFilter = "STORE_CD='" & _dr(" toreNum").ToString.PadLeft(4, "0") & "'"
                    _dv.RowFilter = "STORE_CD=" & _dr("StoreNum")
                    If _dv.Count > 0 Then
                        'Response.Write(_dr("StoreNum").ToString.PadLeft(4, "0") & "-" & dv.Count & "-" & dv.Table.Rows.Count)
                        mdReport = Page.LoadControl("MdReport.ascx")
                        Dim strW As StreamWriter
                        Dim dataGridHTML As String
                        Dim SB As New System.Text.StringBuilder()
                        Dim SW As New StringWriter(SB)
                        Dim htmlTW As New HtmlTextWriter(SW)
                        Dim reportHeader As MDReport.ReportHeader
                        With reportHeader
                            .CompanyName = _dr("CompanyName")
                            If rdoReportBy.SelectedItem.Value = 1 Then
                                .EffectiveDate = txtMDEffectiveDate.Text
                            Else
                                .EffectiveDate = _dv.Item(0).Row.Item("BEG_DT")
                            End If
                            .IssueDate = Now.ToShortDateString
                            .RptCaption = GetReportCaption(lstRptType.SelectedItem.Value, _splInstruction.Length)
                            .ReportType = lstRptType.SelectedItem.Value
                            .StoreNum = _dr("StoreNum").ToString.PadLeft(4, "0")
							.StoreName = _dr("StoreName")
							If _splInstruction.Length > 0 Then
								.SplInstruction = "SPECIAL INSTRUCTIONS: " + _splInstruction
							End If
							.CancelDate = ""
						End With
                        mdReport.rptHeader = reportHeader
                        mdReport.DV = _dv
                        mdReport.BindData()
                        mdReport.RenderControl(htmlTW)
                        dataGridHTML = SB.ToString()
                        strW = System.IO.File.CreateText(_dirStaging.FullName & rptSubmission.DefaultFileName & _dr("StoreNum").ToString.PadLeft(4, "0") & "_" & _rptSubmissionID & ".htm")
                        strW.WriteLine(dataGridHTML)
                        strW.Close()
                        htmlTW.Close()
                        SW.Close()
                        _fileCnt += 1
                        mdReport.Dispose()
                    End If
                    _dv.RowFilter = ""
                Next
                If _fileCnt > 0 Then
                    _sessionUser.SubmitReport(rptSubmission)
                    'go to lists
                    Response.Redirect("ListReportSubmissions.aspx")
                Else
                    _sessionUser.DeleteReportSubmissionById(rptSubmission.ReportSubmissionId)
                    lblExMsg.Text = "No records found"
                    Return
                End If
            End If
        Catch ex As RDAppException
            lblExMsg.Text = ex.Message
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub CreateMasterReports()
        'Master Reports
        Try
            Dim rptSubmission As Submission = New Submission(Convert.ToInt32(lstRptTitle.SelectedItem.Value), True)
            Dim fileName As String
            _stagingPath = Application("MDMasterFilePath")

            If Not _stagingPath.EndsWith("\") Then
                _stagingPath &= "\"
            End If
            'validate staging path
            If Not PathExists(_stagingPath) Then
                lblExMsg.Text = "Report folder is inaccessible."
                Return
            End If
            'create MD Report files

            With rptSubmission
                .ReportSubmissionId = 0
                If rdoReportBy.SelectedItem.Value = 1 Then
                    .MDEffectiveDate = txtMDEffectiveDate.Text
                    '# RMS change 02/08/2013 by LJ
                    'ElseIf rdoReportBy.SelectedItem.Value = 2 Then
                    '    .MDPcNum = txtPrcNum.Text
                End If
                .MDReportType = lstRptType.SelectedItem.Value
            End With
          
            Dim cOwner As CompanyOwner
            For Each cOwner In rptSubmission.CompanyOwners
                _sessionUser.GetMDMasterReportDataSet(rptSubmission, _mdDs, cOwner.CompanyId)
                _dv = _mdDs.Tables(0).DefaultView
                fileName = cOwner.CompanyName & "_Master_" & CreateMasterReportFileName(rptSubmission)
                _fileFullPath = _stagingPath & fileName & ".htm"
                If _dv.Count > 0 Then
                    'Response.Write(_dr("StoreNum").ToString.PadLeft(4, "0") & "-" & dv.Count & "-" & dv.Table.Rows.Count)
                    Dim mdReport As MDMasterReport
                    mdReport = Page.LoadControl("MdMasterReport.ascx")
                    Dim strW As StreamWriter
                    Dim dataGridHTML As String
                    Dim SB As New System.Text.StringBuilder()
                    Dim SW As New StringWriter(SB)
                    Dim htmlTW As New HtmlTextWriter(SW)
                    Dim reportHeader As MDMasterReport.ReportHeader
                    With reportHeader
                        .CompanyName = cOwner.CompanyName.ToUpper
                        .RptCaption = lstRptType.SelectedItem.Text
                        .SplInstruction = txtSplInstruction.Text
                        .CancelDate = ""
                    End With
                    mdReport.rptHeader = reportHeader
                    mdReport.DV = _dv
                    mdReport.BindData()
                    mdReport.RenderControl(htmlTW)
                    dataGridHTML = SB.ToString()
                    strW = System.IO.File.CreateText(_fileFullPath)
                    strW.WriteLine(dataGridHTML)
                    strW.Close()
                    htmlTW.Close()
                    SW.Close()
                    lblExMsg.Text &= "<font class=stepStyleOn>Report file <a href='" & _fileFullPath & "' target=_blank >" & _fileFullPath & "</a> creation was successfull.</font><br>"
                End If
            Next
        Catch ex As RDAppException
            lblExMsg.Text &= ex.Message & "<br>"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub CreateWHReports()
        'Warehouse Reports
        Try
            Dim rptSubmission As Submission = New Submission(Convert.ToInt32(lstRptTitle.SelectedItem.Value), True)
            Dim fileName As String

            If cbSplInstruction.Checked Then
                If txtSplInstruction.Text.Trim.Length > 0 Then
                    _splInstruction = txtSplInstruction.Text.Trim
                Else
                    txtSplInstruction.Text = ""
                    Throw New RDAppException("Special instructions can not be blank")
                End If
            End If
            _stagingPath = Application("MDWHSFilePath")

            If Not _stagingPath.EndsWith("\") Then
                _stagingPath &= "\"
            End If
            'validate staging path
            If Not PathExists(_stagingPath) Then
                lblExMsg.Text = "Report folder is inaccessible."
                Return
            End If
            'create MD Report files

            With rptSubmission
                .ReportSubmissionId = 0
                If rdoReportBy.SelectedItem.Value = 1 Then
                    .MDEffectiveDate = txtMDEffectiveDate.Text
                    '# RMS change 02/08/2013 by LJ
                    'ElseIf rdoReportBy.SelectedItem.Value = 2 Then
                    '    .MDPcNum = txtPrcNum.Text
                End If
                .MDReportType = lstRptType.SelectedItem.Value
            End With

            Dim cOwner As CompanyOwner
            For Each cOwner In rptSubmission.CompanyOwners

                _sessionUser.GetMDWHReportDataSet(rptSubmission, _mdDs, cOwner.CompanyId, rdoMDWarehouse.SelectedItem.Value)
                _dv = _mdDs.Tables(0).DefaultView
                '_dv.RowFilter = "STORE_CD=" & rdoMDWarehouse.SelectedItem.Value
                fileName = cOwner.CompanyName & "_WHS_" & rdoMDWarehouse.SelectedItem.Value & "_" & CreateMasterReportFileName(rptSubmission)
                _fileFullPath = _stagingPath & fileName & ".htm"
                If _dv.Count > 0 Then
                    Dim mdReport As MDWHSReport
                    mdReport = Page.LoadControl("MdWHSReport.ascx")
                    Dim strW As StreamWriter
                    Dim dataGridHTML As String
                    Dim SB As New System.Text.StringBuilder()
                    Dim SW As New StringWriter(SB)
                    Dim htmlTW As New HtmlTextWriter(SW)
                    Dim reportHeader As MDWHSReport.ReportHeader

                    With reportHeader
                        .CompanyName = cOwner.CompanyName.ToUpper
                        If rdoReportBy.SelectedItem.Value = 1 Then
                            .EffectiveDate = txtMDEffectiveDate.Text
                        Else
                            .EffectiveDate = _dv.Item(0).Row.Item("BEG_DT")
                        End If
                        .StoreNum = rdoMDWarehouse.SelectedItem.Value '_dr("StoreNum").ToString.PadLeft(4, "0")
                        .StoreName = rdoMDWarehouse.SelectedItem.Value & " Warehouse" '_dr("StoreName")
                        .IssueDate = Now.ToShortDateString
                        .RptCaption = GetReportCaption(lstRptType.SelectedItem.Value, _splInstruction.Length)
                        .SplInstruction = _splInstruction
                        .CancelDate = ""
                    End With
                    mdReport.loadCtrl = True
                    mdReport.rptHeader = reportHeader
                    mdReport.DV = _dv
                    mdReport.BindData()
                    mdReport.RenderControl(htmlTW)
                    dataGridHTML = SB.ToString()
                    strW = System.IO.File.CreateText(_fileFullPath)
                    strW.WriteLine(dataGridHTML)
                    strW.Close()
                    htmlTW.Close()
                    SW.Close()
                    lblExMsg.Text &= "<font class=stepStyleOn>Report file <a href='" & _fileFullPath & "' target=_blank>" & _fileFullPath & "</a> creation was successfull.</font><br>"
                    'Dim reportHeader As MDWHSReport.ReportHeader
                    'With reportHeader
                    '    .CompanyName = "Hot Topic"
                    '    If rdoReportBy.SelectedItem.Value = 1 Then
                    '        .EffectiveDate = txtMDEffectiveDate.Text
                    '    Else
                    '        .EffectiveDate = _dv.Item(0).Row.Item("BEG_DT")
                    '    End If
                    '    .IssueDate = Now.ToShortDateString
                    '    .RptCaption = GetReportCaption(lstRptType.SelectedItem.Value, txtSplInstruction.Text.Trim.Length)
                    '    .SplInstruction = txtSplInstruction.Text
                    '    .CancelDate = ""
                    'End With
                    'whsReport.rptHeader = reportHeader
                    'whsReport.DV = _dv
                    'whsReport.loadCtrl = True
                    'whsReport.BindData()
                End If
            Next

        Catch ex As RDAppException
            lblExMsg.Text = ex.Message & "<br>"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function GetReportCaption(ByVal reportType As Int16, Optional ByVal splInstructionStrLen As Int16 = 0) As String
        Dim rptCaption As String
        Select Case reportType
            Case MDReportType.Regular
                rptCaption = "Regular Markdown"
            Case MDReportType.Further
                rptCaption = "Further Markdown"
            Case MDReportType.MarkUp
                rptCaption = "Regular MarkUp"
            Case MDReportType.MDCancel
                rptCaption = "Markdown Cancellation"
            Case MDReportType.MUCancel
                rptCaption = "Markup Cancellation"
            Case MDReportType.MOS
                rptCaption = "Mark Out Of Stock"
            Case MDReportType.MDAll
                rptCaption = "Markdown"
            Case Else
                rptCaption = "Markdown Report"
        End Select
		'If splInstructionStrLen > 0 Then
		'    rptCaption &= "-Special Instruction"
		'End If
        Return rptCaption
    End Function

    Private Sub rdoReportBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdoReportBy.SelectedIndexChanged
        If rdoReportBy.SelectedItem.Value = 1 Then
            pnlEffectiveDt.Visible = True
            pnlPcNum.Visible = False
            'Else
            '    pnlEffectiveDt.Visible = False
            '    pnlPcNum.Visible = False
        End If
    End Sub
    Private Shared Function PathExists(ByVal path As String, Optional ByVal searchPattern As String = "") As Boolean
        Try
            If Not path.EndsWith("\") Then
                path += "\"
            End If
            Dim di As DirectoryInfo = New DirectoryInfo(path)
            If searchPattern = "" Then
                Return di.Exists
            Else
                Return (di.GetFiles(searchPattern).Length > 0)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function CreateMasterReportFileName(ByVal rptSubmission As Submission) As String
        Dim fileName As String
        '# disable some reports for RMS changes. 01/31/2013 by LJ
        Select Case rptSubmission.MDReportType
            Case MDReportType.Regular
                fileName = "MDR"
            Case MDReportType.Further
                fileName = "MDF"
            Case MDReportType.MOS
                fileName = "MOS"
            Case MDReportType.MDAll
                fileName = "MD"
                'Case MDReportType.MarkUp
                '    fileName = "MU"
                'Case MDReportType.MDCancel
                '    fileName = "MDC"
                'Case MDReportType.MUCancel
                '    fileName = "MUC"
        End Select

        If rptSubmission.MDPcNum <> "" AndAlso IsNumeric(rptSubmission.MDPcNum) Then
            fileName &= "_PCN_" & rptSubmission.MDPcNum
        ElseIf IsDate(rptSubmission.MDEffectiveDate) Then
            fileName &= "_" & rptSubmission.MDEffectiveDate.Month.ToString.PadLeft(2, "0") & rptSubmission.MDEffectiveDate.Day.ToString.PadLeft(2, "0") & rptSubmission.MDEffectiveDate.Year
        End If
        Return fileName
    End Function

    Private Sub rdoMdReportScope_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdoMdReportScope.SelectedIndexChanged
        If rdoMdReportScope.SelectedItem.Value = 1 Then
            pnlRptMetaData.Visible = True
            pnlWarehouse.Visible = False
        ElseIf rdoMdReportScope.SelectedItem.Value = 3 Then
            pnlRptMetaData.Visible = False
            pnlWarehouse.Visible = True
        Else
            pnlRptMetaData.Visible = False
            pnlWarehouse.Visible = False
        End If
    End Sub

    Private Sub lstRptType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstRptType.SelectedIndexChanged
        txtSplInstruction.Text = ""
        cbSplInstruction.Checked = False
        If lstRptType.SelectedIndex > 0 Then
            Dim cacheMgr As CacheManager = _sessionUser.GetCacheManager()
            Dim mdReportds As DataSet
            Dim dr As DataRow
            Dim rptTypeId As Int16 = lstRptType.SelectedItem.Value
            mdReportds = cacheMgr.ListMDReportTypes(ConfigurationSettings.AppSettings("CacheDependency"))
            If mdReportds.Tables(0).Rows.Count > 0 Then
                For Each dr In mdReportds.Tables(0).Rows
                    If dr("MdReportTypeID") = rptTypeId Then
                        If Not IsDBNull(dr("SplInstructions")) Then
                            txtSplInstruction.Text = dr("SplInstructions")
                        End If
                        Exit For
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub lstRptTitle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstRptTitle.SelectedIndexChanged

        hidLocationPath.Value = ""
        If lstRptTitle.SelectedIndex > 0 Then
            Dim rpt As Report = _sessionUser.GetReportOnly(Convert.ToInt32(lstRptTitle.SelectedItem.Value))
            hidLocationPath.Value = rpt.LocationPath
            If rpt.ApprovalType = ReportApprovalType.None Then
                hidStatus.Value = ReportStatus.Approved
            Else
                hidStatus.Value = ReportStatus.Pending
            End If
            If rpt.ExpirationType = ExpType.ExpDate Then
                divExp.Style("display") = "inline"
            ElseIf rpt.ExpirationType = ExpType.NthReport Then
                divExp.Style("display") = "none"
            End If
            Requiredfieldvalidator4.Enabled = (rpt.ExpirationType = ExpType.ExpDate)
            Comparevalidator3.Enabled = (rpt.ExpirationType = ExpType.ExpDate)
            CompareValidator4.Enabled = (rpt.ExpirationType = ExpType.ExpDate)
        End If
    End Sub
End Class
