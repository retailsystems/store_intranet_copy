
Public MustInherit Class MDWHSReport
    Inherits System.Web.UI.UserControl

    Public ReportDs As New DataSet()
    Public DV As DataView
    Public rptHeader As ReportHeader
    Protected dr As DataRow
    Public loadCtrl As Boolean = False
    Structure ReportHeader
        Public IssueDate As String
        Public EffectiveDate As String
        Public CancelDate As String
        Public StoreNum As String
        Public StoreName As String
        Public RptCaption As String
        Public SplInstruction As String
        Public CompanyName As String
    End Structure

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

    End Sub
    Public Sub BindData()
    End Sub
End Class
