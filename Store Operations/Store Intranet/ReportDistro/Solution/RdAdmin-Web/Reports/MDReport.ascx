<%@ Control Language="vb" AutoEventWireup="false" Codebehind="MDReport.ascx.vb" Inherits="ReportDistroAdmin.MDReport" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
 <script type="text/javascript">
function imgError(image) {
    image.onerror = "";
    image.src = "http://htintranet/hottopic/RptAdmin/images/wspacer.gif";
    return true;
}
function swapImage(image, imageSrc) {
    image.src = imageSrc;
}
</script>
<style> BODY { margin: 5px; }

    TR { page-break-inside:avoid; page-break-after:auto }
	TH { font-weight: bold; font-size: 10px; font-family: 'Arial Narrow' ,Arial, Verdana; }
	TD { font-size: 10px; font-family: 'Arial Narrow' ,Arial, Verdana;
	page-break-inside:avoid; page-break-after:auto }
	.PageTitle { font-weight: bold; font-size: 14pt; font-family: Arial, Verdana; }
	.Caption { font-weight: bold; font-size: 11pt; font-family: Arial, Verdana; }
	.StrongText { font-weight: bold; font-size: 12px; font-family: 'Arial Narrow' , Arial, Verdana; }
	table.gridtable {
		BORDER-RIGHT: black 1px solid;
		BORDER-left: black 1px solid;
border-collapse:collapse;
page-break-inside:avoid !important;
valign: top;
}
table.td {
border-collapse:collapse;
}

</style>
<table width="670" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="middle">
			<table width="100%">
				<tr>
					<td>Issue Date:
						<%=rptHeader.IssueDate%>
					</td>
					<td align="middle" class="PageTitle"><b><%=rptHeader.CompanyName%></b></td>
					<td class="StrongText">STORE:
						<%=rptHeader.StoreNum%>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="middle" class="StrongText">Price Change Notification</td>
					<td class="StrongText"><%=rptHeader.StoreName%></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="middle">
						<table width="400" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid" cellpadding="0" cellspacin="0">
							<tr>
								<td>EFFECTIVE DATE:<%=rptHeader.EffectiveDate%></td>
								<td>CANCEL DATE:<%=rptHeader.CancelDate%></td>
							</tr>
						</table>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="middle" class="Caption"><%=rptHeader.rptCaption%><br>
		</td>
	</tr>
	<tr>
		<td align="middle" height="5">
		</td>
	</tr>
	<tr>
		<td align="middle">
			<table width="80%" border="0">
				<tr>
					<td class="StrongText"><%=rptHeader.SplInstruction%></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="middle" height="5">
		</td>
	</tr>
	<tr>
		<td align="middle" width="100%">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="gridtable">
				<tr>
					
					<th style="BORDER-RIGHT: black solid 1px;">
				<table align="left" width="100%" cellpadding="1" cellspacing="0"><tr>
					<th width="45" align="left">
						DEPT:<br>STYLE</th>
						<th align="left" width="110">
						CHILD SKU</th>															
					<th width="35" align="right">
						CURR. PRICE</th>
					<th width="35" align="right">
						NEW PRICE</th>
					<th  align="right"  >
						OH QTY</th></tr>
						</table>
						</th>
					<th style="BORDER-RIGHT: black solid 1px;">
						<table align="left" width="100%" cellpadding="1" cellspacing="0"><tr>
					<th width="45" align="left">
						DEPT:<br>STYLE</th>
						<th align="left" width="110">
						CHILD SKU</th>															
					<th width="35" align="right">
						CURR. PRICE</th>
					<th width="35" align="right">
						NEW PRICE</th>
					<th  align="right"  >
						OH QTY</th></tr>
						</table>
						</th>
						<th>
						<table align="left" width="100%" cellpadding="1" cellspacing="0"><tr>
					<th width="45" align="left">
						DEPT:<br>STYLE</th>
						<th align="left" width="110">
						CHILD SKU</th>															
					<th width="35" align="right">
						CURR. PRICE</th>
					<th width="35" align="right">
						NEW PRICE</th>
					<th  align="right"  >
						OH QTY</th></tr>
						</table>
						</th>
				</tr>
				<tr>
					<td colspan="16"><hr style="COLOR: black;HEIGHT: 2px">
					</td>
				</tr>
				<%
				IF DV.Count > 0 THEN
				        Dim i As Integer = 0
				        Dim j As Integer = 0
					
				        Dim classCd As Integer = 0
				        Dim isDept As Boolean = False
				        For i = 0 To DV.Count - 1
				            If i = DV.Count - 1 Then
				                breakPage = False
				            End If
				            dr = DV.Item(i).Row
				          
				            
				            If deptCd <> dr("DEPT_CD") Then
				                deptCd = dr("DEPT_CD")
				             
				                classCd = 0
				                'write item data
				                If (i > 0 AndAlso rptItem.ItemCd <> "") Then
							%>
				<%=PrepareItemStrFinal(rptItem)%>
				<%	
								IntializeItem(rptItem)
							end if
							%>
				
				<%
						END IF
						IF classCd <> dr("CLASS_CD") THEN							
				    classCd = dr("CLASS_CD")
				    'do pos check and line break check, re-pos if possible 
				   
				    %>
				
				    
							<%													
							        If (i > 0 AndAlso rptItem.ItemCd <> "") Then
							%>
				<%=PrepareItemStrFinal(rptItem)%>
				<%	
								IntializeItem(rptItem)
							end if
							%>
							
	
				<%
				    breakLine = True
						END IF
				If (dr("ITEM_CD") <> rptItem.ItemCd) Then
				   
				
				    If (i > 0 AndAlso rptItem.ItemCd <> "") Then
				       
				%>
				<%=PrepareItemStrFinal(rptItem)%>
				<%		
							End IF							
						'fill new item data
							IntializeItem(rptItem)
							With rptItem
							
				    .ItemCd = dr("ITEM_CD")
				    .ItemDesc = dr("ITEM_DESC")
				    .Color = dr("SKU") & "      " & dr("ITEM_COLOR")
				    .Size = dr("SKU") & "      " & dr("ITEM_COLOR") & ": " & NullToString(dr("SIZE_CD")) & "|"
								.CurrentPrice = dr("PREV_PERM_RET")
								.NewPrice = dr("RET_PRC")
				    .Qty = dr("OH_QTY")
				    .Count = DV.Count
							End With
            Else
				
				If NullToString(dr("SIZE_CD")) <> "" Then
				    rptItem.Size &= dr("SKU") & "      " & dr("ITEM_COLOR") & ": " & NullToString(dr("SIZE_CD")) & "|"
				    rptItem.Qty += dr("OH_QTY")
				End If
								
				
            End If
					Next
					'write final item data
				%>
				<%=PrepareItemStrFinal(rptItem)%>
				<%	
						
				END IF%>
			</table>
		</td>
	</tr>
	<!-- BEGIN MOD: 5/5/05 Add DM signature in the report footer-->
	<%IF (rptHeader.reportType = "6" AndAlso rptHeader.CompanyName.toUpper = "HOT TOPIC") OR (rptHeader.CompanyName.toUpper = "TORRID")%>
	<tr>
		<td width="100%">
			<br>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="75%">
						<b>DM Signature<b>:_______________________________</b></b>
					</td>
					<td>
						<b># of boxes<b>:________</b></b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<%END IF%>
	<!-- END MOD: 5/5/05-->
</table>
