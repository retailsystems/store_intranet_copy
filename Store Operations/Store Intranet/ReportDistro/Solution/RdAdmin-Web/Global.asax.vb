Imports System.Web
Imports System.Web.SessionState
Imports HotTopic.RD.Settings
Imports HotTopic.RD.Application
Public Class [Global]
    Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        ' Get all application settings
        Dim settings As New AppSetting()
        With settings
            .ConnectionString = ConfigurationSettings.AppSettings("ConnectionString")
            .SecurityConnectionString = ConfigurationSettings.AppSettings("SecurityConnectionString")
            .CacheDependencyFile = ConfigurationSettings.AppSettings("CacheDependency")
            .SmtpServer = ConfigurationSettings.AppSettings("SmtpServer")
            .SmtpSender = ConfigurationSettings.AppSettings("SmtpSender")
            .ApproverEmailBody = ConfigurationSettings.AppSettings("ApproverEmailBody")
            .DeclinedEmailBody = ConfigurationSettings.AppSettings("DeclinedEmailBody")
            .CancelledEmailBody = ConfigurationSettings.AppSettings("CancelledEmailBody")
            .OracleConnectionString = ConfigurationSettings.AppSettings("OracleConnectionString")
        End With
        Application("AppSetting") = settings
        Application("MDMasterFilePath") = ConfigurationSettings.AppSettings("MDMasterFilePath")
        Application("MDWHSFilePath") = ConfigurationSettings.AppSettings("MDWHSFilePath")

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
        'Application("appUrl") = ConfigurationSettings.AppSettings("APP_URL")
        'If IsNothing(Context.Cache("lookUpDs")) Then
        '    Dim cmObj As HotTopic.RD.Application.CacheManager
        '    cmObj = New HotTopic.RD.Application.CacheManager()
        '    Context.Cache("lookUpDs") = cmObj.ListLookups
        'End If
        'If IsNothing(Session("SessionUser")) Then
        '    Dim sessionObj As Session
        '    sessionObj = New Session(1)
        'End If
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
        'Application("ErrDesc") = Server.GetLastError.ToString
        'Application("ErrSrc") = Server.GetLastError.Source
        'Application("ErrMsg") = Server.GetLastError.Message
        Application("Exp") = Server.GetLastError
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class
