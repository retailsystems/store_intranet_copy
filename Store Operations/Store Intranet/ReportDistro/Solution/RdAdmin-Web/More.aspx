<%@ Page Language="vb" AutoEventWireup="false" Codebehind="More.aspx.vb" Inherits="ReportDistroAdmin.More" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Report Comments</title>
		<LINK href="include/Styles.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body bgcolor="#000000" onload="self.focus()">
		<form id="Form1" method="post" runat="server">
			<font color="white">
				<table width="500" align="center" cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="3" align="middle" class="pageCaption"><u>Report Comments</u></td>
					</tr>
					<tr height="10">
						<td colspan="3"></td>
					</tr>
					<tr>
						<td class="cellvaluecaption" width="110">Report Title</td>
						<td width="10" class="reqStyle"></td>
						<td class="cellvaluecaption">
							<asp:label Runat="server" ID="lblTitle" EnableViewState="False"></asp:label>
						</td>
					</tr>
					<tr height="10">
						<td colspan="3"></td>
					</tr>
					<tr>
						<td class="cellvaluecaption" width="110">Fiscal Week</td>
						<td width="10" class="reqStyle"></td>
						<td class="cellvaluecaption">
							<asp:Label ID="lblFiscalWeek" Runat="server" EnableViewState="False"></asp:Label>
						</td>
					</tr>
					<tr height="10">
						<td colspan="3"></td>
					</tr>
					<tr>
						<td class="cellvaluecaption" width="110">Comments</td>
						<td width="10" class="reqStyle"></td>
						<td class="cellvaluewhite">
							<asp:Label ID="lblComment" Runat="server" EnableViewState="False"></asp:Label>
						</td>
					</tr>
					<tr height="10">
						<td colspan="3"></td>
					</tr>
					<tr>
						<td colspan="3" align="middle">
							<input type="button" name="clr" value="Close" class='B1' onclick="self.close()">
						</td>
					</tr>
				</table>
		</form>
		</FONT>
	</body>
</HTML>
