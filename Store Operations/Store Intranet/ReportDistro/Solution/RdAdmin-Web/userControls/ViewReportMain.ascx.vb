Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity

Partial  Class ViewReportMain
    Inherits System.Web.UI.UserControl
    'Protected WithEvents lblDesc As System.Web.UI.WebControls.Label
    Public report As report
    Private _sessionUser As HotTopic.RD.Application.Session
    Protected reportId As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        If Not IsPostBack Then
            InitializeForm()
        End If

    End Sub
    Private Sub InitializeForm()
        Dim cacheMgr As CacheManager = _sessionUser.GetCacheManager()
        Dim lookUpds As DataSet
        Dim i As Integer = 0
        lookUpds = cacheMgr.ListLookups(ConfigurationSettings.AppSettings("LookupDependency"))
        With report
            reportId = .ReportId
            lblTitle.Text = .Title
            lblReportId.Text = .ReportId
            'lblDesc.Text = .Description
            txtDesc.Text = .Description
            lblInputType.Text = GetLookUpText(lookUpds, "InPutType", .InputType)
            lblScope.Text = GetLookUpText(lookUpds, "ContentScope", .ContentScope)
            lblStagingPath.Text = .StagingPath
            lblLocationPath.Text = .LocationPath
            lblDefaultFile.Text = .DefaultFileName
            Dim id As String = GetLookUpText(lookUpds, "FileIdentifier", .DefaultFileIdentifier)
            If id.Trim().Length = 0 Then
                lblIdentifier.Text = ""
            Else
                lblIdentifier.Text = "&lt;" & id & "&gt;"
            End If
            lblFormat.Text = .FileFormat
            lblMaxFileSize.Text = .MaxFileSize
            lblApproverEmail.Text = .ApproverGroupEmail
            lblApprovalType.Text = GetLookUpText(lookUpds, "ApprovalType", .ApprovalType)
            Dim company As CompanyOwner
            If .CompanyOwners.Count > 1 Then
                company = .CompanyOwners(0)
                If company.CDStoresFlag = True Then
                    rptOwners.Text = GetLookUpText(lookUpds, "Company", StoreOwner.CDStores)
                Else
                    rptOwners.Text = GetLookUpText(lookUpds, "Company", StoreOwner.AllStores)
                End If
            Else
                company = .CompanyOwners(0)
                rptOwners.Text += company.CompanyName
            End If
            'For i = 0 To .CompanyOwners.Count - 1
            '    Dim company As CompanyOwner = .CompanyOwners(i)
            '    'If Len(rptOwners.Text) > 0 Then
            '    '    rptOwners.Text += ","
            '    'End If
            '    'rptOwners.Text += company.CompanyName

            'Next
            chkReadOnly.Checked = .ReadOnlyFlag
            chkAllowMulti.Checked = .AllowMultiFlag
            lblExpiry.Text = GetLookUpText(lookUpds, "ExpirationType", .ExpirationType)
            If .ArchiveNth > 0 And .ExpirationType = ExpType.NthReport Then
                lblArchive.Text = " (Archives after " & .ArchiveNth & " reports.)"
            End If
        End With
    End Sub
    Private Function GetLookUpText(ByVal lookUpds As DataSet, ByVal lookUpType As String, ByVal lookUpVal As String) As String
        Dim lookUpView As DataView = lookUpds.Tables(0).DefaultView
        Dim lookUpText As String = ""
        If Trim(lookUpType) <> "" And IsNumeric(lookUpVal) Then
            lookUpView.RowFilter = "lookUpType='" & lookUpType & "' and lookUpValue=" & lookUpVal
            If lookUpView.Count > 0 Then
                lookUpText = lookUpView.Item(0).Row.Item("lookUptext")
            End If
        End If
        Return lookUpText
    End Function

End Class
