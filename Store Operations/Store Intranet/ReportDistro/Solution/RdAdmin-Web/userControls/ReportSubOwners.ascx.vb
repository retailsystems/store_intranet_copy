Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Partial  Class ReportSubOwners
    Inherits System.Web.UI.UserControl
    Public report As Report
    Private _coSubOwners As ArrayList
    Protected reportId As Integer
    Protected ShowDetails As Boolean = True

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        With report
            _coSubOwners = .CompanySubOwners
            reportId = .ReportId
            If .ContentScope <> ContentScopeType.Company Then
                BindData()
            Else
                ShowDetails = False
            End If
        End With

    End Sub
    Private Sub BindData()
        Dim ds As DataSet = New DataSet()
        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow
        dt.TableName = "Table1"
        dt.Columns.Add("OwnerName")
        dt.Columns.Add("CompanyName")
        Dim co As HotTopic.RD.Entity.SubCompanyOwner

        Dim i As Integer
        For Each co In _coSubOwners
            dr = dt.NewRow
            dr.Item(0) = co.OwnerName
            dr.Item(1) = co.CompanyName
            dt.Rows.Add(dr)
        Next
        ds.Tables.Add(dt)
        dgSubOwners.DataSource = ds.Tables(0)
        dgSubOwners.DataBind()

    End Sub

    Private Sub dgSubOwners_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgSubOwners.PageIndexChanged
        dgSubOwners.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub
End Class
