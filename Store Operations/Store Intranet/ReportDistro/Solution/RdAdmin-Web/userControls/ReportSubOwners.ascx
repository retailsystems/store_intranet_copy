<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ReportSubOwners.ascx.vb" Inherits="ReportDistroAdmin.ReportSubOwners" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%if ShowDetails then%>
<table align="center" width="290" height="100%" border="0" cellpadding="2" cellspacing="0" style="BORDER-RIGHT: crimson 1px solid; BORDER-TOP: crimson 1px solid; BORDER-LEFT: crimson 1px solid; BORDER-BOTTOM: crimson 1px solid">
	<tr height="15">
		<td bgcolor="crimson" class="cellvaluecaption">&nbsp;Restricted Access By Sub 
			Company</td>
		<td bgcolor="crimson" align="right" valign="center">
			<input type="button" name="btnEdit" value="Update" id="btnEdit" class="btnSmall" onclick="location.href='ManageSubCompanyOwners.aspx?reportID=<%=reportId%>'">
		</td>
	</tr>
	<tr height="5">
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2" valign="top">
			<table cellspacing="0" cellpadding="0" bordercolor="#990000" border="0">
				<tr>
					<td width="5"></td>
					<td>
						<asp:DataGrid ID="dgSubOwners" Runat="server" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false">
							<Columns>
								<asp:BoundColumn DataField="OwnerName" HeaderStyle-Width="140" HeaderText="Owner Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="CompanyName" HeaderStyle-Width="140" HeaderText="Company"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr height="5">
		<td colspan="2"></td>
	</tr>
</table>
<%end if%>
