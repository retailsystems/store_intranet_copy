Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Partial  Class ReportJobCodeOwners
    Inherits System.Web.UI.UserControl
    Public report As Report
    Private _codeOwnersArr As ArrayList
    Protected reportId As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        With report
            _codeOwnersArr = .JobCodeOwners
            reportId = .ReportId
        End With
        BindData()
    End Sub
    Private Sub BindData()
        Dim ds As DataSet = New DataSet()
        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow
        dt.TableName = "Table1"
        dt.Columns.Add("CompanyName")
        dt.Columns.Add("JobCode")
        Dim co As HotTopic.RD.Entity.JobCodeOwner

        Dim i As Integer
        For Each co In _codeOwnersArr
            dr = dt.NewRow
            dr.Item(0) = co.CompanyName
            dr.Item(1) = co.JobCode
            dt.Rows.Add(dr)
        Next
        ds.Tables.Add(dt)

        'dgCodeOwners.DataSource = _codeOwnersArr
        dgCodeOwners.DataSource = ds.Tables(0)
        dgCodeOwners.DataBind()
    End Sub
    Public Function GetRole(ByVal roleId As String) As String
        Dim str As String
        Dim li As ListItem
        For Each li In lstUserRole.Items
            If li.Value = roleId Then
                str = li.Text
                Exit For
            End If
        Next
        Return str
    End Function
End Class