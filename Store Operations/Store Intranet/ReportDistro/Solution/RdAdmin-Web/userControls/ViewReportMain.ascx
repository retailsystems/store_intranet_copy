<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ViewReportMain.ascx.vb" Inherits="ReportDistroAdmin.ViewReportMain" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table style="BORDER-RIGHT: crimson 1px solid; BORDER-TOP: crimson 1px solid; BORDER-LEFT: crimson 1px solid; BORDER-BOTTOM: crimson 1px solid" cellSpacing="0" cellPadding="2" width="600" align="center" border="0">
	<tr>
		<td class="cellvaluecaption" bgColor="crimson" colSpan="2">&nbsp;Report Settings</td>
		<td vAlign="center" align="right" bgColor="crimson" colSpan="2"><input class=btnSmall id=btnEdit onclick="window.location='NewReportStep1.aspx?mode=update&amp;ReportId=<%=reportID%>'" type=button value=Update name=btnEdit>
		</td>
	</tr>
	<tr>
		<td class="cellvaluecaption" noWrap width="110">Report Title</td>
		<td class="cellvaluewhite" width="200"><asp:label id="lblTitle" runat="server"></asp:label></td>
		<td class="cellvaluecaption" width="100">Report ID</td>
		<td class="cellvaluewhite" width="190"><asp:label id="lblReportId" runat="server"></asp:label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Input Type</td>
		<td class="cellvaluewhite"><asp:label id="lblInputType" runat="server"></asp:label></td>
		<td class="cellvaluecaption">Scope</td>
		<td class="cellvaluewhite"><asp:label id="lblScope" runat="server"></asp:label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Staging Path</td>
		<td class="cellvaluewhite" colSpan="3"><asp:label id="lblStagingPath" runat="server"></asp:label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Location Path</td>
		<td class="cellvaluewhite" colSpan="3"><asp:label id="lblLocationPath" runat="server"></asp:label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Default FileName</td>
		<td class="cellvaluewhite"><asp:label id="lblDefaultFile" runat="server"></asp:label><asp:label id="lblIdentifier" runat="server"></asp:label></td>
		<td class="cellvaluecaption">File Format</td>
		<td class="cellvaluewhite"><asp:label id="lblFormat" runat="server"></asp:label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Max File Size</td>
		<td class="cellvaluewhite"><asp:label id="lblMaxFileSize" runat="server"></asp:label>Kb.</td>
		<td class="cellvaluecaption">Expiry Type</td>
		<td class="cellvaluewhite"><asp:label id="lblExpiry" runat="server"></asp:label><asp:label id="lblArchive" runat="server"></asp:label><asp:label id="lblApproverEmail" runat="server" Visible="False"></asp:label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Approval From</td>
		<td class="cellvaluewhite"><asp:label id="lblApprovalType" runat="server"></asp:label></td>
		<td class="cellvaluecaption">Report Owners</td>
		<td class="cellvaluewhite"><asp:label id="rptOwners" runat="server"></asp:label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Allow Multiples</td>
		<td class="cellvaluewhite"><asp:checkbox id="chkAllowMulti" runat="server" Enabled="False"></asp:checkbox></td>
		<td class="cellvaluecaption">Read Only</td>
		<td class="cellvaluewhite">
			<asp:CheckBox id="chkReadOnly" runat="server" Enabled="False"></asp:CheckBox></td>
	</tr>
	<!--tr>
		<td class="cellvaluecaption">Approver Email</td>
		<td class="cellvaluewhite" colspan="3">
			</td>
	</tr-->
	<tr height="3">
		<td colSpan="4"></td>
	</tr>
	<tr>
		<td class="cellvaluecaption" vAlign="top">Report Description</td>
		<td class="cellvaluewhite" colSpan="3">
			<asp:TextBox ID="txtDesc" Runat="server" CssClass="cellvalueleft" Rows="3" Width="400px" TextMode="MultiLine" ReadOnly="True"></asp:TextBox></td>
	</tr>
</table>
