Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Partial  Class ReportApprovers
    Inherits System.Web.UI.UserControl
    Public report As Report
    Private _approverArr As ArrayList
    Protected reportId As Integer
    Protected ShowApprovers As Boolean = True
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        With report
            _approverArr = .Approvers
            reportId = .ReportId
        End With
        If report.ApprovalType = ReportApprovalType.None Then
            ShowApprovers = False
        Else
            BindData()
        End If

    End Sub
    Private Sub BindData()
        dgApprovers.DataSource = _approverArr
        dgApprovers.DataBind()
    End Sub
End Class
