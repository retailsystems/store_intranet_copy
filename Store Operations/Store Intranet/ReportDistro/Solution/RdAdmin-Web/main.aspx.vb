Imports HotTopic.RD.Application

Public Class main
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.RD.Application.Session
    Private _employeeId As Integer = 1
    Protected menuStr As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            

            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr


        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try

    End Sub

End Class
