Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity

Partial Class ViewReport
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.RD.Application.Session
    Protected menuStr As String
    Private _rpt As Report
    Private _reportId As Integer
    Protected WithEvents reportmain As ViewReportMain
    Protected WithEvents approvers As ReportApprovers
    Protected WithEvents codeOwners As ReportJobCodeOwners
    Protected WithEvents subOwners As ReportSubOwners
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load

            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        If Not IsPostBack Then
            If IsNumeric(Request("reportId")) Then
                _reportId = Request("reportId")
                _rpt = _sessionUser.GetReport(_reportId)
                reportmain.report = _rpt
                approvers.report = _rpt
                codeOwners.report = _rpt
                subOwners.report = _rpt
            End If
        End If
    End Sub

End Class
