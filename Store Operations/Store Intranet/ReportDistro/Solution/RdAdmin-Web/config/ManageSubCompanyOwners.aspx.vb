Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Partial Class ManageSubCompanyOwners
    Inherits System.Web.UI.Page

    Private dsAllSubOwners As DataSet
    Private subOwnerStr As String
    Private _reportID As Integer
    Private _rpt As Report
    Private _subOwenerArr As ArrayList
    Private _sessionUser As HotTopic.RD.Application.Session
    Protected menuStr As String
    Private li As ListItem
    Private i As Integer
    Private indArr As ArrayList
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        _reportID = CInt("0" + Request("ReportID"))
        lblError.Text = ""
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr
        
        If Not IsPostBack Then
            'lstAllSubOwners = _sessionUser.ListAllsubOwner
                _rpt = _sessionUser.GetReport(_reportID)
                If _rpt.ContentScope = ContentScopeType.Company Then
                    'skip this page
                    Response.Redirect("ViewReport.aspx?ReportID=" & _reportID)
                End If
                If Request("mode") = "new" Then

                Else
                    btnSubmit.Text = "Update"
                End If

                _subOwenerArr = _rpt.CompanySubOwners
                lblRptTitle.Text = _rpt.Title
                If _subOwenerArr.Count > 0 Then
                    Dim i As Integer = 0
                    Dim subOwner As SubCompanyOwner
                    For i = 0 To _subOwenerArr.Count - 1
                        subOwner = _subOwenerArr(i)
                        lstSubOwners.Items.Add(New ListItem(subOwner.OwnerName & "(" & subOwner.CompanyName & ")", subOwner.CompanyId & "/" & subOwner.OwnerID))
                    Next
                End If
                Dim dr As DataRow
                dsAllSubOwners = _sessionUser.ListAllSubCompanyOwners(_reportID)
                For Each dr In dsAllSubOwners.Tables(0).Rows
                    li = New ListItem(dr("OwnerName") & "(" & dr("CompanyName") & ")", dr("CompanyID") & "/" & dr("OwnerID"))
                    If lstSubOwners.Items.IndexOf(li) = -1 Then
                        lstAllSubOwners.Items.Add(li)
                    End If
                Next

            End If
        Catch ex As RDAppException
            lblError.Text = ex.Message
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
    End Sub

    Private Sub moveRight_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles moveRight.Click
       
        indArr = New ArrayList()
        For Each li In lstAllSubOwners.Items
            If li.Selected Then
                If lstSubOwners.Items.IndexOf(li) = -1 Then
                    lstSubOwners.Items.Add(li)
                    'lstAllSubOwners.Items.Remove(li)
                    indArr.Add(li)
                End If
            End If
        Next
        If indArr.Count > 0 Then
            For i = 0 To indArr.Count - 1
                lstAllSubOwners.Items.Remove(indArr(i))
            Next
        End If
        lstAllSubOwners.SelectedIndex = -1
        lstSubOwners.SelectedIndex = -1
    End Sub

    Private Sub moveLeft_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles moveLeft.Click
        indArr = New ArrayList()
        For Each li In lstSubOwners.Items
            If li.Selected Then
                If lstAllSubOwners.Items.IndexOf(li) = -1 Then
                    lstAllSubOwners.Items.Add(li)
                End If
                indArr.Add(li)
            End If
        Next
        If indArr.Count > 0 Then
            For i = 0 To indArr.Count - 1
                lstSubOwners.Items.Remove(indArr(i))
            Next
        End If
        lstAllSubOwners.SelectedIndex = -1
        lstSubOwners.SelectedIndex = -1
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            'If lstSubOwners.Items.Count > 0 Then

                subOwnerStr = ""
                Dim subOwenerArr As ArrayList
                Dim subOwner As SubCompanyOwner
                subOwenerArr = New ArrayList()
                Dim li As ListItem
                For Each li In lstSubOwners.Items
                    'subOwner = New HotTopic.RD.Entity.Approver(li.Value)
                    subOwner = New SubCompanyOwner()
                    With subOwner
                        .CompanyId = li.Value.Split("/")(0)
                        .OwnerID = li.Value.Split("/")(1)
                    End With
                    subOwenerArr.Add(subOwner)
                Next

                _sessionUser.SaveSubCompanyOwners(_reportID, subOwenerArr)

               
                Response.Redirect("ViewReport.aspx?ReportID=" & _reportID)

                'Else
                '    lblError.Text = "Please select approvers"
                'End If
        Catch ex As RDAppException
            lblError.Text = ex.Message
        Catch ex As Exception
            'lblError.Text = "Following Error Occured while processing your request :" & ex.Message
            Throw ex
        End Try
    End Sub
End Class
