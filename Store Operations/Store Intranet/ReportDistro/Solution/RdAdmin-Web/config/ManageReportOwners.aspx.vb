Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Partial Class ManageReportOwners
    Inherits System.Web.UI.Page
    Private ds As DataSet
    Private dt As DataTable
    Private dr As DataRow
    Private _sessionUser As HotTopic.RD.Application.Session
    Protected menuStr As String
    Private _reportID As Integer
    Private jobCodeArr As ArrayList = New ArrayList()
    Private _companyArr As ArrayList = New ArrayList()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'ToDo: update and delete functionality are incomplete
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            lblExMsg.Text = ""
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        _reportID = CInt("0" + Request("ReportID"))

        If Not IsPostBack Then
            If Request("mode") = "new" Then
                btnDone.Text = "Next>>"
            Else
                btnDone.Text = "Done"
            End If


            BindData()


        End If
    End Sub
    Private Sub BindData()

        Dim rpt As New Report()

        rpt = _sessionUser.GetReport(_reportID)
        lblRptTitle.Text = rpt.Title
        jobCodeArr = rpt.JobCodeOwners
        dg.DataSource = GetDataSetFromArrayList(jobCodeArr)
        UIManager.SetDataGridProperties(dg, 520, 10)
        dg.AllowPaging = False
        dg.DataBind()
        'populate company list
        _companyArr = rpt.CompanyOwners
        Dim i As Integer = 0
        Dim company As CompanyOwner
        Dim li As ListItem
        For i = 0 To _companyArr.Count - 1
            '8/5/03 SRI: Duplicate list item fix.
            company = _companyArr(i)
            li = New ListItem(company.CompanyName, company.CompanyId)
            If lstCompany.Items.IndexOf(li) < 0 Then
                lstCompany.Items.Add(li)
            End If
        Next
    End Sub
    Private Function GetDataSetFromArrayList(ByVal arrayList As ArrayList) As DataSet
        ds = New DataSet()
        dt = New DataTable()
        dt.TableName = "Table1"
        dt.Columns.Add("CompanyID")
        dt.Columns.Add("CompanyName")
        dt.Columns.Add("JobCode")
        Dim i As Integer

        For i = 0 To arrayList.Count - 1
            Dim _jcOwner As JobCodeOwner = New JobCodeOwner()
            _jcOwner = arrayList(i)
            With _jcOwner
                dr = dt.NewRow
                dr.Item(0) = .CompanyId
                dr.Item(1) = .CompanyName
                dr.Item(2) = .JobCode
                dt.Rows.Add(dr)
            End With
        Next
        ds.Tables.Add(dt)
        Return ds
    End Function

    Private Sub dg_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dg.EditCommand
        dg.EditItemIndex = e.Item.ItemIndex

        pnlNew.Visible = False
        BindData()
        'Dim lstRole As New DropDownList()
        'Dim curRole As Label
        'Dim li As ListItem
        'curRole = e.Item.FindControl("lblJobCode")
        'For Each li In lstUserRole.Items
        '    lstRole.Items.Add(li)
        'Next
        'For Each li In lstRole.Items
        '    If li.Text = curRole.Text Then
        '        li.Selected = True
        '    End If
        'Next
        'lstRole.Enabled = True
        'lstRole.Visible = True
        'e.Item.Cells(2).Controls.AddAt(1, lstRole)

        'Dim li As ListItem
        '_jobCode.SelectedIndex = -1
        'For Each li In li.
        '    If li.Value = _oldCode.Text Then
        '        li.Selected = True
        '    End If
        'Next
       

    End Sub

    Private Sub dg_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dg.CancelCommand
        dg.EditItemIndex = -1
        pnlNew.Visible = False
        BindData()
    End Sub

    Private Sub dg_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dg.DeleteCommand
        Try
            Dim companyId As Integer
            Dim _companyID As TextBox
            Dim _jobCode As Label
            _companyID = e.Item.FindControl("txtCompanyId")
            companyId = CInt("0" + _companyID.Text)
            _jobCode = e.Item.FindControl("lblJobCode")
            _sessionUser.DeleteJobCodeOwners(_reportID, companyId, _jobCode.Text)
            dg.EditItemIndex = -1
            pnlNew.Visible = False
            BindData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dg_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dg.UpdateCommand
        Try
            Dim companyId As Integer
            'Dim _jobCode As TextBox
            Dim _jobCode As DropDownList
            Dim _oldCode, _companyID As TextBox

            _companyID = e.Item.FindControl("txtCompanyId")
            companyId = CInt("0" + _companyID.Text)
            '_jobCode = e.Item.FindControl("txtJobCode")
            _jobCode = e.Item.FindControl("lstRole")
            _oldCode = e.Item.FindControl("txtOldCode")
            'If Trim(_oldCode.Text) <> Trim(_jobCode.Text) Then
            '    _sessionUser.SaveJobCodeOwners(_reportID, companyId, _jobCode.Text, _oldCode.Text)
            'End If
            If Trim(_oldCode.Text) <> Trim(_jobCode.SelectedItem.Value) Then
                _sessionUser.SaveJobCodeOwners(_reportID, companyId, _jobCode.SelectedItem.Value, _oldCode.Text)
            End If
            dg.EditItemIndex = -1
            pnlNew.Visible = False
            BindData()
        Catch ex As RDAppException
            lblExMsg.Text = ex.Message
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        InitializeForm()
        pnlNew.Visible = True
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlNew.Visible = False
        BindData()
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            '_sessionUser.SaveJobCodeOwners(_reportID, lstCompany.SelectedItem.Value, JobCode.Text)
            _sessionUser.SaveJobCodeOwners(_reportID, lstCompany.SelectedItem.Value, lstUserRole.SelectedItem.Value)
            pnlNew.Visible = False
            BindData()
        Catch ex As RDAppException
            lblExMsg.Text = ex.Message
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dg_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dg.ItemCreated
        Dim myDeleteButton As TableCell
        myDeleteButton = e.Item.Cells(4)

        If dg.EditItemIndex = e.Item.ItemIndex Then
            myDeleteButton.Text = ""
        Else
            myDeleteButton.Attributes.Add("onclick", "return confirm('Are you Sure you want to delete this JobCode?');")
        End If
    End Sub
    Private Sub InitializeForm()
        lstCompany.SelectedItem.Selected = False
        lstCompany.SelectedIndex = 0
        JobCode.Text = ""
    End Sub

    Private Sub btnDone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDone.Click
        If Request("mode") = "new" Then
            Response.Redirect("ManageSubCompanyOwners.aspx?mode=new&ReportId=" & _reportID)
        Else
            Response.Redirect("ViewReport.aspx?ReportId=" & _reportID)
        End If
    End Sub
    Public Function GetRole(ByVal roleId As String) As String
        Dim str As String
        Dim li As ListItem
        For Each li In lstUserRole.Items
            If li.Value = roleId Then
                str = li.Text
                Exit For
            End If
        Next
        Return str
    End Function
End Class
