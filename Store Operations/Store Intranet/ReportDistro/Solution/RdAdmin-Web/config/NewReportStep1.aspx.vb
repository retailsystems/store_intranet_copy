Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity

Partial Class NewReportStep1
    Inherits System.Web.UI.Page
    'Protected WithEvents txtComment As System.Web.UI.WebControls.TextBox

    Private _sessionUser As HotTopic.RD.Application.Session
    Private _company As CompanyOwner
    Private _reportId As Integer
    Protected WithEvents RequiredFieldValidator51 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected menuStr As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
            menuStr = _sessionUser.MenuStr
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        'response.Write 
        'Put user code to initialize the page here
        If Request("mode") = "update" Then
            _reportId = Request("ReportID")
        Else
            _reportId = 0
        End If
        If Not IsPostBack Then
            InitializeForm()
        End If
    End Sub

    Private Sub InitializeForm()
        Try
            Dim cacheMgr As CacheManager = _sessionUser.GetCacheManager()
            Dim lookUpds As DataSet
            Dim lookUpView As DataView
            'lookUpds = Cache("lookUpDs")

            lookUpds = cacheMgr.ListLookups(ConfigurationSettings.AppSettings("CacheDependency"))

            fillListFromLookUp(lstInputType, lookUpds, "InputType")
            fillListFromLookUp(lstScope, lookUpds, "ContentScope")
            fillListFromLookUp(lstExpType, lookUpds, "ExpirationType")
            fillListFromLookUp(lstIdentifier, lookUpds, "FileIdentifier")
            fillListFromLookUp(lstStoreOwner, lookUpds, "Company")
            If Request("mode") = "update" Then
                _reportId = Request("ReportID")
                lblCaption.Text = "Update Report Id: " & _reportId
                btnSubmit.Text = "Update"
                SetFormData()
            Else
                lblCaption.Text = "New Report Setup - Step1"
            End If
        Catch ex As RDAppException
            lblErrMsg.Text = ex.Message
        Catch ex As Exception
            'lblErrMsg.Text = "Following Error Occured while processing your request :" & ex.ToString
        End Try
    End Sub
    Private Sub SetFormData()
        Dim _rpt As New Report()
        _rpt = _sessionUser.GetReport(_reportId)
        With _rpt
            txtRptTitle.Text = .Title
            txtRptDesc.Text = .Description
            SetListItem(lstInputType, .InputType)
            txtFilePath.Text = .StagingPath
            txtLocation.Text = .LocationPath


            txtFileName.Text = .DefaultFileName
            SetListItem(lstIdentifier, .DefaultFileIdentifier)
            SetListItem(lstScope, .ContentScope)
            lblOldContentScope.Text = .ContentScope
            SetListItem(lstExpType, .ExpirationType)
            If .ExpirationType = ExpType.NthReport Then
                txtArchiveNth.Text = .ArchiveNth
                lblArchive.Visible = True
                txtArchiveNth.Visible = True
            Else
                lblArchive.Visible = False
                txtArchiveNth.Visible = False
            End If
            txtFileForamt.Text = .FileFormat
            txtMaxSize.Text = .MaxFileSize
            txtOrder.Text = .SortOrder
            txtEmail.Text = .ApproverGroupEmail
            SetRadioItem(rdoApproveProcess, .ApprovalType)
            If .AllowMultiFlag = True AndAlso .InputType <> FileInputType.System Then
                rdoAllowMulti.Items(1).Selected = False
                rdoAllowMulti.Items(0).Selected = True
            Else
                rdoAllowMulti.Items(0).Selected = False
                rdoAllowMulti.Items(1).Selected = True
            End If
            If .InputType = FileInputType.System Then
                rdoAllowMulti.Enabled = False
            End If
            If .ReadOnlyFlag = True Then
                rdoReadOnly.Items(0).Selected = True
            Else
                rdoReadOnly.Items(1).Selected = True
            End If
            If .CompanyOwners.Count > 1 Then
                'Check if cd store flag is on
                _company = New CompanyOwner()
                _company = .CompanyOwners.Item(0)
                With _company
                    If .CDStoresFlag = True Then
                        SetListItem(lstStoreOwner, StoreOwner.CDStores)
                    Else
                        SetListItem(lstStoreOwner, StoreOwner.AllStores)
                    End If
                End With

            Else
                Dim company As CompanyOwner = .CompanyOwners(0)
                SetListItem(lstStoreOwner, company.CompanyId)
            End If
            lstStoreOwner.Enabled = False

            SetFileIdentifier()
            SetInputDependencies()

        End With
    End Sub
    Private Sub IsValidPath(ByVal path As String, ByVal errText As String)
        Try
            If _sessionUser.PathExists(path) = False Then
                Throw New RDAppException("Inaccessible Path.")
            End If
        Catch ex As RDAppException
            Throw New RDAppException("Error in " & errText & " Path. Reason: " & ex.Message)
        End Try

    End Sub
    Private Sub SetListItem(ByRef lstCtrl As DropDownList, ByVal SelectedValue As String)
        If lstCtrl.Items.Count > 0 Then
            lstCtrl.SelectedItem.Selected = False
            Dim li As ListItem
            For Each li In lstCtrl.Items
                If CStr(li.Value) = SelectedValue Then
                    li.Selected = True
                End If
            Next
        End If
    End Sub
    Private Sub SetRadioItem(ByRef lstCtrl As RadioButtonList, ByVal SelectedValue As String)
        If lstCtrl.Items.Count > 0 Then
            lstCtrl.SelectedItem.Selected = False
            Dim li As ListItem
            For Each li In lstCtrl.Items
                If CStr(li.Value) = SelectedValue Then
                    li.Selected = True
                End If
            Next
        End If
    End Sub
    Private Sub fillListFromLookUp(ByRef lstCtrl As DropDownList, ByVal lookUpds As DataSet, ByVal filterVal As String, Optional ByVal SelectedValue As String = "")
        Dim li As ListItem
        Dim lookUpView As DataView
        lstCtrl.Items.Clear()
        If lookUpds.Tables(0).Rows.Count > 0 Then

            lookUpView = lookUpds.Tables(0).DefaultView
            If Trim(filterVal) <> "" Then
                lookUpView.RowFilter = "lookUpType='" & filterVal & "'"
            End If
            lstCtrl.DataTextField = "LookUpText"
            lstCtrl.DataValueField = "LookUpValue"
            lstCtrl.DataSource = lookUpView
            lstCtrl.DataBind()
        End If
        lstCtrl.Items.Insert(0, New ListItem("---", ""))

        If SelectedValue <> "" Then
            For Each li In lstCtrl.Items
                If li.Value = SelectedValue Then
                    li.Selected = True
                End If
            Next
        End If
    End Sub
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        '--save data
        'Response.Write(lstStoreOwner.SelectedItem.Value)
        'Response.End()
        'validate
        If lstInputType.SelectedItem.Value = FileInputType.System And _
            rdoAllowMulti.Items(0).Selected = True Then
            lblErrMsg.Text = "System-generated reports not allowed multiples for same fiscal week."
            Return
        End If

        Dim rpt As Report = New Report()
        Dim storeArr As ArrayList = New ArrayList()
        Dim li As ListItem
        Select Case lstStoreOwner.SelectedItem.Value
            Case StoreOwner.HotTopic
                _company = New CompanyOwner()
                With _company
                    .CDStoresFlag = False
                    .CompanyId = StoreOwner.HotTopic
                End With
                storeArr.Add(_company)
            Case StoreOwner.Torrid
                _company = New CompanyOwner()
                With _company
                    .CDStoresFlag = False
                    .CompanyId = StoreOwner.Torrid
                End With
                storeArr.Add(_company)
            Case StoreOwner.CDStores

                _company = New CompanyOwner()
                With _company
                    .CDStoresFlag = True
                    .CompanyId = StoreOwner.HotTopic
                End With
                storeArr.Add(_company)
                _company = New CompanyOwner()
                With _company
                    .CDStoresFlag = True
                    .CompanyId = StoreOwner.Torrid
                End With
                storeArr.Add(_company)
            Case StoreOwner.AllStores
                '7/25/03 All Stores
                _company = New CompanyOwner()
                With _company
                    .CDStoresFlag = False
                    .CompanyId = StoreOwner.HotTopic
                End With
                storeArr.Add(_company)
                _company = New CompanyOwner()
                With _company
                    .CDStoresFlag = False
                    .CompanyId = StoreOwner.Torrid
                End With
				storeArr.Add(_company)
			Case StoreOwner.BoxLunch
				_company = New CompanyOwner()
				With _company
					.CDStoresFlag = False
					.CompanyId = StoreOwner.BoxLunch
				End With
				storeArr.Add(_company)
		End Select
        Try
            With rpt
                .ReportId = _reportId
                .Title = txtRptTitle.Text
                .Description = txtRptDesc.Text
                .InputType = lstInputType.SelectedItem.Value
                .StagingPath = txtFilePath.Text
                .LocationPath = txtLocation.Text
                'check for valid path
                IsValidPath(.StagingPath, "Staging")
                IsValidPath(.LocationPath, "Location")
                .DefaultFileName = txtFileName.Text
                If lstIdentifier.SelectedItem.Value <> "" Then
                    .DefaultFileIdentifier = lstIdentifier.SelectedItem.Value
                End If
                .ExpirationType = lstExpType.SelectedItem.Value
                .FileFormat = txtFileForamt.Text
                .MaxFileSize = txtMaxSize.Text
                .SortOrder = txtOrder.Text
                .ApproverGroupEmail = txtEmail.Text
                .CompanyOwners = storeArr
                .ApprovalType = rdoApproveProcess.SelectedItem.Value

                If rdoAllowMulti.SelectedItem.Value = 1 Then
                    .AllowMultiFlag = True
                Else
                    .AllowMultiFlag = False
                End If
                If rdoReadOnly.SelectedItem.Value = 1 Then
                    .ReadOnlyFlag = True
                Else
                    .ReadOnlyFlag = False
                End If
                If lstExpType.SelectedItem.Value = ExpType.NthReport Then
                    .ArchiveNth = CInt("0" + txtArchiveNth.Text)
                End If
                .ContentScope = lstScope.SelectedItem.Value

            End With

            _sessionUser.SaveReport(rpt)
            'SRI:delete subCompanyOwners if scope changes
            'TODO: Notify User about the deltion.

            If rpt.ReportId > 0 Then
                If Request("mode") = "update" Then
                    If rpt.ContentScope <> CInt("0" + lblOldContentScope.Text) Then
                        _sessionUser.DeleteSubCompanyOwners(rpt.ReportId)
                    End If
                    Response.Redirect("ViewReport.aspx?ReportID=" & rpt.ReportId)
                Else
                    Response.Redirect("ManageApprovers.aspx?mode=new&ReportID=" & rpt.ReportId)
                End If

            End If
        Catch ex As RDAppException
            lblErrMsg.Text = ex.Message
        Catch ex As Exception
            'lblErrMsg.Text = "Following Error Occured while processing your request :" & ex.ToString
            Throw ex
        End Try
        'Response.Write("new report id is: " & rpt.ReportId)
        'Response.End()
    End Sub


    Private Sub lstExpType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstExpType.SelectedIndexChanged
        If lstExpType.SelectedItem.Value = "2" Then
            txtArchiveNth.Visible = True
            lblArchive.Visible = True
        Else
            txtArchiveNth.Visible = False
            lblArchive.Visible = False
        End If
    End Sub

    Private Sub lstInputType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstInputType.SelectedIndexChanged
        SetInputDependencies()
    End Sub

    Private Sub lstScope_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstScope.SelectedIndexChanged
        SetFileIdentifier()
    End Sub

    Private Sub SetInputDependencies()
        If lstInputType.SelectedItem.Value = FileInputType.System Then
            rdoAllowMulti.Items(0).Selected = False
            rdoAllowMulti.Items(1).Selected = True
            rdoAllowMulti.Enabled = False
            RequiredFieldValidator5.Enabled = True
            lblReqFileName.Visible = True
            SetFileIdentifier()
        Else
            rdoAllowMulti.Enabled = True
            RequiredFieldValidator5.Enabled = False
            lblReqFileName.Visible = False
            lstIdentifier.SelectedIndex = -1
            lblFileIdentifier.Text = ""
        End If

    End Sub

    Private Sub SetFileIdentifier()
        lstIdentifier.SelectedIndex = -1
        If lstScope.SelectedIndex > 0 Then
            If lstScope.SelectedItem.Value <> ContentScopeType.Company Then
                lstIdentifier.Items.FindByValue(lstScope.SelectedItem.Value).Selected = True
                lblFileIdentifier.Text = "&lt;" & lstIdentifier.SelectedItem.Text & "&gt;"
            Else
                lblFileIdentifier.Text = ""

            End If
        Else
            lblFileIdentifier.Text = ""
        End If
    End Sub
End Class
