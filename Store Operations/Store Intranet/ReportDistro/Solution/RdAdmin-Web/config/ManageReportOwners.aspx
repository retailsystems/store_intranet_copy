<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageReportOwners.aspx.vb" Inherits="ReportDistroAdmin.ManageReportOwners" %>
<HTML>
	<!--#include file="../include/header.inc"-->
	<body>
		<form id="Form1" method="post" runat="server">
			<br>
			<table border="0" cellpadding="0" cellspacing="0" width="520" align="center">
				<tr>
					<td align="middle" class="pageCaption">Manage Report Owners</td>
				</tr>
				<tr height="10">
					<td></td>
				</tr>
				<%If Request("mode") = "new" Then%>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td width="50" class="stepStyleOn">
									Step 1:
								</td>
								<td class="stepStyleOn">
									Enter Basic Report Settings.
								</td>
							</tr>
							<TR>
								<TD class="stepStyleOff" width="50"></TD>
								<TD class="stepStyleOff">
							<TR>
								<TD class="stepStyleOn" width="50"></TD>
								<TD class="stepStyleOn"></TD>
							</TR>
							<TR>
								<TD class="stepStyleOn" width="50"></TD>
								<TD class="stepStyleOn"></TD>
								<TD class="stepStyleOn"></TD>
							</TR>
							<tr>
								<td width="50" class="stepStyleOn">
									Step 2:
								</td>
								<td class="stepStyleOn">
									Add Approvers to Report if Approval Process is on.
								</td>
							</tr>
							<tr>
								<td width="50" class="stepStyleOff">
									Step 3:
								</td>
								<td class="stepStyleOff">
									Associate User Roles for Restricted Access (Optional).&nbsp;
								</td>
							</tr>
							<tr>
								<td width="50" class="stepStyleOn">
									Step 4:
								</td>
								<td class="stepStyleOn">
									Associate Sub Company (Region/District/Store) for Restricted Access (Optional).
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<%end if%>
				<tr>
					<td align="middle" width="100%">
						<table align="center" width="400">
							<tr>
								<td class="cellvaluecaption">Report ID:</td>
								<td class="cellvaluewhite"><%=request("ReportID")%></td>
							</tr>
							<tr>
								<td class="cellvaluecaption">Report Title:</td>
								<td class="cellvaluewhite"><asp:Label Runat="server" ID="lblRptTitle"></asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="15">
					<td align="middle">
						<asp:Label ID="lblExMsg" Runat="server" CssClass="errStyle"></asp:Label>
					</td>
				</tr>
				<tr height="20">
					<td></td>
				</tr>
				<tr>
					<td align="middle">
						<asp:DataGrid ID="dg" Width="350" Runat="server" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false">
							<Columns>
								<asp:TemplateColumn HeaderStyle-Width="1px" Visible="true">
									<ItemTemplate>
										<asp:TextBox ID="txtCompanyId" Runat=server Visible=false text='<%#Container.DataItem("CompanyId")%>' CssClass=cellvalueleft>
										</asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderStyle-Width="150px">
									<HeaderTemplate>
										Company
									</HeaderTemplate>
									<ItemTemplate>
										<%#Container.DataItem("CompanyName")%>
									</ItemTemplate>
									<EditItemTemplate>
										<%#Container.DataItem("CompanyName")%>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderStyle-Width="120px">
									<HeaderTemplate>
										User Role
									</HeaderTemplate>
									<ItemTemplate>
										<%#GetRole(Container.DataItem("JobCode"))%>
										<asp:Label ID="lblJobCode" Runat=server text='<%#Container.DataItem("JobCode")%>' Visible=False>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox ID="txtJobCode" Runat=server Width=100px text='<%#Container.DataItem("JobCode")%>' CssClass=cellvalueleft Visible=False>
										</asp:TextBox>
										<asp:DropDownList ID="lstRole" Runat="server" CssClass="cellvalueleft">
											<asp:ListItem Value="4">DM</asp:ListItem>
											<asp:ListItem Value="6">RM</asp:ListItem>
											<asp:ListItem Value="5">Store</asp:ListItem>
											<asp:ListItem Value="12">MLU</asp:ListItem>
										</asp:DropDownList>
										<asp:TextBox ID="txtOldCode" Runat=server visible=False text='<%#Container.DataItem("JobCode")%>' CssClass=cellvalueleft>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:EditCommandColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" EditText="Edit" UpdateText="Update" CancelText="Cancel"></asp:EditCommandColumn>
								<asp:ButtonColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25px" HeaderStyle-Width="25px" ButtonType="LinkButton" CommandName="Delete" Text="Delete"></asp:ButtonColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
				<tr>
					<td align="right">
						<asp:Button Runat="server" ID="btnAdd" Text="Add New" CssClass="B1"></asp:Button>
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
				<tr>
					<td align="middle" width="100%">
						<asp:Panel ID="pnlNew" Runat="server" Visible="False">
							<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="400" align="center">
								<TR height="5">
									<TD colSpan="2"></TD>
								</TR>
								<TR>
									<TD class="cellvaluecaption">Company:</TD>
									<TD>
										<asp:DropDownList id="lstCompany" Runat="server" CssClass="cellvaluleft"></asp:DropDownList></TD>
								</TR>
								<TR height="5">
									<TD colSpan="2"></TD>
								</TR>
								<TR>
									<TD class="cellvaluecaption">User Role:</TD>
									<TD>
										<asp:DropDownList id="lstUserRole" Runat="server" CssClass="cellvalueleft">
											<asp:ListItem Value="4">DM</asp:ListItem>
											<asp:ListItem Value="6">RM</asp:ListItem>
											<asp:ListItem Value="5">Store</asp:ListItem>
											<asp:ListItem Value="12">MLU</asp:ListItem>
										</asp:DropDownList>
										<asp:TextBox id="JobCode" Runat="server" CssClass="cellvalueleft" Width="100px" Visible="False"></asp:TextBox></TD>
								</TR>
								<TR>
									<TD>&nbsp;</TD>
								</TR>
								<TR>
									<TD align="middle" colSpan="2">
										<asp:Button id="btnSubmit" Runat="server" CssClass="B1" Text="Submit"></asp:Button>
										<asp:Button id="btnCancel" Runat="server" CssClass="B1" Text="Cancel"></asp:Button></TD>
								</TR>
								<TR height="5">
									<TD colSpan="2"></TD>
								</TR>
							</TABLE>
						</asp:Panel>
					</td>
				</tr>
			</table>
			<!--footer--> </TD></TR>
			<tr>
				<td><hr color='#990000'>
				</td>
				<td><hr color='#990000'>
				</td>
				<td><hr color='#990000'>
				</td>
				<td><hr color='#990000'>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td align='right'>
					<asp:Button CssClass="B1" ID="btnDone" Runat="server" Text="Done"></asp:Button>&nbsp;&nbsp; 
					<!--#include file="../include/homeLink.inc"-->
				</td>
			</tr>
			</TABLE></form>
	</body>
</HTML>
