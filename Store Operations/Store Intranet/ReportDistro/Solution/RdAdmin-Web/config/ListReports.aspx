<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ListReports.aspx.vb" Inherits="ReportDistroAdmin.ListReports" %>
<HTML>
	<!--#include file="../include/header.inc"-->
	<body>
		<form id="Form1" method="post" runat="server">
			<br>
			<table border="0" cellpadding="0" cellspacing="0" width="520" align="center">
				<tr>
					<td align="middle" class="pageCaption">Manage Report Setup</td>
				</tr>
				<tr height="10">
					<td></td>
				</tr>
				<tr>
					<td>
						<asp:DataGrid ID="dgReport" Width="650" Runat="server" BorderColor="#990000" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false" AllowPaging="True" AllowSorting="True" PageSize="10" PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
							<Columns>
								<asp:BoundColumn DataField="reportId" SortExpression="reportId" Visible="False"></asp:BoundColumn>
								<asp:HyperLinkColumn DataTextField="ReportTitle" SortExpression="ReportTitle" Visible=False HeaderText="Title" DataNavigateUrlField="ReportID" DataNavigateUrlFormatString="ViewReport.aspx?ReportID={0}"></asp:HyperLinkColumn>
								<asp:TemplateColumn SortExpression="ReportTitle" HeaderText="Title">
									<ItemTemplate>
										<a href="ViewReport.aspx?ReportID=<%#Container.DataItem("ReportID")%>"><%#Container.DataItem("ReportTitle")%><%#GetCompleteStatus(Container.DataItem("SetupCompleteFlag"))%></a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="StagingPath" SortExpression="StagingPath" HeaderText="Staging"></asp:BoundColumn>
								<asp:BoundColumn DataField="Scope" SortExpression="Scope" HeaderText="Scope"></asp:BoundColumn>
								<asp:BoundColumn DataField="DefaultFileName" SortExpression="DefaultFileName" HeaderText="File Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="FileIdentifier" SortExpression="FileIdentifier" HeaderText="File Identifier"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
						<asp:Label ID="SortBy" Visible="False" Runat="server"></asp:Label>
						<asp:Label ID="SortDirection" Visible="False" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
			</table>
		</form>
		<!--#include file="../include/footer.inc"--> </TBODY></TABLE>
	</body>
</HTML>
