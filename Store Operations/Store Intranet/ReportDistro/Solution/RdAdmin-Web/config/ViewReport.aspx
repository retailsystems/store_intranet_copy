<%@ Register TagPrefix="UC" TagName="SubOwners" src="../userControls/ReportSubOwners.ascx" %>
<%@ Register TagPrefix="UC" TagName="JobCodeOwners" src="../userControls/ReportJobCodeOwners.ascx" %>
<%@ Register TagPrefix="UC" TagName="Approvers" src="../userControls/ReportApprovers.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ViewReport.aspx.vb" Inherits="ReportDistroAdmin.ViewReport" %>
<%@ Register TagPrefix="UC" TagName="ReportMain" src="../userControls/ViewReportMain.ascx" %>
<HTML>
	<!--#include file="../include/header.inc"-->
	<body>
		<form id="Form1" method="post" runat="server">
			<br>
			<table border="0" cellpadding="0" cellspacing="0" width="520" align="center">
				<tr>
					<td align="middle" class="pageCaption">Report Details</td>
				</tr>
				<tr height="10">
					<td></td>
				</tr>
				<tr>
					<td>
						<UC:ReportMain runat="server" id="reportMain"></UC:ReportMain>
					</td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
				<tr>
					<td valign="top">
						<table align="center" width="600">
							<tr>
								<td width="300" valign="top" height=100%>
									<UC:JobCodeOwners runat="server" id="codeOwners"></UC:JobCodeOwners>
									
									<UC:Approvers runat="server" id="approvers"></UC:Approvers>
									
									
								</td>
								<td>
								<td width="5"></td>
								<td valign="top" height="100%">
									<UC:SubOwners runat="server" id="subOwners"></UC:SubOwners>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
		<!--#include file="../include/footer.inc"--> </TBODY></TABLE>
	</body>
</HTML>
