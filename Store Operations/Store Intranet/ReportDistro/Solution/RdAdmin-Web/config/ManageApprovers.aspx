<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageApprovers.aspx.vb" Inherits="ReportDistroAdmin.ManageApprovers" %>
<HTML>
	<body>
		<!--#include file="../include/header.inc"-->
		<form id="Form1" method="post" runat="server">
			<br>
			<table width="700" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="170">
					</td>
					<td>
						<table align="center" border="0">
							<tr>
								<td colspan="3" align="middle" class="pageCaption">Manage Approvers</td>
							</tr>
							<%If Request("mode") = "new" Then%>
							<tr>
								<td colspan="3">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td width="50" class="stepStyleOn">
												Step 1:
											</td>
											<td class="stepStyleOn">
												Enter Basic Report Settings.&nbsp;
											</td>
										</tr>
										<tr>
											<td width="50" class="stepStyleOff">
												Step 2:
											</td>
											<td class="stepStyleOff">
												Add Approvers to Report if Approval Process is on.
											</td>
										</tr>
										<tr>
											<td width="50" class="stepStyleOn">
												Step 3:
											</td>
											<td class="stepStyleOn">
												Associate User Roles for Restricted Access (Optional).&nbsp;
											</td>
										</tr>
										<tr>
											<td width="50" class="stepStyleOn">
												Step 4:
											</td>
											<TD class="stepStyleOn">Associate Sub Company (Region/District/Store) for 
												Restricted Access (Optional).
											</TD>
										</tr>
									</table>
								</td>
							</tr>
							<%end if%>
							<tr height="15">
								<td colspan="3">
									<asp:Label ID="lblError" Runat="server" CssClass="errStyle"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">Report ID</td>
								<td width="10" class="reqStyle"></td>
								<td class="cellvaluecaption"><%=request("ReportID")%>
								</td>
							</tr>
							<tr height="5">
								<td colspan="3"></td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">Report Title</td>
								<td width="10" class="reqStyle"></td>
								<td class="cellvaluecaption"><asp:Label Runat="server" ID="lblRptTitle"></asp:Label>
								</td>
							</tr>
							<tr height="5">
								<td colspan="3"></td>
							</tr>
						</table>
					</td>
					<td width="170" align="right">
						<table cellpadding="2" cellspacing="0" style="BORDER-RIGHT: #cc0033 1px solid; BORDER-TOP: #cc0033 1px solid; BORDER-LEFT: #cc0033 1px solid; BORDER-BOTTOM: #cc0033 1px solid">
							<tr>
								<td colspan="2" class="cellvaluecaption">Legend</td>
							</tr>
							<tr>
								<td class="cellvalueleft" valign="center">
									<img src="../Images/MoveRight.gif">
								</td>
								<td valign="center" class="cellvaluewhite">-- Add Approver(s)</td>
							</tr>
							<tr>
								<td class="cellvalueleft" valign="center">
									<img src="../Images/MoveLeft.gif">
								</td>
								<td valign="center" class="cellvaluewhite">-- Delete Approver</td>
							</tr>
							<!--
							<tr>
								<td class="cellvalueleft" valign="center">
									<img src="../Images/MoveUp.gif">
								</td>
								<td valign="center" class="cellvaluewhite">-- RouteOrder Move Up</td>
							</tr>
							<tr>
								<td class="cellvalueleft" valign="center">
									<img src="../Images/MoveDown.gif">
								</td>
								<td valign="center" class="cellvaluewhite">-- RouteOrder Move Down</td>
							</tr>
							-->
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<table align="center" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="165" class="cellvaluecaption" align="middle">Available Approvers</td>
								<td width="30"></td>
								<td width="165" class="cellvaluecaption" align="middle">Selected Approvers</td>
								<td width="30"></td>
							</tr>
							<tr height="5">
								<td colspan="3"></td>
							</tr>
							<tr>
								<td width="165" align="right">
									<asp:ListBox ID="lstUsers" Runat="server" Height="200" CssClass="cellvalueleft" Width="160" SelectionMode="Multiple"></asp:ListBox>
								</td>
								<td width="30" valign="center" align="middle">
									<asp:ImageButton ID="moveRight" Runat="server" ImageUrl="../Images/MoveRight.gif" AlternateText="Add Approver(s)"></asp:ImageButton>
									<asp:ImageButton ID="moveLeft" Runat="server" ImageUrl="../Images/MoveLeft.gif" AlternateText="Delete Approver"></asp:ImageButton>
								</td>
								<td width="165" class="cellvaluecaption">
									<asp:ListBox id="lstApprovers" Runat="server" Height="200" CssClass="cellvalueleft" Width="160"></asp:ListBox>
								</td>
								<td width="30" valign="center" align="middle" class="cellvaluecaption">
									<asp:Button ID="MoveUp" Runat="server" CssClass="btnSmall" Width="87" Text="RouteOrder Up"></asp:Button>
									<asp:Button ID="MoveDown" Runat="server" CssClass="btnSmall" Width="87" Text="RouteOrder Down"></asp:Button>
									<!--asp:ImageButton ID="Imagebutton1" ImageUrl="../Images/MoveUp.gif" AlternateText="RouteOrder Move Up"></asp:ImageButton-->
									<!--asp:ImageButton ID="Imagebutton2" ImageUrl="../Images/MoveDown.gif" AlternateText="RouteOrder Move Down"></asp:ImageButton-->
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<!-- end here-->
			<!--footer--> </TD></TR>
			<tr>
				<td><hr color='#990000'>
				</td>
				<td><hr color='#990000'>
				</td>
				<td><hr color='#990000'>
				</td>
				<td><hr color='#990000'>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td align='right'>
					<asp:Button CssClass="B1" ID="btnSubmit" Runat="server" Text="Next>>"></asp:Button>&nbsp;&nbsp; 
					<!--#include file="../include/homeLink.inc"-->
				</td>
			</tr>
			</TABLE></form>
	</body>
</HTML>
