/*
	Function domTab()
	written by Christian Heilmann
*/
function domTab(i){
	// Variables for customisation:
	var numberOfTabs = 2;
	var colourOfInactiveTab = "#ff6633";
	var colourOfActiveTab = "#ff0000";
	var colourOfInactiveLink = "#333333";
	var colourOfActiveLink = "#ffffcc";
	// end variables
	if (document.getElementById){
		for (f=1;f<numberOfTabs+1;f++){
			document.getElementById('contentblock'+f).style.display='none';
			document.getElementById('link'+f).style.background=colourOfInactiveTab;
			document.getElementById('link'+f).style.color=colourOfInactiveLink;
		}
		document.getElementById('contentblock'+i).style.display='block';
		document.getElementById('link'+i).style.background=colourOfActiveTab;
		document.getElementById('link'+i).style.color=colourOfActiveLink;
	}
}
