<%@import NameSpace="HotTopic.ExceptionManagement" %>
<script runat="server">  
'******************************************************************************
'**		Auth: Sri Bajjuri
'**		Date: 2/2/2004
'*******************************************************************************
'**		Change History
'*******************************************************************************
'**		Date:		Author:				Description:
'**		--------	--------		--------------------------------------------
'**    		
'*******************************************************************************/  
' Server Side coding, no code-behind (.vb) file is being used to avoid re-compilation.
Dim sessionExpired As Boolean = False
Sub Page_Load(s as object, e as eventArgs)		
        'Put user code to initialize the page here
        Try		

            If Session.Count <= 0 Then
                If Not ConfigurationSettings.AppSettings("SessionCheckOn") Is Nothing AndAlso ConfigurationSettings.AppSettings("SessionCheckOn").ToUpper = "TRUE" Then
                    sessionExpired = True
                    If Not ConfigurationSettings.AppSettings("SessionExpiryRedirectUrl") Is Nothing Then
                        SessionExpiryRedirectUrl.Value = ConfigurationSettings.AppSettings("SessionExpiryRedirectUrl")
                    End If
                End If
            End If 
            
            If Not IsPostBack And Not sessionExpired And Not Application("Exp") Is Nothing Then
                If Not ConfigurationSettings.AppSettings("DebugMode") Is Nothing AndAlso ConfigurationSettings.AppSettings("DebugMode").ToUpper = "TRUE" Then
                    txtDetails.Text = Application("Exp").ToString
                    txtDetails.Visible = True                   
                Else 
                    Dim exceptionSettings As HotTopic.ExceptionManagement.ConfigSettings
                    IF Not Application("exceptionSettings") Is Nothing THEN
						exceptionSettings = Application("exceptionSettings")						
                    ELSE
						With exceptionSettings
							If Not ConfigurationSettings.AppSettings("ProjectName") Is Nothing AndAlso ConfigurationSettings.AppSettings("ProjectName").Length > 0 Then
								.ProjectName = ConfigurationSettings.AppSettings("ProjectName")
							Else
								.ProjectName = ""
							End If
							If Not ConfigurationSettings.AppSettings("ErrorLog") Is Nothing AndAlso ConfigurationSettings.AppSettings("ErrorLog").Length > 0 Then
								.LogFileName = ConfigurationSettings.AppSettings("ErrorLog")
							Else
								Return
							End If
							If Not ConfigurationSettings.AppSettings("SendExceptionMail") Is Nothing AndAlso ConfigurationSettings.AppSettings("SendExceptionMail").ToUpper = "TRUE" Then
								.SendMail = True
							Else
								.SendMail = False
							End If
							If Not ConfigurationSettings.AppSettings("SmtpServer") Is Nothing AndAlso ConfigurationSettings.AppSettings("SmtpServer").Length > 0 Then
								.SmtpServer = ConfigurationSettings.AppSettings("SmtpServer")                      
							End If
							If Not ConfigurationSettings.AppSettings("SmtpSender") Is Nothing AndAlso ConfigurationSettings.AppSettings("SmtpSender").Length > 0 Then
								.SmtpSender = ConfigurationSettings.AppSettings("SmtpSender")                       
							End If
							If Not ConfigurationSettings.AppSettings("WebmasterEmail") Is Nothing AndAlso ConfigurationSettings.AppSettings("WebmasterEmail").Length > 0 Then
								.WebMasterEmail = ConfigurationSettings.AppSettings("WebmasterEmail")                       
							End If
							IF (.SmtpServer = "" OR .SmtpSender = "" OR .WebMasterEmail = "") THEN
								.SendMail = False
							End If 
						End With
						Application("exceptionSettings") = exceptionSettings
					END IF

                    HotTopic.ExceptionManagement.ExceptionPublisher.Publish(Application("Exp"), exceptionSettings)
					
                End If
            End If             
        Catch ex As Exception
            txtDetails.Text = ex.ToString
            txtDetails.Visible = True
        Finally 
			Application("Exp") = Nothing
        End Try			
    End Sub
</script>
<HTML>
	<HEAD>
		<title>Report Distribution System</title>
		<style>
			.errStyle { FONT-WEIGHT: bold; FONT-SIZE: xx-small; COLOR: #ff0000; FONT-FAMILY: Arial; TEXT-ALIGN: left }
			</style>
	</HEAD>
	<body text="white" vLink="white" aLink="white" link="white" bgColor="black" face="arial">
		<form id="Form1" method="post" runat="server">
			<input type="hidden" id="SessionExpiryRedirectUrl" name="SessionExpiryRedirectUrl" runat="server">
			<%IF sessionExpired THEN%>
			<script language="javascript"> 
					var redirUrl = Form1.SessionExpiryRedirectUrl.value;					
					document.bgColor = "#000000";				
					if (redirUrl != ""){
						var obj = window.opener;
						alert('Active session expired, Please log back again');
						if(obj){
						window.close();						
						window.opener.top.location.href=redirUrl; 
						}else{
							window.top.location.href=redirUrl;
						}
					}	
			</script>
			<table cellSpacing="0" cellPadding="0" width="500" align="center">
				<tr>
					<td colSpan="2"><br>
						<br>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="errStyle">
						<P>Active session was expired ,&nbsp; You may need to re-submit your login 
							information.
						</P>
					</td>
				</tr>
			</table>
			<%ELSE%>
			<table cellSpacing="0" cellPadding="0" width="500" align="center">
				<tr>
					<td colSpan="2"><br>
						<br>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="errStyle">
						<P>Unexpected error occured while processing your request,&nbsp;you 
							may&nbsp;contact your system administrator for further assistance.
						</P>
						<P>
							Click <a href="javascript:history.go(-1)">here</a> to go back.
						</P>
					</td>
				</tr>
				<tr>
					<td colspan="2"><br>
						&nbsp;<asp:TextBox Runat="server" TextMode="MultiLine" CssClass="cellvalueleft" Width="500" Rows="7" id="txtDetails" Visible="false"></asp:TextBox>
					</td>
				</tr>
			</table>
			<%END IF%>
		</form>
	</body>
</HTML>
