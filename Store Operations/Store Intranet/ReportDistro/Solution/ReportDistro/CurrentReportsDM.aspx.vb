Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Public Class CurrentReportsDM
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.RD.Application.Session
    Protected _treeStr As String
    Protected WithEvents lstGrpBy As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblErrMsg As System.Web.UI.WebControls.Label
    Private _grpBy As Integer = 1
    'Protected WithEvents linkWTD As System.Web.UI.WebControls.HyperLink
    Protected rptCaption As String
    'Added by Davendar 7/10/09
    Protected WithEvents shkReferUrl As System.Web.UI.WebControls.HyperLink
    Protected WithEvents shkEmpReferUrl As System.Web.UI.WebControls.HyperLink
    'Added by Davendar 12/22/2009
    Protected WithEvents lnkHTPlus As System.Web.UI.WebControls.HyperLink
    Private _userScope As Integer
    '6/1/04 tree image variables
    Protected _treeImgSfx As String = ""
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            lblErrMsg.Text = ""
            lblErrMsg.Visible = False
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
            'Added by Davendar 7/10/09
            SetShkReferPermission(_sessionUser.SessionUser.StoreNum, _sessionUser.SessionUser.CurrentUserRoleId, _sessionUser.SessionUser.JobCode)
            'rptCaption = GetChildNodeName(_sessionUser.SessionUser.OwnerId, _sessionUser.SessionUser.OwnerScope)
            'rptCaption = GetChildNodeName(21, 1)
            'Added by Davendar 11/19/09
            SetHTCapturePermission(_sessionUser.SessionUser.StoreNum, _sessionUser.SessionUser.CurrentUserRoleId, _sessionUser.SessionUser.JobCode)

            Select Case _sessionUser.SessionUser.CurrentUserRoleId
                Case UserRole.DM
                    rptCaption = "District " & _sessionUser.SessionUser.DistrictId
                    'linkWTD.NavigateUrl = "javascript:OpenPop('ViewWTDReport.aspx?DistrictId=" & _sessionUser.SessionUser.DistrictId & "',600,550)"
                    'linkWTD.Visible = True
                    _userScope = ContentScopeType.District
                Case UserRole.Store
                    rptCaption = "Store " & _sessionUser.SessionUser.StoreNum
                    _userScope = ContentScopeType.Store
                Case UserRole.RM
                    rptCaption = "Region " & _sessionUser.SessionUser.RegionId
                    _userScope = ContentScopeType.Region
                Case UserRole.MLU
                    rptCaption = "Market: " & _sessionUser.SessionUser.Market
                    _userScope = ContentScopeType.Company
            End Select
            '6/1/04 initialize the _treeImgSfx
            If Not IsNothing(Application("Mode")) AndAlso Application("Mode").toUpper = "TORRID" Then
                _treeImgSfx = "_TD"
            End If
            If Not IsPostBack Then
                If Request("GrpBy") <> "" Then
                    _grpBy = Request("GrpBy")
                    lstGrpBy.SelectedIndex = -1
                    lstGrpBy.Items.FindByValue(_grpBy).Selected = True
                Else
                    _grpBy = 1
                End If

                BuildTree()
            End If
        Catch ex As RDAppException
            If ex.Message = "Unauthenticated user." Then
                Dim jsStr As String = ""
                jsStr += "<script>"
                jsStr += "alert('Active Session Expired!');"
                jsStr += "window.top.location.reload();"
                jsStr += "</script>"
                Response.Write(jsStr)
                Response.End()
            End If
            lblErrMsg.Text = ex.Message
            lblErrMsg.Visible = True
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        
    End Sub
    
    Private Sub BuildTree()
        _treeStr = ""
        Dim ds As DataSet = _sessionUser.ListFiles(0, _grpBy)
        Dim nodeName, nodeID, childNode, childNodeID As String
        Dim cnt As Integer = 0
        Dim childCnt As Integer = 0
        Dim dr As DataRow
        Select Case _grpBy
            Case 1
                ' group by title
                For Each dr In ds.Tables(0).Rows

                    If nodeID <> dr("reportID") Then
                        If childCnt > 0 Then
                            _treeStr = _treeStr & "	</table></td></tr>"
                        End If
                        If cnt > 0 Then
                            _treeStr = _treeStr & "	</table></td></tr>"
                        End If

                        nodeName = dr("ReportTitle")
                        nodeID = dr("reportID")
                        BuildNodeHtml(nodeID, nodeName)
                        childNodeID = ""
                        childCnt = 0
                    End If
                    'SRI 8/7/2003 don't build child node if scope equal/higher than user role eg. for dm build child nodes if scope is store.
                    If dr("ContentScopeType") > _userScope Then
                        If childNodeID <> GetChildNodeID(dr("ownerID"), dr("ContentScopeType")) Then
                            childNodeID = GetChildNodeID(dr("ownerID"), dr("ContentScopeType"))
                            childNode = GetChildNodeName(dr("ownerID"), dr("ContentScopeType"))
                            If childCnt > 0 Then
                                _treeStr = _treeStr & "	</table></td></tr>"
                            End If

                            BuildChildNodeHtml(nodeID & "_" & childNodeID, childNode)
                            childCnt = childCnt + 1
                        End If
                    End If
                    BuildLeafHtml(dr)
                    cnt = cnt + 1

                Next
            Case 2
                'group by fiscal week
                For Each dr In ds.Tables(0).Rows

                    If nodeID <> dr("fiscalWeek") Then
                        If childCnt > 0 Then
                            _treeStr = _treeStr & "	</table></td></tr>"
                        End If
                        If cnt > 0 Then
                            _treeStr = _treeStr & "	</table></td></tr>"
                        End If
                        nodeName = "Week " & dr("ReportDate")
                        nodeID = dr("fiscalWeek")
                        BuildNodeHtml(nodeID, nodeName)
                        childNodeID = ""
                        childCnt = 0
                    End If
                    'SRI 8/7/2003 don't build child node if scope equal/higher than user role eg. for dm build child nodes if scope is store.
                    If dr("ContentScopeType") > _userScope Then
                        If childNodeID <> GetChildNodeID(dr("ownerID"), dr("ContentScopeType")) Then
                            childNodeID = GetChildNodeID(dr("ownerID"), dr("ContentScopeType"))
                            childNode = GetChildNodeName(dr("ownerID"), dr("ContentScopeType"))
                            If childCnt > 0 Then
                                _treeStr = _treeStr & "	</table></td></tr>"
                            End If

                            BuildChildNodeHtml(nodeID & "_" & childNodeID, childNode)
                            childCnt = childCnt + 1
                        End If
                    End If
                    BuildLeafHtmlByWeek(dr)
                    cnt = cnt + 1

                Next

        End Select

    End Sub
    Private Sub BuildNodeHtml(ByVal nodeID As String, ByVal nodeName As String)
        _treeStr = _treeStr & "<TR><TD width=17  valign=top>&nbsp;</TD><TD align=right width=17><IMG onclick=javascript:showtree('" & nodeID & "') height=11 src='../images/Plus" & _treeImgSfx & ".jpg' width=17 border=0 name=I" & nodeID & "></TD>" & _
                    "<TD width=685  class=cellValueCaption>" & nodeName & "</TD>" & _
                    "<TD width=13>&nbsp;</TD></TR><TR><TD colSpan=5><TABLE id=S" & nodeID & " style='DISPLAY: none' cellSpacing=0 cellPadding=0 width=100% border=0>"

    End Sub
    Private Sub BuildChildNodeHtml(ByVal nodeID As String, ByVal nodeName As String)
        _treeStr = _treeStr & "<TR><TD width=17>&nbsp;</TD><TD width=17><IMG src='../images/Line2" & _treeImgSfx & ".jpg' border=0></TD><TD align=right width=17><IMG onclick=javascript:showtree('" & nodeID & "') height=11 src='../images/Plus" & _treeImgSfx & ".jpg' width=17 border=0 name=I" & nodeID & "></TD>" & _
                    "<TD width=685  class=cellValueCaption>" & nodeName & "</TD>" & _
                    "<TD width=13>&nbsp;</TD></TR><TR><TD colSpan=5><TABLE id=S" & nodeID & " style='DISPLAY: none' cellSpacing=0 cellPadding=0 width=100% border=0>"

    End Sub
    Private Sub BuildLeafHtml(ByVal dr As DataRow)
        Dim desc As String = ""
        Dim newFile As String = ""
        If dr("isAccessed") = 0 Then
            newFile = "&nbsp;<img src='images/new.gif' border=0>&nbsp;"
        Else
            newFile = ""
        End If
        _treeStr = _treeStr & "<TR><TD width=14>&nbsp;</TD><TD width=17><IMG src='../images/Line4" & _treeImgSfx & ".jpg' border=0></TD><TD width=17><IMG src='../images/Line2" & _treeImgSfx & ".jpg' border=0></TD>" & _
                                "<TD colspan=2 class='cellvaluewhite'><IMG src='../images/Line3_1" & _treeImgSfx & ".jpg' border=0>" & _
                                 newFile & "<A href='ViewReportFile.aspx?reportFileId=" & dr("ReportFileId") & "' target=_blank><FONT class='cellvaluewhite'>week " & dr("ReportDate") & "</FONT></A>"
        If Len(dr("Comment")) > 65 Then
            _treeStr = _treeStr & "&nbsp;-&nbsp;" & Left(dr("Comment"), 65) & " ...<a href=""javascript:OpenPop('More.aspx?ReportId=" & dr("reportId") & "&submissionID=" & dr("ReportSubmissionId") & "',550,350)"" ><FONT color=#0099cc>more</font></a></TD>"
        Else
            _treeStr = _treeStr & "&nbsp;-&nbsp;" & dr("Comment") & "</TD>"
        End If
        _treeStr = _treeStr & "</tr>"


    End Sub
    Private Sub BuildLeafHtmlByWeek(ByVal dr As DataRow)
        Dim desc As String = ""
        Dim newFile As String = ""
        If dr("isAccessed") = 0 Then
            newFile = "&nbsp;<img src='images/new.gif' border=0>&nbsp;"
        Else
            newFile = ""
        End If
        _treeStr = _treeStr & "<TR><TD width=14>&nbsp;</TD><TD width=17><IMG src='../images/Line4" & _treeImgSfx & ".jpg' border=0></TD><TD width=17><IMG src='../images/Line2" & _treeImgSfx & ".jpg' border=0></TD>" & _
                                "<TD colspan=2 class='cellvaluewhite'><IMG src='../images/Line3_1" & _treeImgSfx & ".jpg' border=0>" & _
                                 newFile & "<A href='ViewReportFile.aspx?reportFileId=" & dr("ReportFileId") & "' target=_blank><FONT class='cellvaluewhite'>" & dr("ReportTitle") & "</FONT></A>"
        If Len(dr("Comment")) > 65 Then
            _treeStr = _treeStr & "&nbsp;-&nbsp;" & Left(dr("Comment"), 65) & " ...<a href=""javascript:OpenPop('More.aspx?ReportId=" & dr("reportId") & "&submissionID=" & dr("ReportSubmissionId") & "',550,350)"" ><FONT color=#0099cc>more</font></a></TD>"
        Else
            _treeStr = _treeStr & "&nbsp;-&nbsp;" & dr("Comment") & "</TD>"
        End If
        _treeStr = _treeStr & "</tr>"


    End Sub

    Private Sub lstGrpBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstGrpBy.SelectedIndexChanged
        If lstGrpBy.SelectedItem.Value = 3 Then
            Server.Transfer("CalendarView.aspx")
        Else
            _grpBy = lstGrpBy.SelectedItem.Value
            BuildTree()
        End If
    End Sub
    Private Function GetChildNodeID(ByVal ownerId As String, ByVal contentScope As Integer) As String
        Return contentScope & "_" & ownerId
    End Function
    Private Function GetChildNodeName(ByVal ownerId As String, ByVal contentScope As Integer) As String
        Dim nme As String
        Select Case contentScope
            Case 1
                Select Case ownerId
                    Case "1"
                        nme = "HotTopic"
                    Case "2"
                        nme = "Torrid"
                End Select
                'nme = "Company_" & ownerId
            Case 2
                nme = "Region_" & ownerId
            Case 3
                nme = "District_" & ownerId
            Case 4
                nme = "Store_" & ownerId
            Case Else
                nme = "Unknown_" & ownerId
        End Select
        Return nme
    End Function


    Private Sub SetShkReferPermission(ByVal StoreNum As Short, ByVal RoleId As Short, ByVal JobCode As String)
        Dim _shkReferUrl As String = "#"
        Dim _shkAccReferUrl As String
        Dim _shkEmpReferUrl As String
        If Not IsNothing(Application("ShkReferRptDir")) Then
            _shkReferUrl = Application("ShkReferRptDir").Trim
            If Not _shkReferUrl.EndsWith("/") Then
                _shkReferUrl &= "/"
            End If
        End If

        _shkEmpReferUrl = _shkReferUrl & "All/Shockhound Employee Referrals.xls"

        _shkAccReferUrl = _shkReferUrl & "All/Shockhound Account Referrals.xls"

        shkReferUrl.NavigateUrl = "javascript:OpenPop('" & _shkAccReferUrl & "',600,550)"
        shkEmpReferUrl.NavigateUrl = "javascript:OpenPop('" & _shkEmpReferUrl & "',600,550)"
    End Sub

    'Added by Davendar on 11/19/2009 for HT+1 Capture Rate Rpt Security
    Private Sub SetHTCapturePermission(ByVal StoreNum As Short, ByVal RoleId As Short, ByVal JobCode As String)
        
        lnkHTPlus.NavigateUrl = "javascript:OpenPop('WTDReportMain.aspx?RegionId=" & _sessionUser.SessionUser.RegionId.ToString.PadLeft(2, "0") & "&RptType=5',600,550)"

        If (RoleId = UserRole.DM Or RoleId = UserRole.RM) Then

            lnkHTPlus.Visible = True
        Else
            lnkHTPlus.Visible = False

        End If

    End Sub

End Class
