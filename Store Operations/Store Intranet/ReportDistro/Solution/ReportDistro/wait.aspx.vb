Public Class wait
    Inherits System.Web.UI.Page
    Protected PanelForm As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim strForm As New System.Text.StringBuilder()
        Dim k As String
        Dim viewStateAdded As Boolean
        strForm.Append("<FORM name=""Form1"" id=""Form1"" method=""post"">" & vbCrLf)
        For Each k In Request.QueryString.Keys
            If k <> "__VIEWSTATE" Then
                strForm.Append("<Input type='hidden' id='" & k & "' name='" & k & "' value='" & Request.QueryString.Item(k) & "'>" & vbCrLf)
            End If
        Next
       

        strForm.Append("</FORM>")
        PanelForm.InnerHtml = strForm.ToString()

       
    End Sub

End Class
