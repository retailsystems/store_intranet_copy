<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ArchivedReports.aspx.vb" Inherits="ReportDistro.ArchivedReports"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ArchivedReports</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="<%=Application("StyleSheet")%>" type="text/css" rel="stylesheet">
	</HEAD>
	<body bgcolor="black" text="white" vlink="white" alink="white" link="white" face="arial">
		<form id="Form1" method="post" runat="server">
		<asp:Label Runat="server" ID="lblErrMsg" Visible="False" CssClass="errStyle"></asp:Label>
			<table width="100%">
				<tr>
					<td>
						<TABLE cellSpacing="0" cellPadding="0" width="100%">
							<TR>
								<TD class="cellvaluecaption">Report Title &nbsp;
									<asp:DropDownList id="lstTitle" Width="160" CssClass="cellvalueleft" Runat="server"></asp:DropDownList>&nbsp;&nbsp;Fiscal 
									Week Date&nbsp;
									<asp:TextBox id="txtFiscalDate" Width="60" CssClass="cellvalueleft" Runat="server"></asp:TextBox>
									&nbsp;&nbsp;<asp:Button id="btnSearch" CssClass="B1" Runat="server" Text="Search"></asp:Button>&nbsp;
									&nbsp;&nbsp;<asp:Button id="btnClear" CausesValidation="False" CssClass="B1" Runat="server" Text="Clear"></asp:Button>
								</TD>
							</TR>
							<tr>
								<td>
									<asp:RequiredFieldValidator id="RequiredFieldValidator1" CssClass="errStyle" ErrorMessage="Please select a report title" runat="server" Display="Dynamic" ControlToValidate="lstTitle"></asp:RequiredFieldValidator>
									<asp:CompareValidator id="CompareValidator1" CssClass="errStyle" ErrorMessage="Invalid Date" runat="server" Display="Dynamic" ControlToValidate="txtFiscalDate" Type="Date" Operator="DataTypeCheck"></asp:CompareValidator>
								</td>
							</tr>
						</TABLE>
					</td>
				</tr>
				<tr height=5><td></td></tr>
				<tr>
					<td>
						<asp:DataGrid ID="dgReport" Width="650" Runat="server" GridLines=Vertical CellPadding="1" HeaderStyle-CssClass="DATAGRID_HeaderSm" CssClass="DatagridSm" ItemStyle-CssClass="BC111111sm" AlternatingItemStyle-CssClass="BC333333sm" AutoGenerateColumns="false" AllowPaging="True" PageSize=10 AllowSorting="True" PagerStyle-Mode="NumericPages" PagerStyle-CssClass="pagerStyle">
						<Columns>
								
								<asp:TemplateColumn HeaderText="FiscalWeek" SortExpression="ReportDate"  HeaderStyle-Width="75px">
									<ItemTemplate>
										<a href="ViewReportFile.aspx?ReportFileId=<%# Container.DataItem("ReportFileID")%>" target=_blank ><%# Container.DataItem("ReportDate")%></a>
									</ItemTemplate>
								</asp:TemplateColumn>		
								<asp:BoundColumn DataField="Owner" HeaderStyle-Width="75px" SortExpression="Owner" HeaderText="Owner"></asp:BoundColumn>					
								<asp:BoundColumn DataField="Comment" HeaderStyle-Width="500px" SortExpression="Comment" HeaderText="Comment"></asp:BoundColumn>
								
							</Columns>
						</asp:DataGrid>
						<asp:Label ID="SortBy" Visible="False" Runat="server"></asp:Label>
						<asp:Label ID="SortDirection" Visible="False" Runat="server"></asp:Label>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
