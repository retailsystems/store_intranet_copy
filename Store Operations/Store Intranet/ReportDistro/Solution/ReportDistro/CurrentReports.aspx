<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CurrentReports.aspx.vb" Inherits="ReportDistro.CurrentReports"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CurrentReports</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<%
if request("role") = "dm" then
	response.redirect ("currentReportsDm.aspx?role=dm")
end if
%>
		<LINK href='<%=Application("StyleSheet")%>' type=text/css rel=stylesheet>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<%
			Response.Write("<SCRIPT language='Javascript'>" )
			Response.Write("var plusImg = '../images/Plus" & _treeImgSfx & ".jpg';")
			Response.Write("var minusImg = '../images/Minus" & _treeImgSfx & ".jpg';")
			Response.Write("</SCRIPT>")
		%>
		<SCRIPT language="Javascript">



function showtree(lngSection){
	if (eval('S' + lngSection).style.display == 'none'){
		eval('S' + lngSection).style.display = 'block';
		eval('document.Form1.I' + lngSection).src = minusImg;
	}else{
		eval('S' + lngSection).style.display = 'none';
		eval('document.Form1.I' + lngSection).src = plusImg;
	}
}
function ShowReport(){
	var id = document.Form1.grpBy[document.Form1.grpBy.selectedIndex].value;
	if(id == "1"){
		document.all.grp1.style.display = 'inline';
		document.all.grp2.style.display = 'none';	
	}
	if(id == "2"){
		document.all.grp1.style.display = 'none';
		document.all.grp2.style.display = 'inline';	
	}
}
function ShowGrid(flg){
	if (flg == 1){
	document.all.gridDiv.style.display = 'inline';	
	}else{
	document.all.gridDiv.style.display = 'none';	
	}
}
		</SCRIPT>
		<script language="javascript">
			 function OpenPop(url,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,addressbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,'cw',winProp);
		   }
		   	 function OpenPop1(url,cw,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,addressbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,cw,winProp);
		   }
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:label id="lblErrMsg" CssClass="errStyle" Visible="False" Runat="server"></asp:label>
			<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<TBODY>
					<tr>
						<td height="5"></td>
					</tr>
					<TR>
						<TD class="cellvaluecaption" vAlign="top">&nbsp;&nbsp;
							<%=RptCaption%>
							Reports By
							<asp:dropdownlist id="lstGrpBy" Runat="server" AutoPostBack="True">
								<asp:ListItem Value="1" Selected="True">Report Title</asp:ListItem>
								<asp:ListItem Value="2">Fiscal Week</asp:ListItem>
							</asp:dropdownlist></FONT> 
							<!--asp:ListItem Value="3">Calendar View<--/asp:ListItem--></TD>
						<td vAlign="top" align="right" width="150" rowSpan="3">
							<table class="TableBorder" height="200" cellSpacing="0" cellPadding="2" width="150" align="center"
								border="0">
								<tr height="15">
									<td class="Redbg"><font class="cellvaluecaption">&nbsp;Daily Reports</font>
									</td>
								</tr>
								<tr height="5">
									<td></td>
								</tr>
								<tr>
									<td vAlign="top">
										<table borderColor="#990000" cellSpacing="0" cellPadding="0" border="0">
											<tr>
												<td width="5"></td>
												<td>
													<%
													Dim _salesPlanUrl as string = "#"
													If Not IsNothing(Application("DailySalesPlanDir")) Then
														_salesPlanUrl = Application("DailySalesPlanDir").Trim
														If Not _salesPlanUrl.EndsWith("\") Then
															_salesPlanUrl &= "\"
														End If
														_salesPlanUrl = _salesPlanUrl.Replace("\","\\")
														_salesPlanUrl &= "Store " & Session("SessionUser").StoreNum.ToString.PadLeft(4, "0") & ".xls"
														'_salesPlanUrl = "ViewReportFile.aspx?FilePath=" & _salesPlanUrl
													End If
													%>
													<A class=cellvaluecaption href="javascript:OpenPop('<%=_salesPlanUrl%>',700,700)" >Sales 
														Plan</A>
													<br>
													<asp:hyperlink id="linkWTD" CssClass="cellvaluecaption" Visible="False" Runat="server" text="WTD By Department"></asp:hyperlink>
													<%IF Not IsNothing(Application("ShowApReport")) AndAlso Application("ShowApReport").ToUpper = "TRUE" THEN%>
													<br>
													<!--11/09/2004 AP Report renamed as Pulse Report --><A class="cellvaluecaption" href="javascript:OpenPop1('WTDReportMain.aspx?RptType=3','wtd',790,590)">Pulse 
														Report</A>
														
													<%End IF%>
										<br>
													<asp:hyperlink id="lnkKiosk" runat="server" Visible="False" CssClass="cellvaluecaption" text="Ship to Store"></asp:hyperlink>
													<%IF Not IsNothing(Application("ShowShkReport")) AndAlso Application("ShowShkReport").ToUpper = "TRUE" THEN%>
													<br>
													<asp:hyperlink id="shkReferUrl" runat="server" Visible="True" CssClass="cellvaluecaption" text="Shk Referral Report"></asp:hyperlink>
									
													<%End IF%>
													<!--A class=cellvaluecaption href="javascript:OpenPop('_shkReferUrl>',700,700)" >Shockhound Referral Report</A-->
													<!--<%IF Not IsNothing(Application("ShowHTReport")) AndAlso Application("ShowHTReport").ToUpper = "TRUE" THEN%>
													<br>
													<asp:hyperlink id="lnkHTPlus" runat="server" Visible="False" CssClass="cellvaluecaption" text="HT+1 Capture WTD"></asp:hyperlink>
												
													<%End IF%> -->
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr height="5">
									<td></td>
								</tr>
							</table>
						</td>
					</TR>
					<tr>
						<td height="5"></td>
					</tr>
					<tr>
						<td vAlign="top" width="100%">
							<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<%=_treeStr%>
							</TABLE>
						</td>
					</tr>
				</TBODY>
			</TABLE>
		</form>
	</body>
</HTML>
