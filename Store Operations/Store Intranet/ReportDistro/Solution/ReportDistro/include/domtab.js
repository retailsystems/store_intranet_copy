/*
	Function domTab()
	written by Christian Heilmann
*/
function domTab(i, mode){
	// Variables for customisation:
	var numberOfTabs = 2;
	var colourOfInactiveTab, colourOfActiveTab, colourOfInactiveLink, colourOfActiveLink;
	if (mode=="torrid"){
		colourOfInactiveTab = "#FAE5F0";
		colourOfActiveTab = "#FF9ACE";
		colourOfInactiveLink = "#333333";
		colourOfActiveLink = "#333333";
	}
	else{
		colourOfInactiveTab = "#ff6633";
		colourOfActiveTab = "#ff0000";
		colourOfInactiveLink = "#333333";
		colourOfActiveLink = "#ffffcc";
	}
	/*
	var colourOfInactiveTab = "#ff6633";
	var colourOfActiveTab = "#ff0000";
	var colourOfInactiveLink = "#333333";
	var colourOfActiveLink = "#ffffcc";
	
	var colourOfInactiveTab = "#FAE5F0";
	var colourOfActiveTab = "#FF9ACE";
	var colourOfInactiveLink = "#333333";
	var colourOfActiveLink = "#333333";
	*/
	// end variables
	if (document.getElementById){
		for (f=1;f<numberOfTabs+1;f++){
			document.getElementById('contentblock'+f).style.display='none';
			document.getElementById('link'+f).style.background=colourOfInactiveTab;
			document.getElementById('link'+f).style.color=colourOfInactiveLink;
		}
		document.getElementById('contentblock'+i).style.display='block';
		document.getElementById('link'+i).style.background=colourOfActiveTab;
		document.getElementById('link'+i).style.color=colourOfActiveLink;
	}
}
