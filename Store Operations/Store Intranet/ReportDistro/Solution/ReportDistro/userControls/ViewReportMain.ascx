<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ViewReportMain.ascx.vb" Inherits="ReportDistro.ViewReportMain" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table align="center" width="600" border="0" cellpadding="2" cellspacing="0" style="BORDER-RIGHT: crimson 1px solid; BORDER-TOP: crimson 1px solid; BORDER-LEFT: crimson 1px solid; BORDER-BOTTOM: crimson 1px solid">
	<tr>
		<td colspan="4" bgcolor="crimson" align="right" valign="center">
			<input type="button" name="btnEdit" value="Update" id="btnEdit" class="btnSmall" onclick="window.location='NewReportStep1.aspx?mode=update&ReportId=<%=reportID%>'">
		</td>
	</tr>
	<tr>
		<td class="cellvaluecaption" width="130">Report Title</td>
		<td class="cellvaluewhite" width="200"><asp:Label ID="lblTitle" runat="server"></asp:Label></td>
		<td class="cellvaluecaption" width="100">Report ID</td>
		<td class="cellvaluewhite"><asp:Label ID="lblReportId" runat="server"></asp:Label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Input Type</td>
		<td class="cellvaluewhite"><asp:Label ID="lblInputType" runat="server"></asp:Label></td>
		<td class="cellvaluecaption">Scope</td>
		<td class="cellvaluewhite"><asp:Label ID="lblScope" runat="server"></asp:Label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Staging Path</td>
		<td class="cellvaluewhite"><asp:Label ID="lblStagingPath" runat="server"></asp:Label></td>
		<td class="cellvaluecaption">Location Path</td>
		<td class="cellvaluewhite"><asp:Label ID="lblLocationPath" runat="server"></asp:Label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Default File</td>
		<td class="cellvaluewhite"><asp:Label ID="lblDefaultFile" runat="server"></asp:Label></td>
		<td class="cellvaluecaption">File Identifier</td>
		<td class="cellvaluewhite"><asp:Label ID="lblIdentifier" runat="server"></asp:Label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">File Format</td>
		<td class="cellvaluewhite"><asp:Label ID="lblFormat" runat="server"></asp:Label></td>
		<td class="cellvaluecaption">Max File Size</td>
		<td class="cellvaluewhite"><asp:Label ID="lblMaxFileSize" runat="server"></asp:Label>Kb.</td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Approver Email</td>
		<td class="cellvaluewhite"><asp:Label ID="lblApproverEmail" runat="server"></asp:Label></td>
		<td class="cellvaluecaption">Report Owners</td>
		<td class="cellvaluewhite"><asp:Label ID="rptOwners" runat="server"></asp:Label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Approval Reqd.By</td>
		<td class="cellvaluewhite"><asp:Label ID="lblApprovalType" runat="server"></asp:Label></td>
		<td class="cellvaluecaption">Readonly</td>
		<td class="cellvaluewhite"><asp:Label ID="lblReadOnly" runat="server"></asp:Label></td>
	</tr>
	<tr>
		<td class="cellvaluecaption">Expiry Type</td>
		<td class="cellvaluewhite" colspan="3"><asp:Label ID="lblExpiry" runat="server"></asp:Label>
			<asp:Label ID="lblArchive" runat="server"></asp:Label></td>
	</tr>
	<tr height="3">
		<td colspan="4"></td>
	</tr>
	<tr>
		<td class="cellvaluecaption" valign="top">Report Description</td>
		<td colspan="3" class="cellvaluewhite">
			<asp:Label ID="lblDesc" runat="server"></asp:Label>
		</td>
	</tr>
</table>
