<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ReportJobCodeOwners.ascx.vb" Inherits="ReportDistro.ReportJobCodeOwners" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table align="center" width="290" border="0" cellpadding="2" cellspacing="0" style="BORDER-RIGHT: crimson 1px solid; BORDER-TOP: crimson 1px solid; BORDER-LEFT: crimson 1px solid; BORDER-BOTTOM: crimson 1px solid">
	<tr height="15">
		<td bgcolor="crimson" class="cellvaluecaption">&nbsp;JobCode Owners</td>
		<td bgcolor="crimson" align="right" valign="center">
			<input type="button" name="btnEdit" value="Update" id="btnEdit" class="btnSmall" onClick="location.href='ManageReportOwners.aspx?ReportID=<%=reportID%>'">
		</td>
	</tr>
	<tr height="5">
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2">
			<table cellspacing="0" cellpadding="0" bordercolor="#990000" border="0">
				<tr>
					<td width="10"></td>
					<td>
						<asp:DataGrid width=275 ID="dgCodeOwners" Runat="server" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false">
							<Columns>
								<asp:BoundColumn DataField="CompanyName" HeaderText="Company"></asp:BoundColumn>
								<asp:BoundColumn DataField="jobCode" HeaderText="Job Code"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
					<td width="10"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
