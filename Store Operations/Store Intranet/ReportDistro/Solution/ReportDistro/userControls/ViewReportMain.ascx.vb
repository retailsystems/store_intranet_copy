Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity

Public MustInherit Class ViewReportMain
    Inherits System.Web.UI.UserControl
    Protected WithEvents lblTitle As System.Web.UI.WebControls.Label
    Protected WithEvents lblReportId As System.Web.UI.WebControls.Label
    Protected WithEvents lblDesc As System.Web.UI.WebControls.Label
    Protected WithEvents lblInputType As System.Web.UI.WebControls.Label
    Protected WithEvents lblScope As System.Web.UI.WebControls.Label
    Protected WithEvents lblStagingPath As System.Web.UI.WebControls.Label
    Protected WithEvents lblLocationPath As System.Web.UI.WebControls.Label
    Protected WithEvents lblDefaultFile As System.Web.UI.WebControls.Label
    Protected WithEvents lblIdentifier As System.Web.UI.WebControls.Label
    Protected WithEvents lblFormat As System.Web.UI.WebControls.Label
    Protected WithEvents lblMaxFileSize As System.Web.UI.WebControls.Label
    Protected WithEvents lblApproverEmail As System.Web.UI.WebControls.Label
    Protected WithEvents rptOwners As System.Web.UI.WebControls.Label
    Protected WithEvents lblApprovalType As System.Web.UI.WebControls.Label
    Protected WithEvents lblReadOnly As System.Web.UI.WebControls.Label
    Protected WithEvents lblExpiry As System.Web.UI.WebControls.Label
    Protected WithEvents lblArchive As System.Web.UI.WebControls.Label
    Public report As report
    Private _sessionUser As HotTopic.RD.Application.Session
    Protected reportId As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current, 1)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        If Not IsPostBack Then
            InitializeForm()
        End If
        
    End Sub
    Private Sub InitializeForm()
        Dim cacheMgr As CacheManager = _sessionUser.GetCacheManager()
        Dim lookUpds As DataSet
        Dim i As Integer = 0
        lookUpds = cacheMgr.ListLookups(ConfigurationSettings.AppSettings("LookupDependency"))
        With report
            reportId = .ReportId
            lblTitle.Text = .Title
            lblReportId.Text = .ReportId
            lblDesc.Text = .Description
            lblInputType.Text = GetLookUpText(lookUpds, "InPutType", .InputType)
            lblScope.Text = GetLookUpText(lookUpds, "ContentScope", .ContentScope)
            lblStagingPath.Text = .StagingPath
            lblLocationPath.Text = .LocationPath
            lblDefaultFile.Text = .DefaultFileName
            lblIdentifier.Text = GetLookUpText(lookUpds, "FileIdentifier", .DefaultFileIdentifier)
            lblFormat.Text = .FileFormat
            lblMaxFileSize.Text = .MaxFileSize
            lblApproverEmail.Text = .ApproverGroupEmail
            lblApprovalType.Text = GetLookUpText(lookUpds, "ApprovalType", .ApprovalType)
            For i = 0 To .CompanyOwners.Count - 1
                Dim company As CompanyOwner = .CompanyOwners(i)
                If Len(rptOwners.Text) > 0 Then
                    rptOwners.Text += ","
                End If
                rptOwners.Text += company.CompanyName
            Next
            If .ReadOnlyFlag = True Then
                lblReadOnly.Text = "True"
            Else
                lblReadOnly.Text = "False"
            End If
            lblExpiry.Text = GetLookUpText(lookUpds, "ExpirationType", .ExpirationType)
            If .ArchiveNth > 0 Then
                lblArchive.Text = " (Archives after " & .ArchiveNth & " weeks.)"
            End If
        End With
    End Sub
    Private Function GetLookUpText(ByVal lookUpds As DataSet, ByVal lookUpType As String, ByVal lookUpVal As String) As String
        Dim lookUpView As DataView = lookUpds.Tables(0).DefaultView
        Dim lookUpText As String = ""
        If Trim(lookUpType) <> "" And IsNumeric(lookUpVal) Then
            lookUpView.RowFilter = "lookUpType='" & lookUpType & "' and lookUpValue=" & lookUpVal
            If lookUpView.Count > 0 Then
                lookUpText = lookUpView.Item(0).Row.Item("lookUptext")
            End If
        End If
        Return lookUpText
    End Function

End Class
