Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Imports System.IO
Imports System.Data


Public Class ViewHTCaptureReport
    Inherits System.Web.UI.Page

    Private _sessionUser As HotTopic.RD.Application.Session



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dlArticleList As System.Web.UI.WebControls.DataList

    Protected WithEvents lblStoreInfo As System.Web.UI.WebControls.Label
    'Private _sessionUser As HotTopic.RD.Application.Session
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Protected dtFiles As DataTable


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       
        ' If Not Request.QueryString("StoreNo") Or Not Request.QueryString("DistrictId") Is Nothing Then
        Dim strMyStore As String
        Dim strMyDistrict As String
        Dim strMyRegion As String
        strMyStore = Request.QueryString("StoreNo")
        strMyDistrict = Request.QueryString("DistrictId")
        strMyRegion = Request.QueryString("RegionId")
        dtFiles = New DataTable
        Dim dirInfo As DirectoryInfo
        Dim strMyPath As String
        Dim subDir As String

        Dim i As Integer
        Dim fi As FileInfo()

        Dim upper As Integer

        If Not Request.QueryString("StoreNo") And Not Request.QueryString("DistrictId") Is Nothing Then

            dirInfo = New DirectoryInfo(Server.MapPath(Application("HTCaptureRptDir") & "/District/"))
            Me.lblStoreInfo.Text = "HT Capture Report for District: " & strMyDistrict
            strMyPath = "*_" & strMyDistrict & "_*.xls"
            fi = dirInfo.GetFiles(strMyPath)
            Array.Sort(fi, New CompareFileInfoEntries)
            subDir += "/District/"

        ElseIf Not Request.QueryString("DistrictId") And Not Request.QueryString("RegionId") Is Nothing Then
            dirInfo = New DirectoryInfo(Server.MapPath(Application("HTCaptureRptDir") & "/Region/"))
            Me.lblStoreInfo.Text = "HT Capture Report for Region: " & strMyRegion
            strMyPath = "*_" & strMyRegion & "_*.xls"
            fi = dirInfo.GetFiles(strMyPath)
            Array.Sort(fi, New CompareFileInfoEntries)
            subDir += "/Region/"
        End If

        Dim dc1 As DataColumn
        Dim dc2 As DataColumn
        Dim dc3 As DataColumn

        dc1 = New DataColumn("FileName", System.Type.GetType("System.String"))
        dc2 = New DataColumn("TextName", System.Type.GetType("System.String"))
        dc3 = New DataColumn("Created", System.Type.GetType("System.String"))

        dtFiles.Columns.Add(dc1)
        dtFiles.Columns.Add(dc2)
        dtFiles.Columns.Add(dc3)

        If fi.Length >= 9 Then
            upper = 9
        Else
            upper = fi.Length - 1
        End If


        For i = 0 To upper - 1
            Dim dro As DataRow
            dro = dtFiles.NewRow()
            dro("FileName") = Application("HTCaptureRptDir") & subDir & fi(i).Name

            Dim reportDate As String
            reportDate = (fi(i).Name.Substring(fi(i).Name.Length - 12, 8))
            reportDate = reportDate.Substring(4, 2) & "/" & reportDate.Substring(6, 2) & "/" & reportDate.Substring(0, 4)
            dro("TextName") = "HT Capture Report WTD " & reportDate
            dro("Created") = fi(i).CreationTime.ToShortDateString
            dtFiles.Rows.Add(dro)

        Next

        dlArticleList.DataSource = dtFiles
        ' dtFiles.DefaultView.Sort = "Created DESC"

        dlArticleList.DataBind()
        dtFiles.Clear()
        dtFiles = Nothing
        dirInfo = Nothing
        ' End If

    End Sub

  

End Class

'Public Class CompareFileInfoEntries
'    Implements IComparer

    
'End Class
