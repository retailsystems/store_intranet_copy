<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SubmitReport.aspx.vb" Inherits="ReportDistro.SubmitReport1"%>
<HTML>
	<body>
		<!--#include file="../include/header.inc"-->
		<!--#include file="../include/calender.js"-->
		<iframe id="CalFrame" style="DISPLAY: none; Z-INDEX: 100; WIDTH: 148px; POSITION: absolute; HEIGHT: 194px" marginWidth="0" marginHeight="0" src="../include/calendar/calendar.htm" frameBorder="0" noResize scrolling="no"></iframe>
		<form id="Form1" method="post" runat="server">
			<INPUT type="hidden" runat="server" id="hidLocationPath"> <INPUT type="hidden" runat="server" id="hidStatus" NAME="Hidden1">
			<br>
			<table cellSpacing="0" cellPadding="0" width="500" border="0">
				<tr>
					<td class="pageCaption" align="middle" colSpan="3"><u>Submit Report</u></td>
				</tr>
				<tr height="15">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td colSpan="3">
						<asp:Label id="lblExMsg" runat="server" CssClass="ErrStyle"></asp:Label></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Report Title</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft"><asp:dropdownlist id="lstRptTitle" CssClass="cellvalueleft" Width="200px" AutoPostBack="True" Runat="server"></asp:dropdownlist><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" CssClass="ErrStyle" ControlToValidate="lstRptTitle" Display="Dynamic" ErrorMessage="Select a report title"></asp:requiredfieldvalidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Report Date</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft"><asp:textbox id="txtReportDate" CssClass="cellvalueleft" Width="140" Runat="server"></asp:textbox>&nbsp;<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar1,document.Form1.txtReportDate,null,0,330)"><IMG id="calendar1" src="../include/calendar/calendar.gif" border="0" name="calendar1" align="absBottom"></A>
						<asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" CssClass="ErrStyle" ControlToValidate="txtReportDate" Display="Dynamic" ErrorMessage="Report date is required."></asp:requiredfieldvalidator><asp:comparevalidator id="CompareValidator1" runat="server" CssClass="ErrStyle" ControlToValidate="txtReportDate" Display="Dynamic" ErrorMessage="Invalid Date." Type="Date" Operator="DataTypeCheck"></asp:comparevalidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">File(s) Location</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft"><asp:textbox id="txtStagingPath" CssClass="cellvalueleft" Width="200px" Runat="server"></asp:textbox></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Effectivity Date</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft"><asp:textbox id="txtEffectiveDt" CssClass="cellvalueleft" Width="140" Runat="server"></asp:textbox>&nbsp;<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar2,document.Form1.txtEffectiveDt,null,0,330)"><IMG id="calendar2" src="../include/calendar/calendar.gif" border="0" name="calendar2" align="absBottom"></A>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" CssClass="ErrStyle" ControlToValidate="txtEffectiveDt" Display="Dynamic" ErrorMessage="Effective date is required."></asp:requiredfieldvalidator><asp:comparevalidator id="Comparevalidator2" runat="server" CssClass="ErrStyle" ControlToValidate="txtEffectiveDt" Display="Dynamic" ErrorMessage="Invalid Date." Type="Date" Operator="DataTypeCheck"></asp:comparevalidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Expires On</td>
					<td class="reqStyle" width="10"></td>
					<td class="cellvalueleft"><asp:textbox id="txtExpiryDt" CssClass="cellvalueleft" Width="140" Runat="server"></asp:textbox>&nbsp;<A onclick="event.cancelBubble=true;" href="javascript:ShowCalendar(document.Form1.calendar3,document.Form1.txtExpiryDt,null,0,330)"><IMG id="calendar3" src="../include/calendar/calendar.gif" border="0" name="calendar3" align="absBottom"></A>
						<asp:comparevalidator id="Comparevalidator3" runat="server" CssClass="ErrStyle" ControlToValidate="txtExpiryDt" Display="Dynamic" ErrorMessage="Invalid Date." Type="Date" Operator="DataTypeCheck"></asp:comparevalidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" vAlign="top" width="110">Comments</td>
					<td width="10"></td>
					<td class="cellvalueleft"><asp:textbox id="txtComment" CssClass="cellvalueleft" Runat="server" TextMode="MultiLine" Rows="4" Columns="40"></asp:textbox></td>
				</tr>
			</table>
			<!-- end here-->
			<!--footer--> </TD></TR><tr>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td align="right"><asp:button id="btnSubmit" CssClass="B1" Runat="server" Text="Submit"></asp:button>&nbsp;&nbsp;<input class="B1" id="Button1" onclick="document.location='Home.htm';" type="button" value="Home" name="Home"></td>
			</tr>
			</TABLE></form>
	</body>
</HTML>
