Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Public Class SubmitReport1
    Inherits System.Web.UI.Page
    Protected WithEvents lstRptTitle As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtEffectiveDt As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtExpiryDt As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtComment As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents RequiredFieldValidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents CompareValidator1 As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Comparevalidator2 As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents Comparevalidator3 As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents txtReportDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtStagingPath As System.Web.UI.WebControls.TextBox
    Protected WithEvents hidLocationPath As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hidStatus As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblExMsg As System.Web.UI.WebControls.Label

    Private _sessionUser As HotTopic.RD.Application.Session

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current, 1)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try

        If Not IsPostBack Then
            InitializeForm()
        End If
    End Sub

    Private Sub InitializeForm()
        Dim ds As DataSet = _sessionUser.GetAllReportTitles()

        lstRptTitle.DataValueField = "ReportID"
        lstRptTitle.DataTextField = "ReportTitle"
        lstRptTitle.DataSource = ds
        lstRptTitle.DataBind()
        lstRptTitle.Items.Insert(0, New ListItem("---", ""))

    End Sub

    Private Sub lstRptTitle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstRptTitle.SelectedIndexChanged
        txtStagingPath.Text = ""
        hidLocationPath.Value = ""
        If lstRptTitle.SelectedIndex > 0 Then
            Dim rpt As Report = _sessionUser.GetReportOnly(Convert.ToInt32(lstRptTitle.SelectedItem.Value))

            txtStagingPath.Text = rpt.StagingPath
            hidLocationPath.Value = rpt.LocationPath
            If rpt.ApprovalType = ReportApprovalType.None Then
                hidStatus.Value = ReportStatus.Approved
            Else
                hidStatus.Value = ReportStatus.Pending
            End If
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim rptSubmission As Submission = New Submission(lstRptTitle.SelectedItem.Value)
        Try
            With rptSubmission
                .ReportSubmissionId = 0
                .StagingPath = txtStagingPath.Text
                .LocationPath = hidLocationPath.Value
                .ReportDate = txtReportDate.Text
                .EffectivityStartDate = txtEffectiveDt.Text
                If txtExpiryDt.Text.Trim.Length > 0 Then
                    .EffectivityEndDate = txtExpiryDt.Text
                End If
                .Comment = txtComment.Text
                .Status = hidStatus.Value
            End With
            _sessionUser.SubmitReport(rptSubmission)
            'go to lists
            Response.Redirect("ListReportSubmissions.aspx")
        Catch ex As RDAppException
            lblExMsg.Text = ex.Message
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class