<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CurrentReportsDM.aspx.vb" Inherits="ReportDistro.CurrentReportsDM" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CurrentReportsDM</title>
		<LINK href="<%=Application("StyleSheet")%>" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<%
			Response.Write("<SCRIPT language='Javascript'>" )
			Response.Write("var plusImg = '../images/Plus" & _treeImgSfx & ".jpg';")
			Response.Write("var minusImg = '../images/Minus" & _treeImgSfx & ".jpg';")
			Response.Write("</SCRIPT>")
		%>		
<SCRIPT language="Javascript">
function showtree(lngSection){
	//alert(lngSection);
	if (eval('S' + lngSection).style.display == 'none'){
		eval('S' + lngSection).style.display = 'block';
		eval('document.Form1.I' + lngSection).src = minusImg;
	}else{
		eval('S' + lngSection).style.display = 'none';
		eval('document.Form1.I' + lngSection).src =  plusImg;
	}
}
function ShowReport(){
	var id = document.Form1.grpBy[document.Form1.grpBy.selectedIndex].value;
	if(id == "1"){
		document.all.grp1.style.display = 'inline';
		document.all.grp2.style.display = 'none';	
	}
	if(id == "2"){
		document.all.grp1.style.display = 'none';
		document.all.grp2.style.display = 'inline';	
	}
}
function ShowGrid(flg){
	if (flg == 1){
	document.all.gridDiv.style.display = 'inline';	
	}else{
	document.all.gridDiv.style.display = 'none';	
	}
}
		</SCRIPT>
		<script language='javascript'>
			 function OpenPop(url,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'toolbar=no,status=no,menubar=yes,resizable=yes,directories=no,location=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,'cw',winProp);
		   }
		   	 function OpenPop1(url,cw,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'toolbar=no,status=no,menubar=yes,resizable=yes,directories=no,location=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,cw,winProp);
		   }
		</script>
		
	</HEAD>
	<body bgcolor="black" text="white" vlink="white" alink="white" link="white" face="arial">
		<form id="Form1" method="post" runat="server">
			<asp:Label Runat="server" ID="lblErrMsg" Visible="False" CssClass="errStyle"></asp:Label>
			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" ID="Table5" align="center">
				<TBODY>
					<tr>
						<td height="5"></td>
					</tr>
					<TR>
						<TD class="cellvaluecaption" valign=top height=15px>&nbsp;&nbsp;
							<%=RptCaption%>
							Reports By
							<asp:DropDownList ID="lstGrpBy" Runat="server" AutoPostBack="True">
								<asp:ListItem Value="1" Selected="True">Report Title</asp:ListItem>
								<asp:ListItem Value="2">Fiscal Week</asp:ListItem>
							</asp:DropDownList> 
							
						</TD>
					
						<td align="right" rowspan="3" valign="top" width="150">
							<table align="center" width="150px" height="200" border="0" cellpadding="2" cellspacing="0" class=TableBorder>
								<tr height="15">
									<td class="Redbg"><font class="cellvaluecaption">&nbsp;Daily Reports</font>
									</td>
								</tr>
								<tr height="5">
									<td></td>
								</tr>
								<tr>
									<td valign="top">
										<table cellspacing="0" cellpadding="0" bordercolor="#990000" border="0">
											<tr>
												<td width="5"></td>
												<td Class="cellvaluecaption">
													<a href="javascript:OpenPop1('WTDReportMain.aspx?RptType=2','wtd',750,700)"  class="cellvaluecaption">Sales Plan</a>
													<br>
													<a href="javascript:OpenPop1('WTDReportMain.aspx','wtd',750,550)"  class="cellvaluecaption">WTD By Department</a>
													<%IF Not IsNothing(Application("ShowApReport")) AndAlso Application("ShowApReport").ToUpper = "TRUE" THEN%>
													<br>
													<!--11/09/2004 AP Report renamed as Pulse Report -->
													<a href="javascript:OpenPop1('WTDReportMain.aspx?RptType=3','wtd',790,590)"  class="cellvaluecaption">Pulse Report</a>
													<%End IF%>
													<!--asp:HyperLink ID="linkWTD" CssClass="cellvaluecaption" text="WTD By Department" Visible="False"><--/asp:HyperLink-->
													<!--07/16/2009 View Shk Referral Report on hottopic page -->
													<%IF Not IsNothing(Application("ShowShkReport")) AndAlso Application("ShowShkReport").ToUpper = "TRUE" THEN%>
													<br>
													<asp:hyperlink id="shkReferUrl" runat="server" Visible="True" CssClass="cellvaluecaption" text="Shk Referral Report"></asp:hyperlink>
													<br>				
													<asp:hyperlink id="shkEmpReferUrl" runat="server" Visible="True" CssClass="cellvaluecaption" text="Shk Emp Referral Rpt"></asp:hyperlink>
													<%End IF%>
													<!--11/19/2009 View HT+1 Capture Report on hottopic page -->
													<%IF Not IsNothing(Application("ShowHTReport")) AndAlso Application("ShowHTReport").ToUpper = "TRUE" THEN%>
													<br>
													<asp:hyperlink id="lnkHTPlus" runat="server" Visible="False" CssClass="cellvaluecaption" text="HT+1 Capture WTD"></asp:hyperlink>
													<%End IF%>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr height="5">
									<td></td>
								</tr>
							</table>
						</td>
					</TR>
					<tr>
						<td height="2"></td>
					</tr>
					<tr>
						<td valign=top height=100px>
							<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" ID="Table6"  >
							
								<%=_treeStr%>
							
							</TABLE>
						</td>
					</tr>
					
				</TBODY>
			</TABLE>
		</form>
	</body>
</HTML>
