Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity
Public Class ViewReportFiles
    Inherits System.Web.UI.Page
    Protected WithEvents lblExMsg As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitle As System.Web.UI.WebControls.Label
    Protected WithEvents lblfiscalWeek As System.Web.UI.WebControls.Label
    Protected WithEvents lblCnt As System.Web.UI.WebControls.Label
    Protected WithEvents dgReport As System.Web.UI.WebControls.DataGrid

    Private _sessionUser As HotTopic.RD.Application.Session
    Private _reportId As Integer
    Private _submissionId As Integer
    Private _submission As Submission
    Protected WithEvents txtComments As System.Web.UI.WebControls.TextBox
    Private _fwDate As Date
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
            _reportId = Request("rid")
            _submissionId = Request("sid")
            _submission = New Submission(_reportId, _submissionId)
            If Not IsPostBack Then
                InitializeForm()
            End If
        Catch ex As RDAppException
            lblExMsg.Text = ex.Message
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try


    End Sub
    Private Sub InitializeForm()
        With _submission
            lblTitle.Text = .Title
            lblfiscalWeek.Text = .FiscalWeekEndingDate
            txtComments.Text = .Comment
        End With
        If Request("mode") = "comment" Then
            dgReport.Visible = False
        Else
            BindData()

        End If

    End Sub
    Private Sub BindData()
        Try
            Dim ds As DataSet
            'ds = _sessionUser.ListFiles(_submission)
            ds = _sessionUser.SearchFiles(_reportId, "", , _submissionId)
            dgReport.DataSource = ds
            If ds.Tables(0).Rows.Count = 1 Then
                Response.Redirect("ViewReportFile.aspx?reportFileId=" & ds.Tables(0).Rows(0).Item("reportFileId"))
            End If

            lblCnt.Text = ds.Tables(0).Rows.Count
            dgReport.DataBind()
        Catch ex As RDAppException
            lblExMsg.Text = ex.Message
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgReport_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgReport.PageIndexChanged
        dgReport.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub
    Public Function IsNew(ByVal isAccessed As String) As String
        Dim retStr As String = ""
        If isAccessed <> "1" Then
            retStr = "<IMG src='images/new.gif' border=0></IMG>"
        End If
        Return retStr
    End Function
End Class
