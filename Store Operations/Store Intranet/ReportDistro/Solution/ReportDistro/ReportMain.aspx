<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ReportMain.aspx.vb" Inherits="ReportDistro.ReportMain"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
	</HEAD>
	<% Response.CacheControl = "no-cache" %>
	<% Response.AddHeader("pragma", "no-cache") %>
	<% Response.Expires = -1 %>
	<%
		Response.Write("<SCRIPT language='Javascript'>")
		Response.Write(" var mode = '" &  Application("Mode") & "';")
		Response.Write("</SCRIPT>")
	%>
	<SCRIPT language="Javascript">



function showtree(lngSection){
	if (eval('S' + lngSection).style.display == 'none'){
		eval('S' + lngSection).style.display = 'block';
		eval('document.Form1.I' + lngSection).src = 'Images/Minus.jpg';
	}else{
		eval('S' + lngSection).style.display = 'none';
		eval('document.Form1.I' + lngSection).src = 'Images/Plus.jpg';
	}
}
function ShowReport(){
	var id = document.Form1.grpBy[document.Form1.grpBy.selectedIndex].value;
	if(id == "1"){
		document.all.grp1.style.display = 'inline';
		document.all.grp2.style.display = 'none';	
	}
	if(id == "2"){
		document.all.grp1.style.display = 'none';
		document.all.grp2.style.display = 'inline';	
	}
}
function ShowGrid(flg){
	if (flg == 1){
	document.all.gridDiv.style.display = 'inline';	
	}else{
	document.all.gridDiv.style.display = 'none';	
	}
}
	</SCRIPT>
	<script language='javascript'>
			 function OpenPop(url,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,'cw',winProp);
		   }
		   	 function OpenPop1(url,cw,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,cw,winProp);
		   }
	</script>
	<LINK href='<%=Application("StyleSheet")%>' type="text/css" rel="stylesheet">
		<script language="javascript" type="text/javascript" src="Include/domtab.js"></script>
</STYLE>
			<!--#include file="include/header.inc"-->
			<form id="Form1" method="post" runat="server">
			<table border='0' cellspacing='0' cellpadding='0' width='750' ID="Table3">
				<tr>
					<td colspan="4">
						<asp:Label Runat="server" ID="lblErrMsg" Visible="False" CssClass="errStyle"></asp:Label>
					</td>
				</tr>
				<%if not blnLoginFail then%>
				<tr>
					<td colspan="4">
						<!--
	These are the tabs. Make sure that each of them has the correct id,
		target and corresponding number in the domTab() call.
	 -->
						<a href="#target1" onclick="domTab(1,mode)" id="link1" class="tablink">Current 
							Reports</a> <a href="#target2" onclick="domTab(2,mode)" id="link2" class="tablink">
							Archived Reports</a>
						<div id="contentblock1" class="tab" style="WIDTH:750px;HEIGHT:390px">
							<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
								<tr height="5" class="tabTD">
									<td width="100%" valign="top"></td>
								</tr>
							</table>
							<a name="target1"></a>
							<!-- current report  tree view control-->
							<iframe width="100%" name="IF1" id="IF1" src='wait.aspx?role=<%=request("role")%>&amp;FormAction=<%=FormAction%>' height="98%" frameborder="0" scrolling="auto">
							</iframe>
						</div>
						<div id="contentblock2" class="tab" style="DISPLAY:none;WIDTH:750px;HEIGHT:390px">
							<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
								<tr height="5" class="tabTD">
									<td width="100%" valign="top"></td>
								</tr>
							</table>
							<a name="target2"></a>
							<iframe width="100%" name="IF2" id="IF2" src="ArchivedReports.aspx" height="98%" frameborder="0"
								scrolling="auto"></iframe>
						</div>
					</td>
				</tr>
				<%end if%>
			</table>
			</TD></TR> 
			<!--tr >
					<td colspan=4  height=2><hr color='#990000' >
					</td>
					
				</tr-->
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td align='right'><input class='B1' type='button' name='Home' value='Home' onClick="document.location='logOut.aspx';"
						ID="Button1"></td>
			</tr>
			</TABLE>
		</form>
			<%if not blnLoginFail then%>
			<script>
	function intialize(){
	domTab(1,mode);
	
	//set tab div heights based on resolution
	document.all.contentblock1.style.height = window.screen.height - 335;
	document.all.contentblock2.style.height = window.screen.height - 335;
	
	}
	window.onload=intialize;
		</script>
			<%end if%>
</HTML>
