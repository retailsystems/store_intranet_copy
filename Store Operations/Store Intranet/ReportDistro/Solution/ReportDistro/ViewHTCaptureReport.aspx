<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ViewHTCaptureReport.aspx.vb" Inherits="ReportDistro.ViewHTCaptureReport"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ViewHTCaptureReport</title>
		<LINK href='<%=Application("StyleSheet")%>' type=text/css rel=stylesheet>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<br>
			<TABLE cellSpacing="0" cellPadding="0" width="90%" align="center">
				<TBODY>
					<tr>
						<td align="center" height="30"><asp:label id="lblStoreInfo" runat="server" CssClass="cellvaluecaption"></asp:label></td>
					</tr>
					<tr>
						<td align="center"><asp:datalist id="dlArticleList" runat="server" RepeatLayout="Table" RepeatDirection="Vertical"
								CellPadding="0" GridLines="None">
								<HeaderTemplate>
									<table class="TableBorder" cellpadding="2" cellspacing="0">
										<tr class="RedBG">
											<td>
												<font class="cellvaluecaption">Report Name</font>
											</td>
											<td>
												<font class="cellvaluecaption">Creation Date</font>
											</td>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr height="25">
										<td>
											<a target="_blank"   class="cellvaluecaption" href='<%#Container.DataItem("FileName")%>'>
												<%#Container.DataItem("TextName")%>
											</a>
										</td>
										<td>
											<font class="cellvaluecaption">
												<%#Container.DataItem("Created")%>
											</font>
										</td>
									</tr>
								</ItemTemplate>
								<FooterTemplate>
			</TABLE>
			</FooterTemplate> </asp:datalist></form>
		</TD></TR></TBODY></TABLE>
	</body>
</HTML>
