Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity

Public Class WTDReportLeftFrame
    Inherits System.Web.UI.Page
    Private _sessionUser As HotTopic.RD.Application.Session
    Protected _treeStr As String
    Protected WithEvents lblErrMsg As System.Web.UI.WebControls.Label
    Protected WithEvents hdnAbsFileDir As System.Web.UI.HtmlControls.HtmlInputHidden
    Private _grpBy As Integer = 1

    Protected rptCaption As String = "WTD By Department"
    Private _userScope As Integer
    Protected StartupUrl, StartUpnode As String
    '6/1/04 tree image variables
    Protected _treeImgSfx As String = ""
    Private _salesPlanUrl As String = ""
    Private _HTPlusUrl As String = ""
    Private _reportType As DailyReportType = DailyReportType.WTD

    Private Enum DailyReportType
        WTD = 1
        SalesPlan = 2
        APReport = 3
        Kiosk = 4
        HTPlus = 5
    End Enum
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            lblErrMsg.Text = ""
            lblErrMsg.Visible = False
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current)
            '6/1/04 initialize the _treeImgSfx
            If Not IsNothing(Application("Mode")) AndAlso Application("Mode").toUpper = "TORRID" Then
                _treeImgSfx = "_TD"
            End If
            If Not IsNothing(Request("RptType")) AndAlso Request("RptType").Trim.Length > 0 Then
                Select Case Request("RptType").Trim
                    Case "5"
                        _reportType = DailyReportType.HTPlus
                        If Not IsNothing(Application("HTCaptureRptDir")) Then
                            _HTPlusUrl = Application("HTCaptureRptDir").Trim
                            If Not _HTPlusUrl.EndsWith("/") Then
                                _HTPlusUrl &= "/"
                            End If
                        End If
                        rptCaption = "HT+1 Capture WTD"
                        hdnAbsFileDir.Value = Application("HTCaptureRptDir")
                    Case "4"
                        _reportType = DailyReportType.Kiosk
                        rptCaption = "Kiosk Daily Report"
                        hdnAbsFileDir.Value = Application("KioskAbsDir")
                    Case "3"
                        _reportType = DailyReportType.APReport
                        rptCaption = "AP Report"
                        hdnAbsFileDir.Value = Application("ApReportAbsDir")
                    Case "2"
                        _reportType = DailyReportType.SalesPlan
                        If Not IsNothing(Application("DailySalesPlanDir")) Then
                            _salesPlanUrl = Application("DailySalesPlanDir").Trim
                            If Not _salesPlanUrl.EndsWith("/") Then
                                _salesPlanUrl &= "/"
                            End If
                        End If
                        rptCaption = "Sales Plan"
                        hdnAbsFileDir.Value = Application("DailySalesAbsDir")
                    Case Else
                        _reportType = DailyReportType.WTD
                        rptCaption = "WTD By Department"
                End Select
            End If
           
            If Not IsPostBack Then
                If _reportType = DailyReportType.APReport Then
                    BuildApReportTree()
                ElseIf _reportType = DailyReportType.Kiosk Then
                    BuildKioskReportTree()
                ElseIf _reportType = DailyReportType.HTPlus Then
                    BuildHTCaptureRptTree()
                Else
                    BuildTree()
                End If
            End If
        Catch ex As RDAppException
            If ex.Message = "Unauthenticated user." Then
                Dim jsStr As String = ""
                jsStr += "<script>"
                jsStr += "alert('Active Session Expired!');"
                jsStr += "window.top.close();"
                jsStr += "</script>"
                Response.Write(jsStr)
                Response.End()
            End If
            lblErrMsg.Text = ex.Message
            lblErrMsg.Visible = True
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try

    End Sub
    Private Sub BuildApReportTree()
        _treeStr = ""
        Dim ds As DataSet
        Dim nodeName, nodeID, childNodeID As String
        Dim cnt As Integer = 0
        Dim dr As DataRow
        Dim nodeUrl As String = ""
        Dim leafUrl, leafName As String
        StartupUrl = "FileNotFound.htm"
        '_salesPlanUrl = _salesPlanUrl.Replace("\", "\\")
        Select Case _sessionUser.SessionUser.CurrentUserRoleId


            'Case UserRole.MLU
            '    ds = _sessionUser.ListMLUDistricts
            '    For Each dr In ds.Tables(0).Rows

            '        nodeName = "<FONT class='cellvaluewhite'>Region " & dr("RegionID") & "</FONT></a>"
            '        leafName = "District " & dr("DistrictID").ToString.PadLeft(3, "0")
            '        childNodeID = dr("DistrictID").ToString.PadLeft(3, "0")
            '        If nodeID <> dr("RegionID") Then
            '            nodeID = dr("RegionID")
            '            If cnt > 0 Then
            '                _treeStr = _treeStr & "	</table></td></tr>"
            '            Else
            '                StartupUrl = GetApReportStartUpUrl(nodeID, "REGION")
            '                StartUpnode = nodeID
            '            End If
            '            BuildApReportNode(nodeID, nodeName, "REGION")
            '        End If
            '        'BuildLeafHtml(dr, "DISTRICT")
            '        BuildApReportChildNode(childNodeID, leafName, "DISTRICT")
            '        cnt = cnt + 1
            '    Next
            '    _treeStr &= "Torrid Chain " & BuildApReportLinks(0, "CHAIN")
            Case UserRole.MLU, UserRole.RM, UserRole.DM

                Dim companyID As Int16 = 0
                Dim strCompany As String
                Dim drv As DataRowView
                If Not IsNothing(Application("CompanyID")) AndAlso IsNumeric(Application("CompanyID")) Then
                    companyID = Application("CompanyID")
                End If
                If companyID = 1 Then
                    strCompany = "Hottopic"
                ElseIf companyID = 5 Then
                    strCompany = "Torrid"
                Else
                    strCompany = "Blackheart"
                End If
                ds = _sessionUser.ListAllActiveStoresByCompany(companyID)
                '********************************************************************
                'show all stores & districts
                '********************************************************************
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    
                    Dim curRegionID As String = 0
                    Dim n As Int16 = 0
                    For Each dr In ds.Tables(0).Rows
                        If curRegionID <> dr("RegionID") Then
                            curRegionID = dr("RegionID")
                            If n <> 0 Then
                                _treeStr &= "</table>"
                            Else
                                If _sessionUser.SessionUser.CurrentUserRoleId = UserRole.RM Then
                                    '_treeStr = "<FONT class='cellvaluecaption'>Region " & _sessionUser.SessionUser.RegionId.PadLeft(2, "0") & "</font>" & BuildApReportLinks(_sessionUser.SessionUser.RegionId.PadLeft(2, "0"), "REGION")
                                    If _sessionUser.SessionUser.RegionId.PadLeft(2, "0") <> "00" Then
                                        StartupUrl = GetApReportStartUpUrl(_sessionUser.SessionUser.RegionId.PadLeft(2, "0"), "REGION")
                                    Else
                                        StartupUrl = GetApReportStartUpUrl(curRegionID.PadLeft(2, "0"), "REGION")
                                    End If
                                ElseIf _sessionUser.SessionUser.CurrentUserRoleId = UserRole.DM Then
                                    StartupUrl = GetApReportStartUpUrl(_sessionUser.SessionUser.DistrictId.PadLeft(3, "0"), "DISTRICT")
                                ElseIf _sessionUser.SessionUser.CurrentUserRoleId = UserRole.MLU Then
                                    StartupUrl = GetApReportStartUpUrl(curRegionID.PadLeft(2, "0"), "REGION")
                                End If
                            End If
                            n += 1
                            _treeStr &= "<table border=0>"
                            _treeStr &= "<tr><td colspan=2>"
                            _treeStr &= "<FONT class='cellvaluecaption'>Region " & curRegionID.PadLeft(2, "0") & "</font>" & BuildApReportLinks(curRegionID.PadLeft(2, "0"), "REGION")
                            _treeStr &= "</td></tr>"
                        End If
                        nodeName = "<FONT class='cellvaluewhite'>District " & dr("DistrictID").ToString.PadLeft(3, "0") & "</FONT></a>"
                        leafName = "Store " & dr("StoreNum").ToString.PadLeft(4, "0")
                        childNodeID = dr("StoreNum").ToString.PadLeft(4, "0")
                        If nodeID <> dr("DistrictID") Then
                            nodeID = dr("DistrictID")
                            If cnt > 0 Then
                                _treeStr &= "	</table></td></tr>"
                            Else
                                StartUpnode = nodeID
                            End If
                            BuildApReportNode(nodeID, nodeName, "DISTRICT")
                        End If
                        BuildApReportChildNode(childNodeID, leafName, "STORE")
                        cnt = cnt + 1
                    Next
                    _treeStr &= "</table>"
                End If
                _treeStr &= "<tr><td colspan=2><FONT class='cellvaluecaption'>" & strCompany & "Chain </font>" & BuildApReportLinks(0, "CHAIN") & "</td></tr>"
                '********************************************************************
                ' end show all stores & districts
                '********************************************************************
                'ds = _sessionUser.ListDmStores()
                ' group by title
                'If _sessionUser.SessionUser.CurrentUserRoleId = UserRole.RM Then
                '    _treeStr = "<FONT class='cellvaluecaption'>Region " & _sessionUser.SessionUser.RegionId.PadLeft(2, "0") & "</font>" & BuildApReportLinks(_sessionUser.SessionUser.RegionId.PadLeft(2, "0"), "REGION")
                '    StartupUrl = GetApReportStartUpUrl(_sessionUser.SessionUser.RegionId.PadLeft(2, "0"), "REGION")
                'Else
                '    StartupUrl = GetApReportStartUpUrl(_sessionUser.SessionUser.DistrictId.PadLeft(3, "0"), "DISTRICT")
                'End If
                'For Each dr In ds.Tables(0).Rows
                '    nodeName = "<FONT class='cellvaluewhite'>District " & dr("DistrictID").ToString.PadLeft(3, "0") & "</FONT></a>"
                '    leafName = "Store " & dr("StoreNum").ToString.PadLeft(4, "0")
                '    childNodeID = dr("StoreNum").ToString.PadLeft(4, "0")
                '    If nodeID <> dr("DistrictID") Then
                '        nodeID = dr("DistrictID")
                '        If cnt > 0 Then
                '            _treeStr = _treeStr & "	</table></td></tr>"
                '        Else
                '            StartUpnode = nodeID
                '        End If
                '        BuildApReportNode(nodeID, nodeName, "DISTRICT")
                '    End If
                '    BuildApReportChildNode(childNodeID, leafName, "STORE")
                '    cnt = cnt + 1
                'Next
                ''_treeStr &= "</table>" 
                '_treeStr &= "<br><FONT class='cellvaluecaption'>Torrid Chain </font>" & BuildApReportLinks(0, "CHAIN")
                ''If _sessionUser.SessionUser.CurrentUserRoleId = UserRole.DM Then
                ''    _treeStr &= "<br><FONT class='cellvaluecaption'>Region " & _sessionUser.SessionUser.RegionId.PadLeft(2, "0") & "</font>" & BuildApReportLinks(_sessionUser.SessionUser.RegionId.PadLeft(2, "0"), "REGION")
                ''    _treeStr &= "<br><FONT class='cellvaluecaption'>Torrid Chain </font>" & BuildApReportLinks(0, "CHAIN")
                ''Else
                ''    _treeStr &= "<br><FONT class='cellvaluecaption'>Torrid Chain </font>" & BuildApReportLinks(0, "CHAIN")
                ''End If

            Case UserRole.Store
                    _treeStr = "<FONT class='cellvaluecaption'>Store " & _sessionUser.SessionUser.StoreNum.ToString.PadLeft(4, "0") & "</font>" & BuildApReportLinks(_sessionUser.SessionUser.StoreNum.ToString.PadLeft(4, "0"), "STORE")
                _treeStr &= "<FONT class='cellvaluecaption'>District " & _sessionUser.SessionUser.DistrictId.PadLeft(3, "0") & "</font>" & BuildApReportLinks(_sessionUser.SessionUser.DistrictId.PadLeft(3, "0"), "DISTRICT")
                    StartupUrl = GetApReportStartUpUrl(_sessionUser.SessionUser.StoreNum.ToString.PadLeft(4, "0"), "STORE")
        End Select
    End Sub


    Private Sub BuildKioskReportTree()
        _treeStr = ""
        Dim ds As DataSet
        Dim nodeName, nodeID, childNodeID As String
        Dim cnt As Integer = 0
        Dim dr As DataRow
        Dim nodeUrl As String = ""
        Dim leafUrl, leafName As String
        StartupUrl = "ViewKioskReport.aspx"
        Select Case _sessionUser.SessionUser.CurrentUserRoleId

            Case UserRole.Store
                'StartupUrl += "?StoreNo=all"
                Dim strStore As String
                strStore = _sessionUser.SessionUser.StoreNum.ToString.PadLeft(4, "0")
                StartupUrl += "?StoreNo=" & strStore
                _treeStr = "<a href='" & StartupUrl & "' target='main'><FONT class='cellvaluecaption'>Store " & strStore & "</font></a>"
            Case UserRole.Admin
                Dim strList As String()
                strList = Application("KioskList")

                Dim i As Integer
                _treeStr = "<table>"
                For i = 0 To strList.Length - 1
                    _treeStr += "<tr>td><a href='" & StartupUrl & "?StoreNo=" & strList(i) & "' target='main'><FONT class='cellvaluecaption'>Store " & strList(i) & "</font></a></td></tr>"

                Next

        End Select


    End Sub

    Private Sub BuildHTCaptureRptTree()
        _treeStr = ""
        Dim ds As DataSet
        Dim nodeName, nodeID, childNodeID As String
        Dim cnt As Integer = 0
        Dim dr As DataRow
        Dim nodeUrl As String = ""
        Dim leafUrl, leafName As String
        StartupUrl = "ViewHTCaptureReport.aspx"
        Select Case _sessionUser.SessionUser.CurrentUserRoleId

            Case UserRole.Store
                'StartupUrl += "?StoreNo=all"
                Dim strDistrict As String
                Dim strStore As String
                strDistrict = _sessionUser.SessionUser.DistrictId.ToString.PadLeft(3, "0")
                strStore = _sessionUser.SessionUser.StoreNum.ToString.PadLeft(4, "0")
                StartupUrl += "?DistrictId=" & strDistrict
                _treeStr = "<a href='" & StartupUrl & "' target='main'><FONT class='cellvaluecaption'>District " & strDistrict & "</font></a>"
            Case UserRole.DM, UserRole.RM
                Dim strRegion As String
                Dim strStore As String
                strRegion = _sessionUser.SessionUser.RegionId.ToString.PadLeft(2, "0")
                strStore = _sessionUser.SessionUser.StoreNum.ToString.PadLeft(4, "0")
                StartupUrl += "?RegionId=" & strRegion
                _treeStr = "<a href='" & StartupUrl & "' target='main'><FONT class='cellvaluecaption'>Region " & strRegion & "</font></a>"

        End Select
    End Sub

    Private Sub BuildTree()
        _treeStr = ""
        Dim ds As DataSet
        Dim nodeName, nodeID As String
        Dim cnt As Integer = 0
        Dim dr As DataRow
        Dim nodeUrl As String = ""
        Dim leafUrl, leafName As String
        StartupUrl = "FileNotFound.htm"
        '_salesPlanUrl = _salesPlanUrl.Replace("\", "\\")

        If _sessionUser.SessionUser.CurrentUserRoleId = UserRole.MLU Then
            ds = _sessionUser.ListMLUDistricts
            For Each dr In ds.Tables(0).Rows
                Select Case _reportType
                    Case DailyReportType.SalesPlan
                        nodeUrl = _salesPlanUrl & "Region " & dr("RegionID").ToString.PadLeft(2, "0") & ".xls"
                        nodeName = "<a  target='main'  href='" & nodeUrl & "' ><FONT class='cellvaluewhite'>Region " & dr("RegionID") & "</FONT></a>"
                        leafUrl = _salesPlanUrl & "District " & dr("DistrictID").ToString.PadLeft(3, "0") & ".xls"
                        leafName = "District " & dr("DistrictID").ToString.PadLeft(3, "0")
                    Case Else
                        nodeUrl = "ViewWtdReport.aspx?DistrictID=" & dr("DistrictID")
                        nodeName = "<a  href='#' ><FONT class='cellvaluewhite'>Region " & dr("RegionID") & "</FONT></a>"
                        leafUrl = "ViewWtdReport.aspx?DistrictID=" & dr("DistrictID").ToString.PadLeft(3, "0")
                        leafName = "District " & dr("DistrictID").ToString.PadLeft(3, "0")
                End Select

                If nodeID <> dr("RegionID") Then
                    nodeID = dr("RegionID")
                    If cnt > 0 Then
                        _treeStr = _treeStr & "	</table></td></tr>"
                    Else
                        StartupUrl = nodeUrl.Replace("\", "\\")
                        StartUpnode = nodeID
                    End If
                    BuildNodeHtml(nodeID, nodeName)
                End If

                'BuildLeafHtml(dr, "DISTRICT")
                BuildLeafHtml(leafUrl, leafName)
                cnt = cnt + 1
            Next
        Else
            ds = _sessionUser.ListDmStores()
            ' group by title
            For Each dr In ds.Tables(0).Rows
                Select Case _reportType
                    Case DailyReportType.SalesPlan
                        'nodeUrl = "ViewReportFile.aspx?FilePath=" & _salesPlanUrl & "District " & dr("DistrictID").ToString.PadLeft(3, "0") & ".xls"
                        nodeUrl = _salesPlanUrl & "District " & dr("DistrictID").ToString.PadLeft(3, "0") & ".xls"
                        nodeName = "<a  target='main' href='" & nodeUrl & "' ><FONT class='cellvaluewhite'>District " & dr("DistrictID").ToString.PadLeft(3, "0") & "</FONT></a>"
                        leafUrl = _salesPlanUrl & "Store " & dr("StoreNum").ToString.PadLeft(4, "0") & ".xls"
                        'leafUrl = "ViewReportFile.aspx?FilePath=" & _salesPlanUrl & "Store " & dr("StoreNum").ToString.PadLeft(4, "0") & ".xls"
                        leafName = "Store " & dr("StoreNum").ToString.PadLeft(4, "0")
                    Case Else
                        nodeUrl = "ViewWtdReport.aspx?DistrictID=" & dr("DistrictID").ToString.PadLeft(3, "0")
                        'nodeName = "<a  href='#' >District " & dr("RegionID") & "</a>"
                        nodeName = "<a target='main' href='ViewWtdReport.aspx?DistrictID=" & dr("DistrictID") & "' ><FONT class='cellvaluewhite'>District " & dr("DistrictID") & "</FONT></a>"
                        leafUrl = "ViewWtdReport.aspx?StoreID=" & dr("StoreNum").ToString.PadLeft(4, "0")
                        leafName = "Store " & dr("StoreNum").ToString.PadLeft(4, "0")
                End Select
                If nodeID <> dr("DistrictID") Then
                    nodeID = dr("DistrictID")
                    If cnt > 0 Then
                        _treeStr = _treeStr & "	</table></td></tr>"
                    Else
                        StartupUrl = nodeUrl.Replace("\", "\\")
                        StartUpnode = nodeID

                    End If
                    BuildNodeHtml(nodeID, nodeName)
                End If

                'BuildLeafHtml(dr)
                BuildLeafHtml(leafUrl, leafName)
                cnt = cnt + 1

            Next
        End If


    End Sub
    Private Sub BuildNodeHtml(ByVal nodeID As String, ByVal nodeName As String)
        _treeStr = _treeStr & "<TR><TD align=right width=17><IMG onclick=javascript:showtree('" & nodeID & "') height=11 src='../images/Plus" & _treeImgSfx & ".jpg' width=17 border=0 name=I" & nodeID & "></TD>" & _
                    "<TD class=cellvaluecaption align=left width=90%>" & nodeName & "</TD>" & _
                    "</TR><TR><TD colSpan=2><TABLE id=S" & nodeID & " style='DISPLAY: none' cellSpacing=0 cellPadding=0 width=100% border=0>"

    End Sub

    Private Sub BuildLeafHtml(ByVal dr As DataRow, Optional ByVal leafType As String = "STORE")
        Select Case leafType
            Case "DISTRICT"
                _treeStr &= "<TR><TD width=17><IMG src='../images/Line2" & _treeImgSfx & ".jpg' border=0></TD>" & _
                               "<TD  class='cellvaluecaption'>" & _
                               "<A target='main' href='ViewWtdReport.aspx?DistrictID=" & CStr(dr("DistrictID")).PadLeft(3, "0") & "' target=_blank><FONT class='cellvaluewhite'>District " & CStr(dr("DistrictID")).PadLeft(3, "0") & "</FONT></A>"
                _treeStr &= "</TD></tr>"
            Case "STORE"
                _treeStr &= "<TR><TD width=17><IMG src='../images/Line2" & _treeImgSfx & ".jpg' border=0></TD>" & _
                                "<TD  class='cellvaluecaption'>" & _
                                "<A target='main' href='ViewWtdReport.aspx?StoreID=" & CStr(dr("StoreNum")).PadLeft(4, "0") & "' target=_blank><FONT class='cellvaluewhite'>Store " & CStr(dr("StoreNum")).PadLeft(4, "0") & "</FONT></A>"
                _treeStr &= "</TD></tr>"
        End Select
    End Sub
    Private Sub BuildLeafHtml(ByVal leafUrl As String, ByVal leafName As String)
        _treeStr &= "<TR><TD width=17><IMG src='../images/Line2" & _treeImgSfx & ".jpg' border=0></TD>" & _
                                "<TD  class='cellvaluecaption'>" & _
                                "<A target='main' href='" & leafUrl & "'><FONT class='cellvaluewhite'>" & leafName & "</FONT></A>"
        _treeStr &= "</TD></tr>"
    End Sub
    Private Sub BuildApReportNode(ByVal nodeID As String, ByVal nodeName As String, ByVal scope As String)

        _treeStr = _treeStr & "<TR><TD align=right width=17><IMG onclick=javascript:showtree('" & nodeID & "') height=11 src='../images/Plus" & _treeImgSfx & ".jpg' width=17 border=0 name=I" & nodeID & "></TD>" & _
                   "<TD class=cellvaluecaption align=left width=90%>" & nodeName & _
                    BuildApReportLinks(nodeID, scope) & "</TD>" & _
                   "</TR><TR><TD colSpan=2><TABLE id=S" & nodeID & " style='DISPLAY: none' cellSpacing=0 cellPadding=0 width=100% border=0>"
    End Sub
    Private Sub BuildApReportChildNode(ByVal nodeID As String, ByVal nodeName As String, ByVal scope As String)
        _treeStr &= "<TR><TD width=17><IMG src='../images/Line2" & _treeImgSfx & ".jpg' border=0></TD>" & _
                               "<TD  class='cellvaluecaption'>" & _
                               nodeName & BuildApReportLinks(nodeID, scope)
        _treeStr &= "</TD></tr>"
    End Sub


    Private Function BuildApReportLinks(ByVal nodeID As String, ByVal scope As String) As String
        Dim apReportLinks As String
        Dim aprArchiveSfx As String = ""
        If Not IsNothing(Application("AprArchiveSfx")) Then
            aprArchiveSfx = Application("AprArchiveSfx")
        End If
        Select Case scope.ToUpper
            Case "CHAIN"
                'apReportLinks = " <a  target='main' href ='ApReports/" & scope & "_Day/ChainW.xls' Title='Daily'><FONT class='cellvaluewhite'>W</FONT></a> "
                'apReportLinks &= " <a  target='main' href ='ApReports/" & scope & "_Month/ChainM.xls' Title='Monthly'><FONT class='cellvaluewhite'>M</FONT></a> "
                'apReportLinks &= " <a  target='main' href ='ApReports/" & scope & "_Qtr/ChainQ.xls' Title='Quarterly'><FONT class='cellvaluewhite'>Q</FONT></a> "
                'apReportLinks &= " <a  target='main' href ='ApReports/Archives/" & scope & "_Day/ChainW" & aprArchiveSfx & ".xls' Title='Previous Week'><FONT class='cellvaluewhite'>P</FONT></a> "
            Case "REGION"
                'apReportLinks = " <a  target='main' href ='ApReports/" & scope & "_Day/R" & nodeID & "W.xls' Title='Daily'><FONT class='cellvaluewhite'>W</FONT></a> "
                'apReportLinks &= " <a  target='main' href ='ApReports/" & scope & "_Month/R" & nodeID & "M.xls' Title='Monthly'><FONT class='cellvaluewhite'>M</FONT></a> "
                'apReportLinks &= " <a  target='main' href ='ApReports/" & scope & "_Qtr/R" & nodeID & "Q.xls' Title='Quarterly'><FONT class='cellvaluewhite'>Q</FONT></a> "
                'apReportLinks &= " <a  target='main' href ='ApReports/Archives/" & scope & "_Day/R" & nodeID & "W" & aprArchiveSfx & ".xls' Title='Previous Week'><FONT class='cellvaluewhite'>P</FONT></a> "
            Case "DISTRICT"
                'apReportLinks = " <a  target='main' href ='ApReports/" & scope & "_Day/D" & nodeID & "W.xls' Title='Daily'><FONT class='cellvaluewhite'>W</FONT></a> "
                'apReportLinks &= " <a  target='main' href ='ApReports/" & scope & "_Month/D" & nodeID & "M.xls' Title='Monthly'><FONT class='cellvaluewhite'>M</FONT></a> "
                'apReportLinks &= " <a  target='main' href ='ApReports/" & scope & "_Qtr/D" & nodeID & "Q.xls' Title='Quarterly'><FONT class='cellvaluewhite'>Q</FONT></a> "
                'apReportLinks &= " <a  target='main' href ='ApReports/Archives/" & scope & "_Day/D" & nodeID & "W" & aprArchiveSfx & ".xls' Title='Previous Week'><FONT class='cellvaluewhite'>P</FONT></a> "
            Case "STORE"
                apReportLinks = " <a  target='main' href ='ApReports/" & scope & "_Day/S" & nodeID & "W.xls' Title='Daily'><FONT class='cellvaluewhite'>W</FONT></a> "
                apReportLinks &= " <a  target='main' href ='ApReports/" & scope & "_Month/S" & nodeID & "M.xls' Title='Monthly'><FONT class='cellvaluewhite'>M</FONT></a> "
                apReportLinks &= " <a  target='main' href ='ApReports/" & scope & "_Qtr/S" & nodeID & "Q.xls' Title='Quarterly'><FONT class='cellvaluewhite'>Q</FONT></a> "
                apReportLinks &= " <a  target='main' href ='ApReports/Archives/" & scope & "_Day/S" & nodeID & "W" & aprArchiveSfx & ".xls' Title='Previous Week'><FONT class='cellvaluewhite'>P</FONT></a> "
        End Select
        Return apReportLinks
    End Function

    Private Function GetApReportStartUpUrl(ByVal nodeID As String, ByVal scope As String) As String
        Dim apReportLinks As String
        Select Case scope.ToUpper
            Case "CHAIN"
                'apReportLinks = "ApReports/" & scope & "_Day/ChainW.xls'"
                apReportLinks = "ApReports/blank.html'"
            Case "REGION"
                'apReportLinks = "ApReports/" & scope & "_Day/R" & nodeID & "W.xls"
                apReportLinks = "ApReports/blank.html'"
            Case "DISTRICT"
                'apReportLinks = "ApReports/" & scope & "_Day/D" & nodeID & "W.xls"
                apReportLinks = "ApReports/blank.html'"
            Case "STORE"
                apReportLinks = "ApReports/" & scope & "_Day/S" & nodeID & "W.xls"
        End Select
        Return apReportLinks
    End Function



End Class
