<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageApprovers.aspx.vb" Inherits="ReportDistro.ManageApprovers"%>
<HTML>
	<body>
		<!--#include file="../include/header.inc"-->
		<form id="Form1" method="post" runat="server">
			<br>
			<table width="500" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="3">
						<table align="center" border="0">
							<tr>
								<td colspan="3" align="middle" class="pageCaption">Manage Approvers</td>
							</tr>
							<tr height="15">
								<td colspan="3">
									<asp:Label ID="lblError" Runat="server" CssClass="errStyle"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">Report ID</td>
								<td width="10" class="reqStyle"></td>
								<td class="cellvaluecaption"><%=request("ReportID")%>
								</td>
							</tr>
							<tr height="5">
								<td colspan="3"></td>
							</tr>
							<tr>
								<td class="cellvaluecaption" width="110">Report Title</td>
								<td width="10" class="reqStyle"></td>
								<td class="cellvaluecaption">Report Title1
								</td>
							</tr>
							<tr height="5">
								<td colspan="3"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<table align="center" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="165" class="cellvaluecaption" align="middle">HQ Users</td>
								<td width="30"></td>
								<td width="165" class="cellvaluecaption" align="middle">Approvers</td>
								<td width="30"></td>
							</tr>
							<tr height="5">
								<td colspan="3"></td>
							</tr>
							<tr>
								<td width="165" align="right">
									<asp:ListBox ID="lstUsers" Runat="server" Height="200" CssClass="cellvalueleft" Width="160" SelectionMode="Multiple"></asp:ListBox>
								</td>
								<td width="30" valign="center" align="middle">
									<asp:ImageButton ID="moveRight" Runat="server" ImageUrl="../Images/MoveRight.gif"></asp:ImageButton>
									<asp:ImageButton ID="moveLeft" Runat="server" ImageUrl="../Images/MoveLeft.gif"></asp:ImageButton>
								</td>
								<td width="165" class="cellvaluecaption">
									<asp:ListBox id="lstApprovers" Runat="server" Height="200" CssClass="cellvalueleft" Width="160"></asp:ListBox>
								</td>
								<td width="30" valign="center" align="middle">
									<asp:ImageButton ID="moveUp" Runat="server" ImageUrl="../Images/MoveUp.gif"></asp:ImageButton>
									<asp:ImageButton ID="moveDown" Runat="server" ImageUrl="../Images/MoveDown.gif"></asp:ImageButton>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<!-- end here-->
			<!--footer--> </TD></TR>
			<tr>
				<td><hr color='#990000'>
				</td>
				<td><hr color='#990000'>
				</td>
				<td><hr color='#990000'>
				</td>
				<td><hr color='#990000'>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td align='right'>
					<asp:Button CssClass="B1" ID="btnSubmit" Runat="server" Text="Next>>"></asp:Button>
					&nbsp;&nbsp;<input class='B1' type='button' name='Home' value='Home' onClick="document.location='Home.htm';" ID="Button1"></td>
			</tr>
			</TABLE></form>
	</body>
</HTML>
