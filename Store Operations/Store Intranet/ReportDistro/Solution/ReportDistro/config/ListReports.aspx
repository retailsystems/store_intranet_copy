<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ListReports.aspx.vb" Inherits="ReportDistro.ListReports"%>
<HTML>
	<!--#include file="../include/header.inc"-->
	<body>
		<form id="Form1" method="post" runat="server">
			<br>
			<table border="0" cellpadding="0" cellspacing="0" width="520" align="center">
			
					<tr>
						<td align="middle" class="pageCaption"><u>Manage Reports</u></td>
					</tr>
					<tr height="10">
						<td></td>
					</tr>
					<tr>
						<td>
							<asp:DataGrid ID="dgReport" Width="650" Runat="server" CellPadding="1" HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle" AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false">
								<Columns>
									<asp:BoundColumn DataField="reportId" Visible=False></asp:BoundColumn>
									<asp:HyperLinkColumn DataTextField="ReportTitle" DataNavigateUrlField="ReportID" DataNavigateUrlFormatString="ViewReport.aspx?ReportID={0}"></asp:HyperLinkColumn>
									<asp:BoundColumn DataField="StagingPath" HeaderText="Staging"></asp:BoundColumn>
									<asp:BoundColumn DataField="Scope" HeaderText="Scope"></asp:BoundColumn>
									<asp:BoundColumn DataField="DefaultFileName" HeaderText="File Name"></asp:BoundColumn>
									<asp:BoundColumn DataField="FileIdentifier" HeaderText="File Identifier"></asp:BoundColumn>
								</Columns>
							</asp:DataGrid>
						</td>
					</tr>
					<tr height="5">
						<td></td>
					</tr>
					</table>
		</form>
		<!--#include file="../include/footer.inc"--> </TBODY></TABLE>
	</body>
</HTML>
