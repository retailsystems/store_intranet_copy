
Imports HotTopic.RD.Application
Imports HotTopic.RD.Entity

Public Class ListReports
    Inherits System.Web.UI.Page
    Protected WithEvents dgReport As System.Web.UI.WebControls.DataGrid
    Private _sessionUser As HotTopic.RD.Application.Session
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'need to pass httpcontext at every page load
            'TODO: temp emp id passed in; emp id/uid&pwd only passed in from login
            _sessionUser = New Session(HttpContext.Current, 1)
        Catch ex As Exception
            Throw ex 'ASP.NET error page should handle
        End Try
        If Not IsPostBack Then
            BindData()
        End If
    End Sub
    Private Sub BindData()
        Dim rds As DataSet = _sessionUser.ListReports()
        'Response.Write(rds.Tables(0).Rows.Count)
        dgReport.DataSource = rds
        dgReport.DataBind()

    End Sub
End Class
