<%@ Page Language="vb" AutoEventWireup="false" Codebehind="NewReportStep1.aspx.vb" Inherits="ReportDistro.NewReportStep1"%>
<HTML>
	<body>
		<!--#include file="../include/header.inc"-->
		<form id="Form1" method="post" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" width="500" border="0">
				<tr>
					<td class="pageCaption" align="middle" colSpan="3">
						<asp:Label ID="lblCaption" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr height="15">
					<td colSpan="3" align="middle">
						<asp:Label ID="lblErrMsg" Runat="server" CssClass="errStyle"></asp:Label>
					</td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Report Title</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft"><asp:textbox id="txtRptTitle" Runat="server" Width="140" CssClass="cellvalueleft"></asp:textbox>
						<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" CssClass="errStyle" ErrorMessage="Report Title is required." Display="Dynamic" ControlToValidate="txtRptTitle"></asp:RequiredFieldValidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" vAlign="top" width="110">Report Description</td>
					<td class="reqStyle" width="10"></td>
					<td class="cellvalueleft"><asp:textbox id="txtRptDesc" Runat="server" Width="250" CssClass="cellvalueleft" TextMode="MultiLine" rows="2"></asp:textbox></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Input Type</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft"><asp:dropdownlist id="lstInputType" Runat="server" CssClass="cellvalueleft"></asp:dropdownlist>
						<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" CssClass="errStyle" ErrorMessage="Select input type." Display="Dynamic" ControlToValidate="lstInputType"></asp:RequiredFieldValidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Staging Path</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft"><asp:textbox id="txtFilePath" Runat="server" Width="140" CssClass="cellvalueleft"></asp:textbox>
						<asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" CssClass="errStyle" ErrorMessage="Staging Path is required." Display="Dynamic" ControlToValidate="txtFilePath"></asp:RequiredFieldValidator></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Location Path</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft"><asp:textbox id="txtLocation" Runat="server" Width="140" CssClass="cellvalueleft"></asp:textbox>
						<asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" CssClass="errStyle" ErrorMessage="Location path is required." Display="Dynamic" ControlToValidate="txtLocation"></asp:RequiredFieldValidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Default FileName</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvaluecaption"><asp:textbox id="txtFileName" Runat="server" Width="140" CssClass="cellvalueleft"></asp:textbox>&nbsp;Identifier
						<asp:dropdownlist id="lstIdentifier" Runat="server"></asp:dropdownlist>
						<asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" CssClass="errStyle" ErrorMessage="Default filename is required." Display="Dynamic" ControlToValidate="txtFileName"></asp:RequiredFieldValidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Content Scope</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvalueleft">
						<asp:dropdownlist id="lstScope" Runat="server" CssClass="cellvalueleft"></asp:dropdownlist>
						<asp:RequiredFieldValidator id="Requiredfieldvalidator9" runat="server" CssClass="errStyle" ErrorMessage="Select Content Scope." Display="Dynamic" ControlToValidate="lstScope"></asp:RequiredFieldValidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Expiration Type</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvaluewhite"><asp:dropdownlist id="lstExpType" Runat="server" CssClass="cellvalueleft" AutoPostBack="True"></asp:dropdownlist>
						<asp:Label Runat="server" ID="lblArchive" text=" Specify No." cssclass="cellvaluewhite" Visible="False"></asp:Label>
						<asp:TextBox Runat="server" ID="txtArchiveNth" CssClass="cellvalueleft" Width="20" MaxLength="2" Text="04" Visible="False"></asp:TextBox>
						<asp:RequiredFieldValidator id="RequiredFieldValidator6" runat="server" CssClass="errStyle" ErrorMessage="Expiration type is required." Display="Dynamic" ControlToValidate="lstExpType"></asp:RequiredFieldValidator>
						<asp:CompareValidator id="CompareValidator1" runat="server" CssClass="errStyle" ControlToValidate="txtArchiveNth" Display="Dynamic" ErrorMessage="Invalid input." Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">File Format</td>
					<td class="reqStyle" width="10"></td>
					<td class="cellvaluewhite"><asp:textbox id="txtFileForamt" Runat="server" Width="50" CssClass="cellvalueleft"></asp:textbox></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Max FileSize</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvaluewhite"><asp:textbox id="txtMaxSize" Runat="server" Width="50" CssClass="cellvalueleft"></asp:textbox>&nbsp;Kb.
						<asp:RequiredFieldValidator id="RequiredFieldValidator7" runat="server" CssClass="errStyle" ErrorMessage="Max file size is required." Display="Dynamic" ControlToValidate="txtMaxSize"></asp:RequiredFieldValidator>
					</td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Display Order</td>
					<td class="reqStyle" width="10"></td>
					<td class="cellvaluewhite"><asp:textbox id="txtOrder" Runat="server" Width="30" CssClass="cellvalueleft"></asp:textbox></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Approver Group Email</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvaluewhite"><asp:textbox id="txtEmail" Runat="server" Width="140" CssClass="cellvalueleft"></asp:textbox>
						<asp:RequiredFieldValidator id="RequiredFieldValidator10" runat="server" CssClass="ErrStyle" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Email is required."></asp:RequiredFieldValidator>
						<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" CssClass="ErrStyle" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Approval Reqd. By</td>
					<td class="reqStyle" width="10"></td>
					<td class="cellvaluewhite"><asp:radiobuttonlist id="rdoApproveProcess" Runat="server" CssClass="cellvaluewhite" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="1" Selected="True">None</asp:ListItem>
							<asp:ListItem Value="2">Any One</asp:ListItem>
							<asp:ListItem Value="3">All</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				</TR>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Read Only</td>
					<td class="reqStyle" width="10"></td>
					<td class="cellvaluewhite">
						<asp:radiobuttonlist id="rdoReadOnly" Runat="server" CssClass="cellvaluewhite" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="0" Selected="True">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr height="5">
					<td colSpan="3"></td>
				</tr>
				<tr>
					<td class="cellvaluecaption" width="110">Report Owners</td>
					<td class="reqStyle" width="10">*</td>
					<td class="cellvaluewhite"><asp:dropdownlist id="lstStoreOwner" Runat="server" CssClass="cellvalueleft">
							<asp:ListItem>--Choose one--</asp:ListItem>
							<asp:ListItem>Hot Topic</asp:ListItem>
							<asp:ListItem>Torrid</asp:ListItem>
							<asp:ListItem>CD Stores</asp:ListItem>
						</asp:dropdownlist>
						<asp:RequiredFieldValidator id="RequiredFieldValidator8" runat="server" CssClass="errStyle" ErrorMessage="Select report owner." Display="Dynamic" ControlToValidate="lstStoreOwner"></asp:RequiredFieldValidator></td>
				</tr>
				
			</table>
			<!-- end here-->
			<!--footer--> </TD></TR><tr>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
				<td>
					<hr color="#990000">
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td align="right">
					<asp:Button CssClass="B1" ID="btnSubmit" Runat="server" Text="Next>>"></asp:Button>
					&nbsp;&nbsp;<input class="B1" id="Button1" onclick="document.location='Home.htm';" type="button" value="Home" name="Home"></td>
			</tr>
			</TABLE></form>
	</body>
</HTML>
