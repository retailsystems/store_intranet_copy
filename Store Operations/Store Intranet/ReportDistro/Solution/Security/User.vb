' Static Model

Namespace HotTopic.RD.Security

    <Serializable()> _
    Public Structure User

        Private _userId As Int32
        Public Property UserId() As Int32
            Get
                Return _userId
            End Get
            Set(ByVal Value As Int32)
                _userId = Value
                UserProjects = UserHelper.GetUserProjects(_userId)
            End Set
        End Property
        Public EmployeeId As String
        Public FirstName As String
        Public LastName As String
        Public StoreNum As Int16
        Public DistrictId As String
        Public RegionId As String
        Public JobCode As String
        Public IsNotIndividual As Boolean
        Public UserProjects As Hashtable 'projectname is key
        Public CurrentProjectId As Int16 'current project user is in
        Public CurrentUserRoleId As Int16 'for current project user is in
        Public CurrentModuleId As Int16 'for current project user is in
        Public CurrentDefaultPage As String
        Public Market As String '5/23/04 for Market level users
        Public CompanyID As String '4/18/08 - Sri - Get the companyID 1 - HT, 5 -TD

    End Structure ' END STRUCTURE DEFINITION User
    <Serializable()> _
    Public Structure UserProject
        Public ProjectId As Int16
        Public UserRoleId As Int16
        Public ProjectName As String
        Public DefaultPage As String
    End Structure

    Friend Class UserHelper
        Public Shared Function GetUserProjects(ByVal userId As Int32) As Hashtable
            Dim userProjs As Hashtable
            Try
                Dim dt As DataTable = SecurityManager.ListProjectsByUserId(userId)
                If dt.Rows.Count = 0 Then
                    Return Nothing
                Else
                    userProjs = New Hashtable(dt.Rows.Count)
                End If
                Dim dr As DataRow
                For Each dr In dt.Rows
                    Dim userProj As UserProject = New UserProject()
                    userProj.ProjectId = Convert.ToInt32(dr("ProjectId"))
                    userProj.UserRoleId = Convert.ToInt32(dr("UserRoleId"))
                    userProj.ProjectName = Convert.ToString(dr("ProjectName"))
                    userProj.DefaultPage = HotTopic.RD.Services.UtilityManager.NullToString(dr("ModuleUrl"))
                    userProjs.Add(userProj.ProjectName.ToLower(), userProj)
                Next
                Return userProjs
            Catch ex As Exception
                Throw ex
            End Try

        End Function

    End Class

End Namespace ' HotTopic.RD.Security

