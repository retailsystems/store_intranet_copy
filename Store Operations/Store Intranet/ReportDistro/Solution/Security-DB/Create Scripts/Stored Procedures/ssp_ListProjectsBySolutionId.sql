IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'ssp_ListProjectsBySolutionId')
	BEGIN
		PRINT 'Dropping Procedure ssp_ListProjectsBySolutionId'
		DROP  Procedure  ssp_ListProjectsBySolutionId
	END

GO

PRINT 'Creating Procedure ssp_ListProjectsBySolutionId'
GO
CREATE Procedure ssp_ListProjectsBySolutionId
	@solutionId	smallint
AS

/******************************************************************************
**		File: ssp_ListProjectsBySolutionId.sql
**		Name: ssp_ListProjectsBySolutionId
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Jeric Saez
**		Date: 7/1/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

	SET NOCOUNT ON
	
	SELECT	SolutionId
			,SolutionName
			,SolutionUrl
			,SolutionDesc
			,SP.ProjectId
			,ProjectName
			,ProjectUrl
			,ProjectDesc
			,SP.DefaultProjectCSSClass
			,SortOrder
			,UserRoleId
	FROM	svw_SolutionProject SP LEFT JOIN
			ProjectRole PR ON SP.ProjectId = PR.ProjectId
	WHERE	SP.SolutionId = @solutionId
	ORDER BY SortOrder

GO

GRANT EXEC ON ssp_ListProjectsBySolutionId TO AppUser

GO
