IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'ssp_GetAllApprovers')
	BEGIN
		PRINT 'Dropping Procedure ssp_GetAllApprovers'
		DROP  Procedure  ssp_GetAllApprovers
	END

GO

PRINT 'Creating Procedure ssp_GetAllApprovers'
GO
CREATE Procedure ssp_GetAllApprovers
	/* Param List */
AS

/******************************************************************************
**		File: ssp_GetAllApprovers.sql
**		Name: ssp_GetAllApprovers
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    7/25/2003		Sri Bajjuri			Include "Admin" users to approver list.
*******************************************************************************/

SELECT     EmployeeID, ltrim(rtrim(FirstName)) +' ' +ltrim(rtrim(LastName)) as FullName
FROM         UserProfile usr 
JOIN UserProfileRole upf ON usr.UserID = upf.UserID
JOIN UserRole ur
ON upf.UserRoleID = ur.UserRoleID and userRoleName in('Admin','Approver')


GO

GRANT EXEC ON ssp_GetAllApprovers TO AppUser

GO
