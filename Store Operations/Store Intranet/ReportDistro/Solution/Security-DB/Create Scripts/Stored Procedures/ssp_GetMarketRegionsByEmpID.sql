IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'ssp_GetMarketRegionsByEmpID')
	BEGIN
		PRINT 'Dropping Procedure ssp_GetMarketRegionsByEmpID'
		DROP  Procedure  ssp_GetMarketRegionsByEmpID
	END

GO

PRINT 'Creating Procedure ssp_GetMarketRegionsByEmpID'
GO
CREATE Procedure ssp_GetMarketRegionsByEmpID
	/* Param List */
	@EmployeeID varchar(5)
AS

/******************************************************************************
**		File: ssp_GetMarketRegionsByEmpID.sql
**		Name: ssp_GetMarketRegionsByEmpID
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/
CREATE TABLE #MyTempRegion
	(RegionID int not null)
	
DECLARE @MarketID int
SELECT @MarketID = MarketID FROM MarketOfficers WHERE EmployeeID = @EmployeeID
INSERT #MyTempRegion EXECUTE  ssp_GetRegionsByMarketID  @MarketID

SELECT * FROM #MyTempRegion

DROP TABLE #MyTempRegion

GO

GRANT EXEC ON ssp_GetMarketRegionsByEmpID TO AppUser

GO
