IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'ssp_GetProjectRoleByUserId')
	BEGIN
		PRINT 'Dropping Procedure ssp_GetProjectRoleByUserId'
		DROP  Procedure  ssp_GetProjectRoleByUserId
	END

GO

PRINT 'Creating Procedure ssp_GetProjectRoleByUserId'
GO
CREATE Procedure ssp_GetProjectRoleByUserId
	@userId		int
	,@projectId	smallint
AS

/******************************************************************************
**		File: ssp_GetProjectRoleByUserId.sql
**		Name: ssp_GetProjectRoleByUserId
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Jeric Saez
**		Date: 7/1/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

	SET NOCOUNT ON
	
	SELECT	UP.UserId
			,UP.ProjectId
			,UP.UserRoleId
			,UR.UserRoleName
			,UR.UserRoleDesc
	FROM	UserProfileRole UP INNER JOIN
			UserRole UR ON UP.UserRoleId = UP.UserRoleId
	WHERE	UP.UserId = @userId
	AND		UP.ProjectId = @projectId


GO

GRANT EXEC ON ssp_GetProjectRoleByUserId TO AppUser

GO
