IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'ssp_ListProjectsByUserId')
	BEGIN
		PRINT 'Dropping Procedure ssp_ListProjectsByUserId'
		DROP  Procedure  ssp_ListProjectsByUserId
	END

GO

PRINT 'Creating Procedure ssp_ListProjectsByUserId'
GO
CREATE Procedure ssp_ListProjectsByUserId
	@userId			int
	,@solutionId	int = NULL
AS

/******************************************************************************
**		File: ssp_ListProjectsByUserId.sql
**		Name: ssp_ListProjectsByUserId
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Jeric Saez
**		Date: 7/1/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

	SET NOCOUNT ON
	
	IF (@solutionId IS NULL)
	BEGIN
		SELECT	SP.ProjectId
				,ProjectName
				,ProjectUrl
				,ProjectDesc
				,SP.DefaultProjectCSSClass
				,SortOrder
				,PR.UserRoleId
				,P.DefaultModuleId
				,M.ModuleUrl
		FROM	svw_SolutionProject SP INNER JOIN
				UserProfileRole PR ON (SP.ProjectId = PR.ProjectId AND PR.UserId = @userId) INNER JOIN
				ProjectRole P ON (PR.ProjectId = P.ProjectId AND PR.UserRoleId = P.UserRoleId) LEFT JOIN
				Module M ON P.DefaultModuleId = M.ModuleId 
		ORDER BY SortOrder
	END
	ELSE
	BEGIN
		SELECT	SP.ProjectId
				,ProjectName
				,ProjectUrl
				,ProjectDesc
				,SP.DefaultProjectCSSClass
				,SortOrder
				,PR.UserRoleId
				,P.DefaultModuleId
				,M.ModuleUrl
		FROM	svw_SolutionProject SP INNER JOIN
				UserProfileRole PR ON (SP.ProjectId = PR.ProjectId AND PR.UserId = @userId) INNER JOIN
				ProjectRole P ON (PR.ProjectId = P.ProjectId AND PR.UserRoleId = P.UserRoleId) LEFT JOIN
				Module M ON P.DefaultModuleId = M.ModuleId 
		WHERE	SP.SolutionId = @solutionId
		ORDER BY SolutionId, SortOrder
	END
	

GO

GRANT EXEC ON ssp_ListProjectsByUserId TO AppUser

GO
