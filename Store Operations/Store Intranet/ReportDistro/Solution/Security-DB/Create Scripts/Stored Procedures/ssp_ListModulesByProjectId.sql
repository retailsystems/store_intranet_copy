IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'ssp_ListModulesByProjectId')
	BEGIN
		PRINT 'Dropping Procedure ssp_ListModulesByProjectId'
		DROP  Procedure  ssp_ListModulesByProjectId
	END

GO

PRINT 'Creating Procedure ssp_ListModulesByProjectId'
GO
CREATE Procedure ssp_ListModulesByProjectId
	@projectId	smallint
AS

/******************************************************************************
**		File: ssp_ListModulesByProjectId.sql
**		Name: ssp_ListModulesByProjectId
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Jeric Saez
**		Date: 7/1/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

	SET NOCOUNT ON
	
	SELECT	DISTINCT PM.ProjectId
			,ProjectName
			,ProjectUrl
			,ProjectDesc
			,PM.DefaultProjectCSSClass
			,ModuleId
			,ModuleName
			,ModuleUrl
			,ModuleDesc
			,ModuleOrder
			,UserRoleId
	FROM	svw_ProjectModuleControl PM LEFT JOIN
			ProjectRole PR ON PM.ProjectId = PR.ProjectId
	WHERE	PM.ProjectId = @projectId
	ORDER BY UserRoleId, ModuleOrder


GO

GRANT EXEC ON ssp_ListModulesByProjectId TO AppUser

GO
