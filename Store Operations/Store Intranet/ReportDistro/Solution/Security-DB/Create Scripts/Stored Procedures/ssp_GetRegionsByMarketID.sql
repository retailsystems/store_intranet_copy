IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'ssp_GetRegionsByMarketID')
	BEGIN
		PRINT 'Dropping Procedure ssp_GetRegionsByMarketID'
		DROP  Procedure  ssp_GetRegionsByMarketID
	END

GO

PRINT 'Creating Procedure ssp_GetRegionsByMarketID'
GO
CREATE Procedure ssp_GetRegionsByMarketID
	/* Param List */
	@ParentMarketID varchar(5)
	,@CurLevel int = 0 -- Pass zero as default
	
AS

/******************************************************************************
**		File: ssp_GetRegionsByMarketID.sql
**		Name: ssp_GetRegionsByMarketID
**		Desc: 
**
**		This template can be customized:
**              
**		Return values: Returns coverage regions of market level users
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: Sri Bajjuri	
**		Date: 5/23/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

IF EXISTS(SELECT 1 FROM MarketLevel WHERE ParentMarketID = @ParentMarketID)
	BEGIN
		DECLARE @MarketID int
		DECLARE RegCur CURSOR LOCAL FOR
			SELECT MarketID FROM  MarketLevel  WHERE ParentMarketID = @ParentMarketID
			
		OPEN RegCur
		FETCH NEXT FROM RegCur INTO @MarketID
		WHILE @@FETCH_STATUS = 0	
		BEGIN
			IF EXISTS(SELECT 1 FROM MarketRegionMap WHERE MarketID = @MarketID)
				BEGIN
					SELECT RegionID FROM MarketRegionMap WHERE MarketID = @MarketID
				END
				SET @CurLevel = @CurLevel + 1
				EXEC ssp_GetRegionsByMarketID @MarketID,@CurLevel
			FETCH NEXT FROM RegCur INTO @MarketID
		END

		CLOSE RegCur
		DEALLOCATE RegCur
	END
ELSE
	BEGIN	
		IF (@CurLevel = 0)
		SELECT RegionID FROM MarketRegionMap WHERE MarketID = @ParentMarketID
	END
GO

GRANT EXEC ON ssp_GetRegionsByMarketID TO AppUser

GO
