IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'ssp_LoginByEmpID')
	BEGIN
		PRINT 'Dropping Procedure ssp_LoginByEmpID'
		DROP  Procedure  ssp_LoginByEmpID
	END

GO

PRINT 'Creating Procedure ssp_LoginByEmpID'
GO
CREATE Procedure ssp_LoginByEmpID
	@employeeId		varchar(10)	
	,@retValue int OUTPUT
AS

/******************************************************************************
**		File: ssp_LoginByEmpID.sql
**		Name: ssp_LoginByEmpID
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

SET NOCOUNT ON
	
	DECLARE	@rows int
	IF EXISTS(SELECT EmployeeId FROM UserProfile WHERE	EmployeeId = @employeeId
														AND		ActiveFlag = 1)
	
	SELECT	UserId
			,EmployeeId
			,Password
			,LastName
			,MiddleName
			,FirstName
			,JobCode
			,StoreNum
			,DistrictId
			,RegionId
			,NonIndividualFlag
			,dbo.sfn_GetMarketByEmpID(@employeeId) AS Market
			,InsertBy
			,InsertDate
			,UpdateBy
			,UpdateDate
	FROM	UserProfile 
	WHERE	EmployeeId = @employeeId	
	AND		ActiveFlag = 1
	
	SELECT	@rows = @@ROWCOUNT
	
	IF (@rows > 0)
	BEGIN
		--RETURN 0 --login success
		set @retValue = 0
	END
	ELSE
	BEGIN		
			--RETURN -2 --wrong employeeid
			set @retValue = -2
	END
			


GO

GRANT EXEC ON ssp_LoginByEmpID TO AppUser

GO
