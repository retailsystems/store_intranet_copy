IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'svw_ProjectModuleControl')
	BEGIN
		PRINT 'Dropping View svw_ProjectModuleControl'
		DROP  View svw_ProjectModuleControl
	END
GO

/******************************************************************************
**		File: svw_ProjectModuleControl.sql
**		Name: svw_ProjectModuleControl
**		Desc: 
**
**		This template can be customized:
**              
**
**		Auth: Jeric Saez
**		Date: 7/1/2003
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/

PRINT 'Creating View svw_ProjectModuleControl'
GO
CREATE View svw_ProjectModuleControl
as

	SELECT	PM.ProjectId
			,P.ProjectName
			,P.ProjectUrl
			,P.ProjectDesc
			,P.DefaultProjectCSSClass
			,M.ModuleId
			,M.ModuleName
			,M.ModuleUrl
			,M.ModuleDesc
			,M.InheritedModuleId
			,PM.SortOrder AS ModuleOrder
			,MC.ModuleControlId
			,MC.ControlURL
			,MC.SortOrder AS ModuleControlOrder
			,C.ControlId 
			,C.ControlName
			,C.ControlType
			,C.ControlDesc
			,C.ParentControlId
			,C.SortOrder AS ControlOrder
			,C.InheritableFlag
	FROM	ProjectModule PM INNER JOIN
			Project	P ON PM.ProjectId = P.ProjectId INNER JOIN
			Module M ON PM.ModuleId = M.ModuleId INNER JOIN
			ModuleControl MC ON M.ModuleId = MC.ModuleId LEFT OUTER JOIN
			Control C ON MC.ControlId = C.ControlId


GO


GRANT SELECT ON svw_ProjectModuleControl TO AppUser

GO
