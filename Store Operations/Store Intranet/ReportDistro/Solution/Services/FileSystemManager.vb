Imports System.IO

Namespace HotTopic.RD.Services


    Public Class FileSystemManager

        Private Enum FileSysOp
            MoveOp
            CopyOp
        End Enum

        ' Moves all files in source to target, unless optional file specified
        Public Shared Sub MoveFiles(ByVal sourcePath As String, ByVal targetPath As String, Optional ByVal searchPattern As String = "", Optional ByVal fileName As String = "")
            Try
                FilesOperation(FileSysOp.MoveOp, sourcePath, targetPath, searchPattern, fileName)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ' Copies all files in source to target, unless optional file specified
        Public Shared Sub CopyFiles(ByVal sourcePath As String, ByVal targetPath As String, Optional ByVal searchPattern As String = "", Optional ByVal fileName As String = "")
            Try
                FilesOperation(FileSysOp.CopyOp, sourcePath, targetPath, searchPattern, fileName)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub DeleteFiles(ByVal path As String, Optional ByVal searchPattern As String = "")
            Try
                Dim di As DirectoryInfo = New DirectoryInfo(path)
                If di.Exists Then
                    Dim ienum As IEnumerator
                    If searchPattern = "" Then
                        ienum = di.GetFiles().GetEnumerator()
                    Else
                        ienum = di.GetFiles(searchPattern).GetEnumerator()
                    End If
                    While ienum.MoveNext
                        Dim fi As FileInfo = DirectCast(ienum.Current, FileInfo)
                        fi.Delete()
                    End While
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function CanWrite(ByVal path As String) As Boolean
            Const fileNAME As String = "test.bin"
            Dim s As System.IO.MemoryStream
            Try
                s = New System.IO.MemoryStream()
                s.WriteByte(0)
                Dim filePath As String = path
                If Not path.EndsWith("\") Then
                    filePath += "\"
                End If
                filePath += fileNAME
                FileSystemManager.SaveFile(filePath, 1, s)
                Return True
            Catch ex As Exception
                Throw ex
            Finally
                If Not s Is Nothing Then
                    s.Close()
                End If
                FileSystemManager.DeleteFiles(path, fileNAME)
            End Try
        End Function

        Public Shared Function ListFileNames(ByVal path As String, Optional ByVal searchPattern As String = "") As ArrayList
            Try
                Dim di As DirectoryInfo = New DirectoryInfo(path)
                Dim ienum As IEnumerator
                If searchPattern = "" Then
                    ienum = di.GetFiles().GetEnumerator()
                Else
                    ienum = di.GetFiles(searchPattern).GetEnumerator()
                End If
                Dim fileNames As ArrayList = New ArrayList()
                While ienum.MoveNext
                    Dim fi As FileInfo = DirectCast(ienum.Current, FileInfo)
                    'TODO: chk why this file's there
                    If fi.Name = "desktop.ini" Then
                    Else
                        fileNames.Add(fi.Name)
                    End If
                End While
                Return fileNames
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub SaveFile(ByVal filePathName As String, ByVal fileLength As Int32, ByVal inStream As Stream)
            Dim fs As FileStream = New FileStream(filePathName, FileMode.OpenOrCreate, FileAccess.Write)

            Try
                Dim input(fileLength) As Byte
                inStream.Read(input, 0, fileLength)

                fs.Write(input, 0, fileLength)

            Catch ex As Exception
            Finally
                fs.Close()
            End Try
        End Sub

        Public Shared Sub EnsureExists(ByVal path As String)
            Try
                Dim di As DirectoryInfo = New DirectoryInfo(path)
                If Not di.Exists Then
                    di.Create()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function Exists(ByVal path As String, Optional ByVal searchPattern As String = "") As Boolean
            Try
                If Not path.EndsWith("\") Then
                    path += "\"
                End If
                Dim di As DirectoryInfo = New DirectoryInfo(path)
                If searchPattern = "" Then
                    Return di.Exists
                Else
                    Return (di.GetFiles(searchPattern).Length > 0)
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Shared Sub FilesOperation(ByVal opType As FileSysOp, ByVal sourcePath As String, ByVal targetPath As String, Optional ByVal searchPattern As String = "", Optional ByVal fileName As String = "")
            Try
                If Not sourcePath.EndsWith("\") Then
                    sourcePath += "\"
                End If
                If Not targetPath.EndsWith("\") Then
                    targetPath += "\"
                End If
                'add escape chars if needed
                If sourcePath.IndexOf("\\", 2) = -1 Then
                    sourcePath = sourcePath.Replace("\", "\\")
                End If
                If targetPath.IndexOf("\\", 2) = -1 Then
                    targetPath = targetPath.Replace("\", "\\")
                End If

                Dim diSource As DirectoryInfo = New DirectoryInfo(sourcePath)
                Dim diTarget As DirectoryInfo = New DirectoryInfo(targetPath)
                Dim fiInfo As FileInfo
                'check if doing all files/subdirs
                If fileName = "" Then
                    'try to move/copy all in one shot if target does not exist
                    If diTarget.Exists() Then
                        'move/copy files
                        Dim ienum As IEnumerator
                        If searchPattern = "" Then
                            ienum = diSource.GetFiles().GetEnumerator()
                        Else
                            ienum = diSource.GetFiles(searchPattern).GetEnumerator()
                        End If
                        While ienum.MoveNext
                            fiInfo = DirectCast(ienum.Current, FileInfo)
                            'TODO: chk why this file's there
                            If fiInfo.Name <> "desktop.ini" Then

                                Select Case opType
                                    Case FileSysOp.MoveOp
                                        Dim fiTarget As FileInfo = New FileInfo(targetPath + fiInfo.Name)
                                        If fiTarget.Exists Then
                                            fiTarget.Delete()
                                        End If
                                        fiInfo.MoveTo(targetPath)
                                    Case FileSysOp.CopyOp
                                        fiInfo.CopyTo(targetPath + fiInfo.Name, True)
                                End Select
                            End If
                        End While

                        'JDS 7/24/2003: no need to go to subfolders
                        ''move/copy subdirs
                        'Dim subDirInfo As DirectoryInfo
                        'Dim targetSubDir As String
                        'ienum = diSource.GetDirectories().GetEnumerator()
                        'While ienum.MoveNext
                        '    subDirInfo = DirectCast(ienum.Current, DirectoryInfo)
                        '    targetSubDir = targetPath + subDirInfo.Name
                        '    'use same function - recursive call
                        '    Select Case opType
                        '        Case FileSysOp.MoveOp
                        '            MoveFiles(subDirInfo.FullName, targetSubDir)
                        '        Case FileSysOp.CopyOp
                        '            CopyFiles(subDirInfo.FullName, targetSubDir)
                        '    End Select
                        'End While

                    Else 'if target doesn't exist
                        Dim ienum As IEnumerator
                        Select Case opType
                            Case FileSysOp.MoveOp
                                If searchPattern = "" Then
                                    diSource.MoveTo(targetPath)
                                Else
                                    CopyFiles(sourcePath, targetPath, searchPattern)
                                    ienum = diSource.GetFiles(searchPattern).GetEnumerator()
                                    While ienum.MoveNext
                                        fiInfo = DirectCast(ienum.Current, FileInfo)
                                        fiInfo.Delete()
                                    End While
                                End If
                            Case FileSysOp.CopyOp
                                diTarget.Create()
                                If searchPattern = "" Then
                                    CopyFiles(sourcePath, targetPath)
                                Else
                                    CopyFiles(sourcePath, targetPath, searchPattern)
                                End If

                        End Select
                    End If

                Else 'if only one file
                    fiInfo = New FileInfo(sourcePath + fileName)
                    If fiInfo.Exists() Then
                        'first, make sure target folder exists
                        If Not diTarget.Exists() Then
                            diTarget.Create()
                        End If
                        Select Case opType
                            Case FileSysOp.MoveOp
                                Dim fiTarget As FileInfo = New FileInfo(targetPath + fiInfo.Name)
                                If fiTarget.Exists Then
                                    fiTarget.Delete()
                                End If
                                fiInfo.MoveTo(targetPath + fiInfo.Name)
                            Case FileSysOp.CopyOp
                                fiInfo.CopyTo(targetPath + fiInfo.Name, True)
                        End Select
                    Else
                        Throw New FileNotFoundException("Source file does not exist.")
                    End If
                End If

            Catch ex As Exception
                Throw ex
            End Try

        End Sub
    End Class ' END CLASS DEFINITION FileSystemManager

End Namespace ' HotTopic.RD.Services

