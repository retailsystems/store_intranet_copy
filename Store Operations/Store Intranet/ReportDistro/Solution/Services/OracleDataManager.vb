Imports System.Data.OracleClient
Namespace HotTopic.RD.Services
    Public Enum EnumRSTypes As Byte
        DataSet = 1
        DataReader = 2
    End Enum
    Public Class OracleDataManager
        Public strConn As String
        Private cnnSQL As OracleConnection
        Sub New()
            GetConnectionString()
        End Sub


        Public ReadOnly Property Connection() As OracleConnection
            Get
                Return cnnSQL
            End Get
        End Property

        Private Sub GetConnectionString()
            strConn = HotTopic.RD.Settings.AppSetting.OracleConnectionString
        End Sub
        Public Function OpenConnection() As OracleConnection
            If IsNothing(cnnSQL) Then
                cnnSQL = New OracleConnection()
            End If

            Try
                cnnSQL.ConnectionString = strConn
                cnnSQL.Open()
                Return cnnSQL
            Catch e As Exception
                Dim x As String = e.ToString()
            End Try
        End Function
        Public Function CloseConnection()
            If cnnSQL.State = ConnectionState.Open Then
                cnnSQL.Close()
            End If
        End Function

        Public Function CloseConnection(ByRef oCnn As OracleConnection)
            oCnn.Close()
        End Function
        Public Function ExecuteQuery(ByVal rsType As EnumRSTypes, ByVal strSQL As String) As Object
            If IsNothing(cnnSQL) Then
                cnnSQL = New OracleConnection()
            End If

            If cnnSQL.State = ConnectionState.Closed Or cnnSQL.State = ConnectionState.Broken Then
                'oprn connection string
                OpenConnection()
            End If

            Select Case rsType
                Case EnumRSTypes.DataReader
                    'open reader
                    Dim cmd As New OracleCommand(strSQL, cnnSQL)
                    Return cmd.ExecuteReader()
                Case EnumRSTypes.DataSet
                    'open data set
                    Dim da As New OracleDataAdapter(strSQL, cnnSQL)

                    Dim ds As New DataSet()
                    da.Fill(ds, "SQLTable")
                    Return ds
                Case Else
                    Return Nothing
            End Select
        End Function
        Public Function ExecuteNonQuery(ByVal strSQL As String) As Integer

            If IsNothing(cnnSQL) Then
                cnnSQL = New OracleConnection()
            End If

            If cnnSQL.State = ConnectionState.Closed Or cnnSQL.State = ConnectionState.Broken Then
                'oprn connection string
                OpenConnection()
            End If

            Dim cmd As New OracleCommand(strSQL, cnnSQL)
            cmd.CommandType = CommandType.Text
            Return cmd.ExecuteNonQuery()
        End Function
    End Class
End Namespace