' Static Model
Imports HotTopic.RD.Entity
Imports HotTopic.RD.Reports
Imports HotTopic.RD.Services
Imports System.Data.SqlClient
Namespace HotTopic.RD.Reports
    '*******************************************************************************
    '**		Change History
    '*******************************************************************************
    '**		Date:		Author:				Description:
    '**		6/17/03		Sri bajjuri			new method AddApprovers added.
    '**    
    '*******************************************************************************/



    Public Class ReportManager

        Public Shared Sub Save(ByVal rpt As Report, ByVal userID As String)
            Try
                ' Set report parameters (18 input, 1 output) 
                Dim arParms() As SqlParameter = New SqlParameter(19) {}
                With rpt
                    arParms(0) = New SqlParameter("@reportId", SqlDbType.Int, 4, ParameterDirection.InputOutput)
                    arParms(0).Value = .ReportId
                    arParms(1) = New SqlParameter("@ReportTitle", SqlDbType.VarChar, 30)
                    arParms(1).Value = .Title
                    arParms(2) = New SqlParameter("@ReportDesc", SqlDbType.VarChar, 255)
                    arParms(2).Value = .Description
                    arParms(3) = New SqlParameter("@InputType", SqlDbType.Int)
                    arParms(3).Value = .InputType
                    arParms(4) = New SqlParameter("@StagingPath", SqlDbType.VarChar, 255)
                    arParms(4).Value = .StagingPath
                    arParms(5) = New SqlParameter("@LocationPath", SqlDbType.VarChar, 255)
                    arParms(5).Value = .LocationPath
                    arParms(6) = New SqlParameter("@ExpirationType", SqlDbType.Int)
                    arParms(6).Value = .ExpirationType
                    arParms(7) = New SqlParameter("@SortOrder", SqlDbType.Int)
                    arParms(7).Value = .SortOrder
                    arParms(8) = New SqlParameter("@DefaultFile", SqlDbType.VarChar, 50)
                    arParms(8).Value = .DefaultFileName
                    arParms(9) = New SqlParameter("@UserId", SqlDbType.VarChar, 20)
                    arParms(9).Value = userID
                    arParms(10) = New SqlParameter("@Scope", SqlDbType.Int)
                    arParms(10).Value = .ContentScope
                    arParms(11) = New SqlParameter("@FileIdentifier", SqlDbType.VarChar, 50)
                    arParms(11).Value = .DefaultFileIdentifier
                    arParms(12) = New SqlParameter("@FileFormat", SqlDbType.VarChar, 50)
                    arParms(12).Value = .FileFormat
                    arParms(13) = New SqlParameter("@ArchiveNth", SqlDbType.Int)
                    arParms(13).Value = UtilityManager.IntegerToNull(.ArchiveNth)
                    arParms(14) = New SqlParameter("@MaxFileSize", SqlDbType.Int)
                    arParms(14).Value = .MaxFileSize
                    arParms(15) = New SqlParameter("@ApproverEmail", SqlDbType.VarChar, 50)
                    arParms(15).Value = .ApproverGroupEmail
                    arParms(16) = New SqlParameter("@ApprovalType", SqlDbType.Int)
                    arParms(16).Value = .ApprovalType
                    arParms(17) = New SqlParameter("@ReadOnlyFlag", SqlDbType.Bit)
                    arParms(17).Value = .ReadOnlyFlag
                    arParms(18) = New SqlParameter("@NewReportID", SqlDbType.Int)
                    arParms(18).Value = 0
                    arParms(18).Direction = ParameterDirection.Output
                    arParms(19) = New SqlParameter("@AllowMultiFlag", SqlDbType.Bit)
                    arParms(19).Value = .AllowMultiFlag


                End With
                SQLDataManager.GetInstance().Execute("dbo.RSP_UpdateReport", arParms)
                rpt.ReportId = UtilityManager.NullToInteger(arParms(18).Value)
                If rpt.ReportId > 0 Then
                    ' add company owners
                    Dim i As Integer = 0
                    Dim arParms1() As SqlParameter = New SqlParameter(2) {}
                    For i = 0 To rpt.CompanyOwners.Count - 1
                        Dim _company As New CompanyOwner()
                        _company = rpt.CompanyOwners(i)
                        With _company
                            arParms1(0) = New SqlParameter("@ReportId", SqlDbType.Int)
                            arParms1(0).Value = rpt.ReportId
                            arParms1(1) = New SqlParameter("@CompanyId", SqlDbType.Int)
                            arParms1(1).Value = .CompanyId
                            arParms1(2) = New SqlParameter("@CDStoresFlag", SqlDbType.Bit)
                            arParms1(2).Value = .CDStoresFlag
                            SQLDataManager.GetInstance().Execute("dbo.RSP_AddCompanyOwner", arParms1)
                        End With
                    Next
                End If
           
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub

        ' Gets a report and associated approvers and owners
        Public Shared Function GetReport(ByVal reportId As Int32) As Report
            Try
                Dim rpt As Report = New Report()
                rpt.ReportId = reportId
                EntityManager.GetReportById(rpt, True)
                Return rpt
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Function

        ' Lists all reports in the Report table.
        Public Shared Function List() As DataSet
            'Dim rds As ReportDataSet = New ReportDataSet()
            Dim ds As DataSet
            ds = SQLDataManager.GetInstance().GetDataSet("dbo.RSP_ListReports")

            Return ds
        End Function

        Public Shared Sub Submit(ByVal rptSubmission As Submission, ByVal userID As String)
            Try
                ' Set report parameters (12 input) 
                Dim arParms() As SqlParameter = New SqlParameter(12) {}
                With rptSubmission
                    arParms(0) = New SqlParameter("@ReportSubmissionID", SqlDbType.Int)
                    arParms(0).Direction = ParameterDirection.InputOutput
                    arParms(0).Value = .ReportSubmissionId
                    arParms(1) = New SqlParameter("@reportId", SqlDbType.Int)
                    arParms(1).Value = .ReportId
                    arParms(2) = New SqlParameter("@ReportDate", SqlDbType.DateTime)
                    arParms(2).Value = .ReportDate
                    arParms(3) = New SqlParameter("@Status", SqlDbType.Int)
                    arParms(3).Value = .Status
                    arParms(4) = New SqlParameter("@UserId", SqlDbType.VarChar, 30)
                    arParms(4).Value = userID
                    arParms(5) = New SqlParameter("@StagingPath", SqlDbType.VarChar, 255)
                    arParms(5).Value = .StagingPath
                    arParms(6) = New SqlParameter("@LocationPath", SqlDbType.VarChar, 255)
                    arParms(6).Value = .LocationPath
                    arParms(7) = New SqlParameter("@EffectivityStartDate", SqlDbType.DateTime)
                    arParms(7).Value = .EffectivityStartDate
                    arParms(8) = New SqlParameter("@Comment", SqlDbType.VarChar, 2000)
                    arParms(8).Value = .Comment
                    arParms(9) = New SqlParameter("@EffectivityEndDate", SqlDbType.DateTime)
                    arParms(9).Value = UtilityManager.DateToNull(.EffectivityEndDate)
                    arParms(10) = New SqlParameter("@MDReportType", SqlDbType.Int)
                    arParms(10).Value = UtilityManager.IntegerToNull(.MDReportType)
                    arParms(11) = New SqlParameter("@MDEffectiveDate", SqlDbType.DateTime)
                    arParms(11).Value = UtilityManager.DateToNull(.MDEffectiveDate)
                    arParms(12) = New SqlParameter("@MDPcNum", SqlDbType.VarChar, 50)
                    arParms(12).Value = UtilityManager.StringToNull(.MDPcNum)

                End With
                SQLDataManager.GetInstance().Execute("dbo.RSP_AddReportSubmission", arParms)
                rptSubmission.ReportSubmissionId = arParms(0).Value
                EntityManager.GetReportSubmissionById(rptSubmission)
                Try
                    'put file into staging if upload
                    If rptSubmission.InputType = FileInputType.Upload Then
                        'JDS 7/22/2003: remove; will just use submissionid to uniquely identify file uploaded
                        'Dim stagingPath As String = rptSubmission.StagingPath + rptSubmission.FiscalWeekEndingDate.ToString("MMddyyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo) + "\"
                        'Dim fileName As String = rptSubmission.FileSubmitted.Name.Replace(rptSubmission.FileSubmitted.Name.Substring(0, rptSubmission.FileSubmitted.Name.LastIndexOf(".") + 1), rptSubmission.ReportSubmissionId)
                        Dim fileName As String = rptSubmission.ReportSubmissionId & "_" & rptSubmission.FileSubmitted.Name
                        FileSystemManager.EnsureExists(rptSubmission.StagingPath)
                        'FileSystemManager.DeleteFiles(rptSubmission.StagingPath)
                        FileSystemManager.SaveFile(rptSubmission.StagingPath + fileName, rptSubmission.FileSubmitted.Length, rptSubmission.FileSubmitted.FileStream)
                    End If
                 
                Catch ex As Exception
                    'rollback tran; delete ReportSubmission
                    arParms = New SqlParameter(0) {}
                    arParms(0) = New SqlParameter("@reportSubmissionId", SqlDbType.Int)
                    arParms(0).Value = rptSubmission.ReportSubmissionId
                    SQLDataManager.GetInstance().Execute("dbo.RSP_DeleteReportSubmissionById", arParms)
                    Throw ex
                End Try
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        'Public Shared Sub GetMDMasterDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByVal companyId As Int16)
        '    'Markdown master report
        '    Try
        '        'Create Store Dataset
        '        Dim Cn As New OracleDataManager()
        '        Dim strSql As String

        '        Dim effectiveDate, pc_tp, prcSql, whereSql As String
        '        Dim companyFilter As Int16 ' 1- HotTopic , 2 - Torrid, 0 - Both

        '        prcSql = ""
        '        Select Case rptSubmission.MDReportType
        '            Case MDReportType.Regular
        '                pc_tp = "MD"
        '                'prcSql = "and (gm_prc.ret_prc like '%.99' or gm_prc.ret_prc LIKE '%.98') "
        '                prcSql = "and gm_prc.ret_prc <> .01 and HOTTOPIC.HT_GET_PRC_STAT.GET_ITEM_SINGLE_PRC_STAT(gm_prc.itm_cd,least(trunc(sysdate),trunc(gm_prc.beg_dt - 1)) ,'9999') = 'REG' "
        '            Case MDReportType.Further
        '                pc_tp = "MD"
        '                'prcSql = "and gm_prc.ret_prc > .01 and gm_prc.ret_prc not like '%.98' and gm_prc.ret_prc not like '%.99' "
        '                prcSql = "and gm_prc.ret_prc <> .01 and HOTTOPIC.HT_GET_PRC_STAT.GET_ITEM_SINGLE_PRC_STAT(gm_prc.itm_cd,least(trunc(sysdate),trunc(gm_prc.beg_dt - 1)) ,'9999') = 'MD' "
        '            Case MDReportType.MOS
        '                pc_tp = "MD"
        '                prcSql = "and gm_prc.ret_prc = .01 "
        '            Case MDReportType.MDAll
        '                pc_tp = "MD"
        '                prcSql = ""
        '            Case MDReportType.MarkUp
        '                pc_tp = "MU"
        '                prcSql = ""
        '            Case MDReportType.MDCancel
        '                pc_tp = "MDC"
        '                prcSql = ""
        '            Case MDReportType.MUCancel
        '                pc_tp = "MUC"
        '                prcSql = ""
        '        End Select


        '        If rptSubmission.MDPcNum <> "" AndAlso IsNumeric(rptSubmission.MDPcNum) Then
        '            whereSql = "where pc_num = '" & rptSubmission.MDPcNum & "' "
        '        ElseIf IsDate(rptSubmission.MDEffectiveDate) Then
        '            effectiveDate = rptSubmission.MDEffectiveDate.Day & "-" & MonthName(rptSubmission.MDEffectiveDate.Month, True).ToLower & "-" & rptSubmission.MDEffectiveDate.Year
        '            whereSql = "where beg_dt = '" & effectiveDate & "' "
        '        Else
        '            Throw New Exception("MDReport:Invalid MD report request")
        '        End If

        '        Select Case companyId
        '            Case 1 ' Hot Topic
        '                whereSql &= "and dept.div_cd = 1 "
        '            Case 2 ' Torrid
        '                whereSql &= "and dept.div_cd = 5 "
        '            Case 3 ' BH
        '                whereSql &= "and dept.div_cd = 8 "
        '        End Select
        '        'Get MD Dataset
        '        'strSql = "SELECT PRC.SKU_NUM,PRC.ITM_CD,PRC.RET_PRC,PRC.PREV_PERM_RET, PRC.PC_NUM, PRC.BEG_DT " & _
        '        '        " ,PRC.PC_TP,GM_ITM.DEPT_CD,CLASS.CLASS_CD,CLASS.DES AS CLASS_DESC" & _
        '        '        " , GM_ITM.DES1 AS ITEM_DESC,GM_SKU.SIZE_CD, LPAD(GM_ITM.VE_CD,7) AS VE_CD " & _
        '        '        " FROM GM_PRC PRC, CLASS, GM_ITM, GM_SKU, DEPT" & _
        '        '        whereSql & " AND pc_tp ='" & pc_tp & "' " & prcSql & _
        '        '        " AND PC_NUM = (SELECT MAX(PC_NUM) FROM GM_PRC t1 WHERE PRC.SKU_NUM = t1.SKU_NUM AND PRC.ITM_CD = t1.ITM_CD)" & _
        '        '        " AND PRC.ITM_CD = GM_ITM.ITM_CD " & _
        '        '        " AND GM_ITM.DEPT_CD = DEPT.DEPT_CD " & _
        '        '        " AND GM_ITM.CLASS_CD = CLASS.CLASS_CD" & _
        '        '        " AND GM_SKU.SKU_NUM = PRC.SKU_NUM" & _
        '        '        " ORDER BY GM_ITM.DEPT_CD,CLASS.CLASS_CD,VE_CD ,PRC.ITM_CD,PRC.SKU_NUM"
        '        ' 06/30/2006 - Sri Bajjuri - Set the current RET_PRC (effective today) as PREV_PERM_RET price
        '        strSql = "select /*+ ALL_ROWS*/ /*Sri Bajjuri - Report distro MD reports*/ gm_prc.sku_num, gm_prc.itm_cd, gm_prc.ret_prc, gm_prc.pc_num, gm_prc.beg_dt, gm_prc.pc_tp, gm_prc.prev_perm_ret, " & _
        '                "gm_sku.size_cd, lpad(gm_itm.ve_cd,7) as ve_cd, gm_itm.item_desc as item_desc, gm_itm.dept_cd, gm_itm.class_cd, class.des as class_desc " & _
        '                "from gm_prc " & _
        '                "inner join gm_sku on gm_sku.sku_num = gm_prc.sku_num " & _
        '                " inner join (select i.itm_cd, i.ve_cd,i.dept_cd,i.class_cd, NVL(wd.web_des, i.des1) as item_desc FROM GM_ITM i " & _
        '                "left join RPT_SERVICES_GERS_INQ.bm_itm_wdes wd on i.itm_cd = wd.itm_cd) gm_itm on gm_itm.itm_cd = gm_prc.itm_cd " & _
        '                "inner join class on gm_itm.class_cd = class.class_cd " & _
        '                "inner join dept on gm_itm.dept_cd = dept.dept_cd " & _
        '                whereSql & "and pc_tp ='" & pc_tp & "' " & prcSql & _
        '                "and gm_prc.prc_zone_cd = (select prc_zone_cd from store where store_cd = 9999) " & _
        '                "and gm_prc.prc_grp_cd = (select prc_grp_cd from store where store_cd = 9999) " & _
        '                "order by gm_itm.dept_cd, gm_itm.class_cd, gm_prc.ret_prc desc, ve_cd, gm_prc.itm_cd, gm_prc.sku_num"
        '        'Throw New Exception("MDReport:" & strSql)
        '        mdDs = Cn.ExecuteQuery(EnumRSTypes.DataSet, strSql)

        '        Cn.CloseConnection()
        '        If mdDs.Tables(0).Rows.Count = 0 Then
        '            Throw New Exception("MDReport:No records found")
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        'Public Shared Sub GetMDWHDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByVal companyId As Int16, ByVal warehouse As String)
        '    'Markdown ware house report
        '    Try
        '        'Create Store Dataset
        '        Dim Cn As New OracleDataManager()
        '        Dim strSql As String

        '        Dim effectiveDate, pc_tp, prcSql, whereSql As String

        '        Select Case rptSubmission.MDReportType
        '            Case MDReportType.Regular
        '                pc_tp = "MD"
        '                'prcSql = "and (gm_prc.ret_prc like '%.99' or gm_prc.ret_prc LIKE '%.98') "
        '                prcSql = "and gm_prc.ret_prc <> .01 and HOTTOPIC.HT_GET_PRC_STAT.GET_ITEM_SINGLE_PRC_STAT(gm_prc.itm_cd,least(trunc(sysdate),trunc(gm_prc.beg_dt - 1)) ,'9999') = 'REG' "
        '            Case MDReportType.Further
        '                pc_tp = "MD"
        '                'prcSql = "and gm_prc.ret_prc > .01 and gm_prc.ret_prc not like '%.98' and gm_prc.ret_prc not like '%.99' "
        '                prcSql = "and gm_prc.ret_prc <> .01 and HOTTOPIC.HT_GET_PRC_STAT.GET_ITEM_SINGLE_PRC_STAT(gm_prc.itm_cd,least(trunc(sysdate),trunc(gm_prc.beg_dt - 1)) ,'9999') = 'MD' "
        '            Case MDReportType.MOS
        '                pc_tp = "MD"
        '                prcSql = "and gm_prc.ret_prc = .01 "
        '            Case MDReportType.MDAll
        '                pc_tp = "MD"
        '                prcSql = ""
        '            Case MDReportType.MarkUp
        '                pc_tp = "MU"
        '                prcSql = ""
        '            Case MDReportType.MDCancel
        '                pc_tp = "MDC"
        '                prcSql = ""
        '            Case MDReportType.MUCancel
        '                pc_tp = "MUC"
        '                prcSql = ""
        '        End Select


        '        If rptSubmission.MDPcNum <> "" AndAlso IsNumeric(rptSubmission.MDPcNum) Then
        '            whereSql = "where pc_num = '" & rptSubmission.MDPcNum & "' "
        '        ElseIf IsDate(rptSubmission.MDEffectiveDate) Then
        '            effectiveDate = rptSubmission.MDEffectiveDate.Day & "-" & MonthName(rptSubmission.MDEffectiveDate.Month, True).ToLower & "-" & rptSubmission.MDEffectiveDate.Year
        '            whereSql = "where beg_dt = '" & effectiveDate & "' "
        '        Else
        '            Throw New Exception("MDReport:Invalid MD report request")
        '        End If
        '        Select Case companyId
        '            Case 1 ' Hot Topic
        '                whereSql &= "and dept.div_cd = 1 "
        '            Case 2 ' Torrid
        '                whereSql &= "and dept.div_cd = 5 "
        '            Case 3 ' BlackHeart
        '                whereSql &= "and dept.div_cd = 8 "
        '        End Select
        '        'Get MD Dataset
        '        ' 06/30/2006 - Sri Bajjuri - Set the current RET_PRC (effective today) as PREV_PERM_RET price
        '        strSql = "select /*+ ALL_ROWS*/ /*Sri Bajjuri - Report distro MD reports*/ gm_prc.sku_num, gm_prc.itm_cd, gm_prc.ret_prc, gm_prc.pc_num, gm_prc.beg_dt, gm_prc.pc_tp, gm_prc.prev_perm_ret, " & _
        '                "gm_inv_loc_v.store_cd, gm_inv_loc_v.loc_cd, gm_inv_loc_v.loc_tp, gm_inv_loc_v.oh_qty, " & _
        '                "gm_sku.size_cd, lpad(gm_itm.ve_cd,7) as ve_cd, gm_itm.item_desc as item_desc,  gm_itm.dept_cd, gm_itm.class_cd, class.des as class_desc, ap.ve.ve_name " & _
        '                "from gm_prc " & _
        '                "inner join gm_sku on gm_sku.sku_num = gm_prc.sku_num " & _
        '                "inner join gm_inv_loc_v on gm_inv_loc_v.sku_num = gm_prc.sku_num and gm_inv_loc_v.store_cd = " & warehouse & " and gm_inv_loc_v.oh_qty > 0 " & _
        '               " inner join (select i.itm_cd, i.ve_cd,i.dept_cd,i.class_cd, NVL(wd.web_des, i.des1) as item_desc FROM GM_ITM i " & _
        '                "left join RPT_SERVICES_GERS_INQ.bm_itm_wdes wd on i.itm_cd = wd.itm_cd) gm_itm on gm_itm.itm_cd = gm_prc.itm_cd " & _
        '                "inner join class on gm_itm.class_cd = class.class_cd " & _
        '                "inner join dept on gm_itm.dept_cd = dept.dept_cd " & _
        '                "inner join ap.ve on ap.ve.ve_cd = gm_itm.ve_cd " & _
        '                whereSql & "and pc_tp ='" & pc_tp & "' " & prcSql & _
        '                "and gm_prc.prc_zone_cd = (select prc_zone_cd from store where store_cd = 9999) " & _
        '                "and gm_prc.prc_grp_cd = (select prc_grp_cd from store where store_cd = 9999) " & _
        '                "order by gm_inv_loc_v.loc_tp, gm_inv_loc_v.loc_cd, gm_itm.dept_cd, gm_itm.class_cd, gm_prc.ret_prc desc, gm_itm.ve_cd, gm_prc.itm_cd, gm_prc.sku_num"
        '        mdDs = Cn.ExecuteQuery(EnumRSTypes.DataSet, strSql)
        '        'Throw New Exception(strSql)
        '        Cn.CloseConnection()
        '        If mdDs.Tables(0).Rows.Count = 0 Then
        '            Throw New Exception("MDReport:No records found")
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        ''SRI 12/18/03 MD Reports
        'Public Shared Sub GetMDReportDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByRef storeDs As DataSet)
        '    Try
        '        'Create Store Dataset
        '        Dim Cn As New OracleDataManager()
        '        Dim strSql As String

        '        Dim effectiveDate, pc_tp, prcSql, whereSql As String
        '        Dim companyFilter As Int16 ' 1- HotTopic , 2 - Torrid, 0 - Both
        '        Dim cOwner As CompanyOwner
        '        If rptSubmission.CompanyOwners.Count = 1 Then
        '            cOwner = rptSubmission.CompanyOwners(0)
        '            companyFilter = cOwner.CompanyId
        '        Else
        '            companyFilter = 0
        '        End If

        '        Select Case rptSubmission.MDReportType
        '            Case MDReportType.Regular
        '                pc_tp = "MD"
        '                'prcSql = "and (gm_prc.ret_prc like '%.99' or gm_prc.ret_prc LIKE '%.98') "
        '                prcSql = "and gm_prc.ret_prc <> .01 and HOTTOPIC.HT_GET_PRC_STAT.GET_ITEM_SINGLE_PRC_STAT(gm_prc.itm_cd,least(trunc(sysdate),trunc(gm_prc.beg_dt - 1)) ,'9999') = 'REG' "
        '            Case MDReportType.Further
        '                pc_tp = "MD"
        '                'prcSql = "and gm_prc.ret_prc > .01 and gm_prc.ret_prc not like '%.98' and gm_prc.ret_prc not like '%.99' "
        '                prcSql = "and gm_prc.ret_prc <> .01 and HOTTOPIC.HT_GET_PRC_STAT.GET_ITEM_SINGLE_PRC_STAT(gm_prc.itm_cd,least(trunc(sysdate),trunc(gm_prc.beg_dt - 1)) ,'9999') = 'MD' "
        '            Case MDReportType.MOS
        '                pc_tp = "MD"
        '                prcSql = "and gm_prc.ret_prc = .01 "
        '            Case MDReportType.MDAll
        '                pc_tp = "MD"
        '                prcSql = ""
        '                '# disable for RMS  01/31/2013 by LJ
        '                'Case MDReportType.MarkUp
        '                '    pc_tp = "MU"
        '                '    prcSql = ""
        '                'Case MDReportType.MDCancel
        '                '    pc_tp = "MDC"
        '                '    prcSql = ""
        '                'Case MDReportType.MUCancel
        '                '    pc_tp = "MUC"
        '                '    prcSql = ""
        '        End Select


        '        If rptSubmission.MDPcNum <> "" AndAlso IsNumeric(rptSubmission.MDPcNum) Then
        '            whereSql = "where pc_num = '" & rptSubmission.MDPcNum & "' "
        '            '6/7/04 5PM quick fix roll this back
        '            '6/7/04 6PM quick fix rolled back
        '            'prcSql = " AND PRC.RET_PRC LIKE '%.99' "
        '        ElseIf IsDate(rptSubmission.MDEffectiveDate) Then
        '            effectiveDate = rptSubmission.MDEffectiveDate.Day & "-" & MonthName(rptSubmission.MDEffectiveDate.Month, True).ToLower & "-" & rptSubmission.MDEffectiveDate.Year
        '            whereSql = "where beg_dt = '" & effectiveDate & "' "
        '        Else
        '            Throw New Exception("MDReport:Invalid MD report request")
        '        End If
        '        ' ht, torrid dept filter
        '        If companyFilter > 0 Then
        '            Select Case companyFilter
        '                Case 1 ' Hot Topic
        '                    whereSql &= "and dept.div_cd = 1 "
        '                Case 2 ' Torrid
        '                    whereSql &= "and dept.div_cd = 5 "
        '                Case 3 'Blackheart
        '                    whereSql &= "and dept.div_cd = 8 "
        '            End Select
        '        End If

        '        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        '        arParms(0) = New SqlParameter("@CompanyFilter", SqlDbType.Int)
        '        arParms(0).Value = companyFilter
        '        storeDs = SQLDataManager.GetInstance().GetDataSet("dbo.RSP_ListStores", arParms)
        '        '" WHERE BEG_DT = '" & effectiveDate & "' AND pc_tp ='" & pc_tp & "' " & prcSql & _
        '        'Get MD Dataset
        '        ' MOD #10/04/2004 Exclude layaway inventory (layaway loc_cd is 'HOLD') from MD report
        '        ' 06/30/2006 - Sri Bajjuri - Set the current RET_PRC (effective today) as PREV_PERM_RET price
        '        strSql = "select /*+ ALL_ROWS*/ /*Sri Bajjuri - Report distro MD reports*/ gm_prc.sku_num, gm_prc.itm_cd, gm_prc.ret_prc, gm_prc.pc_num, gm_prc.beg_dt, gm_prc.pc_tp, gm_prc.prev_perm_ret, " & _
        '                "to_number(gm_inv_loc_v.store_cd) as store_cd, gm_inv_loc_v.oh_qty, " & _
        '                "gm_sku.size_cd, lpad(gm_itm.ve_cd,7) as ve_cd, gm_itm.item_desc as item_desc, gm_itm.dept_cd, gm_itm.class_cd, class.des as class_desc, ap.ve.ve_name " & _
        '                "from gm_prc " & _
        '                "inner join gm_inv_loc_v on gm_inv_loc_v.sku_num = gm_prc.sku_num and gm_inv_loc_v.oh_qty > 0 and loc_cd <> 'HOLD' " & _
        '                "inner join gm_sku on gm_sku.sku_num = gm_prc.sku_num " & _
        '                " inner join (select i.itm_cd, i.ve_cd,i.dept_cd,i.class_cd, NVL(wd.web_des, i.des1) as item_desc FROM GM_ITM i " & _
        '                "left join RPT_SERVICES_GERS_INQ.bm_itm_wdes wd on i.itm_cd = wd.itm_cd) gm_itm on gm_itm.itm_cd = gm_prc.itm_cd " & _
        '                "inner join class on gm_itm.class_cd = class.class_cd " & _
        '                "inner join dept on gm_itm.dept_cd = dept.dept_cd " & _
        '                "inner join ap.ve on ap.ve.ve_cd = gm_itm.ve_cd " & _
        '                whereSql & "and pc_tp ='" & pc_tp & "' " & prcSql & _
        '                "and gm_prc.prc_zone_cd = (select prc_zone_cd from store where store_cd = 9999) " & _
        '                "and gm_prc.prc_grp_cd = (select prc_grp_cd from store where store_cd = 9999) " & _
        '                "order by gm_inv_loc_v.store_cd, gm_itm.dept_cd, gm_itm.class_cd, gm_prc.ret_prc desc, gm_prc.itm_cd, gm_prc.sku_num"

        '        mdDs = Cn.ExecuteQuery(EnumRSTypes.DataSet, strSql)
        '        'Throw New Exception(strSql)
        '        Cn.CloseConnection()
        '        If mdDs.Tables(0).Rows.Count = 0 Then
        '            Throw New Exception("MDReport:No records found")
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        Public Shared Sub DeleteReportSubmissionById(ByVal rptSubmissionId As Integer)
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@reportSubmissionId", SqlDbType.Int)
                arParms(0).Value = rptSubmissionId
                SQLDataManager.GetInstance().Execute("dbo.RSP_DeleteReportSubmissionById", arParms)
            Catch ex As Exception

            End Try
        End Sub
        'SRI 8/19/03 Update Submission Comment
        Public Shared Sub UpdateReportSubmissionComment(ByVal rptSubmission As Submission, ByVal userID As String)
            Try
                ' Set report parameters (3 input) 
                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                With rptSubmission
                    arParms(0) = New SqlParameter("@ReportSubmissionID", SqlDbType.Int)
                    arParms(0).Value = .ReportSubmissionId
                    arParms(1) = New SqlParameter("@UserId", SqlDbType.VarChar, 30)
                    arParms(1).Value = userID
                    arParms(2) = New SqlParameter("@Comment", SqlDbType.VarChar, 2000)
                    arParms(2).Value = .Comment
                End With
                SQLDataManager.GetInstance().Execute("dbo.RSP_UpdateSubmissionComment", arParms)

            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Public Shared Sub SaveJobCodeOwners(ByVal reportId As Integer, ByVal companyId As Integer, ByVal jobCode As String, ByVal userID As String, Optional ByVal oldJobCode As String = "")
            Try
                'oldJobCode is required for updates
                Dim arParms() As SqlParameter = New SqlParameter(4) {}
                arParms(0) = New SqlParameter("@reportId", SqlDbType.Int)
                arParms(0).Value = reportId
                arParms(1) = New SqlParameter("@companyID", SqlDbType.Int)
                arParms(1).Value = companyId
                arParms(2) = New SqlParameter("@jobcode", SqlDbType.VarChar, 30)
                arParms(2).Value = jobCode
                arParms(3) = New SqlParameter("@UserId", SqlDbType.VarChar, 30)
                arParms(3).Value = userID
                arParms(4) = New SqlParameter("@oldJobcode", SqlDbType.VarChar, 30)
                arParms(4).Value = oldJobCode

                SQLDataManager.GetInstance().Execute("dbo.RSP_AddJobCodeOwner", arParms)
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        Public Shared Sub DeleteJobCodeOwner(ByVal reportId As Integer, ByVal companyId As Integer, ByVal jobCode As String)
            Try
                'oldJobCode is required for updates
                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                arParms(0) = New SqlParameter("@reportId", SqlDbType.Int)
                arParms(0).Value = reportId
                arParms(1) = New SqlParameter("@companyID", SqlDbType.Int)
                arParms(1).Value = companyId
                arParms(2) = New SqlParameter("@jobcode", SqlDbType.VarChar, 30)
                arParms(2).Value = jobCode
                SQLDataManager.GetInstance().Execute("dbo.RSP_DeleteJobCodeOwner", arParms)
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        Public Shared Sub DeleteSubCompanyOwners(ByVal reportId As Integer)
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@ReportId", SqlDbType.Int)
                arParms(0).Value = reportId
                SQLDataManager.GetInstance().Execute("RSP_DeleteSubCompanyOwnersByReport", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub SaveApprovers(ByVal reportId As Integer, ByVal approvers As ArrayList, ByVal userID As String)

            'delimApprovers is a "/" seperated list of approvers
            Dim delimApprovers As String
            Dim app As Approver

            For Each app In approvers
                If Len(delimApprovers) > 0 Then
                    delimApprovers += "/"
                End If
                delimApprovers += app.EmployeeId
            Next

            ' Set report parameters (3 input) 
            Try
                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                arParms(0) = New SqlParameter("@reportId", SqlDbType.Int)
                arParms(0).Value = reportId
                arParms(1) = New SqlParameter("@approverStr", SqlDbType.VarChar, 2000)
                arParms(1).Value = delimApprovers
                arParms(2) = New SqlParameter("@SubmitBy", SqlDbType.VarChar, 30)
                arParms(2).Value = userID

                SQLDataManager.GetInstance().Execute("dbo.RSP_AddApprovers", arParms)
            Catch ex As Exception
                ' exception handling code.
                Throw ex
            End Try
        End Sub
        Public Shared Function SaveSubCompanyOwners(ByVal reportId As Integer, ByVal SubCompanyOwners As ArrayList, ByVal userID As String)
            Try
                'Delete existing sub co.owners
                DeleteSubCompanyOwners(reportId)

                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                Dim subCoOwner As SubCompanyOwner
                For Each subCoOwner In SubCompanyOwners

                    arParms(0) = New SqlParameter("@ReportId", SqlDbType.Int)
                    arParms(0).Value = reportId
                    arParms(1) = New SqlParameter("@CompanyId", SqlDbType.Int)
                    arParms(1).Value = subCoOwner.CompanyId
                    arParms(2) = New SqlParameter("@OwnerId", SqlDbType.VarChar, 10)
                    arParms(2).Value = subCoOwner.OwnerID
                    SQLDataManager.GetInstance().Execute("RSP_AddSubCompanyOwner", arParms)
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetAllApprovers() As DataSet
            Dim ds As DataSet = SQLSecurityManager.GetInstance().GetDataSet("ssp_GetAllApprovers")
            Return ds
        End Function
        Public Shared Function ListDmStores(ByVal EmployeeID As String, ByVal UserRoleID As Integer) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(1) {}
                arParms(0) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 20)
                arParms(0).Value = EmployeeID
                arParms(1) = New SqlParameter("@UserRoleID", SqlDbType.Int)
                arParms(1).Value = UserRoleID
                Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_GetDMStores", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListAllActiveStores(ByVal CompanyID As Int16) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@CompanyID", SqlDbType.Int)
                arParms(0).Value = CompanyID '1-HT 2 - torrid

                Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_GetAllActiveStores", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListMLUDistricts(ByVal EmployeeID As String) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 20)
                arParms(0).Value = EmployeeID

                Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_GetMLUDistricts", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ListAllSubCompanyOwners(ByVal ReportId As Integer) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@ReportId", SqlDbType.Int)
                arParms(0).Value = ReportId
                Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("RSP_GetAllSubCompanyOwners", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Shared Function GetAllReportTitles(Optional ByVal inputType As FileInputType = 0) As DataSet
            Try
                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@inputType", SqlDbType.SmallInt)
                arParms(0).Value = UtilityManager.IntegerToNull(inputType)

                Dim ds As DataSet = SQLDataManager.GetInstance().GetDataSet("dbo.RSP_GetReportTitles", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub GetMDMasterDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByVal companyId As Int16)
            'Markdown master report
            Try
                'Create Store Dataset
                Dim Cn As New OracleDataManager()
                Dim strSql As String

                Dim effectiveDate, pc_tp, prcSql, whereSql, whereBegDt As String
                Dim companyFilter As Int16 ' 1- HotTopic , 2 - Torrid, 0 - Both
                strSql = ""
                prcSql = ""
                Select Case rptSubmission.MDReportType
                    Case MDReportType.Regular
                        'pc_tp = "MD" 
                        '# RMS change 02/08/2013 by LJ Regular/further/all markdown ret_prc>.01
                        prcSql = "and mrb.clear_retail>.01 "
                    Case MDReportType.Further
                        'pc_tp = "MD"
                        prcSql = "and mrb.clear_retail>.01 "
                    Case MDReportType.MOS
                        'pc_tp = "MD"
                        'prcSql = "and mrb.clear_retail=.01 "
                        prcSql = " "
                    Case MDReportType.MDAll
                        'pc_tp = "MD"
                        prcSql = "and mrb.clear_retail>.01 "
                        '# disabled for RMS 01/31/2013 by LJ
                        'Case MDReportType.MarkUp
                        '    pc_tp = "MU"
                        '    prcSql = ""
                        'Case MDReportType.MDCancel
                        '    pc_tp = "MDC"
                        '    prcSql = ""
                        'Case MDReportType.MUCancel
                        '    pc_tp = "MUC"
                        '    prcSql = ""
                End Select

                '# RMS change 02/08/2013 by LJ
                'If rptSubmission.MDPcNum <> "" AndAlso IsNumeric(rptSubmission.MDPcNum) Then
                '    '# RMS change for PC_NUM 02/07/13 by LJ
                '    'whereSql = "where pc_num = '" & rptSubmission.MDPcNum & "' " 
                '    whereSql = " and mrb.clearance_display_id = '" & rptSubmission.MDPcNum & "' "
                'Else
                If IsDate(rptSubmission.MDEffectiveDate) Then
                    effectiveDate = rptSubmission.MDEffectiveDate.Day & "-" & MonthName(rptSubmission.MDEffectiveDate.Month, True).ToLower & "-" & rptSubmission.MDEffectiveDate.Year
                    '# RMS change for effect date 02/07/13 by LJ
                    'whereSql = "where beg_dt = '" & effectiveDate & "' "
                    'whereBegDt = " and rfr.action_date = '" & effectiveDate & "' "

                    If (rptSubmission.MDReportType <> MDReportType.MOS) Then
                        whereBegDt = " and rfr.action_date = '" & effectiveDate & "' "
                    Else
                        whereBegDt = " and ud.uda_date = '" & effectiveDate & "' "
                    End If
                Else
                    Throw New Exception("MDReport:Invalid MD report request")
                End If

                Select Case companyId
                    Case 1 ' Hot Topic
						whereSql &= " and vd.division=1 "
                    Case 2 ' Torrid
                        whereSql &= " and vd.division=5 "
                    Case 3 ' BH
                        whereSql &= " and vd.division=8 "
                End Select

                '# RMS change 02/08/2013 by LJ 
                Select Case rptSubmission.MDReportType
                    Case MDReportType.Regular
						strSql = "WITH mrb AS " & _
				  "  (SELECT " & _
				  "    /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ " & _
				  "    im.item, " & _
				  "    rfr.item item_cd, " & _
				  "    NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
				  "    NVL(DECODE ('SIZE', " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1 " & _
				  "    ), im.diff_1, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 " & _
				  "    ), im.diff_2, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3 " & _
				  "    ), im.diff_3, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4 " & _
				  "    ), im.diff_4),'NONE') size_cd, " & _
				  "    im.dept, " & _
				  "    im.class, " & _
				  "    cl.class_name, " & _
				  "    rzl.zone_id, " & _
				  "    rzl.location, " & _
				  "    rfr.action_date, " & _
				  "    clear_retail, " & _
				  "    clear_start_ind, " & _
				  "    clear_mkdn_index, " & _
				  "    rfr.clearance_id, " & _
				  "    rfr.clearance_display_id, " & _
				  "    isp.supplier, " & _
				  "    sp.sup_name " & _
				  "  FROM rpm_future_retail rfr, " & _
				  "    item_master im, " & _
				  "    rpm_zone_location rzl, " & _
				  "    rpm_clearance rcl, " & _
				  "    class cl, " & _
				  "    item_supplier isp, " & _
				  "    sups sp " & _
				  "  WHERE rfr.item           = im.item_parent " & _
				  "  AND im.item_level        = im.tran_level " & _
				  "  AND rzl.zone_id          = rfr.location " & _
				  "  AND rfr.zone_node_type   = 1 " & _
				  "  AND rfr.clear_mkdn_index =1 " & _
				  "  AND rfr.clear_start_ind  =1 " & _
				  "  AND rfr.clearance_id     = rcl.clearance_id " & _
				  "  AND rcl.reset_date      IS NULL " & _
				  whereBegDt & _
				  "  AND im.dept              = cl.dept " & _
				  "  AND im.class             = cl.class " & _
				  "  AND im.item              = isp.item " & _
				  "  AND isp.primary_supp_ind = 'Y' " & _
				  "  AND isp.supplier         = sp.supplier " & _
				  "  UNION " & _
				  "  SELECT " & _
				  "    /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ " & _
				  "    im.item, " & _
				  "    rfr.item item_cd, " & _
				  "    NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
				  "    NVL(DECODE ('SIZE', " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1 " & _
				  "    ), im.diff_1, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 " & _
				  "    ), im.diff_2, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3 " & _
				  "    ), im.diff_3, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4 " & _
				  "    ), im.diff_4),'NONE') size_cd, " & _
				  "    im.dept, " & _
				  "    im.class, " & _
				  "    cl.class_name, " & _
				  "    rcl.zone_id, " & _
				  "    rcl.location, " & _
				  "    rfr.action_date, " & _
				  "    clear_retail, " & _
				  "    clear_start_ind, " & _
				  "    clear_mkdn_index, " & _
				  "    rfr.clearance_id, " & _
				  "    rfr.clearance_display_id, " & _
				  "    isp.supplier, " & _
				  "    sp.sup_name " & _
				  "  FROM rpm_future_retail rfr, " & _
				  "    item_master im, " & _
				  "    rpm_clearance rcl, " & _
				  "    class cl, " & _
				  "    item_supplier isp, " & _
				  "    sups sp " & _
				  "  WHERE rfr.item           = im.item_parent " & _
				  "  AND im.item_level        = im.tran_level " & _
				  "  AND rfr.location   =  rcl.location " & _
				  "  AND rfr.zone_node_type   = 0 " & _
				  "  AND rfr.clear_mkdn_index =1 " & _
				  "  AND rfr.clear_start_ind  =1 " & _
				  "  AND rfr.clearance_id     = rcl.clearance_id " & _
				  "  AND rcl.reset_date      IS NULL " & _
					whereBegDt &
				  "  AND im.dept              = cl.dept " & _
				  "  AND im.class             = cl.class " & _
				  "  AND im.item              = isp.item " & _
				  "  AND isp.primary_supp_ind = 'Y' " & _
				  "  AND isp.supplier         = sp.supplier " & _
				  "  ), " & _
				  "  max_date AS " & _
				  "  (SELECT im.item, " & _
				  "    rzl.zone_id, " & _
				  "    rzl.location, " & _
				  "    MAX(rfr.action_date) action_date " & _
				  "  FROM rpm_future_retail rfr, " & _
				  "    item_master im, " & _
				  "    rpm_zone_location rzl, " & _
				  "    mrb " & _
				  "  WHERE rfr.item        = im.item_parent " & _
				  "  AND im.item_level     = im.tran_level " & _
				  "  AND rzl.zone_id       = rfr.location " & _
				  "  AND mrb.item          = im.item " & _
				  " AND mrb.location      = rzl.location " & _
				  "  AND rfr.action_date   < mrb.action_date " & _
				  "  AND rfr.zone_node_type= 1 " & _
				  "  GROUP BY im.item, " & _
				  "    rzl.zone_id, " & _
				  "    rzl.location " & _
				  "  UNION " & _
				  "  SELECT im.item, " & _
				  "    mrb.zone_id, " & _
				  "    mrb.location, " & _
				  "    MAX(rfr.action_date) action_date " & _
				  "  FROM rpm_future_retail rfr, " & _
				  "    item_master im, " & _
				  "    mrb " & _
				  "  WHERE rfr.item        = im.item_parent " & _
				  " AND im.item_level     = im.tran_level " & _
				  "  AND mrb.item          = im.item " & _
				  "  AND mrb.location = rfr.location " & _
				  "  AND rfr.action_date   < mrb.action_date " & _
				  "  AND rfr.zone_node_type= 0 " & _
				  "  GROUP BY im.item, " & _
				  "    mrb.zone_id, " & _
				  "    mrb.location " & _
				  "  ), " & _
				  "  prev_retail AS " & _
				  "  (SELECT md.item, " & _
				  "    md.location, " & _
				  "    rfr.selling_retail " & _
				  "  FROM rpm_future_retail rfr , " & _
				  "    mrb, " & _
				  "    max_date md " & _
				  "  WHERE rfr.item        = mrb.item_cd " & _
				  "  AND rfr.location      = mrb.zone_id " & _
				  "  AND rfr.action_date   = md.action_date " & _
				  "  AND md.item           = mrb.item " & _
				  "  AND md.location       = mrb.location " & _
				  "  AND rfr.zone_node_type= 1 " & _
				  "  UNION " & _
				  "  SELECT md.item, " & _
				  "    md.location, " & _
				  "    rfr.selling_retail " & _
				  "  FROM rpm_future_retail rfr , " & _
				  "    mrb, " & _
				  "    max_date md " & _
				  "  WHERE rfr.item        = mrb.item_cd " & _
				  "  AND rfr.action_date   = md.action_date " & _
				  "  AND md.item           = mrb.item " & _
				  "  AND md.location       = mrb.location " & _
				  "  AND rfr.zone_node_type= 0 " & _
				  "  ) " & _
				  "SELECT UNIQUE mrb.item sku_num, " & _
				  "  mrb.item_cd itm_cd, " & _
				  "  mrb.clear_retail ret_prc, " & _
				  "  mrb.clearance_display_id pc_num, " & _
				  "  mrb.action_date beg_dt, " & _
				  "  'MD' pc_tp, " & _
				  "  pr.selling_retail prev_perm_ret, " & _
				  "  mrb.size_cd, " & _
				  "  lpad(mrb.supplier,7) ve_cd, " & _
				  "  mrb.item_desc, " & _
				  "  mrb.dept dept_cd, " & _
				  "  mrb.class class_cd, " & _
				  "  mrb.class_name class_desc " & _
				  "FROM mrb, " & _
				  "  prev_retail pr, " & _
				  "  item_loc_soh ils, " & _
				  "  v_deps vd " & _
				  "WHERE(pr.item   = mrb.item " & _
				  "AND pr.location = mrb.location " & _
				  "AND ils.item    = mrb.item " & _
				  "AND ils.loc     = mrb.location " & _
				  "AND mrb.dept    = vd.dept) " & _
				  "AND NOT EXISTS " & _
				  "  (SELECT 1 " & _
				  "  FROM uda_item_lov uil " & _
				  "  WHERE uil.item = mrb.item " & _
				  "  AND uil.uda_id = '9997' " & _
				  "  ) " & _
				   whereSql &
				   prcSql &
				  "ORDER BY mrb.dept, " & _
				  "  mrb.class, " & _
				  "  mrb.clear_retail, " & _
				  "  lpad(mrb.supplier,7), " & _
				  "  mrb.item_cd, " & _
				  "  mrb.item "

                        'strSql = "WITH mrb AS (SELECT /*+parallel*/ im.item ,rfr.item item_cd,NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc," & _
                        '"NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2)," & _
                        '"im.diff_2,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4" & _
                        '"),im.diff_4),'NONE') size_cd,im.dept,im.class,cl.class_name,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind,clear_mkdn_index,rfr.clearance_id," & _
                        '"rfr.clearance_display_id,isp.supplier,sp.sup_name FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp," & _
                        '" sups sp WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rzl.zone_id=rfr.location AND rfr.zone_node_type=1 AND rfr.clear_mkdn_index =1" & _
                        '" AND rfr.clear_start_ind=1 AND rfr.clearance_id=rcl.clearance_id AND rcl.reset_date IS NULL AND rfr.action_date=get_vdate AND im.dept=cl.dept AND im.class=cl.class" & _
                        '" AND im.item=isp.item AND isp.primary_supp_ind='Y' AND isp.supplier=sp.supplier),max_date AS (SELECT im.item,rzl.zone_id,rzl.location,MAX(rfr.action_date) action_date " & _
                        '" FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,mrb WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rzl.zone_id=rfr.location" & _
                        '" AND mrb.item=im.item AND mrb.location=rzl.location AND rfr.action_date<mrb.action_date AND rfr.zone_node_type=1 GROUP BY im.item,rzl.zone_id,rzl.location),prev_retail AS" & _
                        '" (SELECT md.item,md.location,rfr.selling_retail FROM rpm_future_retail rfr,mrb,max_date md WHERE rfr.item=mrb.item_cd AND rfr.location=mrb.zone_id AND rfr.action_date=md.action_date" & _
                        '" AND md.item=mrb.item AND md.location=mrb.location) " & _
                        '" SELECT /*+parallel(rfr)*/ mrb.item AS sku_num,mrb.item_cd AS itm_cd,mrb.clear_retail ret_prc, mrb.clearance_display_id pc_num,mrb.action_date AS beg_dt,'MD' AS pc_tp," & _
                        '" pr.selling_retail prev_perm_ret,mrb.size_cd,lpad(mrb.supplier,7) ve_cd,mrb.item_desc,mrb.dept AS dept_cd,mrb.class AS class_cd," & _
                        '" mrb.class_name AS class_desc FROM mrb,item_loc_soh ils,v_deps vd,prev_retail pr " & _
                        '" WHERE(pr.item = mrb.item And pr.location = mrb.location And ils.item = mrb.item And ils.loc = mrb.location And mrb.dept = vd.dept)" & _
                        '" AND NOT EXISTS (SELECT 1 FROM uda_item_lov uil  WHERE uil.item = mrb.item  AND uil.uda_id = '9997') " & _   " & whereBegDt & "
                        'whereSql & prcSql & _
                        '" order by mrb.dept,mrb.class, mrb.clear_retail,mrb.supplier,mrb.item_cd,mrb.item"

                    Case MDReportType.Further
						strSql = "WITH mrb AS ( " & _
				  "  (SELECT " & _
				  "    /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ " & _
				  "    im.item, " & _
				  "    rfr.item item_cd, " & _
				  "    NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
				  "    NVL(DECODE ('SIZE', " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1 " & _
				  "    ),im.diff_1, " & _
				  "   (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 " & _
				  "    ),im.diff_2, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3 " & _
				  "    ),im.diff_3, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4 " & _
				  "    ),im.diff_4),'NONE') size_cd, " & _
				  "    im.dept, " & _
				  "    im.class, " & _
				  "    cl.class_name, " & _
				  "    'MD' prc_tp, " & _
				  "    rzl.zone_id, " & _
				  "    rzl.location, " & _
				  "    rfr.action_date, " & _
				  "    clear_retail, " & _
				  "    clear_start_ind, " & _
				  "    clear_mkdn_index, " & _
				  "    rfr.clearance_id, " & _
				  "    rfr.clearance_display_id, " & _
				  "    isp.supplier, " & _
				  "    sp.sup_name " & _
				  "  FROM rpm_future_retail rfr, " & _
				  "    item_master im, " & _
				  "    rpm_zone_location rzl, " & _
				  "    rpm_clearance rcl, " & _
				  "    class cl, " & _
				  "    item_supplier isp, " & _
				  "    sups sp " & _
				  "  WHERE(rfr.item           = im.item_parent " & _
				  "  AND im.item_level        = im.tran_level " & _
				  "  AND rzl.zone_id          = rfr.location " & _
				  "  AND rfr.zone_node_type   = 1 " & _
				  "  AND rfr.clear_mkdn_index > 1 " & _
				  "  AND rfr.clear_start_ind >= 1) " & _
				  "  AND rfr.clearance_id     =rcl.clearance_id " & _
				  "  AND rcl.reset_date      IS NULL " & _
				   whereBegDt &
				  "  AND im.dept              =cl.dept " & _
				  "  AND im.class             =cl.class " & _
				  "  AND im.item              =isp.item " & _
				  "  AND isp.primary_supp_ind ='Y' " & _
				  "  AND isp.supplier         =sp.supplier " & _
				  "  UNION " & _
				  "  SELECT " & _
				  "    /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ " & _
				  "    im.item, " & _
				  "    rfr.item item_cd, " & _
				  "    NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
				  "    NVL(DECODE ('SIZE', " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1 " & _
				  "    ),im.diff_1, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 " & _
				  "    ),im.diff_2, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3 " & _
				  "    ),im.diff_3, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4 " & _
				  "    ),im.diff_4),'NONE') size_cd, " & _
				  "    im.dept, " & _
				  "    im.class, " & _
				  "    cl.class_name, " & _
				  "    'MD' prc_tp, " & _
				  "    rcl.zone_id, " & _
				  "    rcl.location, " & _
				  "    rfr.action_date, " & _
				  "    clear_retail, " & _
				  "    clear_start_ind, " & _
				  "    clear_mkdn_index, " & _
				  "    rfr.clearance_id, " & _
				  "    rfr.clearance_display_id, " & _
				  "    isp.supplier, " & _
				  "    sp.sup_name " & _
				  "  FROM rpm_future_retail rfr, " & _
				  "    item_master im, " & _
				  "    rpm_clearance rcl, " & _
				  "    class cl, " & _
				  "    item_supplier isp, " & _
				  "    sups sp " & _
				  "  WHERE(rfr.item           = im.item_parent " & _
				  "  AND im.item_level        = im.tran_level " & _
				  "  AND rfr.location  = rcl.location " & _
				  "  AND rfr.zone_node_type   = 0 " & _
				  "  AND rfr.clear_mkdn_index > 1 " & _
				  "  AND rfr.clear_start_ind >= 1) " & _
				  "  AND rfr.clearance_id     =rcl.clearance_id " & _
				  "  AND rcl.reset_date      IS NULL " & _
				   whereBegDt &
				  "  AND im.dept              =cl.dept " & _
				  "  AND im.class             =cl.class " & _
				  "  AND im.item              =isp.item " & _
				  "  AND isp.primary_supp_ind ='Y' " & _
				  "  AND isp.supplier         =sp.supplier " & _
				  "  ) " & _
				  "UNION " & _
				  "  (SELECT " & _
				  "    /*+parallel (rfr,4)*/ " & _
				  "    im.item, " & _
				  "    rfr.item item_cd, " & _
				  "    NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
				  "    NVL(DECODE ('SIZE', " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1 " & _
				  "    ),im.diff_1, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 " & _
				  "    ),im.diff_2, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3 " & _
				  "    ),im.diff_3, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4 " & _
				  "    ), im.diff_4),'NONE') size_cd, " & _
				  "    im.dept, " & _
				  "    im.class, " & _
				  "    cl.class_name, " & _
				  "    'MD' prc_tp, " & _
				  "    rzl.zone_id, " & _
				  "    rzl.location, " & _
				  "    rfr.action_date, " & _
				  "    clear_retail, " & _
				  "    clear_start_ind, " & _
				  "    clear_mkdn_index, " & _
				  "    rfr.clearance_id, " & _
				  "    rfr.clearance_display_id, " & _
				  "    isp.supplier, " & _
				  "    sp.sup_name " & _
				  "  FROM rpm_future_retail rfr, " & _
				  "    item_master im, " & _
				  "    rpm_zone_location rzl, " & _
				  "    rpm_clearance rcl, " & _
				  "    class cl, " & _
				  "    item_supplier isp, " & _
				  "    sups sp, " & _
				  "    uda_item_lov uil " & _
				  "  WHERE(rfr.item           = im.item_parent " & _
				  "  AND im.item_level        = im.tran_level " & _
				  "  AND rzl.zone_id          = rfr.location " & _
				  "  AND rfr.zone_node_type   = 1 " & _
				  "  AND rfr.clear_mkdn_index = 1 " & _
				  "  AND rfr.clear_start_ind  = 1) " & _
				  "  AND rfr.clearance_id     = rcl.clearance_id " & _
				  "  AND rcl.reset_date      IS NULL " & _
				   whereBegDt &
				  "  AND uil.item             =im.item " & _
				  "  AND uil.uda_id           = '9997' " & _
				  "  AND im.dept              = cl.dept " & _
				  "  AND im.class             =cl.class " & _
				  "  AND im.item              =isp.item " & _
				  "  AND isp.primary_supp_ind = 'Y' " & _
				  "  AND isp.supplier         = sp.supplier " & _
				  "  UNION " & _
				  "  SELECT " & _
				  "    /*+parallel (rfr,4)*/ " & _
				  "    im.item, " & _
				  "    rfr.item item_cd, " & _
				  "    NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
				  "    NVL(DECODE ('SIZE', " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1 " & _
				  "    ),im.diff_1, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 " & _
				  "    ),im.diff_2, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3 " & _
				  "    ),im.diff_3, " & _
				  "    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4 " & _
				  "    ), im.diff_4),'NONE') size_cd, " & _
				  "    im.dept, " & _
				  "    im.class, " & _
				  "    cl.class_name, " & _
				  "    'MD' prc_tp, " & _
				  "    rcl.zone_id, " & _
				  "    rcl.location, " & _
				  "    rfr.action_date, " & _
				  "    clear_retail, " & _
				  "    clear_start_ind, " & _
				  "    clear_mkdn_index, " & _
				  "    rfr.clearance_id, " & _
				  "    rfr.clearance_display_id, " & _
				  "    isp.supplier, " & _
				  "    sp.sup_name " & _
				  "  FROM rpm_future_retail rfr, " & _
				  "    item_master im, " & _
				  "    rpm_clearance rcl, " & _
				  "    class cl, " & _
				  "    item_supplier isp, " & _
				  "    sups sp, " & _
				  "    uda_item_lov uil " & _
				  "  WHERE(rfr.item           = im.item_parent " & _
				  "  AND im.item_level        = im.tran_level " & _
				  "  AND rfr.location =  rcl.location " & _
				  "  AND rfr.zone_node_type   = 0 " & _
				  "  AND rfr.clear_mkdn_index = 1 " & _
				  "  AND rfr.clear_start_ind  = 1) " & _
				  "  AND rfr.clearance_id     = rcl.clearance_id " & _
				  "  AND rcl.reset_date      IS NULL " & _
				   whereBegDt &
				  "  AND uil.item             =im.item " & _
				  "  AND uil.uda_id           = '9997' " & _
				  "  AND im.dept              = cl.dept " & _
				  "  AND im.class             =cl.class " & _
				  "  AND im.item              =isp.item " & _
				  "  AND isp.primary_supp_ind = 'Y' " & _
				  "  AND isp.supplier         = sp.supplier " & _
				  "  )), " & _
				  "  max_date AS " & _
				  "  (SELECT im.item, " & _
				  "    rzl.zone_id, " & _
				  "    rzl.location, " & _
				  "    MAX(rfr.action_date) action_date " & _
				  "  FROM rpm_future_retail rfr, " & _
				  "    item_master im, " & _
				  "    rpm_zone_location rzl, " & _
				  "    mrb " & _
				  "  WHERE rfr.item        =im.item_parent " & _
				  "  AND im.item_level     =im.tran_level " & _
				  "  AND rzl.zone_id       =rfr.location " & _
				  "  AND mrb.item          =im.item " & _
				  "  AND mrb.location      =rzl.location " & _
				  "  AND rfr.action_date   < mrb.action_date " & _
				  "  AND rfr.zone_node_type=1 " & _
				  "  GROUP BY im.item, " & _
				  "    rzl.zone_id, " & _
				  "    rzl.location " & _
				  "  UNION " & _
				  "  SELECT im.item, " & _
				  "    mrb.zone_id, " & _
				  "    mrb.location, " & _
				  "    MAX(rfr.action_date) action_date " & _
				  "  FROM rpm_future_retail rfr, " & _
				  "    item_master im, " & _
				  "    mrb " & _
				  "  WHERE rfr.item        =im.item_parent " & _
				  "  AND im.item_level     =im.tran_level " & _
				  "  AND mrb.item          =im.item " & _
				  "  AND mrb.location = rfr.location " & _
				  "  AND rfr.action_date   < mrb.action_date " & _
				  "  AND rfr.zone_node_type=0 " & _
				  "  GROUP BY im.item, " & _
				  "    mrb.zone_id, " & _
				  "    mrb.location " & _
				  "  ), " & _
				  "  prev_retail AS " & _
				  "  (SELECT md.item, " & _
				  "    md.location, " & _
				  "    rfr.selling_retail " & _
				  "  FROM rpm_future_retail rfr, " & _
				  "    mrb, " & _
				  "    max_date md " & _
				  "  WHERE rfr.item         =mrb.item_cd " & _
				  "  AND rfr.location       =mrb.zone_id " & _
				  "  AND rfr.action_date    =md.action_date " & _
				  "  AND md.item            =mrb.item " & _
				  "  AND md.location        =mrb.location " & _
				  "  AND rfr.zone_node_type = 1 " & _
				  "  UNION " & _
				  "  SELECT md.item, " & _
				  "    md.location, " & _
				  "    rfr.selling_retail " & _
				  "  FROM rpm_future_retail rfr, " & _
				  "    mrb, " & _
				  "    max_date md " & _
				  "  WHERE rfr.item         =mrb.item_cd " & _
				  "  AND rfr.action_date    =md.action_date " & _
				  "  AND md.item            =mrb.item " & _
				  "  AND md.location        =mrb.location " & _
				  "  AND rfr.zone_node_type = 0 " & _
				  "  ) " & _
				  "SELECT " & _
				  "  /*+parallel(rfr,4)*/ " & _
				  "  UNIQUE mrb.item sku_num, " & _
				  "  mrb.item_cd itm_cd, " & _
				  "  mrb.clear_retail ret_prc, " & _
				  "  mrb.clearance_display_id pc_num, " & _
				  "  mrb.action_date beg_dt, " & _
				  "  mrb.prc_tp pc_tp, " & _
				  "  pr.selling_retail prev_perm_ret, " & _
				  "  mrb.size_cd, " & _
				  "  lpad(mrb.supplier,7) ve_cd, " & _
				  "  mrb.item_desc, " & _
				  "  mrb.dept dept_cd, " & _
				  "  mrb.class class_cd, " & _
				  "  mrb.class_name class_desc " & _
				  "FROM mrb, " & _
				  "  prev_retail pr, " & _
				  "  item_loc_soh ils, " & _
				  "  v_deps vd " & _
				  "WHERE(pr.item       = mrb.item " & _
				  "AND pr.location     = mrb.location " & _
				  "AND ils.item        = mrb.item " & _
				  "AND ils.loc         = mrb.location " & _
				  "AND mrb.dept        = vd.dept) " & _
				   whereSql & _
				   prcSql & _
				  "ORDER BY mrb.dept, " & _
				  "  mrb.class, " & _
				  "  mrb.clear_retail, " & _
				  "  lpad(mrb.supplier,7), " & _
				  "  mrb.item_cd, " & _
				  "  mrb.item"

                        ''strSql = "with mrb as ((select /*+parallel*/  im.item,rfr.item item_cd,nvl(substr(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc," & _
                        '     "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2" & _
                        '     "),im.diff_2,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3,(select diff_type from v_diff_id_group_type where id_group = im.diff_4" & _
                        '     "),im.diff_4),'NONE') size_cd,im.dept,im.class,cl.class_name,'MD' prc_tp,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind,clear_mkdn_index,rfr.clearance_id," & _
                        '     "rfr.clearance_display_id,isp.supplier,sp.sup_name FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp,sups sp" & _
                        '     " WHERE(rfr.item = im.item_parent And im.item_level = im.tran_level And rzl.zone_id = rfr.location And rfr.zone_node_type = 1 And rfr.clear_mkdn_index > 1 And rfr.clear_start_ind >= 1)" & _
                        '     " and rfr.clearance_id=rcl.clearance_id and rcl.reset_date is null " & whereBegDt & " and im.dept=cl.dept and im.class=cl.class and im.item=isp.item" & _
                        '     " and isp.primary_supp_ind='Y' and isp.supplier=sp.supplier) union (select /*+parallel*/  im.item,rfr.item item_cd,nvl(substr(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc," & _
                        '     " NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2" & _
                        '     "),im.diff_2,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3),im.diff_3,(select diff_type from v_diff_id_group_type where id_group = im.diff_4" & _
                        '     "), im.diff_4),'NONE') size_cd,im.dept,im.class,cl.class_name,'MD' prc_tp,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind,clear_mkdn_index,rfr.clearance_id," & _
                        '     " rfr.clearance_display_id,isp.supplier,sp.sup_name FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp,sups sp,uda_item_lov uil" & _
                        '     " WHERE(rfr.item = im.item_parent And im.item_level = im.tran_level And rzl.zone_id = rfr.location And rfr.zone_node_type = 1 And rfr.clear_mkdn_index = 1 And rfr.clear_start_ind = 1)" & _
                        '     " and rfr.clearance_id = rcl.clearance_id and rcl.reset_date is null " & whereBegDt & " and uil.item=im.item and uil.uda_id = '9997' and im.dept = cl.dept" & _
                        '     " and im.class=cl.class and im.item=isp.item and isp.primary_supp_ind = 'Y' and isp.supplier = sp.supplier)),max_date as (SELECT im.item,rzl.zone_id,rzl.location,max(rfr.action_date) action_date" & _
                        '     " FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,mrb WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level and rzl.zone_id=rfr.location and mrb.item=im.item" & _
                        '     " and mrb.location=rzl.location and rfr.action_date < mrb.action_date AND rfr.zone_node_type=1 group by im.item,rzl.zone_id,rzl.location),prev_retail as " & _
                        '     " (select md.item, md.location, rfr.selling_retail from rpm_future_retail rfr,mrb, max_date md where rfr.item=mrb.item_cd AND rfr.location=mrb.zone_id AND rfr.action_date=md.action_date" & _
                        '     " and md.item=mrb.item AND md.location=mrb.location) select /*+parallel(rfr)*/ unique mrb.item sku_num,mrb.item_cd itm_cd,mrb.clear_retail ret_prc,mrb.clearance_display_id pc_num,mrb.action_date beg_dt," & _
                        '     " mrb.prc_tp pc_tp,pr.selling_retail prev_perm_ret,mrb.size_cd,lpad(mrb.supplier,7) ve_cd,mrb.item_desc,mrb.dept dept_cd,mrb.class class_cd," & _
                        '     " mrb.class_name class_desc from  mrb,prev_retail pr,item_loc_soh ils,v_deps vd" & _
                        '     " WHERE(pr.item = mrb.item And pr.location = mrb.location And ils.item = mrb.item And ils.loc = mrb.location And mrb.dept = vd.dept)" & _
                        '     whereSql & prcSql & _
                        '     " order by mrb.dept,mrb.class, mrb.clear_retail,lpad(mrb.supplier,7),mrb.item_cd,mrb.item "


                    Case MDReportType.MOS
                        strSql = "SELECT im.item   AS sku_num, " & _
                        "  im.item_parent AS itm_cd, " & _
                        "  0              AS ret_prc, " & _
                        "  ''             AS pc_num, " & _
                        "  ud.uda_date beg_dt, " & _
                        "  'MOS' pc_tp, " & _
                        "  il.selling_unit_retail AS prev_perm_ret, " & _
                        "  NVL(DECODE ('SIZE', " & _
                        "  (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1 " & _
                        "  ), im.diff_1, " & _
                        "  (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 " & _
                        "  ), im.diff_2, " & _
                        "  (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3 " & _
                        "  ), im.diff_3, " & _
                        "  (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4 " & _
                        "  ), im.diff_4),'NONE') size_cd, " & _
                        "  lpad(isp.supplier,7) AS ve_cd, " & _
                        "  NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
                        "  im.dept       AS dept_cd, " & _
                        "  im.class      AS class_cd, " & _
                        "  CL.CLASS_NAME AS class_desc " & _
                        "FROM item_master im, " & _
                        "  item_loc il, " & _
                        "  uda_item_date ud, " & _
                        "  item_supplier isp, " & _
                        "  class cl, " & _
                        "  sups sp, " & _
                        "  v_deps vd " & _
                        "WHERE im.item            = il.item " & _
                        "AND IM.ITEM              = UD.ITEM " & _
                        "AND ud.uda_id            = '56' " & _
                        "AND im.item_level        = im.tran_level " & _
                        "AND IM.DEPT              = vd.dept " & _
                        "AND im.dept              = cl.dept " & _
                        "AND im.class             = cl.class " & _
                        "AND im.item              = isp.item " & _
                         whereBegDt & _
                        "AND sp.supplier          = isp.supplier " & _
                         whereSql & _
                        "AND isp.primary_supp_ind = 'Y' " & _
                        "ORDER BY im.dept, " & _
                        "  im.class, " & _
                        "  il.selling_unit_retail, " & _
                        "  lpad(isp.supplier,7), " & _
                        "  im.item, " & _
                        "  im.item_parent"

                    Case MDReportType.MDAll
						strSql = "WITH mrb AS " & _
"  (SELECT " & _
"    /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ " & _
"    im.item, " & _
"    rfr.item item_cd, " & _
"    NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
"    NVL(DECODE ('SIZE', " & _
"    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1 " & _
"    ), im.diff_1, " & _
"    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 " & _
"    ), im.diff_2, " & _
"    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3 " & _
"    ), im.diff_3, " & _
"    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4 " & _
"    ), im.diff_4),'NONE') size_cd, " & _
"    im.dept, " & _
"    im.class, " & _
"    cl.class_name, " & _
"    rzl.zone_id , " & _
"    rzl.location, " & _
"    rfr.action_date, " & _
"    clear_retail, " & _
"    clear_start_ind, " & _
"    clear_mkdn_index, " & _
"    rfr.clearance_id, " & _
"    rfr.clearance_display_id, " & _
"    isp.supplier, " & _
"    sp.sup_name " & _
"  FROM rpm_future_retail rfr, " & _
"    item_master im, " & _
"    rpm_zone_location rzl, " & _
"    rpm_clearance rcl, " & _
"    class cl, " & _
"    item_supplier isp, " & _
"    sups sp " & _
"  WHERE rfr.item           = im.item_parent " & _
"  AND im.item_level        = im.tran_level " & _
"  AND rzl.zone_id          = rfr.location " & _
"  AND rfr.zone_node_type   = 1 " & _
"  AND rfr.clear_mkdn_index >0 " & _
"  AND rfr.clearance_id     = rcl.clearance_id " & _
"  AND rcl.reset_date      IS NULL " & _
 whereBegDt & _
"  AND im.dept              = cl.dept " & _
"  AND im.class             = cl.class " & _
"  AND im.item              = isp.item " & _
"  AND isp.primary_supp_ind = 'Y' " & _
"  AND isp.supplier         = sp.supplier " & _
"  UNION " & _
"  SELECT " & _
"    /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ " & _
"    im.item, " & _
"    rfr.item item_cd, " & _
"    NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
"    NVL(DECODE ('SIZE', " & _
"    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1 " & _
"    ), im.diff_1, " & _
"    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 " & _
"    ), im.diff_2, " & _
"    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3 " & _
"    ), im.diff_3, " & _
"    (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4 " & _
"    ), im.diff_4),'NONE') size_cd, " & _
"    im.dept, " & _
"    im.class, " & _
"    cl.class_name, " & _
"    rcl.zone_id, " & _
"    rcl.location, " & _
"    rfr.action_date, " & _
"    clear_retail, " & _
"    clear_start_ind, " & _
"    clear_mkdn_index, " & _
"    rfr.clearance_id, " & _
"    rfr.clearance_display_id, " & _
"    isp.supplier, " & _
"    sp.sup_name " & _
"  FROM rpm_future_retail rfr, " & _
"    item_master im, " & _
"    rpm_clearance rcl, " & _
"    class cl, " & _
"    item_supplier isp, " & _
"    sups sp " & _
"  WHERE rfr.item           = im.item_parent " & _
"  AND im.item_level        = im.tran_level " & _
"  AND rfr.location = rcl.location " & _
"  AND rfr.zone_node_type   = 0 " & _
"  AND rfr.clear_mkdn_index >0 " & _
"  AND rfr.clearance_id     = rcl.clearance_id " & _
"  AND rcl.reset_date      IS NULL " & _
 whereBegDt & _
"  AND im.dept              = cl.dept " & _
"  AND im.class             = cl.class " & _
"  AND im.item              = isp.item " & _
"  AND isp.primary_supp_ind = 'Y' " & _
"  AND isp.supplier         = sp.supplier " & _
"  ), " & _
"  max_date AS " & _
"  (SELECT im.item, " & _
"    rzl.zone_id, " & _
"    rzl.location, " & _
"    MAX(rfr.action_date) action_date " & _
"  FROM rpm_future_retail rfr, " & _
"    item_master im, " & _
"    rpm_zone_location rzl, " & _
"    mrb " & _
"  WHERE rfr.item        = im.item_parent " & _
"  AND im.item_level     = im.tran_level " & _
"  AND rzl.zone_id       = rfr.location " & _
"  AND mrb.item          = im.item " & _
"  AND mrb.location      = rzl.location " & _
"  AND rfr.action_date   < mrb.action_date " & _
"  AND rfr.zone_node_type= 1 " & _
"  GROUP BY im.item, " & _
"    rzl.zone_id, " & _
"    rzl.location " & _
"  UNION " & _
"  SELECT im.item, " & _
"    mrb.zone_id, " & _
"    mrb.location, " & _
"    MAX(rfr.action_date) action_date " & _
"  FROM rpm_future_retail rfr, " & _
"    item_master im, " & _
"    mrb " & _
"  WHERE rfr.item        = im.item_parent " & _
"  AND im.item_level     = im.tran_level " & _
"  AND mrb.item          = im.item " & _
"  AND mrb.location = rfr.location " & _
"  AND rfr.action_date   < mrb.action_date " & _
"  AND rfr.zone_node_type= 0 " & _
"  GROUP BY im.item, " & _
"    mrb.zone_id, " & _
"    mrb.location " & _
"  ), " & _
"  prev_retail AS " & _
"  (SELECT md.item, " & _
"    md.location, " & _
"    rfr.selling_retail " & _
"  FROM rpm_future_retail rfr , " & _
"    mrb, " & _
"    max_date md " & _
"  WHERE rfr.item        = mrb.item_cd " & _
"  AND rfr.location      = mrb.zone_id " & _
"  AND rfr.action_date   = md.action_date " & _
"  AND md.item           = mrb.item " & _
"  AND md.location       = mrb.location " & _
"  AND rfr.zone_node_type= 1 " & _
"  UNION " & _
"  SELECT md.item, " & _
"    md.location, " & _
"    rfr.selling_retail " & _
"  FROM rpm_future_retail rfr , " & _
"    mrb, " & _
"    max_date md " & _
"  WHERE rfr.item        = mrb.item_cd " & _
"  AND rfr.action_date   = md.action_date " & _
"  AND md.item           = mrb.item " & _
"  AND md.location       = mrb.location " & _
"  AND rfr.zone_node_type= 0 " & _
"  ) " & _
"SELECT " & _
"  /*+parallel(rfr,4)*/ " & _
"  UNIQUE mrb.item sku_num, " & _
"  mrb.item_cd itm_cd, " & _
"  mrb.clear_retail ret_prc, " & _
"  mrb.clearance_display_id pc_num, " & _
"  mrb.action_date beg_dt, " & _
"  'MD' pc_tp, " & _
"  pr.selling_retail prev_perm_ret, " & _
"  mrb.size_cd, " & _
"  lpad(mrb.supplier,7) ve_cd, " & _
"  mrb.item_desc, " & _
"  mrb.dept dept_cd, " & _
"  mrb.class class_cd, " & _
"  mrb.class_name class_desc " & _
"FROM mrb, " & _
"  v_deps vd, " & _
"  prev_retail pr " & _
"WHERE pr.item       = mrb.item " & _
"AND pr.location     = mrb.location " & _
"AND mrb.dept        = vd.dept " & _
 whereSql & _
 prcSql & _
"ORDER BY mrb.dept, " & _
"  mrb.class, " & _
"  mrb.clear_retail, " & _
"  lpad(mrb.supplier,7), " & _
"  mrb.item_cd, " & _
"  mrb.item"



                End Select

                'strSql = "select /*+ ALL_ROWS*/ /*Sri Bajjuri - Report distro MD reports*/ gm_prc.sku_num, gm_prc.itm_cd, gm_prc.ret_prc, gm_prc.pc_num, gm_prc.beg_dt, gm_prc.pc_tp, gm_prc.prev_perm_ret, " & _
                '        "gm_sku.size_cd, lpad(gm_itm.ve_cd,7) as ve_cd, gm_itm.item_desc as item_desc, gm_itm.dept_cd, gm_itm.class_cd, class.des as class_desc " & _
                '        "from gm_prc " & _
                '        "inner join gm_sku on gm_sku.sku_num = gm_prc.sku_num " & _
                '        " inner join (select i.itm_cd, i.ve_cd,i.dept_cd,i.class_cd, NVL(wd.web_des, i.des1) as item_desc FROM GM_ITM i " & _
                '        "left join RPT_SERVICES_GERS_INQ.bm_itm_wdes wd on i.itm_cd = wd.itm_cd) gm_itm on gm_itm.itm_cd = gm_prc.itm_cd " & _
                '        "inner join class on gm_itm.class_cd = class.class_cd " & _
                '        "inner join dept on gm_itm.dept_cd = dept.dept_cd " & _
                '        whereSql & "and pc_tp ='" & pc_tp & "' " & prcSql & _
                '        "and gm_prc.prc_zone_cd = (select prc_zone_cd from store where store_cd = 9999) " & _
                '        "and gm_prc.prc_grp_cd = (select prc_grp_cd from store where store_cd = 9999) " & _
                '        "order by gm_itm.dept_cd, gm_itm.class_cd, gm_prc.ret_prc desc, ve_cd, gm_prc.itm_cd, gm_prc.sku_num"

                'Throw New Exception("MDReport:" & strSql)

                mdDs = Cn.ExecuteQuery(EnumRSTypes.DataSet, strSql)

                Cn.CloseConnection()
                If mdDs.Tables(0).Rows.Count = 0 Then
                    Throw New Exception("MDReport:No records found")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub GetMDWHDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByVal companyId As Int16, ByVal warehouse As String)
            'Markdown ware house report
            Try
                'Create Store Dataset
                Dim Cn As New OracleDataManager()
                Dim strSql As String

                Dim effectiveDate, pc_tp, prcSql, whereSql, whereBegDt As String
                strSql = ""
                prcSql = ""

                Select Case rptSubmission.MDReportType
                    Case MDReportType.Regular
                        'pc_tp = "MD"
                        '# RMS change 02/08/2013 by LJ Regular/further/all markdown ret_prc>.01
                        prcSql = "and mrb.clear_retail>.01 "
                    Case MDReportType.Further
                        'pc_tp = "MD"
                        prcSql = "and mrb.clear_retail>.01 "
                    Case MDReportType.MOS
                        'pc_tp = "MD"
                        prcSql = " "
                    Case MDReportType.MDAll
                        'pc_tp = "MD"
                        prcSql = "and mrb.clear_retail>.01 "
                        '# diabled for RMS 01/31/2013 by LJ
                        'Case MDReportType.MarkUp
                        '    pc_tp = "MU"
                        '    prcSql = ""
                        'Case MDReportType.MDCancel
                        '    pc_tp = "MDC"
                        '    prcSql = ""
                        'Case MDReportType.MUCancel
                        '    pc_tp = "MUC"
                        '    prcSql = ""
                End Select

                '# RMS change for PC_NUM 02/07/13 by LJ
                'If rptSubmission.MDPcNum <> "" AndAlso IsNumeric(rptSubmission.MDPcNum) Then


                '    'whereSql = "where pc_num = '" & rptSubmission.MDPcNum & "' " 
                '    whereSql = " and mrb.clearance_display_id = '" & rptSubmission.MDPcNum & "' "
                'Else 
         

                If IsDate(rptSubmission.MDEffectiveDate) Then
                    effectiveDate = rptSubmission.MDEffectiveDate.Day & "-" & MonthName(rptSubmission.MDEffectiveDate.Month, True).ToLower & "-" & rptSubmission.MDEffectiveDate.Year

                    '# RMS change for effect date 02/07/13 by LJ
                    'whereSql = "where beg_dt = '" & effectiveDate & "' "
                    If (rptSubmission.MDReportType <> MDReportType.MOS) Then
                        whereBegDt = " and rfr.action_date = '" & effectiveDate & "' "
                    Else
                        whereBegDt = " and ud.uda_date = '" & effectiveDate & "' "
                    End If
                Else
                    Throw New Exception("MDReport:Invalid MD report request")
                End If
                Select Case companyId
                    Case 1 ' Hot Topic
                        whereSql &= "and vd.division = 1 "
                    Case 2 ' Torrid
                        whereSql &= "and vd.division = 5 "
                    Case 3 ' BlackHeart
                        whereSql &= "and vd.division = 8 "
                End Select
                'Get MD Dataset
                '# RMS change 2/7/2013 - by LJ 
                Select Case rptSubmission.MDReportType
                    Case MDReportType.Regular
						strSql = "WITH mrb AS (SELECT /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ im.item, rfr.item item_cd, " & _
						   "NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
						   "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
						   "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2), im.diff_2, " & _
						   "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
						   "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4),im.diff_4),'NONE') size_cd, " & _
						   "im.dept,im.class,cl.class_name,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind, " & _
						   "clear_mkdn_index,rfr.clearance_id,rfr.clearance_display_id,isp.supplier,sp.sup_name " & _
						   "FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp,sups sp " & _
						   "WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rzl.zone_id=rfr.location AND rfr.zone_node_type=1 " & _
						   "AND rfr.clear_mkdn_index=1 AND rfr.clear_start_ind=1 AND rfr.clearance_id=rcl.clearance_id AND rcl.reset_date IS NULL " & _
						   whereBegDt & " AND im.dept=cl.dept AND im.class=cl.class AND im.item=isp.item AND isp.primary_supp_ind='Y' AND isp.supplier=sp.supplier " & _
						   "UNION " & _
						   "SELECT /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ im.item,rfr.item item_cd, " & _
						   "NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
						   "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
						   "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2), im.diff_2, " & _
						   "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
						   "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4),im.diff_4),'NONE') size_cd, " & _
						   "im.dept,im.class,cl.class_name,rcl.zone_id,rcl.location,rfr.action_date,clear_retail,clear_start_ind, " & _
						   "clear_mkdn_index,rfr.clearance_id,rfr.clearance_display_id,isp.supplier,sp.sup_nameFROM rpm_future_retail rfr, " & _
						   "item_master im,rpm_clearance rcl,class cl,item_supplier isp,sups sp " & _
						   "WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rfr.location=rcl.location AND rfr.zone_node_type=2 " & _
						   "AND rfr.clear_mkdn_index=1 AND rfr.clear_start_ind=1 AND rfr.clearance_id=rcl.clearance_id AND rcl.reset_date IS NULL " & _
						   prcSql & " AND im.dept=cl.dept AND im.class=cl.class AND im.item=isp.item AND isp.primary_supp_ind='Y' AND isp.supplier=sp.supplier), " & _
						   "max_date AS (SELECT im.item,rzl.zone_id,rzl.location,MAX(rfr.action_date) action_date " & _
						   "FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,mrb " & _
						   "WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rzl.zone_id=rfr.location AND mrb.item=im.item " & _
						   "AND mrb.location=rzl.location AND rfr.action_date<mrb.action_date AND rfr.zone_node_type=1 " & _
						   "GROUP BY im.item,rzl.zone_id,rzl.location " & _
						   "UNION " & _
						   "SELECT im.item,mrb.zone_id,mrb.location,MAX(rfr.action_date) action_date " & _
						   "FROM rpm_future_retail rfr,item_master im,mrb " & _
						   "WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND mrb.item=im.item " & _
						   "AND mrb.location=rfr.location AND rfr.action_date<mrb.action_date AND rfr.zone_node_type=2 " & _
						   "GROUP BY im.item,mrb.zone_id,mrb.location), " & _
						   "prev_retail AS (SELECT md.item,md.location,rfr.selling_retail " & _
						   "FROM rpm_future_retail rfr,mrb,max_date md " & _
						   "WHERE rfr.item=mrb.item_cd AND rfr.location=mrb.zone_id AND rfr.action_date=md.action_date " & _
						   "AND md.item=mrb.item AND md.location=mrb.location AND rfr.zone_node_type=1 " & _
						   "UNION " & _
						   "SELECT md.item,md.location,rfr.selling_retail " & _
						   "FROM rpm_future_retail rfr,mrb,max_date md " & _
						   "WHERE rfr.item=mrb.item_cd AND rfr.location=mrb.location AND rfr.action_date=md.action_date " & _
						   "AND md.item=mrb.item AND md.location=mrb.location AND rfr.zone_node_type=2) " & _
						   "SELECT /*+parallel(ils,4)*/ mrb.item AS sku_num,mrb.item_cd AS itm_cd,mrb.clear_retail ret_prc,mrb.clearance_display_id pc_num, " & _
						   "mrb.action_date AS beg_dt,'MD' AS pc_tp,pr.selling_retail prev_perm_ret,to_number(mrb.location) store_cd,'RECV' AS loc_cd, " & _
						   "'RCV'  AS loc_tp,ils.stock_on_hand oh_qty,mrb.size_cd,lpad(mrb.supplier,7) ve_cd,mrb.item_desc,mrb.dept AS dept_cd, " & _
						   "mrb.class AS class_cd,mrb.class_name AS class_desc,mrb.sup_name AS VE_name " & _
						   "FROM mrb,item_loc_soh ils,v_deps vd,prev_retail pr " & _
						   "WHERE(pr.item = mrb.item AND pr.location = mrb.location AND ils.item = mrb.item AND ils.loc = mrb.location " & _
						   "AND to_number(mrb.location) =  " & warehouse & " AND ils.stock_on_hand>0 AND mrb.dept = vd.dept) " & _
						   "AND NOT EXISTS(SELECT 1 FROM uda_item_lov uil WHERE uil.item = mrb.item AND uil.uda_id = '9997') " & _
						   whereSql & prcSql &
						   " ORDER BY loc_tp,loc_cd,mrb.dept,mrb.class,mrb.clear_retail,lpad(mrb.supplier,7),mrb.item_cd,mrb.item"


                        'strSql = "WITH mrb AS (SELECT /*+parallel*/ im.item ,rfr.item item_cd,NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc," & _
                        '       "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2)," & _
                        '       "im.diff_2,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4" & _
                        '       "),im.diff_4),'NONE') size_cd,im.dept,im.class,cl.class_name,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind,clear_mkdn_index,rfr.clearance_id," & _
                        '       " rfr.clearance_display_id,isp.supplier,sp.sup_name FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp," & _
                        '       " sups sp  WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rzl.zone_id=rfr.location AND rfr.zone_node_type=1 AND rfr.clear_mkdn_index =1" & _
                        '       " AND rfr.clear_start_ind =1 AND rfr.clearance_id=rcl.clearance_id AND rcl.reset_date IS NULL " & whereBegDt & " AND im.dept=cl.dept AND im.class=cl.class" & _
                        '       " AND im.item=isp.item AND isp.primary_supp_ind='Y' AND isp.supplier=sp.supplier),max_date AS (SELECT im.item,rzl.zone_id,rzl.location,MAX(rfr.action_date) action_date" & _
                        '       " FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,mrb WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rzl.zone_id=rfr.location" & _
                        '       " AND mrb.item=im.item AND mrb.location=rzl.location AND rfr.action_date<mrb.action_date AND rfr.zone_node_type=1 GROUP BY im.item,rzl.zone_id,rzl.location),prev_retail AS" & _
                        '       " (SELECT md.item,md.location,rfr.selling_retail FROM rpm_future_retail rfr,mrb,max_date md WHERE rfr.item=mrb.item_cd AND rfr.location=mrb.zone_id AND rfr.action_date=md.action_date" & _
                        '       " AND md.item=mrb.item AND md.location=mrb.location) " & _
                        '       " SELECT /*+parallel(rfr)*/ mrb.item AS sku_num,mrb.item_cd AS itm_cd,mrb.clear_retail ret_prc, mrb.clearance_display_id pc_num,mrb.action_date AS beg_dt,'MD' AS pc_tp," & _
                        '       " pr.selling_retail prev_perm_ret,to_number(mrb.location) store_cd, 'RECV' AS loc_cd,'RCV' AS loc_tp, ils.stock_on_hand oh_qty,mrb.size_cd,lpad(mrb.supplier,7) ve_cd,mrb.item_desc,mrb.dept AS dept_cd,mrb.class AS class_cd," & _
                        '       " mrb.class_name AS class_desc,mrb.sup_name as VE_name FROM mrb,item_loc_soh ils,v_deps vd,prev_retail pr " & _
                        '       " WHERE(pr.item = mrb.item And pr.location = mrb.location And ils.item = mrb.item And ils.loc = mrb.location and to_number(mrb.location) = " & warehouse & " and ils.stock_on_hand>0 And mrb.dept = vd.dept)" & _
                        '       " AND NOT EXISTS (SELECT 1  FROM uda_item_lov uil  WHERE uil.item = mrb.item  AND uil.uda_id = '9997') " & _
                        '       whereSql & prcSql & _
                        '       " order by loc_tp,loc_cd,mrb.dept,mrb.class, mrb.clear_retail,mrb.supplier,mrb.item_cd,mrb.item"

                    Case MDReportType.Further
						strSql = "WITH mrb AS ( (SELECT /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ im.item,rfr.item item_cd,NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
						 "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1), im.diff_1, " & _
						 "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2), im.diff_2, " & _
						 "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3),im.diff_3, " & _
						 "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4), im.diff_4),'NONE') size_cd, " & _
						 "im.dept,im.class,cl.class_name,'MD' prc_tp,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind, " & _
						 "clear_mkdn_index,rfr.clearance_id,rfr.clearance_display_id,isp.supplier,sp.sup_name " & _
						 "FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp,sups sp " & _
						 "WHERE rfr.item =im.item_parent AND im.item_level =im.tran_level AND rzl.zone_id =rfr.location AND rfr.zone_node_type =1 " & _
						 "AND rfr.clear_mkdn_index>1 AND rfr.clear_start_ind>=1 AND rfr.clearance_id =rcl.clearance_id AND rcl.reset_date IS NULL " & _
						 whereBegDt & " AND im.dept =cl.dept AND im.class =cl.class AND im.item =isp.item AND isp.primary_supp_ind='Y' AND isp.supplier =sp.supplier" & _
						 "UNION " & _
						 "SELECT /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ im.item,rfr.item item_cd, " & _
						 "NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
						 "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1), im.diff_1, " & _
						 "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2), im.diff_2, " & _
						 "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3),im.diff_3, " & _
						 "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4), im.diff_4),'NONE') size_cd, " & _
						 "im.dept,im.class,cl.class_name,'MD' prc_tp,rcl.zone_id,rcl.location,rfr.action_date,clear_retail,clear_start_ind, " & _
						 "clear_mkdn_index,rfr.clearance_id,rfr.clearance_display_id,isp.supplier,sp.sup_nameFROM rpm_future_retail rfr, " & _
						 "item_master im,rpm_clearance rcl,class cl,item_supplier isp,sups sp " & _
						 "WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rfr.location= rcl.location AND rfr.zone_node_type=2 " & _
						 "AND rfr.clear_mkdn_index>1 AND rfr.clear_start_ind>=1 AND rfr.clearance_id=rcl.clearance_id AND rcl.reset_date IS NULL " & _
						 whereBegDt & " AND im.dept=cl.dept AND im.class=cl.class AND im.item=isp.item AND isp.primary_supp_ind='Y' AND isp.supplier=sp.supplier)" & _
						 "UNION " & _
						 "(SELECT /*+parallel (rfr,4)*/ im.item,rfr.item item_cd,NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
						 "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1),im.diff_1, " & _
						 "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2), im.diff_2, " & _
						 "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3), im.diff_3, " & _
						 "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4), im.diff_4),'NONE') size_cd, " & _
						 "im.dept,im.class,cl.class_name,'MD' prc_tp,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind, " & _
						 "clear_mkdn_index,rfr.clearance_id,rfr.clearance_display_id,isp.supplier,sp.sup_name " & _
						 "FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp,sups sp,uda_item_lov uil " & _
						 "WHERE rfr.item=im.item_parent AND im.item_level= im.tran_level AND rzl.zone_id=rfr.location AND rfr.zone_node_type=1 " & _
						 "AND rfr.clear_mkdn_index=1 AND rfr.clear_start_ind=1 AND rfr.clearance_id=rcl.clearance_id AND rcl.reset_date IS NULL " & _
						 whereBegDt & " AND uil.item = im.item AND uil.uda_id = '9997' AND im.dept =cl.dept AND im.class =cl.class AND im.item = isp.item AND isp.primary_supp_ind = 'Y' AND isp.supplier = sp.supplier" & _
						 "UNION " & _
						 "SELECT /*+parallel (rfr,4)*/ im.item,rfr.item item_cd,NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
						 "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1),im.diff_1, " & _
						 "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2), im.diff_2, " & _
						 "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3), im.diff_3, " & _
						 "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4), im.diff_4),'NONE') size_cd, " & _
						 "im.dept,im.class,cl.class_name,'MD' prc_tp,rcl.zone_id,rcl.location,rfr.action_date,clear_retail,clear_start_ind, " & _
						 "clear_mkdn_index,rfr.clearance_id,rfr.clearance_display_id,isp.supplier,sp.sup_name " & _
						 "FROM rpm_future_retail rfr,item_master im,rpm_clearance rcl,class cl,item_supplier isp,sups sp,uda_item_lov uil " & _
						 "WHERE rfr.item=im.item_parent AND im.item_level = im.tran_level AND rfr.location = rcl.location AND rfr.zone_node_type=2 AND rfr.clear_mkdn_index=1 " & _
						 "AND rfr.clear_start_ind=1 AND rfr.clearance_id=rcl.clearance_id AND rcl.reset_date IS NULL " & whereBegDt & _
						 "AND uil.item = im.item AND uil.uda_id = '9997' AND im.dept=cl.dept AND im.class=cl.class AND im.item = isp.item AND isp.primary_supp_ind = 'Y' AND isp.supplier = sp.supplier)), " & _
						 "max_date AS (SELECT im.item,rzl.zone_id,rzl.location,MAX(rfr.action_date) action_date " & _
						 "FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,mrb " & _
						 "WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rzl.zone_id=rfr.location AND mrb.item=im.item AND mrb.location=rzl.location AND rfr.action_date < mrb.action_date AND rfr.zone_node_type=1 " & _
						 "GROUP BY im.item,rzl.zone_id,rzl.location " & _
						 "UNION " & _
						 "SELECT im.item,mrb.zone_id,mrb.location,MAX(rfr.action_date) action_date " & _
						 "FROM rpm_future_retail rfr,item_master im,mrb " & _
						 "WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND mrb.item=im.item AND mrb.location=rfr.location AND rfr.action_date < mrb.action_date AND rfr.zone_node_type=2 " & _
						 "GROUP BY im.item,mrb.zone_id,mrb.location), " & _
						 "prev_retail AS (SELECT md.item,md.location,rfr.selling_retail " & _
						 "FROM rpm_future_retail rfr,mrb,max_date md " & _
						 "WHERE rfr.item=mrb.item_cd AND rfr.location = mrb.zone_id AND rfr.action_date = md.action_date AND md.item=mrb.item AND md.location=mrb.location AND rfr.zone_node_type= 1 " & _
						 "UNION " & _
						 "SELECT md.item,md.location,rfr.selling_retail " & _
						 "FROM rpm_future_retail rfr,mrb,max_date md " & _
						 "WHERE rfr.item=mrb.item_cd AND rfr.action_date = md.action_date AND md.item=mrb.item AND md.location=mrb.location AND rfr.zone_node_type= 2) " & _
						 "SELECT /*+parallel(ils,4)*/ mrb.item sku_num,mrb.item_cd itm_cd,mrb.clear_retail ret_prc,mrb.clearance_display_id pc_num,mrb.action_date beg_dt,mrb.prc_tp pc_tp, " & _
						 "pr.selling_retail prev_perm_ret,to_number(mrb.location) store_cd,'RECV' AS loc_cd,'RCV'  AS loc_tp,ils.stock_on_hand oh_qty,mrb.size_cd,lpad(mrb.supplier,7) ve_cd, " & _
						 "mrb.item_desc,mrb.dept dept_cd,mrb.class class_cd,mrb.class_name class_desc,mrb.sup_name AS VE_name " & _
						 "FROM mrb,prev_retail pr,item_loc_soh ils,v_deps vd " & _
						 "WHERE pr.item=mrb.item AND pr.location=mrb.location AND ils.item=mrb.item AND ils.loc=mrb.location AND mrb.dept=vd.dept AND to_number(mrb.location)=" & warehouse & _
						 "AND ils.stock_on_hand>0 " & whereSql & prcSql & _
						 "ORDER BY loc_tp,loc_cd,mrb.dept,mrb.class,mrb.clear_retail,lpad(mrb.supplier,7),mrb.item_cd,mrb.item"

                        'strSql = "with mrb as ((select /*+parallel*/  im.item,rfr.item item_cd,nvl(substr(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc," & _
                        '        "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1), im.diff_1,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2" & _
                        '        "), im.diff_2,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3),im.diff_3,(select diff_type from v_diff_id_group_type where id_group = im.diff_4" & _
                        '        "), im.diff_4),'NONE') size_cd,im.dept,im.class,cl.class_name,'MD' prc_tp,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind,clear_mkdn_index," & _
                        '        " rfr.clearance_id,rfr.clearance_display_id,isp.supplier,sp.sup_name FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl," & _
                        '        " item_supplier isp,sups sp WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rzl.zone_id=rfr.location and rfr.zone_node_type=1 and rfr.clear_mkdn_index>1" & _
                        '        " and rfr.clear_start_ind>=1 and rfr.clearance_id=rcl.clearance_id and rcl.reset_date is null " & whereBegDt & " and im.dept=cl.dept and im.class=cl.class " & _
                        '        " and im.item=isp.item and isp.primary_supp_ind='Y' and isp.supplier=sp.supplier) union " & _
                        '        " (select /*+parallel*/  im.item,rfr.item item_cd,nvl(substr(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc," & _
                        '        " NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1),im.diff_1,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2" & _
                        '        "), im.diff_2,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3), im.diff_3,(select diff_type from v_diff_id_group_type where id_group = im.diff_4" & _
                        '        "), im.diff_4),'NONE') size_cd,im.dept,im.class,cl.class_name,'MD' prc_tp,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind,clear_mkdn_index," & _
                        '        " rfr.clearance_id,rfr.clearance_display_id,isp.supplier,sp.sup_name FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp," & _
                        '        " sups sp,uda_item_lov uil WHERE rfr.item=im.item_parent AND im.item_level= im.tran_level AND rzl.zone_id=rfr.location AND rfr.zone_node_type=1 and rfr.clear_mkdn_index=1 " & _
                        '        " and rfr.clear_start_ind =1 and rfr.clearance_id =rcl.clearance_id and rcl.reset_date is null " & whereBegDt & " and uil.item = im.item and uil.uda_id = '9997'" & _
                        '        " and im.dept=cl.dept and im.class=cl.class and im.item = isp.item and isp.primary_supp_ind = 'Y' and isp.supplier = sp.supplier)),max_date as (SELECT im.item,rzl.zone_id," & _
                        '        " rzl.location,max(rfr.action_date) action_date FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,mrb WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level" & _
                        '        " and rzl.zone_id=rfr.location and mrb.item=im.item and mrb.location=rzl.location and rfr.action_date < mrb.action_date AND rfr.zone_node_type=1 " & _
                        '        " group by im.item,rzl.zone_id,rzl.location),prev_retail as (select md.item, md.location, rfr.selling_retail from rpm_future_retail rfr , mrb, max_date md" & _
                        '        " where rfr.item=mrb.item_cd AND rfr.location= mrb.zone_id AND rfr.action_date = md.action_date and md.item=mrb.item AND md.location=mrb.location)" & _
                        '        " SELECT /*+parallel(rfr)*/mrb.item sku_num,mrb.item_cd itm_cd,mrb.clear_retail ret_prc,mrb.clearance_display_id pc_num,mrb.action_date beg_dt,mrb.prc_tp pc_tp,pr.selling_retail prev_perm_ret," & _
                        '        " to_number(mrb.location) store_cd,'RECV' AS loc_cd,'RCV' AS loc_tp,ils.stock_on_hand oh_qty,mrb.size_cd,lpad(mrb.supplier,7) ve_cd,mrb.item_desc,mrb.dept dept_cd,mrb.class class_cd,mrb.class_name class_desc,mrb.sup_name as VE_name " & _
                        '        " from  mrb,prev_retail pr,item_loc_soh ils,v_deps vd WHERE pr.item=mrb.item AND pr.location=mrb.location and ils.item=mrb.item and ils.loc=mrb.location and mrb.dept=vd.dept and to_number(mrb.location)= " & warehouse & " and ils.stock_on_hand>0 " & _
                        '        whereSql & prcSql & _
                        '        " order by loc_tp,loc_cd,mrb.dept,mrb.class, mrb.clear_retail,mrb.supplier,mrb.item_cd,mrb.item"

                    Case MDReportType.MOS
						strSql = "SELECT im.item AS sku_num,im.item_parent AS itm_cd,'CHARITY' AS ret_prc,'' AS pc_num,ud.uda_date beg_dt, " & _
						   "'MOS' pc_tp,il.selling_unit_retail AS prev_perm_ret,to_number(il.loc) AS store_cd,'RECV' AS loc_cd,'RCV' AS loc_tp,ils.stock_on_hand AS oh_qty, " & _
						   "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1), im.diff_1, " & _
						   "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2), im.diff_2, " & _
						   "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3), im.diff_3, " & _
						   "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4), im.diff_4),'NONE') size_cd, " & _
						   "lpad(isp.supplier,7) AS ve_cd,NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
						   "im.dept AS dept_cd,im.class AS class_cd,CL.CLASS_NAME AS class_desc,sp.sup_name AS ve_name " & _
						   "FROM item_master im,item_loc il,uda_item_date ud,item_supplier isp,class cl,sups sp,item_loc_soh ils,v_deps vd " & _
						   "WHERE im.item = il.item AND IM.ITEM = UD.ITEM AND ud.uda_id = '56' AND im.item_level = im.tran_level AND IM.DEPT = vd.dept " & _
						   "AND im.dept = cl.dept AND im.class = cl.class AND im.item = isp.item AND sp.supplier = isp.supplier " & whereSql & _
						   "AND ils.item = il.item AND IL.LOC = ILS.LOC " & whereBegDt & " AND to_number(il.loc) = " & warehouse & " AND isp.primary_supp_ind = 'Y' " & _
						   "ORDER BY loc_tp,loc_cd,im.dept,im.class,il.selling_unit_retail,lpad(isp.supplier,7),im.item,im.item_parent"


                    Case MDReportType.MDAll

						strSql = "WITH mrb AS (SELECT /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ im.item,rfr.item item_cd,NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
						  "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1), im.diff_1, " & _
						  "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2), im.diff_2, " & _
						  "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3), im.diff_3, " & _
						  "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4), im.diff_4),'NONE') size_cd, " & _
						  "im.dept,im.class,cl.class_name,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind,clear_mkdn_index, " & _
						  "rfr.clearance_id,rfr.clearance_display_id,isp.supplier,sp.sup_name " & _
						  "FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp,sups sp " & _
						  "WHERE(rfr.item = im.item_parent AND im.item_level = im.tran_level AND rzl.zone_id = rfr.location AND rfr.zone_node_type = 1 " & _
						  "AND rfr.clear_mkdn_index > 0) AND rfr.clearance_id = rcl.clearance_id AND rcl.reset_date IS NULL " & whereBegDt & " AND im.dept = cl.dept " & _
						  "AND im.class = cl.class AND im.item = isp.item AND isp.primary_supp_ind = 'Y' AND isp.supplier = sp.supplier " & _
						  "UNION " & _
						  "SELECT /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ im.item,rfr.item item_cd,NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
						  "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1), im.diff_1, " & _
						  "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2), im.diff_2, " & _
						  "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3), im.diff_3, " & _
						  "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4), im.diff_4),'NONE') size_cd, " & _
						  "im.dept,im.class,cl.class_name,rcl.zone_id,rcl.location,rfr.action_date,clear_retail,clear_start_ind, " & _
						  "clear_mkdn_index,rfr.clearance_id,rfr.clearance_display_id,isp.supplier,sp.sup_name " & _
						  "FROM rpm_future_retail rfr,item_master im,rpm_clearance rcl,class cl,item_supplier isp,sups sp " & _
						  "WHERE(rfr.item = im.item_parent AND im.item_level = im.tran_level AND rfr.location = rcl.location AND rfr.zone_node_type = 2 " & _
						  "AND rfr.clear_mkdn_index > 0) AND rfr.clearance_id = rcl.clearance_id AND rcl.reset_date IS NULL " & whereBegDt & _
						  "AND im.dept = cl.dept AND im.class = cl.class AND im.item = isp.item AND isp.primary_supp_ind = 'Y' AND isp.supplier = sp.supplier), " & _
						  "max_date AS (SELECT im.item,rzl.zone_id,rzl.location,MAX(rfr.action_date) action_date " & _
						  "FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,mrb " & _
						  "WHERE(rfr.item = im.item_parent AND im.item_level = im.tran_level AND rzl.zone_id = rfr.location AND mrb.item = im.item) " & _
						  "AND mrb.location = rzl.location AND rfr.action_date < mrb.action_date AND rfr.zone_node_type= 1 " & _
						  "GROUP BY im.item,rzl.zone_id,rzl.location " & _
						  "UNION " & _
						  "SELECT im.item,mrb.zone_id,mrb.location,MAX(rfr.action_date) action_date " & _
						  "FROM rpm_future_retail rfr,item_master im,mrb " & _
						  "WHERE(rfr.item = im.item_parent AND im.item_level = im.tran_level AND mrb.item = im.item) AND mrb.location = rfr.location " & _
						  "AND rfr.action_date < mrb.action_date AND rfr.zone_node_type= 2 " & _
						  "GROUP BY im.item,mrb.zone_id,mrb.location), " & _
						  "prev_retail AS (SELECT md.item,md.location,rfr.selling_retail " & _
						  "FROM rpm_future_retail rfr,mrb,max_date md " & _
						  "WHERE rfr.item = mrb.item_cd AND rfr.location = mrb.zone_id AND rfr.action_date = md.action_date AND md.item = mrb.item AND md.location = mrb.location AND rfr.zone_node_type= 1 " & _
						  "UNION " & _
						  "SELECT md.item,md.location,rfr.selling_retail " & _
						  "FROM rpm_future_retail rfr,mrb,max_date md " & _
						  "WHERE rfr.item = mrb.item_cd AND rfr.action_date = md.action_date AND md.item = mrb.item AND md.location = mrb.location AND rfr.zone_node_type = 2) " & _
						  "SELECT /*+parallel(ils,4)*/ mrb.item sku_num,mrb.item_cd itm_cd,mrb.clear_retail ret_prc,mrb.clearance_display_id pc_num,mrb.action_date beg_dt,'MD' pc_tp, " & _
						  "pr.selling_retail prev_perm_ret,to_number(mrb.location) store_cd,'RECV' AS loc_cd,'RCV' AS loc_tp,ils.stock_on_hand oh_qty,mrb.size_cd,lpad(mrb.supplier,7) ve_cd, " & _
						  "mrb.item_desc,mrb.dept dept_cd,mrb.class class_cd,mrb.class_name class_desc,mrb.sup_name AS VE_name " & _
						  "FROM mrb,item_loc_soh ils,v_deps vd,prev_retail pr " & _
						  "WHERE pr.item = mrb.item AND pr.location = mrb.location AND to_number(mrb.location) = " & warehouse & " AND ils.item = mrb.item AND ils.stock_on_hand>0" & _
						  "AND ils.loc = mrb.location AND mrb.dept = vd.dept " & whereSql & prcSql & _
						  "ORDER BY loc_tp,loc_cd,mrb.dept,mrb.class,mrb.clear_retail,lpad(mrb.supplier,7),mrb.item_cd,mrb.item"

						'strSql = "with mrb as (select /*+parallel*/  im.item,rfr.item item_cd,nvl(substr(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc," & _
                        '             "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1), im.diff_1,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2" & _
                        '             "), im.diff_2,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3), im.diff_3,(select diff_type from v_diff_id_group_type where id_group = im.diff_4" & _
                        '             "), im.diff_4),'NONE') size_cd,im.dept,im.class,cl.class_name,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind,clear_mkdn_index,rfr.clearance_id," & _
                        '             " rfr.clearance_display_id,isp.supplier,sp.sup_name FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp,sups sp" & _
                        '             " WHERE(rfr.item = im.item_parent And im.item_level = im.tran_level And rzl.zone_id = rfr.location And rfr.zone_node_type = 1 And rfr.clear_mkdn_index > 0) " & _
                        '             " and rfr.clearance_id = rcl.clearance_id and rcl.reset_date is null " & whereBegDt & " and im.dept = cl.dept " & _
                        '             " and im.class = cl.class and im.item = isp.item and isp.primary_supp_ind = 'Y' and isp.supplier = sp.supplier)," & _
                        '             " max_date as (SELECT im.item,rzl.zone_id,rzl.location,max(rfr.action_date) action_date FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,mrb" & _
                        '             " WHERE(rfr.item = im.item_parent And im.item_level = im.tran_level And rzl.zone_id = rfr.location And mrb.item = im.item)" & _
                        '             " and mrb.location= rzl.location and rfr.action_date < mrb.action_date AND rfr.zone_node_type= 1 " & _
                        '             " group by im.item,rzl.zone_id,rzl.location),prev_retail as (select md.item, md.location, rfr.selling_retail from rpm_future_retail rfr , mrb, max_date md" & _
                        '             " where rfr.item= mrb.item_cd AND rfr.location= mrb.zone_id AND rfr.action_date = md.action_date and md.item= mrb.item AND md.location= mrb.location)" & _
                        '             " SELECT /*+parallel(rfr)*/mrb.item sku_num,mrb.item_cd itm_cd,mrb.clear_retail ret_prc,mrb.clearance_display_id pc_num," & _
                        '             " mrb.action_date beg_dt,'MD' pc_tp,pr.selling_retail prev_perm_ret,to_number(mrb.location) store_cd, 'RECV' AS loc_cd,'RCV' AS loc_tp,ils.stock_on_hand oh_qty," & _
                        '             " mrb.size_cd,lpad(mrb.supplier,7) ve_cd,mrb.item_desc,mrb.dept dept_cd,mrb.class class_cd,mrb.class_name class_desc,mrb.sup_name as VE_name " & _
                        '             " FROM mrb,item_loc_soh ils,v_deps vd,prev_retail pr WHERE pr.item= mrb.item and pr.location= mrb.location and to_number(mrb.location) = " & warehouse & " and ils.item= mrb.item and ils.stock_on_hand>0 " & _
                        '             " and ils.loc= mrb.location and mrb.dept = vd.dept " & _
                        '              whereSql & prcSql & _
                        '             " order by loc_tp,loc_cd,mrb.dept,mrb.class, mrb.clear_retail,mrb.supplier,mrb.item_cd,mrb.item"

                End Select

                ''Get MD Dataset
                '' 06/30/2006 - Sri Bajjuri - Set the current RET_PRC (effective today) as PREV_PERM_RET price
                'strSql = "select /*+ ALL_ROWS*/ /*Sri Bajjuri - Report distro MD reports*/ gm_prc.sku_num, gm_prc.itm_cd, gm_prc.ret_prc, gm_prc.pc_num, gm_prc.beg_dt, gm_prc.pc_tp, gm_prc.prev_perm_ret, " & _
                '        "gm_inv_loc_v.store_cd, gm_inv_loc_v.loc_cd, gm_inv_loc_v.loc_tp, gm_inv_loc_v.oh_qty, " & _
                '        "gm_sku.size_cd, lpad(gm_itm.ve_cd,7) as ve_cd, gm_itm.item_desc as item_desc,  gm_itm.dept_cd, gm_itm.class_cd, class.des as class_desc, ap.ve.ve_name " & _
                '        "from gm_prc " & _
                '        "inner join gm_sku on gm_sku.sku_num = gm_prc.sku_num " & _
                '        "inner join gm_inv_loc_v on gm_inv_loc_v.sku_num = gm_prc.sku_num and gm_inv_loc_v.store_cd = " & warehouse & " and gm_inv_loc_v.oh_qty > 0 " & _
                '       " inner join (select i.itm_cd, i.ve_cd,i.dept_cd,i.class_cd, NVL(wd.web_des, i.des1) as item_desc FROM GM_ITM i " & _
                '        "left join RPT_SERVICES_GERS_INQ.bm_itm_wdes wd on i.itm_cd = wd.itm_cd) gm_itm on gm_itm.itm_cd = gm_prc.itm_cd " & _
                '        "inner join class on gm_itm.class_cd = class.class_cd " & _
                '        "inner join dept on gm_itm.dept_cd = dept.dept_cd " & _
                '        "inner join ap.ve on ap.ve.ve_cd = gm_itm.ve_cd " & _
                '        whereSql & "and pc_tp ='" & pc_tp & "' " & prcSql & _
                '        "and gm_prc.prc_zone_cd = (select prc_zone_cd from store where store_cd = 9999) " & _
                '        "and gm_prc.prc_grp_cd = (select prc_grp_cd from store where store_cd = 9999) " & _
                '        "order by gm_inv_loc_v.loc_tp, gm_inv_loc_v.loc_cd, gm_itm.dept_cd, gm_itm.class_cd, gm_prc.ret_prc "

                mdDs = Cn.ExecuteQuery(EnumRSTypes.DataSet, strSql)
                'Throw New Exception(strSql)
                Cn.CloseConnection()
                If mdDs.Tables(0).Rows.Count = 0 Then
                    Throw New Exception("MDReport:No records found")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        '8/22/13 Michelle An - New queries
        Public Shared Sub GetMDReportDataSet(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByRef storeDs As DataSet)
            Try
                'Create Store Dataset
                Dim Cn As New OracleDataManager()
                Dim strSql As String

                Dim effectiveDate, pc_tp, prcSql, whereSql, whereBegDt As String
                Dim companyFilter As Int16 ' 1- HotTopic , 2 - Torrid, 0 - Both
                Dim cOwner As CompanyOwner
                If rptSubmission.CompanyOwners.Count = 1 Then
                    cOwner = rptSubmission.CompanyOwners(0)
                    companyFilter = cOwner.CompanyId
                ElseIf rptSubmission.CompanyOwners.Count = 2 Then
                    If DirectCast(rptSubmission.CompanyOwners(0), HotTopic.RD.Entity.CompanyOwner).CompanyName.ToLower().Trim() = "blackheart" Then
                        cOwner = rptSubmission.CompanyOwners(1)
                        companyFilter = 3
                    ElseIf DirectCast(rptSubmission.CompanyOwners(0), HotTopic.RD.Entity.CompanyOwner).CompanyName = "boxlunch" Then
                        cOwner = rptSubmission.CompanyOwners(0)
                        companyFilter = 3
                    End If
                Else
                    companyFilter = 0
                End If
                strSql = ""
                prcSql = ""

                Select Case rptSubmission.MDReportType
                    Case MDReportType.Regular

                        prcSql = "and rfr.clear_retail>.01 "
                    Case MDReportType.Further

                        prcSql = "and rfr.clear_retail>.01 "
                    Case MDReportType.MOS

                        prcSql = " "
                    Case MDReportType.MDAll

                        prcSql = "and rfr.clear_retail>.01 "

                End Select


                If IsDate(rptSubmission.MDEffectiveDate) Then
                    effectiveDate = rptSubmission.MDEffectiveDate.Day & "-" & MonthName(rptSubmission.MDEffectiveDate.Month, True).ToLower & "-" & rptSubmission.MDEffectiveDate.Year

                    If (rptSubmission.MDReportType <> MDReportType.MOS) Then
                        whereBegDt = " and rcl.effective_date = '" & effectiveDate & "' "
                    Else
                        whereBegDt = " and ud.uda_date = '" & effectiveDate & "' "
                    End If
                Else
                    Throw New Exception("MDReport:Invalid MD report request")
                End If
                ' ht, torrid dept filter
                If companyFilter > 0 Then
                    Select Case companyFilter
                        Case 1 ' Hot Topic
                            whereSql &= "and vd.division = 1 "
                        Case 2 ' Torrid
                            whereSql &= "and vd.division = 5 "
                        Case 3, 4 'Blackheart
                            whereSql &= "and vd.division in (4,8) "
                        Case 9 'LoveSick
                            whereSql &= "and vd.division = 9 "
                    End Select
                End If

                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@CompanyFilter", SqlDbType.Int)
                arParms(0).Value = companyFilter
                storeDs = SQLDataManager.GetInstance().GetDataSet("dbo.RSP_ListStores", arParms)


                Select Case rptSubmission.MDReportType
                    Case MDReportType.Regular
                        strSql = "With Zone_CTE as (SELECT rcl.clearance_display_id,rcl.item ITEM_CD,im.item SKU,'MD' AS PC_TP,NVL(wd.web_des,im.short_desc) AS ITEM_DESC, " & _
                          "DECODE(rcl.zone_node_type,0,rcl.location,1,rzl.location) STORE_CD,rcl.effective_date BEG_DT,rfr.selling_retail PREV_PERM_RET,rfr.clear_retail RET_PRC, " & _
                          "NVL(DECODE('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') size_cd, " & _
                          "NVL(DECODE ('COLOR',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1), im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') item_color, " & _
                          "im.dept DEPT_CD,im.class CLASS_CD,cl.class_name CLASS_DESC,ils.stock_on_hand OH_QTY " & _
                          "FROM rpm_clearance rcl LEFT JOIN rtlintf.item_web_desc wd ON rcl.item=wd.item JOIN item_master im ON rcl.item=im.item_parent " & _
                          "JOIN rpm_future_retail rfr ON rfr.clearance_id=rcl.clearance_id AND rfr.item=rcl.item AND rfr.zone_node_type=rcl.zone_node_type AND (rfr.zone_node_type=1) " & _
                          "JOIN rpm_zone_location rzl ON rfr.location=rzl.zone_id JOIN v_deps vd ON im.dept=vd.dept JOIN class cl ON IM.DEPT=cl.dept AND im.class=cl.class " & _
                          "JOIN item_loc_soh ils ON ils.item=im.item AND ils.loc=rzl.location " & _
                          "WHERE im.item_level=im.tran_level AND ils.stock_on_hand > 0 AND rfr.clear_mkdn_index=1 AND rfr.clear_start_ind=1 " & _
                          "AND NOT EXISTS(SELECT 1 FROM uda_item_lov uil WHERE uil.item=im.item AND uil.uda_id='9997') " & _
                          whereSql & prcSql & whereBegDt & _
                          "AND rcl.state IN ('pricechange.state.approved','pricechange.state.executed')) " & _
                          ",Loc_CTE as (SELECT rcl.clearance_display_id,rcl.item ITEM_CD,im.item SKU,'MD' AS PC_TP,NVL(wd.web_des,im.short_desc) AS ITEM_DESC, " & _
                          "rcl.location STORE_CD,rcl.effective_date BEG_DT,rfr.selling_retail PREV_PERM_RET,rfr.clear_retail RET_PRC, " & _
                          "NVL(DECODE('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') size_cd, " & _
                          "NVL(DECODE ('COLOR',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') item_color, " & _
                          "im.dept DEPT_CD,im.class CLASS_CD,cl.class_name CLASS_DESC,ils.stock_on_hand OH_QTY " & _
                          "FROM rpm_clearance rcl LEFT JOIN rtlintf.item_web_desc wd ON rcl.item=wd.item JOIN item_master im ON rcl.item=im.item_parent " & _
                          "JOIN rpm_future_retail rfr ON rfr.clearance_id=rcl.clearance_id AND rfr.item=rcl.item and rfr.location=rcl.location AND rfr.zone_node_type=rcl.zone_node_type AND rfr.zone_node_type=0 " & _
                          "JOIN v_deps vd ON im.dept=vd.dept JOIN class cl ON IM.DEPT=cl.dept AND im.class=cl.class JOIN item_loc_soh ils ON ils.item=im.item AND ils.loc=rcl.location " & _
                          "WHERE im.item_level=im.tran_level AND ils.stock_on_hand>0 AND rfr.clear_mkdn_index=1 AND rfr.clear_start_ind=1 " & _
                          "AND NOT EXISTS(SELECT 1 FROM uda_item_lov uil WHERE uil.item=im.item AND uil.uda_id='9997') " & _
                          whereSql & prcSql & whereBegDt & _
                          "AND rcl.state IN ('pricechange.state.approved','pricechange.state.executed')) " & _
                          "select clearance_display_id,ITEM_CD,SKU,PC_TP,ITEM_DESC,STORE_CD,BEG_DT,PREV_PERM_RET,RET_PRC,size_cd, item_color, DEPT_CD, CLASS_CD, CLASS_DESC, OH_QTY from ( " & _
                          "SELECT Z.* FROM ZONE_CTE Z LEFT OUTER JOIN LOC_CTE L ON Z.SKU= L.SKU AND Z.BEG_DT = L.BEG_DT AND Z.STORE_CD = L.STORE_CD " & _
                          "WHERE L.SKU IS NULL UNION SELECT * FROM LOC_CTE) ORDER BY store_cd,DEPT_CD,class_cd,RET_PRC,item_cd,sku"

                    Case MDReportType.Further
                        strSql = "with ZONE_CTE as (SELECT rcl.clearance_display_id,rcl.item ITEM_CD,im.item SKU,'MD' AS PC_TP,NVL(wd.web_des, im.short_desc) AS ITEM_DESC, " & _
                          "DECODE(rcl.zone_node_type,0,rcl.location,1,rzl.location) STORE_CD,rcl.effective_date BEG_DT,rfr.selling_retail PREV_PERM_RET,rfr.clear_retail RET_PRC, " & _
                          "NVL(DECODE('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1), im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') size_cd, " & _
                          "NVL(DECODE ('COLOR',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') item_color, " & _
                          "im.dept DEPT_CD,im.class CLASS_CD,cl.class_name CLASS_DESC,ils.stock_on_hand OH_QTY " & _
                          "FROM rpm_clearance rcl LEFT JOIN rtlintf.item_web_desc wd ON rcl.item=wd.item JOIN item_master im ON rcl.item=im.item_parent " & _
                          "JOIN rpm_future_retail rfr ON rfr.clearance_id=rcl.clearance_id AND rfr.item=rcl.item AND rfr.zone_node_type=rcl.zone_node_type AND (rfr.location=rcl.location OR rfr.location=rcl.zone_id) AND (rfr.zone_node_type = 1) " & _
                          "JOIN rpm_zone_location rzl ON rfr.location=rzl.zone_id JOIN v_deps vd ON im.dept=vd.dept JOIN class cl ON IM.DEPT=cl.dept AND im.class=cl.class JOIN item_loc_soh ils ON ils.item=im.item AND ils.loc=rzl.location " & _
                          "WHERE im.item_level=im.tran_level AND ils.stock_on_hand>0 AND rfr.clear_mkdn_index=2 AND rfr.clear_start_ind=1 " & _
                          whereSql & prcSql & whereBegDt & _
                          "AND rcl.state IN ('pricechange.state.approved','pricechange.state.executed')) " & _
                          ", LOC_CTE as (SELECT rcl.clearance_display_id,rcl.item ITEM_CD,im.item SKU,'MD' AS PC_TP,NVL(wd.web_des, im.short_desc) AS ITEM_DESC, " & _
                          "rcl.location store_cd,rcl.effective_date BEG_DT,rfr.selling_retail PREV_PERM_RET,rfr.clear_retail RET_PRC, " & _
                          "NVL(DECODE('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') size_cd, " & _
                          "NVL(DECODE('COLOR',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') item_color, " & _
                          "im.dept DEPT_CD,im.class CLASS_CD,cl.class_name CLASS_DESC,ils.stock_on_hand OH_QTY " & _
                          "FROM rpm_clearance rcl LEFT JOIN rtlintf.item_web_desc wd ON rcl.item=wd.item JOIN item_master im ON rcl.item = im.item_parent " & _
                          "JOIN rpm_future_retail rfr ON rfr.clearance_id=rcl.clearance_id AND rfr.item=rcl.item AND rfr.zone_node_type=rcl.zone_node_type AND (rfr.location=rcl.location OR rfr.location=rcl.zone_id) AND (rfr.zone_node_type=0) " & _
                          "JOIN v_deps vd ON im.dept=vd.dept JOIN class cl ON IM.DEPT=cl.dept AND im.class=cl.class JOIN item_loc_soh ils ON ils.item=im.item AND ils.loc=rcl.location " & _
                          "WHERE im.item_level=im.tran_level AND ils.stock_on_hand>0 AND rfr.clear_mkdn_index=2 AND rfr.clear_start_ind=1 " & _
                            whereSql & prcSql & whereBegDt & _
                          "AND rcl.state IN ('pricechange.state.approved','pricechange.state.executed')) " & _
                          "select clearance_display_id,ITEM_CD,SKU,PC_TP,ITEM_DESC,STORE_CD,BEG_DT,PREV_PERM_RET,RET_PRC,size_cd, item_color, DEPT_CD, CLASS_CD, CLASS_DESC, OH_QTY from ( " & _
                          "SELECT Z.* FROM ZONE_CTE Z LEFT OUTER JOIN LOC_CTE L ON Z.SKU= L.SKU AND Z.BEG_DT = L.BEG_DT AND Z.STORE_CD = L.STORE_CD " & _
                          "WHERE L.SKU IS NULL UNION SELECT * FROM LOC_CTE) ORDER BY store_cd,DEPT_CD,class_cd,RET_PRC,item_cd,sku"

                    Case MDReportType.History
                        strSql = "SELECT rcl.item ITEM_CD, im.item SKU, 'MD' AS PC_TP, NVL(wd.web_des, im.short_desc) AS ITEM_DESC, ils.loc STORE_CD, rcl.MDDATE BEG_DT, rcl.curr_retail PREV_PERM_RET, rcl.curr_retail RET_PRC, " & _
                          "NVL(DECODE('SIZE', (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1), im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 ), im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3 ), im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4 ), im.diff_4),'NONE') size_cd, " & _
                          "NVL(DECODE ('COLOR', (SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1 ), im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 ), im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3 ), im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4 ), im.diff_4),'NONE') item_color, " & _
                          "im.dept DEPT_CD, im.class CLASS_CD, cl.class_name CLASS_DESC, ils.stock_on_hand OH_QTY " & _
                          "FROM ITEM_MD_HISTORY rcl LEFT JOIN item_web_desc wd ON rcl.item = wd.item JOIN item_master im ON rcl.item = im.item_parent " & _
                          "JOIN v_deps vd ON im.dept = vd.dept JOIN class cl ON IM.DEPT = cl.dept AND im.class = cl.class JOIN item_loc_soh ils ON ils.item = im.item " & _
                          "WHERE im.item_level = im.tran_level AND ils.stock_on_hand > 0 " & _
                          whereSql & "and rcl.MDDATE='" & effectiveDate & "' " & _
                          "ORDER BY ils.loc,im.dept,im.class,rcl.curr_retail,rcl.item,im.item"

                    Case MDReportType.MOS
                        strSql = "SELECT im.item AS sku,im.item_parent AS item_cd,0 AS ret_prc,'' AS pc_num,ud.uda_date beg_dt,'MOS' pc_tp, " & _
                           "il.selling_unit_retail AS prev_perm_ret,to_number(il.loc) AS store_cd,ils.stock_on_hand AS oh_qty, " & _
                           "NVL(DECODE('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
                           "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                           "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                           "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') size_cd, " & _
                           "lpad(isp.supplier,7) AS ve_cd, " & _
                           "NVL(DECODE('COLOR',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
                           "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                           "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                           "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') item_color, " & _
                           "NVL(NVL(wd.web_des,wd1.web_des),NVL(SUBSTR(short_desc,1,instr(item_desc,':',1,1)-1),im.short_desc)) AS ITEM_DESC, " & _
                           "im.dept AS dept_cd,im.class AS class_cd,CL.CLASS_NAME AS class_desc,sp.sup_name AS ve_name " & _
                           "FROM item_master im JOIN item_loc il ON im.item=il.item AND im.item_level=im.tran_level " & _
                           "JOIN uda_item_date ud ON IM.ITEM = UD.ITEM AND ud.uda_id='56' JOIN item_supplier isp ON im.item=isp.item " & _
                           "JOIN class cl ON im.dept=cl.dept AND im.class=cl.class JOIN sups sp ON sp.supplier=isp.supplier " & _
                           "JOIN item_loc_soh ils ON ils.item=il.item AND IL.LOC=ILS.LOC JOIN v_deps vd ON IM.DEPT=vd.dept " & _
                           "LEFT JOIN item_web_desc wd ON im.item=wd.item LEFT JOIN rtlintf.item_web_desc wd1 ON im.item_parent=wd1.item " & _
                           "WHERE " & whereSql.Substring(4, whereSql.Length - 1 - 4) & whereBegDt & " AND isp.primary_supp_ind='Y' AND ils.stock_on_hand>0 " & _
                           "ORDER BY to_number(il.loc),im.dept,im.class,il.selling_unit_retail,im.item,im.item_parent "

                    Case MDReportType.MDAll
                        strSql = "WITH ZONE_CTE as (SELECT rcl.clearance_display_id,rcl.item ITEM_CD,im.item SKU,'MD' AS PC_TP,NVL(wd.web_des,im.short_desc) AS ITEM_DESC, " & _
                          "DECODE(rcl.zone_node_type,0,rcl.location,1,rzl.location) STORE_CD,rcl.effective_date BEG_DT,rfr.selling_retail PREV_PERM_RET,rfr.clear_retail RET_PRC, " & _
                          "NVL(DECODE('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') size_cd, " & _
                          "NVL(DECODE('COLOR',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') item_color, " & _
                          "im.dept DEPT_CD,im.class CLASS_CD,cl.class_name CLASS_DESC,ils.stock_on_hand OH_QTY " & _
                          "FROM rpm_clearance rcl LEFT JOIN rtlintf.item_web_desc wd ON rcl.item=wd.item JOIN item_master im ON rcl.item=im.item_parent " & _
                          "JOIN rpm_future_retail rfr ON rfr.clearance_id=rcl.clearance_id AND rfr.item=rcl.item AND rfr.zone_node_type=rcl.zone_node_type " & _
                          "AND (rfr.location=rcl.location OR rfr.location=rcl.zone_id) AND (rfr.zone_node_type=1) " & _
                          "JOIN rpm_zone_location rzl ON rfr.location=rzl.zone_id JOIN v_deps vd ON im.dept=vd.dept " & _
                          "JOIN class cl ON IM.DEPT=cl.dept AND im.class=cl.class JOIN item_loc_soh ils ON ils.item=im.item AND ils.loc=rzl.location " & _
                          "WHERE im.item_level=im.tran_level AND ils.stock_on_hand>0 AND rfr.clear_start_ind=1 " & _
                          "AND NOT EXISTS (SELECT 1 FROM uda_item_lov uil WHERE uil.item=im.item AND uil.uda_id='9997') " & _
                          whereSql & prcSql & whereBegDt & _
                          "AND rcl.state IN ('pricechange.state.approved','pricechange.state.executed')) " & _
                          ", LOC_CTE as (SELECT rcl.clearance_display_id,rcl.item ITEM_CD,im.item SKU,'MD' AS PC_TP,NVL(wd.web_des,im.short_desc) AS ITEM_DESC, " & _
                          "rcl.location store_cd,rcl.effective_date BEG_DT,rfr.selling_retail PREV_PERM_RET,rfr.clear_retail RET_PRC, " & _
                          "NVL(DECODE('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') size_cd, " & _
                          "NVL(DECODE('COLOR',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2),im.diff_2, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3, " & _
                          "(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_4),im.diff_4),'NONE') item_color, " & _
                          "im.dept DEPT_CD,im.class CLASS_CD,cl.class_name CLASS_DESC,ils.stock_on_hand OH_QTY " & _
                          "FROM rpm_clearance rcl LEFT JOIN rtlintf.item_web_desc wd ON rcl.item=wd.item JOIN item_master im ON rcl.item=im.item_parent " & _
                          "JOIN rpm_future_retail rfr ON rfr.clearance_id=rcl.clearance_id AND rfr.item=rcl.item AND rfr.zone_node_type=rcl.zone_node_type " & _
                          "AND (rfr.location=rcl.location OR rfr.location=rcl.zone_id) AND (rfr.zone_node_type=0) " & _
                          "JOIN v_deps vd ON im.dept=vd.dept JOIN class cl ON IM.DEPT=cl.dept AND im.class=cl.class " & _
                          "JOIN item_loc_soh ils ON ils.item=im.item AND ils.loc=rcl.location " & _
                          "WHERE im.item_level=im.tran_level AND ils.stock_on_hand>0 AND rfr.clear_start_ind=1 " & _
                          "AND NOT EXISTS (SELECT 1 FROM uda_item_lov uil WHERE uil.item=im.item AND uil.uda_id='9997') " & _
                          whereSql & prcSql & whereBegDt & _
                          "AND rcl.state IN ('pricechange.state.approved','pricechange.state.executed')) " & _
                          "select clearance_display_id,ITEM_CD,SKU,PC_TP,ITEM_DESC,STORE_CD,BEG_DT,PREV_PERM_RET,RET_PRC,size_cd, item_color, DEPT_CD, CLASS_CD, CLASS_DESC, OH_QTY from ( " & _
                          "SELECT Z.* FROM ZONE_CTE Z LEFT OUTER JOIN LOC_CTE L ON Z.SKU= L.SKU AND Z.BEG_DT = L.BEG_DT AND Z.STORE_CD = L.STORE_CD " & _
                          "WHERE L.SKU IS NULL UNION SELECT * FROM LOC_CTE) ORDER BY store_cd,DEPT_CD,class_cd,RET_PRC,item_cd,sku"
                End Select

                mdDs = Cn.ExecuteQuery(EnumRSTypes.DataSet, strSql)

                Cn.CloseConnection()
                If mdDs.Tables(0).Rows.Count = 0 Then
                    Throw New Exception("MDReport:No records found")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

		Public Shared Sub GetPromoReportDataSet(ByVal rptSubmission As Submission, ByRef promoDs As DataSet, ByRef storeDs As DataSet)
			Try
				'Create Store Dataset
				Dim Cn As New OracleDataManager()
				Dim strSql As String

                Dim effectiveDate, prcSql, whereSql, lwhereSql, whereBegDt As String
				Dim companyFilter As Int16 ' 1- HotTopic , 2 - Torrid, 0 - Both
				Dim cOwner As CompanyOwner
				If rptSubmission.CompanyOwners.Count = 1 Then
					cOwner = rptSubmission.CompanyOwners(0)
					companyFilter = cOwner.CompanyId
				Else
					companyFilter = 0
				End If
				strSql = ""

				If IsDate(rptSubmission.MDEffectiveDate) Then
					effectiveDate = rptSubmission.MDEffectiveDate.Day & "-" & MonthName(rptSubmission.MDEffectiveDate.Month, True).ToLower & "-" & rptSubmission.MDEffectiveDate.Year

					whereBegDt = " and dtl.start_date = '" & effectiveDate & "' "
				Else
					Throw New Exception("PromoReport:Invalid Promo report request")
				End If
				' ht, torrid dept filter
                'If companyFilter > 0 Then
                'Select Case companyFilter
                '   Case 1 ' Hot Topic
                'whereSql = "and ((l.zone_id in (6, 8)) or ((ils.loc=l.location) and l.location in(select location from rpm_zone_location where zone_id in(6, 8)))) "
                '   Case 2 ' Torrid
                'whereSql = "and ((l.zone_id in (7)) or ((ils.loc=l.location) and l.location in(select location from rpm_zone_location where zone_id in(7)))) "
                '    Case 3 'Blackheart
                'whereSql = "and ((l.zone_id in (9)) or ((ils.loc=l.location) and l.location in(select location from rpm_zone_location where zone_id in(9)))) "
                '    Case 9 'LoveSick
                'whereSql = "and ((l.zone_id in (249)) or ((ils.loc=l.location) and l.location in(select location from rpm_zone_location where zone_id in(249)))) "
                'End Select
                'End If

                If companyFilter > 0 Then
                    Select Case companyFilter
                        Case 1 ' Hot Topic
                            whereSql = "and (l.zone_id in (6, 8)) "
                            lwhereSql = "and l.location in (select location from rpm_zone_location where zone_id in (6,8)) "
                        Case 2 ' Torrid
                            whereSql = "and (l.zone_id in (7,209)) "
                            lwhereSql = "and l.location in (select location from rpm_zone_location where zone_id in (7,209)) "
                        Case 3 'Blackheart
                            whereSql = "and (l.zone_id in (9))  "
                            lwhereSql = "and l.location in (select location from rpm_zone_location where zone_id in (9)) "
                        Case 4 'BoxLunch
                            whereSql = "and (l.zone_id in (166))  "
                            lwhereSql = "and l.location in (select location from rpm_zone_location where zone_id in (166)) "
                        Case 9 'LoveSick
                            whereSql = "and (l.zone_id in (249)) "
                            lwhereSql = "and l.location in (select location from rpm_zone_location where zone_id in (249)) "
                    End Select
                End If

                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@CompanyFilter", SqlDbType.Int)
                If companyFilter = 4 Then
                    arParms(0).Value = 3
                Else
                    arParms(0).Value = companyFilter
                End If
                storeDs = SQLDataManager.GetInstance().GetDataSet("dbo.RSP_ListStores", arParms)

                '   strSql = "select distinct " & _
                '   "lpad(im.dept,4,'0') dept, " & _
                '   "im.class, " & _
                '   "im.item_parent style, " & _
                '   "im.item sku, " & _
                '   "f.location, " & _
                '   "decode(dl.change_type,'0',abs(dl.change_percent)||'% off','1','$'||abs(dl.change_amount)||' off','2','$'||abs(dl.change_amount)) change_by, " & _
                '   "f.simple_promo_retail, " & _
                '   "decode(f.clearance_id,null,selling_retail,clear_retail) current_retail, " & _
                '   "ils.stock_on_hand, " & _
                '   "nvl(wd.web_des,im.short_desc) web_des, " & _
                '   "im.diff_1 size_cd " & _
                '   "from rpm_promo_dtl_merch_node mn, " & _
                '   "item_master im, " & _
                '   "rpm_promo_dtl dtl, " & _
                '   "rpm_promo_zone_location l, " & _
                '   "rpm_future_retail f, " & _
                '   "rpm_promo r, " & _
                '   "rpm_promo_comp c, " & _
                '   "rpm_promo_dtl_disc_ladder dl, " & _
                '   "item_loc_soh ils, " & _
                '   "rtlintf.ITEM_WEB_DESC wd " & _
                '   "where dtl.promo_dtl_id=mn.promo_dtl_id " & _
                '   "and im.item_parent=mn.item " & _
                '   whereBegDt & _
                '   "and dtl.promo_dtl_id=l.promo_dtl_id " & _
                '   "and r.promo_id=c.promo_id " & _
                '   "and c.promo_comp_id=dtl.promo_comp_id " & _
                '   "and dl.promo_dtl_list_id=mn.promo_dtl_list_id " & _
                '   "and f.item=im.item_parent " & _
                '   "and f.item=mn.item " & _
                '   "and f.zone_node_type=0 " & _
                '   "and f.action_date=dtl.start_date " & _
                '   "and f.item_parent is null " & _
                '   "and ils.item = im.item " & _
                '   "and ils.loc = f.location " & _
                '   whereSql & _
                '  "and im.item_parent = wd.item(+) " & _
                '  "and ils.stock_on_hand > 0 " & _
                '  "order by change_by, dept, style, sku"

                strSql = "select distinct " & _
                "lpad(im.dept,4,'0') dept, " & _
                "im.class, " & _
                "im.item_parent style, " & _
                "im.item sku, " & _
                "f.location, " & _
                "decode(dl.change_type,'0',abs(dl.change_percent)||'% off','1','$'||abs(dl.change_amount)||' off','2','$'||abs(dl.change_amount)) change_by, " & _
                "f.simple_promo_retail, " & _
                "decode(f.clearance_id,null,selling_retail,clear_retail) current_retail, " & _
                "ils.stock_on_hand, " & _
                "nvl(wd.web_des,im.short_desc) web_des, " & _
                "im.diff_1 size_cd " & _
                "from rpm_promo_dtl_merch_node mn, " & _
                "item_master im, " & _
                "rpm_promo_dtl dtl, " & _
                "rpm_promo_zone_location l, " & _
                "rpm_future_retail f, " & _
                "rpm_promo r, " & _
                "rpm_promo_comp c, " & _
                "rpm_promo_dtl_disc_ladder dl, " & _
                "item_loc_soh ils, " & _
                "rtlintf.ITEM_WEB_DESC wd, " & _
                "rpm_zone_location zl " & _
                "where dtl.promo_dtl_id=mn.promo_dtl_id " & _
                "and im.item_parent=mn.item " & _
                whereBegDt & _
                "and dtl.promo_dtl_id=l.promo_dtl_id " & _
                "and r.promo_id=c.promo_id " & _
                "and c.promo_comp_id=dtl.promo_comp_id " & _
                "and dl.promo_dtl_list_id=mn.promo_dtl_list_id " & _
                "and f.item=im.item_parent " & _
                "and f.item=mn.item " & _
                "and f.zone_node_type=0 " & _
                "and l.zone_node_type=1 " & _
                "and f.action_date=dtl.start_date " & _
                "and f.item_parent is null " & _
                "and ils.item = im.item " & _
                "and ils.loc = f.location " & _
                "and zl.location=f.location " & _
                "and zl.zone_id=l.zone_id " & _
                whereSql & _
                "and im.item_parent = wd.item(+) " & _
                "and ils.stock_on_hand > 0 " & _
                "union " & _
                "select distinct " & _
                "lpad(im.dept,4,'0') dept, " & _
                "im.class, " & _
                "im.item_parent style, " & _
                "im.item sku, " & _
                "f.location, " & _
                "decode(dl.change_type,'0',abs(dl.change_percent)||'% off','1','$'||abs(dl.change_amount)||' off','2','$'||abs(dl.change_amount)) change_by, " & _
                "f.simple_promo_retail, " & _
                "decode(f.clearance_id,null,selling_retail,clear_retail) current_retail, " & _
                "ils.stock_on_hand, " & _
                "nvl(wd.web_des,im.short_desc) web_des, " & _
                "im.diff_1 size_cd " & _
                "from rpm_promo_dtl_merch_node mn, " & _
                "item_master im, " & _
                "rpm_promo_dtl dtl, " & _
                "rpm_promo_zone_location l, " & _
                "rpm_future_retail f, " & _
                "rpm_promo r, " & _
                "rpm_promo_comp c, " & _
                "rpm_promo_dtl_disc_ladder dl, " & _
                "item_loc_soh ils, " & _
                "rtlintf.ITEM_WEB_DESC wd " & _
                "where dtl.promo_dtl_id=mn.promo_dtl_id " & _
                "and im.item_parent=mn.item " & _
                whereBegDt & _
                "and dtl.promo_dtl_id=l.promo_dtl_id " & _
                "and r.promo_id=c.promo_id " & _
                "and c.promo_comp_id=dtl.promo_comp_id " & _
                "and dl.promo_dtl_list_id=mn.promo_dtl_list_id " & _
                "and f.item=im.item_parent " & _
                "and f.item=mn.item " & _
                "and f.zone_node_type=0 " & _
                "and l.zone_node_type=0 " & _
                "and f.action_date=dtl.start_date " & _
                "and f.item_parent is null " & _
                "and ils.item = im.item " & _
                "and ils.loc = f.location " & _
                "and ils.loc=l.location " & _
                lwhereSql & _
                "and im.item_parent = wd.item(+) " & _
                "and ils.stock_on_hand > 0 " & _
                "order by change_by, dept, style, sku"


                promoDs = Cn.ExecuteQuery(EnumRSTypes.DataSet, strSql)

                Cn.CloseConnection()

                If promoDs.Tables(0).Rows.Count = 0 Then
                    Throw New Exception("PromoReport:No records found")
                End If
            Catch ex As Exception
                Throw ex
            End Try
		End Sub


        'SRI 12/18/03 MD Reports
        Public Shared Sub GetMDReportDataSetOld(ByVal rptSubmission As Submission, ByRef mdDs As DataSet, ByRef storeDs As DataSet)
            Try
                'Create Store Dataset
                Dim Cn As New OracleDataManager()
                Dim strSql As String

                Dim effectiveDate, pc_tp, prcSql, whereSql, whereBegDt As String
                Dim companyFilter As Int16 ' 1- HotTopic , 2 - Torrid, 0 - Both
                Dim cOwner As CompanyOwner

                If rptSubmission.CompanyOwners.Count = 1 Then
                    cOwner = rptSubmission.CompanyOwners(0)
                    companyFilter = cOwner.CompanyId
                Else
                    companyFilter = 0
                End If
                strSql = ""
                prcSql = ""

                Select Case rptSubmission.MDReportType
                    Case MDReportType.Regular
                        '# RMS change 02/08/2013 by LJ Regular/further/all markdown ret_prc>.01
                        prcSql = "and mrb.clear_retail>.01 "
                    Case MDReportType.Further
                        'pc_tp = "MD"
                        prcSql = "and mrb.clear_retail>.01 "
                    Case MDReportType.MOS
                        'pc_tp = "MD"
                        prcSql = " "
                    Case MDReportType.MDAll
                        'pc_tp = "MD"
                        prcSql = "and mrb.clear_retail>.01 "
                        '# disable for RMS  01/31/2013 by LJ
                        'Case MDReportType.MarkUp
                        '    pc_tp = "MU"
                        '    prcSql = ""
                        'Case MDReportType.MDCancel
                        '    pc_tp = "MDC"
                        '    prcSql = ""
                        'Case MDReportType.MUCancel
                        '    pc_tp = "MUC"
                        '    prcSql = ""
                End Select

                '# RMS change for PC_NUM 02/07/13 by LJ
                'If rptSubmission.MDPcNum <> "" AndAlso IsNumeric(rptSubmission.MDPcNum) Then

                '    'whereSql = "where pc_num = '" & rptSubmission.MDPcNum & "' " 
                '    whereSql = " and mrb.clearance_display_id = '" & rptSubmission.MDPcNum & "' "

                '    '6/7/04 5PM quick fix roll this back
                '    '6/7/04 6PM quick fix rolled back
                '    'prcSql = " AND PRC.RET_PRC LIKE '%.99' "
                'Else
                If IsDate(rptSubmission.MDEffectiveDate) Then
                    effectiveDate = rptSubmission.MDEffectiveDate.Day & "-" & MonthName(rptSubmission.MDEffectiveDate.Month, True).ToLower & "-" & rptSubmission.MDEffectiveDate.Year
                    '# RMS change for effect date 02/07/13 by LJ
                    'whereSql = "where beg_dt = '" & effectiveDate & "' " 
                    If (rptSubmission.MDReportType <> MDReportType.MOS) Then
                        whereBegDt = " and rfr.action_date = '" & effectiveDate & "' "
                    Else
                        whereBegDt = " and ud.uda_date = '" & effectiveDate & "' "
                    End If
                Else
                    Throw New Exception("MDReport:Invalid MD report request")
                End If
                ' ht, torrid dept filter
                If companyFilter > 0 Then
                    Select Case companyFilter
                        Case 1 ' Hot Topic
                            whereSql &= "and vd.division = 1 "
                        Case 2 ' Torrid
                            whereSql &= "and vd.division = 5 "
                        Case 3 'Blackheart
                            whereSql &= "and vd.division = 8 "
                    End Select
                End If

                Dim arParms() As SqlParameter = New SqlParameter(0) {}
                arParms(0) = New SqlParameter("@CompanyFilter", SqlDbType.Int)
                arParms(0).Value = companyFilter
                storeDs = SQLDataManager.GetInstance().GetDataSet("dbo.RSP_ListStores", arParms)
                '" WHERE BEG_DT = '" & effectiveDate & "' AND pc_tp ='" & pc_tp & "' " & prcSql & _
                'Get MD Dataset
                ' MOD #10/04/2004 Exclude layaway inventory (layaway loc_cd is 'HOLD') from MD report
                ' 06/30/2006 - Sri Bajjuri - Set the current RET_PRC (effective today) as PREV_PERM_RET price
                'strSql = "with mrb as (select /*+parallel*/  im.item as sku_num, rfr.item item_cd as itm_cd,  "

                Select Case rptSubmission.MDReportType
                    Case MDReportType.Regular
                        strSql = "WITH mrb AS (SELECT /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ im.item ,rfr.item item_cd,NVL(SUBSTR(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc," & _
                                 "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_1),im.diff_1,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_2), " & _
                                 "im.diff_2,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group=im.diff_3),im.diff_3,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_4 " & _
                                 "),im.diff_4),'NONE') size_cd,im.dept,im.class,cl.class_name,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind,clear_mkdn_index,rfr.clearance_id," & _
                                 " rfr.clearance_display_id,isp.supplier,sp.sup_name FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp," & _
                                 " sups sp  WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rzl.zone_id=rfr.location AND rfr.zone_node_type=1 AND rfr.clear_mkdn_index =1 " & _
                                 " AND rfr.clear_start_ind  =1 AND rfr.clearance_id=rcl.clearance_id AND rcl.reset_date IS NULL " & whereBegDt & " AND im.dept=cl.dept AND im.class=cl.class " & _
                                 " AND im.item=isp.item AND isp.primary_supp_ind='Y' AND isp.supplier=sp.supplier),max_date AS (SELECT im.item,rzl.zone_id,rzl.location,MAX(rfr.action_date) action_date " & _
                                 " FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,mrb WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rzl.zone_id=rfr.location " & _
                                 " AND mrb.item=im.item AND mrb.location=rzl.location AND rfr.action_date<mrb.action_date AND rfr.zone_node_type=1 GROUP BY im.item,rzl.zone_id,rzl.location),prev_retail AS " & _
                                 " (SELECT md.item,md.location,rfr.selling_retail FROM rpm_future_retail rfr,mrb,max_date md WHERE rfr.item=mrb.item_cd AND rfr.location=mrb.zone_id " & _
                                 " AND rfr.action_date=md.action_date AND md.item=mrb.item AND md.location=mrb.location AND rfr.zone_node_type= 1) " & _
                                 " SELECT  /*+parallel(ils,4)*/ mrb.item AS sku_num,mrb.item_cd AS itm_cd,mrb.clear_retail ret_prc, mrb.clearance_display_id pc_num,mrb.action_date AS beg_dt,'MD' AS pc_tp, " & _
                                 " pr.selling_retail prev_perm_ret,to_number(mrb.location) store_cd,ils.stock_on_hand oh_qty,mrb.size_cd,lpad(mrb.supplier,7) ve_cd,mrb.item_desc,mrb.dept AS dept_cd,mrb.class AS class_cd, " & _
                                 " mrb.class_name AS class_desc,mrb.sup_name as VE_name FROM mrb,item_loc_soh ils,v_deps vd,prev_retail pr " & _
                                 " WHERE(pr.item = mrb.item And pr.location = mrb.location And ils.item = mrb.item And ils.loc = mrb.location And ils.stock_on_hand > 0 And mrb.dept = vd.dept)" & _
                                 " AND NOT EXISTS (SELECT 1 FROM uda_item_lov uil WHERE uil.item=mrb.item AND uil.uda_id = '9997') " & _
                                 whereSql & prcSql & _
                                 " order by mrb.location,mrb.dept,mrb.class, mrb.clear_retail,mrb.item_cd,mrb.item"


                    Case MDReportType.Further
                        strSql = "with mrb as ((select /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ im.item,rfr.item item_cd,nvl(substr(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
                             "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1), im.diff_1,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 " & _
                            "), im.diff_2,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3),im.diff_3,(select diff_type from v_diff_id_group_type where id_group = im.diff_4" & _
                            "), im.diff_4),'NONE') size_cd,im.dept,im.class,cl.class_name,'MD' prc_tp,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind,clear_mkdn_index," & _
                            " rfr.clearance_id,rfr.clearance_display_id,isp.supplier,sp.sup_name FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl," & _
                            " item_supplier isp,sups sp WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level AND rzl.zone_id=rfr.location and rfr.zone_node_type=1 and rfr.clear_mkdn_index>1" & _
                            " and rfr.clear_start_ind>=1 and rfr.clearance_id=rcl.clearance_id and rcl.reset_date is null " & whereBegDt & "  and im.dept=cl.dept and im.class=cl.class" & _
                            " and im.item=isp.item and isp.primary_supp_ind='Y' and isp.supplier=sp.supplier) union " & _
                            " (select /*+parallel (rfr,4)*/  im.item,rfr.item item_cd,nvl(substr(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc," & _
                            " NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1),im.diff_1,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 " & _
                            " ), im.diff_2,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3), im.diff_3,(select diff_type from v_diff_id_group_type where id_group = im.diff_4 " & _
                            " ), im.diff_4),'NONE') size_cd,im.dept,im.class,cl.class_name,'MD' prc_tp,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind,clear_mkdn_index," & _
                            " rfr.clearance_id,rfr.clearance_display_id,isp.supplier,sp.sup_name FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp," & _
                            " sups sp,uda_item_lov uil WHERE rfr.item=im.item_parent AND im.item_level= im.tran_level AND rzl.zone_id=rfr.location AND rfr.zone_node_type=1 and rfr.clear_mkdn_index=1 " & _
                            " and rfr.clear_start_ind =1 and rfr.clearance_id =rcl.clearance_id and rcl.reset_date is null  " & whereBegDt & "  and uil.item = im.item and uil.uda_id = '9997'" & _
                            " and im.dept=cl.dept and im.class=cl.class and im.item = isp.item and isp.primary_supp_ind = 'Y' and isp.supplier = sp.supplier)),max_date as (SELECT im.item,rzl.zone_id," & _
                            " rzl.location,max(rfr.action_date) action_date FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,mrb WHERE rfr.item=im.item_parent AND im.item_level=im.tran_level" & _
                            " and rzl.zone_id=rfr.location and mrb.item=im.item and mrb.location=rzl.location and rfr.action_date < mrb.action_date AND rfr.zone_node_type=1" & _
                            " group by im.item,rzl.zone_id,rzl.location),prev_retail as (select md.item, md.location, rfr.selling_retail from rpm_future_retail rfr , mrb, max_date md" & _
                            " where rfr.item=mrb.item_cd AND rfr.location= mrb.zone_id AND rfr.action_date = md.action_date and md.item=mrb.item AND md.location=mrb.location AND rfr.zone_node_type= 1)" & _
                            " SELECT  /*+parallel(ils,4)*/ mrb.item sku_num,mrb.item_cd itm_cd,mrb.clear_retail ret_prc,mrb.clearance_display_id pc_num,mrb.action_date beg_dt,mrb.prc_tp pc_tp,pr.selling_retail prev_perm_ret, " & _
                            " to_number(mrb.location) store_cd,ils.stock_on_hand oh_qty,mrb.size_cd,lpad(mrb.supplier,7) ve_cd,mrb.item_desc,mrb.dept dept_cd,mrb.class class_cd,mrb.class_name class_desc,mrb.sup_name ve_name " & _
                            " from mrb,prev_retail pr,item_loc_soh ils,v_deps vd WHERE pr.item=mrb.item AND pr.location=mrb.location and ils.item=mrb.item and ils.loc=mrb.location and mrb.dept=vd.dept and ils.stock_on_hand>0  " & _
                            whereSql & prcSql & _
                            " order by mrb.location,mrb.dept,mrb.class, mrb.clear_retail,mrb.item_cd,mrb.item"


                    Case MDReportType.MOS
                        strSql = "select im.item as sku_num,im.item_parent as itm_cd,'CHARITY' as ret_prc,'' as pc_num," & _
                                 "ud.uda_date beg_dt,'MOS' pc_tp,il.selling_unit_retail as prev_perm_ret," & _
                                 "to_number(il.loc) as store_cd,ils.stock_on_hand as oh_qty," & _
                                 "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1" & _
                                 "), im.diff_1,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2" & _
                                 "), im.diff_2,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3" & _
                                 "), im.diff_3,(select diff_type from v_diff_id_group_type where id_group = im.diff_4" & _
                                 "), im.diff_4),'NONE') size_cd,lpad(isp.supplier,7) as ve_cd," & _
                                 "nvl(substr(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc," & _
                                 "im.dept as dept_cd,im.class as class_cd,CL.CLASS_NAME as class_desc,sp.sup_name as ve_name " & _
                                 " from item_master im, item_loc il, uda_item_date ud, item_supplier isp, class cl, sups sp, item_loc_soh ils,v_deps vd " & _
                                 " where im.item = il.item AND IM.ITEM = UD.ITEM and ud.uda_id = '56' and im.item_level = im.tran_level and IM.DEPT = vd.dept " & _
                                 " and im.dept = cl.dept and im.class = cl.class and im.item = isp.item and sp.supplier = isp.supplier " & _
                                 whereSql & _
                                 " and ils.item = il.item AND IL.LOC = ILS.LOC  " & whereBegDt & " and isp.primary_supp_ind = 'Y' " & _
                                 " order by to_number(il.loc),im.dept,im.class, il.selling_unit_retail,im.item,im.item_parent"

                    Case MDReportType.MDAll


                        strSql = "with mrb as (select /*+parallel (rfr,4) use_hash(rfr,im,isp)*/ im.item,rfr.item item_cd,nvl(substr(short_desc, 1, instr(item_desc, ':', 1, 1)-1),im.short_desc) item_desc, " & _
                            "NVL(DECODE ('SIZE',(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_1), im.diff_1,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_2 " & _
                            "), im.diff_2,(SELECT diff_type FROM v_diff_id_group_type WHERE id_group = im.diff_3), im.diff_3,(select diff_type from v_diff_id_group_type where id_group = im.diff_4" & _
                            "), im.diff_4),'NONE') size_cd,im.dept,im.class,cl.class_name,rzl.zone_id,rzl.location,rfr.action_date,clear_retail,clear_start_ind,clear_mkdn_index,rfr.clearance_id," & _
                            " rfr.clearance_display_id,isp.supplier,sp.sup_name FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,rpm_clearance rcl,class cl,item_supplier isp,sups sp" & _
                            " WHERE(rfr.item = im.item_parent And im.item_level = im.tran_level And rzl.zone_id = rfr.location And rfr.zone_node_type = 1 And rfr.clear_mkdn_index > 0)" & _
                            " and rfr.clearance_id = rcl.clearance_id and rcl.reset_date is null  " & whereBegDt & "  and im.dept = cl.dept" & _
                            " and im.class = cl.class and im.item = isp.item and isp.primary_supp_ind = 'Y' and isp.supplier = sp.supplier)," & _
                            " max_date as (SELECT im.item,rzl.zone_id,rzl.location,max(rfr.action_date) action_date FROM rpm_future_retail rfr,item_master im,rpm_zone_location rzl,mrb " & _
                            " WHERE(rfr.item = im.item_parent And im.item_level = im.tran_level And rzl.zone_id = rfr.location And mrb.item = im.item)" & _
                            " and mrb.location= rzl.location and rfr.action_date < mrb.action_date AND rfr.zone_node_type= 1 " & _
                            " group by im.item,rzl.zone_id,rzl.location),prev_retail as (select md.item, md.location, rfr.selling_retail from rpm_future_retail rfr , mrb, max_date md" & _
                            " where rfr.item= mrb.item_cd AND rfr.location= mrb.zone_id AND rfr.action_date = md.action_date and md.item= mrb.item AND md.location= mrb.location AND rfr.zone_node_type= 1)" & _
                            " SELECT /*+parallel(ils,4)*/ mrb.item sku_num,mrb.item_cd itm_cd,mrb.clear_retail ret_prc,mrb.clearance_display_id pc_num," & _
                            " mrb.action_date beg_dt,'MD' pc_tp,pr.selling_retail prev_perm_ret,to_number(mrb.location) store_cd, ils.stock_on_hand oh_qty," & _
                            " mrb.size_cd,lpad(mrb.supplier,7) ve_cd,mrb.item_desc,mrb.dept dept_cd,mrb.class class_cd,mrb.class_name class_desc,mrb.sup_name as VE_name  " & _
                            " FROM mrb,item_loc_soh ils,v_deps vd,prev_retail pr WHERE pr.item= mrb.item AND pr.location= mrb.location and ils.item= mrb.item and ils.stock_on_hand>0 " & _
                            " and ils.loc= mrb.location and mrb.dept = vd.dept " & _
                            whereSql & prcSql & _
                            " order by mrb.location,mrb.dept,mrb.class, mrb.clear_retail,mrb.item_cd,mrb.item"

                End Select

                'strSql = "select /*+ ALL_ROWS*/ /*Sri Bajjuri - Report distro MD reports*/ gm_prc.sku_num, gm_prc.itm_cd, gm_prc.ret_prc, gm_prc.pc_num, gm_prc.beg_dt, gm_prc.pc_tp, gm_prc.prev_perm_ret, " & _
                '        "to_number(gm_inv_loc_v.store_cd) as store_cd, gm_inv_loc_v.oh_qty, " & _
                '        "gm_sku.size_cd, lpad(gm_itm.ve_cd,7) as ve_cd, gm_itm.item_desc as item_desc, gm_itm.dept_cd, gm_itm.class_cd, class.des as class_desc, ap.ve.ve_name " & _
                '        "from gm_prc " & _
                '        "inner join gm_inv_loc_v on gm_inv_loc_v.sku_num = gm_prc.sku_num and gm_inv_loc_v.oh_qty > 0 and loc_cd <> 'HOLD' " & _
                '        "inner join gm_sku on gm_sku.sku_num = gm_prc.sku_num " & _
                '        " inner join (select i.itm_cd, i.ve_cd,i.dept_cd,i.class_cd, NVL(wd.web_des, i.des1) as item_desc FROM GM_ITM i " & _
                '        "left join RPT_SERVICES_GERS_INQ.bm_itm_wdes wd on i.itm_cd = wd.itm_cd) gm_itm on gm_itm.itm_cd = gm_prc.itm_cd " & _
                '        "inner join class on gm_itm.class_cd = class.class_cd " & _
                '        "inner join dept on gm_itm.dept_cd = dept.dept_cd " & _
                '        "inner join ap.ve on ap.ve.ve_cd = gm_itm.ve_cd " & _
                '        whereSql & "and pc_tp ='" & pc_tp & "' " & prcSql & _
                '        "and gm_prc.prc_zone_cd = (select prc_zone_cd from store where store_cd = 9999) " & _
                '        "and gm_prc.prc_grp_cd = (select prc_grp_cd from store where store_cd = 9999) " & _
                '        "order by gm_inv_loc_v.store_cd, gm_itm.dept_cd, gm_itm.class_cd, gm_prc.ret_prc desc, gm_prc.itm_cd, gm_prc.sku_num"

                mdDs = Cn.ExecuteQuery(EnumRSTypes.DataSet, strSql)
                'Throw New Exception(strSql)
                Cn.CloseConnection()
                If mdDs.Tables(0).Rows.Count = 0 Then
                    Throw New Exception("MDReport:No records found")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class ' END CLASS DEFINITION ReportManager

End Namespace ' HotTopic.RD.Reports

