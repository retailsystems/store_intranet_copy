' Static Model

Namespace HotTopic.RD.Entity


    Public Class JobCodeOwner
        Inherits CompanyOwner

        Private _jobCode As String

        Public Property JobCode() As String
            Get
                Return _jobCode
            End Get

            Set(ByVal Value As String)
                _jobCode = Value
            End Set
        End Property

    End Class ' END CLASS DEFINITION JobCodeOwner

End Namespace ' HotTopic.RD.Entity

