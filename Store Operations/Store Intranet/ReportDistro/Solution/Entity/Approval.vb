' Static Model

Namespace HotTopic.RD.Entity


    Public Class Approval

        Private _approver As HotTopic.RD.Entity.Approver
        Private _rptSubmissionId As Int32
        Private _comment As String
        Private _status As Int16

        Public Sub New(ByVal rptSubmissionId As Int32, ByVal employeeId As Int32)
            _rptSubmissionId = rptSubmissionId
            _approver = New Approver(employeeId)
        End Sub

        Public ReadOnly Property ReportApprover() As Approver
            Get
                Return _approver
            End Get

        End Property

        Public ReadOnly Property ReportSubmissionId() As Long
            Get
                Return _rptSubmissionId
            End Get

        End Property

        Public Property Comment() As String
            Get
                Return _comment
            End Get

            Set(ByVal Value As String)
                _comment = Value
            End Set
        End Property

        Public Property Status() As Integer
            Get
                Return _status
            End Get

            Set(ByVal Value As Integer)
                _status = Value
            End Set
        End Property

    End Class ' END CLASS DEFINITION Approval

End Namespace ' HotTopic.RD.Entity

