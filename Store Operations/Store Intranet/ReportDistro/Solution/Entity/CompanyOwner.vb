' Static Model

Namespace HotTopic.RD.Entity


    Public Class CompanyOwner

        Private _companyId As Int32
        Private _companyName As String
        Private _cdStoresFlag As Boolean

        Public Property CompanyId() As Long
            Get
                Return _companyId
            End Get

            Set(ByVal Value As Long)
                _companyId = Value
            End Set
        End Property

        Public Property CompanyName() As String
            Get
                Return _companyName
            End Get

            Set(ByVal Value As String)
                _companyName = Value
            End Set
        End Property

        Public Property CDStoresFlag() As Boolean
            Get
                Return _cdStoresFlag
            End Get

            Set(ByVal Value As Boolean)
                _cdStoresFlag = Value
            End Set
        End Property

    End Class ' END CLASS DEFINITION CompanyOwner

End Namespace ' HotTopic.RD.Entity

