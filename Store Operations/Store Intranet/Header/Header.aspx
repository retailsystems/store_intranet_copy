<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Header.aspx.vb" Inherits="Header.frmHeader" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Header</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
    <meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link id="linkStyles" href="" type="text/css" rel="stylesheet" runat="server" />
    <link id="Link1" href="/Global/bootstrap.min.css" type="text/css" rel="stylesheet" runat="server" />
    <link id="Link2" href="/Global/bootstrap-theme.min.css" type="text/css" rel="stylesheet" runat="server" />
    <!--[if lt IE 9]>
      <script src="/Global/js/html5shiv.min.js"></script>
      <script src="/Global/js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="Header">
    <form id="frmHeader" method="post" runat="server">
        <!-- Fixed navbar -->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation"><!--class="navbar navbar-default navbar-fixed-top"-->
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span
                            class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" target="_parent" href="/Home/Home.aspx" >
                        <asp:image imageurl="" alternatetext="" id="logo" runat="server"/>Intranet</a>
                </div>
                <div class="hdr_sec">
                    <asp:Label ID="lblTitle" runat="server">Title</asp:Label>
                    <asp:Label ID="lblVersion" runat="server"></asp:Label>
                </div>
                <div class="navbar-collapse collapse">
                    <div class="text-right welcomemsg">
                        Welcome <b>
                            <asp:Label ID="lblUser" runat="server">User</asp:Label>&nbsp;
							<asp:Label ID="lblStore" runat="server"></asp:Label></b>&nbsp;
							<asp:HyperLink ID="hypLogOff" runat="server" NavigateUrl="/home/home.aspx?mode=logoff" Target="_top">Log Off</asp:HyperLink>&nbsp;
                    </div>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
        <!-- End Fixed Header  -->
    </form>
    <script src="/Global/js/jquery.min.js"></script>
    <script src="/Global/js/bootstrap.min.js"></script>
</body>
</html>
