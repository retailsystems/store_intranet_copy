Option Explicit On 

Partial Class frmHeader

    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strMode As String
        Dim strStore As String
        Dim strTitle As String
        Dim strUser As String
        Dim strVersion As String

        strMode = Request("mode")
        strStore = Request("location")
        strUser = Request("user")
        strTitle = Request("title")
        strVersion = Request("Version")

        If strMode = "" Then
            strMode = "HotTopic"
        End If

        If strStore = "" Then
            strStore = "&nbsp;"
        End If

        If strUser = "" Then
            strUser = "&nbsp;"
            hypLogOff.Text = ""
        End If

        If strTitle = "" Then
            strTitle = "&nbsp;"
        End If

        lblVersion.Text = Replace(strVersion, " ", "&nbsp;")

		linkStyles.Attributes.Add("href", ConfigurationSettings.AppSettings(strMode & "_Stylesheet"))
		logo.ImageUrl = ConfigurationSettings.AppSettings(strMode & "_Logo")
		logo.AlternateText = strMode

		lblUser.Text = Replace(strUser, " ", "&nbsp;")
        hypLogOff.NavigateUrl = ConfigurationSettings.AppSettings(strMode & "_Home") & "?Mode=LOGOFF"
        lblStore.Text = Replace(strStore, " ", "&nbsp;")

        If UCase(ConfigurationSettings.AppSettings("Test")) = "TRUE" Then
            lblTitle.Text = Replace("THIS IS THE TEST WEBSITE -- " & strTitle, " ", "&nbsp;")
        Else
            lblTitle.Text = Replace(strTitle, " ", "&nbsp;")
        End If

    End Sub

End Class
