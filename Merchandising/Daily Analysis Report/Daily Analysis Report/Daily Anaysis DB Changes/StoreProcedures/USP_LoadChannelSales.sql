﻿/****** Object:  StoredProcedure [dbo].[USP_LoadChannelSales]    Script Date: 01/16/2012 13:22:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_LoadChannelSales]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_LoadChannelSales]
GO

/****** Object:  StoredProcedure [dbo].[USP_LoadChannelSales]    Script Date: 01/16/2012 13:22:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[USP_LoadChannelSales] 
AS 

--Delete old records
DELETE FROM Report_Sales
WHERE Trn_Date in (select distinct tran_date from dbo.ReSA_SALES)
AND Channel_Key <> 0

--Channel Sales and Units
INSERT INTO dbo.Report_Sales (Trn_Date,Division,Dept_CD,Channel_Key,Reg_Sales,CLR_Sales,Reg_Units,CLR_Units)
SELECT  RESULT.Trn_Date,
		RESULT.Division,
		RESULT.Dept_CD,
		RESULT.Channel_Key,
		SUM(RESULT.Reg_Sales) AS Reg_Sales,
		SUM(RESULT.CLR_Sales) AS CLR_Sales,
		SUM(RESULT.Reg_Units) AS Reg_Units,
		SUM(RESULT.CLR_Units) AS CLR_Units
FROM
(
	SELECT	RESULT.Trn_Date,
			RESULT.Division,
			RESULT.Dept_CD,
			RESULT.Channel_Key,
			CASE RESULT.Prc_Stat WHEN 'REG' THEN SUM(RESULT.Amt)ELSE NULL END AS Reg_Sales,
			CASE RESULT.Prc_Stat WHEN 'MD' THEN SUM(RESULT.Amt) ELSE NULL END AS CLR_Sales,
			CASE RESULT.Prc_Stat WHEN 'REG' THEN SUM(RESULT.Qty) ELSE NULL END AS Reg_Units,
			CASE RESULT.Prc_Stat WHEN 'MD' THEN SUM(RESULT.Qty) ELSE NULL END AS CLR_Units
	FROM
	(
		SELECT O.Trn_Dt As Trn_Date,	
			O.Division,
			O.Dept_CD,
			O.Channel_Key,
			O.Fullorderno,
			O.Line_No,
			O.LN_TP,
			O.Qty,
			O.Ext_Prc,
			EMP.EDisc_Rtl,
			O.Prc_Stat,
			ISNULL(O.Ext_Prc, 0) + ISNULL(EMP.EDisc_Rtl, 0) As Amt
		FROM
		(
			SELECT	OD.Trn_Dt,
					S.DIV_CD AS Division,
					S.Dept_CD,
					OH.Channel_Key,
					OH.Fullorderno,
					OD.LINE_NO,
					OD.LN_TP,
					OD.QTY,
					OD.EXT_PRC,
					OD.PRC_STAT
			FROM [ChannelDM].[dbo].[Fact_ChannelOrderHdr] OH,
					[ChannelDM].[dbo].[Fact_ChannelOrderDtl] OD,
					[ChannelDM].[dbo].[DimSku] S
			WHERE OD.Trn_DT in (select distinct tran_date from dbo.ReSA_SALES)
			AND OH.Fullorderno = OD.Fullorderno
			AND OD.SKU_Num = S.SKU_Num
			AND OH.Division in (1, 5, 8)
		)O
		LEFT OUTER JOIN  (
			SELECT ED.Trn_Dt,
					ED.Fullorderno,
					ED.LINE_NO,
					ED.LN_TP,
					ED.EDisc_Rtl
			FROM [ChannelDM].[dbo].[Fact_EmployeeOrderDtl] ED
			WHERE ED.Trn_DT in (select distinct tran_date from dbo.ReSA_SALES)
			AND ED.LN_TP IN ('SAL','RET')
			AND ED.QTY <> 0 
			AND ED.EXT_PRC <> 0
		) EMP
		ON O.Trn_DT = EMP.Trn_DT
		AND O.Fullorderno = EMP.Fullorderno
		AND O.LINE_NO = EMP.LINE_NO
		AND O.LN_TP = EMP.LN_TP
	)RESULT
	GROUP BY RESULT.Trn_Date, RESULT.Division, RESULT.Dept_CD, RESULT.Channel_Key, RESULT.PRC_STAT
)RESULT
GROUP BY RESULT.Trn_Date, RESULT.Division, RESULT.Channel_Key, RESULT.DEPT_CD


--Transaction Count
UPDATE	Report_Sales
SET		Trans_Count = SUBTBL.Trans_Count
FROM dbo.Report_Sales RS
INNER MERGE JOIN
(
	SELECT	RESULT.Trn_Date,
			RESULT.Division,
			RESULT.Channel_Key,
			COUNT(RESULT.Orderno) AS Trans_Count
	FROM
	(
			SELECT	DISTINCT
					OD.Trn_Dt AS Trn_Date,
					OH.Division,
					OH.Channel_Key,
					OH.Orderno
			FROM [ChannelDM].[dbo].[Fact_ChannelOrderHdr] OH,
					[ChannelDM].[dbo].[Fact_ChannelOrderDtl] OD
			WHERE OD.Trn_DT IN(select distinct tran_date from dbo.ReSA_SALES)
			AND OH.Fullorderno = OD.Fullorderno
			AND OH.Division in (1, 5, 8)
	)RESULT
	GROUP BY RESULT.Trn_Date, RESULT.Division, RESULT.Channel_Key
)SUBTBL
ON RS.Trn_Date = SUBTBL.Trn_Date
AND RS.Division = SUBTBL.Division
AND RS.Channel_Key = SUBTBL.Channel_Key
AND ISNULL(RS.Trans_Count,0) <> SUBTBL.Trans_Count

--Department Transaction Count
UPDATE	Report_Sales
SET		Dept_Trans_Count = SUBTBL.Dept_Trans_Count
FROM dbo.Report_Sales RS
INNER MERGE JOIN
(
	SELECT RESULT.Trn_Date,
			RESULT.Division,
			RESULT.Dept_CD,
			RESULT.Channel_Key,
			COUNT(RESULT.Orderno) As Dept_Trans_Count
	FROM
	(
			SELECT	DISTINCT
					OD.Trn_Dt AS Trn_Date,
					OH.Division,
					S.Dept_CD,
					OH.Channel_Key,
					OH.Orderno
			FROM [ChannelDM].[dbo].[Fact_ChannelOrderHdr] OH,
					[ChannelDM].[dbo].[Fact_ChannelOrderDtl] OD,
					[ChannelDM].[dbo].[DimSku] S
			WHERE OD.Trn_DT IN (select distinct tran_date from dbo.ReSA_SALES)
			AND OH.Fullorderno = OD.Fullorderno
			AND OD.SKU_Num = S.SKU_Num
			AND OH.Division in (1, 5, 8)
	)RESULT
	GROUP BY RESULT.Trn_Date, RESULT.Division, RESULT.Dept_CD, RESULT.Channel_Key
)SUBTBL
ON RS.Trn_Date = SUBTBL.Trn_Date
AND RS.Division = SUBTBL.Division
AND RS.Dept_CD = SUBTBL.Dept_CD
AND RS.Channel_Key = SUBTBL.Channel_Key
AND ISNULL(RS.Dept_Trans_Count,0) <> SUBTBL.Dept_Trans_Count

