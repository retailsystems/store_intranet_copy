﻿/****** Object:  StoredProcedure [dbo].[USP_LoadStoreSales]    Script Date: 01/16/2012 13:46:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_LoadStoreSales]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_LoadStoreSales]
GO
/****** Object:  StoredProcedure [dbo].[USP_LoadStoreSales]    Script Date: 01/16/2012 13:46:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[USP_LoadStoreSales] 
AS

--Delete old records
DELETE FROM Report_Sales
WHERE Trn_Date in (select distinct trn_dt from dbo.GERS_POS_TRN_LN)
AND Channel_Key = 0

truncate table dbo.Stage_Price;
INSERT INTO dbo.Stage_Price (TRN_DT,DIV_CD,DEPT_CD,SKU_NUM,PRC_STAT)
SELECT TRN_DT,RTRIM(S.DIV_CD),	RTRIM(S.DEPT_CD), RTRIM(A.SKU_NUM),[ChannelDM].[dbo].[fnGetPriceStatBySKU] (A.SKU_NUM, A.TRN_DT) as Prc_Stat
	FROM (SELECT DISTINCT TRN_DT, SKU_NUM FROM dbo.GERS_POS_TRN_LN WITH (NOLOCK)) A
	JOIN [ChannelDM].[dbo].[DimSku] S WITH (NOLOCK) ON A.SKU_NUM = S.SKU_NUM;
	
--Update Sales and Units
INSERT INTO dbo.Report_Sales (Trn_Date,Division,Dept_CD,Channel_Key,Reg_Sales,CLR_Sales,Reg_Units,CLR_Units)
SELECT RESULT.Trn_Date,
	RESULT.Division,
	RESULT.Dept_CD,
	0 AS Channel_Key,
	SUM(RESULT.Reg_Sales) AS Reg_Sales,
	SUM(RESULT.CLR_Sales) AS CLR_Sales,
	SUM(RESULT.Reg_Units) AS Reg_Units,
	SUM(RESULT.CLR_Units) AS CLR_Units
FROM
(
	SELECT	A.TRN_DT AS Trn_Date,
			A.Division,
			A.DEPT_CD,
			CASE A.Prc_Stat WHEN 'REG' THEN SUM(A.TOT_AMT)ELSE NULL END AS Reg_Sales,
			CASE A.Prc_Stat WHEN 'MD' THEN SUM(A.TOT_AMT) ELSE NULL END AS CLR_Sales,
			CASE A.Prc_Stat WHEN 'REG' THEN SUM(A.TOT_QTY) ELSE NULL END AS Reg_Units,
			CASE A.Prc_Stat WHEN 'MD' THEN SUM(A.TOT_QTY) ELSE NULL END AS CLR_Units
	FROM
	(
		--Sales
		SELECT SAL.TRN_DT,
				SAL.Division,
				SAL.DEPT_CD, 
				SAL.STORE_CD, 
				SAL.LN_TP, 
				SAL.TERM_NUM, 
				SAL.TRN_NUM, 
				SAL.SKU_NUM, 
				SAL.VOID, 
				SAL.TOT_QTY, 
				TOT_AMT + ISNULL(EMD_AMT, 0) AS TOT_AMT,
				S.Prc_Stat
				--[ChannelDM].[dbo].[fnGetPriceStatBySKU] (SAL.SKU_NUM, SAL.TRN_DT) as Prc_Stat
		FROM  
		( 
			SELECT P.TRN_DT,
					S.DIV_CD AS Division,
					S.DEPT_CD,
					P.STORE_CD, 
					P.LN_TP, 
					P.TERM_NUM, 
					P.TRN_NUM, 
					S.SKU_NUM, 
					P.VOID, 
					SUM(CASE P.VOID WHEN 'P' THEN -1 ELSE P.QTY END) AS TOT_QTY,
					SUM((P.EFF_AMT) * P.QTY) AS TOT_AMT
			FROM dbo.GERS_POS_TRN_LN P,
				[ChannelDM].[dbo].[DimSku] S
			WHERE P.LN_TP = 'SAL'
			AND P.STORE_CD NOT IN (4490, 5990, 8890)
			AND P.SKU_NUM = S.SKU_NUM
			AND ISNULL(P.VOID , ' ') <> 'T' 
			GROUP BY P.TRN_DT, S.DIV_CD, S.DEPT_CD, P.STORE_CD, P.TERM_NUM, P.TRN_NUM, S.SKU_NUM, P.LN_TP, P.VOID 
		)SAL 
		JOIN dbo.Stage_Price S ON SAL.SKU_NUM = S.SKU_NUM AND SAL.TRN_DT = S.TRN_DT
		LEFT OUTER JOIN( 
			SELECT P.TRN_DT, 
					P.STORE_CD, 
					P.TERM_NUM, 
					P.TRN_NUM, 
					P.SKU_NUM, 
					SUM(P.AMT * -1) AS EMD_AMT 
			FROM dbo.GERS_POS_TRN_LN P  
			WHERE P.STORE_CD NOT IN (4490, 5990, 8890)
			AND P.AMT < 0  
			AND P.LN_TP in ('EMD', 'TED')
			AND ISNULL(P.VOID , ' ') <> 'T'  
			GROUP BY P.TRN_DT, P.STORE_CD, P.TERM_NUM, P.TRN_NUM, P.SKU_NUM, P.LN_TP 
		)EMD_SAL 
		ON  SAL.TRN_DT = EMD_SAL.TRN_DT 
		AND SAL.STORE_CD = EMD_SAL.STORE_CD 
		AND SAL.TERM_NUM = EMD_SAL.TERM_NUM 
		AND SAL.TRN_NUM = EMD_SAL.TRN_NUM 
		AND SAL.SKU_NUM = EMD_SAL.SKU_NUM 
		UNION ALL
		--Returns
		SELECT RET.TRN_DT, 
				RET.Division,
				RET.DEPT_CD,
				RET.STORE_CD, 
				RET.LN_TP, 
				RET.TERM_NUM, 
				RET.TRN_NUM, 
				RET.SKU_NUM, 
				RET.VOID, 
				RET.TOT_QTY, 
				TOT_AMT + ISNULL(EMD_AMT,0) AS TOT_AMT,
				S.Prc_Stat
				--[ChannelDM].[dbo].[fnGetPriceStatBySKU] (RET.SKU_NUM, RET.TRN_DT) as Prc_Stat
		FROM  
		( 
			SELECT P.TRN_DT,
					S.DIV_CD AS Division,
					S.DEPT_CD,
					P.STORE_CD, 
					P.LN_TP, 
					P.TERM_NUM, 
					P.TRN_NUM, 
					P.SKU_NUM, 
					P.VOID, 
					SUM(CASE P.VOID WHEN 'P'THEN 1 ELSE -1 * P.QTY END) AS TOT_QTY, 
					SUM((P.EFF_AMT) * P.QTY) AS TOT_AMT
			FROM dbo.GERS_POS_TRN_LN P,
				[ChannelDM].[dbo].[DimSku] S
			WHERE P.LN_TP = 'RET'
			AND P.STORE_CD NOT IN (4490, 5990, 8890)
			AND P.SKU_NUM = S.SKU_NUM  
			AND ISNULL(P.VOID , ' ') <> 'T' 
			GROUP BY P.TRN_DT, S.DIV_CD, S.DEPT_CD, P.STORE_CD, P.TERM_NUM, P.TRN_NUM, P.SKU_NUM, P.LN_TP, P.VOID 
		)RET 
		JOIN dbo.Stage_Price S ON RET.SKU_NUM = S.SKU_NUM AND RET.TRN_DT = S.TRN_DT
		LEFT OUTER JOIN( 
			SELECT 
			   P.TRN_DT, 
			   P.STORE_CD, 
			   P.TERM_NUM, 
			   P.TRN_NUM, 
			   P.SKU_NUM, 
			   SUM(P.AMT * -1) AS EMD_AMT 
			FROM dbo.GERS_POS_TRN_LN P 
			WHERE P.STORE_CD NOT IN (4490, 5990, 8890)
			AND P.AMT > 0  
			AND P.LN_TP in ('EMD', 'TED')
			AND ISNULL(P.VOID , ' ') <> 'T' 
			GROUP BY P.TRN_DT, P.STORE_CD, P.TERM_NUM, P.TRN_NUM, P.SKU_NUM, P.LN_TP 
		)EMD_RET 
		ON  RET.TRN_DT = EMD_RET.TRN_DT 
		AND RET.STORE_CD = EMD_RET.STORE_CD 
		AND RET.TERM_NUM = EMD_RET.TERM_NUM 
		AND RET.TRN_NUM = EMD_RET.TRN_NUM 
		AND RET.SKU_NUM = EMD_RET.SKU_NUM
		UNION ALL
		--Gift Cards
		SELECT	P.TRN_DT,
				CASE
					WHEN CAST(P.STORE_CD AS INT) >= 1 and CAST(P.STORE_CD AS INT) <= 4490 THEN 1
					WHEN CAST(P.STORE_CD AS INT) >= 5000 and CAST(P.STORE_CD AS INT) <= 5990 THEN 5
					ELSE 8
				END AS DIVISION,
				CASE
					WHEN CAST(P.STORE_CD AS INT) >= 1 and CAST(P.STORE_CD AS INT) <= 4490 THEN '0099'
					WHEN CAST(P.STORE_CD AS INT) >= 5000 and CAST(P.STORE_CD AS INT) <= 5990 THEN '0098'
					ELSE '0088'
				END AS DEPT,
				P.STORE_CD,
				P.LN_TP,
				P.TERM_NUM,
				P.TRN_NUM,
				P.SKU_NUM,
				P.VOID,
				CASE P.VOID
					WHEN 'P' THEN -1 * P.QTY 
					ELSE P.QTY
				END As TOT_QTY,
				P.EFF_AMT As TOT_AMT,
				'REG' As Prc_Stat
		FROM dbo.GERS_POS_TRN_LN P
		WHERE P.LN_TP IN ('GCA', 'GAN')
		AND P.STORE_CD NOT IN (4490, 5990, 8890)
	)A
	GROUP BY A.TRN_DT, A.Division, A.DEPT_CD, A.Prc_Stat
)RESULT
GROUP BY RESULT.Trn_Date, RESULT.Division, RESULT.DEPT_CD


--Transaction Count
UPDATE	Report_Sales
SET		Trans_Count = A.Trans_Count_By_Store 
FROM 
 Report_Sales RS 
 INNER MERGE JOIN
 (SELECT P.TRN_DT,P.Division
        ,COUNT(TRN_NUM) as Trans_Count_By_Store         
              FROM 
              (select P.TRN_DT,P.STORE_CD,P.TRN_NUM,P.TERM_NUM,L.Div_Code As Division
              FROM
              dbo.GERS_POS_TRN_LN P WITH (NOLOCK)                  
              INNER JOIN [HTmdm].[dbo].[DimLocation] L WITH (NOLOCK) ON P.STORE_CD = L.Store_ID
              WHERE P.LN_TP IN ('SAL', 'RET', 'GCA', 'GAN')
              AND P.STORE_CD NOT IN (4490, 5990, 8890)              
              AND (P.VOID IS NULL OR P.VOID = 'P')
              GROUP BY P.TRN_DT,P.STORE_CD,P.TRN_NUM,P.TERM_NUM,L.Div_Code
              ) P
         GROUP BY P.TRN_DT,P.Division
    ) A ON RS.Trn_Date = A.TRN_DT AND RS.Division = A.Division
    WHERE RS.channel_Key = 0


--Department Transaction Count
UPDATE	Report_Sales
SET		Dept_Trans_Count = SUBTBL.Dept_Trans_Count
FROM dbo.Report_Sales RS
INNER MERGE JOIN
(
	SELECT A.TRN_DT AS Trn_Date,
			CASE
				WHEN (CAST(A.Dept_CD AS INT) >= 1 and CAST(A.Dept_CD AS INT) < 50)  or CAST(A.Dept_CD AS INT) = 97 or CAST(A.Dept_CD AS INT) = 99 THEN 1
				WHEN (CAST(A.Dept_CD AS INT) >= 50 and CAST(A.Dept_CD AS INT) < 80) or CAST(A.Dept_CD AS INT) = 98 THEN 5
				ELSE 8
			END AS DIVISION,
			A.Dept_CD,				
			COUNT(A.TRN_NUM) AS Dept_Trans_Count
	FROM
	(		
			-- Sales and Return Transactions
			SELECT	DISTINCT 
					P.TRN_DT,
					P.STORE_CD,
					S.DEPT_CD,
					P.TRN_NUM,
					P.TERM_NUM
			FROM dbo.GERS_POS_TRN_LN P,
				[ChannelDM].[dbo].[DimSku] S
			WHERE P.LN_TP IN ('SAL', 'RET')
			AND P.STORE_CD NOT IN (4490, 5990, 8890)
			AND (P.VOID IS NULL OR P.VOID = 'P')
			AND P.SKU_NUM = S.SKU_NUM
			UNION ALL
			SELECT	DISTINCT 
					P.TRN_DT,
					P.STORE_CD,
					CASE
						WHEN CAST(P.STORE_CD AS INT) >= 1 and CAST(P.STORE_CD AS INT) <= 4490 THEN '0099'
						WHEN CAST(P.STORE_CD AS INT) >= 5000 and CAST(P.STORE_CD AS INT) <= 5990 THEN '0098'
						ELSE '0088'
					END AS DEPT_CD,
					P.TRN_NUM,
					P.TERM_NUM
			FROM dbo.GERS_POS_TRN_LN P
			WHERE P.LN_TP IN ('GCA', 'GAN')
			AND P.STORE_CD NOT IN (4490, 5990, 8890)
			AND (P.VOID IS NULL OR P.VOID = 'P')
	)A
	GROUP BY A.TRN_DT, A.Dept_CD
)SUBTBL
ON RS.Trn_Date = SUBTBL.Trn_Date
AND RS.Division = SUBTBL.Division
AND RS.Dept_CD = SUBTBL.Dept_CD
AND RS.Channel_Key = 0
AND ISNULL(RS.Dept_Trans_Count,0) <> SUBTBL.Dept_Trans_Count
GO