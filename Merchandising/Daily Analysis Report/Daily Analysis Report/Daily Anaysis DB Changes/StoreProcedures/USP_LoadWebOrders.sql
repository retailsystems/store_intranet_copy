﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'USP_LoadWebOrders')
	BEGIN
		DROP Procedure dbo.USP_LoadWebOrders
	END
GO

CREATE PROCEDURE [dbo].[USP_LoadWebOrders] 
AS 

DELETE FROM Report_WebOrders
WHERE Entry_Date in (select distinct entrydate from dbo.ECOMETRY_ORDER_DETAIL)

--Add Sales and Units
INSERT INTO dbo.Report_WebOrders (Entry_Date,Division,Dept_CD,Channel_Key,Reg_Sales,CLR_Sales,Reg_Units,CLR_Units)
SELECT  RESULT.Entry_Date,
		RESULT.Division,
		RESULT.Dept_CD,
		RESULT.Channel_Key,
		SUM(RESULT.Reg_Sales) AS Reg_Sales,
		SUM(RESULT.CLR_Sales) AS CLR_Sales,
		SUM(RESULT.Reg_Units) AS Reg_Units,
		SUM(RESULT.CLR_Units) AS CLR_Units
FROM
(
	SELECT A.Entry_Date,
			A.Division,
			A.Dept_CD,
			A.Channel_Key,
			CASE A.Prc_Stat WHEN 'REG' THEN SUM(A.Amt)ELSE NULL END AS Reg_Sales,
			CASE A.Prc_Stat WHEN 'MD' THEN SUM(A.Amt) ELSE NULL END AS CLR_Sales,
			CASE A.Prc_Stat WHEN 'REG' THEN SUM(A.Qty) ELSE NULL END AS Reg_Units,
			CASE A.Prc_Stat WHEN 'MD' THEN SUM(A.Qty) ELSE NULL END AS CLR_Units
	FROM
	(
		SELECT E.EntryDate As Entry_Date,                                                                                       
				S.DIV_CD As Division,
				S.DEPT_CD,
				E.Channel_Key,
				E.ExtPrices,
				E.EDISC_RTL,
				ISNULL(E.ExtPrices,0) + ISNULL(E.EDISC_RTL,0) AS Amt,
				E.Qty,
				[ChannelDM].[dbo].[fnGetPriceStatBySKU] (E.Sku_Num, E.EntryDate) as Prc_Stat
		FROM dbo.ECOMETRY_ORDER_DETAIL E,
			[ChannelDM].[dbo].[DimSku] S
		WHERE E.Division in (1, 5, 8)
		AND S.SKU_NUM = E.SKU_NUM
		AND NOT (E.EDPNOSTATUS = 'C' AND E.ORDERnoSTATUS <> 'C')
	)A
	GROUP BY A.Entry_Date, A.Division, A.Channel_Key, A.DEPT_CD, A.Prc_Stat
)RESULT
GROUP BY RESULT.Entry_Date, RESULT.Division, RESULT.Channel_Key, RESULT.DEPT_CD

--Channel Order Count
UPDATE	Report_WebOrders
SET		Order_Count = SUBTBL.Order_Count
FROM dbo.Report_WebOrders WO
INNER JOIN
(

	SELECT	RESULT.Entry_Date,
			RESULT.Division,
			RESULT.Channel_Key,
			COUNT(RESULT.OrderNO) As Order_Count
	FROM
	(
		SELECT DISTINCT E.EntryDate As Entry_Date,
				S.DIV_CD AS Division,
				E.Channel_Key,
				E.OrderNO
		FROM dbo.ECOMETRY_ORDER_DETAIL E,
			[ChannelDM].[dbo].[DimSku] S
		WHERE S.SKU_NUM = E.SKU_NUM
		AND NOT (E.EDPNOSTATUS = 'C' AND E.ORDERnoSTATUS <> 'C')
	)RESULT
	GROUP BY RESULT.Entry_Date, RESULT.Division, RESULT.Channel_Key
)SUBTBL
ON WO.Entry_Date = SUBTBL.Entry_Date
AND WO.Division = SUBTBL.Division
AND WO.Channel_Key = SUBTBL.Channel_Key
AND ISNULL(WO.Order_Count,0) <> SUBTBL.Order_Count;

--Department Order Count
UPDATE	Report_WebOrders
SET		Dept_Order_Count = SUBTBL.Dept_Order_Count
FROM dbo.Report_WebOrders WO
INNER JOIN
(
	SELECT RESULT.Entry_Date,
			RESULT.Division,
			RESULT.Dept_CD,
			RESULT.Channel_Key,
			COUNT(RESULT.OrderNO) AS Dept_Order_Count
	FROM
	(
		SELECT DISTINCT E.EntryDate As Entry_Date,
				S.DIV_CD AS Division,
				S.DEPT_CD,
				E.Channel_Key,
				E.OrderNO
		FROM dbo.ECOMETRY_ORDER_DETAIL E,
			[ChannelDM].[dbo].[DimSku] S
		WHERE S.SKU_NUM = E.SKU_NUM
		AND NOT (E.EDPNOSTATUS = 'C' AND E.ORDERnoSTATUS <> 'C')
	)RESULT
	GROUP BY RESULT.Entry_Date, RESULT.Division, RESULT.Dept_CD, RESULT.Channel_Key
)SUBTBL
ON WO.Entry_Date = SUBTBL.Entry_Date
AND WO.Division = SUBTBL.Division
AND WO.Dept_CD = SUBTBL.Dept_CD
AND WO.Channel_Key = SUBTBL.Channel_Key
AND ISNULL(WO.Dept_Order_Count,0) <> SUBTBL.Dept_Order_Count;