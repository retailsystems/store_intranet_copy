﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'USP_UpdateChannelKey')
	BEGIN
		DROP Procedure dbo.USP_UpdateChannelKey
	END
GO

CREATE PROCEDURE [dbo].[USP_UpdateChannelKey]
AS 

DECLARE @Channel_Key int, @Div_Code int, @Ecom_Filter varchar(500)
DECLARE @sql varchar(5000);

DECLARE ChannelCur CURSOR FOR
select Channel_Key,Div_Code, Ecom_Filter
	from [ChannelDM].[dbo].[UVW_DimChannelRules]
	Where Active_Flag = 1 AND Primary_Channel_Ind = 0
	ORDER BY Channel_Key, Div_Code;

--Update Channels
OPEN ChannelCur
FETCH NEXT FROM ChannelCur INTO @Channel_Key,@Div_Code,@Ecom_Filter;
WHILE @@FETCH_STATUS = 0
BEGIN
	SET @sql = 'UPDATE dbo.ECOMETRY_ORDER_DETAIL
		SET Channel_Key = ' + convert(varchar,@Channel_Key);
	SET @sql =	@sql + ' WHERE Channel_Key IS NULL AND ' + @Ecom_Filter;
	EXEC (@sql);
	FETCH NEXT FROM ChannelCur INTO @Channel_Key,@Div_Code,@Ecom_Filter;
END;

--close cursor
CLOSE ChannelCur;
DEALLOCATE ChannelCur;

--Update Web Orders Channels
UPDATE dbo.ECOMETRY_ORDER_DETAIL
SET CHANNEL_KEY = CR.Channel_Key	
FROM dbo.ECOMETRY_ORDER_DETAIL H
JOIN [ChannelDM].[dbo].[UVW_DimChannelRules] CR 
	ON H.DIVISION = CR.Div_Code AND CR.Active_Flag = 1 AND CR.Primary_Channel_Ind = 1
WHERE H.CHANNEL_KEY IS NULL;