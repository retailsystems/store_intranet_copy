﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'USP_DailyAnalysisReport')
	BEGIN
		DROP Procedure dbo.USP_DailyAnalysisReport
	END
GO

CREATE PROCEDURE [dbo].[USP_DailyAnalysisReport]
	@P_Div As Int,
	@P_StartDate As varchar(10),
	@P_EndDate As varchar(10)
AS  

DECLARE @sql As varchar(5000);
DECLARE @Division As varchar(7);
DECLARE @DivCode As varchar(5);
DECLARE @Column As varchar(10);
DECLARE @PlusOneTotal As  varchar(52);
DECLARE @PlusMainOneColumn As varchar(52);
DECLARE @PlusOneColumn As varchar(52);
DECLARE @PlusOnePivot As varchar(52);

IF @P_Div = 1
	BEGIN
		SET @Division = '''Div 1''';
		SET @Column = '[HT.Com]';
		SET @PlusOneTotal = ' ';
		SET @PlusMainOneColumn = ' ';
		SET @PlusOneColumn = ' ';
		SET @PlusOnePivot = ' ';
	END
ELSE
	BEGIN
        SET @Division = '''Div 5''';
        SET @Column = '[TD.Com]';
		SET @P_Div = 5
		SET @PlusOneTotal = ' + ISNULL([OneStop+], 0) '
		SET @PlusMainOneColumn = 'ISNULL(D.[OneStop+], 0) As [OneStop+], '
		SET @PlusOneColumn = 'ISNULL([OneStop+], 0) As [OneStop+], '
		SET @PlusOnePivot = ', [OneStop+]'
	END

SET @sql = 'SELECT	R.Trn_Date As Date, ' +
			'R.Metric_Name AS Metric, ' +
			'CASE R.Dept_CD  ' +
				'WHEN ' + @Division + ' THEN R.Dept_CD ' +
				'ELSE ''Dept '' + R.Dept_CD ' +
			'END AS [Div/Dept], ' +
			'ISNULL(D.Store, 0) AS Stores, ' +
			'ISNULL(D.Kiosk, 0) AS Kiosk, ' +
			'ISNULL(D.'+ @Column +', 0) AS [.Com], ' +
			'ISNULL(D.Amazon, 0) AS Amazon, ' +
			@PlusMainOneColumn +
			'ISNULL(D.Total, 0) AS Total ' +
	'FROM (select DISTINCT A.Trn_Date, B.*, C.Div_Order, C.Dept_CD FROM ' +
			'(Select DISTINCT Trn_Date from dbo.Report_Sales WHERE Trn_Date between '''+ @P_StartDate +''' and '''+ @P_EndDate +''' ' +
		') A, ' +
			'(Select * from dbo.Report_Metric) B, ' +
			'(Select 1 as Div_Order, ' + @Division + ' as Dept_CD union Select DISTINCT 2 as Div_Order, DEPT_CD as Dept_CD FROM [ChannelDM].[dbo].[DimSku] ' +
			 'WHERE div_cd = '+ CAST(@P_Div AS varchar(1)) +' AND Dept_CD <> ''0080'' ) C  ' +
			') R ' +
	'LEFT OUTER JOIN ' +
	'( ' +
		--Division
		'select Trn_Date,Metric_ID,1 AS Div_Order, ' + @Division + ' as Dept_CD, ' +
			'ISNULL([Store], 0) AS Store, ISNULL([Kiosk], 0) AS Kiosk, ISNULL('+ @Column +',0) AS '+ @Column +', ISNULL([Amazon], 0) AS Amazon, ' + @PlusOneColumn +
			'ISNULL([Store], 0) + ISNULL([Kiosk], 0) + ISNULL('+ @Column +', 0) + ISNULL([Amazon], 0) '+ @PlusOneTotal +' As Total ' +
		'from ' +
		'( ' +
			  'select      S.Trn_Date, ' +
						  'C.Channel_Name, ' +
						  'S.Metric_ID, ' +
						  'S.Meas_Val ' +
			  'from dbo.UVW_RptSummary S, ' +
						  'dbo.UVW_DimChannel C ' +
			  'where trn_date  between '''+ @P_StartDate +''' and '''+ @P_EndDate +''' ' +
			  'and S.Channel_Key = C.Channel_Key ' +
			  'and S.Division = '+ CAST(@P_Div AS varchar(1))  +' ' +
			  'and S.Metric_ID NOT IN (5.5, 8.5) ' +
		')a ' +
		'pivot ' +
		'( ' +
			  'Sum(Meas_Val)  ' +
			  'for Channel_Name in ([Store], [Kiosk], '+ @Column +', [Amazon] '+ @PlusOnePivot +') ' +
		')as pvsr ' +
		'UNION ' +
		--Department '
		'select	Trn_Date,Metric_ID,2 AS Div_Order, Dept_CD,  ' +
			'ISNULL([Store], 0) AS Store, ISNULL([Kiosk], 0) AS Kiosk, ISNULL('+ @Column +',0) AS '+ @Column +', ISNULL([Amazon], 0) AS Amazon, ' + @PlusOneColumn +
			'ISNULL([Store], 0) + ISNULL([Kiosk], 0) + ISNULL('+ @Column +', 0) + ISNULL([Amazon], 0) '+ @PlusOneTotal +' As Total ' +
		'from ' +
		'( ' +
				'select    S.Trn_Date, ' +
						  'S.Dept_CD, ' +
						  'C.Channel_Name, ' +
						'CASE S.Metric_ID ' +
							'WHEN 5.5 THEN 5 ' +
							'WHEN 8.5 THEN 8 ' +
							'ELSE S.Metric_ID ' +
						'END AS Metric_ID, ' +
						'S.Meas_Val ' +
				'from dbo.UVW_RptSummary S, ' +
						  'dbo.UVW_DimChannel C ' +
				'where trn_date between '''+ @P_StartDate +''' and '''+ @P_EndDate +''' ' +
				'and S.Channel_Key = C.Channel_Key ' +
				'and S.Division = ' + CAST(@P_Div AS varchar(1))  + ' ' +
				'and S.Metric_ID NOT IN (5, 8) ' +
		')a ' +
		'pivot ' +
		'( ' +
			  'Sum(Meas_Val)  ' +
			  'for Channel_Name in ([Store], [Kiosk], '+ @Column +', [Amazon] '+ @PlusOnePivot +') ' +
		')as dept ' +
	')D ' +
	'ON R.Trn_Date = D.Trn_Date ' +
		'AND R.Dept_CD = D.Dept_CD ' +
		'AND R.Metric_ID = D.Metric_ID ' +
		'AND R.Div_Order = D.Div_Order ' +
	'ORDER BY R.Trn_Date, R.Metric_Order, R.Div_Order, R.Dept_CD ';

EXEC (@sql); 