﻿USE [DailyAnalysis]

IF OBJECT_ID ('UVW_RptSummary', 'V') IS NOT NULL
DROP VIEW UVW_RptSummary ;
GO
CREATE VIEW [dbo].[UVW_RptSummary]
AS

--Total R Sls
SELECT Trn_Date, Division, 1 AS Div_Order, 
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 0 as Metric_ID, SUM(ISNULL(Reg_Sales, 0)) + SUM(ISNULL(CLR_Sales, 0)) AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)
GROUP BY Trn_Date, Division, Channel_Key
UNION
SELECT Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 0 as Metric_ID, ISNULL(Reg_Sales, 0) + ISNULL(CLR_Sales, 0) AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)
--Reg R Sls
UNION
SELECT Trn_Date, Division, 1 AS Div_Order, 
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 1 as Metric_ID, SUM(ISNULL(Reg_Sales, 0)) AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)
GROUP BY Trn_Date, Division, Channel_Key
UNION
SELECT Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 1 as Metric_ID, Reg_Sales AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)
--MD R Sls
UNION
SELECT Trn_Date, Division, 1 AS Div_Order, 
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 2 as Metric_ID, SUM(ISNULL(CLR_Sales, 0)) AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)
GROUP BY Trn_Date, Division, Channel_Key
UNION
SELECT Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 2 as Metric_ID, CLR_Sales AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)
--Total U Sls
UNION
SELECT Trn_Date, Division, 1 AS Div_Order, 
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 3 as Metric_ID, SUM(ISNULL(Reg_Units, 0)) + SUM(ISNULL(CLR_Units, 0)) AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)
GROUP BY Trn_Date, Division, Channel_Key
UNION
SELECT Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 3 as Metric_ID, ISNULL(Reg_Units, 0) + ISNULL(CLR_Units, 0) AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK) 
--Reg U Sls
UNION
SELECT Trn_Date, Division, 1 AS Div_Order, 
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 4 as Metric_ID, SUM(ISNULL(Reg_Units, 0)) AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)
GROUP BY Trn_Date, Division, Channel_Key
UNION
SELECT Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 4 as Metric_ID, Reg_Units AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)     
--MD U Sls
UNION
SELECT Trn_Date, Division, 1 AS Div_Order, 
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 5 as Metric_ID, SUM(ISNULL(CLR_Units, 0)) AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)
GROUP BY Trn_Date, Division, Channel_Key
UNION
SELECT Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 5 as Metric_ID, CLR_Units AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)
--Transactions
UNION
SELECT DISTINCT Trn_Date, Division, 1 AS Div_Order,
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 6 as Metric_ID, Trans_Count AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)
UNION
SELECT Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 6 as Metric_ID, Dept_Trans_Count AS Meas_Val FROM dbo.Report_Sales WITH (NOLOCK)
--R Demand
UNION
SELECT Entry_Date As Trn_Date, Division, 1 AS Div_Order, 
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 7 as Metric_ID, SUM(ISNULL(Reg_Sales, 0)) +  Sum(ISNULL(CLR_Sales, 0)) AS Meas_Val FROM dbo.Report_WebOrders WITH (NOLOCK)
GROUP BY Entry_Date, Division, Channel_Key
UNION
SELECT Entry_Date AS Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 7 as Metric_ID,  ISNULL(Reg_Sales, 0) +  ISNULL(CLR_Sales, 0) AS Meas_Val FROM dbo.Report_WebOrders WITH (NOLOCK)
--U Demand
UNION
SELECT Entry_Date As Trn_Date, Division, 1 AS Div_Order, 
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 8 as Metric_ID, SUM(ISNULL(Reg_Units, 0)) +  Sum(ISNULL(CLR_Units, 0)) AS Meas_Val FROM dbo.Report_WebOrders WITH (NOLOCK)
GROUP BY Entry_Date, Division, Channel_Key
UNION
SELECT Entry_Date AS Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 8 as Metric_ID,  ISNULL(Reg_Units, 0) +  ISNULL(CLR_Units, 0) AS Meas_Val FROM dbo.Report_WebOrders WITH (NOLOCK)
--Orders
UNION
SELECT DISTINCT Entry_Date AS Trn_Date, Division, 1 AS Div_Order,
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 9 as Metric_ID, Order_Count AS Meas_Val FROM dbo.Report_WebOrders WITH (NOLOCK)
UNION
SELECT Entry_Date AS Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 9 as Metric_ID, Dept_Order_Count AS Meas_Val FROM dbo.Report_WebOrders WITH (NOLOCK)
--Reg Perm MD
UNION 
SELECT Trn_Date, Division, 1 AS Div_Order, 
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 10 as Metric_ID, SUM(ISNULL(Reg_Perm_MD_Amt, 0)) AS Meas_Val FROM dbo.Report_Markdowns WITH (NOLOCK)
GROUP BY Trn_Date, Division, Channel_Key
UNION
SELECT Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 10 as Metric_ID, Reg_Perm_MD_Amt AS Meas_Val FROM dbo.Report_Markdowns WITH (NOLOCK)
--MD Perm MD
UNION 
SELECT Trn_Date, Division, 1 AS Div_Order, 
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 11 as Metric_ID, SUM(ISNULL(Clr_Perm_MD_Amt, 0)) AS Meas_Val FROM dbo.Report_Markdowns WITH (NOLOCK)
GROUP BY Trn_Date, Division, Channel_Key
UNION
SELECT Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 11 as Metric_ID, Clr_Perm_MD_Amt AS Meas_Val FROM dbo.Report_Markdowns WITH (NOLOCK) 
--Reg POS MD
UNION
SELECT Trn_Date, Division, 1 AS Div_Order, 
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 12 as Metric_ID, SUM(ISNULL(Reg_Promo_MD_Amt, 0)) AS Meas_Val FROM dbo.Report_Markdowns WITH (NOLOCK)
GROUP BY Trn_Date, Division, Channel_Key
UNION 
SELECT Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 12 as Metric_ID, Reg_Promo_MD_Amt AS Meas_Val FROM dbo.Report_Markdowns WITH (NOLOCK) 
--MD POS MD
UNION 
SELECT Trn_Date, Division, 1 AS Div_Order, 
		Case Division
			When 1 then 'Div 1'
			When 5 then 'Div 5'
			Else 'Div 8'
		End As Dept_CD, Channel_Key, 13 as Metric_ID, SUM(ISNULL(Clr_Promo_MD_Amt, 0)) AS Meas_Val FROM dbo.Report_Markdowns WITH (NOLOCK)
GROUP BY Trn_Date, Division, Channel_Key
UNION 
SELECT Trn_Date, Division, 2 AS Div_Order, 'Dept ' + Dept_CD, Channel_Key, 13 as Metric_ID, Clr_Promo_MD_Amt AS Meas_Val FROM dbo.Report_Markdowns WITH (NOLOCK) 
