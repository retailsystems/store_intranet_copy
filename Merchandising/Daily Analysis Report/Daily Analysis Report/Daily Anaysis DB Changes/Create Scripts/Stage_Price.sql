﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Report_WebOrders') AND type in (N'U'))
BEGIN
	DROP TABLE dbo.Report_WebOrders
END
GO

CREATE TABLE [dbo].[Stage_Price](
	[TRN_DT] [datetime] NOT NULL,
	[DIV_CD] [varchar](10) NULL,
	[DEPT_CD] [varchar](10) NULL,
	[SKU_NUM] [varchar](12) NOT NULL,
	[PRC_STAT] [varchar](6) NULL,
	[RET_PRC] [smallmoney] NULL,
 CONSTRAINT [PK_Stage_Price] PRIMARY KEY CLUSTERED 
(
	[TRN_DT] ASC,
	[SKU_NUM] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO 