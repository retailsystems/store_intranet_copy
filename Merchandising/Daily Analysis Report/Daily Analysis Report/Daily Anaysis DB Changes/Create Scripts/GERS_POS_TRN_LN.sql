﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'GERS_POS_TRN_LN') AND type in (N'U'))
BEGIN
	DROP TABLE dbo.GERS_POS_TRN_LN
END
GO

CREATE TABLE dbo.GERS_POS_TRN_LN(
	TRN_DT datetime NOT NULL,
	STORE_CD varchar(4) NOT NULL,
	TERM_NUM varchar(3) NOT NULL,
	TRN_NUM numeric(6, 0) NOT NULL,
	LN_NUM numeric(6, 0) NOT NULL,
	LN_TP varchar(3) NOT NULL,
	AMT money NULL,
	SKU_NUM varchar(12) NULL,
	QTY numeric(5) NULL,
	VOID varchar(1) NULL,
	EFF_AMT money NULL
) ON [PRIMARY]

GO 