﻿IF OBJECT_ID ('UVW_DimChannel', 'V') IS NOT NULL
DROP VIEW UVW_DimChannel ;
GO
CREATE VIEW [UVW_DimChannel] AS
/*
SELECT '0' AS Channel_Key, 
		'0' AS Channel_Type_Key, 
		'Store' As Channel_SubType,
		'ST' AS Channel_CD,
		'Store' AS Channel_Name,
		'Physical Store' AS Channel_Desc,
		'0' As MultiLocation_Flag,
		'1' As Active_Flag,
		'2011-11-29 15:30:09.163' AS TimeStamp
UNION
*/
select ChannelDM.dbo.DimChannel.*
from ChannelDM.dbo.DimChannel