﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Report_WebOrders') AND type in (N'U'))
BEGIN
	DROP TABLE dbo.Report_WebOrders
END
GO

CREATE TABLE dbo.Report_WebOrders(
	Entry_Date datetime NOT NULL,
	Division numeric(2,0) NOT NULL,
	Dept_CD varchar(6) NOT NULL,
	Channel_Key smallint NOT NULL,
	Reg_Sales numeric(10, 2) NULL,
	CLR_Sales numeric(10, 2) NULL,
	Order_Count int NULL,
	Dept_Order_Count int NULL,
	Reg_Units int NULL,
	CLR_Units int NULL
 CONSTRAINT PK_Report_WebOrders PRIMARY KEY CLUSTERED 
(
	Entry_Date ASC,
	Division ASC,
	Dept_CD ASC,
	Channel_Key ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO   