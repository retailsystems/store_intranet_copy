﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'ReSA_SALES') AND type in (N'U'))
BEGIN
	DROP TABLE dbo.ReSA_SALES
END
GO
CREATE TABLE [dbo].[ReSA_SALES](
	[DIVISION] [numeric](4, 0) NULL,
	[TRAN_DATE] [datetime] NULL,
	[TRAN_NO] [numeric](10, 0) NULL,
	[TRAN_SEQ_NO] [numeric](20, 0) NULL,
	[STORE_DAY_SEQ_NO] [numeric](20, 0) NULL,
	[STORE] [numeric](10, 0) NULL,
	[DEPT] [numeric](4, 0) NULL,
	[ITEM_SEQ_NO] [numeric](4, 0) NULL,
	[SKU] [varchar](25) NULL,
	[UNIT_RETAIL] [numeric](20, 4) NULL,
	[ITEM_TYPE] [varchar](6) NULL,
	[ITEM_STATUS] [varchar](6) NULL,
	[NON_MERCH_ITEM] [varchar](25) NULL,
	[STATUS] [varchar](6) NULL,
	[EMD_AMT] [numeric](20, 4) NULL,
	[OD_AMT] [numeric](20, 4) NULL,
	[TOTAL_DISCOUNT_AMT] [numeric](20, 4) NULL,
	[NET_RETAIL] [numeric](20, 4) NULL,
	[QTY] [numeric](12, 4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF 