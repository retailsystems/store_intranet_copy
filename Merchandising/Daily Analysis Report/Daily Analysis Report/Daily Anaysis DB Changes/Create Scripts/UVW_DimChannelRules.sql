﻿IF OBJECT_ID ('UVW_DimChannelRules', 'V') IS NOT NULL
DROP VIEW UVW_DimChannelRules ;
GO
CREATE VIEW [dbo].[UVW_DimChannelRules]
AS

SELECT 0 As Channel_Key, 
		'ST' Channel_CD,
		'Stores' AS Channel_Name, 
		'Physical Stores' AS Channel_Desc, 
		0 AS Channel_Type_Key,
		'Physical Stores' AS Channel_Type_Name,
		0 AS MultiLocation_Flag,
		1 AS Active_Flag,
		1 AS Div_Code,
		NULL AS Ifc_Store_Key,
		449000 Channel_Key_Order,
		0 AS Primary_Channel_Ind,
		NULL AS Ecom_Filter

UNION

SELECT 0 AS Channel_Key, 
		'ST' Channel_CD,
		'Stores' AS Channel_Name, 
		'Physical Stores' AS Channel_Desc, 
		0 AS Channel_Type_Key,
		'Physical Stores' AS Channel_Type_Name,
		0 AS MultiLocation_Flag,
		1 AS Active_Flag,
		5 AS Div_Code,
		NULL AS Ifc_Store_Key,
		599000 Channel_Key_Order,
		0 AS Primary_Channel_Ind,
		NULL AS Ecom_Filter

UNION

SELECT 0 AS Channel_Key, 
		'ST' Channel_CD,
		'Stores' AS Channel_Name, 
		'Physical Stores' AS Channel_Desc, 
		0 AS Channel_Type_Key,
		'Physical Stores' AS Channel_Type_Name,
		0 AS MultiLocation_Flag,
		1 AS Active_Flag,
		8 AS Div_Code,
		NULL AS Ifc_Store_Key,
		810000 Channel_Key_Order,
		0 AS Primary_Channel_Ind,
		NULL AS Ecom_Filter

UNION

SELECT     CH.Channel_Key, CH.Channel_CD, CH.Channel_Name, CH.Channel_Desc, CT.Channel_Type_Key, CT.Channel_Type_Name, CH.MultiLocation_Flag, 
                    CH.Active_Flag, RL.Div_Code, RL.Store_Key AS Ifc_Store_Key, RL.Channel_Key_Order,
					RL.Primary_Channel_Ind, 'DIVISION = ' + CONVERT(VARCHAR, RL.Div_Code) 
                      + ' AND ' + RL.Ecom_Filter AS Ecom_Filter
FROM         [ChannelDM].[dbo].[DimChannel] AS CH INNER JOIN
             [ChannelDM].[dbo].[DimChannelType] AS CT ON CH.Channel_Type_Key = CT.Channel_Type_Key INNER JOIN
             [ChannelDM].[dbo].[MDChannelETLRules] AS RL ON CH.Channel_Key = RL.Channel_Key

GO