﻿IF OBJECT_ID ('UVW_DimChannelType', 'V') IS NOT NULL
DROP VIEW UVW_DimChannelType ;
GO
CREATE VIEW [UVW_DimChannelType] AS

SELECT '0' AS Channel_Type_Key, 
		'Stores' AS Channel_Type_Name, 
		'Physical Stores' AS Channel_Type_Desc, 
		'2011-11-29 15:30:09.163' AS TimeStamp
UNION

select ChannelDM.dbo.DimChannelType.*
from ChannelDM.dbo.DimChannelType