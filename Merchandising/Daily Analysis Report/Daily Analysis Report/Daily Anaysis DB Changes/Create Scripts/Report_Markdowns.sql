﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Report_Markdowns') AND type in (N'U'))
BEGIN
	DROP TABLE dbo.Report_Markdowns
END
GO

CREATE TABLE dbo.Report_Markdowns(
	Trn_Date datetime NOT NULL,
	Division numeric(2, 0) NOT NULL,
	Dept_CD varchar(6) NOT NULL,
	Channel_Key smallint NOT NULL,
	Reg_Perm_MD_Amt numeric(10, 2) NULL,
	CLR_Perm_MD_Amt numeric(10, 2) NULL,
	Reg_Perm_MD_Units int NULL,
	CLR_Perm_MD_Units int NULL,
	Reg_Promo_MD_Amt numeric(10, 2) NULL,
	CLR_Promo_MD_Amt numeric(10, 2) NULL,
	Reg_Promo_MD_Units int NULL,
	CLR_Promo_MD_Units int NULL
CONSTRAINT PK_Report_Markdowns PRIMARY KEY CLUSTERED
(
	Trn_Date ASC,
	Division ASC,
	Dept_CD ASC,
	Channel_Key ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO  