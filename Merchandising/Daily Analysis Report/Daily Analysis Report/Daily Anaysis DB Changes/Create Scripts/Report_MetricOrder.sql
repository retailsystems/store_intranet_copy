﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Report_Metric') AND type in (N'U'))
BEGIN
	DROP TABLE dbo.Report_Metric
END
GO

CREATE TABLE [dbo].[Report_Metric](
	[Metric_ID] [int] IDENTITY(1,1) NOT NULL,
	[Metric_Order] [int] NOT NULL,
	[Metric_Name] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Report_Metric] PRIMARY KEY CLUSTERED 
(
	[Metric_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
