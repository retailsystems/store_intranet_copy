using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Principal;

namespace PositivePay
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aRPAPToolStripMenuItem_Click(object sender, EventArgs e)
        {

            APForm frmAP = new APForm();
            frmAP.Show();
        }

        private void aRPPRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PRForm frmPR = new PRForm();
            frmPR.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutFrom frmAbout = new AboutFrom();
            frmAbout.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            APForm frmAP = new APForm();
            frmAP.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PRForm frmPR = new PRForm();
            frmPR.Show();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();

            string APUsersList = configurationAppSettings.GetValue("APUsersList", typeof(string)).ToString().ToUpper(); 
            string PRUsersList = configurationAppSettings.GetValue("PRUsersList", typeof(string)).ToString().ToUpper();

            string uname = WindowsIdentity.GetCurrent().Name.ToUpper().Split('\\')[1];

            //MessageBox.Show("Username2:" + uname);
            //this.nametext = WindowsIdentity.GetCurrent().Name;
           

            //if (APUsersList.IndexOf(uname) != -1)
            //    this.button2.Visible = false; // Disabling button2(Payroll button) for AccountPayable user)
            //else // nhendon,agarza,tvasquez  - Users for Payroll only
            //    if (PRUsersList.IndexOf(uname) != -1)
            //    //if ((uname == "NHENDON") || (uname == "AGARZA") || (uname == "TVASQUEZ"))
            //        this.button1.Visible = false; //Disabling button1(AccountPayable button) for Payroll user)

            
            if (APUsersList.IndexOf(uname) != -1)
            {
                this.button1.Visible = true;
                this.button1.Location = new Point(90, 57);
            }
            else
            {
                if (PRUsersList.IndexOf(uname) != -1)
                {
                    this.button2.Visible = true;
                    this.button2.Location = new Point(90, 57);
                }
                else
                { MessageBox.Show("User "+uname+" does not have access to PositivePay application"); }
                
            }
          }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {
          System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();

            string APUsersList = configurationAppSettings.GetValue("APUsersList", typeof(string)).ToString().ToUpper(); 
            string PRUsersList = configurationAppSettings.GetValue("PRUsersList", typeof(string)).ToString().ToUpper();
            string uname = WindowsIdentity.GetCurrent().Name.ToUpper().Split('\\')[1];

            //if (APUsersList.IndexOf(uname) != -1)
            //{
            //    this.aRPPRToolStripMenuItem.Enabled = false; // Disablling Payroll form form AP Users
            //}
            //else // nhendon,agarza,tvasquez  - Users for Payroll only
            //    if (PRUsersList.IndexOf(uname) != -1)
            //        this.aRPAPToolStripMenuItem.Enabled = false;

            if (APUsersList.IndexOf(uname) != -1)
            {
                this.aRPAPToolStripMenuItem.Enabled = true;
            }
            else
            {
                if (PRUsersList.IndexOf(uname) != -1)
                {
                    this.aRPPRToolStripMenuItem.Enabled = true;
                }
                else
                { MessageBox.Show("User " + uname + " does not have access to PositivePay application"); }

            }
        }



    
    }
}