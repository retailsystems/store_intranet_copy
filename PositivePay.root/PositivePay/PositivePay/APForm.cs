using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.OracleClient;
using System.IO;
using HTError;
using System.Net.Mail;


namespace PositivePay
{
    public partial class APForm : Form
    {
        public APForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = "ARP_AccountPayable";
            Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Text = "ACCOUNT RECONCILEMENT(ARP_AP) - Wells Fargo";
        }

       
        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult msgresult = MessageBox.Show("Do you want to exit the Application?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (msgresult == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                this.Close();


            }
        }

        private void button1_Click(object sender, EventArgs e)
        { //begin
             DateTime dt1 = dateTimePicker1.Value;
             DateTime dt2 = dateTimePicker2.Value;
            if (dt1 > dt2)
            {
                MessageBox.Show("FromDate should be less than ToDate");
                this.Close();
            }
            else
            {
                //trun your cursor into hrglass while process is running
                Cursor.Current = Cursors.WaitCursor;



                System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();

                string dbconn = configurationAppSettings.GetValue("ConnectionString", typeof(string)).ToString();
                string schema = configurationAppSettings.GetValue("Schema", typeof(string)).ToString();

                //Establish Oracle Connection and create a sql string
                //OracleConnection conn = new OracleConnection("Data Source=PRD.HOTTOPIC.COM;Persist Security Info=True;User ID=financeinq;password=inqfinance;Unicode=True");
                OracleConnection conn = new OracleConnection(dbconn);
                try
                {
                    conn.Open();
                    //MessageBox.Show("Connection Established");

                }
                catch (OracleException ex)
                {
                    MessageBox.Show(ex.Message.ToString() + "Connection not Established");

                }
                //Build and execute Account Payable Report Query
                //Open the file directly using StreamWriter
                try
                {
                    //MessageBox.Show("Building SQL Query");

                    string APSql = "SELECT   a.trans_nbr doc_num, a.check_date pmt_dt,b.bank_acct_nbr bnk_acct_num, SUM (a.bank_chk_amt)*100 amt_pd,'320' AS void_seq," +
                                            "CASE " +
                                            " WHEN TRIM (a.remit_to_code) IS NOT NULL " +
                                            " THEN ( SELECT vendor_vname FROM lawprod.apvenloc d" +
                                               " WHERE a.pay_vendor = d.vendor" +
                                               " AND a.remit_to_code = d.location_code)" +
                                            " ELSE (SELECT vendor_vname FROM lawprod.apvenmast c" +
                                              " WHERE a.pay_vendor = c.vendor)" +
                                            " END payee" +
                                   " FROM lawprod.appayment a, lawprod.cbcashcode b" +
                                   " WHERE a.cash_code = b.cash_code" +
                                   " AND RTRIM (b.bank_acct_nbr) =" + this.textBox1.Text + "" +
                                   " AND check_date BETWEEN to_date('" + this.dateTimePicker1.Text + "','mm/dd/yyyy') AND to_date('" + this.dateTimePicker2.Text + "','mm/dd/yyyy') AND bank_inst_code ='SYS'" +
                                   " GROUP BY a.trans_nbr, a.check_date, b.bank_acct_nbr, a.remit_to_code, a.pay_vendor" +
                                   " HAVING SUM (a.bank_chk_amt) > 0" +
                                   " UNION " +
                                   "SELECT   a.trans_nbr doc_num, a.check_date pmt_dt,b.bank_acct_nbr bnk_acct_num, SUM (a.bank_chk_amt)*100 amt_pd,'370' AS void_seq," +
                                            "CASE " +
                                            " WHEN TRIM (a.remit_to_code) IS NOT NULL" +
                                            " THEN (SELECT vendor_vname FROM lawprod.apvenloc d" +
                                                  " WHERE a.pay_vendor = d.vendor" +
                                                  " AND a.remit_to_code = d.location_code)" +
                                            " ELSE (SELECT vendor_vname FROM lawprod.apvenmast c" +
                                                   " WHERE a.pay_vendor = c.vendor)" +
                                            " END payee" +
                                  " FROM lawprod.appayment a, lawprod.cbcashcode b" +
                                  " WHERE a.cash_code = b.cash_code" +
                                  " AND RTRIM (b.bank_acct_nbr) = " + this.textBox1.Text + "" +
                                  " AND void_date BETWEEN to_date('" + this.dateTimePicker1.Text + "','mm/dd/yyyy')-1 AND to_date('" + this.dateTimePicker2.Text + "','mm/dd/yyyy')-1 AND bank_inst_code ='SYS' AND a.void_seq <> 0" +
                                  " GROUP BY a.trans_nbr, a.check_date, b.bank_acct_nbr, a.remit_to_code,a.pay_vendor" +
                                  " HAVING SUM (a.bank_chk_amt) > 0";

                    //MessageBox.Show("SQL Query string has been created");
                    OracleDataAdapter da = new OracleDataAdapter(APSql, conn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "APTable");


                    //Open the file directly using StreamWriter
                    string APFile = configurationAppSettings.GetValue("APFilePath", typeof(string)).ToString();
                    //StreamWriter sw = new StreamWriter("C:\\Program Files\\HoTTApps\\ARP\\AP\\" + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");
                    //StreamWriter sw = new StreamWriter(APFile + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");
                    StreamWriter sw = new StreamWriter(APFile + "ARPW" + DateTime.Now.ToString("MMddhhmmtt") + ".txt");


                    sw.WriteLine("$$ADD ID=HTIUSRF1 BID='DII4758372973'");
                    sw.WriteLine("WRRECV        9600112644                                                        ");
                    //float amt = 0;
                    double amt = 0;
                    foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
                    {

                        sw.WriteLine("{0}{1}{2}{3}{4}{5}",
                        dr["DOC_NUM"].ToString().Trim().PadLeft(10, '0'),
                        DateTime.Parse(dr["PMT_DT"].ToString().Trim()).ToString("MMddyy"),
                        dr["BNK_ACCT_NUM"].ToString().Trim(),
                        dr["void_seq"].ToString().Trim(),
                        dr["AMT_PD"].ToString().Replace(".", "").PadLeft(10, '0').Trim(),
                        dr["PAYEE"].ToString().Trim()
                       );
                        //amt += float.Parse(dr["AMT_PD"].ToString());
                        amt += (double.Parse(dr["AMT_PD"].ToString())/100);
                        //MessageBox.Show("test:" + amt);
                    }
                    sw.WriteLine("&              " + ds.Tables[0].Rows.Count.ToString().PadLeft(5, '0') + "   " + amt.ToString().Replace(".", "").PadLeft(10, '0').Trim());

                    sw.Close();

                }
                catch (IOException ex)
                {
                    MessageBox.Show(ex.Message + " Problem creating Account Payable file");

                }
                finally
                {
                    //Closing Oracle DB connection
                    conn.Close();
                    MessageBox.Show("Positive Pay AccountPayable file created successfully.");

                    //Reset cursor back to default position
                    Cursor.Current = Cursors.Default;
                }
            }
            }//end

        private void timer1_Tick(object sender, EventArgs e)
        {

        }
        }
}
