using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using HTError;

namespace PositivePay
{
    class SuccessEmail
    {
        public void Email(Boolean AP, Boolean PR)
        {
            try
            {
                System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();

                string APFile = configurationAppSettings.GetValue("APFilePath", typeof(string)).ToString() + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt";
                string PRFile = configurationAppSettings.GetValue("PRFilePath", typeof(string)).ToString() + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt";
                string EmailTo = configurationAppSettings.GetValue("To", typeof(string)).ToString();
                string EmailFrom = configurationAppSettings.GetValue("From", typeof(string)).ToString();

                MailMessage mail = new MailMessage();
                MailAddress mailAddress = new MailAddress(EmailFrom);
                mail.From = mailAddress;
                mail.To.Add(EmailTo);

                //MailMessage mail = new MailMessage();
                //MailAddress mailAddress = new MailAddress("nchokshi@hottopic.com");
                //mail.From = mailAddress;
                //mail.To.Add("nchokshi@hottopic.com");

                //string APFile = "C:\\Program Files\\HoTTApps\\ARP\\AP\\" + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt";
                //string PRFile = "C:\\Program Files\\HoTTApps\\ARP\\Payroll\\" + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt";

                if (AP == true)
                {
                    mail.Subject = "PositivePay - Account Payable file created successfully @" + DateTime.Now.ToString("MMddHHtt");
                    mail.Body = "PositivePay Account Payable file attached";
               
               
                Attachment attach = new Attachment(APFile);
                mail.Attachments.Add(attach);
                
                }
                else
                {
                    mail.Subject = "PositivePay - Payroll file created successfully @" + DateTime.Now.ToString("MMddHHtt");
                    mail.Body = "TESTING - PositivePay Payroll file attached";
                    Attachment attach = new Attachment(PRFile);
                    mail.Attachments.Add(attach);
                }

                SmtpClient smtpClient = new SmtpClient("filter.hottopic.com", Convert.ToInt32(25));
                smtpClient.Send(mail);
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                //Console.Read();
                ErrorHandler errHandler = new ErrorHandler();
                errHandler.LogError(ex, "Problem sending success email");
                Environment.Exit(-1);
                
            }
        }

        
    }
}
