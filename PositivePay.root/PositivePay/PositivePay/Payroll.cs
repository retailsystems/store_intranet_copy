using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.OracleClient;
using System.IO;
using HTError;
using System.Net.Mail;
using System.Configuration;

namespace PositivePay
{
    class Payroll
    {
        static void Main(string[] args)
        {
            System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();
            string EmailTo = configurationAppSettings.GetValue("PRDistroTo", typeof(string)).ToString();
            string EmailFrom = configurationAppSettings.GetValue("PRDistroFrom", typeof(string)).ToString();
            string PRFile = configurationAppSettings.GetValue("PRFilePath", typeof(string)).ToString();

            // Ensure that log files does not exist. Finance doesn't want to accumalte log file.
            File.Delete(PRFile + "Production.log");
            File.Delete(PRFile + "Debug.log");

            HTError.ErrorHandler errHandlar = new ErrorHandler(PRFile+"Production.log", PRFile+"Debug.log", "filter.hottopic.com", EmailFrom, EmailTo, HTError.LogLevel.Debug);

            errHandlar.EnableDebugLog = true;
            errHandlar.EnableProductionLog = true;
            errHandlar.EnableEmail = true;
            errHandlar.SendMailOnError = true;

           //System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();

           string dbconn = configurationAppSettings.GetValue("ConnectionString", typeof(string)).ToString();
           string schema = configurationAppSettings.GetValue("Schema", typeof(string)).ToString();
            

            //OracleConnection conn = new OracleConnection("Data Source=PRD.HOTTOPIC.COM;Persist Security Info=True;User ID=financeinq;password=inqfinance;Unicode=True");
           OracleConnection conn = new OracleConnection(dbconn);
            try
            {
                conn.Open();
                Console.WriteLine("Creating Positive Pay Payroll file...");
                //Console.WriteLine("Connection Established");
                errHandlar.LogInformation("OracleDB connection Established successfully");

            }
            catch (OracleException ex)
            {
                //Console.WriteLine(ex.ToString());
                errHandlar.LogError(ex, "problem with oracle connection");
                Environment.Exit(-1);
            }

			DataTable Accounts = new DataTable();

			try
			{
				string accountsql = "SELECT 'TD' company, bnk_acct_nbr FROM " + schema + ".bankacct WHERE SEND_PT_NAME like 'TORRID%' ";
				//+ "union "
				//+ "SELECT 'HT' company, bnk_acct_nbr FROM " + schema + ".bankacct WHERE SEND_PT_NAME like 'HOT TOPIC%' ";

				errHandlar.LogInformation("Getting bank account numbers...");
				OracleDataAdapter oda = new OracleDataAdapter(accountsql, conn);
				oda.Fill(Accounts);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Following error occured while getting Bank Account Information:" + ex.ToString());
				errHandlar.LogError(ex, "Problem getting Bank Accounts");
				Environment.Exit(-1);
				conn.Close();
			}

			//Build and execute Account Payable Report Query
            //Open the file directly using StreamWriter
            try
            {
				foreach (DataRow datarow in Accounts.Rows)
				{
					string bank_account = datarow["bnk_acct_nbr"].ToString().Trim();
					string company = datarow["company"].ToString();
					//Console.WriteLine("Building SQL Query");
					errHandlar.LogInformation("Building SQL Query String...");

					//string PayRollSql = "SELECT a.check_nbr DOC_NUM, a.check_date PMT_DT, c.bnk_acct_nbr BNK_ACCT_NUM, a.check_net AMT_PD, a.check_ID " +
					//                        " FROM lawprod.paymastr a, lawprod.bankfile c " +
					//                        " WHERE a.bank_code = c.bank_code " +
					//                        " AND RTRIM(c.bnk_acct_nbr) = '4984037531'" +
					//                        " AND a.check_date BETWEEN trunc(sysdate) AND trunc(sysdate) AND a.check_type in ('M','R') AND A.check_net > 0" +
					//                        " ORDER BY a.check_nbr, a.check_date, c.bnk_acct_nbr";

					string PayRollSql = "SELECT   a.check_nbr DOC_NUM, a.check_date PMT_DT," +
											" c.bnk_acct_nbr BNK_ACCT_NUM, '320' TRAN_CODE, (a.check_net*100) AMT_PD," +
											" trim(b.last_name)||','||' '||trim(b.first_name)||' '||trim(b.middle_init) PAYEE" +
											" FROM " + schema + ".paymastr a, " + schema + ".bankfile c, " + schema + ".employee b" +
											" WHERE a.bank_code = c.bank_code" +
											" AND RTRIM (c.bnk_acct_nbr) = '" + bank_account + "'" +
											" AND a.employee = b.employee" +
											" AND a.check_date BETWEEN trunc(sysdate) AND trunc(sysdate) AND a.check_type = 'M' AND a.check_net > '0'" +
											" UNION " +
											" SELECT   a.check_nbr DOC_NUM, a.check_date PMT_D," +
											" c.bnk_acct_nbr BNK_ACCT_NUM, '370' as TRAN_CODE, ABS (a.check_net)*100 AMT_PD, " +
											" trim(b.last_name)||','||' '||trim(b.first_name)||' '||trim(b.middle_init) PAYEE" +
											" FROM " + schema + ".paymastr a, " + schema + ".bankfile c, " + schema + ".employee b" +
											" WHERE a.bank_code = c.bank_code" +
											" AND RTRIM (c.bnk_acct_nbr) = '" + bank_account + "'" +
											" AND a.employee = b.employee" +
											" AND a.check_date BETWEEN trunc(sysdate) AND trunc(sysdate) AND a.check_type = 'R' AND a.check_net < '0'" +
											" AND a.check_nbr IN (SELECT check_nbr  FROM " + schema + ".paymastr WHERE check_type = 'M' AND check_net > '0')";


					//Console.WriteLine("SQL Query string has been created");
					errHandlar.LogInformation("SQL Query String has been created successfully");

					OracleDataAdapter da = new OracleDataAdapter(PayRollSql, conn);
					DataSet ds = new DataSet();
					da.Fill(ds, "APTable");


					//Open the file directly using StreamWriter
	               
					//StreamWriter sw = new StreamWriter("C:\\Program Files\\HoTTApps\\ARP\\Payroll\\" + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");
					//StreamWriter sw = new StreamWriter(PRFile + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");
					StreamWriter sw = new StreamWriter(PRFile + "ARPW_" + company + "_PR_" + DateTime.Now.ToString("yyyyMMddhhmmtt") + ".txt");


					//sw.WriteLine("Executer:Nina Chokshi");
					//sw.WriteLine("$$ADD ID=HOTUSRF1 BID='DII4984037531'");
					//sw.WriteLine("WRRECV        4984037531                                                        ");
					sw.WriteLine("*03" + "00182" + bank_account.PadLeft(15, '0') + "0" + string.Empty.PadLeft(61, ' '));
					//float amt = 0;
					double amt = 0;
					foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
					{

						sw.WriteLine("{0}{1}{2}{3}{4}{5}",
						dr["DOC_NUM"].ToString().Trim().PadLeft(10, '0'),
						DateTime.Parse(dr["PMT_DT"].ToString().Trim()).ToString("MMddyy"),
						dr["BNK_ACCT_NUM"].ToString().Trim().PadLeft(15, '0'),
						dr["TRAN_CODE"].ToString().Trim(),
						dr["AMT_PD"].ToString().Replace(".", "").PadLeft(10, '0').Trim(),
						dr["PAYEE"].ToString().Trim()
					   );
						amt += double.Parse(dr["AMT_PD"].ToString());
					}
					sw.WriteLine("&              " + ds.Tables[0].Rows.Count.ToString().PadLeft(7, '0') + "   " + amt.ToString().Replace(".", "").PadLeft(13, '0').Trim());

					sw.Close();
					errHandlar.LogInformation("Payroll file created successfully");

					//SuccessEmail sEmail = new SuccessEmail();
					//sEmail.Email(false,true);
				}

                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Follwoing error occured while creating Payroll file:" + ex.ToString());
                errHandlar.LogError(ex, "Problem creating AccountPayable file");
                Environment.Exit(-1);

            }
            finally
            {
                //Closing Oracle DB connection
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                //Console.WriteLine("Closing the Oracle Connection");
                //errHandlar.LogInformation("Oracle connection closed successfully");

               
            }
        }
    }
}
