using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.OracleClient;
using System.IO;



namespace PositivePay
{
    public partial class PRForm : Form
    {
        public PRForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult msgresult = MessageBox.Show("Do you want to exit the Application?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (msgresult == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
    
            
        { //begin
            DateTime dt1 = dateTimePicker1.Value;
            DateTime dt2 = dateTimePicker2.Value;
            if (dt1 > dt2)
            {
                MessageBox.Show("FromDate should be less than ToDate");
                this.Close();
            }
            else
            {

                //trun your cursor into hrglass while process is running
                Cursor.Current = Cursors.WaitCursor;

                System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();

                string dbconn = configurationAppSettings.GetValue("ConnectionString", typeof(string)).ToString();
                string schema = configurationAppSettings.GetValue("Schema", typeof(string)).ToString();

                //Establish Oracle Connection and create a sql string
                //OracleConnection conn = new OracleConnection("Data Source=PRD.HOTTOPIC.COM;Persist Security Info=True;User ID=financeinq;password=inqfinance;Unicode=True");
                OracleConnection conn = new OracleConnection(dbconn);
                try
                {
                    conn.Open();
                    //MessageBox.Show("Connection Established");

                }
                catch (OracleException ex)
                {
                    MessageBox.Show(ex.Message.ToString() + "Connection not Established");

                }
                try
                { //begin try
                    //Disabled "Manual Checks and System Checks option" as per Jasmyn Han as of 5/20/2009
                    //if (radioButton1.Checked == true) 
                    //{ //1st if Manuual Checks "UNION" Voided check only.  Note check amt for payroll table will be negative thus the use of ABS function.*/
                    //    
                    //    string PayrollSql = "SELECT   a.check_nbr DOC_NUM, a.check_date PMT_DT," +
                    //                        " c.bnk_acct_nbr BNK_ACCT_NUM, '320' TRAN_CODE, a.check_net AMT_PD," +
                    //                        " trim(b.last_name)||','||' '||trim(b.first_name)||' '||trim(b.middle_init) PAYEE" +
                    //                        " FROM lawprod.paymastr a, lawprod.bankfile c, lawprod.employee b" +
                    //                        " WHERE a.bank_code = c.bank_code" +
                    //                        " AND RTRIM (c.bnk_acct_nbr) = " + this.textBox1.Text + " " +
                    //                        " AND a.employee = b.employee" +
                    //                        " AND a.check_date BETWEEN to_date('" + this.dateTimePicker1.Text + "','mm/dd/yyyy') AND to_date('" + this.dateTimePicker2.Text + "','mm/dd/yyyy') AND a.check_type = 'M' AND a.check_net > '0'" +
                    //                        " UNION " +
                    //                        " SELECT   a.check_nbr DOC_NUM, a.check_date PMT_D," +
                    //                        " c.bnk_acct_nbr BNK_ACCT_NUM, '370' as TRAN_CODE, ABS (a.check_net) AMT_PD, " +
                    //                        " trim(b.last_name)||','||' '||trim(b.first_name)||' '||trim(b.middle_init) PAYEE" +
                    //                        " FROM lawprod.paymastr a, lawprod.bankfile c, lawprod.employee b" +
                    //                        " WHERE a.bank_code = c.bank_code" +
                    //                        " AND RTRIM (c.bnk_acct_nbr) = " + this.textBox1.Text + " " +
                    //                        " AND a.employee = b.employee" +
                    //                        " AND a.check_date BETWEEN to_date('" + this.dateTimePicker1.Text + "','mm/dd/yyyy') AND to_date('" + this.dateTimePicker2.Text + "','mm/dd/yyyy') AND a.check_type = 'R' AND a.check_net < '0'" +
                    //                        " AND a.check_nbr IN (SELECT check_nbr  FROM lawprod.paymastr WHERE check_type = 'M' AND check_net > '0')";


                    //    OracleDataAdapter da = new OracleDataAdapter(PayrollSql, conn);
                    //    DataSet ds = new DataSet();
                    //    da.Fill(ds, "tblCombine");
                    //    conn.Close();

                    //    //Open the file directly using StreamWriter
                    //    StreamWriter sw = new StreamWriter("C:\\Program Files\\HoTTApps\\ARP\\Payroll\\" + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");

                    //    sw.WriteLine("$$ADD ID=HOTUSERF1 BID='DII4984037531'");
                    //    sw.WriteLine("WRRECV        4984037531                                                        ");
                    //    foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
                    //    {
                    //        sw.WriteLine("{0}{1}{2}{3}{4}",
                    //            dr["DOC_NUM"].ToString().Trim().PadLeft(10, '0'),
                    //            DateTime.Parse(dr["PMT_DT"].ToString().Trim()).ToString("MMddyy"),
                    //            dr["BNK_ACCT_NUM"].ToString().Trim(),
                    //            dr["TRAN_CODE"].ToString().Trim(),
                    //            dr["AMT_PD"].ToString().Replace(".", "").PadLeft(10, '0').Trim(),
                    //            dr["PAYEE"].ToString().Trim());

                    //    }
                    //    sw.WriteLine("&              " + ds.Tables[0].Rows.Count.ToString().PadLeft(5, '0') + "   ");
                    //    sw.Close();
                    //    MessageBox.Show("1st option");
                    //}//1st if end

                    //Manual Checks Only Option
                    if (radioButton2.Checked == true)
                    { //2nd if
                        //string PayrollSql = "SELECT a.check_nbr DOC_NUM, a.check_date PMT_DT, c.bnk_acct_nbr BNK_ACCT_NUM, a.check_net AMT_PD, a.check_ID " +
                        //                    " FROM lawprod.paymastr a, lawprod.bankfile c " +
                        //                    " WHERE a.bank_code = c.bank_code " +
                        //                    " AND RTRIM(c.bnk_acct_nbr) = " + this.textBox1.Text + " " +
                        //                    " AND a.check_date BETWEEN to_date('" + this.dateTimePicker1.Text + "','mm/dd/yyyy') AND to_date('" + this.dateTimePicker2.Text + "','mm/dd/yyyy') AND a.check_type <> 'M' AND A.check_net > 0" +
                        //                    " ORDER BY a.check_nbr, a.check_date, c.bnk_acct_nbr";

                        string PayrollSql = "SELECT   a.check_nbr DOC_NUM, a.check_date PMT_DT," +
                                             " c.bnk_acct_nbr BNK_ACCT_NUM, '320' TRAN_CODE, (a.check_net*100) AMT_PD," +
                                             " trim(b.last_name)||','||' '||trim(b.first_name)||' '||trim(b.middle_init) PAYEE" +
                                             " FROM lawprod.paymastr a, lawprod.bankfile c, lawprod.employee b" +
                                             " WHERE a.bank_code = c.bank_code" +
                                             " AND RTRIM (c.bnk_acct_nbr) = " + this.textBox1.Text + " " +
                                             " AND a.employee = b.employee" +
                                             " AND a.check_date BETWEEN to_date('" + this.dateTimePicker1.Text + "','mm/dd/yyyy') AND to_date('" + this.dateTimePicker2.Text + "','mm/dd/yyyy') AND a.check_type = 'M' AND a.check_net > '0'";



                        OracleDataAdapter da = new OracleDataAdapter(PayrollSql, conn);
                        DataSet ds = new DataSet();
                        da.Fill(ds, "tblManual");
                        conn.Close();

                        //Open the file directly using StreamWriter
                        string PRFile = configurationAppSettings.GetValue("PRFilePath", typeof(string)).ToString();
                        //StreamWriter sw = new StreamWriter("C:\\Program Files\\HoTTApps\\ARP\\Payroll\\" + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");

                        StreamWriter sw = new StreamWriter(PRFile + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");

                        //sw.WriteLine("Executer:Nina Chokshi");
                        sw.WriteLine("$$ADD ID=HOTUSRF1 BID='DII4984037531'");
                        sw.WriteLine("WRRECV        4984037531                                                        ");
                        double amt = 0;
                        foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
                        {
                            sw.WriteLine("{0}{1}{2}{3}{4}{5}",
                                dr["DOC_NUM"].ToString().Trim().PadLeft(10, '0'),
                                DateTime.Parse(dr["PMT_DT"].ToString().Trim()).ToString("MMddyy"),
                                dr["BNK_ACCT_NUM"].ToString().Trim(),
                                dr["TRAN_CODE"].ToString().Trim(),
                                dr["AMT_PD"].ToString().Replace(".", "").PadLeft(10, '0').Trim(),
                                dr["PAYEE"].ToString().Trim());
                            amt += double.Parse(dr["AMT_PD"].ToString());
                        }
                      //  sw.WriteLine("&              " + ds.Tables[0].Rows.Count.ToString().PadLeft(5, '0') + "   ");
                        sw.WriteLine("&              " + ds.Tables[0].Rows.Count.ToString().PadLeft(5, '0') + "   " + amt.ToString().Replace(".", "").PadLeft(10, '0').Trim());

                        sw.Close();
                        MessageBox.Show("Positive Pay ManualChecks Payroll file created successfully.");
                    }//2nd if end

                    //Voided Checks Only Option
                    if (radioButton3.Checked == true)
                    { //3rd if
                        //string PayrollSql = "SELECT a.check_nbr DOC_NUM, a.check_date PMT_DT, c.bnk_acct_nbr BNK_ACCT_NUM, a.check_net AMT_PD, a.check_ID " +
                        //                    " FROM lawprod.paymastr a, lawprod.bankfile c " +
                        //                    " WHERE a.bank_code = c.bank_code " +
                        //                    " AND RTRIM(c.bnk_acct_nbr) = " + this.textBox1.Text + " " +
                        //                    " AND a.check_date BETWEEN to_date('" + this.dateTimePicker1.Text + "','mm/dd/yyyy') AND to_date('" + this.dateTimePicker2.Text + "','mm/dd/yyyy') AND a.check_type <> 'R' AND A.check_net > 0" +
                        //                    " ORDER BY a.check_nbr, a.check_date, c.bnk_acct_nbr";

                        string PayrollSql = "SELECT   a.check_nbr DOC_NUM, a.check_date PMT_DT," +
                                            "c.bnk_acct_nbr BNK_ACCT_NUM, '370' as TRAN_CODE, ABS (a.check_net)*100 AMT_PD, " +
                                            "trim(b.last_name)||','||' '||trim(b.first_name)||' '||trim(b.middle_init) PAYEE" +
                                            " FROM lawprod.paymastr a, lawprod.bankfile c, lawprod.employee b" +
                                            " WHERE a.bank_code = c.bank_code" +
                                            " AND RTRIM (c.bnk_acct_nbr) = " + this.textBox1.Text + " " +
                                            " AND a.employee = b.employee" +
                                            " AND a.check_date BETWEEN to_date('" + this.dateTimePicker1.Text + "','mm/dd/yyyy') AND to_date('" + this.dateTimePicker2.Text + "','mm/dd/yyyy') AND a.check_type = 'R' AND a.check_net < '0'" +
                                            " AND a.check_nbr IN (SELECT check_nbr FROM lawprod.paymastr WHERE check_type = 'M' AND check_net > '0')";




                        OracleDataAdapter da = new OracleDataAdapter(PayrollSql, conn);
                        DataSet ds = new DataSet();
                        da.Fill(ds, "tblVoided");
                        conn.Close();

                        //Open the file directly using StreamWriter
                        //StreamWriter sw = new StreamWriter("C:\\Program Files\\HoTTApps\\ARP\\Payroll\\" + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");
                        string PRFile = configurationAppSettings.GetValue("PRFilePath", typeof(string)).ToString();
                        //StreamWriter sw = new StreamWriter("C:\\Program Files\\HoTTApps\\ARP\\Payroll\\" + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");
                        //StreamWriter sw = new StreamWriter(PRFile + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");
                        StreamWriter sw = new StreamWriter(PRFile + "ARPW" + DateTime.Now.ToString("MMddhhmmtt") + ".txt");


                        //sw.WriteLine("Executer:Nina Chokshi");
                        sw.WriteLine("$$ADD ID=HOTUSRF1 BID='DII4984037531'");
                        sw.WriteLine("WRRECV        4984037531                                                        ");
                        double amt = 0;                     
                        foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
                        {
                            sw.WriteLine("{0}{1}{2}{3}{4}{5}",
                                dr["DOC_NUM"].ToString().Trim().PadLeft(10, '0'),
                                DateTime.Parse(dr["PMT_DT"].ToString().Trim()).ToString("MMddyy"),
                                dr["BNK_ACCT_NUM"].ToString().Trim(),
                                dr["TRAN_CODE"].ToString().Trim(),
                                //dr["AMT_PD"].ToString().Replace(".", "").PadLeft(10, '0').Trim(),
                                dr["AMT_PD"].ToString().PadLeft(10, '0').Trim(),
                                dr["PAYEE"].ToString().Trim());
                            amt += double.Parse(dr["AMT_PD"].ToString());
                        }
                        // sw.WriteLine("&              " + ds.Tables[0].Rows.Count.ToString().PadLeft(5, '0') + "   ");
                        sw.WriteLine("&              " + ds.Tables[0].Rows.Count.ToString().PadLeft(5, '0') + "   " + amt.ToString().Replace(".", "").PadLeft(10, '0').Trim());

                        sw.Close();
                        MessageBox.Show("Positive Pay VoidedChecks Payroll file created successfully.");
                    }//3rd if end



                }//end try

                catch (IOException ex)
                {
                    MessageBox.Show(ex.Message + " Problem creating Account Payable file");
                }
                finally
                {
                    conn.Close();
                    //MessageBox.Show("Closing the Oracle Connection");

                    //Reset cursor back to default position
                    Cursor.Current = Cursors.Default;
                }
            }

        }//end

        private void PRForm_Load(object sender, EventArgs e)
        {
            Show();
        }
    }
}