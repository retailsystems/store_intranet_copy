using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.Data.Sql;
using System.IO;
using System.Data;
using System.Runtime.InteropServices;
using HTError;
using System.Net.Mail;


namespace PositivePay
{
    class AccountPayable
    {
   
        static void Main(string[] args)
        {
            System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();

            string dbconn = configurationAppSettings.GetValue("ConnectionString", typeof(string)).ToString();
            string schema = configurationAppSettings.GetValue("Schema", typeof(string)).ToString();
            string EmailTo = configurationAppSettings.GetValue("APDistroTo", typeof(string)).ToString();
            string EmailFrom = configurationAppSettings.GetValue("APDistroFrom", typeof(string)).ToString();
            string APFile = configurationAppSettings.GetValue("APFilePath", typeof(string)).ToString() ;

            // Ensure that log files does not exist. Finance doesn't want to accumalte log file.
            File.Delete(APFile + "Production.log");
            File.Delete(APFile + "Debug.log");


            HTError.ErrorHandler errHandlar = new ErrorHandler(APFile+"Production.log", APFile+"Debug.log", "filter.hottopic.com", EmailFrom, EmailTo, HTError.LogLevel.Debug);

            errHandlar.EnableDebugLog = true;
            errHandlar.EnableProductionLog = true;
            errHandlar.EnableEmail = true;
            errHandlar.SendMailOnError = true;
           
            //OracleConnection conn = new OracleConnection("Data Source=PRD.HOTTOPIC.COM;Persist Security Info=True;User ID=financeinq;password=inqfinance;Unicode=True");
            OracleConnection conn = new OracleConnection(dbconn);
            try
            {
                conn.Open();
                Console.WriteLine("Creating Positve Pay AccountPayable file...");
                //Console.ReadLine();
                errHandlar.LogInformation("OracleDB connection Established successfully");

            }
            catch (OracleException ex)
              
            {
                //Console.WriteLine(ex.ToString());
                //Console.Error.WriteLine(ex.Message);
                errHandlar.LogError(ex, "problem with oracle connection");
                Environment.Exit(-1);
                
                
            }

			DataTable Accounts = new DataTable();

			try
			{
				string accountsql = "SELECT 'TD' company, bank_acct_nbr FROM " + schema + ".cbcashcode WHERE CASH_CODE = 'TAPC' ";
					//+ "union "
					//+ "SELECT 'HT' company, bank_acct_nbr FROM " + schema + ".cbcashcode WHERE CASH_CODE = 'APCD' ";

				errHandlar.LogInformation("Getting bank account numbers...");
				OracleDataAdapter oda = new OracleDataAdapter(accountsql, conn);
				oda.Fill(Accounts);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Following error occured while getting Bank Account Information:" + ex.ToString());
				errHandlar.LogError(ex, "Problem getting Bank Accounts");
				Environment.Exit(-1);
				conn.Close();
			}
           
            //Build and execute Account Payable Report Query
            //Open the file directly using StreamWriter
            try
            {
				foreach (DataRow datarow in Accounts.Rows)
				{
					string bank_account = datarow["bank_acct_nbr"].ToString().Trim();
					string company = datarow["company"].ToString();
					//Console.WriteLine("Building SQL Query");
					//Console.ReadLine();
					errHandlar.LogInformation("Building SQL Query String...");

					string APSql = "SELECT   a.trans_nbr doc_num, a.check_date pmt_dt,b.bank_acct_nbr bnk_acct_num, SUM (a.bank_chk_amt)*100 amt_pd,'320' AS void_seq," +
											"CASE " +
											" WHEN TRIM (a.remit_to_code) IS NOT NULL " +
											" THEN ( SELECT vendor_vname FROM " + schema + ".apvenloc d" +
											   " WHERE a.pay_vendor = d.vendor" +
											   " AND a.remit_to_code = d.location_code)" +
											" ELSE (SELECT vendor_vname FROM " + schema + ".apvenmast c" +
											  " WHERE a.pay_vendor = c.vendor)" +
											" END payee" +
								   " FROM " + schema + ".appayment a, " + schema + ".cbcashcode b" +
								   " WHERE a.cash_code = b.cash_code" +
								   " AND RTRIM (b.bank_acct_nbr) = '" + bank_account + "' " +
								   " AND check_date BETWEEN trunc(sysdate) AND trunc(sysdate) AND bank_inst_code ='SYS'" +
								   " GROUP BY a.trans_nbr, a.check_date, b.bank_acct_nbr, a.remit_to_code, a.pay_vendor" +
								   " HAVING SUM (a.bank_chk_amt) > 0" +
								   " UNION " +
								   "SELECT   a.trans_nbr doc_num, a.check_date pmt_dt,b.bank_acct_nbr bnk_acct_num, SUM (a.bank_chk_amt)*100 amt_pd,'370' AS void_seq," +
											"CASE " +
											" WHEN TRIM (a.remit_to_code) IS NOT NULL" +
											" THEN (SELECT vendor_vname FROM " + schema + ".apvenloc d" +
												  " WHERE a.pay_vendor = d.vendor" +
												  " AND a.remit_to_code = d.location_code)" +
											" ELSE (SELECT vendor_vname FROM " + schema + ".apvenmast c" +
												   " WHERE a.pay_vendor = c.vendor)" +
											" END payee" +
								  " FROM " + schema + ".appayment a, " + schema + ".cbcashcode b" +
								  " WHERE a.cash_code = b.cash_code" +
								  " AND RTRIM (b.bank_acct_nbr) = '" + bank_account + "' " +
								  " AND void_date BETWEEN trunc(sysdate-1) AND trunc(sysdate-1) AND bank_inst_code ='SYS' AND a.void_seq <> 0" +
								  " GROUP BY a.trans_nbr, a.check_date, b.bank_acct_nbr, a.remit_to_code,a.pay_vendor" +
								  " HAVING SUM (a.bank_chk_amt) > 0";

					//Console.WriteLine("SQL Query string has been created");
					//Console.Read();
					errHandlar.LogInformation("SQL Query String has been created successfully");
					OracleDataAdapter da = new OracleDataAdapter(APSql, conn);
					DataSet ds = new DataSet();
					da.Fill(ds, "APTable");


					//Open the file directly using StreamWriter
					//StreamWriter sw = new StreamWriter("C:\\Program Files\\HoTTApps\\ARP\\AP\\" + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");
					//StreamWriter sw = new StreamWriter(APFile + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");
					StreamWriter sw = new StreamWriter(APFile + "ARPW_" + company + "_AP_" + DateTime.Now.ToString("yyyyMMddhhmmtt") + ".txt");


					//sw.WriteLine("$$ADD ID=HTIUSRF1 BID='DII4758372973'");
					//sw.WriteLine("WRRECV        9600112644                                                        ");
					sw.WriteLine("*03" + "00259" + bank_account.PadLeft(15, '0') + "0" + string.Empty.PadLeft(61, ' '));
					//float amt = 0;
					double amt = 0;
					foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
					{

						sw.WriteLine("{0}{1}{2}{3}{4}{5}",
						dr["DOC_NUM"].ToString().Trim().PadLeft(10, '0'),
						DateTime.Parse(dr["PMT_DT"].ToString().Trim()).ToString("MMddyy"),
						dr["BNK_ACCT_NUM"].ToString().Trim().PadLeft(15,'0'),
						dr["void_seq"].ToString().Trim(),
						dr["AMT_PD"].ToString().Replace(".", "").PadLeft(10, '0').Trim(),
						dr["PAYEE"].ToString().Trim()
					   );
						amt += double.Parse(dr["AMT_PD"].ToString());
					}
					sw.WriteLine("&              " + ds.Tables[0].Rows.Count.ToString().PadLeft(7, '0') + "   " + amt.ToString().Replace(".", "").PadLeft(13, '0').Trim());

					sw.Close();
					errHandlar.LogInformation("Account Payable file created successfully");
				}

                //SuccessEmail sEmail = new SuccessEmail();
                //sEmail.Email(true,false);
                Environment.Exit(0);
              
            }
            catch (IOException ex)
            {
                //Console.WriteLine(ex.Message + " Problem creating Account Payable file");
                //Console.Error.WriteLine(ex.Message);
                Console.WriteLine("Following error occured while creating Account Payable file:" + ex.ToString());
                errHandlar.LogError(ex, "Problem creating AccountPayable file");
                Environment.Exit(-1);
               

            }
            finally
            {
                //Closing Oracle DB connection
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                //Console.WriteLine("Closing the Oracle Connection");
                //Console.ReadLine();
                //errHandlar.LogInformation("Oracle connection closed successfully");
                
            }
        }
    }
}
