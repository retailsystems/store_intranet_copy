using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using HTError;

namespace PositivePay
{
    public class AccountsPayableFile
	{
		#region Public Members
		public OracleConnection DatabaseConnection;
		public ErrorHandler ErrorHandler;
		public string FilePath;

		public string Schema;
		public string BankAccount;
		public string Company;
		public DateTime StartDate;
		public DateTime EndDate;
		#endregion Public Members

		#region Constructors
		public AccountsPayableFile()
		{
			Schema = string.Empty;
			BankAccount = string.Empty;
			Company = string.Empty;

			StartDate = DateTime.MinValue;
			EndDate = DateTime.MinValue;
		}
		#endregion

		#region Public Methods
		public void CreateFile()
        {
			string startdate1 = StartDate == DateTime.MinValue ? "trunc(sysdate)" : StartDate.ToString("'dd-MMM-yyyy'");
			string enddate1 = EndDate == DateTime.MinValue ? "trunc(sysdate)" : EndDate.ToString("'dd-MMM-yyyy'");

			string startdate2 = StartDate == DateTime.MinValue ? "trunc(sysdate-1)" : startdate1;
			string enddate2 = EndDate == DateTime.MinValue ? "trunc(sysdate-1)" : enddate1;

            //Build and execute Account Payable Report Query
            //Open the file directly using StreamWriter
			ErrorHandler.LogInformation("Building SQL Query String...");

			string APSql = "SELECT   a.trans_nbr doc_num, a.check_date pmt_dt,b.bank_acct_nbr bnk_acct_num, SUM (a.bank_chk_amt)*100 amt_pd,'320' AS void_seq," +
									"CASE " +
									" WHEN TRIM (a.remit_to_code) IS NOT NULL " +
									" THEN ( SELECT vendor_vname FROM " + Schema + ".apvenloc d" +
									   " WHERE a.pay_vendor = d.vendor" +
									   " AND a.remit_to_code = d.location_code)" +
									" ELSE (SELECT vendor_vname FROM " + Schema + ".apvenmast c" +
									  " WHERE a.pay_vendor = c.vendor)" +
									" END payee" +
						   " FROM " + Schema + ".appayment a, " + Schema + ".cbcashcode b" +
						   " WHERE a.cash_code = b.cash_code" +
						   " AND RTRIM (b.bank_acct_nbr) = '" + BankAccount + "' " +
						   " AND check_date BETWEEN " + startdate1 + " AND " + enddate1 + " AND bank_inst_code ='SYS'" +
						   " GROUP BY a.trans_nbr, a.check_date, b.bank_acct_nbr, a.remit_to_code, a.pay_vendor" +
						   " HAVING SUM (a.bank_chk_amt) > 0" +
						   " UNION " +
						   "SELECT   a.trans_nbr doc_num, a.check_date pmt_dt,b.bank_acct_nbr bnk_acct_num, SUM (a.bank_chk_amt)*100 amt_pd,'370' AS void_seq," +
									"CASE " +
									" WHEN TRIM (a.remit_to_code) IS NOT NULL" +
									" THEN (SELECT vendor_vname FROM " + Schema + ".apvenloc d" +
										  " WHERE a.pay_vendor = d.vendor" +
										  " AND a.remit_to_code = d.location_code)" +
									" ELSE (SELECT vendor_vname FROM " + Schema + ".apvenmast c" +
										   " WHERE a.pay_vendor = c.vendor)" +
									" END payee" +
						  " FROM " + Schema + ".appayment a, " + Schema + ".cbcashcode b" +
						  " WHERE a.cash_code = b.cash_code" +
						  " AND RTRIM (b.bank_acct_nbr) = '" + BankAccount + "' " +
						  " AND void_date BETWEEN " + startdate2 + " AND " + enddate2 + " AND bank_inst_code ='SYS' AND a.void_seq <> 0" +
						  " GROUP BY a.trans_nbr, a.check_date, b.bank_acct_nbr, a.remit_to_code,a.pay_vendor" +
						  " HAVING SUM (a.bank_chk_amt) > 0";

			ErrorHandler.LogInformation("SQL Query String has been created successfully");
			OracleDataAdapter da = new OracleDataAdapter(APSql, DatabaseConnection);
			DataSet ds = new DataSet();
			da.Fill(ds, "APTable");

			//Open the file directly using StreamWriter
			StreamWriter sw = new StreamWriter(FilePath + "ARPW_" + Company + "_AP_" + DateTime.Now.ToString("yyyyMMddhhmmtt") + ".txt");

			// Header
			//sw.WriteLine("$$ADD ID=HTIUSRF1 BID='DII4758372973'");
			sw.WriteLine("*03" + "00259" + BankAccount.PadLeft(15, '0') + "0" + string.Empty.PadLeft(61, ' '));

			// Details
			double amt = 0;
			foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
			{

				sw.WriteLine("{0}{1}{2}{3}{4}{5}",
				dr["DOC_NUM"].ToString().Trim().PadLeft(10, '0'),
				DateTime.Parse(dr["PMT_DT"].ToString().Trim()).ToString("MMddyy"),
				dr["BNK_ACCT_NUM"].ToString().Trim().PadLeft(15,'0'),
				dr["void_seq"].ToString().Trim(),
				dr["AMT_PD"].ToString().Replace(".", "").PadLeft(10, '0').Trim(),
				dr["PAYEE"].ToString().Trim()
			   );
				amt += double.Parse(dr["AMT_PD"].ToString());
			}

			// Footer
			sw.WriteLine("&              " + ds.Tables[0].Rows.Count.ToString().PadLeft(7, '0') + "   " + amt.ToString().Replace(".", "").PadLeft(13, '0').Trim());

			sw.Close();
			ErrorHandler.LogInformation("Account Payable file created successfully");
		}
		#endregion Public Methods
	}
}
