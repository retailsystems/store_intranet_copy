using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OracleClient;
using System.IO;

namespace PositivePayWebApp
{
    public enum CheckType
    {
        Manual = 1,
        Void
    }
    public partial class _Default : System.Web.UI.Page
    {
        string _userName;
        string dbconn = "";
        string schema = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //TabContainer1.Tabs[1].Visible = false;
            _userName = User.Identity.Name.ToUpper().Replace("NTDOMAIN\\","");
            dbconn = Application["LawsonDB"].ToString();
            schema = Application["DBSchema"].ToString();
            if (!schema.EndsWith(".")) schema += ".";
            
            Session["userName"] = _userName;
            msg1.Text = "";
            msg2.Text = "";

            if (!Page.IsPostBack)
            {
                lblUser.Text = "Welcome " + _userName;
                // Authenticate user 
                AuthenticateUser();
                string dt = System.DateTime.Now.ToString("MM/dd/yyyy");
                txtFrom.Text = dt;
                txtTo.Text = dt;
                txtPRFrom.Text = dt;
                txtPRTo.Text = dt;
                //populate bank acct #s
                FillAccountNum();
            }
        }
        private void FillAccountNum()
        {
            string sql = "";
            OracleConnection conn = new OracleConnection(dbconn);
            string APAcctNum = "";
            string PRAcctNum = "";
            try
            {
                
                if (Application["APAcctNum"] == null)
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }
                    sql = "SELECT bank_acct_nbr FROM " + schema + "cbcashcode WHERE CASH_CODE = 'APCD' AND rownum = 1";
                    OracleCommand cmd = new OracleCommand(sql, conn);
                    APAcctNum = cmd.ExecuteOracleScalar().ToString().Trim();
                    Application["APAcctNum"] = APAcctNum;
                }
                if (Application["PRAcctNum"] == null)
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }
                    sql = "SELECT BNK_ACCT_NBR from  " + schema + "bankfile WHERE BANK_CODE = 'WF'";
                    OracleCommand cmd = new OracleCommand(sql, conn);
                    PRAcctNum = cmd.ExecuteOracleScalar().ToString().Trim();
                    Application["PRAcctNum"] = PRAcctNum;
                }

                txtAcctNum.Text = Application["APAcctNum"].ToString();
                txtPRAcctNum.Text = Application["PRAcctNum"].ToString();
            }
            catch (Exception ex)
            {
                msg1.Text = "Error:" + ex.Message;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }
        private void AuthenticateUser()
        {
            //string userList = Application["AdminUserList"].ToString();
            if (Application["AdminUserList"].ToString().ToUpper().Contains(_userName))
            {
                Session["userRole"] = "ADMIN";
                
            }
            else if (Application["APUserList"].ToString().ToUpper().Contains(_userName))
            {
                Session["userRole"] = "AP";
                TabContainer1.Tabs[1].Visible = false;
                TabContainer1.Tabs[1].HeaderText = "";
                
            }
            else if (Application["PRUserList"].ToString().ToUpper().Contains(_userName))
            {
                Session["userRole"] = "PR";
                TabContainer1.Tabs[0].Visible = false;
                TabContainer1.Tabs[0].HeaderText = "";
            }
            else
            {
                Response.Redirect("UnauthorisedUser.htm");
            }
        }
        protected void BtnPR_Click(object sender, EventArgs e)
        {
            OracleConnection conn = new OracleConnection(dbconn);
            string PayrollSql = "";
            string msg;
            try
            {
                if (lstCheckType.SelectedValue.Equals("1"))
                {
                    PayrollSql = "SELECT   a.check_nbr DOC_NUM, a.check_date PMT_DT," +
                        " c.bnk_acct_nbr BNK_ACCT_NUM, '320' TRAN_CODE, (a.check_net*100) AMT_PD," +
                        " trim(b.last_name)||','||' '||trim(b.first_name)||' '||trim(b.middle_init) PAYEE" +
                        " FROM " + schema + "paymastr a, " + schema + "bankfile c, " + schema + "employee b" +
                        " WHERE a.bank_code = c.bank_code" +
                        " AND RTRIM (c.bnk_acct_nbr) = '" + txtPRAcctNum.Text + "' " +
                        " AND a.employee = b.employee" +
                        " AND a.check_date BETWEEN to_date('" + txtPRFrom.Text + "','mm/dd/yyyy') AND to_date('" + txtPRTo.Text + "','mm/dd/yyyy') AND a.check_type = 'M' AND a.check_net > '0'";

                    msg = "Positive Pay ManualChecks Payroll file created successfully.";
                }
                else
                {
                    PayrollSql = "SELECT   a.check_nbr DOC_NUM, a.check_date PMT_DT," +
                       "c.bnk_acct_nbr BNK_ACCT_NUM, '370' as TRAN_CODE, ABS (a.check_net)*100 AMT_PD, " +
                       "trim(b.last_name)||','||' '||trim(b.first_name)||' '||trim(b.middle_init) PAYEE" +
                       " FROM " + schema + "paymastr a, " + schema + "bankfile c, " + schema + "employee b" +
                       " WHERE a.bank_code = c.bank_code" +
                       " AND RTRIM (c.bnk_acct_nbr) = '" + txtPRAcctNum.Text + "' " +
                       " AND a.employee = b.employee" +
                       " AND a.check_date BETWEEN to_date('" + txtPRFrom.Text + "','mm/dd/yyyy') AND to_date('" + txtPRTo.Text + "','mm/dd/yyyy') AND a.check_type = 'R' AND a.check_net < '0'" +
                       " AND a.check_nbr IN (SELECT check_nbr FROM " + schema + "paymastr WHERE check_type = 'M' AND check_net > '0')";

                     msg = "Positive Pay VoidedChecks Payroll file created successfully.";
                }
                conn.Open();
                OracleDataAdapter da = new OracleDataAdapter(PayrollSql, conn);
                DataSet ds = new DataSet();
                da.Fill(ds, "tblVoided");
               

                //Open the file directly using StreamWriter
                //StreamWriter sw = new StreamWriter("C:\\Program Files\\HoTTApps\\ARP\\Payroll\\" + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");
                string PRFile = Application["PRFilePath"].ToString();
                //StreamWriter sw = new StreamWriter("C:\\Program Files\\HoTTApps\\ARP\\Payroll\\" + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");
                StreamWriter sw = new StreamWriter(PRFile + "ARPW" + DateTime.Now.ToString("MMddhhmmtt") + ".txt");


                //sw.WriteLine("Executer:Nina Chokshi");
                sw.WriteLine("$$ADD ID=HOTUSRF1 BID='DII4984037531'");
                sw.WriteLine("WRRECV        4984037531                                                        ");
                double amt = 0;
                foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
                {
                    sw.WriteLine("{0}{1}{2}{3}{4}{5}",
                        dr["DOC_NUM"].ToString().Trim().PadLeft(10, '0'),
                        DateTime.Parse(dr["PMT_DT"].ToString().Trim()).ToString("MMddyy"),
                        dr["BNK_ACCT_NUM"].ToString().Trim(),
                        dr["TRAN_CODE"].ToString().Trim(),
                        //dr["AMT_PD"].ToString().Replace(".", "").PadLeft(10, '0').Trim(),
                        dr["AMT_PD"].ToString().PadLeft(10, '0').Trim(),
                        dr["PAYEE"].ToString().Trim());
                    amt += double.Parse(dr["AMT_PD"].ToString());
                }
                // sw.WriteLine("&              " + ds.Tables[0].Rows.Count.ToString().PadLeft(5, '0') + "   ");
                sw.WriteLine("&              " + ds.Tables[0].Rows.Count.ToString().PadLeft(5, '0') + "   " + amt.ToString().Replace(".", "").PadLeft(10, '0').Trim());

                sw.Close();
                msg2.Text = msg;
            }
            catch (Exception ex)
            {
                msg2.Text = "Error:" + ex.Message;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }
        protected void BtnAP_Click(object sender, EventArgs e)
        {
            
            OracleConnection conn = new OracleConnection(dbconn);
            try
            {

                string APSql = "SELECT  /*+full(a)*/  a.trans_nbr doc_num, a.check_date pmt_dt,b.bank_acct_nbr bnk_acct_num, SUM (a.bank_chk_amt)*100 amt_pd,'320' AS void_seq," +
                                            "CASE " +
                                            " WHEN TRIM (a.remit_to_code) IS NOT NULL " +
                                            " THEN ( SELECT vendor_vname FROM " + schema + "apvenloc d" +
                                               " WHERE a.pay_vendor = d.vendor" +
                                               " AND a.remit_to_code = d.location_code)" +
                                            " ELSE (SELECT vendor_vname FROM " + schema + "apvenmast c" +
                                              " WHERE a.pay_vendor = c.vendor)" +
                                            " END payee" +
                                   " FROM " + schema + "appayment a, " + schema + "cbcashcode b" +
                                   " WHERE a.cash_code = b.cash_code" +
                                   " AND RTRIM(b.bank_acct_nbr) ='" + txtAcctNum.Text + "'" +
                                   " AND check_date BETWEEN to_date('" + txtFrom.Text + "','mm/dd/yyyy') AND to_date('" + txtTo.Text + "','mm/dd/yyyy') AND bank_inst_code ='SYS'" +
                                   " GROUP BY a.trans_nbr, a.check_date, b.bank_acct_nbr, a.remit_to_code, a.pay_vendor" +
                                   " HAVING SUM (a.bank_chk_amt) > 0" +
                                   " UNION ALL " +
                                   " SELECT   /*+full(a)*/ a.trans_nbr doc_num, a.check_date pmt_dt,b.bank_acct_nbr bnk_acct_num, SUM (a.bank_chk_amt)*100 amt_pd,'370' AS void_seq," +
                                            "CASE " +
                                            " WHEN TRIM (a.remit_to_code) IS NOT NULL" +
                                            " THEN (SELECT vendor_vname FROM " + schema + "apvenloc d" +
                                                  " WHERE a.pay_vendor = d.vendor" +
                                                  " AND a.remit_to_code = d.location_code)" +
                                            " ELSE (SELECT vendor_vname FROM " + schema + "apvenmast c" +
                                                   " WHERE a.pay_vendor = c.vendor)" +
                                            " END payee" +
                                  " FROM " + schema + "appayment a, " + schema + "cbcashcode b" +
                                  " WHERE a.cash_code = b.cash_code" +
                                  " AND RTRIM(b.bank_acct_nbr) = '" + txtAcctNum.Text + "'" +
                                  " AND void_date BETWEEN to_date('" + txtFrom.Text + "','mm/dd/yyyy')-1 AND to_date('" + txtTo.Text + "','mm/dd/yyyy')-1 AND bank_inst_code ='SYS' AND a.void_seq <> 0" +
                                  " GROUP BY a.trans_nbr, a.check_date, b.bank_acct_nbr, a.remit_to_code,a.pay_vendor" +
                                  " HAVING SUM (a.bank_chk_amt) > 0";

                //MessageBox.Show("SQL Query string has been created");
                conn.Open();
                OracleDataAdapter da = new OracleDataAdapter(APSql, conn);
                DataSet ds = new DataSet();
                da.Fill(ds, "APTable");


                //Open the file directly using StreamWriter
                string APFile = Application["APFilePath"].ToString();
                //StreamWriter sw = new StreamWriter("C:\\Program Files\\HoTTApps\\ARP\\AP\\" + "ARPW" + DateTime.Now.ToString("MMddHHtt") + ".txt");
                StreamWriter sw = new StreamWriter(APFile + "ARPW" + DateTime.Now.ToString("MMddhhmmtt") + ".txt");


                sw.WriteLine("$$ADD ID=HTIUSRF1 BID='DII4758372973'");
                sw.WriteLine("WRRECV        9600112644                                                        ");
                //float amt = 0;
                double amt = 0;
                foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
                {

                    sw.WriteLine("{0}{1}{2}{3}{4}{5}",
                    dr["DOC_NUM"].ToString().Trim().PadLeft(10, '0'),
                    DateTime.Parse(dr["PMT_DT"].ToString().Trim()).ToString("MMddyy"),
                    dr["BNK_ACCT_NUM"].ToString().Trim(),
                    dr["void_seq"].ToString().Trim(),
                    dr["AMT_PD"].ToString().Replace(".", "").PadLeft(10, '0').Trim(),
                    dr["PAYEE"].ToString().Trim()
                   );
                    //amt += float.Parse(dr["AMT_PD"].ToString());
                    amt += (double.Parse(dr["AMT_PD"].ToString()) / 100);
                    //MessageBox.Show("test:" + amt);
                }
                sw.WriteLine("&              " + ds.Tables[0].Rows.Count.ToString().PadLeft(5, '0') + "   " + amt.ToString().Replace(".", "").PadLeft(10, '0').Trim());

                sw.Close();
                msg1.Text = "Positive Pay AccountPayable file created successfully.";
            }
            catch (Exception ex)
            {
                msg1.Text = "Error:" + ex.Message;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

        }

    }
}
