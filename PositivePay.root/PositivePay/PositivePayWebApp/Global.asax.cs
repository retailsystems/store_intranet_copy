using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace PositivePayWebApp
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

            ConnectionStringSettingsCollection connectionStrings = System.Web.Configuration.WebConfigurationManager.ConnectionStrings as ConnectionStringSettingsCollection;
            System.Collections.Specialized.NameValueCollection appSettings = System.Web.Configuration.WebConfigurationManager.AppSettings as System.Collections.Specialized.NameValueCollection;

            Application["LawsonDB"] = connectionStrings["LawsonDB"];
            //appsettings
            Application["APFilePath"] = appSettings["APFilePath"];
            if(!Application["APFilePath"].ToString().EndsWith("\\")) 
                Application["APFilePath"] = Application["APFilePath"].ToString() + "\\";
            Application["PRFilePath"] = appSettings["PRFilePath"];
            if(!Application["PRFilePath"].ToString().EndsWith("\\")) 
                Application["PRFilePath"] = Application["PRFilePath"].ToString() + "\\";
            Application["AdminUserList"] = appSettings["AdminUserList"];            
            Application["APUserList"] = appSettings["APUserList"];
            Application["PRUserList"] = appSettings["PRUserList"];
            Application["DBSchema"] = appSettings["DBSchema"];
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}