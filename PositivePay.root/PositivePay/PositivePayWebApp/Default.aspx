<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PositivePayWebApp._Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>HotTopic Inc. - Positive Pay</title>
    <link href="include/Styles.css" type="text/css" rel="stylesheet"/>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" AsyncPostBackTimeout="1200" />
    <div>
        <table width="520px" align="center">
        <tr>
            <td align="center">
         
        
        </td>
        </tr>
        <tr>
            <td align="right">         
                <asp:Label ID="lblUser" runat="server" CssClass="cellValueLeft"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        </tr>
        <tr>
        <td>
        <cc1:TabContainer ID="TabContainer1" runat="server" Width="500px" Height="300px">
            <cc1:TabPanel ID="TabAP" runat="server" HeaderText="Account Payable">                            
                <ContentTemplate>
                    <asp:UpdatePanel ID="updatePanel1" runat="server">
                        <ContentTemplate>
                            <table width="475px" border="0">
                                <tr>
                                    <td colspan="2" align="center" style="height:5px"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" class="pageCaption">
                                    ACCOUNT RECONCILEMENT(ARP_AP) - WELLS FARGO
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" align="center" style="height:5px"></td>
                                </tr>
                                 <tr>
                                    <td colspan="2" align="center" >
                                        <asp:ValidationSummary runat="server" ID="validGrp1"  ValidationGroup="validAP" Enabled="true" Visible="true" ShowSummary="true" EnableClientScript="true" DisplayMode="BulletList"  />
                                                                               
                                        <asp:Panel ID="Pnl1" runat="server">
                                            <asp:Label ID="msg1" runat="server" CssClass="reqStyle"></asp:Label> 
                                        </asp:Panel>
                                         <cc1:AnimationExtender ID="AnimationExtender1" runat="server" TargetControlID="pnl1">
                                        <Animations>
                                            <OnLoad>
                                                <FadeOut Duration="30" Fps="05" />
                                            </OnLoad>
                                        </Animations>
                                       </cc1:AnimationExtender>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" align="center" style="height:5px"></td>
                                </tr>
                                <tr>
                                    <td class="cellValueCaption" style="width:120px">
                                        Account #:</td>
                                    <td >
                                        <asp:TextBox ID="txtAcctNum" runat="server" CssClass="cellValueLeft"  Width="100px" ReadOnly="true" />
                                        <asp:RequiredFieldValidator runat="server" EnableClientScript="true" ID="req1" CssClass="reqStyle" ControlToValidate="txtAcctNum" ErrorMessage="Account # is required."  ValidationGroup="validAP" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                </tr>
                                  <tr>
                                    <td colspan="2" align="center" style="height:5px"></td>
                                </tr>
                                <tr>                                   
                                  
                                    <td class="cellValueCaption"  style="width:120px">
                                        From (mm/dd/yyyy):
                                    </td>
                                    <td class="cellValueCaption"  style="width:300px">
                                     <asp:TextBox ID="txtFrom" runat="server" CssClass="cellValueLeft" Width="100px" /> 
                                         <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFrom"/>
                                    
                                        To: <asp:TextBox ID="txtTo" runat="server"  CssClass="cellValueLeft" Width="100px" />
                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"/>
                                        <asp:RequiredFieldValidator runat="server" EnableClientScript="true" ID="req2" CssClass="reqStyle" ControlToValidate="txtFrom" ErrorMessage="From date is required." ValidationGroup="validAP" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator runat="server" EnableClientScript="true" ID="req3" CssClass="reqStyle" ControlToValidate="txtTo" ErrorMessage="To date is required." ValidationGroup="validAP" Display="None"></asp:RequiredFieldValidator>
                                         <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtFrom" Display="None" ErrorMessage="Invalid From date." Operator="DataTypeCheck" Type="Date" ValidationGroup="validAP"></asp:CompareValidator>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtTo" Display="None" ErrorMessage="Invalid To date." Operator="DataTypeCheck" Type="Date" ValidationGroup="validAP"></asp:CompareValidator>
                                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtTo" ControlToCompare="txtFrom" Display="None" ErrorMessage="To date must be greater than or equal to From date."  Operator ="GreaterThanEqual" Type="Date" ValidationGroup="validAP"></asp:CompareValidator>
                                        </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" style="height:10px"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Button ID="BtnAP" runat="Server" CausesValidation="true" ValidationGroup="validAP" Text="Create Report" OnClick="BtnAP_Click" CssClass="btnGrey"  />
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" align="center" style="height:10px"></td>
                                </tr>
                                 <tr>
                                    <td colspan="2" align="center">
                                        <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="updatePanel1" runat="server">
                                            <ProgressTemplate>
                                                <table align="center" style="border-color:#000000;" bgcolor="#000000" border="1" cellpadding="0" cellspacing="0" width="300">
                                                      <tr>
                                                        <td class="cellValueCaption" width="100%" height="100%" bgcolor="#CCCCCC" align="CENTER" valign="MIDDLE"> 
		                                                  <br/>Processing.  Please wait...<br/>
                                                          <img src="images/pl_wait.gif" border="1" width="75" height="15" name="pl_wait" id="pl_wait" alt="Please wait"><br/>
                                                          <br/>
                                                        </td>
                                                      </tr>
                                                 </table>
                                            </ProgressTemplate>
                                         </asp:UpdateProgress>
                                    </td>
                                </tr>
                            </table>  
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>               
            </cc1:TabPanel>
            <cc1:TabPanel ID="TabPR" runat="server" HeaderText="Pay Roll">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updatePanel2" runat="server">
                        <ContentTemplate>
                            <table width="475px" border="0">
                                <tr>
                                    <td colspan="2" align="center" style="height:5px"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" class="pageCaption">
                                    ACCOUNT RECONCILEMENT(ARP_PR) - WELLS FARGO
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" align="center" style="height:5px"></td>
                                </tr>
                                 <tr>
                                    <td colspan="2" align="center" >
                                        <asp:ValidationSummary runat="server" ID="ValidationSummary1"  ValidationGroup="validPR" Enabled="true" Visible="true" ShowSummary="true" EnableClientScript="true" DisplayMode="BulletList" />
                                        
                                        <asp:Panel ID="pnl2" runat="server">
                                            <asp:Label ID="msg2" runat="server" CssClass="reqStyle"></asp:Label>
                                        </asp:Panel>
                                         <cc1:AnimationExtender ID="Animation2" runat="server" TargetControlID="pnl2">
                                        <Animations>
                                            <OnLoad>
                                                <FadeOut Duration="30" Fps="05" />
                                            </OnLoad>
                                        </Animations>
                                       </cc1:AnimationExtender>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" align="center" style="height:5px"></td>
                                </tr>
                                <tr>
                                    <td class="cellValueCaption" style="width:120px">
                                        Check Type :</td>
                                    <td >
                                        <asp:DropDownList runat="server" ID="lstCheckType" CssClass="cellValueLeft"  Width="105px">
                                            <asp:ListItem Selected="true" Value="">--select one--</asp:ListItem>
                                            <asp:ListItem Value="1">Manual Checks</asp:ListItem>
                                            <asp:ListItem Value="2">Voided Checks</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator runat="server" EnableClientScript="true" ID="RequiredFieldValidator4" CssClass="reqStyle" ControlToValidate="lstCheckType" ErrorMessage="Check Type is required."  ValidationGroup="validPR" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                </tr>
                                  <tr>
                                    <td colspan="2" align="center" style="height:5px"></td>
                                </tr>
                                <tr>
                                    <td class="cellValueCaption" style="width:120px">
                                        Account #:</td>
                                    <td >
                                        <asp:TextBox ID="txtPRAcctNum" runat="server" CssClass="cellValueLeft"  Width="100px" ReadOnly="true" />
                                        <asp:RequiredFieldValidator runat="server" EnableClientScript="true" ID="RequiredFieldValidator1" CssClass="reqStyle" ControlToValidate="txtPRAcctNum" ErrorMessage="Account # is required."  ValidationGroup="validPR" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                </tr>
                                  <tr>
                                    <td colspan="2" align="center" style="height:5px"></td>
                                </tr>
                                <tr>                                   
                                  
                                    <td class="cellValueCaption"  style="width:120px">
                                        From (mm/dd/yyyy):
                                    </td>
                                    <td class="cellValueCaption">
                                     <asp:TextBox ID="txtPRFrom" runat="server" CssClass="cellValueLeft" Width="100px" /> 
                                         <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtPRFrom"/>
                                    
                                        To: <asp:TextBox ID="txtPRTo" runat="server"  CssClass="cellValueLeft" Width="100px" />
                                        <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtPRTo"/>
                                        <asp:RequiredFieldValidator runat="server" EnableClientScript="true" ID="RequiredFieldValidator2" CssClass="reqStyle" ControlToValidate="txtPRFrom" ErrorMessage="From date is required." ValidationGroup="validPR" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator runat="server" EnableClientScript="true" ID="RequiredFieldValidator3" CssClass="reqStyle" ControlToValidate="txtPRTo" ErrorMessage="To date is required." ValidationGroup="validPR" Display="None"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cv1" runat="server" ControlToValidate="txtPRFrom" Display="None" ErrorMessage="Invalid From date." Operator="DataTypeCheck" Type="Date" ValidationGroup="validPR"></asp:CompareValidator>
                                        <asp:CompareValidator ID="cv2" runat="server" ControlToValidate="txtPRTo" Display="None" ErrorMessage="Invalid To date." Operator="DataTypeCheck" Type="Date" ValidationGroup="validPR"></asp:CompareValidator>
                                        <asp:CompareValidator ID="cv3" runat="server" ControlToValidate="txtPRTo" ControlToCompare="txtPRFrom" Display="None" ErrorMessage="To date must be greater than or equal to From date."  Operator ="GreaterThanEqual" Type="Date" ValidationGroup="validPR"></asp:CompareValidator>
                                        </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" style="height:10px"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Button ID="BtnPR" runat="Server" CausesValidation="true" ValidationGroup="validPR" Text="Create Report" OnClick="BtnPR_Click" CssClass="btnGrey"  />
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" align="center" style="height:10px"></td>
                                </tr>
                                 <tr>
                                    <td colspan="2" align="center">
                                        <asp:UpdateProgress ID="UpdateProgress2" AssociatedUpdatePanelID="updatePanel2" runat="server">
                                            <ProgressTemplate>
                                                <table align="center" style="border-color:#000000;" bgcolor="#000000" border="1" cellpadding="0" cellspacing="0" width="300">
                                                      <tr>
                                                        <td class="cellValueCaption" width="100%" height="100%" bgcolor="#CCCCCC" align="CENTER" valign="MIDDLE"> 
		                                                  <br/>Processing.  Please wait...<br/>
                                                          <img src="images/pl_wait.gif" border="1" width="75" height="15" name="pl_wait" id="pl_wait" alt="Please wait"><br/>
                                                          <br/>
                                                        </td>
                                                      </tr>
                                                 </table>
                                            </ProgressTemplate>                                            
                                         </asp:UpdateProgress>
                                         
                                    </td>
                                </tr>
                            </table>  
                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </cc1:TabPanel>
        </cc1:TabContainer>
          
            </td>
        </tr>
        </table>
        </div>
        
    </form>
</body>
</html>

