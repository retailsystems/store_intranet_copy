using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.IO;
using System.Text;
using HTError;

namespace PositivePay
{
	class Program
	{
		static void Main(string[] args)
		{
			System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();

			string dbconn = configurationAppSettings.GetValue("ConnectionString", typeof(string)).ToString();
			string schema = configurationAppSettings.GetValue("Schema", typeof(string)).ToString();
			string EmailTo = configurationAppSettings.GetValue("APDistroTo", typeof(string)).ToString();
			string EmailFrom = configurationAppSettings.GetValue("APDistroFrom", typeof(string)).ToString();
			string APFile = configurationAppSettings.GetValue("APFilePath", typeof(string)).ToString();

			// Ensure that log files does not exist. Finance doesn't want to accumalte log file.
			File.Delete(APFile + "Production.log");
			File.Delete(APFile + "Debug.log");

			HTError.ErrorHandler errHandlar = new ErrorHandler(APFile + "Production.log", APFile + "Debug.log", "filter.hottopic.com", EmailFrom, EmailTo, HTError.LogLevel.Debug);

			errHandlar.EnableDebugLog = true;
			errHandlar.EnableProductionLog = true;
			errHandlar.EnableEmail = true;
			errHandlar.SendMailOnError = true;

			OracleConnection conn = new OracleConnection(dbconn);
			try
			{
				conn.Open();
				Console.WriteLine("Creating Positve Pay AccountPayable file...");
				errHandlar.LogInformation("OracleDB connection Established successfully");
			}
			catch (OracleException ex)
			{
				errHandlar.LogError(ex, "problem with oracle connection");
				Environment.Exit(-1);
			}

			DataTable Accounts = new DataTable();

			try
			{
				string accountsql = "SELECT 'TD' company, bank_acct_nbr FROM " + schema + ".cbcashcode WHERE CASH_CODE = 'TAPC' ";
				//+ "union "
				//+ "SELECT 'HT' company, bank_acct_nbr FROM " + schema + ".cbcashcode WHERE CASH_CODE = 'APCD' ";

				errHandlar.LogInformation("Getting bank account numbers...");
				OracleDataAdapter oda = new OracleDataAdapter(accountsql, conn);
				oda.Fill(Accounts);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Following error occured while getting Bank Account Information:" + ex.ToString());
				errHandlar.LogError(ex, "Problem getting Bank Accounts");
				Environment.Exit(-1);
				conn.Close();
			}

			try
			{
				foreach (DataRow datarow in Accounts.Rows)
				{
					AccountsPayableFile file = new AccountsPayableFile();
					file.DatabaseConnection = conn;
					file.BankAccount = datarow["bank_acct_nbr"].ToString().Trim();
					file.Company = datarow["company"].ToString();
					file.ErrorHandler = errHandlar;
					file.Schema = schema;
					file.FilePath = APFile;

					file.CreateFile();
					Environment.Exit(0);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Following error occured while creating Account Payable file:" + ex.ToString());
				errHandlar.LogError(ex, "Problem creating AccountPayable file");
				Environment.Exit(-1);
			}
			finally
			{
				if (conn.State == ConnectionState.Open)
				{
					conn.Close();
				}
			}
		}
	}
}
